unit uImportProdXls;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, Mask, ToolEdit, ComCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, JvExControls, JvDBLookup,
  JvExMask, JvToolEdit;

type
  TfrmImportProdXls = class(TForm)
    Table1: TADOTable;
    //FilenameEdit1: TFilenameEdit;
    ProgressBar1: TProgressBar;
    cbbPrograma: TJvDBLookupCombo;
    DataSource1: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TSpeedButton;
    Label3: TLabel;
    ButAjuda: TSpeedButton;
    RichEdit1: TRichEdit;
    ckbVale: TCheckBox;
    ckbQtd2: TCheckBox;
    ckbQtd3: TCheckBox;
    lblC: TLabel;
    FilenameEdit1: TJvFilenameEdit;
    AdoQry1: TADOQuery;
    AdoQry1prog_id: TIntegerField;
    AdoQry1nome: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure ButAjudaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportProdXls: TfrmImportProdXls;

implementation

uses DM, cartao_util, UMenu, UChangeLog;

{$R *.dfm}

procedure TfrmImportProdXls.FormCreate(Sender: TObject);
begin
  AdoQry1.Open;
  FilenameEdit1.Text:= '"'+FMenu.GetPersonalFolder+'\"';
end;

procedure TfrmImportProdXls.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Table1.Close;
  AdoQry1.Close;
end;

procedure TfrmImportProdXls.BitBtn1Click(Sender: TObject);
var
  path, strSql: String;
  prod_id: Integer;
  preco_prod, preco_prod18, preco_prod19: Currency;
begin
  if not FileExists(FilenameEdit1.FileName) then begin
    MsgErro('Arquivo n�o encontrado.');
    FilenameEdit1.SetFocus;
    screen.Cursor := crDefault;
    Exit;
  end;

  if cbbPrograma.KeyValue = 0 then begin
    MsgErro('Selecione um programa para importa��o.');
    cbbPrograma.KeyValue;
    screen.Cursor := crDefault;
    Exit;
  end;

  screen.Cursor  := crHourGlass;
  try
    path := '';
    path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+FilenameEdit1.Text+';';
    path := path + 'Extended Properties="Excel 12.0;HDR=YES;"';
    Table1.Active := False;

    Table1.ConnectionString := path;
    Table1.TableName:= 'prod$';
    Table1.Active := True;
  except on E:Exception do begin
      MsgInf('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
      screen.Cursor := crDefault;
      Exit;
    end;
  end;
  ProgressBar1.Position := 0;
  BitBtn1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
  if MsgSimNao('Deseja limpar os produtos do programa?') then
    DMConexao.ExecuteSql('UPDATE prog_prod SET ativo = ''N'' WHERE prog_id = '+cbbPrograma.KeyValue);
  while not Table1.Eof do begin
  lblC.Caption := inttostr(strtoint(lblC.Caption)+ 1);
  Application.ProcessMessages;
    if Length(Table1.FieldByName('BARRA').AsString) > 13 then begin
      MsgInf('O produto: '+Table1.FieldByName('PRODUTO').AsString+sLineBreak+
             'est� com C�digo de Barras maior que 13 caracteres e por isso'+sLineBreak+'n�o ser� verificado!');
    end else begin
      if Trim(Table1.FieldByName('BARRA').AsString) <> '' then begin
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select prod_id from barras where barras = '+QuotedStr(SoNumero(Table1.FieldByName('BARRA').AsString))+'');
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_ID');
          DMConexao.AdoQry.Open;
          prod_id := DMConexao.AdoQry.Fields[0].AsInteger;
          // cadastrando produto
          strSql:= 'insert into produtos (prod_id, apagado, descricao, preco_vnd, preco_custo) '+
                   'values ('+IntToStr(prod_id)+', ''N'', '''+iif(copy(Table1.FieldByName('PRODUTO').AsString,1,90)='',
                   'DESCRICAO NAO DEFINIDA',copy(Table1.FieldByName('PRODUTO').AsString,1,90))+
                   ''', '+FormatDimIB(ArredondaDin(Table1.FieldByName('BRUTO').AsCurrency))+
                   ', '+FormatDimIB(ArredondaDin(Table1.FieldByName('BRUTO').AsCurrency))+')';
          DMConexao.ExecuteSql(strSql);
          DMConexao.ExecuteSql('insert into barras (barras_id, barras, prod_id) values (NEXT VALUE FOR SBARRAS_ID,'''+SoNumero(Table1.FieldByName('BARRA').AsString)+
                         ''', '+IntToStr(prod_id)+')');
        end
        else
          prod_id := DMConexao.AdoQry.Fields[0].AsInteger;

        // inserindo produtos no programa
        if Table1.FieldByName('BRUTO').AsCurrency > 0 then
          preco_prod:= Table1.FieldByName('BRUTO').AsCurrency
        else begin
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('select preco_vnd from produtos where prod_id = '+IntToStr(prod_id)+'');
            DMConexao.AdoQry.Open;
            if DMConexao.AdoQry.IsEmpty then
              preco_prod:= DMConexao.AdoQry.Fields[0].AsCurrency
            else
              preco_prod := 0.00;
          end;
          //preco_prod:= DMConexao.ExecuteScalar('select preco_vnd from produtos where prod_id = '+IntToStr(prod_id),0.00);

        if Table1.FieldByName('BRUTO_18').AsCurrency > 0 then
          preco_prod18 := Table1.FieldByName('BRUTO_18').AsCurrency
        else
          preco_prod18 := preco_prod;

        if Table1.FieldByName('BRUTO_19').AsCurrency > 0 then
          preco_prod19 := Table1.FieldByName('BRUTO_19').AsCurrency
        else
          preco_prod19 := preco_prod;

        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT prod_id FROM prog_prod WHERE prod_id = '+IntToStr(prod_id)+' AND prog_id = '+cbbPrograma.KeyValue+'');
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then begin
          strSql:= 'INSERT INTO prog_prod (prod_id, prog_id, prc_unit, prc_unit_18, prc_unit_19, perc_desc, qtd_max, obrig_receita, ativo) '+
                   'VALUES ('+IntToStr(prod_id)+','+cbbPrograma.KeyValue+','+FormatDimIB(ArredondaDin(preco_prod))+
                   ','+FormatDimIB(ArredondaDin(preco_prod18))+','+FormatDimIB(ArredondaDin(preco_prod19))+
                   ','+FormatDimIB(ArredondaDin(Table1.FieldByName('PERCENTUAL').AsCurrency))+', 0, ''N'', ''S'') ';
        end else begin
          strSql:= 'UPDATE prog_prod SET prc_unit = '+FormatDimIB(ArredondaDin(preco_prod))+
                   ', prc_unit_18 = '+FormatDimIB(ArredondaDin(preco_prod18))+
                   ', prc_unit_19 = '+FormatDimIB(ArredondaDin(preco_prod19))+
                   ', perc_desc = '+FormatDimIB(ArredondaDin(Table1.FieldByName('PERCENTUAL').AsCurrency))+', ativo = ''S'' '+
                   ' WHERE prod_id = '+IntToStr(prod_id)+' AND prog_id = '+cbbPrograma.KeyValue;
        end;
        DMConexao.ExecuteSql(strSql);
        // inserindo produtos no vale desconto
        if ckbVale.Checked then begin
          //if DMConexao.ExecuteScalar('SELECT prod_id FROM vale_produtos WHERE prod_id = '+IntToStr(prod_id),0) = 0 then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT prod_id FROM vale_produtos WHERE prod_id = '+IntToStr(prod_id)+'');
          DMConexao.AdoQry.Open;
          if DMConexao.AdoQry.IsEmpty then begin
            strSql:= 'INSERT INTO vale_produtos (prod_id, vale_perc) '+
                     'VALUES ('+IntToStr(prod_id)+','+FormatDimIB(ArredondaDin(Table1.FieldByName('VALE').AsCurrency))+') ';
          end else begin
            strSql:= 'UPDATE vale_produtos SET vale_perc = '+FormatDimIB(ArredondaDin(Table1.FieldByName('VALE').AsCurrency))+
                     ' WHERE prod_id = '+IntToStr(prod_id);
          end;
          DMConexao.ExecuteSql(strSql);
        end;

        // inserindo produtos por quantidade 2
        if ckbQtd2.Checked then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT prod_id FROM DESC_PROG_PROD WHERE prod_id = '+IntToStr(prod_id)+' and prog_id = '+cbbPrograma.KeyValue+' and qtd = 2');
          DMConexao.AdoQry.Open;
          if DMConexao.AdoQry.IsEmpty then begin
          //if DMConexao.ExecuteScalar(',0) = 0 then begin
            strSql:= 'INSERT INTO DESC_PROG_PROD (prod_id, prog_id, qtd, perc_desc) '+
                     'VALUES ('+IntToStr(prod_id)+','+cbbPrograma.KeyValue+',2,'+FormatDimIB(ArredondaDin(Table1.FieldByName('QTD2').AsCurrency))+') ';
          end else begin
            strSql:= 'UPDATE DESC_PROG_PROD SET PERC_DESC = '+FormatDimIB(ArredondaDin(Table1.FieldByName('QTD2').AsCurrency))+
                     ' WHERE prod_id = '+IntToStr(prod_id)+' AND prog_id = '+cbbPrograma.KeyValue+' AND qtd = 2';
          end;
          DMConexao.ExecuteSql(strSql);
        end;

        // inserindo produtos por quantidade 3
        if ckbQtd3.Checked then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT prod_id FROM DESC_PROG_PROD WHERE prod_id = '+IntToStr(prod_id)+' and prog_id = '+cbbPrograma.KeyValue+' and qtd = 3');
          DMConexao.AdoQry.Open;
          if DMConexao.AdoQry.IsEmpty then begin
          //if DMConexao.ExecuteScalar('SELECT prod_id FROM DESC_PROG_PROD WHERE prod_id = '+IntToStr(prod_id)+' and prog_id = '+cbbPrograma.KeyValue+' and qtd = 3',0) = 0 then begin
            strSql:= 'INSERT INTO DESC_PROG_PROD (prod_id, prog_id, qtd, perc_desc) '+
                     'VALUES ('+IntToStr(prod_id)+','+cbbPrograma.KeyValue+',3,'+FormatDimIB(ArredondaDin(Table1.FieldByName('QTD3').AsCurrency))+') ';
          end else begin
            strSql:= 'UPDATE DESC_PROG_PROD SET PERC_DESC = '+FormatDimIB(ArredondaDin(Table1.FieldByName('QTD3').AsCurrency))+
                     ' WHERE prod_id = '+IntToStr(prod_id)+' AND prog_id = '+cbbPrograma.KeyValue+' AND qtd = 3';
          end;
          DMConexao.ExecuteSql(strSql);
        end;
      end;
    end;
    Table1.Next;
    ProgressBar1.Position:= ProgressBar1.Position + 1;
  end;
  Table1.Close;
  BitBtn1.Enabled := True;
  screen.Cursor := crDefault;
  MsgInf('Importa��o realizada com sucesso!');
end;

procedure TfrmImportProdXls.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

end.




