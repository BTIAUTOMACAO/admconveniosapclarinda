unit URelExtratoConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, Grids, DBGrids, {JvDBCtrl, ClipBrd,}
  StdCtrls, ComCtrls, Mask, JvToolEdit, DB, {JvMemDS,} ZAbstractRODataset,
  ZDataset, ToolEdit, JvCombobox, ClassImpressao, JvMemoryDataset,
  JvExDBGrids, JvDBGrid, JvExMask, frxClass, frxGradient, frxDBSet,
  ZAbstractDataset, Printers, DBClient, frxExportPDF, ShellApi, ADODB;

type
  TFRelExtratoConv = class(TF1)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Bevel1: TBevel;
    ButListaEmp: TButton;
    DataFecha: TJvDateEdit;
    CheckBaixados: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    but_MarcDem_For: TButton;
    But_MarcaT_For: TButton;
    But_DesmT_For: TButton;
    Panel3: TPanel;
    but_Abre_For: TButton;
    But_Fecha_For: TButton;
    DBGrid1: TJvDBGrid;
    TabConv: TTabSheet;
    Panel1: TPanel;
    ButMarcDesmConv: TButton;
    ButMarcaTodosConv: TButton;
    ButDesmTodosConv: TButton;
    BitBtn1: TBitBtn;
    JvDBGrid2: TJvDBGrid;
    DSEmpresas: TDataSource;
    DSConv: TDataSource;
    TipoRel: TRadioGroup;
    por_dia_fecha: TRadioButton;
    por_periodo: TRadioButton;
    datafin: TJvDateEdit;
    CheckEmp: TCheckBox;
    Panel4: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    ChKMostraChapa: TCheckBox;
    EdMsg: TEdit;
    Label1: TLabel;
    Bevel3: TBevel;
    lblEntregaNF: TLabel;
    pnlOutrasOpcoes: TPanel;
    rgpNFEntregue: TRadioGroup;
    Button2: TButton;
    ckNaoAgruparPorEmpresa: TCheckBox;
    ckQuebraPagina: TCheckBox;
    CKHistAuts: TCheckBox;
    Panel5: TPanel;
    Label14: TLabel;
    lblTotal: TLabel;
    dbContaC: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxConv: TfrxReport;
    dbConv: TfrxDBDataset;
    dbEmpresas: TfrxDBDataset;
    dbFornec: TfrxDBDataset;
    DSFornec: TDataSource;
    cdsEmpresas: TClientDataSet;
    cdsFornec: TClientDataSet;
    cdsConv: TClientDataSet;
    cdsContaC: TClientDataSet;
    cdsEmpresasEMPRES_ID: TIntegerField;
    cdsEmpresasNOME: TStringField;
    cdsEmpresasDATAFIN: TDateTimeField;
    cdsEmpresasDATAINI: TDateTimeField;
    cdsConvTOTAL: TFloatField;
    cdsConvTITULAR: TStringField;
    cdsConvCONV_ID: TIntegerField;
    cdsConvCHAPA: TFloatField;
    cdsConvEMPRES_ID: TIntegerField;
    cdsFornecCRED_ID: TIntegerField;
    cdsFornecFANTASIA: TStringField;
    cdsFornecNOME: TStringField;
    cdsFornecSEGMENTO: TStringField;
    cdsContaCDEBITO: TFloatField;
    cdsContaCCREDITO: TFloatField;
    cdsContaCDATA: TDateTimeField;
    cdsContaCAUTORIZACAO_ID: TIntegerField;
    cdsContaCENTREG_NF: TStringField;
    cdsContaCDIGITO: TIntegerField;
    cdsContaCCRED_ID: TIntegerField;
    cdsContaCFORNECEDOR: TStringField;
    cdsContaCNOME: TStringField;
    cdsContaCHISTORICO: TStringField;
    cdsContaCNF: TIntegerField;
    dsCdsEmpresas: TDataSource;
    dsCdsFornec: TDataSource;
    dsCdsContaC: TDataSource;
    dsCdsConv: TDataSource;
    cdsContaCCONV_ID: TIntegerField;
    cdsContaCEMPRES_ID: TIntegerField;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    bntGerarPDF: TBitBtn;
    cdsContaCRECEITA: TStringField;
    QConv: TADOQuery;
    QConvtotal: TBCDField;
    QConvtitular: TStringField;
    QConvconv_id: TIntegerField;
    QConvchapa: TFloatField;
    QConvempres_id: TIntegerField;
    QContaC: TADOQuery;
    QFornec: TADOQuery;
    QForneccred_id: TIntegerField;
    QFornecfantasia: TStringField;
    QFornecnome: TStringField;
    QFornecsegmento: TStringField;
    QEmpresas: TADOQuery;
    MConv: TJvMemoryData;
    MEmpresas: TJvMemoryData;
    MConvconv_id: TIntegerField;
    MConvtitular: TStringField;
    MConvtotal: TFloatField;
    MConvchapa: TIntegerField;
    MConvempres_id: TIntegerField;
    MConvmarcado: TBooleanField;
    MEmpresasempres_id: TIntegerField;
    MEmpresasnome: TStringField;
    MEmpresasdatafin: TDateTimeField;
    MEmpresasdataini: TDateTimeField;
    MEmpresasmarcado: TBooleanField;
    MFornec: TJvMemoryData;
    MForneccred_id: TIntegerField;
    MFornecfantasia: TStringField;
    MFornecnome: TStringField;
    MFornecsegmento: TStringField;
    MFornecmarcado: TBooleanField;
    QContaCdebito: TBCDField;
    QContaCcredito: TBCDField;
    QContaCreceita: TStringField;
    QContaCdata: TDateTimeField;
    QContaCautorizacao_id: TIntegerField;
    QContaCentreg_nf: TStringField;
    QContaCdigito: TWordField;
    QContaCcred_id: TIntegerField;
    QContaCfornecedor: TStringField;
    QContaCnome: TStringField;
    QContaChistorico: TStringField;
    QContaCnf: TIntegerField;
    QEmpresasempres_id: TIntegerField;
    QEmpresasnome: TStringField;
    QEmpresasdatafin: TDateTimeField;
    QEmpresasdataini: TDateTimeField;
    procedure but_Abre_ForClick(Sender: TObject);
    procedure But_Fecha_ForClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButListaEmpClick(Sender: TObject);
    procedure but_MarcDem_ForClick(Sender: TObject);
    procedure But_MarcaT_ForClick(Sender: TObject);
    procedure But_DesmT_ForClick(Sender: TObject);
    procedure ButMarcDesmConvClick(Sender: TObject);
    procedure ButMarcaTodosConvClick(Sender: TObject);
    procedure ButDesmTodosConvClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabConvShow(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid2DblClick(Sender: TObject);
    procedure JvDBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure por_dia_fechaClick(Sender: TObject);
    procedure por_periodoClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure rgpNFEntregueClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure TipoRelClick(Sender: TObject);
    procedure ckNaoAgruparPorEmpresaClick(Sender: TObject);
    procedure frxConvPreview(Sender: TObject);
    procedure frxConvClosePreview(Sender: TObject);
    procedure frxConvGetValue(const VarName: String; var Value: Variant);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure cdsConvAfterScroll(DataSet: TDataSet);
    procedure cdsEmpresasAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    campo_ord : string;
    desc : Boolean;
    campo_ord2 : string;
    desc2 : Boolean;
    linha : integer;
    lpage : integer;
    empres_sel : String;
    bVisualizaTit, filtrarEntrNF, acumularAutor : Boolean;
    procedure RelTamFixo;
    procedure cabecalho_relfixo;
    procedure ConveniadosSel;
    procedure AbreConsulta(Conv_id:Integer);overload;
    procedure RelTotFornecSeq;
    procedure RelTotFornecSeqTit;
    procedure MostrarColunas;
    procedure VisualizarModelo6_7(modelo:integer);
    procedure ImprimirCabecaModelo6_7(Imp: TImpres);
    procedure ImprimirTitulo(Imp: TImpres);
    procedure incl(n:integer=1);
    procedure Saltar(NovaForma:Boolean; Count: Integer;Imp:TImpres);
    procedure SetText(NovaForma: Boolean; Texto: String; coluna: integer;Imp:TImpres);
    procedure ImprimirTotalEmpModelo6_7(Imp: TImpres;tot_emp_conf,tot_emp_nconf,tot_emp:currency);
    procedure ImprimirCorpoMoledo6_7(Imp: TImpres);
    procedure ImprimirTotalConvModelo6_7(Imp: TImpres; tot_conv_conf,
      tot_conv_nconf, tot_conv: currency);
    procedure AbreConsulta;overload;
  public
    { Public declarations }
    sl : TStrings;
    fornec_sel, conv_sel, conveniados : String;
    MostrarRelatorio : Boolean;
    Marcados : TStringList;
    lastRec  : boolean;
    recConv  : Integer;
    qtdConv  : Integer;
    procedure Fornecedores_Sel;
    procedure Empresas_Sel;
    procedure carregarDataSet;
    function GetConveniadosSelecionados : Integer;
  end;

var
  FRelExtratoConv: TFRelExtratoConv;

implementation

uses DM, impressao, Math, cartao_util;

{$R *.dfm}


procedure TFRelExtratoConv.but_Abre_ForClick(Sender: TObject);
begin
  QFornec.Open;
  MFornec.Open;
  MFornec.EmptyTable;
  MFornec.DisableControls;
  while not QFornec.Eof do begin
    MFornec.Append;
    MForneccred_id.AsInteger := QForneccred_id.AsInteger;
    MFornecnome.AsString := QFornecnome.AsString;
    MFornecfantasia.AsString := QFornecfantasia.AsString;
    MFornecsegmento.AsString := QFornecsegmento.AsString;
    MFornecMarcado.AsBoolean := False;
    MFornec.Post;
    QFornec.Next;
  end;
  MFornec.First;
  MFornec.EnableControls;
  fornec_sel := EmptyStr;
  DBGrid1.SetFocus;
end;

procedure TFRelExtratoConv.But_Fecha_ForClick(Sender: TObject);
begin
  QFornec.Close;
  MFornec.Close;
  fornec_sel := EmptyStr;
end;

procedure TFRelExtratoConv.FormCreate(Sender: TObject);
begin
inherited;
  MostrarRelatorio := False;
  DataFecha.Date := Date;
  datafin.Date   := Date;
  bVisualizaTit  := False;
  DMConexao.Config.Open;
  filtrarEntrNF := (DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'S');
  acumularAutor := (DMConexao.ConfigACUMULA_AUTOR_PROX_FECHA.AsString = 'S');
  DMConexao.Config.Close;
  if not filtrarEntrNF then begin
    Button2.Parent := Panel4;
    Button2.Top := ButMarcaDesmEmp.Top;
    pnlOutrasOpcoes.Visible := False;
    lblEntregaNF.Visible := False;
  end else rgpNFEntregueClick(nil);
  PageControl1.ActivePageIndex := 0;
  cdsEmpresas.CreateDataSet;
  cdsConv.CreateDataSet;
  cdsFornec.CreateDataSet;
  cdsContaC.CreateDataSet;



end;

procedure TFRelExtratoConv.ButListaEmpClick(Sender: TObject);
var IncBaixa : String;
begin
  MConv.Close;
  MFornec.Close;
  IncBaixa := 'N';
  if CheckBaixados.Checked then IncBaixa := 'S';
  QEmpresas.Close;
  QEmpresas.SQL.Clear;
  QEmpresas.SQL.Add(' Select empresas.empres_id, empresas.nome, ''N'' as marcado');
  if por_dia_fecha.Checked then begin
     QEmpresas.SQL.Add(', data_fecha as dataini ');
     QEmpresas.SQL.Add(', data_venc as datafin ');
  end else if por_periodo.Checked then begin
     QEmpresas.SQL.Add(', Cast('+QuotedStr(FormatDateTime('dd/mm/yyyy',datafin.Date))+' as datetime) as datafin ');
     QEmpresas.SQL.Add(', Cast('+QuotedStr(FormatDateTime('dd/mm/yyyy',DataFecha.Date))+' as datetime) as dataini ');
  end;
  QEmpresas.SQL.Add(' from empresas ');
  if por_dia_fecha.Checked then begin
     QEmpresas.SQL.Add(' join dia_fecha on dia_fecha.empres_id = empresas.empres_id ');
     QEmpresas.SQL.Add(' where dia_fecha.data_fecha = '''+FormatDateTime('dd/mm/yyyy',DataFecha.Date)+'''');
  end;
  QEmpresas.SQL.Add(' order by empresas.nome ');
  QEmpresas.Open;
  if QEmpresas.IsEmpty then begin
     msgInf('N�o foi encontrada nenhuma empresa com fechamento nesta data.');
     PageControl1.ActivePageIndex := 0;
     DataFecha.SetFocus;
  end;
  PageControl1.ActivePageIndex := 0;
  JvDBGrid1.SetFocus;

  QEmpresas.First;

  MEmpresas.Open;
  MEmpresas.EmptyTable;
  MEmpresas.DisableControls;
  while not QEmpresas.Eof do begin
      MEmpresas.Append;
      MEmpresasempres_id.AsInteger := QEmpresasempres_id.AsInteger;
      MEmpresasnome.AsString := QEmpresasnome.AsString;
      MEmpresasdataini.AsDateTime :=  QEmpresasdataini.AsDateTime;
      MEmpresasdatafin.AsDateTime := QEmpresasdatafin.AsDateTime;
      MEmpresasMarcado.AsBoolean := False;
      MEmpresas.Post;
      QEmpresas.Next;
  end;
  MEmpresas.First;
  MEmpresas.EnableControls;
  empres_sel := EmptyStr;
end;

procedure TFRelExtratoConv.but_MarcDem_ForClick(Sender: TObject);
begin
  if MFornec.IsEmpty then Exit;
  MFornec.Edit;
  MFornecMarcado.AsBoolean := not MFornecMarcado.AsBoolean;
  MFornec.Post;
  Fornecedores_Sel;
  //DMConexao.MarcaDesm(QConv);
end;

procedure TFRelExtratoConv.But_MarcaT_ForClick(Sender: TObject);
var marca : TBookmark;
begin
  if MFornec.IsEmpty then Exit;
  MFornec.DisableControls;
  marca := MFornec.GetBookmark;
  MFornec.First;
  while not MFornec.eof do begin
    MFornec.Edit;
    MFornecMarcado.AsBoolean := true;
    MFornec.Post;
    MFornec.Next;
  end;
  MFornec.GotoBookmark(marca);
  MFornec.FreeBookmark(marca);
  MFornec.EnableControls;
  Fornecedores_Sel;
//  DMConexao.MarcaDesmTodos(QConv,True);
end;

procedure TFRelExtratoConv.But_DesmT_ForClick(Sender: TObject);
var marca : TBookmark;
begin
  if MFornec.IsEmpty then Exit;
  MFornec.DisableControls;
  marca := MFornec.GetBookmark;
  MFornec.First;
  while not Mfornec.eof do begin
    MFornec.Edit;
    MFornecMarcado.AsBoolean := false;
    MFornec.Post;
    MFornec.Next;
  end;
  MFornec.GotoBookmark(marca);
  MFornec.FreeBookmark(marca);
  MFornec.EnableControls;
  Fornecedores_Sel;
//  DMConexao.MarcaDesmTodos(QConv,False);
end;

procedure TFRelExtratoConv.ButMarcDesmConvClick(Sender: TObject);
begin
if MConv.IsEmpty then Exit;
   MConv.Edit;
   MConvMarcado.AsBoolean := not MConvMarcado.AsBoolean;
   MConv.Post;
   ConveniadosSel;
  //DMConexao.MarcaDesm(QConv);
end;

procedure TFRelExtratoConv.ConveniadosSel;
var marca : TBookmark;
begin
  conveniados := EmptyStr;
  MConv.DisableControls;
  marca := MConv.GetBookmark;
  MConv.First;
  while not MConv.Eof do begin
    if MConvMARCADO.AsBoolean  = true then conveniados := conveniados+','+MConvCONV_ID.AsString;
    MConv.Next;
  end;
  conveniados := Copy(conveniados,2,Length(conveniados));
  MConv.GotoBookmark(marca);
  MConv.FreeBookmark(marca);
  MConv.EnableControls;
end;


procedure TFRelExtratoConv.ButMarcaTodosConvClick(Sender: TObject);
var marca : TBookmark;
begin
  if MConv.IsEmpty then Exit;
  MConv.DisableControls;
  marca := MConv.GetBookmark;
  MConv.First;
  while not MConv.eof do begin
    MConv.Edit;
    MConvMarcado.AsBoolean := true;
    MConv.Post;
    MConv.Next;
  end;
  MConv.GotoBookmark(marca);
  MConv.FreeBookmark(marca);
  MConv.EnableControls;
  ConveniadosSel;
  //DMConexao.MarcaDesmTodos(QConv,True);
end;

procedure TFRelExtratoConv.ButDesmTodosConvClick(Sender: TObject);
var marca : TBookmark;
begin
  if MConv.IsEmpty then Exit;
  MConv.DisableControls;
  marca := MConv.GetBookmark;
  MConv.First;
  while not MConv.eof do begin
    MConv.Edit;
    MConvMarcado.AsBoolean := false;
    MConv.Post;
    MConv.Next;
  end;
  MConv.GotoBookmark(marca);
  MConv.FreeBookmark(marca);
  MConv.EnableControls;
  ConveniadosSel;
  //DMConexao.MarcaDesmTodos(QConv,False);
end;

procedure TFRelExtratoConv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  //Empresas
  if key = vk_F5  then if PageControl1.ActivePageIndex = 0 then ButListaEmp.Click;
  if key = vk_F12 then if PageControl1.ActivePageIndex = 0 then ButMarcaDesmEmp.Click;
  if key = vk_F6  then if PageControl1.ActivePageIndex = 0 then ButMarcaTodasEmp.Click;
  if key = vk_F7  then if PageControl1.ActivePageIndex = 0 then ButDesmarcaTodosEmp.Click;

  //Conveniados
  if key = vk_F11 then if PageControl1.ActivePageIndex = 1 then ButMarcDesmConv.Click;
  if key = vk_F8  then if PageControl1.ActivePageIndex = 1 then ButMarcaTodosConv.Click;
  if key = vk_f9  then if PageControl1.ActivePageIndex = 1 then ButDesmTodosConv.Click;

  //Fornecedores
  if key = vk_F2  then if PageControl1.ActivePageIndex = 2 then but_MarcDem_ForClick(nil);
  if key = vk_F3  then if PageControl1.ActivePageIndex = 2 then But_MarcaT_ForClick(nil);
  if key = vk_F4  then if PageControl1.ActivePageIndex = 2 then But_DesmT_ForClick(nil);
end;

procedure TFRelExtratoConv.Fornecedores_Sel;
var marca : TBookmark;
begin
  fornec_sel := EmptyStr;
  marca := MFornec.GetBookmark;
  MFornec.DisableControls;
  MFornec.First;
  //QConv.Sql.Add(' and contacorrente.cred_id in ('+fornec_sel+')');
  while not MFornec.Eof do begin
    if MFornecMarcado.AsBoolean = true then
      fornec_sel := ' or contacorrente.cred_id = ' + fornec_sel + sLineBreak;
    MFornec.Next;
  end;
  if fornec_sel <> '' then
    fornec_sel := Copy(fornec_sel,Length(' or '),Length(fornec_sel));
  MFornec.GotoBookmark(marca);
  MFornec.FreeBookmark(marca);
  MFornec.EnableControls;
end;


procedure TFRelExtratoConv.TabConvShow(Sender: TObject);
var vlrtotal: Currency;
    s : String;
begin
  if not MEmpresas.Active then
    Exit;
  if (MEmpresas.RecordCount <= 0) or (MEmpresasEmpres_id.AsString = '') or (MEmpresas.IsEmpty) then Exit;
  if bVisualizaTit then begin
    vlrtotal:= 0;
    Screen.Cursor := crHourGlass;
    MEmpresas.First;
    s := '';
    while not MEmpresas.Eof do begin
      if MEmpresasMarcado.AsBoolean = true then
        s := s + ' or conveniados.empres_id = ' + MEmpresasEmpres_id.AsString + sLineBreak;
      MEmpresas.Next;
    end;
    s := Copy(s,Length(' or '),Length(S));
    if s <> '' then begin
      QConv.Close;
      QConv.Sql.Clear;
      QConv.Sql.Add(' Select Sum(Contacorrente.debito-contacorrente.credito) as total, ''N'' AS MARCADO, ');
      QConv.Sql.Add(' Conveniados.titular, Conveniados.conv_id, Conveniados.chapa, Conveniados.empres_id from contacorrente ');
      QConv.Sql.Add(' join conveniados on conveniados.conv_id = contacorrente.conv_id ');

      if por_dia_fecha.Checked then
        QConv.SQL.Add(' where contacorrente.data_fecha_emp = '+formatdataIB(DataFecha.Date))
      else
        QConv.Sql.Add(' where contacorrente.data between '+formatdataIB(MEmpresasDATAINI.AsDateTime)+' and '+formatdataIB(MEmpresasDATAFIN.AsDateTime));

      QConv.Sql.Add(' and (' + s + ')');
      if fornec_sel <> '' then
        QConv.Sql.Add(' and (' + fornec_sel+')');
      if not CheckBaixados.Checked then
        QConv.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
      DMConexao.Config.Open; //Abre o arquivo de configuracoes e tira da sql  o credenciado de baixa.
      if not DMConexao.ConfigCOD_CRED_BAIXA.IsNull then
         QConv.Sql.Add(' and contacorrente.cred_id <> '+DMConexao.ConfigCOD_CRED_BAIXA.AsString);
      DMConexao.Config.Close;
      if filtrarEntrNF then
        if rgpNFEntregue.ItemIndex = 1 then // Somente autorizacoes confirmadas
          QConv.SQL.Add(' and contacorrente.entreg_nf = ''S'' ')
        else if rgpNFEntregue.ItemIndex = 2 then  // Somente autorizacoes n�o confirmadas
          QConv.SQL.Add(' and coalesce(contacorrente.entreg_nf,''N'') = ''N'' ');
      QConv.Sql.Add(' group by Conveniados.titular, Conveniados.conv_id, Conveniados.chapa, Conveniados.empres_id  ');
      QConv.Sql.Add(' having Sum(Contacorrente.debito-contacorrente.credito) <> 0 ');
      QConv.Sql.Add(' order by Conveniados.empres_id, Conveniados.Titular');
      QConv.Sql.Text;
      QConv.Open;
    end;
  QConv.DisableControls;
  while not QConv.Eof do begin
    vlrtotal := vlrtotal + QConvTOTAL.AsCurrency;
    QConv.Next;
  end;
  QConv.EnableControls;
  QConv.First;

  MConv.Open;
  MConv.EmptyTable;
  MConv.DisableControls;
  while not QConv.Eof do begin
     MConv.Append;
     MConvconv_id.AsInteger := QConvconv_id.AsInteger;
     MConvchapa.AsInteger := QConvchapa.AsInteger;
     MConvtitular.AsString := QConvtitular.AsString;
     MConvempres_id.AsInteger := QConvempres_id.AsInteger;
     MConvtotal.AsFloat := QConvtotal.AsFloat;
     MConvmarcado.AsBoolean := False;
     MConv.Post;
     QConv.Next;
  end;
  MConv.First;
  MConv.EnableControls;
  conv_sel := EmptyStr;


  lblTotal.Caption:= FormatDinBR(vlrtotal);
  Screen.Cursor := crDefault;
  bVisualizaTit := False;
  end;
end;

procedure TFRelExtratoConv.JvDBGrid1DblClick(Sender: TObject);
begin
  ButMarcaDesmEmp.Click;
end;

procedure TFRelExtratoConv.JvDBGrid2TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
  var campo : string;
begin
  inherited;
  try
  if Pos(Field.FieldName,QConv.Sort) > 0 then begin
  if Pos(' DESC',QConv.Sort) > 0 then QConv.Sort := Field.FieldName
                          else QConv.Sort := Field.FieldName+' DESC';
  end
  else QConv.Sort := Field.FieldName;
  except
  end;

  QConv.First;

  MConv.Open;
  MConv.EmptyTable;
  MConv.DisableControls;
  while not QConv.Eof do begin
     MConv.Append;
     MConvconv_id.AsInteger := QConvconv_id.AsInteger;
     MConvchapa.AsInteger := QConvchapa.AsInteger;
     MConvtitular.AsString := QConvtitular.AsString;
     MConvempres_id.AsInteger := QConvempres_id.AsInteger;
     MConvtotal.AsFloat := QConvtotal.AsFloat;
     MConvmarcado.AsBoolean := False;
     MConv.Post;
     QConv.Next;
  end;
  MConv.First;
  MConv.EnableControls;
  conv_sel := EmptyStr;

end;

procedure TFRelExtratoConv.incl(n:integer=1);
begin
   inc(linha,n);
   inc(lpage,n);
   if lpage mod 66 = 0 then begin
      NewPage;
      incl(2);
      lpage := 0;
   end;
end;

procedure TFRelExtratoConv.BitBtn1Click(Sender: TObject);
var
  tot_conv, tot_deb, tot_cre, tot_emp : currency;
  tot_deb_conf, tot_deb_nconf, tot_cre_conf, tot_cre_nconf: currency;
  tot_emp_conf, tot_emp_nconf: currency;
  page, cod_conv: integer;
  bEjetar, bPrimeiraPagina: Boolean;
begin
  if Marcados = nil then
    Marcados := TStringList.Create
  else
    Marcados.Clear;
  if not MEmpresas.Active then
    MEmpresas.Open;
  if not MConv.Active then
    MConv.Open;
  cdsEmpresas.EmptyDataSet;
  cdsConv.EmptyDataSet;
  cdsFornec.EmptyDataSet;
  cdsContaC.EmptyDataSet;
  AbreConsulta;
  frxConv.ShowReport;
end;

procedure TFRelExtratoConv.JvDBGrid2DblClick(Sender: TObject);
begin
  ButMarcDesmConvClick(nil);
end;

procedure TFRelExtratoConv.JvDBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then ButMarcDesmConvClick(nil);
end;

procedure TFRelExtratoConv.RelTamFixo;
var
TamBloc : Integer;
BlocPag : Integer;
lbloco, i : Integer;
tot_conv, tot_deb, tot_cre : currency;
tot_conf, tot_nconf: currency;
procedure incl(n:integer=1);
begin
 inc(linha,n);
 inc(lbloco,n);
end;
begin
if not QConv.Active then Exit;
 Screen.Cursor := crHourGlass;
 BlocPag  := 0;
 linha    := 1;
 TamBloc  := 16;
 if TipoRel.ItemIndex = 2 then begin
    TamBloc := 33;
    linha   := 1;
 end;
 IniciaImpressao;
 While not QConv.Eof do begin
    if MConvMarcado.AsBoolean then begin
       AbreConsulta;
       if not QContaC.IsEmpty then begin
          tot_conv := 0;
          tot_deb  := 0;
          tot_cre  := 0;
          tot_conf  := 0;
          tot_nconf := 0;
          QContaC.First;
          Repeat
             inc(BlocPag);
             if ((TipoRel.ItemIndex = 2) and (BlocPag > 2)) or
                ((TipoRel.ItemIndex in [3,4]) and (BlocPag > 4)) then begin
                NewPage;
                BlocPag := 1;
                if TipoRel.ItemIndex = 2      then incl(-2);
                //if TipoRel.ItemIndex in [3,4] then incl(2);
             end;
             lbloco := 1;
             incl(2);
             if QConvEmpres_id.AsInteger <> QEmpresasEmpres_id.AsInteger then QEmpresas.Locate('Empres_id',QConvEmpres_id.AsVariant,[]);
             Imprime('Empresa: '+QEmpresasNome.AsString,linha,3);
             if por_dia_fecha.Checked then
                Imprime('Autorizacoes com Fechamento em '+FormatDataBR(DataFecha.Date), Linha, IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91) + 7)
             else
                Imprime('Periodo de '+FormatDateTime('dd/mm/yyyy',QEmpresasDATAINI.AsDateTime)+' a '+FormatDateTime('dd/mm/yyyy',QEmpresasDATAFIN.AsDateTime),linha,IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91));
             incl;
             if ChKMostraChapa.Checked then
                Imprime('Titular: '+QConvchapa.AsString+' - '+QConvTITULAR.AsString,linha,3)
             else
                Imprime('Titular: '+QConvCONV_ID.AsString+' - '+QConvTITULAR.AsString,linha,3);
             if filtrarEntrNF then
               if rgpNFEntregue.ItemIndex = 1 then
                 Imprime('Somente autorizacoes confirmadas', linha, 91)
               else if rgpNFEntregue.ItemIndex = 2 then
                 Imprime('Somente autorizacoes nao confirmadas', linha, 91);
             incl;
             cabecalho_relfixo;
             incl;
             for i := 0 to (TamBloc - 7) do begin
                if TipoRel.ItemIndex <> 4 then begin
                   Imprime(FormatDateTime('dd/mm/yyyy',QContaC.FieldByName('DATA').AsDateTime),linha,3);
                   Imprime(Direita(QContaC.FieldByName('AUTORIZACAO_ID').AsString+PadL(QContaC.FieldByName('DIGITO').AsString,2,'0')  ,' ',11),linha,15);
                   Imprime(QContaC.FieldByName('NOME').AsString,linha,27);
                   Imprime(QContaC.FieldByName('FORNECEDOR').AsString,linha,70);
                   Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('DEBITO').AsCurrency),' ',8),linha,108);
                   Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('CREDITO').AsCurrency),' ',8),linha,119);
                   if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then
                   begin
                     Imprime(QContaC.FieldByName('ENTREG_NF').AsString, linha, 129);
                     if QContaC.FieldByName('ENTREG_NF').AsString = 'S' then
                       tot_conf  := tot_conf  + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency)
                     else
                       tot_nconf := tot_nconf + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);
                   end;
                end
                else begin
                   Imprime(QContaC.FieldByName('NOME').AsString,linha,5);
                   Imprime(QContaC.FieldByName('FORNECEDOR').AsString,linha,55);
                   Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('DEBITO').AsCurrency),' ',8),linha,108);
                   Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('CREDITO').AsCurrency),' ',8),linha,119);
                end;
                tot_deb  := tot_deb  + QContaC.FieldByName('DEBITO').AsCurrency;
                tot_cre  := tot_cre  + QContaC.FieldByName('CREDITO').AsCurrency;
                tot_conv := tot_conv + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);
                QContaC.Next;
                incl;
                if QContaC.Eof then Break;
             end;
             if not QContaC.Eof then begin
                Imprime('Continua',linha,110);
                incl(2);
                if TipoRel.ItemIndex in [3,4] then // Quatro por folha.
                   Imprime(Preenche('-','-',128),linha,3)
                else if ( ( TipoRel.ItemIndex = 2 ) and ( Odd(BlocPag) ) ) then begin // Dois por folha.
                   Imprime(Preenche('-','-',128),linha,3);
                   incl(2);
                end;
             end;
          until QContaC.Eof;
          if lbloco < TamBloc then incl(TamBloc-lbloco);  //Inclui linhas em branco, para deixar do mesmo tamanho.
          Imprime(EdMsg.Text,linha,3);
          incl;
          if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0) and (TipoRel.ItemIndex in [2, 3])) then
          begin
            Imprime('Total confirmadas: ', linha, 13);
            Imprime(Direita(FormatFloat(',0.00', tot_conf), ' ', 12), linha, 32);
            Imprime('Total nao confirmadas: ', linha, 49);
            Imprime(Direita(FormatFloat(',0.00', tot_nconf), ' ', 12), linha, 72);
          end;
          Imprime('Total a Pagar: ',linha, 89);
          Imprime(Direita(FormatFloat('###,###,##0.00',tot_conv),' ', 12),linha, 104);
          incl(2);
          if TipoRel.ItemIndex in [3,4] then // Quatro por folha.
             Imprime(Preenche('-','-',128),linha,3)
          else if (( TipoRel.ItemIndex = 2 ) and ( Odd(BlocPag) ) ) then begin // Dois por folha.
             Imprime(Preenche('-','-',128),linha,3);
             incl(2);
          end;
       end;
    end;
    QConv.Next;
 end;
 if FileExists('rel1') then DeleteFile('rel1');
 Imprimir(False,'rel1');
 Screen.Cursor := crDefault;
 WinExec('visualizador.exe rel1 132',SW_SHOW);
end;

procedure TFRelExtratoConv.AbreConsulta;
begin
   QContaC.Close;
   QContaC.Sql.Clear;
   if (TipoRel.ItemIndex <> 4) and (TipoRel.ItemIndex <> 5) then begin
      QContaC.Sql.Add(' select  contacorrente.debito,contacorrente.credito, coalesce(contacorrente.receita,''N'') receita,');
      QContaC.Sql.Add(' contacorrente.data, contacorrente.autorizacao_id, contacorrente.entreg_nf, contacorrente.digito, ');
      QContaC.Sql.Add(' contacorrente.cred_id, credenciados.fantasia as fornecedor, cartoes.nome, contacorrente.historico ');
//      if TipoRel.ItemIndex in [6,7] then
         QContaC.Sql.Add(', contacorrente.nf ');
      QContaC.Sql.Add(' from contacorrente ');
      QContaC.Sql.Add(' join credenciados on credenciados.cred_id = contacorrente.cred_id ');
      QContaC.Sql.Add(' join cartoes on cartoes.cartao_id = contacorrente.cartao_id ');
      QContaC.Sql.Add(' join conveniados on conveniados.conv_id = cartoes.conv_id');
      QContaC.Sql.Add(' where contacorrente.conv_id =:CONV_ID');
      QContaC.Sql.Add(' and contacorrente.empres_id =:EMPRES_ID');

      if por_dia_fecha.Checked then
        QContaC.SQL.Add(' and contacorrente.data_fecha_emp = ' + formatdataIB(DataFecha.Date))
      else
        QContaC.Sql.Add(' and contacorrente.data between '+formatdataIB(MEmpresasDATAINI.AsDateTime)+' and '+formatdataIB(MEmpresasDATAFIN.AsDateTime));

      DMConexao.Config.Open; //Abre o arquivo de configuracoes e tira da sql  o credenciado de baixa.
      if not DMConexao.ConfigCOD_CRED_BAIXA.IsNull then
         QContaC.Sql.Add(' and contacorrente.cred_id <> '+DMConexao.ConfigCOD_CRED_BAIXA.AsString);
      DMConexao.Config.Close;
      if not CheckBaixados.Checked then begin
         QContaC.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
      end;
      if fornec_sel <> '' then QContaC.Sql.Add(' and contacorrente.cred_id in ('+fornec_sel+')');
      QContaC.Sql.Add(' and (contacorrente.debito > 0 or contacorrente.credito > 0) ');
      if filtrarEntrNF then
        if rgpNFEntregue.ItemIndex = 1 then // Somente autorizacoes confirmadas
          QContaC.SQL.Add(' and contacorrente.entreg_nf = ''S'' ')
        else if rgpNFEntregue.ItemIndex = 2 then  // Somente autorizacoes nao confirmados
          QContaC.SQL.Add(' and (contacorrente.entreg_nf = ''N'' or contacorrente.entreg_nf is null) ');
      QContaC.Sql.Add(' order by contacorrente.data ');
   end else begin
      QContaC.Sql.Add(' select Sum(contacorrente.debito) as debito, Sum(contacorrente.credito) as credito, coalesce(contacorrente.receita,''N'') receita,');
      QContaC.Sql.Add(' contacorrente.cred_id, credenciados.fantasia as fornecedor, cartoes.nome, contacorrente.historico ');
      QContaC.Sql.Add(' from contacorrente ');
      QContaC.Sql.Add(' join credenciados on credenciados.cred_id = contacorrente.cred_id ');
      QContaC.Sql.Add(' join cartoes on cartoes.cartao_id = contacorrente.cartao_id ');
      QContaC.Sql.Add(' join conveniados on conveniados.conv_id = cartoes.conv_id');
      QContaC.Sql.Add(' where contacorrente.conv_id =:CONV_ID');
      QContaC.Sql.Add(' and contacorrente.empres_id =:EMPRES_ID');
      if por_dia_fecha.Checked then
        QContaC.SQL.Add(' and contacorrente.data_fecha_emp = '+formatdataIB(DataFecha.Date))
      else
        QContaC.Sql.Add(' and contacorrente.data between '+formatdataIB(QEmpresasDATAINI.AsDateTime)+' and '+formatdataIB(QEmpresasDATAFIN.AsDateTime));

      DMConexao.Config.Open; //Abre o arquivo de configuracoes e tira da sql  o credenciado de baixa.
      if not DMConexao.ConfigCOD_CRED_BAIXA.IsNull then
         QContaC.Sql.Add(' and contacorrente.cred_id <> '+DMConexao.ConfigCOD_CRED_BAIXA.AsString);
      DMConexao.Config.Close;
      if not CheckBaixados.Checked then begin
         QContaC.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
      end;
      if fornec_sel <> '' then QContaC.Sql.Add(' and contacorrente.cred_id in ('+fornec_sel+')');
      QContaC.Sql.Add(' and (contacorrente.debito > 0 or contacorrente.credito > 0) ');
      if filtrarEntrNF then
        if rgpNFEntregue.ItemIndex = 1 then // Somente autorizacoes confirmadas
          QContaC.SQL.Add(' and contacorrente.entreg_nf = ''S'' ')
        else if rgpNFEntregue.ItemIndex = 2 then  // Somente autorizacoes nao confirmadas
          QContaC.SQL.Add(' and (contacorrente.entreg_nf = ''N'' or contacorrente.entreg_nf is null) ');
      QContaC.Sql.Add(' group by contacorrente.cred_id, credenciados.fantasia, cartoes.nome, contacorrente.historico, contacorrente.receita');
      QContaC.Sql.Add(' order by credenciados.fantasia, contacorrente.historico');
      if QContaC.FindField('DATA') <> nil then
        QContaC.FindField('DATA').Free;
      if QContaC.FindField('ENTREG_NF') <> nil then
        QContaC.FindField('ENTREG_NF').Free;
   end;
   if QContaC.Parameters.FindParam('EMPRES_ID') = nil then
      QContaC.Parameters.CreateParameter('EMPRES_ID',ftInteger,pdInput,0,null);
   if QContaC.Parameters.FindParam('CONV_ID') = nil then
      QContaC.Parameters.CreateParameter('CONV_ID',ftInteger,pdInput,0,null);
   QContaC.SQL.Text;
   QContaC.Open;
   carregarDataSet;
end;

procedure TFRelExtratoConv.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_Return then JvDBGrid1DblClick(nil);
end;

procedure TFRelExtratoConv.por_dia_fechaClick(Sender: TObject);
begin
  por_dia_fecha.Checked := True;
  datafin.Visible   := False;
  label2.Caption    := 'Dia de Fechamento';
  ButListaEmp.Left  := 320;
  por_periodo.Checked := False;
  DataFecha.SetFocus;
  MostrarColunas;
end;

procedure TFRelExtratoConv.por_periodoClick(Sender: TObject);
begin
  por_periodo.Checked := True;
  datafin.Visible   := True;
  datafin.Date      := DataFecha.Date;
  label2.Caption    := 'Per�odo de Compra';
  ButListaEmp.Left  := 440;
  por_dia_fecha.Checked := False;
  DataFecha.SetFocus;
  MostrarColunas;
end;

procedure TFRelExtratoConv.MostrarColunas;
begin
  GridColByFieldName(JvDBGrid1,'DATAINI').Title.Caption := cartao_util.iif(por_periodo.Checked,'Data Inicial','Data Fecha');
  GridColByFieldName(JvDBGrid1,'DATAFIN').Title.Caption := cartao_util.iif(por_periodo.Checked,'Data Final','Data Venc');
  JvDBGrid1.Repaint;
  Application.ProcessMessages;
end;

procedure TFRelExtratoConv.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QEmpresas.Sort) > 0 then begin
  if Pos(' DESC',QEmpresas.Sort) > 0 then QEmpresas.Sort := Field.FieldName
                          else QEmpresas.Sort := Field.FieldName+' DESC';
  end
  else QEmpresas.Sort := Field.FieldName;
  except
  end;

  QEmpresas.First;

  MEmpresas.Open;
  MEmpresas.EmptyTable;
  MEmpresas.DisableControls;
  while not QEmpresas.Eof do begin
      MEmpresas.Append;
      MEmpresasempres_id.AsInteger := QEmpresasempres_id.AsInteger;
      MEmpresasnome.AsString := QEmpresasnome.AsString;
      MEmpresasdataini.AsDateTime :=  QEmpresasdataini.AsDateTime;
      MEmpresasdatafin.AsDateTime := QEmpresasdatafin.AsDateTime;
      MEmpresasMarcado.AsBoolean := False;
      MEmpresas.Post;
      QEmpresas.Next;
  end;
  MEmpresas.First;
  MEmpresas.EnableControls;
  empres_sel := EmptyStr;
end;

procedure TFRelExtratoConv.cabecalho_relfixo;
begin
  if (TipoRel.ItemIndex <> 4) and (TipoRel.ItemIndex <> 5) then begin
     Imprime('Data',linha,3);
     Imprime('Autorizacao',linha,15);
     Imprime('Nome Cartao',linha,27);
     Imprime('Estabelecimento',linha,70);
     Imprime('Debito',linha,110);
     Imprime('Credito',linha,120);
     if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then
       Imprime('Ent', linha, 128);
  end
  else begin
     Imprime('Nome Cartao',linha,5);
     Imprime('Estabelecimento',linha,55);
     Imprime('Debito',linha,110);
     Imprime('Credito',linha,120);
  end;
end;


Procedure TFRelExtratoConv.SetText(NovaForma:Boolean; Texto:String;coluna:integer;Imp:TImpres);
begin
   if NovaForma then
      Linha := Imp.AddLinha(Texto,coluna)
   else begin
      incl;
      Imprime(Texto,linha,coluna);
   end;
end;

Procedure TFRelExtratoConv.Saltar(NovaForma:Boolean; Count:Integer;Imp:TImpres);
begin
  if NovaForma then
     Imp.SaltarLinhas(Count)
  else
     incl(Count);
end;

procedure TFRelExtratoConv.ImprimirTitulo(Imp:TImpres);
var NovaForma : Boolean;
begin
  NovaForma := assigned(Imp);
  SetText(NovaForma, Direita(FormatDataBR(Date),' ',128), 1,Imp);
  SetText(NovaForma, DMConexao.AdmFANTASIA.AsString,4,Imp);
  Saltar(NovaForma,1,Imp);
  SetText(NovaForma, Centraliza('Extrato do Conveniado.',' ',128), 3,Imp);
  Saltar(NovaForma,1,Imp);
  if por_dia_fecha.Checked then
    SetText(NovaForma,Centraliza('Autorizacoes com Fechamento em ' + FormatDataBR(DataFecha.Date), ' ', 128), 3,Imp)
  else
    SetText(NovaForma,Centraliza('Periodo de '+FormatDataBR(QEmpresasDATAINI.AsDateTime)+' a '+FormatDataBR(QEmpresasDATAFIN.AsDateTime),' ',128),3,Imp);
  Saltar(NovaForma,1,Imp);

  if filtrarEntrNF then
    if rgpNFEntregue.ItemIndex = 1 then
       SetText(NovaForma,Centraliza('Somente autorizacoes confirmadas', ' ', 128), 3,Imp)
    else if rgpNFEntregue.ItemIndex = 2 then
      SetText(NovaForma,Centraliza('Somente autorizacoes nao confirmadas', ' ', 128), 3,Imp);
    Saltar(NovaForma,2,Imp);
end;

procedure TFRelExtratoConv.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  if MEmpresas.IsEmpty then Exit;
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := not MEmpresasMarcado.AsBoolean;
    MEmpresas.Post;
    Empresas_Sel;
  //DMConexao.MarcaDesm(QEmpresas);
end;

procedure TFRelExtratoConv.Empresas_Sel;
var marca : TBookmark;
begin
  empres_sel := EmptyStr;
  marca := MEmpresas.GetBookmark;
  MEmpresas.DisableControls;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    if MEmpresasMarcado.AsBoolean then empres_sel := empres_sel + ','+MEmpresasempres_id.AsString;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  if empres_sel <> '' then empres_sel := Copy(empres_sel,2,Length(empres_sel));
  MEmpresas.EnableControls;
end;

procedure TFRelExtratoConv.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := true;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
  //DMConexao.MarcaDesmTodos(QEmpresas,True);
end;

procedure TFRelExtratoConv.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := false;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
  //DMConexao.MarcaDesmTodos(QEmpresas,False);
end;

procedure TFRelExtratoConv.Button2Click(Sender: TObject);
begin
  inherited;
  bVisualizaTit := True;
  TabConv.Show;
end;

procedure TFRelExtratoConv.rgpNFEntregueClick(Sender: TObject);
begin
  lblEntregaNF.Caption := 'Entrega NF: ' + rgpNFEntregue.Items[rgpNFEntregue.ItemIndex];
end;

procedure TFRelExtratoConv.RelTotFornecSeq;
var
lpage : Integer;
tot_deb, tot_cre, tot_conv : currency;
lFlag: Boolean;
procedure incl(n:integer=1);
begin
 inc(linha,n);
 inc(lpage,n);
 if lpage mod 66 = 0 then begin
    NewPage;
    lpage := 0;
    incl;
 end;
end;
begin
 Screen.Cursor := crHourGlass;
 linha := 1;
 lpage := 0;
 lFlag := True;
 QConv.First;
 IniciaImpressao;
 While not QConv.Eof do begin
    if MConvMarcado.AsBoolean then begin
       AbreConsulta;
       if not QContaC.IsEmpty then begin
          tot_conv  := 0;
          tot_deb   := 0;
          tot_cre   := 0;
          QContaC.First;
          if lFlag then
          begin
            incl;
            lFlag := False;
          end;
          incl;
          if QConvEmpres_id.AsInteger <> QEmpresasEmpres_id.AsInteger then QEmpresas.Locate('Empres_id',QConvEmpres_id.AsVariant,[]);
          Imprime('Empresa: '+QEmpresasNome.AsString,linha,3);
          if por_dia_fecha.Checked then
             Imprime('Autorizacoes com Fechamento em ' + FormatDataBR(DataFecha.Date), linha, IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91) + 7)
          else
             Imprime('Periodo de '+FormatDateTime('dd/mm/yyyy',QEmpresasDATAINI.AsDateTime)+' a '+FormatDateTime('dd/mm/yyyy',QEmpresasDATAFIN.AsDateTime),linha,IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91));
          incl;
          if ChKMostraChapa.Checked then
             Imprime('Titular: '+QConvchapa.AsString+' - '+QConvTITULAR.AsString,linha,3)
          else
             Imprime('Titular: '+QConvCONV_ID.AsString+' - '+QConvTITULAR.AsString,linha,3);
          if filtrarEntrNF then
            if rgpNFEntregue.ItemIndex = 1 then
               Imprime('Somente autorizacoes confirmadas', linha, 91)
            else if rgpNFEntregue.ItemIndex = 2 then
               Imprime('Somente autorizacoes nao confirmadas', linha, 91);
          incl;
          cabecalho_relfixo;
          incl;
          while not QContaC.Eof do
          begin
             Imprime(QContaC.FieldByName('NOME').AsString,linha,5);
             Imprime(QContaC.FieldByName('FORNECEDOR').AsString,linha,55);
             Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('DEBITO').AsCurrency),' ',8),linha,108);
             Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('CREDITO').AsCurrency),' ',8),linha,119);
             tot_deb  := tot_deb  + QContaC.FieldByName('DEBITO').AsCurrency;
             tot_cre  := tot_cre  + QContaC.FieldByName('CREDITO').AsCurrency;
             tot_conv := tot_conv + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);
             QContaC.Next;
             incl;
          end;
          Imprime(EdMsg.Text,linha,3);
          incl;
          Imprime('Total a Pagar: ',linha, 89);
          Imprime(Direita(FormatFloat('###,###,##0.00',tot_conv),' ', 12),linha, 104);
          incl;
          Imprime(Preenche('-','-',128),linha,3)
       end;
    end;
    QConv.Next;
 end;
 if FileExists('rel1') then DeleteFile('rel1');
 Imprimir(False,'rel1');
 Screen.Cursor := crDefault;
 WinExec('visualizador.exe rel1 132',SW_SHOW);
end;

procedure TFRelExtratoConv.DBGrid1DblClick(Sender: TObject);
begin
  but_MarcDem_ForClick(nil);
end;

procedure TFRelExtratoConv.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then but_MarcDem_ForClick(nil);
end;

procedure TFRelExtratoConv.DBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QFornec.Sort) > 0 then begin
  if Pos(' DESC',QFornec.Sort) > 0 then QFornec.Sort := Field.FieldName
                          else QFornec.Sort := Field.FieldName+' DESC';
  end
  else QFornec.Sort := Field.FieldName;
  except
  end;
end;

procedure TFRelExtratoConv.TipoRelClick(Sender: TObject);
begin
  inherited;
  ckNaoAgruparPorEmpresa.Visible := (Sender as TRadioGroup).ItemIndex = 5;
end;

procedure TFRelExtratoConv.AbreConsulta(Conv_id:Integer);
begin
  QContaC.Close;
  QContaC.Sql.Clear;
  QContaC.Sql.Add(' select Sum(contacorrente.debito) as debito, Sum(contacorrente.credito) as credito, coalesce(contacorrente.receita,''N'') receita, ');
  QContaC.Sql.Add(' conveniados.empres_id, empresas.nome as empresa, contacorrente.conv_id, conveniados.titular, conveniados.chapa, contacorrente.cred_id, credenciados.fantasia as fornecedor, cartoes.nome ');
  QContaC.Sql.Add(' from contacorrente ');
  QContaC.Sql.Add(' join credenciados on credenciados.cred_id = contacorrente.cred_id ');
  QContaC.Sql.Add(' join cartoes on cartoes.cartao_id = contacorrente.cartao_id ');
  QContaC.SQL.Add(' join conveniados on conveniados.conv_id = contacorrente.conv_id ');
  QContaC.Sql.Add(' where contacorrente.conv_id = :CONV_ID');
  QContaC.Sql.Add(' and contacorrente.empres_id = :EMPRES_ID');

  if por_dia_fecha.Checked  then
    QContaC.SQL.Add(' and contacorrente.data_fecha_emp = '+formatdataIB(DataFecha.Date))
  else
    QContaC.Sql.Add(' and contacorrente.data between '+formatdataIB(QEmpresasDATAINI.AsDateTime)+' and '+formatdataIB( QEmpresasDATAFIN.AsDateTime));

  DMConexao.Config.Open; //Abre o arquivo de configuracoes e tira da sql  o credenciado de baixa.
  if not DMConexao.ConfigCOD_CRED_BAIXA.IsNull then
     QContaC.Sql.Add(' and contacorrente.cred_id <> '+DMConexao.ConfigCOD_CRED_BAIXA.AsString);
  DMConexao.Config.Close;
  if not CheckBaixados.Checked then begin
     QContaC.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,"N") <> "S" ');
  end;
  if fornec_sel <> '' then
     QContaC.Sql.Add(' and contacorrente.cred_id in ('+fornec_sel+')');
  QContaC.Sql.Add(' and (contacorrente.debito > 0 or contacorrente.credito > 0) ');
  if filtrarEntrNF then
    if rgpNFEntregue.ItemIndex = 1 then // Somente autorizacoes confirmadas
      QContaC.SQL.Add(' and contacorrente.entreg_nf = "S" ')
    else if rgpNFEntregue.ItemIndex = 2 then  // Somente autorizacoes nao confirmadas
      QContaC.SQL.Add(' and (contacorrente.entreg_nf = "N" or contacorrente.entreg_nf is null) ');
  QContaC.Sql.Add(' group by conveniados.empres_id, empresas.nome, contacorrente.conv_id, conveniados.titular, conveniados.chapa, contacorrente.cred_id, credenciados.fantasia, cartoes.nome, contacorrente.receita ');
  QContaC.SQL.Add(' order by conveniados.titular, contacorrente.conv_id, credenciados.fantasia');
  QContaC.Open;
end;


procedure TFRelExtratoConv.RelTotFornecSeqTit;
var
lpage : Integer;  old_sort : string;
tot_deb, tot_cre, tot_conv : currency;
procedure incl(n:integer=1);
begin
 inc(linha,n);
 inc(lpage,n);
 if lpage mod 66 = 0 then begin
    NewPage;
    lpage := 0;
    incl;
 end;
end;
begin
   Screen.Cursor := crHourGlass;
   linha := 1; lpage := 0;
   IniciaImpressao;
   //QConv.SortOnFields('Titular');
   QConv.Sort := 'Titular';
   QConv.First;
   While not QConv.Eof do begin
      if MConvMarcado.AsBoolean then begin
         AbreConsulta(QConvConv_id.AsInteger);
         tot_conv  := 0; tot_deb   := 0; tot_cre   := 0;
         incl;
         if QContac.FieldByName('Empres_id').AsInteger <> QEmpresasEmpres_id.AsInteger then QEmpresas.Locate('Empres_id',QContaC.FieldByName('Empres_id').AsVariant,[]);
         Imprime('Empresa: '+QContaC.FieldByName('EMPRESA').AsString,linha,3);
         if por_dia_fecha.Checked then
            Imprime('Autorizacoes com Fechamento em ' + FormatDataBR(DataFecha.Date), linha, IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91) + 7)
         else
            Imprime('Periodo de '+FormatDataBR(QEmpresasDATAINI.AsDateTime)+' a '+FormatDataBR(QEmpresasDATAFIN.AsDateTime),linha,IfThen((rgpNFEntregue.ItemIndex = 0) or (not filtrarEntrNF), 97, 91));
         incl;
         if ChKMostraChapa.Checked then
            Imprime('Titular: '+QContaC.FieldByName('chapa').AsString+' - '+QContaC.FieldByName('TITULAR').AsString,linha,3)
         else
            Imprime('Titular: '+QContaC.FieldByName('CONV_ID').AsString+' - '+QContaC.FieldByName('TITULAR').AsString,linha,3);
         if filtrarEntrNF then
           if rgpNFEntregue.ItemIndex = 1 then
              Imprime('Somente autorizacoes confirmadas', linha, 91)
           else if rgpNFEntregue.ItemIndex = 2 then
              Imprime('Somente autorizacoes nao confirmadas', linha, 91);
         incl;
         cabecalho_relfixo;
         incl;
         QContaC.First;
         while not QContaC.Eof do begin
            Imprime(QContaC.FieldByName('NOME').AsString,linha,5);
            Imprime(QContaC.FieldByName('FORNECEDOR').AsString,linha,55);
            Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('DEBITO').AsCurrency),' ',8),linha,108);
            Imprime(Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('CREDITO').AsCurrency),' ',8),linha,119);
            tot_deb  := tot_deb  + QContaC.FieldByName('DEBITO').AsCurrency;
            tot_cre  := tot_cre  + QContaC.FieldByName('CREDITO').AsCurrency;
            tot_conv := tot_conv + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);
            QContaC.Next;
            incl;
         end;
         Imprime(EdMsg.Text,linha,3);
         incl;
         Imprime('Total a Pagar: ',linha, 89);
         Imprime(Direita(FormatFloat('###,###,##0.00',tot_conv),' ', 12),linha, 104);
         incl;
         Imprime(Preenche('-','-',128),linha,3);
         QContaC.Close;
      end;
      QConv.Next;
   end;
   if FileExists('rel1') then DeleteFile('rel1');
   Imprimir(False,'rel1');
   Screen.Cursor := crDefault;
   WinExec('visualizador.exe rel1 132',SW_SHOW);
   //QConv.SortOnFields('');
   QConv.Sort := '';
end;


procedure TFRelExtratoConv.VisualizarModelo6_7(modelo:integer);
var Imp : TImpres;  Linha, conv_atual : Integer;
   tot_emp, tot_emp_conf, tot_emp_nconf : Currency;
   tot_conv, tot_conv_conf, tot_conv_nconf : Currency;

begin
   Imp := TImpres.Create;
   Imp.MostrarNumPaginas := False;
   if not ckQuebraPagina.Checked then
      Imp.quebrapagina  := False;
   ImprimirTitulo(Imp);
   ImprimirCabecaModelo6_7(Imp);
   tot_emp  := 0; tot_emp_conf  := 0; tot_emp_nconf  := 0;
   QConv.First;
   conv_atual := QConvConv_id.AsInteger;
   While not QConv.Eof do begin
      if MConvMarcado.AsBoolean then begin
         if QConvEmpres_id.AsInteger <> QEmpresasEmpres_id.AsInteger then begin
            Imp.SaltarLinhas(1);
            ImprimirTotalEmpModelo6_7(Imp,tot_emp_conf,tot_emp_nconf,tot_emp);
            QEmpresas.Locate('Empres_id',QConvEmpres_id.AsVariant,[]);
            Imp.novapagina(True);
            ImprimirTitulo(Imp);
            tot_emp := 0; tot_emp_conf := 0; tot_emp_nconf := 0;
            ImprimirCabecaModelo6_7(Imp);
         end;
         AbreConsulta;
         tot_conv := 0; tot_conv_conf := 0; tot_conv_nconf := 0;
         If not QContaC.IsEmpty then begin
            QContaC.First;
            while not QContaC.Eof do begin
               ImprimirCorpoMoledo6_7(Imp);

               if QContaC.FieldByName('ENTREG_NF').AsString = 'S' then
                  tot_conv_conf  := tot_conv_conf  + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency)
               else
                  tot_conv_nconf := tot_conv_nconf + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);
               tot_conv := tot_conv + (QContaC.FieldByName('DEBITO').AsCurrency - QContaC.FieldByName('CREDITO').AsCurrency);

               QContaC.Next;
            end;
         end;
         tot_emp       := tot_emp + tot_conv;
         tot_emp_conf  := tot_emp_conf + tot_conv_conf;
         tot_emp_nconf := tot_emp_nconf + tot_conv_nconf;
         if (modelo = 7) then
            ImprimirTotalConvModelo6_7(Imp,tot_conv_conf,tot_conv_nconf,tot_conv);
      end;
      QConv.Next;
      Application.ProcessMessages;
   end;
   ImprimirTotalEmpModelo6_7(Imp,tot_emp_conf,tot_emp_nconf,tot_emp);
   Imp.Imprimir();
   Screen.Cursor := crDefault;
end;

procedure TFRelExtratoConv.ImprimirCabecaModelo6_7(Imp:TImpres);
var Linha : Integer;
begin
   QEmpresas.Locate('Empres_id',QConvEmpres_id.AsVariant,[]);
   Imp.AddLinha('Empresa: '+QEmpresasNome.AsString);
   Imp.SaltarLinhas(1);
   Linha := Imp.AddLinha('Chapa',3);
   Imp.AddLinha('Titular',25,Linha);
   Imp.AddLinha('Data',60,Linha);
   Imp.AddLinha('No Nota',75,Linha);
   Imp.AddLinha('Autorizacao',86,Linha);
   Imp.AddLinha('Debito',105,linha);
   Imp.AddLinha('Credito',120,Linha);
   if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then
      Imp.AddLinha('Ent', 128,linha);
   Imp.AddLinhaSeparadora;
end;

procedure TFRelExtratoConv.ImprimirCorpoMoledo6_7(Imp:TImpres);
var l : integer;
begin
   l := Imp.AddLinha(QConvchapa.AsString,3);
   Imp.AddLinha(QConvTitular.AsString,25,l);
   Imp.AddLinha(FormatDataBR(QContaC.FieldByName('DATA').Asdatetime),60,l);
   Imp.AddLinha(Imp.Direita(QContaC.FieldByName('NF').AsString,6,' '),75,l);
   Imp.AddLinha(Imp.Direita(QContaC.FieldByName('AUTORIZACAO_ID').AsString+PadL(QContaC.FieldByName('DIGITO').AsString,2,'0'),11,' '),86,l);
   Imp.AddLinha(Imp.Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('DEBITO').AsCurrency),8,' '),103,l);
   Imp.AddLinha(Imp.Direita(FormatFloat('###,###,##0.00',QContaC.FieldByName('CREDITO').AsCurrency),8,' '),119,l);
   if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then
     Imp.AddLinha(QContaC.FieldByName('ENTREG_NF').AsString, 129,l);
end;

procedure TFRelExtratoConv.ImprimirTotalEmpModelo6_7(Imp:TImpres;tot_emp_conf,tot_emp_nconf,tot_emp:currency);
var linha : integer;
begin
   linha := Imp.AddLinha(' ');
   if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then begin
      Imp.AddLinha('Totalizacao confirmadas: ',3, linha);
      Imp.AddLinha(Direita(FormatFloat(',0.00', tot_emp_conf), ' ', 12), 28, linha);
      Imp.AddLinha('Totalizacao nao confirmadas: ', 44, linha);
      Imp.AddLinha(Direita(FormatFloat(',0.00', tot_emp_nconf), ' ', 12), 73, linha);
   end;
   Imp.AddLinha('Totalizacao da empresa: ', 89, linha);
   Imp.AddLinha(Direita(FormatFloat('###,###,##0.00',tot_emp), ' ', 12), 115 ,linha);
end;

procedure TFRelExtratoConv.ImprimirTotalConvModelo6_7(Imp:TImpres;tot_conv_conf,tot_conv_nconf,tot_conv:currency);
var linha : integer;
begin
   linha := Imp.AddLinha(' ');
   if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then begin
      Imp.AddLinha('Totalizacao confirmadas: ',3, linha);
      Imp.AddLinha(Direita(FormatFloat(',0.00', tot_conv_conf), ' ', 12), 28, linha);
      Imp.AddLinha('Totalizacao nao confirmadas: ', 44, linha);
      Imp.AddLinha(Direita(FormatFloat(',0.00', tot_conv_nconf), ' ', 12), 73, linha);
   end;
   Imp.AddLinha('Totalizacao do conveniado: ', 89, linha);
   Imp.AddLinha(Direita(FormatFloat('###,###,##0.00',tot_conv), ' ', 12), 115 ,linha);
   Imp.AddLinhaSeparadora();
end;

procedure TFRelExtratoConv.ckNaoAgruparPorEmpresaClick(Sender: TObject);
begin
  inherited;
  if ckNaoAgruparPorEmpresa.Checked then
     ckQuebraPagina.Checked:= False;
end;

procedure TFRelExtratoConv.frxConvClosePreview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := False;
  lastRec := False;
  recConv := 0;
end;

procedure TFRelExtratoConv.frxConvPreview(Sender: TObject);
var I : Integer;
begin
  inherited;
  MostrarRelatorio := True;
  frxConv.Variables['tpRel']     := TipoRel.ItemIndex;
  frxConv.Variables['chapa']     := ChKMostraChapa.Checked;
  frxConv.Variables['empPorPag'] := ckQuebraPagina.Checked;
  frxConv.Variables['mensagem']  := QuotedStr(EdMsg.Text);
  frxConv.Variables['qtdRec']    := qtdConv;
  case TipoRel.ItemIndex of
    1: frxConv.Variables['qtdFolha']  := 1;
    2: frxConv.Variables['qtdFolha']  := 2;
    3,4: frxConv.Variables['qtdFolha']:= 4;
  else
    frxConv.Variables['qtdFolha']     := -1;
  end;
end;

procedure TFRelExtratoConv.frxConvGetValue(const VarName: String;
  var Value: Variant);
begin
  inherited;
  if CompareText(VarName, 'estabelecimentos') = 0 then
    Value := sl.Text;
end;

function TFRelExtratoConv.GetConveniadosSelecionados: Integer;
var b : TBookmark;
    contador : Integer;
begin
  QConv.GetBookmark;
  QConv.DisableControls;
  QConv.First;
  Result := 0;
  while not QConv.Eof do begin
    if UpperCase(MConvMARCADO.AsString) = 'S' then
      inc(Result);
    QConv.Next;
  end;
  QConv.GotoBookmark(b);
  QConv.EnableControls;
  //QConv.FreeBookmark(b);
end;

procedure TFRelExtratoConv.carregarDataSet;
var b : TBookmark;
begin
  //CARREGANDO AS EMPRESAS
  b := QEmpresas.GetBookmark;
  MEmpresas.DisableControls;
  MEmpresas.First;
  while not MEmpresas.Eof do begin
    if (MEmpresasMARCADO.AsBoolean = true) then begin
      cdsEmpresas.Append;
      cdsEmpresasNOME.AsString      :=  MEmpresasNOME.AsString;
      cdsEmpresasEMPRES_ID.AsString :=  MEmpresasEMPRES_ID.AsString;
      cdsEmpresasDATAINI.AsString   :=  MEmpresasDATAINI.AsString;
      cdsEmpresasDATAFIN.AsString   :=  MEmpresasDATAFIN.AsString;
      cdsEmpresas.Post;
    end;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(b);
  MEmpresas.EnableControls;
  //CARREGANDO OS CONVENIADOS
  b := MConv.GetBookmark;
  MConv.DisableControls;
  MConv.First;
  while not MConv.Eof do begin
    if (MConvMARCADO.AsBoolean = true) then begin
      cdsConv.Append;
      cdsConvCHAPA.AsString     :=  MConvCHAPA.AsString;
      cdsConvCONV_ID.AsString   :=  MConvCONV_ID.AsString;
      cdsConvEMPRES_ID.AsString :=  MConvEMPRES_ID.AsString;
      cdsConvTITULAR.AsString   :=  MConvTITULAR.AsString;
      cdsConvTOTAL.AsString     :=  MConvTOTAL.AsString;
      cdsConv.Post;
    end;
    MConv.Next;
  end;
  MConv.GotoBookmark(b);
  MConv.EnableControls;
  //CARREGANDO OS FORNECEDORES
  if MFornec.Active then begin
    b := MFornec.GetBookmark;
    MFornec.DisableControls;
    MFornec.First;
    while not QFornec.Eof do begin
      if (MFornecMARCADO.AsBoolean = true) then begin
        cdsFornec.Append;
        cdsFornecCRED_ID.AsString   :=  MFornecCRED_ID.AsString;
        cdsFornecFANTASIA.AsString  :=  MFornecFANTASIA.AsString;
        cdsFornecNOME.AsString      :=  MFornecNOME.AsString;
        cdsFornecSEGMENTO.AsString  :=  MFornecSEGMENTO.AsString;
        cdsFornec.Post;
      end;
      MFornec.Next;
    end;
    MFornec.GotoBookmark(b);
    MFornec.EnableControls;
  end;
  //CARREGANDO OS CONTACORRENTE
  cdsConv.DisableControls;
  cdsConv.First;
  while not cdsConv.Eof do begin
    QContaC.Close;
    QContaC.Parameters.ParamByName('CONV_ID').Value := cdsConvCONV_ID.Value;
    QContaC.Parameters.ParamByName('EMPRES_ID').Value := cdsConvEMPRES_ID.Value;
    QContaC.SQL.Text;
    QContaC.Open;
    while not QContaC.Eof do begin
      cdsContaC.Append;
      cdsContaCCONV_ID.AsString          :=  cdsConvCONV_ID.AsString;
      cdsContaCEMPRES_ID.AsString        :=  cdsConvEMPRES_ID.AsString;
      cdsContaCCREDITO.AsString          :=  QContaC.FieldByName('CREDITO').AsString;
      cdsContaCCRED_ID.AsString          :=  QContaC.FieldByName('CRED_ID').AsString;
      cdsContaCDEBITO.AsString           :=  QContaC.FieldByName('DEBITO').AsString;
      cdsContaCFORNECEDOR.AsString       :=  QContaC.FieldByName('FORNECEDOR').AsString;
      cdsContaCHISTORICO.AsString        :=  QContaC.FieldByName('HISTORICO').AsString;
      cdsContaCNF.AsString               :=  QContaC.FieldByName('NF').AsString;
      cdsContaCNOME.AsString             :=  QContaC.FieldByName('NOME').AsString;
      cdsContaCRECEITA.AsString          :=  QContaC.FieldByName('RECEITA').AsString;
      if (TipoRel.ItemIndex <> 4) and (TipoRel.ItemIndex <> 5) then begin
        cdsContaCAUTORIZACAO_ID.AsString :=  QContaC.FieldByName('AUTORIZACAO_ID').AsString;
        cdsContaCDATA.AsString           :=  QContaC.FieldByName('DATA').AsString;
        cdsContaCDIGITO.AsString         :=  QContaC.FieldByName('DIGITO').AsString;
        cdsContaCENTREG_NF.AsString      :=  QContaC.FieldByName('ENTREG_NF').AsString;
      end;
      cdsContaC.Post;
      QContaC.Next;
    end;
    cdsConv.Next;
  end;
  cdsContaC.First;
  cdsContaC.EnableControls;
  cdsEmpresas.First;
  cdsConv.First;
end;

procedure TFRelExtratoConv.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BitBtn1.Click;
    frxConv.PrepareReport();
    frxConv.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TFRelExtratoConv.cdsConvAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not (DataSet.State in [dsInsert,dsEdit]) then
    frxConv.Variables['conv'] := DataSet.FieldByName('CONV_ID').AsInteger;
end;

procedure TFRelExtratoConv.cdsEmpresasAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not (DataSet.State in [dsInsert,dsEdit]) then
    frxConv.Variables['emp'] := DataSet.FieldByName('EMPRES_ID').AsInteger;
end;

end.

