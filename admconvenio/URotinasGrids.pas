unit URotinasGrids;

interface

uses Windows, SysUtils, Graphics, Controls, Forms, DB, Grids, DBGrids;

const tmAltura  = 0;
      tmLargura = 1;
function BuscaNomeFieldpeloTitulo(Titulo : String ; DBGrid : TDBGrid ; ComecarDaColuna : Integer = 0) : String;
function TamanhoCelulaEmPixel(QtdCaracters : Integer ; EstiloFonte : String = 'Courier New' ;  TamanhoFonte : Integer = 8 ; AlturaOuLargura : Integer = 1) : Integer;
procedure pintaGrid(var Sender: TObject; const Rect: TRect; var DataCol: Integer; var Column: TColumn; var State: TGridDrawState);
implementation

uses Classes;

function BuscaNomeFieldpeloTitulo(Titulo : String ; DBGrid : TDBGrid ; ComecarDaColuna : Integer = 0) : String;
var I : Integer;
begin
if ComecarDaColuna > DBGrid.Columns.Count - 1 then begin
    Result := '';
    Exit;
  end;
for I := ComecarDaColuna to DBGrid.Columns.Count - 1 do
  begin
  if DBGrid.Columns[I].Title.Caption = Titulo then begin
    Result := DBGrid.Columns[I].FieldName;
    Exit;
    end;
  end;
end;

function TamanhoCelulaEmPixel(QtdCaracters : Integer ; EstiloFonte : String ; TamanhoFonte, AlturaOuLargura : Integer): Integer;
var I : Integer;
    S : String;
    can : TControlCanvas;
begin
  S := '';
  if (AlturaOuLargura <> 0) and (QtdCaracters >= 25) and (QtdCaracters <= 34) then
    QtdCaracters := QtdCaracters - 5
  else if (AlturaOuLargura <> 0) and (QtdCaracters >= 35) then
    QtdCaracters := QtdCaracters - 8;
  can := TControlCanvas.Create;
  TControlCanvas(can).Control := Screen.ActiveControl;
  can.Font.Name := EstiloFonte;
  can.Font.Size := TamanhoFonte;
  can.Brush.Color := clBlack;
  can.Brush.Style := bsSolid;
  for I := 1 to QtdCaracters do
    S := S + 'N';
  if AlturaOuLargura = 0 then
    Result := can.TextHeight(S)
  else
    Result := can.TextWidth(S);
  FreeAndNil(can);
end;

procedure pintaGrid(var Sender: TObject; const Rect: TRect; var DataCol: Integer; var Column: TColumn; var State: TGridDrawState);
begin
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
      begin
      TDbGrid(Sender).Canvas.font.Color:= clWhite; //aqui � definida a cor da fonte
      Brush.Color := clNavy; //aqui � definida a cor do fundo
      FillRect(Rect);
      end;
  TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);
end;

end.
