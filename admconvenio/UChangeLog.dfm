object FChangeLog: TFChangeLog
  Left = 506
  Top = 119
  Width = 813
  Height = 524
  Caption = 'Altera'#231#245'es/Atualiza'#231#245'es do sistema.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RichEdit1: TRichEdit
    Left = 0
    Top = 0
    Width = 805
    Height = 493
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'Vers'#227'o 0.0.0.64'
      
        '       - Esta vers'#227'o traz uma novidade para facilitar a opera'#231#227'o' +
        ' do sistema.'
      
        '       - A partir desta vers'#227'o as  Importa'#231#245'es de Conveniados po' +
        'dem ser feitas '
      'automaticamente pelos operadores do sistema,'
      
        'atrav'#233's do menu operacional/Importa'#231#227'o Padr'#227'o de Conveniados, pa' +
        'ra saber como efetuar a '
      'Importa'#231#227'o consulte o ajuda.'
      ''
      'Vers'#227'o 0.0.0.63'
      
        '       -  Nesta vers'#227'o foi acrescentada a op'#231#227'o de fotos dos con' +
        'veniados.'
      
        'Para usar '#233' simples, basta entrar em algum conveniado e clicar n' +
        'a aba Fotos ou digitar F9, '
      'na tela ir'#225' aparecer duas op'#231#245'es.'
      '1'#170' op'#231#227'o '#233' a foto tirada pela webcam.'
      '   Obs.'#201' necess'#225'rio ter uma webcam ligada em seu computador.'
      '2'#170' op'#231#227'o '#233' a assossia'#231#227'o das fotos manualmente.'
      ''
      'Vers'#227'o 0.0.0.43'
      
        '       -  Nesta vers'#227'o foi acrescentada a op'#231#227'o de efetuar lan'#231'a' +
        'mentos individuais seguindo '
      'a regra definida para gerar'
      
        'autoriza'#231#245'es no sistema, incluindo a op'#231#227'o de lan'#231'amentos com fo' +
        'rma de pagamento, para '
      'habilitar esse tipo de lan'#231'amento v'#225
      
        'ao menu Configura'#231#245'es/Configura'#231#245'es do sistema, v'#225' '#224' aba Lan'#231'ame' +
        'ntos/Baixas e marque a op'#231#227'o '
      #8220'Na tela de lan'#231'amentos'
      'utilizar regra de autoriza'#231#227'o inclusive parcelamento'#8221'.'
      
        '- Uma regra de valida'#231#227'o foi adicionada para o cadastro de Forma' +
        's de Pagamento, essa regra '
      'implica em obrigar que os'
      
        'intervalos de dias sejam de trinta de uma parcela para outra, ca' +
        'so a op'#231#227'o '#8220'for'#231'a o dia do '
      'vencimento igual o dia da venda'#8221
      
        'estiver marcada. Devido a erros de configura'#231#227'o ocorreram proble' +
        'mas com datas de '
      'lan'#231'amentos, '#233' aconselh'#225'vel que os'
      
        'administradores ou operadores do sistema revisem todas as formas' +
        ' de pagamento cadastras e '
      'verifiquem se a forma '#8220'A vista'#8221
      
        'est'#225' com a op'#231#227'o '#8220'for'#231'a dia do vencimento igual o dia da venda'#8221' ' +
        'marcada, se estiver '
      'desmarc'#225'-la, Para as formas parceladas'
      
        'que estiverem com a op'#231#227'o '#8220'for'#231'a dia do vencimento igual o dia d' +
        'a venda'#8221' marcada, ent'#227'o '
      'verificar se o intervalo de dias de uma'
      
        'parcela para a outra '#233' de trinta. Para forma de pagamento de tri' +
        'nta dias direto, informar o '
      'n'#250'mero de parcelas igual a um e prazo'
      
        'um igual a trinta, neste caso marcar ou n'#227'o a op'#231#227'o '#8220'for'#231'a dia d' +
        'o vencimento igual o dia da '
      'venda'#8221' '#233' opcional de cada '
      'administradora.'
      
        '       - A partir desta vers'#227'o o cadastro de cart'#245'es teve sua ve' +
        'locidade de pesquisa e '
      'navega'#231#227'o melhoradas visando assim '
      
        'facilitar a manuten'#231#227'o de cart'#245'es, e a utiliza'#231#227'o de outros recu' +
        'rsos como altera'#231#245'es '
      'lineares e gera'#231#227'o de listagem ou arquivos '
      'de exporta'#231#227'o.'
      ''
      'Vers'#227'o 0.0.0.42'
      
        '       -  Nesta vers'#227'o ocorreu uma pequena altera'#231#227'o na grava'#231#227'o' +
        ' de logs do sistema, visto '
      'que no cadastro de conveniados '
      
        'temos um cadastro de cart'#245'es embutido os logs estavam sendo arma' +
        'zenados juntos, o que '
      'acarretou alguns probleminhas de '
      
        'visualiza'#231#227'o das altera'#231#245'es, a partir desta vers'#227'o as altera'#231#245'es' +
        ' feitas nos cart'#245'es ser'#227'o '
      'vistas numa janela de logs de cart'#227'o na '
      
        'aba cart'#245'es do cadastro de conveniados, ou no pr'#243'prio cadastro d' +
        'e cart'#245'es, sendo que as '
      'altera'#231#245'es feitas em qualquer um dos '
      
        'dois cadastros ficam vis'#237'veis para os dois, exemplo: se um cart'#227 +
        'o for alterado no cadastro '
      'de cart'#245'es esta altera'#231#227'o aparecer'#225' '
      
        'neste e tamb'#233'm nos logs de cart'#227'o no cadastro de conveniados, e ' +
        'se a altera'#231#227'o for feita no '
      'cart'#227'o dentro do cadastro de '
      
        'conveniados a altera'#231#227'o aparecer'#225' neste e tamb'#233'm no cadastro de ' +
        'cart'#245'es.'
      
        '       - Na tela de consulta de autoriza'#231#245'es agora ser'#225' poss'#237'vel' +
        ' consultar as autoriza'#231#245'es '
      'por n'#250'mero de nota fiscal, caso exista '
      
        'mais de uma autoriza'#231#227'o com o mesmo n'#250'mero de nota o sistema hab' +
        'ilitar'#225' os bot'#245'es de '
      'navega'#231#227'o (<<,<,>,>>), para a '
      'visualiza'#231#227'o de todas autoriza'#231#245'es encontradas.'
      
        '       - Outra altera'#231#227'o simples, mas '#250'til '#233' a capacidade do  si' +
        'stema de procurar por um ou '
      'mais registros ao mesmo tempo, '
      
        'para pesquisas por campos num'#233'ricos nos cadastros, campos como C' +
        'onv ID, Empres ID, Cr'#233'd ID, '
      'C'#243'digo do Cart'#227'o etc, a '
      
        'partir dessa vers'#227'o estes campos aceitam pesquisas por mais de u' +
        'm valor, simplesmente '
      'separando os valores por v'#237'rgula, '
      
        'exemplo: no cadastro de empresas no campo Empres ID, digite 1000' +
        ',1001 caso exista as '
      'empresas 1000 e 1001 cadastradas, '
      'o sistema listar'#225' as duas.'
      ''
      'Vers'#227'o 0.0.0.41'
      
        '       - Esta vers'#227'o traz uma nova funcionalidade para o sistema' +
        ', '#233' a op'#231#227'o de conferencia '
      'de valores para pagamento de '
      
        'fornecedores, esta conferencia consiste em cadastrar os valores ' +
        'de venda passados pelos '
      'fornecedores e depois gerar uma '
      
        'confer'#234'ncia, na confer'#234'ncia o sistema far'#225' uma compara'#231#227'o dos va' +
        'lores passados pelos '
      'fornecedores e dos valores existentes '
      
        'no sistema provenientes de autoriza'#231#245'es. Os lan'#231'amentos dos valo' +
        'res passados pelos '
      'fornecedores s'#227'o feitos na tela '
      
        #8220'Cadastro de relat'#243'rios para confer'#234'ncia de valores'#8221' que '#233' acess' +
        'ada atrav'#233's do menu '
      #8220'Confer'#234'ncia'#8221'/'#8221' Cadastro de relat'#243'rios para '
      
        'confer'#234'ncia de valores'#8221', e a confer'#234'ncia dos valores lan'#231'ados co' +
        'm os valores existentes no '
      'sistema '#233' feita na tela '#8220'Confer'#234'ncia '
      
        'de valores'#8221' atrav'#233's do menu '#8220'Confer'#234'ncia'#8221'/'#8221'Confer'#234'ncia de valore' +
        's'#8221'.'
      ''
      'Vers'#227'o 0.0.0.40'
      
        '       - Esta vers'#227'o traz a op'#231#227'o de d'#233'bito em conta corrente no' +
        ' padr'#227'o estabelecido pelos '
      'bancos, ou seja, agora '#233' poss'#237'vel no '
      
        'sistema, gerar arquivos para d'#233'bitos em conta corrente de qualqu' +
        'er banco e posteriormente '
      'importar o arquivo de retorno do '
      
        'banco, esse arquivo de retorno informa quais lan'#231'amentos foram d' +
        'ebitados com sucesso e quais '
      'n'#227'o foram, os que n'#227'o forem '
      
        'debitados retornar'#227'o uma mensagem com o motivo. Na tela de impor' +
        'ta'#231#227'o do retorno, ser'#225' '
      'poss'#237'vel baixar os valores debitados '
      'e bloquear os conveniados que n'#227'o tiveram os d'#233'bitos efetuados.'
      
        '      - No cadastro de bancos foi criada uma aba chamada '#8220'contas' +
        ' existentes neste banco'#8221', '
      'nessa aba devem ser cadastradas '
      
        'as contas que a administradora possu'#237' no banco cadastrado, esse ' +
        'cadastro deve ser atualizado '
      'principalmente por '
      'administradoras que forem usar o d'#233'bito em conta corrente.'
      
        '       - Para administradoras que forem usar o d'#233'bito em conta c' +
        'orrente existe a op'#231#227'o de '
      'dividir o total a ser debitado em v'#225'rios '
      
        'lan'#231'amentos, visando que haja o m'#237'nimo de inadimpl'#234'ncia poss'#237'vel' +
        ', para configurar as '
      'divis'#245'es de valores para gera'#231#227'o de '
      
        'd'#233'bito em conta corrente acesse o menu '#8220'Configura'#231#245'es/Configura'#231 +
        #245'es de d'#233'bito em conta '
      'corrente'#8221'.'
      
        '      - Visando facilitar a navega'#231#227'o do operador no sistema Adm' +
        'inistra'#231#227'o de Cart'#245'es, os '
      'menus de acesso foram '
      
        'redistribu'#237'dos, agora existem menus espec'#237'ficos para configura'#231#245 +
        'es, lan'#231'amentos e '
      'operacional.'
      ''
      'Vers'#227'o 0.0.0.39'
      
        '       - Esta vers'#227'o traz algumas novidades para facilitar a ope' +
        'ra'#231#227'o do sistema.'
      
        '       - A partir desta vers'#227'o as  atualiza'#231#245'es de limite e sal'#225 +
        'rios dos funcion'#225'rios podem '
      'ser feitas automaticamente pelos'
      
        'operadores do sistema, atrav'#233's do menu operacional/Importa'#231#227'o de' +
        ' Sal'#225'rio e/ou Limite M'#234's, '
      'para saber como efetuar a'
      'atualiza'#231#227'o consulte a  ajuda.'
      
        '       - Na janela de lan'#231'amentos de taxa, agora ser'#225' poss'#237'vel e' +
        'fetuar lan'#231'amentos '
      'calculados podendo ser utilizado nesse'
      
        'c'#225'lculo o valor do campo Sal'#225'rio e do campo Limite M'#234's do conven' +
        'iado, para efetuar uma '
      'opera'#231#227'o desse tipo, selecione uma '
      
        'ou mais empresas, abra os conveniados e marque os conveniados qu' +
        'e receber'#227'o o lan'#231'amento, '
      'clique em efetuar lan'#231'amento '
      
        'na telinha que se abrir'#225' clique em criar express'#227'o, monte a expr' +
        'ess'#227'o desejada, e ent'#227'o '#233' s'#243' '
      'confirmar os lan'#231'amentos.'
      
        '       - Um novo relat'#243'rio foi adicionado ao sistema, no menu Re' +
        'lat'#243'rios/M'#233'dia por Empresa, '
      'trata-se de um relat'#243'rio gerencial '
      
        'com informa'#231#245'es de um per'#237'odo selecionado. Tamb'#233'm '#233' poss'#237'vel inf' +
        'ormar mais dois per'#237'odos '
      'para compara'#231#227'o de valores. '
      
        'Neste relat'#243'rio existe a op'#231#227'o de imprimir as f'#243'rmulas que foram' +
        ' usadas nos c'#225'lculos, para '
      'isto selecione a op'#231#227'o imprimir '
      'f'#243'rmulas no final, na tela de gera'#231#227'o do relat'#243'rio.'
      ''
      'Vers'#227'o 0.0.0.38'
      
        '       - Esta vers'#227'o traz algumas novidades para facilitar a ope' +
        'ra'#231#227'o do sistema.'
      
        '       - Nos cadastros, na rotina de altera'#231#227'o linear, foi criad' +
        'a uma nova op'#231#227'o chamada '
      'express'#227'o,'
      
        'selecionando essa op'#231#227'o o sistema exibir'#225' uma janelinha onde ser' +
        #225' poss'#237'vel criar uma '
      'express'#227'o'
      
        'com todos os campos de tipos de dados num'#233'ricos do cadastro. Exe' +
        'mplo: supondo que ser'#225' '
      'necess'#225'rio ajustar o limite do '
      
        'conveniado a trinta por cento do seu sal'#225'rio, ent'#227'o ter'#237'amos a e' +
        'xpress'#227'o: ('#8220'Sal'#225'rio'#8221' * '
      '0.30), ou ainda se quis'#233'ssemos dar um '
      
        'aumento de vinte por cento ao limite do conveniado ent'#227'o ter'#237'amo' +
        's a express'#227'o: ('#8220'Limite M'#234's'#8221' '
      '* 1.2).  Para saber como criar '
      'uma express'#227'o veja a ajuda na janela de express'#245'es.'
      
        '       - Na baixa de empresa agora '#233' poss'#237'vel que o operador inf' +
        'orme a data para o '
      'lan'#231'amento da diferen'#231'a, caso seja efetuada '
      
        'uma baixa parcial. O sistema sugerir'#225' uma data para o lan'#231'amento' +
        ' sendo esta baseada em uma '
      'configura'#231#227'o criada nas '
      
        'configura'#231#245'es do sistema, esta nova configura'#231#227'o chamada '#8220'Lan'#231'ar' +
        ' diferen'#231'as na baixa de '
      'empresas para o pr'#243'ximo per'#237'odo'#8221' '
      
        'quando marcada tem a fun'#231#227'o de avisar ao sistema para sugerir qu' +
        'e o lan'#231'amento da diferen'#231'a '
      'seja para o primeiro dia do '
      
        'per'#237'odo subseq'#252'ente ao que est'#225' sendo baixado, quando n'#227'o marcad' +
        'o o sistema sugerir'#225' como '
      'data do lan'#231'amento o '#250'ltimo '
      
        'dia do per'#237'odo que est'#225' sendo baixado.Obs(Esta data '#233' pass'#237'vel d' +
        'e altera'#231#227'o na pr'#243'pria '
      'janela da baixa na aba Titulares.)'
      ''
      'Vers'#227'o 0.0.0.37'
      
        '       - Esta vers'#227'o traz algumas novidades para facilitar a ope' +
        'ra'#231#227'o do sistema.'
      
        '       - Nos Cadastros de Conveniados e Cart'#245'es, dois campos for' +
        'am adicionados no gerador de '
      'listagem, esses campos s'#227'o: '
      
        'd'#233'bito total, e d'#233'bito pr'#243'ximo fechamento, que respectivamente d' +
        'emonstram o total em aberto '
      'do conveniado e o total em aberto '
      
        'para o pr'#243'ximo fechamento. Facilitando assim a cria'#231#227'o de relat'#243 +
        'rios e extratos '
      'demonstrativos.'
      
        '       - No cadastro de conveniados foi adicionado um novo campo' +
        ' chamado '#8220'c'#243'digo do '
      'conveniado na empresa'#8221' esse campo tem '
      
        'por finalidade comportar algum tipo de c'#243'digo pr'#243'prio da empresa' +
        ' que n'#227'o seja a chapa ou '
      'matricula, esse novo campo '#233' do tipo '
      'alfanum'#233'rico de 30 caracteres.'
      
        '       - Uma das mais importantes altera'#231#245'es que esta vers'#227'o tra' +
        'z, '#233' a possibilidade de se '
      'criar uma s'#233'rie de cart'#245'es espec'#237'ficos '
      
        'para uma empresa. Para isso no cadastro de empresas foi criado n' +
        'a aba '#8220'Ficha'#8221' uma nova '
      'palheta chamada '#8220'cart'#245'es da '
      
        'empresa'#8221' onde existem dois campos, o primeiro '#233' do tipo '#8220'checkbo' +
        'x'#8221' chamado '#8220'usa numera'#231#227'o '
      'pr'#243'pria'#8221', o segundo '#233' do tipo '
      
        #8220'TextBox'#8221'  chamado '#8220'numera'#231#227'o dos cart'#245'es'#8221', uma vez estando marc' +
        'ado o campo '#8220'usa numera'#231#227'o '
      'pr'#243'pria'#8221' '#233' obrigat'#243'rio a '
      
        'informa'#231#227'o da numera'#231#227'o dos cart'#245'es, sendo que o segundo a cada ' +
        'cart'#227'o criado para a empresa '
      'ser'#225' incrementado de 1 em '
      
        '1, gerando assim uma seq'#252#234'ncia de cart'#245'es exclusivos da empresa.' +
        ' (Obs: Para que esta op'#231#227'o '
      'funcione corretamente solicite '
      'uma atualiza'#231#227'o do m'#243'dulo de empresas da Internet).'
      
        '       - A '#250'ltima, por'#233'm n'#227'o menos importante novidade desta ver' +
        's'#227'o, e o m'#243'dulo de auditoria '
      'do sistema, este tem por finalidade '
      
        'procurar diverg'#234'ncias no banco de dados do sistema, diverg'#234'ncias' +
        ' estas que possam de alguma '
      'forma causar algum tipo '
      
        'problema no funcionamento do sistema.(Obs: Este novo m'#243'dulo ser'#225 +
        ' atualizado freq'#252'entemente, '
      'em busca de uma otimiza'#231#227'o '
      'na integridade do banco de dados do sistema).'
      ''
      'Vers'#227'o 0.0.0.36'
      
        '       - Esta vers'#227'o tem como principal finalidade corrigir um p' +
        'roblema quanto ao calculo do '
      'saldo devedor. Este quando era '
      
        'executado prendia o c'#243'digo dos cart'#245'es e '#224's vezes causando probl' +
        'emas no momento em que os '
      'fornecedores fossem tirar uma '
      
        'autoriza'#231#227'o para venda, principalmente no m'#243'dulo de resposta aud' +
        #237'vel (URA). '#201' recomendado '
      'que os operadores do sistema '
      
        'enviem seus arquivos de logs do '#8220'URA'#8221' para an'#225'lise. Nesta vers'#227'o' +
        ' o problema foi corrigido, e '
      'ainda assim '#233' recomendado que '
      
        'os administradores/operadores do sistema verifiquem diariamente ' +
        'os arquivos de logs do '#8220'URA'#8221' '
      'para nos participar de qualquer '
      'altera'#231#227'o.'
      
        '       - Nas consultas de movimenta'#231#227'o, fornecedores, empresas e' +
        ' conveniados, foi adicionado '
      'um campo '#8220'Lib'#8221' que informa se o '
      'conveniado a empresa ou o fornecedor est'#225' liberado no sistema.'
      
        '       - No extrato de fornecedores foi criada uma nova op'#231#227'o pa' +
        'ra consultar os fornecedores '
      'pelo dia de fechamento. Informando '
      
        'o dia de fechamento automaticamente o sistema verificar'#225' se o fo' +
        'rnecedor tem movimenta'#231#227'o no '
      'per'#237'odo correspondente ao '
      'fechamento informado.'
      ''
      'Vers'#227'o 0.0.0.35'
      
        '       - Foi adicionada ao sistema nesta vers'#227'o a op'#231#227'o de expor' +
        'tar arquivos do tipo texto '
      'de qualquer janela de cadastro do '
      
        'sistema, para isto fa'#231'a uma busca de dados no cadastro, clique e' +
        'm imprimir, selecione os '
      'campos que ser'#227'o exportados e '
      
        'clique em exportar para arquivo, marque as op'#231#245'es desejadas e cl' +
        'ique em exportar novamente, '
      'ap'#243's o sistema criar o arquivo '
      
        'abrir'#225' uma janela pedindo o nome e a pasta para salvar o arquivo' +
        ', informe os valores e '
      'clique em salvar.'
      ''
      'Vers'#227'o 0.0.0.34'
      
        '       - Foi adicionada ao sistema nesta vers'#227'o a op'#231#227'o de baixa' +
        's nos valores das empresas '
      'informando o per'#237'odo de '
      
        'movimenta'#231#227'o, essa op'#231#227'o vem para facilitar as baixas que podem ' +
        'englobar mais de uma data de '
      'fechamento. No caso de '
      
        'baixa parcial o lan'#231'amento da diferen'#231'a ser'#225' feito na data final' +
        ' do per'#237'odo informado. '
      
        '       - Uma nova op'#231#227'o foi adicionada ao relat'#243'rio Extrato da E' +
        'mpresa, op'#231#227'o esta que '
      'permite que o operador selecione se '
      
        'deseja exibir no relat'#243'rio somente valores de autoriza'#231#245'es de ve' +
        'nda somente com receita, '
      'somente sem receita ou ambos.'
      
        '       - Uma nova op'#231#227'o de pesquisa r'#225'pida foi adicionada a toda' +
        's as grades do sistema no '
      'formato de pesquisa dos editores de '
      
        'texto, pressionando a tecla Ctrl(Control) + F e o foco de contro' +
        'le estando sobre uma grade e '
      'a mesma n'#227'o estando vazia, o '
      
        'sistema exibir'#225' uma telinha de pesquisa contendo um campo no for' +
        'mado de lista suspensa com '
      'todos os campos da grade e '
      
        'um campo para informa'#231#227'o do valor a ser pesquisado, ainda seguem' +
        ' as op'#231#245'es de pesquisa que '
      'podem ser: iniciando com '
      
        '(esta procura valores que iniciem com o valor informado no campo' +
        ' de pesquisa), contendo '
      '(esta procura valores que contenham '
      
        'o valor informado no campo de pesquisa independente de in'#237'cio, m' +
        'eio ou final.), exatamente '
      'igual (esta procura o valor do campo '
      'exatamente igual ao valor informado na pesquisa). '
      ''
      'Corre'#231#245'es efetuadas na vers'#227'o:'
      
        '      Nas consultas de Fornecedores por Empresa e Empresa por Fo' +
        'rnecedores, lan'#231'amentos que '
      'tinham sido baixados na '
      'vers'#227'o antiga do sistema n'#227'o apareciam nestas consultas.  '
      
        '      Melhora na performance do calculo do saldo devedor de todo' +
        's conveniados do sistema, '
      'este no menu Utilit'#225'rios/Calculo do '
      'saldo devedor.'
      ''
      'Vers'#227'o 0.0.0.33'
      '      - Pequenos ajustes no sistema:'
      
        '        Na op'#231#227'o de gerar arquivo de cart'#245'es de conveniados foi ' +
        'corrigido o nome da empresa '
      'que estava sendo apresentado, o '
      
        'sistema estava utilizando o campo Nome do cadastro de empresa, m' +
        'as este '#233' maior que o '
      'permitido no cart'#227'o a partir desta '
      
        'vers'#227'o o sistema utilizar'#225' o campo Nome Cart'#227'o do cadastro de Em' +
        'presas que tem tamanho '
      'pr'#243'prio para esta finalidade.'
      
        '        Em ambos relat'#243'rios Extrato do Conveniado e Extrato da E' +
        'mpresa, foi adicionado um '
      'novo par'#226'metro do tipo '#8220'Checkbox'#8221' '
      
        'com o nome '#8220'Somente empresas com movimento'#8221' como padr'#227'o esta par' +
        #226'metro aparecer'#225' marcado, '
      'esta op'#231#227'o serve para '
      
        'determinar se o sistema listar'#225' apenas empresas com movimento no' +
        ' per'#237'odo selecionado ou '
      'todas empresas que tem '
      
        'fechamento na data informada, se este estiver desmarcado o siste' +
        'ma trar'#225' todas empresas com '
      'fechamento na data solicitada '
      'agilizando assim o processo.'
      
        '        Na tela de lan'#231'amentos em '#8220'operacional'#8221', '#8221'Lan'#231'amentos/Au' +
        'toriza'#231#245'es'#8221' ao consultar o '
      'cart'#227'o por nome na grade de nomes '
      
        'ser'#225' informado se o titular do cart'#227'o e o pr'#243'prio cart'#227'o est'#227'o l' +
        'iberados, estes dois campos '
      'aparecer'#227'o em negrito para f'#225'cil '
      
        'visualiza'#231#227'o do operador do sistema.Caso o operador selecione um' +
        ' cart'#227'o bloqueado ou que o '
      'titular do cart'#227'o esteja bloqueado '
      
        'ao tentar efetuar o lan'#231'amento aparecer'#225' uma mensagem informando' +
        ' que o titular ou cart'#227'o '
      'est'#225' bloqueado pedindo uma '
      
        'confirma'#231#227'o do operador para prosseguir com a opera'#231#227'o. Esta op'#231 +
        #227'o foi inclu'#237'da, pois se '
      'tratando de lan'#231'amentos '
      
        'administrativos '#224's vezes ser'#225' necess'#225'rio efetuar lan'#231'amentos par' +
        'a cart'#245'es n'#227'o liberados.'
      
        '      - No relat'#243'rio Extrato do Conveniado foi criado a op'#231#227'o de' +
        ' selecionar m'#250'ltiplas '
      'empresas para a visualiza'#231#227'o e impress'#227'o '
      
        'dos extratos dos conveniados destas empresas, facilitando assim ' +
        'o processo de gera'#231#227'o de '
      'extratos. Para a visualiza'#231#227'o e '
      
        'impress'#227'o dos extratos dos conveniados de uma empresa s'#243' ser'#225' ne' +
        'cess'#225'rio selecion'#225'-la o que '
      'n'#227'o era preciso na vers'#227'o '
      'anterior.'
      ''
      'Vers'#227'o 0.0.0.32'
      
        '      - Foi adicionado ao sistema nesta vers'#227'o um gerador de lis' +
        'tagens em todos formul'#225'rios '
      'de cadastro do sistema, este '
      
        'gerador de listagens ser'#225' encontrado clicando no bot'#227'o imprimir ' +
        'de qualquer formul'#225'rio de '
      'cadastro do sistema. Com este '
      
        'gerador de listagens '#233' poss'#237'vel criar listagens totalmente perso' +
        'nalizadas de acordo com a '
      'necessidade do operador do sistema.'
      
        '(Leia o help no pr'#243'prio gerador de listagens para saber como cri' +
        'ar listagens.).'
      
        '      - Foi adicionado ao sistema um formul'#225'rio de cadastro de c' +
        'art'#245'es, este segue o modelo '
      'dos outros cadastros do sistema, '
      
        'possibilitando exporta'#231#227'o para o Excel, altera'#231#227'o linear, e filt' +
        'ro de dados. Este cadastro '#233' '
      'um complemento do cadastro de '
      
        'conveniados onde s'#243' '#233' poss'#237'vel visualizar os cart'#245'es de um conve' +
        'niado de cada vez. No '
      'cadastro de cart'#245'es '#233' poss'#237'vel abrir '
      
        'cart'#245'es de v'#225'rios conveniados de uma vez, possibilitando altera'#231 +
        #245'es lineares, filtragem e '
      'exporta'#231#227'o de dados.'
      ''
      'Vers'#227'o 0.0.0.31'
      
        '       - Foi adicionada a op'#231#227'o de altera'#231#227'o linear para todos o' +
        's cadastros do sistema, com '
      'esta op'#231#227'o '#233' poss'#237'vel selecionar um '
      
        'grupo de registros do cadastro e alterar o mesmo campo destes de' +
        ' uma s'#243' vez, ex: selecionar '
      'todos conveniados de uma '
      
        'empresa e alterar o limite m'#234's destes para 500,00 de uma s'#243' vez,' +
        ' ou bloquear todos os '
      'conveniados de uma empresa que '
      
        'cortou o convenio. Uma vez efetuada uma altera'#231#227'o linear o siste' +
        'ma faz um backup dos dados '
      'que foram alterados sendo '
      
        'poss'#237'vel restaur'#225'-los futuramente a partir do menu Utilit'#225'rios, ' +
        'Retorno de altera'#231#227'o linear. '
      'Obs: As op'#231#245'es de altera'#231#227'o linear e '
      
        'retorno de altera'#231#227'o linear s'#243' est'#227'o dispon'#237'veis para operadores' +
        ' que perten'#231'am a algum grupo '
      'de administradores.'
      
        '       - Foram criados dois novos par'#226'metros no sistema, no menu' +
        ' Utilit'#225'rios, configura'#231#245'es, '
      'o sistema pede o fornecedor de '
      
        'lan'#231'amentos da administradora, este fornecedor ser'#225' usado para o' +
        's lan'#231'amentos autom'#225'ticos '
      'que o sistema tiver que fazer '
      
        'como, por exemplo, se for dada uma baixa parcial nos valores dos' +
        ' conveniados o sistema '
      'lan'#231'ar'#225' os valores restantes para o '
      
        'fornecedor de lan'#231'amentos da administradora. Se o par'#226'metro n'#227'o ' +
        'estiver informado, e houver '
      'uma tentativa de baixa de valores, '
      
        'o sistema mostrar'#225' uma mensagem e abortar'#225' a opera'#231#227'o. O segundo' +
        ' novo par'#226'metro '#233' a op'#231#227'o de '
      'poder editar os dados nas '
      
        'grades principais dos cadastros do sistema, se a op'#231#227'o '#8220'Editar G' +
        'rades dos Cadastros'#8221' estiver '
      'marcada o sistema permitir'#225' que '
      
        'o operador altere os dados na grade, com exce'#231#227'o dos c'#243'digos ID'#39 +
        's de cada cadastro.'
      
        '       - No Relat'#243'rio Extrato do Conveniado foi inclu'#237'da mais um' +
        'a op'#231#227'o de modelo de '
      'relat'#243'rio, a nova op'#231#227'o '#8220'Quatro por folha '
      
        '(corte) Totalizado por Fornecedor'#8221', demonstra os valores do conv' +
        'eniado no per'#237'odo solicitado '
      'agrupado por fornecedor, n'#227'o ser'#225' '
      'mostrado autoriza'#231#227'o por autoriza'#231#227'o como nos outros modelos.'
      '       '
      ''
      'Corre'#231#245'es efetuadas na vers'#227'o:'
      
        'No Relat'#243'rio Extrato do Conveniado quando selecionado o tipo '#8220'do' +
        'is por folha (corte)'#8221' a '
      'primeira pagina n'#227'o estava saindo na '
      'mesma posi'#231#227'o das outras, isto foi corrigido na vers'#227'o.'
      
        'No menu '#8220'Operacional'#8221', '#8220'lan'#231'amentos / Autoriza'#231#245'es'#8221' foram corrig' +
        'idos alguns problemas como '
      'data do lan'#231'amento n'#227'o estava '
      
        'gravando e lan'#231'amentos de cr'#233'ditos n'#227'o estavam gravando hist'#243'ric' +
        'os.'
      
        'No menu '#8220'Operacional'#8221', '#8220'Gera arquivo para cart'#227'o conveniado'#8221' foi' +
        ' corrigido um problema que '
      'colocava um cart'#227'o como '
      
        'dependente se o nome do cart'#227'o fosse diferente do titular, agora' +
        ' o programa verifica o campo '
      'titular do cart'#227'o se estiver igual a '
      
        'S mesmo o nome estando diferente ele informa o cart'#227'o como titul' +
        'ar.'
      
        'No cadastro de conveniados foi adicionado o campo chapa para bus' +
        'ca no cadastro.'
      ''
      'Vers'#227'o 0.0.0.30'
      
        '      - Uma altera'#231#227'o fundamental no sistema para quem usa parce' +
        'lamento foi implantada nesta '
      'vers'#227'o, no cadastro de Formas '
      
        'de Pagamento, foi criado um campo chamado '#8220'For'#231'ar dia de vencime' +
        'nto igual ao dia da compra'#8221'.  '
      'Qual a fun'#231#227'o deste campo? '
      
        'A fun'#231#227'o deste campo '#233' a seguinte, se ele n'#227'o estiver checado (m' +
        'arcado) o parcelamento '
      'funcionar'#225' como j'#225' funcionava antes '
      
        'desta op'#231#227'o, ex: se fosse solicitada uma autoriza'#231#227'o de venda em' +
        ' 3 pagamentos sendo '#224' vista, '
      '30 e 60 dias o sistema somaria '
      
        'o numero de dias da parcela '#224' data da venda, para obter a data d' +
        'e lan'#231'amento da parcela, '
      'podendo ocorrer uma diferen'#231'a nos '
      
        'dias em que o m'#234's n'#227'o tiver 30 dias, ex: Uma venda no dia 23 de ' +
        'janeiro (tem 31 dias), a '
      'segunda parcela cairia no dia 22 de '
      
        'fevereiro, e etc. Mas se o campo estiver checado o sistema simpl' +
        'esmente aumentar'#225' o m'#234's, ou '
      'seja, uma venda em 10 '
      
        'pagamentos no dia 25 de janeiro teria suas parcelas lan'#231'adas tod' +
        'os dias 25 at'#233' o m'#234's de '
      'outubro.(Obs: Se uma venda '
      
        'parcelada for feita no dia 31 ou 30(e uma das parcelas cair no m' +
        #234's de fevereiro), o sistema '
      'lan'#231'ar'#225' as parcelas no '#250'ltimo dia do '
      
        'm'#234's, sendo dia 30 para os meses que tem 30 dias e 29 ou 28 para ' +
        'fevereiro.).'
      
        '      - Uma nova op'#231#227'o foi adicionada ao '#8220'Relat'#243'rio Extrato do F' +
        'ornecedor'#8221', esta op'#231#227'o da a '
      'possibilidade do operador do '
      
        'sistema solicitar um relat'#243'rio com as autoriza'#231#245'es que j'#225' foram ' +
        'baixadas (pagas) do '
      'fornecedor em um per'#237'odo informado.Para '
      'isto basta marcar a op'#231#227'o '#8220'Incluir baixados'#8221'.'
      ''
      'Vers'#227'o 0.0.0.29'
      
        '       - Tr'#234's novas consultas foram adicionadas ao sistema nesta' +
        ' vers'#227'o, s'#227'o elas '
      #8220'Movimenta'#231#227'o de Empresas'#8221', '#8220'Movimenta'#231#227'o '
      
        'de Fornecedor'#8221', '#8221'Movimenta'#231#227'o de Conveniados'#8221' ambas no menu '#8220'Con' +
        'sultas'#8221'. Movimenta'#231#227'o de '
      'empresas demonstra a '
      
        'quantidade de autoriza'#231#245'es de d'#233'bitos e cr'#233'ditos dos conveniados' +
        ' de todas empresas '
      'cadastradas no sistema em um per'#237'odo '
      
        'selecionado, e a data da '#250'ltima autoriza'#231#227'o solicitada por algum' +
        ' conveniado da empresa, na '
      'barra de status (A  barra abaixo da '
      
        'janela) s'#227'o mostrados os totalizadores do per'#237'odo. Movimenta'#231#227'o ' +
        'de Fornecedores demonstra a '
      'quantidade de autoriza'#231#245'es de '
      
        'd'#233'bitos e cr'#233'ditos que foram solicitadas pelos fornecedores no p' +
        'er'#237'odo selecionado, os '
      'fornecedores sem movimenta'#231#227'o '
      
        'aparecer'#227'o com '#8220'0'#8221' nas quantidades de autoriza'#231#245'es, tamb'#233'm ser'#225' ' +
        'mostrada a data da '#250'ltima '
      'autoriza'#231#227'o seleciona pelo '
      
        'fornecedor, na barra de status ser'#227'o mostrados os totalizadores ' +
        'do per'#237'odo. Movimenta'#231#227'o de '
      'Conveniados demonstra '
      
        'individualmente as quantidades de autoriza'#231#245'es de d'#233'bitos e cr'#233'd' +
        'itos para todos os '
      'conveniados cadastrados ou de uma '
      
        'determinada empresa selecionada, em um per'#237'odo selecionado, os c' +
        'onveniados sem movimenta'#231#227'o '
      'aparecer'#227'o com '#8220'0'#8221' nas '
      
        'quantidades de autoriza'#231#245'es, na barra de status ser'#227'o mostrados ' +
        'os totalizadores do per'#237'odo.'
      
        'Todas as consultas exportam dados mostrados na grade para o Exce' +
        'l, basta clicar com o bot'#227'o '
      'direito do mouse sobre a grade '
      'e selecionar '#8220'Exportar para o Excel'#8221'.'
      
        'Obs: As consultas apresentadas s'#227'o para finalidade de estat'#237'stic' +
        'a, elas n'#227'o apresentam '
      'valores, e sim quantidades de '
      
        'autoriza'#231#245'es solicitadas, essas consultas tem como objetivo demo' +
        'nstrar quais conveniados, '
      'empresas e fornecedores t'#234'm '
      
        'maior e menor movimenta'#231#227'o na administradora. As datas das '#250'ltim' +
        'as autoriza'#231#245'es n'#227'o '
      'respeitam o per'#237'odo informado, visto '
      
        'que elas poderiam estar fora do mesmo. As consultas n'#227'o diferenc' +
        'iam autoriza'#231#245'es em aberto '
      'ou baixadas, s'#227'o contadas '
      
        'todas autoriza'#231#245'es do per'#237'odo exceto o lan'#231'amento de cr'#233'dito da ' +
        'baixa.'
      
        '       - Na consulta '#8220'Medi'#231#227'o de uso'#8221' foi adicionado um par'#226'metr' +
        'o para demonstrar somente as '
      'autoriza'#231#245'es em aberto.'
      
        '       - Na Baixa de empresa foi adicionado um par'#226'metro para es' +
        'colha de a rotina de baixa '
      'verificar'#225' se o conveniado que ter'#225' '
      
        'suas autoriza'#231#245'es baixadas tem que estar com o cart'#227'o liberado (' +
        'at'#233' esta vers'#227'o era '
      'obrigat'#243'rio que o cart'#227'o estivesse liberado '
      'para os lan'#231'amentos serem baixados).'
      ''
      'Corre'#231#245'es efetuadas na vers'#227'o:'
      
        'No calculo de saldo devedor de conveniado, no cadastro de conven' +
        'iados aba de conta corrente '
      'se '
      
        'a data da autoriza'#231#227'o fosse alterada a atualiza'#231#227'o n'#227'o estava jo' +
        'gando para o fechamento '
      'referente a nova data, isto foi corrigido '
      'nesta vers'#227'o.'
      ''
      ''
      'Vers'#227'o 0.0.0.28'
      
        '       - As rotinas de atualiza'#231#227'o de saldo do sistema est'#227'o dis' +
        'pon'#237'veis a partir desta '
      'vers'#227'o, existem tr'#234's n'#237'veis de atualiza'#231#227'o '
      
        'de saldo, por conveniado, no cadastro de conveniados, na aba con' +
        'ta corrente, por empresa, no '
      'cadastro de empresa na aba '
      
        'atualiza'#231#227'o de saldo, e a atualiza'#231#227'o de saldo geral do sistema ' +
        'que atualiza o saldo de '
      'todos conveniados cadastrados, no '
      'menu utilit'#225'rios / c'#225'lculo do saldo devedor.'
      'Quando utilizar determinado procedimento?'
      
        'A atualiza'#231#227'o de saldo do conveniado deve ser utilizada, caso al' +
        'gum valor da conta corrente '
      'deste conveniado seja alterado '
      'manualmente.'
      
        'A atualiza'#231#227'o de saldo da empresa (Esta rotina atualiza todos os' +
        ' conveniados vinculados '#224' '
      'empresa), deve ser utilizada quando '
      
        'a data de fechamento da empresa foi alterada, ou muitos convenia' +
        'dos desta empresa tiveram '
      'seus valores alterados '
      
        'manualmente. A atualiza'#231#227'o de saldo completa do sistema dever'#225' s' +
        'er usada para casos onde '
      'foram efetuados lan'#231'amentos '
      
        'manuais para diversos conveniados de diversas empresas (Ex: Lan'#231 +
        'amentos de taxas que n'#227'o '
      'verificam o limite do conveniado).'
      
        '       - Dois relat'#243'rios do sistema (Extrato de conveniado e Ext' +
        'rato de empresa) possuem '
      'agora a op'#231#227'o de visualiza'#231#227'o por '
      'per'#237'odo.'
      
        '       - Duas novas consultas foram adicionadas ao sistema, s'#227'o ' +
        'elas '#8220'Medi'#231#227'o de uso do '
      'sistema'#8221' e '#8220'Consulta de fechamentos'#8221', '
      
        'ambas no menu consultas. Medi'#231#227'o de uso do sistema demonstra qua' +
        'ntas autoriza'#231#245'es, por '
      'empresa (conveniados da '
      
        'empresa) que foram tiradas em um determinado per'#237'odo, ordenado p' +
        'or empresa e data, com o '
      'total por data, por empresa e '
      
        'total geral. Nesta ainda temos a op'#231#227'o de verificar o n'#250'mero de ' +
        'autoriza'#231#245'es por conveniado '
      'no per'#237'odo, na aba Usu'#225'rio por '
      
        'empresa. Consulta de Fechamentos Empresa / Fornecedor, esta cons' +
        'ulta demonstra todas as '
      'empresas com os respectivos '
      
        'valores que fecham ou que vencem em uma determinada data, idem p' +
        'ara os fornecedores.'
      ''
      'Corre'#231#245'es do efetuadas na vers'#227'o:'
      
        'Na rotina de baixa da empresa, foi constatado que se a baixa fos' +
        'se parcial, o restante do '
      'saldo devedor estava sendo lan'#231'ado '
      
        'na data da baixa, isso foi alterado para que o restante seja lan' +
        #231'ado no '#250'ltimo dia do '
      'per'#237'odo de compra que est'#225' sendo baixado.'
      ''
      'Vers'#227'o 0.0.0.27'
      
        '       - Uma mudan'#231'a sutil e brusca ao mesmo tempo no cadastro d' +
        'e empresas, o fechamento da '
      'empresa agora '#233' por "data de '
      
        'fechamento" ou seja, uma data '#233' especificada por m'#234's e ano como ' +
        'dia de fechamento da '
      'empresa, com isso a empresa pode, '
      
        'por exemplo, ter fechado o m'#234's de agosto no dia 25 e setembro fe' +
        'char no dia 26, estas '
      'altera'#231#245'es foram feitas esperando que '
      
        'no final do ano o dia de fechamento do m'#234's de dezembro n'#227'o seja ' +
        'o mesmo dos meses '
      'anteriores, e tamb'#233'm outras finalidades '
      
        'como o hist'#243'rico de fechamento dos meses anteriores. As datas se' +
        'r'#227'o geradas seguindo os dias '
      'informados no cadastro de '
      
        'empresa automaticamente na atualiza'#231#227'o do sistema (favor conferi' +
        'r). Somente ser'#225' necess'#225'ria '
      'altera'#231#227'o nas datas quando a '
      'empresa solicitar.'
      
        '       - No menu '#8220'relat'#243'rios'#8221' foi criado um novo relat'#243'rio (Extr' +
        'ato de demitidos), esse '
      'relat'#243'rio mostrar'#225' os funcion'#225'rios que foram '
      
        'demitidos da(s) empresa(s) em um per'#237'odo informado pelo operador' +
        ', marcando uma ou mais '
      'empresas e clicando na paleta '
      
        'titulares ser'#225' mostrado a lista dos funcion'#225'rios com os respecti' +
        'vos valores em aberto no '
      'sistema, o operador tem a op'#231#227'o de '
      
        'imprimir todos os marcados ou somente os que tiverem valor em ab' +
        'erto maior que zero e a '
      'op'#231#227'o de imprimir uma empresa por '
      'folha(se couber) ou seq'#252'encial (uma empresa embaixo da outra).'
      ''
      ''
      'Vers'#227'o 0.0.0.26'
      
        '       - Na Tela de lan'#231'amentos (Operacional / Lan'#231'amentos / aut' +
        'oriza'#231#245'es), foi feita uma '
      'altera'#231#227'o para que o usu'#225'rio informe o '
      'hist'#243'rico do lan'#231'amento.'
      
        '      - Para quem usava a gera'#231#227'o de arquivo para cria'#231#227'o de car' +
        't'#245'es, da vers'#227'o antiga do '
      #8220'AdmCart'#227'oBELLAConv'#8221' esta op'#231#227'o j'#225' est'#225' '
      'dispon'#237'vel na nova vers'#227'o.'
      
        '- Em todos os cadastros do sistema onde existem mais de dois cam' +
        'pos para pesquisa foi feita '
      'uma altera'#231#227'o para que a '
      
        'pesquisa seja incremental, ou seja, ex: no cadastro de conveniad' +
        'os for informado nome = JOAO '
      'e empresa = BELLA ent'#227'o s'#243' '
      
        'ser'#227'o retornados os nomes que conterem JOAO da empresa BELLA Con' +
        'v. Lembrando que as consultas de '
      'campos Alfanum'#233'ricos s'#227'o '
      
        'aproximadas, se for informado JOAO o sistema retornar'#225' todos que' +
        ' conterem JOAO ex: "JOAO '
      'LUCIO",'
      '"GUSTAVO JOAO".'
      ''
      'Vers'#227'o 0.0.0.25'
      
        '     - No menu '#8220'Consultas'#8221' / '#8220'Consulta de autoriza'#231#227'o'#8221', foi cria' +
        'da uma consulta para '
      'acompanhar o status das autoriza'#231#245'es, e '
      'visualizar todos os campos que est'#227'o ligados '#224' autoriza'#231#227'o.'
      '')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
end
