unit UManutencaoEmpConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, StdCtrls, Buttons, Menus, ExtCtrls, DB, ADODB;

type
  TfrmManutencaoEmpConv = class(TForm)
    Panel1: TPanel;
    btnImportarLimiteConv: TBitBtn;
    tExcel: TADOTable;
    procedure btnImportarLimiteConvClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutencaoEmpConv: TfrmManutencaoEmpConv;

implementation

uses DM, UValidacao, cartao_util;

{$R *.dfm}

procedure TfrmManutencaoEmpConv.btnImportarLimiteConvClick(
  Sender: TObject);
var OD : TOpenDialog;
renovacaoId,sql : String;
path,strSql : String;
erro: Boolean;
begin
  inherited;
 try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
     OD.Free;
    tExcel.Active := False;
    tExcel.ConnectionString := path;
    tExcel.TableName := 'Planilha1$';
    tExcel.Active := True;
    try
      tExcel.Open;
    except
      MsgErro('N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "conv" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    //btnImportar.Caption := 'Importar';
    Abort;
  end;
  //while not QManutencaoLimiteConv.Eof do
  //begin
      //if QManutencaoLimiteConvempres_id.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
      //begin

        Screen.Cursor := crHourGlass;
        while not tExcel.Eof do begin
          try

            if tExcel.Fields[0].AsString = '' then
            begin
              MsgInf('O campo CONV_ID da tabela n�o pode estar vazio');
              Abort;
            end;
            if tExcel.Fields[1].AsString = '' then
            begin
              MsgInf('O campo CART�O_ID da tabela n�o pode estar vazio');
              Abort;
            end;
            if tExcel.Fields[2].AsString = '' then
            begin
              MsgInf('O campo EMPRES_ID da tabela n�o pode estar vazio');
              Abort;
            end;
            if tExcel.Fields[3].AsString = '' then
            begin
              MsgInf('O campo EMPRES_ID_NOVO da tabela n�o pode estar vazio');
              Abort;
            end;

            sql:= 'UPDATE TRANSACOES SET EMPRES_ID = ' + tExcel.Fields[3].AsString +' WHERE CARTAO_ID = ' + tExcel.Fields[1].AsString;
            DMConexao.ExecuteSql(sql);
            sql:= 'UPDATE CONTACORRENTE SET EMPRES_ID = ' + tExcel.Fields[3].AsString +' WHERE CARTAO_ID = ' + tExcel.Fields[1].AsString;
            DMConexao.ExecuteSql(sql);
            sql:= 'UPDATE CARTOES SET EMPRES_ID = ' + tExcel.Fields[3].AsString +' WHERE CARTAO_ID = ' + tExcel.Fields[1].AsString;
            DMConexao.ExecuteSql(sql);
            sql:= 'UPDATE CONVENIADOS SET EMPRES_ID = ' + tExcel.Fields[3].AsString +' WHERE CONV_ID = ' + tExcel.Fields[0].AsString;
            DMConexao.ExecuteSql(sql);
            if tExcel.Fields[4].AsString = '' then begin
              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('select GRUPO_CONV_EMP_ID from GRUPO_CONV_EMP where EMPRES_ID = '+tExcel.Fields[3].AsString);
              DMConexao.AdoQry.Open;
              sql:= 'update CONVENIADOS set GRUPO_CONV_EMP = ' + DMConexao.AdoQry.Fields[0].AsString +' where conv_id = ' + tExcel.Fields[0].AsString;
              DMConexao.ExecuteSql(sql);
            end
            else begin
              sql:= 'update CONVENIADOS set GRUPO_CONV_EMP = ' + tExcel.Fields[4].AsString +' where conv_id = ' + tExcel.Fields[0].AsString;
              DMConexao.ExecuteSql(sql);
            end;

          except on E:Exception do begin
              MsgErro('Erro ao realizar altera��es!');
              DMConexao.AdoCon.RollbackTrans;
              Screen.Cursor := crDefault;
              tExcel.Close;
              abort;
            end;
          end;
          //QManutencaoLimiteConv.Next;






          tExcel.Next;
          Application.ProcessMessages;
        end;

        //DMConexao.AdoCon.CommitTrans;
        //QManutencaoLimiteConv.Requery;
        Screen.Cursor := crDefault;
        if tExcel.State in [dsEdit, dsInsert] then
          tExcel.Close;

        msginf('Atualiza��o realizada com sucesso!');

      //end

//      else
//         msginf('Planilha n�o pertence a empresa '+QManutencaoLimiteConvEMPRES_ID.AsString + '. Verifique o arquivo.');

  //end;
end;

end.
