unit UColunas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, CheckLst;

type
  TFColunas = class(TForm)
    Panel1: TPanel;
    ButAcima: TButton;
    ButAbaixo: TButton;
    ButOk: TButton;
    ButCancel: TButton;
    CheckList: TCheckListBox;
    Button1: TButton;
    Button2: TButton;
    procedure ButCancelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButAcimaClick(Sender: TObject);
    procedure ButAbaixoClick(Sender: TObject);
    procedure ButOkClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FColunas: TFColunas;

implementation

{$R *.dfm}

procedure TFColunas.ButCancelClick(Sender: TObject);
begin
 FColunas.ModalResult := mrCancel;
end;

procedure TFColunas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If key = vk_return then butok.Click;
 If key = vk_escape then ButCancel.Click;
end;

procedure TFColunas.ButAcimaClick(Sender: TObject);
var i : integer;
begin
if CheckList.ItemIndex > 0 then begin
   i := CheckList.ItemIndex;
   CheckList.Items.Move(CheckList.ItemIndex,CheckList.ItemIndex-1);
   CheckList.Selected[i-1] := True;
   CheckList.SetFocus;
   end;
end;

procedure TFColunas.ButAbaixoClick(Sender: TObject);
var i : integer;
begin
if CheckList.ItemIndex < CheckList.Items.Count-1 then begin
   i := CheckList.ItemIndex;
   CheckList.Items.Move(CheckList.ItemIndex,CheckList.ItemIndex+1);
   CheckList.Selected[i+1] := True;
   CheckList.SetFocus;
end;
end;

procedure TFColunas.ButOkClick(Sender: TObject);
begin
  FColunas.ModalResult := mrOk;
end;

procedure TFColunas.Button1Click(Sender: TObject);
var i : integer;
begin
 For i := 0 to CheckList.Items.Count-1 do CheckList.Checked[i] := True;
end;

procedure TFColunas.Button2Click(Sender: TObject);
var i : integer;
begin
 For i := 0 to CheckList.Items.Count-1 do CheckList.Checked[i] := False;
end;

end.
