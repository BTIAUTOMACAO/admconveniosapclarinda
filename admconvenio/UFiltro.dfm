object FFiltro: TFFiltro
  Left = 253
  Top = 156
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Filtro de dados do cadastro'
  ClientHeight = 344
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 344
    Align = alClient
    TabOrder = 0
    object Bevel1: TBevel
      Left = -2
      Top = 293
      Width = 635
      Height = 2
    end
    object Label1: TLabel
      Left = 237
      Top = 6
      Width = 81
      Height = 13
      Caption = 'Selecione o Filtro'
    end
    object Label2: TLabel
      Left = 12
      Top = 6
      Width = 97
      Height = 13
      Caption = 'Campos do cadastro'
    end
    object Label3: TLabel
      Left = 237
      Top = 47
      Width = 75
      Height = 13
      Caption = 'Campos do filtro'
    end
    object but1: TSpeedButton
      Left = 212
      Top = 112
      Width = 22
      Height = 25
      Hint = 'Incluir campo no filtro'
      Caption = '>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = but1Click
    end
    object but2: TSpeedButton
      Left = 212
      Top = 152
      Width = 22
      Height = 25
      Hint = 'Excluir campo do filtro.'
      Caption = '<'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = but2Click
    end
    object ButInsert: TButton
      Left = 85
      Top = 309
      Width = 77
      Height = 28
      Caption = '&Novo'
      TabOrder = 7
      OnClick = ButInsertClick
    end
    object ButDelete: TButton
      Left = 8
      Top = 309
      Width = 77
      Height = 28
      Caption = '&Excluir'
      TabOrder = 8
      OnClick = ButDeleteClick
    end
    object ButOk: TButton
      Left = 471
      Top = 309
      Width = 77
      Height = 28
      Caption = '&Ok'
      TabOrder = 4
      OnClick = ButOkClick
    end
    object ButCancel: TButton
      Left = 549
      Top = 309
      Width = 77
      Height = 28
      Caption = '&Cancelar'
      TabOrder = 5
      OnClick = ButCancelClick
    end
    object JvDBGrid2: TJvDBGrid
      Left = 2
      Top = 22
      Width = 208
      Height = 251
      Hint = 'Duplo clique para incluir o campo no filtro.'
      DataSource = DSCampos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = JvDBGrid2DblClick
      AutoAppend = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 16
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 171
          Visible = True
        end>
    end
    object JvDBGrid3: TJvDBGrid
      Left = 236
      Top = 62
      Width = 207
      Height = 212
      Hint = 'Clique com o bot'#227'o direito para excluir o campo'
      DataSource = DSDetalhes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentShowHint = False
      PopupMenu = PopupMenu1
      ShowHint = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = JvDBGrid3CellClick
      OnEnter = JvDBGrid3Enter
      AutoAppend = False
      TitleButtons = True
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 16
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'FIELD_NAME'
          Title.Caption = 'Descri'#231#227'o'
          Width = 171
          Visible = True
        end>
    end
    object DBFiltro: TDBLookupComboBox
      Left = 235
      Top = 22
      Width = 208
      Height = 21
      KeyField = 'FILTRO_ID'
      ListField = 'DESCRICAO'
      ListSource = DSFiltro
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 446
      Top = 19
      Width = 185
      Height = 256
      Caption = 'Compara'#231#227'o'
      TabOrder = 3
      object LabConj: TLabel
        Left = 15
        Top = 32
        Width = 51
        Height = 13
        Caption = 'Conectivo:'
        Visible = False
      end
      object LabTipoComp: TLabel
        Left = 15
        Top = 63
        Width = 99
        Height = 13
        Caption = 'Tipo de Compara'#231#227'o'
        Visible = False
      end
      object DBTipoComp: TDBComboBox
        Left = 16
        Top = 80
        Width = 157
        Height = 21
        Style = csDropDownList
        DataField = 'COMPARACAO_TIPO'
        DataSource = DSDetalhes
        DropDownCount = 11
        ItemHeight = 13
        Items.Strings = (
          'Igual (=)'
          'Maior Igual (>=)'
          'Menor Igual (<=)'
          'Maior (>)'
          'Menor (<)'
          'Entre (>= e <=)'
          'Iniciando Com'
          'Que Contenha'
          'Diferente (<>)'
          'Que n'#227'o Contenha'
          'Terminando Com'
          'Nulo'
          'N'#227'o Nulo')
        TabOrder = 1
        Visible = False
        OnChange = DBTipoCompChange
      end
      object DBConj: TDBComboBox
        Left = 70
        Top = 24
        Width = 54
        Height = 21
        Style = csDropDownList
        DataField = 'CONJUNCAO'
        DataSource = DSDetalhes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 13
        Items.Strings = (
          'E'
          'OU')
        ParentFont = False
        TabOrder = 0
        Visible = False
      end
      object PanComps: TPanel
        Left = 8
        Top = 112
        Width = 169
        Height = 137
        BevelOuter = bvNone
        TabOrder = 2
        OnExit = PanCompsExit
        object LabVal1: TLabel
          Left = 10
          Top = 5
          Width = 33
          Height = 13
          Caption = 'Valor 1'
          Visible = False
        end
        object LabVal2: TLabel
          Left = 10
          Top = 54
          Width = 33
          Height = 13
          Caption = 'Valor 2'
          Visible = False
        end
        object alfa1: TEdit
          Left = 10
          Top = 20
          Width = 153
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
          Visible = False
        end
        object alfa2: TEdit
          Left = 10
          Top = 68
          Width = 153
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 1
          Visible = False
        end
        object data2: TJvDateEdit
          Left = 10
          Top = 68
          Width = 106
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 2
          Visible = False
        end
        object data1: TJvDateEdit
          Left = 10
          Top = 20
          Width = 106
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 3
          Visible = False
        end
        object Float1: TJvValidateEdit
          Left = 10
          Top = 20
          Width = 83
          Height = 21
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfFloat
          DecimalPlaces = 2
          TabOrder = 4
          Visible = False
        end
        object Float2: TJvValidateEdit
          Left = 10
          Top = 68
          Width = 83
          Height = 21
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfFloat
          DecimalPlaces = 2
          TabOrder = 5
          Visible = False
        end
      end
    end
    object ButSum: TButton
      Left = 296
      Top = 309
      Width = 77
      Height = 28
      Caption = 'Sum'#225'rio'
      ParentShowHint = False
      ShowHint = False
      TabOrder = 6
      OnClick = ButSumClick
    end
  end
  object DSFiltro: TDataSource
    DataSet = QFiltro
    Left = 160
    Top = 240
  end
  object DSDetalhes: TDataSource
    DataSet = QDetalhes
    Left = 208
    Top = 240
  end
  object QCampos: TJvMemoryData
    FieldDefs = <
      item
        Name = 'Descricao'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Tipo'
        DataType = ftString
        Size = 20
      end>
    Left = 272
    Top = 296
    object QCamposDescricao: TStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object QCamposTipo: TStringField
      FieldName = 'Tipo'
    end
  end
  object DSCampos: TDataSource
    DataSet = QCampos
    Left = 272
    Top = 240
  end
  object PopupMenu1: TPopupMenu
    Left = 336
    Top = 296
    object Excluircampo1: TMenuItem
      Caption = 'Excluir campo'
      OnClick = Excluircampo1Click
    end
  end
  object QDetalhes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'filtro_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Value = 0
      end>
    SQL.Strings = (
      'select * from filtros_detalhes where filtro_id = :filtro_id')
    Left = 400
    Top = 304
    object QDetalhesID: TIntegerField
      FieldName = 'ID'
    end
    object QDetalhesFILTRO_ID: TIntegerField
      FieldName = 'FILTRO_ID'
    end
    object QDetalhesFIELD_NAME: TStringField
      FieldName = 'FIELD_NAME'
      Size = 45
    end
    object QDetalhesFIELD_TYPE: TStringField
      FieldName = 'FIELD_TYPE'
    end
    object QDetalhesCOMPARACAO_TIPO: TStringField
      FieldName = 'COMPARACAO_TIPO'
    end
    object QDetalhesVALOR1: TStringField
      FieldName = 'VALOR1'
      Size = 40
    end
    object QDetalhesVALOR2: TStringField
      FieldName = 'VALOR2'
      Size = 40
    end
    object QDetalhesCONJUNCAO: TStringField
      FieldName = 'CONJUNCAO'
      FixedChar = True
      Size = 3
    end
    object QDetalhesSUMARIO: TStringField
      FieldKind = fkCalculated
      FieldName = 'SUMARIO'
      Calculated = True
    end
  end
  object QFiltro: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from filtros order by descricao')
    Left = 432
    Top = 304
    object QFiltroFILTRO_ID: TIntegerField
      FieldName = 'FILTRO_ID'
    end
    object QFiltroDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 45
    end
    object QFiltroJANELA: TStringField
      FieldName = 'JANELA'
      Size = 30
    end
  end
end
