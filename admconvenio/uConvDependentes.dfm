object frmConvDependentes: TfrmConvDependentes
  Left = 281
  Top = 252
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Dependentes do Conveniado'
  ClientHeight = 161
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object lblTitular: TLabel
    Left = 0
    Top = 0
    Width = 369
    Height = 16
    Align = alTop
    Caption = 'Titular:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnSair: TButton
    Left = 0
    Top = 16
    Width = 369
    Height = 25
    Caption = '&Sair'
    TabOrder = 0
    OnClick = btnSairClick
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 41
    Width = 369
    Height = 120
    Align = alBottom
    DataSource = DSDependentes
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = JvDBGrid1DrawColumnCell
    TitleButtons = True
    OnTitleBtnClick = JvDBGrid1TitleBtnClick
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
  end
  object DSDependentes: TDataSource
    DataSet = QDependentes
    Left = 328
    Top = 80
  end
  object QDependentes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  c.nome,'
      '  c.codcartimp'
      'from'
      '  cartoes c'
      'where'
      'c.conv_id = :conv_id')
    Left = 296
    Top = 80
    object QDependentesnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QDependentescodcartimp: TStringField
      FieldName = 'codcartimp'
    end
  end
end
