unit uRotinasPopUp;

interface

uses Classes, menus;

function ItemMenuFactory(Caption,Name: String;AOwner: TPopupMenu;OnClickEvent:TNotifyEvent): TMenuItem;

implementation


function ItemMenuFactory(Caption,Name:String;AOwner:TPopupMenu;OnClickEvent:TNotifyEvent):TMenuItem;
begin
   Result := TMenuItem.Create(nil);
   Result.Caption := Caption;
   Result.OnClick := OnClickEvent;
   Result.Name    := Name;
end;

end.
