unit ULancamentoDebitoMesAnterior;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, DBCtrls, StdCtrls, Menus, Buttons, ExtCtrls,
  Mask, JvExMask, JvToolEdit, JvExControls, JvDBLookup, Math, ComCtrls;

type
  TFLancamentoDebitoMesAnterior = class(TF1)
    GroupBox1: TGroupBox;
    dsEmpresas: TDataSource;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    Panel1: TPanel;
    FilenameEdit1: TJvFilenameEdit;
    Label2: TLabel;
    Button1: TButton;
    Label5: TLabel;
    EdEmp_ID: TEdit;
    lbl4: TLabel;
    dblkpEmpresas: TJvDBLookupCombo;
    Table1: TADOTable;
    ProgressBar1: TProgressBar;
    Label1: TLabel;
    lbl1: TLabel;
    cbbMes: TComboBox;
    DsData: TDataSource;
    JvDBLCData: TJvDBLookupCombo;
    qData: TADOQuery;
    JvDBLookupCombo1: TJvDBLookupCombo;
    DsDataVenc: TDataSource;
    qDataVenc: TADOQuery;
    Label3: TLabel;
    Label4: TLabel;
    CBUnificada: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure jvdblkpcmb1Enter(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEmp_IDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBLCDataExit(Sender: TObject);
    procedure JvDBLCDataEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLancamentoDebitoMesAnterior: TFLancamentoDebitoMesAnterior;

implementation

uses DM, UValidacao, cartao_util;

{$R *.dfm}

procedure TFLancamentoDebitoMesAnterior.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  EdEmp_ID.SetFocus;
  //dbLkpEmpresas.Enabled := True;
end;

procedure TFLancamentoDebitoMesAnterior.jvdblkpcmb1Enter(
  Sender: TObject);
begin
  if EdEmp_ID.Text = '' then
       dblkpEmpresas.ClearValue
     else if QEmpresas.Locate('empres_id',EdEmp_ID.Text,[]) then  begin
       dblkpEmpresas.KeyValue := EdEmp_ID.Text;
       dblkpEmpresas.SetFocus;
       //lblStatus.Caption := '';
     end
     else
       dblkpEmpresas.ClearValue;
  if (EdEmp_ID.Text = '1650') or (EdEmp_ID.Text = '1665')   then
    begin
      Label3.Visible := true;
      Label4.Visible := true;
      JvDBLCData.Visible := true;
      JvDBLookupCombo1.Visible := true;
      qData.Close;
      qData.Parameters.ParamByName('empres_id').Value := strtoint(EdEmp_ID.Text);
      qData.Open;
    end
  else
    begin
      Label3.Visible := false;
      Label4.Visible := false;
      JvDBLCData.Visible := false;
      JvDBLookupCombo1.Visible := false;
    end;

  if (EdEmp_ID.Text = '2379') or (EdEmp_ID.Text = '669') then
    begin
      CBUnificada.Visible := true;
      CBUnificada.Checked := true;
    end
  else
    begin
      CBUnificada.Visible := false;
      CBUnificada.Checked := false;
    end;

  JvDBLookupCombo1.ClearValue;
  JvDBLCData.ClearValue;
end;

procedure TFLancamentoDebitoMesAnterior.Button1Click(Sender: TObject);
var path, nome, chapa,serieDoAluno, cpf, rg, dataNasc,dataFechaEmp,dataVencEmp, numdep,sDataAtual,sConvID,sChapa,sCartaoID,sEmpresID,sCredID,sCodAcesso,sValoTransacao,lancaTransacaoSemConsultaSaldo,sCodCard : String;
  conv_id, qtdImp, qtdErr,modeloCartaoCantinex ,grupo_conv_emp,cont,empres_id,I,quantidadeDePalavras,transID,autorID,coutData : Integer;
  limite,dValorTransacao, restanteProx, fechaAtual,dDebito : Double;
  limiteProximoFechamento,consumoProximoFechamento,limiteMes,fechamentoSobra,debito : Double;
  hasDependente,validou : Boolean;
  lista,listaNomesParaInserir : TStringlist;
  dataAtual : TDateTime;
  erro: Boolean;
  //item : TListItem;
  sqlCmdText : String;
begin
  inherited;
  qtdImp:= 0; qtdErr:= 0;
  erro:= False;
  screen.Cursor  := crHourGlass;
  if dblkpEmpresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  if cbbMes.Text = 'Selecione' then
  begin
    ShowMessage('Selecione o M�s para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  if FilenameEdit1.Text = 'C:\AdmCartaoBella\' then
  begin
    ShowMessage('Selecione o Arquivo para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  if (JvDBLCData.Text = '') and ((EdEmp_ID.Text = '1650') or (EdEmp_ID.Text = '1665')) then
  begin
    ShowMessage('Selecione a data de fechamento da empresa para importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  if (JvDBLookupCombo1.Text = '') and ((EdEmp_ID.Text = '1650') or (EdEmp_ID.Text = '1665'))then
  begin
    ShowMessage('Selecione a data de vencimento da empresa para importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  try
    path := '';
    if versaoOffice < 12 then
    begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end
    else
    begin
      path := 'Provider = Microsoft.ACE.OLEDB.12.0; Data Source = '+FilenameEdit1.Text+';';
      path := path + 'Extended Properties ="Excel 12.0 Xml;HDR=YES;IMEX=1";';
    end;
    Table1.Active := False;
    Table1.ConnectionString := path;
    Table1.TableName:= 'DEBITOS$';
    Table1.Active := True;
  except on E:Exception do
    ShowMessage('ULancamentoDebitoMesAnterior_102 - Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  ProgressBar1.Position := 0;
  Button1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
  restanteProx := 0;

  if CBUnificada.Checked = false then
    begin
      coutData := DMConexao.ExecuteScalar('SELECT count(DATA_FECHA) as cout FROM DIA_FECHA WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = '+ EdEmp_ID.Text +' ');
    end;

  while not Table1.Eof do
  begin
    autorID := 0;
    transID := 0;

    chapa := SoNumero(Table1.FieldByName('MATRICULA').AsString);

    //BUSCA DADOS CONVENIADOS
    if CBUnificada.Checked = false then
      begin
        sqlCmdText:= '';
        sqlCmdText := sqlCmdText +
                      'select cart.CODCARTIMP, cart.CARTAO_ID, conv.CONV_ID, conv.EMPRES_ID, conv.limite_mes, CONV.EMPRES_ID' +
                      ' from cartoes cart' +
                      ' inner join CONVENIADOS conv on cart.CONV_ID = conv.CONV_ID' +
                      ' where CONV.EMPRES_ID = '+ QuotedStr(EdEmp_ID.Text) +' and cart.TITULAR = '+QuotedStr('S') +
                      ' and CONV.CHAPA = '+ QuotedStr(chapa) +' ';
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := sqlCmdText;
        DMConexao.AdoQry.Open;
      end
    else
      begin
        sqlCmdText:= '';
        sqlCmdText := sqlCmdText +
                      'select cart.CODCARTIMP, cart.CARTAO_ID, conv.CONV_ID, conv.EMPRES_ID, conv.limite_mes, CONV.EMPRES_ID' +
                      ' from cartoes cart' +
                      ' inner join CONVENIADOS conv on cart.CONV_ID = conv.CONV_ID' +
                      ' where CONV.EMPRES_ID in(2379, 669) and cart.TITULAR = '+QuotedStr('S') +
                      ' and CONV.CHAPA = '+ QuotedStr(chapa) +' ';
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := sqlCmdText;
        DMConexao.AdoQry.Open;
      end;

    empres_id := StrToInt(DMConexao.AdoQry.FieldByName('EMPRES_ID').AsString);
    sEmpresID := DMConexao.AdoQry.FieldByName('EMPRES_ID').AsString;
    sValoTransacao := Table1.FieldByName('valor').AsString;

    if CBUnificada.Checked = true then
    begin
      if sEmpresID = '2379' then
        begin
          coutData := DMConexao.ExecuteScalar('SELECT count(DATA_FECHA) as cout FROM DIA_FECHA WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = 2379 ');
        end
      else
        begin
          coutData := DMConexao.ExecuteScalar('SELECT count(DATA_FECHA) as cout FROM DIA_FECHA WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = 669 ');
        end;
    end;

    fechaAtual := 0;
    restanteProx := 0;
    validou := false;

    if ((DMConexao.AdoQry.FieldByName('CONV_ID').AsString = '') OR (DMConexao.AdoQry.FieldByName('CARTAO_ID').AsString = '')) then begin
                MsgInf('UmanutencaoEmpConv2_134 - Verifique se o conveniado '+chapa+' esta cadastrado na empresa '+sEmpresID+' ');
                MsgInf('Importa��o cancelada');
                Abort;
    end else begin
        sConvID   := DMConexao.AdoQry.FieldByName('CONV_ID').AsString;
        sCartaoID := DMConexao.AdoQry.FieldByName('CARTAO_ID').AsString;
        sCodCard := DMConexao.AdoQry.FieldByName('CODCARTIMP').AsString;
        limiteMes := DMConexao.AdoQry.FieldByName('limite_mes').AsFloat;
        sCredID := '2';
        sCodAcesso := '23';
    end;

    // Percorre todas as datas encontradas para o fechamento
    for I := 0 to coutData - 1 do
    begin
      // Se for a data mais recente o restante recebe o valor jogado pel planilha
      if I=0 then
      begin
        restanteProx := Table1.FieldByName('valor').AsFloat;
      end;

      // Se o restante for positivo, ele continua
      if restanteProx > 0 then
      begin
        // Enquanto ele n�o tiver validado ele continua
        if not validou then
        begin
          //BUSCA DATA DO FECHAMENTO DA EMPRESA
          sqlCmdText:= '';
          sqlCmdText:= sqlCmdText +
          'with CTE_R as('+
          ' SELECT DATA_FECHA,DATA_VENC, ROW_NUMBER() OVER(ORDER BY DATA_FECHA) as RowNum '+
          ' FROM DIA_FECHA '+
          ' WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = '+ sEmpresID +
          ')'+
          'select * from CTE_R where RowNum = '+ IntToStr(I + 1);

          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Text := sqlCmdText;
          DMConexao.AdoQry.Open;

          if (sEmpresID = '1650') or (sEmpresID = '1665') then
          begin
            dataFechaEmp := JvDBLCData.Text;
            dataVencEmp := JvDBLookupCombo1.Text;
          end
          else
          begin
            dataFechaEmp :=  DMConexao.AdoQry.FieldByName('DATA_FECHA').AsString;
            dataVencEmp :=  DMConexao.AdoQry.FieldByName('DATA_VENC').AsString;
          end;

          // Busca debito dos conveniados
          sqlCmdText:= '';
          sqlCmdText := sqlCmdText +
                      'select coalesce(sum(debito - credito),0) as debito' +
                      ' from CONTACORRENTE' +
                      ' where conv_id = '+ QuotedStr(sConvID) +
                      ' and EMPRES_ID = '+ QuotedStr(sEmpresID) +
                      ' and DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('yyyMMdd 00:00:00',StrToDateTime(dataFechaEmp))) + '';
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Text := sqlCmdText;
          DMConexao.AdoQry.Open;

          dDebito := DMConexao.AdoQry.FieldByName('debito').AsFloat;
          fechamentoSobra :=  limiteMes - dDebito;

          // Se a sobra no fechamento for positiva ele continua o processo.
          if (fechamentoSobra > 0) or (sEmpresID = '1650') or (sEmpresID = '1665') then
          begin
            if (fechamentoSobra < restanteProx) and (sEmpresID <> '1650') and (sEmpresID <> '1665') then
            begin
             restanteProx := restanteProx - fechamentoSobra;
             fechaAtual := fechamentoSobra;
            end
            else
            begin
             fechaAtual := restanteProx;
             validou := true;
            end;

            dValorTransacao := fechaAtual;

            transID := DMConexao.ExecuteScalar('SELECT NEXT VALUE FOR STRANS_ID');
            autorID := DMConexao.ExecuteScalar('SELECT NEXT VALUE FOR SAUTORIZACAO_ID');

            dataAtual := Date;
            sDataAtual := FormatDateTime('dd/MM/yyyy', dataAtual);
            //MsgInf('Atual ThousandSeparator : ' + FormatFloat('###,##0.#0', dValorTransacao));
            ThousandSeparator := ',';
            DecimalSeparator := '.';

            sValoTransacao := FormatFloat('###,##0.#0', dValorTransacao);
            sValoTransacao := StringReplace(sValoTransacao,',','',[rfReplaceAll, rfIgnoreCase]);

            // INSERE NA TRANZA��ES
            sqlCmdText:= '';
            sqlCmdText := sqlCmdText
            + 'INSERT INTO dbo.TRANSACOES '
            + ' (TRANS_ID '
            + ' ,CODACESSO '
            + ' ,SENHA '
            + ' ,CRED_ID '
            + ' ,CARTAO '
            + ' ,CARTAO_ID '
            + ' ,CPF '
            + ' ,OPERADOR '
            + ' ,DATAHORA '
            + ' ,ABERTA '
            + ' ,CONFIRMADA '
            + ' ,CANCELADO '
            + ' ,DTCANCELADO '
            + ' ,OPERCANCELADO '
            + ' ,VALOR '
            + ' ,PONTOS '
            + ' ,CUPOM '
            + ' ,VALE_ACUMULADO '
            + ' ,VALE_UTILIZADO '
            + ' ,DTCONFIRMADA '
            + ' ,NSU '
            + ' ,ENTREGA '
            + ' ,EMPRES_ID '
            + ' ,ACCEPTORTAN) '
            + 'VALUES '
            + ' (' + IntToStr(transID)
            + ' ,' + sCodAcesso
            + ' ,' + QuotedStr('')
            + ' ,' + sCredID
            + ' ,' + sCodCard
            + ' ,' + sCartaoID
            + ' ,' + QuotedStr('')
            + ' ,' + QuotedStr('DEBITO_FECHA_' + cbbMes.Text)
            + ' ,' + QuotedStr(sDataAtual)
            + ' ,' + QuotedStr('N')
            + ' ,' + QuotedStr('S')
            + ' ,' + QuotedStr('N')
            + ' ,' + QuotedStr('')
            + ' ,' + QuotedStr('')
            + ' ,' + sValoTransacao
            + ' ,' + QuotedStr('0')
            + ' ,' + QuotedStr('')
            + ' ,' + QuotedStr('0')
            + ' ,' + QuotedStr('0')
            + ' ,' + QuotedStr('')
            + ' ,' + QuotedStr('0')
            + ' ,' + QuotedStr('')
            + ' ,' + sEmpresID
            + ' ,' + QuotedStr('0')+')';

            //COLOCAR A INSTRU��O DM AQUI
            DMConexao.ExecuteNonQuery(sqlCmdText);

            //INSERT NA AUTOR_TRANS
            sqlCmdText:= '';
            sqlCmdText := sqlCmdText
            + 'INSERT INTO dbo.AUTOR_TRANSACOES '
            + ' (AUTOR_ID '
            + ' ,TRANS_ID '
            + ' ,DATAHORA '
            + ' ,DOC_FISCAL '
            + ' ,DIGITO '
            + ' ,VALOR '
            + ' ,HISTORICO '
            + ' ,FORMAPAGTO_ID '
            + ' ,RECEITA '
            + ' ,DATA_VENC_EMP '
            + ' ,DATA_FECHA_EMP '
            + ' ,DATA_VENC_FOR '
            + ' ,DATA_FECHA_FOR '
            + ' ,CONV_ID '
            + ' ,CRED_ID '
            + ' ,CARTAO_ID) '
            + 'VALUES '
            + ' (' + IntToStr(autorID)
            + ' ,' + IntToStr(transID)
            + ' ,' + QuotedStr(DateToStr(Date))
            + ' ,0'
            + ' ,0'
            + ' ,' + sValoTransacao
            + ' ,'+ QuotedStr('DEBITO_FECHA_' + cbbMes.Text)
            + ' ,1'
            + ' ,'+ QuotedStr('N')
            + ' ,'+ QuotedStr(dataVencEmp)
            + ' ,'+ QuotedStr(dataFechaEmp)
            + ' ,'+ QuotedStr(dataVencEmp)
            + ' ,'+ QuotedStr(dataVencEmp)
            + ' ,' + sConvID
            + ' ,' + sCredID
            + ' ,' + sCartaoID + ')'
            ;

            DMConexao.ExecuteNonQuery(sqlCmdText);

            //INSERT NA CONTACORRENTE
            sqlCmdText:= '';
            sqlCmdText := sqlCmdText
            + 'INSERT INTO CONTACORRENTE ( '
            + '   autorizacao_id '
            + '	,digito '
            + '	,data '
            + '	,hora '
            + '	,datavenda '
            + '	,conv_id '
            + '	,cartao_id '
            + '	,cred_id '
            + '	,debito '
            + '	,credito '
            + '	,operador '
            + '	,historico '
            + '	,autorizacao_id_canc '
            + '	,formapagto_id '
            + '	,baixa_conveniado '
            + '	,baixa_credenciado '
            + '	,nf '
            + '	,receita '
            + '	,data_venc_emp '
            + '	,data_fecha_emp '
            + '	,data_venc_for '
            + '	,data_fecha_for '
            + '	,trans_id '
            + '	,previamente_cancelada '
            + '	,nsu '
            + '	,empres_id '
            + '	) '
            + 'VALUES ( '
            + '	' + IntToStr(autorID)
            + '	,1 '
            + '	,'+QuotedStr(sDataAtual)
            + '	,'+QuotedStr('00:00:00')
            + '	,'+QuotedStr(DateToStr(Date))
            + '	,' + sConvID
            + '	,' + sCartaoID
            + '	,' + sCredID
            + '	,' + sValoTransacao
            + '	,0.0 '
            + '	,'+ QuotedStr('DEBITO_FECHA_' + cbbMes.Text)
            + '	,'+ QuotedStr('DEBITO_FECHA_' + cbbMes.Text)
            + '	,NULL '
            + '	,1 '
            + '	,'+ QuotedStr('N')
            + '	,'+ QuotedStr('N')
            + '	,NULL'
            + '	,'+ QuotedStr('N')
            + '	,'+ QuotedStr(dataVencEmp)
            + '	,'+ QuotedStr(dataFechaEmp)
            + '	,'+ QuotedStr(dataVencEmp)
            + '	,'+ QuotedStr(dataFechaEmp)
            + '	,' + IntToStr(transID)
            + '	,'+ QuotedStr('N')
            + '	,0 '
            + '	,' + sEmpresID
            + '	)';

            DMConexao.ExecuteNonQuery(sqlCmdText);

            // Caso a data seja a mais pr�xima ele faz um update adicionando o valor da transa��o ao consumo do m�s.
            if I = 1 then
            begin
              sqlCmdText:= '';
              sqlCmdText := sqlCmdText +
                      ' UPDATE CONVENIADOS SET CONSUMO_MES_1 = CONSUMO_MES_1 + ' + sValoTransacao + ' WHERE CONV_ID = ' + sConvID +' ';
              DMConexao.ExecuteSql(sqlCmdText);
            end;
          end;
        end;
      end;
    end;

    Table1.Next;
    ProgressBar1.Position := ProgressBar1.Position +1;
  end;
  Screen.Cursor := crDefault;
  if Table1.State in [dsEdit, dsInsert] then
     Table1.Close;

  msginf('Lan�amento finalizado!');
  ProgressBar1.Position := 0;
end;



procedure TFLancamentoDebitoMesAnterior.FormShow(Sender: TObject);
begin
  inherited;
  EdEmp_ID.SetFocus;
end;

procedure TFLancamentoDebitoMesAnterior.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end ;


end;



procedure TFLancamentoDebitoMesAnterior.EdEmp_IDKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_TAB then
    begin
      if (EdEmp_ID.Text = '1650') or (EdEmp_ID.Text = '1665')   then
        begin
          JvDBLCData.Visible := true;
          CBUnificada.Visible := false;
          CBUnificada.Checked := false;
        end
      else
        begin
          JvDBLCData.Visible := false;
          CBUnificada.Visible := false;
          CBUnificada.Checked := false;
        end;
      if (EdEmp_ID.Text = '2379') or (EdEmp_ID.Text = '669') then
        begin
          CBUnificada.Visible := true;
          CBUnificada.Checked := true;
        end;
    end;

end;

procedure TFLancamentoDebitoMesAnterior.JvDBLCDataExit(Sender: TObject);
begin
  qDataVenc.Close;
  qDataVenc.Parameters.ParamByName('data_fecha').Value := JvDBLCData.Text;
  qDataVenc.Parameters.ParamByName('empres_id').Value := strtoint(EdEmp_ID.Text);
  qDataVenc.Open;

  JvDBLookupCombo1.ClearValue;
  JvDBLookupCombo1.KeyValue := qDataVenc.FieldValues['DATA_VENC'];
end;

procedure TFLancamentoDebitoMesAnterior.JvDBLCDataEnter(Sender: TObject);
begin
  JvDBLookupCombo1.ClearValue;

end;



end.
