// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:2510/wsconvenio.asmx?wsdl
// Encoding : utf-8
// Version  : 1.0
// (17/09/2014 08:41:21 - 1.33.2.5)
// ************************************************************************ //

unit wsconvenio_new;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : wsconvenio
  // soapAction: wsconvenio/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : wsconvenioSoap
  // service   : wsconvenio
  // port      : wsconvenioSoap
  // URL       : http://localhost:2510/wsconvenio.asmx
  // ************************************************************************ //
  wsconvenioSoap = interface(IInvokable)
  ['{165697DC-9E9C-5F36-D210-29DD6D99AE7C}']
    function  MObterEmpresasIncCart(const xml: WideString): WideString; stdcall;
    function  MAdicionarConveniado(const xml: WideString): WideString; stdcall;
    function  MObterGruposProd(const xml: WideString): WideString; stdcall;
    function  MListarEmpresas(const xml: WideString): WideString; stdcall;
    function  MConsultarCartoes(const xml: WideString): WideString; stdcall;
    function  MAbrirTransacao(const xml: WideString): WideString; stdcall;
    function  MValidarProdutos(const xml: WideString): WideString; stdcall;
    function  MFecharTransacao(const xml: WideString): WideString; stdcall;
    function  MConfirmarTransacao(const xml: WideString): WideString; stdcall;
    function  MObterTransacoesPendentes(const xml: WideString): WideString; stdcall;
    function  MCancelarTransacao(const xml: WideString): WideString; stdcall;
    function  MSolicitarResgate(const xml: WideString): WideString; stdcall;
    function  MValidarFormasPagto(const xml: WideString): WideString; stdcall;
    function  MConsultarTransacao(const xml: WideString): WideString; stdcall;
    function  MConsultarCartaoSeg(const xml: WideString): WideString; stdcall;
    function  MInformarNF(const xml: WideString): WideString; stdcall;
    function  MSolicitarAutorizacao(const xml: WideString): WideString; stdcall;
    function  MCancelarAutorizacao(const xml: WideString): WideString; stdcall;
    function  MConsultarFormasPagto(const xml: WideString): WideString; stdcall;
    function  MConsultarRegrasDesc(const xml: WideString): WideString; stdcall;
    function  MConsultarEmpresa(const xml: WideString): WideString; stdcall;
    function  MConsultarEmpresas(const xml: WideString): WideString; stdcall;
    function  MObterSaldoRestante(const xml: WideString): WideString; stdcall;
    function  MObterProdutosBloqueados(const xml: WideString): WideString; stdcall;
    function  MObterPagamentos(const xml: WideString): WideString; stdcall;
    function  MObterDetalhePagto(const xml: WideString): WideString; stdcall;
    function  MObterAutorizacoesPagto(const xml: WideString): WideString; stdcall;
    function  MInformarProdutos(const xml: WideString): WideString; stdcall;
    function  AbrirTransacao(const xml: WideString): WideString; stdcall;
    function  ValidarProdutos(const xml: WideString): WideString; stdcall;
    function  FecharTransacao(const xml: WideString): WideString; stdcall;
    function  ConfirmarTransacao(const xml: WideString): WideString; stdcall;
    function  ValidarFormasPagto(const xml: WideString): WideString; stdcall;
    function  ConsultarAutorizacao(const xml: WideString): WideString; stdcall;
    function  ConsultarCartao(const xml: WideString): WideString; stdcall;
    function  ConsultarCartaoSeg(const xml: WideString): WideString; stdcall;
    function  InformarNF(const xml: WideString): WideString; stdcall;
    function  InformarNFHist(const xml: WideString): WideString; stdcall;
    function  SolicitarAutorizacao(const xml: WideString): WideString; stdcall;
    function  CancelarAutorizacao(const xml: WideString): WideString; stdcall;
    function  ListarEmpresas(const xml: WideString): WideString; stdcall;
    function  ConsultarCartoes(const xml: WideString): WideString; stdcall;
    function  ObterGruposProd: WideString; stdcall;
    function  ConsultarFormasPagto: WideString; stdcall;
    function  ConsultarRegrasDesc(const xml: WideString): WideString; stdcall;
    function  ConsultarEmpresa(const xml: WideString): WideString; stdcall;
    function  ConsultarEmpresas: WideString; stdcall;
    function  ObterSaldoRestante(const xml: WideString): WideString; stdcall;
    function  ObterProdutosBloqueados(const xml: WideString): WideString; stdcall;
    function  ObterPagamentos(const xml: WideString): WideString; stdcall;
    function  ObterDetalhePagto(const xml: WideString): WideString; stdcall;
    function  ObterAutorizacoesPagto(const xml: WideString): WideString; stdcall;
    function  InformarProdutos(const xml: WideString): WideString; stdcall;
    function  ValidarLogin(const xml: WideString): WideString; stdcall;
  end;

function GetwsconvenioSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): wsconvenioSoap;


implementation

function GetwsconvenioSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): wsconvenioSoap;
const
  defWSDL = 'http://localhost:2510/wsconvenio.asmx?wsdl';
  defURL  = 'http://localhost:2510/wsconvenio.asmx';
  defSvc  = 'wsconvenio';
  defPrt  = 'wsconvenioSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as wsconvenioSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(wsconvenioSoap), 'wsconvenio', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(wsconvenioSoap), 'wsconvenio/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(wsconvenioSoap), ioDocument);
end.