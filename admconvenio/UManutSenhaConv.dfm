inherited FManutSenhaConv: TFManutSenhaConv
  Left = 1642
  Top = 124
  Caption = 'Manuten'#231#227'o de Senha de Conveniados'
  ClientWidth = 943
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label17: TLabel [0]
    Left = 503
    Top = 207
    Width = 16
    Height = 13
    Caption = 'RG'
  end
  inherited PageControl1: TPageControl
    Width = 943
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 935
        inherited Label2: TLabel
          Left = 213
        end
        object Label29: TLabel [8]
          Left = 953
          Top = 3
          Width = 45
          Height = 13
          Caption = 'Cart'#227'o ID'
          FocusControl = EdBuscaCartao
          Visible = False
        end
        object Label56: TLabel [9]
          Left = 478
          Top = 3
          Width = 31
          Height = 13
          Caption = 'Cha&pa'
          FocusControl = EdChapa
        end
        object Label31: TLabel [10]
          Left = 550
          Top = 3
          Width = 49
          Height = 13
          Caption = '&Empres ID'
          FocusControl = EdCodEmp
        end
        object Label32: TLabel [11]
          Left = 614
          Top = 3
          Width = 108
          Height = 13
          Caption = '&Raz'#227'o/Nome Empresa'
          FocusControl = EdNomeEmp
        end
        object Label33: TLabel [12]
          Left = 84
          Top = 3
          Width = 46
          Height = 13
          Caption = 'N'#186' Cart'#227'o'
        end
        inherited EdNome: TEdit
          Left = 213
          TabOrder = 3
        end
        inherited ButBusca: TBitBtn
          Left = 788
          Height = 42
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 851
          TabOrder = 10
        end
        inherited ButFiltro: TBitBtn
          TabOrder = 8
        end
        inherited ButAltLin: TButton
          TabOrder = 9
        end
        object EdBuscaCartao: TEdit
          Left = 953
          Top = 18
          Width = 55
          Height = 21
          Hint = 'Busca por c'#243'digo'
          AutoSelect = False
          CharCase = ecUpperCase
          TabOrder = 7
          Visible = False
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdChapa: TEdit
          Left = 478
          Top = 18
          Width = 66
          Height = 21
          Hint = 'Busca por chapa'
          AutoSelect = False
          CharCase = ecUpperCase
          TabOrder = 4
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdCodEmp: TEdit
          Left = 550
          Top = 18
          Width = 58
          Height = 21
          Hint = 'Busca por c'#243'digo da empresa'
          CharCase = ecUpperCase
          TabOrder = 5
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdNomeEmp: TEdit
          Left = 614
          Top = 18
          Width = 169
          Height = 21
          Hint = 'Busca por nome da empresa'
          CharCase = ecUpperCase
          TabOrder = 6
          OnKeyPress = EdNomeKeyPress
        end
        object EdCartao: TEdit
          Left = 84
          Top = 18
          Width = 119
          Height = 21
          Hint = 'Busca por n'#250'mero de cart'#227'o'
          CharCase = ecUpperCase
          TabOrder = 2
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 935
        Columns = <
          item
            Expanded = False
            FieldName = 'CONV_ID'
            Title.Caption = 'Cod.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHAPA'
            Title.Caption = 'Chapa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TITULAR'
            Title.Caption = 'Nome'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Title.Caption = 'Liberado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Title.Caption = 'Cod. Empresa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANTASIA'
            Title.Caption = 'Empresa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CARGO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SETOR'
            Title.Caption = 'Setor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_NASCIMENTO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RG'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENDERECO'
            Title.Caption = 'Logradouro'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUMERO'
            Title.Caption = 'Numero'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMPLEMENTO'
            Title.Caption = 'Complemento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CIDADE'
            Title.Caption = 'Cidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ESTADO'
            Title.Caption = 'UF'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE1'
            Title.Caption = 'Telefone 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE2'
            Title.Caption = 'Telefone 2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CELULAR'
            Title.Caption = 'Celular'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMAIL'
            Title.Caption = 'Email'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Width = 935
        Panels = <
          item
            Width = 50
          end>
      end
    end
    inherited TabFicha: TTabSheet
      OnExit = TabFichaExit
      inherited Panel2: TPanel
        Top = 446
        Width = 935
        Height = 45
        inherited btnFirstB: TSpeedButton
          Left = 112
        end
        inherited btnPriorB: TSpeedButton
          Left = 145
        end
        inherited btnNextB: TSpeedButton
          Left = 178
        end
        inherited btnLastB: TSpeedButton
          Left = 211
        end
        inherited ButEdit: TBitBtn
          Left = 634
          TabOrder = 5
          Visible = False
        end
        inherited ButInclui: TBitBtn
          Left = 447
          TabOrder = 3
          Visible = False
        end
        inherited ButApaga: TBitBtn
          Left = 540
          TabOrder = 4
          Visible = False
        end
        inherited ButGrava: TBitBtn
          Left = 254
          TabOrder = 1
          Visible = False
        end
        inherited ButCancela: TBitBtn
          Left = 340
          TabOrder = 2
          Visible = False
        end
        object ButLimpaSenha: TBitBtn
          Left = 4
          Top = 3
          Width = 100
          Height = 41
          Caption = 'Limpar Senha'
          TabOrder = 0
          OnClick = ButLimpaSenhaClick
          Glyph.Data = {
            96050000424D9605000000000000960300002800000020000000100000000100
            08000000000000020000232E0000232E0000D800000000000000FFFFFF00CCFF
            FF0099FFFF0066FFFF0033FFFF0000FFFF00FFCCFF00CCCCFF0099CCFF0066CC
            FF0033CCFF0000CCFF00FF99FF00CC99FF009999FF006699FF003399FF000099
            FF00FF66FF00CC66FF009966FF006666FF003366FF000066FF00FF33FF00CC33
            FF009933FF006633FF003333FF000033FF00FF00FF00CC00FF009900FF006600
            FF003300FF000000FF00FFFFCC00CCFFCC0099FFCC0066FFCC0033FFCC0000FF
            CC00FFCCCC00CCCCCC0099CCCC0066CCCC0033CCCC0000CCCC00FF99CC00CC99
            CC009999CC006699CC003399CC000099CC00FF66CC00CC66CC009966CC006666
            CC003366CC000066CC00FF33CC00CC33CC009933CC006633CC003333CC000033
            CC00FF00CC00CC00CC009900CC006600CC003300CC000000CC00FFFF9900CCFF
            990099FF990066FF990033FF990000FF9900FFCC9900CCCC990099CC990066CC
            990033CC990000CC9900FF999900CC9999009999990066999900339999000099
            9900FF669900CC66990099669900666699003366990000669900FF339900CC33
            990099339900663399003333990000339900FF009900CC009900990099006600
            99003300990000009900FFFF6600CCFF660099FF660066FF660033FF660000FF
            6600FFCC6600CCCC660099CC660066CC660033CC660000CC6600FF996600CC99
            660099996600669966003399660000996600FF666600CC666600996666006666
            66003366660000666600FF336600CC3366009933660066336600333366000033
            6600FF006600CC00660099006600660066003300660000006600FFFF3300CCFF
            330099FF330066FF330033FF330000FF3300FFCC3300CCCC330099CC330066CC
            330033CC330000CC3300FF993300CC9933009999330066993300339933000099
            3300FF663300CC66330099663300666633003366330000663300FF333300CC33
            330099333300663333003333330000333300FF003300CC003300990033006600
            33003300330000003300FFFF0000CCFF000099FF000066FF000033FF000000FF
            0000FFCC0000CCCC000099CC000066CC000033CC000000CC0000FF990000CC99
            000099990000669900003399000000990000FF660000CC660000996600006666
            00003366000000660000FF330000CC3300009933000066330000333300000033
            0000FF000000CC00000099000000660000003300000000000000002D092D0000
            00000000000000000000012B072B0000000000000000000000002D35350B5F00
            000000000000000000002B2B2B2B2B000000000000000000000009033B350B5F
            0000000000000000000007002B2B072B000000000000000000002D010B5F350B
            5F0000000000000000000700002B2B2B2B0000000000000000000035010A3B35
            04350000000000000000002B00002B2B072B0000000000000000000035010A5F
            350A352D00000000000000002B00002B2B012B010000000000000000340A0909
            3B350A353B5F5F3400000000070000002B2B012B2B2B2B2B0000000035013501
            09350909090909345F0000002B002B00002B00000000002B2B00000009350935
            01030203020302030A340000012B012B0000010000000100072B000000000009
            340102020101010102350000000000012B00000000000000002B000000000000
            350102020335350902350000000000002B000000012B2B00002B000000000000
            350101013500013501350000000000002B0000002B00002B002B000000000000
            3501010135071101092E0000000000002B0000002B002B00012B000000000000
            2E0901012D5F010235000000000000002B0000002B2B00002B00000000000000
            00350901010109350000000000000000002B01000000012B0000000000000000
            00000A3535350A00000000000000000000002B2B2B2B2B000000}
          NumGlyphs = 2
        end
      end
      inherited Panel3: TPanel
        Width = 935
        Height = 446
        object GroupBox2: TGroupBox
          Left = 2
          Top = 2
          Width = 931
          Height = 181
          Align = alTop
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 15
            Width = 39
            Height = 13
            Caption = 'Conv ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 81
            Top = 15
            Width = 37
            Height = 13
            Caption = 'Titular'
            FocusControl = dbEdit2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 527
            Top = 15
            Width = 37
            Height = 13
            Caption = 'Chapa'
            FocusControl = dbEdtChapa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = 663
            Top = 15
            Width = 54
            Height = 13
            Caption = 'Data Nasc.'
          end
          object Label7: TLabel
            Left = 450
            Top = 95
            Width = 28
            Height = 13
            Caption = 'Cargo'
            FocusControl = DBEdit5
            Visible = False
          end
          object Label8: TLabel
            Left = 577
            Top = 95
            Width = 67
            Height = 13
            Caption = 'Departamento'
            Visible = False
          end
          object Label30: TLabel
            Left = 98
            Top = 95
            Width = 49
            Height = 13
            Caption = 'Empresa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label58: TLabel
            Left = 8
            Top = 135
            Width = 107
            Height = 13
            Caption = 'C'#243'd. do Conv. na Emp'
            FocusControl = DBEdit4
          end
          object Label10: TLabel
            Left = 328
            Top = 55
            Width = 27
            Height = 13
            Caption = 'Bairro'
            FocusControl = DBEdit8
          end
          object Label9: TLabel
            Left = 10
            Top = 55
            Width = 54
            Height = 13
            Caption = 'Logradouro'
            FocusControl = DBEdit7
          end
          object Label11: TLabel
            Left = 525
            Top = 55
            Width = 33
            Height = 13
            Caption = 'Cidade'
          end
          object Label13: TLabel
            Left = 679
            Top = 55
            Width = 30
            Height = 13
            Caption = 'C.E.P.'
          end
          object Label12: TLabel
            Left = 474
            Top = 55
            Width = 14
            Height = 13
            Caption = 'UF'
          end
          object Label14: TLabel
            Left = 128
            Top = 135
            Width = 20
            Height = 13
            Caption = 'CPF'
            FocusControl = DBEdit12
          end
          object Label15: TLabel
            Left = 263
            Top = 135
            Width = 16
            Height = 13
            Caption = 'RG'
            FocusControl = DBEdit13
          end
          object Label93: TLabel
            Left = 280
            Top = 55
            Width = 12
            Height = 13
            Caption = 'N'#186
            FocusControl = DBEdit10
          end
          object Label39: TLabel
            Left = 10
            Top = 95
            Width = 66
            Height = 13
            Caption = 'Cod. Empresa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TLabel
            Left = 367
            Top = 135
            Width = 51
            Height = 13
            Caption = 'Telefone 1'
            FocusControl = DBEdit3
          end
          object Label18: TLabel
            Left = 471
            Top = 135
            Width = 51
            Height = 13
            Caption = 'Telefone 2'
            FocusControl = DBEdit3
          end
          object Label19: TLabel
            Left = 575
            Top = 135
            Width = 25
            Height = 13
            Caption = 'Email'
            FocusControl = DBEdit3
          end
          object DBEdit1: TDBEdit
            Left = 10
            Top = 30
            Width = 65
            Height = 21
            Hint = 'C'#243'digo do conveniado'
            TabStop = False
            Color = clBtnFace
            DataField = 'CONV_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object dbEdit2: TDBEdit
            Left = 81
            Top = 30
            Width = 440
            Height = 21
            Hint = 'Nome do titular'
            CharCase = ecUpperCase
            DataField = 'TITULAR'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object dbEdtChapa: TDBEdit
            Left = 527
            Top = 30
            Width = 126
            Height = 21
            Hint = 'Chapa/Matricula do titular na empresa'
            DataField = 'CHAPA'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 450
            Top = 110
            Width = 119
            Height = 21
            Hint = 'Cargo'
            CharCase = ecUpperCase
            DataField = 'CARGO'
            DataSource = DSCadastro
            TabOrder = 12
            Visible = False
          end
          object DBDateEdit3: TDBDateEdit
            Left = 663
            Top = 30
            Width = 111
            Height = 21
            Hint = 'Data de nascimento'
            DataField = 'DT_NASCIMENTO'
            DataSource = DSCadastro
            DialogTitle = 'Informe a data'
            GlyphKind = gkCustom
            Glyph.Data = {
              CE040000424DCE0400000000000036000000280000001C0000000E0000000100
              18000000000098040000232E0000232E00000000000000000000FDFDFDB7BFEE
              B7BFEEB7BFEEC9CEEFCFD2F0CFD2F0CFD2F0C9CEEFC9CEEFB7BFEEB7BFEEB7BF
              EEF9F9F9FDFDFDE9E9E9E7E7E8EBECECEDEDEDF0F0F0F2F2F2F2F2F2F0F0F0ED
              EDEDECECEDE9E9E9E9E9E9FDFDFDFFF7EFE5B17FFFEEDDFFEEDDFFEEDDFFEEDD
              FFEEDDFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDE5B17FFFF4E9F9F9F9DADADBF7F7
              F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7DADADBF9
              F9F9FFF7EFE5B17FFFEFE0FFEFE0FFEFE0D9CEC3FFEEDDE1E1E1D9CEC3FFEFE0
              FFEFE0FFEFE0E5B17FFFF4E9F9F9F9DBDBDCF8F8F8F8F8F8F8F8F8E9EAEAF7F7
              F7EEEEEEE9EAEAF8F8F8F8F8F8F8F8F8DBDBDCF9F9F9FFF9F3EDB986FFF4E9FF
              F4E9FFF4E98B8987E1E1E1D9D0C8858585FFEEDDFFF4E9FFF4E9EDB986FFF6EE
              FAFAFADFDFE0FAFAFAFAFAFAFAFAFACDCECEF7F7F7EBEBEBCBCCCDF7F7F7FAFA
              FAFAFAFADFDFE0FAFAFAFFF9F3F5C18EFFF6EEFFF6EEFFF6EE8B8987E6E6E6EB
              EBEB9A9A9AC4C5C5FFF6EEFFF6EEF5C18EFFF6EEFAFAFAE1E1E1FBFBFBFBFBFB
              FBFBFBCDCECEF7F7F7F7F7F7D2D3D3E5E5E6FBFBFBFBFBFBE1E1E1FAFAFAFFF9
              F3FBC894FFF9F3FFF9F3D9D4D0767676E6E6E6FFF6EEB1B0AEA9A9A9FFF9F3FF
              F9F3FBC894FFF6EEFBFBFBE4E5E5FCFCFCFCFCFCEEEEEEC5C6C7F7F7F7F9F9F9
              DFDFE0D6D7D7FCFCFCFCFCFCE4E5E5FBFBFBFFFBF6FFCC98FFFCF8FFFCF8CCCC
              CC767676E9EAEA7676767676768B8987EBEBEBFFFCF8FFCC98FFF9F3FBFBFBE9
              EAEAFEFEFEFEFEFEE9EAEAC5C6C7F7F7F7C5C5C6C4C5C5D1D1D2F7F7F7FEFEFE
              E9EAEAFBFBFBFFFBF6FFCC98FFFEFCFFFEFCFFFEFCFFFEFCFFFEFCFFFEFCFFFE
              FCFFFEFCFFFEFCFFFEFCFFCC98FFF9F3FCFCFCEAEAEBFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEBFCFCFCFAFAFAB6B7B8
              BFC3EABFC3EABFC3EABFC3EABFC3EABFC3EAB7BDE9BFC3EABFC3EABFC3EAB6B7
              B8F7F7F7FBFBFBDFDFE0EDEDEDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
              EEEEEEEEEEEDEDEDDFDFE0FBFBFBF0F0F00526D11739E46576BA6576BA1739E4
              1739E41739E41739E41132DD1132DD0F30DB0526D1EAEAEBF7F7F7B8B9B9BFC0
              C1CFCFD0CBCCCDBFC0C1BFC0C1BFC0C1BFC0C1BEBFBFBDBEBEBCBDBEB8B9B9F7
              F7F7F0F0F01739E45972F2B1B0AE9A9A9A5972F23B5DF73B5DF73B5DF76576BA
              6576BA3B5DF71739E4EAEAEBF7F7F7BFC0C1D1D1D2DDDEDED6D7D7CDCECECDCE
              CECDCECECBCCCDC5C6C7C5C6C7CBCCCDBFC0C1F7F7F7F3F3F33B5DF75972F29A
              9A9A9A9A9A5972F23B5DF73B5DF75972F29A9A9A9A9A9A5972F23B5DF7EEEEEE
              F9F9F9CBCCCDCBCCCDD5D5D6D2D3D3CFCFD0CFCFD0CFCFD0CFCFD0D2D3D3D5D5
              D6CBCCCDCBCCCDF9F9F9FDFDFDE2E3E3B7BDE9BCBDBEB5B6B7BFC3EABFC3EABF
              C3EABFC3EAB5B6B7BCBDBEB7BDE9E3E4E4FCFCFCFEFEFEF7F7F7E5E5E6E4E5E5
              DFDFE0EBEBEBF7F7F7F7F7F7EBEBEBDFDFE0E4E5E5E5E5E6F7F7F7FEFEFEFFFF
              FFFFFFFFEBEBEBBFC0C1C4C5C5EEEEEEFFFFFFFFFFFFEEEEEEC4C5C5BFC0C1EB
              EBEBFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E4E5E5E6E6E6FAFAFAFFFFFFFFFFFF
              FAFAFAE6E6E6E4E5E5F8F8F8FFFFFFFFFFFF}
            NumGlyphs = 2
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 150
            Width = 113
            Height = 21
            Hint = 'C'#243'digo do conveniado na empresa'
            CharCase = ecUpperCase
            DataField = 'COD_EMPRESA'
            DataSource = DSCadastro
            TabOrder = 14
          end
          object DBEmpresa: TJvDBLookupCombo
            Left = 96
            Top = 110
            Width = 338
            Height = 21
            Hint = 'Nome da empresa'
            DisplayAllFields = True
            DataField = 'EMPRES_ID'
            DataSource = DSCadastro
            EmptyValue = '0'
            LookupField = 'EMPRES_ID'
            LookupDisplay = 'nome'
            LookupDisplayIndex = -1
            LookupSource = DSEmpresa
            TabOrder = 11
            OnChange = DBEmpresaChange
          end
          object DBEdit8: TDBEdit
            Left = 328
            Top = 70
            Width = 137
            Height = 21
            Hint = 'Bairro do titular'
            CharCase = ecUpperCase
            DataField = 'BAIRRO'
            DataSource = DSCadastro
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 10
            Top = 70
            Width = 263
            Height = 21
            Hint = 'Endere'#231'o do titular'
            CharCase = ecUpperCase
            DataField = 'ENDERECO'
            DataSource = DSCadastro
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 679
            Top = 70
            Width = 96
            Height = 21
            Hint = 'Cep'
            DataField = 'CEP'
            DataSource = DSCadastro
            TabOrder = 9
          end
          object DBEdit12: TDBEdit
            Left = 128
            Top = 150
            Width = 129
            Height = 21
            Hint = 'CPF'
            DataField = 'CPF'
            DataSource = DSCadastro
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 15
          end
          object DBEdit13: TDBEdit
            Left = 263
            Top = 150
            Width = 97
            Height = 21
            Hint = 'Registro Geral'
            DataField = 'RG'
            DataSource = DSCadastro
            TabOrder = 16
          end
          object DBEdit10: TDBEdit
            Left = 280
            Top = 70
            Width = 42
            Height = 21
            DataField = 'NUMERO'
            DataSource = DSCadastro
            TabOrder = 5
          end
          object dbLkpCidades: TDBLookupComboBox
            Left = 526
            Top = 70
            Width = 145
            Height = 21
            DataField = 'CIDADE'
            DataSource = DSCadastro
            KeyField = 'NOME'
            ListField = 'NOME'
            TabOrder = 8
          end
          object dbLkpEstados: TDBLookupComboBox
            Left = 472
            Top = 70
            Width = 47
            Height = 21
            DataField = 'ESTADO'
            DataSource = DSCadastro
            KeyField = 'UF'
            ListField = 'UF'
            TabOrder = 7
          end
          object edtEmpr: TEdit
            Left = 10
            Top = 110
            Width = 79
            Height = 21
            TabOrder = 10
          end
          object DBSetor: TJvDBLookupCombo
            Left = 576
            Top = 110
            Width = 201
            Height = 21
            Hint = 'Nome da empresa'
            DisplayAllFields = True
            DataField = 'SETOR'
            DataSource = DSCadastro
            EmptyValue = '0'
            LookupField = 'DEPT_ID'
            LookupDisplay = 'DESCRICAO'
            LookupDisplayIndex = -1
            TabOrder = 13
            Visible = False
          end
          object DBEdit3: TDBEdit
            Left = 367
            Top = 150
            Width = 97
            Height = 21
            Hint = 'Registro Geral'
            DataField = 'TELEFONE1'
            DataSource = DSCadastro
            TabOrder = 17
          end
          object DBEdit9: TDBEdit
            Left = 471
            Top = 152
            Width = 97
            Height = 21
            Hint = 'Registro Geral'
            DataField = 'TELEFONE2'
            DataSource = DSCadastro
            TabOrder = 18
          end
          object DBEdit6: TDBEdit
            Left = 575
            Top = 152
            Width = 202
            Height = 21
            Hint = 'Registro Geral'
            DataField = 'RG'
            DataSource = DSCadastro
            TabOrder = 19
          end
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 192
          Width = 487
          Height = 233
          Caption = 'Dependente(S)'
          TabOrder = 1
          object grdDependentes: TJvDBGrid
            Left = 8
            Top = 25
            Width = 473
            Height = 200
            DataSource = DSCartao
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 17
            TitleRowHeight = 17
            Columns = <
              item
                Expanded = False
                FieldName = 'CONV_ID'
                ReadOnly = True
                Title.Caption = 'Cod.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME'
                ReadOnly = True
                Width = 249
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CODCARTIMP'
                ReadOnly = True
                Title.Caption = 'Num. Cart'#227'o'
                Width = 140
                Visible = True
              end>
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 935
      end
      inherited GridHistorico: TJvDBGrid
        Width = 935
        Columns = <
          item
            Expanded = False
            FieldName = 'DETALHE'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_HORA'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CADASTRO'
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAMPO'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_ANT'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_POS'
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERACAO'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MOTIVO'
            Width = 493
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SOLICITANTE'
            Visible = True
          end>
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 943
  end
  inherited panStatus2: TPanel
    Width = 943
  end
  inherited DSCadastro: TDataSource
    Left = 572
    Top = 392
  end
  inherited PopupBut: TPopupMenu
    Left = 812
    Top = 96
  end
  inherited QHistorico: TADOQuery
    Left = 612
    Top = 361
  end
  inherited DSHistorico: TDataSource
    Left = 612
    Top = 393
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      '  select'
      '    conveniados.CONV_ID,'
      '    conveniados.TITULAR,'
      '    conveniados.LIBERADO,'
      '    conveniados.EMPRES_ID,'
      '    conveniados.GRUPO_CONV_EMP,'
      '    conveniados.CARGO,'
      '    conveniados.SETOR,'
      '    conveniados.COD_EMPRESA,'
      '    conveniados.DT_NASCIMENTO,'
      '    conveniados.CPF,'
      '    conveniados.RG,'
      '    conveniados.ENDERECO,'
      '    conveniados.NUMERO,'
      '    conveniados.BAIRRO,'
      '    conveniados.CIDADE,'
      '    conveniados.ESTADO,'
      '    conveniados.CEP,'
      '    conveniados.BANCO,'
      '    conveniados.AGENCIA,'
      '    conveniados.CONTACORRENTE,'
      '    conveniados.DIGITO_CONTA,'
      '    conveniados.TIPOPAGAMENTO,'
      '    conveniados.TELEFONE1,'
      '    conveniados.TELEFONE2,'
      '    conveniados.CELULAR,'
      '    conveniados.OBS1,'
      '    conveniados.OBS2,'
      '    conveniados.DTCADASTRO,'
      '    conveniados.OPERCADASTRO,'
      '    conveniados.DTALTERACAO,'
      '    conveniados.OPERADOR,'
      '    conveniados.DTULTCESTA,'
      '    conveniados.DTASSOCIACAO,'
      '    conveniados.EMAIL,'
      '    conveniados.LIMITE_MES,'
      '    conveniados.CONSUMO_MES_1,'
      '    conveniados.CONSUMO_MES_2,'
      '    conveniados.CONSUMO_MES_3,'
      '    conveniados.CONSUMO_MES_4,'
      '    conveniados.LIMITE_TOTAL,'
      '    conveniados.LIMITE_PROX_FECHAMENTO,'
      '    conveniados.CESTABASICA,'
      '    conveniados.SALARIO,'
      '    conveniados.FIDELIDADE,'
      '    conveniados.CONTRATO,'
      '    conveniados.TIPOSALARIO,'
      '    conveniados.FLAG,'
      '    conveniados.SENHA,'
      '    conveniados.SETOR_ID,'
      '    conveniados.DTAPAGADO,'
      '    conveniados.APAGADO,'
      '    conveniados.VALE_DESCONTO,'
      '    conveniados.LIBERA_GRUPOSPROD,'
      '    conveniados.COMPLEMENTO,'
      '    conveniados.USA_SALDO_DIF,'
      '    conveniados.ABONO_MES,'
      '    conveniados.SALDO_RENOVACAO,'
      '    conveniados.SALDO_ACUMULADO,'
      '    conveniados.DATA_ATUALIZACAO_ACUMULADO,'
      '    conveniados.CHAPA,'
      '   conveniados.DATA_ADMISSAO,'
      '   conveniados.DATA_DEMISSAO,'
      '   conveniados.NUM_DEPENDENTES,'
      '   conveniados.SALDO_DEVEDOR,'
      '   conveniados.SALDO_DEVEDOR_FAT,'
      '   conveniados.PIS,'
      '   conveniados.NOME_PAI,'
      '   conveniados.NOME_MAE,'
      '   conveniados.CART_TRAB_NUM,'
      '   conveniados.CART_TRAB_SERIE,'
      '   conveniados.REGIME_TRAB,'
      '   conveniados.VENC_TOTAL,'
      '   conveniados.FIM_CONTRATO,'
      '   conveniados.DISTRITO,'
      '   conveniados.ESTADO_CIVIL,'
      '   empresas.FANTASIA,'
      '   empresas.BAND_ID,'
      '   empresas.nome empresa,'
      '   empresas.TIPO_CREDITO'
      '  from conveniados'
      
        '  join empresas on empresas.empres_id = conveniados.empres_id an' +
        'd empresas.apagado <> '#39'S'#39
      '  where conveniados.conv_id = 0')
    Left = 572
    Top = 361
    object QCadastroCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCadastroTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCadastroGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object QCadastroCARGO: TStringField
      FieldName = 'CARGO'
      Size = 45
    end
    object QCadastroSETOR: TStringField
      FieldName = 'SETOR'
      Size = 45
    end
    object QCadastroCOD_EMPRESA: TStringField
      FieldName = 'COD_EMPRESA'
      Size = 30
    end
    object QCadastroDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
    object QCadastroCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCadastroRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QCadastroENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCadastroNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCadastroBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QCadastroAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QCadastroCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object QCadastroDIGITO_CONTA: TStringField
      FieldName = 'DIGITO_CONTA'
      Size = 2
    end
    object QCadastroTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object QCadastroTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QCadastroTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QCadastroCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QCadastroOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QCadastroOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroDTULTCESTA: TDateTimeField
      FieldName = 'DTULTCESTA'
    end
    object QCadastroDTASSOCIACAO: TDateTimeField
      FieldName = 'DTASSOCIACAO'
    end
    object QCadastroEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QCadastroLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QCadastroCONSUMO_MES_1: TBCDField
      FieldName = 'CONSUMO_MES_1'
      Precision = 15
      Size = 2
    end
    object QCadastroCONSUMO_MES_2: TBCDField
      FieldName = 'CONSUMO_MES_2'
      Precision = 15
      Size = 2
    end
    object QCadastroCONSUMO_MES_3: TBCDField
      FieldName = 'CONSUMO_MES_3'
      Precision = 15
      Size = 2
    end
    object QCadastroCONSUMO_MES_4: TBCDField
      FieldName = 'CONSUMO_MES_4'
      Precision = 15
      Size = 2
    end
    object QCadastroLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object QCadastroLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object QCadastroCESTABASICA: TBCDField
      FieldName = 'CESTABASICA'
      Precision = 15
      Size = 2
    end
    object QCadastroSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object QCadastroFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object QCadastroCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QCadastroTIPOSALARIO: TStringField
      FieldName = 'TIPOSALARIO'
      Size = 15
    end
    object QCadastroFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroSETOR_ID: TIntegerField
      FieldName = 'SETOR_ID'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object QCadastroLIBERA_GRUPOSPROD: TStringField
      FieldName = 'LIBERA_GRUPOSPROD'
      FixedChar = True
      Size = 1
    end
    object QCadastroCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object QCadastroUSA_SALDO_DIF: TStringField
      FieldName = 'USA_SALDO_DIF'
      FixedChar = True
      Size = 1
    end
    object QCadastroABONO_MES: TBCDField
      FieldName = 'ABONO_MES'
      Precision = 15
      Size = 2
    end
    object QCadastroSALDO_RENOVACAO: TBCDField
      FieldName = 'SALDO_RENOVACAO'
      Precision = 15
      Size = 2
    end
    object QCadastroSALDO_ACUMULADO: TBCDField
      FieldName = 'SALDO_ACUMULADO'
      Precision = 15
      Size = 2
    end
    object QCadastroDATA_ATUALIZACAO_ACUMULADO: TDateTimeField
      FieldName = 'DATA_ATUALIZACAO_ACUMULADO'
    end
    object QCadastroCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QCadastroDATA_ADMISSAO: TDateTimeField
      FieldName = 'DATA_ADMISSAO'
    end
    object QCadastroDATA_DEMISSAO: TDateTimeField
      FieldName = 'DATA_DEMISSAO'
    end
    object QCadastroNUM_DEPENDENTES: TIntegerField
      FieldName = 'NUM_DEPENDENTES'
    end
    object QCadastroSALDO_DEVEDOR: TBCDField
      FieldName = 'SALDO_DEVEDOR'
      Precision = 15
      Size = 2
    end
    object QCadastroSALDO_DEVEDOR_FAT: TBCDField
      FieldName = 'SALDO_DEVEDOR_FAT'
      Precision = 15
      Size = 2
    end
    object QCadastroPIS: TFloatField
      FieldName = 'PIS'
    end
    object QCadastroNOME_PAI: TStringField
      FieldName = 'NOME_PAI'
      Size = 45
    end
    object QCadastroNOME_MAE: TStringField
      FieldName = 'NOME_MAE'
      Size = 45
    end
    object QCadastroCART_TRAB_NUM: TIntegerField
      FieldName = 'CART_TRAB_NUM'
    end
    object QCadastroCART_TRAB_SERIE: TStringField
      FieldName = 'CART_TRAB_SERIE'
      Size = 10
    end
    object QCadastroREGIME_TRAB: TStringField
      FieldName = 'REGIME_TRAB'
      Size = 45
    end
    object QCadastroVENC_TOTAL: TBCDField
      FieldName = 'VENC_TOTAL'
      Precision = 15
      Size = 2
    end
    object QCadastroFIM_CONTRATO: TDateTimeField
      FieldName = 'FIM_CONTRATO'
    end
    object QCadastroDISTRITO: TStringField
      FieldName = 'DISTRITO'
      Size = 45
    end
    object QCadastroESTADO_CIVIL: TStringField
      FieldName = 'ESTADO_CIVIL'
      Size = 25
    end
    object QCadastroFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object QCadastroBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object QCadastroempresa: TStringField
      FieldName = 'empresa'
      Size = 60
    end
    object QCadastroTIPO_CREDITO: TIntegerField
      FieldName = 'TIPO_CREDITO'
    end
    object QCadastroBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QCadastroCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QCadastroESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object QEmpresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select empres_id, nome, coalesce(usa_cod_importacao,'#39'N'#39') as usa_' +
        'cod_importacao,'
      'case when fantasia is null'
      'then nome else fantasia end as fantasia,'
      'coalesce(fidelidade,'#39'N'#39') as fidelidade,'
      'band_id'
      'from empresas where apagado <> '#39'S'#39' order by nome')
    Left = 568
    Top = 424
    object QEmpresaempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresausa_cod_importacao: TStringField
      FieldName = 'usa_cod_importacao'
      ReadOnly = True
      Size = 1
    end
    object QEmpresafantasia: TStringField
      FieldName = 'fantasia'
      ReadOnly = True
      Size = 60
    end
    object QEmpresafidelidade: TStringField
      FieldName = 'fidelidade'
      ReadOnly = True
      Size = 1
    end
    object QEmpresaband_id: TIntegerField
      FieldName = 'band_id'
    end
  end
  object DSEmpresa: TDataSource
    DataSet = QEmpresa
    Left = 616
    Top = 424
  end
  object QCartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT CONV_ID, NOME, CODCARTIMP FROM CARTOES WHERE CONV_ID = :C' +
        'ONV_ID')
    Left = 572
    Top = 321
    object QCartaoCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCartaoNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QCartaoCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
  end
  object DSCartao: TDataSource
    DataSet = QCartao
    Left = 604
    Top = 321
  end
end
