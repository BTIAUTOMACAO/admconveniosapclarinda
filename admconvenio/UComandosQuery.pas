unit UComandosQuery;

interface

uses Windows, SysUtils, Dialogs, Controls, cartao_util, DB, uValidacao;

function fnNewFirst(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
function fnNewPrior(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
function fnNewNext(DataSet  : TDataSet ; Mensagem : String = '') : Boolean;
function fnNewLast(DataSet  : TDataSet ; Mensagem : String = '') : Boolean;
function fnAcessaDataSet(DataSet : TDataSet ; Mensagem : String = '') : Boolean;

implementation

function fnAcessaDataSet(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
begin
Result := False;
  if DataSet.IsEmpty then Abort
  else prVerfEAbreCon(DataSet);
  if fnQtdRegistro(Mensagem, DataSet) then Abort;
Result := True;
end;

function fnNewFirst(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
begin
Result := fnAcessaDataSet(DataSet, Mensagem);
DataSet.Next;
end;

function fnNewPrior(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
begin
Result := fnAcessaDataSet(DataSet, Mensagem);
DataSet.Next;
end;

function fnNewNext(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
begin
Result := fnAcessaDataSet(DataSet, Mensagem);
DataSet.Next;
end;

function fnNewLast(DataSet : TDataSet ; Mensagem : String = '') : Boolean;
begin
Result := fnAcessaDataSet(DataSet, Mensagem);
DataSet.Next;
end;




end.
