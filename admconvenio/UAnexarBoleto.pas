unit UAnexarBoleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, Mask, ToolEdit, ExtCtrls, Buttons,
  DB, ZAbstractRODataset, ZDataset, {JvLookup,} ADODB, ZStoredProcedure,
  ZSqlUpdate, ZAbstractDataset, JvToolEdit, ShellApi, JvExControls,
  JvDBLookup, JvExMask, dblookup, RXDBCtrl;

type
  TFAnexarBoleto = class(TForm)
    ProgressBar1: TProgressBar;
    DSEmpresas: TDataSource;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasUSA_COD_IMPORTACAO: TStringField;
    qBoletos: TADOQuery;
    qBoletosBOLETO_ID: TAutoIncField;
    qBoletosCAMINHO_ARQUIVO: TStringField;
    qBoletosDATA_VENCIMENTO: TDateTimeField;
    QBoletosFATURA_ID1: TIntegerField;
    qFaturaFechamento: TADOQuery;
    QFaturaFechamentoFATURA_ID3: TIntegerField;
    qFaturaFechamentoFECHAMENTO: TDateTimeField;
    dsFaturaFechamento: TDataSource;
    qFaturaVencimento: TADOQuery;
    qFaturaVencimentoDATA_VENCIMENTO: TDateTimeField;
    dsFaturaVencimento: TDataSource;
    pnl1: TPanel;
    lbl3: TLabel;
    lbl4: TLabel;
    btnButAjuda: TSpeedButton;
    lbl5: TLabel;
    bvl1: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    btn1: TButton;
    redt1: TRichEdit;
    dblkpEmpresas: TJvDBLookupCombo;
    FilenameEdit1: TJvFilenameEdit;
    DBDateEditDATA_VENCIMENTO: TDBDateEdit;
    dblkpFechamento: TJvDBLookupCombo;
    dlgAbrePDF: TOpenDialog;
    qBoletosDATA_ALTERACAO: TDateTimeField;
    qBoletosOPERADOR: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnButAjudaClick(Sender: TObject);
    procedure dblkpEmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure btn1Click(Sender: TObject);
    procedure dblkpEmpresasChange(Sender: TObject);
    procedure dblkpFechamentoChange(Sender: TObject);
    procedure LimparTela();
  public
    { Public declarations }
  end;

var
  FAnexarBoleto: TFAnexarBoleto;

implementation

uses DM, UChangeLog, UMenu, cartao_util, UValidacao, Math;

{$R *.dfm}

procedure TFAnexarBoleto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end;
end;

procedure TFAnexarBoleto.FormCreate(Sender: TObject);
begin
  QEmpresas.Open;
  qBoletos.Open;
  FilenameEdit1.Text := '"\\tsclient\Z\"';
end;

procedure TFAnexarBoleto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresas.Close;
  qBoletos.Close;
end;

procedure TFAnexarBoleto.btnButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Carregamento de Boletos';
  FChangeLog.RichEdit1.Lines := redt1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFAnexarBoleto.dblkpEmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
    Perform(WM_NEXTDLGCTL,0,0);
  end;
end;

procedure TFAnexarBoleto.btn1Click(Sender: TObject);
var arquivoPDF : String;
    caminhoAno, caminhoMes, caminhoCompleto, caminhoRelativo : string;
    dia, mes, ano : Word;
begin
  screen.Cursor  := crHourGlass;
  if dblkpEmpresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa.');
    screen.Cursor := crDefault;
    Exit;
  end;
  if dblkpFechamento.Text = '' then
  begin
    ShowMessage('Selecione a Data do Fechamento.');
    screen.Cursor := crDefault;
    Exit;
  end;

  if ExtractFileExt(FilenameEdit1.FileName) <> '.pdf' then
  begin
    ShowMessage('Selecione o arquivo a ser carregado.');
    screen.Cursor := crDefault;
    Exit;
  end;

  if MsgSimNao('Deseja realmente anexar o boleto selecionado' + sLineBreak + '� empresa ' + dblkpEmpresas.Text + '?', False) then
  begin
    qBoletos.Close;
    qBoletos.Parameters.ParamByName('FATURA_ID').Value := dblkpFechamento.KeyValue;
    qBoletos.Open;
    qFaturaVencimento.Edit;

    if qBoletos.IsEmpty then begin
      qBoletos.Insert;
      QBoletosFATURA_ID1.AsInteger :=  dblkpFechamento.KeyValue;
    end else
      qBoletos.Edit;
    arquivoPDF := FilenameEdit1.FileName;
    DecodeDate(Now, ano, mes, dia);
    caminhoAno := 'C:\Dados\Web\webEmpresas\Faturas\' + IntToStr(ano);
    if Length(IntToStr(mes)) < 2 then
      caminhoMes := '0' + IntToStr(mes)
    else
      caminhoMes := IntToStr(mes);

    if not DirectoryExists(caminhoAno) then
      CreateDir(caminhoAno);

    if not DirectoryExists(caminhoAno + '\' + caminhoMes) then
      CreateDir(caminhoAno + '\' + caminhoMes);

    caminhoCompleto := caminhoAno + '\' + caminhoMes + '\' + IntToStr(dblkpFechamento.KeyValue) + '.pdf';
    caminhoRelativo := 'FATURAS\' + IntToStr(ano) + '\' + caminhoMes + '\' + IntToStr(dblkpFechamento.KeyValue) + '.pdf';

    if CopyFile(PChar(arquivoPDF), PChar(caminhoCompleto), False) then begin
      qBoletosCAMINHO_ARQUIVO.Text := caminhoRelativo;
      qBoletosDATA_VENCIMENTO.AsDateTime := DBDateEditDATA_VENCIMENTO.Date;
      qBoletosOPERADOR.AsString := Operador.Nome;
      qBoletosDATA_ALTERACAO.AsDateTime := Now;
      qBoletos.Post;
      qFaturaVencimentoDATA_VENCIMENTO.AsDateTime := DBDateEditDATA_VENCIMENTO.Date;
      qFaturaVencimento.Post;

      ShowMessage('Boleto carregado com sucesso!');
    end else
      ShowMessage('O Boleto n�o foi carregado. Tente novamente!');
  end
  else
    ShowMessage('O Boleto n�o foi carregado!');
  LimparTela();
end;

procedure TFAnexarBoleto.dblkpEmpresasChange(Sender: TObject);
begin
  qFaturaFechamento.Close;
  qFaturaFechamento.Parameters.ParamByName('id').Value := dblkpEmpresas.KeyValue;
  qFaturaFechamento.Open;
end;

procedure TFAnexarBoleto.dblkpFechamentoChange(Sender: TObject);
begin
  qFaturaVencimento.Close;
  qFaturaVencimento.Parameters.ParamByName('FATURA_ID').Value := dblkpFechamento.KeyValue;
  qFaturaVencimento.Open;
end;

procedure TFAnexarBoleto.LimparTela();
begin
  FilenameEdit1.Clear;
  FilenameEdit1.Text := '"\\tsclient\Z\"';
  dblkpEmpresas.ResetField;
  dblkpFechamento.ClearValue;
  DBDateEditDATA_VENCIMENTO.Clear;
end;

end.
