unit URelExtratoAlim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DBCtrls, DB, ADODB,
  frxClass, frxGradient, frxExportPDF, frxDBSet, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvMemoryDataset;

type
  TFrmRelExtratoAlim = class(TF1)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbLkpEmpresas: TDBLookupComboBox;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    dsEmpresas: TDataSource;
    dbConveniados: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    frxGradientObject1: TfrxGradientObject;
    frxReport1: TfrxReport;
    sd: TSaveDialog;
    qConv: TADOQuery;
    qConvCONV_ID: TIntegerField;
    qConvTITULAR: TStringField;
    qConvCODCARTIMP: TStringField;
    qConvNOME: TStringField;
    qConvDATA_RENOVACAO: TWideStringField;
    qConvRENOVACAO_VALOR: TBCDField;
    qConvABONO_VALOR: TBCDField;
    qConvSALDO_ACUMULADO_ANT: TBCDField;
    qConvCONSUMO: TBCDField;
    qConvSALDO_ACUMULADO_PROX: TBCDField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    JvDBGrid1: TJvDBGrid;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    qBusca: TADOQuery;
    qBuscaCONV_ID: TIntegerField;
    qBuscaTITULAR: TStringField;
    dsBusca: TDataSource;
    BitBtn1: TBitBtn;
    MBusca: TJvMemoryData;
    MBuscaconv_id: TIntegerField;
    MBuscatitular: TStringField;
    MBuscamarcado: TBooleanField;
    procedure FormCreate(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
  private
    conv_sel : String;

    { Private declarations }
  public
     procedure Conveniados_Sel;
    { Public declarations }
  end;

var
  FrmRelExtratoAlim: TFrmRelExtratoAlim;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFrmRelExtratoAlim.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  btnVisualizar.Enabled := false;
  btnGerarPDF.Enabled := false;
  dbLkpEmpresas.SetFocus;
end;

procedure TFrmRelExtratoAlim.btnVisualizarClick(Sender: TObject);
begin
  inherited;
  if DMConexao.ContaMarcados(MBusca) = 0 then begin
    msgInf('Nenhum conveniado selecionado');
    JvDBGrid1.SetFocus;
    Exit;
  end;

  qConv.Close;
  qConv.SQL.Clear;
  qConv.SQL.Add(' SELECT CONV.CONV_ID, CONV.TITULAR, CART.CODCARTIMP, EMP.NOME,');
  qConv.SQL.Add(' CONVERT (VARCHAR, AH.DATA_RENOVACAO,103) AS DATA_RENOVACAO,');
  qConv.SQL.Add(' COALESCE(AH.RENOVACAO_VALOR,0) AS RENOVACAO_VALOR, COALESCE(AH.ABONO_VALOR,0) AS ABONO_VALOR,');
  qConv.SQL.Add(' COALESCE(AH.SALDO_ACUMULADO_ANT,0) AS SALDO_ACUMULADO_ANT, COALESCE(SUM(CC.DEBITO - CC.CREDITO),0.00) CONSUMO,');
  qConv.SQL.Add(' (COALESCE(AH.RENOVACAO_VALOR,0) + COALESCE(AH.ABONO_VALOR,0) + COALESCE(AH.SALDO_ACUMULADO_ANT,0)) - COALESCE(SUM(CC.DEBITO - CC.CREDITO),0.00) AS SALDO_ACUMULADO_PROX');
  qConv.SQL.Add(' FROM CONVENIADOS CONV');
  qConv.SQL.Add(' INNER JOIN CARTOES CART ON CART.CONV_ID = CONV.CONV_ID');
  qConv.SQL.Add(' INNER JOIN EMPRESAS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID');
  qConv.SQL.Add(' INNER JOIN ALIMENTACAO_HISTORICO AH ON AH.CONV_ID = CONV.CONV_ID');
  qConv.SQL.Add(' LEFT JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  qConv.SQL.Add(' AND CC.DATA BETWEEN CASE WHEN');
  qConv.SQL.Add(' (SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA WHERE DIA_FECHA.DATA_FECHA < CONVERT(DATE,AH.DATA_FECHAMENTO)');
  qConv.SQL.Add(' AND DIA_FECHA.EMPRES_ID = ' +qEmpresasEMPRES_ID.AsString + ' order by 1 desc) > CONVERT(DATE,AH.DATA_RENOVACAO) then ');
  qConv.SQL.Add(' (SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA WHERE DIA_FECHA.DATA_FECHA < CONVERT(DATE,AH.DATA_FECHAMENTO)');
  qConv.SQL.Add(' AND DIA_FECHA.EMPRES_ID = ' +qEmpresasEMPRES_ID.AsString + ' order by 1 desc) else AH.DATA_RENOVACAO end ');
  qConv.SQL.Add(' AND COALESCE((SELECT TOP 1 DATEADD(DD,-1,DATA_RENOVACAO) FROM ALIMENTACAO_HISTORICO WHERE');
  qConv.SQL.Add(' CONV_ID = CONV.CONV_ID');
  qConv.SQL.Add(' AND DATA_RENOVACAO > AH.DATA_RENOVACAO ');
  qConv.SQL.Add(' ORDER BY DATA_RENOVACAO ASC), CONVERT(DATE,CURRENT_TIMESTAMP))');
  qConv.SQL.Add(' WHERE CART.APAGADO <> ''S''');
  qConv.SQL.Add(' AND coalesce(CART.titular,''S'') = ''S''');
  qConv.SQL.Add(' and emp.empres_id = '+qEmpresasEMPRES_ID.AsString);
  qConv.SQL.Add(' and conv.conv_id in ('+conv_sel +')');
  qConv.SQL.Add(' GROUP BY CONV.TITULAR, CART.CODCARTIMP, EMP.NOME, AH.DATA_RENOVACAO, AH.RENOVACAO_VALOR, AH.ABONO_VALOR,AH.SALDO_ACUMULADO_ANT,');
  qConv.SQL.Add(' CONV.CONV_ID, AH.SALDO_ACUMULADO_PROX');
  qConv.SQL.Add(' ORDER BY EMP.NOME, CONV.TITULAR, AH.DATA_RENOVACAO');
  qConv.SQL.Text;
  qConv.Open;

  if qConv.IsEmpty then
  begin
    MsgInf('N�o foi encontrado nenhum lan�amento.');
    dbLkpEmpresas.SetFocus;
    abort;
  end;
    frxReport1.ShowReport;
end;



procedure TFrmRelExtratoAlim.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if(dbLkpEmpresas.KeyValue = Null) then begin
    MsgInf('Selecione uma empresa.');
    dbLkpEmpresas.SetFocus;
  end else begin
    qBusca.Close;
    qBusca.Parameters.ParamByName('empres_id').Value := qEmpresasEMPRES_ID.AsInteger;
    qBusca.Open;

    if qBusca.IsEmpty then
    begin
      MsgInf('N�o foi encontrado nenhum lan�amento para essa empresa.');
      dbLkpEmpresas.SetFocus;
      btnVisualizar.Enabled := false;
      btnGerarPDF.Enabled := false;
      abort;
    end;

    btnVisualizar.Enabled := true;
    btnGerarPDF.Enabled := true;

    qBusca.First;
    MBusca.Open;
    MBusca.EmptyTable;
    MBusca.DisableControls;
    while not QBusca.Eof do
    begin
      MBusca.Append;
      MBuscaCONV_ID.AsInteger       := QBuscaCONV_ID.AsInteger;
      MBuscaTITULAR.AsString        := QBuscaTITULAR.AsString;
      MBuscaMARCADO.AsBoolean       := False;
      QBusca.Next;
    end;
    MBusca.First;
    MBusca.EnableControls;
    conv_sel := EmptyStr;
end;

end;

procedure TFrmRelExtratoAlim.Button1Click(Sender: TObject);
begin
  inherited;
   if MBusca.IsEmpty then Exit;
    MBusca.Edit;
    MBuscaMarcado.AsBoolean := not MBuscaMarcado.AsBoolean;
    MBusca.Post;
    Conveniados_Sel;
end;

procedure TFrmRelExtratoAlim.Conveniados_Sel;
var marca : TBookmark;
begin
  conv_sel := EmptyStr;
  MBusca.DisableControls;
  marca := MBusca.GetBookmark;
  MBusca.First;
  while not MBusca.Eof do begin
    if MBuscaMARCADO.AsBoolean  = true then conv_sel := conv_sel+','+MBuscaCONV_ID.AsString;
    MBusca.Next;
  end;
  conv_sel := Copy(conv_sel,2,Length(conv_sel));
  MBusca.GotoBookmark(marca);
  MBusca.FreeBookmark(marca);
  MBusca.EnableControls;
end;


procedure TFrmRelExtratoAlim.Button2Click(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MBusca.IsEmpty then Exit;
  MBusca.DisableControls;
  marca := MBusca.GetBookmark;
  MBusca.First;
  while not MBusca.eof do begin
    MBusca.Edit;
    MBuscaMarcado.AsBoolean := true;
    MBusca.Post;
    MBusca.Next;
  end;
  MBusca.GotoBookmark(marca);
  MBusca.FreeBookmark(marca);
  MBusca.EnableControls;
  Conveniados_Sel;
end;

procedure TFrmRelExtratoAlim.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  Button1.Click;
end;

procedure TFrmRelExtratoAlim.Button3Click(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MBusca.IsEmpty then Exit;
  MBusca.DisableControls;
  marca := MBusca.GetBookmark;
  MBusca.First;
  while not MBusca.eof do begin
    MBusca.Edit;
    MBuscaMarcado.AsBoolean := false;
    MBusca.Post;
    MBusca.Next;
  end;
  MBusca.GotoBookmark(marca);
  MBusca.FreeBookmark(marca);
  MBusca.EnableControls;
  Conveniados_Sel;
end;

procedure TFrmRelExtratoAlim.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_F5 then BitBtn1.Click;
  if key = vk_F10 then  Button1.Click;
  if key = vk_F11 then  Button2.Click;
  if key = vk_F12 then  Button3.Click;
end;

procedure TFrmRelExtratoAlim.btnGerarPDFClick(Sender: TObject);
begin
  inherited;
  begin
    sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      btnVisualizar.Click;
      frxReport1.Export(frxPDFExport1);
    end;
  end;
end;

procedure TFrmRelExtratoAlim.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QBusca.Sort) > 0 then begin
  if Pos(' DESC',QBusca.Sort) > 0 then QBusca.Sort := Field.FieldName
                          else QBusca.Sort := Field.FieldName+' DESC';
  end
  else QBusca.Sort := Field.FieldName;
  except
  end;

   qBusca.First;
   MBusca.Open;
   MBusca.EmptyTable;
   MBusca.DisableControls;
   while not QBusca.Eof do
   begin
      MBusca.Append;
      MBuscaCONV_ID.AsInteger       := QBuscaCONV_ID.AsInteger;
      MBuscaTITULAR.AsString        := QBuscaTITULAR.AsString;
      MBuscaMARCADO.AsBoolean       := False;
      QBusca.Next;
   end;
   MBusca.First;
   MBusca.EnableControls;
   conv_sel := EmptyStr;
end;

end.
