unit USQLMount;

interface

uses DB, DBTables, Classes, StrUtils, SysUtils, cartao_util;

type
   TSqlMountType = (smtUpdate,smtInsert,smtReplace);

   TSqlMount = class(TObject)
   private
      table  : string;
      where  : string;
      smtype : TSqlMountType;
      Params : TParams;
      Fields, FixedParams : TStringList;
      function CreateUpdateSQL: TStrings;
      function CreateInsertSQL: TStrings;
    function GetFieldValue(FieldName: String): String;
   public
      constructor Create(SMType:TSqlMountType;TableName:String);
      destructor Destroy;override;
      procedure AddField(FieldName:String;Value:Variant;FieldType:TFieldType);
      procedure AddIntegerField(FieldName:String;Value:Integer); overload;
      procedure AddInt64Field(FieldName:String;Value:Largeint); overload;
      procedure AddStringField(FieldName:String;Value:String);
      procedure AddCurrencyField(FieldName:String;Value:Currency);
      procedure AddDateTimeField(FieldName:String;Value:TDateTime);
      procedure AddDateField(FieldName:String;Value:TDateTime);
      procedure AddFixedValueField(FieldName, Value: String);
      procedure AddBlobField(FieldName: String; Value: TMemoryStream);
      procedure RemoveField(FieldName: String);
      function GetSqlString:TStrings;
      function GetParams:TParams;
      function GetParamsWithValues:String;
      procedure SetWhere(WhereSql:String);
      function GetWhere:String;
      procedure SetTableName(TableName:String);
      procedure SetSqlMountType(SMType:TSqlMountType);
      function GetTableName:String;
      function GetSqlMountType:TSqlMountType;
      function FieldCount: Integer;
      procedure ClearFields; 
   end;

implementation

{ TSqlMount }

procedure TSqlMount.AddField(FieldName: String; Value: Variant;
  FieldType: TFieldType);
var P : TParam;
begin
   P := TParam.Create(Params,ptInput);
   P.DataType := FieldType;
   P.Value    := Value;
   P.Name     := FieldName;

   Fields.Add(FieldName);
   //AddFixedValueField(FieldName,string(Value));
end;

procedure TSqlMount.AddBlobField(FieldName: String; Value: TMemoryStream);
var P : TParam;
begin
   P := TParam.Create(Params,ptInput);
   P.DataType := ftBlob;
   P.LoadFromStream(Value,ftBlob);
   P.Name     := FieldName;
   Fields.Add(FieldName);
end;

procedure TSqlMount.AddFixedValueField(FieldName: String; Value: String);
var P : TParam;
begin
   Fields.Add(FieldName);
   FixedParams.Add(FieldName+'='+Value);
end;

constructor TSqlMount.Create(SMType: TSqlMountType; TableName: String);
begin
   Self.smtype := SMType;
   table       := TableName;
   Params      := TParams.Create;

   FixedParams := TStringList.Create;
   Fields      := TStringList.Create;
end;

function TSqlMount.GetSqlString:TStrings;
begin
   if Fields.Count = 0 then
      raise Exception.Create('Nao ha campos para gerar a instrucao sql.');
//   Result := TStringList.Create;
   if smtype in [smtInsert,smtReplace] then
      Result := CreateInsertSQL
   else if smtype = smtUpdate then
      Result := CreateUpdateSQL
   else
      raise Exception.Create('SQLMountType inv�lido.');
end;

function TSqlMount.GetFieldValue(FieldName:String):String;
var s : string;
begin
   if Params.ParamByName(FieldName).AsString <> EmptyStr then begin
    if Params.ParamByName(FieldName).DataType in [ftFloat,ftInteger] then
      s := StringReplace(Params.ParamByName(FieldName).AsString,',','.',[rfReplaceAll])
    else if Params.ParamByName(FieldName).DataType in [ftDate] then
      s := QuotedStr(FormatDateTime('dd.mm.yyyy',Params.ParamByName(FieldName).AsDate))
    else if Params.ParamByName(FieldName).DataType in [ftDateTime] then
      s := QuotedStr(FormatDateTime('dd.mm.yyyy hh:nn:ss',Params.ParamByName(FieldName).AsDateTime))
    else if Params.ParamByName(FieldName).DataType in [ftString] then
      s := QuotedStr(Params.ParamByName(FieldName).AsString);
   end else
      s := ':'+FieldName;
   Result := s;
end;

function TSqlMount.CreateUpdateSQL:TStrings;
var i : Integer;
begin
   Result := TStringList.Create;
   Result.Add(' update '+table+' set ');
   for i := 0 to pred(Fields.Count) do begin
       if i = 0 then
          Result.Add(Fields[i]+' = '+GetFieldValue(Fields[i]))
       else
          Result.Add(','+Fields[i]+' = '+GetFieldValue(Fields[i]));
   end;
   Result.Add(where);
end;

function TSqlMount.CreateInsertSQL: TStrings;
var i : Integer;  F : String;
begin
   Result := TStringList.Create;
   If smtype = smtInsert then
      Result.Add('insert into '+table+' ( ')
   else
      Result.Add('replace into '+table+' ( ');

   for i := 0 to pred(Fields.Count) do begin
       if i = 0 then
          Result.Text := Result.Text+Fields[i]
       else
          Result.Text := Result.Text+','+Fields[i];
   end;
   Result.Add(' ) ');
   Result.Add(' values ( ');
   for i := 0 to pred(Fields.Count) do begin
       if i = 0 then
          Result.Text := Result.Text+GetFieldValue(Fields[i])
       else
          Result.Text := Result.Text+','+GetFieldValue(Fields[i]);
   end;
   Result.Add(' ) ');
end;

function TSqlMount.GetParams: TParams;
begin
   Result := Params;
end;

function TSqlMount.GetParamsWithValues: String;
var I : Integer;
begin
  Result := '';
  for I:= 0 to Params.Count-1 do begin
    if I > 0 then
      Result := Result + ',';
    if Params[I].DataType in [ftInteger,ftFloat] then
      Result := Result + Params[I].AsString + #13
    else if Params[I].DataType in [ftDateTime] then
      Result := Result + QuotedStr(FormatDateTime('dd.mm.yyyy hh:nn:ss',Params[I].AsDateTime)) + #13
    else if Params[I].DataType in [ftString] then
      Result := Result + QuotedStr(Params[I].AsString) + #13;
  end;
end;

procedure TSqlMount.ClearFields;
begin
   Params.Clear;
   FixedParams.Clear;
   Fields.Clear;
end;

procedure TSqlMount.SetSqlMountType(SMType: TSqlMountType);
begin
   Self.smtype := SMType;
end;

procedure TSqlMount.SetTableName(TableName: String);
begin
   Self.table := TableName;
end;

function TSqlMount.GetSqlMountType: TSqlMountType;
begin
   Result := Self.smtype;
end;

function TSqlMount.GetTableName: String;
begin
   Result := Self.table;
end;

function TSqlMount.GetWhere: String;
begin
   if smtype = smtInsert then
      raise Exception.Create('SqlWhere nao permitido para insert.');
   Result := Self.where;
end;

procedure TSqlMount.SetWhere(WhereSql: String);
begin
   if smtype = smtInsert then
      raise Exception.Create('SqlWhere nao permitido para insert.');
   where := WhereSql;
end;

destructor TSqlMount.Destroy;
begin
   Params.Free;
   FixedParams.Free;
   Fields.Free;
   inherited Destroy;
end;

procedure TSqlMount.AddCurrencyField(FieldName:String; Value: Currency);
begin
   AddField(FieldName,Value,ftCurrency);
end;

procedure TSqlMount.AddDateField(FieldName: String; Value: TDateTime);
begin
   AddField(FieldName,Value,ftDate);
end;

procedure TSqlMount.AddDateTimeField(FieldName: String; Value: TDateTime);
begin
   AddField(FieldName,Value,ftDateTime);
end;

procedure TSqlMount.AddIntegerField(FieldName: String; Value: Integer);
begin
   AddField(FieldName,Value,ftInteger);
end;

procedure TSqlMount.AddStringField(FieldName, Value: String);
begin
   AddField(FieldName,Value,ftString);
end;

procedure TSqlMount.RemoveField(FieldName: String);
begin
   if Fields.IndexOf(FieldName) >= 0 then
   begin
      Fields.Delete(Fields.IndexOf(FieldName));
      if Params.FindParam(FieldName) <> nil then
         Params.RemoveParam(Params.FindParam(FieldName))
      else if FixedParams.Values[FieldName] <> EmptyStr then
         FixedParams.Delete(FixedParams.IndexOfName(FieldName));
   end;
end;

procedure TSqlMount.AddInt64Field(FieldName: String; Value: Largeint);
begin
  AddField(FieldName,Value,ftLargeint);
end;

function TSqlMount.FieldCount: Integer;
begin
  Result := Fields.Count;
end;

end.
