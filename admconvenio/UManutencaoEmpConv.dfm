inherited frmManutencaoEmpConv: TfrmManutencaoEmpConv
  Left = 499
  Top = 256
  Anchors = [akLeft]
  Caption = 'Manuten'#231#227'o de Empresas - Conveniados'
  ClientHeight = 126
  ClientWidth = 383
  WindowState = wsNormal
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 383
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 398
    Height = 205
    TabOrder = 1
    DesignSize = (
      398
      205)
    object btnImportarLimiteConv: TBitBtn
      Left = 107
      Top = 59
      Width = 158
      Height = 34
      Hint = 'Importar'
      Anchors = [akLeft]
      Caption = 'Importa'#231#227'o de Planilha'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnImportarLimiteConvClick
      NumGlyphs = 2
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 236
    Top = 40
  end
  object tExcel: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 792
    Top = 440
  end
end
