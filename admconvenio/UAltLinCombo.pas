unit UAltLinCombo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, {JvLookup,}
  Mask, ToolEdit, CurrEdit, JvExControls, JvDBLookup, JvExStdCtrls, JvEdit,
  JvValidateEdit, ADODB;

type
  TFAltLinCombo = class(TForm)
    //edValorReal: TCurrencyEdit;
    Combo: TJvDBLookupCombo;
    lbCombo: TLabel;
    lbValor: TLabel;
    BtnOk: TBitBtn;
    BtnCancel: TBitBtn;
    DSQ: TDataSource;
    edValorReal: TJvValidateEdit;
    Q: TADOQuery;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAltLinCombo: TFAltLinCombo;

implementation

uses DM;

{$R *.dfm}

procedure TFAltLinCombo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [vk_return,vk_escape] then
  begin
    if (ActiveControl is TJvDBLookupCombo) then
    begin
      if TJvDBLookupCombo(ActiveControl).ListVisible then
      begin
        if key = vk_return then
        begin
          TJvDBLookupCombo(ActiveControl).CloseUp(true);
        end
        else
        begin
          TJvDBLookupCombo(ActiveControl).CloseUp(false);
        end;
        exit;
      end;
    end
    else if (ActiveControl is TCustomComboBox) then
    begin
      if TCustomComboBox(ActiveControl).DroppedDown then
      begin
        TCustomComboBox(ActiveControl).DroppedDown := False;
        exit;
      end;
    end;
    if key = vk_return then
    begin
      SelectNext(ActiveControl,true,true);
    end;
  end;

  if key = vk_escape then
  begin
    BtnCancel.Click;
  end;
end;

end.
