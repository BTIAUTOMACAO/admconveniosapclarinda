unit uProgramaDesconto;

interface

uses SysUtils, Classes;

type
  TProgramaDesconto = class(TObject)
  private
    FProg_Id: Integer;
    FNome : String;
    FDt_Inicio : TDateTime;
    FDt_Fim : TDateTime;
    FVerfica_Valor : String;
    FEmpres_Id : Integer;
    FCred_Id : Integer;
    FObriga_Desconto : Boolean;
  public
    property Prog_Id         : Integer read FProg_Id write FProg_Id;
    property Nome            : String read FNome write FNome;
    property Dt_Inicio       : TDateTime read FDt_Inicio write FDt_Inicio;
    property Dt_Fim          : TDateTime read FDt_Fim write FDt_Fim;
    property Verfica_Valor   : String read FVerfica_Valor write FVerfica_Valor;
    property Empres_Id       : Integer read FEmpres_Id write FEmpres_Id;
    property Cred_Id         : Integer read FCred_Id write FCred_Id ;
    property Obriga_Desconto : Boolean read FObriga_Desconto write FObriga_Desconto; 
    constructor Create;
  end;

type
  TProgramasDesconto = array of TProgramaDesconto;
implementation

{ TProgramaDesconto }

{ TProgramaDesconto }

constructor TProgramaDesconto.Create;
begin

end;

end.
