unit USaldoMes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, ToolEdit, JvExMask, JvToolEdit;

type
  TFSaldoMes = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    ChSaldoMes: TCheckBox;
    ChSaldoTotal: TCheckBox;
    RBFecha: TRadioButton;
    RBPeriodo: TRadioButton;
    Label1: TLabel;
    //datainiST: TDateEdit;
    //datafinST: TDateEdit;
    //datainiSM: TDateEdit;
    //datafinSM: TDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Butatua: TButton;
    ButCancel: TButton;
    datainiSM: TJvDateEdit;
    datafinSM: TJvDateEdit;
    datafinST: TJvDateEdit;
    datainiST: TJvDateEdit;
    procedure RBFechaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSaldoMes: TFSaldoMes;

implementation

{$R *.dfm}

procedure TFSaldoMes.RBFechaClick(Sender: TObject);
begin
RBPeriodo.Checked := not RBFecha.Checked;
RBFecha.Checked   := not RBPeriodo.Checked;
if RBFecha.Checked then begin
   Label3.Caption := 'Data de fechamento';
   datafinSM.Hide;
end
else begin
   Label3.Caption := 'Data Inicial                              Data Final';
   datafinSM.Show;
end;
end;

procedure TFSaldoMes.FormCreate(Sender: TObject);
begin
datainiSM.Date := Date;
datainiST.Date := Date;
datafinSM.Date := Date;
datafinST.Date := Date;
end;

procedure TFSaldoMes.FormKeyPress(Sender: TObject; var Key: Char);
begin
if key = #13 then begin
   Key := #0;
   Perform(WM_NEXTDLGCTL,0,0);
end;
end;

end.
