object FRelBemEstar: TFRelBemEstar
  Left = 706
  Top = 268
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Relat'#243'rio Bem Estar - Mensal'
  ClientHeight = 125
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 300
    Height = 125
    Align = alClient
    Locked = True
    TabOrder = 0
    object lbl1: TLabel
      Left = 24
      Top = 16
      Width = 66
      Height = 13
      Caption = 'Data de Inicio'
    end
    object lbl2: TLabel
      Left = 176
      Top = 16
      Width = 81
      Height = 13
      Caption = 'Data de Termiino'
    end
    object Button1: TButton
      Left = 178
      Top = 72
      Width = 91
      Height = 41
      Caption = 'Gerar Relat'#243'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = Button1Click
    end
    object DataInicio: TJvDateEdit
      Left = 24
      Top = 35
      Width = 100
      Height = 21
      ShowNullDate = False
      TabOrder = 0
    end
    object DataFim: TJvDateEdit
      Left = 178
      Top = 35
      Width = 100
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object btn1: TButton
      Left = 8
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 3
      OnClick = btn1Click
    end
  end
  object QBusca: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT DISTINCT CC.CONV_ID,CONV.TITULAR,e.EMPRES_ID, e.NOME NOME' +
        '_EMPRESA,DEBITO,DATA_VENC_EMP DATA_VENC_FATURA '
      'FROM CONTACORRENTE CC'
      'INNER JOIN EMPRESAS E ON CC.EMPRES_ID = E.EMPRES_ID'#10
      'INNER JOIN CONVENIADOS CONV ON CONV.CONV_ID = CC.CONV_ID'
      'WHERE CC.CRED_ID = 2939')
    Left = 136
    Top = 8
    object intgrfldADOQuery1CONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object strngfldADOQuery1TITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object intgrfldADOQuery1EMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object strngfldADOQuery1NOME_EMPRESA: TStringField
      FieldName = 'NOME_EMPRESA'
      Size = 60
    end
    object QBuscaDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object QBuscaDATA_VENC_FATURA: TDateTimeField
      FieldName = 'DATA_VENC_FATURA'
    end
  end
end
