object FNewGeraLista: TFNewGeraLista
  Left = 196
  Top = 176
  ActiveControl = CheckList
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Gerador de Listagem'
  ClientHeight = 386
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 686
    Height = 386
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Configura'#231#227'o da Listagem'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 678
        Height = 358
        Align = alClient
        BorderStyle = bsSingle
        TabOrder = 0
        object Label1: TLabel
          Left = 196
          Top = 264
          Width = 106
          Height = 16
          Caption = 'T'#237'tulo da Listagem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Bevel1: TBevel
          Left = 510
          Top = 177
          Width = 150
          Height = 2
        end
        object Label10: TLabel
          Left = 615
          Top = 312
          Width = 53
          Height = 13
          Caption = 'F1 - Help'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 479
          Top = 264
          Width = 176
          Height = 16
          Caption = 'Estilo da Fonte:       Tamanho:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 311
          Height = 257
          BorderStyle = bsSingle
          TabOrder = 0
          object lblRodape: TLabel
            Left = 193
            Top = 201
            Width = 87
            Height = 13
            Caption = 'Inserir no Rodap'#233':'
            Visible = False
          end
          object Label11: TLabel
            Left = 192
            Top = 168
            Width = 3
            Height = 13
          end
          object CheckList: TCheckListBox
            Left = 1
            Top = 1
            Width = 184
            Height = 251
            Hint = 
              'Pressione Control + Seta para baixo ou para cima para mover os i' +
              'tens.'#13#10'Use seta para baixo ou para cima para mudar de item.'
            OnClickCheck = CheckListClickCheck
            Align = alLeft
            ItemHeight = 13
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = CheckListClick
            OnKeyDown = CheckListKeyDown
          end
          object Button1: TButton
            Left = 193
            Top = 107
            Width = 100
            Height = 25
            Caption = '&Marca Todos'
            TabOrder = 1
            OnClick = Button1Click
          end
          object Button2: TButton
            Left = 193
            Top = 139
            Width = 100
            Height = 25
            Caption = '&Desmarca Todos'
            TabOrder = 2
            OnClick = Button2Click
          end
          object ButAbaixo: TButton
            Left = 193
            Top = 40
            Width = 100
            Height = 25
            Caption = 'A&baixo'
            TabOrder = 3
            OnClick = ButAbaixoClick
          end
          object ButAcima: TButton
            Left = 193
            Top = 8
            Width = 100
            Height = 25
            Caption = 'A&cima'
            TabOrder = 4
            OnClick = ButAcimaClick
          end
          object cbRodape: TComboBox
            Left = 192
            Top = 216
            Width = 102
            Height = 21
            Style = csDropDownList
            CharCase = ecUpperCase
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 5
            Text = '--NADA--'
            Visible = False
            OnChange = cbRodapeChange
            Items.Strings = (
              '--NADA--'
              'SOMA'
              'M'#201'DIA'
              'M'#193'XIMO'
              'M'#205'NIMO')
          end
        end
        object EdTitulo: TJvEdit
          Left = 6
          Top = 281
          Width = 459
          Height = 20
          BiDiMode = bdLeftToRight
          ParentBiDiMode = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Orator10 BT'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 1
          OnClick = EdTituloClick
          OnEnter = EdTituloEnter
          OnKeyDown = EdTituloKeyDown
        end
        object ChTotlinhas: TCheckBox
          Left = 7
          Top = 312
          Width = 166
          Height = 17
          Caption = 'Mostrar total de Registros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object Barra: TStatusBar
          Left = 1
          Top = 334
          Width = 672
          Height = 19
          Panels = <
            item
              Width = 60
            end
            item
              Width = 200
            end
            item
              Width = 50
            end>
        end
        object ButGeraLista: TBitBtn
          Left = 510
          Top = 192
          Width = 150
          Height = 25
          Hint = 'Gera a listagem para o visualizador.'
          Caption = '&Gerar Listagem'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = ButGeraListaClick
        end
        object Panel3: TPanel
          Left = 310
          Top = 0
          Width = 185
          Height = 257
          BorderStyle = bsSingle
          TabOrder = 5
          object Label4: TLabel
            Left = 1
            Top = 1
            Width = 179
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Detalhes do Campo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbname: TLabel
            Left = 1
            Top = 156
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbdescri: TLabel
            Left = 1
            Top = 27
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 1
            Top = 57
            Width = 179
            Height = 13
            Align = alTop
            BiDiMode = bdLeftToRight
            Caption = 'Tipo do Campo'
            ParentBiDiMode = False
          end
          object Label8: TLabel
            Left = 1
            Top = 143
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Nome Interno do Campo'
          end
          object Label9: TLabel
            Left = 1
            Top = 14
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Descri'#231#227'o do Campo'
          end
          object lbtipo: TLabel
            Left = 1
            Top = 70
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 1
            Top = 100
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Tamanho do Campo'
          end
          object lbtamanho: TLabel
            Left = 1
            Top = 113
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object RichEdit1: TRichEdit
            Left = -4
            Top = 253
            Width = 185
            Height = 89
            Lines.Strings = (
              'Configura'#231#227'o da Listagem:'
              
                '    Para configurar uma listagem basta selecionar os campos dand' +
                'o um clique com o mouse ou pressionando a barra de espa'#231'os sobre' +
                ' o campo desejado na janelinha de campos, ap'#243's selecionar os cam' +
                'pos desejados para sair na listagem, coloque-os na ordem que des' +
                'ejar marcando um item e clicando no bot'#227'o '#8220'acima'#8221' ou '#8220'abaixo'#8221' ta' +
                'mb'#233'm '#233' poss'#237'vel alterar a ordem dos campos pressionando a tela c' +
                'ontrol + seta para baixo ou para cima. No campo '#8220'T'#237'tulo da lista' +
                'gem'#8221' '#233' poss'#237'vel informar um t'#237'tulo para a listagem este contendo' +
                ' no m'#225'ximo duas linha, marcando a op'#231#227'o '#8220'T'#237'tulo Centralizado'#8221', a' +
                's duas linhas do t'#237'tulo sempre ficar'#227'o centralizadas na listagem' +
                ', marcando a op'#231#227'o '#8220'Mostrar Data'#8221' ser'#225' adicionada a data atual n' +
                'o formato dd/mm/aaaa ao lado direito da primeira linha do titulo' +
                '. No campo '#8220'Cabe'#231'alho das Colunas'#8221' '#233' poss'#237'vel alterar os nomes d' +
                'os campos a serem listados; exemplo: campo chapa para algumas em' +
                'presas pode ser interessante o t'#237'tulo como matricula. Tamb'#233'm '#233' p' +
                'oss'#237'vel configurar manualmente o espa'#231'amento entre os campos, pa' +
                'ra isto defina o espa'#231'amento igual no campo '#8220'Cabe'#231'alho das Colun' +
                'as'#8221' e no campo '#8220'Detalhe das colunas'#8221' para que a listagem n'#227'o sai' +
                'a desalinhada, nesta segunda n'#227'o altere os nomes dos campos, poi' +
                's atrav'#233's destes nomes o programa saber'#225' qual o valor a ser info' +
                'rmado.'
              
                '    Ap'#243's configurar a listagem '#233' poss'#237'vel verificar como esta sa' +
                'ir'#225' na impressora, clicando na aba '#8220'Visualiza'#231#227'o da primeira p'#225'g' +
                'ina'#8221'. Estando a configura'#231#227'o no formato necess'#225'rio para o operad' +
                'or do sistema '#233' poss'#237'vel salva-l'#225' no banco de dados do sistema p' +
                'ara que possa ser reutilizada posteriormente, para isso clique e' +
                'm '#8220'Salvar Modelo como...'#8221' e informe uma descri'#231#227'o para este mode' +
                'lo. Para reutilizar um modelo j'#225' configurado e salvo, basta clic' +
                'ar em '#8220'Abrir Modelo'#8221' e selecionar o modelo desejado. Caso o mode' +
                'lo aberto for alterado e o operador do sistema quiser salvar as ' +
                'altera'#231#245'es efetuadas no modelo corrente basta clicar no bot'#227'o '#8220'S' +
                'alvar Modelo'#8221' ou '#8220'Salvar Modelo como...'#8221' caso queira criar um no' +
                'vo modelo a partir de um preexistente.'
              
                '    Para renomear um modelo existente abra a janela de sele'#231#227'o d' +
                'e modelos clicando no bot'#227'o '#8220'Abrir Modelo'#8221', ent'#227'o clique com o b' +
                'ot'#227'o direito na grade de modelos e clique em '#8220'Alterar Descri'#231#227'o ' +
                'do Modelo'#8221' e informe a nova descri'#231#227'o.'
              
                '    Para que os n'#250'meros das p'#225'ginas sejam mostrados na listagem ' +
                'marque a op'#231#227'o '#8220'Mostrar N'#186' da P'#225'gina'#8221', as op'#231#245'es '#8220'Mostrar total ' +
                'de registros'#8221' e '#8220'Usar separador de linhas'#8221' servem respectivament' +
                'e para que ao final da listagem seja exibida uma linha informand' +
                'o o total de registros listados e para que cada linha seja separ' +
                'ada por outra linha contendo caracteres do tipo h'#237'fen '#39'-'#39'.')
            TabOrder = 0
            Visible = False
            WordWrap = False
          end
          object edttamanho: TEdit
            Left = 2
            Top = 115
            Width = 35
            Height = 21
            TabOrder = 1
            Text = '0'
            OnChange = edttamanhoChange
            OnKeyPress = edttamanhoKeyPress
          end
        end
        object AbreMod: TBitBtn
          Left = 510
          Top = 62
          Width = 150
          Height = 25
          Hint = 'Abre modelo gravado.'
          Caption = '&Abrir Modelo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = AbreModClick
        end
        object SalvaMod: TBitBtn
          Left = 510
          Top = 99
          Width = 150
          Height = 25
          Hint = 'Salva o modelo em aberto.'
          Caption = 'Sa&lvar Modelo'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = SalvaModClick
        end
        object SalvaModComo: TBitBtn
          Left = 510
          Top = 136
          Width = 150
          Height = 25
          Hint = 'Salvar novo modelo'
          Caption = '&Salvar Modelo como..'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = SalvaModComoClick
        end
        object Panel4: TPanel
          Left = 496
          Top = 0
          Width = 176
          Height = 53
          BorderStyle = bsSingle
          TabOrder = 9
          object Label6: TLabel
            Left = 1
            Top = 1
            Width = 170
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Descri'#231#227'o do Modelo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object labmodelo: TLabel
            Left = 1
            Top = 14
            Width = 170
            Height = 13
            Align = alTop
            Caption = 'Novo Modelo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object ProgressBar1: TProgressBar
          Left = 263
          Top = 446
          Width = 409
          Height = 17
          TabOrder = 10
        end
        object Button3: TButton
          Left = 510
          Top = 231
          Width = 150
          Height = 25
          Caption = 'E&xportar para arquivo'
          TabOrder = 11
          OnClick = Button3Click
        end
        object cbTamFonte: TComboBox
          Left = 600
          Top = 280
          Width = 41
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 6
          TabOrder = 12
          Text = '7'
          OnChange = cbTamFonteChange
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '10'
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20')
        end
        object btnAlEsq: TBitBtn
          Left = 7
          Top = 256
          Width = 25
          Height = 25
          Hint = 'Alinhar Texto '#224' Esquerda'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 13
          OnClick = btnAlEsqClick
          Glyph.Data = {
            6A030000424D6A030000000000006A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004D0000004D000000CC986500CE9A
            6700D19D6A00D4A06D00D7A37000DBA77400DFAB7800E3AF7C00E6B27F00E8B4
            8100ECB88500F0BC8900F4C08D00F7C39000FAC79300FDCA9600FFCC9800E8CF
            B600EBD4BD00FFD0A000FFD6AB00FFD9B200FFDCB800CFCFD000D0D0D100D1D2
            D200D2D3D300D4D5D500D6D6D700D7D8D800D9DADA00DADADB00DBDBDC00DCDD
            DD00DEDFDF00ECD9C600EED9C500F1DECB00FFE2C300FFE6CC00F3E1D000FFEE
            DD00FFEFDF00E0E0E100E2E2E200E3E4E400E4E5E500E5E5E600E8E8E800E9EA
            EA00EAEAEB00EBEBEB00ECECEC00EEEEEE00FFF0E100FFF1E300FFF3E600FFF4
            E900FFF6EC00F0F0F000F2F2F200F3F3F300F7F7F700FFF7F000FFF9F300FFFA
            F600F8F8F800F9F9F900FAFAFA00FBFBFB00FFFCF900FFFDFB00FCFCFC00FDFD
            FD00FFFEFD00FEFEFE00FFFFFF004C4C2311122425282825241211234C4C4C4C
            353133353B3C3C3B353331354C4C4C4C0129292929292929292929014C4C4C4C
            183E3E3E3E3E3E3E3E3E3E184C4C4C4C022A0000000000002A2A2A024C4C4C4C
            193E3030303030303E3E3E194C4C4C4C0336363636363636363636034C4C4C4C
            1A424242424242424242421A4C4C4C4C0437000000000000000037044C4C4C4C
            1B433030303030303030431B4C4C4C4C0538383838383838383838054C4C4C4C
            1C444444444444444444441C4C4C4C4C06390000000000003A3A39064C4C4C4C
            1D443030303030304444441D4C4C4C4C073A3A3A3A3A3A3A3A3A3A074C4C4C4C
            1E444444444444444444441E4C4C4C4C093F00000000000000003F094C4C4C4C
            2045303030303030303045204C4C4C4C0A404040404040404040400A4C4C4C4C
            2148484848484848484848214C4C4C4C0B410000000000004A4A410B4C4C4C4C
            2249303030303030494949224C4C4C4C0C464646464646464646460C4C4C4C4C
            2B4B4B4B4B4B4B4B4B4B4B2B4C4C4C4C0D470000000000000000470D4C4C4C4C
            2C4C30303030303030304C2C4C4C4C4C0E4A4A4A4A4A4A4A4A4A4A0E4C4C4C4C
            2D4C4C4C4C4C4C4C4C4C4C2D4C4C4C4C0F4C4C4C4C4C4C4C4C4C4C0F4C4C4C4C
            2E4C4C4C4C4C4C4C4C4C4C2E4C4C4C4C1513141626272726161413154C4C4C4C
            343032353B3D3D3B353230344C4C}
          NumGlyphs = 2
        end
        object btnAlCenter: TBitBtn
          Left = 31
          Top = 256
          Width = 25
          Height = 25
          Hint = 'Centralizar'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 14
          OnClick = btnAlCenterClick
          Glyph.Data = {
            6A030000424D6A030000000000006A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004D0000004D000000CC986500CE9A
            6700D19D6A00D4A06D00D7A37000DBA77400DFAB7800E3AF7C00E6B27F00E8B4
            8100ECB88500F0BC8900F4C08D00F7C39000FAC79300FDCA9600FFCC9800E8CF
            B600EBD4BD00FFD0A000FFD6AB00FFD9B200FFDCB800CFCFD000D0D0D100D1D2
            D200D2D3D300D4D5D500D6D6D700D7D8D800D9DADA00DADADB00DBDBDC00DCDD
            DD00DEDFDF00ECD9C600EED9C500F1DECB00FFE2C300FFE6CC00F3E1D000FFEE
            DD00FFEFDF00E0E0E100E2E2E200E3E4E400E4E5E500E5E5E600E8E8E800E9EA
            EA00EAEAEB00EBEBEB00ECECEC00EEEEEE00FFF0E100FFF1E300FFF3E600FFF4
            E900FFF6EC00F0F0F000F2F2F200F3F3F300F7F7F700FFF7F000FFF9F300FFFA
            F600F8F8F800F9F9F900FAFAFA00FBFBFB00FFFCF900FFFDFB00FCFCFC00FDFD
            FD00FFFEFD00FEFEFE00FFFFFF004C4C2311122425282825241211234C4C4C4C
            353133353B3C3C3B353331354C4C4C4C0129292929292929292929014C4C4C4C
            183E3E3E3E3E3E3E3E3E3E184C4C4C4C022A2A0000000000002A2A024C4C4C4C
            193E3E3030303030303E3E194C4C4C4C0336363636363636363636034C4C4C4C
            1A424242424242424242421A4C4C4C4C0437000000000000000037044C4C4C4C
            1B433030303030303030431B4C4C4C4C0538383838383838383838054C4C4C4C
            1C444444444444444444441C4C4C4C4C06392A0000000000003A39064C4C4C4C
            1D444430303030303044441D4C4C4C4C073A3A3A3A3A3A3A3A3A3A074C4C4C4C
            1E444444444444444444441E4C4C4C4C093F00000000000000003F094C4C4C4C
            2045303030303030303045204C4C4C4C0A404040404040404040400A4C4C4C4C
            2148484848484848484848214C4C4C4C0B41410000000000004A410B4C4C4C4C
            2249493030303030304949224C4C4C4C0C464646464646464646460C4C4C4C4C
            2B4B4B4B4B4B4B4B4B4B4B2B4C4C4C4C0D470000000000000000470D4C4C4C4C
            2C4C30303030303030304C2C4C4C4C4C0E4A4A4A4A4A4A4A4A4A4A0E4C4C4C4C
            2D4C4C4C4C4C4C4C4C4C4C2D4C4C4C4C0F4C4C4C4C4C4C4C4C4C4C0F4C4C4C4C
            2E4C4C4C4C4C4C4C4C4C4C2E4C4C4C4C1513141626272726161413154C4C4C4C
            343032353B3D3D3B353230344C4C}
          NumGlyphs = 2
        end
        object btnAlDir: TBitBtn
          Left = 55
          Top = 256
          Width = 25
          Height = 25
          Hint = 'Alinhar Texto '#224' Direita'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 15
          OnClick = btnAlDirClick
          Glyph.Data = {
            6A030000424D6A030000000000006A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004D0000004D000000CC986500CE9A
            6700D19D6A00D4A06D00D7A37000DBA77400DFAB7800E3AF7C00E6B27F00E8B4
            8100ECB88500F0BC8900F4C08D00F7C39000FAC79300FDCA9600FFCC9800E8CF
            B600EBD4BD00FFD0A000FFD6AB00FFD9B200FFDCB800CFCFD000D0D0D100D1D2
            D200D2D3D300D4D5D500D6D6D700D7D8D800D9DADA00DADADB00DBDBDC00DCDD
            DD00DEDFDF00ECD9C600EED9C500F1DECB00FFE2C300FFE6CC00F3E1D000FFEE
            DD00FFEFDF00E0E0E100E2E2E200E3E4E400E4E5E500E5E5E600E8E8E800E9EA
            EA00EAEAEB00EBEBEB00ECECEC00EEEEEE00FFF0E100FFF1E300FFF3E600FFF4
            E900FFF6EC00F0F0F000F2F2F200F3F3F300F7F7F700FFF7F000FFF9F300FFFA
            F600F8F8F800F9F9F900FAFAFA00FBFBFB00FFFCF900FFFDFB00FCFCFC00FDFD
            FD00FFFEFD00FEFEFE00FFFFFF004C4C2311122425282825241211234C4C4C4C
            353133353B3C3C3B353331354C4C4C4C0129292929292929292929014C4C4C4C
            183E3E3E3E3E3E3E3E3E3E184C4C4C4C022A2A2A0000000000002A024C4C4C4C
            193E42423030303030303E194C4C4C4C0336363636363636363636034C4C4C4C
            1A424242424242424242421A4C4C4C4C0437000000000000000037044C4C4C4C
            1B433030303030303030431B4C4C4C4C0538383838383838383838054C4C4C4C
            1C444444444444444444441C4C4C4C4C0639393900000000000039064C4C4C4C
            1D444444303030303030441D4C4C4C4C073A3A3A3A3A3A3A3A3A3A074C4C4C4C
            1E444444444444444444441E4C4C4C4C093F00000000000000003F094C4C4C4C
            2045303030303030303045204C4C4C4C0A404040404040404040400A4C4C4C4C
            2148484848484848484848214C4C4C4C0B414141000000000000410B4C4C4C4C
            2249494930303030303049224C4C4C4C0C464646464646464646460C4C4C4C4C
            2B4B4B4B4B4B4B4B4B4B4B2B4C4C4C4C0D470000000000000000470D4C4C4C4C
            2C4C30303030303030304C2C4C4C4C4C0E4A4A4A4A4A4A4A4A4A4A0E4C4C4C4C
            2D4C4C4C4C4C4C4C4C4C4C2D4C4C4C4C0F4C4C4C4C4C4C4C4C4C4C0F4C4C4C4C
            2E4C4C4C4C4C4C4C4C4C4C2E4C4C4C4C1513141626272726161413154C4C4C4C
            343032353B3D3D3B353230344C4C}
          NumGlyphs = 2
        end
        object cbFontes: TComboBox
          Left = 480
          Top = 280
          Width = 105
          Height = 21
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 16
          Text = 'Courier New'
          OnChange = cbFontesChange
          Items.Strings = (
            'Courier New')
        end
        object cbFonteEstilo: TCheckBox
          Left = 183
          Top = 312
          Width = 146
          Height = 17
          Caption = 'Aplicar Fonte e Estilo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          OnClick = cbFonteEstiloClick
        end
        object pnlCorPar: TPanel
          Left = 330
          Top = 311
          Width = 20
          Height = 18
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Color = 14933984
          TabOrder = 18
          TabStop = True
          OnClick = pnlCorParClick
        end
        object btnCorPar: TButton
          Left = 348
          Top = 312
          Width = 75
          Height = 18
          Caption = 'Cor linha par'
          TabOrder = 19
          OnClick = btnCorParClick
        end
        object btnCorImpar: TButton
          Left = 460
          Top = 312
          Width = 85
          Height = 18
          Caption = 'Cor linha impar'
          TabOrder = 20
          OnClick = btnCorImparClick
        end
        object pnlCorImpar: TPanel
          Left = 442
          Top = 311
          Width = 20
          Height = 18
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Color = clWhite
          TabOrder = 21
          TabStop = True
          OnClick = pnlCorParClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Visualiza'#231#227'o da primeira p'#225'gina'
      ImageIndex = 1
      object ListPrev: TListBox
        Left = 3
        Top = 4
        Width = 669
        Height = 460
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Orator10 BT'
        Font.Style = []
        ItemHeight = 12
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object DSRels: TDataSource
    Left = 362
    Top = 216
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    FileName = 'arquivo.txt'
    Filter = 'Arquivos Texto (*.txt)|(*.txt)'
    Left = 442
    Top = 168
  end
  object SaveDialog2: TSaveDialog
    DefaultExt = 'xml'
    FileName = 'arquivo.xml'
    Filter = 'Arquivos XML (*.xml)|(*.xml)'
    Left = 362
    Top = 168
  end
  object cd: TColorDialog
    Left = 442
    Top = 216
  end
end
