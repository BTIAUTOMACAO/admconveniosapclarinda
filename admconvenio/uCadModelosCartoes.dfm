inherited FCadModelosCartoes: TFCadModelosCartoes
  Left = 203
  Top = 242
  Caption = 'Cadastro de Modelos de Cart'#245'es'
  ClientWidth = 765
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 765
    ActivePage = TabGrade
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 757
        inherited Label2: TLabel
          Width = 48
          Caption = '&Descri'#231#227'o'
        end
        object Label5: TLabel [8]
          Left = 349
          Top = 3
          Width = 58
          Height = 13
          Caption = '&Observa'#231#227'o'
          FocusControl = edObs
        end
        inherited EdNome: TEdit
          Hint = 'Busca por descri'#231#227'o'
        end
        inherited ButBusca: TBitBtn
          Left = 612
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 673
        end
        object edObs: TEdit
          Left = 349
          Top = 18
          Width = 257
          Height = 21
          Hint = 'Busca por observa'#231#227'o'
          CharCase = ecUpperCase
          TabOrder = 6
          OnKeyPress = EdNomeKeyPress
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 757
      end
      inherited Barra: TStatusBar
        Width = 757
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Width = 757
      end
      inherited Panel3: TPanel
        Width = 757
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 753
          Height = 103
          Align = alTop
          Caption = 'Dados cadastrais'
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 15
            Width = 49
            Height = 13
            Caption = 'Empres ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 78
            Top = 15
            Width = 58
            Height = 13
            Caption = 'Descri'#231#227'o'
            FocusControl = dbEdtNm
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label16: TLabel
            Left = 8
            Top = 55
            Width = 58
            Height = 13
            Caption = 'Observa'#231#227'o'
            FocusControl = DBEdit14
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 30
            Width = 65
            Height = 21
            Hint = 'C'#243'digo da empresa'
            TabStop = False
            CharCase = ecUpperCase
            Color = clBtnFace
            DataField = 'MOD_CART_ID'
            DataSource = DSCadastro
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
          object dbEdtNm: TDBEdit
            Left = 78
            Top = 30
            Width = 667
            Height = 21
            Hint = 'Raz'#227'o social da empresa'
            CharCase = ecUpperCase
            DataField = 'DESCRICAO'
            DataSource = DSCadastro
            MaxLength = 59
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 8
            Top = 70
            Width = 736
            Height = 21
            Hint = 'Nome da empresa que ser'#225' exibido no cart'#227'o'
            CharCase = ecUpperCase
            DataField = 'OBSERVACAO'
            DataSource = DSCadastro
            MaxLength = 30
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
          end
        end
        object GroupBox5: TGroupBox
          Left = 2
          Top = 415
          Width = 753
          Height = 60
          Align = alBottom
          Caption = 'Informa'#231#245'es Adicionais/Dados da '#250'ltima altera'#231#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object Label20: TLabel
            Left = 8
            Top = 15
            Width = 141
            Height = 13
            Caption = 'Data da Altera'#231#227'o / Operador'
            FocusControl = DBEdit35
          end
          object Label54: TLabel
            Left = 288
            Top = 15
            Width = 138
            Height = 13
            Caption = 'Data do Cadastro / Operador'
            FocusControl = DBEdit36
          end
          object DBEdit35: TDBEdit
            Left = 8
            Top = 30
            Width = 121
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            Enabled = False
            TabOrder = 0
          end
          object DBEdit36: TDBEdit
            Left = 288
            Top = 30
            Width = 105
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'DTCADASTRO'
            DataSource = DSCadastro
            Enabled = False
            TabOrder = 1
          end
          object DBEdit37: TDBEdit
            Left = 397
            Top = 30
            Width = 100
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'OPERCADASTRO'
            DataSource = DSCadastro
            Enabled = False
            TabOrder = 2
          end
          object DBEdit38: TDBEdit
            Left = 137
            Top = 30
            Width = 100
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            Enabled = False
            TabOrder = 3
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 757
      end
      inherited GridHistorico: TJvDBGrid
        Width = 757
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 765
  end
  inherited panStatus2: TPanel
    Width = 765
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterPost = QCadastroAfterPost
    SQL.Strings = (
      'select'
      '  *'
      'from modelos_cartoes'
      'where mod_cart_id = 0')
    object QCadastroMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
    end
    object QCadastroOBSERVACAO: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'OBSERVACAO'
      Size = 50
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Visible = False
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
end
