unit cartao_util;

interface

uses Windows, Forms, WinSock, DB, Variants, TypInfo, dbgrids, classes, comobj, dialogs,
controls, math,uDatas, Excel_TLB,SysUtils, StrUtils,Messages,dateutils;

type TipoData = (Inicial,Final,SemHora);

//FUN��ES//
function Crypt(Action, Src, Key : String) : String;
function FormatDinBR(Valor:Extended;num_dec:Integer=2):String;
function replicate(caracter : char; tamanho : integer) : String;
function GetMajorVersionExe(FileName: string): Cardinal;
function GetMinorVersionExe(FileName: string): Cardinal;
function CalcDigVerf2(Numero : String ; Verificar : Boolean = False) : Integer;
function gerarCartao(numBase : Integer) : String;
function gerarCartaoCantinex(numBase : string) : String;
function GetReleaseVersionExe(FileName: string): Cardinal;
function GetBuildVersionExe(FileName: string): Cardinal;
function CalcFixo(N: String): Integer;
function iif(question : boolean; STrue, SFalse : String) : String; overload;
function iif(question : boolean; STrue, SFalse : Variant) : Variant; overload;
function FileTimeToDTime(FTime: TFileTime): TDateTime;
function GetProxDiaUtil (dData : TDateTime) : TDateTime;
function GetDia(DataIn: TDateTime) : integer;
function GetMes(DataIn: TDateTime) : integer;
function GetAno(DataIn: TDateTime) : integer;
function FormatDateIB(Data:TDateTime;ComAspas:Boolean=True;Tipo:TipoData=SemHora):String;
function SoNumero(texto : String) : String;
function GetIP:string;
function PadR(Texto : String; Tamanho : Integer; Caracter : Char) : string;
function GetExeVersion:Integer;
function GetBuildInfo:string;
function IsFloatField(FieldType : TFieldType):Boolean;
function IsIntegerField(FieldType : TFieldType):Boolean;
function IsDateTimeField(FieldType : TFieldType):Boolean;
function IsNumericField(FieldType : TFieldType):Boolean;
function IsStringField(FieldType : TFieldType):Boolean;
function RemoveCaracter(S : String ; Caracter : Char = ' ') : String;
function DigitoCartao(Numero : double) : integer;
function Coalesce(Value:Variant):Extended;
function StringToEnum(S : String; EnumType: PTypeInfo ):Integer;
function FormatDimIB(Valor:Extended):String;
function TipodoCampo(tipo : TFieldType ) : String;
function IsEditableField(Field:TField;ChavePri:String):Boolean;
function ValidaCNPJ(cgc :String) : Boolean;
Function ValidaCPF(const s:string): Boolean;
function ArredondaDin(valor : Currency) : Currency;
function DigitoControle(Numero : double) : integer;
function val2(texto : String) : integer;
function PadL(Texto : String; Tamanho : Integer; Caracter : Char) : string;
function verificaCartaoExistente(cartao: String) : Boolean;
function verificaCartaoExistenteHistorico(cartao: String) : Boolean;
function verificaCartaoExistenteTabelaTemp(cartao: String) : Boolean;
function verificaCartaoExistenteBemEstar(cartao: String) : Boolean;
function verificaCartaoExistenteBemEstarHistorico(cartao: String) : Boolean;
function validarCartao(cartao : string) : Boolean;
function VerificarCartao2(Numero : String) : Boolean;
function VerfCartForte(Card : String) : Boolean;
function GetNomeComputador: String;
function IsValidDateTime(StrDate:String):Boolean;
function GridColByFieldName(DBGrid:TDBGrid;  FieldName:String;RaiseException:Boolean=True):TColumn;
function PrimeiroDiaMes(data: TDateTime): TDateTime;
function MakePropertyValue(ooServer: Variant; PropName:string; PropValue:variant): variant;
function RetornaRange(left, top, right, botton: integer):string;
function ConvertLetra(num:integer):string;
function FileName2URL(FileName: string): string;
function RemoveAcento(txt : string) : string;
function FormatDataHoraIB(Data:TDateTime;ComAspas:Boolean=True):String;
function extenso(valor: real): string;

//FORMATA DATAS//
function FormatDataBR(Data:TDateTime):String;
function FormatDataIB(Data:TDateTime;ComAspas:Boolean=True;Tipo:TipoData=SemHora):String;
function ChangeSNtoSimNao(S:String):String;
function FormataDataSql(mcad : string):string;
function RetornaMesExtenso(data: string):string;

//MENSAGENS//
function MsgSimNao(Txt : String;DefaultButtonYes:Boolean=True) : Boolean;


//PROCEDURES//
procedure MsgInf(Txt : String);
procedure MsgErro(Txt : String);
procedure IsNumericKey(Sender: TObject; var Key: Char);
procedure GridScroll(Sender: TObject; var Key: Word;Shift: TShiftState);
procedure Grade_to_PlanilhaExcel(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False);
procedure SortZQuery(Query:TDataset;FieldName:string);
Procedure Grade_to_PlanilhaMarcado(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False;SoMarcados:Boolean=False);
Procedure Grade_to_PlanilhaCalcMarcado(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False;SoMarcados:Boolean=False);
procedure ooDispatch(ooServer, ooDocument: Variant; ooCommand: string; ooParams: variant);
function LastDayCurrMon: TDate;
function LastDayMon(data : TDateTime): TDate; overload;
function LastDayMon(mes : integer): Integer; overload;
function MsgDataHora(diaSemanaPadrao : TDiaSemana = Hoje; selecaoSemana : TSelecaoSemana = ssTudo) : TDateTime;
function IsTipoCreditoCantinex(empres_id : Integer) : Boolean;
function GerarCVV(codCartImp : String) : String;
function JaEmitidoCartaoNovo(cartaoID : String) : Boolean;

implementation

uses VarUtils, URotinasTexto, DM, uSelecaoDataHora;

//FUN��ES//
function Crypt(Action, Src, Key : String) : String;
var
   KeyLen    : Integer;
   KeyPos    : Integer;
   offset    : Integer;
   dest      : string;
   SrcPos    : Integer;
   SrcAsc    : Integer;
   TmpSrcAsc : Integer;
   Range     : Integer;
begin
     dest:='';
     KeyLen:=Length(Key);
     KeyPos:=0;
     Range:=256;
     if Action = UpperCase('E') then
     begin
          Randomize;
          offset:=Random(Range);
          dest:=format('%1.2x',[offset]);
          for SrcPos := 1 to Length(Src) do
          begin
               SrcAsc:=(Ord(Src[SrcPos]) + offset) MOD 255;
               if KeyPos < KeyLen then KeyPos:= KeyPos + 1 else KeyPos:=1;
               SrcAsc:= SrcAsc xor Ord(Key[KeyPos]);
               dest:=dest + format('%1.2x',[SrcAsc]);
               offset:=SrcAsc;
          end;
     end;
     if Action = UpperCase('D') then
     begin
          offset:=StrToInt('$'+ copy(src,1,2));
          SrcPos:=3;
          repeat
                SrcAsc:=StrToInt('$'+ copy(src,SrcPos,2));
                if KeyPos < KeyLen Then KeyPos := KeyPos + 1 else KeyPos := 1;
                TmpSrcAsc := SrcAsc xor Ord(Key[KeyPos]);
                if TmpSrcAsc <= offset then
                     TmpSrcAsc := 255 + TmpSrcAsc - offset
                else
                     TmpSrcAsc := TmpSrcAsc - offset;
                dest := dest + chr(TmpSrcAsc);
                offset:=srcAsc;
                SrcPos:=SrcPos + 2;
          until SrcPos >= Length(Src);
     end;
     Crypt:=dest;
end;

function FormatDinBR(Valor:Extended;num_dec:Integer=2):String;
var dec : string;
begin
  dec := replicate('0',num_dec);
  dec := '###,###,##0.'+dec;
  Result := FormatFloat(dec,Valor);
end;

{M�TODO QUE BUSCA O CART�O PELO CODCARTIMP E, CASO J� EXISTA, RETORNA FALSO.}
function verificaCartaoExistente(cartao: String) : Boolean;
var s : String;
    d : Integer;
begin
  d := DMConexao.ExecuteSql('SELECT TOP 1 COALESCE(CODCARTIMP,'''') AS CODCARTIMP FROM CARTOES WHERE CODCARTIMP = '+QuotedStr(cartao));

  Result := 0 = d;

end;

function verificaCartaoExistenteHistorico(cartao: String) : Boolean;
var s : String;
    d : Integer;
begin
  d := DMConexao.ExecuteSql('SELECT TOP 1 COALESCE(CODCARTIMP,'''') AS CODCARTIMP FROM CARTOES_HISTORICO WHERE CODCARTIMP = '+QuotedStr(cartao));

  Result := 0 = d;

end;

function verificaCartaoExistenteTabelaTemp(cartao: String) : Boolean;
var s : String;
    d : Integer;
begin
  d := DMConexao.ExecuteSql('SELECT TOP 1 COALESCE(CODCARTIMP,'''') AS CODCARTIMP FROM CARTOES_TEMP WHERE CODCARTIMP = '+QuotedStr(cartao));

  Result := 0 = d;

end;

function verificaCartaoExistenteBemEstar(cartao: String) : Boolean;
var s : String;
    d : Integer;
begin
  d := DMConexao.ExecuteSql('SELECT TOP 1 COALESCE(CODCARTIMP,'''') AS CODCARTIMP FROM CARTOES_BEM_ESTAR WHERE CODCARTIMP = '+QuotedStr(cartao));

  Result := 0 = d;

end;

function verificaCartaoExistenteBemEstarHistorico(cartao: String) : Boolean;
var s : String;
    d : Integer;
begin
  d := DMConexao.ExecuteSql('SELECT TOP 1 COALESCE(CODCARTIMP,'''') AS CODCARTIMP FROM CARTOES_HISTORICO_BEM_ESTAR WHERE CODCARTIMP = '+QuotedStr(cartao));

  Result := 0 = d;

end;

function IsTipoCreditoCantinex(empres_id : Integer) : Boolean;
var tipoCredito : Integer;
begin
  tipoCredito := DMConexao.ExecuteScalar('SELECT coalesce(tipo_credito,-1) FROM empresas WHERE empres_id = '+IntToStr(empres_id)+'');
  Result := 'S' = IfThen(tipoCredito = 4,'S','N');
end;

function RemoveAcento(txt : string) : string;
Const
	ComAcento = '����������������������������';
	SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
var
    key : char;
    i 	: integer;
Begin
  for i := 1 to length(txt) do begin
     key := txt[i];
     if Pos(key, ComAcento)<>0 Then begin
        key := SemAcento[Pos(key, ComAcento)];
	txt[i] := key;
     end;
  end;
  Result := txt;
end;

function ConvertTextoFB(txt: string): string;
begin
  Result:= UpperCase(RemoveAcento(Trim(txt)));
end;

procedure GridScroll(Sender: TObject; var Key: Word;Shift: TShiftState);
var Grid : TDbGrid;
begin
if not (Sender is TDBGrid) then
   raise Exception.Create('Objeto passado n�o � uma DBGrid!');
Grid := Sender as TDBGrid;
case Key  of
   VK_LEFT   : Grid.Perform(WM_HSCROLL,0,0);
   VK_RIGHT  : Grid.Perform(WM_HSCROLL,1,0);
end;
if Key in [vk_left,vk_right] then Key := 0;
end;

function LastDayCurrMon: TDate;
begin
   result := EncodeDate(YearOf(Now),MonthOf(Now), DaysInMonth(Now)) ;
end;

function LastDayMon(data : TDateTime): TDate; overload;
var dia,mes,ano : word;
begin
  decodeDate(data,ano,mes,dia);
  result := encodedate(ano,mes,DaysInMonth(date));
end;

function LastDayMon(mes : Integer): Integer; overload;
var data : TDate;
  m : string;
begin
  if (mes > 12) or (mes < 1) then
    result := MonthOf(Date);
  m := iif(mes > 9, IntToStr(mes), '0'+inttostr(mes));
  data := StrToDateTime(FormatDateTime('dd/'+m+'/yyyy',now));
  data := EncodeDate(YearOf(data),MonthOf(data), DaysInMonth(data)) ;
  result := StrToInt(formatDateTime('dd',data));
end;

function MsgDataHora(diaSemanaPadrao : TDiaSemana = Hoje; selecaoSemana : TSelecaoSemana = ssTudo) : TDateTime;
begin
  frmSelecaoDataHora := TfrmSelecaoDataHora.Create(nil);
  frmSelecaoDataHora.ExibirMsgDataHora(diaSemanaPadrao,selecaoSemana);
  frmSelecaoDataHora.ShowModal;
  if frmSelecaoDataHora.ModalResult = mrOk then begin
    Result := frmSelecaoDataHora.calendario.Date;
    FreeAndNil(frmSelecaoDataHora);
  end else
    Result := 0;
end;

function gerarCartao(numBase : Integer) : String;
var s : String;
    P : Integer;
begin
  Randomize;
  P := RandomRange(1000000, 9999998);
  s := IntToStr(numBase) + IntToStr(P);
  s := IntToStr(CalcDigVerf2(S));
  s := fnMascarar(IntToStr(numBase) + s,'#### #### #### ####');
  result := s;
end;


function gerarCartaoCantinex(numBase : string) : String;
var s : String;
    P : Integer;
begin
  Randomize;
  P := RandomRange(1000, 9998);
  s := IntToStr(P);
  //s := IntToStr(CalcDigVerf2(S));
  s := fnMascarar((numBase) + s,'#### #### #### ####');
  result := s;
end;

function RetornaMesExtenso(data : string) : String;
var s : String;
    P : Integer;
begin
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' SELECT');
  DMConexao.Q.SQL.Add(' CASE DATEPART(MONTH, '+ QuotedStr(data) + ')');
  DMConexao.Q.SQL.Add('       WHEN 1 THEN ''JANEIRO''');
  DMConexao.Q.SQL.Add('       WHEN 2 THEN ''FEVEREIRO''');
  DMConexao.Q.SQL.Add('       WHEN 3 THEN ''MARCO''');
  DMConexao.Q.SQL.Add('       WHEN 4 THEN ''ABRIL''');
  DMConexao.Q.SQL.Add('       WHEN 5 THEN ''MAIO''');
  DMConexao.Q.SQL.Add('       WHEN 6 THEN ''JUNHO''');
  DMConexao.Q.SQL.Add('       WHEN 7 THEN ''JULHO''');
  DMConexao.Q.SQL.Add('       WHEN 8 THEN ''AGOSTO''');
  DMConexao.Q.SQL.Add('       WHEN 9 THEN ''SETEMBRO''');
  DMConexao.Q.SQL.Add('       WHEN 10 THEN ''OUTUBRO''');
  DMConexao.Q.SQL.Add('       WHEN 11 THEN ''NOVEMBRO''');
  DMConexao.Q.SQL.Add('       WHEN 12 THEN ''DEZEMBRO''');
  DMConexao.Q.SQL.Add('     END AS ''MES''');
  DMConexao.Q.Open;
  s := DMConexao.Q.Fields[0].Value;
  result := s;
end;

procedure IsNumericKey(Sender: TObject; var Key: Char);
begin
   if not (Key in ['0'..'9',#13,#8]) then
      Key := #0;
end;

function VerfCartForte(Card: String): Boolean;
var I, C : Integer;
    S,S2 : String;
begin
  for I := 48 to 90 do begin
    S := chr(I);
    S2 := chr(I);
    C := 1;
    //Preenchendo String auxiliar para descobrir se a senha � fraca!
    while Length(S) < Length(Card) do begin
      S := S + chr(I);
      S2 := S2 + chr(I+C);
      C := C + 1;
    end;
    if (UpperCase(Card) = UpperCase(S)) or (UpperCase(Card) = UpperCase(S2)) then begin
      Result := False;
      Exit;
    end;
  end;
Result := True;
end;

function ValidaCNPJ(cgc :String) : Boolean;
var
  a, j, i, d1, d2 : Integer;
  resultado : integer;
begin
  resultado := 0;
  If (Length(cgc) = 8) And (StrToInt(cgc) > 0) Then begin
//     a := 0;
     j := 0;
//     d1 := 0;
     For i := 1 To 7 do begin
         a := StrToInt(Copy(cgc, i, 1));
         If (i Mod 2) <> 0 Then a := a * 2;
         If a > 9          Then j := j + strtoint(floattostr(Int(a / 10) + (a Mod 10)))
         Else                   j := j + a;
         end;

     if (j mod 10) <> 0 then  d1 := 10 - (j mod 10)
     else                     d1 := 0;
     If d1 = StrToInt(Copy(cgc, 8, 1)) Then ValidaCNPJ := True
     Else                                   ValidaCNPJ := False;
     end
  else begin
    for i := 1 to length(cgc) do resultado := resultado + strtoint(copy(cgc, i, 1));
    If (Length(cgc) = 14) And (resultado > 0) Then begin
       a := 0;
//       i := 0;
//       d1 := 0;
//       d2 := 0;
       j := 5;
       For i := 1 To 12 do begin
           a := a + (strtoint(Copy(cgc, i, 1)) * j);
           if j > 2 then j := j-1
           else          j := 9;
           end;
      a := a Mod 11;
      if a > 1 then d1 := 11 - a
      else          d1 := 0;

      a := 0;
//      i := 0;
      j := 6;

      For i := 1 To 13 do begin
        a := a + (strtoint(copy(cgc, i, 1)) * j);
        If(j > 2) then j := j -1
        else           j := 9;
      end;

      a := a Mod 11;

      If (a > 1) then d2 := 11 - a
      else            d2 := 0;

      If (d1 = strtoint(copy(cgc, 13, 1))) And (d2 = strtoint(copy(cgc, 14, 1))) Then ValidaCNPJ := True
      Else                                                                            ValidaCNPJ := False;
    End
    Else
      ValidaCNPJ := False;
  end;
end;

Function ValidaCPF(const s:string): Boolean;
var
i, Numero, Resto : Byte ;
DV1, DV2 : Byte ;
Total, Soma : Integer ;

Begin
result := FALSE;
if length (Trim (s)) = 11 Then
begin
  Total := 0 ;
  Soma := 0 ;
  for i := 1 to 9 do
    Begin
      Try
        Numero := StrToInt (s[i]);
      Except
        Numero := 0;
      end;
  Total := Total + (Numero * (11 - i)) ;
  Soma := Soma + Numero;
end;
Resto := Total mod 11;
if Resto > 1 then
  DV1 := 11 - Resto
else
  DV1 := 0;
Total := Total + Soma + 2 * DV1;
Resto := Total mod 11;
if Resto > 1 then
  DV2 := 11 - Resto
else
  DV2 := 0;
if (IntToStr (DV1) = s[10]) and (IntToStr (DV2) = s[11]) then
  result := TRUE;
end;
end;

function VerificarCartao2(Numero: String): Boolean;
var I : Integer;
begin
Numero := RemoveCaracter(Numero);
  for I := 1 to Length(Numero) do
    if not (Numero[I] in ['0'..'9']) then
      Result := False;
  if Result then begin
    if (CalcDigVerf2(Numero,True) > -1) and (VerfCartForte(IntToStr(CalcDigVerf2(Numero)))) then
      Result := True
    else
      Result := False;
  end;
end;

function CalcDigVerf2(Numero: String ; Verificar : Boolean = False) : Integer;
var vFixo, r, r2 : Integer;
    nAnt : String;
begin
  r2 := 0;
  vFixo := CalcFixo(Copy(Numero,1,8));
  Result := 0;
  Numero := fnRemoveCaracters(' ',Numero); //Removendo os espa�os em branco
  if Verificar then
    nAnt   := Copy(Numero,9,8);
  Numero := Copy(Numero,9,7);
  If (Length(Numero) <> 7) or (vFixo <= -1) then begin
    Result := -1;
    Exit;
  end;
  if Numero[3] = '0' then
    r := StrToInt(Numero[2]) * StrToInt(Numero[1]) div 1 + vFixo
  else
    r := StrToInt(Numero[2]) * StrToInt(Numero[1]) div StrToInt(Numero[3]) + vFixo;
  while r > 9 do begin
    if r mod 2 = 0 then
      r := r div 4
    else
      r := r div 3;
  end;
  if (StrToInt(copy(Numero,1,3)) > 0) and (StrToInt(copy(Numero,1,3)) <= 222) then begin
    SubstCarct(Numero,IntToStr(r),4);
    r2 := StrToInt(Numero[4]);
  end else if (StrToInt(copy(Numero,1,3)) > 222) and (StrToInt(copy(Numero,1,3)) <= 444) then begin
    SubstCarct(Numero,IntToStr(r),5);
    r2 := StrToInt(Numero[5]);
  end else if (StrToInt(copy(Numero,1,3)) > 444) and (StrToInt(copy(Numero,1,3)) <= 777) then begin
    SubstCarct(Numero,IntToStr(r),6);
    r2 := StrToInt(Numero[6]);
  end else if (StrToInt(copy(Numero,1,3)) > 777) and (StrToInt(copy(Numero,1,3)) <= 999) then begin
    SubstCarct(Numero,IntToStr(r),7);
    r2 := StrToInt(Numero[7]);
  end;
  if r + StrToInt(Numero[1]) - StrToInt(Numero[2]) >= 0 then begin
    r := r + StrToInt(Numero[1]) - StrToInt(Numero[2]);
    while r > 9 do begin
      if r mod 2 = 0 then
        r := r div 6
      else
        r := r div 5;
    end;
  end;
  if Length(Numero) <= 8 then
    Numero := Numero + IntToStr(r)
  else
    SubstCarct(Numero,IntToStr(r),8);
  Result := strToInt(Numero);
  if Verificar then begin
    case StrToInt(copy(Numero,1,3)) of
    0  ..222:
      if (r2 <> StrToInt(Numero[4])) or (StrToInt(Numero[8]) <> r) or (nAnt <> Numero) then
        Result := -1;
    223..444:
      if (r2 <> StrToInt(Numero[5])) or (StrToInt(Numero[8]) <> r) or (nAnt <> Numero) then
        Result := -1;
    445..777:
      if (r2 <> StrToInt(Numero[6])) or (StrToInt(Numero[8]) <> r) or (nAnt <> Numero) then
        Result := -1;
    778..999:
      if (r2 <> StrToInt(Numero[7])) or (StrToInt(Numero[8]) <> r) or (nAnt <> Numero) then
        Result := -1;
    end;
  end;
end;

function DigitoCartao(Numero : double) : integer;
var
   SNumero : String;
   xloop, inumero, iMulti, digito1, digito2 : Integer;

   Soma : Integer;
begin
     SNumero := FormatFloat('000000000', Numero);
     Soma := 0;
     for xloop := 1 to 9 do begin
         inumero := val2( SNumero[ xloop ] );
         iMulti := 10 - xloop;
         Soma := Soma + (INumero * iMulti);
     end;
     digito1 := (Soma mod 10);
     Soma := digito1 * 2;
     for xloop := 1 to 9 do begin
         inumero := val2( SNumero[ xloop ] );
         iMulti := 11 - xloop;
         Soma := Soma + (INumero * iMulti);
     end;
     digito2 := (Soma mod 10);
     result := Digito2 * 10 + Digito1;
end;


function RemoveCaracter(S : String ; Caracter : Char) : String;
var I : Integer;
begin
I := 1;
Result := '';
  if Length(S) = 0 then begin
    Result := '';
    Exit;
  end;
  while (I <= Length(S)) do begin
    if S[I] <> Caracter then
      Result := Result + S[I];
    I := I + 1;
  end;
end;

function PadR(Texto : String; Tamanho : Integer; Caracter : Char) : string;
var
   TamTexto : Integer;
begin
     TamTexto := Length(Texto);
     if TamTexto < Tamanho then
        Result := Texto + replicate(Caracter, tamanho - TamTexto )
     else
         if Tamanho < TamTexto then
            Result := copy(Texto, 1, tamanho)
         else
             Result := Texto;
end;

function FormatDateIB(Data:TDateTime;ComAspas:Boolean=True;Tipo:TipoData=SemHora):String;
begin
  case Tipo of
     Inicial : Result := FormatDataIB(Data,False)+' 00:00:00';
     Final   : Result := FormatDataIB(Data,False)+' 23:59:59';
     SemHora : Result := FormatDataIB(Data,False);
  end;
  if ComAspas then
     Result := QuotedStr(Result);
end;

function replicate(caracter : char; tamanho : integer) : string;
var
   xloop : integer;
   volta : string;
begin
    volta := '';
    for xloop := 1 to tamanho
        do volta := volta + caracter;
    replicate := volta;
end;

function GetMajorVersionExe(FileName: string): Cardinal;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  GetMem(PVerInfo, VerInfoSize);
  try
    if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          Result := HiWord(PVerValue.dwFileVersionMS);
  finally
    FreeMem(PVerInfo, VerInfoSize);
  end;
end;

function GetMinorVersionExe(FileName: string): Cardinal;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  GetMem(PVerInfo, VerInfoSize);
  try
    if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
        Result := LoWord(PVerValue.dwFileVersionMS);
  finally
    FreeMem(PVerInfo, VerInfoSize);
  end;
end;

function GetReleaseVersionExe(FileName: string): Cardinal;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  GetMem(PVerInfo, VerInfoSize);
  try
    if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
        Result := HiWord(PVerValue.dwFileVersionLS);
  finally
    FreeMem(PVerInfo, VerInfoSize);
  end;
end;

function GetBuildVersionExe(FileName: string): Cardinal;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  GetMem(PVerInfo, VerInfoSize);
  try
    if GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, PVerInfo) then
      if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
        with PVerValue^ do
          Result := LoWord(PVerValue.dwFileVersionLS);
  finally
    FreeMem(PVerInfo, VerInfoSize);
  end;
end;

function iif(question : boolean; STrue, SFalse : String) : String;
begin
  if(question) then result := STrue
  else result := SFalse;
end;

function iif(question : boolean; STrue, SFalse : Variant) : Variant;
begin
  if(question) then result := STrue
  else result := SFalse;
end;

function FileTimeToDTime(FTime: TFileTime): TDateTime;
var
  LocalFTime: TFileTime;
  STime: TSystemTime;
begin
  FileTimeToLocalFileTime(FTime, LocalFTime);
  FileTimeToSystemTime(LocalFTime, STime);
  Result := SystemTimeToDateTime(STime);
end;

function CalcFixo(N: String): Integer;
var I : Integer;
    s : String;
begin
  Result := 0;
  N := fnRemoveCaracters(' ',N); //Removendo os espa�os em branco
  If Length(N) <> 8 then begin
    Result := -1;
    Exit;
  end;
  for I := 1 to Length(N) do begin
    s := N[I];
    if strToInt(s) mod 3 = 1 then
      s := IntToStr(StrToInt(s) * 2);
    if length(s) > 1 then
      s := s[Length(s)];
    Result := Result + StrToInt(s);
  end;
  Result := (Result div 7) + (Result mod 7);
end;

function FormatDataIB(Data:TDateTime;ComAspas:Boolean=True;Tipo:TipoData=SemHora):String;
begin
  Result := FormatDateTime('dd/mm/yyyy',Data);
  case Tipo of
     Inicial : Result := Result + ' 00:00:00';
     Final   : Result := Result + ' 23:59:59';
  end;
  if ComAspas then Result := QuotedStr(Result);
end;

function FormatDataBR(Data:TDateTime):String;
begin
  Result := FormatDateTime('dd/mm/yyyy',Data);
end;

function MsgSimNao(Txt : String;DefaultButtonYes:Boolean=True) : Boolean;
var DefaultBut : Integer;
begin
   if DefaultButtonYes then
      DefaultBut := MB_DEFBUTTON1
   else
      DefaultBut := MB_DEFBUTTON2;
   result := (Application.MessageBox(Pchar(Txt),'Confirma��o...',MB_YESNO+MB_ICONQUESTION+DefaultBut) = IDYES);
end;

function GetProxDiaUtil (dData : TDateTime) : TDateTime;
begin
  if DayOfWeek(dData) = 7 then
    dData := dData + 1
  else
  if DayOfWeek(dData) = 1 then
    dData := dData + 2;
  Result := dData;
end;

function GetDia(DataIn: TDateTime) : integer;
Var
   dia, mes, ano: Word;
begin
   DecodeDate( DataIn, ano,mes,dia);
   getdia := dia;
end;

function GetMes(DataIn: TDateTime) : integer;
Var
   dia, mes, ano: Word;
begin
   DecodeDate( DataIn, ano,mes,dia);
   getmes := mes;
end;

function GetAno(DataIn: TDateTime) : integer;
Var
   dia, mes, ano: Word;
begin
   DecodeDate( DataIn, ano,mes,dia);
   getano := ano;
end;

function SoNumero(texto : String) : String;
var
   xloop : integer;
   volta : string;
begin
    volta := '';
    for xloop := 1 to length(texto) do
        if texto[ xloop ] in ['0'..'9'] then
           volta := volta + texto[ xloop ];
    Result := volta;
end;

function GetIP:string;
//--> Declare a Winsock na clausula uses da unit
var
  WSAData: TWSAData;
  HostEnt: PHostEnt;
  Name:string;
begin
  WSAStartup(2, WSAData);
  SetLength(Name, 255);
  Gethostname(PChar(Name), 255);
  SetLength(Name, StrLen(PChar(Name)));
  HostEnt := gethostbyname(PChar(Name));
  with HostEnt^  do
     begin
     Result := Format('%d.%d.%d.%d',
               [Byte(h_addr^[0]),Byte(h_addr^[1]),
               Byte(h_addr^[2]),Byte(h_addr^[3])]);
     end;
  WSACleanup;
end;

function GetExeVersion:Integer;
begin
    Result := StrToInt(SoNumero(GetBuildInfo));
end;

function GetBuildInfo:string;
var
VerInfoSize: DWORD;
VerInfo: Pointer;
VerValueSize: DWORD;
VerValue: PVSFixedFileInfo;
Dummy: DWORD;
V1, V2, V3, V4: Word;
Prog : string;
x1, x2, x3, x4: String;
begin
  Prog := Application.Exename;
  VerInfoSize := GetFileVersionInfoSize(PChar(prog), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(prog), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
    begin
      V1 := dwFileVersionMS shr 16;
      V2 := dwFileVersionMS and $FFFF;
      V3 := dwFileVersionLS shr 16;
      V4 := dwFileVersionLS and $FFFF;
    end;
  FreeMem(VerInfo, VerInfoSize);

  result := IntToStr (v1) + '.' +
    IntToStr (v2) + '.' +
    IntToStr (v3) + '.' +
    IntToStr (v4);
end;

function IsFloatField(FieldType : TFieldType):Boolean;
begin
   Result := FieldType in [ftFloat, ftCurrency, ftBCD,ftFMTBcd];
end;

function IsIntegerField(FieldType : TFieldType):Boolean;
begin
   Result := FieldType in [ftSmallint, ftInteger, ftWord,ftAutoInc, ftLargeint];
end;

function IsDateTimeField(FieldType : TFieldType):Boolean;
begin
   Result := FieldType in [ftDate, ftTime, ftDateTime,ftTimeStamp];
end;

function IsNumericField(FieldType : TFieldType):Boolean;
begin
   Result := FieldType in [ftFloat, ftCurrency, ftBCD,ftFMTBcd, ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint];
end;

function IsStringField(FieldType : TFieldType):Boolean;
begin
   Result := FieldType in [ftString,ftFixedChar, ftWideString,ftMemo];
end;

function Coalesce(Value:Variant):Extended;
begin
   if Value = null then begin
      Result := 0;
   end
   else begin
      if not TryStrToFloat(Value,Result) then
         Result := 0;
   end;
end;

function StringToEnum(S : String; EnumType: PTypeInfo ):Integer;
begin
 Result := GetEnumValue(EnumType,S) ;
end;

function FormatDimIB(Valor:Extended):String;
var dec : string;
begin
  dec := '##0.00';
  Result := StringReplace(FormatFloat(dec,Valor),',','.',[]);
end;

function TipodoCampo(tipo : TFieldType) : String;
begin
       case tipo of
           ftUnknown   :   result := 'ftUnknown';
           ftString :      result := 'ftString';
           ftSmallint :    result := 'ftSmallint';
           ftInteger :     result := 'ftInteger';
           ftWord :        result := 'ftWord';
           ftBoolean :     result := 'ftBoolean';
           ftFloat :       result := 'ftFloat';
           ftCurrency:    result := 'ftCurrency';
           ftBCD :         result := 'ftBCD';
           ftDate :        result := 'ftDate';
           ftTime :        result := 'ftTime';
           ftDateTime :    result := 'ftDateTime';
           ftBytes :       result := 'ftBytes';
           ftVarBytes :    result := 'ftVarBytes';
           ftAutoInc :     result := 'ftAutoInc';
           ftBlob :        result := 'ftBlob';
           ftMemo :        result := 'ftMemo';
           ftGraphic :     result := 'ftGraphic';
           ftFmtMemo :     result := 'ftFmtMemo';
           ftParadoxOle :  result := 'ftParadoxOle';
           ftDBaseOle :    result := 'ftDBaseOle';
           ftTypedBinary : result := 'ftTypedBinary';
           ftCursor      : result := 'ftCursor';
       end;

end;

function IsEditableField(Field:TField;ChavePri:String):Boolean;
begin
  Result := (Field.FieldName <> 'APAGADO')     and (Field.FieldName <> 'SENHA') and
            (Field.FieldName <> 'DTALTERACAO') and (Field.FieldName <> 'DTCADASTRO') and
            (Field.FieldName <> 'OPERCADASTRO') and
            (Field.FieldName <> 'DTAPAGADO') and
            (Field.FieldName <> 'LIMITE_PROX_FECHAMENTO') and
            (Field.FieldName <> 'CONV_ID_1') and
            (Field.FieldName <> 'FLAG') and
            (Field.FieldName <> 'OPERADOR')    and (not Field.ReadOnly) and
            (Field.FieldKind = fkData)         and (UpperCase(Field.FieldName) <> UpperCase(ChavePri) );
end;

function ArredondaDin(valor : Currency) : Currency;
begin
    valor := valor * 100;
    valor := round(valor);
    ArredondaDin := valor / 100;
end;

function validarCartao(cartao : string) : Boolean;
begin
  if VerificarCartao2(cartao) then
    result := True
  else
    result := False;
end;

function DigitoControle(Numero : double) : integer;
var
   SNumero : String;
   xloop, inumero, iMulti, digito1, digito2 : Integer;

   Soma : Integer;
begin
     SNumero := FormatFloat('000000000000000', Numero);
     Soma := 0;
     for xloop := 1 to 15 do begin
         inumero := val2( SNumero[ xloop ] );
         iMulti := 17 - xloop;
         Soma := Soma + (INumero * iMulti);
     end;
     digito1 := (Soma mod 10);
     Soma := digito1 * 2;
     for xloop := 1 to 15 do begin
         inumero := val2( SNumero[ xloop ] );
         iMulti := 18 - xloop;
         Soma := Soma + (INumero * iMulti);
     end;
     digito2 := (Soma mod 10);
     //result := Digito2 * 10 + Digito1;
     result :=  Digito1;
end;

function val2(texto : String) : integer;
begin
     try
        Result := strtoint(texto);
     Except
           Result := 0;
     end;
end;

function PadL(Texto : String; Tamanho : Integer; Caracter : Char) : string;
var
   TamTexto : Integer;
begin
     TamTexto := Length(Texto);
     if TamTexto < Tamanho then
        Result := replicate(Caracter, tamanho - TamTexto ) + texto
     else
         if Tamanho < TamTexto then
            Result := copy(Texto, 1, tamanho)
         else
             Result := Texto;
end;

function GetNomeComputador: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;


function IsValidDateTime(StrDate:String):Boolean;
var d : TDateTime;
begin
   Result := TryStrToDate(StrDate,d);
end;

function GridColByFieldName(DBGrid:TDBGrid;  FieldName:String;RaiseException:Boolean=True):TColumn;
var I : integer;
begin
Result := nil;
for i := 0 to pred(DBGrid.Columns.Count) do
    if SameText(DBGrid.Columns[i].FieldName,FieldName) then begin
       Result := DBGrid.Columns[i];
       Exit;
    end;
if RaiseException then
   raise Exception.Create('N�o h� coluna com o fieldname: '+FieldName );
end;

function ChangeSNtoSimNao(S:String):String;
begin
   S := UpperCase(S);
   if ( not (S[1] in ['S','N'])) or (Length(S) > 1) then
      raise Exception.Create('Valor inv�lido, necess�rio N ou S');
   Result := iif(S = 'N','Nao','Sim');
end;

function PrimeiroDiaMes(data: TDateTime): TDateTime;
var ano, mes, dia: Word;
begin
   DecodeDate(data, ano, mes, dia);
   dia := 1;
   Result := EncodeDate(ano, mes, dia);
end;

function MakePropertyValue(ooServer: Variant; PropName:string; PropValue:variant): variant;
begin
  Result := ooServer.Bridge_GetStruct('com.sun.star.beans.PropertyValue');
  Result.Name := PropName;
  Result.Value := PropValue;
end;

function RetornaRange(left, top, right, botton: integer):string;
begin
  result:= '$'+ConvertLetra(left)+'$'+IntToStr(top)+':$'+ConvertLetra(right)+'$'+IntToStr(botton);
end;

function ConvertLetra(num:integer):string;
begin
  case num of
    0: Result:= 'A';
    1: Result:= 'B';
    2: Result:= 'C';
    3: Result:= 'D';
    4: Result:= 'E';
    5: Result:= 'F';
    6: Result:= 'G';
    7: Result:= 'H';
    8: Result:= 'I';
    9: Result:= 'J';
    10: Result:= 'K';
    11: Result:= 'L';
    12: Result:= 'M';
    13: Result:= 'N';
    14: Result:= 'O';
    15: Result:= 'P';
    16: Result:= 'Q';
    17: Result:= 'R';
    18: Result:= 'S';
    19: Result:= 'T';
    20: Result:= 'U';
    21: Result:= 'V';
    22: Result:= 'W';
    23: Result:= 'X';
    24: Result:= 'Y';
    25: Result:= 'Z';
    26: Result:= 'AA';
    27: Result:= 'AB';
    28: Result:= 'AC';
    29: Result:= 'AD';
    30: Result:= 'AE';
    31: Result:= 'AF';
    32: Result:= 'AG';
    33: Result:= 'AH';
    34: Result:= 'AI';
    35: Result:= 'AJ';
    36: Result:= 'AK';
    37: Result:= 'AL';
    38: Result:= 'AM';
    39: Result:= 'AN';
    40: Result:= 'AO';
    41: Result:= 'AP';
    42: Result:= 'AQ';
    43: Result:= 'AR';
    44: Result:= 'AS';
    45: Result:= 'AT';
    46: Result:= 'AU';
    47: Result:= 'AV';
    48: Result:= 'AW';
    49: Result:= 'AX';
    50: Result:= 'AY';
    51: Result:= 'AZ';
    52: Result:= 'BA';
    53: Result:= 'BB';
    54: Result:= 'BC';
    55: Result:= 'BD';
    56: Result:= 'BE';
    57: Result:= 'BF';
    58: Result:= 'BG';
    59: Result:= 'BH';
    60: Result:= 'BI';
    61: Result:= 'BJ';
    62: Result:= 'BK';
    63: Result:= 'BL';
    64: Result:= 'BM';
    65: Result:= 'BN';
    66: Result:= 'BO';
    67: Result:= 'BP';
    68: Result:= 'BQ';
    69: Result:= 'BR';
    70: Result:= 'BS';
    71: Result:= 'BT';
    72: Result:= 'BU';
    73: Result:= 'BV';
    74: Result:= 'BW';
    75: Result:= 'BX';
    76: Result:= 'BY';
    77: Result:= 'BZ';
    78: Result:= 'CA';
    79: Result:= 'CB';
    80: Result:= 'CC';
    81: Result:= 'CD';
    82: Result:= 'CE';
    83: Result:= 'CF';
    84: Result:= 'CG';
    85: Result:= 'CH';
    86: Result:= 'CI';
    87: Result:= 'CJ';
    88: Result:= 'CK';
    89: Result:= 'CL';
    90: Result:= 'CM';
    91: Result:= 'CN';
    92: Result:= 'CO';
    93: Result:= 'CP';
    94: Result:= 'CQ';
    95: Result:= 'CR';
    96: Result:= 'CS';
    97: Result:= 'CT';
    98: Result:= 'CU';
    99: Result:= 'CV';
    100: Result:= 'CW';
    101: Result:= 'CX';
    102: Result:= 'CY';
    103: Result:= 'CZ';
  else
    Result:= 'DA';
  end;
end;

function FileName2URL(FileName: string): string;
begin
  result:= '';
  if LowerCase(copy(FileName,1,8))<>'file:///' then
    result:= 'file:///';
  result:= result + StringReplace(FileName, '\', '/', [rfReplaceAll, rfIgnoreCase]);
end;

function FormatDataHoraIB(Data:TDateTime;ComAspas:Boolean=True):String;
begin
  Result := FormatDateTime('dd/MM/yyyy hh:nn:ss',Data);
  if ComAspas then Result := QuotedStr(Result);
end;




//PROCEDURES//
procedure MsgInf(Txt : String);
begin
  Application.MessageBox(PChar(Txt),'Aten��o...', MB_ICONINFORMATION );
end;

procedure MsgErro(Txt : String);
begin
  Application.MessageBox(PChar(Txt),'Erro...', MB_ICONERROR );
end;

procedure Grade_to_PlanilhaExcel(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False);
var
  Dados : Variant;  Range : String;
  i, linha, ColsCount: integer;
  Excel, WorkSheet: Variant;
  ColsVisible : TList;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
  DataSet : TDataSet;
begin
try
  Excel := CreateOleObject('Excel.Application');
  Excel.Workbooks.Add;
  WorkSheet := Excel.ActiveSheet;
  if Nome <> '' then
     Excel.ActiveSheet.Name := Nome;
except
  ShowMessage('N�o foi possivel abrir o excel!');
  SysUtils.Abort;
end;
try
  Screen.Cursor := crHourGlass;
  DataSet := Grade.DataSource.DataSet;
  if DataSet.IsEmpty then begin
     MsgInf('N�o h� dados para sem exportados');
     Exit;
  end;
  ColsVisible := TList.Create;
  for i := 0 to Grade.Columns.Count-1 do begin
      if Grade.Columns[i].Visible then
         ColsVisible.Add(Grade.Columns[i]);
  end;
  ColsCount := ColsVisible.Count-1;
  Dados := VarArrayCreate([0, DataSet.RecordCount,0,ColsVisible.Count], varVariant);
  if MostrarTitulos then begin
     for i := 0 to ColsCount do begin
         Dados[0,i] := TColumn(ColsVisible[i]).Title.Caption;
     end;
  end;

  AfterScroll  := DataSet.AfterScroll;
  BeforeScroll := DataSet.BeforeScroll;
  DataSet.AfterScroll  := nil;
  DataSet.BeforeScroll := nil;
  DataSet.First;
  while not DataSet.Eof do begin
     for i :=0 to ColsCount do begin
        try
          if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
             Dados[DataSet.RecNo,i] := RoundTo(TColumn(ColsVisible[i]).Field.AsCurrency,-2)
          else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
             Dados[DataSet.RecNo,i] := TColumn(ColsVisible[i]).Field.AsDateTime
          else
             Dados[DataSet.RecNo,i] := TColumn(ColsVisible[i]).Field.DisplayText;
        except end;
     end;
     Grade.DataSource.DataSet.Next;
  end;


  for i :=0 to ColsCount do begin
      Range := WorkSheet.Cells[2, i+1].AddressLocal+':'+
               WorkSheet.Cells[DataSet.Recno, i+1].AddressLocal;
      if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then begin
         WorkSheet.Range[Range].NumberFormat := '#.##0,00';
      end
      else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then begin
         WorkSheet.Range[Range].HorizontalAlignment := xlRight;
      end
      else if IsStringField(TColumn(ColsVisible[i]).Field.DataType) then
         WorkSheet.Range[Range].NumberFormat := '@';
  end;

  Range := 'A1:'+WorkSheet.Cells[DataSet.RecordCount+1, ColsCount+1].AddressLocal;
  WorkSheet.Range[Range].value := dados;

  WorkSheet.Columns.Autofit;

  DataSet.AfterScroll  := AfterScroll;
  DataSet.BeforeScroll := BeforeScroll;
  DataSet.First;

  if SalvarComo <> '' then
     Excel.ActiveSheet.SaveAs(SalvarComo);

  if AbrirExcel then
  begin
    Excel.Visible := True;
  end
  else
  begin
    Excel.Quit;
  end;

  Screen.Cursor := crDefault;
except on E:Exception do  begin
     DataSet.AfterScroll  := AfterScroll;
     DataSet.BeforeScroll := BeforeScroll;
     DataSet.First;
     Screen.Cursor := crDefault;
     ShowMessage('Ocorreu um problema durande a gera��o do arquivo.'+#13+'Erro: '+E.Message);
  end;
end;
end;

procedure SortZQuery(Query:TDataset;FieldName:string);
var Fields : String;
begin
   Fields := GetPropValue(Query,'Sort',True);
   if Pos(FieldName,Fields) > 0 then begin
      if AnsiContainsText(Fields,' DESC') then
         SetPropValue(Query,'Sort',FieldName)
      else
         SetPropValue(Query,'Sort',FieldName+ ' DESC');
   end
   else begin
      SetPropValue(Query,'Sort',FieldName)
   end;
end;


Procedure Grade_to_PlanilhaMarcado(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False;SoMarcados:Boolean=False);
var
  Dados : Variant;  Range: String;
  i, linha, ColsCount: integer;
  Excel, WorkSheet: Variant;
  ColsVisible : TList;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
  DataSet : TDataSet;
begin
  try
    linha:= 0;
    Excel := CreateOleObject('Excel.Application');
    Excel.Workbooks.Add;
    WorkSheet := Excel.ActiveSheet;
    if Nome <> '' then
    begin
       Excel.ActiveSheet.Name := Nome;
    end;
  except
    //ShowMessage('N�o foi possivel abrir o excel!');
    Grade_to_PlanilhaCalcMarcado(Grade,Nome,MostrarTitulos,SalvarComo,True,SoMarcados);
//    SysUtils.Abort;
    Exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    DataSet := Grade.DataSource.DataSet;
    if DataSet.IsEmpty then
    begin
      MsgInf('N�o h� dados para sem exportados');
      Exit;
    end;
    ColsVisible := TList.Create;
    for i := 0 to Grade.Columns.Count-1 do
    begin
      if Grade.Columns[i].Visible then
      begin
        ColsVisible.Add(Grade.Columns[i]);
      end;
    end;
    ColsCount := ColsVisible.Count-1;
    Dados := VarArrayCreate([0, DataSet.RecordCount,0,ColsVisible.Count], varVariant);
    if MostrarTitulos then
    begin
      for i := 0 to ColsCount do
      begin
        Dados[0,i] := TColumn(ColsVisible[i]).Title.Caption;
      end;
    end;

    AfterScroll  := DataSet.AfterScroll;
    BeforeScroll := DataSet.BeforeScroll;
    DataSet.AfterScroll  := nil;
    DataSet.BeforeScroll := nil;
    DataSet.First;
    while not DataSet.Eof do
    begin
      if DataSet.FieldByName('MARCADO').AsString = 'S' then
      begin
        linha:= linha + 1;
        for i :=0 to ColsCount do
        begin
          try
            if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
            begin
              Dados[linha,i] := RoundTo(TColumn(ColsVisible[i]).Field.AsCurrency,-2);
            end
            else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
            begin
              Dados[linha,i] := TColumn(ColsVisible[i]).Field.AsDateTime;
            end
            else
            begin
              Dados[linha,i] := TColumn(ColsVisible[i]).Field.DisplayText;
            end;
          except

          end;
        end;
      end;
      Grade.DataSource.DataSet.Next;
    end;
    for i :=0 to ColsCount do
    begin
      Range := WorkSheet.Cells[2, i+1].AddressLocal+':'+
        WorkSheet.Cells[DataSet.RecordCount+1, i+1].AddressLocal;
      if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        WorkSheet.Range[Range].NumberFormat := '#.##0,00';
      end
      else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        WorkSheet.Range[Range].HorizontalAlignment := xlRight;
      end
      else if IsStringField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        WorkSheet.Range[Range].NumberFormat := '@';
      end;
    end;

    Range := 'A1:'+WorkSheet.Cells[DataSet.RecordCount+1, ColsCount+1].AddressLocal;
    WorkSheet.Range[Range].value := dados;

    WorkSheet.Columns.Autofit;

    DataSet.AfterScroll  := AfterScroll;
    DataSet.BeforeScroll := BeforeScroll;
    DataSet.First;

    if SalvarComo <> '' then
    begin
       if pos('"',copy(SalvarComo,1,1))>0 then
          SalvarComo:= PChar(SalvarComo);
       if pos('.',SalvarComo) = 0 then
          SalvarComo:= SalvarComo + '.xls';
       Excel.ActiveSheet.SaveAs(SalvarComo, 56);
    end;

    if AbrirExcel then
    begin
      Excel.Visible := True;
    end
    else
    begin
      Excel.Quit;
    end;

    Screen.Cursor := crDefault;
    except on E:Exception do
    begin
      DataSet.AfterScroll  := AfterScroll;
      DataSet.BeforeScroll := BeforeScroll;
      DataSet.First;
      Screen.Cursor := crDefault;
      ShowMessage('Ocorreu um problema durande a gera��o do arquivo.'+#13+'Erro: '+E.Message);
    end;
  end;
end;


Procedure Grade_to_PlanilhaCalcMarcado(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='';AbrirExcel:Boolean=False;SoMarcados:Boolean=False);
var
  OOoServer, OOoDesktop, VariantArr, OOoDocument: variant;
  Column, Sheets, Sheet, Range, Dados, args: variant;
  Linha : array of Variant;
  i, l, QtdLinha, DataNum, ColsCount: integer;
  TargetCell, caminho : String;
  ColsVisible : TList;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
  DataSet : TDataSet;
begin
  try
    OOoServer := CreateOleObject('com.sun.star.ServiceManager');
    OOoDesktop := OOoServer.createInstance('com.sun.star.frame.Desktop');
    VariantArr := VarArrayCreate([0, 2], varVariant);
    VariantArr[0] := MakePropertyValue(OOoServer, 'Hidden', true);
    VariantArr[1] := MakePropertyValue(OOoServer, 'Overwrite', True);
    VariantArr[2] := MakePropertyValue(OOoServer, 'OptimalWidth', True);
    OOoDocument := OOoDesktop.LoadComponentFromURL('private:factory/scalc', '_blank', 0, VariantArr);

    Sheets := OOoDocument.Sheets;
    Sheet := Sheets.getByIndex(0);

    if Nome <> '' then
    begin
      args:= VarArrayCreate([0, 0], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'Name',Nome);
      ooDispatch(OOoServer, OOoDocument, '.uno:RenameTable',args);
    end
  except
    ShowMessage('N�o foi possivel abrir o excel ou calc!');
    SysUtils.Abort;
  end;
  try
    Screen.Cursor := crHourGlass;
    DataSet := Grade.DataSource.DataSet;
    if DataSet.IsEmpty then
    begin
      MsgInf('N�o h� dados para sem exportados');
      Exit;
    end;
    ColsVisible := TList.Create;
    for i := 0 to Grade.Columns.Count-1 do
    begin
      if Grade.Columns[i].Visible then
      begin
        ColsVisible.Add(Grade.Columns[i]);
      end;
    end;
    ColsCount := ColsVisible.Count-1;
    SetLength(Linha, ColsCount+1);
    QtdLinha:= 0;
    DataSet.First;
    while not DataSet.Eof do
    begin
      if DataSet.FieldByName('MARCADO').AsString = 'S' then
        QtdLinha:= QtdLinha + 1;
      DataSet.Next;
    end;
    Dados := VarArrayCreate([iif(MostrarTitulos,0,1),QtdLinha], varVariant);
    if MostrarTitulos then
    begin
      for i := 0 to ColsCount do
      begin
        Linha[i] := TColumn(ColsVisible[i]).Title.Caption;
      end;
      Dados[0] := Linha;
    end;
    AfterScroll  := DataSet.AfterScroll;
    BeforeScroll := DataSet.BeforeScroll;
    DataSet.AfterScroll  := nil;
    DataSet.BeforeScroll := nil;
    l:= 0;
    DataSet.First;
    while not DataSet.Eof do
    begin
      if DataSet.FieldByName('MARCADO').AsString = 'S' then
      begin
        l:= l + 1;
        for i :=0 to ColsCount do
        begin
          try
            if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
            begin
              Linha[i] := RoundTo(TColumn(ColsVisible[i]).Field.AsFloat,-2);
            end
            else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
            begin
              if TColumn(ColsVisible[i]).Field.IsNull then
              begin
                Linha[i]:= '';
              end
              else
              begin
                DataNum:= Trunc(TColumn(ColsVisible[i]).Field.AsDateTime);
                Linha[i] := DataNum;
              end;
            end
            else if not TColumn(ColsVisible[i]).Field.IsNull then
            begin
              Linha[i]:= TColumn(ColsVisible[i]).Field.AsVariant;
            end
            else
            begin
              Linha[i]:= '';
            end;
          except
          end;
        end;
        Dados[l] := Linha;
      end;
      Grade.DataSource.DataSet.Next;
    end;
    for i :=0 to ColsCount do
    begin
      args:= VarArrayCreate([0, 0], varVariant);
      if MostrarTitulos then
      begin
        TargetCell:= RetornaRange(i,1,i,DataSet.RecordCount+1);
      end
      else
      begin
        TargetCell:= RetornaRange(i,0,i,QtdLinha);
      end;
      if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',36);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end
      else if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',4);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end
      else if IsStringField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',100);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end;
    end;
    args[0]:= MakePropertyValue(OOoServer, 'ToPoint','$A$1');
    ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
    Range := sheet.getCellRangeByPosition(0{esquerda},0{topo},ColsCount{direita},iif(MostrarTitulos,QtdLinha,QtdLinha-1){rodape});
    Range.setDataArray(dados);
    Column:= OOoDocument.GetCurrentController.GetActiveSheet.GetColumns;
    Column.OptimalWidth:=True;
    if SalvarComo <> '' then
    begin
      SalvarComo:= PChar(SalvarComo);
      caminho:= FileName2URL(SalvarComo);
      if pos('.xls',caminho)>0 then
         caminho:= copy(caminho,1,pos('.xls',caminho)-1);
      args:= VarArrayCreate([0, 1], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'URL',caminho+'.xls');
      args[1]:= MakePropertyValue(OOoServer, 'FilterName','MS Excel 97');
      ooDispatch(OOoServer, OOoDocument, '.uno:SaveAs',args);
    end
    else
    begin
      caminho:= FileName2URL(SalvarComo);
      args:= VarArrayCreate([0, 0], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'FilterName','MS Excel 97');
      ooDispatch(OOoServer, OOoDocument, '.uno:SaveAs',args);
    end;
    DataSet.AfterScroll  := AfterScroll;
    DataSet.BeforeScroll := BeforeScroll;
    DataSet.First;
    Screen.Cursor := crDefault;
    if AbrirExcel then
    begin
      OOoDocument.getCurrentController.getFrame.getContainerWindow.setVisible(true);
    end
    else
    begin
      OOoDocument.Dispose;
      OOoDocument:= Null;
    end;
    OOoDesktop:= Unassigned;
    OOoServer:= Unassigned;
  except on E:Exception
    do
    begin
      DataSet.AfterScroll  := AfterScroll;
      DataSet.BeforeScroll := BeforeScroll;
      DataSet.First;
      Screen.Cursor := crDefault;
      ShowMessage('Ocorreu um problema durande a gera��o do arquivo.'+#13+'Erro: '+E.Message);
    end;
  end;
end;

procedure ooDispatch(ooServer, ooDocument: Variant; ooCommand: string; ooParams: variant);
var
  ooDispatcher, ooFrame: variant;
begin
  if (VarIsEmpty(ooParams) or VarIsNull(ooParams)) then
  begin
    ooParams:= VarArrayCreate([0, -1], varVariant);
  end;
  ooFrame:= ooDocument.getCurrentController.getFrame;
  ooDispatcher:= ooServer.createInstance('com.sun.star.frame.DispatchHelper');
  ooDispatcher.executeDispatch(ooFrame, ooCommand, '', 0, ooParams );
end;

function extenso (valor: real): string;
var
Centavos, Centena, Milhar, Milhao, Texto, msg: string;
const
Unidades: array[1..9] of string = ('Um', 'Dois', 'Tres', 'Quatro', 'Cinco', 'Seis', 'Sete', 'Oito', 'Nove');
Dez: array[1..9] of string = ('Onze', 'Doze', 'Treze', 'Quatorze', 'Quinze', 'Dezesseis', 'Dezessete', 'Dezoito', 'Dezenove');
Dezenas: array[1..9] of string = ('Dez', 'Vinte', 'Trinta', 'Quarenta', 'Cinquenta', 'Sessenta', 'Setenta', 'Oitenta', 'Noventa');
Centenas: array[1..9] of string = ('Cento', 'Duzentos', 'Trezentos', 'Quatrocentos', 'Quinhentos', 'Seiscentos', 'Setecentos', 'Oitocentos', 'Novecentos');
function ifs(Expressao: Boolean; CasoVerdadeiro, CasoFalso: String): String;
begin
if Expressao
then Result:=CasoVerdadeiro
else Result:=CasoFalso;
end;

function MiniExtenso (trio: string): string;
var
Unidade, Dezena, Centena: string;
begin
Unidade:='';
Dezena:='';
Centena:='';
if (trio[2]='1') and (trio[3]<>'0') then
  begin
  Unidade:=Dez[strtoint(trio[3])];
  Dezena:='';
end
else
 begin
  if trio[2]<>'0' then Dezena:=Dezenas[strtoint(trio[2])];
  if trio[3]<>'0' then Unidade:=Unidades[strtoint(trio[3])];
 end;
if (trio[1]='1') and (Unidade='') and (Dezena='')
 then Centena:='Cem'
else
 if trio[1]<>'0'
  then Centena:=Centenas[strtoint(trio[1])]
  else Centena:='';
 Result:= Centena + ifs((Centena<>'') and ((Dezena<>'') or (Unidade<>'')), ' e ', '')
  + Dezena + ifs((Dezena<>'') and (Unidade<>''),' e ', '') + Unidade;
end;
begin
if (valor>999999.99) or (valor<0) then
 begin
  msg:='O valor est� fora do intervalo permitido.';
  msg:=msg+'O n�mero deve ser maior ou igual a zero e menor que 999.999,99.';
  msg:=msg+' Se n�o for corrigido o n�mero n�o ser� escrito por extenso.';
  showmessage(msg);
  Result:='';
  exit;
 end;
if valor=0 then
 begin
  Result:='';
  Exit;
 end;
Texto:=formatfloat('000000.00',valor);
Milhar:=MiniExtenso(Copy(Texto,1,3));
Centena:=MiniExtenso(Copy(Texto,4,3));
Centavos:=MiniExtenso('0'+Copy(Texto,8,2));
Result:=Milhar;
if Milhar<>'' then
  if copy(texto,4,3)='000' then
  Result:=Result+' Mil Reais'
  else
  Result:=Result+' Mil, ';
if (((copy(texto,4,2)='00') and (Milhar<>'')
  and (copy(texto,6,1)<>'0')) or (centavos=''))
  and ((Centena<>'') and (Milhar<>'')) then Result:=Result+' e ';
if (Milhar+Centena <>'') then Result:=Result+Centena;
if (Milhar='') and (copy(texto,4,3)='001') then
  Result:=Result+' Real'
 else
  if (copy(texto,4,3)<>'000') then Result:=Result+' Reais';
if Centavos='' then
 begin
  Result:=Result+'.';
  Exit;
 end
else
 begin
  if Milhar+Centena='' then
  Result:=Centavos
  else
  Result:=Result+', e '+Centavos;
if (copy(texto,8,2)='01') and (Centavos<>'') then
  Result:=Result+' Centavo.'
 else
  Result:=Result+' Centavos.';
end;
end;

//formata data para dd/mm/yyyy ja que consulta do sql retorna yyyy-mm-dd
function FormataDataSql(mcad : string):string;
var
ts1,ts2,ts3,tsdat : string ;
begin
  ts1:= copy(mcad,9,2) ;
  ts2:= copy(mcad,6,2);
  ts3:= copy(mcad,1,4);
  tsdat:= ts1+'/'+ts2+'/'+ts3;
  result:= tsdat;
end;

function JaEmitidoCartaoNovo(cartaoID : String) : Boolean;
var
  jaEmitidoCartaoNovo : string;
begin
   jaEmitidoCartaoNovo := DMConexao.ExecuteQuery('SELECT coalesce(JAEMITIDO_CARTAO_NOVO,''N'') JAEMITIDO_CARTAO_NOVO  FROM CARTOES WHERE CARTAO_ID = '+ cartaoID);
   if(jaEmitidoCartaoNovo = 'S') then
    result := true
   else
    result := false;
end;

function GerarCVV(codCartImp : String) : String;
var
i,j,contPar,contImpar,somatorioPares,somatorioImpares,somatorioTodosOsNumeros : Integer;
charPar,charImpar,charCartao,cvv : String;
primeiroDigitoCvv, segundoDigitoCvv, terceiroDigitoCvv : String;
caracteresImpares,caracteresPares : array of String;
codCartImpLength : Integer;
begin
  codCartImpLength := length(codCartImp);
  SetLength(caracteresImpares,8);
  SetLength(caracteresPares,8);
  contPar := 0;
  contImpar := 0;
  somatorioPares := 0;
  somatorioImpares := 0;
  somatorioTodosOsNumeros := 0;
  for i := 1 to codCartImpLength do
  begin
    if i mod 2 = 0 then
    begin
      caracteresPares[contPar] := codCartImp[i];
      contPar := contPar + 1;
    end
    else
    begin
      caracteresImpares[contImpar] := codCartImp[i];
      contImpar := contImpar + 1;
    end;
  end;
   j := 0;
  //soma da multiplica��o de todos os pares por 2
  for j := 0 to (length(caracteresPares) - 1) do
  begin
    charPar := caracteresPares[j];
    somatorioPares := somatorioPares + (StrToInt(charPar));
    //MsgInf(caracteresPares[j]);
  end;

  j := 0;
  //soma de todos �mpares
  for j := 0 to (length(caracteresImpares) - 1) do
  begin
    charImpar := caracteresPares[j];
    somatorioImpares := somatorioImpares + (StrToInt(charImpar) * 2);
    //MsgInf(caracteresPares[j]);
  end;

  j := 1;
  //somatorio de todos os numeros do cartao
  for j := 1 to (codCartImpLength) do
  begin
    charCartao := codCartImp[j];
    somatorioTodosOsNumeros := somatorioTodosOsNumeros + (StrToInt(charCartao));
  end;

  if(length(IntToStr(somatorioPares)) < 2 )  then
  primeiroDigitoCvv := copy(IntToStr(somatorioPares),1,1)

  else if (copy(IntToStr(somatorioPares),2,1) = '0') then

    primeiroDigitoCvv := copy(IntToStr(somatorioPares),1,1)

  else
  primeiroDigitoCvv := copy(IntToStr(somatorioPares),2,1);

  segundoDigitoCvv := copy(IntToStr(somatorioImpares),2,1);

  terceiroDigitoCvv := copy(IntToStr(somatorioTodosOsNumeros),2,1);

    //MsgInf(IntToStr(somatorioPares));
    //MsgInf(IntToStr(somatorioImpares));
    //MsgInf(IntToStr(somatorioTodosOsNumeros));

  cvv := primeiroDigitoCvv + segundoDigitoCvv + terceiroDigitoCvv;
  result := cvv;

end;




end.

