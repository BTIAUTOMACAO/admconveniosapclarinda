object FManutencaoEmpConv2: TFManutencaoEmpConv2
  Left = 312
  Top = 174
  Width = 560
  Height = 404
  Caption = 'Formul'#225'rio de Transf. de Comveniados / Nova Empresa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 552
    Height = 373
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 550
      Height = 121
      Align = alTop
      TabOrder = 0
      DesignSize = (
        550
        121)
      object Label1: TLabel
        Left = 7
        Top = 8
        Width = 522
        Height = 16
        Caption = 'TELA PARA TRANFER'#202'NCIA DE CONVENIADOS PARA OUTRA EMPRESA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ButAjuda: TSpeedButton
        Left = 515
        Top = 95
        Width = 33
        Height = 22
        Hint = 'Ajuda'
        Flat = True
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
          5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
          DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
          CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
          E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
          A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
          2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
          D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
          3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
          F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
          3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
          FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
          3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
          FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
          3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
          FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
          5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
          FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
          FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
          FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
          FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
          E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
          BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
          C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
          7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
          D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
          E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
          FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
          FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
          D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        OnClick = ButAjudaClick
      end
      object btnImportarLimiteConv: TBitBtn
        Left = 203
        Top = 36
        Width = 158
        Height = 34
        Hint = 'Importar'
        Anchors = [akLeft]
        Caption = 'Importa'#231#227'o de Planilha'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = btnImportarLimiteConvClick
        NumGlyphs = 2
      end
      object RichEdit1: TRichEdit
        Left = 4
        Top = 80
        Width = 53
        Height = 25
        Lines.Strings = (
          'Importa'#231#227'o de Conveniados para limpar senha do cart'#227'o'
          ''
          '   Para realizar a importa'#231#227'o de um arquivo dos conveniados, '
          #233' necess'#225'rio que o arquivo seja do tipo (.xls) Excel 97-2003 e'
          'esteja no formato esperado pelo sistema.'
          ''
          
            'Favor usar a Planilha "Importa'#231#227'o de Conveniados para limpar sen' +
            'ha do cart'#227'o.xlsx" na pasta ADMConv'#234'nio_Manuais para importar os' +
            ' conveniados.'
          ''
          
            '   Obs.: Caso tenha algum erro na importa'#231#227'o ser'#225' informado na t' +
            'ela'
          ''
          '   Segue abaixo o formato para o arquivo.'
          ''
          
            '   Campo Chapa obrigat'#243'rio contendo a identifica'#231#227'o do conveniad' +
            'o.'
          '   (Obs: Campo do tipo num'#233'rico sem decimais).'
          ''
          '   Campo NOME contendo o nome completo do conveniado.'
          ''
          '   Aten'#231#227'o: O nome da planilha deve ser CONV.'
          '   Para o nome do arquivo '#233' sugerido que n'#227'o contenha espa'#231'os.'
          ''
          
            '   Exemplo do formato da planilha, onde todos campos para atuali' +
            'za'#231#227'o'
          'foram selecionados:'
          ''
          'CHAPA |'#9'EMPRESA ID | NOVA EMPRESA ID'
          '--------|---------------|---------------------|'
          '37737  | '#9'2169            | 1550   '
          '--------|---------------|---------------------|'
          '37745  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          '37736  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          '37738  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          '37743  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          '37744  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          '37746  |'#9'2169            | 1550'
          '--------|---------------|---------------------|'
          ''
          ''
          ''
          ''
          '_______\conv/_________'
          ''
          ''
          ''
          
            'Para maiores informa'#231#245'es acesse a pasta "ADMConv'#234'nio - Manuais" ' +
            'dentro do "pool" '
          
            'atrav'#233's do caminho \\servidor\pool\ADMConv'#234'nio_Manuais e abra o ' +
            'arquivo Importa'#231#227'o_Conveniados_Transferencia_Outra_Empresa.pdf')
        TabOrder = 1
        Visible = False
        WordWrap = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 122
      Width = 550
      Height = 250
      Align = alClient
      Caption = 'Transfer'#234'ncia de Autoriza'#231#245'es'
      TabOrder = 1
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 93
        Height = 13
        Caption = 'CONV_ID ORIGEM'
      end
      object Label3: TLabel
        Left = 16
        Top = 104
        Width = 98
        Height = 13
        Caption = 'CONV_ID DESTINO'
      end
      object Label4: TLabel
        Left = 16
        Top = 160
        Width = 210
        Height = 13
        Caption = 'AUTORs IDs - SEPARAR POR V'#205'RGULA (,)'
      end
      object txtConvIDOrigem: TEdit
        Left = 16
        Top = 72
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object txtConvIDDestino: TEdit
        Left = 16
        Top = 120
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object Edit3: TEdit
        Left = 16
        Top = 176
        Width = 513
        Height = 21
        TabOrder = 2
      end
      object btnTransferir: TBitBtn
        Left = 224
        Top = 210
        Width = 145
        Height = 33
        Caption = 'TRANSFERIR AUTOs'
        TabOrder = 3
        OnClick = btnTransferirClick
      end
    end
  end
  object tExcel: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 792
    Top = 440
  end
  object ConvOrigem: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 264
    Top = 192
  end
  object ConvDestino: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 320
    Top = 192
  end
end
