unit qRelSimples;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls, printers,
    CheckLst, StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, dialogs, math;

type
  TqrRelSimples = class(TQuickRep)
    QRBand1: TQRBand;
    DetailBand1: TQRBand;
    lblTitulo: TQRLabel;
    QRGroup1: TQRGroup;
    QRChildBand1: TQRChildBand;
    QRSubDetail1: TQRSubDetail;
    FooterFilho: TQRBand;
    QRBand2: TQRBand;
    lblPag: TQRSysData;
    lblTotalRegistros: TQRLabel;
    qrBandRodape: TQRBand;
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QuickRepAfterPreview(Sender: TObject);
    procedure QRSubDetail1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    teste, HPai, HFilho : Integer;
    ExisteMarcadoPai, ExisteMarcadoFilho : Boolean;
    function ValidaValores(Valor : String) : Boolean;
    function Soma(Valor : String ; Tag : Integer) : Variant;
    function Media(Valor : String ; Tag, TotalRegistros : Integer) : Extended;
    function Maximo(Valor : String ; Tag : Integer) : Variant;
    function Minimo(Valor : String ; Tag : Integer) : Variant;
  public
    procedure FormatarValores(Field : TField ; qrText : TQRDBText ; qrLabel : TQRLabel);
    procedure MontaRelSimples(var DSet : TDataSet ; TotalRegistros : Boolean = False ; Titulo : String = 'Relat�rio B�sico' ; NmFonte : String = '' ; TamFonte : Integer = 7; AlinhamentoTitulo : TAlignment = taLeftJustify ; CorLnPar : TColor = clWhite; CorLnImpar : TColor = $00E3DFE0);
    procedure MontaRelSimplesComFilho(var DSetPai, DSetFilho : TDataSet ; TotalRegistros : Boolean = False ; Titulo : String = 'Relat�rio B�sico' ; NmFonte : String = '' ; TamFonte : Integer = 7; AlinhamentoTitulo : TAlignment = taLeftJustify ; CorLnPar : TColor = clWhite; CorLnImpar : TColor = $00E3DFE0);
  end;

var
  qrRelSimples: TqrRelSimples;

implementation

uses UNewGeraLista, URotinasGrids;

{$R *.DFM}

{ TqrRelSimples }
var
  CorPaiLnPar, CorPaiLnImpar : TColor;
  contadorPai  : Integer = 0;
  contadorFilho: Integer = 0;
  TotalRegistrosPai   : Integer;
  TotalRegistrosFilho : Integer;

function TqrRelSimples.ValidaValores(Valor : String) : Boolean;
var I : Integer;
begin
  Result := True;
  for I:= 1 to Length(Valor) do
    if not(Valor[I] in ['0'..'9',DecimalSeparator]) then begin
      Result := False;
      Break;
    end;
end;

function TqrRelSimples.Soma(Valor: String ; Tag : Integer): Variant;
var F : Double;
begin
  try
    F := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsFloat;
    if teste = 105 then
      teste := teste + 1;
    if not ValidaValores(Valor) then begin Result := ''; Exit; end;
    if (Valor = '') and (Trim(TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) <> '') then
      Valor := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString;
    // se eu encontrar o separador de casa decimal � pq ele eh float...
    //if Pos(DecimalSeparator,TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) > 0 then
    if Pos(DecimalSeparator,Valor) > 0 then
      Result := StrTofloat(Valor) + F //Pego o componente pelo Indice e somo com o valor q ele ta o meu valor atual do rodap�!
    else
      Result := StrToInt(Valor) + TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsInteger; //Pego o componente pelo Indice e somo com o valor q ele ta o meu valor atual do rodap�!
  except
  end;
end;

function TqrRelSimples.Media(Valor: String ; Tag,TotalRegistros : Integer): Extended;
begin
  try
    if not ValidaValores(Valor) then begin Result := 0.00; Exit; end;
    if (Valor = '') and (Trim(TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) <> '') then
      Valor := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString;
    if Pos(',',TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) > 0 then
      Result := RoundTo(StrToFloat(Valor) / TotalRegistros,-2)
    else
      Result := Round(RoundTo(StrToFloat(Valor) / TotalRegistros,-2)); //Pego o componente pelo Indice e somo com o valor q ele ta o meu valor atual do rodap�!
  except
  end;
end;

function TqrRelSimples.Maximo(Valor: String ; Tag : Integer): Variant;
begin
  try
    if not ValidaValores(Valor) then begin Result := 0; Exit; end;
    if (Valor = '') and (Trim(TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) <> '') then
      Valor := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString;
    if Pos(DecimalSeparator,TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) > 0 then begin // se eu encontrar o separador de casa decimal � pq ele eh float...
      if StrToFloat(Valor) < TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsFloat then
        Result := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsFloat
      else
        Result := StrToInt(Valor);
    end else
      if StrToInt(Valor) < TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsInteger then
        Result := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsInteger
      else
        Result := StrToInt(Valor);
  except
    Result := StrToInt(Valor);
  end;
end;

function TqrRelSimples.Minimo(Valor: String ; Tag : Integer): Variant;
begin
  try
    if not ValidaValores(Valor) then begin Result := 0; Exit; end;
    if (Valor = '') and (Trim(TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) <> '') then
      Valor := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString;
    if Pos(DecimalSeparator,TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsString) > 0 then begin // se eu encontrar o separador de casa decimal � pq ele eh float...
      if StrToFloat(Valor) > TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsFloat then
        Result := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsFloat
      else
        Result := StrToInt(Valor);
    end else
      if StrToInt(Valor) > TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsInteger then
        Result := TQRDBText(Components[Tag]).DataSet.FieldByName(TQRDBText(Components[Tag]).DataField).AsInteger
      else
        Result := StrToInt(Valor);
  except
    Result := StrToInt(Valor);
  end;
//  if Result = 1 then
//    showmessage(inttostr(Result)+' -- '+inttostr(contadorPai));
end;

procedure TqrRelSimples.FormatarValores(Field : TField ; qrText : TQRDBText ; qrLabel : TQRLabel);
begin
  if (Field is TIntegerField) or (Field is TFloatField) then begin
    qrText.Alignment := taRightJustify;
    qrLabel.Alignment := taRightJustify;
  if Field.Value < 0 then
    qrText.Font.Color := clRed
  else
    qrText.Font.Color := clBlue;
  end;
  if Field is TDateField then
  //  qrText.Alignment := taCenter;
end;

procedure TqrRelSimples.MontaRelSimples(var DSet : TDataSet ; TotalRegistros : Boolean ; Titulo, NmFonte : String ; TamFonte : Integer ; AlinhamentoTitulo : TAlignment; CorLnPar, CorLnImpar : TColor);
var TamCelW, C,I,X,W : Integer;
    lbl,lblRod : array of TQRLabel;
    txt : array of TQRDBText;
begin
  X := 0;
  W := 0;
  qrRelSimples.DataSet := DSet;
  TotalRegistrosPai := DSet.RecordCount;
  CorPaiLnPar   := CorLnPar;
  CorPaiLnImpar := CorLnImpar;
  ExisteMarcadoPai := DSet.FindField('MARCADO') <> nil;
  for I := 0 to DSet.Fields.Count -1 do begin
    if DSet.Fields[I].Visible then begin
      setLength(lbl,DSet.Fields.Count);
      setLength(txt,DSet.Fields.Count);
    //CRIANDO O CABECALHO DE CADA UM....
      lbl[I] := TQRLabel.Create(Self);
      lbl[I].Parent := QRGroup1;
      lbl[I].Name := 'lbl'+DSet.Fields[I].Name;
      lbl[I].Caption := DSet.Fields[I].DisplayName;
      lbl[I].AutoSize := False;
      TamCelW := TamanhoCelulaEmPixel(DSet.Fields[I].DisplayWidth,NmFonte,TamFonte);
      lbl[I].Width := TamCelW;
      lbl[I].Top := 0;
      lbl[I].Left := X;
      lbl[I].Font.Size := TamFonte+1;
      lbl[I].Font.Style := [fsBold];
      lbl[I].Font.Name := NmFonte;

    //CRIANDO O REGISTROS DE CADA UM....
      txt[I] := TQRDBText.Create(Self);
      txt[I].Parent := DetailBand1;
      txt[I].Name := 'txt'+ DSet.Fields[I].Name;
      txt[I].DataSet := DSet;
      txt[I].DataField := DSet.Fields[I].FieldName;
      txt[I].Caption := '';
      txt[I].Top := 0;
      txt[I].Left := X;
      //txt[I].Color := clLime;
      txt[I].Font.Size := TamFonte;
      txt[I].AutoSize := False;
      txt[I].Width := TamCelW;
      txt[I].Font.Name := NmFonte;
      W := W + TamCelW + 5;
      txt[I].Transparent := True;
//      txt[I].OnPrint := DbLblMarcado.OnPrint;
      FormatarValores(DSet.Fields[I],txt[I],lbl[I]);
      //Criando o rodap� se o usu�rio quer q ele tenha o rodap�!
      if DSet.Fields[I].Tag <> 0 then begin
        setLength(lblRod,Length(lblRod)+2);
        C := Length(lblRod)-2;
        lblRod[C]   := TQRLabel.Create(Self);
        lblRod[C+1] := TQRLabel.Create(Self);
        lblRod[C].Parent   := qrBandRodape;
        lblRod[C+1].Parent := qrBandRodape;
        if (DSet.Fields[I] is TIntegerField) or  (DSet.Fields[I] is TFloatField) then begin
          lblRod[C].Alignment   := taRightJustify;
          lblRod[C+1].Alignment := taRightJustify;
        end;
        case DSet.Fields[I].Tag of
          1 : begin lblRod[C+1].Caption := 'Soma'; lblRod[C].Name := 'lblSoma' + DSet.Fields[I].Name; end;
          2 : begin lblRod[C+1].Caption := 'M�d.'; lblRod[C].Name := 'lblMedia'+ DSet.Fields[I].Name; end;
          3 : begin lblRod[C+1].Caption := 'M�x.'; lblRod[C].Name := 'lblMax'  + DSet.Fields[I].Name; end;
          4 : begin lblRod[C+1].Caption := 'M�n.'; lblRod[C].Name := 'lblMin'  + DSet.Fields[I].Name; end;
        end;
        //lblRod[C].AutoSize := True;
        lblRod[C].Width   := TamCelW;
        lblRod[C+1].Width := TamCelW;
        lblRod[C].Tag := txt[I].ComponentIndex;
        lblRod[C].Left   := X;
        lblRod[C+1].Left := X;
        lblRod[C].Top   := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0) + 2;
        lblRod[C+1].Top := 0;
        lblRod[C].Font.Size   := TamFonte;
        lblRod[C+1].Font.Size := TamFonte;
        lblRod[C].Caption := '';
        lblRod[C].Transparent   := True;
        lblRod[C+1].Transparent := True;
      end;
      // Calculando onde devo deixar o pr�ximo campo
      X := W;
      // Se a quantidade de campos for maior do que o tamnho da folha em retrato deixo ela em modo de paisagem.
      if X > QRBand1.Width then
        qrRelSimples.Page.Orientation := poLandscape;
    end;
  end;
  if TotalRegistros then
    lblTotalRegistros.Caption := IntToStr(DSet.RecordCount)
  else
    lblTotalRegistros.Caption := '';

  lblTitulo.Caption := Titulo;
  lblTitulo.Alignment := AlinhamentoTitulo;
  //Configurando altura e largura das bandas e do t�tulo  
  FooterFilho.Height  := 0;
  QRChildBand1.Height := 0;
  QRSubDetail1.Height := 0;
  if Length(lblRod) = 0 then
    qrBandRodape.Height  := 0
  else begin
    qrBandRodape.Height  := (TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0) * 2) + 4;
  end;
  lblTitulo.Width := QRBand1.Width;
  lblPag.Left := QRBand1.Width - lblPag.width;
  Application.ProcessMessages;
//  if lbl[0] <> nil then
  QRGroup1.Height    := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0);//tamlbl[0].Height;
//  if txt[0] <> nil then
  QRChildBand1.Height := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0);//txt[0].Height;
  HPai := DetailBand1.Height;
//  DSet.Close;
  DSet.Open;
end;

procedure TqrRelSimples.MontaRelSimplesComFilho(var DSetPai, DSetFilho: TDataSet; TotalRegistros : Boolean ; Titulo, NmFonte : String ; TamFonte : Integer ; AlinhamentoTitulo : TAlignment; CorLnPar, CorLnImpar : TColor);
var TamCelW, C,I,X,W : Integer;
    lblPai, lblFilho,lblRod : array of TQRLabel;
    txtPai, txtFilho : array of TQRDBText;
begin
  X := 0;
  W := 0;
  qrRelSimples.DataSet := DSetPai;
  TotalRegistrosPai := DSetPai.RecordCount;
  CorPaiLnPar   := CorLnPar;
  CorPaiLnImpar := CorLnImpar;
  ExisteMarcadoPai := DSetPai.FindField('MARCADO') <> nil;
  for I := 0 to DSetPai.Fields.Count -1 do begin
    if DSetPai.Fields[I].Visible then begin
      setLength(lblPai,DSetPai.Fields.Count);
      setLength(txtPai,DSetPai.Fields.Count);
    //CRIANDO O CABECALHO DE CADA UM....
      lblPai[I] := TQRLabel.Create(Self);
      lblPai[I].Parent := QRGroup1;
      lblPai[I].Name := 'lblPai'+DSetPai.Fields[I].Name;
      lblPai[I].Caption := DSetPai.Fields[I].DisplayName;
      lblPai[I].AutoSize := False;
      TamCelW := TamanhoCelulaEmPixel(DSetPai.Fields[I].DisplayWidth,NmFonte,TamFonte);
      lblPai[I].Width := TamCelW;
      lblPai[I].Top := 0;
      lblPai[I].Left := X;
      lblPai[I].Font.Size := TamFonte+1;
      lblPai[I].Font.Style := [fsBold];
      lblPai[I].Font.Name := NmFonte;

    //CRIANDO O REGISTROS DE CADA UM....
      txtPai[I] := TQRDBText.Create(Self);
      txtPai[I].Parent := DetailBand1;
      txtPai[I].Name := 'txtPai'+ DSetPai.Fields[I].Name;
      txtPai[I].DataSet := DSetPai;
      txtPai[I].DataField := DSetPai.Fields[I].FieldName;
      txtPai[I].Caption := '';
      txtPai[I].Top := 0;
      txtPai[I].Left := X;
      //txtPai[I].Color := clLime;
      txtPai[I].Font.Size := TamFonte;
      txtPai[I].AutoSize := False;
      txtPai[I].Width := TamCelW;
      txtPai[I].Font.Name := NmFonte;
      W := W + TamCelW + 5;
      txtPai[I].Transparent := True;
//      txtPai[I].OnPrint := DbLblMarcado.OnPrint;
      FormatarValores(DSetPai.Fields[I],txtPai[I],lblPai[I]);
      //Criando o rodap� se o usu�rio quer q ele tenha o rodap�!
      if DSetPai.Fields[I].Tag <> 0 then begin
        setLength(lblRod,Length(lblRod)+2);
        C := Length(lblRod)-2;
        lblRod[C]   := TQRLabel.Create(Self);
        lblRod[C+1] := TQRLabel.Create(Self);
        lblRod[C].Parent   := qrBandRodape;
        lblRod[C+1].Parent := qrBandRodape;
        if (DSetPai.Fields[I] is TIntegerField) or  (DSetPai.Fields[I] is TFloatField) then begin
          lblRod[C].Alignment   := taRightJustify;
          lblRod[C+1].Alignment := taRightJustify;
        end;
        case DSetPai.Fields[I].Tag of
          1 : begin lblRod[C+1].Caption := 'Soma'; lblRod[C].Name := 'lblSoma' + DSetPai.Fields[I].Name; end;
          2 : begin lblRod[C+1].Caption := 'M�d.'; lblRod[C].Name := 'lblMedia'+ DSetPai.Fields[I].Name; end;
          3 : begin lblRod[C+1].Caption := 'M�x.'; lblRod[C].Name := 'lblMax'  + DSetPai.Fields[I].Name; end;
          4 : begin lblRod[C+1].Caption := 'M�n.'; lblRod[C].Name := 'lblMin'  + DSetPai.Fields[I].Name; end;
        end;
        //lblRod[C].AutoSize := True;
        lblRod[C].Width   := TamCelW;
        lblRod[C+1].Width := TamCelW;
        lblRod[C].Tag := txtPai[I].ComponentIndex;
        lblRod[C].Left   := X;
        lblRod[C+1].Left := X;
        lblRod[C].Top   := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0) + 2;
        lblRod[C+1].Top := 0;
        lblRod[C].Font.Size   := TamFonte;
        lblRod[C+1].Font.Size := TamFonte;
        lblRod[C].Caption := '';
        lblRod[C].Transparent   := True;
        lblRod[C+1].Transparent := True;
      end;
      // Calculando onde devo deixar o pr�ximo campo
      X := W;
      // Se a quantidade de campos for maior do que o tamnho da folha em retrato deixo ela em modo de paisagem.
      if X > QRBand1.Width then
        qrRelSimples.Page.Orientation := poLandscape;
    end;
  end;
  //Configurando Filho
  X := 0;
  W := 0;
  TotalRegistrosFilho := DSetFilho.RecordCount;
  CorPaiLnPar   := CorLnPar;
  CorPaiLnImpar := CorLnImpar;
  ExisteMarcadoFilho := DSetFilho.FindField('MARCADO') <> nil;
  QRSubDetail1.DataSet := DSetFilho;
  for I := 0 to DSetFilho.Fields.Count -1 do begin
    if DSetFilho.Fields[I].Visible then begin
      setLength(lblFilho,DSetFilho.Fields.Count);
      setLength(txtFilho,DSetFilho.Fields.Count);
    //CRIANDO O CABECALHO DE CADA UM....
      lblFilho[I] := TQRLabel.Create(Self);
      lblFilho[I].Parent := QRChildBand1;
      lblFilho[I].Name := 'lblFilho'+DSetFilho.Fields[I].Name;
      lblFilho[I].Caption := DSetFilho.Fields[I].DisplayName;
      lblFilho[I].AutoSize := False;
      TamCelW := TamanhoCelulaEmPixel(DSetFilho.Fields[I].DisplayWidth,NmFonte,TamFonte);
      lblFilho[I].Width := TamCelW;
      lblFilho[I].Top := 0;
      lblFilho[I].Left := X;
      lblFilho[I].Font.Size := TamFonte+1;
      lblFilho[I].Font.Style := [fsBold];
      lblFilho[I].Font.Name := NmFonte;

    //CRIANDO O REGISTROS DE CADA UM....
      txtFilho[I] := TQRDBText.Create(Self);
      txtFilho[I].Parent := QRSubDetail1;
      txtFilho[I].Name := 'txtFilho'+ DSetFilho.Fields[I].Name;
      txtFilho[I].DataSet := DSetFilho;
      txtFilho[I].DataField := DSetFilho.Fields[I].FieldName;
      txtFilho[I].Caption := '';
      txtFilho[I].Top := 0;
      txtFilho[I].Left := X;
      //txtFilho[I].Color := clLime;
      txtFilho[I].Font.Size := TamFonte;
      txtFilho[I].AutoSize := False;
      txtFilho[I].Width := TamCelW;
      txtFilho[I].Font.Name := NmFonte;
      W := W + TamCelW + 5;
      txtFilho[I].Transparent := True;
//      txtFilho[I].OnPrint := DblblFilhoMarcado.OnPrint;
      FormatarValores(DSetFilho.Fields[I],txtFilho[I],lblFilho[I]);
      //Criando o rodap� se o usu�rio quer q ele tenha o rodap�!
      if DSetFilho.Fields[I].Tag <> 0 then begin
        setLength(lblRod,Length(lblRod)+2);
        C := Length(lblRod)-2;
        lblRod[C]   := TQRLabel.Create(Self);
        lblRod[C+1] := TQRLabel.Create(Self);
        lblRod[C].Parent   := qrBandRodape;
        lblRod[C+1].Parent := qrBandRodape;
        if (DSetFilho.Fields[I] is TIntegerField) or  (DSetFilho.Fields[I] is TFloatField) then begin
          lblRod[C].Alignment   := taRightJustify;
          lblRod[C+1].Alignment := taRightJustify;
        end;
        case DSetFilho.Fields[I].Tag of
          1 : begin lblRod[C+1].Caption := 'Soma'; lblRod[C].Name := 'lblSoma' + DSetFilho.Fields[I].Name; end;
          2 : begin lblRod[C+1].Caption := 'M�d.'; lblRod[C].Name := 'lblMedia'+ DSetFilho.Fields[I].Name; end;
          3 : begin lblRod[C+1].Caption := 'M�x.'; lblRod[C].Name := 'lblMax'  + DSetFilho.Fields[I].Name; end;
          4 : begin lblRod[C+1].Caption := 'M�n.'; lblRod[C].Name := 'lblMin'  + DSetFilho.Fields[I].Name; end;
        end;
        //lblRod[C].AutoSize := True;
        lblRod[C].Width   := TamCelW;
        lblRod[C+1].Width := TamCelW;
        lblRod[C].Tag := txtFilho[I].ComponentIndex;
        lblRod[C].Left   := X;
        lblRod[C+1].Left := X;
        lblRod[C].Top   := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0) + 2;
        lblRod[C+1].Top := 0;
        lblRod[C].Font.Size   := TamFonte;
        lblRod[C+1].Font.Size := TamFonte;
        lblRod[C].Caption := '';
        lblRod[C].Transparent   := True;
        lblRod[C+1].Transparent := True;
      end;
      // Calculando onde devo deixar o pr�ximo campo
      X := W;
      // Se a quantidade de campos for maior do que o tamnho da folha em retrato deixo ela em modo de paisagem.
      if X > QRBand1.Width then
        qrRelSimples.Page.Orientation := poLandscape;
    end;
  end;

  if TotalRegistros then
    lblTotalRegistros.Caption := IntToStr(DSetFilho.RecordCount)
  else
    lblTotalRegistros.Caption := '';

  lblTitulo.Caption := Titulo;
  lblTitulo.Alignment := AlinhamentoTitulo;
  //Configurando altura e largura das bandas e do t�tulo  
  if Length(lblRod) = 0 then
    qrBandRodape.Height  := 0
  else begin
    qrBandRodape.Height  := (TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0) * 2) + 4;
  end;
  lblTitulo.Width := QRBand1.Width;
  lblPag.Left := QRBand1.Width - lblPag.width;
  Application.ProcessMessages;
  QRGroup1.Height     := TamanhoCelulaEmPixel(1,NmFonte,TamFonte,0);//tamlbl[0].Height;
  QRChildBand1.Height := QRGroup1.Height;
  DetailBand1.Height  := QRGroup1.Height;
  QRSubDetail1.Height := QRGroup1.Height;
  HPai   := DetailBand1.Height;
  HFilho := QRSubDetail1.Height;
  DSetPai.Open;
  DSetFilho.Open;
end;

procedure TqrRelSimples.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  DetailBand1.Height := HPai;
  if ExisteMarcadoPai then begin
    if (DataSet.FieldByName('MARCADO').AsString = 'True')
    or (DataSet.FieldByName('MARCADO').AsString = 'S') then begin
      if odd(contadorPai)then
        DetailBand1.color := CorPaiLnPar
      else
        DetailBand1.color := CorPaiLnImpar;
      contadorPai := contadorPai + 1;
    end else
      DetailBand1.Height := 0;
  end else begin
    if odd(contadorPai)then
      DetailBand1.color := CorPaiLnPar
    else
      DetailBand1.color := CorPaiLnImpar;
    contadorPai := contadorPai + 1;
  end;
end;

procedure TqrRelSimples.QRSubDetail1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRSubDetail1.Height := HFilho;
  if ExisteMarcadoPai and ExisteMarcadoFilho then begin
    if DataSet.FieldByName('MARCADO').AsString = 'True' then begin
      if odd(contadorFilho)then
        QRSubDetail1.color := $00DDFFFD
      else
        QRSubDetail1.color := $00C4F0FF;
      contadorFilho := contadorFilho + 1;
    end else
      DetailBand1.Height := 0;
  end else begin
    if odd(contadorFilho)then
      QRSubDetail1.color := $00DDFFFD
    else
      QRSubDetail1.color := $00C4F0FF;
    contadorFilho := contadorFilho + 1;
  end;
end;

procedure TqrRelSimples.DetailBand1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
var I : Integer;
begin
  for I:=0 to qrBandRodape.ControlCount-1 do begin
    try
    if (Pos('lblSoma',qrBandRodape.Controls[I].Name) > 0) or (Pos('lblMedia',qrBandRodape.Controls[I].Name) > 0) then begin// se eu encontrei 'lblSoma' no componente
      TQRLabel(qrBandRodape.Controls[I]).Caption := Soma(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
      if (Pos('lblMedia',qrBandRodape.Controls[I].Name) > 0) and (contadorPai = TotalRegistrosPai) then
        TQRLabel(qrBandRodape.Controls[I]).Caption := FloatToStr(Media(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag, TotalRegistrosPai));
    end else if Pos('lblMax',qrBandRodape.Controls[I].Name) > 0 then begin
      TQRLabel(qrBandRodape.Controls[I]).Caption := Maximo(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
    end else if Pos('lblMin',qrBandRodape.Controls[I].Name) > 0 then
      TQRLabel(qrBandRodape.Controls[I]).Caption := Minimo(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
    except
    end;
  end;
end;

procedure TqrRelSimples.QuickRepAfterPreview(Sender: TObject);
begin
  teste               := 0;
  contadorPai         := 0;
  TotalRegistrosPai   := 0;
  contadorFilho       := 0;
  TotalRegistrosFilho := 0;
end;

procedure TqrRelSimples.QRSubDetail1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
var I : Integer;
begin
  for I:=0 to qrBandRodape.ControlCount-1 do begin
    try
    if (Pos('lblSoma',qrBandRodape.Controls[I].Name) > 0) or (Pos('lblMedia',qrBandRodape.Controls[I].Name) > 0) then begin// se eu encontrei 'lblSoma' no componente
      TQRLabel(qrBandRodape.Controls[I]).Caption := Soma(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
      if (Pos('lblMedia',qrBandRodape.Controls[I].Name) > 0) and (contadorPai = TotalRegistrosPai) then
        TQRLabel(qrBandRodape.Controls[I]).Caption := FloatToStr(Media(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag, TotalRegistrosPai));
    end else if Pos('lblMax',qrBandRodape.Controls[I].Name) > 0 then begin
      TQRLabel(qrBandRodape.Controls[I]).Caption := Maximo(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
    end else if Pos('lblMin',qrBandRodape.Controls[I].Name) > 0 then
      TQRLabel(qrBandRodape.Controls[I]).Caption := Minimo(TQRLabel(qrBandRodape.Controls[I]).Caption , TQRLabel(qrBandRodape.Controls[I]).Tag);
    except
    end;
  end;
end;

end.


