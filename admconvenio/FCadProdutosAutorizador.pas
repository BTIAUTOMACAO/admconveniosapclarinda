unit FCadProdutosAutorizador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, Menus, DB, ZDataset, ZAbstractRODataset, ZAbstractDataset,
  StdCtrls, Mask, JvExMask, JvToolEdit, ComCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons, ExtCtrls, DBCtrls, ADODB;

type
  TFrmCadProdutosAutorizador = class(TFCad)
    QCadastroPROD_DESCR: TStringField;
    QCadastroLABORATORIO: TStringField;
    QCadastroPRE_VALOR: TFloatField;
    QCadastroALT_LIBERADA: TStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    db: TDBCheckBox;
    QCadastroPROD_CODIGO: TStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    QCadastroPROD_AUT_ID: TIntegerField;
    QCadastroOPERADOR: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroPRE_VALOR_19: TBCDField;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadProdutosAutorizador: TFrmCadProdutosAutorizador;

implementation

uses DM, UMenu, UCadastroProgProd, cartao_util, URotinasTexto;

{$R *.dfm}

procedure TFrmCadProdutosAutorizador.FormCreate(Sender: TObject);
begin
  chavepri := 'PROD_AUT_ID';
  detalhe := 'PROD AUT_ID: ';
  inherited;
  QCadastro.Open;
end;

procedure TFrmCadProdutosAutorizador.ButBuscaClick(Sender: TObject);
begin
  inherited;
  screen.Cursor := crHourGlass;
  QCadastro.Close;
  QCadastro.Sql.Text := ' SELECT * FROM produtos_autorizador'+
    ' WHERE apagado <> ''S'' ';
  if ((Trim(EdCod.Text) <> '') or (Trim(EdNome.Text) <> '')) then begin
    if Trim(EdCod.Text) <> '' then
      QCadastro.Sql.Add(' AND PROD_CODIGO  = '+QuotedStr(EdCod.Text)+' order by prod_aut_id ');
    if Trim(EdNome.Text) <> '' then
      QCadastro.Sql.Add(' AND prod_descr LIKE ''%'+EdNome.Text+'%'' ');
  end else
    QCadastro.Sql.Add(' ORDER BY prod_descr ');

  QCadastro.SQL.Text;
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus;
  edcod.Clear;
  EdNome.Clear;
  screen.Cursor := crDefault;
end;

procedure TFrmCadProdutosAutorizador.QCadastroAfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  QCadastroPROD_AUT_ID.ReadOnly := false;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_AUT_ID AS PROD_AUT_ID');
  DMConexao.AdoQry.Open;
  QCadastroPROD_AUT_ID.AsInteger            := DMConexao.AdoQry.FieldByName('PROD_AUT_ID').AsInteger;
  QCadastroALT_LIBERADA.AsString := 'N';
  DBEdit1.SetFocus;
end;

procedure TFrmCadProdutosAutorizador.ButGravaClick(Sender: TObject);
begin
  inherited;
//  DMConexao.AdoQry.SQL.Clear;
//  DMConexao.AdoQry.SQL.Add('select prod_aut_id from produtos_autorizador where prod_codigo = '+QuotedStr(SoNumero(Table1.FieldByName('MED_BARRA').AsString))+'');
//  DMConexao.AdoQry.Open;
//  if MsgSimNao('Deseja cadastrar o produto em alguma regra de conv�nio?'+sLineBreak) then
//  begin
//    FCadastroProgProd  := TFCadastroProgProd.Create(Self);
//    FCadastroProgProd.txtCodBarra.Caption := QCadastroPROD_CODIGO.AsString;
//    FCadastroProgProd.txtValTot.Caption := QCadastroPRE_VALOR.AsString;
//    FCadastroProgProd.txtDesc.Caption := QCadastroPROD_DESCR.AsString;
//    FCadastroProgProd.ShowModal;
//
//  end;
end;

end.
