unit ClassImpressao;

interface

uses
  Windows, Messages, SysUtils, Classes, strutils, forms;

type
  TRelatorio = class(TStringList)
  private
    NumPagina : Integer;
    LinhasPag : Integer;
    cab, tit : TStringList;
    cab_todas, tit_todas : Boolean;
    function AddObject(const S: string; AObject: TObject): Integer;override;
    procedure InternalNovaPagina;
  public
    TamPagina : Integer;
    MostrarNumPag  : Boolean;
    EsconderData : Boolean;
    QuebraPagina : Boolean;
    procedure NovaPagina;
    constructor Create;
  end;

  TImpres = class(TObject)
  private
    linhas_pagina, pagina: integer;
    quebra_pagina, titulo_todas, cabeca_todas: boolean;
    Titulo:    TStringList;
    Cabecalho: TStringList;
    Linhas:    TStringList;
    FMostrarNumPaginas: boolean;
    Fesconderdata: boolean;
    procedure setcabecaemtodas(const Value: boolean);
    procedure setlinhaspagina(const Values: integer);
    procedure setquebrapagina(const Value: boolean);
    procedure settituloemtodas(const Value: boolean);
    function gettextolinhas: string;
    procedure SetMostrarNumPaginas(const Value: boolean);
    procedure Imprimir2();
    procedure Setesconderdata(const Value: boolean);


  public
    constructor Create;
    procedure getLinhas(var sl: TStringList);
    function AddCabecalho(Texto: String;Coluna:Integer=3;linha:integer=-1):integer;
    procedure ClearCabecalho;
    function AddLinha(Texto: String;Coluna:Integer=3;linha:integer=-1):Integer;
    function AddTitulo(Texto: String;Coluna:Integer=3;linha:integer=-1):integer;
    function AddLinhaSeparadora(Preenchedor:string='-'):Integer;
    function Centraliza(Texto:string;Tamanho:Integer=127;Preenchedor:string=' '): String;
    class function Direita(Texto:string;Tamanho:Integer;Preenchedor:string=' '): String;
    procedure Imprimir(Caminho: String='Relatorio.txt'; Visualizar:Boolean=True; qtdcolunas: Integer=128;bPaisagem:Boolean=False);
    procedure novapagina(Forca:Boolean=False);
    function Preenche(Texto:string='-';Tamanho:Integer=127;Preenchedor:string='-'): String;
    function SaltarLinhas(Count: Integer): Integer;
    function LinhasCount:Integer;
    procedure TrimLinhas;
    function GetText: String;
  published
    property cabecaemtodas : boolean read cabeca_todas write setcabecaemtodas;
    property linhaspagina : integer read linhas_pagina write setlinhaspagina;
    property quebrapagina : boolean read quebra_pagina write setquebrapagina;
    property tituloemtodas : boolean read titulo_todas write settituloemtodas;
    property esconderdata : boolean read Fesconderdata write Setesconderdata;
    property MostrarNumPaginas : boolean  read FMostrarNumPaginas write SetMostrarNumPaginas;
    property textolinhas : string read gettextolinhas;
  end;

implementation

uses  cartao_util;

//  M�todos Classe  TImpres

constructor TImpres.Create;
begin
   inherited;
   cabecaemtodas := False;
   tituloemtodas := False;
   quebrapagina  := True;
   MostrarNumPaginas := True;
   esconderdata  := False;
   linhaspagina  := 66;
   pagina := 0;
   Titulo    := TStringList.Create;
   Linhas    := TStringList.Create;
   Cabecalho := TStringList.Create;
end;

procedure TImpres.novapagina(Forca:Boolean=False);
begin
if (not quebrapagina) or Forca then begin
   Linhas.Add(#12);  //adiciona linhas se a quebra de p�gina n�o for autom�tica.
end;
end;

procedure TImpres.TrimLinhas();
var i, c : integer;
begin
c := Linhas.Count-1;
for i := 0 to c do
  linhas[i] := Trim(linhas[i]);

end;

procedure TImpres.Imprimir2();
var Rel : TRelatorio;
    i : Integer;
begin
    Rel := TRelatorio.Create;
    Rel.MostrarNumPag := MostrarNumPaginas;
    Rel.TamPagina := linhas_pagina;
    Rel.tit_todas := tituloemtodas;
    Rel.cab_todas := cabecaemtodas;
    Rel.EsconderData := esconderdata;
    REl.QuebraPagina := quebrapagina;
    Rel.tit := Titulo;
    Rel.cab := Cabecalho;
    if Titulo.Count > 0 then
       Rel.AddStrings(Titulo );
    if Cabecalho.Count > 0 then
       Rel.AddStrings(Cabecalho);
    for i := 0 to Linhas.Count-1 do begin
        if pos(#12,Linhas[i]) > 0 then begin
           Rel.InternalNovaPagina;
           Linhas[i] := StringReplace(Linhas[i], #12,'',[]);
           if Trim(Linhas[i]) <> '' then
              Rel.add(Linhas[i]);
        end
        else
          Rel.add(Linhas[i]);
    end;
    Linhas := Rel;
end;

procedure TImpres.Imprimir(Caminho: String='Relatorio.txt'; Visualizar:Boolean=True; qtdcolunas: Integer=128; bPaisagem: Boolean=False);
begin
  Imprimir2();
  if Caminho <> 'Relatorio.txt' then
    linhas.SavetoFile(PChar(Caminho));
//  else
  linhas.SavetoFile(PChar(ExtractFilePath(Application.ExeName)+'Relatorio.txt'));
  if Visualizar then
  begin
    if not (FileExists('visualizador.exe')) and not (FileExists(ExtractFilePath(Application.ExeName)+'visualizador.exe')) then
      MsgErro('Visualizador de Relat�rios n�o econtrado!')
    else
    begin
      If bPaisagem then
        WinExec(PChar(ExtractFilePath(Application.ExeName)+'visualizador.exe '+ExtractFilePath(Application.ExeName)+'Relatorio.txt 132 paisagem '), SW_SHOWDEFAULT)
      else
        WinExec(PChar(ExtractFilePath(Application.ExeName)+'visualizador.exe '+ExtractFilePath(Application.ExeName)+'Relatorio.txt 132 '), SW_SHOWDEFAULT);
    end;
  end;
end;

function TImpres.GetText:String;
begin
 Imprimir2();
 Result := linhas.Text;
end;

procedure TImpres.setcabecaemtodas(const Value: boolean);
begin
cabeca_todas := Value;
end;

procedure TImpres.setlinhaspagina(const Values: integer);
begin
linhas_pagina := Values;
end;

procedure TImpres.setquebrapagina(const Value: boolean);
begin
quebra_pagina := Value;
end;

procedure TImpres.settituloemtodas(const Value: boolean);
begin
titulo_todas := Value;
end;

function TImpres.AddTitulo(Texto: String;Coluna:Integer=3;linha:integer=-1):integer;
var i, l : integer;
begin
if linha = -1 then begin
   l := Titulo.add('');
end
else begin
   if Titulo.count < linha then for i := Titulo.count to linha do Titulo.add('');
   l := linha - 1;
end;
if Length(TItulo[l]) < coluna then for i := Length(Titulo[l]) to coluna do Titulo[l] := Titulo[l]+' ';
Titulo[l] := StuffString(Titulo[l],coluna,length(texto),Texto);
Result := Titulo.count;
end;

function TImpres.AddLinha(Texto: String;Coluna:Integer=3;linha:integer=-1):Integer;
var i, l : integer;
begin
if linha = -1 then begin
   l := Linhas.add('');
end
else begin
   if Linhas.count < linha then for i := Linhas.count to linha do Linhas.add('');
   l := linha - 1;
end;
if Length(Linhas[l]) < coluna then for i := Length(Linhas[l]) to coluna do Linhas[l] := Linhas[l]+' ';
Linhas[l] := StuffString(Linhas[l],coluna,length(texto),Texto);
Result := Linhas.count;
end;

function TImpres.AddCabecalho(Texto: String;Coluna:Integer=3;linha:integer=-1):integer;
var i, l : integer;
begin
if linha = -1 then begin
   l := Cabecalho.add('');
end
else begin
   if Cabecalho.count < linha then for i := Cabecalho.count to linha do Cabecalho.add('');
   l := linha - 1;
end;
if Length(Cabecalho[l]) < coluna then for i := Length(Cabecalho[l]) to coluna do Cabecalho[l] := Cabecalho[l]+' ';
Cabecalho[l] := StuffString(Cabecalho[l],coluna,length(texto),Texto);
Result := Cabecalho.count;
end;

class function TImpres.Direita(Texto:string;Tamanho:Integer;Preenchedor:string=' '): String;
var tamanho2, i: Integer;
    texto2: String;
begin
   tamanho2 := 0;
   i        := 0;
   texto2   := '';

   tamanho2 := Tamanho-Length(Texto);
   for i := 1 to tamanho2 do texto2 := texto2 + Preenchedor;
   Result := texto2 + texto;
   end;

function TImpres.Centraliza(Texto:string;Tamanho:Integer=127;Preenchedor:string=' '): String;
begin
   if Length(Texto) > Tamanho then Texto := Copy(Texto , 1, Tamanho);
   while Length(Texto) < Tamanho do begin
      if (Length(Texto) mod 2) = 0 then Texto := Texto + Preenchedor
                                   else Texto := Preenchedor + Texto;
   end;
   Result := Texto;
end;

function TImpres.Preenche(Texto:string='-';Tamanho:Integer=127;Preenchedor:string='-'): String;
var i      :integer;
    texto2 :string;
begin
   i := 0;
   texto2 := '';
   texto2 := Texto;
   for i := 1 to Tamanho-Length(Texto) do texto2 := texto2 + Preenchedor;
   Preenche := texto2;
end;

function TImpres.gettextolinhas: string;
begin
Result := Linhas.Text;
end;

procedure TImpres.SetMostrarNumPaginas(const Value: boolean);
begin
  FMostrarNumPaginas := Value;
end;

function TImpres.AddLinhaSeparadora(Preenchedor: string): Integer;
begin
   Result := AddLinha(Preenche(Preenchedor,127,Preenchedor));
end;

function TImpres.SaltarLinhas(Count:Integer):Integer;
var i : integer;
begin
  for i := 1 to Count do
      Result := Linhas.Add('');
end;

{ TRelatorio }

function TRelatorio.AddObject(const S: string; AObject: TObject): Integer;
begin
  if (Count = 0) then begin
     if MostrarNumPag then begin
        inherited AddObject(sLineBreak,AObject);
        Add('  Data/Hora '+FormatDateTime('dd/mm/yyyy hh:nn', Now)+TImpres.Direita('Pagina '+IntToStr(NumPagina),100));
        Add(' ');
     end
     else if not EsconderData then begin
        inherited AddObject(sLineBreak,AObject);
        Add('  Data/Hora '+FormatDateTime('dd/mm/yyyy hh:nn', Now));
        Add(' ');
     end;
  end;
  Result := inherited AddObject(S,AObject);
  if LinhasPag = TamPagina then
     NovaPagina;

  Inc(linhaspag);
end;

constructor TRelatorio.Create;
begin
inherited;
   NumPagina := 1;
   TamPagina := 66;
   LinhasPag := 0;
   EsconderData := False;
   QuebraPagina := True;
end;

procedure TRelatorio.NovaPagina;
begin
   if QuebraPagina then begin
      InternalNovaPagina;
   end;
end;

procedure TRelatorio.InternalNovaPagina;
begin
      LinhasPag := 0;
      Add(#12);
      inc(NumPagina);
      if MostrarNumPag then begin
         Add('  Data/Hora '+FormatDateTime('dd/mm/yyyy hh:nn', Now)+TImpres.Direita('Pagina '+IntToStr(NumPagina),100));
         Add(' ');
      end
      else if not EsconderData then begin
         Add('  Data/Hora '+FormatDateTime('dd/mm/yyyy hh:nn', Now));
         Add(' ');
      end;
      if tit_todas and (tit.Count > 0) and (NumPagina > 1) then
         AddStrings(tit);
      if cab_todas and (cab.Count > 0) and (NumPagina > 1) then
         AddStrings(cab);

end;

function TImpres.LinhasCount: Integer;
begin
   Result := Linhas.Count;
end;

procedure TImpres.Setesconderdata(const Value: boolean);
begin
  Fesconderdata := Value;
end;

procedure TImpres.ClearCabecalho;
begin
   Cabecalho.Clear;
end;

procedure TImpres.getLinhas(var sl: TStringList);
begin
  Imprimir2;
  sl := linhas;
end;

end.
