unit URelExtratoEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Grids, DBGrids, {JvDBCtrl,} ExtCtrls, ComCtrls,
  StdCtrls, Mask, JvToolEdit, DB, {JvMemDS,} ZAbstractRODataset, ZDataset,
  Buttons, dateutils, ZAbstractDataset, UTipos, ClassImpressao, DBClient,
  frxClass, frxExportPDF, frxDBSet, frxGradient, JvMemoryDataset, JvExMask,
  JvExDBGrids, JvDBGrid, {ClipBrd,} frxIBXComponents, ShellApi, frxExportXLS,
  DBCtrls, ADODB;

type
  TFRelExtratoEmp = class(TF1)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    JvDBGrid1: TJvDBGrid;
    ButMarcDesm_Emp: TButton;
    ButMarcaTodos_Emp: TButton;
    ButDesmTodos_Emp: TButton;
    DSEmpresas: TDataSource;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    but_MarcDem_For: TButton;
    But_MarcaT_For: TButton;
    But_DesmT_For: TButton;
    Panel3: TPanel;
    but_Abre_For: TButton;
    But_Fecha_For: TButton;
    DBGrid1: TJvDBGrid;
    QFornec: TJvMemoryData;
    DSFornec: TDataSource;
    QFornecCRed_id: TIntegerField;
    QFornecRazao: TStringField;
    QFornecFantasia: TStringField;
    QFornecSegmento: TStringField;
    BitBtn1: TBitBtn;
    QFornecMarcado: TBooleanField;
    ComboOrdem: TComboBox;
    Label3: TLabel;
    CheckOrdem: TCheckBox;
    RGReceita: TRadioGroup;
    JvDBGrid3: TJvDBGrid;
    Splitter1: TSplitter;
    DSQGruposemp: TDataSource;
    rgpNFEntregue: TRadioGroup;
    pnlOutrasOpcoes: TPanel;
    ChkMostraLim: TCheckBox;
    ChkDemit: TCheckBox;
    ChkDescEmp: TCheckBox;
    ChkTotMes: TCheckBox;
    RGBaixa: TRadioGroup;
    CKTotFinal: TCheckBox;
    CKUsarCodConvEmp: TCheckBox;
    frxReport1: TfrxReport;
    frxGradientObject1: TfrxGradientObject;
    dbEmpresas: TfrxDBDataset;
    dbQuery1: TfrxDBDataset;
    StringDS: TfrxUserDataSet;
    BitBtn2: TBitBtn;
    sd: TSaveDialog;
    frxPDFExport1: TfrxPDFExport;
    tsRegistros: TTabSheet;
    dbGridExtratoEmp: TJvDBGrid;
    DSQuery1: TDataSource;
    cbSoMontar: TCheckBox;
    cdsQuery1: TClientDataSet;
    cdsQuery1CHAPA: TFloatField;
    cdsQuery1TITULAR: TStringField;
    cdsQuery1LIMITE_MES: TFloatField;
    cdsQuery1VALOR: TFloatField;
    bntGerarPDF: TBitBtn;
    BitBtn3: TBitBtn;
    DSQuery1Zeos: TDataSource;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Bevel1: TBevel;
    ButListaEmp: TButton;
    DataFecha: TJvDateEdit;
    por_dia_fecha: TRadioButton;
    por_periodo: TRadioButton;
    datafin: TJvDateEdit;
    CheckEmp: TCheckBox;
    Label1: TLabel;
    dsLkpEmp: TDataSource;
    dbLkpCidade: TDBLookupComboBox;
    QReadOnlyEmpresas: TADOQuery;
    QReadOnlyEmpresasempres_id: TAutoIncField;
    QReadOnlyEmpresasnome: TStringField;
    QReadOnlyEmpresasfantasia: TStringField;
    QReadOnlyEmpresasdesconto_emp: TBCDField;
    QReadOnlyEmpresasdatafecha: TDateTimeField;
    QReadOnlyEmpresasdatavenc: TDateTimeField;
    QReadOnlyEmpresasdataini: TDateTimeField;
    QReadOnlyEmpresasdatafin: TDateTimeField;
    QGruposEmp: TADOQuery;
    QGruposEmpGRUPO_CONV_EMP_ID: TIntegerField;
    QGruposEmpDESCRICAO: TStringField;
    QGruposEmpEMPRES_ID: TIntegerField;
    QGruposEmpimprime: TStringField;
    QBuscaFornec: TADOQuery;
    QBuscaForneccred_id: TIntegerField;
    QBuscaFornecfantasia: TStringField;
    QBuscaFornecnome: TStringField;
    QBuscaFornecsegmento: TStringField;
    Query1: TADOQuery;
    Query1Valor: TBCDField;
    Query1conv_id: TIntegerField;
    Query1chapa: TFloatField;
    Query1titular: TStringField;
    Query1limite_mes: TBCDField;
    Query1cod_empresa: TStringField;
    qLkpEmp: TADOQuery;
    MEmpresas: TJvMemoryData;
    MEmpresasempres_id: TIntegerField;
    MEmpresasnome: TStringField;
    MEmpresasfantasia: TStringField;
    MEmpresasdesconto_emp: TFloatField;
    MEmpresasdataini: TDateTimeField;
    MEmpresasdatafin: TDateTimeField;
    MEmpresasdatavenc: TDateTimeField;
    MEmpresasdatafecha: TDateTimeField;
    MEmpresasmarcado: TBooleanField;
    dbLkpOperadores: TDBLookupComboBox;
    Label4: TLabel;
    QOperadores: TADOQuery;
    DSOperadores: TDataSource;
    QOperadoresNOME: TStringField;
    qLkpEmpcidade: TStringField;
    qLkpEmpcod: TIntegerField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure but_Abre_ForClick(Sender: TObject);
    procedure But_Fecha_ForClick(Sender: TObject);
    procedure but_MarcDem_ForClick(Sender: TObject);
    procedure But_MarcaT_ForClick(Sender: TObject);
    procedure But_DesmT_ForClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DataFechaChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure por_dia_fechaClick(Sender: TObject);
    procedure por_periodoClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QGruposEmpBeforePost(DataSet: TDataSet);
    procedure JvDBGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QReadOnlyEmpresasAfterOpen(DataSet: TDataSet);
    procedure QReadOnlyEmpresasAfterScroll(DataSet: TDataSet);
    procedure JvDBGrid3Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure frxReport1Preview(Sender: TObject);
    procedure frxReport1GetValue(const VarName: String;
      var Value: Variant);
    procedure frxReport1ClosePreview(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure JvDBGrid3TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure MEmpresasAfterOpen(DataSet: TDataSet);
    procedure MEmpresasAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
//    Marcados : TStringList;
    btn2 : Boolean;
    sl : TStringList;
    fornec_sel : String;
    empres_sel : String;
    tot_emp : currency;
    total_rel: currency;
    Valor_Total: Currency;
    Valor_TotalComDesconto: Currency;
    total_func: integer;
    MostrarRelatorio, filtrarEntrNF, acumularAutor : Boolean;
    function CalculaTotal(Empres_id : Integer): Currency;
    procedure Fornecedores_Sel;
    procedure Empresas_Sel;
    function Grupos_emp_id_nao_sel(Empres_id: integer): string;
    procedure SomaPeriodos(var Periodos: TPeriodos);
    procedure SomaPeriodos2(var Periodos: TPeriodos; emp_id : Integer);
    procedure AbrirGrupos;
    procedure AbrirCCorrente(excel : boolean = false);
    procedure FiltrarGrupos;
    procedure ImpCabecalho(imp:TImpres);
    procedure ImpRodape(imp:TImpres);
    procedure ImpCorpo(imp:TImpres);
  public

    { Public declarations }
  end;

var
  FRelExtratoEmp: TFRelExtratoEmp;

implementation

uses impressao,  DM, cartao_util;

{$R *.dfm}

function TFRelExtratoEmp.CalculaTotal(Empres_id : Integer): Currency;
var s, sql : String;
begin
  sql := '';
  s := UpperCase(Query1.SQL.Text);
  sql := copy(s,1,Pos('VALOR',s)+Length('VALOR'))+sLineBreak;
  sql := sql + copy(s,Pos('FROM',s),Pos('GROUP BY',s)-Pos('FROM',s));
  sql := StringReplace(sql, ':EMPRES_ID',inttostr(EMPRES_ID),[rfReplaceAll, rfIgnoreCase]);
//  clipboard.AsText := sql;
  Result := DMConexao.ExecuteScalar(sql);
end;

procedure TFRelExtratoEmp.ButListaEmpClick(Sender: TObject);
var IncBaixa, empMov : String;
begin
  IncBaixa := 'N';
  if RGBaixa.ItemIndex in [1,2] then IncBaixa := 'S';

  GridColByFieldName(JvDBGrid1,'DATAINI').Visible   := por_periodo.Checked;
  GridColByFieldName(JvDBGrid1,'DATAFIN').Visible   := por_periodo.Checked;
  GridColByFieldName(JvDBGrid1,'DATAFECHA').Visible := por_dia_fecha.Checked;
  GridColByFieldName(JvDBGrid1,'DATAVENC').Visible  := por_dia_fecha.Checked;
  QReadOnlyEmpresas.Close;
  QReadOnlyEmpresas.SQL.Clear;
  QReadOnlyEmpresas.SQL.Add(' Select empresas.empres_id, empresas.nome, empresas.fantasia, empresas.desconto_emp ');
  if por_dia_fecha.Checked then begin
     QReadOnlyEmpresas.SQL.Add(' ,cast('+ formatdataIB(DataFecha.Date)+' as datetime) as datafecha, ');
     QReadOnlyEmpresas.SQL.Add(' (select data_venc from dia_fecha where data_fecha = '+formatdataIB(DataFecha.Date)+' and empres_id = empresas.empres_id) as  datavenc, ');
     QReadOnlyEmpresas.SQL.Add(' current_timestamp as dataini, ');
     QReadOnlyEmpresas.SQL.Add(' current_timestamp as datafin ');
  end else begin
     QReadOnlyEmpresas.SQL.Add(' ,current_timestamp as datafecha, ');
     QReadOnlyEmpresas.SQL.Add(' current_timestamp as  datavenc, ');
     QReadOnlyEmpresas.SQL.Add(' cast('+formatdataIB(DataFecha.Date)+' as datetime) as dataini, ');
     QReadOnlyEmpresas.SQL.Add(' cast('+formatdataIB(datafin.Date)+' as datetime) as datafin ');
  end;
  QReadOnlyEmpresas.SQL.Add(' from empresas where empresas.apagado <> ''S'' ');
  if por_dia_fecha.Checked then
     QReadOnlyEmpresas.SQL.Add(' and empresas.empres_id in (select empres_id from dia_fecha where data_fecha = '+FormatDataIB(DataFecha.Date)+')');
  if dbLkpCidade.Text <> '<TODAS AS CIDADES>' then
     QReadOnlyEmpresas.SQL.Add(' and cidade = ' + FloatToStr(dbLkpCidade.KeyValue) + '');
  if dbLkpOperadores.KeyValue <> '<TODOS>' then
     QReadOnlyEmpresas.SQL.Add(' and responsavel_fechamento = ' + QuotedStr(dbLkpOperadores.KeyValue) + '');
  QReadOnlyEmpresas.SQL.Add(' order by empresas.nome ');
  QReadOnlyEmpresas.SQL.Text;
  QReadOnlyEmpresas.Open;
  if por_dia_fecha.Checked and QReadOnlyEmpresas.IsEmpty then begin
     ShowMessage('N�o foi encontrada nenhuma empresa com fechamento nesta data.');
     MEmpresas.EmptyTable;
     DataFecha.SetFocus;
  end else begin
     //Verifica s� com movimento.
     if CheckEmp.Checked then begin
        empMov := '';
        QReadOnlyEmpresas.DisableControls;
        QReadOnlyEmpresas.First;
        while not QReadOnlyEmpresas.Eof do begin
           if (por_periodo.Checked and ( not DMConexao.VerificaMovEmpPeriodo(QReadOnlyEmpresasEMPRES_ID.AsInteger,QReadOnlyEmpresasDATAINI.AsDateTime,QReadOnlyEmpresasDATAFIN.AsDateTime,IncBaixa) ) )
           or (por_dia_fecha.Checked   and ( not DMConexao.VerificaMovEmpFechamento(QReadOnlyEmpresasEMPRES_ID.AsInteger,QReadOnlyEmpresasDATAFECHA.AsDateTime,IncBaixa) ) ) then begin
              if not QReadOnlyEmpresas.IsEmpty then
                 empMov := empMov + QReadOnlyEmpresasEMPRES_ID.AsString + ',';
           end;
           QReadOnlyEmpresas.Next;
        end;
        if empMov <> '' then
        begin
          QReadOnlyEmpresas.EnableControls;
          QReadOnlyEmpresas.Close;
          QReadOnlyEmpresas.SQL.Clear;
          QReadOnlyEmpresas.SQL.Add(' Select empresas.empres_id, empresas.nome, empresas.fantasia, empresas.desconto_emp ');
          if por_dia_fecha.Checked then begin
            QReadOnlyEmpresas.SQL.Add(' ,cast('+ formatdataIB(DataFecha.Date)+' as datetime) as datafecha, ');
            QReadOnlyEmpresas.SQL.Add(' (select data_venc from dia_fecha where data_fecha = '+formatdataIB(DataFecha.Date)+' and empres_id = empresas.empres_id) as  datavenc, ');
            QReadOnlyEmpresas.SQL.Add(' current_timestamp as dataini, ');
            QReadOnlyEmpresas.SQL.Add(' current_timestamp as datafin ');
          end else begin
            QReadOnlyEmpresas.SQL.Add(' ,current_timestamp as datafecha, ');
            QReadOnlyEmpresas.SQL.Add(' current_timestamp as  datavenc, ');
            QReadOnlyEmpresas.SQL.Add(' cast('+formatdataIB(DataFecha.Date)+' as datetime) as dataini, ');
            QReadOnlyEmpresas.SQL.Add(' cast('+formatdataIB(datafin.Date)+' as datetime) as datafin ');
          end;
          QReadOnlyEmpresas.SQL.Add(' from empresas where empresas.apagado <> ''S'' ');
          QReadOnlyEmpresas.SQL.Add(' and empres_id not in (' + Copy(empMov,0,(Length(empMov) - 1)) + ')');
          if por_dia_fecha.Checked then
            QReadOnlyEmpresas.SQL.Add(' and empresas.empres_id in (select empres_id from dia_fecha where data_fecha = '+FormatDataIB(DataFecha.Date)+')');
          if dbLkpCidade.KeyValue <> '<TODAS AS CIDADES>' then
            QReadOnlyEmpresas.SQL.Add(' and cidade = ' + QuotedStr(dbLkpCidade.KeyValue) + '');
          if dbLkpOperadores.KeyValue <> '<TODOS>' then
            QReadOnlyEmpresas.SQL.Add(' and responsavel_fechamento = ' + QuotedStr(dbLkpOperadores.KeyValue) + '');
          QReadOnlyEmpresas.SQL.Add(' order by empresas.nome ');
          QReadOnlyEmpresas.SQL.Text;
          QReadOnlyEmpresas.Open;
        end
        else
          begin
           ShowMessage('N�o foi encontrada nenhuma empresa com fechamento nesta data.');
           QReadOnlyEmpresas.EnableControls;
           QReadOnlyEmpresas.SQL.Clear;
           MEmpresas.EmptyTable;
          end;
     end;

     if QReadOnlyEmpresas.SQL.Text <> '' then
     begin
      QReadOnlyEmpresas.First;

      MEmpresas.Open;
      MEmpresas.EmptyTable;
      MEmpresas.DisableControls;
      while not QReadOnlyEmpresas.Eof do begin
        MEmpresas.Append;
        MEmpresasempres_id.AsInteger := QReadOnlyEmpresasempres_id.AsInteger;
        MEmpresasnome.AsString := QReadOnlyEmpresasnome.AsString;
        MEmpresasfantasia.AsString := QReadOnlyEmpresasfantasia.AsString;
        MEmpresasdesconto_emp.AsFloat := QReadOnlyEmpresasdesconto_emp.AsFloat;
        MEmpresasdataini.AsDateTime :=  QReadOnlyEmpresasdataini.AsDateTime;
        MEmpresasdatafin.AsDateTime := QReadOnlyEmpresasdatafin.AsDateTime;
        MEmpresasdatavenc.AsDateTime := QReadOnlyEmpresasdatavenc.AsDateTime;
        MEmpresasdatafecha.AsDateTime := QReadOnlyEmpresasdatafecha.AsDateTime;
        MEmpresasMarcado.AsBoolean := False;
        MEmpresas.Post;
        QReadOnlyEmpresas.Next;
      end;
      MEmpresas.First;
      MEmpresas.EnableControls;
      empres_sel := EmptyStr;

      JvDBGrid1.SetFocus;

      AbrirGrupos;
     end;
  end;
end;

procedure TFRelExtratoEmp.ButMarcDesm_EmpClick(Sender: TObject);
begin
if MEmpresas.IsEmpty then Exit;
  MEmpresas.Edit;
  MEmpresasMarcado.AsBoolean := not MEmpresasMarcado.AsBoolean;
  MEmpresas.Post;
  Empresas_Sel;
end;

procedure TFRelExtratoEmp.Empresas_Sel;
var marca : TBookmark;
begin
  empres_sel := EmptyStr;
  marca := MEmpresas.GetBookmark;
  MEmpresas.DisableControls;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    if MEmpresasMarcado.AsBoolean then empres_sel := empres_sel + ','+MEmpresasempres_id.AsString;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  if empres_sel <> '' then empres_sel := Copy(empres_sel,2,Length(empres_sel));
  MEmpresas.EnableControls;
end;

procedure TFRelExtratoEmp.ButMarcaTodos_EmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := true;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
end;

procedure TFRelExtratoEmp.ButDesmTodos_EmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := false;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
end;

procedure TFRelExtratoEmp.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_F5  then ButListaEmp.Click;
  if key = vk_F11 then ButMarcDesm_Emp.Click;
  if key = vk_F8  then ButMarcaTodos_Emp.Click;
  if key = vk_f9  then ButDesmTodos_Emp.Click;
  if key = vk_F2  then but_MarcDem_ForClick(nil);
  if key = vk_F3  then But_MarcaT_ForClick(nil);
  if key = vk_F4  then But_DesmT_ForClick(nil);
end;

procedure TFRelExtratoEmp.but_Abre_ForClick(Sender: TObject);
begin
QBuscaFornec.Open;
QBuscaFornec.First;
QFornec.Open;
QFornec.EmptyTable;
QFornec.DisableControls;
while not QBuscaFornec.Eof do begin
   QFornec.Append;
   QFornecCRed_id.AsInteger := QBuscaFornecCRED_ID.AsInteger;
   QFornecFantasia.AsString := QBuscaFornecFANTASIA.AsString;
   QFornecRazao.AsString    := QBuscaFornecNOME.AsString;
   QFornecSegmento.AsString := QBuscaFornecSEGMENTO.AsString;
   QFornecMarcado.AsBoolean := False;
   QFornec.Post;
   QBuscaFornec.Next;
end;
QBuscaFornec.Close;
QFornec.First;
QFornec.EnableControls;
fornec_sel := EmptyStr;
DBGrid1.SetFocus;
end;

procedure TFRelExtratoEmp.Fornecedores_Sel;
var marca : TBookmark;
begin
  fornec_sel := EmptyStr;
  marca := QFornec.GetBookmark;
  QFornec.DisableControls;
  QFornec.First;
  while not qfornec.eof do begin
    if QFornecMarcado.AsBoolean then fornec_sel := fornec_sel + ','+QFornecCRed_id.AsString;
    QFornec.Next;
  end;
  QFornec.GotoBookmark(marca);
  QFornec.FreeBookmark(marca);
  if fornec_sel <> '' then fornec_sel := Copy(fornec_sel,2,Length(fornec_sel));
  QFornec.EnableControls;
end;

procedure TFRelExtratoEmp.But_Fecha_ForClick(Sender: TObject);
begin
  QFornec.Close;
  fornec_sel := EmptyStr;
end;

procedure TFRelExtratoEmp.but_MarcDem_ForClick(Sender: TObject);
begin
  if QFornec.IsEmpty then Exit;
  QFornec.Edit;
  QFornecMarcado.AsBoolean := not QFornecMarcado.AsBoolean;
  QFornec.Post;
  Fornecedores_Sel;
end;

procedure TFRelExtratoEmp.But_MarcaT_ForClick(Sender: TObject);
var marca : TBookmark;
begin
  if QFornec.IsEmpty then Exit;
  QFornec.DisableControls;
  marca := QFornec.GetBookmark;
  QFornec.First;
  while not qfornec.eof do begin
    QFornec.Edit;
    QFornecMarcado.AsBoolean := true;
    QFornec.Post;
    QFornec.Next;
  end;
  QFornec.GotoBookmark(marca);
  QFornec.FreeBookmark(marca);
  QFornec.EnableControls;
  Fornecedores_Sel;
end;

procedure TFRelExtratoEmp.But_DesmT_ForClick(Sender: TObject);
var marca : TBookmark;
begin
  if QFornec.IsEmpty then Exit;
  QFornec.DisableControls;
  marca := QFornec.GetBookmark;
  QFornec.First;
  while not qfornec.eof do begin
    QFornec.Edit;
    QFornecMarcado.AsBoolean := false;
    QFornec.Post;
    QFornec.Next;
  end;
  QFornec.GotoBookmark(marca);
  QFornec.FreeBookmark(marca);
  QFornec.EnableControls;
  Fornecedores_Sel;
end;

procedure TFRelExtratoEmp.DBGrid1DblClick(Sender: TObject);
begin
  but_MarcDem_ForClick(nil);
end;

procedure TFRelExtratoEmp.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then but_MarcDem_ForClick(nil);
end;

procedure TFRelExtratoEmp.FormCreate(Sender: TObject);
begin
  inherited;
  DataFecha.Date := Date;
  DMConexao.Config.Open;
  filtrarEntrNF := (DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'S');
  acumularAutor := (DMConexao.ConfigACUMULA_AUTOR_PROX_FECHA.AsString = 'S');
  MostrarRelatorio := False;
  DMConexao.Config.Close;
  if not filtrarEntrNF then begin
    rgpNFEntregue.Visible := False;
    pnlOutrasOpcoes.Left := rgpNFEntregue.Left - 4;
  end;
  qLkpEmp.Open;
  dbLkpCidade.KeyValue := 0;
  QOperadores.Open;
  dbLkpOperadores.KeyValue := '<TODOS>';
end;

procedure TFRelExtratoEmp.DataFechaChange(Sender: TObject);
begin
  QReadOnlyEmpresas.Close;
  QGruposemp.Close;
end;

procedure TFRelExtratoEmp.BitBtn1Click(Sender: TObject);
var
  Imp: TImpres;
  marca : TBookmark;
  tot_emp : currency;
  linha, lpage, pag, i : integer;
  grupos_nao_sel, sParteCab : string;
  periodos : TPeriodos;
  periodo  : TPeriodo;
begin  // se voltar para o rel antigo, tirar esse begin
  btn2 := False;
  linha := 0;
  total_rel:= 0;
  total_func:= 0;
  if ChkTotMes.Checked then begin
     periodos := QuebraEmPeriodos(DataFecha.Date,datafin.Date);
  end;
  marca := QReadOnlyEmpresas.GetBookmark;
  QReadOnlyEmpresas.First;
  screen.cursor := crHourGlass;
  DMConexao.Adm.Open;
  imp := TImpres.Create;
  imp.quebrapagina  := True;
  imp.tituloemtodas := True;
  imp.AddTitulo(sLineBreak);
  imp.AddTitulo(imp.Centraliza(DMConexao.AdmFANTASIA.AsString));
  imp.AddTitulo('');
  if por_dia_fecha.Checked then begin
    periodo := DMConexao.ObterPeriodoEmpresa(MEmpresasempres_id.AsInteger,DataFecha.Date);
    Imp.AddTitulo(imp.Centraliza('Extrato da empresa para o fechamento ' + FormatDataBR(DataFecha.Date)+' ref. periodo de '+FormatDataBR(periodo.DataIni)+' a '+FormatDataBR(periodo.DataFin)));
  end
  else
    Imp.AddTitulo(imp.Centraliza('Extrato da empresa no periodo de '+FormatDataBR(QReadOnlyEmpresasdataini.AsDateTime)+' a '+FormatDataBR(QReadOnlyEmpresasdatafin.AsDateTime)));

  while not QReadOnlyEmpresas.Eof do begin
     AbrirCCorrente;
     if not Query1.IsEmpty then begin
        if Imp.LinhasCount > 0 then
           Imp.novapagina(True);
        if RGReceita.ItemIndex = 1 then
           sParteCab := 'Somente autorizacoes com receita'
        else if RGReceita.ItemIndex = 2 then
           sParteCab := 'Somente autorizacoes sem receita';
        if filtrarEntrNF then
           if rgpNFEntregue.ItemIndex = 1 then
           begin
              if sParteCab <> '' then
                 sParteCab := sParteCab + ' e confirmadas'
              else
                 sParteCab := 'Somente autorizacoes confirmadas';
           end
           else if rgpNFEntregue.ItemIndex = 2 then
           begin
              if sParteCab <> '' then
                 sParteCab := sParteCab + ' e nao confirmadas'
              else
                 sParteCab := 'Somente autorizacoes nao confirmadas';
           end;
        if sParteCab <> '' then
           Imp.AddTitulo(Centraliza(sParteCab, ' ', 128), linha, 3);
        imp.SaltarLinhas(1);

        ImpCabecalho(Imp);
        ImpCorpo(Imp);
        ImpRodape(Imp);
        Query1.Close;
     end;
     QReadOnlyEmpresas.Next;
  end;

  if CKTotFinal.Checked then begin
     Imp.novapagina(True);
     Imp.SaltarLinhas(1);
     Imp.AddLinha('Totalizacao do Relatorio');
     Imp.AddLinhaSeparadora;
     Imp.AddLinha('Totalizacao da Funcionarios no Periodo: '+IntToStr(total_func),3);
     Imp.AddLinha('Valor Total do Periodo:                 '+FormatFloat('###,###,##0.00',total_rel),3);
  end;

  QReadOnlyEmpresas.GotoBookmark(marca);
  QReadOnlyEmpresas.FreeBookmark(marca);
  imp.Imprimir();
  imp.Free;
  DMConexao.Adm.Close;

  screen.Cursor := crDefault;

end;

procedure TFRelExtratoEmp.JvDBGrid1DblClick(Sender: TObject);
begin
  ButMarcDesm_EmpClick(nil);
end;

procedure TFRelExtratoEmp.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case Key  of
      VK_RETURN : ButMarcDesm_EmpClick(nil);
      VK_LEFT   : JvDBGrid1.Perform(WM_HSCROLL,0,0);
      VK_RIGHT  : JvDBGrid1.Perform(WM_HSCROLL,1,0);
   end;
   if Key in [vk_left,vk_right] then Key := 0;
end;

procedure TFRelExtratoEmp.FormShow(Sender: TObject);
begin
  inherited;
  DataFecha.SetFocus;
end;

procedure TFRelExtratoEmp.por_dia_fechaClick(Sender: TObject);
begin
  por_dia_fecha.Checked := True;
  datafin.Visible   := False;
  label2.Caption    := 'Dia de Fechamento';
  //ButListaEmp.Left  := 320;
  por_periodo.Checked := False;
  ChkTotMes.Enabled  := False;
  ChkTotMes.Checked  := False;
  DataFecha.SetFocus;
end;

procedure TFRelExtratoEmp.por_periodoClick(Sender: TObject);
begin
  por_periodo.Checked := True;
  datafin.Visible   := True;
  datafin.Date      := DataFecha.Date;
  label2.Caption    := 'Per�odo de Compra';
  //ButListaEmp.Left  := 440;
  por_dia_fecha.Checked := False;
  ChkTotMes.Enabled  := True;
  DataFecha.SetFocus;
end;

procedure TFRelExtratoEmp.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
    if Pos(Field.FieldName,QReadOnlyEmpresas.Sort) > 0 then begin
       if Pos(' DESC',QReadOnlyEmpresas.Sort) > 0 then QReadOnlyEmpresas.Sort := Field.FieldName
                                                  else QReadOnlyEmpresas.Sort := Field.FieldName+' DESC';
    end
    else QReadOnlyEmpresas.Sort := Field.FieldName;
    except
   end;

   QReadOnlyEmpresas.First;

   MEmpresas.Open;
   MEmpresas.EmptyTable;
   MEmpresas.DisableControls;
   while not QReadOnlyEmpresas.Eof do begin
      MEmpresas.Append;
      MEmpresasempres_id.AsInteger   := QReadOnlyEmpresasempres_id.AsInteger;
      MEmpresasnome.AsString         := QReadOnlyEmpresasnome.AsString;
      MEmpresasfantasia.AsString     := QReadOnlyEmpresasfantasia.AsString;
      MEmpresasdesconto_emp.AsFloat  := QReadOnlyEmpresasdesconto_emp.AsFloat;
      MEmpresasdataini.AsDateTime    :=  QReadOnlyEmpresasdataini.AsDateTime;
      MEmpresasdatafin.AsDateTime    := QReadOnlyEmpresasdatafin.AsDateTime;
      MEmpresasdatavenc.AsDateTime   := QReadOnlyEmpresasdatavenc.AsDateTime;
      MEmpresasdatafecha.AsDateTime  := QReadOnlyEmpresasdatafecha.AsDateTime;
      MEmpresasMarcado.AsBoolean     := False;
      MEmpresas.Post;
      QReadOnlyEmpresas.Next;
   end;
   MEmpresas.First;
   MEmpresas.EnableControls;
   empres_sel := EmptyStr;

end;

procedure TFRelExtratoEmp.QGruposEmpBeforePost(DataSet: TDataSet);
begin
  inherited;
  if not (QGruposempIMPRIME.AsString[1] in ['S','N']) then
     QGruposempIMPRIME.AsString := 'N';
end;

procedure TFRelExtratoEmp.JvDBGrid3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ((key = vk_return) and (QGruposemp.State = dsEdit)) then QGruposemp.Post;
end;

function TFRelExtratoEmp.Grupos_emp_id_nao_sel(Empres_id:integer):string;
begin
Result := '';
QGruposemp.First;
while not QGruposemp.Eof do begin
   if QGruposempIMPRIME.AsString = 'N'  then
      Result := Result+','+QGruposempGRUPO_CONV_EMP_ID.AsString;
   QGruposemp.Next;
end;
if Result <> '' then
   Delete(Result,1,1);
end;

procedure TFRelExtratoEmp.SomaPeriodos(var Periodos:TPeriodos);
var i, Posicao : integer; s, s2:string;
begin
  Posicao := Pos('GROUP BY', AnsiUpperCase(Query1.Sql.Text));
  if Posicao = 0 then begin
    for i := 0 to length(Periodos)-1 do
      Periodos[i].Valor := 0;
  end else begin
    s2 := Copy(Query1.Sql.Text, 1, Posicao - 1);
    DMConexao.Q.Close;
    DMConexao.Q.Sql.Text := s2;
    DMConexao.Q.Sql[1] := '';                          // Elimina o texto: ' ,conveniados.conv_id, conveniados.chapa, conveniados.titular, conveniados.limite_mes '
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1); // Desabilitado porque n�o considerava as diversas op��es de filtro deste relat�rio
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1);
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1);
    for i := 0 to length(Periodos)-1 do begin
      s := ' where contacorrente.data between '+formatdataIB(Periodos[i].DataIni)+' and '+formatdataIB(Periodos[i].DataFin);
      if ChkDemit.Checked then
         DMConexao.Q.Sql[5] := s                     // where contacorrente.data ...
      else
         DMConexao.Q.Sql[4] := s;                    // where contacorrente.data ...
      DMConexao.Q.Open;
      Periodos[i].Valor := DMConexao.Q.Fields[0].AsCurrency;
      DMConexao.Q.Close;
    end;
  end;
end;

procedure TFRelExtratoEmp.SomaPeriodos2(var Periodos:TPeriodos ; emp_id : Integer);
var i, Posicao : integer; s, s2:string;
begin
  Posicao := Pos('GROUP BY', AnsiUpperCase(Query1.Sql.Text));
  if Posicao = 0 then begin
    for i := 0 to length(Periodos)-1 do
      Periodos[i].Valor := 0;
  end else begin
    s2 := Copy(Query1.Sql.Text, 1, Posicao - 1);
    DMConexao.Q.Close;
    DMConexao.Q.Sql.Text := s2;
    DMConexao.Q.Sql[1] := '';                          // Elimina o texto: ' ,conveniados.conv_id, conveniados.chapa, conveniados.titular, conveniados.limite_mes '
//    ClipBoard.AsText := Dm1.Query1.SQL.Text;
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1); // Desabilitado porque n�o considerava as diversas op��es de filtro deste relat�rio
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1);
    // Dm1.Query1.Sql.Delete(Dm1.Query1.Sql.Count-1);
    for i := 0 to length(Periodos)-1 do begin
      s := ' where contacorrente.data between '+formatdataIB(Periodos[i].DataIni)+' and '+formatdataIB(Periodos[i].DataFin);
      if ChkDemit.Checked then
         DMConexao.Q.Sql[5] := s                     // where contacorrente.data ...
      else
         DMConexao.Q.Sql[4] := s;                    // where contacorrente.data ...
      DMConexao.Q.Parameters[0].Value := emp_id;
//      ClipBoard.AsText := Dm1.Query1.SQL.Text;
      DMConexao.Q.Open;
      Periodos[i].Valor := DMConexao.Q.Fields[0].AsCurrency;
      DMConexao.Q.Close;
    end;
  end;
end;

procedure TFRelExtratoEmp.QReadOnlyEmpresasAfterOpen(DataSet: TDataSet);
begin
  inherited;
  AbrirGrupos;
end;

procedure TFRelExtratoEmp.AbrirGrupos;
begin
  QGruposemp.Close;
  if not MEmpresas.IsEmpty then begin
     QGruposemp.SQL.Text := ' Select grupo_conv_emp.*, ''S'' as imprime from grupo_conv_emp ';
     QGruposemp.Open;
  end;
  FiltrarGrupos;
end;

//Necess�rio usar filtro para manter as edi��es em cache.
procedure TFRelExtratoEmp.FiltrarGrupos;
begin
  if MEmpresasEMPRES_ID.AsString <> '' then
  begin
    QGruposemp.DisableControls;
    QGruposemp.Filtered := False;
    QGruposemp.Filter   := ' empres_id = '+MEmpresasEMPRES_ID.AsString;
    QGruposemp.Filtered := True;
    QGruposemp.EnableControls;
  end;
end;


procedure TFRelExtratoEmp.QReadOnlyEmpresasAfterScroll(DataSet: TDataSet);
var i : integer;
    desconto : Currency;
    periodos : TPeriodos;
begin
  inherited;
  FiltrarGrupos;
  if MostrarRelatorio then begin
//    if not DataSet.Eof then
//      Marcados.Add(DataSet.FieldByName('MARCADO').AsString);
    if DataSet.FieldByName('MARCADO').AsString = 'S' then begin
      Query1.Close;
      Query1.Parameters[0].Value := QReadOnlyEmpresasEMPRES_ID.Value;
      Query1.Open;
      if (ChkTotMes.Checked) then begin
        if sl = nil then
          sl := TStringList.Create
        else
          sl.Clear;
        if (ChkTotMes.Checked) and not (DataSet.Eof) then begin
          periodos := QuebraEmPeriodos(DataFecha.Date,datafin.Date);
          SomaPeriodos2(periodos,DataSet.FieldByName('EMPRES_ID').AsInteger);
        end;
        for i := 0 to Length(periodos)-1 do
          sl.Add('Mes '+FormatFloat('00',periodos[i].Mes)+'/'+IntToStr(periodos[i].Ano)+' Total: '+Direita(FormatFloat('###,###,##0.00',periodos[i].Valor),' ',10)+'  periodo somado: '+FormatDataBR(periodos[i].DataIni)+ ' a '+FormatDataBR(periodos[i].DataFin));
        StringDS.RangeEnd := reCount;
        StringDS.RangeEndCount := sl.Count;
        frxReport1.Variables['TamHMes'] := 25 * sl.Count;
      end;
      if (CKTotFinal.Checked) and not (DataSet.Eof)  then begin
        Valor_TotalComDesconto := Valor_TotalComDesconto + CalculaTotal(QReadOnlyEmpresasEMPRES_ID.Value);
        Valor_Total := Valor_Total + CalculaTotal(QReadOnlyEmpresasEMPRES_ID.Value);
        if QReadOnlyEmpresasDESCONTO_EMP.Value = 0 then
          desconto := 1
        else
          desconto := QReadOnlyEmpresasDESCONTO_EMP.Value;
        Valor_TotalComDesconto := Valor_TotalComDesconto - ((Valor_TotalComDesconto*desconto)/100);
        total_func := total_func + Query1.RecordCount;
        frxReport1.Variables['ValorTotalComDesconto'] := Valor_TotalComDesconto;
        frxReport1.Variables['ValorTotal'] := Valor_Total;
        frxReport1.Variables['TotalFunc'] := total_func;
      end;
//      ClipBoard.AsText := Query1.SQL.Text;
    end;
  end;
end;

procedure TFRelExtratoEmp.JvDBGrid3Exit(Sender: TObject);
begin
  inherited;
  if QGruposemp.state = dsEdit then
     QGruposemp.Post;
end;

procedure TFRelExtratoEmp.ImpCabecalho(imp: TImpres);
var linha:integer;
begin
  Imp.AddLinha('Empresa: '+PadL(QReadOnlyEmpresasEmpres_id.AsString,4,' ')+' - Razao: '+QReadOnlyEmpresasNome.AsString+' Fant.: '+QReadOnlyEmpresasFANTASIA.AsString,3);
  Imp.AddLinhaSeparadora;
  if CKUsarCodConvEmp.Checked then
     linha := Imp.AddLinha('Codigo',3)
  else
     linha := Imp.AddLinha('Conv ID',3);
  Imp.AddLinha('Chapa',13,linha);
  Imp.AddLinha('Titular',30,linha);
  Imp.AddLinha('Valor',82,linha);
  if ChkMostraLim.Checked then
     Imp.AddLinha('Limite',100,linha);
end;

procedure TFRelExtratoEmp.ImpCorpo(imp: TImpres);
var linha:integer;
begin
  Query1.First;
  tot_emp:= 0;
  while not Query1.Eof do begin
     if Query1VALOR.AsCurrency <> 0 then begin
        if CKUsarCodConvEmp.Checked then
           linha:= imp.AddLinha(imp.Direita(Query1COD_EMPRESA.AsString,7),3)
        else
           linha:= imp.AddLinha(imp.Direita(Query1CONV_ID.AsString,7),3);
        Imp.AddLinha(Query1CHAPA.AsString ,13,linha);
        Imp.AddLinha(Query1TITULAR.AsString,30,linha);
        Imp.AddLinha(imp.Direita(FormatFloat('###,###,##0.00',Query1VALOR.AsCurrency),10,' '),77,linha);
        if ChkMostraLim.Checked then
          Imp.AddLinha(imp.Direita(FormatFloat('###,###,##0.00',Query1LIMITE_MES.AsCurrency),10,' '),96,linha);
        tot_emp := tot_emp + Query1VALOR.AsCurrency;
        total_rel:= total_rel + Query1VALOR.AsCurrency;
     end;
     Query1.Next;
  end;
end;

procedure TFRelExtratoEmp.ImpRodape(imp: TImpres);
var linha, i:integer;
periodos : TPeriodos;
begin
  if ChkTotMes.Checked then begin
     periodos := QuebraEmPeriodos(DataFecha.Date,datafin.Date);
  end;
  Imp.AddLinhaSeparadora;
  linha:= Imp.AddLinha('Totalizacao da Empresa ',3);
  Imp.AddLinha('Titulares '+IntToStr(Query1.RecordCount),28,linha);
  total_func:= total_func + Query1.RecordCount;
  Imp.AddLinha('Valor Total',47,linha);
  Imp.AddLinha(Direita(FormatFloat('###,###,##0.00',tot_emp),' ',12),75,linha);

  if ChkDescEmp.Checked and (QReadOnlyEmpresasDESCONTO_EMP.AsFloat > 0) then begin
     linha:= Imp.AddLinha('Desc. Empresa. '+FormatFloat('##0.00 %',QReadOnlyEmpresasDESCONTO_EMP.AsFloat),47);
     Imp.AddLinha(Direita(FormatFloat('###,###,##0.00',(tot_emp/100)*QReadOnlyEmpresasDESCONTO_EMP.AsFloat),' ',12),75,linha);
     tot_emp := tot_emp - ((tot_emp/100)*QReadOnlyEmpresasDESCONTO_EMP.AsFloat);
     linha:= Imp.AddLinha('Valor Liq.',linha,47);
     Imp.AddLinha(Direita(FormatFloat('###,###,##0.00',tot_emp),' ',12),75,linha);
  end;
  if ChkTotMes.Checked then begin
     SomaPeriodos(periodos);
     Imp.AddLinha('Totalizacao por Mes:',3);
     for i := 0 to Length(periodos)-1 do begin
         Imp.AddLinha('Mes '+FormatFloat('00',periodos[i].Mes)+'/'+IntToStr(periodos[i].Ano)+' Total: '+Direita(FormatFloat('###,###,##0.00',periodos[i].Valor),' ',10)+'  periodo somado: '+FormatDataBR(periodos[i].DataIni)+ ' a '+FormatDataBR(periodos[i].DataFin),3);
     end;
  end;
end;

procedure TFRelExtratoEmp.AbrirCCorrente(excel : boolean);
var grupos_nao_sel:string;
    sql : String;
    bm : TBookmark;
begin
  Application.ProcessMessages;
  if (excel) then begin
    sql := '';
    MEmpresas.DisableControls;
    bm := MEmpresas.GetBookmark;

    while not MEmpresas.Eof do begin
      if (MEmpresasMARCADO.AsBoolean = true) then begin
        sql := sql + ' or ';
        sql := sql + ' conveniados.empres_id = '+MEmpresasEmpres_id.AsString;
      end;
      MEmpresas.Next;
    end;
    if sql = '' then begin
      msgInf('Selecione ao menos uma empresa');
      MEmpresas.EnableControls;
      MEmpresas.GotoBookmark(bm);
      MEmpresas.FreeBookmark(bm);
      Abort;
    end;
    sql := Copy(sql,5,length(sql)-4);
    sql := ' and (' + sql + ')';
    MEmpresas.GotoBookmark(bm);
    MEmpresas.EnableControls;
  end;
  //if QReadOnlyEmpresasMarcado.AsString = 'S' then begin
  Query1.Close;
  Query1.Sql.Clear;
  // N�o alterar as linhas da sql, pq afetam outra rotina. Ver rotina: SomaPeriodos
  Query1.Sql.Add(' select sum(contacorrente.debito-contacorrente.credito) as Valor ');
  Query1.Sql.Add(' ,conveniados.conv_id, conveniados.chapa, conveniados.titular, conveniados.limite_mes, conveniados.cod_empresa ');
  Query1.Sql.Add(' from contacorrente ');
  Query1.Sql.Add(' join conveniados on (conveniados.conv_id = contacorrente.conv_id) ');

  if ChkDemit.Checked then
    Query1.Sql.Add(' left join conv_detail on conv_detail.conv_id = conveniados.conv_id ');

  if por_dia_fecha.Checked then
    Query1.SQL.Add(' where contacorrente.data_fecha_emp = ' + FormatDataIB(MEmpresasDATAFECHA.AsDateTime))
  else
    Query1.Sql.Add(' where contacorrente.data between '+FormatDataIB(MEmpresasdataini.AsDateTime)+' and '+FormatDataIB(MEmpresasdatafin.AsDateTime));

  if btn2 then
    Query1.Sql.Add(' and conveniados.empres_id = :empres_id')
  else
    if (excel) then
      Query1.Sql.Add(sql)
    else
      Query1.Sql.Add(' and conveniados.empres_id = '+MEmpresasEmpres_id.AsString);
  grupos_nao_sel := Grupos_emp_id_nao_sel(MEmpresasEMPRES_ID.AsInteger);
  if grupos_nao_sel  <> '' then
     Query1.Sql.Add(' and ('+DMConexao.fnDivideComandoINSQL(grupos_nao_sel,'conveniados.grupo_conv_emp',True)+')');
  if RGBaixa.ItemIndex = 0 then
     Query1.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') = ''N'' ')
  else if RGBaixa.ItemIndex = 2 then
     Query1.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') = ''S'' ');

  if fornec_sel <> '' then
     Query1.Sql.Add(' and ('+DMConexao.fnDivideComandoINSQL(fornec_sel,'contacorrente.cred_id')+') ');

  if RGReceita.ItemIndex = 1 then // Somente com receita
     Query1.Sql.Add(' and contacorrente.receita = ''S'' ')
  else if RGReceita.ItemIndex = 2 then // Somente com receita
     Query1.Sql.Add(' and (contacorrente.receita = ''N'' or contacorrente.receita is null )');

  if filtrarEntrNF then
    if rgpNFEntregue.ItemIndex = 1 then // Somente autorizacoes confirmadas
      Query1.SQL.Add(' and contacorrente.entreg_nf = ''S'' ')
    else if rgpNFEntregue.ItemIndex = 2 then  // Somente autorizacoes nao confirmadas
      Query1.SQL.Add(' and (contacorrente.entreg_nf = ''N'' or contacorrente.entreg_nf is null) ');

  DMConexao.Config.Open; //Remove da sql o credenciado de baixa.
  if not DMConexao.ConfigCOD_CRED_BAIXA.IsNull then
     Query1.SQL.Add(' and contacorrente.cred_id <> '+DMConexao.ConfigCOD_CRED_BAIXA.AsString);
  DMConexao.Config.Close;
  if ChkDemit.Checked then
     Query1.Sql.Add(' and conv_detail.data_demissao is null ');
  Query1.Sql.Add(' group by conveniados.conv_id, conveniados.chapa, conveniados.titular, conveniados.limite_mes, conveniados.cod_empresa ');
  // Query1.Sql.Add(' having sum(contacorrente.debito-contacorrente.credito) > 0 ');
  if ComboOrdem.Text = 'Valor' then Query1.Sql.Add(' order by sum(contacorrente.debito-contacorrente.credito)')
                               else Query1.Sql.Add(' order by '+ComboOrdem.Text);
  if CheckOrdem.checked then Query1.Sql.Add(' desc ');
  Query1.Open;
   //end;
end;


procedure TFRelExtratoEmp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  sl.Free;
end;

procedure TFRelExtratoEmp.frxReport1Preview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := True;
end;

procedure TFRelExtratoEmp.frxReport1ClosePreview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := False;
end;

procedure TFRelExtratoEmp.frxReport1GetValue(const VarName: String;
  var Value: Variant);
var periodo : TPeriodo;
begin
  inherited;
  if CompareText(VarName, 'meses') = 0 then
    Value := sl.Text;
  if CompareText(VarName, 'Titulo') = 0 then
    Value := DMConexao.AdmFANTASIA.AsString;
  if CompareText(VarName, 'Periodo') = 0 then begin
    if por_dia_fecha.Checked then begin
      periodo := DMConexao.ObterPeriodoEmpresa(MEmpresasempres_id.AsInteger,DataFecha.Date);
      Value := 'Extrato da empresa para o fechamento ' + FormatDataBR(DataFecha.Date)+' ref. periodo de '+FormatDataBR(periodo.DataIni)+' a '+FormatDataBR(periodo.DataFin);
    end else
      Value := 'Extrato da empresa no periodo de '+FormatDataBR(QReadOnlyEmpresasdataini.AsDateTime)+' a '+FormatDataBR(QReadOnlyEmpresasdatafin.AsDateTime);
  end;      
end;

procedure TFRelExtratoEmp.BitBtn2Click(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MEmpresas.Active = False then begin
    msgInf('Clique em ''Listar empresas'' para podermos gerar o relat�rio');
    Exit;
  end;
  if DMConexao.ContaMarcados(MEmpresas) = 0 then begin
    msgInf('Nenhuma empresa selecionada');
    pageControl1.ActivePageIndex := 0;
    JvDBGrid1.SetFocus;
    Exit;
  end;
  total_func := 0;
  DMConexao.Adm.Open;

  btn2 := True;
  AbrirCCorrente;
  marca := MEmpresas.GetBookmark;
  Query1.Close;
  if not cbSoMontar.Checked then begin
    frxReport1.Variables['MostraLim']      := ChkMostraLim.Checked;
    frxReport1.Variables['Demit']          := ChkDemit.Checked;
    frxReport1.Variables['DescEmp']        := ChkDescEmp.Checked;
    frxReport1.Variables['UsarCodConvEmp'] := CKUsarCodConvEmp.Checked;
    frxReport1.Variables['TotFinal']       := CKTotFinal.Checked;
    frxReport1.Variables['TotMes']         := ChkTotMes.Checked;
    //frxReport1.Variables['Titulo']         := DM1.AdmFANTASIA.AsString;
  end;
  Valor_Total := 0;
  Valor_TotalComDesconto := 0;
  Query1.Open;

  if not cbSoMontar.Checked then begin
    frxReport1.ShowReport;
  end;

  MEmpresas.Open;
  MEmpresas.First;
  if cdsQuery1.Active then
    cdsQuery1.Close;
  cdsQuery1.CreateDataSet;
  cdsQuery1.Open;
  while not MEmpresas.Eof do begin
    if cbSoMontar.Checked then begin
      if MEmpresasMARCADO.AsBoolean = true then begin
        Query1.Close;
        Query1.Parameters[0].Value := MEmpresasEMPRES_ID.Value;
        Query1.Open;
        while not Query1.Eof do begin
          cdsQuery1.Append;
          cdsQuery1CHAPA.AsString      := Query1CHAPA.AsString;
          cdsQuery1LIMITE_MES.AsString := Query1LIMITE_MES.AsString;
          cdsQuery1TITULAR.AsString    := Query1TITULAR.AsString;
          cdsQuery1VALOR.AsString      := Query1VALOR.AsString;
          cdsQuery1.Post;
          Query1.Next;
        end;
      end;
    end;
    MEmpresas.Next;
  end;
  //QReadOnlyEmpresas.Eof
  MEmpresas.GotoBookmark(marca);
  DMConexao.Adm.Close;



end;

procedure TFRelExtratoEmp.bntGerarPDFClick(Sender: TObject);
begin
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BitBtn2.Click;
    frxReport1.PrepareReport();
    frxReport1.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TFRelExtratoEmp.BitBtn3Click(Sender: TObject);
begin
  inherited;
  AbrirCCorrente(true);
  PageControl1.ActivePageIndex := 2;
  dbGridExtratoEmp.PopupMenu.Items[2].Click;
end;

procedure TFRelExtratoEmp.JvDBGrid3TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  DMConexao.SortZQuery(QGruposEmp,Field.FieldName);
end;



procedure TFRelExtratoEmp.MEmpresasAfterOpen(DataSet: TDataSet);
begin
  inherited;
  //AbrirGrupos;
end;

procedure TFRelExtratoEmp.MEmpresasAfterScroll(DataSet: TDataSet);
var i : integer;
    desconto : Currency;
    periodos : TPeriodos;
begin
  inherited;
  FiltrarGrupos;
  if MostrarRelatorio then begin
    if DataSet.FieldByName('MARCADO').AsBoolean = true then begin
      Query1.Close;
      Query1.Parameters[0].Value := MEmpresasEMPRES_ID.Value;
      Query1.Open;
      if (ChkTotMes.Checked) then begin
        if sl = nil then
          sl := TStringList.Create
        else
          sl.Clear;
        if (ChkTotMes.Checked) and not (DataSet.Eof) then begin
          periodos := QuebraEmPeriodos(DataFecha.Date,datafin.Date);
          SomaPeriodos2(periodos,DataSet.FieldByName('EMPRES_ID').AsInteger);
        end;
        for i := 0 to Length(periodos)-1 do
          sl.Add('Mes '+FormatFloat('00',periodos[i].Mes)+'/'+IntToStr(periodos[i].Ano)+' Total: '+Direita(FormatFloat('###,###,##0.00',periodos[i].Valor),' ',10)+'  periodo somado: '+FormatDataBR(periodos[i].DataIni)+ ' a '+FormatDataBR(periodos[i].DataFin));
        StringDS.RangeEnd := reCount;
        StringDS.RangeEndCount := sl.Count;
        frxReport1.Variables['TamHMes'] := 25 * sl.Count;
      end;
      if (CKTotFinal.Checked) and not (DataSet.Eof)  then begin
        Valor_TotalComDesconto := Valor_TotalComDesconto + CalculaTotal(MEmpresasEMPRES_ID.Value);
        Valor_Total := Valor_Total + CalculaTotal(MEmpresasEMPRES_ID.Value);
        if MEmpresasDESCONTO_EMP.Value = 0 then
          desconto := 1
        else
          desconto := MEmpresasDESCONTO_EMP.Value;
        Valor_TotalComDesconto := Valor_TotalComDesconto - ((Valor_TotalComDesconto*desconto)/100);
        total_func := total_func + Query1.RecordCount;
        frxReport1.Variables['ValorTotalComDesconto'] := Valor_TotalComDesconto;
        frxReport1.Variables['ValorTotal'] := Valor_Total;
        frxReport1.Variables['TotalFunc'] := total_func;
      end;
//      ClipBoard.AsText := Query1.SQL.Text;
    end;
  end;

end;

end.
