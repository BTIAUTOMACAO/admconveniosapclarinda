unit URelExtratoAlimentacaoMensal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DBCtrls, DB, ADODB,
  frxClass, frxGradient, frxExportPDF, frxDBSet, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvMemoryDataset;

type
  TFrmRelExtratoAlimentacaoMensal = class(TF1)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbLkpEmpresas: TDBLookupComboBox;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    dsEmpresas: TDataSource;
    dbConveniados: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    frxGradientObject1: TfrxGradientObject;
    frxReport1: TfrxReport;
    sd: TSaveDialog;
    qConv: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    JvDBGrid1: TJvDBGrid;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    qBusca: TADOQuery;
    qBuscaCONV_ID: TIntegerField;
    qBuscaTITULAR: TStringField;
    dsBusca: TDataSource;
    BitBtn1: TBitBtn;
    lblFechamento: TLabel;
    dbLbpFechamentos: TDBLookupComboBox;
    qFechamentos: TADOQuery;
    dsFechamentos: TDataSource;
    qFechamentosdata_fechamento: TStringField;
    qFechamentosempres_id: TIntegerField;
    MBusca: TJvMemoryData;
    MBuscaconv_id: TIntegerField;
    MBuscatitular: TStringField;
    MBuscamarcado: TBooleanField;
    Button1: TButton;
    qConvDATA: TDateTimeField;
    qConvAUTORIZACAO_ID: TIntegerField;
    qConvDIGITO: TWordField;
    qConvDATA_FECHA_EMP: TDateTimeField;
    qConvNF: TIntegerField;
    qConvCREDENCIADO: TStringField;
    qConvCONV_ID: TIntegerField;
    qConvCODCARTIMP: TStringField;
    qConvVALOR: TBCDField;
    qConvTITULAR: TStringField;
    qConvNOME: TStringField;
    dbCreditos: TfrxDBDataset;
    qGastos: TADOQuery;
    qGastosCREDITO_ACUMULADO: TBCDField;
    qGastosCREDITO: TBCDField;
    qGastosGASTO: TBCDField;
    qdadosConv: TADOQuery;
    qdadosConvTITULAR: TStringField;
    qdadosConvNOME: TStringField;
    qdadosConvCODCARTIMP: TStringField;
    qdadosConvFECHAMENTO: TStringField;
    dbDadosConv: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure dbLkpEmpresasExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    conv_sel : String;

    { Private declarations }
  public
    procedure Conveniados_Sel;
    { Public declarations }
  end;

var
  FrmRelExtratoAlimentacaoMensal: TFrmRelExtratoAlimentacaoMensal;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFrmRelExtratoAlimentacaoMensal.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  btnVisualizar.Enabled := false;
  btnGerarPDF.Enabled := false;
  dbLkpEmpresas.SetFocus;
  lblFechamento.Visible := false;
  dbLbpFechamentos.Visible := false;
  BitBtn1.Enabled := false;
end;

procedure TFrmRelExtratoAlimentacaoMensal.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  qConv.Close;
  qConv.SQL.Clear;
  qConv.SQL.Add(' select distinct CC.DATA, CC.AUTORIZACAO_ID, CC.DIGITO, CC.DATA_FECHA_EMP, ');
  qConv.SQL.Add(' COALESCE(CC.NF,0) AS NF, coalesce(CRED.FANTASIA, CRED.NOME) as CREDENCIADO,');
  qConv.SQL.Add(' CC.CONV_ID, CAR.CODCARTIMP, coalesce(sum(CC.DEBITO - CC.CREDITO), 0) as VALOR,');
  qConv.SQL.Add(' CONV.TITULAR, EMP.NOME from CONVENIADOS CONV, CONTACORRENTE CC, CREDENCIADOS CRED, CARTOES CAR, EMPRESAS EMP ');
  qConv.SQL.Add(' where (CONV.EMPRES_ID = '+qEmpresasEMPRES_ID.AsString + ')');
  qConv.SQL.Add(' and (CONV.APAGADO <> ''S'') AND CAR.TITULAR = ''S''');
  qConv.SQL.Add(' and (CC.DATA_FECHA_EMP in (''' + dbLbpFechamentos.KeyValue + '''))');
  qConv.SQL.Add(' and (CC.CONV_ID = CONV.CONV_ID)');
  qConv.SQL.Add(' and (CRED.CRED_ID = CC.CRED_ID)');
  qConv.SQL.Add(' and (CC.CARTAO_ID = CAR.CARTAO_ID)');
  qConv.SQL.Add(' and (COALESCE(CC.BAIXA_CONVENIADO, ''N'') <> ''S'')');
  qConv.SQL.Add(' and conv.conv_id in ('+conv_sel +') AND CONV.EMPRES_ID = EMP.EMPRES_ID');
  qConv.SQL.Add(' group by CC.NF, CC.DATA_FECHA_EMP, CC.CONV_ID, CC.DATA, CC.AUTORIZACAO_ID, CC.DIGITO, CRED.FANTASIA, CRED.NOME, CC.DEBITO,');
  qConv.SQL.Add(' CC.CREDITO, CAR.CODCARTIMP, CONV.TITULAR, EMP.NOME');
  qConv.SQL.Text;
  qConv.Open;

  qGastos.Close;
  qGastos.SQL.Clear;
  qGastos.SQL.Add(' SELECT COALESCE((SELECT SALDO_ACUMULADO_ANT FROM ALIMENTACAO_HISTORICO A WHERE  ');
  qGastos.SQL.Add(' A.TIPO_CREDITO = ''R'' AND A.CONV_ID = AH.CONV_ID');
  qGastos.SQL.Add(' AND A.DATA_FECHAMENTO = CONVERT(DATE,''' + dbLbpFechamentos.KeyValue + ''')),0) CREDITO_ACUMULADO,');
  qGastos.SQL.Add(' COALESCE(SUM(AH.RENOVACAO_VALOR + AH.ABONO_VALOR),0) AS CREDITO, ');
  qGastos.SQL.Add(' COALESCE((SELECT SUM(CC.DEBITO - CC.CREDITO) FROM CONTACORRENTE ');
  qGastos.SQL.Add(' CC WHERE AH.DATA_FECHAMENTO = CC.DATA_FECHA_EMP AND CC.CONV_ID = AH.CONV_ID),0) AS GASTO');
  qGastos.SQL.Add(' FROM ALIMENTACAO_HISTORICO AH');
  qGastos.SQL.Add(' WHERE AH.EMPRES_ID = ' + qEmpresasEMPRES_ID.AsString );
  qGastos.SQL.Add(' AND AH.CONV_ID = ' + conv_sel);
  qGastos.SQL.Add(' AND AH.DATA_FECHAMENTO = CONVERT(DATE,''' + dbLbpFechamentos.KeyValue + ''')');
  qGastos.SQL.Add(' GROUP BY AH.DATA_FECHAMENTO, AH.CONV_ID');
  qGastos.SQL.Text;
  qGastos.Open;


  qdadosConv.Close;
  qdadosConv.SQL.Clear;
  qdadosConv.SQL.Add(' SELECT CONV.TITULAR, EMP.NOME, CART.CODCARTIMP, ''' + dbLbpFechamentos.KeyValue + ''' AS FECHAMENTO ');
  qdadosConv.SQL.Add(' FROM CONVENIADOS CONV, EMPRESAS EMP, CARTOES CART');
  qdadosConv.SQL.Add(' WHERE CONV.EMPRES_ID = EMP.EMPRES_ID ');
  qdadosConv.SQL.Add(' AND CART.CONV_ID = CONV.CONV_ID AND CART.TITULAR = ''S'' ');
  qdadosConv.SQL.Add(' AND CONV.CONV_ID = ' + conv_sel);
  qdadosConv.SQL.Text;
  qdadosConv.Open;

  if qConv.IsEmpty and qGastos.IsEmpty and qdadosConv.IsEmpty then
  begin
    MsgInf('N�o foi encontrado nenhum lan�amento.');
    dbLkpEmpresas.SetFocus;
    abort;
  end;
    frxReport1.Variables['acumulado_prox']       := (qGastosCREDITO_ACUMULADO.ascurrency + qGastosCREDITO.ascurrency) - qGastosGASTO.ascurrency;
    frxReport1.ShowReport;
end;



procedure TFrmRelExtratoAlimentacaoMensal.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if(dbLbpFechamentos.KeyValue = Null) then begin
    MsgInf('Selecione uma Data de Fechamento.');
    dbLbpFechamentos.SetFocus;
  end else begin
    qBusca.Close;
    qBusca.Parameters.ParamByName('empres_id').Value := qEmpresasEMPRES_ID.AsInteger;
    qBusca.Parameters.ParamByName('data_fechamento').Value := dbLbpFechamentos.KeyValue;
    qBusca.Open;

    if qBusca.IsEmpty then
    begin
      MsgInf('N�o foi encontrado nenhum lan�amento para essa empresa.');
      dbLkpEmpresas.SetFocus;
      btnVisualizar.Enabled := false;
      btnGerarPDF.Enabled := false;
      abort;
    end;

    btnVisualizar.Enabled := true;
    btnGerarPDF.Enabled := true;

    qBusca.First;
    MBusca.Open;
    MBusca.EmptyTable;
    MBusca.DisableControls;
    while not QBusca.Eof do
    begin
      MBusca.Append;
      MBuscaCONV_ID.AsInteger       := QBuscaCONV_ID.AsInteger;
      MBuscaTITULAR.AsString        := QBuscaTITULAR.AsString;
      MBuscaMARCADO.AsBoolean       := False;
      QBusca.Next;
    end;
    MBusca.First;
    MBusca.EnableControls;
    conv_sel := EmptyStr;
end;

end;


procedure TFrmRelExtratoAlimentacaoMensal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_F5 then BitBtn1.Click;
end;

procedure TFrmRelExtratoAlimentacaoMensal.btnGerarPDFClick(Sender: TObject);
begin
  inherited;
  begin
    sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      btnVisualizar.Click;
      frxReport1.Export(frxPDFExport1);
    end;
  end;
end;

procedure TFrmRelExtratoAlimentacaoMensal.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QBusca.Sort) > 0 then begin
  if Pos(' DESC',QBusca.Sort) > 0 then QBusca.Sort := Field.FieldName
                          else QBusca.Sort := Field.FieldName+' DESC';
  end
  else QBusca.Sort := Field.FieldName;
  except
  end;

end;

procedure TFrmRelExtratoAlimentacaoMensal.dbLkpEmpresasExit(
  Sender: TObject);
begin
  inherited;
   qFechamentos.Close;
   qFechamentos.Parameters.ParamByName('empres_id').Value := dbLkpEmpresas.KeyValue;
   qFechamentos.Open;
   if(qFechamentos.RecordCount = 0) then
   begin
      dbLbpFechamentos.Visible := false;
      lblFechamento.Visible := false;
      BitBtn1.Enabled := false;
      MsgInf('N�o � poss�vel exibir o relat�rio. A empresa n�o possui lan�amentos de alimenta��o!');
      dbLkpEmpresas.SetFocus;
   end
   else
      begin
        dbLbpFechamentos.Visible := true;
        lblFechamento.Visible := true;
        dbLbpFechamentos.SetFocus;
        BitBtn1.Enabled := true;
      end;


end;

procedure TFrmRelExtratoAlimentacaoMensal.Button1Click(Sender: TObject);
var existe:boolean;
begin
  inherited;
  existe := false;
  if MBusca.IsEmpty then Exit;
    MBusca.Edit;
    MBuscaMarcado.AsBoolean := not MBuscaMarcado.AsBoolean;
    MBusca.Post;
    while not MBusca.Eof do begin
      if MBuscaMARCADO.AsBoolean  = true then existe := true;
      MBusca.Next;
    end;
    if existe = true then
      Conveniados_Sel
    else
      conv_sel := EmptyStr;
end;

procedure TFrmRelExtratoAlimentacaoMensal.Conveniados_Sel;
var marca : TBookmark;
begin
  if conv_sel = '' then
  begin
    conv_sel := EmptyStr;
    MBusca.DisableControls;
    marca := MBusca.GetBookmark;
    MBusca.First;
    while not MBusca.Eof do begin
      if MBuscaMARCADO.AsBoolean  = true then conv_sel := conv_sel+','+MBuscaCONV_ID.AsString;
      MBusca.Next;
    end;
    conv_sel := Copy(conv_sel,2,Length(conv_sel));
    MBusca.GotoBookmark(marca);
    MBusca.FreeBookmark(marca);
    MBusca.EnableControls;
  end
  else
    begin
        MsgInf('N�o � poss�vel selecionar mais de um Conveniado!');
        conv_sel := EmptyStr;
        if MBusca.IsEmpty then Exit;
        MBusca.DisableControls;
        marca := MBusca.GetBookmark;
        MBusca.First;
        while not MBusca.eof do begin
          MBusca.Edit;
          MBuscaMarcado.AsBoolean := false;
          MBusca.Post;
          MBusca.Next;
        end;
        MBusca.GotoBookmark(marca);
        MBusca.FreeBookmark(marca);
        MBusca.EnableControls;
    end;
end;


procedure TFrmRelExtratoAlimentacaoMensal.JvDBGrid1DblClick(
  Sender: TObject);
begin
  inherited;
  Button1.Click;
end;

end.
