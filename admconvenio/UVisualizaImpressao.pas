unit UVisualizaImpressao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, ComCtrls, StdCtrls, inifiles, printers, JvSpeedbar,
  JvExExtCtrls, JvExtComponent;

type
  TFVisualizaImpressao = class(TForm)
    Toolbar: TJvSpeedBar;
    SpeedbarSection1: TJvSpeedbarSection;
    SpeedbarSection2: TJvSpeedbarSection;
    SpeedbarSection3: TJvSpeedbarSection;
    SpeedbarSection4: TJvSpeedbarSection;
    ExitBtn: TJvSpeedItem;
    StatusBar1: TStatusBar;
    SpeedItem1: TJvSpeedItem;
    SpeedItem2: TJvSpeedItem;
    SaveDialog1: TSaveDialog;
    ListBox1: TListBox;
    PrintDialog1: TPrintDialog;
    procedure ExitBtnClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    arquivo : string;
    procedure Imprimir(Arquivo: String);

  end;

var
  FVisualizaImpressao: TFVisualizaImpressao;

implementation

{$R *.DFM}

function Direita(Texto, Preenchedor: String; Tamanho: Integer): String;
var tamanho2, i: Integer;
    texto2     : String;
begin
   tamanho2 := Tamanho - Length(Texto);

   for i := 1 to tamanho2 do texto2 := texto2 + Preenchedor;

   Result := texto2 + Texto;
end;


procedure TFVisualizaImpressao.Imprimir(Arquivo: String);
var ArquivoTexto: TextFile;
    i : Integer;
    linha : string;
begin
   if Arquivo = '' then begin
      AssignFile(ArquivoTexto, 'LPT1');
      Rewrite(ArquivoTexto);
      end
   else begin
      AssignFile(ArquivoTexto, Arquivo);
      Rewrite(ArquivoTexto);
      end;
   Writeln(ArquivoTexto, #27#48+#15);
   for i := 0 to ListBox1.Count - 1 do begin
      linha := ListBox1.Items[i];
      Writeln(ArquivoTexto,linha);
   end;
   CloseFile(ArquivoTexto);
end;

procedure TFVisualizaImpressao.ExitBtnClick(Sender: TObject);
begin
   Close;
end;

procedure TFVisualizaImpressao.SpeedItem1Click(Sender: TObject);
begin
   if SaveDialog1.Execute then Imprimir(SaveDialog1.FileName);
end;

procedure TFVisualizaImpressao.SpeedItem2Click(Sender: TObject);
begin
Imprimir(arquivo);
end;

procedure TFVisualizaImpressao.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #27 then
  begin
    Close;
  end;
end;

end.
