unit UcadFil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DB, ADODB, Menus, Buttons, ExtCtrls, StdCtrls, Mask,
  DBCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, FOcorrencia, URotinasTexto;

type
  TFCadFil = class(TForm)
    DSCadastro: TDataSource;
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    Minimizar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    PopupGrid1: TPopupMenu;
    PopupCC: TPopupMenu;
    MenuItem1: TMenuItem;
    Exportarparaoexcel2: TMenuItem;
    PopupDesconto: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PopCodAcesso: TPopupMenu;
    ColocarCodAcessoManual1: TMenuItem;
    PopupAlteraID: TPopupMenu;
    AlterarIddoFornecedor1: TMenuItem;
    DuplicarEstabelecimentoalterandoID1: TMenuItem;
    PopupComissaoEmpr: TPopupMenu;
    AltLineardeComissoporEmpresa1: TMenuItem;
    PopupMenu1: TPopupMenu;
    este1: TMenuItem;
    DSEstados: TDataSource;
    QEstados: TADOQuery;
    QEstadosESTADO_ID: TIntegerField;
    QEstadosUF: TStringField;
    QEstadosICMS_ESTADUAL: TFloatField;
    QCadastro: TADOQuery;
    QOcorrencias: TADOQuery;
    QOcorrenciasatendimento_id: TIntegerField;
    QOcorrenciasnome_solicitante: TStringField;
    QOcorrenciastel_solictante: TStringField;
    QOcorrenciasmotivo: TStringField;
    QOcorrenciasdesc_atendimento: TStringField;
    QOcorrenciasdata_atendimento: TDateTimeField;
    QOcorrenciascred_id: TIntegerField;
    QOcorrenciasoperador: TStringField;
    QOcorrenciasSTATUS_ID: TIntegerField;
    DSOcorrencias: TDataSource;
    PopupPOS: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    PageControl1: TPageControl;
    tsGradeFilial: TTabSheet;
    tsCadastroFilial: TTabSheet;
    QCadastroFILIAL_ID: TIntegerField;
    QCadastroBANCO_ID: TIntegerField;
    QCadastroFILIAL_NOME: TStringField;
    QCadastroCODIGO_CONVENIO: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroCOMPLEMENTO: TStringField;
    QCadastroCIDADE: TStringField;
    QCadastroESTADO: TStringField;
    grp1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    dbedtFILIAL_NOME: TDBEdit;
    dbedtCODIGO_CONVENIO: TDBEdit;
    dbedtTIPO_INSCRICAO: TDBEdit;
    dbedtCONTACORRENTE: TDBEdit;
    dbedtNUMERO_INSCRICAO: TDBEdit;
    dbedtAGENCIA: TDBEdit;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    dbedtENDERECO: TDBEdit;
    dbedtENDERECO1: TDBEdit;
    dbedtCIDADE: TDBEdit;
    dbedtCEP: TDBEdit;
    dbedtCOMPLEMENTO: TDBEdit;
    dbedtESTADO: TDBEdit;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Label13: TLabel;
    dbedtFILIAL_ID: TDBEdit;
    Panel2: TPanel;
    Label14: TLabel;
    Label38: TLabel;
    ButBusca: TBitBtn;
    ButAtualiza: TBitBtn;
    EdCod: TEdit;
    EdNome: TEdit;
    DSFilial: TDataSource;
    QFilial: TADOQuery;
    QFilialDESCRICAO: TStringField;
    jvdbgrd1: TJvDBGrid;
    QFilialFILIAL_ID: TIntegerField;
    QCadastroTIPO_INSCRICAO: TIntegerField;
    QCadastroNUMERO_INSCRICAO: TStringField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTACORRENTE: TStringField;
    QCadastroCEP: TStringField;
    QCadastroENDERECO_NUMERO: TStringField;
    PanStatus: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    dbedtSEQUENCIA_ARQUIVO: TDBEdit;
    lbl1: TLabel;
    ADORegraQCadastroSEQUENCIA_ARQUIVO: TIntegerField;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure jvdbgrd1CellClick(Column: TColumn);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ButCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadFil: TFCadFil;

implementation

{$R *.dfm}




procedure TFCadFil.BitBtn2Click(Sender: TObject);
begin
  QCadastro.Append;
  
end;

procedure TFCadFil.BitBtn4Click(Sender: TObject);
var Ocorrencia : TFrmOcorrencia;
begin
  try
    dbedtFILIAL_NOME.Text := fnCompletarCom(dbedtFILIAL_NOME.Text,30,' ',True);
    dbedtAGENCIA.Text := fnCompletarCom(dbedtAGENCIA.Text,6,'0');
    dbedtCONTACORRENTE.Text := fnCompletarCom(dbedtCONTACORRENTE.Text,13,'0');
    dbedtCODIGO_CONVENIO.Text := fnCompletarCom(dbedtCODIGO_CONVENIO.Text,20,' ',True);
    dbedtENDERECO.Text := fnCompletarCom(dbedtENDERECO.Text,30,' ',True);
    dbedtCOMPLEMENTO.Text := fnCompletarCom(dbedtCOMPLEMENTO.Text,15,' ',True);
    dbedtENDERECO1.Text := fnCompletarCom(dbedtENDERECO1.Text,5,'0');
    dbedtCIDADE.Text := fnCompletarCom(dbedtCIDADE.Text,30,' ',True);
    dbedtNUMERO_INSCRICAO.Text := fnCompletarCom(dbedtNUMERO_INSCRICAO.Text,14,' ',True);
    QCadastro.Post;
    QCadastro.Edit;
    ShowMessage('Opera��o executada com sucesso!');
  except
    ShowMessage('Erro ao gravar filial');
  end;

end;

procedure TFCadFil.FormCreate(Sender: TObject);
begin
  QFilial.SQL.Clear;
  QFilial.SQL.Add('SELECT FILIAL_ID, DESCRICAO FROM FILIAL WHERE FILIAL_ID >= 1');
  QFilial.Open;

  if(dbedtFILIAL_ID.Text = '') then begin
    QCadastro.SQL.Clear;
    QCadastro.SQL.Add('SELECT * FROM FILIAL_DADOS_BANCO WHERE FILIAL_ID = 0');
    QCadastro.Open;
  end;

  if QCadastro.IsEmpty then begin
    QCadastro.Append
  end else begin
    QCadastro.Edit;
  end;
end;

procedure TFCadFil.ButBuscaClick(Sender: TObject);
begin
  QFilial.SQL.Clear;
  QFilial.SQL.Add('SELECT FILIAL_ID, DESCRICAO FROM FILIAL  ');
  if(EdCod.Text <> '') then
    QFilial.SQL.Add(' WHERE FILIAL_ID = '+EdCod.Text+' ');
  if ((EdNome.Text <> '') and (EdCod.Text <> '')) then
    QFilial.SQL.Add(' AND DESCRICAO LIKE ''%'+EdNome.Text+'%'' ');
  if ((EdNome.Text <> '') and (EdCod.Text = ''))  then
    QFilial.SQL.Add(' WHERE DESCRICAO LIKE ''%'+EdNome.Text+'%'' ');
  QFilial.Open;
end;

procedure TFCadFil.jvdbgrd1CellClick(Column: TColumn);
begin
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('SELECT * FROM FILIAL_DADOS_BANCO WHERE FILIAL_ID = '+QFilialFILIAL_ID.Text);
  QCadastro.Open;
  if QCadastro.IsEmpty then begin
    QCadastro.Append
  end else begin
    QCadastro.Edit;
  end;
  dbedtFILIAL_ID.Text :=  QFilialFILIAL_ID.AsString;
  PageControl1.ActivePage := tsCadastroFilial;
end;

procedure TFCadFil.BitBtn5Click(Sender: TObject);
begin
  QCadastro.Cancel;
  QCadastro.Edit;
end;

procedure TFCadFil.BitBtn3Click(Sender: TObject);
begin
  QCadastro.Delete;
  ShowMessage('Filial deletada com sucesso');
end;

procedure TFCadFil.ButCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFCadFil.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
