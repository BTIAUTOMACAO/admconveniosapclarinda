object FAltLinSenha: TFAltLinSenha
  Left = 192
  Top = 107
  Width = 312
  Height = 199
  Caption = 'Altera'#231#227'o linear de senha'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroup1: TRadioGroup
    Left = 16
    Top = 16
    Width = 273
    Height = 105
    Caption = 'Alterar senha para:'
    ItemIndex = 0
    Items.Strings = (
      'Alterar para senha padr'#227'o (4 '#250'ltimos n'#250'meros do cart'#227'o)'
      'Alterar para uma senha customizada')
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 64
    Top = 136
    Width = 75
    Height = 25
    Caption = '&Alterar'
    ModalResult = 1
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 160
    Top = 136
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
end
