unit USelTipoImp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFSelTipoImp = class(TForm)
    Bevel1: TBevel;
    ButOk: TButton;
    ButCanc: TButton;
    RadioGroup1: TRadioGroup;
  private
    { Private declarations }
  public
    { Public declarations }
    class function AbrirJanela(Itens:array of string;DefaultItemIndex:Integer=0;FormCaption:String='';GroupCaption:String=''):Integer;
  end;

implementation

{$R *.dfm}

{ TFSelTipoImp }

//Abre a Janela com os itens passados e retorna o index do selecionado, caso cancelado retorna -1
class function TFSelTipoImp.AbrirJanela(Itens: array of string;
  DefaultItemIndex:Integer=0;FormCaption:String='';GroupCaption:String=''): Integer;
  var Form : TFSelTipoImp; i : integer;
begin
  if DefaultItemIndex > Length(Itens)-1 then
     raise Exception.Create('DefaultIndex inv�lido!');
  Form := TFSelTipoImp.Create(nil);
  if Trim(FormCaption) <> '' then
     Form.Caption := FormCaption;
  if Trim(GroupCaption) <> '' then
     Form.RadioGroup1.Caption := GroupCaption;   
  for i := 0 to length(Itens)-1 do
      Form.RadioGroup1.Items.Add(Itens[i]);
  Form.RadioGroup1.ItemIndex := DefaultItemIndex;
  Form.ShowModal;
  if Form.ModalResult = mrCancel then
     Result := -1
  else
     Result := Form.RadioGroup1.ItemIndex;
  Form.Free;
end;


end.
