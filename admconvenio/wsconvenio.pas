// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:2510/wsconvenio.asmx?wsdl
// Encoding : utf-8
// Version  : 1.0
// (17/09/2014 08:24:32 - 1.33.2.5)
// ************************************************************************ //

unit wsconvenio;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns, sysutils, Classes,
  TypInfo, dialogs, cartao_util, URotinasTexto;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : wsconvenio
  // soapAction: wsconvenio/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : wsconvenioSoap
  // service   : wsconvenio
  // port      : wsconvenioSoap
  // URL       : http://localhost:2510/wsconvenio.asmx
  // ************************************************************************ //
  wsconvenioSoap = interface(IInvokable)
  ['{165697DC-9E9C-5F36-D210-29DD6D99AE7C}']
    function  MObterEmpresasIncCart(const xml: WideString): WideString; stdcall;
    function  MAdicionarConveniado(const xml: WideString): WideString; stdcall;
    function  MObterGruposProd(const xml: WideString): WideString; stdcall;
    function  MListarEmpresas(const xml: WideString): WideString; stdcall;
    function  MConsultarCartoes(const xml: WideString): WideString; stdcall;
    function  MAbrirTransacao(const xml: WideString): WideString; stdcall;
    function  MValidarProdutos(const xml: WideString): WideString; stdcall;
    function  MFecharTransacao(const xml: WideString): WideString; stdcall;
    function  MConfirmarTransacao(const xml: WideString): WideString; stdcall;
    function  MObterTransacoesPendentes(const xml: WideString): WideString; stdcall;
    function  MCancelarTransacao(const xml: WideString): WideString; stdcall;
    function  MSolicitarResgate(const xml: WideString): WideString; stdcall;
    function  MValidarFormasPagto(const xml: WideString): WideString; stdcall;
    function  MConsultarTransacao(const xml: WideString): WideString; stdcall;
    function  MConsultarCartaoSeg(const xml: WideString): WideString; stdcall;
    function  MInformarNF(const xml: WideString): WideString; stdcall;
    function  MSolicitarAutorizacao(const xml: WideString): WideString; stdcall;
    function  MCancelarAutorizacao(const xml: WideString): WideString; stdcall;
    function  MConsultarFormasPagto(const xml: WideString): WideString; stdcall;
    function  MConsultarRegrasDesc(const xml: WideString): WideString; stdcall;
    function  MConsultarEmpresa(const xml: WideString): WideString; stdcall;
    function  MConsultarEmpresas(const xml: WideString): WideString; stdcall;
    function  MObterSaldoRestante(const xml: WideString): WideString; stdcall;
    function  MObterProdutosBloqueados(const xml: WideString): WideString; stdcall;
    function  MObterPagamentos(const xml: WideString): WideString; stdcall;
    function  MObterDetalhePagto(const xml: WideString): WideString; stdcall;
    function  MObterAutorizacoesPagto(const xml: WideString): WideString; stdcall;
    function  MInformarProdutos(const xml: WideString): WideString; stdcall;
    function  AbrirTransacao(const xml: WideString): WideString; stdcall;
    function  ValidarProdutos(const xml: WideString): WideString; stdcall;
    function  FecharTransacao(const xml: WideString): WideString; stdcall;
    function  ConfirmarTransacao(const xml: WideString): WideString; stdcall;
    function  ValidarFormasPagto(const xml: WideString): WideString; stdcall;
    function  ConsultarAutorizacao(const xml: WideString): WideString; stdcall;
    function  ConsultarCartao(const xml: WideString): WideString; stdcall;
    function  ConsultarCartaoSeg(const xml: WideString): WideString; stdcall;
    function  InformarNF(const xml: WideString): WideString; stdcall;
    function  InformarNFHist(const xml: WideString): WideString; stdcall;
    function  SolicitarAutorizacao(const xml: WideString): WideString; stdcall;
    function  CancelarAutorizacao(const xml: WideString): WideString; stdcall;
    function  ListarEmpresas(const xml: WideString): WideString; stdcall;
    function  ConsultarCartoes(const xml: WideString): WideString; stdcall;
    function  ObterGruposProd: WideString; stdcall;
    function  ConsultarFormasPagto: WideString; stdcall;
    function  ConsultarRegrasDesc(const xml: WideString): WideString; stdcall;
    function  ConsultarEmpresa(const xml: WideString): WideString; stdcall;
    function  ConsultarEmpresas: WideString; stdcall;
    function  ObterSaldoRestante(const xml: WideString): WideString; stdcall;
    function  ObterProdutosBloqueados(const xml: WideString): WideString; stdcall;
    function  ObterPagamentos(const xml: WideString): WideString; stdcall;
    function  ObterDetalhePagto(const xml: WideString): WideString; stdcall;
    function  ObterAutorizacoesPagto(const xml: WideString): WideString; stdcall;
    function  InformarProdutos(const xml: WideString): WideString; stdcall;
    function  ValidarLogin(const xml: WideString): WideString; stdcall;
  end;

  TCredenciado = class(TObject)
  private
    FCodAcesso :  Cardinal;
    FSenha : String;
  public
    property CodAcesso : Cardinal read FCodAcesso write FCodAcesso;
    property Senha : String read FSenha write FSenha;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCodAcesso : Cardinal ; pSenha : String);
  end;

  //**********************PARAMETROS DE ENTRADA**********************\\

  TAbrirTransacao = class(TObject)
  private
    FCredenciado : TCredenciado;
    FCartao : String;
    FOperador : String;
    FCPF : String;
    FEntrega : String;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property Cartao : String read FCartao write FCartao;
    property Operador : String read FOperador write FOperador;
    property CPF : String read FCPF write FCPF;
    property ENTREGA : String read FENTREGA write FENTREGA;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCredenciado : TCredenciado; pCartao, pCPF, pOperador : String; pEntrega : String );
  end;

  TProduto = class(TObject)
  private
    FCodBarras : String;
    FDescricao : String;
    FQtd : Cardinal;
    FPreUnitBru : Cardinal;
    FPreUnitLiq : Cardinal;
    FPreFabrica : Cardinal;
    FGrupo : Integer;
    FBlocDesc : Boolean;
    usarBloc : Boolean;
  public
    property CodBarras : String read FCodBarras write FCodBarras;
    property Descricao : String read FDescricao write FDescricao;
    property Qtd : Cardinal read FQtd write FQtd;
    property PreUnitBru : Cardinal read FPreUnitBru write FPreUnitBru;
    property PreUnitLiq : Cardinal read FPreUnitLiq write FPreUnitLiq;
    property PreFabrica : Cardinal read FPreFabrica write FPreFabrica;
    property Grupo : Integer read FGrupo write FGrupo;
    property BlocDesc : Boolean read FBlocDesc write FBlocDesc;
    function getXML : String;
    function XMLValido : Boolean;
    procedure decodeXmlToProduto(XML : String);
    constructor Create(pCodBarras, pDescricao : String;  pQtd, pPreUnitBru, pPreUnitLiq, pPreFabrica : Cardinal; pGrupo : Integer; pBlocDesc : Boolean); overload;
    constructor Create(pCodBarras, pDescricao : String;  pQtd, pPreUnitBru, pPreUnitLiq, pPreFabrica : Cardinal; pGrupo : Integer); overload;
    constructor Create(); overload;
  end;

  ArrayProduto = array of TProduto;
  TProdutos = class(TObject)
  private
    FProdutos : ArrayProduto;
    FValorTotal : Cardinal;
    FQtd : Integer;
  public
    property ValorTotal : Cardinal read FValorTotal write FValorTotal;
    function Add(Produto : TProduto) :Integer; //Retorna a quantidade atual
    function Count : Integer;
    function getXML : String;
    function XMLValido : Boolean;
    procedure decodeXmlToProdutos(XML : String);
    procedure Clone(var Produtos : TProdutos);
    constructor Create(pValorTotal : Cardinal);
  end;

  TValidarProdutos = class(TObject)
  private
    FCredenciado : TCredenciado;
    FCartao : String;
    FCpf : String;
    FTransId : Cardinal;
    FProdutos : TProdutos;
    FOperador: Cardinal;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property Cartao : String read FCartao write FCartao;
    property Cpf : String read FCpf write FCpf;
    property TransId : Cardinal read FOperador write FOperador;
    property Produtos : TProdutos read FProdutos write FProdutos;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCredenciado : TCredenciado; pCartao, pCpf : String; pTransId : Cardinal; pProdutos : TProdutos);
  end;


  TPrescritor = class(TObject)
  private
    FTipoPrescritor : Cardinal;
    FUfPrescritor : String;
    FNumPrescritor : String;
    FNumReceita : Cardinal;
    FDataReceita : String;
  public
    property TipoPrescritor : Cardinal read FTipoPrescritor write FTipoPrescritor;
    property UfPrescritor : String read FUfPrescritor write FUfPrescritor;
    property NumPrescritor : String read FNumPrescritor write FNumPrescritor;
    property NumReceita : Cardinal read FNumReceita write FNumReceita;
    property DataReceita : String read FDataReceita write FDataReceita;
    function getXML : String;    
    constructor Create(pTipoPrescritor : Cardinal; pUfPrescritor : String; pNumPrescritor : String; pNumReceita : Cardinal = 0; pDataReceita : String = '');
  end;

  TPrescrProd = class(TObject)
  private
    FCodBarras : String;
    FPrescritor : TPrescritor;
  public
    property CodBarras : String read FCodBarras write FCodBarras;
    property Prescritor : TPrescritor read FPrescritor write FPrescritor;
    function getXML : String;
    constructor Create(pCodBarras : String ; pPrescritor : TPrescritor);
  end;

  TArrayPrescProd = Array of TPrescrProd;
  TPrescrsProd = class(TObject)
  private
    FQtd : Cardinal;
    FPrescrsProd : TArrayPrescProd;
  public
    function Add(pPrescrProd : TPrescrProd) :Integer; //Retorna a quantidade atual
    function Count : Integer;
    function getXML : String;
    procedure Clone(var pPrescrsProd: TPrescrsProd);
    constructor Create();
  end;

  TFecharTransacao = class(TObject)
  private
    FCredenciado : TCredenciado;
    FCartao: String;
    FCPF : String;
    FTransID : Cardinal;
    FProdutos : TPrescrsProd;
    FFormaPagto : Cardinal;
    FValorAut : Cardinal;
    FSenhaCartao : String;
    FNovaSenhaCartao : String;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property Cartao : String read FCartao write FCartao;
    property Cpf : String read FCpf write FCpf;
    property TransId : Cardinal read FTransId write FTransId;
    property Produtos : TPrescrsProd read FProdutos write FProdutos;
    property FormaPagto : Cardinal read FFormaPagto write FFormaPagto;
    property ValorAut : Cardinal read FValorAut write FValorAut;
    property SenhaCartao : String read FSenhaCartao write FSenhaCartao;
    property NovaSenhaCartao: String read FNovaSenhaCartao write FNovaSenhaCartao;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCredenciado : TCredenciado; pCartao, pCpf : String; pTransId : Cardinal; pSenhaCartao : String; pFormaPagto : Cardinal; pValorAut : Cardinal; pNovaSenhaCartao: String; pProdutos : TPrescrsProd = nil); overload;
  end;

  TConfirmarTransacao = class(TObject)
  private
    FCredenciado : TCredenciado;
    FCartao : String;
    FCPF : String;
    FTransId : Cardinal;
    FDocFiscal : Cardinal;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property Cartao : String read FCartao write FCartao;
    property Cpf : String read FCpf write FCpf;
    property TransId : Cardinal read FTransId write FTransId;
    property DocFiscal : Cardinal read FDocFiscal write FDocFiscal;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCredenciado : TCredenciado; pCartao, pCpf : String; pTransId, pDocFiscal : Cardinal);
  end;

  TCancelarTrasnacao = class(TObject)
  private
    FCredenciado : TCredenciado;
    FTransId : Cardinal;
    FOperador : String;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property TransId : Cardinal read FTransId write FTransId;
    property Operador : String read FOperador write FOperador;
    function getXML : String;
    function XMLValido : Boolean;
    constructor Create(pCredenciado : TCredenciado; pTransId : Cardinal; pOperador : String);
  end;

  TValidarFormaPgto = class(TObject)
  private
    FCredenciado : TCredenciado;
    FEmpresId : Cardinal;
  public
    property Credenciado : TCredenciado read FCredenciado write FCredenciado;
    property EmpresId : Cardinal read FEmpresId write FEmpresId;
    function getXML : String;
    constructor Create(pCredenciado : TCredenciado; pEmpresId : Cardinal);
  end;
  //**********************PARAMETROS DE SAIDA**********************\\
  TAbrirTransacaoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FTransId : Cardinal;
    FNomeCredenciado : String;
    FNomeConveniado : String;
    FProgDesc : Boolean;
    FFidelidade : Boolean;
    FSaldoPontos : Cardinal;
    FEmpresId : Cardinal;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property TransId : Cardinal read FTransId write FTransId;
    property NomeCredenciado : String read FNomeCredenciado write FNomeCredenciado;
    property NomeConveniado : String read FNomeConveniado write FNomeConveniado;
    property ProgDesc : Boolean read FProgDesc write FProgDesc;
    property Fidelidade : Boolean read FFidelidade write FFidelidade;
    property SaldoPontos : Cardinal read FSaldoPontos write FSaldoPontos;
    property EmpresId : Cardinal read FEmpresId write FEmpresId;
    constructor Create(XML : String);
  end;

  TProdVal = class(TObject)
  private
    FCodBarras : String;
    FStatusProd : Cardinal;
    FQtd : Cardinal;
    FPrcUnit : Cardinal;
    FVlrBru : Cardinal;
    FPercDesc : Cardinal;
    FVlrDesc : Cardinal;
    FVlrLiq : Cardinal;
    FNomePrograma : String;
    FObrigReceita : Boolean;
    FPontosProd : Integer;
  public
    property CodBarras : String read FCodBarras write FCodBarras;
    property StatusProd : Cardinal read FStatusProd write FStatusProd;
    property Qtd : Cardinal read FQtd write FQtd;
    property PrcUnit : Cardinal read FPrcUnit write FPrcUnit;
    property VlrBru : Cardinal read FVlrBru write FVlrBru;
    property PercDesc : Cardinal read FPercDesc write FPercDesc;
    property VlrDesc : Cardinal read FVlrDesc write FVlrDesc;
    property VlrLiq : Cardinal read FVlrLiq write FVlrLiq;
    property NomePrograma : String read FNomePrograma write FNomePrograma;
    property ObrigReceita : Boolean read FObrigReceita write FObrigReceita;
    property PontosProd : Integer read FPontosProd write FPontosProd;
    procedure decodeXmlToProdVal(XML : String);
    function getXML : String;
    constructor Create(pCodBarras : String; pStatusProd, pQtd, pPrcUnit, pVlrBru, pPercDesc, pVlrDesc,
      pVlrLiq : Cardinal; pNomePrograma : String = ''; pObrigReceita : Boolean = false; pPontosProd : Integer = 0); overload;
    constructor Create(); overload;
  end;

  ArrayProdVal = array of TProdVal;
  TProdsVal = class(TObject)
  private
    FProdsVal : ArrayProdVal;
    FQtd : Integer;
  public
    function Add(ProdVal : TProdVal) :Integer; //Retorna a quantidade atual
    function Count : Integer;
    function getXML : String;
    function XMLValido : Boolean;
    procedure decodeXmlToProdsVal(XML : String);
    procedure Clone(var pProdsVal: TProdsVal);
    constructor Create();
  end;

  TValidarProdutosRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FTransId : Cardinal;
    FProdutos : TProdsVal;
    FPontosTrans: Integer;
    FMensagemFId : String;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property TransId : Cardinal read FTransId write FTransId;
    property Produtos : TProdsVal read FProdutos write FProdutos;
    property PontosTrans : Integer read FPontosTrans write FPontosTrans;
    property MensagemFId : String read FMensagemFId write FMensagemFId;
    constructor Create(XML : String);
  end;

  TFecharTransacaoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FTransId : Cardinal;
    FCupomAut : TStrings;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property TransId : Cardinal read FTransId write FTransId;
    property CupomAut : TStrings read FCupomAut write FCupomAut;
    constructor Create(XML : String);
  end;

  TConfirmarTransacaoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FTransId : Cardinal;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property TransId : Cardinal read FTransId write FTransId;
    constructor Create(XML : String);
  end;

  TCancelarTransacaoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    constructor Create(XML : String);
  end;

  TParcela = class(TObject)
  private
    FVencimento : TDateTime; //Data
    FPercentual : Cardinal;
  public
    property Vencimento : TDateTime read FVencimento write FVencimento;
    property Percentual : Cardinal read FPercentual write FPercentual;
    constructor Create(XML : String);
  end;

  ArrayParcelas = Array of TParcela;
  TParcelas = class(TObject)
  private
    FParcelas : ArrayParcelas;
    FQtd : Integer;
  public
    function Add(Parcela : TParcela) :Integer; //Retorna a quantidade atual
    constructor Create(XML : String);
  end;

  TFormaPgto = class(TObject)
  private
    FCodigo : Cardinal;
    FDescricao : String;
    FParcelas : TParcelas;
  public
    property Codigo : Cardinal read FCodigo write FCodigo;
    property Descricao : String read FDescricao write FDescricao;
    property Parcelas : TParcelas read FParcelas write FParcelas;
    constructor Create(XML : String);
  end;

  ArrayFormaPgto = array of TFormaPgto;
  TFormasPgto = class(TObject)
  private
    FQtd : Integer;
    FFormasPgto : ArrayFormaPgto;
  public
    property FormasPgto : ArrayFormaPgto read FFormasPgto write FFormasPgto;
    property Count : Integer read FQtd;
    function Add(FormaPgto : TFormaPgto) : Integer;
    constructor Create(XML : String);
  end;

  TValidarFormaPgtoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FFormasPagto : TFormasPgto;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property FormasPagtos : TFormasPgto read FFormasPagto write FFormasPagto;
    constructor Create(XML : String);
  end;



  TConsultarCartaoRet = class(TObject)
  private
    FStatus : Cardinal;
    FMensagem : String;
    FSaldoRestante : String;
  public
    property Status : Cardinal read FStatus write FStatus;
    property Mensagem : String read FMensagem write FMensagem;
    property SaldoRestante : String read FSaldoRestante write FSaldoRestante;
    constructor Create(XML : String);
  end;

function GetwsconvenioSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): wsconvenioSoap;
const INICIO_XML : String = '';//'<?xml version="1.0" encoding="iso-8859-1" ?>';

implementation

function isDate(DateTime : string) : Boolean;
begin
  try
    StrToDateTime(dateTime);
    Result := True;
  except
    Result := False;
  end;
end;

function DataAnoMesDia(data : String) : String;
var ano,mes,dia : String;
begin
  ano := Copy(data,1,4);
  mes := Copy(data,6,2);
  dia := Copy(data,9,2);
  Result :=  dia + '/' + mes + '/' + ano;
end;



function StringToXsdDateTime(Data : string) : string;
var dia, mes, ano : Word;
    sDia, sMes : String;
begin
  if isDate(Data) then begin
    DecodeDate(StrToDateTime(Data),ano,mes,dia);
    sMes := iif(mes < 10 , '0'+IntToStr(mes),IntToStr(mes));
    sDia := iif(dia < 10 , '0'+IntToStr(dia),IntToStr(dia));
    Result := IntToStr(Ano)+'-'+sMes+'-'+sDia;//+'T00:00:00';
  end else
    Result := '';
end;

function valorXML(valor : Integer) : String;
begin
  if (valor mod 100 = 0) then
    Result := IntToStr(valor * 100)
  else if (valor mod 10 = 0) then
    Result := IntToStr(valor * 10)
  else
    Result := IntToStr(valor);
end;


function GetwsconvenioSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): wsconvenioSoap;
const
  defWSDL = 'http://localhost:2510/wsconvenio.asmx?wsdl';
  defURL  = 'http://localhost:2510/wsconvenio.asmx';
  defSvc  = 'wsconvenio';
  defPrt  = 'wsconvenioSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as wsconvenioSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;

{ TCredenciado }

constructor TCredenciado.Create(pCodAcesso: Cardinal; pSenha: String);
begin
  FCodAcesso := pCodAcesso;
  FSenha := pSenha;
end;

function TCredenciado.getXML: String;
begin
  Result := '<CREDENCIADO><CODACESSO>'+ IntToStr(FCodAcesso) + '</CODACESSO>' +
    '<SENHA>' + FSenha + '</SENHA></CREDENCIADO>';
end;

function TCredenciado.XMLValido: Boolean;
begin
  Result := (FCodAcesso > 0) and (Length(FSenha) > 0 );
end;

{ TAbrirTransacao }

constructor TAbrirTransacao.Create(pCredenciado: TCredenciado; pCartao, pCPF,
  pOperador : String; pEntrega : String);
begin
  FCredenciado := pCredenciado;
  FCartao := pCartao;
  FCPF := pCPF;
  FOperador := pOperador;
  FEntrega := pEntrega;
end;

function TAbrirTransacao.getXML: String;
begin
  Result := INICIO_XML+'<ABRIR_TRANSACAO_PARAM>';
  Result := Result + FCredenciado.getXML;
  Result := Result + '<CARTAO>' + FCartao + '</CARTAO>';
  Result := Result + iif(FCPF = '', '<CPF/>','<CPF>' + FCPF + '</CPF>');
  Result := Result + '<OPERADOR>' + FOperador + '</OPERADOR>';
  Result := Result + '<ENTREGA>' + FEntrega + '</ENTREGA>';
  Result := Result + '</ABRIR_TRANSACAO_PARAM>';
end;

function TAbrirTransacao.XMLValido: Boolean;
begin
  Result := (FCredenciado.XMLValido)
    and ((length(FCartao) >= 0) and (length(FCartao) <= 20))
    and ((Length(FCPF) >= 0) and (Length(FCPF) <= 11))
    and ((Length(FOPERADOR) > 0) and (Length(FOPERADOR) <= 25))
    and (Entrega[1] in ['S','N']);
end;

{ TProduto }

constructor TProduto.Create(pCodBarras, pDescricao: String; pQtd,
  pPreUnitBru, pPreUnitLiq, pPreFabrica: Cardinal; pGrupo: Integer;
  pBlocDesc: Boolean);
begin
  Create(pCodBarras,pDescricao,pQtd,pPreUnitBru,pPreUnitLiq,pPreFabrica,pGrupo);
  usarBloc  := True;
  FBlocDesc := pBlocDesc;
end;

constructor TProduto.Create(pCodBarras, pDescricao: String; pQtd,
  pPreUnitBru, pPreUnitLiq, pPreFabrica: Cardinal; pGrupo: Integer);
begin
  FCodBarras  := pCodBarras;
  FDescricao  := pDescricao;
  FQtd        := pQtd;
  FPreUnitBru := pPreUnitBru;
  FPreUnitLiq := pPreUnitLiq;
  FPreFabrica := pPreFabrica;
  FGrupo      := pGrupo;
  usarBloc    := False;
end;


constructor TProduto.Create;
begin
  //n faz nada;
end;

procedure TProduto.decodeXmlToProduto(XML: String);
begin
  FCodBarras := getElementXML(XML,'CODBARRAS');
  FDescricao := getElementXML(XML,'DESCRICAO');
  FQtd := StrToInt(getElementXML(XML,'QTDE'));
  FPreUnitBru := StrToInt(getElementXML(XML,'PRCUNITBRU'));
  FPreUnitLiq := StrToInt(getElementXML(XML,'PRCUNITLIQ'));
  FPreFabrica := StrToInt(getElementXML(XML,'PRCFABRICA'));
  FGrupo := StrToInt(getElementXML(XML,'GRUPO'));
  if not usarBloc then
    usarBloc := Pos('BLOCDESC',XML) > 0;
  if usarBloc then
    FBlocDesc := UpperCase(getElementXML(XML,'BLOCDESC')) = 'TRUE';
end;

function TProduto.getXML: String;
begin
  Result := '<PRODUTO>';
  Result := Result + '<CODBARRAS>' + FCodBarras + '</CODBARRAS>';
  Result := Result + '<DESCRICAO>' + FDescricao + '</DESCRICAO>';
  Result := Result + '<QTDE>' + IntToStr(FQtd) + '</QTDE>';
  Result := Result + '<PRCUNITBRU>' + IntToStr(FPreUnitBru) + '</PRCUNITBRU>';
  Result := Result + '<PRCUNITLIQ>' + IntToStr(FPreUnitLiq) + '</PRCUNITLIQ>';
  Result := Result + '<PRCFABRICA>' + IntToStr(FPreFabrica) + '</PRCFABRICA>';
  Result := Result + '<GRUPO>' + IntToStr(FGrupo) + '</GRUPO>';
  if usarBloc then
    Result := Result + iif(usarBloc, '<BLOC_DESC>' + iif(FBlocDesc,'true','false') + '</BLOC_DESC>','');
  Result := Result + '</PRODUTO>';
end;

function TProduto.XMLValido: Boolean;
begin
  Result := (Length(FCodBarras) = 13)
    and ((Length(FDescricao) > 0) and (Length(FDescricao) <= 100))
    and (FQtd > 0) and (FPreUnitLiq > 0) and (FPreUnitBru > 0)
    and (FPreFabrica > 0);
end;

{ TProdutos }

constructor TProdutos.Create(pValorTotal : Cardinal);
begin
  FValorTotal := pValorTotal;
  FQtd := 0;
end;

function TProdutos.Add(Produto: TProduto): Integer;
begin
  FQtd := FQtd + 1;
  SetLength(FProdutos,FQtd);
  FProdutos[FQtd-1] := Produto;
  Result := FQtd;
end;

procedure TProdutos.Clone(var Produtos: TProdutos);
begin
  Produtos := Self;
end;

function TProdutos.Count: Integer;
begin
  Result := FQtd;
end;

function TProdutos.getXML: String;
var I : Integer;
begin
  Result := '<PRODUTOS>';
  for I := 0 to FQtd-1 do
    Result := Result + FProdutos[I].getXML;
  if FQtd <= 0 then
    Result := Result + '<VALORTOTAL>' + IntToStr(ValorTotal) + '</VALORTOTAL>';
  Result := Result + '</PRODUTOS>';
end;

function TProdutos.XMLValido: Boolean;
var I : Integer;
begin
  Result := (FValorTotal > 13);
  if Result then
    for I := 0 to FQtd-1 do
      if not FProdutos[I].XMLValido then begin
        Result := False;
        Break;
      end;
end;

procedure TProdutos.decodeXmlToProdutos(XML: String);
var pIni, pFin: Integer;
    produtos : String;
    p : TProduto;
begin
  produtos := getElementXML(XML,'PRODUTO');
  if (Length(produtos) > 0) then
    while Pos('<PRODUTO>',XML) > 0 do begin
      pIni := Pos('<PRODUTO>',XML);
      pFin := Pos('</PRODUTO>',XML) +10; //+10 po�s � o tamanho do "<produto>";
      p := TProduto.Create();
      p.decodeXmlToProduto(Copy(XML,pIni,pFin));
      Self.Add(p);
      XML := Copy(XML,pFin,Length(XML)-pFin);
      FreeAndNil(p);
    end;
end;

{ TValidarProdutos }

constructor TValidarProdutos.Create(pCredenciado: TCredenciado; pCartao,
  pCpf: String; pTransId: Cardinal; pProdutos: TProdutos);
begin
  FCredenciado := pCredenciado;
  FCartao := pCartao;
  FCpf := pCpf;
  FTransId := pTransId;
  FProdutos := pProdutos;
end;

function TValidarProdutos.getXML: String;
begin
  Result := INICIO_XML+'<VALIDAR_PRODUTOS_PARAM>' +
    FCredenciado.getXML +
    '<CARTAO>' + FCartao + '</CARTAO>' +
    iif(FCpf = '', '<CPF/>', '<CPF>' + FCpf + '</CPF>') +
    '<TRANSID>' + IntToStr(FTransId)+ '</TRANSID>' +
    FProdutos.getXML + '</VALIDAR_PRODUTOS_PARAM>';
end;

function TValidarProdutos.XMLValido: Boolean;
begin
  Result := FCredenciado.XMLValido
    and ((length(FCartao) >= 0) and (length(FCartao) <= 20))
    and ((Length(FCPF) >= 0) and (Length(FCPF) <= 11))
    and (FTransId > 0) and FProdutos.XMLValido;
end;

{ TPrescritor }

constructor TPrescritor.Create(pTipoPrescritor: Cardinal;
  pUfPrescritor: String; pNumPrescritor : String; pNumReceita: Cardinal;
  pDataReceita: String);
const
  ufs: array[1..27] of string = ('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
                                    'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI',
                                    'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO');

var I : Integer;
    ok : boolean;
begin
  FTipoPrescritor := pTipoPrescritor;
  FNumPrescritor := pNumPrescritor;

  ok := pufPrescritor = '';
  if not ok then begin
    for I := 0 to Length(ufs) -1 do begin
      if (ufs[I] = pUfPrescritor) then begin
        ok := True;
        Break;
      end;
    end;
  end;

  if not ok then
    pUfPrescritor := '';
  FUFPrescritor := pUfPrescritor;
  FNumReceita := pNumReceita;
  try
    StrToDate(pDataReceita);
    FDataReceita := pDataReceita;
  except
    FDataReceita := '';
  end;
end;

function TPrescritor.getXML: String;
begin
//<DATARECEITA>2012-04-10</DATARECEITA></PRESCRITOR></PRODUTO><PRODUTO><CODBARRAS>7897322705415</CODBARRAS><PRESCRITOR><TIPOPRESCRITOR>1</TIPOPRESCRITOR><UFPRESCRITOR>RJ</UFPRESCRITOR><NUMPRESCRITOR>25025</NUMPRESCRITOR><NUMRECEITA>1212121</NUMRECEITA><DATARECEITA>2012-04-10</DATARECEITA></PRESCRITOR></PRODUTO><PRODUTO><CODBARRAS>7897851250530</CODBARRAS><PRESCRITOR><TIPOPRESCRITOR>1</TIPOPRESCRITOR><UFPRESCRITOR>RJ</UFPRESCRITOR><NUMPRESCRITOR>25025</NUMPRESCRITOR><NUMRECEITA>1212121</NUMRECEITA><DATARECEITA>2012-04-10</DATARECEITA></PRESCRITOR></PRODUTO><PRODUTO><CODBARRAS>7896226502199</CODBARRAS><PRESCRITOR><TIPOPRESCRITOR>1</TIPOPRESCRITOR><UFPRESCRITOR>RJ</UFPRESCRITOR><NUMPRESCRITOR>25025</NUMPRESCRITOR><NUMRECEITA>1212121</NUMRECEITA><DATARECEITA>2012-04-10</DATARECEITA></PRESCRITOR></PRODUTO></PRODUTOS>
  Result :=  '<PRESCRITOR>' +
  '<TIPOPRESCRITOR>' + IntToStr(FTipoPrescritor) + '</TIPOPRESCRITOR>' +
  '<UFPRESCRITOR>' + FUfPrescritor + '</UFPRESCRITOR>' +
  '<NUMPRESCRITOR>' + FNumPrescritor + '</NUMPRESCRITOR>' +
  iif(FNumReceita = 0, '', '<NUMRECEITA>' + IntToStr(FNumReceita) + '</NUMRECEITA>') +
  iif(FDataReceita = '', '', '<DATARECEITA>' + StringToXsdDateTime(FDataReceita) + '</DATARECEITA>') +
  '</PRESCRITOR>';
end;

{ TPrescrProd }

constructor TPrescrProd.Create(pCodBarras: String;
  pPrescritor: TPrescritor);
begin
  FCodBarras := pCodBarras;
  FPrescritor := pPrescritor;
end;

function TPrescrProd.getXML: String;
begin
  Result := '<PRODUTO>' +
    '<CODBARRAS>' + FCodBarras + '</CODBARRAS>' +
    FPrescritor.getXML +
    '</PRODUTO>';
end;

{ TPrescrsProd }

function TPrescrsProd.Add(pPrescrProd: TPrescrProd): Integer;
begin
  FQtd := FQtd + 1;
  SetLength(FPrescrsProd,FQtd);
  FPrescrsProd[FQtd -1] := pPrescrProd;
  Result := FQtd;
end;

procedure TPrescrsProd.Clone(var pPrescrsProd: TPrescrsProd);
begin
  pPrescrsProd := Self;
end;

function TPrescrsProd.Count: Integer;
begin
  Result := FQtd;
end;

constructor TPrescrsProd.Create;
begin
  FQtd := 0;
end;

function TPrescrsProd.getXML: String;
var I : Integer;
begin
  Result := '<PRODUTOS>';
  for I := 0 to FQtd-1 do
    Result := Result + FPrescrsProd[I].getXML;
  Result := Result + '</PRODUTOS>';
end;

{ TFecharTransacao }

constructor TFecharTransacao.Create(pCredenciado: TCredenciado; pCartao,
  pCpf: String; pTransId: Cardinal; pSenhaCartao: String; pFormaPagto : Cardinal;
  pValorAut: Cardinal; pNovaSenhaCartao: String; pProdutos: TPrescrsProd);
begin
  FCredenciado := pCredenciado;
  FCartao := pCartao;
  FCpf := pCpf;
  FTransId := pTransId;
  FSenhaCartao := pSenhaCartao;
  FProdutos := pProdutos;
  FFormaPagto := pFormaPagto;
  FValorAut := pValorAut;
  FNovaSenhaCartao := pNovaSenhaCartao;
end;

function TFecharTransacao.getXML: String;
begin
  Result := INICIO_XML+'<FECHAR_TRANSACAO_PARAM>' + FCredenciado.getXML +
    '<CARTAO>' + FCartao + '</CARTAO>' +
    iif(FCPF = '','<CPF/>','<CPF>' + FCPF + '</CPF>') +
    '<TRANSID>' + IntToStr(FTransID) + '</TRANSID>';
    if FProdutos = nil then
      Result := Result + ''
    else
      Result := Result + FProdutos.getXML;
//    iif(FProdutos = nil, '', FProdutos.getXML) +
    Result := Result + iif(FFormaPagto = 0, '', '<FORMAPAGTO>' + IntToStr(FFormaPagto) + '</FORMAPAGTO>') +
    iif(FValorAut = 0, '', '<VALORAUT>' + IntToStr(FValorAut) + '</VALORAUT>') +
    iif(FSenhaCartao='','<SENHA_CARTAO/>','<SENHA_CARTAO>' + FSenhaCartao + '</SENHA_CARTAO>') +
    iif(FNovaSenhaCartao='','<NOVA_SENHA_CARTAO/>','<NOVA_SENHA_CARTAO>' + FNovaSenhaCartao + '</NOVA_SENHA_CARTAO>') +
    '</FECHAR_TRANSACAO_PARAM>';
end;

function TFecharTransacao.XMLValido: Boolean;
begin
  Result := (FCredenciado.XMLValido)
    and ((length(FCartao) >= 0) and (length(FCartao) <= 20))
    and ((Length(FCPF) >= 0) and (Length(FCPF) <= 11))
    and (FTransId > 0) {and (FProdutos.XMLValido)
    and (FProdutos.XMLValido)} and (FValorAut > 0) and (FormaPagto > 0);
end;

{ TConfirmarTransacao }

constructor TConfirmarTransacao.Create(pCredenciado: TCredenciado; pCartao,
  pCpf: String; pTransId, pDocFiscal: Cardinal);
begin
  FCredenciado := pCredenciado;
  FCartao := pCartao;
  FCpf := pCpf;
  FTransId := pTransId;
  FDocFiscal := pDocFiscal;
end;

function TConfirmarTransacao.getXML: String;
begin
  Result := INICIO_XML + '<CONFIRMAR_TRANSACAO_PARAM>' + FCredenciado.getXML +
    '<CARTAO>' + FCartao + '</CARTAO>' +
    '<CPF>' + FCPF + '</CPF>' +
    '<TRANSID>' + IntToStr(FTransID) + '</TRANSID>' +
    '<DOCFISCAL>' + IntToStr(FDocFiscal) + '</DOCFISCAL>' +
    '</CONFIRMAR_TRANSACAO_PARAM>';
end;

function TConfirmarTransacao.XMLValido: Boolean;
begin
  Result := (FCredenciado.XMLValido)
    and ((length(FCartao) >= 0) and (length(FCartao) <= 20))
    and ((Length(FCPF) >= 0) and (Length(FCPF) <= 11))
    and (FTransId > 0) and (FDocFiscal > 0);
end;

{ TCancelarTrasnacao }

constructor TCancelarTrasnacao.Create(pCredenciado: TCredenciado;
  pTransId: Cardinal; pOperador: String);
begin
  FCredenciado := pCredenciado;
  FTransId := pTransId;
  FOperador := pOperador;
end;

function TCancelarTrasnacao.getXML: String;
begin
  Result := '<CANCELAR_TRANSACAO_PARAM>' + FCredenciado.getXML +
    '<TRANSID>' + IntToStr(FTransId) + '</TRANSID>' +
    '<OPERADOR>' + FOperador +'</OPERADOR>' +
    '</CANCELAR_TRANSACAO_PARAM>';
end;

function TCancelarTrasnacao.XMLValido: Boolean;
begin
  Result := (FCredenciado.XMLValido) and (FTransId > 0)
    and ((Length(FOperador) > 0) and (Length(FOperador) <= 25));
end;

{ TValidarFormaPgto }

constructor TValidarFormaPgto.Create(pCredenciado: TCredenciado;
  pEmpresId: Cardinal);
begin
  FCredenciado := pCredenciado;
  FEmpresId := pEmpresId;
end;

function TValidarFormaPgto.getXML: String;
begin
  Result := '<VALIDAR_FORMASPAGTO_PARAM>' +
    FCredenciado.getXML +
    '<EMPRES_ID>' + IntToStr(EmpresId) + '</EMPRES_ID>' +
    '</VALIDAR_FORMASPAGTO_PARAM>';
end;

//*******************OBJETOS DE RETORNO*******************\\


{ TAbrirTransacaoRet }

constructor TAbrirTransacaoRet.Create(XML: String);
var s : String;
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus = 0 then begin
    FTransId := StrToInt(getElementXML(XML,'TRANSID'));
    FNomeCredenciado := getElementXML(XML,'NOMECREDENCIADO');
    FNomeConveniado := getElementXML(XML,'NOMECONVENIADO');
    FProgDesc := UpperCase(getElementXML(XML,'PROGDESC')) = 'TRUE';
    FFidelidade := UpperCase(getElementXML(XML,'FIDELIDADE')) = 'TRUE';
    //FSaldoPontos := StrToInt(StringReplace(getElementXML(XML,'SALDO_PONTOS'),DecimalSeparator,'',[rfIgnoreCase,rfReplaceAll]);
    s:= getElementXML(XML,'SALDO_PONTOS');
    if s <> '' then
      FSaldoPontos := StrToInt(getElementXML(XML,'SALDO_PONTOS'));
    FEmpresId := StrToInt(getElementXML(XML,'EMPRES_ID'));
  end else begin
    FMensagem := getElementXML(XML,'MSG');
  end;
end;

{ TProdval }

constructor TProdval.Create(pCodBarras: String; pStatusProd, pQtd,
  pPrcUnit, pVlrBru, pPercDesc, pVlrDesc, pVlrLiq: Cardinal;
  pNomePrograma: String; pObrigReceita: Boolean; pPontosProd: Integer);
begin
    FCodBarras := pCodBarras;
    FStatusProd := pStatusProd;
    FQtd := pQtd;
    FPrcUnit := pPrcUnit;
    FVlrBru := pVlrBru;
    FPercDesc := pPercDesc;
    FVlrDesc := pVlrDesc;
    FVlrLiq := pVlrLiq;
    FNomePrograma := pNomePrograma;
    FObrigReceita := pObrigReceita;
    FPontosProd := pPontosProd;
end;

constructor TProdVal.Create;
begin
  // N�o faz nd
end;

procedure TProdVal.decodeXmlToProdVal(XML: String);
var s : String;
begin
  FCodBarras := getElementXML(XML,'CODBARRAS');
  FStatusProd := StrToInt(getElementXML(XML,'STATUSPROD'));
  FQtd := StrToInt(getElementXML(XML,'QTDE'));
  FPrcUnit := StrToInt(getElementXML(XML,'PRCUNIT'));
  FVlrBru := StrToInt(getElementXML(XML,'VLRBRU'));
  FPercDesc := StrToInt(getElementXML(XML,'PERCDESC'));
  FVlrDesc := StrToInt(getElementXML(XML,'VLRDESC'));
  FVlrLiq := StrToInt(getElementXML(XML,'VLRLIQ'));
  FNomePrograma := getElementXML(XML,'NOMEPROGRAMA');
  FObrigReceita := UpperCase(getElementXML(XML,'OBRIGRECEITA')) = 'S';
  s := getElementXML(XML,'PONTOSPROD');
  FPontosProd := StrToInt(iif(s = '', '0', s));
end;

function TProdVal.getXML: String;
begin
  Result := '<PRODUTO>' +
    '<CODBARRAS>' + FCodBarras + '</CODBARRAS>' +
    '<STATUSPROD>' + IntToStr(FStatusProd) + '</STATUSPROD>' +
    '<QTDE>' + IntToStr(FQtd) + '</QTDE>' +
    '<PRCUNIT>' + IntToStr(FPrcUnit) + '</PRCUNIT>' +
    '<VLRBRU>' + IntToStr(FVlrBru) + '</VLRBRU>' +
    '<VLRDESC>' + IntToStr(FVlrDesc) + '</VLRDESC>' +
    '<VLRLIQ>' + IntToStr(FVlrLiq) + '</VLRLIQ>' +
    '<NOMEPROGRAMA>' + FNomePrograma + '</NOMEPROGRAMA>' +
    '<OBRIGRECEITA>' + iif(FObrigReceita, 'true','false') + '</OBRIGRECEITA>' +
    '<PONTOSPROD>' + IntToStr(FPontosProd) + '</PONTOSPROD>';
end;

{ TProdsVal }

function TProdsVal.Add(ProdVal: TProdVal): Integer;
begin
  FQtd := FQtd + 1;
  SetLength(FProdsVal,FQtd);
  FProdsVal[FQtd-1] := ProdVal;
  Result := FQtd;
end;

procedure TProdsVal.Clone(var pProdsVal: TProdsVal);
begin
  pProdsVal := Self; 
end;

function TProdsVal.Count: Integer;
begin
  Result := FQtd;
end;

constructor TProdsVal.Create;
begin
  FQtd := 0 ;
end;

procedure TProdsVal.decodeXmlToProdsVal(XML: String);
var pIni, pFin: Integer;
    produtos : String;
    p : TProdVal;
begin
  produtos := getElementXML(XML,'PRODUTO');
  if (Length(produtos) > 0) then begin
    if Self = nil then
      Self := TProdsVal.Create;
    while Pos('<PRODUTO>',XML) > 0 do begin
      pIni := Pos('<PRODUTO>',XML);
      pFin := Pos('</PRODUTO>',XML) +10; //+10 po�s � o tamanho do "</PRODUTO>";
      p := TProdVal.Create();
      p.decodeXmlToProdVal(Copy(XML,pIni,pFin));
      Self.Add(p);
      XML := Copy(XML,pFin,Length(XML)-pFin+1);
      //FreeAndNil(p);
    end;
  end;
end;

function TProdsVal.getXML: String;
var I : Integer;
begin
  Result := '<PRODUTOS>';
  for I := 0 to FQtd-1 do
    Result := Result + FProdsVal[I].getXml;
  Result := Result + '</PRODUTOS>';
end;

function TProdsVal.XMLValido: Boolean;
//var I : Integer;
begin
  Result := True;
{  if Result then
    for I := 0 to FQtd-1 do
      if not FProdsVal[I].XMLValido then begin
        Result := False;
        Break;
      end;}
end;

{ TValidarProdutosRet }

constructor TValidarProdutosRet.Create(XML: String);
var s : String;
begin
  s := '';
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus = 0 then begin
    FTransId := StrToInt(getElementXML(XML,'TRANSID'));
    FProdutos := TProdsVal.Create;
    s := getElementXML(XML,'PONTOSTRANS');
    FPontosTrans := StrToInt(iif(s = '','0',s));
    FMensagemFId := getElementXML(XML,'MSGFID');
    FProdutos.decodeXmlToProdsVal(getElementXML(XML,'PRODUTOS'));
  end else begin
    FMensagem := getElementXML(XML,'MSG');
  end;
end;

{ TFecharTransacaoRet }

constructor TFecharTransacaoRet.Create(XML: String);
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus = 0 then begin
    FTransId := StrToInt(getElementXML(XML,'TRANSID'));
    FCupomAut := TStringList.Create;
    FCupomAut.Text := getElementXML(XML,'CUPOMAUT');
  end else begin
    FMensagem := getElementXML(XML,'MSG');
  end;
end;

{ TConfirmarTransacaoRet }

constructor TConfirmarTransacaoRet.Create(XML: String);
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus = 0 then begin
    FTransId := StrToInt(getElementXML(XML,'TRANSID'));
  end else begin
    FMensagem := getElementXML(XML,'MSG');
  end;
end;

{ TCancelarTransacaoRet }

constructor TCancelarTransacaoRet.Create(XML: String);
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus <> 0 then
    FMensagem := getElementXML(XML,'MSG');
end;

{ TParcela }

constructor TParcela.Create(XML: String);
begin
  FVencimento := StrToDateTime(DataAnoMesDia(getElementXML(XML,'VENCIMENTO')));
  FPercentual := StrToInt(getElementXML(XML,'PERCENTUAL'));
end;

{ TParcelas }

function TParcelas.Add(Parcela: TParcela): Integer;
begin
  FQtd := fQtd + 1;
  SetLength(FParcelas,FQtd);
  FParcelas[FQtd-1] := Parcela;
  Result := FQtd;
end;

constructor TParcelas.Create(XML: String);
var pIni, pFin : Integer;
  p : TParcela;
begin
  FQtd := 0;
  while Pos('<PARCELA>',XML) > 0 do begin
    pIni := Pos('<PARCELA>',XML);
    pFin := Pos('</PARCELA>',XML) +10; //+10 po�s � o tamanho do "</PARCELA>";
    p := TParcela.Create(Copy(XML,pIni,pFin));
    Self.Add(p);
    XML := Copy(XML,pFin,Length(XML)-pFin+1);
  end;
end;

{ TFormaPgto }

constructor TFormaPgto.Create(XML: String);
begin
  FCodigo := StrToInt(getElementXML(XML,'CODIGO'));
  FDescricao := getElementXML(XML,'DESCRICAO');
  FParcelas := TParcelas.Create(getElementXML(XML,'PARCELAS'));
end;

{ TFormasPgto }

function TFormasPgto.Add(FormaPgto: TFormaPgto) : Integer;
begin
  FQtd := FQtd + 1;
  SetLength(FFormasPgto,FQtd);
  FFormasPgto[FQtd-1] := FormaPgto;
  Result := FQtd;
end;

constructor TFormasPgto.Create(XML: String);
var pIni, pFin : Integer;
  f : TFormaPgto;
begin
  FQtd := 0;
  while Pos('<FORMAPAGTO>',XML) > 0 do begin
    pIni := Pos('<FORMAPAGTO>',XML);
    pFin := Pos('</FORMAPAGTO>',XML) +12; //+12 po�s � o tamanho do "</FORMAPAGTO>";
    f := TFormaPgto.Create(Copy(XML,pIni,pFin));

    Self.Add(f);
    XML := Copy(XML,pFin,Length(XML)-pFin+1);
  end;
end;

{ TValidarFormaPgtoRet }

constructor TValidarFormaPgtoRet.Create(XML: String);
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  FMensagem := getElementXML(XML,'MSG');
  if FStatus = 0 then
    FFormasPagto := TFormasPgto.Create(getExtremesElementsXML(XML,'FORMAPAGTO'));
end;



constructor TConsultarCartaoRet.Create(XML: String);
begin
  FStatus := StrToInt(getElementXML(XML,'STATUS'));
  if FStatus = 0 then begin
    FSaldoRestante := getElementXML(XML,'SALDO_RESTANTE');
  end else begin
    FMensagem := getElementXML(XML,'MSG');
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(wsconvenioSoap), 'wsconvenio', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(wsconvenioSoap), 'wsconvenio/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(wsconvenioSoap), ioDocument);
end. 