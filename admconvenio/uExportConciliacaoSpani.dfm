object FrmExportConciliacaoSpani: TFrmExportConciliacaoSpani
  Left = 594
  Top = 222
  BorderStyle = bsSingle
  Caption = 'Exporta'#231#227'o de Arquivo de Concilia'#231#227'o'
  ClientHeight = 154
  ClientWidth = 570
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 138
    Width = 570
    Height = 16
    Align = alBottom
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 570
    Height = 138
    Align = alClient
    TabOrder = 0
    DesignSize = (
      570
      138)
    object lblStatus: TLabel
      Left = 1
      Top = 124
      Width = 568
      Height = 13
      Align = alBottom
    end
    object Label1: TLabel
      Left = 11
      Top = 8
      Width = 136
      Height = 13
      Caption = 'Selecione o local de destino:'
    end
    object Bevel1: TBevel
      Left = 0
      Top = 180
      Width = 576
      Height = 4
      Anchors = [akLeft, akTop, akRight]
    end
    object Label3: TLabel
      Left = 330
      Top = 49
      Width = 41
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Empresa'
    end
    object Label5: TLabel
      Left = 18
      Top = 49
      Width = 53
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Data Inicial'
    end
    object lbl1: TLabel
      Left = 162
      Top = 49
      Width = 48
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Data Final'
    end
    object btnExportar: TButton
      Left = 120
      Top = 88
      Width = 322
      Height = 33
      Anchors = [akLeft, akTop, akRight]
      Caption = '&Exportar'
      TabOrder = 4
      OnClick = btnExportarClick
    end
    object edtCaminho: TJvDirectoryEdit
      Left = 10
      Top = 21
      Width = 542
      Height = 21
      DialogKind = dkWin32
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object JvDateEdit1: TJvDateEdit
      Left = 16
      Top = 64
      Width = 121
      Height = 21
      ShowNullDate = False
      TabOrder = 1
    end
    object JvDateEdit2: TJvDateEdit
      Left = 160
      Top = 64
      Width = 121
      Height = 21
      ShowNullDate = False
      TabOrder = 2
    end
    object cbbEmpresa: TComboBox
      Left = 328
      Top = 64
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 3
      Text = 'Escolha um modelo'
      Items.Strings = (
        'Farma Ponte'
        'Spani'
        'Spani Layout Novo'
        'V Nunes'
        'Auto Posto Mangueira')
    end
  end
  object sd: TSaveDialog
    Filter = 'txt|*.txt'
    Left = 560
    Top = 464
  end
  object QConfigCadastramento: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 944
    Top = 496
  end
end
