inherited FMovimentosDuvidosos: TFMovimentosDuvidosos
  Left = 50
  Top = 0
  Caption = 'Movimentos Duvidosos'
  ClientHeight = 697
  ClientWidth = 1301
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1301
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 1301
    Height = 91
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 78
      Height = 13
      Caption = 'Estabelecimento'
    end
    object Label2: TLabel
      Left = 8
      Top = 48
      Width = 41
      Height = 13
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Left = 352
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Cart'#227'o'
    end
    object Label4: TLabel
      Left = 351
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Per'#237'odo'
    end
    object Status: TLabel
      Left = 608
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Status'
    end
    object lblStatus: TLabel
      Left = 608
      Top = 25
      Width = 5
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 480
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Operador'
    end
    object lblTotal: TLabel
      Left = 720
      Top = 69
      Width = 5
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblRegistros: TLabel
      Left = 720
      Top = 50
      Width = 5
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbbEstab: TJvDBLookupCombo
      Left = 64
      Top = 24
      Width = 273
      Height = 21
      DisplayEmpty = 'Todos os estabelecimentos'
      EmptyValue = '0'
      LookupField = 'CRED_ID'
      LookupDisplay = 'NOME'
      LookupSource = dsEstab
      TabOrder = 1
      OnChange = cbbEstabChange
    end
    object btnConsultar: TBitBtn
      Left = 609
      Top = 60
      Width = 97
      Height = 25
      Caption = 'Consultar (F5)'
      TabOrder = 4
      OnClick = btnConsultarClick
      Glyph.Data = {
        AE030000424DAE03000000000000AE0100002800000020000000100000000100
        08000000000000020000232E0000232E00005E00000000000000512600005157
        61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
        7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
        9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
        BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
        BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
        CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
        D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
        DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
        E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
        ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
        F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
        FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
        09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
        0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
        23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
        065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
        5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
        5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
        5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
        5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
        5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
        5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
        5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
        5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
        5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
        5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
        5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
        5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
      NumGlyphs = 2
    end
    object edtEstab: TEdit
      Left = 8
      Top = 24
      Width = 48
      Height = 21
      Hint = 'Consulta pelo c'#243'digo do fornecedor'
      TabOrder = 0
      Text = '0'
      OnChange = edtEstabChange
      OnKeyPress = edtEstabKeyPress
    end
    object cbbEmp: TJvDBLookupCombo
      Left = 64
      Top = 64
      Width = 273
      Height = 21
      DisplayEmpty = 'Todos as Empresas'
      EmptyValue = '0'
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'NOME'
      LookupSource = dsListEmp
      TabOrder = 6
      OnChange = cbbEmpChange
    end
    object edtEmp: TEdit
      Left = 8
      Top = 64
      Width = 48
      Height = 21
      Hint = 'Consulta pelo c'#243'digo do fornecedor'
      TabOrder = 5
      Text = '0'
      OnChange = edtEmpChange
      OnKeyPress = edtEstabKeyPress
    end
    object edtCartao: TEdit
      Left = 352
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 7
      OnChange = edtEstabChange
      OnKeyPress = edtEstabKeyPress
    end
    object dataIni: TJvDateEdit
      Left = 351
      Top = 24
      Width = 122
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 2
    end
    object dataFin: TJvDateEdit
      Left = 480
      Top = 24
      Width = 110
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
    end
    object cbOperador: TComboBox
      Left = 480
      Top = 64
      Width = 113
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 5
      TabOrder = 8
      Text = 'TODOS'
      Items.Strings = (
        'AUTOR. BTI'
        'AUTOR.SITE'
        'POS'
        'POS.DB'
        'TEF'
        'TODOS')
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 669
    Width = 1301
    Height = 28
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      1301
      28)
    object btnCancelarTrans: TBitBtn
      Left = 902
      Top = 2
      Width = 145
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Cancelar Transa'#231#227'o'
      TabOrder = 3
      OnClick = btnCancelarTransClick
      Glyph.Data = {
        0E040000424D0E040000000000000E0200002800000020000000100000000100
        08000000000000020000232E0000232E000076000000000000000021CC001031
        DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
        DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
        FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
        F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
        F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
        FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
        E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
        ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
        FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
        C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
        CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
        D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
        E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
        E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
        F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
        75757575757575757575757575757575757575757575622F080000082F627575
        757575757575705E493434495E70757575757575753802030E11110E03023875
        7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
        7575757567354D5354555554534D35677575756307102A00337575442C151007
        63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
        367575604B545568345E7575745B544B607575171912301C3700317575401219
        1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
        057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
        0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
        217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
        3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
        65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
        757575756C566058483434485860566C75757575754324283237373228244375
        75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
        757575757575736A5D55555D6A73757575757575757575757575757575757575
        757575757575757575757575757575757575}
      NumGlyphs = 2
    end
    object btnConfirmarTrans: TBitBtn
      Left = 1047
      Top = 2
      Width = 145
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Con&firmar Transa'#231#227'o'
      TabOrder = 4
      OnClick = btnConfirmarTransClick
      Glyph.Data = {
        46030000424D4603000000000000460100002800000020000000100000000100
        08000000000000020000232E0000232E00004400000000000000117611001379
        1300177D17001C821C0021872100278D27002C922C0032983200379D37003CA2
        3C0040A6400043A9430054BA540058BE58005DC35D0063CA63006AD16A0071D8
        710078DF78007EE57E00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD00BEBF
        BF009BCE9B0083EA830087EE8700A1D4A100BFC0C100C2C3C300C5C5C600C6C7
        C800C8C9C900CACACB00C3DDC300C4DDC400C5DEC500D1D1D200D2D3D300D5D5
        D600D7D8D800DADADB00DDDEDE00C6E0C600C7E1C700C9E2C900CAE4CA00CCE5
        CC00CDE6CD00CEE8CE00CFE9CF00D0E9D000E0E0E100E2E2E200E2E3E300E4E5
        E500E5E5E600E7E7E700EDEDED00EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1
        F100F2F2F200FFFFFF0043434343434343434343434343434343434343434343
        4343434343434343434343434343240000244343434343434343434343433C14
        143C434343434343434343434325010C0C0125434343434343434343433C1527
        27153C43434343434343434326020D0D0D0D022643434343434343433D162828
        2828163D434343434343432D030E0E1C1C0E0E032D4343434343433D1729293B
        3B2929173D434343434343040F0F1C04041C0F0F042E4343434343182A2A3B18
        183B2A2A183E434343434305101C052F2F051C1010052F43434343192B3B193F
        3F193B2B2B193F43434343061C0630434330061C111106304343431E3B1E3F43
        433F1E3B2C2C1E3F4343430707314343434331071C1212073143431F1F404343
        4343401F3B36361F4043431A3243434343434332081C13130843433741434343
        43434341203B383820434343434343434343434333091C1B0943434343434343
        4343434341213B3A21434343434343434343434343340A1C0A43434343434343
        434343434342223B2243434343434343434343434343350B0B43434343434343
        4343434343434223234343434343434343434343434343351D43434343434343
        4343434343434342394343434343434343434343434343434343434343434343
        4343434343434343434343434343434343434343434343434343434343434343
        43434343434343434343}
      NumGlyphs = 2
    end
    object btnMarcaDesmEmp: TBitBtn
      Left = 5
      Top = 1
      Width = 118
      Height = 25
      Caption = 'Marca/Desm.(F12)'
      TabOrder = 0
      OnClick = btnMarcaDesmEmpClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnMarcaTodasEmp: TBitBtn
      Left = 123
      Top = 1
      Width = 118
      Height = 25
      Caption = 'Marca Todos (F6)'
      TabOrder = 1
      OnClick = btnMarcaTodasEmpClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnDesmarcaTodosEmp: TBitBtn
      Left = 241
      Top = 1
      Width = 118
      Height = 25
      Caption = 'Desm. Todos (F7)'
      TabOrder = 2
      OnClick = btnDesmarcaTodosEmpClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  object grdTrans: TJvDBGrid [3]
    Left = 0
    Top = 114
    Width = 1301
    Height = 555
    Align = alClient
    DataSource = dsTrans
    DefaultDrawing = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = grdTransDblClick
    AutoAppend = False
    TitleButtons = True
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'DATAHORA'
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'NSU'
        Width = 55
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'TRANS_ID'
        Title.Caption = 'ID. Trans'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPERADOR'
        Width = 70
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'CONV_ID'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TITULAR'
        Width = 200
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'CRED_ID'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CRED_ID_NOME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TELEFONE1'
        Width = 75
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'EMPRES_ID'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HISTORICO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BAND_ID'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'SEG_ID'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'CODACESSO'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'SENHA'
        Visible = False
      end>
  end
  inherited PopupBut: TPopupMenu
    Left = 28
    Top = 336
  end
  object dsTrans: TDataSource
    DataSet = MTrans
    Left = 88
    Top = 297
  end
  object dsEstab: TDataSource
    DataSet = qListEstab
    Left = 56
    Top = 296
  end
  object dsListEmp: TDataSource
    DataSet = qListEmp
    Left = 24
    Top = 296
  end
  object HTTPRIO1: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 88
    Top = 336
  end
  object qListEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome from empresas where apagado <> '#39'S'#39)
    Left = 24
    Top = 264
    object qListEmpempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qListEmpnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object qListEstab: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      
        'select cred_id, nome, fantasia, codacesso, senha from credenciad' +
        'os where apagado <> '#39'S'#39
      'order by nome')
    Left = 56
    Top = 264
    object qListEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qListEstabnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qListEstabfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object qListEstabcodacesso: TIntegerField
      FieldName = 'codacesso'
    end
    object qListEstabsenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
  end
  object qAutorTrans: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'trans_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ''
      'SELECT'
      '  trans_id,'
      '  autor_id,'
      '  digito,'
      '  valor,'
      '  datahora,'
      '  historico,'
      '  COALESCE(formapagto_id, 0) AS formapagto_id,'
      '  receita,'
      '  data_venc_emp,'
      '  data_fecha_emp,'
      '  data_venc_for,'
      '  data_fecha_for,'
      '  conv_id,'
      '  cred_id,'
      '  cartao_id'
      'FROM Autor_Transacoes'
      'WHERE trans_id = :trans_id')
    Left = 88
    Top = 264
    object qAutorTranstrans_id: TIntegerField
      FieldName = 'trans_id'
    end
    object qAutorTransautor_id: TIntegerField
      FieldName = 'autor_id'
    end
    object qAutorTransdigito: TWordField
      FieldName = 'digito'
    end
    object qAutorTransvalor: TBCDField
      FieldName = 'valor'
      Precision = 15
      Size = 2
    end
    object qAutorTransdatahora: TDateTimeField
      FieldName = 'datahora'
    end
    object qAutorTranshistorico: TStringField
      FieldName = 'historico'
      Size = 80
    end
    object qAutorTransformapagto_id: TIntegerField
      FieldName = 'formapagto_id'
      ReadOnly = True
    end
    object qAutorTransreceita: TStringField
      FieldName = 'receita'
      FixedChar = True
      Size = 1
    end
    object qAutorTransdata_venc_emp: TDateTimeField
      FieldName = 'data_venc_emp'
    end
    object qAutorTransdata_fecha_emp: TDateTimeField
      FieldName = 'data_fecha_emp'
    end
    object qAutorTransdata_venc_for: TDateTimeField
      FieldName = 'data_venc_for'
    end
    object qAutorTransdata_fecha_for: TDateTimeField
      FieldName = 'data_fecha_for'
    end
    object qAutorTransconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qAutorTranscred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qAutorTranscartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
  end
  object qTrans: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'dataIni'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'dataFin'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'cred_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'codcartimp'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'operador'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 25
        Value = Null
      end>
    SQL.Strings = (
      'select t.datahora, t.nsu, t.trans_id, '
      't.operador, atr.conv_id, c.titular, cred.cred_id,'
      'cred.fantasia cred_id_nome,  cred.telefone1, t.empres_id,'
      
        'emp.nome, atr.valor, atr.historico, t.cartao, cred.seg_id, emp.b' +
        'and_id,'
      'cred.codacesso, cred.senha'
      'from transacoes t'
      'join autor_transacoes atr on atr.trans_id = t.trans_id'
      'join credenciados cred on cred.cred_id = t.cred_id'
      'join conveniados c on c.conv_id = atr.conv_id'
      'join empresas emp on emp.empres_id = c.empres_id'
      
        'join cartoes cart on cart.cartao_id = t.cartao_id where t.aberta' +
        ' = '#39'S'#39
      'and t.confirmada = '#39'N'#39
      'and t.cancelado = '#39'N'#39
      'and t.datahora > '#39'30.04.2014 12:00:00'#39
      
        'and t.datahora between coalesce(:dataIni,t.datahora) and coalesc' +
        'e(:dataFin,t.datahora)'
      'and t.cred_id =:cred_id'
      'and c.empres_id =:empres_id'
      'and  cart.codcartimp = coalesce(:codcartimp,  cart.codcartimp)'
      'and t.operador = coalesce(:operador, t.operador)'
      'order by t.datahora')
    Left = 120
    Top = 264
    object qTransdatahora: TDateTimeField
      FieldName = 'datahora'
    end
    object qTransnsu: TIntegerField
      FieldName = 'nsu'
    end
    object qTranstrans_id: TIntegerField
      FieldName = 'trans_id'
    end
    object qTransoperador: TStringField
      FieldName = 'operador'
      Size = 25
    end
    object qTransconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qTranstitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object qTranscred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qTranscred_id_nome: TStringField
      FieldName = 'cred_id_nome'
      Size = 58
    end
    object qTranstelefone1: TStringField
      FieldName = 'telefone1'
      Size = 14
    end
    object qTransempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qTransnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qTransvalor: TBCDField
      FieldName = 'valor'
      Precision = 15
      Size = 2
    end
    object qTranshistorico: TStringField
      FieldName = 'historico'
      Size = 80
    end
    object qTranscartao: TStringField
      FieldName = 'cartao'
    end
    object qTransseg_id: TIntegerField
      FieldName = 'seg_id'
    end
    object qTransband_id: TIntegerField
      FieldName = 'band_id'
    end
    object qTranscodacesso: TIntegerField
      FieldName = 'codacesso'
    end
    object qTranssenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
  end
  object qContaCorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      '  *'
      'from contacorrente'
      'where autorizacao_id = 0')
    Left = 152
    Top = 264
    object qContaCorrenteAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object qContaCorrenteCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object qContaCorrenteCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object qContaCorrenteCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qContaCorrenteDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object qContaCorrenteDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object qContaCorrenteHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object qContaCorrenteDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object qContaCorrenteDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object qContaCorrenteCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object qContaCorrenteVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object qContaCorrenteBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object qContaCorrenteFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object qContaCorrenteFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qContaCorrentePAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object qContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object qContaCorrenteOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qContaCorrenteDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object qContaCorrenteDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object qContaCorrenteDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object qContaCorrenteDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object qContaCorrenteHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object qContaCorrenteNF: TIntegerField
      FieldName = 'NF'
    end
    object qContaCorrenteDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object qContaCorrenteDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object qContaCorrenteDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object qContaCorrenteOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object qContaCorrenteOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object qContaCorrenteDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object qContaCorrenteOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object qContaCorrenteCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteNSU: TIntegerField
      FieldName = 'NSU'
    end
    object qContaCorrentePREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qContaCorrenteEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object qEmp: TADOQuery
    Connection = DMConexao.AdoCon
    DataSource = dsTrans
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select'
      '  empres_id,'
      '  nome'
      'from'
      '  empresas'
      'where empres_id = :empres_id')
    Left = 184
    Top = 264
    object qEmpempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qEmpnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object MTrans: TJvMemoryData
    FieldDefs = <
      item
        Name = 'DATAHORA'
        DataType = ftDateTime
      end
      item
        Name = 'NSU'
        DataType = ftInteger
      end
      item
        Name = 'TRANS_ID'
        DataType = ftInteger
      end
      item
        Name = 'OPERADOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONV_ID'
        DataType = ftInteger
      end
      item
        Name = 'TITULAR'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CRED_ID'
        DataType = ftInteger
      end
      item
        Name = 'CRED_ID_NOME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TELEFONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EMPRES_ID'
        DataType = ftInteger
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'VALOR'
        DataType = ftFloat
      end
      item
        Name = 'HISTORICO'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'MARCADO'
        DataType = ftBoolean
      end
      item
        Name = 'BAND_ID'
        DataType = ftInteger
      end
      item
        Name = 'SEG_ID'
        DataType = ftInteger
      end
      item
        Name = 'CODACESSO'
        DataType = ftInteger
      end
      item
        Name = 'SENHA'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CARTAO'
        DataType = ftString
        Size = 20
      end>
    Left = 128
    Top = 296
    object MTransDATAHORA: TDateTimeField
      DisplayLabel = 'Data/Hora'
      FieldName = 'DATAHORA'
    end
    object MTransNSU: TIntegerField
      FieldName = 'NSU'
    end
    object MTransTRANS_ID: TIntegerField
      DisplayLabel = 'Trans. ID'
      FieldName = 'TRANS_ID'
    end
    object MTransOPERADOR: TStringField
      DisplayLabel = 'Op.'
      FieldName = 'OPERADOR'
    end
    object MTransCONV_ID: TIntegerField
      DisplayLabel = 'Conv. ID'
      FieldName = 'CONV_ID'
    end
    object MTransTITULAR: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'TITULAR'
      Size = 50
    end
    object MTransCRED_ID: TIntegerField
      DisplayLabel = 'Cred. ID'
      FieldName = 'CRED_ID'
    end
    object MTransCRED_ID_NOME: TStringField
      DisplayLabel = 'Nome do Estabelecimento'
      FieldName = 'CRED_ID_NOME'
      Size = 50
    end
    object MTransTELEFONE1: TStringField
      DisplayLabel = 'Telefone'
      FieldName = 'TELEFONE1'
    end
    object MTransEMPRES_ID: TIntegerField
      DisplayLabel = 'Emp. ID'
      FieldName = 'EMPRES_ID'
    end
    object MTransNOME: TStringField
      DisplayLabel = 'Nome da Empresa'
      FieldName = 'NOME'
      Size = 50
    end
    object MTransVALOR: TFloatField
      DisplayLabel = 'Valor'
      FieldName = 'VALOR'
      DisplayFormat = '##0.00'
    end
    object MTransHISTORICO: TStringField
      DisplayLabel = 'Hist'#243'rico'
      FieldName = 'HISTORICO'
      Size = 80
    end
    object MTransMARCADO: TBooleanField
      FieldName = 'MARCADO'
    end
    object MTransBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object MTransSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object MTransCODACESSO: TIntegerField
      FieldName = 'CODACESSO'
    end
    object MTransSENHA: TStringField
      FieldName = 'SENHA'
      Size = 30
    end
    object MTransCARTAO: TStringField
      FieldName = 'CARTAO'
    end
  end
end
