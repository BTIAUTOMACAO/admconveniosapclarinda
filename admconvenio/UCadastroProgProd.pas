unit UCadastroProgProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, JvExStdCtrls, JvEdit,
  JvValidateEdit, ExtCtrls, DB, ADODB, JvExControls, JvDBLookup, JvExMask,
  JvToolEdit, JvMaskEdit, JvDBControls;

type
  TFCadastroProgProd = class(TForm)
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    Panel1: TPanel;
    Label4: TLabel;
    txtCodBarra: TLabel;
    Label6: TLabel;
    txtDesc: TLabel;
    Label7: TLabel;
    txtValTot: TLabel;
    Panel2: TPanel;
    Label1: TLabel;
    btnGravar: TBitBtn;
    btnCancela: TBitBtn;
    LabAguarde: TLabel;
    cbBanco: TJvDBLookupCombo;
    DSProgramas: TDataSource;
    QProgramas: TADOQuery;
    QProgramasPROG_ID: TIntegerField;
    QProgramasNOME: TStringField;
    Label2: TLabel;
    txtDesconto: TEdit;
    procedure FormShow(Sender: TObject);
    procedure txtValorPagoKeyPress(Sender: TObject; var Key: Char);
    procedure btnGravarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroProgProd: TFCadastroProgProd;

implementation

{$R *.dfm}

procedure TFCadastroProgProd.FormShow(Sender: TObject);
begin
  QProgramas.Open;
end;

procedure TFCadastroProgProd.txtValorPagoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = '.' then Key := ',';
end;

procedure TFCadastroProgProd.btnGravarClick(Sender: TObject);
var
strSql,prod_id, prog_id : string;

begin
  //BUSCA PROD_ID NA TABELA BARRAS MEDIANTE AO CODIGO DE BARRAS

  //insere produto da produto autorizador na prog_prod
//  strSql := ''+
//      'INSERT INTO PROG_PROD   '+
//      '     PROD_ID             '+
//      '    ,PROG_ID            '+
//      '    ,PRC_UNIT           '+
//      '    ,PERC_DESC          '+
//      '    ,QTD_MAX            '+
//      '    ,OBRIG_RECEITA      '+
//      '    ,ATIVO              '+
//      '    ,VALE_PERC          '+
//      '    ,PRC_UNIT_18        '+
//      '    ,PRC_UNIT_19        '+
//      'VALUES                  '+

end;

end.
