unit URelRepasseAnalitico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, RxMemDS, DB, JvMemoryDataset, frxClass, frxDBSet,
  ADODB, Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid, ExtCtrls, Mask,
  JvExMask, JvToolEdit, StdCtrls, JvExControls, JvDBLookup, Menus,
  JvDialogs, frxExportPDF, U1;

type
  TfrmRelRepasseAnalitico = class(TF1)
    SD: TJvSaveDialog;
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    Minimizar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    PanAbe: TPanel;
    Bevel4: TBevel;
    Bevel2: TBevel;
    Label1: TLabel;
    cbbEstab: TJvDBLookupCombo;
    edtEstab: TEdit;
    gpbDatas: TGroupBox;
    lblDe: TLabel;
    lblA: TLabel;
    ckbTodasDatas: TCheckBox;
    edtDia: TEdit;
    dataIni: TJvDateEdit;
    DataFin: TJvDateEdit;
    GroupBox2: TGroupBox;
    cbbPgtoPor: TJvDBLookupCombo;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Edit1: TEdit;
    dataCompensa: TJvDateEdit;
    cbbSetManual: TCheckBox;
    rbEstab: TRadioGroup;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel4: TPanel;
    Panel5: TPanel;
    GridDescontos: TJvDBGrid;
    Panel7: TPanel;
    GridValEmp: TJvDBGrid;
    Panel8: TPanel;
    Panel2: TPanel;
    Bevel7: TBevel;
    BtnImprimir: TBitBtn;
    BtnMarcDesm: TButton;
    BtnMarcaTodos: TButton;
    BtnDesmTodos: TButton;
    ckQuebraPagina: TCheckBox;
    bntGerarPDF: TBitBtn;
    Panel9: TPanel;
    Bevel1: TBevel;
    Panel10: TPanel;
    Label12: TLabel;
    LabLiq: TLabel;
    Label14: TLabel;
    LabBruto: TLabel;
    Label8: TLabel;
    LabDesc: TLabel;
    Panel3: TPanel;
    dsEmpr: TDataSource;
    dsEstab: TDataSource;
    dsPgtoEstab: TDataSource;
    dsPgtoEmp: TDataSource;
    dsPgtoDesc: TDataSource;
    dsPgtoPor: TDataSource;
    dsTemp: TDataSource;
    dsBancos: TDataSource;
    qPgtoPor: TADOQuery;
    qPgtoPorPAGA_CRED_POR_ID: TIntegerField;
    qPgtoPorDESCRICAO: TStringField;
    qEmpr: TADOQuery;
    qEmprempres_id: TIntegerField;
    qEmprnome: TStringField;
    qEmprfantasia: TStringField;
    qEstab: TADOQuery;
    qEstabcred_id: TIntegerField;
    qEstabnome: TStringField;
    qEstabfantasia: TStringField;
    qPgtoEstab: TADOQuery;
    qPgtoEstabcred_id: TIntegerField;
    qPgtoEstabnome: TStringField;
    qPgtoEstabdiafechamento1: TWordField;
    qPgtoEstabvencimento1: TWordField;
    qPgtoEstabCORRENTISTA: TStringField;
    qPgtoEstabcgc: TStringField;
    qPgtoEstabCOMISSAO: TBCDField;
    qPgtoEstabCONTACORRENTE: TStringField;
    qPgtoEstabAGENCIA: TStringField;
    qPgtoEstabCODBANCO: TIntegerField;
    qPgtoEstabNOME_BANCO: TStringField;
    qPgtoEstabBAIXADO: TStringField;
    qPgtoEstabBRUTO: TBCDField;
    qPgtoEstabTAXA_EXTRA: TFloatField;
    qPgtoEstabCOMISSAO_ADM: TBCDField;
    qPgtoEstabTOTAL_RETIDO_ADM: TFloatField;
    qPgtoEstabLIQUIDO: TFloatField;
    qPgtoEstabATRASADO: TStringField;
    qPgtoEstabTX_DVV: TFloatField;
    qPgtoEmpr: TADOQuery;
    qPgtoEmprcred_id: TIntegerField;
    qPgtoEmprempres_id: TIntegerField;
    qPgtoEmprfatura_id: TIntegerField;
    qPgtoEmprdata_fatura: TDateTimeField;
    qPgtoEmprnome: TStringField;
    qPgtoEmprCOMISSAO: TBCDField;
    qPgtoEmprBRUTO: TBCDField;
    qPgtoEmprCOMISSAO_ADM: TBCDField;
    qPgtoEmprLIQUIDO: TBCDField;
    qTemp: TADOQuery;
    qBancos: TADOQuery;
    qBancoscodbanco: TIntegerField;
    qBancosbanco: TStringField;
    frxReport1: TfrxReport;
    frxBancos: TfrxDBDataset;
    frxPgtoEstab: TfrxDBDataset;
    MDPagtoEstab: TJvMemoryData;
    MDPagtoEstabnome: TStringField;
    MDPagtoEstabcorrentista: TStringField;
    MDPagtoEstabcgc: TStringField;
    MDPagtoEstabcomissao: TCurrencyField;
    MDPagtoEstabcontacorrente: TStringField;
    MDPagtoEstabagencia: TStringField;
    MDPagtoEstabbruto: TCurrencyField;
    MDPagtoEstabtaxa_extra: TCurrencyField;
    MDPagtoEstabcomissao_adm: TCurrencyField;
    MDPagtoEstabtotal_retido_adm: TCurrencyField;
    MDPagtoEstabliquido: TCurrencyField;
    MDPagtoEstabcred_id: TIntegerField;
    MDPagtoEstabmarcado: TBooleanField;
    MDPagtoEstabdiafechamento1: TIntegerField;
    MDPagtoEstabvencimento1: TIntegerField;
    MDPagtoEstabCODBANCO: TIntegerField;
    MDPagtoEstabNOME_BANCO: TStringField;
    MDPagtoEstabBAIXADO: TStringField;
    MDPagtoEstabATRASADO: TStringField;
    MDPagtoEstabTX_DVV: TFloatField;
    MDPagtoEstabPAGAMENTO_CRED_ID: TIntegerField;
    MDPgtoDesc: TRxMemoryData;
    MDPgtoDesccred_id: TIntegerField;
    MDPgtoDesctaxa_id: TIntegerField;
    MDPgtoDescdescricao: TStringField;
    MDPgtoDescvalor: TFloatField;
    MDPgtoDescmarcado: TBooleanField;
    CDSPagDesc: TClientDataSet;
    CDSPagDesccred_id: TIntegerField;
    CDSPagDesctaxa_id: TIntegerField;
    CDSPagDescdescricao: TStringField;
    CDSPagDescvalor: TFloatField;
    CDSPagDescmarcado: TBooleanField;
    QAux: TADOQuery;
    DSAux: TDataSource;
    popCancTaxa: TPopupMenu;
    CancelaAutorizao1: TMenuItem;
    QLancCred: TADOQuery;
    qPgtoDescCRED_ID: TIntegerField;
    qPgtoDesctaxa_id: TIntegerField;
    qPgtoDescdescricao: TStringField;
    qPgtoDescvalor: TFloatField;
    qPgtoDescmarcado: TStringField;
    qPgtoDescid: TIntegerField;
    qPgtoDescTAXAS_PROX_PAG_ID_FK: TIntegerField;
    btnConsultar: TBitBtn;
    qPgtoDesc: TADOQuery;
    JvDBGrid1: TJvDBGrid;
    frxPDFExport1: TfrxPDFExport;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SQLFactory;
    procedure SQLDestroyString;
    procedure AbrirPagamentos;
    procedure ZerarLabels;
    procedure HabilitarBotoes;
    procedure AbrirValoresDescontos;
    procedure btnConsultarClick(Sender: TObject);
    procedure dsPgtoPorDataChange(Sender: TObject; Field: TField);
    procedure dataCompensaChange(Sender: TObject);
    procedure qPgtoEstabBeforeOpen(DataSet: TDataSet);
    procedure dsPgtoEstabStateChange(Sender: TObject);
    procedure dsPgtoEstabDataChange(Sender: TObject; Field: TField);
    procedure qPgtoEmprBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelRepasseAnalitico: TfrmRelRepasseAnalitico;
  MarcouTodos, DesmarcouTodos : Boolean;
  sql : string;

implementation

uses UMenu, DM, cartao_util, DateUtils;

{$R *.dfm}

procedure TfrmRelRepasseAnalitico.FormCreate(Sender: TObject);
begin
  DataIni.Date := Date;
  DataFin.Date := Date;
  //HabilitarBotoes;
  qEmpr.Open;
  qEstab.Open;
  qPgtoPor.Open;
  cbbPgtoPor.KeyValue := 1;
  FMenu.vRelRepasseAnalitico := True;
  //taxa_do_banco_old := 0;
end;

procedure TfrmRelRepasseAnalitico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FMenu.vRelRepasseAnalitico := False;
end;

procedure TfrmRelRepasseAnalitico.SQLFactory;
begin
   // O from � baseado na tabela credenciados cred
   sql := ''                          +
   'SELECT * FROM('                   +
   'SELECT '                          +
   'cred.cred_id,'                    +
   'cred.nome,'                       +
   'cred.diafechamento1,'             +
   'cred.vencimento1,'                +
   'cred.CORRENTISTA,'                +
   'cred.cgc,'                        +
   'cred.COMISSAO,'                   +
   'cred.CONTACORRENTE,'              ;
   if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 2 then
     sql:= sql + 'cast(((coalesce(cred.TX_DVV,0.0) * 41)/100) as float) as TX_DVV ,'
   else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 10 then
     sql:= sql + 'cast(((coalesce(cred.TX_DVV,0.0) * 30)/100)as float) as TX_DVV, '
   else
     sql := sql + '0.0 as TX_DVV, ';
   sql := sql + 'cred.AGENCIA,'       +
   'cc.BAIXA_CREDENCIADO AS BAIXADO, '+
   //'cc.PAGAMENTO_CRED_ID, '           +
   'cred.BANCO as CODBANCO,'          +
   'ba.BANCO AS NOME_BANCO,'          +
   'COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'+
   '0.0 AS TAXA_EXTRA,'+
   //'(SELECT COALESCE(SUM(t.valor),0)'+
   //'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA,'+
   //-----------------------------------------------------------------------------
   //--seleciona TAXA ADMINISTRADORA(COMISS�O)'+
   '(coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0)) as COMISSAO_ADM,'+
   //'-- fim da consulta TAXA ADMINISTRADORA(COMISS�O)'+
   //'-----------------------------------------------------------------------------'+
   //'--seleciona TOTAL RETIDO PELA ADMINISTRADORA'+
   '(coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100))'+
   '+'+
   '(SELECT COALESCE(SUM(t.valor),0)'+
   'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'+
   'AS TOTAL_RETIDO_ADM,'+
   //'-- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'+
   //'-----------------------------------------------------------------------------'+
   //'--seleciona o TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
   //'--(BRUTO - (TAXAS EXTRAS - COMISSAO))'+
   '((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0))'+
   '-'+
   '((coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0))+(SELECT COALESCE(SUM(t.valor),0)'+
   'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID))'+
   ') AS LIQUIDO'+
   //'--Fim da consulta TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
   //'-----------------------------------------------------------------------------'+
   ', ''N'' as ATRASADO'+
   ' from credenciados cred'+
   ' LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between :DATA_INICIAL and :DATA_FINAL';

    //RETORNA APENAS OS PAGAMENTOS CONFIRMADOS

    sql := sql+' AND cc.BAIXA_CREDENCIADO = ''S''';

    sql := sql+' LEFT JOIN BANCOS ba ON ba.codigo = cred.banco';
    if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 6 then
    begin
      sql := sql+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID';
      if rbEstab.ItemIndex = 1 then
        sql := sql+' AND CRED.SEG_ID = 14'
      else
        sql := sql+' AND CRED.SEG_ID = 39';
    end;

    if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 7 then
    begin
      sql := sql+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID and CRED.SEG_ID = 39';
    end;

    if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 8 then
    begin
      sql := sql+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SEG_ID = 39';
    end;

    if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 4 then
    begin
      sql := sql+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SEG_ID <> 39 and CRED.SEG_ID <> 14 AND cred.PAGA_CRED_POR_ID in (4,1) where cred.APAGADO = ''N''';
    end
    else
      sql := sql+' WHERE cred.PAGA_CRED_POR_ID = '+qPgtoPorPAGA_CRED_POR_ID.AsString +' AND cred.APAGADO = ''N''';

    if edtEstab.Text <> '' then
    begin
      sql := sql+' AND cred.CRED_ID = '+qEstabcred_id.AsString;
    end;
      {if rdgPagamentos.ItemIndex < 2 then begin
        sql := sql+' GROUP BY cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
        ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV';

      end}
    //else
    //begin
    sql := sql+' GROUP BY CC.PAGAMENTO_CRED_ID, cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
      ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV';

    //end;
    sql := sql + ' )A WHERE A.BRUTO <> 0';
end;

procedure TfrmRelRepasseAnalitico.SQLDestroyString;
begin
  sql := '';
end;



procedure TfrmRelRepasseAnalitico.btnConsultarClick(Sender: TObject);
begin
  inherited;
  ZerarLabels;
  Screen.Cursor := crHourGlass;
  DMConexao.ExecuteSql('delete from taxas_repasse_temp');

  if (cbbEstab.DisplayEmpty <> cbbEstab.Text) and (string(cbbEstab.KeyValue) <> edtEstab.Text) then begin
    MsgInf('O c�digo do Estabelecimento n�o corresponde ao estabelecimento selecionado.'+sLineBreak+'Por favor, selecione o estabelecimento correto.');
    cbbEstab.SetFocus;
    Exit;
  end;
  if cbbPgtoPor.KeyValue = 0 then
    MsgInf('Selecione uma forma de pagamento de estabelecimento.');

  if (cbbPgtoPor.KeyValue = 3) and (edtDia.Text = '') then
  begin
    MsgInf('Digite um dia do fechamento');
    edtDia.SetFocus;
    Exit;
  end;

  if integer(cbbPgtoPor.KeyValue) >= 1 then
  begin
    AbrirPagamentos;
    //Button1.Enabled      := true;
  end
  else
    raise Exception.Create('Forma de pagamento de Fornecedores Inv�lida!');

  Screen.Cursor := crDefault;
end;

procedure TfrmRelRepasseAnalitico.AbrirPagamentos;
var mes : integer;
  dia : string;
  CNPJsoNumero,imprimeCNPJ : String;
  i : Integer;
  lista_credID : TList;
  valor_taxa,total_adm,total,tx_dvv : Double;
begin
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >= 2) then
  begin
    SQLFactory; //Esta Procedure cria a query din�mica atribuindo valor a uma
    //vari�vel global chamada sql para passar como par�metro pro m�todo ADD da propriedade SQL
    qPgtoEstab.Close;
    qPgtoEstab.SQL.Clear;
    qPgtoEstab.SQL.Add(sql);
    qPgtoEstab.Open;
    if(qPgtoEstab.IsEmpty)then
    begin
      MDPagtoEstab.EmptyTable;
      qPgtoDesc.Close;
      qPgtoEmpr.Close;
      MsgInf('Os crit�rios de busca aplicados n�o retornaram nenhum valor!');
      exit;
    end;
    qPgtoEstab.First;
    MDPagtoEstab.Open;
    MDPagtoEstab.EmptyTable;
    MDPagtoEstab.DisableControls;
    while not qPgtoEstab.Eof do
    begin
      //if qPgtoEstabBRUTO.AsCurrency > 0.0 then begin
        MDPagtoEstab.Append;
        MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
        MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
        MDPagtoEstabdiafechamento1.AsInteger    := qPgtoEstabdiafechamento1.AsInteger;
        MDPagtoEstabvencimento1.AsInteger       := qPgtoEstabvencimento1.AsInteger;
        //MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        qPgtoEstabCORRENTISTA.AsString;
        //CNPJsoNumero := RetornaNumeros(qPgtoEstabCORRENTISTA.AsString);
        if qPgtoEstabCORRENTISTA.AsString <> '' then
        begin
          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
          end;
          if Length(CNPJsoNumero) = 14 then
          begin
            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
          end
          else
          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
        end
        else
        begin
          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        end;
        MDPagtoEstabcgc.AsString                := qPgtoEstabcgc.AsString;
        MDPagtoEstabcomissao.AsFloat            := qPgtoEstabCOMISSAO.AsFloat;
        MDPagtoEstabcontacorrente.AsString      := qPgtoEstabCONTACORRENTE.AsString;
        MDPagtoEstabagencia.AsString            := qPgtoEstabAGENCIA.AsString;
        MDPagtoEstabCODBANCO.AsInteger          := qPgtoEstabCODBANCO.AsInteger;
        MDPagtoEstabNOME_BANCO.AsString         := qPgtoEstabNOME_BANCO.AsString;
        MDPagtoEstabbruto.AsFloat               := qPgtoEstabBRUTO.AsFloat;
        MDPagtoEstabtaxa_extra.AsFloat          := qPgtoEstabTAXA_EXTRA.AsFloat;

        MDPagtoEstabcomissao_adm.AsFloat        := qPgtoEstabCOMISSAO_ADM.AsFloat;
        total_adm := (qPgtoEstabBRUTO.AsFloat * (qPgtoEstabCOMISSAO.AsFloat/100)) + qPgtoEstabTAXA_EXTRA.AsFloat + MDPagtoEstabtaxa_extra.AsFloat;
        MDPagtoEstabtotal_retido_adm.AsFloat    := total_adm;
        if total_adm > qPgtoEstabBRUTO.AsFloat then begin
          MDPagtoEstabliquido.AsFloat           := qPgtoEstabBRUTO.AsFloat - qPgtoEstabCOMISSAO_ADM.AsFloat;
        end
        else
        MDPagtoEstabliquido.AsFloat             := qPgtoEstabBRUTO.AsFloat - total_adm;

        if(qPgtoPorPAGA_CRED_POR_ID.AsInteger in [2,10]) then
        begin
          tx_dvv := qPgtoEstabTX_DVV.AsFloat * MDPagtoEstabliquido.AsFloat;
          MDPagtoEstabliquido.AsFloat           := MDPagtoEstabliquido.AsFloat - (tx_dvv);
          MDPagtoEstabTX_DVV.AsFloat            := tx_dvv;
        end
        else
        MDPagtoEstabTX_DVV.AsFloat              := 0.0;
        MDPagtoEstabBAIXADO.AsString            := qPgtoEstabBAIXADO.AsString;
        MDPagtoEstabATRASADO.AsString           := qPgtoEstabATRASADO.AsString;
        //MDPagtoEstabPAGAMENTO_CRED_ID.AsInteger := qPgtoEstabpagamento_cred_id.AsInteger;
        MDPagtoEstabmarcado.AsBoolean           := False;
        MDPagtoEstab.Post;
      //end;
      CNPJsoNumero := '';
      qPgtoEstab.Next;
    end;
    MDPagtoEstab.Next;
    MDPagtoEstab.First;
    MDPagtoEstab.EnableControls;
    //qPgtoEstab.Requery();  // adicionado hoe 06/01 - teste
    SQLDestroyString;


  end

end;

procedure TfrmRelRepasseAnalitico.ZerarLabels;
begin
  LabLiq.Caption   := '0,00';
  LabBruto.Caption := '0,00';
  //LabComis.Caption := '0,00';
  LabDesc.Caption  := '0,00';
  MarcouTodos := False;
  DesmarcouTodos := False;
end;

procedure TfrmRelRepasseAnalitico.dsPgtoPorDataChange(Sender: TObject;
  Field: TField);

var data_ini_aux : TDateTime;
var Ultimodia,PrimeiroDia,CurDate : TDateTime;
var    data_i,myDate : TDate;
begin
  inherited;
  if (qPgtoPorPAGA_CRED_POR_ID.Value = 2)then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira(default),''dd/MM/yyyy'')');
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-14,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-8,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 4) then
  begin
    if(DayOf(Date) < 12) then
    begin
      myDate := EncodeDate(YearOf(Date),MonthOf(Date),11);
      dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
      DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(Date))));
    end
    else
    begin
      myDate := EncodeDate(YearOf(Date),MonthOf(Date),26);
      dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),1));
      DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),15));
    end;
    dataCompensa.Text := DateToStr(myDate);
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 6) then
  begin
    if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-42,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-36,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 7) then
  begin
    if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-57,(dbo.getTercaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-51,(dbo.getTercaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 8) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth(default),''dd/MM/yyyy'')');
    dataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(mm,-1,dateadd(dd,-day('+QuotedStr(DateToStr(dataCompensa.Date))+')+1,'+QuotedStr(DateToStr(dataCompensa.Date))+')),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('SELECT format(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'+QuotedStr(DateToStr(dataIni.Date))+')+1,0)),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 9) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(getdate(),''dd/MM/yyyy'')');
    DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 13) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira(default),''dd/MM/yyyy'')');
    dataIni.Text      := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-7,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    dataFin.Text      := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-1,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 12) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia10(default),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1)
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 11) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia14(default),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 10) then
  begin
    PrimeiroDia := StartOfTheMonth(Now);
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 14) then
  begin
    dataCompensa.Text := DateToStr(IncDay(now,1));
    DataFin.Date := Now;
    dataIni.Date := Now;
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 15) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(getdate(),''dd/MM/yyyy'')');
    DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
  end;
end;


procedure TfrmRelRepasseAnalitico.dataCompensaChange(Sender: TObject);
var CurDate, PrimeiroDia, dataCompensaOld,myDate, dataCompensaChanged,dataParam : TDateTime;
begin
  inherited;
  if not cbbSetManual.Checked then
  begin
    if(qPgtoPorPAGA_CRED_POR_ID.Value = 2)then begin
      dataCompensa.Text := DMConexao.ExecuteQuery('SELECT FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-14,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-8,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

     if(qPgtoPorPAGA_CRED_POR_ID.Value = 4)then
     begin
        if(DayOf(dataCompensa.Date) < 12) then
        begin
          myDate := EncodeDate(YearOf(dataCompensa.Date),MonthOf(dataCompensa.Date),11);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(dataIni.Date))));
        end
        else if (DayOf(dataCompensa.Date) > 26) then
        begin
          myDate := EncodeDate(YearOf((IncMonth(dataCompensa.Date,1))),MonthOf(IncMonth(dataCompensa.Date,1)),11);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(dataIni.Date))));

        end
        else
        begin
          myDate := EncodeDate(YearOf(dataCompensa.Date),MonthOf(dataCompensa.Date),26);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),1));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),15));
        end;
        dataCompensa.Text := DateToStr(myDate);
     end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 6) then
    begin
      if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
        dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-42,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
        DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-36,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
        dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-43,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
        DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-37,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');

      end;
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 7) then
    begin
      if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-57,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-51,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 8) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(mm,-1,dateadd(dd,-day('+QuotedStr(DateToStr(dataCompensa.Date))+')+1,'+QuotedStr(DateToStr(dataCompensa.Date))+')),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('SELECT format(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'+QuotedStr(DateToStr(dataIni.Date))+')+1,0)),''dd/MM/yyyy'')');
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 9) then
    begin
      //dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');

    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 13) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-7,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-1,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 12) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia10('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataFin.Date := EndOfTheMonth(dataCompensa.Date);
      dataIni.Date := StartOfTheMonth(dataCompensa.Date);
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 11) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia14('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
      dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 10) then
    begin
       dataCompensaOld := dataCompensa.Date;
       PrimeiroDia := StartOfTheMonth(dataCompensa.Date);
       dataCompensaChanged := StrToDate(DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')'));
       if dataCompensaOld <= dataCompensaChanged then
       begin
         DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
         dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
         dataCompensa.Text := DateToStr(dataCompensaChanged);
       end
       else begin
         PrimeiroDia := StartOfTheMonth(IncMonth(dataCompensaOld,1));
         DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
         dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
         dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')');
       end;
    end;
                                                                                         
    if(qPgtoPorPAGA_CRED_POR_ID.Value = 14) then
    begin
      DataFin.Date := IncDay(StrToDate(dataCompensa.Text),-1);
      dataIni.Date := IncDay(StrToDate(dataCompensa.Text),-1)
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 15) then
    begin
      //dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');

    end;

  end;
end;

procedure TfrmRelRepasseAnalitico.HabilitarBotoes;
begin
  BtnMarcDesm.Enabled     := true;
  BtnMarcaTodos.Enabled   := true;
  BtnDesmTodos.Enabled    := true;
  //BtnEfetuar.Enabled      := not qPgtoEstab.IsEmpty;
  BtnImprimir.Enabled     := not qPgtoEstab.IsEmpty;
  bntGerarPDF.Enabled     := not qPgtoEstab.IsEmpty;
  //BtnCancelarPag.Enabled  := not qPgtoEstab.IsEmpty;
end;

procedure TfrmRelRepasseAnalitico.qPgtoEstabBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >=2) then
  begin
    qPgtoEstab.Parameters[0].Value := dataIni.Text;
    qPgtoEstab.Parameters[1].Value := DataFin.Text;
  end;
end;

procedure TfrmRelRepasseAnalitico.dsPgtoEstabStateChange(Sender: TObject);
begin
  HabilitarBotoes;
end;

procedure TfrmRelRepasseAnalitico.dsPgtoEstabDataChange(Sender: TObject;
  Field: TField);
begin
   if not MDPagtoEstab.IsEmpty then
   begin
     AbrirValoresDescontos;
     qPgtoEmpr.Close;
     qPgtoEmpr.SQL.Clear;
     qPgtoEmpr.SQL.Add('SELECT cred.cred_id,cc.empres_id,fat.fatura_id,fat.data_fatura,e.nome, ');
     qPgtoEmpr.SQL.Add('cred.COMISSAO, ');
     qPgtoEmpr.SQL.Add('COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO, ');
     qPgtoEmpr.SQL.Add('(coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0)) as COMISSAO_ADM, ');
     qPgtoEmpr.SQL.Add('((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0)) - (coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0))) AS LIQUIDO ');
     qPgtoEmpr.SQL.Add('from credenciados cred ');
     if MDPagtoEstabATRASADO.AsString = 'S' then
     begin
        qPgtoEmpr.SQL.Add('LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between :data_ini and :data_fim ');
     end
     else
        qPgtoEmpr.SQL.Add('LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between :data_ini and :data_fim ');
     qPgtoEmpr.SQL.Add('and cred.cred_id = '+MDPagtoEstabcred_id.AsString);
     qPgtoEmpr.SQL.Add(' LEFT JOIN FATURA fat ON fat.FATURA_ID = CC.FATURA_ID');
     qPgtoEmpr.SQL.Add(' INNER JOIN EMPRESAS e ON e.EMPRES_ID = cc.EMPRES_ID');
     qPgtoEmpr.SQL.Add(' WHERE PAGA_CRED_POR_ID = '+qPgtoPorPAGA_CRED_POR_ID.AsString);
     qPgtoEmpr.SQL.Add(' GROUP BY cred.CRED_ID,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,');
     qPgtoEmpr.SQL.Add(' cred.CONTACORRENTE,fat.DATA_FATURA,cred.AGENCIA,cred.COMISSAO,CC.EMPRES_ID,FAT.FATURA_ID,FAT.TIPO,e.nome');
     qPgtoEmpr.Open;
   end
end;
  //Screen.Cursor := crDefault;


procedure TfrmRelRepasseAnalitico.AbrirValoresDescontos;
var {sql : TSqlMount;}
    data : TDateTime;
    mes,ano : Integer;
    mesConvertido : Integer;
var taxa_id, cred_id,flag : Integer;

begin
  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA N�O POSSUI TAXAS EXTRAS ATRASADAS
  if MDPagtoEstabATRASADO.AsString = 'N' then
  begin
    if MDPagtoEstabBAIXADO.AsString = 'N' then
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Add('SELECT tr.ID id,rtc.CRED_ID,rtc.taxa_id,t.descricao,t.valor,coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK ');
      qPgtoDesc.SQL.Add('FROM REL_TAXA_CRED rtc LEFT JOIN taxas_repasse_temp tr ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id');
      qPgtoDesc.SQL.Add(',taxas t where rtc.taxa_id = t.taxa_id');
      qPgtoDesc.SQL.Add(' and rtc.TAXA_ID not in (select taxa_id from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString);
      qPgtoDesc.SQL.Add(' and datepart(mm,(select max(dt_desconto) from TAXAS_REPASSE where cred_id = '+MDPagtoEstabcred_id.AsString+'))');
      data := dataCompensa.Date;
      mesConvertido := MonthOf(data);
      qPgtoDesc.SQL.Add(' = '+QuotedStr(IntToStr(mesConvertido))+') and rtc.cred_id = '+MDPagtoEstabcred_id.AsString);
      qPgtoDesc.SQL.Add(' UNION ALL');
      qPgtoDesc.SQL.Add(' SELECT  TR.ID, TPP.CRED_ID, 0 AS TAXA_ID, TPP.descricao, TPP.valor, coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK');
      qPgtoDesc.SQL.Add(' FROM TAXAS_PROX_PAG TPP LEFT JOIN taxas_repasse_temp tr ON TPP.TAXAS_PROX_PAG_ID = TR.TAXAS_PROX_PAG_ID_FK WHERE TPP.CRED_ID = '+MDPagtoEstabcred_id.AsString+'');
      qPgtoDesc.SQL.Text;
      qPgtoDesc.Open;
    end
    //Se dados do grid principal forem BAIXADOS = 'S'
    else
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Add('SELECT CONVERT(INT,next value for STAXAS_TEMP) id, tr.cred_id, tr.taxa_id,t.descricao,t.valor,coalesce(trt.marcado,''N'') marcado,coalesce(TR.TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK');
      qPgtoDesc.SQL.Add('from taxas_repasse tr LEFT JOIN taxas_repasse_temp trt ON tr.cred_id = trt.cred_id and tr.taxa_id = trt.taxa_id');
      qPgtoDesc.SQL.Add('inner join taxas t ON t.taxa_id = tr.taxa_id');
      qPgtoDesc.SQL.Add('WHERE tr.cred_id = '+MDPagtoEstabcred_id.AsString+' and tr.dt_desconto = '+QuotedStr(dataIni.Text)+'');
      qPgtoDesc.Open;
    end;
  end
  
  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA POSSUI TAXAS EXTRAS ATRASADAS. CASO ENTRE NESTA CONDI��O BUSCA TAXA NA TABELA
  //TAXAS_REPASSE_ATRASADA 
  else
  begin
    qPgtoDesc.Close;
    qPgtoDesc.SQL.Clear;
    qPgtoDesc.SQL.Add('SELECT tra.id id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
    qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
    qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
    //qPgtoDesc.SQL.Add('INNER JOIN taxas_repasse tr ON tr.taxa_id = tra.taxa_id AND TR.CRED_ID = TRA.CRED_ID');
    qPgtoDesc.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString+' AND tra.baixado = ''N''');
    //qPgtoDesc.SQL.Add('and tra.taxa_id not in (select taxa_id from taxas_repasse where cred_id = '+MDPagtoEstabcred_id.AsString+' and negociada = ''S'')');
    qPgtoDesc.Open;
  end;
end;

procedure TfrmRelRepasseAnalitico.qPgtoEmprBeforeOpen(DataSet: TDataSet);
begin
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >=2) then
  begin
    qPgtoEmpr.Parameters[0].Value := dataIni.Text;
    qPgtoEmpr.Parameters[1].Value := DataFin.Text;
  end;
end;

end.
