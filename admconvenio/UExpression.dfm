object FExpression: TFExpression
  Left = 226
  Top = 191
  BorderStyle = bsSingle
  Caption = 'Criando a express'#227'o...'
  ClientHeight = 277
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 140
    Top = 27
    Width = 3
    Height = 249
  end
  object Campos: TListBox
    Left = 0
    Top = 28
    Width = 136
    Height = 249
    Hint = 'Clique duas vezes sobre o campo para adicion'#225'-lo a empress'#227'o.'
    Align = alLeft
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnDblClick = CamposDblClick
    OnKeyDown = CamposKeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 478
    Height = 28
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 1
    object Label1: TLabel
      Left = 5
      Top = 8
      Width = 38
      Height = 13
      Caption = 'Campos'
    end
    object Label2: TLabel
      Left = 151
      Top = 8
      Width = 49
      Height = 13
      Caption = 'Express'#227'o'
    end
    object Label10: TLabel
      Left = 415
      Top = 8
      Width = 53
      Height = 13
      Caption = 'F1 - Help'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Memo1: TMemo
    Left = 148
    Top = 27
    Width = 330
    Height = 115
    Align = alCustom
    TabOrder = 2
    OnChange = Memo1Change
    OnKeyPress = Memo1KeyPress
  end
  object Panel2: TPanel
    Left = 148
    Top = 143
    Width = 330
    Height = 133
    Align = alCustom
    BorderStyle = bsSingle
    TabOrder = 3
    object ButParent1: TSpeedButton
      Left = 11
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Abrir par'#234'ntese'
      Caption = '('
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButParent2: TSpeedButton
      Left = 41
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Fechar Par'#234'ntese'
      Caption = ')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButDividir: TSpeedButton
      Left = 71
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Dividir'
      Caption = '/'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButMultip: TSpeedButton
      Left = 101
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Multiplicar'
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButSoma: TSpeedButton
      Left = 131
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Somar'
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButParentdup: TSpeedButton
      Left = 11
      Top = 62
      Width = 30
      Height = 22
      Hint = 'Abrir e fechar par'#234'nteses'
      Caption = '(  )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParentdupClick
    end
    object ButSub: TSpeedButton
      Left = 162
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Subtrair'
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButValidate: TSpeedButton
      Left = 192
      Top = 66
      Width = 121
      Height = 22
      Hint = 'Validar a express'#227'o'
      Caption = 'Validar a empress'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButValidateClick
    end
    object Bevel2: TBevel
      Left = 0
      Top = 89
      Width = 324
      Height = 2
    end
    object ButDiv: TSpeedButton
      Left = 41
      Top = 62
      Width = 30
      Height = 22
      Hint = 'Somente retorno inteiro da divis'#227'o'
      Caption = 'Div'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButMod: TSpeedButton
      Left = 71
      Top = 62
      Width = 30
      Height = 22
      Hint = 'Resto da divis'#227'o'
      Caption = 'Mod'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But0: TSpeedButton
      Left = 11
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Zero'
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But1: TSpeedButton
      Left = 41
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Um'
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But2: TSpeedButton
      Left = 71
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Dois'
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But3: TSpeedButton
      Left = 101
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Tr'#234's'
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But4: TSpeedButton
      Left = 131
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Quatro'
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But5: TSpeedButton
      Left = 162
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Cinco'
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But6: TSpeedButton
      Left = 192
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Seis'
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But7: TSpeedButton
      Left = 222
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Sete'
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But8: TSpeedButton
      Left = 252
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Oito'
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object But9: TSpeedButton
      Left = 282
      Top = 4
      Width = 30
      Height = 22
      Hint = 'Nove'
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButPonto: TSpeedButton
      Left = 193
      Top = 34
      Width = 30
      Height = 22
      Hint = 'Ponto'
      Caption = '.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = ButParent1Click
    end
    object ButClear: TSpeedButton
      Left = 263
      Top = 35
      Width = 49
      Height = 22
      Caption = '&Limpar'
      OnClick = ButClearClick
    end
    object Label3: TLabel
      Left = 5
      Top = 112
      Width = 141
      Height = 13
      Caption = 'Aten'#231#227'o, validar antes do Ok.'
    end
    object ButOk: TButton
      Left = 170
      Top = 96
      Width = 75
      Height = 25
      Caption = '&Ok'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
    end
    object ButCancel: TButton
      Left = 245
      Top = 96
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
    end
  end
  object RichEdit1: TRichEdit
    Left = 40
    Top = 500
    Width = 273
    Height = 255
    Lines.Strings = (
      ' Configura'#231#227'o da Express'#227'o:'
      
        '    Para configurar uma express'#227'o o operador pode adicionar camp' +
        'os, cujos nomes ser'#227'o substitu'#237'dos'
      
        ' pelos valores correspondes e adicionar algarismos num'#233'ricos e o' +
        'peradores matem'#225'ticos. Tamb'#233'm '#233
      
        ' poss'#237'vel criar  uma express'#227'o somente com operadores e algarism' +
        'os num'#233'ricos, o uso de'
      ' campos n'#227'o '#233' obrigat'#243'rio.'
      ''
      ' Exemplos:'
      
        ' Express'#227'o 1 :   Supondo que o sal'#225'rio dos conveniados da empres' +
        'a X teve um aumento de dez por cento,'
      
        ' ent'#227'o ter'#237'amos a express'#227'o:  '#8220'Sal'#225'rio'#8221' + ('#8220'Sal'#225'rio'#8221'  * 0.10).  ' +
        'Ou seja, o sal'#225'rio atual, mais dez'
      ' por cento dele mesmo.'
      ''
      
        ' Express'#227'o 2: Supondo que o limite m'#234's do conveniados '#233' de trint' +
        'a por cento do seu sal'#225'rio ent'#227'o ter'#237'amos'
      ' a express'#227'o: '#8220'Sal'#225'rio'#8221' * 0.30.'
      ''
      
        ' Obs: Para adicionar um campo a express'#227'o de dois cliques sobre ' +
        'o campo desejado na lista de campos.')
    ScrollBars = ssBoth
    TabOrder = 4
    Visible = False
    WordWrap = False
  end
end
