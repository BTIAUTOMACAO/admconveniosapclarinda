unit UCadProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, DBCtrls, DB, ToolEdit, CurrEdit, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ADODB; {JvDBCtrl;}

type
  TFCadProduto = class(TForm)
    BtnGrava: TBitBtn;
    BtnCancela: TBitBtn;
    DSProduto: TDataSource;
    DSPreco: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    dbEdtDesc: TDBEdit;
    Label6: TLabel;
    dbEdtPrecoVenda: TDBEdit;
    Label4: TLabel;
    dbEdtPrecoTabela: TDBEdit;
    Label17: TLabel;
    btnIncBarra: TSpeedButton;
    btnDelBarra: TSpeedButton;
    btnGravaBarra: TSpeedButton;
    btnCancelBarra: TSpeedButton;
    grdBarra: TJvDBGrid;
    DSBarras: TDataSource;
    QProduto: TADOQuery;
    QPreco: TADOQuery;
    QBarras: TADOQuery;
    QProdutoPROD_ID: TIntegerField;
    QProdutoFABR_ID: TIntegerField;
    QProdutoAPAGADO: TStringField;
    QProdutoCODINBS: TStringField;
    QProdutoDESCRICAO: TStringField;
    QProdutoSN: TStringField;
    QProdutoFLAGNOME: TStringField;
    QProdutoMT: TStringField;
    QProdutoDATA: TDateTimeField;
    QProdutoPRECO_FINAL: TBCDField;
    QProdutoPRECO_SEM_DESCONTO: TBCDField;
    QProdutoENVIADO_FARMACIA: TStringField;
    QProdutoDIGITADO_SITE: TStringField;
    QProdutoCODBARRAS: TStringField;
    QProdutoCOD_ABC: TIntegerField;
    QProdutoCOD_GUIA: TIntegerField;
    QProdutoLIBCIP: TStringField;
    QProdutoLABORA: TStringField;
    QProdutoNOME: TStringField;
    QProdutoAPRES: TStringField;
    QProdutoPRECO_VND: TBCDField;
    QProdutoPRECO_CUSTO: TBCDField;
    QProdutoPRINCIPIO: TStringField;
    QProdutoUNID_CAIXA: TIntegerField;
    QProdutoCLASSE_PRODUTO: TStringField;
    QProdutoIPI: TBCDField;
    QProdutoLISTA: TStringField;
    QProdutoDTAPAGADO: TDateTimeField;
    QProdutoDTALTERACAO: TDateTimeField;
    QProdutoOPERADOR: TStringField;
    QProdutoDTCADASTRO: TDateTimeField;
    QProdutoOPERCADASTRO: TStringField;
    QProdutoLAB_ID: TIntegerField;
    QProdutoFAM_ID: TIntegerField;
    QProdutoPAT_ID: TIntegerField;
    QProdutoCLAS_ID: TIntegerField;
    QProdutoSCLAS_ID: TIntegerField;
    QProdutoGRUPO_PROD_ID: TIntegerField;
    QProdutoREGMS: TStringField;
    QProdutoGENERICO: TStringField;
    QProdutoPORTARIA: TStringField;
    QProdutoVIGENCIA: TDateTimeField;
    QPrecoPROD_ID: TIntegerField;
    QPrecoDATA: TDateTimeField;
    QPrecoPRECO_FINAL: TBCDField;
    QPrecoPRECO_SEM_DESCONTO: TBCDField;
    QBarrasBARRAS_ID: TIntegerField;
    QBarrasBARRAS: TStringField;
    QBarrasCODINBS: TStringField;
    QBarrasPROD_ID: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure BtnGravaClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnCancelaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnIncBarraClick(Sender: TObject);
    procedure btnDelBarraClick(Sender: TObject);
    procedure btnGravaBarraClick(Sender: TObject);
    procedure btnCancelBarraClick(Sender: TObject);
    procedure DSBarrasStateChange(Sender: TObject);
    procedure qBarrasAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadProduto: TFCadProduto;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFCadProduto.FormCreate(Sender: TObject);
var prod_id: integer;
begin
  QProduto.Open;
  QPreco.Open;
  //comentado ariane prod_id:= DMConexao.getGeneratorValue('GEN_PRODUTO');

  // inserindo na tabela de produtos
  QProduto.Append;
  QProdutoPROD_ID.AsInteger:= prod_id;
  QProdutoCODINBS.AsString:= '9991000009999';
  QProdutoMT.AsString:= 'N';
  QProdutoSN.AsString:= '';
  QProdutoFLAGNOME.AsString:= '';
  QProdutoAPAGADO.AsString:= 'N';
  QProdutoDATA.AsString:= FormatDateTime('dd/MM/yyyy',Now);
  QProdutoPRECO_FINAL.AsCurrency:= 0;
  QProdutoPRECO_SEM_DESCONTO.AsCurrency:= 0;
  QProdutoENVIADO_FARMACIA.AsString:= 'N';
  QProdutoDIGITADO_SITE.AsString:= 'N';

  // inserindo na tabela de precos
  QPreco.Append;
  QPrecoPROD_ID.AsInteger:= prod_id;
  QPrecoDATA.AsString:= FormatDateTime('dd/MM/yyyy',Now);
  QPrecoPRECO_FINAL.AsCurrency:= 0;
  QPrecoPRECO_SEM_DESCONTO.AsCurrency:= 0;

  //Abrindo a tabela de c�digo de barras e setando par�metro
  QBarras.Parameters.ParamByName('PROD_ID').Value := qProduto.FieldByName('PROD_ID').Value;
  QBarras.Open;
end;

procedure TFCadProduto.BtnGravaClick(Sender: TObject);
begin
  if QProdutoDESCRICAO.AsString = '' then begin
     MsgErro('Favor inserir a descri��o do produto!');
     dbEdtDesc.SetFocus;
     Exit;
  end
  else if QPrecoPRECO_FINAL.AsString = '' then begin
     MsgErro('Favor inserir o pre�o de venda do produto!');
     dbEdtPrecoVenda.SetFocus;
     Exit;
  end
  else if QPrecoPRECO_SEM_DESCONTO.AsString = '' then begin
     MsgErro('Favor inserir o pre�o de tabela do produto!');
     dbEdtPrecoTabela.SetFocus;
     Exit;
  end;
  if MsgSimNao('Deseja gravar o produto?') then begin
     QPrecoPRECO_FINAL.AsCurrency:= QProdutoPRECO_FINAL.AsCurrency;
     QPrecoPRECO_SEM_DESCONTO.AsCurrency:= QProdutoPRECO_SEM_DESCONTO.AsCurrency;
     QProduto.Post;
     QPreco.Post;
     MsgInf('Registro salvo com sucesso!');
  end;
end;

procedure TFCadProduto.FormKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then begin
   Key := #0;
   Perform(WM_NEXTDLGCTL,0,0);
end;
end;

procedure TFCadProduto.BtnCancelaClick(Sender: TObject);
begin
  QProduto.Cancel;
  QPreco.Cancel;
  qBarras.Cancel;
end;

procedure TFCadProduto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QProduto.Close;
  QPreco.Close;
  QBarras.Close;
end;

procedure TFCadProduto.btnIncBarraClick(Sender: TObject);
begin
qBarras.Append;
grdBarra.Options:= [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
grdBarra.SetFocus;
end;

procedure TFCadProduto.btnDelBarraClick(Sender: TObject);
begin
  if (not qBarras.IsEmpty) and MsgSimNao('Confirma a exclus�o do codigo de barras: '+sLineBreak+qBarrasBARRAS.AsString+'?') then
    begin
    if qBarras.RecordCount = 1 then
      begin
      if not MsgSimNao('Este produto ficar� sem c�digo de barras associado a ele!'+sLineBreak+'Confirma essa opera��o?') then
        begin
        qBarras.Cancel;
        Exit;
        end;
      qProduto.Edit;
      qProduto.FieldByName('CODBARRAS').Clear;
      qProduto.Post;
      end;
    qBarras.Delete;
  end;
end;

procedure TFCadProduto.btnGravaBarraClick(Sender: TObject);
begin
  qBarras.Post;
  grdBarra.Options:= [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
end;

procedure TFCadProduto.btnCancelBarraClick(Sender: TObject);
begin
qBarras.Cancel;
end;

procedure TFCadProduto.DSBarrasStateChange(Sender: TObject);
begin
btnIncBarra.Enabled    := qBarras.State = dsBrowse;
btnDelBarra.Enabled    := (qBarras.State = dsBrowse) and (not(qBarras.IsEmpty));
btnCancelBarra.Enabled := qBarras.State in [dsEdit,dsInsert];
btnGravaBarra.Enabled  := qBarras.State in [dsEdit,dsInsert];
end;

procedure TFCadProduto.qBarrasAfterInsert(DataSet: TDataSet);
begin
DataSet.FieldByName('PROD_ID').Value := qProduto.fieldByName('PROD_ID').Value;
end;

end.
