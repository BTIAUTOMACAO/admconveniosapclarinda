inherited FFatura: TFFatura
  Left = 303
  Top = 144
  Caption = 'Faturas'
  ClientHeight = 575
  ClientWidth = 1008
  OldCreateOrder = True
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1008
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 1008
    Height = 82
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 1
    object rdgTipo: TRadioGroup
      Left = 97
      Top = 1
      Width = 96
      Height = 76
      Hint = 
        'Selecione se deseja listar apenas as faturas que est'#227'o em aberto' +
        ','#13#10'as faturas ja baixadas ou todas as faturas feitas'
      Align = alLeft
      Caption = 'Tipo...'
      ItemIndex = 0
      Items.Strings = (
        'Todas'
        'Empresas'
        'Conveniados'
        'D'#233'bito Conta')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      OnClick = rdgTipoClick
    end
    object GroupBox1: TGroupBox
      Left = 193
      Top = 1
      Width = 198
      Height = 76
      Align = alLeft
      Caption = 'Data de Vencimento'
      TabOrder = 2
      object Label2: TLabel
        Left = 11
        Top = 26
        Width = 164
        Height = 13
        Caption = 'Data Inicial                     Data Final'
      end
      object DataIni: TJvDateEdit
        Left = 10
        Top = 42
        Width = 87
        Height = 21
        Hint = 'Data inicial da fatura para pesquisa'
        DefaultToday = True
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 1
        OnExit = DataIniExit
      end
      object DataFim: TJvDateEdit
        Left = 102
        Top = 42
        Width = 88
        Height = 21
        Hint = 'Data final da fatura para pesquisa'
        DefaultToday = True
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 2
      end
      object ChkTodasDatas: TCheckBox
        Left = 110
        Top = 8
        Width = 81
        Height = 17
        Hint = 'consultar todas as datas'
        Caption = 'Todas datas'
        TabOrder = 0
        OnClick = ChkTodasDatasClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 391
      Top = 1
      Width = 288
      Height = 76
      Align = alLeft
      Caption = 'Selecionar a empresa'
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 26
        Width = 49
        Height = 13
        Caption = 'Empres ID'
      end
      object Label3: TLabel
        Left = 66
        Top = 26
        Width = 31
        Height = 13
        Caption = 'Nome '
      end
      object EdCod: TEdit
        Left = 9
        Top = 42
        Width = 50
        Height = 21
        Hint = 'Insira o c'#243'digo da empresa'
        TabOrder = 0
        OnChange = EdCodChange
        OnKeyPress = EdCodKeyPress
      end
      object DBEmpresa: TJvDBLookupCombo
        Left = 65
        Top = 42
        Width = 216
        Height = 21
        Hint = 'Consulte pelo nome da empresa'
        DropDownWidth = 350
        DisplayEmpty = 'Todas Empresas'
        EmptyValue = '0'
        FieldsDelimiter = #0
        LookupField = 'EMPRES_ID'
        LookupDisplay = 'NOME'
        LookupSource = DSEmpresa
        TabOrder = 1
        OnChange = DBEmpresaChange
      end
    end
    object Button1: TButton
      Left = 789
      Top = 30
      Width = 91
      Height = 28
      Hint = 'Buscar faturas'
      Caption = '&Abrir'
      TabOrder = 5
      OnClick = Button1Click
    end
    object RGFaturas: TRadioGroup
      Left = 1
      Top = 1
      Width = 96
      Height = 76
      Hint = 
        'Selecione se deseja listar apenas as faturas que est'#227'o em aberto' +
        ','#13#10'as faturas ja baixadas ou todas as faturas feitas'
      Align = alLeft
      Caption = 'Faturas...'
      ItemIndex = 0
      Items.Strings = (
        'Em Aberto'
        'Todas'
        'Baixadas')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = rdgTipoClick
    end
    object GroupBox3: TGroupBox
      Left = 679
      Top = 1
      Width = 92
      Height = 76
      Align = alLeft
      Caption = 'N'#186' da Fatura'
      TabOrder = 4
      object Label6: TLabel
        Left = 8
        Top = 26
        Width = 44
        Height = 13
        Caption = 'Fatura ID'
      end
      object edtFaturaID: TEdit
        Left = 8
        Top = 42
        Width = 76
        Height = 21
        TabOrder = 0
        OnKeyPress = edtFaturaIDKeyPress
      end
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 531
    Width = 1008
    Height = 44
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 4
    DesignSize = (
      1004
      40)
    object Bevel2: TBevel
      Left = 199
      Top = 6
      Width = 2
      Height = 27
    end
    object Label63: TLabel
      Left = 26
      Top = 15
      Width = 43
      Height = 13
      Caption = 'Baixadas'
    end
    object ButFechar: TButton
      Left = 81
      Top = 4
      Width = 111
      Height = 33
      Hint = 'Baixar fatura'
      Caption = 'Baixar Fatura(s)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = ButFecharClick
    end
    object ButImprimir: TBitBtn
      Left = 912
      Top = 4
      Width = 85
      Height = 33
      Hint = 'Op'#231#245'es de Impress'#227'o de fatura'
      Anchors = [akTop, akRight]
      Caption = '&Imprimir'
      TabOrder = 5
      OnClick = ButImprimirClick
      Glyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B0040000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC6CED2A8ADAFB0B1B2A8A6A6868585919394A69698987879A29A9AB4B5B5
        B2B3B4B2B7BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B8BAAAAB
        ACC2C2C2E7E6E6DADADAA0A1A19994945B4A494A4141606060858585AEAEADC2
        C1C1ACADAEB1B5B7FFFFFFFFFFFFFFFFFFBAC1C5A6A7A9B8B8B8E5E5E5F1F1F1
        EAEAEACECECE9999999696965858583939394545454F4F4F6E6E6E969595BAB9
        B9B7B7B7A1A2A4FFFFFFFFFFFFAEB0B1DBDBDAFAFAFAF3F3F3EDEDEDCDCDCDA2
        A2A27D7D7D8F8F8FA6A6A6A2A2A28A8A8A6F6F6F6D6D6D5C5C5C7171719A9B9B
        ACB2B4FFFFFFFFFFFFC3C3C2FFFFFFF2F2F2D0D0D09595959999999E9E9E7878
        787070707171718080809C9C9CAEAEAEA7A7A79090908F8B8DB1B0B2B7BEC1FF
        FFFFFFFFFFAFAFAED5D5D5939393959595C0C0C0C3C3C3C8C8C8BFBFBFA2A2A2
        9191918787877777776F6F6F828282A3A4A36AA27B7FA08BBBBDC5FFFFFFFFFF
        FF7F7F7E939393CDCDCDD7D7D7C7C7C7C1C1C1DADADAC5C5C5CDCDCDC9C9C9C2
        C2C2BDBDBDB5B5B59B9B9B7B7B7B757173848385B6BDC0FFFFFFFFFFFF979695
        F0F0F0D2D2D2C6C6C6C2C2C2DBDBDBBEBEBEC7C7C7C8C8C8B8B8B8B0B0B0BDBD
        BDBDBDBDC1C1C1CFCFCFC7C7C7A3A4A4B4BABEFFFFFFFFFFFFB3B7B8CBCBCBC6
        C6C6C3C3C3CCCCCCB8B8B8DDDDDDF5F5F5F2F2F2E9E9E9DFDFDFD4D4D4BFBFBF
        B1B1B1B1B1B1B1B0B0BBBDBEC1C9CDFFFFFFFFFFFFC4CCCFBDC2C5A5A7A8A8A8
        A8C3C4C4B5B7B8B0B1B1D1D1D1E0E0E0E1E1E1E6E6E6E9EAEAE9E9E9DFDFDFC0
        BFBF9D9E9EBBC1C5CBD3D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFFFE
        CEC7C1A9ADB0A1A5AA9EA1A4A6A8AAB6B7B9C2B6B6C1B7B7B3B4B4A7A9AABBC2
        C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1AFB0DEB094FED5A5F4
        CBA2EECAA7ECD2B7E3D3C2D6CBC1AB8D8DAAA5A7BEC6CAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8A8A8E3AC86FFD2A1FFCE9EFFCF
        9FFFD0A0FFD1A2F0C09BAD8D8DCAD5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB49A95FDD5ADFFD7B0FFD6B0FFD6B0FFD6B0
        FFDDB4C39A8DAF9495FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFB79188FFECD0FFE3C9FFE3C9FFE3C9FFE4CAFDE3C9B0
        8B89BEBCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB2A7AADABBAFFFEFDBFFEBD8FFEBD8FFEBD8FFF2DEDCBFB4AA8383FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA
        A2A3FFFFF8FFFFF8FFFFF8FFFFF8FFFFF9FFFFFEC4A5A1AB9394FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA89799B99797CBB0
        B0CAB0B0CAB0B0CAB0B0CAB1B0C9ADACB59999C2C2C6FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C1C5BDBABDBBB8BBBBB8BB
        BBB8BBBBB8BBBBB8BBBBB7BAC3C5C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
    end
    object Panel17: TPanel
      Left = 5
      Top = 14
      Width = 17
      Height = 14
      BorderStyle = bsSingle
      Color = clRed
      TabOrder = 6
    end
    object btnInsObs: TButton
      Left = 205
      Top = 4
      Width = 156
      Height = 33
      Hint = 'Inserir Observa'#231#227'o na Fatura'
      Caption = 'Inserir Observa'#231#227'o na Fatura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btnInsObsClick
    end
    object btnAlterarVenc: TButton
      Left = 365
      Top = 4
      Width = 204
      Height = 33
      Hint = 'Alterar a Data de Vencimento da Fatura'
      Caption = 'Alterar a Data de Vencimento da Fatura '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnAlterarVencClick
    end
    object btnRFatura: TButton
      Left = 573
      Top = 4
      Width = 86
      Height = 33
      Hint = 'Reabrir Fatura'
      Caption = 'Reabrir Fatura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btnRFaturaClick
    end
    object btnCanReab: TButton
      Left = 664
      Top = 4
      Width = 211
      Height = 33
      Hint = 'Cancelar Fatura e Reabrir Autoriza'#231#245'es'
      Caption = 'Cancelar Fatura e Reabrir Autoriza'#231#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btnCanReabClick
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 105
    Width = 1008
    Height = 397
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Faturas encontradas '
      object GridFatur: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1000
        Height = 369
        Align = alClient
        DataSource = DSFatura
        DefaultDrawing = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        PopupMenu = PopupMenu1
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GridFaturDrawColumnCell
        OnDblClick = GridFaturDblClick
        OnKeyDown = GridFaturKeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = GridFaturTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Title.Caption = 'Id'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Title.Caption = 'Nome'
            Width = 181
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'FATURA_ID'
            Title.Caption = 'N'#186' Fatura'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'FECHAMENTO'
            Title.Caption = 'Data Fecha.'
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_FATURA'
            Title.Caption = 'Data Fatura'
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'HORA_FATURA'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_VENCIMENTO'
            Title.Caption = 'Data Venc.'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Title.Caption = 'Valor Fatura'
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BAIXADA'
            Title.Caption = 'Baixada'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_PAGO'
            Title.Caption = 'Valor Pago '
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Title.Caption = 'Operador'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_BAIXA'
            Title.Caption = 'Data Baixa'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SO_CONFIRMADAS'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TIPO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESC_EMPRESA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRE_BAIXA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_PRE_BAIXA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BANCO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FORMA_PGTO'
            Title.Caption = 'Forma Pgto'#13#10
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 1
      OnShow = TabSheet2Show
      object PanelHistorico: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 65
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object LabelDataini: TLabel
          Left = 8
          Top = 14
          Width = 53
          Height = 13
          Caption = 'Data Inicial'
        end
        object LabelDataFinal: TLabel
          Left = 115
          Top = 14
          Width = 48
          Height = 13
          Caption = 'Data Final'
        end
        object Buthist: TButton
          Left = 232
          Top = 28
          Width = 79
          Height = 23
          Caption = '&Visualizar'
          TabOrder = 2
          OnClick = ButhistClick
        end
        object DBNavigatorHistorico: TDBNavigator
          Left = 325
          Top = 28
          Width = 124
          Height = 24
          DataSource = DSHistorico
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          Flat = True
          TabOrder = 3
        end
        object DataIni2: TJvDateEdit
          Left = 8
          Top = 28
          Width = 102
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 0
          OnExit = DataIniExit
        end
        object DataFim2: TJvDateEdit
          Left = 114
          Top = 28
          Width = 102
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 1
          OnExit = DataIniExit
        end
      end
      object GridHistorico: TJvDBGrid
        Left = 0
        Top = 65
        Width = 1000
        Height = 304
        Align = alClient
        DataSource = DSHistorico
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'DETALHE'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_HORA'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CADASTRO'
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAMPO'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_ANT'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_POS'
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERACAO'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MOTIVO'
            Visible = True
          end>
      end
    end
  end
  object Panel3: TPanel [4]
    Left = 0
    Top = 502
    Width = 1008
    Height = 29
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      1008
      29)
    object Label4: TLabel
      Left = 668
      Top = 11
      Width = 99
      Height = 16
      Caption = 'Valor Baixado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 426
      Top = 11
      Width = 99
      Height = 16
      Caption = 'Valor '#224' Baixar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Bevel3: TBevel
      Left = 767
      Top = 5
      Width = 2
      Height = 27
      Anchors = [akTop, akRight]
    end
    object ButMarcDesm: TButton
      Left = 3
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar ou desmarcar um fornecedor'
      Caption = 'Marca/Desm.(F11)'
      TabOrder = 0
      OnClick = ButMarcDesmClick
    end
    object ButMarcaTodos: TButton
      Left = 112
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar todos os fornecedores'
      Caption = 'Marca Todos (F8)'
      TabOrder = 1
      Visible = False
      OnClick = ButMarcaTodosClick
    end
    object ButDesmTodos: TButton
      Left = 220
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Desmarcar todos os fornecedores'
      Caption = 'Desm. Todos (F9)'
      TabOrder = 2
      Visible = False
      OnClick = ButDesmTodosClick
    end
    object EdBaixar: TJvValidateEdit
      Left = 527
      Top = 3
      Width = 124
      Height = 24
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
    end
    object EdBaixado: TJvValidateEdit
      Left = 767
      Top = 3
      Width = 124
      Height = 24
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 228
    Top = 248
  end
  object DSEmpresa: TDataSource
    DataSet = Empresa
    Left = 96
    Top = 360
  end
  object DSFatura: TDataSource
    DataSet = MFatura
    Left = 225
    Top = 360
  end
  object DSDadosEmp: TDataSource
    DataSet = QDadosEmp
    Left = 268
    Top = 361
  end
  object DSHistorico: TDataSource
    DataSet = QHistorico
    Left = 148
    Top = 361
  end
  object PopupMenu1: TPopupMenu
    Left = 236
    Top = 200
    object InserirObservao1: TMenuItem
      Caption = 'Inserir Observa'#231#227'o na Fatura'
      Visible = False
      OnClick = InserirObservao1Click
    end
    object AlteraraDatadeVencimento1: TMenuItem
      Caption = 'Alterar a Data de Vencimento da Fatura'
      Visible = False
      OnClick = AlteraraDatadeVencimento1Click
    end
    object ReabreFatura: TMenuItem
      Caption = 'Reabrir Fatura'
      Visible = False
      OnClick = ReabreFaturaClick
    end
    object CancelaFatura: TMenuItem
      Caption = 'Cancelar Fat. e Reabrir Auts.'
      Visible = False
      OnClick = CancelaFaturaClick
    end
    object AplicaPreBaixa: TMenuItem
      Caption = 'Aplicar Pr'#233'-Baixa'
      OnClick = AplicaPreBaixaClick
    end
    object CancelaPreBaixa: TMenuItem
      Caption = 'Cancelar Pr'#233'-Baixa'
      Enabled = False
      OnClick = CancelaPreBaixaClick
    end
    object ImprimeBoletoWeb: TMenuItem
      Caption = 'Imprimir Boleto da Fatura'
      Enabled = False
      OnClick = ImprimeBoletoWebClick
    end
  end
  object QDescEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'Select cc.cred_id, cc.data_fecha_emp, sum(cc.debito-cc.credito) ' +
        'valor, emp.desconto_emp,'
      
        'sum(cc.debito-cc.credito)*emp.desconto_emp/100 as desc_valor fro' +
        'm contacorrente cc'
      
        'join conveniados conv on conv.conv_id = cc.conv_id join empresas' +
        ' emp on conv.empres_id = emp.empres_id'
      
        'where coalesce(cc.baixa_conveniado,'#39'N'#39') <> '#39'S'#39' and coalesce(cc.f' +
        'atura_id,0) = 0 and cc.data_fecha_emp = '#39'10/10/2008'#39' and emp.emp' +
        'res_id = 1036'
      'group by cc.cred_id, cc.data_fecha_emp, emp.desconto_emp')
    Left = 52
    Top = 329
    object QDescEmpcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QDescEmpdata_fecha_emp: TDateTimeField
      FieldName = 'data_fecha_emp'
    end
    object QDescEmpvalor: TBCDField
      FieldName = 'valor'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QDescEmpdesconto_emp: TBCDField
      FieldName = 'desconto_emp'
      Precision = 6
      Size = 2
    end
    object QDescEmpdesc_valor: TBCDField
      FieldName = 'desc_valor'
      ReadOnly = True
      Precision = 32
      Size = 6
    end
  end
  object Empresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select empres_id, nome, desconto_emp from empresas  '
      'where apagado <> '#39'S'#39' '
      'order by nome')
    Left = 92
    Top = 329
    object Empresaempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object Empresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object Empresadesconto_emp: TBCDField
      FieldName = 'desconto_emp'
      Precision = 6
      Size = 2
    end
  end
  object QHistorico: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from logs')
    Left = 148
    Top = 329
    object QHistoricoLOG_ID: TIntegerField
      FieldName = 'LOG_ID'
    end
    object QHistoricoJANELA: TStringField
      FieldName = 'JANELA'
      Size = 30
    end
    object QHistoricoCAMPO: TStringField
      FieldName = 'CAMPO'
    end
    object QHistoricoVALOR_ANT: TStringField
      FieldName = 'VALOR_ANT'
      Size = 200
    end
    object QHistoricoVALOR_POS: TStringField
      FieldName = 'VALOR_POS'
      Size = 200
    end
    object QHistoricoOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QHistoricoOPERACAO: TStringField
      FieldName = 'OPERACAO'
      Size = 25
    end
    object QHistoricoDATA_HORA: TDateTimeField
      FieldName = 'DATA_HORA'
    end
    object QHistoricoCADASTRO: TStringField
      FieldName = 'CADASTRO'
      Size = 50
    end
    object QHistoricoID: TIntegerField
      FieldName = 'ID'
    end
    object QHistoricoDETALHE: TStringField
      FieldName = 'DETALHE'
      Size = 50
    end
    object QHistoricoMOTIVO: TStringField
      FieldName = 'MOTIVO'
      Size = 150
    end
    object QHistoricoSOLICITANTE: TStringField
      FieldName = 'SOLICITANTE'
      Size = 50
    end
  end
  object QDadosConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'fat_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select sum(cc.debito-cc.credito), '
      '0.0 as valor_rec, 0.0 as valor_sem, '
      'conv.conv_id, conv.chapa, conv.titular, cc.receita'
      'from conveniados conv'
      'join contacorrente cc on cc.conv_id = conv.conv_id'
      'where cc.fatura_id = :fat_id'
      'group by conv.conv_id, conv.chapa, conv.titular, cc.receita'
      'order by conv.titular')
    Left = 188
    Top = 329
    object QDadosConvCOLUMN1: TBCDField
      FieldName = 'COLUMN1'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QDadosConvvalor_rec: TBCDField
      FieldName = 'valor_rec'
      ReadOnly = True
      Precision = 1
      Size = 18
    end
    object QDadosConvvalor_sem: TBCDField
      FieldName = 'valor_sem'
      ReadOnly = True
      Precision = 1
      Size = 18
    end
    object QDadosConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QDadosConvchapa: TFloatField
      FieldName = 'chapa'
    end
    object QDadosConvtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QDadosConvreceita: TStringField
      FieldName = 'receita'
      FixedChar = True
      Size = 1
    end
  end
  object QFatura: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      'fat.id, '
      'fat.data_fatura, '
      'coalesce(fat.hora_fatura,'#39'00:00:00'#39') as hora_fatura,'
      'fat.data_vencimento, '
      'fat.so_confirmadas, '
      'fat.obs, '
      'desc_empresa,'
      'fat.fatura_id, '
      'fat.fechamento, '
      'fat.operador,'
      'fat.banco_id,'
      'fat.forma_id,'
      'fat.valor, '
      'fat.data_baixa, '
      'fat.apagado,'
      'fat.baixada, '
      'fat.pre_baixa, '
      'fat.data_pre_baixa, '
      'fat.valor_pago,'
      'banco.banco,'
      'FORMASPAGTO.DESCRICAO FORMA_PGTO,'
      'coalesce(fat.tipo,'#39'E'#39') as tipo, '
      'case when coalesce(fat.tipo,'#39'E'#39') = '#39'E'#39' then'
      
        'emp.nome else conv.titular end as nome from fatura fat left join' +
        ' empresas emp on'
      'coalesce(fat.tipo,'#39'E'#39') = '#39'E'#39' and emp.empres_id = fat.id'
      
        'left join conveniados conv on  fat.tipo = '#39'C'#39' and conv.conv_id =' +
        ' fat.id'
      'INNER JOIN BANCOS banco ON fat.BANCO_ID = banco.CODIGO '
      
        'INNER JOIN FORMAPAGTO_FATURAS FORMASPAGTO ON FORMASPAGTO.FORMA_I' +
        'D = FAT.FORMA_ID')
    Left = 228
    Top = 329
    object QFaturaid: TIntegerField
      FieldName = 'id'
    end
    object QFaturadata_fatura: TDateTimeField
      FieldName = 'data_fatura'
    end
    object QFaturahora_fatura: TStringField
      FieldName = 'hora_fatura'
      ReadOnly = True
      Size = 10
    end
    object QFaturadata_vencimento: TDateTimeField
      FieldName = 'data_vencimento'
    end
    object QFaturaso_confirmadas: TStringField
      FieldName = 'so_confirmadas'
      FixedChar = True
      Size = 1
    end
    object QFaturaobs: TStringField
      FieldName = 'obs'
      Size = 60
    end
    object QFaturadesc_empresa: TFloatField
      FieldName = 'desc_empresa'
    end
    object QFaturafatura_id: TIntegerField
      FieldName = 'fatura_id'
    end
    object QFaturafechamento: TDateTimeField
      FieldName = 'fechamento'
    end
    object QFaturaoperador: TStringField
      FieldName = 'operador'
    end
    object QFaturabanco_id: TIntegerField
      FieldName = 'banco_id'
    end
    object QFaturaforma_id: TIntegerField
      FieldName = 'forma_id'
    end
    object QFaturavalor: TFloatField
      FieldName = 'valor'
    end
    object QFaturadata_baixa: TDateTimeField
      FieldName = 'data_baixa'
    end
    object QFaturaapagado: TStringField
      FieldName = 'apagado'
      FixedChar = True
      Size = 1
    end
    object QFaturabaixada: TStringField
      FieldName = 'baixada'
      FixedChar = True
      Size = 1
    end
    object QFaturapre_baixa: TStringField
      FieldName = 'pre_baixa'
      FixedChar = True
      Size = 1
    end
    object QFaturadata_pre_baixa: TDateTimeField
      FieldName = 'data_pre_baixa'
    end
    object QFaturatipo: TStringField
      FieldName = 'tipo'
      ReadOnly = True
      Size = 1
    end
    object QFaturanome: TStringField
      FieldName = 'nome'
      ReadOnly = True
      Size = 60
    end
    object QFaturabanco: TStringField
      FieldName = 'banco'
      Size = 45
    end
    object QFaturaFORMA_PGTO: TStringField
      FieldName = 'FORMA_PGTO'
      Size = 50
    end
    object QFaturavalor_pago: TBCDField
      FieldName = 'valor_pago'
      DisplayFormat = '#,##0.00'
      Precision = 16
      Size = 2
    end
  end
  object QDadosEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 268
    Top = 329
  end
  object MFatura: TJvMemoryData
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'DATA_FATURA'
        DataType = ftDateTime
      end
      item
        Name = 'HORA_FATURA'
        DataType = ftTime
      end
      item
        Name = 'DATA_VENCIMENTO'
        DataType = ftDateTime
      end
      item
        Name = 'SO_CONFIRMADAS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OBS'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FATURA_ID'
        DataType = ftInteger
      end
      item
        Name = 'FECHAMENTO'
        DataType = ftDateTime
      end
      item
        Name = 'OPERADOR'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'VALOR'
        DataType = ftFloat
      end
      item
        Name = 'DATA_BAIXA'
        DataType = ftDateTime
      end
      item
        Name = 'APAGADO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BAIXADA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRE_BAIXA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DATA_PRE_BAIXA'
        DataType = ftDateTime
      end
      item
        Name = 'TIPO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DESC_EMPRESA'
        DataType = ftFloat
      end
      item
        Name = 'MARCADO'
        DataType = ftBoolean
      end
      item
        Name = 'BANCO_ID'
        DataType = ftInteger
      end
      item
        Name = 'FORMAPGTO_ID'
        DataType = ftInteger
      end
      item
        Name = 'BANCO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FORMA_PGTO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'VALOR_PAGO'
        DataType = ftString
        Size = 20
      end>
    AfterScroll = MFaturaAfterScroll
    Left = 228
    Top = 393
    object MFaturaID: TIntegerField
      FieldName = 'ID'
    end
    object MFaturaDATA_FATURA: TDateTimeField
      FieldName = 'DATA_FATURA'
    end
    object MFaturaHORA_FATURA: TTimeField
      DisplayLabel = 'Hora Fatura'
      FieldName = 'HORA_FATURA'
    end
    object MFaturaDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
    end
    object MFaturaSO_CONFIRMADAS: TStringField
      DisplayLabel = 'S'#243' Confirmadas'
      FieldName = 'SO_CONFIRMADAS'
      Size = 1
    end
    object MFaturaOBS: TStringField
      DisplayLabel = 'OBS.'
      FieldName = 'OBS'
      Size = 50
    end
    object MFaturaFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object MFaturaFECHAMENTO: TDateTimeField
      FieldName = 'FECHAMENTO'
    end
    object MFaturaOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 30
    end
    object MFaturaVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object MFaturaDATA_BAIXA: TDateTimeField
      FieldName = 'DATA_BAIXA'
    end
    object MFaturaAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object MFaturaBAIXADA: TStringField
      FieldName = 'BAIXADA'
      Size = 1
    end
    object MFaturaPRE_BAIXA: TStringField
      DisplayLabel = 'Pr'#233'-Baixa'
      FieldName = 'PRE_BAIXA'
      Size = 1
    end
    object MFaturaDATA_PRE_BAIXA: TDateTimeField
      DisplayLabel = 'Data Pr'#233'-Baixa'
      FieldName = 'DATA_PRE_BAIXA'
    end
    object MFaturaTIPO: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'TIPO'
      Size = 1
    end
    object MFaturaNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object MFaturaDESC_EMPRESA: TFloatField
      DisplayLabel = 'Desc. Empresa'
      FieldName = 'DESC_EMPRESA'
    end
    object MFaturaMARCADO: TBooleanField
      FieldName = 'MARCADO'
    end
    object MFATURABANCO_ID: TIntegerField
      FieldName = 'BANCO_ID'
    end
    object MFaturaFORMAPGTO_ID: TIntegerField
      FieldName = 'FORMAPGTO_ID'
    end
    object MFaturaBANCO: TStringField
      FieldName = 'BANCO'
      Size = 50
    end
    object MFaturaFORMA_PGTO: TStringField
      FieldName = 'FORMA_PGTO'
      Size = 50
    end
    object MFaturaVALOR_PAGO: TStringField
      FieldName = 'VALOR_PAGO'
    end
  end
end
