unit UAtualizarConsumoMes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, {JvLookup,} StdCtrls, Grids,
  DBGrids, {JvDBCtrl,} DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  XMLDoc, JvExDBGrids, JvDBGrid, JvExControls, JvDBLookup, Mask, JvExMask,
  JvToolEdit, wsconvenio_new, XSBuiltIns, InvokeRegistry, Rio,
  SOAPHTTPClient, ShellApi, DBClient, DBTables, JvBDEMemTable, clipbrd,
  ZSqlUpdate, ADODB, saleService2, JvMemoryDataset, ComCtrls;

type
  TFAtualizarConsumoMes = class(TF1)
    Panel1: TPanel;
    btnAtualizar: TBitBtn;
    dsListEmp: TDataSource;
    cbbEmp: TJvDBLookupCombo;
    edtEmp: TEdit;
    Label2: TLabel;
    lblStatus: TLabel;
    qListEmp: TADOQuery;
    qListEmpempres_id: TAutoIncField;
    qListEmpnome: TStringField;
    lblTotal: TLabel;
    lblRegistros: TLabel;
    GroupBox1: TGroupBox;
    QBusca: TADOQuery;
    pb: TProgressBar;
    QBuscaEMPRES_ID: TIntegerField;
    QBuscaBAND_ID: TIntegerField;
    QBuscaQTD_LIMITES: TIntegerField;
    QBuscaDATA_FECHA: TDateTimeField;
    procedure btnConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbbEmpChange(Sender: TObject);
    procedure edtEmpChange(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure Valida;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAtualizarConsumoMes: TFAtualizarConsumoMes;

implementation

uses DM, cartao_util, UMenu, Math, URotinasTexto;

{$R *.dfm}

procedure TFAtualizarConsumoMes.edtEmpChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEmp.Text) <> '' then begin
    if qListEmp.Locate('empres_id',edtEmp.Text,[]) then
      cbbEmp.KeyValue := edtEmp.Text
    else
      cbbEmp.ClearValue;
  end else
    cbbEmp.ClearValue;
end;


procedure TFAtualizarConsumoMes.cbbEmpChange(Sender: TObject);
begin
  inherited;
  if edtEmp.Text <> cbbEmp.KeyValue then
    edtEmp.Text := string(cbbEmp.KeyValue);
end;

procedure TFAtualizarConsumoMes.Valida;
begin
  if cbbEmp.KeyValue = 0 then
  begin
      msginf('Necess�rio informar a Empresa');
      Abort;
      edtEmp.SetFocus;
  end;
end;

procedure TFAtualizarConsumoMes.btnConsultarClick(Sender: TObject);
var dtIni, dtFin : String;
begin
  inherited;
  screen.Cursor := crHourGlass;

  screen.Cursor := crDefault;
end;

procedure TFAtualizarConsumoMes.FormCreate(Sender: TObject);
begin
  inherited;
  qListEmp.Open;
end;

procedure TFAtualizarConsumoMes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qListEmp.Close;
end;

procedure TFAtualizarConsumoMes.btnAtualizarClick(Sender: TObject);
var strSql : String;
begin
  inherited;
  Valida;
  
  if Application.MessageBox('Confirma a atualiza��o?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin

    QBusca.Close;
    QBusca.SQL.Clear;
    QBusca.SQL.Add(' SELECT TOP 1 E.EMPRES_ID, E.BAND_ID, B.QTD_LIMITES, D.DATA_FECHA ');
    QBusca.SQL.Add(' FROM EMPRESAS E');
    QBusca.SQL.Add(' LEFT JOIN BANDEIRAS B ON B.BAND_ID = E.BAND_ID');
    QBusca.SQL.Add(' INNER JOIN DIA_FECHA D ON  D.EMPRES_ID = E.EMPRES_ID AND D.DATA_FECHA > CONVERT(DATE,CURRENT_TIMESTAMP)');
    QBusca.SQL.Add(' WHERE E.EMPRES_ID = ' + cbbEmp.KeyValue);

    QBusca.Open;
    if QBusca.IsEmpty then begin
      msgInf('N�o foi encontrado nenhum registro.');
      cbbEmp.KeyValue := 0;
      edtEmp.Text := '0';
      pb.Visible := false;
      edtEmp.SetFocus;
      exit;
    end;

    QBusca.First;
    pb.Visible := true;
    pb.Max := QBusca.RecordCount + 1;
    pb.Position := 0;
    pb.Step := 1;
    Screen.Cursor := crHourGlass;
    while not QBusca.Eof do begin
      try
        DMConexao.AdoCon.BeginTrans;
        DMConexao.ExecuteSql('EXEC CONSUMO_MES_EMP ' + QBuscaEMPRES_ID.AsString + ',' + QBuscaBAND_ID.AsString + ',''' + QBuscaDATA_FECHA.AsString + ''',' + QBuscaQTD_LIMITES.AsString);
        DMConexao.AdoCon.CommitTrans;
      except
        on e:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
         end;
      end;
      pb.StepIt;
      QBusca.Next;
    end;

    Screen.Cursor := crDefault;
    msgInf('Atualiza��o realizada com sucesso!');

    cbbEmp.KeyValue := 0;
    edtEmp.Text := '0';
    pb.Visible := false;
    edtEmp.SetFocus;
  end;

end;




procedure TFAtualizarConsumoMes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f5 : btnAtualizar.Click;
  end;
end;

end.
