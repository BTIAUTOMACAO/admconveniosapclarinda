unit uExportConciliacaoSpani;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, DB, ZAbstractRODataset,
  ZDataset, Math, Mask, ToolEdit, DBCtrls, ClipBrd, ZAbstractDataset,
  frxClass, frxDBSet, JvExMask, JvToolEdit, ADODB, frxExportPDF,
  JvExControls, JvDBLookup;

type
  TFrmExportConciliacaoSpani = class(TForm)
    ProgressBar1: TProgressBar;
    sd: TSaveDialog;
    QConfigCadastramento: TADOQuery;
    Panel1: TPanel;
    lblStatus: TLabel;
    Label1: TLabel;
    Bevel1: TBevel;
    Label3: TLabel;
    Label5: TLabel;
    lbl1: TLabel;
    btnExportar: TButton;
    edtCaminho: TJvDirectoryEdit;
    JvDateEdit1: TJvDateEdit;
    JvDateEdit2: TJvDateEdit;
    cbbEmpresa: TComboBox;
    procedure btnExportarClick(Sender: TObject);
    private
    Cancelar : Boolean;
    procedure LimparCampos;
    procedure SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
    //Layout das Empresas
    procedure LayoultConciliacaoSpani(var d: TDateTime);
    procedure LayoultConciliacaoSpaniLayoutNovo(var d: TDateTime);
    procedure LayoultConciliacaoMangueira(var d: TDateTime);
    procedure LayoultConciliacaoFarmaPonte(var d: TDateTime);
    procedure LayoultConciliacaoVNUNES(var d: TDateTime);
    //procedure LayoutSPANI_UNIFICADO();
    { Private declarations }
    function MascaraCartao (var cartao: String): string;
    function RetiraCaracteresEspeciais (var valor: String): string;
    function ConverteDiaParaAmericano (var dia: TDateTime): String;
    function FormataDinheiro (var valor: String): String;
  public
    contador,totalRegistros : Integer;
    totalizador : Double;
    ext, nomeArqTxt, orderBy : string;
    head, body, footer : string;
    SList : TStringList;
    //function verificaDepartamento() : Boolean;
    { Public declarations }
  end;

var
  FrmExportConciliacaoSpani: TFrmExportConciliacaoSpani;
  var flag : Boolean = False; // A flag est� sendo utilizada para verificar se achou ou n�o departamentos
  caminhoArquivo : string;

implementation

uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;

{$R *.dfm}

//Fun��o que converte um formato data em string no formato YYYYMMDD
function TFrmExportConciliacaoSpani.ConverteDiaParaAmericano (var dia: TDateTime): String;
var myDay, myMonth, myYear : Word;
    diam,mes,ano: string;

begin
    DecodeDate(dia, myYear, myMonth, myDay);
    if myMonth < 10 then begin
      mes := '0'+IntToStr(myMonth);
    end else begin
      mes :=  IntToStr(myMonth);
    end;
    if myDay < 10 then begin
      diam := '0'+ IntToStr(myDay);;
    end else begin
      diam := IntToStr(myDay);
    end;

    ano := IntToStr(myYear);
    result := ano+mes+diam ;


end;

//Function que retira caracteres que n�o sejam n�meros de cart�es, cnpj
function TFrmExportConciliacaoSpani.RetiraCaracteresEspeciais (var valor: String): String;
var aux: String;
    count: Integer;
begin
  aux := valor[1];
  for count:=2 to Length(valor) do begin
    if (Pos(valor[count],'0123456789') <> 0) then begin
       aux := aux + valor[count];
    end;
  end;
  Result := aux;
end;

function TFrmExportConciliacaoSpani.FormataDinheiro (var valor: String): String;
var aux: String;
    count: Integer;
begin
  aux := '';
  for count:=0 to Length(valor) do begin
    if (Pos(valor[count],'0123456789') <> 0) then begin
       aux := aux + valor[count];
    end;
  end;
  Result := aux;
end;

//C�digo mascarando cart�o
function TFrmExportConciliacaoSpani.MascaraCartao (var cartao: String): string;
var count: Integer;
begin
  if (Length(cartao)>=16) then begin
       for count:=7 to Length(cartao)-4 do begin
         cartao[count] := 'X';
       end;

  end else if  (Length(cartao)>=13) then begin
      for count:=5 to Length(cartao)-4 do begin
         cartao[count] := 'X';
       end;
  end;
  Result := cartao;
end;

procedure TFrmExportConciliacaoSpani.btnExportarClick(Sender: TObject);
var S : String;
    i : Integer;
    dias: Double;
    SL : TStrings;
    dia : TDateTime;
    Erro : Boolean;
begin

  if not DirectoryExists(ExtractFilePath(edtCaminho.Text)) then begin
    MsgInf('Caminho n�o encontrado!');
    edtCaminho.SetFocus;
    Abort;
  end;
  if  ((not IsDate(JvDateEdit1.Text)) or (not IsDate(JvDateEdit2.Text)))  then begin
    MsgInf('Data inv�lida');
    JvDateEdit1.SetFocus;
    Abort;
  end;

  if UpperCase(btnExportar.Caption) = '&CANCELAR' then begin
    Cancelar := True;
    Abort;
  end;

  lblStatus.Caption := '';
  dias := JvDateEdit2.Date - JvDateEdit1.Date;
  dia := JvDateEdit1.Date;

  if cbbEmpresa.Text = 'Farma Ponte' then begin
    for i := 0 to Round(dias) do begin
      LayoultConciliacaoFarmaPonte(dia);
      dia :=  dia+1;
    end;
  end else if cbbEmpresa.Text = 'Spani' then begin
    for i := 0 to Round(dias) do begin
      LayoultConciliacaoSpani(dia);
      dia :=  dia+1;
    end;
  end else if cbbEmpresa.Text = 'Spani Layout Novo' then begin
    for i := 0 to Round(dias) do begin
      LayoultConciliacaoSpaniLayoutNovo(dia);
      dia :=  dia+1;
    end;
  end else if cbbEmpresa.Text = 'V Nunes' then begin
    for i := 0 to Round(dias) do begin
      LayoultConciliacaoVNUNES(dia);
      dia :=  dia+1;
    end;
  end else if cbbEmpresa.Text = 'Auto Posto Mangueira' then begin
    for i := 0 to Round(dias) do begin
      LayoultConciliacaoMangueira(dia);
      dia :=  dia+1;
    end;

  end;

  LimparCampos;
  lblStatus.Caption := 'Arquivos gerados...';
  MsgInf('Arquivos gerados com sucesso');


end;


procedure TFrmExportConciliacaoSpani.SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
begin
  if termino = false then
    SList.Add(Linha);

  if Termino = true then
  begin
    if SList.Count > 0 then begin

      if UpperCase(ExtractFileExt(edtCaminho.Text)) <>  Formato then begin
        caminhoArquivo := edtCaminho.Text + '\' + NomeArq + Formato;
        SList.SaveToFile(caminhoArquivo);
        //AbrirArquivo(ExtractFilePath(caminhoArquivo),ExtractFileName(caminhoArquivo),fmOpenRead);
      end else begin
        SList.SaveToFile(edtCaminho.Text);
        AbrirArquivo(ExtractFilePath(edtCaminho.Text),ExtractFileName(edtCaminho.Text),fmOpenRead);
      end;
    end;
  end;
end;

procedure TFrmExportConciliacaoSpani.LimparCampos();
begin
  edtCaminho.Text := '';
  JvDateEdit1.Text := '';
  JvDateEdit2.Text := '';
  cbbEmpresa.Text := 'Escolha um Modelo';
end;

procedure TFrmExportConciliacaoSpani.LayoultConciliacaoFarmaPonte(var d: TDateTime);
var
  Erro,hasDepartamento : Boolean;
  parcelas,count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  aux,cartao,mesvalor, S, valor,dec,endereco,valorTotal,MOVIMENTO_ID, nomeAdministradora, teste : String;
  ano,mes,dia : Integer;
  myHour, myMin, mySec, myMilli : Word;
  prefixo_nome_txt,nomeArquivo : String;
  dataIncial : TDateTime;
  data : TDate;
  valorFinal,valorLiquido, valorDesconto, valorEmpresa, valorRepasse, valorRepasseEmpresa : Double;
  dinheiroaux :Currency;
begin
  valorFinal := 0;
  valorEmpresa := 0;
  valorRepasse := 0;
  valorRepasseEmpresa :=0;
  ProgressBar1.Position := 0;
  lblStatus.Caption :=  'Gerando arquivo...';
  lblStatus.Refresh;
  Erro := False;
  contadorDeLinhasDoArquivo := 0;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  //hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMOV_ID_SITEF AS MOVIMENTO_ID');
  DMConexao.AdoQry.Open;
  MOVIMENTO_ID := DMConexao.AdoQry.Fields[0].Value;


  //PREFIXO FIXO PARA GRAVAR O NOME DO ARQUIVO
  prefixo_nome_txt := 'rpccrt';
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  nomeArquivo := prefixo_nome_txt + aux + fnCompletarCom(MOVIMENTO_ID,6,'0');

  //Cabe�alho
  //variaveis fixas do cabecalho
  nomeAdministradora := 'PLANTAO CARD';

  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom('A0',2);//A0 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom('0001.1',6);//VVV.RR "0016d"
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  DecodeTime(Now,myHour, myMin, mySec, myMilli);
  S := S+ fnCompletarCom(IntToStr(myHour),02);
  S := S+ fnCompletarCom(IntToStr(myMin),02);
  S := S+ fnCompletarCom(IntToStr(mySec),02); // A04 - gera��o do arquivo
  S := S+ fnCompletarCom(MOVIMENTO_ID,6,'0');  //A05 ID DO MOVIMENTO
  S := S+ fnCompletarCom(nomeAdministradora,30,' ',True);
  S := S+ fnCompletarCom('1',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho


  //Segunda linha - Header do Lote de transa��es
  S := '';
  S := S+ fnCompletarCom('L0',2);//L0 - CODIGO DE REGISTRO =L0
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  S := S+ fnCompletarCom('RE',2);
  S := S+ fnCompletarCom('2',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim da Segunda Linha

  data :=   d;


  //Come�o da Terceira Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - PREVIS�O
  //VENDAS OCORRIDAS NO DIA
  //COMENT�RIOS CV LAYOUT PLANT�O CARD 1.1

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //FARMA PONTE
  DMConexao.AdoQry.SQL.Add('SELECT CC.NSU, DATA, FORMAPAGTO_ID, CR.CGC, COMISSAO, DEBITO AS VALOR, ');
  DMConexao.AdoQry.SQL.Add('CC.CARTAO_ID, HISTORICO, DEBITO, AUTORIZACAO_ID, AGENCIA, CONTACORRENTE, ');
  DMConexao.AdoQry.SQL.Add('CC.CRED_ID, CC.EMPRES_ID, CA.CODCARTIMP, MOD_CART_ID, COD_BANCO ');
  DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN CREDENCIADOS CR ON CC.CRED_ID = CR.CRED_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CA.CARTAO_ID = CC.CARTAO_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
  DMConexao.AdoQry.SQL.Add('WHERE CC.DATA BETWEEN '+QuotedStr(DateToStr(data))+' AND '+QuotedStr(DateToStr(data)+' 23:59:59')+' ');
  DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE'+QuotedStr('%FARMA PONTE%')+' AND CC.DEBITO > 0');


  DMConexao.AdoQry.Open;
  while not DMConexao.AdoQry.Eof do begin
    if not (DMConexao.AdoQry.IsEmpty)  then begin
      S := '';
      S := S+ fnCompletarCom('CV',2);
      //CV02 - CNPJ in�cio
      aux := DMConexao.AdoQry.Fields[3].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
      S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV03 - NSU
      //In�cio CV04  - data da venda
      dataIncial :=  DMConexao.AdoQry.Fields[1].Value;
      aux := ConverteDiaParaAmericano(dataIncial);
      S := S+ fnCompletarCom(aux,08);//CV04
      DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli);
      S := S+ fnCompletarCom(IntToStr(myHour),02);
      S := S+ fnCompletarCom(IntToStr(myMin),02);
      S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
      //In�cio CV06  - Data de lan�amento ap�s 35 dias
      dataIncial := DMConexao.AdoQry.Fields[1].Value;
      if DayOfTheWeek(dataIncial) = 7 then begin
        dataIncial := dataIncial + 37;
      end else begin
        dataIncial := dataIncial + (7- DayOfTheWeek(dataIncial)) + 37;
      end;
      aux := ConverteDiaParaAmericano(dataIncial);
      S := S + fnCompletarCom(aux,8);//CV06 FIM
      S := S + '0';//CV 07 - TIPO DE LAN�AMENTO
      S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
      //In�cio CV09  - valor bruto
      valorFinal := valorFinal+ DMConexao.AdoQry.Fields[8].Value;
      dinheiroaux := DMConexao.AdoQry.Fields[8].Value;
      aux := Format('%m',[dinheiroaux]);

      aux := FormataDinheiro(aux);
      S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
      //CV10 In�cio  - valor da comiss�o em R$
      if (DMConexao.AdoQry.Fields[2].value = 1) then begin
        valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        valorEmpresa := valorEmpresa + valorDesconto;
        dinheiroaux := valorDesconto;
        aux := Format('%m',[dinheiroaux]);
        aux := FormataDinheiro(aux);
        S := S+ fnCompletarCom(aux,11,'0');
      end else begin
        valorDesconto := 0;
        S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
      end;
      //CV11 In�cio  - Valor l�quido
      valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
      valorLiquido := SimpleRoundTo(valorLiquido,-2);
      dinheiroaux := valorLiquido;
      aux := Format('%m',[dinheiroaux]);
      aux := FormataDinheiro(aux);
      S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
      //CV12 In�cio - N�MERO DO CART�O
      cartao := DMConexao.AdoQry.Fields[14].Value;
      cartao := MascaraCartao(cartao);
      S := S+ fnCompletarCom(cartao,19, '0');
      //CV12 Fim
      //CV13 e CV14 In�cio
      parcelas :=   DMConexao.AdoQry.Fields[2].value;
      if (parcelas = 1) then begin
       S := S+ fnCompletarCom('0',4,'0');
      end else begin
        aux := DMConexao.AdoQry.Fields[7].Value;
        S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
        S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
      end;//CV 14 e CV 15 Fim
      S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV15 - NSU PARCELA
      if (parcelas = 1) then begin
       S := S+ fnCompletarCom('0',33);
      end else begin
        //CV16 - VALOR BRUTO PARCELA in�cio
        aux := DMConexao.AdoQry.Fields[8].Value;
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
        //CV17 - VALOR DESCONTO PARCELA In�cio
        valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        aux := FloatToStr(valorDesconto);
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
        //CV18 - VALOR L�QUIDO PARCELA In�cio
        valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
        valorLiquido := SimpleRoundTo(valorLiquido,-2);
        aux := FloatToStr(valorLiquido);
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
      end;
      //CV 19 - Adicionar Produto
      if (DMConexao.AdoQry.Fields[15].Value = '7') then begin
          S := S+ fnCompletarCom('2',3,'0');
      end else begin
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[15].Value,3,'0');
      end;
      //CV19 FIM
      S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[16].Value),3,'0');//CV20 C�DIGO BANCO
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[10].Value,6,'0');//CV21 AG�NCIA
      aux:=   DMConexao.AdoQry.Fields[11].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,11,' ',True);//CV22 CONTA
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[9].Value,12,'0');//CV23 AUTORIZA��O
      contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
      S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//CV24 - NSEQ
      aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
      SalvarArquivo(S,nomeArquivo,false,'.txt');
      DMConexao.AdoQry.Next;
    end;
  end;
  //Fim da Terceira Linha

  //Come�o da Quarta Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - LIQUIDA��O
  //CONFIRMA��O DE REPASSE DAS TRANSA��ES
  //COMENT�RIOS CV COM BASE NO MODELO 1.1 PLANT�O CARD

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT CC.AUTORIZACAO_ID,CC.NSU, CC.DEBITO, CC.DATA, CC.FORMAPAGTO_ID, CA.CODCARTIMP, ');
  DMConexao.AdoQry.SQL.Add('CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, CR.COMISSAO,  ');
  DMConexao.AdoQry.SQL.Add('PC.DATA_COMPENSACAO,CC.HISTORICO, BA.COD_BANCO, EM.MOD_CART_ID ');
  DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN PAGAMENTO_CRED PC ON CC.PAGAMENTO_CRED_ID = PC.PAGAMENTO_CRED_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON CR.CRED_ID = CC.CRED_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CC.CARTAO_ID = CA.CARTAO_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
  DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
  DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%FARMA PONTE%') + '  ');
  DMConexao.AdoQry.SQL.Add('AND CC.DEBITO > 0 ');
  DMConexao.AdoQry.SQL.Add('AND CC.VALOR_CANCELADO <= 0 ');
  DMConexao.AdoQry.Open;


  while not DMConexao.AdoQry.Eof do begin
    if not (DMConexao.AdoQry.IsEmpty)  then begin
      S := '';
      S := S+ fnCompletarCom('CV',2);
      //CV02 - Cnpj in�cio
      aux := DMConexao.AdoQry.Fields[6].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
      S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU
      //In�cio CV04  - data da venda
      dataIncial :=  DMConexao.AdoQry.Fields[3].Value;
      aux := ConverteDiaParaAmericano(dataIncial);
      S := S+ fnCompletarCom(aux,08);//CV04 FIM
      DecodeTime(DMConexao.AdoQry.Fields[3].Value, myHour, myMin, mySec, myMilli);
      S := S+ fnCompletarCom(IntToStr(myHour),02);
      S := S+ fnCompletarCom(IntToStr(myMin),02);
      S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
      //In�cio CV06  - Data de lan�amento liquida��o imediata
      dataIncial := DMConexao.AdoQry.Fields[10].Value;
      aux := ConverteDiaParaAmericano(dataIncial);
      S := S + fnCompletarCom(aux,8);//CV06 FIM
      S := S + '1';//CV 07 - TIPO DE LAN�AMENTO
      S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
      //In�cio CV09  - valor bruto
      valorRepasse := valorRepasse + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
      dinheiroaux := DMConexao.AdoQry.Fields[2].Value;
      aux := Format('%m',[dinheiroaux]);
      aux := FormataDinheiro(aux);
      S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
      //CV10 In�cio  - valor da comiss�o em R$
      if (DMConexao.AdoQry.Fields[4].value = 1) then begin
        valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        valorRepasseEmpresa := valorRepasseEmpresa + valorDesconto;
        dinheiroaux := valorDesconto;
        aux := Format('%m',[dinheiroaux]);
        aux := FormataDinheiro(aux);
        S := S+ fnCompletarCom(aux,11,'0');
      end else begin
        valorDesconto := 0;
        S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
      end;
      //CV11 In�cio  - Valor l�quido
      valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
      valorLiquido := SimpleRoundTo(valorLiquido,-2);

      dinheiroaux := valorLiquido;
      aux := Format('%m',[dinheiroaux]);
      aux := FormataDinheiro(aux);
      S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
      //CV12 In�cio - N�MERO DO CART�O
      cartao := DMConexao.AdoQry.Fields[5].Value;
      cartao := MascaraCartao(cartao);
      S := S+ fnCompletarCom(cartao,19, '0');
      //CV12 Fim
      //CV13 e CV14 In�cio
      parcelas :=   DMConexao.AdoQry.Fields[4].value;
      if (parcelas = 1) then begin
       S := S+ fnCompletarCom('0',4,'0');
      end else begin
        aux := DMConexao.AdoQry.Fields[11].Value;
        S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
        S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
      end;//CV 13 e CV 14 Fim
      S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV15 NSU PARCELA
      if (parcelas = 1) then begin
       S := S+ fnCompletarCom('0',33);
      end else begin
        //CV16 in�cio VALOR BRUTO PARCELA
        aux := DMConexao.AdoQry.Fields[2].Value;
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
        //CV17 In�cio  - VALOR DE DESCONTO DA PARCELA
        valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        aux := FloatToStr(valorDesconto);
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
        //CV18 In�cio - VALOR L�QUIDO DA PARCELA
        valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
        valorLiquido := SimpleRoundTo(valorLiquido,-2);
        aux := FloatToStr(valorLiquido);
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
      end;

      //CV 19 - Adicionar Produto
      if (DMConexao.AdoQry.Fields[13].Value = '7') then begin
          S := S+ fnCompletarCom('2',3,'0');
      end else begin
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[13].Value,3,'0');
      end;
      //In�cio CV20 - C�DIGO BANCO
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[12].Value,3,'0');
     //CV20 Fim
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,6,'0');//CV21   - AG�NCIA
      aux:=   DMConexao.AdoQry.Fields[8].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,11,' ',True);//CV22  - CONTA
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//CV23  - AUTORIZA��O
      contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
      S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
      aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
      SalvarArquivo(S,nomeArquivo,false,'.txt');
      DMConexao.AdoQry.Next;
    end;
  end;

  //Final da Quarta Linha


  //Come�o da Quinta Linha - AJUSTES A D�BITO OU AJUSTES A CR�DITO

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT PC.DATA_AUTORIZACAO_REPASSE,BA.COD_BANCO, CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, ');
  DMConexao.AdoQry.SQL.Add('TR.VALOR, TR.HISTORICO, TR.TAXA_ID, TA.DESCRICAO   ');
  DMConexao.AdoQry.SQL.Add('FROM PAGAMENTO_CRED PC INNER JOIN TAXAS_REPASSE TR ON PC.PAGAMENTO_CRED_ID = TR.PAGAMENTO_CRED_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON PC.CRED_ID = CR.CRED_ID ');
  DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON CR.BANCO = BA.CODIGO ');
  DMConexao.AdoQry.SQL.Add('LEFT JOIN TAXAS TA ON TA.TAXA_ID = TR.TAXA_ID ');
  DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_AUTORIZACAO_REPASSE = '+QuotedStr(DateToStr(data))+' ');
  DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%FARMA PONTE%') + '  ');
  DMConexao.AdoQry.Open;

  //COMENT�RIOS AJ COM BASE NO MODELO 1.1 PLANT�O CARD
  while not DMConexao.AdoQry.Eof do begin
    if not (DMConexao.AdoQry.IsEmpty)  then begin
      S := '';
      S := S+ fnCompletarCom('AJ',2);
      //AJ02 - CNPJ
      aux := DMConexao.AdoQry.Fields[2].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,15,'0'); //AJ02 - CNPJ fim
      //AJ03 - DATA DE LAN�AMENTO
      aux := ConverteDiaParaAmericano(dataIncial);
      S := S + fnCompletarCom(aux,8);//AJ03 FIM
      S := S + '2';//AJ04 - TIPO DE AJUSTE
      //In�cio AJ06 - VALOR BRUTO  TODO - SOMAR EM VARI�VEL QUE VAI RETIRAR DO REPASSE
      valorRepasseEmpresa := valorRepasseEmpresa + StrToFloat(DMConexao.AdoQry.Fields[5].Value);
      dinheiroaux := DMConexao.AdoQry.Fields[5].Value;
      aux := Format('%m',[dinheiroaux]);
      aux := FormataDinheiro(aux);
      S := S+ fnCompletarCom(aux,11,'0');  //AJ06 Fim
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,3,'0');  //AJ07 TAXA_ID
      //AJ08 - DESCRI��O DA TAXA
      //SE EXISTE DESCRI��O NA TABELA TAXAS UTILIZA, SEN�O UTILIZA O HIST�RICO DA TABELA TAXAS_REPASSE
      if(DMConexao.AdoQry.Fields[7].Value = '0') then begin
           S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,30,' ', True);
      end else begin
           S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[8].Value,30,' ',True);
      end;//AJ08 - FIM
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[1].Value,3,'0'); //In�cio AJ09 - C�DIGO DO BANCO
      S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[3].Value,6,'0');//AJ10 - AG�NCIA
      //AJ11 - CONTA
      aux:=   DMConexao.AdoQry.Fields[4].Value;
      aux := RetiraCaracteresEspeciais(aux);
      S := S+ fnCompletarCom(aux,11,' ',True);//AJ11 - FIM
      contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
      S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//AJ12 - NSEQ
      aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
      SalvarArquivo(S,nomeArquivo,false,'.txt');
      DMConexao.AdoQry.Next;
    end;
  end;

  //Final da Quinta Linha



  //Come�o da Sexta Linha
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //FARMA PONTE
  DMConexao.AdoQry.SQL.Add('SELECT nsu,DATA,FORMAPAGTO_ID,cgc,debito, COMISSAO FROM CONTACORRENTE cc inner join CREDENCIADOS c on c.CRED_ID = cc.CRED_ID');
  DMConexao.AdoQry.SQL.Add('WHERE DATA between ' + DateToStr(d) + ' and ' + QuotedStr(DateToStr(d) + ' 23:59:59') + '  and CANCELADA = ' + QuotedStr('S'));
  DMConexao.AdoQry.SQL.Add('AND cc.CRED_ID IN (select cred_id from CREDENCIADOS where nome like ' + QuotedStr('%FARMA PONTE%') + 'AND DEBITO > 0 AND VALOR_CANCELADO > 0 )');
  DMConexao.AdoQry.Open;

  while not DMConexao.AdoQry.Eof do begin
    if not (DMConexao.AdoQry.IsEmpty)  then begin
      data :=   DMConexao.AdoQry.Fields[1].Value;
      if (DateToStr(d) = DateToStr(data)) then begin
        //Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
        valorDesconto := DMConexao.AdoQry.Fields[4].Value * (DMConexao.AdoQry.fields[5].Value/100);
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        valorEmpresa := valorEmpresa - valorDesconto;
        valorDesconto := DMConexao.AdoQry.Fields[4].Value;
        valorDesconto := SimpleRoundTo(valorDesconto,-2);
        valorFinal := valorFinal - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
        //Fim Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
        S := '';
        S := S+ fnCompletarCom('CC',2);
        aux := DMConexao.AdoQry.Fields[3].Value; //CNPJ IN�CIO
        aux := RetiraCaracteresEspeciais(aux);
        S := S+ fnCompletarCom(aux,15,'0');//CNPJ FIM
        S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
        dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
        aux := ConverteDiaParaAmericano(dataIncial);
        S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
        if(DMConexao.AdoQry.Fields[2].Value = 1) then begin//N�mero da parcela IN�CIO
          S := S+ fnCompletarCom('0',2,'0');
        end else begin
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[2].Value),2,'0');
        end;//N�mero da parcela FIM
        S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
        dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
        aux := ConverteDiaParaAmericano(dataIncial);
        S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
        DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli); //Convers�o de hora IN�CIO
        S := S+ fnCompletarCom(IntToStr(myHour),02);
        S := S+ fnCompletarCom(IntToStr(myMin),02);
        S := S+ fnCompletarCom(IntToStr(mySec),02);//Convers�o de hora FIM
        contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
        S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
        aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
        SalvarArquivo(S,nomeArquivo,false,'.txt');
      end;
      DMConexao.AdoQry.Next;
    end;
  end;

  //Fim da Sexta Linha

  //Come�o da S�tima Linha
  S := '';
  S := S+ fnCompletarCom('L9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo-2),6,'0');//L902
  dinheiroaux := valorFinal;//L903 IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //L903 Fim
  dinheiroaux := valorEmpresa;//VALOR TOTAL DE DESCONTO NO POS IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //VALOR TOTAL DE DESCONTO NO POS Fim
  dinheiroaux := valorRepasse;//valor repasse bruto
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//valor repasse bruto Fim
  dinheiroaux := valorRepasseEmpresa;//taxas do repasse IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//taxas do repasse Fim
//  aux := FloatToStr(valorFinal - valorEmpresa);//VALOR L�QUIDO IN�CIO
//  aux := RetiraCaracteresEspeciais(aux);
//  S := S+ fnCompletarCom(aux,14,'0');  //VALOR l�quidoFim

  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  //Fim da S�tima Linha

  //Come�o da Oitava Linha
  S := '';
  S := S+ fnCompletarCom('A9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo+1),6); //L902
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');

  //Fim da Oitava Linha
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,true,'.txt');


end;

procedure TFrmExportConciliacaoSpani.LayoultConciliacaoMangueira(var d: TDateTime);
var
  Erro,hasDepartamento : Boolean;
  parcelas,count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  aux,cartao,mesvalor, S, valor,dec,endereco,valorTotal,MOVIMENTO_ID, nomeAdministradora, teste : String;
  ano,mes,dia : Integer;
  myHour, myMin, mySec, myMilli : Word;
  prefixo_nome_txt,nomeArquivo : String;
  dataIncial : TDateTime;
  data : TDate;
  valorFinal,valorLiquido, valorDesconto, valorEmpresa, valorRepasse, valorRepasseEmpresa : Double;
  dinheiroaux :Currency;
begin
  valorFinal := 0;
  valorEmpresa := 0;
  valorRepasse := 0;
  valorRepasseEmpresa :=0;
  ProgressBar1.Position := 0;
  lblStatus.Caption :=  'Gerando arquivo...';
  lblStatus.Refresh;
  Erro := False;
  contadorDeLinhasDoArquivo := 0;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  //hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMOV_ID_SITEF AS MOVIMENTO_ID');
  DMConexao.AdoQry.Open;
  MOVIMENTO_ID := DMConexao.AdoQry.Fields[0].Value;

  //PREFIXO FIXO PARA GRAVAR O NOME DO ARQUIVO
  prefixo_nome_txt := 'rpccrt_APM_';
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  nomeArquivo := prefixo_nome_txt + aux + '_'+ fnCompletarCom(MOVIMENTO_ID,6,'0');

  //Cabe�alho
  //variaveis fixas do cabecalho
  nomeAdministradora := 'PLANTAO CARD';

  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom('A0',2);//A0 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom('0001.1',6);//VVV.RR "0016d"
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  DecodeTime(Now,myHour, myMin, mySec, myMilli);
  S := S+ fnCompletarCom(IntToStr(myHour),02);
  S := S+ fnCompletarCom(IntToStr(myMin),02);
  S := S+ fnCompletarCom(IntToStr(mySec),02); // A04 - gera��o do arquivo
  S := S+ fnCompletarCom(MOVIMENTO_ID,6,'0');  //A05 ID DO MOVIMENTO
  S := S+ fnCompletarCom(nomeAdministradora,30,' ',True); //Nome ADM
  S := S+ fnCompletarCom('1',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho

  //Segunda linha - Header do Lote de transa��es
  S := '';
  S := S+ fnCompletarCom('L0',2);//L0 - CODIGO DE REGISTRO =L0
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  S := S+ fnCompletarCom('RE',2);
  S := S+ fnCompletarCom('2',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim da Segunda Linha

  data :=   d;

  //Come�o da Terceira Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - PREVIS�O
  //VENDAS OCORRIDAS NO DIA
  //COMENT�RIOS CV LAYOUT PLANT�O CARD 1.1

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //Mangueira
      DMConexao.AdoQry.SQL.Add('SELECT CC.NSUHOST, DATA, FORMAPAGTO_ID, CR.CGC, COMISSAO, DEBITO AS VALOR, ');
      DMConexao.AdoQry.SQL.Add('CC.CARTAO_ID, HISTORICO, DEBITO, AUTORIZACAO_ID, AGENCIA, CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('CC.CRED_ID, CC.EMPRES_ID, CA.CODCARTIMP, MOD_CART_ID, COD_BANCO, DATAVENDA, CC.OPERADOR');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN CREDENCIADOS CR ON CC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CA.CARTAO_ID = CC.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CA.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('WHERE CC.DATA BETWEEN '+QuotedStr(DateToStr(data))+' AND '+QuotedStr(DateToStr(data)+' 23:59:59')+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.CRED_ID = 451 AND CC.DEBITO > 0');


      DMConexao.AdoQry.Open;
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - CNPJ in�cio
          aux := DMConexao.AdoQry.Fields[3].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim

          if (IntToStr(DMConexao.AdoQry.Fields[0].Value) = '0') then
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[9].Value),12,'0')//CV03 - NSU
          else
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV03 - NSU


          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[1].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04
          DecodeTime(DMConexao.AdoQry.Fields[17].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento ap�s 35 dias
          dataIncial := DMConexao.AdoQry.Fields[1].Value;
          if DayOfTheWeek(dataIncial) = 7 then begin
            dataIncial := dataIncial + 38;
          end else begin
            dataIncial := dataIncial + (7- DayOfTheWeek(dataIncial)) + 38 ;
          end;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '0';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorFinal := valorFinal+ DMConexao.AdoQry.Fields[8].Value;
          dinheiroaux := DMConexao.AdoQry.Fields[8].Value;
          aux := Format('%m',[dinheiroaux]);

          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[2].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);
          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[14].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[2].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[7].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 14 e CV 15 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV15 - NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 - VALOR BRUTO PARCELA in�cio
            aux := DMConexao.AdoQry.Fields[8].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 - VALOR DESCONTO PARCELA In�cio
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 - VALOR L�QUIDO PARCELA In�cio
            valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[15].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[15].Value,3,'0');
          end;
          //CV19 FIM
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[16].Value),3,'0');//CV20 C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[10].Value,6,'0');//CV21 AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[11].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22 CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[9].Value,12,'0');//CV23 AUTORIZA��O
          if(DMConexao.AdoQry.Fields[18].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[18].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[18].Value = 'TEF')or(DMConexao.AdoQry.Fields[18].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//CV24 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
      //Fim da Terceira Linha

      //Come�o da Quarta Linha
      //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - LIQUIDA��O
      //CONFIRMA��O DE REPASSE DAS TRANSA��ES
      //COMENT�RIOS CV COM BASE NO MODELO 1.1 PLANT�O CARD

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT CC.AUTORIZACAO_ID,CC.NSUHOST, CC.DEBITO, CC.DATA, CC.FORMAPAGTO_ID, CA.CODCARTIMP, ');
      DMConexao.AdoQry.SQL.Add('CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, CR.COMISSAO,  ');
      DMConexao.AdoQry.SQL.Add('PC.DATA_COMPENSACAO,CC.HISTORICO, BA.COD_BANCO, EM.MOD_CART_ID, CC.OPERADOR ');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN PAGAMENTO_CRED PC ON CC.PAGAMENTO_CRED_ID = PC.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON CR.CRED_ID = CC.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CC.CARTAO_ID = CA.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.CRED_ID = 451 ');
      DMConexao.AdoQry.SQL.Add('AND CC.DEBITO > 0 ');
      DMConexao.AdoQry.SQL.Add('AND CC.VALOR_CANCELADO <= 0 ');
      DMConexao.AdoQry.Open;


      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - Cnpj in�cio
          aux := DMConexao.AdoQry.Fields[6].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU
          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[3].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04 FIM
          DecodeTime(DMConexao.AdoQry.Fields[3].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento liquida��o imediata
          dataIncial := DMConexao.AdoQry.Fields[10].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '1';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorRepasse := valorRepasse + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[2].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[4].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorRepasseEmpresa := valorRepasseEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);

          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[5].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[4].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[11].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 13 e CV 14 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV15 NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 in�cio VALOR BRUTO PARCELA
            aux := DMConexao.AdoQry.Fields[2].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 In�cio  - VALOR DE DESCONTO DA PARCELA
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 In�cio - VALOR L�QUIDO DA PARCELA
            valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[13].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[13].Value,3,'0');
          end;
          //In�cio CV20 - C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[12].Value,3,'0');
         //CV20 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,6,'0');//CV21   - AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[8].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22  - CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//CV23  - AUTORIZA��O
          if(DMConexao.AdoQry.Fields[14].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[14].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[14].Value = 'TEF')or(DMConexao.AdoQry.Fields[14].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;

      //Final da Quarta Linha


      //Come�o da Quinta Linha - AJUSTES A D�BITO OU AJUSTES A CR�DITO

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT PC.DATA_AUTORIZACAO_REPASSE,BA.COD_BANCO, CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('TR.VALOR, TR.HISTORICO, TR.TAXA_ID, TA.DESCRICAO   ');
      DMConexao.AdoQry.SQL.Add('FROM PAGAMENTO_CRED PC INNER JOIN TAXAS_REPASSE TR ON PC.PAGAMENTO_CRED_ID = TR.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON PC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON CR.BANCO = BA.CODIGO ');
      DMConexao.AdoQry.SQL.Add('LEFT JOIN TAXAS TA ON TA.TAXA_ID = TR.TAXA_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.CRED_ID = 451 ');
      DMConexao.AdoQry.Open;
    
      //COMENT�RIOS AJ COM BASE NO MODELO 1.1 PLANT�O CARD
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('AJ',2);
          //AJ02 - CNPJ
          aux := DMConexao.AdoQry.Fields[2].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //AJ02 - CNPJ fim
          //AJ03 - DATA DE LAN�AMENTO
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//AJ03 FIM
          S := S + '2';//AJ04 - TIPO DE AJUSTE
          //In�cio AJ06 - VALOR BRUTO  TODO - SOMAR EM VARI�VEL QUE VAI RETIRAR DO REPASSE
          valorRepasseEmpresa := valorRepasseEmpresa + StrToFloat(DMConexao.AdoQry.Fields[5].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[5].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //AJ06 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,3,'0');  //AJ07 TAXA_ID
          //AJ08 - DESCRI��O DA TAXA
          //SE EXISTE DESCRI��O NA TABELA TAXAS UTILIZA, SEN�O UTILIZA O HIST�RICO DA TABELA TAXAS_REPASSE
          if(DMConexao.AdoQry.Fields[7].Value = '0') then begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,30,' ', True);
          end else begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[8].Value,30,' ',True);
          end;//AJ08 - FIM
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[1].Value,3,'0'); //In�cio AJ09 - C�DIGO DO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[3].Value,6,'0');//AJ10 - AG�NCIA
          //AJ11 - CONTA
          aux:=   DMConexao.AdoQry.Fields[4].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//AJ11 - FIM
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//AJ12 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
    
      //Final da Quinta Linha
      //Come�o da Sexta Linha
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
    
      //Spani
      DMConexao.AdoQry.SQL.Add('SELECT NSUHOST,DATA,FORMAPAGTO_ID,cgc,debito, COMISSAO, cc.AUTORIZACAO_ID FROM CONTACORRENTE cc inner join CREDENCIADOS c on c.CRED_ID = cc.CRED_ID');
      DMConexao.AdoQry.SQL.Add('WHERE DATA between ' + DateToStr(d) + ' and ' + QuotedStr(DateToStr(d) + ' 23:59:59') + '  and CANCELADA = ' + QuotedStr('S'));
      DMConexao.AdoQry.SQL.Add('AND cc.CRED_ID = 451');
      DMConexao.AdoQry.Open;
    
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          data :=   DMConexao.AdoQry.Fields[1].Value;
          if (DateToStr(d) = DateToStr(data)) then begin
            //Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            valorDesconto := DMConexao.AdoQry.Fields[4].Value * (DMConexao.AdoQry.fields[5].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa - valorDesconto;
            valorDesconto := DMConexao.AdoQry.Fields[4].Value;
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorFinal := valorFinal - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
            //Fim Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            S := '';
            S := S+ fnCompletarCom('CC',2);
            aux := DMConexao.AdoQry.Fields[3].Value; //CNPJ IN�CIO
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,15,'0');//CNPJ FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            if(DMConexao.AdoQry.Fields[2].Value = 1) then begin//N�mero da parcela IN�CIO
              S := S+ fnCompletarCom('0',2,'0');
            end else begin
              S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[2].Value),2,'0');
            end;//N�mero da parcela FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli); //Convers�o de hora IN�CIO
            S := S+ fnCompletarCom(IntToStr(myHour),02);
            S := S+ fnCompletarCom(IntToStr(myMin),02);
            S := S+ fnCompletarCom(IntToStr(mySec),02);//Convers�o de hora FIM
            contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,12,'0'); //C�DIGO DE AUTORIZA��O
            S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
            aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
            SalvarArquivo(S,nomeArquivo,false,'.txt');
          end;
          DMConexao.AdoQry.Next;
        end;
      end;
  //Fim da Sexta Linha

  //Come�o da S�tima Linha
  S := '';
  S := S+ fnCompletarCom('L9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo-2),6,'0');//L902
  dinheiroaux := valorFinal;//L903 IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //L903 Fim
  dinheiroaux := valorEmpresa;//VALOR TOTAL DE DESCONTO NO POS IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //VALOR TOTAL DE DESCONTO NO POS Fim
  dinheiroaux := valorRepasse;//valor repasse bruto
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//valor repasse bruto Fim
  dinheiroaux := valorRepasseEmpresa;//taxas do repasse IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//taxas do repasse Fim
//  aux := FloatToStr(valorFinal - valorEmpresa);//VALOR L�QUIDO IN�CIO
//  aux := RetiraCaracteresEspeciais(aux);
//  S := S+ fnCompletarCom(aux,14,'0');  //VALOR l�quidoFim

  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  //Fim da S�tima Linha

  //Come�o da Oitava Linha
  S := '';
  S := S+ fnCompletarCom('A9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo+1),6); //L902
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');

  //Fim da Oitava Linha
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,true,'.txt');


end;

procedure TFrmExportConciliacaoSpani.LayoultConciliacaoSpani(var d: TDateTime);
var
  Erro,hasDepartamento : Boolean;
  parcelas,count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  aux,cartao,mesvalor, S, valor,dec,endereco,valorTotal,MOVIMENTO_ID, nomeAdministradora, teste : String;
  ano,mes,dia : Integer;
  myHour, myMin, mySec, myMilli : Word;
  prefixo_nome_txt,nomeArquivo : String;
  dataIncial : TDateTime;
  data : TDate;
  valorFinal,valorLiquido, valorDesconto, valorEmpresa, valorRepasse, valorRepasseEmpresa : Double;
  dinheiroaux :Currency;
begin
  valorFinal := 0;
  valorEmpresa := 0;
  valorRepasse := 0;
  valorRepasseEmpresa :=0;
  ProgressBar1.Position := 0;
  lblStatus.Caption :=  'Gerando arquivo...';
  lblStatus.Refresh;
  Erro := False;
  contadorDeLinhasDoArquivo := 0;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  //hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMOV_ID_SITEF AS MOVIMENTO_ID');
  DMConexao.AdoQry.Open;
  MOVIMENTO_ID := DMConexao.AdoQry.Fields[0].Value;

  //PREFIXO FIXO PARA GRAVAR O NOME DO ARQUIVO
  prefixo_nome_txt := 'rpccrt_zar_';
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  nomeArquivo := prefixo_nome_txt + aux + '_'+ fnCompletarCom(MOVIMENTO_ID,6,'0');

  //Cabe�alho
  //variaveis fixas do cabecalho
  nomeAdministradora := 'PLANTAO CARD';

  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom('A0',2);//A0 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom('0001.1',6);//VVV.RR "0016d"
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  DecodeTime(Now,myHour, myMin, mySec, myMilli);
  S := S+ fnCompletarCom(IntToStr(myHour),02);
  S := S+ fnCompletarCom(IntToStr(myMin),02);
  S := S+ fnCompletarCom(IntToStr(mySec),02); // A04 - gera��o do arquivo
  S := S+ fnCompletarCom(MOVIMENTO_ID,6,'0');  //A05 ID DO MOVIMENTO
  S := S+ fnCompletarCom(nomeAdministradora,30,' ',True);
  S := S+ fnCompletarCom('1',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho

  //Segunda linha - Header do Lote de transa��es
  S := '';
  S := S+ fnCompletarCom('L0',2);//L0 - CODIGO DE REGISTRO =L0
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  S := S+ fnCompletarCom('RE',2);
  S := S+ fnCompletarCom('2',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim da Segunda Linha

  data :=   d;

  //Come�o da Terceira Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - PREVIS�O
  //VENDAS OCORRIDAS NO DIA
  //COMENT�RIOS CV LAYOUT PLANT�O CARD 1.1

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //SPANI
      DMConexao.AdoQry.SQL.Add('SELECT CC.NSUHOST, DATA, FORMAPAGTO_ID, CR.CGC, COMISSAO, DEBITO AS VALOR, ');
      DMConexao.AdoQry.SQL.Add('CC.CARTAO_ID, HISTORICO, DEBITO, AUTORIZACAO_ID, AGENCIA, CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('CC.CRED_ID, CC.EMPRES_ID, CA.CODCARTIMP, MOD_CART_ID, COD_BANCO, DATAVENDA, CC.OPERADOR');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN CREDENCIADOS CR ON CC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CA.CARTAO_ID = CC.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CA.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('WHERE CC.DATA BETWEEN '+QuotedStr(DateToStr(data))+' AND '+QuotedStr(DateToStr(data)+' 23:59:59')+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE'+QuotedStr('%ZARAGOZA%')+' AND CC.DEBITO > 0');


      DMConexao.AdoQry.Open;
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - CNPJ in�cio
          aux := DMConexao.AdoQry.Fields[3].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim

          if (IntToStr(DMConexao.AdoQry.Fields[0].Value) = '0') then
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[9].Value),12,'0')//CV03 - NSU
          else
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV03 - NSU


          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[1].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04
          DecodeTime(DMConexao.AdoQry.Fields[17].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento ap�s 35 dias
          dataIncial := DMConexao.AdoQry.Fields[1].Value;
          if DayOfTheWeek(dataIncial) = 7 then begin
            dataIncial := dataIncial + 38;
          end else begin
            dataIncial := dataIncial + (7- DayOfTheWeek(dataIncial)) + 38 ;
          end;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '0';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorFinal := valorFinal+ DMConexao.AdoQry.Fields[8].Value;
          dinheiroaux := DMConexao.AdoQry.Fields[8].Value;
          aux := Format('%m',[dinheiroaux]);

          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[2].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);
          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[14].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[2].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[7].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 14 e CV 15 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV15 - NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 - VALOR BRUTO PARCELA in�cio
            aux := DMConexao.AdoQry.Fields[8].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 - VALOR DESCONTO PARCELA In�cio
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 - VALOR L�QUIDO PARCELA In�cio
            valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[15].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[15].Value,3,'0');
          end;
          //CV19 FIM
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[16].Value),3,'0');//CV20 C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[10].Value,6,'0');//CV21 AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[11].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22 CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[9].Value,12,'0');//CV23 AUTORIZA��O
          if(DMConexao.AdoQry.Fields[18].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[18].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[18].Value = 'TEF')or(DMConexao.AdoQry.Fields[18].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//CV24 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
      //Fim da Terceira Linha

      //Come�o da Quarta Linha
      //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - LIQUIDA��O
      //CONFIRMA��O DE REPASSE DAS TRANSA��ES
      //COMENT�RIOS CV COM BASE NO MODELO 1.1 PLANT�O CARD

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT CC.AUTORIZACAO_ID,CC.NSUHOST, CC.DEBITO, CC.DATA, CC.FORMAPAGTO_ID, CA.CODCARTIMP, ');
      DMConexao.AdoQry.SQL.Add('CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, CR.COMISSAO,  ');
      DMConexao.AdoQry.SQL.Add('PC.DATA_COMPENSACAO,CC.HISTORICO, BA.COD_BANCO, EM.MOD_CART_ID, CC.OPERADOR ');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN PAGAMENTO_CRED PC ON CC.PAGAMENTO_CRED_ID = PC.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON CR.CRED_ID = CC.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CC.CARTAO_ID = CA.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%ZARAGOZA%') + '  ');
      DMConexao.AdoQry.SQL.Add('AND CC.DEBITO > 0 ');
      DMConexao.AdoQry.SQL.Add('AND CC.VALOR_CANCELADO <= 0 ');
      DMConexao.AdoQry.Open;


      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - Cnpj in�cio
          aux := DMConexao.AdoQry.Fields[6].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU
          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[3].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04 FIM
          DecodeTime(DMConexao.AdoQry.Fields[3].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento liquida��o imediata
          dataIncial := DMConexao.AdoQry.Fields[10].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '1';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorRepasse := valorRepasse + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[2].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[4].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorRepasseEmpresa := valorRepasseEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);

          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[5].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[4].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[11].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 13 e CV 14 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV15 NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 in�cio VALOR BRUTO PARCELA
            aux := DMConexao.AdoQry.Fields[2].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 In�cio  - VALOR DE DESCONTO DA PARCELA
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 In�cio - VALOR L�QUIDO DA PARCELA
            valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[13].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[13].Value,3,'0');
          end;
          //In�cio CV20 - C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[12].Value,3,'0');
         //CV20 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,6,'0');//CV21   - AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[8].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22  - CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//CV23  - AUTORIZA��O
          if(DMConexao.AdoQry.Fields[14].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[14].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[14].Value = 'TEF')or(DMConexao.AdoQry.Fields[14].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;

      //Final da Quarta Linha


      //Come�o da Quinta Linha - AJUSTES A D�BITO OU AJUSTES A CR�DITO

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT PC.DATA_AUTORIZACAO_REPASSE,BA.COD_BANCO, CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('TR.VALOR, TR.HISTORICO, TR.TAXA_ID, TA.DESCRICAO   ');
      DMConexao.AdoQry.SQL.Add('FROM PAGAMENTO_CRED PC INNER JOIN TAXAS_REPASSE TR ON PC.PAGAMENTO_CRED_ID = TR.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON PC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON CR.BANCO = BA.CODIGO ');
      DMConexao.AdoQry.SQL.Add('LEFT JOIN TAXAS TA ON TA.TAXA_ID = TR.TAXA_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%ZARAGOZA%') + '  ');
      DMConexao.AdoQry.Open;
    
      //COMENT�RIOS AJ COM BASE NO MODELO 1.1 PLANT�O CARD
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('AJ',2);
          //AJ02 - CNPJ
          aux := DMConexao.AdoQry.Fields[2].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //AJ02 - CNPJ fim
          //AJ03 - DATA DE LAN�AMENTO
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//AJ03 FIM
          S := S + '2';//AJ04 - TIPO DE AJUSTE
          //In�cio AJ06 - VALOR BRUTO  TODO - SOMAR EM VARI�VEL QUE VAI RETIRAR DO REPASSE
          valorRepasseEmpresa := valorRepasseEmpresa + StrToFloat(DMConexao.AdoQry.Fields[5].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[5].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //AJ06 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,3,'0');  //AJ07 TAXA_ID
          //AJ08 - DESCRI��O DA TAXA
          //SE EXISTE DESCRI��O NA TABELA TAXAS UTILIZA, SEN�O UTILIZA O HIST�RICO DA TABELA TAXAS_REPASSE
          if(DMConexao.AdoQry.Fields[7].Value = '0') then begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,30,' ', True);
          end else begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[8].Value,30,' ',True);
          end;//AJ08 - FIM
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[1].Value,3,'0'); //In�cio AJ09 - C�DIGO DO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[3].Value,6,'0');//AJ10 - AG�NCIA
          //AJ11 - CONTA
          aux:=   DMConexao.AdoQry.Fields[4].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//AJ11 - FIM
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//AJ12 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
    
      //Final da Quinta Linha
      //Come�o da Sexta Linha
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
    
      //Spani
      DMConexao.AdoQry.SQL.Add('SELECT NSUHOST,DATA,FORMAPAGTO_ID,cgc,debito, COMISSAO, cc.AUTORIZACAO_ID FROM CONTACORRENTE cc inner join CREDENCIADOS c on c.CRED_ID = cc.CRED_ID');
      DMConexao.AdoQry.SQL.Add('WHERE DATA between ' + DateToStr(d) + ' and ' + QuotedStr(DateToStr(d) + ' 23:59:59') + '  and CANCELADA = ' + QuotedStr('S'));
      DMConexao.AdoQry.SQL.Add('AND cc.CRED_ID IN (select cred_id from CREDENCIADOS where nome like ' + QuotedStr('%zarago%') + 'AND DEBITO > 0 AND VALOR_CANCELADO > 0 )');
      DMConexao.AdoQry.Open;
    
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          data :=   DMConexao.AdoQry.Fields[1].Value;
          if (DateToStr(d) = DateToStr(data)) then begin
            //Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            valorDesconto := DMConexao.AdoQry.Fields[4].Value * (DMConexao.AdoQry.fields[5].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa - valorDesconto;
            valorDesconto := DMConexao.AdoQry.Fields[4].Value;
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorFinal := valorFinal - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
            //Fim Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            S := '';
            S := S+ fnCompletarCom('CC',2);
            aux := DMConexao.AdoQry.Fields[3].Value; //CNPJ IN�CIO
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,15,'0');//CNPJ FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            if(DMConexao.AdoQry.Fields[2].Value = 1) then begin//N�mero da parcela IN�CIO
              S := S+ fnCompletarCom('0',2,'0');
            end else begin
              S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[2].Value),2,'0');
            end;//N�mero da parcela FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli); //Convers�o de hora IN�CIO
            S := S+ fnCompletarCom(IntToStr(myHour),02);
            S := S+ fnCompletarCom(IntToStr(myMin),02);
            S := S+ fnCompletarCom(IntToStr(mySec),02);//Convers�o de hora FIM
            contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,12,'0'); //C�DIGO DE AUTORIZA��O
            S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
            aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
            SalvarArquivo(S,nomeArquivo,false,'.txt');
          end;
          DMConexao.AdoQry.Next;
        end;
      end;
  //Fim da Sexta Linha

  //Come�o da S�tima Linha
  S := '';
  S := S+ fnCompletarCom('L9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo-2),6,'0');//L902
  dinheiroaux := valorFinal;//L903 IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //L903 Fim
  dinheiroaux := valorEmpresa;//VALOR TOTAL DE DESCONTO NO POS IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //VALOR TOTAL DE DESCONTO NO POS Fim
  dinheiroaux := valorRepasse;//valor repasse bruto
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//valor repasse bruto Fim
  dinheiroaux := valorRepasseEmpresa;//taxas do repasse IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//taxas do repasse Fim
//  aux := FloatToStr(valorFinal - valorEmpresa);//VALOR L�QUIDO IN�CIO
//  aux := RetiraCaracteresEspeciais(aux);
//  S := S+ fnCompletarCom(aux,14,'0');  //VALOR l�quidoFim

  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  //Fim da S�tima Linha

  //Come�o da Oitava Linha
  S := '';
  S := S+ fnCompletarCom('A9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo+1),6); //L902
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');

  //Fim da Oitava Linha
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,true,'.txt');


end;

procedure TFrmExportConciliacaoSpani.LayoultConciliacaoSpaniLayoutNovo(var d: TDateTime);
var
  Erro,hasDepartamento : Boolean;
  parcelas,count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  aux,cartao,mesvalor, S, valor,dec,endereco,valorTotal,MOVIMENTO_ID, nomeAdministradora, teste, idNSUHOST : String;
  ano,mes,dia : Integer;
  myHour, myMin, mySec, myMilli : Word;
  prefixo_nome_txt,nomeArquivo : String;
  dataIncial : TDateTime;
  data : TDate;
  valorFinal,valorLiquido, valorDesconto, valorEmpresa, valorRepasse, valorRepasseEmpresa, CVBruto, CPBruto, AJBruto,
  CCBruto, CVLiquido, CPLiquido, AJCLiquido : Double;
  dinheiroaux :Currency;
begin
  valorFinal := 0;
  valorEmpresa := 0;
  valorRepasse := 0;
  valorRepasseEmpresa :=0;
  ProgressBar1.Position := 0;
  lblStatus.Caption :=  'Gerando arquivo...';
  lblStatus.Refresh;
  Erro := False;
  contadorDeLinhasDoArquivo := 0;
  CVBruto := 0;
  CPBruto := 0;
  AJBruto := 0;
  CCBruto := 0;
  CVLiquido := 0;
  CPLiquido := 0;
  AJCLiquido := 0;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  //hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMOV_ID_SITEF AS MOVIMENTO_ID');
  DMConexao.AdoQry.Open;
  MOVIMENTO_ID := DMConexao.AdoQry.Fields[0].Value;


  //PREFIXO FIXO PARA GRAVAR O NOME DO ARQUIVO
  prefixo_nome_txt := 'rpccrt_zar_';
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  nomeArquivo := prefixo_nome_txt + aux + '_'+ fnCompletarCom(MOVIMENTO_ID,6,'0');

  //Cabe�alho
  //variaveis fixas do cabecalho
  nomeAdministradora := 'PLANTAO CARD';

  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom('A0',2);//A0 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom('001.6d',6);//VVV.RR "0016d"
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  DecodeTime(Now,myHour, myMin, mySec, myMilli);
  S := S+ fnCompletarCom(IntToStr(myHour),02);
  S := S+ fnCompletarCom(IntToStr(myMin),02);
  S := S+ fnCompletarCom(IntToStr(mySec),02); // A04 - gera��o do arquivo
  S := S+ fnCompletarCom(MOVIMENTO_ID,6,'0');  //A05 ID DO MOVIMENTO
  S := S+ fnCompletarCom(nomeAdministradora,30,' ',True);
  S := S+ fnCompletarCom('0',4,'0');
  S := S+ fnCompletarCom('0',6,'0');
  S := S+ fnCompletarCom('1',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho

  //Segunda linha - Header do Lote de transa��es
  S := '';
  S := S+ fnCompletarCom('L0',2);//L0 - CODIGO DE REGISTRO =L0
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  S := S+ fnCompletarCom('RE',2);
  S := S+ fnCompletarCom('2',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim da Segunda Linha

  data :=   d;

  //Come�o da Terceira Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - PREVIS�O
  //VENDAS OCORRIDAS NO DIA
  //COMENT�RIOS CV LAYOUT PLANT�O CARD 1.1

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //SPANI
      DMConexao.AdoQry.SQL.Add('SELECT CASE WHEN CC.OPERADOR = ''TEF'' THEN TRANS.ACCEPTORTAN ELSE CC.NSUHOST END AS NSUHOST ');
      DMConexao.AdoQry.SQL.Add(' , DATA, FORMAPAGTO_ID, CR.CGC, COMISSAO, DEBITO AS VALOR, ');
      DMConexao.AdoQry.SQL.Add('CC.CARTAO_ID, HISTORICO, DEBITO, AUTORIZACAO_ID, AGENCIA, CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('CC.CRED_ID, CC.EMPRES_ID, CA.CODCARTIMP, MOD_CART_ID, COD_BANCO, DATAVENDA, CC.OPERADOR ');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN CREDENCIADOS CR ON CC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CA.CARTAO_ID = CC.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CA.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN TRANSACOES TRANS ON TRANS.TRANS_ID = CC.TRANS_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE CC.DATA BETWEEN '+QuotedStr(DateToStr(data))+' AND '+QuotedStr(DateToStr(data)+' 23:59:59')+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE'+QuotedStr('%ZARAGOZA%')+' AND CC.DEBITO > 0');


      DMConexao.AdoQry.Open;
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - CNPJ in�cio
          aux := DMConexao.AdoQry.Fields[3].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
            if (IntToStr(DMConexao.AdoQry.Fields[0].Value) = '0') then
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[9].Value),12,'0')//CV03 - NSU
          else
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV03 - NSU

          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[1].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04
          DecodeTime(DMConexao.AdoQry.Fields[17].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento ap�s 35 dias
          dataIncial := DMConexao.AdoQry.Fields[1].Value;
          if DayOfTheWeek(dataIncial) = 7 then begin
            dataIncial := dataIncial + 38;
          end else begin
            dataIncial := dataIncial + (7- DayOfTheWeek(dataIncial)) + 38 ;
          end;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + '0';//CV 07 - TIPO DE LAN�AMENTO
          S := S + fnCompletarCom(aux,8);//CV06 FIM

          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          if(DMConexao.AdoQry.Fields[18].Value = 'AUTOR.SITE') then begin
            S := S + '5';
          end else begin
            if (DMConexao.AdoQry.Fields[18].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[18].Value = 'TEF')or(DMConexao.AdoQry.Fields[18].Value = 'GETNET')) then begin
                S := S + '3';
              end else begin
                S := S + '9';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA

          //In�cio CV09  - valor bruto
          valorFinal := valorFinal+ DMConexao.AdoQry.Fields[8].Value;
          CVBruto := CVBruto+ DMConexao.AdoQry.Fields[8].Value;
          dinheiroaux := DMConexao.AdoQry.Fields[8].Value;
          aux := Format('%m',[dinheiroaux]);

          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[2].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);
          CVLiquido := CVLiquido+ valorLiquido;
          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[14].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[2].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[7].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 14 e CV 15 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV15 - NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 - VALOR BRUTO PARCELA in�cio
            aux := DMConexao.AdoQry.Fields[8].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 - VALOR DESCONTO PARCELA In�cio
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 - VALOR L�QUIDO PARCELA In�cio
            valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          //if (DMConexao.AdoQry.Fields[15].Value = '7') then begin
          //    S := S+ fnCompletarCom('2',3,'0');
          //end else begin
          //    S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[15].Value,3,'0');
          //end;
          //CV19 FIM
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[16].Value),3,'0');//CV20 C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[10].Value,6,'0');//CV21 AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[11].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22 CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[9].Value,12,'0');//CV23 AUTORIZA��O

          S := S+ fnCompletarCom('0',3,'0');     //CODIGO DA BANDEIRA
          S := S+ fnCompletarCom('0',3,'0');     //CODIGO DO PRODUTO

          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//CV24 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
      //Fim da Terceira Linha

      //Come�o da Quarta Linha
      //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - LIQUIDA��O
      //CONFIRMA��O DE REPASSE DAS TRANSA��ES
      //COMENT�RIOS CV COM BASE NO MODELO 1.1 PLANT�O CARD

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT CC.AUTORIZACAO_ID,CASE WHEN CC.OPERADOR = ''TEF'' THEN TRANS.ACCEPTORTAN ELSE CC.NSUHOST END AS NSUHOST ');
      DMConexao.AdoQry.SQL.Add(' , CC.DEBITO, CC.DATA, CC.FORMAPAGTO_ID, CA.CODCARTIMP, ');
      DMConexao.AdoQry.SQL.Add('CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, CR.COMISSAO,  ');
      DMConexao.AdoQry.SQL.Add('PC.DATA_COMPENSACAO,CC.HISTORICO, BA.COD_BANCO, EM.MOD_CART_ID, CC.OPERADOR ');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN PAGAMENTO_CRED PC ON CC.PAGAMENTO_CRED_ID = PC.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON CR.CRED_ID = CC.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CC.CARTAO_ID = CA.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add(' INNER JOIN TRANSACOES TRANS ON CC.TRANS_ID = TRANS.TRANS_ID');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%ZARAGOZA%') + '  ');
      DMConexao.AdoQry.SQL.Add('AND CC.DEBITO > 0 ');
      DMConexao.AdoQry.SQL.Add('AND CC.VALOR_CANCELADO <= 0 ');
      DMConexao.AdoQry.Open;


      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CP',2);
          //CV02 - Cnpj in�cio
          aux := DMConexao.AdoQry.Fields[6].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
            if (IntToStr(DMConexao.AdoQry.Fields[1].Value) = '0') then
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0')//CV03 - NSU
          else
            S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU

          //S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU
          
          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[3].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04 FIM
          DecodeTime(DMConexao.AdoQry.Fields[3].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento liquida��o imediata
          dataIncial := DMConexao.AdoQry.Fields[10].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + '1';//CV 07 - TIPO DE LAN�AMENTO
          S := S + fnCompletarCom(aux,8);//CV06 FIM

          if(DMConexao.AdoQry.Fields[14].Value = 'AUTOR.SITE') then begin
            S := S + '5';
          end else begin
            if (DMConexao.AdoQry.Fields[14].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[14].Value = 'TEF')or(DMConexao.AdoQry.Fields[14].Value = 'GETNET')) then begin
                S := S + '3';
              end else begin
                S := S + '9';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA

          //S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorRepasse := valorRepasse + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          CPBruto := CPBruto + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[2].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[4].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorRepasseEmpresa := valorRepasseEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);
          CPLiquido := CPLiquido + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[5].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim

          //COMENTADO ARIANE
          //CV13 e CV14 In�cio
          //parcelas :=   DMConexao.AdoQry.Fields[4].value;
          //if (parcelas = 1) then begin
          // S := S+ fnCompletarCom('0',4,'0');
          //end else begin
          //  aux := DMConexao.AdoQry.Fields[11].Value;
          //  S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
          //  S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          //end;//CV 13 e CV 14 Fim
          //S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV15 NSU PARCELA
          //if (parcelas = 1) then begin
          // S := S+ fnCompletarCom('0',33);
          //end else begin
          //  //CV16 in�cio VALOR BRUTO PARCELA
          //  aux := DMConexao.AdoQry.Fields[2].Value;
          //  aux := RetiraCaracteresEspeciais(aux);
          //  S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 In�cio  - VALOR DE DESCONTO DA PARCELA
          //  valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
          //  valorDesconto := SimpleRoundTo(valorDesconto,-2);
          //  aux := FloatToStr(valorDesconto);
          //  aux := RetiraCaracteresEspeciais(aux);
          //  S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 In�cio - VALOR L�QUIDO DA PARCELA
           // valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
          //  valorLiquido := SimpleRoundTo(valorLiquido,-2);
          //  aux := FloatToStr(valorLiquido);
          //  aux := RetiraCaracteresEspeciais(aux);
          //  S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          //end;
          //CV 19 - Adicionar Produto
          //if (DMConexao.AdoQry.Fields[13].Value = '7') then begin
          //    S := S+ fnCompletarCom('2',3,'0');
          //end else begin
          //    S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[13].Value,3,'0');
          //end;
          //In�cio CV20 - C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[12].Value,3,'0');
         //CV20 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,6,'0');//CV21   - AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[8].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22  - CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//CV23  - AUTORIZA��O


          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;

      //Final da Quarta Linha


      //Come�o da Quinta Linha - AJUSTES A D�BITO OU AJUSTES A CR�DITO


      //Final da Quinta Linha
      //Come�o da Sexta Linha
      {DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
    
      //Spani
      DMConexao.AdoQry.SQL.Add('SELECT NSUHOST,DATA,FORMAPAGTO_ID,cgc,debito, COMISSAO, cc.AUTORIZACAO_ID, cc.operador FROM CONTACORRENTE cc inner join CREDENCIADOS c on c.CRED_ID = cc.CRED_ID');
      DMConexao.AdoQry.SQL.Add('WHERE DATA between ' + DateToStr(d) + ' and ' + QuotedStr(DateToStr(d) + ' 23:59:59') + '  and CANCELADA = ' + QuotedStr('S'));
      DMConexao.AdoQry.SQL.Add('AND cc.CRED_ID IN (select cred_id from CREDENCIADOS where nome like ' + QuotedStr('%zarago%') + 'AND DEBITO > 0 AND VALOR_CANCELADO > 0 )');
      DMConexao.AdoQry.Open;
    
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          data :=   DMConexao.AdoQry.Fields[1].Value;
          if (DateToStr(d) = DateToStr(data)) then begin
            //Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            valorDesconto := DMConexao.AdoQry.Fields[4].Value * (DMConexao.AdoQry.fields[5].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            //valorEmpresa := valorEmpresa - valorDesconto;
            valorDesconto := DMConexao.AdoQry.Fields[4].Value;
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorFinal := valorFinal - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
            CCBruto := CCBruto - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
            //Fim Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            S := '';
            S := S+ fnCompletarCom('CC',2);
            aux := DMConexao.AdoQry.Fields[3].Value; //CNPJ IN�CIO
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,15,'0');//CNPJ FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            if(DMConexao.AdoQry.Fields[2].Value = 1) then begin//N�mero da parcela IN�CIO
              S := S+ fnCompletarCom('0',2,'0');
            end else begin
              S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[2].Value),2,'0');
            end;//N�mero da parcela FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli); //Convers�o de hora IN�CIO
            S := S+ fnCompletarCom(IntToStr(myHour),02);
            S := S+ fnCompletarCom(IntToStr(myMin),02);
            S := S+ fnCompletarCom(IntToStr(mySec),02);//Convers�o de hora FIM
            contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;

          if(DMConexao.AdoQry.Fields[8].Value = 'AUTOR.SITE') then begin
            S := S + '5';
          end else begin
            if (DMConexao.AdoQry.Fields[8].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[8].Value = 'TEF')or(DMConexao.AdoQry.Fields[8].Value = 'GETNET')) then begin
                S := S + '3';
              end else begin
                S := S + '9';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA

            //S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,12,'0'); //C�DIGO DE AUTORIZA��O
            S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
            aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
            SalvarArquivo(S,nomeArquivo,false,'.txt');
          end;
          DMConexao.AdoQry.Next;
        end;
      end;
  //Fim da Sexta Linha    }

  //Come�o da S�tima Linha
  S := '';
  S := S+ fnCompletarCom('L9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo-2),6,'0');//L902
  dinheiroaux := CVBruto - CPBruto;//L903 IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //L903 Fim
  //dinheiroaux := valorEmpresa;//VALOR TOTAL DE DESCONTO NO POS IN�CIO
  //aux := Format('%m',[dinheiroaux]);
  //aux := FormataDinheiro(aux);
  //S := S+ fnCompletarCom(aux,14,'0');  //VALOR TOTAL DE DESCONTO NO POS Fim
  //dinheiroaux := valorRepasse;//valor repasse bruto
  //aux := Format('%m',[dinheiroaux]);
  //aux := FormataDinheiro(aux);
  //S := S+ fnCompletarCom(aux,14,'0');//valor repasse bruto Fim
  //dinheiroaux := valorRepasseEmpresa;//taxas do repasse IN�CIO
  //aux := Format('%m',[dinheiroaux]);
  //aux := FormataDinheiro(aux);
  //S := S+ fnCompletarCom(aux,14,'0');//taxas do repasse Fim
//  aux := FloatToStr(valorFinal - valorEmpresa);//VALOR L�QUIDO IN�CIO
//  aux := RetiraCaracteresEspeciais(aux);
//  S := S+ fnCompletarCom(aux,14,'0');  //VALOR l�quidoFim

  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  //Fim da S�tima Linha

  //Come�o da Oitava Linha
  S := '';
  S := S+ fnCompletarCom('A9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo+1),6); //L902
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');

  //Fim da Oitava Linha
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,true,'.txt');


end;

procedure TFrmExportConciliacaoSpani.LayoultConciliacaoVNUNES(var d: TDateTime);
var
  Erro,hasDepartamento : Boolean;
  parcelas,count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  aux,cartao,mesvalor, S, valor,dec,endereco,valorTotal,MOVIMENTO_ID, nomeAdministradora, teste : String;
  ano,mes,dia : Integer;
  myHour, myMin, mySec, myMilli : Word;
  prefixo_nome_txt,nomeArquivo : String;
  dataIncial : TDateTime;
  data : TDate;
  valorFinal,valorLiquido, valorDesconto, valorEmpresa, valorRepasse, valorRepasseEmpresa : Double;
  dinheiroaux :Currency;
begin
  valorFinal := 0;
  valorEmpresa := 0;
  valorRepasse := 0;
  valorRepasseEmpresa :=0;
  ProgressBar1.Position := 0;
  lblStatus.Caption :=  'Gerando arquivo...';
  lblStatus.Refresh;
  Erro := False;
  contadorDeLinhasDoArquivo := 0;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  //hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMOV_ID_SITEF AS MOVIMENTO_ID');
  DMConexao.AdoQry.Open;
  MOVIMENTO_ID := DMConexao.AdoQry.Fields[0].Value;

  //PREFIXO FIXO PARA GRAVAR O NOME DO ARQUIVO
  prefixo_nome_txt := 'rpccrt_zar_';
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  nomeArquivo := prefixo_nome_txt + aux + '_'+ fnCompletarCom(MOVIMENTO_ID,6,'0');

  //Cabe�alho
  //variaveis fixas do cabecalho
  nomeAdministradora := 'PLANTAO CARD';

  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom('A0',2);//A0 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom('0001.1',6);//VVV.RR "0016d"
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  DecodeTime(Now,myHour, myMin, mySec, myMilli);
  S := S+ fnCompletarCom(IntToStr(myHour),02);
  S := S+ fnCompletarCom(IntToStr(myMin),02);
  S := S+ fnCompletarCom(IntToStr(mySec),02); // A04 - gera��o do arquivo
  S := S+ fnCompletarCom(MOVIMENTO_ID,6,'0');  //A05 ID DO MOVIMENTO
  S := S+ fnCompletarCom(nomeAdministradora,30,' ',True);
  S := S+ fnCompletarCom('1',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho

  //Segunda linha - Header do Lote de transa��es
  S := '';
  S := S+ fnCompletarCom('L0',2);//L0 - CODIGO DE REGISTRO =L0
  dataIncial := d;
  aux := ConverteDiaParaAmericano(dataIncial);
  S := S+ fnCompletarCom(aux,08); //AAAAMMDD
  S := S+ fnCompletarCom('RE',2);
  S := S+ fnCompletarCom('2',6,'0');
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim da Segunda Linha

  data :=   d;

  //Come�o da Terceira Linha
  //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - PREVIS�O
  //VENDAS OCORRIDAS NO DIA
  //COMENT�RIOS CV LAYOUT PLANT�O CARD 1.1

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;

  //SPANI
      DMConexao.AdoQry.SQL.Add('SELECT CC.NSUHOST, DATA, FORMAPAGTO_ID, CR.CGC, COMISSAO, DEBITO AS VALOR, ');
      DMConexao.AdoQry.SQL.Add('CC.CARTAO_ID, HISTORICO, DEBITO, AUTORIZACAO_ID, AGENCIA, CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('CC.CRED_ID, CC.EMPRES_ID, CA.CODCARTIMP, MOD_CART_ID, COD_BANCO, DATAVENDA, CC.OPERADOR');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN CREDENCIADOS CR ON CC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CA.CARTAO_ID = CC.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CA.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('WHERE CC.DATA BETWEEN '+QuotedStr(DateToStr(data))+' AND '+QuotedStr(DateToStr(data)+' 23:59:59')+' ');
      DMConexao.AdoQry.SQL.Add('AND AND CR.NOME LIKE'+QuotedStr('%V NUNES%')+' AND CC.DEBITO > 0');


      DMConexao.AdoQry.Open;
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - CNPJ in�cio
          aux := DMConexao.AdoQry.Fields[3].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV03 - NSU
          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[1].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04
          DecodeTime(DMConexao.AdoQry.Fields[17].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento ap�s 35 dias
          dataIncial := DMConexao.AdoQry.Fields[1].Value;
          if DayOfTheWeek(dataIncial) = 7 then begin
            dataIncial := dataIncial + 38;
          end else begin
            dataIncial := dataIncial + (7- DayOfTheWeek(dataIncial)) + 38 ;
          end;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '0';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorFinal := valorFinal+ DMConexao.AdoQry.Fields[8].Value;
          dinheiroaux := DMConexao.AdoQry.Fields[8].Value;
          aux := Format('%m',[dinheiroaux]);

          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[2].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);
          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[14].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[2].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[7].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 14 e CV 15 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[0].Value),12,'0');//CV15 - NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 - VALOR BRUTO PARCELA in�cio
            aux := DMConexao.AdoQry.Fields[8].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 - VALOR DESCONTO PARCELA In�cio
            valorDesconto := DMConexao.AdoQry.Fields[8].Value * (DMConexao.AdoQry.fields[4].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 - VALOR L�QUIDO PARCELA In�cio
            valorLiquido := DMConexao.AdoQry.Fields[8].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[15].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[15].Value,3,'0');
          end;
          //CV19 FIM
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[16].Value),3,'0');//CV20 C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[10].Value,6,'0');//CV21 AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[11].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22 CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[9].Value,12,'0');//CV23 AUTORIZA��O
          if(DMConexao.AdoQry.Fields[18].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[18].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[18].Value = 'TEF')or(DMConexao.AdoQry.Fields[18].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//CV24 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
      //Fim da Terceira Linha

      //Come�o da Quarta Linha
      //COMPROVANTE DE VENDA COM TIPO DE LAN�AMENTO - LIQUIDA��O
      //CONFIRMA��O DE REPASSE DAS TRANSA��ES
      //COMENT�RIOS CV COM BASE NO MODELO 1.1 PLANT�O CARD

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT CC.AUTORIZACAO_ID,CC.NSUHOST, CC.DEBITO, CC.DATA, CC.FORMAPAGTO_ID, CA.CODCARTIMP, ');
      DMConexao.AdoQry.SQL.Add('CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, CR.COMISSAO,  ');
      DMConexao.AdoQry.SQL.Add('PC.DATA_COMPENSACAO,CC.HISTORICO, BA.COD_BANCO, EM.MOD_CART_ID, CC.OPERADOR ');
      DMConexao.AdoQry.SQL.Add('FROM CONTACORRENTE CC INNER JOIN PAGAMENTO_CRED PC ON CC.PAGAMENTO_CRED_ID = PC.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON CR.CRED_ID = CC.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CARTOES CA ON CC.CARTAO_ID = CA.CARTAO_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON BA.CODIGO = CR.BANCO ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN EMPRESAS EM ON EM.EMPRES_ID = CC.EMPRES_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%V NUNES%') + '  ');
      DMConexao.AdoQry.SQL.Add('AND CC.DEBITO > 0 ');
      DMConexao.AdoQry.SQL.Add('AND CC.VALOR_CANCELADO <= 0 ');
      DMConexao.AdoQry.Open;


      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('CV',2);
          //CV02 - Cnpj in�cio
          aux := DMConexao.AdoQry.Fields[6].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //CV02 CNPJ fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV03 - NSU
          //In�cio CV04  - data da venda
          dataIncial :=  DMConexao.AdoQry.Fields[3].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S+ fnCompletarCom(aux,08);//CV04 FIM
          DecodeTime(DMConexao.AdoQry.Fields[3].Value, myHour, myMin, mySec, myMilli);
          S := S+ fnCompletarCom(IntToStr(myHour),02);
          S := S+ fnCompletarCom(IntToStr(myMin),02);
          S := S+ fnCompletarCom(IntToStr(mySec),02);//CV05  - hor�rio data venda
          //In�cio CV06  - Data de lan�amento liquida��o imediata
          dataIncial := DMConexao.AdoQry.Fields[10].Value;
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//CV06 FIM
          S := S + '1';//CV 07 - TIPO DE LAN�AMENTO
          S := S + 'V';//CV08 - TIPO DE CART�O - VOUCHER
          //In�cio CV09  - valor bruto
          valorRepasse := valorRepasse + StrToFloat(DMConexao.AdoQry.Fields[2].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[2].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //CV09 Fim
          //CV10 In�cio  - valor da comiss�o em R$
          if (DMConexao.AdoQry.Fields[4].value = 1) then begin
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorRepasseEmpresa := valorRepasseEmpresa + valorDesconto;
            dinheiroaux := valorDesconto;
            aux := Format('%m',[dinheiroaux]);
            aux := FormataDinheiro(aux);
            S := S+ fnCompletarCom(aux,11,'0');
          end else begin
            valorDesconto := 0;
            S := S+ fnCompletarCom('0',11,'0');//CV10 Fim
          end;
          //CV11 In�cio  - Valor l�quido
          valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
          valorLiquido := SimpleRoundTo(valorLiquido,-2);

          dinheiroaux := valorLiquido;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');//CV11 Fim
          //CV12 In�cio - N�MERO DO CART�O
          cartao := DMConexao.AdoQry.Fields[5].Value;
          cartao := MascaraCartao(cartao);
          S := S+ fnCompletarCom(cartao,19, '0');
          //CV12 Fim
          //CV13 e CV14 In�cio
          parcelas :=   DMConexao.AdoQry.Fields[4].value;
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',4,'0');
          end else begin
            aux := DMConexao.AdoQry.Fields[11].Value;
            S := S + fnCompletarCom(aux[10],2,'0'); //CV13 - N�MERO DA PARCELA
            S:= S+ fnCompletarCom(IntToStr(parcelas),2);  //CV14 - N�MERO TOTAL DE PARCELAS
          end;//CV 13 e CV 14 Fim
          S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[1].Value),12,'0');//CV15 NSU PARCELA
          if (parcelas = 1) then begin
           S := S+ fnCompletarCom('0',33);
          end else begin
            //CV16 in�cio VALOR BRUTO PARCELA
            aux := DMConexao.AdoQry.Fields[2].Value;
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV16 Fim
            //CV17 In�cio  - VALOR DE DESCONTO DA PARCELA
            valorDesconto := DMConexao.AdoQry.Fields[2].Value * (DMConexao.AdoQry.fields[9].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            aux := FloatToStr(valorDesconto);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV17 Fim
            //CV18 In�cio - VALOR L�QUIDO DA PARCELA
            valorLiquido := DMConexao.AdoQry.Fields[2].Value - valorDesconto;
            valorLiquido := SimpleRoundTo(valorLiquido,-2);
            aux := FloatToStr(valorLiquido);
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,11,'0');//CV18 Fim
          end;
          //CV 19 - Adicionar Produto
          if (DMConexao.AdoQry.Fields[13].Value = '7') then begin
              S := S+ fnCompletarCom('2',3,'0');
          end else begin
              S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[13].Value,3,'0');
          end;
          //In�cio CV20 - C�DIGO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[12].Value,3,'0');
         //CV20 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,6,'0');//CV21   - AG�NCIA
          aux:=   DMConexao.AdoQry.Fields[8].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//CV22  - CONTA
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//CV23  - AUTORIZA��O
          if(DMConexao.AdoQry.Fields[14].Value = 'AUTOR.SITE') then begin
            S := S + '3';
          end else begin
            if (DMConexao.AdoQry.Fields[14].Value = 'POS.DB') then begin
              S := S + '2';
            end else begin
              if((DMConexao.AdoQry.Fields[14].Value = 'TEF')or(DMConexao.AdoQry.Fields[14].Value = 'GETNET')) then begin
                S := S + '1';
              end else begin
                S := S + '4';
              end;
            end;
          end;    //CV 24 - MEIO DE CAPTURA
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;

      //Final da Quarta Linha


      //Come�o da Quinta Linha - AJUSTES A D�BITO OU AJUSTES A CR�DITO

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT PC.DATA_AUTORIZACAO_REPASSE,BA.COD_BANCO, CR.CGC, CR.AGENCIA, CR.CONTACORRENTE, ');
      DMConexao.AdoQry.SQL.Add('TR.VALOR, TR.HISTORICO, TR.TAXA_ID, TA.DESCRICAO   ');
      DMConexao.AdoQry.SQL.Add('FROM PAGAMENTO_CRED PC INNER JOIN TAXAS_REPASSE TR ON PC.PAGAMENTO_CRED_ID = TR.PAGAMENTO_CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN CREDENCIADOS CR ON PC.CRED_ID = CR.CRED_ID ');
      DMConexao.AdoQry.SQL.Add('INNER JOIN BANCOS BA ON CR.BANCO = BA.CODIGO ');
      DMConexao.AdoQry.SQL.Add('LEFT JOIN TAXAS TA ON TA.TAXA_ID = TR.TAXA_ID ');
      DMConexao.AdoQry.SQL.Add('WHERE PC.DATA_COMPENSACAO = '+QuotedStr(DateToStr(data))+' ');
      DMConexao.AdoQry.SQL.Add('AND CR.NOME LIKE ' + QuotedStr('%V NUNES%') + '  ');
      DMConexao.AdoQry.Open;
    
      //COMENT�RIOS AJ COM BASE NO MODELO 1.1 PLANT�O CARD
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          S := '';
          S := S+ fnCompletarCom('AJ',2);
          //AJ02 - CNPJ
          aux := DMConexao.AdoQry.Fields[2].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,15,'0'); //AJ02 - CNPJ fim
          //AJ03 - DATA DE LAN�AMENTO
          aux := ConverteDiaParaAmericano(dataIncial);
          S := S + fnCompletarCom(aux,8);//AJ03 FIM
          S := S + '2';//AJ04 - TIPO DE AJUSTE
          //In�cio AJ06 - VALOR BRUTO  TODO - SOMAR EM VARI�VEL QUE VAI RETIRAR DO REPASSE
          valorRepasseEmpresa := valorRepasseEmpresa + StrToFloat(DMConexao.AdoQry.Fields[5].Value);
          dinheiroaux := DMConexao.AdoQry.Fields[5].Value;
          aux := Format('%m',[dinheiroaux]);
          aux := FormataDinheiro(aux);
          S := S+ fnCompletarCom(aux,11,'0');  //AJ06 Fim
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[7].Value,3,'0');  //AJ07 TAXA_ID
          //AJ08 - DESCRI��O DA TAXA
          //SE EXISTE DESCRI��O NA TABELA TAXAS UTILIZA, SEN�O UTILIZA O HIST�RICO DA TABELA TAXAS_REPASSE
          if(DMConexao.AdoQry.Fields[7].Value = '0') then begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,30,' ', True);
          end else begin
               S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[8].Value,30,' ',True);
          end;//AJ08 - FIM
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[1].Value,3,'0'); //In�cio AJ09 - C�DIGO DO BANCO
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[3].Value,6,'0');//AJ10 - AG�NCIA
          //AJ11 - CONTA
          aux:=   DMConexao.AdoQry.Fields[4].Value;
          aux := RetiraCaracteresEspeciais(aux);
          S := S+ fnCompletarCom(aux,11,' ',True);//AJ11 - FIM
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//AJ12 - NSEQ
          aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
          SalvarArquivo(S,nomeArquivo,false,'.txt');
          DMConexao.AdoQry.Next;
        end;
      end;
    
      //Final da Quinta Linha
      //Come�o da Sexta Linha
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
    
      //Spani
      DMConexao.AdoQry.SQL.Add('SELECT NSUHOST,DATA,FORMAPAGTO_ID,cgc,debito, COMISSAO, cc.AUTORIZACAO_ID FROM CONTACORRENTE cc inner join CREDENCIADOS c on c.CRED_ID = cc.CRED_ID');
      DMConexao.AdoQry.SQL.Add('WHERE DATA between ' + DateToStr(d) + ' and ' + QuotedStr(DateToStr(d) + ' 23:59:59') + '  and CANCELADA = ' + QuotedStr('S'));
      DMConexao.AdoQry.SQL.Add('AND cc.CRED_ID IN (select cred_id from CREDENCIADOS where nome like ' + QuotedStr('%V NUNES%') + 'AND DEBITO > 0 AND VALOR_CANCELADO > 0 )');
      DMConexao.AdoQry.Open;
    
      while not DMConexao.AdoQry.Eof do begin
        if not (DMConexao.AdoQry.IsEmpty)  then begin
          data :=   DMConexao.AdoQry.Fields[1].Value;
          if (DateToStr(d) = DateToStr(data)) then begin
            //Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            valorDesconto := DMConexao.AdoQry.Fields[4].Value * (DMConexao.AdoQry.fields[5].Value/100);
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorEmpresa := valorEmpresa - valorDesconto;
            valorDesconto := DMConexao.AdoQry.Fields[4].Value;
            valorDesconto := SimpleRoundTo(valorDesconto,-2);
            valorFinal := valorFinal - StrToFloat(DMConexao.AdoQry.Fields[4].Value);
            //Fim Retira o valor do cancelamento do total bruto e do total da taxa de administra��o
            S := '';
            S := S+ fnCompletarCom('CC',2);
            aux := DMConexao.AdoQry.Fields[3].Value; //CNPJ IN�CIO
            aux := RetiraCaracteresEspeciais(aux);
            S := S+ fnCompletarCom(aux,15,'0');//CNPJ FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            if(DMConexao.AdoQry.Fields[2].Value = 1) then begin//N�mero da parcela IN�CIO
              S := S+ fnCompletarCom('0',2,'0');
            end else begin
              S := S+ fnCompletarCom(IntToStr(DMConexao.AdoQry.Fields[2].Value),2,'0');
            end;//N�mero da parcela FIM
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,12,'0');//NSU
            dataIncial :=  DMConexao.AdoQry.Fields[1].Value;//Convers�o de data IN�CIO
            aux := ConverteDiaParaAmericano(dataIncial);
            S := S+ fnCompletarCom(aux,08);//Convers�o de data FIM
            DecodeTime(DMConexao.AdoQry.Fields[1].Value, myHour, myMin, mySec, myMilli); //Convers�o de hora IN�CIO
            S := S+ fnCompletarCom(IntToStr(myHour),02);
            S := S+ fnCompletarCom(IntToStr(myMin),02);
            S := S+ fnCompletarCom(IntToStr(mySec),02);//Convers�o de hora FIM
            contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
            S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[6].Value,12,'0'); //C�DIGO DE AUTORIZA��O
            S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
            aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
            SalvarArquivo(S,nomeArquivo,false,'.txt');
          end;
          DMConexao.AdoQry.Next;
        end;
      end;
  //Fim da Sexta Linha

  //Come�o da S�tima Linha
  S := '';
  S := S+ fnCompletarCom('L9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo-2),6,'0');//L902
  dinheiroaux := valorFinal;//L903 IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //L903 Fim
  dinheiroaux := valorEmpresa;//VALOR TOTAL DE DESCONTO NO POS IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');  //VALOR TOTAL DE DESCONTO NO POS Fim
  dinheiroaux := valorRepasse;//valor repasse bruto
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//valor repasse bruto Fim
  dinheiroaux := valorRepasseEmpresa;//taxas do repasse IN�CIO
  aux := Format('%m',[dinheiroaux]);
  aux := FormataDinheiro(aux);
  S := S+ fnCompletarCom(aux,14,'0');//taxas do repasse Fim
//  aux := FloatToStr(valorFinal - valorEmpresa);//VALOR L�QUIDO IN�CIO
//  aux := RetiraCaracteresEspeciais(aux);
//  S := S+ fnCompletarCom(aux,14,'0');  //VALOR l�quidoFim

  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');
  //Fim da S�tima Linha

  //Come�o da Oitava Linha
  S := '';
  S := S+ fnCompletarCom('A9',2);
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo+1),6); //L902
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');//NSEQ
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,false,'.txt');

  //Fim da Oitava Linha
  aux :=  stringreplace(DateToStr(d), '/', '-',[rfReplaceAll]);
  SalvarArquivo(S,nomeArquivo,true,'.txt');


end;







end.


