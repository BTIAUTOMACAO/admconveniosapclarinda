unit ThreadLoadCadastro;

interface

uses
  Classes;

type
  CarregaQuery = class(TThread)
  public
  constructor Create(Param_Qry: String);
  private
    { Private declarations }
  protected

    //tSQL: String;
    procedure Execute; override;
    procedure RodaQuery();
  end;

  var Qry: String;
implementation

uses DM;

constructor CarregaQuery.Create(Param_Qry : String);
begin
  inherited Create(False);
  FreeOnTerminate:=True;
  Qry := Param_Qry;
end;


procedure CarregaQuery.RodaQuery();
begin
  try
     with DMConexao.AdoQry do
     begin
       DMConexao.AdoQry.Close;
       DMConexao.AdoQry.SQL.Clear;
       DMConexao.AdoQry.SQL.Add(Qry);
       DMConexao.AdoQry.Open;
     end;
  except

end;

end;

procedure CarregaQuery.Execute;
begin
  { Place thread code here }
  RodaQuery();

end;


end.
