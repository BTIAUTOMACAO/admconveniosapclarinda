inherited FCadManutTaxaFixaPadraoBemEstar: TFCadManutTaxaFixaPadraoBemEstar
  Left = 506
  Top = 101
  Caption = 'Manuten'#231#227'o de Taxas de Mensalidade Padr'#227'o - Bem Estar'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
      end
      inherited DBGrid1: TJvDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'TAXA_ID'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FAIXA_INICIO'
            Title.Caption = 'Inicio'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FAIXA_FIM'
            Title.Caption = 'Fim'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAXA_VALOR'
            Title.Caption = 'Valor'
            Visible = True
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel3: TPanel
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 399
          Height = 183
          Align = alCustom
          Caption = 'Manuten'#231#227'o'
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 15
            Width = 14
            Height = 13
            Caption = ' ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 8
            Top = 72
            Width = 144
            Height = 13
            Caption = 'Faixa de Qtde de cart'#245'es'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 8
            Top = 88
            Width = 27
            Height = 13
            Caption = 'Inicial'
          end
          object Label6: TLabel
            Left = 152
            Top = 88
            Width = 22
            Height = 13
            Caption = 'Final'
          end
          object Label7: TLabel
            Left = 8
            Top = 136
            Width = 41
            Height = 13
            Caption = 'Valor R$'
          end
          object DBEdit1: TDBEdit
            Left = 10
            Top = 30
            Width = 65
            Height = 21
            Hint = 'C'#243'digo do taxa'
            TabStop = False
            Color = clBtnFace
            DataField = 'TAXA_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 8
            Top = 104
            Width = 121
            Height = 21
            DataField = 'FAIXA_INICIO'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 152
            Top = 104
            Width = 121
            Height = 21
            DataField = 'FAIXA_FIM'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 8
            Top = 152
            Width = 121
            Height = 21
            DataField = 'TAXA_VALOR'
            DataSource = DSCadastro
            TabOrder = 3
          end
        end
        object GroupBox2: TGroupBox
          Left = 416
          Top = 2
          Width = 337
          Height = 183
          Align = alCustom
          Caption = 'Cadastro Atual'
          TabOrder = 1
          object JvDBGrid1: TJvDBGrid
            Left = 8
            Top = 24
            Width = 321
            Height = 120
            DataSource = DSCadastro
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 17
            TitleRowHeight = 17
            Columns = <
              item
                Expanded = False
                FieldName = 'TAXA_ID'
                Title.Caption = 'ID'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FAIXA_INICIO'
                Title.Caption = 'Inicial'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FAIXA_FIM'
                Title.Caption = 'Fim'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TAXA_VALOR'
                Title.Caption = 'Valor'
                Visible = True
              end>
          end
        end
      end
    end
  end
  inherited DSCadastro: TDataSource
    Left = 316
    Top = 312
  end
  inherited QHistorico: TADOQuery
    Left = 372
    Top = 281
  end
  inherited DSHistorico: TDataSource
    Left = 372
    Top = 313
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'SELECT  *  FROM EMP_TAXAS_BEM_ESTAR_PADRAO WHERE TAXA_ID = 0')
    Left = 316
    Top = 281
    object QCadastroTAXA_ID: TIntegerField
      FieldName = 'TAXA_ID'
    end
    object QCadastroFAIXA_INICIO: TIntegerField
      FieldName = 'FAIXA_INICIO'
    end
    object QCadastroFAIXA_FIM: TIntegerField
      FieldName = 'FAIXA_FIM'
    end
    object QCadastroTAXA_VALOR: TBCDField
      FieldName = 'TAXA_VALOR'
      Precision = 6
      Size = 2
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
  end
end
