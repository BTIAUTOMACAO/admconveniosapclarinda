unit UmanutencaoEmpConv2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, DB, ADODB, ComCtrls;

type
  TFManutencaoEmpConv2 = class(TForm)
    Panel1: TPanel;
    tExcel: TADOTable;
    Panel2: TPanel;
    btnImportarLimiteConv: TBitBtn;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    txtConvIDOrigem: TEdit;
    txtConvIDDestino: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    btnTransferir: TBitBtn;
    ConvOrigem: TADOQuery;
    ConvDestino: TADOQuery;
    ButAjuda: TSpeedButton;
    RichEdit1: TRichEdit;
    procedure btnImportarLimiteConvClick(Sender: TObject);
    procedure btnTransferirClick(Sender: TObject);
    procedure ButAjudaClick(Sender: TObject);

  private
    { Private declarations }
    //function fnCompletarCom(Valor : String ; TamanhoMaximo : Integer ; Caracter : String = '0'; Direita : Boolean = False) : String;
    //function fnAddCaracter(Caracters, Valor : String; const Posicao : Integer; const Direita : Boolean = False ) : String;

  public
    { Public declarations }
end;

var
  FManutencaoEmpConv2: TFManutencaoEmpConv2;

implementation

uses cartao_util, UValidacao, DM, URotinasTexto, UChangeLog;

{$R *.dfm}

procedure TFManutencaoEmpConv2.btnImportarLimiteConvClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId,sql : String;
path,strSql, id_empres, conv_id, chapa, cartao_id, empres_id_novo,empres_id : String;
erro: Boolean;
begin
  inherited;
 try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
     OD.Free;
    tExcel.Active := False;
    tExcel.ConnectionString := path;
    tExcel.TableName := 'conv$';
    tExcel.Active := True;
    try
      tExcel.Open;
    except
      MsgErro('UmanutencaoEmpConv2_80 - N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "conv" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('UmanutencaoEmpConv2_84 - Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    //btnImportar.Caption := 'Importar';
    Abort;
  end;
  //while not QManutencaoLimiteConv.Eof do
  //begin
      //if QManutencaoLimiteConvempres_id.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
      //begin

        Screen.Cursor := crHourGlass;
        while not tExcel.Eof do begin

          if tExcel.FieldByName('CHAPA').AsString <> '' then begin //conv_id - Fields[0]
              //MsgInf('O campo Chapa da tabela n�o pode estar vazio');
              //Abort;
            //end else begin
              chapa := tExcel.FieldByName('CHAPA').AsString;
            //end;

            //if tExcel.FieldByName('CARTAO_ID').AsString = '' then begin // cartao_id
            //  MsgInf('O campo CART�O_ID da tabela n�o pode estar vazio');
            //   Abort;
            //end else begin
            //  cartao_id := tExcel.FieldByName('CARTAO_ID').AsString;
            //end;

            if tExcel.FieldByName('EMPRESA ID').AsString = '' then begin //empres_id da atual empresa
              MsgInf('UmanutencaoEmpConv2_113 - O campo EMPRES_ID da tabela n�o pode estar vazio');
              MsgInf('Importa��o cancelada');
              Abort;
            end else begin
                empres_id := tExcel.FieldByName('EMPRESA ID').AsString;
            end;

            if tExcel.FieldByName('NOVA EMPRESA ID').AsString = '' then begin //empres_id da nova empresa
              MsgInf('UmanutencaoEmpConv2_120 - O campo EMPRES_ID_NOVO da tabela n�o pode estar vazio');
              MsgInf('Importa��o cancelada');
              Abort;
            end else begin
              empres_id_novo := tExcel.FieldByName('NOVA EMPRESA ID').AsString;
            end;

            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Text := 'select CART.CONV_ID, CART.CARTAO_ID from CONVENIADOS CONV inner join CARTOES CART on (CONV.CONV_ID = CART.CONV_ID) where CONV.CHAPA = '+ QuotedStr(chapa) +' AND CONV.EMPRES_ID = '+ QuotedStr(empres_id) +' ';
            //DMConexao.AdoQry.SQL.Add('select CART.CONV_ID, CART.CARTAO_ID from CONVENIADOS CONV inner join CARTOES CART on (CONV.CONV_ID = CART.CONV_ID) where CONV.CHAPA = '+ chapa +' AND CONV.EMPRES_ID = '+ empres_id +' ');
            DMConexao.AdoQry.Open;

            if ((DMConexao.AdoQry.FieldByName('CONV_ID').AsString = '') OR (DMConexao.AdoQry.FieldByName('CARTAO_ID').AsString = '')) then begin
                MsgInf('UmanutencaoEmpConv2_134 - Verifique se o conveniado '+chapa+' esta cadastrado na empresa '+empres_id+' ');
                MsgInf('Importa��o cancelada');
                Abort;
            end else begin
                conv_id   := DMConexao.AdoQry.FieldByName('CONV_ID').AsString;
                cartao_id := DMConexao.AdoQry.FieldByName('CARTAO_ID').AsString;
            end;

            //cartao_id := DMConexao.AdoQry.FieldByName('CARTAO_ID').AsString;

            sql:= 'UPDATE TRANSACOES SET EMPRES_ID = ' + empres_id_novo +' WHERE CARTAO_ID = ' + cartao_id;
            DMConexao.ExecuteSql(sql);

            sql:= 'UPDATE CONTACORRENTE SET EMPRES_ID = ' + empres_id_novo +' WHERE CARTAO_ID = ' + cartao_id;
            DMConexao.ExecuteSql(sql);

            sql:= 'UPDATE CARTOES SET EMPRES_ID = ' + empres_id_novo +' WHERE CARTAO_ID = ' + cartao_id;
            DMConexao.ExecuteSql(sql);

            sql:= 'UPDATE CONVENIADOS SET EMPRES_ID = ' + empres_id_novo +' WHERE CONV_ID = ' + conv_id;
            DMConexao.ExecuteSql(sql);

            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT CGC, NOME FROM empresas where EMPRES_ID = '+empres_id_novo);
            DMConexao.AdoQry.Open;
            //sql := 'UPDATE [SAP].[SBO_BELLA_GOLD].[DBO].[@CONVENIADO] set U_CNPJ = '+ QuotedStr(DMConexao.AdoQry.Fields[0].AsString) +', U_CNPJ_EM = '+ QuotedStr(DMConexao.AdoQry.Fields[0].AsString) +', U_CodEmpresa = '+ QuotedStr(empres_id_novo) +', U_Empresa = '+ QuotedStr(DMConexao.AdoQry.Fields[1].AsString) +' WHERE u_CONVID = '+ QuotedStr(conv_id);
            DMConexao.ExecuteSql(sql);


            id_empres:= URotinasTexto.fnCompletarCom(empres_id_novo,5,'0');


            sql := 'UPDATE [SAP].[SBO_BELLA_GOLD].[DBO].[@CARTOES] set U_Empresa_id = ''CEM'+id_empres+' WHERE u_CONV_ID = '+ QuotedStr(tExcel.Fields[0].AsString) +' ';

            sql:= 'UPDATE CONV_BEMESTAR SET EMPRES_ID = ' + empres_id_novo +' WHERE CONV_ID = ' + conv_id;
            DMConexao.ExecuteSql(sql);

            //if tExcel.Fields[4].AsString = '' then begin
              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('select GRUPO_CONV_EMP_ID from GRUPO_CONV_EMP where EMPRES_ID = '+ empres_id_novo);
              DMConexao.AdoQry.Open;
              sql:= 'update CONVENIADOS set GRUPO_CONV_EMP = ' + DMConexao.AdoQry.Fields[0].AsString +' where conv_id = ' + conv_id;
              DMConexao.ExecuteSql(sql);

          end
          else
          begin
              MsgInf('UmanutencaoEmpConv2_185 - O campo CHAPA da tabela n�o pode estar vazio');
              MsgInf('Importa��o Finalizada');
              Abort;
          end;
          tExcel.Next;
        end;

        Screen.Cursor := crDefault;
        if tExcel.State in [dsEdit, dsInsert] then
           tExcel.Close;

        msginf('Atualiza��o Finalizada!');
end;

          
procedure TFManutencaoEmpConv2.btnTransferirClick(Sender: TObject);
begin
  ConvOrigem.Close;
  ConvOrigem.SQL.Text := '';
  ConvOrigem.SQL.Text := 'select cartao_id,conveniados.conv_id,cartoes.empres_id from CARTOES inner join CONVENIADOS  '+
  'on CONVENIADOS.conv_id = cartoes.CONV_ID where CONVENIADOS.CONV_ID = '+txtConvIDOrigem.Text+ '';
  ConvOrigem.Open;

  ConvDestino.Close;
  ConvDestino.SQL.Text := '';
  ConvDestino.SQL.Text := 'select cartao_id,conveniados.conv_id,cartoes.empres_id from CARTOES inner join CONVENIADOS  '+
  'on CONVENIADOS.conv_id = cartoes.CONV_ID where CONVENIADOS.CONV_ID = '+txtConvIDDestino.Text+ '';
  ConvDestino.Open;



end;

procedure TFManutencaoEmpConv2.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o para limpar a senha';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

end.
