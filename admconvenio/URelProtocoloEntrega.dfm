inherited FRelProtocoloEntrega: TFRelProtocoloEntrega
  Left = 231
  Top = 92
  Caption = 'Protocolo de Entrega'
  ClientHeight = 540
  ClientWidth = 928
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox [0]
    Left = 0
    Top = 23
    Width = 928
    Height = 122
    Align = alTop
    TabOrder = 1
    object ButListaEmp: TButton
      Left = 896
      Top = 34
      Width = 73
      Height = 50
      Caption = 'Busca (F5)'
      TabOrder = 2
      OnClick = ButListaEmpClick
    end
    object GroupBox2: TGroupBox
      Left = 4
      Top = 14
      Width = 239
      Height = 99
      Caption = 'Selecione o per'#237'odo de emiss'#227'o do lote'
      TabOrder = 0
      object datafin: TJvDateEdit
        Left = 124
        Top = 29
        Width = 107
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 1
      end
      object DataIni: TJvDateEdit
        Left = 4
        Top = 28
        Width = 107
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object grpOrdena: TGroupBox
      Left = 586
      Top = 14
      Width = 241
      Height = 43
      Caption = 'Ordenar Por...'
      TabOrder = 1
      object chkOrdenaSetor: TCheckBox
        Left = 8
        Top = 16
        Width = 129
        Height = 17
        Caption = 'Setor'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
    end
    object GroupBox4: TGroupBox
      Left = 586
      Top = 65
      Width = 241
      Height = 49
      Caption = 'Modelo do Cart'#227'o'
      TabOrder = 3
      object lkpModeloCartao: TJvDBLookupCombo
        Left = 8
        Top = 16
        Width = 225
        Height = 21
        LookupField = 'MOD_CART_ID'
        LookupDisplay = 'DESCRICAO'
        LookupSource = DSModelos
        TabOrder = 0
      end
    end
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 145
    Width = 928
    Height = 395
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = '&Lotes'
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 920
        Height = 321
        Align = alClient
        DataSource = DSLotesCartao
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        OnKeyDown = JvDBGrid1KeyDown
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'seq_lote'
            Title.Caption = 'Lote ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'Empres ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'fantasia'
            Title.Caption = 'Nome da Empresa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'setor'
            Title.Caption = 'Setor'
            Width = 224
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'data'
            Title.Caption = 'Data do Lote'
            Width = 158
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome_arq'
            Title.Caption = 'Cam. do Arquivo'
            Width = 179
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Title.Caption = 'Cidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cep'
            Title.Caption = 'CEP'
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 321
        Width = 920
        Height = 46
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 1
        DesignSize = (
          916
          42)
        object ButMarcaDesmEmp: TButton
          Left = 7
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F12)'
          TabOrder = 2
          OnClick = ButMarcaDesmEmpClick
        end
        object ButMarcaTodasEmp: TButton
          Left = 115
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F6)'
          TabOrder = 3
          OnClick = ButMarcaTodasEmpClick
        end
        object ButDesmarcaTodosEmp: TButton
          Left = 224
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F7)'
          TabOrder = 4
          OnClick = ButDesmarcaTodosEmpClick
        end
        object BitBtn2: TBitBtn
          Left = 559
          Top = 8
          Width = 92
          Height = 29
          Caption = '&Visualizar'
          TabOrder = 0
          OnClick = BitBtn2Click
          Glyph.Data = {
            E6040000424DE604000000000000360000002800000014000000140000000100
            180000000000B0040000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFC6CED2A8ADAFB0B1B2A8A6A6868585919394A69698987879A29A9AB4B5B5
            B2B3B4B2B7BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B8BAAAAB
            ACC2C2C2E7E6E6DADADAA0A1A19994945B4A494A4141606060858585AEAEADC2
            C1C1ACADAEB1B5B7FFFFFFFFFFFFFFFFFFBAC1C5A6A7A9B8B8B8E5E5E5F1F1F1
            EAEAEACECECE9999999696965858583939394545454F4F4F6E6E6E969595BAB9
            B9B7B7B7A1A2A4FFFFFFFFFFFFAEB0B1DBDBDAFAFAFAF3F3F3EDEDEDCDCDCDA2
            A2A27D7D7D8F8F8FA6A6A6A2A2A28A8A8A6F6F6F6D6D6D5C5C5C7171719A9B9B
            ACB2B4FFFFFFFFFFFFC3C3C2FFFFFFF2F2F2D0D0D09595959999999E9E9E7878
            787070707171718080809C9C9CAEAEAEA7A7A79090908F8B8DB1B0B2B7BEC1FF
            FFFFFFFFFFAFAFAED5D5D5939393959595C0C0C0C3C3C3C8C8C8BFBFBFA2A2A2
            9191918787877777776F6F6F828282A3A4A36AA27B7FA08BBBBDC5FFFFFFFFFF
            FF7F7F7E939393CDCDCDD7D7D7C7C7C7C1C1C1DADADAC5C5C5CDCDCDC9C9C9C2
            C2C2BDBDBDB5B5B59B9B9B7B7B7B757173848385B6BDC0FFFFFFFFFFFF979695
            F0F0F0D2D2D2C6C6C6C2C2C2DBDBDBBEBEBEC7C7C7C8C8C8B8B8B8B0B0B0BDBD
            BDBDBDBDC1C1C1CFCFCFC7C7C7A3A4A4B4BABEFFFFFFFFFFFFB3B7B8CBCBCBC6
            C6C6C3C3C3CCCCCCB8B8B8DDDDDDF5F5F5F2F2F2E9E9E9DFDFDFD4D4D4BFBFBF
            B1B1B1B1B1B1B1B0B0BBBDBEC1C9CDFFFFFFFFFFFFC4CCCFBDC2C5A5A7A8A8A8
            A8C3C4C4B5B7B8B0B1B1D1D1D1E0E0E0E1E1E1E6E6E6E9EAEAE9E9E9DFDFDFC0
            BFBF9D9E9EBBC1C5CBD3D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFFFE
            CEC7C1A9ADB0A1A5AA9EA1A4A6A8AAB6B7B9C2B6B6C1B7B7B3B4B4A7A9AABBC2
            C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1AFB0DEB094FED5A5F4
            CBA2EECAA7ECD2B7E3D3C2D6CBC1AB8D8DAAA5A7BEC6CAFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8A8A8E3AC86FFD2A1FFCE9EFFCF
            9FFFD0A0FFD1A2F0C09BAD8D8DCAD5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFB49A95FDD5ADFFD7B0FFD6B0FFD6B0FFD6B0
            FFDDB4C39A8DAF9495FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFB79188FFECD0FFE3C9FFE3C9FFE3C9FFE4CAFDE3C9B0
            8B89BEBCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFB2A7AADABBAFFFEFDBFFEBD8FFEBD8FFEBD8FFF2DEDCBFB4AA8383FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA
            A2A3FFFFF8FFFFF8FFFFF8FFFFF8FFFFF9FFFFFEC4A5A1AB9394FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA89799B99797CBB0
            B0CAB0B0CAB0B0CAB0B0CAB1B0C9ADACB59999C2C2C6FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C1C5BDBABDBBB8BBBBB8BB
            BBB8BBBBB8BBBBB8BBBBB7BAC3C5C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF}
        end
        object bntGerarPDF: TBitBtn
          Left = 560
          Top = 8
          Width = 92
          Height = 29
          Anchors = [akTop, akRight]
          Caption = '&Gerar PDF'
          TabOrder = 1
          OnClick = bntGerarPDFClick
          Glyph.Data = {
            F6060000424DF606000000000000360000002800000018000000180000000100
            180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
            FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
            FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
            9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
            FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
            FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
            E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
            B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
            FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
            FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
            F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
            F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
            F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
            F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
            F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
            EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
            F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
            EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
            EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
            A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
            00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
            3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
            99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
            85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
            AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
            FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
            03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
            3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
            FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
            1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
            BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        end
      end
    end
  end
  inherited Panel6: TPanel
    Width = 928
    inherited panTitulo: TPanel
      Left = 9
    end
  end
  object GroupBox3: TGroupBox [3]
    Left = 248
    Top = 38
    Width = 313
    Height = 99
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 10
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object DBEmpresa: TJvDBLookupCombo
      Left = 8
      Top = 51
      Width = 297
      Height = 21
      LookupField = 'empres_id'
      LookupDisplay = 'nome'
      LookupSource = dsEmpCombo
      TabOrder = 1
    end
    object EdEmp_ID: TEdit
      Left = 8
      Top = 28
      Width = 89
      Height = 21
      TabOrder = 0
      OnChange = EdEmp_IDChange
      OnKeyPress = EdEmp_IDKeyPress
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 212
    Top = 152
  end
  object DSLotesCartao: TDataSource
    DataSet = MLotes
    Left = 64
    Top = 313
  end
  object MLotes: TJvMemoryData
    FieldDefs = <
      item
        Name = 'seq_lote'
        DataType = ftInteger
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end
      item
        Name = 'data'
        DataType = ftDateTime
      end
      item
        Name = 'nome_arq'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'fantasia'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'setor'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'cep'
        DataType = ftString
        Size = 20
      end>
    Left = 64
    Top = 344
    object MLotesseq_lote: TIntegerField
      FieldName = 'seq_lote'
    end
    object MLotesempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object MLotesmarcado: TBooleanField
      FieldName = 'marcado'
    end
    object MLotesdata: TDateTimeField
      FieldName = 'data'
    end
    object MLotesnome_arq: TStringField
      FieldName = 'nome_arq'
      Size = 80
    end
    object MLotesfantasia: TStringField
      FieldName = 'fantasia'
      Size = 50
    end
    object MLotessetor: TStringField
      FieldName = 'setor'
      Size = 50
    end
    object MLotesnome: TStringField
      FieldName = 'nome'
      Size = 50
    end
    object MLotescep: TStringField
      FieldName = 'cep'
    end
  end
  object QLotesCartao: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cld.seq_lote, FORMAT(data,'#39'dd/MM/yyyy'#39') '
      'as DATA, nome_arq,hora, e.fantasia,e.EMPRES_ID,'
      'EMP_DPTOS.DESCRICAO, ci.nome, e.cep '
      
        'FROM cartoes_lotes cl, empresas e, cartoes_lotes_detalhe cld, EM' +
        'P_DPTOS, cidades ci'
      'WHERE cld.empres_id =  e.empres_id and '
      'cld.SEQ_LOTE = cl.SEQ_LOTE and'
      'ci.cid_id = e.cidade and'
      'EMP_DPTOS.DEPT_ID = cl.setor_id and'
      'DATA  BETWEEN '#39'15/10/2014'#39'AND '#39'15/10/2014'#39' '
      'and e.tipo_credito <= 1 '
      
        'group by e.empres_id,cld.SEQ_LOTE,cl.data,cl.hora,cl.NOME_ARQ,e.' +
        'FANTASIA,'
      'emp_dptos.DESCRICAO, ci.nome, e.cep')
    Left = 64
    Top = 280
    object QLotesCartaoseq_lote: TIntegerField
      FieldName = 'seq_lote'
    end
    object QLotesCartaoDATA: TWideStringField
      FieldName = 'DATA'
      ReadOnly = True
      Size = 4000
    end
    object QLotesCartaonome_arq: TStringField
      FieldName = 'nome_arq'
      Size = 40
    end
    object QLotesCartaohora: TWideStringField
      FieldName = 'hora'
      Size = 16
    end
    object QLotesCartaofantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
    object QLotesCartaoEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QLotesCartaoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 150
    end
    object QLotesCartaonome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QLotesCartaocep: TStringField
      FieldName = 'cep'
      Size = 9
    end
  end
  object frxProtocolo: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41919.476905034700000000
    ReportOptions.LastChange = 42674.388247870370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <Cartao."empres_fornece_cartao"> = '#39'DROGABELLA'#39' THEN'
      '       begin                 '
      '         Picture1.visible := TRUE;'
      '         Picture2.visible := FALSE;'
      '       END;                            '
      '       if <Cartao."empres_fornece_cartao"> = '#39'PLANT'#195'O CARD'#39' THEN'
      '           BEGIN                        '
      '           Picture2.visible := TRUE;'
      '           Picture1.visible := FALSE;'
      '           END;                     '
      'end;'
      ''
      'procedure Page1OnBeforePrint(Sender: TfrxComponent);'
      'var memo : TfrxMemoView;                           '
      'begin'
      '  if <hasSetor> THEN'
      '  BEGIN'
      
        '       GroupHeader1.Condition := '#39'Cartao."setor_id"'#39';           ' +
        '   '
      '  END'
      '  ELSE          '
      '  BEGIN'
      
        '       GroupHeader1.Condition := '#39'Cartao."empres_id"'#39';          ' +
        '   '
      '  END  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnBeforePrint = frxProtocoloBeforePrint
    Left = 656
    Top = 256
    Datasets = <
      item
        DataSet = dbCartao
        DataSetName = 'Cartao'
      end
      item
        DataSet = dbEmpresas
        DataSetName = 'Empresas'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'hasSetor'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object PageHeader1: TfrxPageHeader
        Height = 105.826840000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Memo9: TfrxMemoView
          Align = baWidth
          Left = 173.858275040000000000
          Top = 37.795300000000000000
          Width = 566.551651960000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'PROTOCOLO DE ENTREGA')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 7.559055120000000000
          Top = 11.338590000000000000
          Width = 158.740157480000000000
          Height = 83.149606299212600000
          OnBeforePrint = 'Picture1OnBeforePrint'
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Picture2: TfrxPictureView
          Left = 11.338590000000000000
          Top = 15.118110240000000000
          Width = 162.519685040000000000
          Height = 83.149606300000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Cartaosetor: TfrxMemoView
          Left = 340.157700000000000000
          Top = 75.590600000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'setor'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[Cartao."setor"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 374.173470000000000000
        Width = 740.409927000000000000
        DataSet = dbCartao
        DataSetName = 'Cartao'
        RowCount = 0
        object Memo25: TfrxMemoView
          Left = 222.992270000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'titular'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Cartao."titular"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 71.811070000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'codcartimp'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Cartao."codcartimp"]')
          ParentFont = False
        end
        object Cartaochapa: TfrxMemoView
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'chapa'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Cartao."chapa"]')
          ParentFont = False
        end
        object Cartaodependente: TfrxMemoView
          Left = 464.882190000000000000
          Width = 275.905690000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'dependente'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Cartao."dependente"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 166.299320000000000000
        Top = 185.196970000000000000
        Width = 740.409927000000000000
        Condition = 'Cartao."empres_id"'
        ReprintOnNewPage = True
        StartNewPage = True
        object Memo5: TfrxMemoView
          Left = 222.992270000000000000
          Top = 147.401670000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'TITULAR')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 147.401670000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'CHAPA')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 464.882190000000000000
          Top = 147.401670000000000000
          Width = 275.905690000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'DEPENDENTE')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 15.118120000000000000
          Top = 15.118120000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Empresa:')
          ParentFont = False
        end
        object Cartaonome: TfrxMemoView
          Left = 94.488250000000000000
          Top = 15.118120000000000000
          Width = 510.236550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'nome'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Cartao."nome"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 15.118120000000000000
          Top = 37.795300000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Fantasia:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 15.118120000000000000
          Top = 60.472480000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Bandeira:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 15.118120000000000000
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Endere'#195#167'o:')
          ParentFont = False
        end
        object CartaoFANTASIA: TfrxMemoView
          Left = 94.488250000000000000
          Top = 39.795300000000000000
          Width = 510.236550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FANTASIA'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Cartao."FANTASIA"]')
          ParentFont = False
        end
        object Cartaoendereco: TfrxMemoView
          Left = 94.488250000000000000
          Top = 86.929190000000000000
          Width = 880.630490000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Cartao."endereco"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.811070000000000000
          Top = 147.401670000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'CARTAO')
          ParentFont = False
        end
        object Cartaodescricao: TfrxMemoView
          Left = 98.267780000000000000
          Top = 60.472480000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'descricao'
          DataSet = dbCartao
          DataSetName = 'Cartao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Cartao."descricao"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 222.992270000000000000
        Top = 415.748300000000000000
        Width = 740.409927000000000000
        object Memo6: TfrxMemoView
          Left = 11.338590000000000000
          Top = 22.677180000000000000
          Width = 706.772110000000000000
          Height = 185.196970000000000000
          ShowHint = False
          Memo.UTF8 = (
            
              'Recebi de [Cartao."empres_fornece_cartao"], os cart'#195#181'es acima li' +
              'stados, relativo ao conv'#195#170'nio mantido com esta Administradora,'
            'ficando ciente e respons'#195#161'vel pelos mesmos.'
            'Sequ'#195#170'ncial de lote de cart'#195#181'es: '
            ''
            
              'Respons'#195#161'vel pelo recebimento: Nome:____________________________' +
              '______________________________'
            ''
            
              'Ass.:____________________________________________ Carimbo:______' +
              '___________________________'
            ''
            'Data de Entrega: ____/____/________')
        end
      end
    end
  end
  object cdsLote: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 608
    Top = 288
    object cdsLoteSeq_lote: TIntegerField
      FieldName = 'Seq_lote'
    end
    object cdsLoteData: TDateField
      FieldName = 'Data'
    end
    object cdsLoteNome_arq: TStringField
      FieldName = 'Nome_arq'
      Size = 80
    end
    object cdsLoteEmpres_id: TIntegerField
      FieldName = 'Empres_id'
    end
    object cdsLoteFantasia: TStringField
      FieldName = 'Fantasia'
      Size = 80
    end
  end
  object dbLote: TfrxDBDataset
    UserName = 'Lotes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'seq_lote=seq_lote'
      'DATA=data'
      'nome_arq=nome_arq'
      'hora=hora'
      'fantasia=fantasia'
      'EMPRES_ID=empres_id'
      'DESCRICAO=DESCRICAO'
      'nome=nome'
      'cep=cep')
    DataSet = QLotesCartao
    BCDToCurrency = False
    Left = 608
    Top = 256
  end
  object cdsEmpresa: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 568
    Top = 288
    object cdsEmpresaNome: TStringField
      FieldName = 'Nome'
      Size = 120
    end
    object cdsEmpresaFantasia: TStringField
      FieldName = 'Fantasia'
      Size = 80
    end
    object cdsEmpresaBand_id: TIntegerField
      FieldName = 'Band_id'
    end
    object cdsEmpresaEndereco: TStringField
      FieldName = 'Endereco'
      Size = 80
    end
  end
  object dbEmpresas: TfrxDBDataset
    UserName = 'Empresas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nome=nome'
      'fantasia=fantasia'
      'band_id=band_id'
      'endereco=endereco')
    DataSet = QEmpresas
    BCDToCurrency = False
    Left = 568
    Top = 256
  end
  object dsCdsEmpresas: TDataSource
    DataSet = cdsEmpresa
    Left = 568
    Top = 328
  end
  object dsCdsLote: TDataSource
    DataSet = cdsLote
    Left = 608
    Top = 328
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'empres_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT empres_id,nome,fantasia,'
      
        'CONCAT(endereco,'#39' , '#39',numero,'#39' , '#39',bairro,'#39' , '#39',cep,'#39' , '#39',cidade' +
        ','#39' - '#39',estado)as endereco,'
      'band_id from empresas where empres_id = :empres_id')
    Left = 104
    Top = 280
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
    object QEmpresasband_id: TIntegerField
      FieldName = 'band_id'
    end
    object QEmpresasendereco: TStringField
      FieldName = 'endereco'
      ReadOnly = True
      Size = 158
    end
  end
  object dsEmpresas: TDataSource
    DataSet = QEmpresas
    Left = 104
    Top = 312
  end
  object QEmpCombo: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select empres_id, nome, mod_cart_id from empresas where '
      'apagado <> '#39'S'#39' '
      'order by nome')
    Left = 344
    Top = 88
  end
  object dsEmpCombo: TDataSource
    DataSet = QEmpCombo
    Left = 384
    Top = 88
  end
  object QCartao: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT conv.chapa, cart.codcartimp,cl.seq_lote,b.descricao,coale' +
        'sce(EMP_DPTOS.DEPT_ID,-1) setor_id, coalesce(EMP_DPTOS.DESCRICAO' +
        ','#39#39#39#39') setor,'
      
        'conv.TITULAR as titular,case cart.TITULAR when '#39'N'#39' then cart.nom' +
        'e else '#39#39' end as dependente,'
      
        'e.empres_id,e.nome,e.tipo_credito,e.FANTASIA,CONCAT(e.endereco,'#39 +
        ' , '#39',e.numero,'#39' , '#39',e.bairro,'#39' , '#39',e.cep,'#39' , '#39',e.cidade,'#39' - '#39',e.' +
        'estado)as endereco,'
      
        'CASE e.mod_cart_id when 1 then '#39'DROGABELLA'#39' else '#39'PLANT'#195'O CARD'#39' ' +
        'end as empres_fornece_cartao,'
      
        'CASE mc.DESCRICAO WHEN '#39'DB - Drogabella'#39' then '#39'DROGABELLA'#39' WHEN ' +
        #39'RPC - Plantao Card'#39' then '#39'PLANT'#195'O CARD'#39' WHEN '#39'AL - ALIMENTA'#199#195'O'#39 +
        ' then '#39'PLANT'#195'O CARD ALIMENTA'#199#195'O'#39' '
      'when '#39'RF - Refei'#231#227'o'#39' then '#39'PLANT'#195'O CARD REFEI'#199#195'O'#39' END '
      'from CONVENIADOS conv'
      'inner join CARTOES cart ON conv.CONV_ID = cart.CONV_ID'
      
        'inner join CARTOES_LOTES_DETALHE cld ON CART.CARTAO_ID in(cld.CA' +
        'RTAO_ID)'
      
        'inner join CARTOES_LOTES cl ON cl.SEQ_LOTE = cld.SEQ_LOTE and cl' +
        '.SEQ_LOTE between 19 and 21'
      'inner join EMPRESAS e ON e.EMPRES_ID = conv.EMPRES_ID'
      
        'inner join bandeiras b ON b.band_id = (SELECT band_id from empre' +
        'sas WHERE empres_id = conv.empres_id)'
      'inner join MODELOS_CARTOES mc ON mc.mod_cart_id = e.mod_cart_id'
      'left join EMP_DPTOS ON EMP_DPTOS.DEPT_ID = cl.setor_id'
      
        'group by conv.TITULAR,conv.CHAPA,cart.CODCARTIMP,cl.SEQ_LOTE,car' +
        't.NOME,cart.TITULAR,e.nome,e.FANTASIA,b.descricao,'
      
        'e.empres_id,e.endereco,e.numero,e.bairro,e.cep,e.cidade,e.estado' +
        ',e.MOD_CART_ID,mc.DESCRICAO,e.tipo_credito,EMP_DPTOS.DEPT_ID,EMP' +
        '_DPTOS.DESCRICAO '
      'order by e.empres_id')
    Left = 24
    Top = 280
    object QCartaochapa: TFloatField
      FieldName = 'chapa'
    end
    object QCartaocodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object QCartaoseq_lote: TAutoIncField
      FieldName = 'seq_lote'
      ReadOnly = True
    end
    object QCartaotitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QCartaodependente: TStringField
      FieldName = 'dependente'
      ReadOnly = True
      Size = 58
    end
    object QCartaoempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QCartaonome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QCartaotipo_credito: TIntegerField
      FieldName = 'tipo_credito'
    end
    object QCartaoFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object QCartaoendereco: TStringField
      FieldName = 'endereco'
      ReadOnly = True
      Size = 158
    end
    object QCartaoempres_fornece_cartao: TStringField
      FieldName = 'empres_fornece_cartao'
      ReadOnly = True
      Size = 12
    end
    object QCartaoCOLUMN1: TStringField
      FieldName = 'COLUMN1'
      ReadOnly = True
      Size = 24
    end
    object QCartaodescricao: TStringField
      FieldName = 'descricao'
      Size = 60
    end
    object QCartaosetor_id: TIntegerField
      FieldName = 'setor_id'
      ReadOnly = True
    end
    object QCartaosetor: TStringField
      FieldName = 'setor'
      ReadOnly = True
      Size = 150
    end
  end
  object dsCartao: TDataSource
    DataSet = QCartao
    Left = 24
    Top = 312
  end
  object dbCartao: TfrxDBDataset
    UserName = 'Cartao'
    CloseDataSource = False
    FieldAliases.Strings = (
      'chapa=chapa'
      'codcartimp=codcartimp'
      'seq_lote=seq_lote'
      'titular=titular'
      'dependente=dependente'
      'empres_id=empres_id'
      'nome=nome'
      'tipo_credito=tipo_credito'
      'FANTASIA=FANTASIA'
      'endereco=endereco'
      'empres_fornece_cartao=empres_fornece_cartao'
      'COLUMN1=COLUMN1'
      'descricao=descricao'
      'setor_id=setor_id'
      'setor=setor')
    DataSet = QCartao
    BCDToCurrency = False
    Left = 528
    Top = 256
  end
  object cdsCartao: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 528
    Top = 288
    object cdsCartaoChapa: TFloatField
      FieldName = 'Chapa'
    end
    object cdsCartaoCodCartao: TStringField
      FieldName = 'CodCartao'
    end
    object cdsCartaoSeq_Lote: TIntegerField
      FieldName = 'Seq_Lote'
    end
    object cdsCartaoTitular: TStringField
      FieldName = 'Titular'
      Size = 80
    end
    object cdsCartaoDependente: TStringField
      FieldName = 'Dependente'
      Size = 80
    end
  end
  object dsCdsCartao: TDataSource
    DataSet = QCartao
    Left = 528
    Top = 328
  end
  object frxPDFExport1: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Creator = 'FastReport (http://www.fast-report.com)'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 656
    Top = 287
  end
  object sd: TSaveDialog
    Filter = '.pdf|.pdf'
    Left = 344
    Top = 272
  end
  object DSModelos: TDataSource
    DataSet = QModelos
    Left = 336
    Top = 336
  end
  object QModelos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      '  *'
      'from modelos_cartoes')
    Left = 300
    Top = 337
    object QModelosMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
    object QModelosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
    end
    object QModelosOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 50
    end
    object QModelosAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QModelosDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QModelosDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QModelosDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QModelosOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QModelosOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
  end
end
