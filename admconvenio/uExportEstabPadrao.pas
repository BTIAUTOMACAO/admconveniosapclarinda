unit uExportEstabPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, DB, ZAbstractRODataset,
  ZDataset, Math, Mask, ToolEdit, DBCtrls, ClipBrd, ZAbstractDataset,
  frxClass, frxDBSet, JvExMask, JvToolEdit, ADODB, frxExportPDF,
  JvExControls, JvDBLookup, JvDBControls;

type
  TFrmExportEstabPadrao = class(TForm)
    ProgressBar1: TProgressBar;
    dsEmp: TDataSource;
    frxReport1: TfrxReport;
    frxEmps: TfrxDBDataset;
    frxCcPdf: TfrxDBDataset;
    QEmpresa: TADOQuery;
    QData: TADOQuery;    
    QDataDATA_FECHA: TDateTimeField;
    dsData: TDataSource;
    QBusca: TADOQuery;
    QBuscaCOM_RECEITA: TBCDField;
    QBuscaSEM_RECEITA: TBCDField;
    QBuscaTOTAL: TBCDField;
    QBuscaPdf: TADOQuery;
    dsPdf: TDataSource;
    QBuscaPdfCHAPA: TFloatField;
    QBuscaPdfTITULAR: TStringField;
    QBuscaPdfDESCRICAO: TStringField;
    QBuscaPdfDEPT_ID: TIntegerField;
    QBuscaPdfTOTAL: TBCDField;
    QBuscaPdfDATA_FECHA_EMP: TWideStringField; 
    sd: TSaveDialog;
    QDpto: TADOQuery;
    QDptodept_id: TIntegerField;
    QEmpLEAR: TADOQuery;
    QEmpLEARCHAPA: TFloatField;
    QEmpLEARSUM: TBCDField;
    QEmpLEARDSDesigner: TStringField;
    QEmpLEARREGISTRO: TFloatField;
    QEmpLEARVERBA: TStringField;
    QEmpLEARMS: TStringField;
    QEmpLEARANO: TIntegerField;
    QEmpLEARVALOR: TStringField;
    QEmpLEARQTDE: TIntegerField;
    QEmpLEARNDOC: TIntegerField;
    QEmpLEARIDENTIDADE: TStringField;
    QEmpLEARORDEMDECARGA: TStringField;
    QEmpLEARCODIGOVERBA: TStringField;
    QEmpLEARCENTROCUSTO: TStringField;
    QEmpLEARDATAPERIODO: TStringField;
    QEmpLEARCNPJ: TStringField;
    qCcCount: TADOQuery;
    qCc: TADOQuery;
    qCcmatricula: TFloatField;
    qCcnome: TStringField;
    qCcnf: TIntegerField;
    qCcvalor: TBCDField;
    qCccodigo_servico: TStringField;
    qCcDATARECEITA: TDateTimeField;
    qCcdatavenda: TDateTimeField;
    qCcCounttotal: TIntegerField;
    QOtica: TADOQuery;
    QOticamatricula: TFloatField;
    QOticanome: TStringField;
    QOticanf: TIntegerField;
    QOticavalor: TBCDField;
    QOticatotal_compra: TFloatField;
    QOticacodigo_servico: TStringField;
    QOticaDATARECEITA: TDateTimeField;
    QOticacred_id: TIntegerField;
    QOticadatavenda: TDateTimeField;
    QOticanome_otica: TStringField;
    QDepart: TADOQuery;
    DSDepart: TDataSource;
    QDepartempres_id: TIntegerField;
    QDepartdescricao: TStringField;
    QDepartdept_id: TIntegerField;
    QAlltec: TADOQuery;
    QAlltecFIXO1: TStringField;
    QAlltecFIXO2: TStringField;
    QAlltecFIXO3: TStringField;
    QAlltecCHAPA: TStringField;
    QAlltecTOTAL: TStringField;
    QBuscaTITULAR: TStringField;
    QConfigCadastramento: TADOQuery;
    QConfigTrailler: TADOQuery;
    QDataFechaEdp: TADOQuery;
    frxPDFExport1: TfrxPDFExport;
    frxPDFExport2: TfrxPDFExport;
    frxPDFExport3: TfrxPDFExport;
    frxReport2: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    QDepartamento: TADOQuery;
    QDepartamentoDEPARTAMENTO: TStringField;
    frxDBDataset2: TfrxDBDataset;
    frxPDFExport4: TfrxPDFExport;
    ReportData1: TfrxReport;
    QDados: TADOQuery;
    QDadosNOME: TStringField;
    frxDBDataset3: TfrxDBDataset;
    QDadosDATA_FECHAMENTO: TStringField;
    ADOQuery: TADOQuery;
    ADOQueryCHAPA: TFloatField;
    ADOQueryCPF: TStringField;
    ADOQueryTITULAR: TStringField;
    ADOQueryCOM_RECEITA: TBCDField;
    ADOQuerySEM_RECEITA: TBCDField;
    ADOQueryTOTAL: TBCDField;
    ADOQuerySEGMENTO: TIntegerField;
    ADOQueryDATACOMPRA: TDateTimeField;
    QBuscaCHAPA: TFloatField;
    Panel1: TPanel;
    lblStatus: TLabel;
    Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btnExportar: TButton;
    lkpDataFechamento: TDBLookupComboBox;
    edtCaminho: TJvDirectoryEdit;
    EdEmp_ID: TEdit;
    lkpEmp: TJvDBLookupCombo;
    cmbSegmento: TComboBox;
    ExportarApenasDemitidos: TCheckBox;
    ExportarFechamentoAnterior: TCheckBox;
    QBuscaSindservComReceita: TBCDField;
    QBuscaSindservSemReceita: TBCDField;
    QBuscaSindservTotal: TBCDField;
    QBuscaSindservTitular: TStringField;
    QBuscaSindservChapa: TFloatField;
    QBuscaSindservCPF: TStringField;
    QBuscaSindserv: TADOQuery;
    QBuscaConvSemChapa: TADOQuery;
    LancDebit: TCheckBox;
    LancNormal: TCheckBox;
    CBJuntar669: TCheckBox;
    Data1: TJvDateEdit;
    Data2: TJvDateEdit;
    Label6: TLabel;
    Label7: TLabel;
    LancUnic: TCheckBox;
    procedure btnExportarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmp_IDKeyPress(Sender: TObject; var Key: Char);
    procedure EdEmp_IDChange(Sender: TObject);
    procedure lkpEmpKeyPress(Sender: TObject; var Key: Char);
    procedure edtCaminhoKeyPress(Sender: TObject; var Key: Char);
    procedure lkpDataFechamentoKeyPress(Sender: TObject; var Key: Char);
    procedure lkpEmpExit(Sender: TObject);
    procedure EdEmp_IDExit(Sender: TObject);
    procedure lkpEmpEnter(Sender: TObject);
    procedure LancNormalClick(Sender: TObject);
    procedure LancDebitClick(Sender: TObject);
    procedure CBJuntar669Click(Sender: TObject);
    procedure LancUnicClick(Sender: TObject);
    private
    Cancelar : Boolean;
    procedure HabDesabBtnProcesso(Ativar: Boolean);
    procedure LimparCampos;
    procedure VerificarLayout
    (empres_id : Integer);
    procedure SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
    procedure SalvarXLS(Dados : TADOQuery; NomeArq : String);
    procedure SalvarXLSLEAR(Dados : TADOQuery; NomeArq : String);
    //Layout das Empresas
    procedure LayoutCRYLORRACICINYLON();
    procedure LayoutPREFUBATUBA();
    procedure LayoutADCCONFAB();
    procedure LayoutADCURBAM();
    procedure LayoutAFAPEM();
    procedure LayoutIPSM();
    procedure LayoutSINDSEP_FACILITY_TINTAUBATE();
    procedure LayoutTIBRASIL();
    procedure LayoutBUNDYPINDA();
    procedure LayoutTRELLEBORG();
    procedure LayoutPREFSJCAMPOS();
    procedure LayoutCAMARASJC_PREFJAC();
    procedure LayoutASSEJUS();
    procedure LayoutVOLKS();
    procedure LayoutAPOLO();
    procedure LayoutHEATCRAFT();
    procedure LayoutHITACHI();
    procedure LayoutCETEC();
    procedure LayoutSPANI_UNIFICADO();
    procedure LayoutDepositoCardoso();
    procedure LayoutULTIMATE();
    procedure LayoutUNIVAP();
    procedure LayoutEATON();
    procedure LayoutTECNORED();
    procedure LayoutPARKER();
    procedure LayoutMEXICHEM();
    procedure LayoutPREF_PIRASSUNUNGA();
    procedure LayoutSPANI();
    procedure LayoutVILLAREAL();
    procedure LayoutVILLAREAL_NOVO();
    procedure LayoutSPANI_NOVO();
    procedure LayoutLATECOERE();
    procedure LayoutTOTAL_CONSTRUTORA();
    procedure LayoutSUPERALEANDEP();
    procedure LayoutGremio();
    procedure LayoutSUPERALEAN();
    procedure LayoutPOLIEDRO();
    procedure LayoutPROSERV();
    procedure LayoutADATEX();
    procedure LayoutDROGACLIN();
    procedure LayoutSINDICATOJACAREI();
    procedure LayoutCOOPER();
    procedure LayoutLEAR();
    procedure LayoutCONFAB();
    procedure LayoutAESJ();
    procedure LayoutCENTERVALE();
    procedure LayoutSATLOG();
    procedure LayoutPGR();
    procedure LayoultMASSAGUACU();
    procedure LayoutALLTEC();
    procedure LayoutABCTRANSPORTES();
    procedure LayoultGRUPOCONNECTA();
    procedure LayoutEDP();
    procedure LayoutPREFILHABELA();
    procedure LayoutSINDAPARECIDA();
    procedure LayoutSINDSERVSJC();
    procedure LayoutViacaoJacarei();
    procedure LayoutMubea();
    procedure LayoutEuroquadros();
    procedure LayoutSAAEAPARECIDA();
    procedure LayoutITWFUIDS();
    procedure LayoutPINDA();
    procedure LayoutIPSM2();
    procedure ExportarExcelSpani();
    procedure LayoutGRUPOSAOPAULO();
    procedure LayoutPREFPOTIM();
    { Private declarations }
  public
    contador,totalRegistros : Integer;
    totalizador : Double;
    ext, nomeArqTxt, orderBy : string;
    head, body, footer : string;
    SList : TStringList;
    procedure LayoutSECULUM;
    function verificaDepartamento() : Boolean;
    { Public declarations }
  end;

var
  FrmExportEstabPadrao: TFrmExportEstabPadrao;
  var flag : Boolean = False; // A flag est� sendo utilizada para verificar se achou ou n�o departamentos
  caminhoArquivo : string;

implementation

uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;

{$R *.dfm}

procedure TFrmExportEstabPadrao.HabDesabBtnProcesso(Ativar: Boolean);
begin
  if Ativar then begin
    Screen.Cursor := crHourGlass;
    btnExportar.Caption := '&Cancelar';
    ProgressBar1.Position := 0;
    Cancelar := False;
    btnExportar.Enabled := False;
  end else begin
    Screen.Cursor := crDefault;
    btnExportar.Caption := '&Exportar';
    btnExportar.Enabled := True;
  end;
  Application.ProcessMessages;
end;

procedure TFrmExportEstabPadrao.btnExportarClick(Sender: TObject);
var S : String;
    SL : TStrings;
    Erro : Boolean;
begin
  if lkpEmp.KeyValue < 0 then begin
    MsgInf('Selecione uma empresa para a exporta��o');
    lkpEmp.SetFocus;
    Abort;
  end;
  if not DirectoryExists(ExtractFilePath(edtCaminho.Text)) then begin
    MsgInf('Caminho n�o encontrado!');
    edtCaminho.SetFocus;
    Abort;
  end;

  if lkpDataFechamento.KeyValue < 0 then begin
    MsgInf('Selecione uma data de fechamento para a exporta��o');
    lkpDataFechamento.SetFocus;
    Abort;
  end;
  if ((lkpEmp.KeyValue = 455) and (cmbSegmento.ItemIndex = -1)) then begin
    MsgInf('Selecione um segmento para a exporta��o');
    cmbSegmento.SetFocus;
    //ExportarApenasDemitidos.SetFocus;
    Abort;
  end;
  if ((CBJuntar669.Checked = true) and(EdEmp_Id.Text = '2379') and ((Data1.Date = EncodeDate(1899,12,30)) or (Data2.Date = EncodeDate(1899,12,30)))) then begin
    MsgInf('Selecione as datas de exporta��o para a empresa 669');
    Abort;
  end;
  if ((CBJuntar669.Checked = true) and (Data1.Date > Data2.Date )) then begin
    MsgInf('Datas inv�lidas selecionadas na exporta��o para a empresa 669');
    Abort;
  end;

  if UpperCase(btnExportar.Caption) = '&CANCELAR' then begin
    Cancelar := True;
    Abort;
  end;
  lblStatus.Caption := '';
  VerificarLayout(lkpEmp.KeyValue);
end;

procedure TFrmExportEstabPadrao.BitBtn1Click(Sender: TObject);
var Dir : string;
begin
  Dir := 'C:\';
  if SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], 0) then
    edtCaminho.Text := Dir;
end;

procedure TFrmExportEstabPadrao.FormShow(Sender: TObject);
var dia,mes,ano : Word;
begin

end;

procedure TFrmExportEstabPadrao.FormCreate(Sender: TObject);
begin
  QEmpresa.Open;
end;

procedure TFrmExportEstabPadrao.VerificarLayout( empres_id :Integer);
begin
  lblStatus.Caption := 'Capturando informa��es...';
  lblStatus.Refresh;
  btnExportar.Enabled := False;
  HabDesabBtnProcesso(True);
  Application.ProcessMessages;

  if ((empres_id = 361) or (empres_id = 362)) then
    LayoutCRYLORRACICINYLON
  else if empres_id = 59 then
    LayoutPREFUBATUBA
  else if empres_id = 542 then
    LayoutADCCONFAB
  else if empres_id = 1084 then
    LayoutADCURBAM
  else if empres_id = 42 then
    LayoutAFAPEM
  else if ((empres_id = 506) or (empres_id = 535) or (empres_id = 737) or (empres_id = 741)) then
    LayoutSINDSEP_FACILITY_TINTAUBATE
  else if ((empres_id = 145) or (empres_id = 168)) then
    LayoutTIBRASIL
  else if empres_id = 299 then
    LayoutBUNDYPINDA
  else if empres_id = 2568 then
    LayoutIPSM2
  else if empres_id = 115 then
    LayoutTRELLEBORG
  else if ((empres_id = 1512) or (empres_id = 2600)) then
    LayoutDepositoCardoso
  else if ((empres_id = 669) or (empres_id = 2202)) then
    LayoutPREFSJCAMPOS
  else if ((empres_id = 400) or (empres_id = 90)) then
    LayoutCAMARASJC_PREFJAC
  else if empres_id = 387 then
    LayoutASSEJUS
  else if ((empres_id = 1217) or (empres_id = 1665)) then
    LayoutVOLKS
  else if empres_id = 707 then
    LayoutAPOLO
  else if empres_id = 221 then
    LayoutHEATCRAFT
  else if empres_id = 201 then
    LayoutHITACHI
  else if empres_id = 182 then
    LayoutCETEC
  else if ((empres_id = 1088) or (empres_id = 1887) or (empres_id = 1888) or (empres_id = 1889)) then
    LayoutCETEC
  else if empres_id = 354 then
    LayoutULTIMATE
  else if empres_id = 118 then
    LayoutUNIVAP
  else if (empres_id = 171) or (empres_id = 356)then
    LayoutEATON
  //else if empres_id = 356 then
  //  LayoutEATON
  else if((empres_id = 127) or (empres_id = 128)) then
    LayoutPARKER
  else if empres_id = 132 then
    LayoutMEXICHEM
  else if empres_id = 2 then
    LayoutPREF_PIRASSUNUNGA
  else if((empres_id = 2228) or (empres_id = 2232) or (empres_id = 727) or (empres_id = 2234)or (empres_id = 2229) or (empres_id = 2235) or (empres_id = 2230) or (empres_id = 2233) or (empres_id = 2231) or (empres_id = 2227)
    or (empres_id = 2236) or (empres_id = 2237) or (empres_id = 2238) or (empres_id = 2239) or (empres_id = 2240) or (empres_id = 2241) or (empres_id = 2240) or (empres_id = 2224) or (empres_id = 2226) or (empres_id = 2223) or (empres_id = 2225) or (empres_id = 2247) or (empres_id = 2248) or (empres_id = 2249) or (empres_id = 2250) or (empres_id = 2251)
    or (empres_id = 2336)or (empres_id = 2371)or (empres_id = 2372)or (empres_id = 2368)or (empres_id = 2369)or (empres_id = 2370) or (empres_id = 2684) or (empres_id = 2704)) then
    LayoutSPANI_UNIFICADO
  //else if((empres_id = 2227) or (empres_id = 2228) or (empres_id = 2247)or (empres_id = 2229) or (empres_id = 2230) or (empres_id = 2231) or (empres_id = 2232) or (empres_id = 727) or (empres_id = 2233) or (empres_id = 2234) or (empres_id = 2236) or (empres_id = 2237) or (empres_id = 2238) or (empres_id = 2250) or (empres_id = 2248) or (empres_id = 2249) or (empres_id = 2240) or (empres_id = 2239) or (empres_id = 2241) or (empres_id = 2371) or (empres_id = 2372) or (empres_id = 2368) or (empres_id = 2369) or (empres_id = 2251) or (empres_id = 2370) or (empres_id = 2336)) then
    //LayoutSPANI_NOVO
  //else if((empres_id = 2223) or (empres_id = 2224) or (empres_id = 2225) or (empres_id = 2226) or (empres_id = 490) or (empres_id = 2235)) then
    //LayoutVILLAREAL_NOVO
  //else if((empres_id = 491) or (empres_id = 492) or (empres_id = 493) or (empres_id = 494) or (empres_id = 495) or (empres_id = 728) or (empres_id = 727) or (empres_id = 1227) or (empres_id = 1447) or (empres_id = 1879) or (empres_id = 1947) or (empres_id = 2037)) then
    //LayoutSPANI
  //else if((empres_id = 486) or (empres_id = 487) or (empres_id = 488) or (empres_id = 489) or (empres_id = 490) or (empres_id = 1590)) then
    //LayoutVILLAREAL
  else if empres_id = 575 then
    LayoutLATECOERE
  else if empres_id = 1363 then
    LayoutTOTAL_CONSTRUTORA
  else if ((empres_id = 1159) or (empres_id = 1160)) then
    LayoutSUPERALEANDEP
  else if ((empres_id = 1158) or (empres_id = 1161) or (empres_id = 1682) or (empres_id = 1683) or (empres_id = 1684) or (empres_id = 1683) or (empres_id = 1688) or (empres_id = 1833) or (empres_id = 1895))  then
    LayoutSUPERALEAN
  else if ((empres_id = 122) or (empres_id = 123) or (empres_id = 125) or (empres_id = 126) or (empres_id = 41)) then
    LayoutPOLIEDRO
  else if empres_id = 342 then
    LayoutPROSERV
  else if ((empres_id = 314) or (empres_id = 735)) then
    LayoutADATEX
  else if empres_id = 45 then
    LayoutDROGACLIN
  else if ((empres_id = 20) or (empres_id = 2430)) then
    LayoutSINDICATOJACAREI
  else if empres_id = 1396 then
    LayoutLEAR
  else if empres_id = 455 then
    LayoutCONFAB
  else if ((empres_id = 1670) or (empres_id = 1671) or (empres_id = 1672) or (empres_id = 1673)) then
    LayoutSECULUM
  else if empres_id = 202 then
    LayoutCOOPER
  else if empres_id = 165 then
    LayoutAESJ
  else if ((empres_id = 318) or (empres_id = 391) or(empres_id = 392)) then
    LayoutCENTERVALE
  else if ((empres_id = 438)) then
    LayoutSATLOG
  else if ((empres_id = 430)) then
    LayoutALLTEC
  else if ((empres_id = 437)) then
    LayoutPGR
  else if ((empres_id = 159)) then
    LayoutABCTRANSPORTES
  else if empres_id = 1337 then
    LayoultMASSAGUACU
  else if empres_id = 1974 then
    LayoutEDP
  else if empres_id = 2184 then
    LayoutPREFILHABELA
  else if empres_id = 2334 then
    LayoutSINDAPARECIDA
  else if empres_id = 2379 then
    LayoutSINDSERVSJC
  else if empres_id = 2397 then
    LayoutTECNORED
  else if ((empres_id = 1706) or (empres_id = 1707) or (empres_id = 2662) or (empres_id = 2663) or (empres_id = 2664) or (empres_id = 2665)) then
    LayoutEuroquadros
  else if empres_id = 334 then
    LayoutMubea
  else if ((empres_id = 310) or (empres_id = 358) or (empres_id = 240)) then
    LayoutViacaoJacarei
  else if ((empres_id = 217) or (empres_id = 743) or(empres_id = 163) or(empres_id = 195)or (empres_id = 2755)) then
    LayoultGRUPOCONNECTA
  else if empres_id = 2620 then
    LayoutSAAEAPARECIDA
  else if ((empres_id = 2666) or (empres_id = 2667)) then
    LayoutGremio
  else if empres_id = 341 then
    LayoutITWFUIDS
  else if ((empres_id = 2394) or (empres_id = 2708)) then
    LayoutPINDA
  else if (empres_id = 365) then
    LayoutGRUPOSAOPAULO
  else if (empres_id = 2797) then
    LayoutPREFPOTIM;
end;

procedure TFrmExportEstabPadrao.SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
begin
  if termino = false then
    SList.Add(Linha);

  if Termino = true then
  begin
    if SList.Count > 0 then begin

      if UpperCase(ExtractFileExt(edtCaminho.Text)) <>  Formato then begin
        caminhoArquivo := edtCaminho.Text + '\' + NomeArq + Formato;
        SList.SaveToFile(caminhoArquivo);
        //AbrirArquivo(ExtractFilePath(caminhoArquivo),ExtractFileName(caminhoArquivo),fmOpenRead);
      end else begin
        SList.SaveToFile(edtCaminho.Text);
        AbrirArquivo(ExtractFilePath(edtCaminho.Text),ExtractFileName(edtCaminho.Text),fmOpenRead);
      end;
    end;
  end;
end;

procedure TFrmExportEstabPadrao.SalvarXLS(Dados : TADOQuery; NomeArq : String);
var excel: variant;
    i , coluna, linha: integer;
    valor: variant;
begin
   //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        excel.Workbooks.add(1);
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      try
        Screen.Cursor := crHourGlass;
        coluna := 0;
        linha  := 1;
        for i:=0 to Dados.FieldCount - 1  do begin
          inc(coluna);
          valor := Dados.Fields[i].DisplayLabel;
          excel.cells[1,coluna] := valor;
          excel.cells[linha,coluna].Font.bold := True;
        end;
        inc(linha);

        Dados.First;
        While not Dados.Eof do begin
          Coluna := 0;
          for i:= 0 to Dados.FieldCount-1 do begin
            inc(coluna);
            if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
              valor := Dados.Fields[i].AsFloat;
              if (Dados.Fields[i] is TBCDField) then
                excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)'
              else
              excel.cells[linha,coluna].NumberFormat := '#';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end else begin
              valor := Dados.Fields[i].AsString;
              excel.cells[linha,coluna].NumberFormat := '@';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
            end;
            excel.cells[linha,coluna].Value := valor;
          end;
          inc(linha);
          Dados.Next;
        end;
        excel.columns.AutoFit;
        excel.ActiveWorkBook.SaveAs(FileName:=edtCaminho.Text + '\' + NomeArq,Password := '');
        excel.Visible := true;

    except on e:Exception do
        msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
        e.message);
      end;

end;

procedure TFrmExportEstabPadrao.SalvarXLSLEAR(Dados : TADOQuery; NomeArq : String);
var excel: variant;
    i , coluna, linha: integer;
    valor: variant;
begin
   //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        //excel.Workbooks.add(1);
        excel.WorkBooks.Add;
        excel.Workbooks[1].Sheets.Add;
        excel.Workbooks[1].WorkSheets[1].Name := 'DIGITA��O';
        excel.Workbooks[1].WorkSheets[2].Name := 'ARQIMPORTACAO';
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      try
        Screen.Cursor := crHourGlass;
        coluna := 0;
        linha  := 1;
        for i:=0 to Dados.FieldCount - 1  do begin
          inc(coluna);
          valor := Dados.Fields[i].DisplayLabel;
          excel.Workbooks[1].WorkSheets[1].cells[1,coluna] := valor;
        end;
        inc(linha);

        Dados.First;
        While not Dados.Eof do begin
          Coluna := 0;
          for i:= 0 to Dados.FieldCount-1 do begin
            inc(coluna);
            if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
              //excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)';
              if (Dados.Fields[i] is TBCDField) then
                excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)'
              else
              excel.cells[linha,coluna].NumberFormat := '#';
              valor := Dados.Fields[i].AsString;
              excel.Workbooks[1].WorkSheets[1].cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end else begin
              valor := Dados.Fields[i].AsString;
              excel.Workbooks[1].WorkSheets[1].cells[linha,coluna].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[1].cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end;
            excel.Workbooks[1].WorkSheets[1].cells[linha,coluna].Value := valor;
          end;
          inc(linha);
          Dados.Next;
        end;
        excel.columns.AutoFit;

        //GERA PLANILHA DE ARQDEIMPORTACAO//
        coluna := 0;
        linha  := 1;
        Dados.First;
        While not Dados.Eof do begin
          Coluna := 0;
          for i:= 0 to Dados.FieldCount-1 do begin
            if Dados.Fields[i].DisplayLabel = 'REGISTRO' then
            begin
              inc(coluna);
              valor := Dados.Fields[i].AsString;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna].ColumnWidth := 15.38;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna].Value := valor;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna + 1].Value := ' ';
            end else if Dados.Fields[i].DisplayLabel = 'VERBA' then begin
              inc(coluna);
              valor := fnCompletarCom(Dados.Fields[i].AsString,10,'0');
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 10.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
            end else if Dados.Fields[i].DisplayLabel = 'M�S' then begin
              inc(coluna);
              valor := Dados.Fields[i].AsString;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 2.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
            end else if Dados.Fields[i].DisplayLabel = 'ANO' then begin
              inc(coluna);
              valor := Dados.Fields[i].AsString;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 4.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
            end else if Dados.Fields[i].DisplayLabel = 'VALOR' then begin
              inc(coluna);
              valor := fnCompletarCom(Dados.Fields[i].AsString,17,'0');
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 17.50;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
            end else if Dados.Fields[i].DisplayLabel = 'QTDE' then begin
              inc(coluna);
              valor := fnCompletarCom(Dados.Fields[i].AsString,9,'0');
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 9.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
            end else if Dados.Fields[i].DisplayLabel = 'N� DOC.' then begin
              inc(coluna);
              valor := fnCompletarCom(Dados.Fields[i].AsString,10,'0');
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +1].ColumnWidth := 10.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +1].Value := valor;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +2].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +2].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +2].ColumnWidth := 10.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +2].Value := '0000000000';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +3].ColumnWidth := 10.25;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].Value := '0000000000';
             end else if Dados.Fields[i].DisplayLabel = 'CNPJ' then begin
              inc(coluna);
              valor := fnCompletarCom(Dados.Fields[i].AsString,13,'0');
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].NumberFormat := '@';
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].HorizontalAlignment := $FFFFEFC8;
              excel.Workbooks[1].WorkSheets[2].Cells[Linha,coluna +3].ColumnWidth := 14.38;
              excel.Workbooks[1].WorkSheets[2].cells[linha,coluna +3].Value := valor;
            end;
          end;
          inc(linha);
          Dados.Next;
        end;

        excel.ActiveWorkBook.SaveAs(FileName:=edtCaminho.Text + '\' + NomeArq,Password := '');
        excel.Visible := true;

    except on e:Exception do
        msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
        e.message);
      end;

end;

//Paulinho code//
procedure TFrmExportEstabPadrao.LayoutSAAEAPARECIDA();
var
  Erro,hasDepartamento : Boolean;
  count,contLinha: integer;
  S, valor, dec,valorTotalStr : String;
  valorTotal    : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) as TOTAL ');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR order by conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  S := '';
  S := S+ '0043672880000155SERVI�O AUT�NOMO DE �GUA E ESGOTO DE APARECIDA              0000000001';
  SalvarArquivo(S,'2620_SAAE_APARECIDA', false,'.txt');
  contLinha := 2;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ '1';
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,10,'0');
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,50,' ',true);
        S := S+ '0000001181';
        valorTotal := valorTotal + QBuscaTOTAL.AsFloat;
        valor := FormatFloat('###,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-1,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-2),crNUM) + dec;
        S := S+ fnCompletarCom(valor,12,'0');
        S := S+ fnCompletarCom(IntToStr(contLinha),10,'0');
        SalvarArquivo(S,'2620_SAAE_APARECIDA', false,'.txt');
        contLinha := contLinha + 1;
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;


    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;



  if not QBusca.IsEmpty then begin

      S:= '';
      S := S+ '9';
      S := S+ fnCompletarCom(IntToStr(contLinha-2),10,'0');
      valorTotalStr  := FormatFloat('###,##0.00',valorTotal);
      dec := Copy(valorTotalStr,Length(valorTotalStr)-1,3);
      valorTotalStr := fnRemoveCaractersQueNaoSejam(Copy(valorTotalStr,1,Length(valorTotalStr)-2),crNUM) + dec;
      S := S+ fnCompletarCom(valorTotalStr,12,'0');
      S := S+ fnCompletarCom(' ',60,' ',true);
      S := S+ fnCompletarCom(IntToStr(contLinha),10,'0');
      SalvarArquivo(S,'2620_SAAE_APARECIDA', false,'.txt');
      SalvarArquivo(S,'2620_SAAE_APARECIDA', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutITWFUIDS();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  aux, cpfaux, cpfvalor, S, valor, dec : String;

begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;

  ADOQuery.Close;
  ADOQuery.SQL.Clear;
  ADOQuery.SQL.Add(' select CONV.CPF,CONV.CHAPA ,CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) as TOTAL, seg.SEG_ID as SEGMENTO, cc.data as DATACOMPRA');
  ADOQuery.SQL.Add(' from CONVENIADOS CONV');
  ADOQuery.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  ADOQuery.SQL.Add(' inner join CREDENCIADOS cred on cred.CRED_ID = cc.CRED_ID');
  ADOQuery.SQL.Add(' inner join SEGMENTOS seg on seg.SEG_ID = cred.SEG_ID');
  ADOQuery.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  ADOQuery.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  ADOQuery.SQL.Add(' GROUP BY CONV.CPF,CONV.CHAPA ,CONV.TITULAR, seg.SEG_ID, cc.data order by conv.CPF,conv.TITULAR');
  ADOQuery.Open;
  count := ADOQuery.RecordCount;
  SList := TStringList.Create;

  while not ADOQuery.Eof do begin
    if ADOQueryTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ 'H';
        S := S+ FormatDateTime('yyyy',now) + FormatDateTime('mm', now) + FormatDateTime('dd',EndOfTheMonth(now)) + FormatDateTime('hh', now) + FormatDateTime('nn',now);
        S := S +  fnCompletarCom(' ',63,' ');
        S := S + #13;
        S := S + 'D';
        S := S + '07125955000114';
        if(ADOQuerySEGMENTO.AsInteger = 8) then
          S := S + 'FARMACIA'
        else
          S := S + 'CONVENIO';

        S := S + fnCompletarCom(' ',22,' ');
        if (ADOQueryCPF.Value <> '') then begin
          cpfaux := ADOQueryCPF.AsString;
          cpfvalor := AnsiReplaceStr(cpfaux,'.','');
          cpfvalor := AnsiReplaceStr(cpfvalor,'-','');
          S := S + cpfvalor
        end
        else
          S := S + fnCompletarCom('',11,' ');

          S := S + fnCompletarCom(' ',7,' ');

        if(ADOQueryTOTAL.Value < 10) then begin
          aux := aux + '0';
          aux := aux + ADOQueryTOTAL.AsString;
          aux := aux + ',00';
          S := S + StringReplace(aux,',','.',[rfReplaceAll, rfIgnoreCase]);
        end
        else
          S := S + StringReplace(fnCompletarCom(ADOQueryTOTAL.AsString,5,'0'),',','.',[rfReplaceAll, rfIgnoreCase]);

        S := S + Copy(ADOQueryDATACOMPRA.AsString,Length(ADOQueryDATACOMPRA.AsString)-3,4); //ano
        S := S + Copy(ADOQueryDATACOMPRA.AsString,Length(ADOQueryDATACOMPRA.AsString)-6,2); //mes
        S := S + Copy(ADOQueryDATACOMPRA.AsString,Length(ADOQueryDATACOMPRA.AsString)-9,2); //Dia

        S := S + #13;
        S := S + 'T';
        S := S + fnCompletarCom('',75,'9');
       //S := S + fnCompletarCom(' ',4,' ');
        //valor := FormatFloat('###,##0.00',ADOQueryTOTAL.Value);
        //dec := Copy(valor,Length(valor)-1,3);
        //valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-2),crNUM) + dec;
        //S := S+valor;
        SalvarArquivo(S,'341_ITWFLUIDS', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    ADOQuery.Next;
    ProgressBar1.Position := ((ADOQuery.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not ADOQuery.IsEmpty then begin
      SalvarArquivo(S,'341_ITWFLUIDS', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutCONFAB();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho, segmento : integer;
  S, valor, dec, empresa, mes : String;
  valorTotal : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  hasDepartamento := verificaDepartamento;
  qCc.Close;
  qCcCount.Close;
  qCc.Parameters[1].Value := 1;
  qCc.CommandTimeout := 0;
  QOtica.Parameters[1].Value := 1;
  qCcCount.Parameters[1].Value := 1;
  if cmbSegmento.ItemIndex = 0 then
    segmento := 1
  else
    segmento := 2;

  //if ExportarApenasDemitidos.Checked then begin
  //   qCc.SQL.Add('and conv.data_demissao = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',(now))));
  // end;

  if segmento = 1 then begin
    qCc.Parameters[0].Value := lkpEmp.KeyValue;
    qCcCount.Parameters[0].Value := lkpEmp.KeyValue;
    if ExportarApenasDemitidos.Checked then begin
      qCc.SQL.Add('and conv.data_demissao <= ' + QuotedStr(FormatDateTime('dd/mm/yyyy',(now))));
        if   ExportarFechamentoAnterior.checked then begin
          qCc.SQL.Add('and cc.data_fecha_emp >= (SELECT top 1 DATA_FECHA from DIA_FECHA where DATEADD(MONTH,+1,DATA_FECHA) > GETDATE() and EMPRES_ID= '+lkpEmp.KeyValue+')');
          end else
          qCc.SQL.Add('and cc.data_fecha_emp >= (SELECT top 1 DATA_FECHA from DIA_FECHA where DATA_FECHA >= CONVERT(varchar,getdate(),103) and EMPRES_ID= '+lkpEmp.KeyValue+')');
      end else begin
      qCc.SQL.Add('and cc.data_fecha_emp =  ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
    end;
    if hasDepartamento then
       qCc.SQL.Add('order by conv.SETOR, conv.titular')
       else
       qCc.SQL.Add('order by conv.titular');
    //qCcCount.SQL.Add('and cc.data_fecha_emp = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
    lblStatus.Caption := 'Capturando informa��es...';
    lblStatus.Refresh;
    HabDesabBtnProcesso(True);
    Application.ProcessMessages;
    qCc.Open;
    qCcCount.Open;
    count := qCcCountTOTAL.AsInteger;
    S := '';
    Erro := False;
    lblStatus.Caption := 'Gerando cabe�alho';
    lblStatus.Refresh;
    Application.ProcessMessages;
    SList := TStringList.Create;

    empresa := 'ExportacaoNotasSeg1-' + Copy(FormatDateTime('dd/mm/yyyy',date),7,4) + Copy(FormatDateTime('dd/mm/yyyy',date),4,2) + Copy(FormatDateTime('dd/mm/yyyy',date),1,2);

    while not qCc.Eof do begin
      if qCcVALOR.Value > 0.00 then begin
        S := '';
        valor := '';
        dec := '';
        lblStatus.Caption := 'Exportando nota:'+ qCcNF.AsString;
        try
          if Cancelar then
            if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
             Abort;
          end;
          S := S+ fnCompletarCom(qCcMATRICULA.AsString,6) + ',"';
          S := S+ fnCompletarCom(qCcNOME.AsString,30,' ',True) + '",';
          S := S+ fnCompletarCom(qCcNF.AsString,6) + ',';
          valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',qCcVALOR.Value));
          dec := Copy(valor,Length(valor)-2,3);
          valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
          S := S+ fnCOmpletarCom(valor,13) + ',';
          S := S+ fnCompletarCom(qCcCODIGO_SERVICO.AsString,4) + ',';
          S := S+ fnCompletarCom(qCcDATAVENDA.AsString,10,' ',True) + ',';
          S := S+ FormatDateTime('mmyy',qCcDATAVENDA.AsDateTime)+'46';
          SalvarArquivo(S,empresa,false,'.txt');
          except on E:Exception do begin
            Erro := True;
            MsgErro('Erro: '+E.message);
          end;
        end;
      end;
      qCc.Next;
      ProgressBar1.Position := ((qCc.RecNo*100) div count);
      ProgressBar1.Refresh;
      Application.ProcessMessages;
    end;
    if not qCc.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
    end;
  end
  else begin
    QOtica.Close;

    QOtica.Parameters[0].Value := lkpEmp.KeyValue;
    if ExportarApenasDemitidos.Checked then begin
      QOtica.SQL.Add('and conv.data_demissao = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',(now))));
      QOtica.SQL.Add('and cc.data_fecha_emp >= (SELECT top 1 DATA_FECHA from DIA_FECHA where DATA_FECHA > CONVERT(varchar,getdate(),103) and EMPRES_ID= '+lkpEmp.KeyValue+')');
    end
    else
      QOtica.SQL.Add('and cc.data_fecha_emp = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
    if hasDepartamento then
       QOtica.SQL.Add(' order by conv.setor, conv.titular, conv.chapa')
    else
       QOtica.SQL.Add(' order by conv.titular');
    lblStatus.Caption := 'Capturando informa��es...';
    lblStatus.Refresh;
    HabDesabBtnProcesso(True);
    Application.ProcessMessages;
    Erro := False;
    Application.ProcessMessages;
    SList := TStringList.Create;

    empresa := 'ExportacaoNotasSeg2e3-' + Copy(FormatDateTime('dd/mm/yyyy',date),7,4) + Copy(FormatDateTime('dd/mm/yyyy',date),4,2) + Copy(FormatDateTime('dd/mm/yyyy',date),1,2);
    QOtica.CommandTimeout := 0;
    QOtica.Open;
    S := '';
    while not QOtica.Eof do begin
      if QOticaVALOR.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      lblStatus.Caption := 'Exportando nota:'+ QOticaNF.AsString;
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QOticaMATRICULA.AsString,6) + ',"';
        S := S+ fnCompletarCom(QOticaNOME.AsString,30,' ',True) + '",';
        S := S+ fnCompletarCom(QOticaNF.AsString,6) + ',';
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QOticaTOTAL_COMPRA.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12) + ',';
        S := S+ fnCompletarCom(QOticaCODIGO_SERVICO.AsString,4) + ',';
        S := S+ '000045,';
        S := S+ fnCompletarCom(QOticaDATAVENDA.AsString,10,' ',True) + ',';
        S := S+ FormatDateTime('mmyy',QOticaDATAVENDA.AsDateTime)+'45,';
        S := S+ fnCompletarCom(QOticaNOME_OTICA.AsString,50,' ',True);
        SalvarArquivo(S,empresa, false,'.txt');;
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
      QOtica.Next;
      ProgressBar1.Position := ((QOtica.RecNo*100) div QOtica.RecordCount) ;
      Application.ProcessMessages;
    end;

    if not QOtica.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
    end;

  end;

  if not Erro then
    //lblStatus.Caption := 'Exporta��o concluida com sucesso!'
    begin
      MsgInf('Exporta��o concluida com sucesso!');
      edtCaminho.Text :='';
      EdEmp_ID.Text := '';
      lkpEmp.KeyValue := -1;
      lkpDataFechamento.KeyValue := -1;
      cmbSegmento.ItemIndex := -1;
      lblStatus.Caption := '';
    end
  else
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';

  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

  qCc.SQL.Clear;
  qCc.Close;
  qCcCount.SQL.Clear;
  qCcCount.Close;
end;

procedure TFrmExportEstabPadrao.LimparCampos();
begin
  edtCaminho.Text := '';
  EdEmp_ID.Text := '';
  lkpEmp.KeyValue := -1;
  lkpDataFechamento.KeyValue := -1;
  lblStatus.Caption := '';
  cmbSegmento.ItemIndex := -1;
end;

procedure TFrmExportEstabPadrao.LayoutTECNORED();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, ');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''S'') = ''S'' then cc.debito-cc.credito else 0 end),0) as COM_RECEITA,');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''N'') = ''N'' then cc.debito-cc.credito else 0 end),0) as SEM_RECEITA,');
  QBusca.SQL.Add(' COALESCE(SUM(cc.debito-cc.credito),0) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR order by conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  QDados.Close;
  QDados.SQL.Clear;
  QDados.SQL.Add(' SELECT NOME, ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue))+ ' AS DATA_FECHAMENTO FROM EMPRESAS WHERE EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QDados.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  if not QBusca.IsEmpty then begin
        frxPDFExport4.FileName := edtCaminho.Text + '\tecnored' + StringReplace(lkpDataFechamento.Text,'/','-',[rfReplaceAll]) + '.pdf';
        ReportData1.ShowReport;
        ReportData1.Export(frxPDFExport4);
        MsgInf('Exporta��o concluida com sucesso!');
        LimparCampos;
  end;


end;

procedure TFrmExportEstabPadrao.LayoutAESJ();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR order by conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,30,' ', true);
        S := S+ fnCompletarCom(';',1);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,9,' ');
        S := S+ fnCompletarCom(';',1);
        SalvarArquivo(S,'165_AESJ', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'165_AESJ', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;





procedure TFrmExportEstabPadrao.LayoutEDP();
var
  Erro,hasDepartamento : Boolean;
  count,contadorArquivo,contadorDeLinhasDoArquivo : integer;
  S, valor,dec,endereco,valorTotal : String;
  dataIncial : TDateTime;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  //hasDepartamento := verificaDepartamento;


  //Cabe�alho
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Text := 'SELECT * FROM CONFIG_EDP_HEADER';
  DMConexao.AdoQry.Open;
  QDataFechaEdp.Close;
  QDataFechaEdp.SQL.Clear;
  QDataFechaEdp.SQL.Add('SELECT TOP(1) DATA_FECHA FROM DIA_FECHA WHERE EMPRES_ID = '+IntToStr(lkpEmp.KeyValue)+' AND DATA_FECHA >= GETDATE()') ;
  QDataFechaEdp.Open;
  SList := TStringList.Create;
  S := '';
  S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,1);//A01 - CODIGO DE REGISTRO =A
  S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[1].Value,1);//A02 - CODIGO DE REMESSA = 2
  S := S+ fnCompletarCom(' ',20,' '); //A03 - RESERVADO PARA O FUTURO
  S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[2].Value,20,' ', true); // A04 - NOME DA EMPRESA
  S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[3].Value,3); //A05 - CODIGO DO AGENTE EXTERNO = 072
  S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[4].Value,20,' ', true); //A06 NOME NO AGENTE EXTERNO
  //S := S+ fnCompletarCom(Copy(FormatDateTime('dd;mm;yyyy',lkpDataFechamento.KeyValue),4,7),7);
  S := S+ fnCompletarCom(FormatDateTime('yyyy',QDataFechaEdp.Fields[0].Value) + FormatDateTime('mm',QDataFechaEdp.Fields[0].Value) + FormatDateTime('dd',QDataFechaEdp.Fields[0].Value),8); //A07 - DATA GERA��O DO ARQUIVO
  S := S+ fnCompletarCom(' ',77,' '); //A08 - RESERVADO PARA O FUTURO
  SalvarArquivo(S,'EDPS',false,'.txt');
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  //Fim do Cabe�alho

  //Registro Tipo B - Cadastramento
  QConfigCadastramento.Close;
  QConfigCadastramento.SQL.Clear;
  QConfigCadastramento.SQL.Add(' select CODIGO_REGISTRO, CODIGO_VALOR_EXTRA, CODIGO_PRODUTO,');
  QConfigCadastramento.SQL.Add(' NUMERO_PARCELAS, VALOR_PARCELA');
  QConfigCadastramento.SQL.Add(' from CONFIG_EDP_CADASTRAMENTO');
  QConfigCadastramento.Open;

  //BUSCA DOS REGISTRO COM OS RESPECTIVOS N�MEROS DE INSTALA��O
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('select CONV.NUMERO_INSTALACAO_EDP,'); //0
  DMConexao.AdoQry.SQL.Add('CONV.ENDERECO LOGRADOURO, '); //1
  DMConexao.AdoQry.SQL.Add('CONV.NUMERO,'); //2
  DMConexao.AdoQry.SQL.Add('B.DESCRICAO BAIRRO, '); //3
  DMConexao.AdoQry.SQL.Add('C.NOME CIDADE, '); //4
  DMConexao.AdoQry.SQL.Add('E.DESCRICAO UF, '); //5
  DMConexao.AdoQry.SQL.Add('CONV.CEP '); //6
  DMConexao.AdoQry.SQL.Add('from CONVENIADOS CONV ');
  DMConexao.AdoQry.SQL.Add('LEFT JOIN BAIRROS B ON B.BAIRRO_ID = CONV.BAIRRO ');
  DMConexao.AdoQry.SQL.Add('LEFT JOIN CIDADES C ON C.CID_ID = CONV.CIDADE ');
  DMConexao.AdoQry.SQL.Add('LEFT JOIN ESTADOS E ON E.ESTADO_ID = CONV.ESTADO ');
  DMConexao.AdoQry.SQL.Add('where empres_id = ' +IntToStr(lkpEmp.KeyValue)+ ' and liberado = ''S'' and apagado = ''N'' ');
  DMConexao.AdoQry.Open;
  count := DMConexao.AdoQry.RecordCount;


  while not DMConexao.AdoQry.Eof do begin
    if not (DMConexao.AdoQry.Fields[0].Value = null)  then begin
      if (DMConexao.AdoQry.Fields[0].Value <> '') and (Length(DMConexao.AdoQry.Fields[0].Value) >= 8) then begin
        contadorArquivo:= contadorArquivo + 1;
        S := '';
        valor := '';
        dec := '';
        try
          if Cancelar then
            if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
              Abort;
          end;
          S := S+ fnCompletarCom(QConfigCadastramento.Fields[0].Value,1);
          S := S+ fnCompletarCom(DMConexao.AdoQry.Fields[0].Value,9,'0');
          S := S+ fnCompletarCom(QConfigCadastramento.Fields[1].Value,2);
          S := S+ fnCompletarCom(QConfigCadastramento.Fields[2].Value,3);
          S := S+ fnCompletarCom(QConfigCadastramento.Fields[3].Value,2);
          valor := FormatFloat('#,##0.00',23.42);
          dec := Copy(valor,Length(valor)-1,2);
          valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
          S := S+ fnCOmpletarCom(valor,15,'0');
          S := S+ fnCompletarCom(' ',12,' ');
          dataIncial := IncMonth(Date);
          S := S+ fnCompletarCom(FormatDateTime('yyyy',dataIncial) + FormatDateTime('mm',IncMonth(dataIncial)) + '01',8);
          S := S+ fnCompletarCom('0',08);
          endereco := DMConexao.AdoQry.Fields[1].AsString + ', ' + DMConexao.AdoQry.Fields[2].AsString + ', ' + DMConexao.AdoQry.Fields[3].AsString + ', '+ DMConexao.AdoQry.Fields[4].AsString + ', '+ DMConexao.AdoQry.Fields[5].AsString + ', ' +DMConexao.AdoQry.Fields[6].AsString;
          S := S+ fnCompletarCom(endereco,40,' ');
          S := s+ fnCompletarCom(' ',47, ' ');
          S := s+ fnCompletarCom('01',02);
          S := s+ fnCompletarCom('2',01);

          SalvarArquivo(S,'EDPS', false,'.txt');
          contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
          except on E:Exception do begin
            Erro := True;
            MsgErro('Erro: '+E.message);
          end;
        end;
      end;
    end;




    DMConexao.AdoQry.Next;
    ProgressBar1.Position := ((DMConexao.AdoQry.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;
  //Fim do Registro Tipo B - Cadastramento

  //REGISTRO TIPO Z - TRAILLER
  S := '';
  S := S+ fnCompletarCom('Z',01);
  contadorDeLinhasDoArquivo := contadorDeLinhasDoArquivo + 1;
  S := S+ fnCompletarCom(IntToStr(contadorDeLinhasDoArquivo),6,'0');
  valorTotal := FloatToStr((23.42 * contadorArquivo));
  valorTotal := FormatFloat('#,##0.00',StrToFloat(valorTotal));
  dec := Copy(valorTotal,Length(valorTotal)-1,2);
  valorTotal := fnRemoveCaractersQueNaoSejam(Copy(valorTotal,1,Length(valorTotal)-3),crNUM) + dec;
  S := S+ fnCompletarCom(valorTotal,17,'0');
  S := S+ fnCompletarCom(' ',126, ' ');
  SalvarArquivo(S,'EDPS', false,'.txt');
  //FIM DO REGISTRO TIPO Z

  if not DMConexao.AdoQry.IsEmpty then begin
      SalvarArquivo(S,'EDPS', true,'.txt');
  end;

//  tamanho := 2;
//  valorTotal := 0;
//
//  QBusca.Close;
//  QBusca.SQL.Clear;
//  QBusca.SQL.Add(' select NUMERO_INSTALACAO_EDP');
//  QBusca.SQL.Add(' from CONVENIADOS CONV');
//  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
//  QBusca.SQL.Add(' conv.TITULAR');
//  QBusca.Open;
//  count := QBusca.RecordCount;
//  SList := TStringList.Create;
//
//  while not QBusca.Eof do begin
//    if QBuscaTOTAL.Value > 0.00 then begin
//      S := '';
//      valor := '';
//      dec := '.';
//      try
//        if Cancelar then
//          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
//            Abort;
//        end;
//        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
//        S := S+ fnCompletarCom(';',1);
//        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,30,' ', true);
//        S := S+ fnCompletarCom(';',1);
//        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
//        dec := Copy(valor,Length(valor)-2,3);
//        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
//        S := S+ fnCOmpletarCom(valor,9,' ');
//        S := S+ fnCompletarCom(';',1);
//        SalvarArquivo(S,'165_AESJ', false,'.txt');
//        except on E:Exception do begin
//          Erro := True;
//          MsgErro('Erro: '+E.message);
//        end;
//      end;
//    end;
//    QBusca.Next;
//    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
//    ProgressBar1.Refresh;
//    Application.ProcessMessages;
//  end;
//
//  if not QBusca.IsEmpty then begin
//      SalvarArquivo(S,'165_AESJ', true,'.txt');
//  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutLEAR();
var
  Erro, hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa, mes : String;
  valorTotal : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QEmpLEAR.Close;
  QEmpLEAR.SQL.Clear;
  QEmpLEAR.SQL.Add(' select conv.CHAPA, SUM(DEBITO - CREDITO) AS SUM, REPLACE(SUM(DEBITO - CREDITO),''.'','''') as '' '',');
  QEmpLEAR.SQL.Add(' CONV.CHAPA AS REGISTRO, ''962'' AS VERBA, RIGHT(''00''+ CONVERT(VARCHAR,month(CC.DATA_FECHA_EMP)),2) as M�S,');
  QEmpLEAR.SQL.Add(' YEAR(CC.DATA_FECHA_EMP) AS ANO, REPLACE(SUM(DEBITO - CREDITO),''.'','''') AS VALOR,');
  QEmpLEAR.SQL.Add(' 1 AS QTDE, 1 AS ''N� DOC.'', '''' AS IDENTIDADE, '''' AS ''ORDEM DE CARGA'', '''' AS ''CODIGO VERBA'',');
  QEmpLEAR.SQL.Add(' '''' AS ''CENTRO CUSTO'', '''' AS ''DATA PERIODO'', '''' AS CNPJ');
  QEmpLEAR.SQL.Add(' FROM CONVENIADOS CONV');
  QEmpLEAR.SQL.Add(' JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QEmpLEAR.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QEmpLEAR.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QEmpLEAR.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  QEmpLEAR.SQL.Add(' GROUP BY CONV.CHAPA,CC.DATA_FECHA_EMP,CONV.SETOR');
  if hasDepartamento then
     QEmpLEAR.SQL.Add(' ORDER BY CONV.setor')
     else
         QEmpLEAR.SQL.Add(' ORDER BY CONV.CHAPA');
  QEmpLEAR.Open;

  if not QEmpLEAR.IsEmpty then begin
     SalvarXLSLEAR(QEmpLEAR, '1396_LEAR_'+ StringReplace(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue),'/','',[rfReplaceAll]));
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSINDAPARECIDA();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa, mes : String;
  valorTotal : double;
  nomeDaEmpresaCabecalho,cnpj,cod : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  mes := RetornaMesExtenso(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue));
  mes := mes + '_' + Copy(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue),9,2);
//  hasDepartamento := verificaDepartamento;
//  QDpto.Close;
//  QDpto.SQL.Clear;
//  QDpto.SQL.Add('select dept_id from EMP_DPTOS EMP where EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
//  QDpto.Open;
  //count :=  QDpto.RecordCount;
  //while not QDpto.Eof do begin
      SList := TStringList.Create;
      QBuscaPdf.Close;
      QBuscaPdf.SQL.Clear;
      QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
      QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
      QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
      QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaPdf.SQL.Add(' INNER JOIN EMP_DPTOS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR');
      QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
      QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
      QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
      //QBuscaPdf.SQL.Add(' AND EMP.DEPT_ID =' +QDptoDEPT_ID.AsString);
      QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CC.DATA_FECHA_EMP,conv.SETOR');
      if hasDepartamento then
         QBuscaPdf.SQL.Add(' ORDER BY CONV.SETOR, CONV.TITULAR, EMP.DESCRICAO')
      else
         QBuscaPdf.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
      QBuscaPdf.Open;

      if not QBuscaPdf.IsEmpty then begin
        //if((QDptoDEPT_ID.AsString = '42') or (QDptoDEPT_ID.AsString = '40') or (QDptoDEPT_ID.AsString = '43') or (QDptoDEPT_ID.AsString = '46')) then begin
          DMConexao.Q.Close;
          DMConexao.Q.SQL.Clear;
          DMConexao.Q.SQL.Text := 'SELECT CGC, NOME  FROM EMPRESAS WHERE EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue);
          DMConexao.Q.Open;
           //'IPMJ'
          //if QDptodept_id.AsString = '40' then
          //begin
          nomeDaEmpresaCabecalho := 'MUNICIPIO DE APARECIDA';
          cnpj := '46680518000114';
          cod := '2453';
          //end
          //'Prefeitura'
          //else if QDptodept_id.AsString = '42' then
          //begin
            //nomeDaEmpresaCabecalho := 'SINDICATO DOS SERVIDORES P�BLICOS MUN DE APARECIDA';
            //cnpj := '03685605000151';
            //cod := '2147';
          //end
          //'Prolar'
          //else if QDptodept_id.AsString = '43' then
          //begin
            //nomeDaEmpresaCabecalho := 'PREFEITURA MUNICIPAL DE JACAREI';
            //cnpj := '046694139000183' ;
            //cod := '1537' ;
          //end;

          //Cabe�alho
          S := '';
          S := S+ fnCompletarCom('0',1);
          S := S+ fnCompletarCom(cnpj,15);
          S := S+ fnCompletarCom(nomeDaEmpresaCabecalho,60,' ', true);
          S := S+ fnCompletarCom(' ',7,' ');
          S := S+ fnCompletarCom('1',10,'0');
          SalvarArquivo(S,'2334_SINDICATO_APARECIDA_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');
          tamanho := 2;
          valorTotal := 0;

          while not QBuscaPdf.Eof do begin
            if QBuscaPdfTOTAL.Value >= 0.00 then begin
              S := '';
              valor := '';
              dec := '.';
              try
                if Cancelar then
                  if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
                  Abort;
              end;
                S := S+ fnCompletarCom('1',1);
                S := S+ fnCompletarCom(QBuscaPdfCHAPA.AsString,10,'0');
                S := S+ fnCompletarCom(QBuscaPdfTITULAR.AsString,50,' ',true);
                S := S+ fnCompletarCom(cod,10,'0');
                valorTotal := valorTotal + QBuscaPdfTOTAL.AsFloat;
                valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaPdfTOTAL.Value));
                dec := Copy(valor,Length(valor)-2,3);
                valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
                S := S+ fnCOmpletarCom(valor,12,'0');
                S := S+ fnCOmpletarCom(IntToStr(tamanho),10,'0');
                SalvarArquivo(S,'2334_SINDICATO_APARECIDA_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');
                except on E:Exception do begin
                  Erro := True;
                  MsgErro('Erro: '+E.message);
                end;
              end;
            end;
            QBuscaPdf.Next;
            tamanho := tamanho + 1;
          end;

          //RODAP�
          S := '';
          S := S+ fnCompletarCom('9',1);
          S := S+ fnCompletarCom(IntToStr(QBuscaPdf.RecordCount),10,'0');
          valor := StringReplace(FloatToStr(valorTotal),',','',[rfReplaceAll]);
          S := S+ fnCompletarCom(valor,10,'0');
          S := S+ fnCompletarCom(' ',60,' ');
          S := S+ fnCompletarCom(IntToStr(tamanho),12,'0');
          SalvarArquivo(S,'2334_SINDICATO_APARECIDA_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');

          if not QBuscaPdf.IsEmpty then begin
              SalvarArquivo(S,'2334_SINDICATO_APARECIDA_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, true,'.txt');
          end;
      end;

//        frxPDFExport1.FileName := edtCaminho.Text + '\2334_SINDICATO_APARECIDA_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '.pdf';
//        frxReport1.ShowReport;
//        frxReport1.Export(frxPDFExport1);

  //end;
//    ProgressBar1.Position := ((QDpto.RecNo*100) div count);
//    ProgressBar1.Refresh;
//    Application.ProcessMessages;


    if not Erro then
    begin
      MsgInf('Exporta��o concluida com sucesso!');
      LimparCampos;
    end
    else
    begin
      lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
      LimparCampos;
    end;
    HabDesabBtnProcesso(False);
    FreeAndNil(SList);
    ProgressBar1.Position := 0;
    ProgressBar1.CleanupInstance;
    ProgressBar1.Refresh;
    btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSINDICATOJACAREI();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa, mes,chapas : String;
  valorTotal : double;
  nomeDaEmpresaCabecalho,cnpj,cod : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  mes := RetornaMesExtenso(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue));
  mes := mes + '_' + Copy(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue),9,2);
  hasDepartamento := verificaDepartamento;
  QDpto.Close;
  QDpto.SQL.Clear;
  QDpto.SQL.Add('select dept_id from EMP_DPTOS EMP where EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QDpto.Open;
  count :=  QDpto.RecordCount;
  while not QDpto.Eof do begin
//      SList := TStringList.Create;
//      QBuscaPdf.Close;
//      QBuscaPdf.SQL.Clear;
//      QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
//      QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
//      QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
//      QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
//      QBuscaPdf.SQL.Add(' INNER JOIN EMP_DPTOS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID AND CONV.SETOR = '+QDptoDEPT_ID.AsString+'');
//      QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID in(20,2430)');
//      QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
//      QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
//      QBuscaPdf.SQL.Add(' AND EMP.DEPT_ID =' +QDptoDEPT_ID.AsString);
//      QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CC.DATA_FECHA_EMP,conv.SETOR');
//      if hasDepartamento then
//         QBuscaPdf.SQL.Add(' ORDER BY CONV.SETOR, CONV.TITULAR, EMP.DESCRICAO')
//         else
//             QBuscaPdf.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
//      QBuscaPdf.Open;
      DMConexao.ExecuteNonQuery('update contacorrente set previamente_cancelada = '+QuotedStr('N')+' where empres_id = 2430 and PREVIAMENTE_CANCELADA = '+QuotedStr('S')+' and data_fecha_emp = '+QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue))+'');

      QBuscaConvSemChapa.SQL.Add('select chapa,titular,setor from CONVENIADOS where	empres_id = 2430 ');
		  QBuscaConvSemChapa.SQL.Add(' AND conv_id in (select distinct conv_id from CONTACORRENTE where empres_id = 2430 and data_fecha_emp = '+QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue))+')');
		  QBuscaConvSemChapa.SQL.Add(' AND (SETOR = '''' or setor is null)');

      QBuscaConvSemChapa.Open;

      if(QBuscaConvSemChapa.RecordCount > 0 ) then
      begin
        while not (QBuscaConvSemChapa.Eof) do begin
          chapas := chapas + QBuscaConvSemChapa.Fields[0].AsString + ',';
          QBuscaConvSemChapa.Next;
        end;
        MsgInf('As conveniados com as matr�culas : '+chapas+ ' est�o sem setor');
      end;

      SList := TStringList.Create;
      QBuscaPdf.Close;
      QBuscaPdf.SQL.Clear;
      QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
      QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
      QBuscaPdf.SQL.Add(' from CONTACORRENTE CC');
      QBuscaPdf.SQL.Add(' INNER JOIN CONVENIADOS CONV ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaPdf.SQL.Add(' JOIN EMP_DPTOS EMP ON EMP.DEPT_ID = '+QDptoDEPT_ID.AsString+'');
      QBuscaPdf.SQL.Add(' WHERE CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
      QBuscaPdf.SQL.Add(' AND CC.CONV_ID IN(SELECT CONV_ID FROM CONVENIADOS WHERE EMPRES_ID = 2430 and SETOR = '+QDptoDEPT_ID.AsString+')');
      QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
      QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CC.DATA_FECHA_EMP,conv.SETOR');
      if hasDepartamento then
         QBuscaPdf.SQL.Add(' ORDER BY CONV.SETOR, CONV.TITULAR, EMP.DESCRICAO')
         else
             QBuscaPdf.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
      QBuscaPdf.Open;

      if not QBuscaPdf.IsEmpty then begin
        if((QDptoDEPT_ID.AsString = '6138') or (QDptoDEPT_ID.AsString = '6136') or (QDptoDEPT_ID.AsString = '6139') or (QDptoDEPT_ID.AsString = '46')) then begin
          DMConexao.Q.Close;
          DMConexao.Q.SQL.Clear;
          DMConexao.Q.SQL.Text := 'SELECT CGC, NOME  FROM EMPRESAS WHERE EMPRES_ID IN(20,2430) ';
          DMConexao.Q.Open;
           //'IPMJ'
          if QDptodept_id.AsString = '6136' then
          begin
            nomeDaEmpresaCabecalho := 'INSTITUTO DE PREVIDENCIA DO MUNICIPIO DE JACAREI';
            cnpj := '096484134000102';
            cod := '1537';
          end
          //'Prefeitura'
          else if QDptodept_id.AsString = '6138' then
          begin
            nomeDaEmpresaCabecalho := 'PREFEITURA MUNICIPAL DE JACAREI';
            cnpj := '046694139000183';
            cod := '2147';
          end
          //'Prolar'
          else if QDptodept_id.AsString = '6139' then
          begin
            nomeDaEmpresaCabecalho := 'PREFEITURA MUNICIPAL DE JACAREI';
            cnpj := '046694139000183' ;
            cod := '2147' ;
          end;

          //Cabe�alho
          S := '';
          S := S+ fnCompletarCom('0',1);
          S := S+ fnCompletarCom(cnpj,15);
          S := S+ fnCompletarCom(nomeDaEmpresaCabecalho,60,' ', true);
          S := S+ fnCompletarCom(' ',7,' ');
          S := S+ fnCompletarCom('1',10,'0');
          SalvarArquivo(S,'SINDICATO_JACAREI_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');
          tamanho := 2;
          valorTotal := 0;

          while not QBuscaPdf.Eof do begin
            if QBuscaPdfTOTAL.Value >= 0.00 then begin
              S := '';
              valor := '';
              dec := '.';
              try
                if Cancelar then
                  if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
                  Abort;
              end;
                S := S+ fnCompletarCom('1',1);
                S := S+ fnCompletarCom(QBuscaPdfCHAPA.AsString,10,'0');
                S := S+ fnCompletarCom(QBuscaPdfTITULAR.AsString,50,' ',true);
                S := S+ fnCompletarCom(cod,10,'0');
                valorTotal := valorTotal + QBuscaPdfTOTAL.AsFloat;
                valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaPdfTOTAL.Value));
                dec := Copy(valor,Length(valor)-2,3);
                valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
                S := S+ fnCOmpletarCom(valor,12,'0');
                S := S+ fnCOmpletarCom(IntToStr(tamanho),10,'0');
                SalvarArquivo(S,'2430_SINDICATO_JACAREI_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');
                except on E:Exception do begin
                  Erro := True;
                  MsgErro('Erro: '+E.message);
                end;
              end;
            end;
            QBuscaPdf.Next;
            tamanho := tamanho + 1;
          end;

          //RODAP�
          S := '';
          S := S+ fnCompletarCom('9',1);
          S := S+ fnCompletarCom(IntToStr(QBuscaPdf.RecordCount),10,'0');
          valor := StringReplace(FloatToStr(valorTotal),',','',[rfReplaceAll]);
          S := S+ fnCompletarCom(valor,10,'0');
          S := S+ fnCompletarCom(' ',60,' ');
          S := S+ fnCompletarCom(IntToStr(tamanho),12,'0');
          SalvarArquivo(S,'SINDICATO_JACAREI_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, false,'.txt');

          if not QBuscaPdf.IsEmpty then begin
              SalvarArquivo(S,'SINDICATO_JACAREI_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '_' + mes, true,'.txt');
          end;
        end;

          frxPDFExport1.FileName := edtCaminho.Text + '\SINDICATO_JACAREI_' + StringReplace(StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]),'-','',[rfReplaceAll]) + '.pdf';
          frxReport1.ShowReport;
          frxReport1.Export(frxPDFExport1);

      end;
      ProgressBar1.Position := ((QDpto.RecNo*100) div count);
      ProgressBar1.Refresh;
      Application.ProcessMessages;
      QDpto.Next;
   end;

    if not Erro then
    begin
      MsgInf('Exporta��o concluida com sucesso!');
      LimparCampos;
    end
    else
    begin
      lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
      LimparCampos;
    end;
    HabDesabBtnProcesso(False);
    FreeAndNil(SList);
    ProgressBar1.Position := 0;
    ProgressBar1.CleanupInstance;
    ProgressBar1.Refresh;
    btnExportar.Enabled := true;
end;

function TFrmExportEstabPadrao.verificaDepartamento() : Boolean;
begin
   if (QDepart.RecordCount <> 0) then
      result := True
   else
      result := False;
end;

procedure TFrmExportEstabPadrao.LayoutDROGACLIN();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  hasDepartamento : Boolean;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' select EMP.DESCRICAO AS SETOR, CONV.TITULAR, CONV.CHAPA, SUM(DEBITO - CREDITO) AS SUM');
  DMConexao.Q.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.Q.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.Q.SQL.Add(' LEFT JOIN EMP_DPTOS EMP ON  EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR ');
  DMConexao.Q.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  DMConexao.Q.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  DMConexao.Q.SQL.Add(' GROUP BY CONV.SETOR,CONV.TITULAR, CONV.CHAPA, EMP.DESCRICAO, CC.DATA_FECHA_EMP');
  if hasDepartamento then
     DMConexao.Q.SQL.Add(' ORDER BY conv.setor, CONV.TITULAR,EMP.DESCRICAO')
     else
     DMConexao.Q.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
     SalvarXLS(DMConexao.Q, '45_DROGACLIN');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;


procedure TFrmExportEstabPadrao.LayoutMubea();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec,empresa : String;
begin

  empresa := '334_MUBEA';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,51,' ',True);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        S := S+ fnCompletarCom(RightStr(valor,6),0);
        S := S+ fnCOmpletarCom(valor,6,' ');
        SalvarArquivo(S,empresa, false,'.txt');;
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;



  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;




procedure TFrmExportEstabPadrao.LayoutViacaoJacarei();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec,empresa : String;
begin
  if lkpEmp.KeyValue = 310 then begin
    empresa := '310_VIACAO_DB';
  end else if lkpEmp.KeyValue = 358 then begin
    empresa := '358_VIACAO_PLANTAOCARD';
  end else if lkpEmp.KeyValue = 240 then begin
    empresa := '240_JTU_DROGABELLA';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA AS CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        if(lkpEmp.KeyValue = 240) then
          S := S+ fnCompletarCom('499',3)
        else
          S := S+ fnCompletarCom('476',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCompletarCom(RightStr(valor,5),0);
        S := S+ fnCOmpletarCom(valor,5,'0');
        SalvarArquivo(S,empresa, false,'.txt');;
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');

  end;
  if lkpEmp.KeyValue = 358 then begin
    //REGI�O DE RELAT�RIO POR DEPARTAMENTO
    QBusca.Close;
    QBusca.SQL.Clear;
    QBusca.SQL.Add(' SELECT CONV.CHAPA, CONV.TITULAR,');
    QBusca.SQL.Add(' coalesce(sum(CASE WHEN coalesce(cc.receita, ''S'') = ''S'' THEN cc.debito-cc.credito ELSE 0 END), 0) AS COM_RECEITA,');
    QBusca.SQL.Add(' coalesce(sum(CASE WHEN coalesce(cc.receita, ''N'') = ''N'' THEN cc.debito-cc.credito ELSE 0 END), 0) AS SEM_RECEITA,');
    QBusca.SQL.Add(' COALESCE(SUM(cc.debito-cc.credito), 0) AS TOTAL FROM CONVENIADOS CONV INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
    QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = 358 and conv.conv_id in(select conv_id from CONVENIADOS where empres_id = 358 and setor = 6017)');
    QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
    QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, Conv.TITULAR');
    QBusca.Open;
    QDepartamento.Close;
    QDepartamento.SQL.Clear;
    QDepartamento.SQL.Add(' select DESCRICAO AS DEPARTAMENTO from EMP_DPTOS where DEPT_ID = 6017');
    QDepartamento.Open;
    count := QBusca.RecordCount;
    frxPDFExport2.FileName := edtCaminho.Text + '\EXTRATO_SINTRADETE.pdf';
    frxReport2.ShowReport;
    frxReport2.Export(frxPDFExport2);
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutADATEX();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec,empresa : String;
begin
  if lkpEmp.KeyValue = 314 then begin
    empresa := '314_ADATEX';
  end else if lkpEmp.KeyValue = 735 then begin
    empresa := '735_ADATEX';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,12,'0');
        S := S+ fnCompletarCom('279',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,22,'0');
        SalvarArquivo(S,empresa, false,'.txt');;
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;


procedure TFrmExportEstabPadrao.LayoutCENTERVALE();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin
  if lkpEmp.KeyValue = 318 then begin
    empresa := '318_CENTERVALE';
  end else if lkpEmp.KeyValue = 391 then begin
    empresa := '391_CENTERVALE';
  end else if lkpEmp.KeyValue = 392 then begin
    empresa := '392_CENTERVALE';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR order by conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('5900',4);
        S := S+ fnCompletarCom(';',1);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ valor;
        S := S+ fnCompletarCom(';',1);
        SalvarArquivo(S,empresa, false,'.csv');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.csv');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;


procedure TFrmExportEstabPadrao.LayoutSATLOG();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  hasDepartamento : Boolean;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' select EMP.DESCRICAO AS SETOR, CONV.TITULAR, CONV.CHAPA, SUM(DEBITO - CREDITO) AS SUM');
  DMConexao.Q.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.Q.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.Q.SQL.Add(' LEFT JOIN EMP_DPTOS EMP ON  EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR ');
  DMConexao.Q.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  DMConexao.Q.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  DMConexao.Q.SQL.Add(' GROUP BY CONV.SETOR,CONV.TITULAR, CONV.CHAPA, EMP.DESCRICAO, CC.DATA_FECHA_EMP');
  if hasDepartamento then
     DMConexao.Q.SQL.Add(' ORDER BY conv.setor, CONV.TITULAR,EMP.DESCRICAO')
     else
     DMConexao.Q.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
     SalvarXLS(DMConexao.Q, '438_SATLOG');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;


procedure TFrmExportEstabPadrao.LayoutPGR();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  hasDepartamento : Boolean;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' select EMP.DESCRICAO AS SETOR, CONV.TITULAR, CONV.CHAPA, SUM(DEBITO - CREDITO) AS SUM');
  DMConexao.Q.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.Q.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.Q.SQL.Add(' LEFT JOIN EMP_DPTOS EMP ON  EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR ');
  DMConexao.Q.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  DMConexao.Q.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  DMConexao.Q.SQL.Add(' GROUP BY CONV.SETOR,CONV.TITULAR, CONV.CHAPA, EMP.DESCRICAO, CC.DATA_FECHA_EMP');
  if hasDepartamento then
     DMConexao.Q.SQL.Add(' ORDER BY conv.setor, CONV.TITULAR,EMP.DESCRICAO')
     else
     DMConexao.Q.SQL.Add(' ORDER BY CONV.TITULAR,EMP.DESCRICAO');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
     SalvarXLS(DMConexao.Q, '437_PGR');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;




procedure TFrmExportEstabPadrao.LayoutPROSERV();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBuscaPdf.Close;
  QBuscaPdf.SQL.Clear;
  QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, ''GERAL '' AS DESCRICAO, 1 AS DEPT_ID, CONVERT(VARCHAR,cc.DATAVENDA,103) AS DATA_FECHA_EMP, (DEBITO - CREDITO) AS TOTAL');
  QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
  QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S'' ORDER BY CONV.TITULAR,conv.CHAPA, CC.DATAVENDA');
  QBuscaPdf.Open;
  count := QBuscaPdf.RecordCount;
  SList := TStringList.Create;

  while not QBuscaPdf.Eof do begin
    if QBuscaPdfTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaPdfCHAPA.AsString,15,'0');
        S := S+ fnCompletarCom(QBuscaPdfTITULAR.AsString,61,' ',true);
        valor := FormatFloat('#,##0.00',QBuscaPdfTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,'0');
        S := S+ fnCOmpletarCom(Copy(QBuscaPdfDATA_FECHA_EMP.AsString,7,4) + Copy(QBuscaPdfDATA_FECHA_EMP.AsString,4,2) + Copy(QBuscaPdfDATA_FECHA_EMP.AsString,1,2),9);
        SalvarArquivo(S,'342_PRO-SERV', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBuscaPdf.Next;
    ProgressBar1.Position := ((QBuscaPdf.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBuscaPdf.IsEmpty then begin
     SalvarArquivo(S,'342_PRO-SERV', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutPOLIEDRO();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin
 // if lkpEmp.KeyValue = 122 then begin
  //  empresa := '122_ALOJA_POLIEDRO';
 // end else if lkpEmp.KeyValue = 123 then begin
 //   empresa := '123_POLIEDRO_NAS';
 // end else if lkpEmp.KeyValue = 125 then begin
 //   empresa := '125_POLIEDRO_MMI';
 // end else if lkpEmp.KeyValue = 126 then begin
 //   empresa := '126_EDITORA_POLIEDRO';
//  end else if lkpEmp.KeyValue = 41 then begin
 //   empresa := '41_CURSO_POLIEDRO';
 // end else if lkpEmp.KeyValue = 124 then begin
    empresa := 'GRUPO_POLIEDRO';
 // end;
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID in (122,123, 125, 126, 41, 124)');
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR,CONV.EMPRES_ID order by CONV.EMPRES_ID, conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom(Copy(FormatDateTime('dd;mm;yyyy',lkpDataFechamento.KeyValue),4,7),7);
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('2195',4);
        S := S+ fnCompletarCom(';',1);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        valor := StringReplace(valor,',','.',[rfReplaceAll]);
        S := S+ valor;
        S := S+ fnCompletarCom(';',1);
        SalvarArquivo(S,empresa, false,'.csv');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.csv');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSUPERALEAN();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa : String;
begin

  if lkpEmp.KeyValue = 1158 then begin
    empresa := '1158_SUPER_SAO_LOURENCO'
  end else if lkpEmp.KeyValue = 1161 then begin
    empresa := '1161_SUPER_ITANHANDU'
  end else if lkpEmp.KeyValue = 1682 then begin
    empresa := '1682_SUPER_ALEAN'
  end else if lkpEmp.KeyValue = 1683 then begin
    empresa := '1683_SUPER_ALEAN'
  end else if lkpEmp.KeyValue = 1684 then begin
    empresa := '1684_SUPER_ALEAN'
  end else if lkpEmp.KeyValue = 1688 then begin
    empresa := '1688_SUPER_ALEAN'
  end else if lkpEmp.KeyValue = 1833 then begin
    empresa := '1833_SUPER_ALEAN';
  end else if lkpEmp.KeyValue = 1895 then begin
    empresa := '1895_SUPER_ALEAN';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.TITULAR, conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom('599',5,' ');
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,' ');
        S := S+ fnCOmpletarCom('0,00',10,' ');
        S := S+ fnCOmpletarCom('0,00',10,' ');
        S := S+ fnCOmpletarCom('0,00',10,' ');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    tamanho := tamanho + 1;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  hasDepartamento := verificaDepartamento;
  QBuscaPdf.Close;
  QBuscaPdf.SQL.Clear;
  QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, ''GERAL'' AS DESCRICAO,  1 AS DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
  QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
  QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
  QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaPdf.SQL.Add(' LEFT JOIN EMP_DPTOS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR');
  QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA = ''N''');
  QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, CC.DATA_FECHA_EMP');
  QBuscaPdf.SQL.Add(' ORDER BY conv.TITULAR, conv.CHAPA');
  QBuscaPdf.Open;

  if not QBuscaPdf.IsEmpty then begin
    frxPDFExport1.FileName := edtCaminho.Text + '\' + empresa + '.pdf';
    frxReport1.PrepareReport(true);
    frxReport1.Export(frxPDFExport1);

    //frxReport1.ShowReport;
    //frxReport1.Export(frxPDFExport1);
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSUPERALEANDEP();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa : String;
begin

 if lkpEmp.KeyValue = 1159 then begin
    empresa := '1159_SUPER_CACHOEIRA_';
  end else if lkpEmp.KeyValue = 1160 then begin
    empresa := '1160_SUPER_CRUZEIRO_';
  end else if lkpEmp.KeyValue = 1682 then begin
    empresa := '1682_ FLV_PRODUTOR_LTDA_(PC)';
  end else if lkpEmp.KeyValue = 1683 then begin
    empresa := '1683_RENATA_B_NUNES_APOIO_ADMINISTRATIVO_EPP';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QDpto.Close;
  QDpto.SQL.Clear;
  QDpto.SQL.Add('select dept_id from EMP_DPTOS EMP where EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QDpto.Open;
  count :=  QDpto.RecordCount;

  while not QDpto.Eof do begin
      SList := TStringList.Create;
      QBuscaPdf.Close;
      QBuscaPdf.SQL.Clear;
      QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
      QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
      QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
      QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaPdf.SQL.Add(' INNER JOIN EMP_DPTOS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR');
      QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
      QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
      QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
      QBuscaPdf.SQL.Add(' AND EMP.DEPT_ID =' +QDptoDEPT_ID.AsString);
      QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CC.DATA_FECHA_EMP');
      QBuscaPdf.SQL.Add(' ORDER BY conv.CHAPA, conv.TITULAR, EMP.DESCRICAO');
      QBuscaPdf.Open;

      if not QBuscaPdf.IsEmpty then begin
        while not QBuscaPdf.Eof do begin
            if QBuscaPdfTOTAL.Value > 0.00 then begin
              S := '';
              valor := '';
              dec := '.';
              try
                if Cancelar then
                  if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
                  Abort;
              end;
              S := S+ fnCompletarCom(QBuscaPdfCHAPA.AsString,6,'0');
              S := S+ fnCompletarCom('599',5,' ');
              valor := FormatFloat('#,##0.00',QBuscaPdfTOTAL.Value);
              dec := Copy(valor,Length(valor)-2,3);
              valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
              S := S+ fnCOmpletarCom(valor,12,' ');
              S := S+ fnCOmpletarCom('0,00',10,' ');
              S := S+ fnCOmpletarCom('0,00',10,' ');
              S := S+ fnCOmpletarCom('0,00',10,' ');
              SalvarArquivo(S,empresa + StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]), false,'.txt');
                except on E:Exception do begin
                  Erro := True;
                  MsgErro('Erro: '+E.message);
                end;
              end;
            end;
            QBuscaPdf.Next;
        end;

        if not QBuscaPdf.IsEmpty then begin
          SalvarArquivo(S,empresa + StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]), true,'.txt');
        end;

      end;

      frxPDFExport1.FileName := edtCaminho.Text + '\' + empresa + StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]) + '.pdf';
      frxReport1.ShowReport;
      frxReport1.Export(frxPDFExport1);

      ProgressBar1.Position := ((QDpto.RecNo*100) div count);
      ProgressBar1.Refresh;
      Application.ProcessMessages;
      QDpto.Next;
   end;

   if not Erro then
   begin
     MsgInf('Exporta��o concluida com sucesso!');
     LimparCampos;
   end
   else
   begin
     lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
     LimparCampos;
   end;
    HabDesabBtnProcesso(False);
    FreeAndNil(SList);
    ProgressBar1.Position := 0;
    ProgressBar1.CleanupInstance;
    ProgressBar1.Refresh;
    btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutTOTAL_CONSTRUTORA();
var
  Erro,hasDepartamento : Boolean;
  count, tamanho : integer;
  S, valor, dec, dataIni, dataFim : String;
begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  tamanho := 1;

  //OBTEM O PRIMEIRO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + '), 0)),103)';
  DMConexao.Q.Open;
  dataIni := DMConexao.Q.Fields[0].Value;

  //OBTEM O ULTIMO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(ms ,-3 ,DATEADD(mm, DATEDIFF(mm, 0,'+  QuotedStr(dataIni) +') + 1, 0))),103)';
  DMConexao.Q.Open;
  dataFim := DMConexao.Q.Fields[0].Value;
  dataFim := StringReplace(dataFim,'/','',[rfReplaceAll]);
  dataIni := StringReplace(dataIni,'/','',[rfReplaceAll]);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(IntToStr(tamanho),6,'0');
        S := S+ fnCompletarCom('00005',5);
        S := S+ fnCompletarCom(Copy(dataIni,1,4) + Copy(dataIni,7,2),6);
        S := S+ fnCompletarCom(Copy(dataFim,1,4) + Copy(dataFim,7,2),6);
        S := S+ fnCompletarCom('54',16,'0');
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,'0');
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom('F',1);
        SalvarArquivo(S,'1363_TOTAL_CONSTRUTORA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    tamanho := tamanho + 1;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'1363_TOTAL_CONSTRUTORA', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;

  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutLATECOERE();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('591',3);
        S := S+ fnCompletarCom(';',1);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,Length(valor));
        SalvarArquivo(S,'575_LATECOERE', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'575_LATECOERE', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutVILLAREAL();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 486 then begin
    empresa := '486_VILLARREAL_TAUBATE';
    codigo := '0008';
  end else if lkpEmp.KeyValue = 487 then begin
    empresa := '487_VILLARREAL_CRUZEIRO';
    codigo := '0001';
  end else if lkpEmp.KeyValue = 1590 then begin
    empresa := '1590_SPANI_LORENA';
    codigo := '0017';
  end else if lkpEmp.KeyValue = 488 then begin
    empresa := '488_VILLARREAL_EMA';
    codigo := '0000';
  end else if lkpEmp.KeyValue = 489 then begin
    empresa := '489_VILLARREAL_CTA';
    codigo := '0006';
  end else if lkpEmp.KeyValue = 490 then begin
    empresa := '490_VILLARREAL_CENTRO';
    codigo := '0007';
  end;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom('165',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSPANI();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 491 then begin
    empresa := '491_SPANI_TAUBATE';
    codigo := '0012';
  end else if lkpEmp.KeyValue = 492 then begin
    empresa := '492_SPANI_ATIBAIA';
    codigo := '0010';
  end else if lkpEmp.KeyValue = 493 then begin
    empresa := '493_SPANI_GUARA';
    codigo := '0005';
  end else if lkpEmp.KeyValue = 494 then begin
    empresa := '494_SPANI_MOGI';
    codigo := '0004';
  end else if lkpEmp.KeyValue = 495 then begin
    empresa := '495_SPANI_SJCAMPOS';
    codigo := '0003';
  end else if lkpEmp.KeyValue = 728 then begin
    empresa := '728_SPANI_RESENDE';
    codigo := '0014';
  end else if lkpEmp.KeyValue = 1227 then begin
    empresa := '1227_SPANI_PINDA';
    codigo := '0015';
  end else if lkpEmp.KeyValue = 1447 then begin
    empresa := '1447_SPANI_CARAGUA';
    codigo := '0011';
  end else if lkpEmp.KeyValue = 1879 then begin
    empresa := '1879_SPANI_BRAGANCA';
    codigo := '0021';
  end else if lkpEmp.KeyValue = 1947 then begin
    empresa := '1947_SPANI_CASSIANO';
    codigo := '0019';
  end else if lkpEmp.KeyValue = 727 then begin
    empresa := '727_SPANI_VOLTA_REDONDA';
    codigo := '0000';
  end else if lkpEmp.KeyValue = 2037 then begin
    empresa := '2037_SPANI_JACAREI';
    codigo := '0022';
  end;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom('165',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;



procedure TFrmExportEstabPadrao.LayoutEuroquadros();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo,data : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 1706 then begin
    empresa := '1706_EUROQUADROS';
    codigo := '01';
  end
  else if lkpEmp.KeyValue = 1707 then
  begin
    empresa := '1707_EUROLOGISTICA';
    codigo := '02';
  end
  else if lkpEmp.KeyValue = 2662 then begin
    empresa := '2662_EUROLOGISTICA';
    codigo := '01';
  end
  else if lkpEmp.KeyValue = 2663 then begin
    empresa := '2663_EUROQUADROS';
    codigo := '01';
  end
  else if lkpEmp.KeyValue = 2664 then begin
    empresa := '2664_EUROLOGISTICA';
    codigo := '02';
  end
  else begin
    empresa := '2665_EUROLOGISTICA';
    codigo := '01';
  end;


  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT REPLACE(CONVERT(VARCHAR(10),'+ QuotedStr(FormatDateTime('dd/MM/yyyy',lkpDataFechamento.KeyValue)) + ',103),''/'','''')';
  DMConexao.Q.Open;
  data := DMConexao.Q.Fields[0].Value;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,2);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        if (AnsiCompareText(empresa,'1706') = 0) or (AnsiCompareText(empresa,'1707') = 0) then begin
          S := S+ fnCompletarCom('473',3);
        end
        else begin
          S := S+ fnCompletarCom('479',3);
        end;
        S := S+ fnCompletarCom(data,8);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,5,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end
  else begin
    MsgInf('Empresa sem Fechamento!');
  end;
  
  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutDepositoCardoso();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 1512 then begin
    empresa := '1512_DEPOSITO_CARDOSO';
    codigo := '0063';
  end;

   if lkpEmp.KeyValue = 2600 then begin
    empresa := '2600_DEPOSITO_CARDOSO';
    codigo := '0063';
  end;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom('001',3);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');

        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSPANI_UNIFICADO();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo: String;
  dia,mes,ano : Word;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  mes := MonthOf(StrToDateTime(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  ano := YearOf(StrToDateTime(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  dia := 13; // DIA DO FECHAMENTO DO SPANI DE ATIBAIA

  hasDepartamento := verificaDepartamento;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(' select CONV.EMPRES_ID,CONV.CHAPA,CONV.CPF, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  DMConexao.AdoQry.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.AdoQry.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.AdoQry.SQL.Add(' WHERE CONV.EMPRES_ID IN (727,728,1447,1590,1879,1947,2037,2194,2195,2196,2223,2224,2225,2226,2227,2228,2229,2230,2231,2232,2233,2234,2235,2236,2237,2238,2239,2240,2241,2247,2248,2249,2250,2251,2336,2368,2369,2370,2371,2372,2570,2684,2704) ');
  DMConexao.AdoQry.SQL.Add(' AND (CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) +' or CC.DATA_FECHA_EMP = '+QuotedStr(DateTimeToStr(EncodeDateTime(ano,mes,dia,0,0,0,000)))+')' );
  DMConexao.AdoQry.SQL.Add(' GROUP BY CONV.EMPRES_ID,CONV.CHAPA,CONV.CPF, CONV.TITULAR ORDER BY CONV.EMPRES_ID,conv.CHAPA,conv.TITULAR');
  DMConexao.AdoQry.Open;
  count := DMConexao.AdoQry.RecordCount;
  SList := TStringList.Create;

  while not DMConexao.AdoQry.Eof do begin

    if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2232 then begin
      codigo := '0014';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 727 then begin
      codigo := '0013';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2228 then begin
      codigo := '0010'
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2234 then begin
      codigo := '0011';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2229 then begin
      codigo := '0005';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2235 then begin
      codigo := '0017';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2230 then begin
      codigo := '0004';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2233 then begin
      codigo := '0015';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2231 then begin
      codigo := '0003';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2227 then begin
      codigo := '0012';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2236 then begin
      codigo := '0021';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2237 then begin
      codigo := '0019';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2238 then begin
      codigo := '0022';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2239 then begin
      codigo := '0024';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2240 then begin
      codigo := '0025';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2241 then begin
      codigo := '0026';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2224 then begin
      codigo := '0001';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2226 then begin
      codigo := '0006';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2223 then begin
      codigo := '0008';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2225 then begin
      codigo := '0000';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2247 then begin
      codigo := '0030';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2248 then begin
      codigo := '0031';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2249 then begin
      codigo := '0032';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2250 then begin
      codigo := '0033';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2251 then begin
      codigo := '0036';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2336 then begin
      codigo := '0035';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2371 then begin
      codigo := '0029';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2372 then begin
      codigo := '0037';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2368 then begin
      codigo := '0027';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2369 then begin
      codigo := '0023';
    end else if DMConexao.AdoQry.FieldByName('EMPRES_ID').AsInteger = 2370 then begin
      codigo := '0028';
    end;


    if DMConexao.AdoQry.FieldByName('TOTAL').Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom(DMConexao.AdoQry.FieldByName('CHAPA').AsString,5,'0');
        S := S+ fnCompletarCom('165',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',DMConexao.AdoQry.FieldByName('TOTAL').Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,'SPANI', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    DMConexao.AdoQry.Next;
    ProgressBar1.Position := ((DMConexao.AdoQry.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not DMConexao.AdoQry.IsEmpty then begin
    SalvarXLS(DMConexao.AdoQry, 'SPANI');
  end;

  if not DMConexao.AdoQry.IsEmpty then begin
      SalvarArquivo(S,'SPANI', true,'.txt');
  end;

  ExportarExcelSpani;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;



procedure TFrmExportEstabPadrao.LayoutSPANI_NOVO();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 2227 then begin
    empresa := '2227_SPANI_TAUBATE';
    codigo := '0012';
  end else if lkpEmp.KeyValue = 2228 then begin
    empresa := '2228_SPANI_ATIBAIA';
    codigo := '0010';
  end else if lkpEmp.KeyValue = 2229 then begin
    empresa := '2229_SPANI_GUARA';
    codigo := '0005';
  end else if lkpEmp.KeyValue = 2230 then begin
    empresa := '2230_SPANI_MOGI';
    codigo := '0004';
  end else if lkpEmp.KeyValue = 2231 then begin
    empresa := '2231_SPANI_SJCAMPOS';
    codigo := '0003';
  end else if lkpEmp.KeyValue = 2232 then begin
    empresa := '2232_SPANI_RESENDE';
    codigo := '0014';
  end else if lkpEmp.KeyValue = 2233 then begin
    empresa := '2233_SPANI_PINDA';
    codigo := '0015';
  end else if lkpEmp.KeyValue = 2234 then begin
    empresa := '2234_SPANI_CARAGUA';
    codigo := '0011';
  end else if lkpEmp.KeyValue = 2236 then begin
    empresa := '2236_SPANI_BRAGANCA';
    codigo := '0021';
  end else if lkpEmp.KeyValue = 2237 then begin
    empresa := '2237_SPANI_ALVORADA';
    codigo := '0019';
  end else if lkpEmp.KeyValue = 1947 then begin
    empresa := '1947_SPANI_CASSIANO';
    codigo := '0019';
  end else if lkpEmp.KeyValue = 727 then begin
    empresa := '727_SPANI_VOLTA_REDONDA';
    codigo := '0013';
  end else if lkpEmp.KeyValue = 2238 then begin
    empresa := '2238_SPANI_JACAREI';
    codigo := '0022';
  end else if lkpEmp.KeyValue = 2336 then begin
    empresa := '2336_SPANI_BUTANTA';
    codigo := '0035';
  end else if lkpEmp.KeyValue = 2250 then begin
    empresa := '2250_SPANI_MARILIA';
    codigo := '0033';
  end else if lkpEmp.KeyValue = 2248 then begin
    empresa := '2248_SPANI_JD_NASCENTE';
    codigo := '0031';
  end else if lkpEmp.KeyValue = 2249 then begin
    empresa := '2249_SPANI_PQ_SAOGERALDO';
    codigo := '0032';
  end else if lkpEmp.KeyValue = 2240 then begin
    empresa := '2240_SPANI_VL_FORMOSA';
    codigo := '0025';
  end else if lkpEmp.KeyValue = 2239 then begin
    empresa := '2239_SPANI_JD_ODETE';
    codigo := '0024';
  end else if lkpEmp.KeyValue = 2241 then begin
    empresa := '2241_SPANI_VL_PRUDENTE';
    codigo := '0026';
  end else if lkpEmp.KeyValue = 2371 then begin
    empresa := '2371_SPANI_DIADEMA';
    codigo := '0029';
  end else if lkpEmp.KeyValue = 2372 then begin
    empresa := '2372_SPANI_RIO_CLARO';
    codigo := '0037';
  end else if lkpEmp.KeyValue = 2368 then begin
    empresa := '2368_SPANI_MAUA';
    codigo := '0027';
  end else if lkpEmp.KeyValue = 2369 then begin
    empresa := '2369_SPANI_BRAGANCA_II';
    codigo := '0023';
  end else if lkpEmp.KeyValue = 2370 then begin
    empresa := '2370_SPANI_AMERICANA';
    codigo := '0028';
  end else if lkpEmp.KeyValue = 2251 then begin
    empresa := '2251_SPANI_MAUA_II';
    codigo := '0036';
  end else if lkpEmp.KeyValue = 2247 then begin
    empresa := '2247_SPANI_GUARULHOS';
    codigo := '0030';
  end;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom('165',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutVILLAREAL_NOVO();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa, codigo : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 2223 then begin
    empresa := '2223_VILLARREAL_TAUBATE';
    codigo := '0008';
  end else if lkpEmp.KeyValue = 2224 then begin
    empresa := '2224_VILLARREAL_CRUZEIRO';
    codigo := '0001';
  end else if lkpEmp.KeyValue = 2235 then begin
    empresa := '2235_SPANI_LORENA';
    codigo := '0017';
  end else if lkpEmp.KeyValue = 2225 then begin
    empresa := '2225_VILLARREAL_EMA';
    codigo := '0000';
  end else if lkpEmp.KeyValue = 2226 then begin
    empresa := '2226_VILLARREAL_CTA';
    codigo := '0006';
  end else if lkpEmp.KeyValue = 490 then begin
    empresa := '490_VILLARREAL_CENTRO';
    codigo := '0007';
  end;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(codigo,4);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom('165',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutPREF_PIRASSUNUNGA();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,11,'0');
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,25,'0');
        S := S+ fnCOmpletarCom('1',1);
        SalvarArquivo(S,'2_PREFEITURA_PIRASSUNUNGA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'2_PREFEITURA_PIRASSUNUNGA', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutMEXICHEM();
var S, valor, dec, empresa, data : String;
      Erro,hasDepartamento : Boolean;
    count, tamanho: integer;
begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  //Vendas com Receita
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' AND CC.RECEITA = ''S''');
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  tamanho := 1;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  //DMConexao.Q.SQL.Text := 'SELECT CONCAT(MONTH('+ QuotedStr(FormatDateTime('dd/MM/yyyy',lkpDataFechamento.KeyValue)) + '), YEAR('+ QuotedStr(FormatDateTime('dd/MM/yyyy',lkpDataFechamento.KeyValue)) +'))';
  DMConexao.Q.SQL.Text := 'SELECT REPLACE(RIGHT(CONVERT(VARCHAR(10),'+ QuotedStr(FormatDateTime('dd/MM/yyyy',lkpDataFechamento.KeyValue)) + ',103),7),''/'','''')';
  DMConexao.Q.Open;
  data := DMConexao.Q.Fields[0].Value;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('"mvtobene',9);
        S := S+ fnCompletarCom(IntToStr(tamanho),5,'0');
        S := S+ fnCompletarCom('001001',6);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,' ');
        S := S+ fnCompletarCom('0000',4);
        S := S+ fnCompletarCom('13'+ data,8);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,11,' ');
        S := S+ fnCOmpletarCom('00000001',8);
        S := S+ fnCOmpletarCom('0',41,'0');
        S := S+ fnCOmpletarCom('"',1);
        SalvarArquivo(S,'132_MEXICHEM_COM_RECEITA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    tamanho := tamanho + 1;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'132_MEXICHEM_COM_RECEITA', true,'.txt');
  end;

  SList := TStringList.Create;
  ProgressBar1.Position := 0;
  //Vendas sem Receita
  hasDepartamento := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' AND CC.RECEITA = ''N''');
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  tamanho := 1;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('"mvtobene',9);
        S := S+ fnCompletarCom(IntToStr(tamanho),5,'0');
        S := S+ fnCompletarCom('001001',6);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,' ');
        S := S+ fnCompletarCom('0000',4);
        S := S+ fnCompletarCom('13'+ data,8);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,11,' ');
        S := S+ fnCOmpletarCom('00000001',8);
        S := S+ fnCOmpletarCom('0',41,'0');
        S := S+ fnCOmpletarCom('"',1);
        SalvarArquivo(S,'132_MEXICHEM_SEM_RECEITA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    tamanho := tamanho + 1;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'132_MEXICHEM_SEM_RECEITA', true,'.txt');
  end;

  //ARQUIVO XLS
  hasDepartamento := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR,');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''S'') = ''S'' then cc.debito-cc.credito else 0 end),0) as COM_RECEITA,');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''N'') = ''N'' then cc.debito-cc.credito else 0 end),0) as SEM_RECEITA,');
  QBusca.SQL.Add(' COALESCE(SUM(cc.debito-cc.credito),0) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.TITULAR, conv.CHAPA');
  QBusca.Open;

  if not QBusca.IsEmpty then begin
     SalvarXLS(QBusca,'132_MEXICHEM');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutPARKER();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin

  if lkpEmp.KeyValue = 127 then
    empresa := '127_PARKER_FILTROS'
   else
    empresa := '128_PARKER_HANNIFFIN';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,Length(QBuscaCHAPA.AsString));
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('3704',4);
        S := S+ fnCompletarCom(';',1);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,Length(valor));
        SalvarArquivo(S,empresa, false,'.csv');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.csv');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutEATON();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, data : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  //OBTEM O ULTIMO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text :=  'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(ms ,-3 ,DATEADD(mm, DATEDIFF(mm, 0,'+  QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) +') + 1, 0))),103)';
  DMConexao.Q.Open;
  data := DMConexao.Q.Fields[0].Value;
  data := StringReplace(data,'/','',[rfReplaceAll]);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('E',3,' ',true);
        S := S+ fnCompletarCom('015',4,' ',true);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        S := S+ fnCompletarCom(' ',3,' ');
        S := S+ fnCompletarCom('D82 C',5);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,14,'0');
        S := S+ fnCompletarCom('M',1);
        S := S+ fnCompletarCom(data,8);
        S := S+ fnCompletarCom(' DROGABELLA',11);
        if IntToStr(lkpEmp.KeyValue) = '356' then
           SalvarArquivo(S,'356_EATON_ALARMSEG', false,'.txt')
           else
           SalvarArquivo(S,'171_EATON', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     if IntToStr(lkpEmp.KeyValue) = '356' then
        SalvarArquivo(S,'356_EATON_ALARMSEG', true,'.txt')
        else
        SalvarArquivo(S,'171_EATON', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutUNIVAP();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom(' ',7,' ');
        S := S+ fnCompletarCom('551',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,22,'0');
        SalvarArquivo(S,'118_UNIVAP', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'118_UNIVAP', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutULTIMATE();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        S := S+ fnCompletarCom('23',2);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,14,'0');
        SalvarArquivo(S,'354_ULTIMATE', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'354_ULTIMATE', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutCETEC();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin

  if lkpEmp.KeyValue = 1088 then begin
    empresa := '1088_CETEC_TAUBATE';
  end else if lkpEmp.KeyValue = 182 then begin
    empresa := '182_CETEC';
  end else if lkpEmp.KeyValue = 1887 then begin
    empresa := '1887_CETEC';
  end else if lkpEmp.KeyValue = 1888 then begin
    empresa := '1888_CETEC';
  end else if lkpEmp.KeyValue = 1889 then begin
    empresa := '1889_CETEC';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,6,' ');
        S := S+ fnCompletarCom('01',2);

        SalvarArquivo(S,empresa, false,'.txt');


        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     //SalvarArquivo(S,'182_CETEC', true,'.txt');
     SalvarArquivo(S,empresa, true,'.txt');
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutHITACHI();
var
  Erro, hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,5,'0');
        S := S+ fnCompletarCom(' ',7,' ');
        S := S+ fnCompletarCom('3',1);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,24,'0');
        SalvarArquivo(S,'201_HITACHI', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'201_HITACHI', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutHEATCRAFT();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('001',3);
        S := S+ fnCompletarCom('003',3);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,11,'0');
        SalvarArquivo(S,'221_HEATCRAFT', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'221_HEATCRAFT', true,'.txt');
  end;
  hasDepartamento := False;
  hasDepartamento := verificaDepartamento;
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' select CC.DATAVENDA, CC.NF, CONV.CHAPA, CONV.TITULAR, PT.DESCRICAO, PT.QTD_APROV, PT.VLR_LIQ');
  DMConexao.Q.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.Q.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.Q.SQL.Add(' INNER JOIN PROD_TRANS PT ON PT.TRANS_ID = CC.TRANS_ID');
  DMConexao.Q.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  DMConexao.Q.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  DMConexao.Q.SQL.Add(' ORDER BY CONV.TITULAR, conv.CHAPA');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
      SalvarXLS(DMConexao.Q, '221_HEATCRAFT_PRODUTOS');
  end;


  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutAPOLO();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, data,mes, ano : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.SQL.Text;
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'select convert(varchar,data_venc,3) data_venc  '+
    ' from DIA_FECHA where DATA_FECHA = '+QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue))+' and EMPRES_ID = 707';
  DMConexao.Q.Open;
  data := DMConexao.Q.Fields[0].Value;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('01',2);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom(' ',4);
        S := S+ fnCompletarCom('602',3);
        S := S+ fnCompletarCom('V',1);
        S := S+ fnCompletarCom('000',3);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,8,'0');
        S := S+ fnCompletarCom(' ',1);
        S := S+ fnCompletarCom('1',1);
        S := S+ fnCompletarCom(data,8);
        S := S+ fnCompletarCom(data,8);
        SalvarArquivo(S,'707_APOLO', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,'707_APOLO', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutVOLKS();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec,mes,ano,empresa : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  if lkpEmp.KeyValue = 1217 then
    empresa := '1217_VOLKS_BELLA'
  else if lkpEmp.KeyValue = 1665 then
    empresa := '1665_VOLKS_BELLA';

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('21',2);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S+ fnCompletarCom('6861',4);
        mes := FormatDateTime('mm',lkpDataFechamento.KeyValue);
        ano := FormatDateTime('yy',lkpDataFechamento.KeyValue);
        S := S + fnCOmpletarCom(mes,2);
        S := S + fnCOmpletarCom(ano,2);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S + fnCOmpletarCom(valor,11,'0');
        S := S + fnCOmpletarCom('5124573',9,'0');
        S := S + fnCOmpletarCom(' ',160,' ');
        S := S + fnCOmpletarCom('2',1);
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutASSEJUS();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.TITULAR, conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,35,' ',true);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        SalvarArquivo(S,'387_ASSEJUS', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'387_ASSEJUS', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutCAMARASJC_PREFJAC();
var
  Erro : Boolean;
  count, tamanho : integer;
  S, valor, dec, empresa : String;

  valorTotal : double;
begin

  if lkpEmp.KeyValue = 400 then
    empresa := '400_CAMARA_SJC'
   else
    empresa := '90_PREFEITURA_JACAREI';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  if lkpEmp.KeyValue = 90 then begin
    QBusca.SQL.Add(' WHERE CONV.EMPRES_ID in(90,2115) ');
  end
  else
  begin
    QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  end;
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.TITULAR, conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CGC, NOME  FROM EMPRESAS WHERE EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue);
  DMConexao.Q.Open;

  //Cabe�alho
  S := '';
  S := S+ fnCompletarCom('0',1);
  S := S+ fnCompletarCom(DMConexao.Q.Fields[0].Value,15);
  S := S+ fnCompletarCom(DMConexao.Q.Fields[1].Value,60,' ', true);
  S := S+ fnCompletarCom(' ',7,' ');
  S := S+ fnCompletarCom('1',10,'0');
  SalvarArquivo(S,empresa, false,'.txt');
  tamanho := 2;
  valorTotal := 0;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('1',1);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,10,'0');
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,50,' ',true);
        if lkpEmp.KeyValue = 400 then
          S := S+ fnCompletarCom('1539',10,'0')
        else
          S := S+ fnCompletarCom('2133',10,'0');
        valorTotal := valorTotal + QBuscaTOTAL.AsFloat;
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        S := S+ fnCOmpletarCom(IntToStr(tamanho),10,'0');
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    tamanho := tamanho + 1;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  //RODAP�
  S := '';
  S := S+ fnCompletarCom('9',1);
  S := S+ fnCompletarCom(IntToStr(QBusca.RecordCount),10,'0');
  valor := StringReplace(FloatToStr(valorTotal),',','',[rfReplaceAll]);
  if lkpEmp.KeyValue = 400 then
    S := S+ fnCompletarCom(valor,12,'0')
  else
    S := S+ fnCompletarCom(valor,10,'0');
  if lkpEmp.KeyValue = 400 then
    S := S+ fnCompletarCom(' ',60,' ')
  else
    S := S+ fnCompletarCom(' ',62,' ');
  S := S+ fnCompletarCom(IntToStr(tamanho),10,'0');
  SalvarArquivo(S,empresa, false,'.txt');

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutPREFSJCAMPOS();
var
  Erro : Boolean;
  count, tamanho : integer;
  S, valor, dec, dataIni, dataFim, empresa : String;
begin
  if lkpEmp.KeyValue = 669 then
    empresa := '669_PREFEITURA_SJCAMPOS'
  else
    empresa := '2202_PREFEITURA_SJCAMPOS';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, ''A'' AS TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  if lkpEmp.KeyValue = 2202 then begin
    QBusca.SQL.Add(' WHERE CONV.EMPRES_ID in(669,2202) ');
  end
  else
  begin
    QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  end;
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA ORDER BY conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  //OBTEM O PRIMEIRO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + '), 0)),103)';
  DMConexao.Q.Open;
  dataIni := DMConexao.Q.Fields[0].Value;

  //OBTEM O ULTIMO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(ms ,-3 ,DATEADD(mm, DATEDIFF(mm, 0,'+  QuotedStr(dataIni) +') + 1, 0))),103)';
  DMConexao.Q.Open;
  dataFim := DMConexao.Q.Fields[0].Value;
  dataFim := StringReplace(dataFim,'/','',[rfReplaceAll]);
  dataIni := StringReplace(dataIni,'/','',[rfReplaceAll]);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      tamanho := 0;
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        tamanho := Length(QBuscaCHAPA.AsString) - 2;
        S := S+ fnCompletarCom(Copy(QBuscaCHAPA.AsString,1,tamanho),6,'0');
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom(RightStr(QBuscaCHAPA.AsString,2),2);
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('53',2);
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom('6',1);
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom(dataIni,8);
        S := S+ fnCompletarCom(';',1);
        S := S+ fnCompletarCom(dataFim,8);
        S := S+ fnCompletarCom(';',1);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,Length(valor));
        SalvarArquivo(S,empresa, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,empresa, true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutSINDSERVSJC();
var
  Erro : Boolean;
  count, tamanho,sequencial : integer;
  S, valor, dec, dataIni, dataFim,dataProxFechamento, empresa : String;
begin
  if ((LancNormal.Checked = false) and (LancDebit.Checked = false) and (LancUnic.Checked = false)) then begin
    MsgInf('SELECIONE UM CHECKBOX!');
  end
  else
  begin
    ProgressBar1.Position := 0;
    lblStatus.Caption := 'Gerando aquivo...';
    lblStatus.Refresh;
    Erro := False;

    QBuscaSindserv.Close;
    QBuscaSindserv.SQL.Clear;

    if CBJuntar669.Checked = false then
    begin
      empresa := '2379_SINDSERV_SJCAMPOS';
      
      QBuscaSindserv.SQL.Add(' select CONV.CHAPA, ''A'' AS TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL, CPF');
      QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
      QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = 2379 ');
      QBuscaSindserv.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));

      if LancNormal.Checked = true then
      begin
        empresa := '2379_SINDSERV_SJCAMPOS_COMPRAS';
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID <> 2');
      end;
      if LancDebit.Checked = true then
      begin
        empresa := '2379_SINDSERV_SJCAMPOS_DEBITOS_MES_ANTERIOR';
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID = 2');
      end;

      QBuscaSindserv.SQL.Add(' GROUP BY CONV.CHAPA, CPF ORDER BY conv.CHAPA');
    end
    else
    begin
      empresa := '2379_E_669_PREF_SJCAMPOS';

      QBuscaSindserv.SQL.Add(' select CONV.CHAPA, ''A'' AS TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL, CPF');
      QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
      QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = 669 ');
      QBuscaSindserv.SQL.Add(' AND CC.DATA >= ' + QuotedStr(Data1.Text));
      QBuscaSindserv.SQL.Add(' AND CC.DATA <= ' + QuotedStr(Data2.Text));

      if LancNormal.Checked = true then
      begin
        empresa := '2379_E_669_PREF_SJCAMPOS_COMPRAS';
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID <> 2');
      end;
      if LancDebit.Checked = true then
      begin
        empresa := '2379_E_669_PREF_SJCAMPOS_DEBITOS_MES_ANTERIOR';
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID = 2');
      end;

      QBuscaSindserv.SQL.Add(' GROUP BY CONV.CHAPA, CPF ');
      QBuscaSindserv.SQL.Add(' UNION');
      QBuscaSindserv.SQL.Add(' select CONV.CHAPA, ''A'' AS TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL, CPF');
      QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
      QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = 2379 ');
      QBuscaSindserv.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));

      if LancNormal.Checked = true then
      begin
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID <> 2');
      end;
      if LancDebit.Checked = true then
      begin
        QBuscaSindserv.SQL.Add(' AND cc.CRED_ID = 2');
      end;
      
      QBuscaSindserv.SQL.Add(' GROUP BY CONV.CHAPA, CPF ORDER BY conv.CHAPA');
    end;

    QBuscaSindserv.Open;


    count := QBuscaSindserv.RecordCount;
    SList := TStringList.Create;

    //OBTEM O PRIMEIRO DIA DO MES
    DMConexao.Q.Close;
    DMConexao.Q.SQL.Clear;
    DMConexao.Q.SQL.Text := 'SELECT TOP 1 CONVERT(VARCHAR,(DATA_FECHA),103) FROM DIA_FECHA WHERE DATA_FECHA > ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) +' AND EMPRES_ID = 2379';
    DMConexao.Q.Open;
    dataProxFechamento := DMConexao.Q.Fields[0].Value;

    DMConexao.Q.Close;
    DMConexao.Q.SQL.Clear;
    DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,''' + dataProxFechamento + '''), 0)),103)';
    //DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + '), 0)),103)';
    DMConexao.Q.Open;
    dataIni := DMConexao.Q.Fields[0].Value;

    //OBTEM O ULTIMO DIA DO MES
    DMConexao.Q.Close;
    DMConexao.Q.SQL.Clear;
    DMConexao.Q.SQL.Text := 'SELECT CONVERT(VARCHAR, CONVERT(DATE,DATEADD(ms ,-3 ,DATEADD(mm, DATEDIFF(mm, 0,'+  QuotedStr(dataIni) +') + 1, 0))),103)';
    DMConexao.Q.Open;
    dataFim := DMConexao.Q.Fields[0].Value;
    dataFim := StringReplace(dataFim,'/','',[rfReplaceAll]);
    dataIni := StringReplace(dataIni,'/','',[rfReplaceAll]);

 {  if not(QBusca.IsEmpty)then
    begin
      //CABE�ALHO
      S := '';
      S := '1';
      S := fnCompletarCom(FormatDateTime('dd',Date) + FormatDateTime('mm',Date) + FormatDateTime('yyyy',Date),8); //A07 - DATA GERA��O DO ARQUIVO
      S := fnCompletarCom('RECORRENCIA',20,' ');
      SalvarArquivo(S,empresa, false,'.txt');
    end;  }

    sequencial := 0;
    while not QBuscaSindserv.Eof do begin
      if QBuscaSindservTOTAL.Value > 0.00 then begin
        tamanho := 0;
        sequencial := sequencial + 1;
        S := '';
        valor := '';
        dec := '';
        try
          if Cancelar then
            if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
              Abort;
          end;
          tamanho := Length(QBuscaSindservChapa.AsString) - 2;
          //S := S + '2';
          //S := S + fnCompletarCom(IntToStr(sequencial),9,'0');

          //S := S+ fnCompletarCom(' ',12,' ',False);
          //S := S+ fnCompletarCom(' ',12,' ',False);
          //S := S+ fnCompletarCom('10 ',10,' ',False);
          S := S+ fnCompletarCom(QBuscaSindservChapa.AsString,8, '0');
          S := S+ fnCompletarCom(QBuscaSindservCPF.AsString,11,'0');
          S := S+ fnCompletarCom(FormatDateTime('mm',IncMonth(StrToDateTime(lkpDataFechamento.KeyValue),+1)) + FormatDateTime('yyyy',StrToDateTime(lkpDataFechamento.KeyValue)),6);


//        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
//        dec := Copy(valor,Length(valor)-1,2);
//        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;



          valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaSindservTOTAL.Value));
          dec := Copy(valor,Length(valor)-1,2);
          valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
          S := S+ fnCOmpletarCom(valor,15,'0');
          S := S+ '004913';
          SalvarArquivo(S,empresa, false,'.txt');
          except on E:Exception do begin
            Erro := True;
            MsgErro('Erro: '+E.message);
          end;
        end;
      end;
      QBuscaSindserv.Next;
      ProgressBar1.Position := ((QBuscaSindserv.RecNo*100) div count);
      ProgressBar1.Refresh;
      Application.ProcessMessages;
    end;

    if not QBuscaSindserv.IsEmpty then begin
      SalvarArquivo(S,empresa, true,'.txt');
    end;

    if not Erro then
    begin
      MsgInf('Exporta��o concluida com sucesso!');
      LimparCampos;
    end
    else
    begin
      lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
      LimparCampos;
    end;
    HabDesabBtnProcesso(False);
    FreeAndNil(SList);
    ProgressBar1.Position := 0;
    ProgressBar1.CleanupInstance;
    ProgressBar1.Refresh;
    btnExportar.Enabled := true;
  end;
end;

procedure TFrmExportEstabPadrao.LayoutTRELLEBORG();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,12,'0');
        S := S+ fnCompletarCom('273',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,22,'0');
        SalvarArquivo(S,'115_TRELLEBORG', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'115_TRELLEBORG', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutBUNDYPINDA  ();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec, dataIni : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + '), 0))';
  DMConexao.Q.Open;
  dataIni := DMConexao.Q.Fields[0].Value;
  dataIni := StringReplace(dataIni,'-','',[rfReplaceAll]);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('1',1);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,10,'0');
        S := S+ fnCompletarCom('77',2);
        S := S+ fnCompletarCom(' ',13,' ');
        S := S+ fnCompletarCom(dataIni,8);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,'0');
        SalvarArquivo(S,'299_BUNDY', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,'299_BUNDY', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  BEGIN
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  END;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;




procedure TFrmExportEstabPadrao.LayoutIPSM();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec, dataIni : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA,CONV.TITULAR ORDER BY conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;


  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        S := S+ fnCompletarCom('515',3);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-1,2);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,18,'0');
        SalvarArquivo(S,'2568_SINDSERV_SJC_IPSM', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,'2568_SINDSERV_SJC_IPSM', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  BEGIN
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  END;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;



procedure TFrmExportEstabPadrao.LayoutTIBRASIL();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin

   if lkpEmp.KeyValue = 145 then
    empresa := '145_TI_BRASIL'
   else
    empresa := '168_TI_CACAPAVA';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add(' select CONV.CPF, CONV.TITULAR, CONV.CHAPA, SUM(DEBITO - CREDITO) AS SUM');
  DMConexao.Q.SQL.Add(' from CONVENIADOS CONV');
  DMConexao.Q.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  DMConexao.Q.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  DMConexao.Q.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
  DMConexao.Q.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, CC.DATA_FECHA_EMP, CONV.CPF');
  DMConexao.Q.SQL.Add(' ORDER BY CONV.TITULAR');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
     SalvarXLS(DMConexao.Q, empresa);
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutSINDSEP_FACILITY_TINTAUBATE();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  if lkpEmp.KeyValue = 506 then
    empresa := '506_SINDSEP_'
  else if lkpEmp.KeyValue = 535 then
    empresa := '535_FACILITY_'
  else if lkpEmp.KeyValue = 737 then
    empresa := '737_TINTAS_TAUBATE_'
  else if lkpEmp.KeyValue = 741 then
    empresa := '741_TINTAS_TAUBATE_';

  QDpto.Close;
  QDpto.SQL.Clear;
  QDpto.SQL.Add('select dept_id from EMP_DPTOS EMP where EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QDpto.Open;
  count :=  QDpto.RecordCount;

  while not QDpto.Eof do begin
      QBuscaPdf.Close;
      QBuscaPdf.SQL.Clear;
      QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,CC.DATA_FECHA_EMP),103) AS DATA_FECHA_EMP,');
      QBuscaPdf.SQL.Add(' SUM(DEBITO - CREDITO) AS TOTAL');
      QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
      QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
      QBuscaPdf.SQL.Add(' INNER JOIN EMP_DPTOS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID AND EMP.DEPT_ID = CONV.SETOR');
      QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
      QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
      QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S''');
      QBuscaPdf.SQL.Add(' AND EMP.DEPT_ID =' +QDptoDEPT_ID.AsString);
      QBuscaPdf.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR, EMP.DESCRICAO, EMP.DEPT_ID, CC.DATA_FECHA_EMP');
      QBuscaPdf.SQL.Add(' ORDER BY conv.TITULAR, conv.CHAPA, EMP.DESCRICAO');
      QBuscaPdf.Open;

      if not QBuscaPdf.IsEmpty then begin
        frxPDFExport1.FileName := edtCaminho.Text + '\' + empresa + StringReplace(QBuscaPdfDESCRICAO.AsString,' ','_',[rfReplaceAll]) + '.pdf';
        frxReport1.ShowReport;
        frxReport1.Export(frxPDFExport1);
      end;

      ProgressBar1.Position := ((QDpto.RecNo*100) div count);
      ProgressBar1.Refresh;
      Application.ProcessMessages;
      QDpto.Next;
   end;


  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutAFAPEM();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,8,'0');
        S := S+ fnCompletarCom('556',3);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,19,'0');
        SalvarArquivo(S,'42_AFAPEM', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'42_AFAPEM', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);                                               
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutADCURBAM();
var
  dia, mes, ano : Word;
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  valorTotal : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),ano, mes, dia);

  //Cabe�alho
  S := '';
  S := S+ fnCompletarCom(IntToStr(mes),2);
  S := S+ fnCompletarCom(IntToStr(ano),4);
  S := S+ fnCompletarCom('0',31,'0');
  SalvarArquivo(S,'1084_ADC_URBAM', false,'.txt');

  valorTotal := 0;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,11,'0');
        S := S+ fnCompletarCom('472',3);
        valorTotal := valorTotal + QBuscaTOTAL.AsFloat;
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,22,'0');
        S := S+ fnCOmpletarCom('1',1);
        SalvarArquivo(S,'1084_ADC_URBAM', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  //Rodape
  S := '';
  S := S+ fnCompletarCom(IntToStr(QBusca.RecordCount),6);
  valor := StringReplace(FloatToStr(valorTotal),',','',[rfReplaceAll]);
  S := S+ fnCompletarCom(valor,24,'0');
  S := S+ fnCompletarCom(IntToStr(QBusca.RecordCount + 2),6);
  S := S+ fnCompletarCom('2',1);
  SalvarArquivo(S,'1084_ADC_URBAM', false,'.txt');

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'1084_ADC_URBAM', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutADCCONFAB();
var
  dia, mes, ano : Word;
  day,month     : String;
  Erro          : Boolean;
  count         : integer;
  S, valor, dec, dataIni, dataFim : String;
  valorTotal    : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, (DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));

  if ExportarApenasDemitidos.Checked then begin
    QBusca.SQL.Add('and conv.data_demissao = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',(now))));
    QBusca.SQL.Add('and cc.data_fecha_emp >= (SELECT top 1 DATA_FECHA from DIA_FECHA where DATA_FECHA > GETDATE() and EMPRES_ID= '+lkpEmp.KeyValue+')');
  end
  else
    QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));

  QBusca.SQL.Add(' AND CC.CANCELADA = ''N'' AND CC.PREVIAMENTE_CANCELADA <> ''S'' AND CC.CREDITO = 0 ORDER BY CONV.TITULAR, CC.DATAVENDA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;


  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'select CONVERT(DATE,DATEADD(MM,-1,' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + '))';
  DMConexao.Q.Open;
  dataIni := DMConexao.Q.Fields[0].Value;

  //OBTEM O PRIMEIRO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(DATE,DATEADD(mm, DATEDIFF(mm, 0,CONVERT(VARCHAR, CONVERT(DATE,'+  QuotedStr(dataIni) +'),103)), 0))';
  DMConexao.Q.Open;
  dataIni := DMConexao.Q.Fields[0].Value;

  //OBTEM O ULTIMO DIA DO MES
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Text := 'SELECT CONVERT(DATE,DATEADD(ms ,-3 ,DATEADD(mm, DATEDIFF(mm, 0,CONVERT(VARCHAR, CONVERT(DATE,'+  QuotedStr(dataIni) +'),103)) + 1, 0)))';
  DMConexao.Q.Open;
  dataFim := DMConexao.Q.Fields[0].Value;
  dataFim := StringReplace(dataFim,'-','',[rfReplaceAll]);
  dataIni := StringReplace(dataIni,'-','',[rfReplaceAll]);

  valorTotal := 0;
  DecodeDate(now,ano, mes, dia);
  day   := FormatFloat('00',StrToFloat(IntToStr(dia)));
  month := FormatFloat('00',StrToFloat(IntToStr(mes)));
  //Cabe�alho
  S := '';
  S := S+ fnCompletarCom('1',1);
  S := S+ fnCompletarCom('0560',4);
  S := S+ fnCompletarCom(dataIni,8);
  S := S+ fnCompletarCom(dataFim,8);
  S := S+ fnCompletarCom('0',89,'0');
  SalvarArquivo(S,day + month + '0560', false,'.HVM');

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom('2',1);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,12,'0');
        S := S+ fnCompletarCom('980560',6);
        S := S+ fnCompletarCom(' ',30,' ');
        S := S+ fnCompletarCom(QBuscaTITULAR.AsString,30,' ', true);
        S := S+ fnCompletarCom('0',12,'0');
        valorTotal := valorTotal + QBuscaTOTAL.AsFloat;
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,11,'0');
        S := S+ fnCOmpletarCom('0',8,'0');
        SalvarArquivo(S,DAY + MONTH + '0560', false,'.HVM');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  //Rodape
  valor := StringReplace(FloatToStr(valorTotal),',','',[rfReplaceAll]);
  S := '';
  S := S+ fnCompletarCom('9',1);
  S := S+ fnCompletarCom('0560',4);
  S := S+ fnCompletarCom('0',11,'0');
  S := S+ fnCompletarCom(valor,11,'0');
  S := S+ fnCompletarCom('0',83,'0');
  SalvarArquivo(S,day + month + '0560', false,'.HVM');

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,day + month + '0560', true,'.HVM');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutCOOPER();
var
  dia, mes, ano : Word;
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, conv.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(CC.DEBITO - CC.CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, conv.TITULAR ORDER BY conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),ano, mes, dia);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,10,'0');
        S := S+ fnCompletarCom('0',5);
        S := S+ fnCompletarCom('142',5,'0');
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCompletarCom(valor,15,'0');
        S := S+ fnCOmpletarCom('01',2);
        //S := S+ fnCompletarCom(FormatDateTime('dd',lkpDataFechamento.KeyValue) + '/' + FormatDateTime('mm',lkpDataFechamento.KeyValue) + '/' + FormatDateTime('yyyy',lkpDataFechamento.KeyValue),10);
        S := S+ fnCompletarCom('0000000000',10);
        S := S+ fnCOmpletarCom(' ',50,' ');
        S := S+ fnCOmpletarCom('00',02);
        SalvarArquivo(S,'202_COOPER', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'202_COOPER', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutPREFUBATUBA();
var
  dia, mes, ano : Word;
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),ano, mes, dia);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('013',3);
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S+ fnCompletarCom('05200',5);
        S := S+ fnCompletarCom(FormatDateTime('yyyy',lkpDataFechamento.KeyValue) + FormatDateTime('mm',lkpDataFechamento.KeyValue),6);
        //S := S+ fnCompletarCom(IntToStr(ano) + IntToStr(mes),6);
        S := S+ fnCompletarCom(FormatDateTime('yyyy',lkpDataFechamento.KeyValue) + FormatDateTime('mm',lkpDataFechamento.KeyValue),6);
        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,'0');
        S := S+ fnCOmpletarCom('000000',6);

        SalvarArquivo(S,'59_PREF_UBATUBA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'59_PREF_UBATUBA', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutSECULUM;
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec, empresa : String;
begin
  if lkpEmp.KeyValue = 1670 then begin
    empresa := '1670_SECULUM_MONITORAMENTO_LTDA';
  end else if lkpEmp.KeyValue = 1671 then begin
    empresa := '1671_SECULUM_VIGILANCIA_LTDA_EPP';
  end else if lkpEmp.KeyValue = 1672 then begin
    empresa := '1672_AE_SERVI�OS_OPERACIONAIS_LTDA_EPP';
  end else if lkpEmp.KeyValue = 1673 then begin
    empresa := '1673_PA_SERVI�OS_GERAIS_LTDA_ME';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA,CONV.TITULAR order by conv.CHAPA, CONV.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;


  //MONTADOR, MONTA O ARQUIVO TXT
  while not QBusca.Eof do begin
    if QBusca.RecordCount > 0 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12,'0');
        S := S+ fnCompletarCom('0',8);
        SalvarArquivo(S,empresa, False,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  If not QBusca.IsEmpty then begin
     //SalvarArquivo(S,'182_CETEC', true,'.txt');
      //if IntToStr(lkpEmp.KeyValue) = '1670' then
      //begin
         SalvarArquivo(S,empresa, true,'.txt');
     // end;
      {if IntToStr(lkpEmp.KeyValue) = '1671' then
      begin
         SalvarArquivo(S,empresa, true,'.txt');
      end;
      if IntToStr(lkpEmp.KeyValue) = '1672' then
      begin
        SalvarArquivo(S,'1672_AE_SERVI�OS_OPERACIONAIS_LTDA_EPP', True,'.txt');
      end;
      if IntToStr(lkpEmp.KeyValue) = '1673' then
      begin
        SalvarArquivo(S,'1673_PA_SERVI�OS_GERAIS_LTDA_ME', True,'.txt');
      end;}
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;






procedure TFrmExportEstabPadrao.LayoutCRYLORRACICINYLON();
var S, valor, dec, empresa : String;
    SL, SL2 : TStringList;
    Erro : Boolean;
    count : integer;
begin
  if lkpEmp.KeyValue = 361 then
    empresa := '361_CRYLOR_'
  else
    empresa := '362_RACICINYLON_';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  //Vendas com Receita
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' AND CC.RECEITA = ''S''');
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,25,' ',True);
        S := S+ fnCompletarCom('0729',4);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,' ');
        S := S+ fnCOmpletarCom('N',16,' ');
        SalvarArquivo(S,empresa + 'COM_RECEITA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,empresa + 'COM_RECEITA', true,'.txt');
  end;

  SList := TStringList.Create;
  ProgressBar1.Position := 0;
  //Vendas sem Receita
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' AND CC.RECEITA = ''N''');
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA,conv.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,25,' ',True);

        S := S+ fnCompletarCom('0420',4);
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,15,' ');
        S := S+ fnCOmpletarCom('N',16,' ');
        SalvarArquivo(S,empresa + 'SEM_RECEITA', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,empresa + 'SEM_RECEITA', true,'.txt');
  end;

  //ARQUIVO XLS
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR,');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''S'') = ''S'' then cc.debito-cc.credito else 0 end),0) as COM_RECEITA,');
  QBusca.SQL.Add(' coalesce(sum(case when coalesce(cc.receita,''N'') = ''N'' then cc.debito-cc.credito else 0 end),0) as SEM_RECEITA,');
  QBusca.SQL.Add(' COALESCE(SUM(cc.debito-cc.credito),0) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.TITULAR, conv.CHAPA');
  QBusca.Open;

  if not QBusca.IsEmpty then begin
    SalvarXLS(QBusca, empresa);
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.EdEmp_IDKeyPress(Sender: TObject;
  var Key: Char);
begin
  //if not (key in ['0'..'9',#13,#8]) then Key := #0;
   

end;

procedure TFrmExportEstabPadrao.EdEmp_IDChange(Sender: TObject);
begin
  { if Sender = EdEmp_ID then begin

  end;}
end;

procedure TFrmExportEstabPadrao.lkpEmpKeyPress(Sender: TObject;
  var Key: Char);
begin
   if Key = #13 then begin
    QData.Close;
    QData.Parameters.ParamByName('empres_id').Value := lkpEmp.KeyValue;
    QData.Open;
    Label2.Visible := true;
    lkpDataFechamento.Visible := true;
    btnExportar.Enabled := true;
    lkpDataFechamento.SetFocus;
    lblStatus.Caption := '';
    if lkpEmp.KeyValue = 455 then begin
      Label4.Visible := true;
      cmbSegmento.Visible := true;
      ExportarApenasDemitidos.Visible := true;
      ExportarFechamentoAnterior.Visible := True;
    end else begin
      Label4.Visible := false;
      cmbSegmento.Visible := false;
      ExportarApenasDemitidos.Visible := False;
      ExportarFechamentoAnterior.Visible := False;
    end;

    if lkpEmp.KeyValue = 2379 then begin
      LancNormal.Visible := true;
      LancDebit.Visible := true;
      LancUnic.Visible := true;

      CBJuntar669.Visible := true;
      CBJuntar669.Checked := false;
      Data1.Text := '';
      Data2.Text := '';
    end
    else
    begin
      LancNormal.Visible := false;
      LancDebit.Visible := false;
      LancUnic.Visible := false;
      
      CBJuntar669.Visible := false;
      CBJuntar669.Checked := false;
      Data1.Visible := false;
      Data2.Visible := false;
      Data1.Text := '';
      Data2.Text := '';
    end;

    if lkpEmp.KeyValue = 542 then begin
      ExportarApenasDemitidos.Visible := true;
      ExportarFechamentoAnterior.Visible := True;
    end else begin
      ExportarApenasDemitidos.Visible := False;
      ExportarFechamentoAnterior.Visible := False;
    end

  end;
end;

procedure TFrmExportEstabPadrao.edtCaminhoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    EdEmp_ID.SetFocus;
end;

procedure TFrmExportEstabPadrao.lkpDataFechamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then begin
     if lkpEmp.KeyValue = 455 then
        cmbSegmento.SetFocus
     else
        btnExportar.SetFocus;
  end;
end;

procedure TFrmExportEstabPadrao.lkpEmpExit(Sender: TObject);
begin
    EdEmp_ID.Text := lkpEmp.KeyValue;
    flag := False;
    QData.Close;
    QData.Parameters.ParamByName('empres_id').Value := lkpEmp.KeyValue;
    QData.Open;
    QDepart.Parameters.ParamByName('empres_id').Value := lkpEmp.KeyValue;
    QDepart.Open;
    if(QDepart.RecordCount <> 0) then
        flag := True;
    Label2.Visible := true;
    lkpDataFechamento.Visible := true;
    btnExportar.Enabled := true;
    if QDepart.RecordCount = 0 then
       lkpDataFechamento.SetFocus;
    lblStatus.Caption := '';
    if ((lkpEmp.KeyValue = 455) or ( lkpEmp.KeyValue = 542)) then begin
      Label4.Visible := true;
      cmbSegmento.Visible := true;
      ExportarApenasDemitidos.Visible := true;
      ExportarFechamentoAnterior.Visible := True;
    end else begin
      Label4.Visible := false;
      cmbSegmento.Visible := false;
      ExportarApenasDemitidos.Visible := False;
      ExportarFechamentoAnterior.Visible := False;
    end;

    if lkpEmp.KeyValue = 2379 then begin
      LancNormal.Visible := true;
      LancDebit.Visible := true;
      LancUnic.Visible := true;
      
      CBJuntar669.Visible := true;
      CBJuntar669.Checked := false;
      Data1.Text := '';
      Data2.Text := '';
    end
    else
    begin
      LancNormal.Visible := false;
      LancDebit.Visible := false;
      LancUnic.Visible := false;
      
      CBJuntar669.Visible := false;
      CBJuntar669.Checked := false;
      Data1.Visible := false;
      Data2.Visible := false;
      Data1.Text := '';
      Data2.Text := '';
    end;
    

//    if lkpEmp.KeyValue = 542 then begin
//      ExportarApenasDemitidos.Visible := true;
//    end else begin
//      ExportarApenasDemitidos.Visible := False;
//    end;

end;

procedure TFrmExportEstabPadrao.EdEmp_IDExit(Sender: TObject);
begin
  {QData.Close;
    QData.Parameters.ParamByName('empres_id').Value := lkpEmp.KeyValue;
    QData.Open;
    QDepart.Parameters[0].Value := lkpEmp.KeyValue;
    QDepart.Open;
    Label2.Visible := true;
    if QDepart.RecordCount <> 0 then begin
      Label5.Visible := True;
      lkDepartamento.Visible := True;
      lkDepartamento.SetFocus;
    end
    else begin
      Label2.Top := 96;
      lkpDataFechamento.Top := 112;
      Label4.Top := 141;
      cmbSegmento.Top := 156;
    end;
    lkpDataFechamento.Visible := true;
    btnExportar.Enabled := true;
    if QDepart.RecordCount <> 0 then
       lkpDataFechamento.SetFocus;
    lblStatus.Caption := '';
    if lkpEmp.KeyValue = 455 then begin
      Label4.Visible := true;
      cmbSegmento.Visible := true;
    end else begin
      Label4.Visible := false;
      cmbSegmento.Visible := false;
    end;  }
end;

procedure TFrmExportEstabPadrao.lkpEmpEnter(Sender: TObject);
begin
  if EdEmp_ID.Text = '' then
       lkpEmp.ClearValue
     else if QEmpresa.Locate('empres_id',EdEmp_ID.Text,[]) then  begin
       lkpEmp.KeyValue := EdEmp_ID.Text;
       lkpEmp.SetFocus;
       lblStatus.Caption := '';
     end
     else
       lkpEmp.ClearValue;
end;


procedure TFrmExportEstabPadrao.LayoultMASSAGUACU();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, filial, verba, dec : String;
  MyLastDate : TDateTime;
  ultimoDiaDoMes,year,month,day : Word;

begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;
  QBuscaPdf.Close;
  QBuscaPdf.SQL.Clear;
  QBuscaPdf.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, ''GERAL '' AS DESCRICAO, 1 AS DEPT_ID, CONVERT (VARCHAR, CONVERT(DATE,cc.DATAVENDA),103) AS DATA_FECHA_EMP,(DEBITO - CREDITO) AS TOTAL');
  QBuscaPdf.SQL.Add(' from CONVENIADOS CONV');
  QBuscaPdf.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaPdf.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBuscaPdf.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaPdf.SQL.Add(' AND CC.PREVIAMENTE_CANCELADA <> ''S'' ORDER BY CONV.TITULAR,conv.CHAPA, CC.DATAVENDA');
  QBuscaPdf.Open;

  //Pegando o ultimo dia do mes selecionado pelo usuario.
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),year,month,day);
  MyLastDate := EndOfAMonth(year,month);
  DecodeDate(MyLastDate,year,month,day);
  ultimoDiaDoMes := day;
  ///
  count := QBuscaPdf.RecordCount;
  SList := TStringList.Create;
  while not QBuscaPdf.Eof do begin
    if QBuscaPdfTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      filial := '01';
      verba := '447';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False)
          then
          begin
            Abort;
          end;
        S      := S + fnCompletarCom(filial,2,' ');
        S := S+ fnCompletarCom(' ',1);
        S      := S + fnCompletarCom(QBuscaPdfCHAPA.AsString,4,'0');
        S := S+ fnCompletarCom(' ',6,' ');
        S      := S + verba;
        S := S+ fnCompletarCom(' ',11,' ');
        S := S+ fnCompletarCom(' ',1);
        valor := FormatFloat('#,##0.00',QBuscaPdfTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,6,'0');
        S := S+ fnCompletarCom(' ',6,' ');
        S := S+ fnCompletarCom(Copy(FormatDateTime('ddmmyyyy',QBuscaPdfDATA_FECHA_EMP.AsDateTime),1,8),8);
        S := S+ IntToStr(ultimoDiaDoMes);

        SalvarArquivo(S,'Desc. Farmacia', false,'.txt');
        except on E:Exception do
        begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBuscaPdf.Next;
    ProgressBar1.Position := ((QBuscaPdf.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBuscaPdf.IsEmpty then begin
      SalvarArquivo(S,'Desc. Farmacia', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutALLTEC();
var S, valor, dec, empresa : String;
    SL, SL2 : TStringList;
    Erro : Boolean;
    count : integer;
begin

  empresa := '430_ALLTEC_';

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  //DADOS DO FECHAMENTO CONFORME MODELO INFORMADO
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' SELECT CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA,(SUM(CC.DEBITO)-SUM(CC.CREDITO)) AS TOTAL');
  QBusca.SQL.Add(' FROM CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON (CC.CONV_ID = CONV.CONV_ID)');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom('01',2) + ',';
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0',False) + ',';
        S := S+ fnCompletarCom('551',3) + ',';
        S := S+ fnCompletarCom('00.000',5) + ',';
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',QBuscaTOTAL.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCompletarCom(valor,8,'0',False);
        //S := S+ fnCOmpletarCom('N',16,' ');
        SalvarArquivo(S,empresa + 'FECHAMENTO', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
    SalvarArquivo(S,empresa + 'FECHAMENTO', true,'.txt');
  end;

  //ARQUIVO XLS
  QAlltec.Close;
  QAlltec.SQL.Clear;
  QAlltec.SQL.Add(' SELECT ''01'' AS FIXO1, REPLICATE(''0'', (6 - LEN(CONV.CHAPA))) + CAST(CONV.CHAPA AS VARCHAR(6)) AS CHAPA, ''551'' AS FIXO2, ''00.00'' AS FIXO3,');
  QAlltec.SQL.Add(' REPLICATE(''0'', (8 - LEN(SUM(CC.DEBITO)-SUM(CC.CREDITO)))) + REPLACE(CAST((SUM(CC.DEBITO)-SUM(CC.CREDITO)) AS VARCHAR(8)), '','', ''.'') AS TOTAL');
  QAlltec.SQL.Add(' FROM CONVENIADOS CONV');
  QAlltec.SQL.Add(' INNER JOIN CONTACORRENTE CC ON (CC.CONV_ID = CONV.CONV_ID)');
  QAlltec.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QAlltec.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QAlltec.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY CONV.CHAPA');
  QAlltec.Open;

  if not QAlltec.IsEmpty then begin
    SalvarXLS(QAlltec, empresa);
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoutABCTRANSPORTES();
var
  dia, mes, ano : Word;
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA,seg.SEG_ID as SEGMENTO, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' inner join credenciados cred on cred.CRED_ID = cc.CRED_ID');
  QBusca.SQL.Add(' inner join segmentos seg on cred.SEG_ID = seg.SEG_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, seg.SEG_ID, CONV.TITULAR ORDER BY conv.CHAPA, CONV.TITULAR');
  QBusca.Open;
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),ano, mes, dia);

  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        valor := fnSubstituiString(',','',FormatFloat('0000.00',QBuscaTOTAL.Value));
        S := S+ fnCOmpletarCom(valor,8,'0');
        SalvarArquivo(S,'159_ABC_TRANSPORTES', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
     SalvarArquivo(S,'159_ABC_TRANSPORTES', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LayoultGRUPOCONNECTA();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
  MyLastDate : TDateTime;
  ultimoDiaDoMes,year,month,day : Word;

begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, CONV.TITULAR');
  QBusca.Open;

  //Pegando o ultimo dia do mes selecionado pelo usuario.
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),year,month,day);
  MyLastDate := EndOfAMonth(year,month);
  DecodeDate(MyLastDate,year,month,day);
  ultimoDiaDoMes := day;
  ///
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False)
          then
          begin
            Abort;
          end;
        S := S + fnCompletarCom('001',3);
        S := S + fnCompletarCom('001',3);
        S := S + fnCompletarCom(QBuscaCHAPA.AsString,6,'0');
        S := S+ fnCompletarCom('615',3);
        //S      := S + verba;
        //S := S+ fnCompletarCom(' ',11,' ');
        //S := S+ fnCompletarCom(' ',1);
        valor := FormatFloat('#,##0.00',QBuscaTOTAL.Value);
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        valor := StringReplace(valor,',','.',[rfReplaceAll]);
        S := S+ fnCOmpletarCom(valor,7,'0');
        S := S+ fnCompletarCom('1',1);
        S := S+ fnCompletarCom('000000000',9);

        SalvarArquivo(S,'Mvto' + FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), false,'.txt');
        except on E:Exception do
        begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'Mvto' +  FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;



 procedure TFrmExportEstabPadrao.LayoutPREFILHABELA();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec : String;
  MyLastDate : TDateTime;
  ultimoDiaDoMes,year,month,day : Word;

begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, CONV.TITULAR');
  QBusca.Open;

  //Pegando o ultimo dia do mes selecionado pelo usuario.
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),year,month,day);
  MyLastDate := EndOfAMonth(year,month);
  DecodeDate(MyLastDate,year,month,day);
  ultimoDiaDoMes := day;
  ///
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False)
          then
          begin
            Abort;
          end;
        S := S + fnCompletarCom('003',3);
        S := S + fnCompletarCom(QBuscaCHAPA.AsString,9,'0');
        S := S + fnCompletarCom('04155',5);
        S := S + fnCompletarCom(FormatDateTime('yyyymm',lkpDataFechamento.KeyValue),6);
        S := S + fnCompletarCom(FormatDateTime('yyyymm',lkpDataFechamento.KeyValue),6);

        valor := FormatFloat('###0.00',QBuscaTOTAL.Value);
        valor := StringReplace(valor,',','',[rfReplaceAll]);
        S := S+ fnCOmpletarCom(valor,15,'0');

        S := S+ fnCompletarCom('000000',6);


        SalvarArquivo(S,'Fechamento' + FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), false,'.txt');
        except on E:Exception do
        begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'2184_PREF_ILHABELA_' +  FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutGremio();
var
  Erro,hasDepartamento : Boolean;
  count,contLinha: integer;
  S, valor, dec,valorTotalStr,cpfaux,cpfvalor,aux : String;
  valorTotal    : double;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  hasDepartamento := verificaDepartamento;

  //com movimento primeira vez
  QBuscaSindserv.Close;
  QBuscaSindserv.SQL.Clear;
  QBuscaSindserv.SQL.Add(' select CONV.CPF,CONV.CHAPA ,CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) as TOTAL');
  QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
  QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaSindserv.SQL.Add(' inner join CREDENCIADOS cred on cred.CRED_ID = cc.CRED_ID');
  QBuscaSindserv.SQL.Add(' inner join SEGMENTOS seg on seg.SEG_ID = cred.SEG_ID');
  QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBuscaSindserv.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaSindserv.SQL.Add(' and cc.conv_id not in (select conv_id from contacorrente where data_fecha_emp = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',IncMonth(lkpDataFechamento.KeyValue,-1))));
  QBuscaSindserv.SQL.Add(' and empres_id = ' + IntToStr(lkpEmp.KeyValue) + ')');
  QBuscaSindserv.SQL.Add(' GROUP by CONV.CHAPA,CONV.TITULAR,conv.cpf order by conv.CPF,conv.TITULAR');
  QBuscaSindserv.SQL.Text;
  QBuscaSindserv.Open;
  count := QBuscaSindserv.RecordCount;
  SList := TStringList.Create;

  while not QBuscaSindserv.Eof do begin
    if QBuscaSindservTotal.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(QBuscaSindservCHAPA.AsString,8,'0');

        if (QBuscaSindservCPF.Value <> '') then begin
          if(Length(QBuscaSindservCPF.Value) < 11) then begin
            S := S + fnCompletarCom(QBuscaSindservCPF.AsString, 11, '0');
          end
          else begin
            cpfaux := '';
            cpfvalor := '';
            cpfaux := QBuscaSindservCPF.AsString;
            cpfvalor := AnsiReplaceStr(cpfaux,'.','');
            cpfvalor := AnsiReplaceStr(cpfvalor,'-','');
            S := S + cpfvalor
          end
        end
        else
          S := S + fnCompletarCom('',11,'0');

        S := S+ FormatDateTime('mm', now) + FormatDateTime('yyyy',now);

        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaSindservTotal.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;

        S := S+ fnCOmpletarCom(valor,15);

        S := S + '018203';

        SalvarArquivo(S,'GREMIO_PRIMEIRA_VENDA_' + lkpEmp.KeyValue, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBuscaSindserv.Next;
    ProgressBar1.Position := ((QBuscaSindserv.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBuscaSindserv.IsEmpty then begin
      SalvarArquivo(S,'GREMIO_PRIMEIRA_VENDA_' + lkpEmp.KeyValue, true,'.txt');
  end;

  //com movimento
  QBuscaSindserv.Close;
  QBuscaSindserv.SQL.Clear;
  QBuscaSindserv.SQL.Add(' select CONV.CPF,CONV.CHAPA ,CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) as TOTAL');
  QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
  QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaSindserv.SQL.Add(' inner join CREDENCIADOS cred on cred.CRED_ID = cc.CRED_ID');
  QBuscaSindserv.SQL.Add(' inner join SEGMENTOS seg on seg.SEG_ID = cred.SEG_ID');
  QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBuscaSindserv.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaSindserv.SQL.Add(' and cc.conv_id in (select conv_id from contacorrente where data_fecha_emp = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',IncMonth(lkpDataFechamento.KeyValue,-1))));
  QBuscaSindserv.SQL.Add(' and empres_id = ' + IntToStr(lkpEmp.KeyValue) + ')');
  QBuscaSindserv.SQL.Add(' GROUP by CONV.CHAPA,CONV.TITULAR,conv.cpf order by conv.CPF,conv.TITULAR');
  QBuscaSindserv.SQL.Text;
  QBuscaSindserv.Open;
  count := QBuscaSindserv.RecordCount;
  SList := TStringList.Create;

  while not QBuscaSindserv.Eof do begin
    if QBuscaSindservTotal.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;

        S := S+ fnCompletarCom(QBuscaSindservCHAPA.AsString,8,'0');

        if (QBuscaSindservCPF.Value <> '') then begin
          if(Length(QBuscaSindservCPF.Value) < 11) then begin
            S := S + fnCompletarCom(QBuscaSindservCPF.AsString, 11, '0');
          end
          else begin
            cpfaux := '';
            cpfvalor := '';
            cpfaux := QBuscaSindservCPF.AsString;
            cpfvalor := AnsiReplaceStr(cpfaux,'.','');
            cpfvalor := AnsiReplaceStr(cpfvalor,'-','');
            S := S + cpfvalor
          end
        end
        else
          S := S + fnCompletarCom('',11,'0');

        S := S+ FormatDateTime('mm', now) + FormatDateTime('yyyy',now);

        valor := fnSubstituiString(',','',FormatFloat('#,##0.00',QBuscaSindservTotal.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;

        S := S+ fnCOmpletarCom(valor,15);

        S := S + '018203';

        SalvarArquivo(S,'GREMIO_COM_MOVIMENTO_' + lkpEmp.KeyValue, false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBuscaSindserv.Next;
    ProgressBar1.Position := ((QBuscaSindserv.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBuscaSindserv.IsEmpty then begin
      SalvarArquivo(S,'GREMIO_COM_MOVIMENTO_' + lkpEmp.KeyValue, true,'.txt');
  end;


  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutPINDA();
var
  Erro,hasDepartamento : Boolean;
  count : integer;
  S, valor, dec , empresa: String;
  MyLastDate : TDateTime;
  ultimoDiaDoMes,year,month,day : Word;

begin

  if lkpEmp.KeyValue = 2394 then begin
    empresa := '2394_SINDSEP_CAMPOS_DO_JORDAO_';
  end else if lkpEmp.KeyValue = 2708 then begin
    empresa := '2708_SINDSERV_PINDA_';
  end;

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, CONV.TITULAR');
  QBusca.Open;

  //Pegando o ultimo dia do mes selecionado pelo usuario.
  DecodeDate(StrToDate(lkpDataFechamento.KeyValue),year,month,day);
  MyLastDate := EndOfAMonth(year,month);
  DecodeDate(MyLastDate,year,month,day);
  ultimoDiaDoMes := day;
  ///
  count := QBusca.RecordCount;
  SList := TStringList.Create;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False)
          then
          begin
            Abort;
          end;
        S := S + fnCompletarCom(QBuscaCHAPA.AsString,7,'0');

        valor := FormatFloat('###0.00',QBuscaTOTAL.Value);
        valor := StringReplace(valor,',','',[rfReplaceAll]);
        S := S+ fnCOmpletarCom(valor,6,'0');


        SalvarArquivo(S,'Fechamento' + FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), false,'.txt');
        except on E:Exception do
        begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,empresa +  FormatDateTime('mm',lkpDataFechamento.KeyValue) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue), true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutIPSM2();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec, dataIni : String;

begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  QBuscaSindserv.Close;
  QBuscaSindserv.SQL.Clear;
  QBuscaSindserv.SQL.Add(' select CONV.CHAPA, CONV.TITULAR AS TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL, conv.cpf');
  QBuscaSindserv.SQL.Add(' from CONVENIADOS CONV');
  QBuscaSindserv.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBuscaSindserv.SQL.Add(' WHERE CONV.EMPRES_ID = 2568 ');
  //QBuscaSindserv.SQL.Add(' AND CONV.CHAPA IN (SELECT CHAPA FROM CHAPASINd) ');
  QBuscaSindserv.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBuscaSindserv.SQL.Add(' GROUP BY CONV.CHAPA, conv.cpf, CONV.TITULAR ORDER BY conv.CHAPA');
  s :=    QBuscaSindserv.SQL.Text;
  QBuscaSindserv.Open;
  count := QBuscaSindserv.RecordCount;
  SList := TStringList.Create;


  while not QBuscaSindserv.Eof do begin
    if QBuscaSindservTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(QBuscaSindservChapa.AsString,10,'0');
        S := S+ fnCompletarCom(QBuscaSindservCPF.AsString,11,'0');
        S := S+ fnCompletarCom(QBuscaSindservTitular.AsString,50,' ',True);
        S := S+ fnCompletarCom('001',3);
        S := S+ fnCompletarCom('001',3);
        S := S+ fnCompletarCom('515',3);
        valor := FormatFloat('#,##0.00',QBuscaSindservTOTAL.Value);
       // dec := Copy(valor,Length(valor)-1,2);
      //  valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        valor := StringReplace(valor,',','.',[rfReplaceAll]);
        S := S+ fnCOmpletarCom(valor,10,'0');
        S := S+ fnCompletarCom('001',3);

        S := S+ fnCompletarCom(FormatDateTime('mm',IncMonth(lkpDataFechamento.KeyValue, +1)) +  FormatDateTime('yyyy',lkpDataFechamento.KeyValue),6);
        S := S+ fnCompletarCom('I',1);
        SalvarArquivo(S,'2568_SINDSERV_SJC_IPSM', false,'.txt');
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBuscaSindserv.Next;
    ProgressBar1.Position := ((QBuscaSindserv.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBuscaSindserv.IsEmpty then begin
    SalvarArquivo(S,'2568_SINDSERV_SJC_IPSM', true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  BEGIN
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  END;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.ExportarExcelSpani();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  hasDepartamento : Boolean;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('select empres_id,fantasia from empresas where empres_id in (727,728,1447,1590,1879,1947,2037,2194,2195,2196,2223,2224,2225,');
  DMConexao.AdoQry.SQL.Add('2226,2227,2228,2229,2230,2231,2232,2233,2234,2235,2236,2237,2238,2239,2240,2241,2247,2248,2249,2250,2251,2336,');
  DMConexao.AdoQry.SQL.Add('2368,2369,2370,2371,2372,2570)');
  DMConexao.AdoQry.Open();
  count := DMConexao.AdoQry.RecordCount;
  SList := TStringList.Create;

  while not DMConexao.AdoQry.Eof do begin

        S:= IntToStr(DMConexao.AdoQry.Fields[0].Value) + '_' + DMConexao.AdoQry.Fields[1].Value;
        DMConexao.Q.Close;
        DMConexao.Q.SQL.Clear;
        DMConexao.Q.SQL.Add(' select conveniados.chapa as CHAPA, conveniados.titular AS TITULAR, conveniados.limite_mes AS LIMITE_MES,');
        DMConexao.Q.SQL.Add(' sum(contacorrente.debito-contacorrente.credito) as VALOR');
        DMConexao.Q.SQL.Add(' from contacorrente');
        DMConexao.Q.SQL.Add(' join conveniados on (conveniados.conv_id = contacorrente.conv_id)');
        DMConexao.Q.SQL.Add(' where contacorrente.data_fecha_emp = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)) + ' and ');
        DMConexao.Q.SQL.Add(' ( conveniados.empres_id = ' + IntToStr(DMConexao.AdoQry.Fields[0].Value) +') and coalesce(contacorrente.baixa_conveniado,''N'') = ''N'' and  ');
        DMConexao.Q.SQL.Add(' contacorrente.cred_id <> 9999 group by conveniados.chapa, conveniados.titular, conveniados.limite_mes  order by Titular  ');
        DMConexao.Q.Open;

        if not DMConexao.Q.IsEmpty then begin
           SalvarXLS(DMConexao.Q, S);
        end;

        DMConexao.AdoQry.Next;
        ProgressBar1.Position := ((DMConexao.AdoQry.RecNo*100) div count);
        ProgressBar1.Refresh;
        Application.ProcessMessages;

  end;

  if not Erro then
  begin
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;


procedure TFrmExportEstabPadrao.LayoutGRUPOSAOPAULO();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec , empresa: String;


begin

  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;
  QBusca.Close;
  QBusca.SQL.Clear;
  QBusca.SQL.Add(' select CONV.CHAPA, CONV.TITULAR, 0.00 AS COM_RECEITA, 0.00 AS SEM_RECEITA, SUM(DEBITO - CREDITO) AS TOTAL');
  QBusca.SQL.Add(' from CONVENIADOS CONV');
  QBusca.SQL.Add(' INNER JOIN CONTACORRENTE CC ON CC.CONV_ID = CONV.CONV_ID');
  QBusca.SQL.Add(' WHERE CONV.EMPRES_ID = ' + IntToStr(lkpEmp.KeyValue));
  QBusca.SQL.Add(' AND CC.DATA_FECHA_EMP = ' + QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  QBusca.SQL.Add(' GROUP BY CONV.CHAPA, CONV.TITULAR ORDER BY conv.CHAPA, CONV.TITULAR');
  QBusca.Open;

  count := QBusca.RecordCount;
  SList := TStringList.Create;
  while not QBusca.Eof do begin
    if QBuscaTOTAL.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '.';
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False)
          then
          begin
            Abort;
          end;
        S := S + fnCompletarCom(QBuscaCHAPA.AsString,10,'0');
        S := S + fnCompletarCom('0',5,'0');
        S := S + fnCompletarCom('1056',5,'0');
        valor := FormatFloat('###0.00',QBuscaTOTAL.Value);
        valor := StringReplace(valor,',','',[rfReplaceAll]);
        S := S+ fnCOmpletarCom(valor,15,'0');
        S := S + fnCompletarCom('01',2,'0');
        S := S + fnCompletarCom('0',10,'0');
        S := S + fnCompletarCom('0',50,'0');
        S := S + fnCompletarCom('0',2,'0');


        SalvarArquivo(S,'001.ORIGEM_1.' + FormatDateTime('yyyy',lkpDataFechamento.KeyValue) + FormatDateTime('mm',lkpDataFechamento.KeyValue), false,'.txt');
        except on E:Exception do
        begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    QBusca.Next;
    ProgressBar1.Position := ((QBusca.RecNo*100) div count);
    ProgressBar1.Refresh;
    Application.ProcessMessages;
  end;

  if not QBusca.IsEmpty then begin
      SalvarArquivo(S,'001.ORIGEM_1.' + FormatDateTime('yyyy',lkpDataFechamento.KeyValue) + FormatDateTime('mm',lkpDataFechamento.KeyValue), true,'.txt');
  end;

  if not Erro then
  begin
    MsgInf('Exporta��o concluida com sucesso!');
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;
end;

procedure TFrmExportEstabPadrao.LayoutPREFPOTIM();
var
  Erro : Boolean;
  count : integer;
  S, valor, dec : String;
  hasDepartamento : Boolean;
begin
  ProgressBar1.Position := 0;
  lblStatus.Caption := 'Gerando aquivo...';
  lblStatus.Refresh;
  Erro := False;

  SList := TStringList.Create;


  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add('select CONVENIADOS.CHAPA as CHAPA, 3991 as CODIGO_EVENTO, CONTACORRENTE.DATA_FECHA_EMP as DATA_FECHAMENTO, sum(debito - credito) as VALOR from CONVENIADOS ');
  DMConexao.Q.SQL.Add('inner join EMPRESAS ');
  DMConexao.Q.SQL.Add('on CONVENIADOS.EMPRES_ID = EMPRESAS.EMPRES_ID ');
  DMConexao.Q.SQL.Add('inner join CONTACORRENTE ');
  DMConexao.Q.SQL.Add('on CONTACORRENTE.CONV_ID = CONVENIADOS.CONV_ID ');
  DMConexao.Q.SQL.Add('where EMPRESAS.EMPRES_ID = 2797 and DATA_FECHA_EMP = ');
  DMConexao.Q.SQL.Add(QuotedStr(FormatDateTime('dd/mm/yyyy',lkpDataFechamento.KeyValue)));
  DMConexao.Q.SQL.Add(' group by CONVENIADOS.CHAPA, CONTACORRENTE.DATA_FECHA_EMP ');
  DMConexao.Q.SQL.Add('order by chapa, CONTACORRENTE.DATA_FECHA_EMP');
  DMConexao.Q.Open;

  if not DMConexao.Q.IsEmpty then begin
    SalvarXLS(DMConexao.Q, 'Fechamento de Potim');
  end;

  Application.ProcessMessages;

  if not Erro then
  begin
    LimparCampos;
  end
  else
  begin
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
    LimparCampos;
  end;
  HabDesabBtnProcesso(False);
  FreeAndNil(SList);
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;
  btnExportar.Enabled := true;

end;

procedure TFrmExportEstabPadrao.LancNormalClick(Sender: TObject);
begin
  LancDebit.Checked := false;
  LancUnic.Checked := false;
end;

procedure TFrmExportEstabPadrao.LancDebitClick(Sender: TObject);
begin
  LancNormal.Checked := false;
  LancUnic.Checked := false;
end;

procedure TFrmExportEstabPadrao.CBJuntar669Click(Sender: TObject);
begin
  if CBJuntar669.Checked = true then
  begin
    Data1.Text := '';
    Data2.Text := '';
    Label6.Visible := true;
    Label7.Visible := true;
    Data1.Visible := true;
    Data2.Visible := true;
  end
  else
  begin
    Data1.Text := '';
    Data2.Text := '';
    Data1.Visible := false;
    Data2.Visible := false;
    Label6.Visible := false;
    Label7.Visible := false;
  end;
end;

procedure TFrmExportEstabPadrao.LancUnicClick(Sender: TObject);
begin
  LancDebit.Checked := false;
  LancNormal.Checked := false;
end;

end.
