object FSelRelModelo: TFSelRelModelo
  Left = 327
  Top = 173
  ActiveControl = DBGrid1
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Selecionar Modelo de Listagem'
  ClientHeight = 259
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 372
    Height = 259
    Hint = 'Pressione Enter para selecionar um modelo'#13#10'ou Esc para cancelar'
    Align = alClient
    DataSource = FNewGeraLista.DSRels
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ReadOnly = True
    ShowHint = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o do Modelo'
        Width = 333
        Visible = True
      end>
  end
  object PopupMenu1: TPopupMenu
    Left = 72
    Top = 160
    object AlterarDescriodoModelo1: TMenuItem
      Caption = 'Alterar Descri'#231#227'o do Modelo'
      OnClick = AlterarDescriodoModelo1Click
    end
  end
end
