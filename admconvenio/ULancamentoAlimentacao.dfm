inherited frmLancamentoAlimentacao: TfrmLancamentoAlimentacao
  Left = 1419
  Top = 54
  Caption = 'Lan'#231'amento de Cr'#233'dito Mensal Alimenta'#231#227'o'
  ClientHeight = 488
  ClientWidth = 1223
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1223
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 23
    Width = 1223
    Height = 66
    Align = alTop
    Caption = 'Filtro de Dados'
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 19
      Width = 46
      Height = 13
      Caption = 'Empresas'
    end
    object dbLkpEmpresas: TDBLookupComboBox
      Left = 8
      Top = 38
      Width = 385
      Height = 21
      KeyField = 'EMPRES_ID'
      ListField = 'NOME'
      ListFieldIndex = 1
      ListSource = dsEmpresas
      TabOrder = 0
      OnExit = dbLkpEmpresasExit
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 161
    Width = 1223
    Height = 278
    Align = alClient
    TabOrder = 4
  end
  object pnlDadosRenovacao: TPanel [3]
    Left = 0
    Top = 89
    Width = 1223
    Height = 72
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Label60: TLabel
      Left = 203
      Top = 17
      Width = 82
      Height = 13
      Caption = 'Data Renova'#231#227'o'
    end
    object dbDataRenovacao: TJvDateEdit
      Left = 203
      Top = 37
      Width = 110
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
    end
    object rgTipo: TRadioGroup
      Left = 10
      Top = 10
      Width = 187
      Height = 49
      Caption = 'Tipo'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Renova'#231#227'o'
        'Complemento')
      TabOrder = 0
    end
    object Button2: TButton
      Left = 333
      Top = 16
      Width = 94
      Height = 43
      Caption = 'Confirmar'
      TabOrder = 2
      OnClick = Button2Click
    end
    object btnExcluir: TButton
      Left = 437
      Top = 15
      Width = 108
      Height = 43
      Caption = 'Excluir Lan'#231'amento'
      TabOrder = 1
      OnClick = btnExcluirClick
    end
  end
  object dbGridAlim: TJvDBGrid [4]
    Left = 0
    Top = 161
    Width = 1223
    Height = 278
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsCredAlim
    DefaultDrawing = False
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    AutoAppend = False
    TitleButtons = True
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'conv_id'
        Title.Caption = 'Conv. ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Title.Caption = 'Titular'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'limite_mes'
        Title.Caption = 'Limite M'#234's'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'abono_mes'
        Title.Caption = 'Abono M'#234's'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'saldo_renovacao'
        Title.Caption = 'Saldo Renova'#231#227'o'
        Width = 100
        Visible = True
      end>
  end
  object pnlDados: TPanel [5]
    Left = 0
    Top = 439
    Width = 1223
    Height = 49
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    Visible = False
    DesignSize = (
      1223
      49)
    object Bevel1: TBevel
      Left = 186
      Top = 6
      Width = 2
      Height = 37
    end
    object Label57: TLabel
      Left = 292
      Top = 6
      Width = 86
      Height = 13
      Caption = 'Saldo Renova'#231#227'o'
    end
    object Label56: TLabel
      Left = 204
      Top = 6
      Width = 54
      Height = 13
      Caption = 'Abono M'#234's'
    end
    object Bevel3: TBevel
      Left = 512
      Top = 6
      Width = 2
      Height = 37
    end
    object Label85: TLabel
      Left = 688
      Top = 24
      Width = 64
      Height = 13
      Caption = 'Total Abono: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label88: TLabel
      Left = 840
      Top = 24
      Width = 119
      Height = 13
      Caption = 'Total Saldo Renova'#231#227'o: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object btnGravarConvAlim: TBitBtn
      Left = 6
      Top = 6
      Width = 80
      Height = 34
      Caption = '&Gravar'
      TabOrder = 0
      OnClick = btnGravarConvAlimClick
      Glyph.Data = {
        A6030000424DA603000000000000A60100002800000020000000100000000100
        08000000000000020000232E0000232E00005C00000000000000343434003535
        3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
        49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
        63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
        800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
        A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
        B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
        BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
        C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
        D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
        CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
        E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
        F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
        5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
        3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
        3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
        2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
        284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
        234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
        54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
        3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
        323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
        5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
        57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
        58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
        5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
        5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
        53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
        5B5B5B5B5B5B5B5B5B5B}
      NumGlyphs = 2
    end
    object btnCancelarConvAlim: TBitBtn
      Left = 92
      Top = 6
      Width = 80
      Height = 34
      Caption = '&Cancelar'
      TabOrder = 1
      Glyph.Data = {
        0E040000424D0E040000000000000E0200002800000020000000100000000100
        08000000000000020000232E0000232E000076000000000000000021CC001031
        DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
        DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
        FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
        F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
        F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
        FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
        E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
        ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
        FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
        C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
        CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
        D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
        E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
        E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
        F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
        75757575757575757575757575757575757575757575622F080000082F627575
        757575757575705E493434495E70757575757575753802030E11110E03023875
        7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
        7575757567354D5354555554534D35677575756307102A00337575442C151007
        63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
        367575604B545568345E7575745B544B607575171912301C3700317575401219
        1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
        057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
        0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
        217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
        3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
        65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
        757575756C566058483434485860566C75757575754324283237373228244375
        75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
        757575757575736A5D55555D6A73757575757575757575757575757575757575
        757575757575757575757575757575757575}
      NumGlyphs = 2
    end
    object edtSaldoRenovacao: TEdit
      Left = 292
      Top = 21
      Width = 93
      Height = 21
      TabOrder = 5
      Text = '0,00'
    end
    object edtAbonoMes: TEdit
      Left = 204
      Top = 21
      Width = 64
      Height = 21
      TabOrder = 4
      Text = '0,00'
    end
    object Button1: TButton
      Left = 398
      Top = 8
      Width = 94
      Height = 34
      Caption = 'Alterar p/ Todos'
      TabOrder = 3
      OnClick = Button1Click
    end
    object btnImportar: TBitBtn
      Left = 523
      Top = 7
      Width = 158
      Height = 34
      Hint = 'Importar'
      Anchors = [akLeft]
      Caption = 'Importa'#231#227'o de Planilha'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnImportarClick
      NumGlyphs = 2
    end
    object lblTotalRenovacao: TJvValidateEdit
      Left = 953
      Top = 24
      Width = 93
      Height = 18
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = True
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
    end
    object lblTotalAbono: TJvValidateEdit
      Left = 760
      Top = 24
      Width = 75
      Height = 18
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = True
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
    end
    object btnLimpar: TButton
      Left = 1085
      Top = 8
      Width = 116
      Height = 33
      Caption = 'Novo Lan'#231'amento'
      TabOrder = 8
      OnClick = btnLimparClick
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 628
    Top = 48
  end
  object dsEmpresas: TDataSource
    DataSet = qEmpresas
    Left = 888
    Top = 40
  end
  object qEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select emp.empres_id, CONCAT(emp.empres_id,'#39' - '#39',emp.nome) nome,' +
        ' emp.liberada, convert(varchar,'
      
        '(select top 1 DATA_FECHA from DIA_FECHA where EMPRES_ID=emp.EMPR' +
        'ES_ID and month(data_fecha) > MONTH(GETDATE ()) and year(DATA_FE' +
        'CHA) = year(GETDATE ())),103) fechamento1 '
      'from empresas emp'
      'where emp.tipo_credito = 2 or emp.TIPO_CREDITO = 3'
      'and emp.liberada = '#39'S'#39' AND emp.APAGADO = '#39'N'#39)
    Left = 856
    Top = 40
    object qEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qEmpresasnome: TStringField
      FieldName = 'nome'
      ReadOnly = True
      Size = 75
    end
    object qEmpresasliberada: TStringField
      FieldName = 'liberada'
      FixedChar = True
      Size = 1
    end
    object qEmpresasfechamento1: TStringField
      FieldName = 'fechamento1'
      ReadOnly = True
      Size = 30
    end
  end
  object QCredAlim: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id_1'
        Size = -1
        Value = Null
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      #9'c.conv_id,'
      '    c.empres_id, '
      '    c.titular, '
      #9'coalesce(c.limite_mes,0.00) limite_mes, '
      '    coalesce(alc.abono_valor,0.00) abono_mes, '
      '    coalesce(alc.renovacao_valor, 0.00) saldo_renovacao, '
      #9'alr.RENOVACAO_ID,'
      #9'alr.DATA_RENOVACAO,'
      #9'alr.TIPO_CREDITO'
      'from'
      '    conveniados c'
      
        #9'left join ALIMENTACAO_RENOVACAO alr on alr.EMPRES_ID = c.EMPRES' +
        '_ID and alr.DATA_RENOVACAO = (SELECT TOP 1 DATA_RENOVACAO FROM A' +
        'LIMENTACAO_RENOVACAO where EMPRES_ID = :empres_id_1 ORDER BY DAT' +
        'A_RENOVACAO ASC)'
      
        #9'left join ALIMENTACAO_RENOVACAO_CREDITOS alc on alc.RENOVACAO_I' +
        'D = alr.RENOVACAO_ID and alc.CONV_ID = c.CONV_ID'
      'where c.empres_id = :empres_id '
      'AND C.LIBERADO = '#39'S'#39' AND C.APAGADO = '#39'N'#39
      'order by c.titular')
    Left = 712
    Top = 40
    object QCredAlimconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QCredAlimempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QCredAlimtitular: TStringField
      FieldName = 'titular'
      ReadOnly = True
      Size = 58
    end
    object QCredAlimlimite_mes: TBCDField
      FieldName = 'limite_mes'
      DisplayFormat = '#0.00'
      Precision = 15
      Size = 2
    end
    object QCredAlimabono_mes: TBCDField
      FieldName = 'abono_mes'
      DisplayFormat = '#0.00'
      Precision = 18
      Size = 2
    end
    object QCredAlimsaldo_renovacao: TBCDField
      FieldName = 'saldo_renovacao'
      DisplayFormat = '#0.00'
      Precision = 18
      Size = 2
    end
    object QCredAlimRENOVACAO_ID: TIntegerField
      FieldName = 'RENOVACAO_ID'
    end
    object QCredAlimDATA_RENOVACAO: TWideStringField
      FieldName = 'DATA_RENOVACAO'
      Size = 10
    end
    object QCredAlimTIPO_CREDITO: TStringField
      FieldName = 'TIPO_CREDITO'
      FixedChar = True
      Size = 1
    end
  end
  object dsCredAlim: TDataSource
    DataSet = QCredAlim
    Left = 745
    Top = 41
  end
  object qUpdate: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'abono_mes'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'saldo_renovacao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'update conveniados set abono_mes = :abono_mes, saldo_renovacao =' +
        ' :saldo_renovacao')
    Left = 776
    Top = 40
  end
  object tExcel: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 816
    Top = 40
  end
end
