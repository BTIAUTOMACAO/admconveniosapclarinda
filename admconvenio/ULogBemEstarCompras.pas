unit ULogBemEstarCompras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  StdCtrls, Mask, JvExMask, JvToolEdit, Menus, ExtCtrls, JvMemoryDataset;

type
  TFLogBemEstarCompras = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    GroupBox2: TGroupBox;
    datafin: TJvDateEdit;
    DataIni: TJvDateEdit;
    GridLogRecargaCantinex: TJvDBGrid;
    Panel4: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    btnGravar: TBitBtn;
    bntGerarPDF: TBitBtn;
    QLogCantinex: TADOQuery;
    DSLogCantinex: TDataSource;
    QLogCantinexCONV_ID: TIntegerField;
    QLogCantinexTITULAR: TStringField;
    MLogRecargaCantinex: TJvMemoryData;
    MLogRecargaCantinexID: TIntegerField;
    MLogRecargaCantinexvalor: TCurrencyField;
    MLogRecargaCantinexvalor_taxa: TCurrencyField;
    MLogRecargaCantinexvalor_recarga: TCurrencyField;
    MLogRecargaCantinexdata_recarga: TDateField;
    MLogRecargaCantinexconv_id: TIntegerField;
    MLogRecargaCantinextitular: TStringField;
    MLogRecargaCantinexmarcado: TBooleanField;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    txtConvID: TEdit;
    txtNome: TEdit;
    Label3: TLabel;
    cbbStatus: TComboBox;
    Label4: TLabel;
    btnCancelar: TBitBtn;
    QLogCantinexLOG_ID: TIntegerField;
    QLogCantinexDATA_SOLICITACAO: TDateField;
    QLogCantinexBRUTO: TCurrencyField;
    QLogCantinexTAXA: TCurrencyField;
    MLogRecargaCantinexvalor_mensalidade: TCurrencyField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure GridLogRecargaCantinexDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    aluno_sel : String;
  public
    { Public declarations }
    procedure BuscaLotes;
  end;

var
  FLogBemEstarCompras: TFLogBemEstarCompras;

implementation

uses cartao_util, URotinasTexto, impressao, Math, DM;
{$R *.dfm}

procedure TFLogBemEstarCompras.ButListaEmpClick(Sender: TObject);
begin
  inherited;
  BuscaLotes;
  txtConvID.Text := '';
  txtNome.Text := '';
  DataIni.Text := '';
  datafin.Text := '';
  cbbStatus.ItemIndex := 0;
  btnGravar.SetFocus;
end;

procedure TFLogBemEstarCompras.BuscaLotes;
begin
  QLogCantinex.Close;
  QLogCantinex.SQL.Clear;
  QLogCantinex.SQL.Add('SELECT LV.LOG_ID,LV.CONV_ID, CV.TITULAR, LV.DT_CADASTRO DATA_SOLICITACAO, LV.VALOR_MENSALIDADE BRUTO, ');
  QLogCantinex.SQL.Add(' LV.VALOR_TAXA TAXA FROM LOG_VENDA_INDIVIDUAL_BEM_ESTAR LV INNER JOIN CONVENIADOS CV ');
  QLogCantinex.SQL.Add(' ON CV.CONV_ID = LV.CONV_ID WHERE ');
  QLogCantinex.SQL.Add(' CV.TITULAR LIKE ''%' + txtNome.Text + '%''');

  if txtConvID.Text <> '' then
    QLogCantinex.SQL.Add(' AND CV.CONV_ID = ' + fnRemoverCaracterSQLInjection(txtConvID.Text));

  if cbbStatus.ItemIndex = 0 then
    QLogCantinex.SQL.Add(' and LV.STATUS = ''P''')
    else if cbbStatus.ItemIndex = 1 then
      QLogCantinex.SQL.Add(' and LV.STATUS = ''E''')
      else
        QLogCantinex.SQL.Add(' and LV.STATUS = ''P''');


  if((DataIni.Date <> 0) or (datafin.Date <> 0)) then
  begin

     if datafin.Date <> 0 then
     begin
        QLogCantinex.SQL.Add(' AND LV.DT_CADASTRO  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataFin.Date)));
     end
     else
     QLogCantinex.SQL.Add(' AND LV.DT_CADASTRO  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',Date)));

  end;

  QLogCantinex.Open;
  if QLogCantinex.IsEmpty then
  begin
    MsgInf('N�o foi encontrado nenhum registro de recebimento Bem-Estar.');
    DataIni.SetFocus;
  end;
  QLogCantinex.First;
  MLogRecargaCantinex.Open;
  MLogRecargaCantinex.EmptyTable;
  MLogRecargaCantinex.DisableControls;
  while not QLogCantinex.Eof do begin
      MLogRecargaCantinex.Append;
      MLogRecargaCantinexID.AsInteger  := QLogCantinexLOG_ID.AsInteger;
      MLogRecargaCantinexvalor_recarga.AsCurrency := QLogCantinexBRUTO.AsCurrency;
      MLogRecargaCantinexvalor_taxa.AsCurrency := QLogCantinexTAXA.AsCurrency;
      MLogRecargaCantinexdata_recarga.AsDateTime := QLogCantinexDATA_SOLICITACAO.AsDateTime;
      MLogRecargaCantinexconv_id.AsInteger := QLogCantinexCONV_ID.AsInteger;
      MLogRecargaCantinextitular.AsString := QLogCantinexTITULAR.AsString;
      MLogRecargaCantinexvalor_mensalidade.AsFloat := QLogCantinexBRUTO.AsFloat - QLogCantinexTAXA.AsFloat;
      MLogRecargaCantinexmarcado.AsBoolean := False;
      MLogRecargaCantinex.Post;
      QLogCantinex.Next;
  end;
  MLogRecargaCantinex.First;
  MLogRecargaCantinex.EnableControls;
end;

procedure TFLogBemEstarCompras.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  if MLogRecargaCantinex.IsEmpty then Exit;
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexMarcado.AsBoolean := not MLogRecargaCantinexMarcado.AsBoolean;
    MLogRecargaCantinex.Post;
end;

procedure TFLogBemEstarCompras.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MLogRecargaCantinex.IsEmpty then Exit;
  MLogRecargaCantinex.DisableControls;
  marca := MLogRecargaCantinex.GetBookmark;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.eof do begin
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexmarcado.AsBoolean := true;
    MLogRecargaCantinex.Post;
    MLogRecargaCantinex.Next;
  end;
  MLogRecargaCantinex.GotoBookmark(marca);
  MLogRecargaCantinex.FreeBookmark(marca);
  MLogRecargaCantinex.EnableControls;
end;

procedure TFLogBemEstarCompras.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
   if MLogRecargaCantinex.IsEmpty then Exit;
  MLogRecargaCantinex.DisableControls;
  marca := MLogRecargaCantinex.GetBookmark;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.eof do begin
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexMarcado.AsBoolean := false;
    MLogRecargaCantinex.Post;
    MLogRecargaCantinex.Next;
  end;
  MLogRecargaCantinex.GotoBookmark(marca);
  MLogRecargaCantinex.FreeBookmark(marca);
  MLogRecargaCantinex.EnableControls;
end;

procedure TFLogBemEstarCompras.btnGravarClick(Sender: TObject);
var conv_id : Integer;
    strComand,valorDaRecarga : String;
begin
  inherited;
  //PEGA OS DADOS DO CONVENIADO
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.Eof  do
  begin
    if (MLogRecargaCantinexmarcado.AsBoolean = True) then
    begin
      //EXECUTA ALTERA��O NO LIMITE_MES
      conv_id := MLogRecargaCantinexconv_id.AsInteger;
      strComand := 'update conveniados set GRUPO_CONV_EMP = 2951 where CONV_ID = '+ IntToStr(conv_id);
      DMConexao.ExecuteNonQuery(strComand);
      strComand := '';
      strComand := 'update CARTOES SET JAEMITIDO = '+ QuotedStr('N') + ' where CONV_ID = ' + IntToStr(MLogRecargaCantinexconv_id.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);
      strComand := '';
      strComand := 'update LOG_VENDA_INDIVIDUAL_BEM_ESTAR SET STATUS = '+ QuotedStr('E') + ', DT_ALTERACAO = GETDATE() where LOG_ID = ' + IntToStr(MLogRecargaCantinexID.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);
      strComand := '';
      strComand := 'update conveniados SET LIBERADO = '+QuotedStr('S') + ' where CONV_ID = ' + IntToStr(MLogRecargaCantinexconv_id.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);
      strComand := '';
      strComand := 'update CARTOES SET LIBERADO = '+QuotedStr('S') + ' where CONV_ID = ' + IntToStr(MLogRecargaCantinexconv_id.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);
    end;
    MLogRecargaCantinex.Next;
  end;
  MsgInf('Transa��o confirmada com sucesso!');


end;

procedure TFLogBemEstarCompras.GridLogRecargaCantinexDblClick(
  Sender: TObject);
begin
  inherited;
  ButMarcaDesmEmpClick(nil);
end;

procedure TFLogBemEstarCompras.btnCancelarClick(Sender: TObject);
var conv_id : Integer;
    strComand : string;
begin
  inherited;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.Eof  do
  begin
    if (MLogRecargaCantinexmarcado.AsBoolean = True) then
    begin
      strComand := 'update CARTOES SET LIBERADO = '+ QuotedStr('N') + '  where CONV_ID = ' + IntToStr(MLogRecargaCantinexconv_id.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);
      strComand := 'update LOG_VENDA_INDIVIDUAL_BEM_ESTAR SET STATUS = '+ QuotedStr('C') + ', DT_ALTERACAO = GETDATE() where LOG_ID = ' + IntToStr(MLogRecargaCantinexID.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);

    end;
    MLogRecargaCantinex.Next;
  end;
  MsgInf('Cancelamento(s) realizado(s) com sucesso!');
end;

procedure TFLogBemEstarCompras.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
     if Key = VK_F12 then ButMarcaDesmEmpClick(ButMarcaDesmEmp);
     if Key = VK_F6 then ButMarcaTodasEmpClick(ButMarcaTodasEmp);
     if Key = VK_F7 then ButDesmarcaTodosEmpClick(ButDesmarcaTodosEmp);
end;



procedure TFLogBemEstarCompras.FormCreate(Sender: TObject);
begin
  inherited;
  //MsgInf(DMConexao.ConfigBIN_BEM_ESTAR.AsString);
end;

end.
