unit SispagEntidades;

interface
 
 { TACBrCedente }
type
  TCedente  = class;
  TSacado   = class;

  TCedente = class
  private
    fCodigoTransmissao: String;
    fLogradouro: String;
    fBairro: String;
    fNumeroRes: String;
    fCEP: String;
    fCidade: String;
    fCodigoCedente: String;
    fComplemento: String;
    fTelefone: String;
    fNomeCedente   : String;
    fAgencia       : String;
    fAgenciaDigito : String;
    fConta         : String;
    fContaDigito   : String;
    fModalidade    : String;
    fConvenio      : String;
    fCNPJCPF       : String;
    fUF            : String;
    fCodBanco      : String;
    procedure SetAgencia(const Value: String);
    procedure SetAgenciaDigito(const Value: String);
    procedure SetBairro(const Value: String);
    procedure SetCEP(const Value: String);
    procedure SetCidade(const Value: String);
    procedure SetCNPJCPF(const Value: String);
    procedure SetCodigoCedente(const Value: String);
    procedure SetCodigoTransmissao(const Value: String);
    procedure SetComplemento(const Value: String);
    procedure SetConta(const Value: String);
    procedure SetContaDigito(const Value: String);
    procedure SetConvenio(const Value: String);
    procedure SetLogradouro(const Value: String);
    procedure SetModalidade(const Value: String);
    procedure SetNomeCedente(const Value: String);
    procedure SetNumeroRes(const Value: String);
    procedure SetTelefone(const Value: String);
    procedure SetUF(const Value: String);
    procedure SetCodBanco(const value: String);

  public
    property NomeCedente  : String read FNomeCedente write SetNomeCedente;
    property CodigoCedente: String read FCodigoCedente write SetCodigoCedente;
    property CodigoTransmissao : String read FCodigoTransmissao write SetCodigoTransmissao;
    property Agencia      : String read FAgencia write SetAgencia;
    property AgenciaDigito: String read FAgenciaDigito write SetAgenciaDigito;
    property Conta        : String read FConta write SetConta;
    property ContaDigito  : String read FContaDigito write SetContaDigito;
    property Modalidade   : String read FModalidade write SetModalidade;
    property Convenio     : String read FConvenio write SetConvenio;
    property CNPJCPF      : String read FCNPJCPF write SetCNPJCPF;
    property Logradouro  : String read FLogradouro write SetLogradouro;
    property NumeroRes   : String read FNumeroRes write SetNumeroRes;
    property Complemento : String read FComplemento write SetComplemento;
    property Bairro      : String read FBairro write SetBairro;
    property Cidade      : String read FCidade write SetCidade;
    property UF          : String read FUF write SetUF;
    property CEP         : String read FCEP write SetCEP;
    property Telefone    : String read FTelefone write SetTelefone;
    property CodBanco    : String read FCodBanco write SetCodBanco;
    constructor Create;
    Destructor  Destroy; Override;// declara��o do metodo destrutor
  end;

  TSacado = class
    private
    fNome                   : String;
    fEnderecoEmpresa        : String;
    fComplemeto             : String;
    fCidade                 : String;
    fCEP                    : String;
    fEstado                 : String;
    fAgencia                : String;
    fConta                  : String;
    fCPFCNPJ                : String;
    fNumero                 : Integer;
    fCodBanco               : String;
    procedure SetAgencia(const Value: String);
    procedure SetCEP(const Value: String);
    procedure SetCidade(const Value: String);
    procedure SetComplemeto(const Value: String);
    procedure SetConta(const Value: String);
    procedure SetCPFCNPJ(const Value: String);
    procedure SetEnderecoEmpresa(const Value: String);
    procedure SetEstado(const Value: String);
    procedure SetNome(const Value: String);
    procedure SetNumero(const Value: Integer);
    procedure SetCodBanco(const value: String);

    public
    property Nome                   : String read FNome write SetNome;   //Nome do Credenciado Favorecido
    property EnderecoEmpresa        : String read FEnderecoEmpresa write SetEnderecoEmpresa; //Endere�o do Credenciado Favorecido
    property Complemeto             : String read FComplemeto write SetComplemeto; //Complemento de Endere�o do Credenciado Favorecido
    property Cidade                 : String read FCidade write SetCidade;
    property CEP                    : String read FCEP write SetCEP;
    property Estado                 : String read FEstado write SetEstado;
    property Numero                 : Integer read FNumero write SetNumero;
    property Agencia                : String read FAgencia write SetAgencia;
    property Conta                  : String read FConta   write SetConta;
    property CPFCNPJ                : String read FCPFCNPJ write SetCPFCNPJ;
    property CodBanco               : String read FCodBanco write SetCodBanco;
    constructor Create;
    Destructor  Destroy; Override; // declara��o do metodo destrutor
  end;



implementation

uses uRotinasPopUp, URotinasTexto, cartao_util;

constructor TCedente.Create;
begin

end;

destructor TCedente.Destroy;
 
begin
 
// metodo destrutor
 
inherited;
 
end;

procedure TCedente.SetAgencia(const Value: String);
begin
  FAgencia := Value;
end;

procedure TCedente.SetAgenciaDigito(const Value: String);
begin
  FAgenciaDigito := Value;
end;

procedure TCedente.SetBairro(const Value: String);
begin
  FBairro := Value;
end;

procedure TCedente.SetCEP(const Value: String);
begin
  FCEP := Value;
end;

procedure TCedente.SetCidade(const Value: String);
begin
  FCidade := Value;
end;

procedure TCedente.SetCNPJCPF(const Value: String);
begin
  FCNPJCPF := Value;
end;

procedure TCedente.SetCodigoCedente(const Value: String);
begin
  FCodigoCedente := Value;
end;

procedure TCedente.SetCodigoTransmissao(const Value: String);
begin
  FCodigoTransmissao := Value;
end;

procedure TCedente.SetComplemento(const Value: String);
begin
  FComplemento := Value;
end;

procedure TCedente.SetConta(const Value: String);
begin
  FConta := Value;
end;

procedure TCedente.SetContaDigito(const Value: String);
begin
  FContaDigito := Value;
end;

procedure TCedente.SetConvenio(const Value: String);
begin
  FConvenio := Value;
end;

procedure TCedente.SetLogradouro(const Value: String);
begin
  FLogradouro := Value;
end;

procedure TCedente.SetModalidade(const Value: String);
begin
  FModalidade := Value;
end;

procedure TCedente.SetNomeCedente(const Value: String);
begin
  FNomeCedente := Value;
end;

procedure TCedente.SetNumeroRes(const Value: String);
begin
  FNumeroRes := Value;
end;

procedure TCedente.SetTelefone(const Value: String);
begin
  FTelefone := Value;
end;

procedure TCedente.SetUF(const Value: String);
begin
  FUF := Value;
end;

procedure TCedente.SetCodBanco(const value: String);
begin
  fCodBanco := Value;
end;


constructor TSacado.Create;
begin
  fNome             := '';
  fEnderecoEmpresa  := '';
  fComplemeto       := '';
  fCidade           := '';
  fCEP              := '';
  fEstado           := '';
  fAgencia          := '';
  fConta            := '';
  fCodBanco         := '';
  fNumero           := 0;
end;

destructor TSacado.Destroy;
begin
  MsgInf('Executou o destructor do TSacado');
  //inherited;
end;

procedure TSacado.SetCEP(const Value: String);
begin
  FCEP := Value;
end;

procedure TSacado.SetCidade(const Value: String);
begin
  FCidade := Value;
end;

procedure TSacado.SetComplemeto(const Value: String);
begin
  FComplemeto := Value;
end;

procedure TSacado.SetEnderecoEmpresa(const Value: String);
begin
  FEnderecoEmpresa := Value;
end;

procedure TSacado.SetEstado(const Value: String);
begin
  FEstado := Value;
end;

procedure TSacado.SetNome(const Value: String);
begin
  FNome := Value;
end;

procedure TSacado.SetNumero(const Value: Integer);
begin
  FNumero := Value;
end;

procedure TSacado.SetConta(const Value: String);
begin
  FConta := Value;
end;

procedure TSacado.SetAgencia(const Value: String);
begin
  FAgencia := Value;
end;

procedure TSacado.SetCPFCNPJ(const Value: String);
begin
  FCPFCNPJ := Value;
end;

procedure TSacado.SetCodBanco(const Value: String);
begin
  fCodBanco := value;
end;


end.
 