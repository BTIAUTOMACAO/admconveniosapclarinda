unit UCancelaAutor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Mask, ToolEdit, CurrEdit, XMLDoc,
  InvokeRegistry, Rio, SOAPHTTPClient, JvExStdCtrls, JvEdit, JvValidateEdit;

type
  TFCancelaAutor = class(TForm)
    txtAutor: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    btnCancela: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    txtConv: TLabel;
    txtEmp: TLabel;
    txtCred: TLabel;
    txtData: TLabel;
    txtValTot: TLabel;
    //txtValor: TCurrencyEdit;
    LabAguarde: TLabel;
    HTTPRIO1: THTTPRIO;
    txtValor: TJvValidateEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    Path_WebService: string;
    cred_id, autorizacao, transacao: Integer;
  end;

var  FCancelaAutor: TFCancelaAutor;

implementation

uses cartao_util, DM, UMenu, UTipos,
  MCancelarAutorizacao, MEfetuarTransacao, wsconvenio,
  URotinasTexto;

{$R *.dfm}

procedure TFCancelaAutor.BitBtn1Click(Sender: TObject);
var
  Retorno, valor,s1,s2 : string;
  Param  : IXMLTCancelarAutorizacaoParam;
  Ret    : IXMLTCancelarAutorizacaoRet;
  Param1 : IXMLTCancelarTransacaoParam;
  Ret1   : IXMLTCancelarTransacaoRet;
  //cancelarTrans : TCancelarTrasnacao;  cancelarTransRet : TCancelarTransacaoRet;
//  sl : TStrings;
//  cred : TCredenciado;
begin
  if Application.MessageBox('Confirma o cancelamento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    //                   janela   ,campo                                                   , vr_ant          , vr_novo     , operador    , operacao                 , cadastro   , id                                          , detalhe                                                ,)
    if not (DMConexao.GravaLog(Self.Name,StringReplace('Autoriza��o ID: ',': ','',[rfReplaceAll]),txtValTot.Caption,txtValor.Text,Operador.Nome,'Cancelamento Autoriza��o',txtAutor.Text,'', '')) then begin
      Abort;
    end;
    if txtValor.Value <= 0 then
    begin
      ShowMessage('Valor do cancelamento inv�lido');
      txtValor.SetFocus;
      Exit;
    end;
    s1 := Copy(txtValTot.Caption, 1, Length(txtValTot.Caption)-3);
    s2 := Copy(txtValTot.Caption, Length(txtValTot.Caption)-2, 3);
    if txtValor.Value > StrToFloat(fnRemoveCaracters('.',s1) + s2)  then
    begin
      ShowMessage('Valor do cancelamento maior que o da autoriza��o');
      txtValor.SetFocus;
      Exit;
    end;
    Try
      LabAguarde.Show;

      if transacao = 0 then
      begin
        Param := NewXMLDocument.GetDocBinding('CANCELAR_AUTORIZACAO_PARAM', TXMLTCancelarAutorizacaoParam, '') as IXMLTCancelarAutorizacaoParam;
        Param.CREDENCIADO.CODACESSO := DMConexao.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id));
        Param.CREDENCIADO.SENHA := Crypt('D',DMConexao.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS');
        Param.AUTORIZACAO := autorizacao;
        valor := IntToStr(Trunc(txtValor.Value*100)); //passar como inteiro.
        Param.VALOR := StrToInt(valor);
        Param.OPERADOR := Operador.Nome;
        try
          Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').MCancelarAutorizacao(Param.OwnerDocument.XML.Text);
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar autoriza��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak+
                    'Favor entrar em contato com o suporte t�cnico');
            LabAguarde.Hide;
            Exit;
          end;
        end;
        Ret := LoadXMLData(retorno).GetDocBinding('CANCELAR_AUTORIZACAO_RET',TXMLTCancelarAutorizacaoRet,'') as IXMLTCancelarAutorizacaoRet;
        if Ret.STATUS <> 0 then
        begin
          MsgErro('N�o foi poss�vel cancelar a autoriza��o.'+sLineBreak+
                  'Motivo: '+Ret.MSG+sLineBreak);
          LabAguarde.Hide;
          exit;
        end;
      end
      else
      begin
        Param1 := NewXMLDocument.GetDocBinding('CANCELAR_TRANSACAO_PARAM', TXMLTCancelarTransacaoParam, '') as IXMLTCancelarTransacaoParam;
        Param1.CREDENCIADO.CODACESSO := DMConexao.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id));
        Param1.CREDENCIADO.SENHA := Crypt('D',DMConexao.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS');
        Param1.TRANSID := transacao;
        Param1.OPERADOR := Operador.Nome;
        try
          Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').MCancelarTransacao(Param1.OwnerDocument.XML.Text);
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar transa��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak+
                    'Favor entrar em contato com o suporte t�cnico');
            LabAguarde.Hide;
            Exit;
          end;
        end;
        Ret1 := LoadXMLData(retorno).GetDocBinding('CANCELAR_TRANSACAO_RET',TXMLTCancelarTransacaoRet,'') as IXMLTCancelarTransacaoRet;
        if Ret1.STATUS <> 0 then
        begin
          MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
                  'Motivo: '+Ret1.MSG+sLineBreak);
          LabAguarde.Hide;
          exit;
        end;
      end;
{
      Param :=  NewXMLDocument.GetDocBinding('CANCELAR_AUTORIZACAO_PARAM', TXMLTCancelarAutorizacaoParam, '') as IXMLTCancelarAutorizacaoParam;
      Param.AUTORIZACAO := StrToInt(txtAutor.Text);
      valor := IntToStr(Trunc(txtValor.Value*100)); //passar como inteiro.
      Param.VALOR       := StrToInt(valor);
      Param.OPERADOR    := Operador.Nome;
      try
        Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').CancelarAutorizacao(Param.OwnerDocument.XML.Text);
        Ret := LoadXMLData(retorno).GetDocBinding('CANCELAR_AUTORIZACAO_RET',TXMLTCancelarAutorizacaoRet,'') as IXMLTCancelarAutorizacaoRet;
      except
        on e:Exception do
        begin
          MsgErro('Erro ao conectar a administradora para cancelar a(s) autoriza��o(�es) de venda.'+sLineBreak+
                  'Erro: '+e.Message+sLineBreak+
                  'Favor entrar em contato com a administradora para efetuar o cancelamento da(s) autoriza��o(�es)');
          Exit;
        end;
      end;
      if Ret.STATUS = 0 then
        MsgInf('Cancelamento efetuado com sucesso!')
      else  //problemas ao cancelar;
      begin
        MsgErro('N�o foi poss�vel cancelar a autoriza��o n� '+txtAutor.Text+' na administradora.'+sLineBreak+
                'Motivo: '+Ret.MSG+sLineBreak+
                'Favor entrar em contato com a administradora para efetuar o cancelamento da autoriza��o.');
      end;
}
      LabAguarde.Hide;
      MsgInf('Autoriza��o Cancelada com Sucesso!');
    except on E:Exception do
      ShowMessage('Erro: '+E.Message);
    end;
  end;
{  if Application.MessageBox('Confirma o cancelamento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    if txtValor.Value <= 0 then
    begin
      ShowMessage('Valor do cancelamento inv�lido');
      txtValor.SetFocus;
      Exit;
    end;
    if txtValor.Value > StrToFloat(txtValTot.Caption) then
    begin
      ShowMessage('Valor do cancelamento maior que o da autoriza��o');
      txtValor.SetFocus;
      Exit;
    end;
    Try
      LabAguarde.Show;
      //showmessage(inttostr(transacao));
      if transacao = 0 then
      begin
        Param := NewXMLDocument.GetDocBinding('CANCELAR_AUTORIZACAO_PARAM', TXMLTCancelarAutorizacaoParam, '') as IXMLTCancelarAutorizacaoParam;
        Param.CREDENCIADO.CODACESSO := DM1.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id));
        Param.CREDENCIADO.SENHA := Crypt('D',DM1.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS');
        Param.AUTORIZACAO := autorizacao;
        valor := IntToStr(Trunc(txtValor.Value*100)); //passar como inteiro.
        Param.VALOR := StrToInt(valor);
        Param.OPERADOR := Operador.Nome;
        try
          Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').MCancelarAutorizacao(Param.OwnerDocument.XML.Text);
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar autoriza��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak+
                    'Favor entrar em contato com o suporte t�cnico');
            LabAguarde.Hide;
            Exit;
          end;
        end;
        Ret := LoadXMLData(retorno).GetDocBinding('CANCELAR_AUTORIZACAO_RET',TXMLTCancelarAutorizacaoRet,'') as IXMLTCancelarAutorizacaoRet;
        if Ret.STATUS <> 0 then
        begin
          //showmessage('m');
          MsgErro('N�o foi poss�vel cancelar a autoriza��o.'+sLineBreak+
                  'Motivo: '+Ret.MSG+sLineBreak);
          //showmessage('n');
          LabAguarde.Hide;
          exit;
        end;
      end
      else
      begin}
{        cred := TCredenciado.Create(DM1.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id)),
                Crypt('D',DM1.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS'));
        cancelarTrans := TCancelarTrasnacao.Create(cred,transacao,Operador.Nome);
        try
          Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak);
            LabAguarde.Hide;
            Exit;
          end;
        end;
        cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
        if cancelarTransRet.Status <> 0 then
        begin
          MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
                  'Motivo: '+cancelarTransRet.Mensagem+sLineBreak);
          LabAguarde.Hide;
          exit;
        end;
        LabAguarde.Hide;
        exit;}
{        Param1 := NewXMLDocument.GetDocBinding('CANCELAR_TRANSACAO_PARAM', TXMLTCancelarTransacaoParam, '') as IXMLTCancelarTransacaoParam;
        Param1.CREDENCIADO.CODACESSO := DM1.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id));
        Param1.CREDENCIADO.SENHA := Crypt('D',DM1.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS');
        Param1.TRANSID := transacao;
        Param1.OPERADOR := Operador.Nome;
        try
          //showmessage('g');
          //showmessage(Param.OwnerDocument.XML.Text);
          Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').MCancelarTransacao(Param1.OwnerDocument.XML.Text);
          //showmessage('h');
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar transa��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak+
                    'Favor entrar em contato com o suporte t�cnico');
            LabAguarde.Hide;
            Exit;
          end;
        end;
        Ret1 := LoadXMLData(retorno).GetDocBinding('CANCELAR_TRANSACAO_RET',TXMLTCancelarTransacaoRet,'') as IXMLTCancelarTransacaoRet;
        if Ret1.STATUS <> 0 then
        begin
          MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
                  'Motivo: '+Ret1.MSG+sLineBreak);
          LabAguarde.Hide;
          exit;
        end;

        Param := NewXMLDocument.GetDocBinding('CANCELAR_AUTORIZACAO_PARAM', TXMLTCancelarAutorizacaoParam, '') as IXMLTCancelarAutorizacaoParam;
        Param.CREDENCIADO.CODACESSO := DM1.ExecuteScalar('select codacesso from credenciados where cred_id = '+IntToStr(cred_id));
        Param.CREDENCIADO.SENHA := Crypt('D',DM1.ExecuteScalar('select senha from credenciados where cred_id = '+IntToStr(cred_id)),'BIGCOMPRAS');
        Param.AUTORIZACAO := autorizacao;
        valor := IntToStr(Trunc(txtValor.Value*100)); //passar como inteiro.
        Param.VALOR := StrToInt(valor);
        Param.OPERADOR := Operador.Nome;
        try
          Retorno := GetwsconvenioSoap(False,Path_WebService+'wsconvenio.asmx').MCancelarAutorizacao(Param.OwnerDocument.XML.Text);
        except
          on e:Exception do
          begin
            MsgErro('Erro ao cancelar autoriza��o.'+sLineBreak+
                    'Erro: '+e.Message+sLineBreak+
                    'Favor entrar em contato com o suporte t�cnico');
            LabAguarde.Hide;
            Exit;
          end;
        end;
        Ret := LoadXMLData(retorno).GetDocBinding('CANCELAR_AUTORIZACAO_RET',TXMLTCancelarAutorizacaoRet,'') as IXMLTCancelarAutorizacaoRet;
        if Ret.STATUS <> 0 then
        begin
          MsgErro('N�o foi poss�vel cancelar a autoriza��o.'+sLineBreak+
                  'Motivo: '+Ret.MSG+sLineBreak);
          LabAguarde.Hide;
          exit;
        end;
        
      end;
      LabAguarde.Hide;
      MsgInf('Autoriza��o Cancelada com Sucesso!');
    except on E:Exception do
      ShowMessage('Erro: '+E.Message);
    end;
  end;}
end;

procedure TFCancelaAutor.FormCreate(Sender: TObject);
begin
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
  DMConexao.Config.Open;
  Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString;
  if Path_WebService = '' then
    ShowMessage('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');
  DMConexao.Config.Close;
  HTTPRIO1.URL := Path_WebService + 'wsconvenio.asmx';;
end;

procedure TFCancelaAutor.btnCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFCancelaAutor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
end;

end.
