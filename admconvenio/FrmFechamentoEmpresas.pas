unit FrmFechamentoEmpresas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, DB, ZAbstractRODataset,
  ZDataset, Math, Mask, ToolEdit, DBCtrls, ClipBrd, ZAbstractDataset,
  frxClass, frxDBSet, JvExMask, JvToolEdit, ADODB, frxExportPDF,
  JvExControls, JvDBLookup, JvDBControls, Grids, DBGrids;

type
  TFrmFechamentoEmpresa = class(TForm)
    Panel1: TPanel;
    GBPesquisa: TGroupBox;
    Label2: TLabel;
    EDataFechamento: TJvDateEdit;
    CBEmpMovim: TCheckBox;
    Label3: TLabel;
    EEmpres_id: TEdit;
    Label4: TLabel;
    CBTodos: TCheckBox;
    CBDrogaBella: TCheckBox;
    CBPlantaoCard: TCheckBox;
    CBMaxCard: TCheckBox;
    CBNatal: TCheckBox;
    CBAlimentacao: TCheckBox;
    CBRefeicao: TCheckBox;
    CBBemEstar: TCheckBox;
    CBCombustivel: TCheckBox;
    BtnPesquisar: TButton;
    DBGPesqEmp: TDBGrid;
    DSPesqEmp: TDataSource;
    QPesqEmp: TADOQuery;
    QPesqEmpEMPRES_ID: TIntegerField;
    QPesqEmpNOME: TStringField;
    QPesqEmpDATA_FECHA_EMP: TDateTimeField;
    QPesqEmpDATA_VENC_EMP: TDateTimeField;
    QPesqEmpTOTAL: TBCDField;
    Label5: TLabel;
    QOperador: TADOQuery;
    dsOperador: TDataSource;
    lkpOperador: TDBLookupComboBox;
    DSEmpId: TDataSource;
    QEmpId: TADOQuery;
    QEmpIdempres_id: TIntegerField;
    QEmpIdfantasia: TStringField;
    EEmpFantasia: TEdit;
    procedure BtnPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EEmpres_idExit(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmFechamentoEmpresa: TFrmFechamentoEmpresa;

implementation

  uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;
{$R *.dfm}







procedure TFrmFechamentoEmpresa.BtnPesquisarClick(Sender: TObject);
begin


  QPesqEmp.Close;
  QPesqEmp.SQL.Clear;
  QPesqEmp.SQL.Add('select empresas.EMPRES_ID, NOME, DATA_FECHA_EMP, DATA_VENC_EMP, sum(DEBITO - CREDITO) TOTAL from DIA_FECHA ');
  QPesqEmp.sql.Add('inner join empresas');
  QPesqEmp.sql.Add('on EMPRESAS.EMPRES_ID = DIA_FECHA.EMPRES_ID');
  QPesqEmp.sql.Add('inner join CONTACORRENTE');
  QPesqEmp.sql.Add('on CONTACORRENTE.EMPRES_ID = EMPRESAS.EMPRES_ID');
  QPesqEmp.sql.Add('where');

  if (EDataFechamento.Date = EncodeDate(1899,12,30)) then
  begin
    MsgInf('Selecione a data de pesquisa');
    abort;
  end
  else begin
    QPesqEmp.sql.Add('DATA_FECHA_EMP between ' + QuotedStr(EDataFechamento.Text) + ' and ' + QuotedStr(EDataFechamento.Text));
    QPesqEmp.sql.Add('and DATA_FECHA between '+ QuotedStr(EDataFechamento.Text) + ' and ' + QuotedStr(EDataFechamento.Text) + ' and LIBERADA = ''S'' and APAGADO = ''N''');
  end;


  if lkpOperador.KeyValue < 0 then
  begin
    MsgInf('Selecione o operador');
    abort;
  end
  else begin
    QPesqEmp.sql.Add('AND RESPONSAVEL_FECHAMENTO LIKE ' + QuotedStr(lkpOperador.KeyValue));
  end;

  QPesqEmp.sql.Add('group by NOME, DATA_FECHA_EMP, DATA_VENC_EMP, empresas.EMPRES_ID');
  QPesqEmp.sql.Add('order by NOME');
  QPesqEmp.Open;
end;

procedure TFrmFechamentoEmpresa.FormCreate(Sender: TObject);
begin
  QOperador.Close;
  QOperador.Open;
end;

procedure TFrmFechamentoEmpresa.EEmpres_idExit(Sender: TObject);
begin
  QEmpId.Close;
  QEmpId.Parameters.ParamByName('empres_id').Value := EEmpres_id.Text;
  QEmpId.Open;

  if  QEmpId.IsEmpty then
  begin
       MsgInf('Empresa n�o encontrada');
  end;

  EEmpFantasia.Text := QEmpIdfantasia.AsString;



end;

end.
