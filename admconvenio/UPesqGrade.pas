unit UPesqGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, DB;

type
  campo = Record
     index : integer;
     fieldname : string;
  end;

  TFPesqGrade = class(TForm)
    Panel1: TPanel;
    EdLoca: TEdit;
    CBCampos: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RGDirec: TRadioGroup;
    Button1: TBitBtn;
    RGTipo: TRadioGroup;
    Label3: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    parar : Boolean;
  public
    { Public declarations }
    Grade : TDBGrid;
    campos : array of campo;
  end;


implementation

{$R *.dfm}

procedure TFPesqGrade.FormKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then begin
   Perform(WM_NEXTDLGCTL,0,0);
   Key := #0;
end;
end;

procedure TFPesqGrade.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var mens : string;
begin
if key = vk_escape then BitBtn2.Click;
if key = vk_f1 then begin
   mens := 'Para efetuar uma pesquisa, selecione o campo a ser pesquisado '+#13#10+
           'e informe o valor a ser localizado, selecione uma das op��es de '+#13#10+
           'pesquisa: iniciando com (procura por valores nos campos que '+#13#10+
           'iniciem com o valor informado), contendo (procura por valores '+#13#10+
           'nos campos que contenham o valor informado), exatamente igual '+#13#10+
           '(procura por valores nos campos exatamente igual ao valor informado). '+#13#10+
           'Dire��o da pesquisa informa ao sistema se a pesquisa deve ser para '+#13#10+
           'baixo ou seja pesquisar os registros que est�o abaixo da posi��o do '+#13#10+
           'cursor na grade, ou para cima.'+#13#10+#13#10+
           'Observa��o: N�o � possivel efetuar pesquisas por mais de um campo '+#13#10+
           'para esta necessidade utilize a filtragem de dados do sistema.';
   Application.MessageBox(PChar(mens),PChar('Ajuda do sistema'),MB_OK+MB_ICONINFORMATION);
end;
end;

procedure TFPesqGrade.BitBtn2Click(Sender: TObject);
begin
Close;
end;

procedure TFPesqGrade.BitBtn1Click(Sender: TObject);
var valor_campo : string;
begin
  if Trim(Self.EdLoca.Text) <> '' then begin
     parar := False;
     BitBtn1.Visible := False;
     Button1.Visible := True;
     if RGDirec.ItemIndex = 0 then begin
        Grade.DataSource.DataSet.Next;
        while not Grade.DataSource.DataSet.Eof do begin
           Application.ProcessMessages; if parar then Break;
           if Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).DataType in [ftFloat, ftCurrency] then
              valor_campo := FormatFloat('###,###,##0.00',Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).AsFloat)
           else valor_campo := Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).AsString;
           if RGTipo.ItemIndex = 0 then begin
              if Pos(EdLoca.Text,valor_campo) = 1 then Break;
           end
           else if RGTipo.ItemIndex = 1 then begin
              if Pos(EdLoca.Text,valor_campo) > 0 then Break;
           end
           else begin
              if valor_campo = EdLoca.Text then Break;
           end;
           Grade.DataSource.DataSet.Next;
        end;
        if Grade.DataSource.DataSet.Eof then ShowMessage('Fim dos registros.');
     end
     else begin
        Grade.DataSource.DataSet.Prior;
        while not Grade.DataSource.DataSet.Bof do begin
           Application.ProcessMessages; if parar then Break;
           if Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).DataType in [ftFloat, ftCurrency] then
              valor_campo := FormatFloat('###,###,##0.00',Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).AsFloat)
           else valor_campo := Grade.DataSource.DataSet.FieldByName(campos[CBCampos.ItemIndex].fieldname).AsString;
           if RGTipo.ItemIndex = 0 then begin
              if Pos(EdLoca.Text,valor_campo) = 1 then Break;
           end
           else if RGTipo.ItemIndex = 1 then begin
              if Pos(EdLoca.Text,valor_campo) > 0 then Break;
           end
           else begin
              if valor_campo = EdLoca.Text then Break;
           end;
           Grade.DataSource.DataSet.Prior;
        end;
        if Grade.DataSource.DataSet.Bof then ShowMessage('In�cio dos registros.');
     end;
     BitBtn1.Visible := True;
     Button1.Visible := False;
     BitBtn1.SetFocus;
  end;
end;

procedure TFPesqGrade.Button1Click(Sender: TObject);
begin
parar := True;
BitBtn1.Visible := True;
Button1.Visible := False;
end;

end.
