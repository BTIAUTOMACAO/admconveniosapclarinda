unit UCadStatusAtendimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, JvExButtons, JvBitBtn, StdCtrls, Buttons, Mask,
  DBCtrls, Menus, ExtCtrls;

type
  TFrmCadStatusAtendimento = class(TForm)
    Panel1: TPanel;
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    Minimizar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    Panel2: TPanel;
    btn4: TSpeedButton;
    btn5: TSpeedButton;
    btn6: TSpeedButton;
    btn7: TSpeedButton;
    EDTDESCRICAO: TDBEdit;
    EDTCOD: TDBEdit;
    btnEditar: TBitBtn;
    btnIncluir: TBitBtn;
    btnApagar: TBitBtn;
    btnGravar: TJvBitBtn;
    btnCancelar: TJvBitBtn;
    QStatus: TADOQuery;
    QStatusSTATUS_ID: TIntegerField;
    QStatusDESCRICAO: TStringField;
    DSSTATUS: TDataSource;
    procedure QStatusAfterInsert(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGravarClick(Sender: TObject);
    procedure Valida();
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure QStatusBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadStatusAtendimento: TFrmCadStatusAtendimento;

implementation

uses DM, Math, cartao_util, UValidacao;

{$R *.dfm}

procedure TFrmCadStatusAtendimento.QStatusAfterInsert(DataSet: TDataSet);
begin
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_CRED');
  DMConexao.AdoQry.Open;
  QStatusSTATUS_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  EDTDESCRICAO.SetFocus;
end;

procedure TFrmCadStatusAtendimento.FormShow(Sender: TObject);
begin
  EDTCOD.SetFocus;
  QStatus.Open;
end;

procedure TFrmCadStatusAtendimento.btnIncluirClick(Sender: TObject);
begin
  if TBitBtn(Sender).Enabled = False then Abort;
    QStatus.Append;
end;

procedure TFrmCadStatusAtendimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QStatus.Close;
end;

procedure TFrmCadStatusAtendimento.btnGravarClick(Sender: TObject);
begin
  QStatus.Post;
end;

procedure TFrmCadStatusAtendimento.Valida();
begin
  if fnVerfCompVazioEmTabSheet('Informe a descri��o do status.',EDTDESCRICAO)then Abort;
end;

procedure TFrmCadStatusAtendimento.btnCancelarClick(Sender: TObject);
begin
  QStatus.Cancel;
end;

procedure TFrmCadStatusAtendimento.btnEditarClick(Sender: TObject);
begin
  QStatus.Edit;
end;

procedure TFrmCadStatusAtendimento.QStatusBeforePost(DataSet: TDataSet);
begin
  if QStatus.State = dsInsert then
      ShowMessage('Cadastro realizado com sucesso!')
    else
      ShowMessage('Altera��o realizada com sucesso!');

end;

end.
