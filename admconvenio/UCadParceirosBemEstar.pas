unit UCadParceirosBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,} uClassLog,
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls, Mask,
  {JvLookup,} JvToolEdit, {JvMemDS,} JvDateTimePicker, JvDBDateTimePicker, Menus, ToolEdit, dateutils,
  CurrEdit, {JvDBComb,} XMLDoc, IdHTTP, ADODB, ComObj, JvExControls,
  JvDBLookup, JvExStdCtrls, JvCombobox, JvDBCombobox, JvExMask,
  JvExDBGrids, JvDBGrid, JvValidateEdit, JvMaskEdit, JvCheckedMaskEdit,
  JvDatePickerEdit, JvDBDatePickerEdit, JvExButtons, JvBitBtn;

type
  TFCadParceirosBemEstar = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox3: TGroupBox;
    ButLimpaSenha: TBitBtn;
    TabConta: TTabSheet;
    DSBanco: TDataSource;
    DSConta: TDataSource;
    ButNovoCod: TBitBtn;
    EdCidade: TEdit;
    Label37: TLabel;
    PopupCC: TPopupMenu;
    MenuItem1: TMenuItem;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    Exportarparaoexcel2: TMenuItem;
    Label38: TLabel;
    EdFanta: TEdit;
    DSDatasFecha: TDataSource;
    DSSaldos: TDataSource;
    DSDesconto: TDataSource;
    PopupDesconto: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PopCodAcesso: TPopupMenu;
    ColocarCodAcessoManual1: TMenuItem;
    PopupAlteraID: TPopupMenu;
    AlterarIddoFornecedor1: TMenuItem;
    DSPagaFornPor: TDataSource;
    DSTaxas: TDataSource;
    DSTaxasProxPag: TDataSource;
    DSEmpresLib: TDataSource;
    PopupComissaoEmpr: TPopupMenu;
    AltLineardeComissoporEmpresa1: TMenuItem;
    DSNovoSaldo: TDataSource;
    PopupMenu1: TPopupMenu;
    este1: TMenuItem;
    DSFormasPgto: TDataSource;
    Label47: TLabel;
    DBPagaForPor: TJvDBLookupCombo;
    DBCheckBox1: TDBCheckBox;
    Label8: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    SpeedButton1: TSpeedButton;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit13: TDBEdit;
    Label17: TLabel;
    DBEdit14: TDBEdit;
    GroupBox9: TGroupBox;
    Label46: TLabel;
    Label50: TLabel;
    DBEdit10: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Panel22: TPanel;
    Panel4: TPanel;
    Label36: TLabel;
    DBNavigator3: TDBNavigator;
    //Data1: TDateEdit;
    //Data2: TDateEdit;
    IncluiB: TCheckBox;
    BitBtn1: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    btnImportar: TBitBtn;
    pb: TProgressBar;
    tExcel: TADOTable;
    Label34: TLabel;
    DBText1: TDBText;
    DSEstados: TDataSource;
    DuplicarEstabelecimentoalterandoID1: TMenuItem;
    DBCheckBox4: TDBCheckBox;
    DSPos: TDataSource;
    dbLkpEstados: TDBLookupComboBox;
    dbLkpCidades: TDBLookupComboBox;
    DSCidades: TDataSource;
    Data1: TJvDateEdit;
    Data2: TJvDateEdit;
    QTaxas: TADOQuery;
    QTaxastaxa_id: TIntegerField;
    QTaxasdescricao: TStringField;
    QTaxasvalor: TFloatField;
    QTaxasrel_taxa_id: TIntegerField;
    QTaxasdescontar: TStringField;
    QDatasFecha: TADOQuery;
    QDatasFechaCRED_ID: TIntegerField;
    QDatasFechaDATA_FECHA: TDateTimeField;
    QDatasFechaDATA_VENC: TDateTimeField;
    QPagaFornPor: TADOQuery;
    QPagaFornPorPAGA_CRED_POR_ID: TIntegerField;
    QPagaFornPorDESCRICAO: TStringField;
    QTaxasProxPag: TADOQuery;
    QTaxasProxPagTAXAS_PROX_PAG_ID: TIntegerField;
    QTaxasProxPagCRED_ID: TIntegerField;
    QTaxasProxPagDESCRICAO: TStringField;
    QTaxasProxPagVALOR: TFloatField;
    QTaxasProxPagFATURA_ID: TIntegerField;
    QTaxasProxPagAPAGADO: TStringField;
    QTaxasProxPagLIQUIDO: TStringField;
    QConta: TADOQuery;
    QContaautorizacao_id: TIntegerField;
    QContadigito: TWordField;
    QContadata: TDateTimeField;
    QContahora: TStringField;
    QContatitular: TStringField;
    QContadebito: TBCDField;
    QContacredito: TBCDField;
    QContanome: TStringField;
    QContaoperador: TStringField;
    QContahistorico: TStringField;
    QContabaixa_credenciado: TStringField;
    QContaemp_nome: TStringField;
    QContaemp_cod: TIntegerField;
    QBanco: TADOQuery;
    QBancoCODIGO: TIntegerField;
    QBancoBANCO: TStringField;
    QBancoAPAGADO: TStringField;
    QBancoLAYOUT: TMemoField;
    QBancoDTAPAGADO: TDateTimeField;
    QBancoDTALTERACAO: TDateTimeField;
    QBancoOPERADOR: TStringField;
    QBancoDTCADASTRO: TDateTimeField;
    QBancoOPERCADASTRO: TStringField;
    QNovoSaldo: TADOQuery;
    QCredenciados: TADOQuery;
    QCredenciadosCRED_ID: TIntegerField;
    QCredenciadosNOME: TStringField;
    QCredenciadosSEG_ID: TIntegerField;
    QCredenciadosLIBERADO: TStringField;
    QCredenciadosCODACESSO: TIntegerField;
    QCredenciadosCOMISSAO: TBCDField;
    QCredenciadosBANCO: TIntegerField;
    QCredenciadosDIAFECHAMENTO1: TWordField;
    QCredenciadosDIAFECHAMENTO2: TWordField;
    QCredenciadosVENCIMENTO1: TWordField;
    QCredenciadosVENCIMENTO2: TWordField;
    QCredenciadosAPAGADO: TStringField;
    QCredenciadosPAGA_CRED_POR_ID: TIntegerField;
    QCredenciadosCONTRATO: TIntegerField;
    QCredenciadosCGC: TStringField;
    QCredenciadosINSCRICAOEST: TStringField;
    QCredenciadosFANTASIA: TStringField;
    QCredenciadosTELEFONE1: TStringField;
    QCredenciadosTELEFONE2: TStringField;
    QCredenciadosFAX: TStringField;
    QCredenciadosCONTATO: TStringField;
    QCredenciadosENVIAR_EMAIL: TStringField;
    QCredenciadosEMAIL: TStringField;
    QCredenciadosHOMEPAGE: TStringField;
    QCredenciadosPOSSUICOMPUTADOR: TStringField;
    QCredenciadosENDERECO: TStringField;
    QCredenciadosNUMERO: TIntegerField;
    QCredenciadosBAIRRO: TStringField;
    QCredenciadosCIDADE: TStringField;
    QCredenciadosCEP: TStringField;
    QCredenciadosESTADO: TStringField;
    QCredenciadosMAQUINETA: TStringField;
    QCredenciadosTIPOFECHAMENTO: TStringField;
    QCredenciadosNOVOCODACESSO: TIntegerField;
    QCredenciadosSENHA: TStringField;
    QCredenciadosAGENCIA: TStringField;
    QCredenciadosCONTACORRENTE: TStringField;
    QCredenciadosOBS1: TStringField;
    QCredenciadosOBS2: TStringField;
    QCredenciadosCORRENTISTA: TStringField;
    QCredenciadosCARTIMPRESSO: TStringField;
    QCredenciadosFORNCESTA: TStringField;
    QCredenciadosCPF: TStringField;
    QCredenciadosACEITA_PARC: TStringField;
    QCredenciadosGERA_LANC_CPMF: TStringField;
    QCredenciadosENCONTRO_CONTAS: TStringField;
    QCredenciadosDTAPAGADO: TDateTimeField;
    QCredenciadosDTALTERACAO: TDateTimeField;
    QCredenciadosOPERADOR: TStringField;
    QCredenciadosDTCADASTRO: TDateTimeField;
    QCredenciadosOPERCADASTRO: TStringField;
    QCredenciadosVALE_DESCONTO: TStringField;
    QCredenciadosCOMPLEMENTO: TStringField;
    QCredenciadosDIA_PAGTO: TIntegerField;
    QCredenciadosTX_DVV: TBCDField;
    QCredenciadosUTILIZA_AUTORIZADOR: TStringField;
    QCredenciadosUTILIZA_COMANDA: TStringField;
    QCredenciadosUTILIZA_RECARGA: TStringField;
    QEstados: TADOQuery;
    QEstadosESTADO_ID: TIntegerField;
    QEstadosUF: TStringField;
    QEstadosICMS_ESTADUAL: TFloatField;
    QCidades: TADOQuery;
    QSaldoCred: TADOQuery;
    QDesconto: TADOQuery;
    QDescontoDESCONTO_ID: TIntegerField;
    QDescontoCRED_ID: TIntegerField;
    QDescontoDATA: TDateTimeField;
    QDescontoVALOR: TBCDField;
    QDescontoHISTORICO: TStringField;
    QDescontoOPERADOR: TStringField;
    QDescontoAPAGADO: TStringField;
    QPos: TADOQuery;
    QFormasPgto: TADOQuery;
    QCidadesCID_ID: TIntegerField;
    QCidadesESTADO_ID: TIntegerField;
    QCidadesNOME: TStringField;
    QEmpresLib: TADOQuery;
    QSaldoCredcred_id: TIntegerField;
    QSaldoCreddata_fecha_for: TDateTimeField;
    QSaldoCreddata_venc_for: TDateTimeField;
    QSaldoCredSALDO: TBCDField;
    QNovoSaldocred_id: TIntegerField;
    QNovoSaldodata_venc_for: TDateTimeField;
    QNovoSaldoaberto: TBCDField;
    QNovoSaldofaturado: TBCDField;
    QNovoSaldoa_pagar: TBCDField;
    QEmpresLibempres_id: TIntegerField;
    QEmpresLibnome: TStringField;
    QEmpresLibliberado: TStringField;
    QEmpresLibcomissao: TBCDField;
    QDatasFechaDESC_FECHAMENTO: TStringField;
    QDatasFechaDESC_VENCIMENTO: TStringField;
    QFormasPgtoforma_id: TIntegerField;
    QFormasPgtodescricao: TStringField;
    QFormasPgtoliberado: TStringField;
    Label49: TLabel;
    EdCNPJ: TEdit;
    QGrupo: TADOQuery;
    DSGrupo: TDataSource;
    QPosPOS_SERIAL_NUMBER: TStringField;
    QPosCRED_ID: TIntegerField;
    QPosUSUARIO_RECARGA: TStringField;
    QPosTIPO_RECARGA: TStringField;
    QPosATU_SERVER_IP: TStringField;
    QPosMODELO_POS: TStringField;
    QPosCAPTURA: TStringField;
    DBCheckBox3: TDBCheckBox;
    QTaxasProxPagTIPO_RECEITA: TStringField;
    TabAtendimento: TTabSheet;
    pgc1: TPageControl;
    ts2: TTabSheet;
    pnl4: TPanel;
    lbl11: TLabel;
    txtProtocolo: TEdit;
    grp5: TGroupBox;
    lbl12: TLabel;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    btnBuscar: TBitBtn;
    pnl5: TPanel;
    grdCredOcorrencias: TJvDBGrid;
    ts3: TTabSheet;
    lbl9: TLabel;
    QOcorrencias: TADOQuery;
    DSOcorrencias: TDataSource;
    QOcorrenciasatendimento_id: TIntegerField;
    QOcorrenciasnome_solicitante: TStringField;
    QOcorrenciastel_solictante: TStringField;
    QOcorrenciasmotivo: TStringField;
    QOcorrenciasdesc_atendimento: TStringField;
    QOcorrenciasdata_atendimento: TDateTimeField;
    QOcorrenciascred_id: TIntegerField;
    QOcorrenciasoperador: TStringField;
    QGrupoGRUPO_ESTAB_ID: TIntegerField;
    QGrupoDESCRICAO: TStringField;
    QGrupoAPAGADO: TStringField;
    QGrupoDTAPAGADO: TDateTimeField;
    QGrupoDTALTERACAO: TDateTimeField;
    QGrupoOPERADOR: TStringField;
    QGrupoDTCADASTRO: TDateTimeField;
    QGrupoOPERCADASTRO: TStringField;
    TabHistoricoCred: TTabSheet;
    Panel26: TPanel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    dtIniHistorico: TJvDateEdit;
    dtFimHistorico: TJvDateEdit;
    ComboBox1: TComboBox;
    BitBtn3: TBitBtn;
    JvDBGrid2: TJvDBGrid;
    DSLogOcorrencias: TDataSource;
    QLogOcorrencias: TADOQuery;
    QLogOcorrenciaslog_id: TIntegerField;
    QLogOcorrenciasjanela: TStringField;
    QLogOcorrenciascampo: TStringField;
    QLogOcorrenciasvalor_ant: TStringField;
    QLogOcorrenciasvalor_pos: TStringField;
    QLogOcorrenciasoperador: TStringField;
    QLogOcorrenciasoperacao: TStringField;
    QLogOcorrenciasdata_hora: TDateTimeField;
    QLogOcorrenciascadastro: TStringField;
    QLogOcorrenciasid: TIntegerField;
    QLogOcorrenciasdetalhe: TStringField;
    QLogOcorrenciasmotivo: TStringField;
    QLogOcorrenciassolicitante: TStringField;
    QLogOcorrenciasprotocolo: TIntegerField;
    pnl1: TPanel;
    Panel27: TPanel;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    txtNomeEmpres: TDBEdit;
    txtCod: TDBEdit;
    grp2: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    grp3: TGroupBox;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    txtNome: TDBEdit;
    txtTelefone: TDBEdit;
    txtMotivo: TDBEdit;
    pnl3: TPanel;
    btn4: TSpeedButton;
    btn5: TSpeedButton;
    btn6: TSpeedButton;
    btn7: TSpeedButton;
    btn3: TBitBtn;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btGravar: TJvBitBtn;
    btCancelar: TJvBitBtn;
    grp4: TGroupBox;
    MemoDesc_atendimento: TDBMemo;
    GroupBox2: TGroupBox;
    Label67: TLabel;
    Label52: TLabel;
    QStatusAtend: TADOQuery;
    DSStatusAtend: TDataSource;
    QStatusAtendstatus_id: TIntegerField;
    QStatusAtenddescricao: TStringField;
    QOcorrenciasSTATUS_ID: TIntegerField;
    JvDBLookupCombo1: TJvDBLookupCombo;
    txtDataAlteracaoCred: TEdit;
    txtOperadorAlteracaoCred: TEdit;
    PanelPendente: TPanel;
    Label53: TLabel;
    Label57: TLabel;
    PanelFinalizado: TPanel;
    GroupBox4: TGroupBox;
    QPosVERSAO_NAVS: TStringField;
    QPosVERSAO_OS: TStringField;
    QPosVERSAO_EOS: TStringField;
    QPosATUALIZOU_LUA: TStringField;
    GroupBox5: TGroupBox;
    Label19: TLabel;
    Label35: TLabel;
    Label58: TLabel;
    DBEdit22: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit23: TDBEdit;
    Label26: TLabel;
    GroupBox7: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    JvDBLookupCombo2: TJvDBLookupCombo;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit19: TDBEdit;
    DBEdit9: TDBEdit;
    Label6: TLabel;
    GroupBox6: TGroupBox;
    Label25: TLabel;
    Label12: TLabel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    QCRED_ESPEC_BEM_ESTAR: TADOQuery;
    DSCred_Espec_Bem_Estar: TDataSource;
    QCRED_ESPEC_BEM_ESTARCRED_BE_ID: TIntegerField;
    QCRED_ESPEC_BEM_ESTARLIBERADO: TStringField;
    QCRED_ESPEC_BEM_ESTARESPECIALIDADE_ID: TIntegerField;
    QCRED_ESPEC_BEM_ESTARESPECIALIDADE: TStringField;
    BitBtn4: TBitBtn;
    dbedtFONE2: TDBEdit;
    cbbLiberado: TComboBox;
    lbl8: TLabel;
    DBSeg: TJvDBLookupCombo;
    Label18: TLabel;
    DSSeg: TDataSource;
    QSeg: TADOQuery;
    QSegSEG_ID: TIntegerField;
    QSegDESCRICAO: TStringField;
    QSegAPAGADO: TStringField;
    QSegDTALTERACAO: TDateTimeField;
    QSegOPERADOR: TStringField;
    QSegDTAPAGADO: TDateTimeField;
    QSegOPERCADASTRO: TStringField;
    QSegDTCADASTRO: TDateTimeField;
    QCadastroCRED_BE_ID: TIntegerField;
    QCadastroNOME: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroCODACESSO: TIntegerField;
    QCadastroCOMISSAO: TBCDField;
    QCadastroPAGA_CRED_POR_ID: TIntegerField;
    QCadastroCONTRATO: TIntegerField;
    QCadastroCNPJ: TStringField;
    QCadastroCPF: TStringField;
    QCadastroINSCRICAO_ESTADUAL: TStringField;
    QCadastroFONE1: TStringField;
    QCadastroFONE2: TStringField;
    QCadastroCONTATO: TStringField;
    QCadastroENVIAR_EMAIL: TStringField;
    QCadastroENVIAR_SMS: TStringField;
    QCadastroEMAIL: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TStringField;
    QCadastroBAIRRO: TStringField;
    QCadastroCID_ID: TIntegerField;
    QCadastroCEP: TIntegerField;
    QCadastroSENHA: TStringField;
    QCadastroBANCO: TIntegerField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTA: TStringField;
    QCadastroCORRENTISTA: TStringField;
    QCadastroOBS: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroUTILIZA_AUTORIZADOR: TStringField;
    QCadastroEXIBIR_BUSCA_SITE: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroESTADO: TStringField;
    QCadastroSEG_ID: TIntegerField;
    Label20: TLabel;
    dbLkpBairros: TDBLookupComboBox;
    btnAdicionaBairro: TBitBtn;
    lblBairro: TLabel;
    QBairros: TADOQuery;
    QBairrosBAIRRO_ID: TAutoIncField;
    QBairrosCID_ID: TIntegerField;
    QBairrosDESCRICAO: TStringField;
    DSBairros: TDataSource;
    procedure QCadastroAfterOpen(DataSet: TDataSet);
    procedure QCadastroAfterClose(DataSet: TDataSet);
    procedure ButIncluiClick(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButNovoCodClick(Sender: TObject);
    procedure ButLimpaSenhaClick(Sender: TObject);
    procedure Valida;
    procedure ButGravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButApagaClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Exportarparaoexcel2Click(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure TabContaShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure TabFichaExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAnoChange(Sender: TObject);
    procedure QDatasFechaBeforePost(DataSet: TDataSet);
    procedure TabSaldoHide(Sender: TObject);
    procedure grdSaldoOldTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QDatasFechaCalcFields(DataSet: TDataSet);
    procedure QDatasFechaBeforeDelete(DataSet: TDataSet);
    procedure QDatasFechaAfterPost(DataSet: TDataSet);
    procedure tbEstabilidadeHide(Sender: TObject);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    procedure DSCadastroStateChange(Sender: TObject);
    procedure ApagaDescClick(Sender: TObject);
    procedure ColocarCodAcessoManual1Click(Sender: TObject);
    procedure PopupAlteraIDPopup(Sender: TObject);
    procedure AlterarIddoFornecedor1Click(Sender: TObject);
    procedure PopCodAcessoPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GridTaxasTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure GridTaxasProxTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QEmpresLibAfterPost(DataSet: TDataSet);
    procedure GridEmpLibTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure TabEmpLibShow(Sender: TObject);
    procedure TabEmpLibHide(Sender: TObject);
    procedure AltLineardeComissoporEmpresa1Click(Sender: TObject);
    procedure GriddatasColExit(Sender: TObject);
    procedure GridTaxasColExit(Sender: TObject);
    procedure GridEmpLibColExit(Sender: TObject);
    procedure QDatasFechaBeforeEdit(DataSet: TDataSet);
    procedure QTaxasBeforeEdit(DataSet: TDataSet);
    procedure QEmpresLibBeforeEdit(DataSet: TDataSet);
    procedure QTaxasBeforeInsert(DataSet: TDataSet);
    procedure QDatasFechaBeforeInsert(DataSet: TDataSet);
    procedure QEmpresLibBeforeInsert(DataSet: TDataSet);
    procedure QTaxasProxPagBeforeInsert(DataSet: TDataSet);
    procedure QTaxasProxPagBeforeEdit(DataSet: TDataSet);
    procedure TabTaxaHide(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure tabFormasPgtoHide(Sender: TObject);
    procedure grdFormasPgtoColExit(Sender: TObject);
    procedure qFormasPgtoBeforeEdit(DataSet: TDataSet);
    procedure qFormasPgtoBeforeInsert(DataSet: TDataSet);
    procedure qFormasPgtoAfterPost(DataSet: TDataSet);
    procedure qFormasPgtoBeforePost(DataSet: TDataSet);
    procedure btnAtlDataFechaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnCancelEmpLibClick(Sender: TObject);
    procedure btnCancFixoClick(Sender: TObject);
    procedure btnGravaDescClick(Sender: TObject);
    procedure btnCancDescClick(Sender: TObject);
    procedure btnApagaDescClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure ButCloseClick(Sender: TObject);
    procedure DuplicarEstabelecimentoalterandoID1Click(Sender: TObject);
    procedure DBPagaForPorChange(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure dbGridPosColExit(Sender: TObject);
    procedure btnGravarPosClick(Sender: TObject);
    procedure btnCancelarPosClick(Sender: TObject);
    procedure tbCadPosHide(Sender: TObject);
    procedure tbCadPosShow(Sender: TObject);
    procedure qPosBeforePost(DataSet: TDataSet);
    procedure dbGridPosTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure btnExcluirPosClick(Sender: TObject);
    procedure qEstadosAfterScroll(DataSet: TDataSet);
    procedure DSEstadosDataChange(Sender: TObject; Field: TField);
    procedure grdSaldoNewTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure btnCancelFormasClick(Sender: TObject);
    procedure btnFirstBClick(Sender: TObject);
    procedure btnFirstCClick(Sender: TObject);
    procedure btnHistClick(Sender: TObject);
    procedure btnLastBClick(Sender: TObject);
    procedure btnLastCClick(Sender: TObject);
    procedure btnNextBClick(Sender: TObject);
    procedure btnNextCClick(Sender: TObject);
    procedure btnPriorBClick(Sender: TObject);
    procedure btnPriorCClick(Sender: TObject);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
    procedure DSOcorrenciasStateChange(Sender: TObject);
    procedure grdCredOcorrenciasDblClick(Sender: TObject);
    procedure grdCredOcorrenciasDrawColumnCell(Sender: TObject; const Rect: TRect;
        DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QTaxasBeforePost(DataSet: TDataSet);
    procedure QEmpresLibBeforePost(DataSet: TDataSet);
    procedure QOcorrenciasAfterInsert(DataSet: TDataSet);
    procedure QOcorrenciasBeforePost(DataSet: TDataSet);
    procedure QTaxasProxPagAfterInsert(DataSet: TDataSet);
    procedure ts2Enter(Sender: TObject);
    procedure ts2Show(Sender: TObject);
    procedure ts3Enter(Sender: TObject);
    procedure ts3Show(Sender: TObject);
    procedure QPosAfterInsert(DataSet: TDataSet);
    procedure btnInserirPosClick(Sender: TObject);
    procedure QCRED_ESPEC_BEM_ESTARBeforeOpen(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure QCRED_ESPEC_BEM_ESTARAfterInsert(DataSet: TDataSet);
    procedure QCRED_ESPEC_BEM_ESTARBeforePost(DataSet: TDataSet);
    procedure BitBtn4Click(Sender: TObject);
    procedure btnAdicionaBairroClick(Sender: TObject);
    procedure DSCidadesDataChange(Sender: TObject; Field: TField);
  private
    LogDataFecha: TLog;
    datas_alteradas : Boolean;
    procedure AbreDatas;
    procedure AtualizaDataCred;
    procedure TrocarCred_ID(new_CredId: string ; duplica : Boolean = False);
    function ExisteEmprBloq(cred_id, empres_id: integer): boolean;
    procedure GravaComissaoEmpres(cred_id, empres_id: integer;
      valor: currency);
    function ExisteComissEmpr(cred_id, empres_id: integer): boolean;
    procedure AddCabecalhoErro(var Arquivo : TStringList);
    procedure carregaICMS_ESTADUAL(estado : string = '');
    { Private declarations }
  public
    procedure BuscarOcorrencia;
    function geraID: Integer;
    procedure habilitarBotoes;
    procedure limparCampos;
    procedure BuscaUltimasAlteracaoesCred;
  end;

var
  FCadParceirosBemEstar: TFCadParceirosBemEstar;
  cLiberado : string;
  tDescontar, fLiberado : string;
  cComissao : currency;
  SavePlace : TBookmark;
  SavePlace1 : TBookmark;
implementation

uses DM, Math, StrUtils, UMenu, cartao_util,
  UCons_Autor, UAltLinCombo, UDBGridHelper, UValidacao, UaltContatoCred,
  UBuscaEspecialidadeBemEstar, UBuscaEspecialidadeBemEstar1,
  UCadStatusAtendimento, FCadBairro;

{$R *.dfm}
procedure TFCadParceirosBemEstar.AddCabecalhoErro(var Arquivo : TStringList);
begin
if Pos('---'+DateTimeToStr(DMConexao.fnData)+'---',Arquivo.Text) <= 0 then
  //Adiciono na parte de cima o cabe�alho e logo em seguida o Arquivo
  Arquivo.Text := '----------------'+#13+'---'+DateTimeToStr(Now)+'---'+#13+'----------------' + #13+ Arquivo.Text;
end;

procedure TFCadParceirosBemEstar.carregaICMS_ESTADUAL(estado : string);
begin
{  if not QCadastro.IsEmpty then begin
    qEstados.Close;
    if Trim(estado) <> '' then
      qEstados.Params[0].AsString := estado
    else
      qEstados.Params[0].AsString := QCadastroESTADO.AsString;
    qEstados.Open;
  end;}      
end;

procedure TFCadParceirosBemEstar.Valida;
begin
  //Nome
  if Trim(QCadastroNOME.AsString) = '' then
  begin
    MsgInf('Digite o Nome!');
    PageControl2.ActivePageIndex := 0;
    DBEdit2.SetFocus;
    Abort;
  end;
  //Nome fantasia
  if Trim(QCadastroFANTASIA.AsString) = '' then
    QCadastroFANTASIA.AsString:= copy(QCadastroNOME.AsString,1,45);
  //Forma de Pagamento
  if Trim(QCadastroPAGA_CRED_POR_ID.AsString) = '0' then
  begin
    MsgInf('Digite a Forma de Pagto desse Estabelecimento!');
    PageControl2.ActivePageIndex := 0;
    DBPagaForPor.SetFocus;
    Abort;
  end;
  //Comiss�o
  if Trim(QCadastroCOMISSAO.AsString) = '' then
  begin
    MsgInf('Digite a Comiss�o!');
    PageControl2.ActivePageIndex := 0;
    DBEdit14.SetFocus;
    Abort;
  end;
  //Codigo de Acesso
  if Trim(QCadastroCODACESSO.AsString) = '' then
    QCadastroCODACESSO.AsString := IntToStr(100+QCadastroCRED_BE_ID.AsInteger)+FormatFloat('00', DigitoControle(100+QCadastroCRED_BE_ID.AsInteger));
  //Liberado
  if Trim(QCadastroLIBERADO.AsString) = '' then
    QCadastroLIBERADO.AsString := 'N'
  else if ((QCadastroLIBERADO.AsString <> 'S') and (QCadastroLIBERADO.AsString <> 'N')) then
    QCadastroLIBERADO.AsString := 'N';
  //Contrato
  if Trim(QCadastroCONTRATO.AsString) = '' then
    QCadastroCONTRATO.AsString := QCadastroCRED_BE_ID.AsString;

  if Trim(QCadastroCONTATO.AsString) = '' then
  begin
    MsgInf('Digite o Contato!');
    PageControl2.ActivePageIndex := 0;
    DBEdit5.SetFocus;
    Abort;
  end;

  if Trim(QCadastroBANCO.AsString) = '' then
  begin
    MsgInf('Selecione o Banco!');
    PageControl2.ActivePageIndex := 0;
    JvDBLookupCombo2.SetFocus;
    Abort;
  end;

  if Trim(QCadastroAGENCIA.AsString) = '' then
  begin
    MsgInf('Dgite o numero da Ag�ncia!');
    PageControl2.ActivePageIndex := 0;
    DBEdit26.SetFocus;
    Abort;
  end;

  if Trim(QCadastroCONTA.AsString) = '' then
  begin
    MsgInf('Digite o numero da conta!');
   PageControl2.ActivePageIndex := 0;
    DBEdit27.SetFocus;
    Abort;
  end;

   if Trim(QCadastroCORRENTISTA.AsString) = '' then
  begin
    MsgInf('Digite o correntista!');
    PageControl2.ActivePageIndex := 0;
    DBEdit28.SetFocus;
    Abort;
  end;
  //Fechamento
//  if Trim(QCadastroDIAFECHAMENTO1.AsString) = '' then
//  begin
//    MsgInf('Digite o Dia de Fechamento!');
//    PageControl2.ActivePageIndex := 0;
//    DBDiaFecha.SetFocus;
//    Abort;
//  end;
  //Vencimento
//  If Trim(QCadastroVENCIMENTO1.AsString) = '' then
//  begin
//    MsgInf('Digite o Dia de Vencimento!');
//    PageControl2.ActivePageIndex := 0;
//    DBDiaVenc.SetFocus;
//    Abort;
//  end;
//  If Trim(QCadastroESTADO.AsString) = '' then
//  begin
//    MsgInf('Selecione o estado do estabelecimento!');
//    PageControl2.ActivePageIndex := 0;
//    //cbbUF.SetFocus;
//    dbLkpEstados.SetFocus;
//    Abort;
//  end;
  //Email
{  if Trim(dbEdtEmail.Text) <> '' then
    if fnIsEmail(dbEdtEmail.Text) then
      begin
      MsgErro('Email inv�lido!');
      dbEdtEmail.SetFocus;
      Abort;
      end;}
//  QCadastroDIAFECHAMENTO2.AsInteger := 0;
//  QCadastroVENCIMENTO2.AsInteger    := 0;
//  if Trim(QCadastroTIPOFECHAMENTO.AsString) = '' then QCadastroTIPOFECHAMENTO.AsString := 'T';
  //Banco
  if QCadastroBANCO.IsNull then QCadastroBANCO.AsInteger := 0;
  //Data Altera��o
//  QCadastroDTALTERACAO.AsDateTime := Date;
  //Senha e Data do Cadastro
  if QCadastro.State = dsInsert then
  begin
    QCadastroDTCADASTRO.AsDateTime := Date;
  end;
end;

procedure TFCadParceirosBemEstar.QCadastroAfterOpen(DataSet: TDataSet);
begin
  inherited;
  QSeg.Open;
  QPagaFornPor.Open;
  QBanco.Open;
  QGrupo.Open;
end;

procedure TFCadParceirosBemEstar.QCadastroAfterClose(DataSet: TDataSet);
begin
  inherited;
  QSeg.Close;
  QPagaFornPor.Close;
  QBanco.Close;
  //QGrupo.Close
end;

procedure TFCadParceirosBemEstar.ButIncluiClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TFCadParceirosBemEstar.ButBuscaClick(Sender: TObject);
var isTipoCredCantina,checkTipoCredito : Boolean;
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select pbe.* from parceiros_bem_estar pbe where 1=1 ');
  isTipoCredCantina := False;
  checkTipoCredito := False;
  if cbbLiberado.ItemIndex = 0 then
    QCadastro.SQL.Add('and liberado = ''S''')
    else
      QCadastro.SQL.Add('and liberado = ''N''');
  if Trim(EdCod.Text) <> '' then begin
    QCadastro.Sql.Add(' and pbe.cred_be_id in ('+EdCod.Text+')');
    checkTipoCredito := True;
  end;
  if Trim(EdFanta.Text) <> '' then
    QCadastro.Sql.Add(' and pbe.fantasia like ''%'+EdFanta.Text+'%''');
  if Trim(EdNome.Text) <> '' then
    QCadastro.Sql.Add(' and pbe.nome like ''%'+EdNome.Text+'%''');
  if Trim(EdCNPJ.Text) <> '' then begin
     QCadastro.SQL.Add(' and pbe.cnpj like''%'+EdCNPJ.Text+'%''');
     checkTipoCredito := True;
  end;
//  if Trim(EdCidade.Text) <> '' then
//    QCadastro.Sql.Add(' and cbe.cidade like ''%'+EdCidade.Text+'%''');
//  if DBSegmento.KeyValue > 0 then begin
//    QCadastro.Sql.Add(' and cbe. = '+ DBSegmento.KeyValue + ' ');
//    checkTipoCredito := True;
//  end;

//  if checkTipoCredito = False then
//     //182 � o valor fixo para testar se o segmento_id � igual cantina ou n�o
//     QCadastro.SQL.Add(' and cbe.seg_id not in(182)');

  QCadastro.Sql.Add(' order by pbe.nome ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then
  begin

    QCidades.Parameters.ParamByName('ESTADO_ID').Value := QCadastroESTADO.Value;
    QBairros.Parameters.ParamByName('CID_ID').Value := QCadastroCID_ID.Value;
    QCidades.Requery();
    QBairros.Requery();

    dbLkpCidades.KeyField := 'cid_id';

    dbLkpBairros.KeyValue := QCadastroBAIRRO.Value;
    dbLkpBairros.KeyField := 'bairro_id';

    Self.TextStatus := 'Estabelecimento: ['+QCadastroCRED_BE_ID.AsString+'] - '+QCadastroNOME.AsString;
    DBGrid1.SetFocus;
  end
  else
    EdCod.SetFocus;
  EdCod.Clear;
  EdFanta.Clear;
  EdNome.Clear;
  EdCidade.Clear;


end;

procedure TFCadParceirosBemEstar.ButNovoCodClick(Sender: TObject);
var codAcesso : String;
begin
  if not QCadastro.IsEmpty then
    if Application.MessageBox(Pchar('Aten��o ser� gerado um novo c�digo de acesso para o estabelecimento.'+slineBreak+'Confirma esta opera��o?'),'Aten��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDYes then begin
      codAcesso := QCadastroCODACESSO.AsString;
      QCadastro.Edit;
    repeat
      Randomize;
      QCadastroCODACESSO.AsString := IntToStr(QCadastroCRED_BE_ID.AsInteger)+ IntToStr(Random(10));
      DMConexao.AdoQry.SQL.Text := 'select Cred_id from credenciados where coalesce(apagado,''N'') <> ''S'' and codacesso = '+QCadastroCODACESSO.AsString;
      DMConexao.AdoQry.Open;
    until DMConexao.AdoQry.IsEmpty;
    DMConexao.AdoQry.Close;
    QCadastro.Post;
    DMConexao.ExecuteSql('UPDATE TRANSACOES SET CODACESSO = ' + QCadastroCODACESSO.AsString + ' WHERE CODACESSO = ' + codAcesso);
  end;
end;

procedure TFCadParceirosBemEstar.ButLimpaSenhaClick(Sender: TObject);
begin
  QCadastro.Edit;
  QCadastro.FieldByName('SENHA').AsString :=  Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.Post;
end;

procedure TFCadParceirosBemEstar.ButGravaClick(Sender: TObject);
begin
  Valida;
  inherited;
end;

procedure TFCadParceirosBemEstar.FormCreate(Sender: TObject);
begin
  chavepri := 'CRED_BE_ID';
  detalhe  := 'Cred ID: ';
  inherited;
  data1.Date := date;
  data2.Date := date;
  //LogDataFecha:= TLog.Create;
  //LogDataFecha.LogarQuery(QDatasFecha,janela,'Estabelecimento: ','Datas de Fechamento',Operador.Nome,'cred_id');

  DMConexao.Config.Open;
//  if DMConexao.ConfigUSA_NOVO_FECHAMENTO.AsString = 'S' then
//  begin
//    grdSaldoOld.Align:= alCustom;
//    grdSaldoOld.Visible:= False;
//    grdSaldoNew.Visible:= True;
//    grdSaldoNew.Align:= alClient;
//  end
//  else
//  begin
//    grdSaldoNew.Align:= alCustom;
//    grdSaldoNew.Visible:= False;
//    grdSaldoOld.Visible:= True;
//    grdSaldoOld.Align:= alClient;
//  end;
  DMConexao.Config.Close;
  FMenu.vCadParceirosBemEstar := True;
  QOcorrencias.Open;
  qEstados.Open;
  qCidades.Open;
  QBairros.Open;
  QCadastro.Open;


end;

procedure TFCadParceirosBemEstar.QCadastroAfterScroll(DataSet: TDataSet);
var bm : TBookmark;
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Estabelecimento: ['+QCadastroCRED_BE_ID.AsString+'] - '+QCadastroNOME.AsString;
  //if QCadastroCIDADE.AsString <> '' then
  //  dbLkpCidades.KeyValue := LowerCase(QCadastroCIDADE.AsString);
  //carregaICMS_ESTADUAL;
end;

procedure TFCadParceirosBemEstar.ButApagaClick(Sender: TObject);
var cred_id : string;
begin
  if not QCadastro.IsEmpty then
  begin
    cred_id := QCadastroCRED_BE_ID.AsString;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Text := 'Select count(autorizacao_id) as num from contacorrente where baixa_credenciado <> ''S'' and cred_id = '+QCadastroCRED_BE_ID.AsString;
    DMConexao.AdoQry.Open;
    If DMConexao.AdoQry.FieldByName('num').AsInteger > 0 then
    begin
      Application.MessageBox('Esse estabelecimento possui movimenta��o de conta corrente.'+#13+'N�o � possivel excluir este estabelecimento.','Aten��o',MB_OK+MB_DEFBUTTON1+MB_ICONINFORMATION);
    end
    else
      inherited;
      DMConexao.AdoQry.Close;

      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('UPDATE CREDENCIADOS SET DTAPAGADO = CURRENT_TIMESTAMP WHERE CRED_ID = '+cred_id+'');
      DMConexao.AdoQry.ExecSQL;
      DMConexao.AdoQry.Close;
      QCadastro.Requery;
  end;
end;

procedure TFCadParceirosBemEstar.JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  try
    if Pos(Field.FieldName,QConta.Sort) > 0 then
    begin
      if Pos(' DESC',QConta.Sort) > 0 then
        QConta.Sort := Field.FieldName
      else
        QConta.Sort := Field.FieldName+' DESC';
    end
    else
      QConta.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadParceirosBemEstar.Exportarparaoexcel2Click(Sender: TObject);
begin
Grade_to_PlanilhaExcel(JvDBGrid1);
end;

procedure TFCadParceirosBemEstar.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPARCEIROS_BEM_ESTAR AS CRED_ID' );
  DMConexao.AdoQry.Open;
  QCadastroCRED_BE_ID.AsInteger         := DMConexao.AdoQry.FieldByName('CRED_ID').AsInteger;
  QCadastroPAGA_CRED_POR_ID.AsInteger   := 0;
  QCadastroCODACESSO.AsString           := IntToStr(QCadastroCRED_BE_ID.AsInteger)+FormatFloat('0', DigitoControle(QCadastroCRED_BE_ID.AsInteger));
  QCadastroLIBERADO.AsString            := 'S';
  QCadastroUTILIZA_AUTORIZADOR.AsString := 'N';
  QCadastroSENHA.AsString               := Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.FieldByName('DTCADASTRO').AsString := DateTimeToStr(Now);
  QCadastro.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
  QCadastro.Tag := 1;//para informar que est� inserindo.
end;

procedure TFCadParceirosBemEstar.TabContaShow(Sender: TObject);
begin
  QConta.Close;
  data1.SetFocus;
end;

procedure TFCadParceirosBemEstar.TabFichaShow(Sender: TObject);
var s : String;
begin
  inherited;
  DBEdit2.SetFocus;
//  if not QCadastro.IsEmpty then begin
//    QCadastro.Edit;
//    s := QCadastroCIDADE.AsString;
//    QCadastroCIDADE.AsString := '';
//    QCadastroCIDADE.AsString := s;
//    QCadastro.Cancel;
//  end;
end;

procedure TFCadParceirosBemEstar.TabFichaExit(Sender: TObject);
begin
  inherited;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFCadParceirosBemEstar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_escape) and (UpperCase(btnImportar.Caption) = '&CANCELAR') then begin
    MsgInf('Cancele a importa��o antes de sair da tela');
    Abort;
  end else begin
    inherited;
    if Key = vk_F12 then if not QCadastro.IsEmpty then TabConta.Show;
  end;
end;

procedure TFCadParceirosBemEstar.AbreDatas;
begin
  QDatasFecha.Close;
  if not QCadastro.IsEmpty then
  begin
    //QDatasFecha.SQL.Text := ' Select * from dia_fecha_cred where cred_id = '+QCadastroCRED_BE_ID.AsString+' and year(data_fecha) = '+CBAno.Text;
    QDatasFecha.Open;
  end;
end;


procedure TFCadParceirosBemEstar.CBAnoChange(Sender: TObject);
begin
  inherited;
  AbreDatas;
end;


procedure TFCadParceirosBemEstar.AtualizaDataCred;
var ano, mes, dia : word;
begin
//  Screen.Cursor := crHourGlass;
//  QDatasFecha.DisableControls;
//  QDatasFecha.Close;
//  QDatasFecha.SQL.Clear;
//  QDatasFecha.SQL.Add(' Select * from dia_fecha_cred where cred_id = '+QCadastroCRED_BE_ID.AsString);
//  if Chdatasapartir.Checked then
//    QDatasFecha.SQL.Add(' and data_fecha > '+formatdataIB(Date));
//  QDatasFecha.Open;
//  if QDatasFecha.IsEmpty then
//  begin
//    ShowMessage('N�o existem datas para serem alteradas, as datas ser�o criadas.');
//    CriarDatas;
//    Exit;
//  end;
//  QDatasFecha.First;
//  ProgressDatas.Position := 0;
//  ProgressDatas.Max := QDatasFecha.RecordCount;
//  while not QDatasFecha.Eof do
//  begin
//    QDatasFecha.Edit;
//    DecodeDate(QDatasFechaDATA_FECHA.AsDateTime,ano,mes,dia);
//    //Se o dia for maior que o ultimo dia do mes assume o �ltimo dia.
//    if QCadastroDIAFECHAMENTO1.AsInteger <= DaysInAMonth(ano,mes) then
//      dia := QCadastroDIAFECHAMENTO1.AsInteger
//    else
//      dia := DaysInAMonth(ano,mes);
//    QDatasFechaDATA_FECHA.AsDateTime := EncodeDate(ano,mes,dia);
//    if QCadastroVENCIMENTO1.AsInteger <= DaysInAMonth(ano,mes) then
//      dia := QCadastroVENCIMENTO1.AsInteger
//    else
//      dia := DaysInAMonth(ano,mes);
//    QDatasFechaDATA_VENC.AsDateTime :=  EncodeDate(ano,mes,dia);
//    if not ChVencnomes.Checked then
//      QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,StrToInt(EdMesesVenc.Text));
//    QDatasFecha.Post;
//    QDatasFecha.Next;
//    ProgressDatas.Position := ProgressDatas.Position+1;
//  end;
//  QDatasFecha.EnableControls;
//  Screen.Cursor := crDefault;
end;


procedure TFCadParceirosBemEstar.QDatasFechaBeforePost(DataSet: TDataSet);
begin
  if QDatasFechaDATA_FECHA.IsNull then begin
     ShowMessage('Data de fechamento obrigat�ria.');
     Sysutils.Abort;
  end;
  if QDatasFechaDATA_VENC.IsNull then begin
     ShowMessage('Data de vencimento obrigat�ria.');
     Sysutils.Abort;
  end;
  if QDatasFechaDATA_VENC.AsDateTime = QDatasFechaDATA_FECHA.AsDateTime then begin
     ShowMessage('Data de vencimento tem que ser maior ou igual a data de fechamento.');
     Sysutils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.TabSaldoHide(Sender: TObject);
begin
  inherited;
  if QSaldoCred.Active then QSaldoCred.Close;
  if QNovoSaldo.Active then QNovoSaldo.Close;
end;

procedure TFCadParceirosBemEstar.grdSaldoOldTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
  try
    if Pos(Field.FieldName, QSaldoCred.Sort) > 0 then begin
       if Pos(' DESC', QSaldoCred.Sort) > 0 then QSaldoCred.Sort := Field.FieldName
                                                   else  QSaldoCred.Sort := Field.FieldName+' DESC';
    end
    else QSaldoCred.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadParceirosBemEstar.QDatasFechaCalcFields(DataSet: TDataSet);
begin
  inherited;
  QDatasFechaDESC_FECHAMENTO.AsString := FormatDateTime('dddd"," dd "de" mmmm "de" yyyy',QDatasFechaDATA_FECHA.AsDateTime);
  QDatasFechaDESC_VENCIMENTO.AsString := FormatDateTime('dddd"," dd "de" mmmm "de" yyyy',QDatasFechaDATA_VENC.AsDateTime);
end;

procedure TFCadParceirosBemEstar.QDatasFechaBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  ShowMessage('N�o � poss�vel excluir.');
  SysUtils.Abort;
end;

procedure TFCadParceirosBemEstar.QDatasFechaAfterPost(DataSet: TDataSet);
begin
  inherited;
  datas_alteradas := True;
end;

procedure TFCadParceirosBemEstar.tbEstabilidadeHide(Sender: TObject);
begin
  inherited;
{
  if datas_alteradas then begin
     Application.MessageBox(PChar('Alguma(s) data(s) foi(ram) alterada(s).'+#13+'A atualiza��o de datas ser� efetuada....'),'Aten��o..',MB_ICONINFORMATION+MB_OK);
     try
        Screen.Cursor := crHourGlass;
        DM1.Query1.Close; //update somente para que o trigger de datas da contacorrente busque as novas datas.
        DM1.Query1.SQL.Text := 'Update contacorrente set autorizacao_id = autorizacao_id where cred_id = '+QCadastroCRED_BE_ID.AsString;
        DM1.Query1.ExecSQL;
        datas_alteradas := False;
        Screen.Cursor := crDefault;
        Application.MessageBox('Datas atualizadas com sucesso....','Confirma��o..',MB_ICONINFORMATION+MB_OK);
     except on E:Exception do
        ShowMessage('Erro ao atualizar as datas. erro: '+E.Message);
     end;
  end;
}
end;

procedure TFCadParceirosBemEstar.QCadastroAfterPost(DataSet: TDataSet);
begin
  inherited;
  if QCadastro.Tag = 1 then begin
     try
        QCadastro.Tag := 0;
     except on E:Exception do
        ShowMessage('Erro ao criar as datas do estabelecimento.');
     end;
  end;

end;

procedure TFCadParceirosBemEstar.DSCadastroStateChange(Sender: TObject);
begin
  inherited;
//  DBDiaFecha.ReadOnly := not(QCadastro.State = dsInsert);
//  DBDiaVenc.ReadOnly  := not(QCadastro.State = dsInsert);

end;

procedure TFCadParceirosBemEstar.ApagaDescClick(Sender: TObject);
begin
  inherited;
if Application.MessageBox('Confirma a exclus�o deste registro?','Aten��o',MB_YESNO+MB_DEFBUTTON2+MB_ICONQUESTION) = IDYes then begin
   QDesconto.Edit;
   QDesconto.FieldByName('APAGADO').AsString := 'S';
   QDesconto.Post;
   QDesconto.Refresh;
end;

end;

procedure TFCadParceirosBemEstar.ColocarCodAcessoManual1Click(Sender: TObject);
var ncod : string;
begin
  inherited;
  if MsgSimNao('Deseja alterar o c�digo de acesso?') then begin
     ncod := SoNumero( InputBox('Informe o novo c�d. acesso','Nov c�d. acesso','') );
     if ncod <> '' then begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.Sql.Text := 'Select nome from credenciados where codacesso = '+ncod;
        DMConexao.AdoQry.Open;
        if not DMConexao.AdoQry.IsEmpty then
           MsgErro('Ja exite um estabelecimento com este c�digo de acesso, nome: '+DMConexao.AdoQry.Fields[0].AsString)
        else begin
           QCadastro.Edit;
           QCadastroCODACESSO.AsInteger := StrToInt(ncod);
           QCadastro.Post;
        end;
        DMConexao.AdoQry.Close;
     end;
  end
end;

procedure TFCadParceirosBemEstar.PopupAlteraIDPopup(Sender: TObject);
begin
  inherited;
  PopupAlteraID.Items[0].Enabled := not QCadastro.IsEmpty;
end;



procedure TFCadParceirosBemEstar.AlterarIddoFornecedor1Click(Sender: TObject);
var fornew : string;
begin
  inherited;
if MsgSimNao('Deseja alterar o c�digo do estabelecimento?') then
   fornew:= SoNumero( InputBox('Digite o novo Id para o Estabelecimento','Novo ID','') );
   if Trim(fornew) <> '' then begin
      TrocarCred_ID(fornew);
   end;
end;

procedure TFCadParceirosBemEstar.TrocarCred_ID(new_CredId:String ; duplica : Boolean);
var existe : boolean; nome:string;
old_credID : string;
begin
   if not duplica then begin
     DMConexao.AdoQry.Close;
     DMConexao.AdoQry.Sql.Text := 'Select nome from credenciados where cred_id = '+new_CredId;
     DMConexao.AdoQry.Open;
     existe := not DMConexao.AdoQry.IsEmpty;
     nome := DMConexao.AdoQry.Fields[0].AsString;
     DMConexao.AdoQry.Close;
     if existe then begin
        MsgErro('Ja existe um estabelecimento cadastrado com este Id, Nome: '+nome);
     end
   end else begin
      Screen.Cursor := crHourGlass;
      DMConexao.AdoCon.BeginTrans;
      try
         old_credID := QCadastroCRED_BE_ID.AsString;
         if not duplica then begin
           QCadastro.Edit;
           QCadastroCRED_BE_ID.AsString := new_CredId;
           QCadastro.Post;
         end;
         with DMConexao do begin
            //Atualiza todas tabelas que usam empres_id.
            AdoQry.SQL.Text := 'update back_alt_linear set chave_prim = '+new_CredId+' where chave_prim = '+old_credID+' and tabela = ''CREDENCIADOS'' ';
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update cad_conferencia set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update conferencia set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_adm = '+new_CredId+' where cod_cred_adm = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_baixa = '+new_CredId+' where cod_cred_baixa = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_cpmf = '+new_CredId+' where cod_cred_cpmf = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update config_webfornecedores set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update contacorrente set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update desconto set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update dia_fecha_cred set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update logs set id = '+new_CredId+' where id = '+old_credID+' and cadastro = ''Cadastro de Estabelecimentos'' ';
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update eventos set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update fidelidade set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update lancamentos set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update autor_transacoes set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update transacoes set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update pagamento_cred set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
         end;
         DMConexao.AdoCon.CommitTrans;
         Screen.Cursor := crDefault;
         MsgInf('Altera��o Conclu�da com sucesso!');
      except
         on e:Exception do begin
            QCadastro.Edit;
            QCadastroCRED_BE_ID.AsString := old_credID;
            QCadastro.Post;
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgErro('Problemas na altera��o do ID, erro: '+e.Message);
         end;
      end;
   end;
Screen.Cursor := crDefault;
end;
procedure TFCadParceirosBemEstar.PopCodAcessoPopup(Sender: TObject);
begin
  inherited;
  PopCodAcesso.Items[0].Enabled := not QCadastro.IsEmpty;
end;

procedure TFCadParceirosBemEstar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if UpperCase(btnImportar.Caption) = '&CANCELAR' then begin
    MsgInf('Cancele a importa��o antes de sair da tela');
    Abort;
  end else begin
    inherited;
    FMenu.vCadParceirosBemEstar := False;
  end;
end;

procedure TFCadParceirosBemEstar.GridTaxasTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
    try
    if Pos(Field.FieldName,QTaxas.Sort) > 0 then begin
       if Pos(' DESC',QTaxas.Sort) > 0 then QTaxas.Sort := Field.FieldName
                                                  else QTaxas.Sort := Field.FieldName+' DESC';
    end
    else QTaxas.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCadParceirosBemEstar.GridTaxasProxTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
    try
    if Pos(Field.FieldName,QTaxasProxPag.Sort) > 0 then begin
       if Pos(' DESC',QTaxasProxPag.Sort) > 0 then QTaxasProxPag.Sort := Field.FieldName
                                                  else QTaxasProxPag.Sort := Field.FieldName+' DESC';
    end
    else QCadastro.Sort := Field.FieldName;
    except
    end;
end;

function TFCadParceirosBemEstar.ExisteEmprBloq(cred_id: integer; empres_id: integer):boolean;
begin
  if DMConexao.ExecuteScalar(' select empres_id from cred_emp_Lib where empres_id = '+
    IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id),0) = 0 then
    Result:= False
  else
    Result:= True;
end;

function TFCadParceirosBemEstar.ExisteComissEmpr(cred_id: integer; empres_id: integer):boolean;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(' select empres_id from cred_emp_comissao where empres_id = '+IntToStr(empres_id)+' ');
  DMConexao.AdoQry.SQL.Add(' and cred_id = '+IntToStr(cred_id)+'');
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.Fields[0].AsString = '' then
     Result:= false
  else
    Result:= true;

  //if DMConexao.ExecuteScalar(' select empres_id from cred_emp_comissao where empres_id = '+
  //  IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id)) = 0 then
 //   Result:= true
  //else
   // Result:= false;
end;

procedure TFCadParceirosBemEstar.QEmpresLibAfterPost(DataSet: TDataSet);
var sql, sqlLog: String;
  cadastro, detalhe: string;
  reg_atual : TBookmark;
  comissaoOldVal : currency;
begin
  inherited;
  if cLiberado <> '' then
    if (QEmpresLibLIBERADO.OldValue <> UpperCase({QEmpresLibLIBERADO.AsString}cLiberado)) then
    begin
      sql:= '';
      sqlLog:= '';
      cadastro:= 'Empr. '+QEmpresLibEMPRES_ID.AsString+' lib. para este estabelecimento';
      detalhe := 'Cred ID';
      if (UpperCase({QEmpresLibLIBERADO.AsString}cLiberado) = 'N') then
      begin
        // da o insert na tabela cred_emp_lib
        sql :=  ' insert into CRED_EMP_LIB(CRED_ID, EMPRES_ID, LIBERADO) '+
              ' values('+QCadastroCRED_BE_ID.AsString+','+QEmpresLibEMPRES_ID.AsString+','''+UpperCase({QEmpresLibLIBERADO.AsString}cLiberado)+''')';
        DMConexao.GravaLog('FCadCred','Empr. Bloq.','S','N',Operador.Nome,'Altera��o',QCadastroCRED_BE_ID.AsString,Self.Name);
      end
      else
      begin
        //  da o delete
        sql :=  'delete from CRED_EMP_LIB ';
        sql :=  sql + ' where CRED_ID = '+QCadastroCRED_BE_ID.AsString+ ' and EMPRES_ID = '+QEmpresLibEMPRES_ID.AsString;
        DMConexao.GravaLog('FCadCred','Empr. Bloq.','N','S',Operador.Nome,'Altera��o',QCadastroCRED_BE_ID.AsString,Self.Name);
      end;
      DMConexao.ExecuteSql(sql);
    end;

  // Altera��o de comiss�o
  if VarIsNull(QEmpresLibCOMISSAO.OldValue) then
          comissaoOldVal := 0
       else
          comissaoOldVal := QEmpresLibCOMISSAO.OldValue;

  if (comissaoOldVal <> {QEmpresLibCOMISSAO.AsCurrency}cComissao) then
  begin
    sql:= '';
    sqlLog:= '';
    cadastro:= 'Comissao  da Empr. '+QEmpresLibEMPRES_ID.AsString+' para este estabelecimento';
    detalhe := 'Cred ID';
    if ({QEmpresLibCOMISSAO.AsCurrency}cComissao <> QCadastroCOMISSAO.AsCurrency) then
    begin
      // da o insert ou update
      if ExisteComissEmpr(QCadastroCRED_BE_ID.AsInteger,QEmpresLibEMPRES_ID.AsInteger) then
      begin
        sql :=  'update CRED_EMP_COMISSAO set COMISSAO = '+FormatDimIB({QEmpresLibCOMISSAO.AsCurrency}cComissao)+
          ' where CRED_ID = '+QCadastroCRED_BE_ID.AsString+' and EMPRES_ID = '+QEmpresLibEMPRES_ID.AsString;
      end
      else
      begin
        sql :=  'insert into CRED_EMP_COMISSAO(CRED_ID, EMPRES_ID, COMISSAO) '+
          ' values('+QCadastroCRED_BE_ID.AsString+','+QEmpresLibEMPRES_ID.AsString+', '+FormatDimIB({QEmpresLibCOMISSAO.AsCurrency}cComissao)+')';
      end;
      DMConexao.GravaLog('FCadCred','Comissao por Emp.',FormatDinBR({QEmpresLibCOMISSAO.OldValue}comissaoOldVal),FormatDinBR({QEmpresLibCOMISSAO.AsCurrency}cComissao),Operador.Nome,'Altera��o',QCadastroCRED_BE_ID.AsString,Self.Name);
    end
    else
    begin
      //  da o delete
      sql :=  'delete from CRED_EMP_COMISSAO ';
      sql :=  sql + ' where CRED_ID = '+QCadastroCRED_BE_ID.AsString+ ' and EMPRES_ID = '+QEmpresLibEMPRES_ID.AsString;
      DMConexao.GravaLog('FCadCred','Comissao por Emp.',FormatDinBR({QEmpresLibCOMISSAO.OldValue}comissaoOldVal),FormatDinBR({QEmpresLibCOMISSAO.AsCurrency}cComissao),Operador.Nome,'Altera��o',QCadastroCRED_BE_ID.AsString,Self.Name);
    end;
    DMConexao.ExecuteSql(sql);
  end;
  QEmpresLib.DisableControls;
  reg_atual := QEmpresLib.GetBookmark;
  QEmpresLib.Requery;
  QEmpresLib.GotoBookmark(reg_atual);
  QEmpresLib.freebookmark(reg_atual);
  QEmpresLib.EnableControls;
  cLiberado := '';
  cComissao := 0;
end;

procedure TFCadParceirosBemEstar.GridEmpLibTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
    try
    if Pos(Field.FieldName,QEmpresLib.Sort) > 0 then begin
       if Pos(' DESC',QEmpresLib.Sort) > 0 then QEmpresLib.Sort := Field.FieldName
                                                  else QEmpresLib.Sort := Field.FieldName+' DESC';
    end
    else QEmpresLib.Sort := Field.FieldName;
    except
    end;
end;


procedure TFCadParceirosBemEstar.TabEmpLibShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QEmpresLib.Close;
    QEmpresLib.Parameters.ParamByName('cred').Value := QCadastroCRED_BE_ID.AsInteger;
    QEmpresLib.Parameters.ParamByName('cred_id').Value := QCadastroCRED_BE_ID.AsInteger;
    QEmpresLib.Parameters.ParamByName('credId').Value := QCadastroCRED_BE_ID.AsInteger;
    QEmpresLib.Open;
  end;
  cLiberado := '';
end;

procedure TFCadParceirosBemEstar.TabEmpLibHide(Sender: TObject);
begin
  inherited;
  QEmpresLib.Close;
end;

procedure TFCadParceirosBemEstar.AltLineardeComissoporEmpresa1Click(Sender: TObject);
var empres_id: Integer;
  valor: Currency;
begin
  inherited;
  FAltLinCombo := TFAltLinCombo.Create(self);
  FAltLinCombo.lbCombo.Caption := 'Selecione uma empresa';
  FAltLinCombo.lbValor.Caption := 'Valor da Comiss�o';
  FAltLinCombo.Caption := 'Alt. Linear de Comiss�o por Empresa';
  FAltLinCombo.Q.Close;
  FAltLinCombo.Q.SQL.Clear;
  FAltLinCombo.Q.SQL.Add(' select empres_id, nome from empresas where coalesce(apagado,"N") <> "S" order by nome ');
  FAltLinCombo.Q.Open;
  FAltLinCombo.Combo.LookupDisplay := 'NOME';
  FAltLinCombo.Combo.LookupField := 'EMPRES_ID';
  FAltLinCombo.ShowModal;
  if FAltLinCombo.ModalResult = mrOk then
  begin
    QCadastro.First;
    empres_id := FAltLinCombo.Combo.KeyValue;
    valor := FAltLinCombo.edValorReal.Value;
    while not QCadastro.Eof do
    begin
      GravaComissaoEmpres(QCadastroCRED_BE_ID.AsInteger,empres_id,valor);
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  QCadastro.First;
  FAltLinCombo.Free;
end;

procedure TFCadParceirosBemEstar.GravaComissaoEmpres(cred_id: Integer; empres_id: Integer; valor:Currency);
var sql: String;
  vlrantigo: Currency;
begin
  vlrantigo:= 0;
  if ExisteComissEmpr(cred_id,empres_id) then
  begin
    vlrantigo:= DMConexao.ExecuteScalar('select COMISSAO from CRED_EMP_LIB where empres_id = '+IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id),0);
    sql :=  'update CRED_EMP_COMISSAO set COMISSAO = '+FormatDimIB(valor)+
    ' where CRED_ID = '+IntToStr(cred_id)+' and EMPRES_ID = '+IntToStr(empres_id);
  end
  else
  begin
    sql :=  'insert into CRED_EMP_COMISSAO(CRED_ID, EMPRES_ID, COMISSAO) '+
    ' values('+IntToStr(cred_id)+','+IntToStr(empres_id)+', '+FormatDimIB(valor)+')';
  end;
  DMConexao.ExecuteSql(sql);
  DMConexao.GravaLog('FCadCred','Comissao por Emp.',FormatDinBR(vlrantigo),FormatDinBR(valor),
    Operador.Nome,'Altera��o','AL-Comiss�o por empresa('+IntToStr(empres_id)+')',
    QCadastroCRED_BE_ID.AsString, Self.Name);
end;

procedure TFCadParceirosBemEstar.GriddatasColExit(Sender: TObject);
begin
  inherited;
  if QDatasFecha.State in [dsEdit] then QDatasFecha.Post;
end;

procedure TFCadParceirosBemEstar.GridTaxasColExit(Sender: TObject);
begin
  inherited;
  if QTaxas.State in [dsEdit] then QTaxas.Post;
end;

procedure TFCadParceirosBemEstar.GridEmpLibColExit(Sender: TObject);
begin
  inherited;
  if QEmpresLib.State in [dsEdit] then QEmpresLib.Post;
end;

procedure TFCadParceirosBemEstar.QDatasFechaBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QTaxasBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QEmpresLibBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QTaxasBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QDatasFechaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QEmpresLibBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.QTaxasProxPagBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if fnVerfCampoVazio('Voc� precisa seleicionar um estabelecimento antes de incluir um desconto!', QCadastroCRED_BE_ID) then abort;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;

end;

procedure TFCadParceirosBemEstar.QTaxasProxPagBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.TabTaxaHide(Sender: TObject);
begin
  inherited;
  QTaxas.Close;
  QTaxasProxPag.Close;
end;

procedure TFCadParceirosBemEstar.este1Click(Sender: TObject);
var empres_id: Integer;
  valor: Currency;
begin
  inherited;
  FAltLinCombo := TFAltLinCombo.Create(self);
  FAltLinCombo.lbCombo.Caption := 'Selecione uma empresa';
  FAltLinCombo.lbValor.Caption := 'Valor da Comiss�o';
  FAltLinCombo.Caption := 'Alt. Linear de Comiss�o por Empresa';
  FAltLinCombo.Q.Close;
  FAltLinCombo.Q.SQL.Clear;
  FAltLinCombo.Q.SQL.Add(' select empres_id, nome from empresas where coalesce(apagado,''N'') <> ''S'' order by nome ');
  FAltLinCombo.Q.Open;
  FAltLinCombo.Combo.LookupDisplay := 'NOME';
  FAltLinCombo.Combo.LookupField := 'EMPRES_ID';
  FAltLinCombo.ShowModal;
  if FAltLinCombo.ModalResult = mrOk then
  begin
    QCadastro.First;
    empres_id := FAltLinCombo.Combo.KeyValue;
    valor := FAltLinCombo.edValorReal.Value;
    while not QCadastro.Eof do
    begin
      GravaComissaoEmpres(QCadastroCRED_BE_ID.AsInteger,empres_id,valor);
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  QCadastro.First;
  FAltLinCombo.Free;
end;

procedure TFCadParceirosBemEstar.tabFormasPgtoHide(Sender: TObject);
begin
  inherited;
  if qFormasPgto.Active then qFormasPgto.Close;
end;

procedure TFCadParceirosBemEstar.grdFormasPgtoColExit(Sender: TObject);
begin
  inherited;
  if qFormasPgto.State in [dsEdit] then qFormasPgto.Post;
end;

procedure TFCadParceirosBemEstar.qFormasPgtoBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.qFormasPgtoBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadParceirosBemEstar.qFormasPgtoAfterPost(DataSet: TDataSet);
begin
  inherited;
  if qFormasPgto.FieldByName('LIBERADO').OldValue <> UpperCase({qFormasPgto.FieldByName('LIBERADO').AsString}fLiberado) then
  begin
    if UpperCase({qFormasPgto.FieldByName('LIBERADO').AsString}fLiberado) = 'S' then
    begin
      DMConexao.ExecuteSql(' insert into formas_cred_lib (cred_id, forma_id) values ('+QCadastro.FieldByName('CRED_ID').AsString+', '+qFormasPgto.FieldByName('FORMA_ID').AsString+') ');
      DMConexao.GravaLog('FCadCred','FormasPgto','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = N','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = S',Operador.Nome,'Altera��o',QCadastro.FieldByName('CRED_ID').AsString,Self.Name);
    end
    else
    begin
      DMConexao.ExecuteSql(' delete from formas_cred_lib where cred_id = '+QCadastro.FieldByName('CRED_ID').AsString+' and forma_id = '+qFormasPgto.FieldByName('FORMA_ID').AsString);
      DMConexao.GravaLog('FCadCred','FormasPgto','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = S','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = N',Operador.Nome,'Altera��o',QCadastro.FieldByName('CRED_ID').AsString,Self.Name);
    end;
    qFormasPgto.Requery;
    qFormasPgto.GotoBookmark(SavePlace1);
    qFormasPgto.FreeBookmark(SavePlace1);
    fLiberado := '';
  end;
end;

procedure TFCadParceirosBemEstar.qFormasPgtoBeforePost(DataSet: TDataSet);
begin
  inherited;
  if ((qFormasPgto.FieldByName('DESCRICAO').AsString = ' A VISTA') and (UpperCase(qFormasPgto.FieldByName('LIBERADO').AsString) = 'N')) then
  begin
    MsgInf('A forma de pagamento [A Vista] n�o pode ser bloqueada pelo sistema');
    qFormasPgto.Cancel;
    Exit;
  end;
  SavePlace1 := qFormasPgto.GetBookmark;
  fLiberado := '';
  qFormasPgto.FieldByName('LIBERADO').AsString:= UpperCase(qFormasPgto.FieldByName('LIBERADO').AsString);
  fLiberado := qFormasPgto.FieldByName('LIBERADO').AsString;
end;

procedure TFCadParceirosBemEstar.btnAtlDataFechaClick(Sender: TObject);
var mesvenc : integer;
begin
//  if ((ChVencnomes.Checked) and (QCadastroDIAFECHAMENTO1.AsInteger > QCadastroVENCIMENTO1.AsInteger)) then
//  begin
//    ShowMessage('A data de vencimento n�o pode ser menor que a data de fechamento.');
//    Exit;
//  end;
//  if not ChVencnomes.Checked then
//    if TryStrToInt(EdMesesVenc.Text,mesvenc) then
//    begin
//      if mesvenc <= 0 then
//      begin
//        ShowMessage('Informe o n�mero de meses a mais do vencimento para seu fechamento.');
//        Exit;
//      end;
//    end
//    else
//    begin
//      ShowMessage('Informe o n�mero de meses a mais do vencimento para seu fechamento.');
//      Exit;
//    end;
//  if QCadastro.State = dsEdit then
//    QCadastro.Post;
//  AtualizaDataCred;
//  ShowMessage('Datas atualizadas com sucesso.');
//  AbreDatas;
end;

procedure TFCadParceirosBemEstar.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if Not QCadastro.IsEmpty then
  begin
    Screen.Cursor := crHourGlass;
    QConta.Close;
    QConta.Sql.Clear;
    QConta.Sql.Add(' Select contacorrente.autorizacao_id, contacorrente.digito, contacorrente.data, contacorrente.hora, conveniados_bem_estar.titular, ');
    QConta.Sql.Add(' contacorrente.debito, contacorrente.credito, cartoes.nome, contacorrente.operador, contacorrente.historico, ');
    QConta.Sql.Add(' contacorrente.baixa_credenciado, empresas.nome emp_nome, empresas.empres_id emp_cod ');
    QConta.Sql.Add(' from contacorrente join conveniados_bem_estar on (conveniados_bem_estar.conv_id = contacorrente.conv_id) ');
    QConta.Sql.Add(' join empresas on (empresas.empres_id = conveniados_bem_estar.empres_id) join cartoes on (cartoes.cartao_id = contacorrente.cartao_id) ');
    QConta.Sql.Add(' where contacorrente.cred_id = '+QCadastroCRED_BE_ID.AsString);
    QConta.Sql.Add(' and contacorrente.data between '''+FormatDateTime('dd/mm/yyyy',Data1.Date)+''' and '''+FormatDateTime('dd/mm/yyyy',Data2.Date)+'''');
    if not IncluiB.Checked then
      QConta.Sql.Add(' and coalesce(BAIXA_CREDENCIADO,''N'') <> ''S''');
    QConta.Open;
    if not QConta.IsEmpty then
      JvDBGrid1.SetFocus;
    Screen.Cursor := crDefault;
  end
  else
    QConta.Close;
end;

procedure TFCadParceirosBemEstar.btCancelarClick(Sender: TObject);
begin
  inherited;
  if QOcorrencias.State in [dsInsert,dsEdit] then
    QOcorrencias.Cancel;
end;

procedure TFCadParceirosBemEstar.btGravarClick(Sender: TObject);
begin
   if QOcorrencias.State in [dsInsert] then begin
      try
        QOcorrenciasdata_atendimento.AsDateTime := Now;
        QOcorrenciasatendimento_id.AsInteger    := StrToInt(lbl4.Caption);
        QOcorrenciascred_id.AsInteger           := QCadastroCRED_BE_ID.AsInteger;
        QOcorrenciasoperador.AsString           := Operador.Nome;

        if QOcorrencias.State in [dsInsert] then begin
            if JvDBLookupCombo1.KeyValue = Null then
            begin
                MsgInf('� necess�rio selecionar um Status para o atendimento.');
                JvDBLookupCombo1.SetFocus;
                Exit;
            end;
          //  colocouMensagem := DMConexao.GravaLogOcorrencia(TabAtendimento.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QOcorrencias.FieldByName('atendimento_id').AsString,Operador.Nome,'Inclus�o',Self.Caption,iif(Trim(QCadastro.FieldByName('cred_id').AsString) = '','NULL',Qcadastro.FieldByName('cred_id').AsString),'', '');
            MsgInf('Ocorr�ncia registrada com sucesso '+#13+'O n�mero do protocolo gerado �: '+QOcorrenciasatendimento_id.AsString+'')
        end
        else begin
      //      colocouMensagem := DMConexao.GravaLogOcorrencia(TabAtendimento.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QOcorrencias.FieldByName('atendimento_id').AsString,Operador.Nome,'Altera��o',Self.Caption,iif(Trim(QCadastro.FieldByName('cred_id').AsString) = '','NULL',Qcadastro.FieldByName('cred_id').AsString),'', '');
            MsgInf('Os dados foram alterados com sucesso');
        end;
        QOcorrencias.Post;
      finally
      end;
   end
   else begin
    // colocouMensagem := DMConexao.GravaLogOcorrencia(TabAtendimento.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QOcorrencias.FieldByName('atendimento_id').AsString,Operador.Nome,'Altera��o',Self.Caption,iif(Trim(QCadastro.FieldByName('cred_id').AsString) = '','NULL',Qcadastro.FieldByName('cred_id').AsString),StringReplace(detalhe,': ','',[rfReplaceAll]),'', '');
     MsgInf('Os dados foram alterados com sucesso');

     QOcorrencias.Post;
   end;


end;

procedure TFCadParceirosBemEstar.btn2Click(Sender: TObject);
begin
  inherited;
  limparCampos;
  txtNome.SetFocus;
  QOcorrencias.Append;
  lbl4.Caption                      := IntToStr(geraID);
  FAltContatoCred                   := TFAltContatoCred.Create(self);
  FAltContatoCred.txtNome1.Text     := QCadastroCONTATO.AsString;
  FAltContatoCred.txtTelefone1.Text := QCadastroFONE1.AsString;
  FAltContatoCred.txtTel2.Text      := QCadastroFONE2.AsString;
end;



procedure TFCadParceirosBemEstar.limparCampos;
Var
i : Integer;
begin
  for i := 0 to ComponentCount -1 do
  begin
    if Components[i] is TEdit then
    begin
      TEdit(Components[i]).Text := '';
    end;

    if Components[i] is TMemo then
    begin
      TMemo(Components[i]).Clear;
    end;
  end;
end;

function TFCadParceirosBemEstar.geraID: Integer;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_CRED');
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].AsInteger;
end;

procedure TFCadParceirosBemEstar.SpeedButton1Click(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: TXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  if (DBEdit9.Text = '') or (Length(SoNumero(DBEdit9.Text)) <> 8) then
  begin
    Application.MessageBox('CEP nulo ou inv�lido.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
    exit;
  end;
  Resposta   := TStringStream.Create('');
  TSConsulta := TStringList.Create;
  IdHTTP1:= TIdHTTP.Create(Self);
  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
  TSConsulta.Values['&cep']  := SoNumero(DBEdit9.Text);
  TSConsulta.Values['&formato']  := 'xml';
  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
  TSConsulta.Free;
  IdHTTP1.Free;
  XMLDocCEP:= TXMLDocument.Create(self);
  XMLDocCEP.Active := True;
  XMLDocCEP.Encoding := 'iso-8859-1';
  XMLDocCEP.LoadFromStream(Resposta);
  try
    try
      QCadastro.Edit;
      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
                                                    ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
      //QCadastro.FieldByName('BAIRRO').AsString      := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
      //QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
      //QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
    except
      ShowMessage('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
    end;
  finally
    Resposta.Free;
    XMLDocCEP.Active := False;
    XMLDocCEP.Free;
  end;
  DBEdit15.SetFocus;
end;

procedure TFCadParceirosBemEstar.btnCancelEmpLibClick(Sender: TObject);
begin
  inherited;
  if QEmpresLib.State in [dsInsert,dsEdit] then QEmpresLib.Cancel;
end;

procedure TFCadParceirosBemEstar.btnCancFixoClick(Sender: TObject);
begin
  inherited;
  if QTaxas.State in [dsInsert,dsEdit] then QTaxas.Cancel;
end;

procedure TFCadParceirosBemEstar.btnGravaDescClick(Sender: TObject);
begin
  inherited;
  if QTaxasProxPag.State in [dsInsert,dsEdit] then QTaxasProxPag.Post;
end;

procedure TFCadParceirosBemEstar.btnCancDescClick(Sender: TObject);
begin
  inherited;
  if QTaxasProxPag.State in [dsInsert,dsEdit] then QTaxasProxPag.Cancel;
end;

procedure TFCadParceirosBemEstar.btnApagaDescClick(Sender: TObject);
begin
  inherited;
  if (not QTaxasProxPag.IsEmpty) and MsgSimNao('Confirma a exclus�o do desconto: '+sLineBreak+QTaxasProxPagDESCRICAO.AsString+'?') then
    DMConexao.GravaLog('FCadCred','Taxa Prox. Mes',QTaxasProxPagVALOR.AsString,'',Operador.Nome,'Excluir',QCadastroCRED_BE_ID.AsString,Self.Name);
     DMConexao.AdoQry.SQL.Clear;
     DMConexao.AdoQry.SQL.Add('DELETE FROM TAXAS_PROX_PAG WHERE TAXAS_PROX_PAG_ID = '+QTaxasProxPagTAXAS_PROX_PAG_ID.AsString+'');
     DMConexao.AdoQry.ExecSQL;
     QTaxasProxPag.Requery;
end;

procedure TFCadParceirosBemEstar.btnBuscarClick(Sender: TObject);
begin
  if QCadastro.IsEmpty then begin
      MsgInf('� necess�rio selecionar uma empresa para efetuar a busca.');
      PageControl1.TabIndex := 0;
      EdCod.SetFocus;
      Exit;
  end;

  if (dtInicial.Date = 0) and (txtProtocolo.Text = '') then begin
      MsgInf('Voc� deve informar a data ou o n�mero do protocolo para efetuar uma busca.');
      dtInicial.SetFocus;
      Exit;
  end
  else if ((dtInicial.Date <> 0) and (dtFinal.Date = 0)) then begin
      MsgInf('Digite a data final para realizar a busca.');
      dtFinal.SetFocus;
      Exit;
  end;

  BuscarOcorrencia();

  if QOcorrencias.IsEmpty then
  begin
      MsgInf('N�o h� ocorr�ncias para os crit�rios de busca aplicados.');
      dtInicial.SetFocus;
  end
  else begin
    QStatusAtend.Open;
  end;
end;

procedure TFCadParceirosBemEstar.BuscarOcorrencia;
begin
  QOcorrencias.Close;
  QOcorrencias.SQL.Clear;
  //QUERY EST�TICA
  QOcorrencias.SQL.Add('SELECT * FROM credenciados_atendimento WHERE cred_id = '+QCadastroCRED_BE_ID.AsString+'');
  //INICIO DA QUERY IN�MICA
  if dtInicial.Date > 0 then begin
    QOcorrencias.SQL.Add(' and CONVERT(varchar(11),data_atendimento,103) BETWEEN '+QuotedStr(dtInicial.Text)+' and '+QuotedStr(dtFinal.Text)+'');
  end;
  if txtProtocolo.Text <> '' then begin
    QOcorrencias.SQL.Add(' and atendimento_id = '+txtProtocolo.Text);
  end;
  QOcorrencias.Open;
end;

procedure TFCadParceirosBemEstar.TabSheet4Show(Sender: TObject);
begin
  inherited;
//    if QCadastro.FieldByName('DIAFECHAMENTO1').AsInteger >= QCadastro.FieldByName('VENCIMENTO1').AsInteger then
//    ChVencnomes.Checked:= False;
end;

procedure TFCadParceirosBemEstar.btnImportarClick(Sender: TObject);
var OD : TOpenDialog;
  LOG_ERRO, Flag, path : String;
  encontrou, erro: Boolean;
  count : Integer;
  S, SL,SLLog : TStringList;
begin
  S  := TStringList.Create;
  SL := TStringList.Create;
  SLLog := TStringList.Create;
  LOG_ERRO := ExtractFilePath(Application.ExeName)+'Erro Importa��o - Estabelcimentos.txt';
  if FileExists(LOG_ERRO) then begin
    SLLog.LoadFromFile(LOG_ERRO);
    AddCabecalhoErro(SLLog);
    if FileExists(ExtractFilePath(Application.ExeName)+'\erros_importacao_estabelecimento.txt') then begin
      SL.LoadFromFile(ExtractFilePath(Application.ExeName)+'\erros_importacao_estabelecimento.txt');
      SLLog.Text := SLLog.Text + SL.Text;
      SL.Clear;
    end;
    SLLog.SaveToFile(ExtractFilePath(Application.ExeName)+'\erros_importacao_estabelecimento.txt');
    SLLOG.Clear;
  end;
  if UpperCase(btnImportar.Caption) = '&CANCELAR' then begin
      if QCadastro.State in [dsInsert, dsEdit] then
        QCadastro.Cancel;
      if tExcel.Active then
        tExcel.Close;
      Abort;
  end;
  erro:= False;
  screen.Cursor  := crHourGlass;
  try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
      //path := path + 'Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
      //path := path + 'Extended Properties="Excel 12.0;HDR=YES;"';
    end;
    OD.Free;
    btnImportar.Caption := '&Cancelar';
    tExcel.Close;
    tExcel.ConnectionString := path;
    tExcel.TableName:= 'Estabelecimentos$';
    tExcel.Active := True;
    try
      //tExcel.Open;
    except
      MsgErro('N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "ESTABELECIMENTOS" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    pb.Visible := False;
    btnImportar.Caption := '&Importar';
    Abort;
  end;
  pb.Max := 100;
  count := tExcel.RecordCount;
  pb.Visible := True;
  if not DMConexao.AdoCon.InTransaction then
    DMConexao.AdoCon.BeginTrans;
  QCadastro.Close;
  qCadastro.SQL.Text := ' Select * from credenciados';
  QCadastro.Open;
  while not tExcel.Eof do begin
    try
      if (UpperCase(tExcel.FieldByName('STATUS').AsString) = 'C') or
         (UpperCase(tExcel.FieldByName('STATUS').AsString) = 'S') then Flag := 'N' else Flag := 'S';
      encontrou := QCadastro.Locate('CRED_ID', tExcel.FieldByName('CRED_ID').AsString,[]);
      if (not encontrou) and (tExcel.fieldByName('COD_ACESSO').AsString <> '') and (not (QCadastro.Locate('CODACESSO',tExcel.FieldByName('COD_ACESSO').Value,[]))) then begin
        QCadastro.Append;
      end else begin
        QCadastro.Edit;
      end;
      QCadastroCRED_BE_ID.AsInteger       := tExcel.fieldByName('CRED_ID').AsInteger;
      QCadastroNOME.AsString              := tExcel.fieldByName('RAZAO').AsString;
      //QCadastroSEG_ID.Value               := tExcel.fieldByName('SEG_ID').Value;
      QCadastroBANCO.AsInteger            := tExcel.fieldByName('BANCO').AsInteger;
      QCadastroFANTASIA.AsString          := tExcel.fieldByName('FANTASIA').AsString;
      QCadastroENDERECO.AsString          := tExcel.fieldByName('ENDERECO').AsString;
      QCadastroNUMERO.AsInteger           := tExcel.fieldByName('NUMERO').AsInteger;
      QCadastroBAIRRO.AsString            := tExcel.fieldByName('BAIRRO').AsString;
      QCadastroCEP.AsString               := tExcel.fieldByName('CEP').AsString;
      //QCadastroCIDADE.AsString            := tExcel.fieldByName('MUNICIPIO').AsString;
      QCadastroLIBERADO.AsString          := Flag;
      QCadastroAGENCIA.AsString           := tExcel.fieldByName('AGENCIA').AsString;
      QCadastroCODACESSO.AsInteger        := tExcel.fieldByName('COD_ACESSO').AsInteger;
      //QCadastroESTADO.AsString            := tExcel.fieldByName('ESTADO').AsString;
      //QCadastroCGC.AsString               := tExcel.fieldByName('CNPJ').AsString;
      QCadastroINSCRICAO_ESTADUAL.AsString:= tExcel.fieldByName('IE').AsString;
      QCadastroBANCO.AsString             := tExcel.fieldByName('BANCO').AsString;
      QCadastroAGENCIA.AsString           := tExcel.fieldByName('AGENCIA').AsString;
      QCadastroCONTA.AsString     := tExcel.fieldByName('CONTA').AsString;
      QCadastroCORRENTISTA.AsString       := tExcel.fieldByName('CORRENTISTA').AsString;
      QCadastroCOMISSAO.AsString          := tExcel.fieldByName('TAXA').AsString;
//      QCadastroDIAFECHAMENTO2.AsInteger   := 0;
//      QCadastroVENCIMENTO2.AsInteger      := 0;
      //QCadastroPAGA_CRED_POR_ID.AsInteger := 1;
      QCadastroCONTRATO.AsInteger         := QCadastroCRED_BE_ID.AsInteger;
      //QCadastroTIPOFECHAMENTO.AsString    := 'T';
      QCadastroSENHA.AsString             := '9584FC69D9';
      QCadastro.Post;
      except on E:Exception do begin
        if Pos('---'+DateTimeToStr(Now)+'---',SLLog.Text) > 0 then begin
          AddCabecalhoErro(SLLog);
          SLLog.Add('Erro ao tentar importar titular. '+E.Message);
        end;
        erro := True;
      end;
    end;
    tExcel.Next;
    Application.ProcessMessages;
    pb.Position := (tExcel.RecNo * 100) div count;
  end;
  qCadastro.Close;
  qCadastro.SQL.Text := 'select * from credenciados where cred_id = 0';
  qCadastro.Open;
  pb.Position := 0;
  pb.Visible := False;
  btnImportar.Caption := '&Importar';
  if erro then begin
    SL.Free;
    SLLog.Free;
    MsgErro('Ouve erro ao importar os Estabelecimentos!');
    if DMConexao.AdoCon.InTransaction then
      DMConexao.AdoCon.RollbackTrans;
  end else
    if DMConexao.AdoCon.InTransaction then
      DMConexao.AdoCon.CommitTrans;
  btnImportar.Caption := '&Importar';
  Screen.Cursor := crDefault;
  if tExcel.State in [dsEdit, dsInsert] then
    tExcel.Close;
  pb.Position := 0;
  pb.Visible := False;
  if SLLog.Count > 0 then begin
    if FileExists(LOG_ERRO) then
      DeleteFile(LOG_ERRO);
    SLLog.SaveToFile(LOG_ERRO);
    MsgInf('Houveram erros na importa��o. Os Erros foram salvos em um arquivo no diret�rio local com o nome de "Erro Importa��o - Estabelecimentos.txt"');
  end;
  if SLLog <> nil then
    SLLog.Free;
  if S.Count > 0 then
    S.SaveToFile('c:\teste.txt');
end;

procedure TFCadParceirosBemEstar.ButCloseClick(Sender: TObject);
begin
  if UpperCase(btnImportar.Caption) = '&CANCELAR' then begin
    MsgInf('Cancele a importa��o antes de sair da tela');
    Abort;
  end else
    inherited;
end;

procedure TFCadParceirosBemEstar.DuplicarEstabelecimentoalterandoID1Click(
  Sender: TObject);
var I : Integer;
begin
  inherited;
  DMConexao.AdoCon.BeginTrans;
  try
    if not qCredenciados.Active then
      qCredenciados.Open;
    qCredenciados.Append;
    for I := 0 to qCredenciados.Fields.Count -1 do begin
      qCredenciados.Fields[I].Value := qCadastro.Fields[I].Value;
    end;
    qCredenciadosCRED_ID.Value := QCadastroCRED_BE_ID.Value + 30;
    qCredenciadosCODACESSO.Value := QCadastroCODACESSO.Value + 1000;
    qCredenciados.Post;
    DMConexao.AdoCon.CommitTrans;
  except
    DMConexao.AdoCon.RollbackTrans;
  end;
  DMConexao.AdoCon.BeginTrans;
  try
    DMConexao.ExecuteSql('DELETE FROM DIA_FECHA_CRED WHERE CRED_ID = ' + qCredenciadosCRED_ID.AsString);
    DMConexao.AdoCon.CommitTrans;
  except
    DMConexao.AdoCon.RollbackTrans;
  end;
  TrocarCred_ID(qCredenciadosCRED_ID.AsString,True);
end;

procedure TFCadParceirosBemEstar.DBPagaForPorChange(Sender: TObject);
begin
  inherited;
//  DbDia.Visible := (DBPagaForPor.KeyValue = 3) or (DBPagaForPor.KeyValue = 5);
//  lblDia.Visible := (DBPagaForPor.KeyValue = 3) or (DBPagaForPor.KeyValue = 5);
//  if DBPagaForPor.KeyValue = 3 then
//    lblDia.Caption := 'Dia';
//  if DBPagaForPor.KeyValue = 5 then
//    lblDia.Caption := 'Dia Pagto';

end;

procedure TFCadParceirosBemEstar.QCadastroBeforePost(DataSet: TDataSet);
begin
  if (DBPagaForPor.KeyValue <> 3) or (DBPagaForPor.KeyValue <> 5) then begin
    //QCadastroDIA_PAGTO.AsString := '';
  end;
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;
end;

procedure TFCadParceirosBemEstar.dbGridPosColExit(Sender: TObject);
begin
  inherited;
  if qPos.State in [dsEdit] then qPos.Post;
end;

procedure TFCadParceirosBemEstar.btnGravarPosClick(Sender: TObject);
var cred_id, especialidade_id : Integer;
    liberado                  : String;
begin
  inherited;
  QCRED_ESPEC_BEM_ESTAR.Post;
  

end;

procedure TFCadParceirosBemEstar.btnCancelarPosClick(Sender: TObject);
begin
  inherited;
  QCRED_ESPEC_BEM_ESTAR.Cancel;
end;

procedure TFCadParceirosBemEstar.btnExcluirPosClick(Sender: TObject);
begin
  inherited;
  QCRED_ESPEC_BEM_ESTAR.Delete;
end;

procedure TFCadParceirosBemEstar.tbCadPosHide(Sender: TObject);
begin
  inherited;
  QPos.Close;
end;

procedure TFCadParceirosBemEstar.tbCadPosShow(Sender: TObject);
begin
  inherited;
  QPos.Close;
  QPos.Parameters.ParamByName('cred_id').Value := QCadastroCRED_BE_ID.AsInteger;
  QPos.Open;
end;

procedure TFCadParceirosBemEstar.qPosBeforePost(DataSet: TDataSet);
begin
  inherited;
  qPosCRED_ID.AsInteger := QCadastroCRED_BE_ID.AsInteger;
end;

procedure TFCadParceirosBemEstar.dbGridPosTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
    try
    if Pos(Field.FieldName,QPos.Sort) > 0 then begin
       if Pos(' DESC',QPos.Sort) > 0 then QPos.Sort := Field.FieldName
                                                  else QPos.Sort := Field.FieldName+' DESC';
    end
    else QPos.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCadParceirosBemEstar.qEstadosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  //carregaICMS_ESTADUAL(cbbUf.Text);
end;

procedure TFCadParceirosBemEstar.DSEstadosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not qCidades.Locate('NOME',dbLkpCidades.KeyValue,[])) then
  begin
    QCidades.Parameters.ParamByName('ESTADO_ID').Value := dbLkpEstados.KeyValue;
    QCidades.Requery;
  end;
end;


procedure TFCadParceirosBemEstar.grdSaldoNewTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
   try
    if Pos(Field.FieldName,QNovoSaldo.Sort) > 0 then begin
       if Pos(' DESC',QNovoSaldo.Sort) > 0 then QNovoSaldo.Sort := Field.FieldName
                                                  else QNovoSaldo.Sort := Field.FieldName+' DESC';
    end
    else QNovoSaldo.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCadParceirosBemEstar.GridHistoricoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
   try
    if Pos(Field.FieldName,QHistorico.Sort) > 0 then
    begin
      if Pos(' DESC',QHistorico.Sort) > 0 then
        QHistorico.Sort := Field.FieldName
      else
        QHistorico.Sort := Field.FieldName+' DESC';
    end
    else
      QHistorico.Sort := Field.FieldName;
  except
  end;

end;

procedure TFCadParceirosBemEstar.btnCancelFormasClick(Sender: TObject);
begin
  inherited;
  if QFormasPgto.State in [dsInsert,dsEdit] then QFormasPgto.Cancel;
end;

procedure TFCadParceirosBemEstar.btnFirstBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.First;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadParceirosBemEstar.btnFirstCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.First;
end;

procedure TFCadParceirosBemEstar.btnHistClick(Sender: TObject);
begin
  if not QCadastro.IsEmpty then begin
    QLogOcorrencias.Close;
    QLogOcorrencias.SQL.Clear;
    QLogOcorrencias.SQL.Add('SELECT * FROM logs_ocorrencias WHERE id = '+QCadastroCRED_BE_ID.AsString+'');
    if((dtIniHistorico.Date > 0) and (dtFimHistorico.Date > 0)) then begin
        QLogOcorrencias.SQL.Add(' and convert(varchar(10), DATA_HORA, 103) between '+QuotedStr(dtIniHistorico.Text)+' and '+QuotedStr(dtFimHistorico.Text));
        QLogOcorrencias.SQL.Add(' and janela  = ''TabAtendimento''');
    end;
    QLogOcorrencias.Open;
  end;
end;

procedure TFCadParceirosBemEstar.btnLastBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Last;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadParceirosBemEstar.btnLastCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Last;
end;

procedure TFCadParceirosBemEstar.btnNextBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Next;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadParceirosBemEstar.btnNextCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Next;
end;

procedure TFCadParceirosBemEstar.btnPriorBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Prior;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadParceirosBemEstar.btnPriorCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Prior;
end;

procedure TFCadParceirosBemEstar.DSCadastroDataChange(Sender: TObject; Field: TField);
begin
  if not QOcorrencias.FieldByName('atendimento_id').IsNull then
    Label4.Caption := IntToStr(geraID);//string(cbbCredInd.KeyValue)
  //else
    //EdCredInd.Text := '';
end;

procedure TFCadParceirosBemEstar.DSOcorrenciasStateChange(Sender: TObject);
begin
  inherited;
  habilitarBotoes;
end;

procedure TFCadParceirosBemEstar.grdCredOcorrenciasDblClick(Sender: TObject);
begin
  if not QOcorrencias.IsEmpty then begin
    pgc1.ActivePage := ts3;
    lbl4.Caption := QOcorrenciasatendimento_id.AsString;
  end;
end;

procedure TFCadParceirosBemEstar.habilitarBotoes;
begin
  btCancelar.Enabled  := QOcorrencias.State in [dsEdit,dsInsert];
  btGravar.Enabled    := QOcorrencias.State in [dsEdit,dsInsert];
  btn2.Enabled        := (QOcorrencias.State = dsBrowse); //and Incluir;
  btn1.Enabled        := (QOcorrencias.State = dsBrowse); {and Alterar}
  btn3.Enabled        := (QOcorrencias.State = dsBrowse); {and Excluir}
end;

procedure TFCadParceirosBemEstar.QTaxasBeforePost(DataSet: TDataSet);
begin
  inherited;
  tDescontar := '';
  QTaxas.FieldByName('DESCONTAR').AsString := UpperCase(QTaxas.FieldByName('DESCONTAR').AsString);
  tDescontar := UpperCase(QTaxas.FieldByName('DESCONTAR').AsString);
  SavePlace := QTaxas.GetBookmark;
end;


procedure TFCadParceirosBemEstar.QEmpresLibBeforePost(DataSet: TDataSet);
begin
  inherited;
  cLiberado := '';
  cComissao := 0;
  QEmpresLib.FieldByName('LIBERADO').AsString := UpperCase(QEmpresLib.FieldByName('LIBERADO').AsString);
  cLiberado := UpperCase(QEmpresLib.FieldByName('LIBERADO').AsString);

  QEmpresLib.FieldByName('COMISSAO').AsString := UpperCase(QEmpresLib.FieldByName('COMISSAO').AsString);
  cComissao := StrtoCurr(UpperCase(QEmpresLib.FieldByName('COMISSAO').AsString));

end;

procedure TFCadParceirosBemEstar.QOcorrenciasAfterInsert(DataSet: TDataSet);
begin
  // GERA ID DA OCORRENCIA E ATRIBUI NO LABEL lbl4
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_CRED');
  DMConexao.AdoQry.Open;
  lbl4.Caption := IntToStr(DMConexao.AdoQry.Fields[0].AsInteger);
  QOcorrenciasatendimento_id.AsInteger := DMConexao.AdoQry.Fields[0].AsInteger;
  QStatusAtend.Open;
  //ABRE O SHOW MODAL PARA CONFIRMAR INFORMA��ES DO CONTATO.
  //lbl4.Caption := IntToStr(geraID);
  FAltContatoCred := TFAltContatoCred.Create(self);
  FAltContatoCred.txtNome1.Text := QCadastroCONTATO.AsString;
  FAltContatoCred.txtTelefone1.Text := QCadastroFONE1.AsString;
  FAltContatoCred.txtTel2.Text      := QCadastroFONE2.AsString;

  //SE O TELEFONE 2 FOR VAZIO OCULTA O MESMO PARA QUE SE
  //TORNE VIS�VEL APENAS SE O OPERADOR QUISER ADICION�-LO
  if QCadastroFONE2.AsString = '' then begin
    FAltContatoCred.txtTel2.Visible := False;
  end;

  FAltContatoCred.ShowModal;

  if FAltContatoCred.ModalResult = mrOk then
  begin
    if(((FAltContatoCred.txtTelefone1.Text <> '') and (FAltContatoCred.txtTelefone1.Text <> QCadastroFONE1.AsString)) or (FAltContatoCred.txtTel2.Text <> QCadastroFONE2.AsString)) then
    begin
      QCadastro.Edit;
      if ((FAltContatoCred.txtTel2.Text <> '') and (FAltContatoCred.txtTel2.Text <> QCadastroFONE2.AsString)) then begin
        QCadastroFONE2.AsString := FAltContatoCred.txtTel2.Text;
      end;
        QCadastroFONE2.AsString := FAltContatoCred.txtTelefone1.Text;
        QCadastro.Post;
    end;
    QOcorrenciasnome_solicitante.AsString := FAltContatoCred.txtNome1.Text;
    QOcorrenciastel_solictante.AsString   := FAltContatoCred.txtTelefone1.Text;
    txtMotivo.SetFocus;
  end
     else
      begin
        txtNome.SetFocus;
      end;
  //FIM DO MODAL

end;

procedure TFCadParceirosBemEstar.QOcorrenciasBeforePost(DataSet: TDataSet);
begin
  if QCadastroCRED_BE_ID.AsInteger = 0 then begin
      MsgInf('� necess�rio selecionar um Estabelecimento bara efetuar a busca.');
      PageControl1.TabIndex := 0;
      EdCod.SetFocus;
      Exit;
  end;
end;

procedure TFCadParceirosBemEstar.QTaxasProxPagAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_PROX_PAG_ID AS TAXAS_PROX_PAG_ID');
  DMConexao.AdoQry.Open;
  QTaxasProxPagTAXAS_PROX_PAG_ID.AsInteger            := DMConexao.AdoQry.FieldByName('TAXAS_PROX_PAG_ID').AsInteger;
end;

procedure TFCadParceirosBemEstar.ts2Enter(Sender: TObject);
begin
  dtInicial.SetFocus;
end;

procedure TFCadParceirosBemEstar.ts2Show(Sender: TObject);
begin
  dtInicial.SetFocus;
  PanelFinalizado.Color := RGB(240,255,255);
  PanelPendente.Color   := RGB(255,106,106);
end;

procedure TFCadParceirosBemEstar.ts3Enter(Sender: TObject);
begin
  txtNome.SetFocus;
end;

procedure TFCadParceirosBemEstar.ts3Show(Sender: TObject);
begin
  txtNome.SetFocus;
  habilitarBotoes;
  if not QCadastro.IsEmpty then
  begin
    BuscaUltimasAlteracaoesCred;
    if not DMConexao.Query1.IsEmpty then begin
      txtDataAlteracaoCred.Text     := DMConexao.Query1.Fields[0].AsString;
      txtOperadorAlteracaoCred.Text := DMConexao.Query1.Fields[1].AsString;
    end;
  end;
end;

procedure TFCadParceirosBemEstar.BuscaUltimasAlteracaoesCred;
begin
  DMConexao.Query1.Close;
  DMConexao.Query1.SQL.Clear;
  DMConexao.Query1.SQL.Add('SELECT top 1 coalesce (data_hora,0) as dt_alteracao, coalesce (operador, '''')');
  DMConexao.Query1.SQL.Add( 'FROM LOGS_OCORRENCIAS ');
  DMConexao.Query1.SQL.Add(' WHERE id = '+QCadastroCRED_BE_ID.asString+' and janela = ''TabAtenimento''');
  DMConexao.Query1.SQL.Add(' order by data_hora desc');
  DMConexao.Query1.Open;
end;

procedure TFCadParceirosBemEstar.grdCredOcorrenciasDrawColumnCell(Sender: TObject; const
    Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  with(grdCredOcorrencias) do
  begin
    if DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 3 then begin
      //FORMATA��O PARA LINHAS COM STATUS 3 - FINALIZADO E/OU RESOLVIDO
      // PARA ESTA CONDI��O A LINHA DEVE SER AZUL
      grdCredOcorrencias.Canvas.Brush.Color := RGB(240, 255, 255);
      grdCredOcorrencias.Canvas.Font.Color:= clBlack;
    end
    else if((DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 1) or (DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 2)) then
    begin
      //FORMATA��O PARA LINHAS COM STATUS 1 E 2 OU SEJA PENDENTES
      // PARA ESTA CONDI��O A LINHA DEVE SER VERMELHA
      grdCredOcorrencias.Canvas.Brush.Color := RGB(255,106,106);
      grdCredOcorrencias.Canvas.Font.Color:= clWhite;
    end;

      grdCredOcorrencias.Canvas.FillRect(Rect);
      Canvas.FillRect(Rect);

      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;


procedure TFCadParceirosBemEstar.QPosAfterInsert(DataSet: TDataSet);
begin
  inherited;
  QPosVERSAO_NAVS.AsString := 'v1.9.1';
  QPosVERSAO_OS.AsString := 'S';
  QPosVERSAO_EOS.AsString := 'S';
  QPosATUALIZOU_LUA.AsString := 'S';
end;

procedure TFCadParceirosBemEstar.btnInserirPosClick(Sender: TObject);
begin
  inherited;
  QCRED_ESPEC_BEM_ESTAR.Append;

end;

procedure TFCadParceirosBemEstar.QCRED_ESPEC_BEM_ESTARBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.IsEmpty then
     QCRED_ESPEC_BEM_ESTAR.Parameters.ParamByName('cred_id').Value := QCadastroCRED_BE_ID.AsInteger
     else
     QCRED_ESPEC_BEM_ESTAR.Parameters.ParamByName('cred_id').Value := 0;
end;

procedure TFCadParceirosBemEstar.BitBtn2Click(Sender: TObject);
begin
  inherited;
  FBuscaEspecialidade1:= TFBuscaEspecialidade1.Create(nil);
  FBuscaEspecialidade1.ShowModal;
//  FCancelaAutor.cred_id:= Qcontacorrente.FieldByName('CRED_ID').AsInteger;
//  FCancelaAutor.txtCred.Caption := QcontacorrenteCREDENCIADO.AsString;
//  FCancelaAutor.autorizacao:= StrToInt(Qcontacorrente.FieldByName('AUTORIZACAO_ID').AsString+PadL(Qcontacorrente.FieldByName('DIGITO').AsString,2,'0'));
//  if Qcontacorrente.FieldByName('TRANS_ID').IsNull then
//    FCancelaAutor.transacao:= 0
//  else
end;

procedure TFCadParceirosBemEstar.QCRED_ESPEC_BEM_ESTARAfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  QCRED_ESPEC_BEM_ESTARLIBERADO.AsString := 'S';
  if not QCadastroCRED_BE_ID.IsNull then
  begin
    QCRED_ESPEC_BEM_ESTARCRED_BE_ID.AsInteger := QCadastroCRED_BE_ID.AsInteger;
  end;
end;


procedure TFCadParceirosBemEstar.QCRED_ESPEC_BEM_ESTARBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if QCadastro.IsEmpty then
    begin
      MsgInf('Selecione um credenciado para efetuar o cadastro de especialidades.');
      PageControl1.ActivePageIndex := 0;
      EdCod.SetFocus;
    end;
  QCRED_ESPEC_BEM_ESTARCRED_BE_ID.AsInteger := QCadastroCRED_BE_ID.AsInteger;
end;

procedure TFCadParceirosBemEstar.BitBtn4Click(Sender: TObject);
begin
  inherited;
  //FrmCadStatusAtendimento := TFrmCadStatusAtendimento.Create(self);
  FrmCadStatusAtendimento := TFrmCadStatusAtendimento.Create(Self);
  FrmCadStatusAtendimento.ShowModal;
end;

procedure TFCadParceirosBemEstar.btnAdicionaBairroClick(Sender: TObject);
begin
  inherited;
  frmBairro := TfrmBairro.Create(Self);
  frmBairro.ShowModal;
  if frmBairro.ModalResult = mrOk then
  begin
    QBairros.Close;
    QBairros.Open;
    QBairros.Insert;
    QBairrosDESCRICAO.AsString := frmBairro.txtBairro.Text;
    QBairrosCID_ID.AsInteger := QBairros.Parameters.ParamByName('CID_ID').Value;
    QBairros.Post;
    QBairros.Requery;
    ShowMessage('Bairro inserido com sucesso!');
  end;
end;

procedure TFCadParceirosBemEstar.DSCidadesDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not qBairros.Locate('DESCRICAO',dbLkpBairros.KeyValue,[])) then
  begin
    QBairros.Parameters.ParamByName('CID_ID').Value := dbLkpCidades.KeyValue;
    QBairros.Requery;
  end;
  btnAdicionaBairro.Enabled := true;
end;

end.
