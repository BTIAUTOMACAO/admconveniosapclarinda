inherited FLancIndiv: TFLancIndiv
  Left = 192
  Top = 0
  Caption = 'Lan'#231'amento Individual de Nota'
  ClientHeight = 706
  ClientWidth = 1024
  OldCreateOrder = True
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1024
  end
  object pc: TPageControl [1]
    Left = 0
    Top = 23
    Width = 1024
    Height = 683
    ActivePage = tsPrincipal
    Align = alClient
    Style = tsButtons
    TabOrder = 1
    object tsPrincipal: TTabSheet
      Caption = 'Principal'
      OnShow = tsPrincipalShow
      DesignSize = (
        1016
        652)
      object Label13: TLabel
        Left = 0
        Top = 639
        Width = 1016
        Height = 13
        Align = alBottom
        Caption = '  Mensagem de retorno:'
        Visible = False
      end
      object lblAut: TLabel
        Left = 272
        Top = 432
        Width = 743
        Height = 193
        Alignment = taCenter
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoSize = False
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -48
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Layout = tlCenter
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 129
        Width = 1016
        Height = 64
        Align = alTop
        Caption = 'Empresa'
        TabOrder = 2
        DesignSize = (
          1016
          64)
        object Label4: TLabel
          Left = 8
          Top = 14
          Width = 49
          Height = 13
          Caption = 'Empres ID'
        end
        object LabEmp: TLabel
          Left = 98
          Top = 14
          Width = 64
          Height = 13
          Caption = 'Raz'#227'o/Nome'
        end
        object cbbEmpr: TJvDBLookupCombo
          Left = 98
          Top = 29
          Width = 908
          Height = 27
          DropDownWidth = 350
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Anchors = [akLeft, akTop, akRight]
          LookupField = 'EMPRES_ID'
          LookupDisplay = 'NOME'
          LookupSource = dsEmpr
          ParentFont = False
          TabOrder = 1
          OnChange = cbbEmprChange
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
        end
        object edtEmpr: TEdit
          Left = 8
          Top = 29
          Width = 81
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = edtEmprChange
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
          OnKeyPress = edtEmprKeyPress
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1016
        Height = 65
        Align = alTop
        Caption = 'Estabelecimento'
        TabOrder = 0
        DesignSize = (
          1016
          65)
        object Label6: TLabel
          Left = 8
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Estab. ID'
        end
        object LabCre: TLabel
          Left = 288
          Top = 16
          Width = 64
          Height = 13
          Caption = 'Raz'#227'o/Nome'
        end
        object Label26: TLabel
          Left = 124
          Top = 16
          Width = 27
          Height = 13
          Caption = 'CNPJ'
        end
        object cbbEstab: TJvDBLookupCombo
          Left = 288
          Top = 30
          Width = 717
          Height = 27
          DropDownWidth = 350
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Anchors = [akLeft, akTop, akRight]
          LookupField = 'CRED_ID'
          LookupDisplay = 'NOME'
          LookupSource = dsEstab
          ParentFont = False
          TabOrder = 2
          OnChange = cbbEstabChange
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
        end
        object edtEstab: TEdit
          Left = 8
          Top = 30
          Width = 109
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnChange = edtEstabChange
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
          OnKeyPress = edtEstabKeyPress
        end
        object edtCnpj: TEdit
          Left = 124
          Top = 30
          Width = 157
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnChange = edtEstabChange
          OnEnter = edtEstabEnter
          OnExit = edtCnpjExit
          OnKeyPress = edtEstabKeyPress
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 65
        Width = 1016
        Height = 64
        Align = alTop
        Caption = 'Conveniado'
        TabOrder = 1
        DesignSize = (
          1016
          64)
        object Label1: TLabel
          Left = 8
          Top = 14
          Width = 31
          Height = 13
          Caption = 'Cart'#227'o'
        end
        object Label2: TLabel
          Left = 184
          Top = 14
          Width = 31
          Height = 13
          Caption = 'Chapa'
        end
        object Label3: TLabel
          Left = 400
          Top = 14
          Width = 28
          Height = 13
          Caption = 'Nome'
        end
        object ButPesqConv: TSpeedButton
          Left = 981
          Top = 27
          Width = 23
          Height = 30
          Hint = 'Pesquisa de Clientes'
          Anchors = [akTop, akRight]
          Caption = '...'
          Layout = blGlyphRight
          ParentShowHint = False
          ShowHint = True
          OnClick = ButPesqConvClick
        end
        object Label12: TLabel
          Left = 288
          Top = 15
          Width = 73
          Height = 13
          AutoSize = False
          Caption = 'Saldo Restante'
        end
        object edtCartao: TEdit
          Left = 8
          Top = 28
          Width = 169
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnEnter = edtEstabEnter
          OnExit = edtCartaoExit
        end
        object edtChapa: TEdit
          Left = 183
          Top = 28
          Width = 97
          Height = 27
          BiDiMode = bdLeftToRight
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
          TabOrder = 1
          OnEnter = edtEstabEnter
          OnExit = edtChapaExit
          OnKeyPress = edtChapaKeyPress
        end
        object edtTitular: TEdit
          Left = 400
          Top = 28
          Width = 578
          Height = 27
          Anchors = [akLeft, akTop, akRight]
          AutoSelect = False
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
          OnKeyDown = edtTitularKeyDown
        end
        object edtSaldo: TJvValidateEdit
          Left = 289
          Top = 28
          Width = 103
          Height = 27
          Alignment = taLeftJustify
          BiDiMode = bdRightToLeft
          ParentBiDiMode = False
          CheckChars = '01234567890'
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfNone
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 369
        Width = 1016
        Height = 24
        Align = alTop
        Caption = 'Produtos'
        TabOrder = 4
        Visible = False
        DesignSize = (
          1016
          24)
        object Label5: TLabel
          Left = 72
          Top = 14
          Width = 73
          Height = 13
          Caption = 'C'#243'd. Barras(F3)'
        end
        object Label7: TLabel
          Left = 168
          Top = 13
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
        end
        object Label17: TLabel
          Left = 8
          Top = 14
          Width = 50
          Height = 13
          Caption = 'Cod. Prod.'
        end
        object Label16: TLabel
          Left = 880
          Top = 13
          Width = 63
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Valor Unit'#225'rio'
        end
        object Label18: TLabel
          Left = 814
          Top = 13
          Width = 55
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Quantidade'
        end
        object Label19: TLabel
          Left = 953
          Top = 13
          Width = 51
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Valor Total'
        end
        object Label10: TLabel
          Left = 6
          Top = 53
          Width = 37
          Height = 13
          Caption = 'Receita'
        end
        object Label21: TLabel
          Left = 195
          Top = 53
          Width = 68
          Height = 13
          Caption = 'Tipo Prescritor'
        end
        object Label22: TLabel
          Left = 95
          Top = 53
          Width = 72
          Height = 13
          Caption = 'N'#250'm. Prescritor'
        end
        object Label23: TLabel
          Left = 818
          Top = 53
          Width = 61
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'UF Prescritor'
        end
        object Label24: TLabel
          Left = 917
          Top = 53
          Width = 63
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Data Receita'
        end
        object lblCodProd: TDBText
          Left = 8
          Top = 29
          Width = 49
          Height = 17
          Alignment = taRightJustify
          DataField = 'ID'
          DataSource = dsCds
        end
        object lblPreco: TDBText
          Left = 879
          Top = 29
          Width = 65
          Height = 17
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          DataField = 'ValorUnitario'
          DataSource = dsCds
        end
        object lblValorTotal: TDBText
          Left = 950
          Top = 29
          Width = 58
          Height = 17
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          DataField = 'ValorTotal'
          DataSource = dsCds
        end
        object Label20: TLabel
          Left = 307
          Top = 53
          Width = 65
          Height = 13
          Caption = 'N'#250'm. Receita'
        end
        object BtnInclui: TBitBtn
          Left = 1
          Top = 97
          Width = 205
          Height = 25
          Hint = 'Incluir registro'
          Caption = 'Incluir (F5)'
          TabOrder = 11
          OnClick = BtnIncluiClick
          Glyph.Data = {
            6A030000424D6A030000000000006A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004D00000000000000CC986500CE9A
            6700D19D6A00D4A06D00D7A37000DBA77400DFAB7800E3AF7C00E6B27F00E8B4
            8100ECB88500F0BC8900F4C08D00F7C39000FAC79300FDCA9600FFCC9800E8CF
            B600EBD4BD00FFD0A000FFD6AB00FFD9B200FFDCB800CFCFD000D0D0D100D1D2
            D200D2D3D300D4D5D500D6D6D700D7D8D800D9DADA00DADADB00DBDBDC00DCDD
            DD00DEDFDF00ECD9C600EED9C500F1DECB00FFE2C300FFE6CC00F3E1D000FFEE
            DD00FFEFDF00E0E0E100E2E2E200E3E4E400E4E5E500E5E5E600E8E8E800E9EA
            EA00EAEAEB00EBEBEB00ECECEC00EEEEEE00FFF0E100FFF1E300FFF3E600FFF4
            E900FFF6EC00F0F0F000F2F2F200F3F3F300F7F7F700FFF7F000FFF9F300FFFA
            F600F8F8F800F9F9F900FAFAFA00FBFBFB00FFFCF900FFFDFB00FCFCFC00FDFD
            FD00FFFEFD00FEFEFE00FFFFFF004C4C2311122425282825241211234C4C4C4C
            353133353B3C3C3B353331354C4C4C4C0129292929292929292929014C4C4C4C
            183E3E3E3E3E3E3E3E3E3E184C4C4C4C022A2A2A2A2A2A2A2A2A2A024C4C4C4C
            193E3E3E3E3E3E3E3E3E3E194C4C4C4C0336363636363636363636034C4C4C4C
            1A424242424242424242421A4C4C4C4C0437373737373737373737044C4C4C4C
            1B434343434343434343431B4C4C4C4C0538383838383838383838054C4C4C4C
            1C444444444444444444441C4C4C4C4C0639393939393939393939064C4C4C4C
            1D444444444444444444441D4C4C4C4C073A3A3A3A3A3A3A3A3A3A074C4C4C4C
            1E444444444444444444441E4C4C4C4C093F3F3F3F3F3F3F3F3F3F094C4C4C4C
            2045454545454545454545204C4C4C4C0A404040404040404040400A4C4C4C4C
            2148484848484848484848214C4C4C4C0B414141414141414141410B4C4C4C4C
            2249494949494949494949224C4C4C4C0C464646464646464646460C4C4C4C4C
            2B4B4B4B4B4B4B4B4B4B4B2B4C4C4C4C0D47474747474747080000004C4C4C4C
            2C4C4C4C4C4C4C4C1F1717174C4C4C4C0E4A4A4A4A4A4A4A104C004C4C4C4C4C
            2D4C4C4C4C4C4C4C2F4C174C4C4C4C4C0F4C4C4C4C4C4C4C10004C4C4C4C4C4C
            2E4C4C4C4C4C4C4C2F174C4C4C4C4C4C1513141626272726104C4C4C4C4C4C4C
            343032353B3D3D3B2F4C4C4C4C4C}
          NumGlyphs = 2
        end
        object btnApaga: TBitBtn
          Left = 411
          Top = 97
          Width = 205
          Height = 25
          Hint = 'Apagar registro'
          Caption = 'Apagar (F6)'
          TabOrder = 13
          OnClick = btnApagaClick
          Glyph.Data = {
            82030000424D8203000000000000820100002800000020000000100000000100
            08000000000000020000232E0000232E000053000000000000000021CC000324
            CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
            F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
            FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
            FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
            FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
            CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
            D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
            F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
            FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
            E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
            F400FFFFFF005252525252525252525252525252525252525252525252525252
            525252525252525216002552525252525225001652525252341A4A5252525252
            524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
            4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
            1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
            2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
            30471E4C52525252523B05211212121221053B5252525252524C244731313131
            47244C525252525252523C0617131317063C52525252525252524D2636323236
            264D52525252525252523D0714141414073D52525252525252524E2733333333
            274E525252525252523E08151521211515083E5252525252524F283535474735
            35284F52525252523F091818210909211818093F525252524F29434347292947
            4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
            4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
            2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
            512E472E4852525223104452525252525244102352525252492F515252525252
            52512F4952525252525252525252525252525252525252525252525252525252
            525252525252}
          NumGlyphs = 2
          Spacing = 2
        end
        object cbbReceita: TComboBox
          Left = 6
          Top = 67
          Width = 57
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 5
          Text = 'N'#227'o'
          OnChange = cbbReceitaChange
          OnExit = cbbReceitaExit
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object btnPesqProd: TBitBtn
          Left = 784
          Top = 27
          Width = 24
          Height = 21
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 3
          OnClick = btnPesqProdClick
        end
        object edtCodBarras: TDBEdit
          Left = 72
          Top = 27
          Width = 89
          Height = 21
          DataField = 'CodBarras'
          DataSource = dsCds
          TabOrder = 1
          OnExit = edtCodBarrasExit
          OnKeyPress = edtEstabKeyPress
        end
        object edtDescricao: TDBEdit
          Left = 168
          Top = 27
          Width = 616
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Descricao'
          DataSource = dsCds
          TabOrder = 2
          OnKeyDown = edtDescricaoKeyDown
        end
        object edtQtd: TDBEdit
          Left = 815
          Top = 27
          Width = 53
          Height = 21
          Anchors = [akTop, akRight]
          DataField = 'Quantidade'
          DataSource = dsCds
          TabOrder = 4
          OnChange = edtQtdChange
          OnKeyPress = edtQtdKeyPress
        end
        object edtNumPresc: TDBEdit
          Left = 96
          Top = 67
          Width = 69
          Height = 21
          Color = clBtnFace
          Ctl3D = True
          DataField = 'NumPrescritor'
          DataSource = dsCds
          Enabled = False
          ParentCtl3D = False
          TabOrder = 6
        end
        object btnGravar: TBitBtn
          Left = 206
          Top = 97
          Width = 205
          Height = 25
          Hint = 'Gravar Registro'
          Caption = 'Gravar (F7)'
          TabOrder = 12
          OnClick = btnGravarClick
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object dtpDtReceita: TDBEdit
          Left = 915
          Top = 67
          Width = 97
          Height = 21
          Anchors = [akTop, akRight]
          Color = clBtnFace
          Ctl3D = True
          DataField = 'dtReceita'
          DataSource = dsCds
          Enabled = False
          ParentCtl3D = False
          TabOrder = 10
          OnExit = dtpDtReceitaExit
        end
        object cbUF: TDBComboBox
          Left = 819
          Top = 67
          Width = 65
          Height = 21
          Style = csDropDownList
          Anchors = [akTop, akRight]
          Color = clBtnFace
          Ctl3D = True
          DataField = 'ufPrescritor'
          DataSource = dsCds
          Enabled = False
          ItemHeight = 13
          Items.Strings = (
            'AC'
            'AL'
            'AP'
            'AM'
            'BA'
            'CE'
            'DF'
            'ES'
            'GO'
            'MA'
            'MT'
            'MS'
            'MG'
            'PA'
            'PB'
            'PR'
            'PE'
            'PI'
            'RJ'
            'RN'
            'RS'
            'RO'
            'RR'
            'SC'
            'SP'
            'SE'
            'TO')
          ParentCtl3D = False
          TabOrder = 9
        end
        object edtNumReceit: TDBEdit
          Left = 305
          Top = 67
          Width = 474
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          Ctl3D = True
          DataField = 'NumReceita'
          DataSource = dsCds
          Enabled = False
          ParentCtl3D = False
          TabOrder = 8
        end
        object z: TJvDBGrid
          Left = 2
          Top = -57
          Width = 1012
          Height = 79
          Align = alBottom
          DataSource = dsCds
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = zKeyDown
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 17
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grupo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodBarras'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Quantidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorUnitario'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorTotal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NumPrescritor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ufPrescritor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NumReceita'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dtReceita'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tipoPrescritor'
              Visible = True
            end>
        end
        object cbTipoPresc: TComboBox
          Left = 196
          Top = 67
          Width = 75
          Height = 21
          Style = csDropDownList
          Color = clBtnFace
          Enabled = False
          ItemHeight = 13
          TabOrder = 7
          OnChange = cbTipoPrescChange
          Items.Strings = (
            'CRM'
            'CRO'
            'CRMV')
        end
      end
      object FLancIndiv: TGroupBox
        Left = 0
        Top = 193
        Width = 1016
        Height = 176
        Align = alTop
        Caption = 'Dados da Autoriza'#231#227'o'
        TabOrder = 3
        DesignSize = (
          1016
          176)
        object Label8: TLabel
          Left = 8
          Top = 15
          Width = 23
          Height = 13
          Caption = 'Data'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 132
          Top = 15
          Width = 14
          Height = 13
          Caption = 'NF'
        end
        object Label11: TLabel
          Left = 686
          Top = 15
          Width = 31
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'D'#233'bito'
        end
        object Label14: TLabel
          Left = 690
          Top = 59
          Width = 114
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Hist'#243'rico do lan'#231'amento'
        end
        object Label15: TLabel
          Left = 9
          Top = 58
          Width = 201
          Height = 13
          Caption = 'Forma de Pagamento(somente se liberado)'
        end
        object LabAguarde: TLabel
          Left = 404
          Top = 142
          Width = 199
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Aguarde! Conectando ao servidor..'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object edtNota: TEdit
          Left = 132
          Top = 29
          Width = 89
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
          OnKeyPress = edtNotaKeyPress
        end
        object edtHistorico: TEdit
          Left = 685
          Top = 74
          Width = 323
          Height = 27
          Anchors = [akTop, akRight]
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          MaxLength = 50
          ParentFont = False
          TabOrder = 4
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
        end
        object cbbFormaPgto: TDBLookupComboBox
          Left = 8
          Top = 74
          Width = 667
          Height = 27
          Hint = 
            'Somente as formas de pagamentos liberadas no sistema ser'#227'o mostr' +
            'as.'
          Anchors = [akLeft, akTop, akRight]
          DropDownWidth = 250
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          KeyField = 'FormaId'
          ListField = 'Descricao'
          ListSource = dsForPgto
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnEnter = edtEstabEnter
          OnExit = edtEstabExit
        end
        object btnPegaAut: TButton
          Left = 224
          Top = 102
          Width = 577
          Height = 31
          Anchors = [akLeft, akTop, akRight]
          Caption = 'Pegar Autoriza'#231#227'o (F12)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = btnPegaAutClick
        end
        object btnNovoLanc: TButton
          Left = 7
          Top = 102
          Width = 179
          Height = 31
          Caption = 'Nova Autoriza'#231#227'o (F9)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = btnNovoLancClick
        end
        object edtDebito: TJvValidateEdit
          Left = 685
          Top = 28
          Width = 80
          Height = 27
          Anchors = [akTop, akRight]
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfFloat
          DecimalPlaces = 2
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnEnter = edtDebitoEnter
          OnExit = edtDebitoExit
          OnKeyPress = edtDebitoKeyPress
        end
        object edtData: TJvDateEdit
          Left = 7
          Top = 28
          Width = 118
          Height = 27
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ShowNullDate = False
          TabOrder = 0
        end
      end
      object reAut: TRichEdit
        Left = 0
        Top = 376
        Width = 265
        Height = 263
        Anchors = [akLeft, akTop, akBottom]
        Color = 13434879
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 5
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 412
    Top = 656
  end
  object dsEstab: TDataSource
    DataSet = qEstab
    Left = 347
    Top = 600
  end
  object dsEmpr: TDataSource
    DataSet = qEmpr
    Left = 378
    Top = 601
  end
  object dsConv: TDataSource
    DataSet = qConv
    Left = 416
    Top = 600
  end
  object dsCCorrente: TDataSource
    DataSet = qCCorrente
    Left = 448
    Top = 600
  end
  object qForPgto: TRxMemoryData
    FieldDefs = <
      item
        Name = 'FormaId'
        DataType = ftInteger
      end
      item
        Name = 'Descricao'
        DataType = ftString
        Size = 60
      end>
    Left = 480
    Top = 656
    object qForPgtoFormaId: TIntegerField
      FieldName = 'FormaId'
    end
    object qForPgtoDescricao: TStringField
      FieldName = 'Descricao'
      Size = 60
    end
  end
  object dsForPgto: TDataSource
    DataSet = qForPgto
    Left = 520
    Top = 656
  end
  object dsProgramas: TDataSource
    DataSet = qProgramas
    Left = 488
    Top = 600
  end
  object cds: TSimpleDataSet
    Aggregates = <>
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    BeforePost = cdsBeforePost
    AfterPost = cdsAfterPost
    AfterDelete = cdsAfterDelete
    Left = 448
    Top = 656
    object cdsID: TIntegerField
      DisplayWidth = 7
      FieldName = 'ID'
    end
    object cdsGrupo: TIntegerField
      DisplayWidth = 7
      FieldName = 'Grupo'
    end
    object cdsDescricao: TStringField
      DisplayWidth = 30
      FieldName = 'Descricao'
      Size = 90
    end
    object cdsCodBarras: TStringField
      DisplayWidth = 12
      FieldName = 'CodBarras'
      Size = 13
    end
    object cdsQuantidade: TIntegerField
      DisplayWidth = 3
      FieldName = 'Quantidade'
    end
    object cdsValorUnitario: TCurrencyField
      DisplayWidth = 7
      FieldName = 'ValorUnitario'
    end
    object cdsValorTotal: TCurrencyField
      DisplayWidth = 7
      FieldName = 'ValorTotal'
    end
    object cdsNumPrescritor: TStringField
      DisplayWidth = 7
      FieldName = 'NumPrescritor'
      Size = 10
    end
    object cdsufPrescritor: TStringField
      FieldName = 'ufPrescritor'
      Size = 2
    end
    object cdsNumReceita: TIntegerField
      DisplayWidth = 8
      FieldName = 'NumReceita'
    end
    object cdsdtReceita: TDateField
      DisplayWidth = 8
      FieldName = 'dtReceita'
      DisplayFormat = 'dd.mm.yyyy'
      EditMask = '!99/99/9999;1;_'
    end
    object cdstipoPrescritor: TStringField
      DisplayWidth = 5
      FieldName = 'tipoPrescritor'
      Size = 10
    end
  end
  object dsCds: TDataSource
    DataSet = cds
    Left = 376
    Top = 656
  end
  object HTTPRIO1: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 344
    Top = 655
  end
  object qEstab: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  cred_id, '
      '  nome, '
      '  fantasia, '
      '  codacesso, '
      '  liberado, '
      '  senha,'
      '  cgc,'
      '  vencimento1'
      'from credenciados '
      'where apagado <> '#39'S'#39)
    Left = 348
    Top = 569
    object qEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qEstabnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEstabfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object qEstabcodacesso: TIntegerField
      FieldName = 'codacesso'
    end
    object qEstabliberado: TStringField
      FieldName = 'liberado'
      FixedChar = True
      Size = 1
    end
    object qEstabsenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
    object qEstabcgc: TStringField
      FieldName = 'cgc'
      Size = 18
    end
    object qEstabvencimento1: TWordField
      FieldName = 'vencimento1'
    end
  end
  object qEmpr: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  empres_id, '
      '  nome, '
      '  fantasia, '
      '  liberada,'
      '  prog_desc'
      'from empresas '
      'where apagado <> '#39'S'#39)
    Left = 380
    Top = 570
    object qEmprempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qEmprnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEmprfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
    object qEmprliberada: TStringField
      FieldName = 'liberada'
      FixedChar = True
      Size = 1
    end
    object qEmprprog_desc: TStringField
      FieldName = 'prog_desc'
      FixedChar = True
      Size = 1
    end
  end
  object qConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  cartoes.conv_id, '
      '  cartoes.cartao_id, '
      '  cartoes.nome,'
      '  conveniados.cpf,'
      '  cartoes.codigo, '
      '  cartoes.digito,'
      '  cartoes.codcartimp,'
      '  cartoes.liberado as cartlib,'
      '  conveniados.liberado as convlib, '
      '  conveniados.chapa, '
      '  conveniados.senha,'
      '  conveniados.empres_id'
      'from cartoes'
      'join conveniados on conveniados.conv_id = cartoes.conv_id'
      'where conveniados.apagado <> '#39'S'#39' '
      'and cartoes.apagado <> '#39'S'#39)
    Left = 412
    Top = 572
    object qConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qConvcartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object qConvnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object qConvcpf: TStringField
      FieldName = 'cpf'
      Size = 14
    end
    object qConvcodigo: TIntegerField
      FieldName = 'codigo'
    end
    object qConvdigito: TWordField
      FieldName = 'digito'
    end
    object qConvcodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object qConvcartlib: TStringField
      FieldName = 'cartlib'
      FixedChar = True
      Size = 1
    end
    object qConvconvlib: TStringField
      FieldName = 'convlib'
      FixedChar = True
      Size = 1
    end
    object qConvchapa: TFloatField
      FieldName = 'chapa'
    end
    object qConvsenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
    object qConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object qCCorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  * '
      'from contacorrente '
      'where autorizacao_id = 0')
    Left = 452
    Top = 572
    object qCCorrenteAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object qCCorrenteCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object qCCorrenteCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object qCCorrenteCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qCCorrenteDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object qCCorrenteDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object qCCorrenteHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object qCCorrenteDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object qCCorrenteDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object qCCorrenteCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object qCCorrenteVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object qCCorrenteBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object qCCorrenteFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object qCCorrenteFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qCCorrentePAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object qCCorrenteAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object qCCorrenteOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qCCorrenteDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object qCCorrenteDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object qCCorrenteDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object qCCorrenteDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object qCCorrenteHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object qCCorrenteNF: TIntegerField
      FieldName = 'NF'
    end
    object qCCorrenteDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object qCCorrenteDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object qCCorrenteDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object qCCorrenteOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object qCCorrenteOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object qCCorrenteDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object qCCorrenteOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object qCCorrenteCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteNSU: TIntegerField
      FieldName = 'NSU'
    end
    object qCCorrentePREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qCCorrenteEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object qProgramas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select prog_id, nome from programas where apagado <> '#39'S'#39)
    Left = 484
    Top = 570
    object qProgramasprog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object qProgramasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object qLogLancIndv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      '  *'
      'from'
      '  LOG_LANC_INDIVIDUAL'
      'where log_lanc_ind_id = 0')
    Left = 524
    Top = 570
    object qLogLancIndvLOG_LANC_IND_ID: TIntegerField
      FieldName = 'LOG_LANC_IND_ID'
    end
    object qLogLancIndvDATA_INI: TDateTimeField
      FieldName = 'DATA_INI'
    end
    object qLogLancIndvDATA_FIN: TDateTimeField
      FieldName = 'DATA_FIN'
    end
    object qLogLancIndvABRIR_TRANS_XML: TMemoField
      FieldName = 'ABRIR_TRANS_XML'
      BlobType = ftMemo
    end
    object qLogLancIndvABRIR_TRANS_XML_RET: TMemoField
      FieldName = 'ABRIR_TRANS_XML_RET'
      BlobType = ftMemo
    end
    object qLogLancIndvVALIDAR_PRODS_XML: TMemoField
      FieldName = 'VALIDAR_PRODS_XML'
      BlobType = ftMemo
    end
    object qLogLancIndvVALIDAR_PRODS_XML_RET: TMemoField
      FieldName = 'VALIDAR_PRODS_XML_RET'
      BlobType = ftMemo
    end
    object qLogLancIndvFECHAR_TRANS_XML: TMemoField
      FieldName = 'FECHAR_TRANS_XML'
      BlobType = ftMemo
    end
    object qLogLancIndvFECHAR_TRANS_XML_RET: TMemoField
      FieldName = 'FECHAR_TRANS_XML_RET'
      BlobType = ftMemo
    end
    object qLogLancIndvCONFIRMAR_TRANS_XML: TMemoField
      FieldName = 'CONFIRMAR_TRANS_XML'
      BlobType = ftMemo
    end
    object qLogLancIndvCONFIRMAR_TRANS_XML_RET: TMemoField
      FieldName = 'CONFIRMAR_TRANS_XML_RET'
      BlobType = ftMemo
    end
    object qLogLancIndvCANCELA_TRANS_XML: TMemoField
      FieldName = 'CANCELA_TRANS_XML'
      BlobType = ftMemo
    end
    object qLogLancIndvCANCELA_TRANS_XML_RET: TMemoField
      FieldName = 'CANCELA_TRANS_XML_RET'
      BlobType = ftMemo
    end
    object qLogLancIndvMSG_ERRO: TMemoField
      FieldName = 'MSG_ERRO'
      BlobType = ftMemo
    end
  end
  object QFormaPagto: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT A.FORMA_ID, B.DESCRICAO FROM FORMAS_EMP_LIB '
      'AS A, FORMASPAGTO AS B WHERE '
      'A.EMP_ID = :empres_id'
      'AND A.FORMA_ID = B.FORMA_ID AND B.LIBERADO = '#39'S'#39' '
      'ORDER BY A.FORMA_ID')
    Left = 572
    Top = 604
    object QFormaPagtoFORMA_ID: TIntegerField
      FieldName = 'FORMA_ID'
    end
    object QFormaPagtoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 60
    end
  end
  object DSFormaPagto: TDataSource
    DataSet = QFormaPagto
    Left = 572
    Top = 572
  end
  object HTTPRIO2: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 600
    Top = 639
  end
end
