object FVisualizaImpressao: TFVisualizaImpressao
  Left = 234
  Top = 225
  Width = 696
  Height = 399
  Caption = 'Visualizar Impress'#227'o'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Toolbar: TJvSpeedBar
    Left = 0
    Top = 0
    Width = 680
    Height = 56
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbAllowResize, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 8
    BtnOffsetVert = 3
    BtnWidth = 54
    BtnHeight = 50
    Version = 3
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TJvSpeedBarSection
      Caption = 'File'
    end
    object SpeedbarSection2: TJvSpeedBarSection
      Caption = 'View'
    end
    object SpeedbarSection3: TJvSpeedBarSection
      Caption = 'Window'
    end
    object SpeedbarSection4: TJvSpeedBarSection
      Caption = 'Help'
    end
    object ExitBtn: TJvSpeedItem
      BtnCaption = 'Fechar'
      Caption = 'Fechar'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD0000777777777777777DDDD7000077777777777777FDFD7700007777
        777777777F3F37770000444444077777FFF4444D0000DDDDD450777F3FF4DDDD
        0000DDDDD45507FFFFF4DDDD0000DDDDD45550FFFFF4DDDD0000DDD0045550FF
        FFF4DDDD0000DDD0A05550FFFFF4DDDD00000000EA0550FFFEF4DDDD00000EAE
        AEA050FFFFF4DDDD00000AEAEAEA00FEFEF4DDDD00000EAEAEA050FFFFF4DDDD
        00000000EA0550FEFEF4DDDD0000DDD0A05550EFEFE4DDDD0000DDD0045550FE
        FEF4DDDD0000DDDDD45550EFEFE4DDDD0000DDDDD44444444444DDDD0000DDDD
        DDDDDDDDDDDDDDDD0000}
      Hint = 'Fechar|'
      Spacing = 3
      Left = 134
      Top = 3
      Visible = True
      OnClick = ExitBtnClick
      SectionName = 'File'
    end
    object SpeedItem1: TJvSpeedItem
      BtnCaption = 'Salvar'
      Caption = 'Salvar Relat'#243'rio'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      Hint = 'Salvar Relat'#243'rio|'
      NumGlyphs = 2
      Spacing = 1
      Left = 8
      Top = 3
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'View'
    end
    object SpeedItem2: TJvSpeedItem
      BtnCaption = 'Imprimir'
      Caption = 'Imprimir Relat'#243'rio'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
        00033FFFFFFFFFFFFFFF0888888888888880777777777777777F088888888888
        8880777777777777777F0000000000000000FFFFFFFFFFFFFFFF0F8F8F8F8F8F
        8F80777777777777777F08F8F8F8F8F8F9F0777777777777777F0F8F8F8F8F8F
        8F807777777777777F7F0000000000000000777777777777777F3330FFFFFFFF
        03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
        03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
        33333337F3FF7F3733333330F08F0F0333333337F7737F7333333330FFFF0033
        33333337FFFF7733333333300000033333333337777773333333}
      Hint = 'Imprimir Relat'#243'rio|'
      NumGlyphs = 2
      Spacing = 1
      Left = 71
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'View'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 342
    Width = 680
    Height = 19
    Panels = <>
  end
  object ListBox1: TListBox
    Left = 0
    Top = 56
    Width = 680
    Height = 286
    Align = alClient
    Font.Charset = OEM_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Terminal'
    Font.Style = []
    ItemHeight = 12
    ParentFont = False
    TabOrder = 2
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Left = 200
    Top = 16
  end
  object PrintDialog1: TPrintDialog
    Left = 272
    Top = 24
  end
end
