object FExporta: TFExporta
  Left = 473
  Top = 230
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Exporta'#231#227'o para arquivo'
  ClientHeight = 236
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 464
    Height = 236
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 40
      Width = 89
      Height = 13
      Caption = 'Informe o caracter:'
    end
    object Bevel1: TBevel
      Left = 192
      Top = 9
      Width = 2
      Height = 42
    end
    object lb: TLabel
      Left = 216
      Top = 16
      Width = 89
      Height = 13
      Caption = 'M'#225'scara para data'
    end
    object CheckBox1: TCheckBox
      Left = 16
      Top = 16
      Width = 171
      Height = 17
      Caption = 'Delimitar campos por caracter'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object EdChar: TEdit
      Left = 112
      Top = 32
      Width = 17
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 1
      ParentFont = False
      TabOrder = 1
      Text = ';'
    end
    object RGDet: TRadioGroup
      Left = 16
      Top = 64
      Width = 273
      Height = 105
      Caption = 'Detalhes para valores tipo dinheiro.'
      ItemIndex = 0
      Items.Strings = (
        '2 casas decimais sem separa'#231#227'o'
        '2 casas decimais separadas por v'#237'rgula'
        '2 casas decimais separadas por ponto')
      TabOrder = 3
    end
    object CKAspas: TCheckBox
      Left = 16
      Top = 200
      Width = 193
      Height = 17
      Caption = 'Colocar campos texto entre aspas.'
      TabOrder = 7
    end
    object Button1: TButton
      Left = 328
      Top = 192
      Width = 121
      Height = 33
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 6
    end
    object Button2: TButton
      Left = 328
      Top = 152
      Width = 121
      Height = 33
      Caption = 'E&xportar'
      ModalResult = 1
      TabOrder = 4
    end
    object ckprimlinha: TCheckBox
      Left = 16
      Top = 176
      Width = 233
      Height = 17
      Caption = 'Mostrar nome do campo na primeira linha.'
      TabOrder = 5
    end
    object edmaskdata: TEdit
      Left = 216
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'dd/mm/aaaa'
    end
  end
end
