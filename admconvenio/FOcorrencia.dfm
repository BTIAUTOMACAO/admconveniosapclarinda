object FrmOcorrencia: TFrmOcorrencia
  Left = 401
  Top = 226
  BorderStyle = bsDialog
  Caption = 'Ocorr'#234'ncia'
  ClientHeight = 224
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 541
    Height = 224
    Align = alClient
    BevelWidth = 3
    Caption = 'Panel1'
    TabOrder = 0
    DesignSize = (
      541
      224)
    object Label1: TLabel
      Left = 8
      Top = 5
      Width = 95
      Height = 13
      Caption = 'Nome do Solicitante'
    end
    object Label2: TLabel
      Left = 8
      Top = 45
      Width = 155
      Height = 13
      Caption = 'Motivo/Descri'#231#227'o da Ocorr'#234'ncia'
    end
    object edtSolicitante: TEdit
      Left = 8
      Top = 21
      Width = 525
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object mmoMotivo: TMemo
      Left = 8
      Top = 61
      Width = 525
      Height = 131
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 8
      Top = 191
      Width = 524
      Height = 25
      Caption = '&Gravar'
      ModalResult = 1
      TabOrder = 2
    end
  end
end
