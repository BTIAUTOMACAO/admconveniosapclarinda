object FPesqConv: TFPesqConv
  Left = 189
  Top = 144
  Width = 758
  Height = 492
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Pesquisa de Conveniados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 742
    Height = 57
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 65
      Height = 13
      Caption = 'Digite o nome'
    end
    object lblStatus: TLabel
      Left = 416
      Top = 26
      Width = 3
      Height = 13
    end
    object Edit1: TEdit
      Left = 16
      Top = 24
      Width = 305
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnKeyDown = Edit1KeyDown
    end
    object Button1: TButton
      Left = 328
      Top = 21
      Width = 75
      Height = 25
      Caption = '&Pesquisar'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 57
    Width = 742
    Height = 378
    Align = alClient
    DataSource = DSConveniados
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'conv_id'
        Title.Caption = 'Conv ID'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'convlib'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'Conv.Lib'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Title.Caption = 'Titular'
        Width = 253
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'chapa'
        Title.Caption = 'Chapa'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'codigo'
        Title.Caption = 'Cart'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'digito'
        Title.Caption = 'Dig'
        Width = 24
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'codcartimp'
        Title.Caption = 'C'#243'd. Cart. Imp.'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cartlib'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'Cart.Lib'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'
        Title.Caption = 'Nome no Cart'#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'empresa'
        Title.Caption = 'Empresa'
        Visible = True
      end>
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 435
    Width = 742
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object DSConveniados: TDataSource
    DataSet = QConveniados
    Left = 32
    Top = 272
  end
  object QConveniados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        ' Select conveniados.titular, cartoes.codcartimp, conveniados.cha' +
        'pa, conveniados.conv_id,'
      
        ' cartoes.cartao_id, cartoes.codigo, cartoes.digito, cartoes.nome' +
        ','
      
        ' cartoes.liberado as cartlib, conveniados.liberado as convlib, c' +
        'onveniados.senha,'
      ' empresas.nome as empresa'
      ' from conveniados'
      ' join cartoes on conveniados.conv_id = cartoes.conv_id'
      ' join empresas on empresas.empres_id = conveniados.empres_id')
    Left = 32
    Top = 240
    object QConveniadostitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QConveniadoscodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object QConveniadoschapa: TFloatField
      FieldName = 'chapa'
    end
    object QConveniadosconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QConveniadoscartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object QConveniadoscodigo: TIntegerField
      FieldName = 'codigo'
    end
    object QConveniadosdigito: TWordField
      FieldName = 'digito'
    end
    object QConveniadosnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QConveniadoscartlib: TStringField
      FieldName = 'cartlib'
      FixedChar = True
      Size = 1
    end
    object QConveniadosconvlib: TStringField
      FieldName = 'convlib'
      FixedChar = True
      Size = 1
    end
    object QConveniadossenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
    object QConveniadosempresa: TStringField
      FieldName = 'empresa'
      Size = 60
    end
  end
end
