{** @abstract(Unit que contem todas as regras de neg�cio da
              calculadora)
    @author(Loiola, C. Emiliano <c_emiliano@uol.com.br>)
    @created(10 de junho de 2004)
    @lastmod(15 de junho de 2004)
}
unit calculadora;

interface

Uses Dialogs, SysUtils, Math;

Type
{** @abstract(Implementa as estrutura de armazenamento de vari�veis.)
  Este record armazena uma vari�vel identificada por ID e possui um valor Val.}
 TVariavel= record
  {** @name - Nome da vari�vel.}
  ID: String[1];
  {** @name - Valor da vari�vel.}
  Val: Double;
 end;

Type
{** @abstract(Classe que implementa um analisador sint�tico de
              express�es aritm�ticas - Calculadora)
    @author(Loiola, C. Emiliano <c_emiliano@uol.com.br>)
    @created(10 de junho de 2004)
    @lastmod(15 de junho de 2004)
    Implementa as opera��es b�sicas de soma, subtra��o, multiplica��o, divis�o e
    potencia��o. Al�m de fornecer a aplica��o de N vari�veis. Trata tamb�m a preced�ncia
    de opera��es com o usu de parenteses.
}

  TCalculadora= class

  private
    {** @name vari�vel respons�vel pela indexa��o do buffer de leitura.}
    I:Integer;
    {** @name Buffer que armazena as produ��es geradas pela express�o avaliada.}
    buffer: String[100];
    {** @name Vetor que armazena as vari�veis que s�o usadas nas express�es, criadas pelo usu�rio}
    TVetor : Array [0..9] of TVariavel;
    {** @name Controla a quantidade de vari�vel criada pelo usu�rio.}
    Quantidade_Variaveis: integer;
    {** @name Valor da express�o avaliada.}
    Resultado: Double;
    {** @name Flag setado quando ocorrer um erro na express�o.}
    Err: Boolean;
    {** @name Tamanho da express�o, expresso pela quantidade de caracter que a mesma possui.}
    SizeBuffer: integer;
    {** avan�a um caracter na express�o.}
    procedure Avance;
    {** Avalia se um carater � um digito v�lido entre zero e nove.}
    function IsDigit(Str: String):Boolean;
    {** Avalia se um caracter � um alfanumerioc v�lido entre "a" e "z" ou "A" e "Z".}
    function IsAlpha(Str: String):Boolean;
    {** Avalia se uma express�o � potencia��o.}
    function Potencia:double;
    {** Avalia se uma express�o � um identificador.}
    function Identificador: String;
    {** Avalia uma express�o.}
    function Expressao: Double;
    {** Avalia um fator.}
    function Fator: Double;
    {** Avalia um termo.}
    function Termo: Double;
    {** Avalia se uma express�o � um n�mero.}
    function Numero: Double;
    {** Avalia se uma express�o � uma atribui��o de vari�vel.}
    function Atribuicao: Double;
    {** procedimento de erro.}
    procedure Erro;
    {** busca uma vari�vel no vetor de vari�vel.
        @link(TVetor)}
    function BuscaVariavel(v: String):Boolean;
  public
    {** Construtor da classe calculadora.}
    constructor Create;
    {** recebe uma express�o a ser calculada.}
    procedure Calcular(exp: String);
    {** Limpa os atributos da calculadora o vetor de vari�vel.
        @link(TVetor)}
    procedure Clear;
    {** Fun��o de retorno do valor da express�o em String.}
    function ResultStr:String;

  end;


implementation

{ TCalculadora }

function TCalculadora.Atribuicao: Double;
Var
 At: integer;
 v: Double;
begin
v:=0;
for At:=0 to Quantidade_Variaveis - 1 do
  if (buffer[I] = TVetor[At].ID) then
  begin
    Avance;
    If buffer[I] = '=' then
    begin
    Avance;
    TVetor[At].Val := Fator;
    end
    else
    v := TVetor[At].Val;
  end;

  Result:= v;

end;

procedure TCalculadora.Avance;
begin
  Inc(I);
  While (buffer[I] = ' ') do
  Inc(I) ;
end;

function TCalculadora.BuscaVariavel(v: String): Boolean;
Var
 I: Integer;
 Retorno: Boolean;
begin
Retorno:= False;
For I:=0 to Quantidade_Variaveis -1 do
 if TVetor[I].ID = v then
 begin
   Retorno := True;
   break;
 end
 else
   Retorno := False;

Result := Retorno;

end;

procedure TCalculadora.Calcular(exp: String);
Var
 temp: integer;
begin
I:=1;
For temp:=0 to 99 do
 buffer[Temp] := #0;

Resultado := 0.0;
Err:= False;
SizeBuffer := 0;
SizeBuffer := Length(exp);

buffer := exp;

If (buffer[I] = '#') then
begin
  Avance;
  If not BuscaVariavel(buffer[I]) then begin
  TVetor[Quantidade_Variaveis].ID  := Identificador;
  TVetor[Quantidade_Variaveis].Val := Expressao;
  Inc(Quantidade_Variaveis);
  end
  else ShowMessage('Variavel j� existe.')
end
else
  Resultado := Expressao;

end;

procedure TCalculadora.Clear;
begin
  I:=1;
  buffer := '';
  Quantidade_Variaveis:=0;
  Resultado := 0.0;
  Err:= False;
end;

constructor TCalculadora.Create;
begin
  I:=1;
  buffer := '';
  Quantidade_Variaveis:=0;
  Resultado := 0.0;
  Err:= False;
end;

procedure TCalculadora.Erro;
begin
 Err := True;
end;

function TCalculadora.Expressao: Double;
Var
 v: Double;
begin

 v := Termo; 
 While (buffer[I] = '+') do
 begin
  Avance;
  v := v + Expressao;
 end;
 While (buffer[I] = '-') do
 begin
  Avance;
  v := v - Expressao;
 end;

 Result := v ;
end;

function TCalculadora.Fator: Double;
Var
 v: double;
begin
 v:=0;
 if IsDigit(buffer[I]) or (buffer[I] = '.') then
   v := Numero
 else if IsAlpha(buffer[I]) then
   v := Atribuicao
 else if (buffer[I] = '-') then
 begin
   Avance;
   v:= Fator * (-1);
 end
 else
   if (buffer[I]='(') then
   begin
    Avance;
    v:= Expressao;
    if (buffer[I] <> ')') then
      Erro;
    Avance;
   end
   else
     Erro;


Result := v;
end;

function TCalculadora.Identificador: String;
Var
 c: String[1];
begin

  if (IsAlpha(buffer[I])) then
  begin
    c := buffer[I];
    Avance;
    if (buffer[I] <> '=') then
      Erro;
    Avance;
  end;
Result := c;
end;

function TCalculadora.IsAlpha(Str: String): Boolean;
begin
  if (Str[1] in ['a'..'z']) OR (Str[1] in ['A'..'Z']) then
    Result := True
  else
    Result := False;
end;

function TCalculadora.IsDigit(Str: String): Boolean;
begin
  if (Str[1] in ['0'..'9']) then
   Result:= True
  else
   Result:=False;
end;

function TCalculadora.Numero: Double;
Var
 v: Double; //valor de retorno
 ni: Double; //Parte inteira do numero
 nf: Double; //Valor do ponto flutuante
 pf: Integer; //conta n�mero de digitos do ponto flututante
begin
 v:=0;
 ni:= 0;
 nf:=0;
 pf:=0;
 While (IsDigit(buffer[I])) and (buffer[I] <> '.') do
 begin
 ni := ni * 10 + StrToInt(buffer[I]);
 v:= ni;
 Avance;
 end;
 if (buffer[I] = '.') then
 begin
 avance;
  while (IsDigit(buffer[I])) do
  begin
   nf := nf * 10 + StrToInt(buffer[I]);
   inc(pf);
   avance;        
  end;
 v := ni + (nf /(Power(10,pf)));
 end;

Result := v;
end;

function TCalculadora.Potencia: double;
Var
 pot,x,t,v: Double;

begin

  v:= Fator;
  x:=1;
  pot := v;
  While (buffer[I] = '^') do
  begin
   Avance;
   t := Fator;
   If buffer[I] <> '^' then
   while (x<t) do
   begin
    pot := pot * v;
    x := x+1;
   end
   else
   Erro;

  end;


Result := pot;
end;

function TCalculadora.ResultStr: String;
var v: String;
begin
  If Err or (SizeBuffer <> I - 1) then
    v:= 'Erro!'
  else
    v := FloatToStr(Resultado);
  Result:= v;
end;

function TCalculadora.Termo: Double;
Var
 v: Double;
begin
  v := Potencia;
  while (buffer[I] = '*') do begin
    Avance;
    v := v * Fator;
  end;
  While (buffer[I] = '/') do begin
    Avance;
    try
      v := v / Fator;
    except
    end;
  end;
  Result := v;
end;

end.
