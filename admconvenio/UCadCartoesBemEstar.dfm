inherited FCadCartoesBemEstar: TFCadCartoesBemEstar
  Left = 142
  Top = 113
  Caption = 'Cadastro de Cart'#245'es Bem Estar'
  ClientHeight = 550
  ClientWidth = 1028
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 1028
    Height = 509
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 390
        Width = 1020
        inherited Label1: TLabel
          Left = 4
          Top = 4
          Width = 39
          Caption = 'Conv &ID'
        end
        inherited Label2: TLabel
          Left = 55
          Top = 4
          Width = 29
          Caption = '&Titular'
        end
        object Label16: TLabel [2]
          Left = 236
          Top = 5
          Width = 74
          Height = 13
          Caption = '&N'#186' Cart'#227'o e Dig'
          FocusControl = EdCartao
        end
        object Label17: TLabel [3]
          Left = 316
          Top = 5
          Width = 77
          Height = 13
          Caption = '&Nome do Cart'#227'o'
          FocusControl = EdNomeCart
        end
        object Label18: TLabel [4]
          Left = 497
          Top = 5
          Width = 49
          Height = 13
          Caption = '&Empres ID'
          FocusControl = EdEmpID
        end
        object Label19: TLabel [5]
          Left = 552
          Top = 5
          Width = 87
          Height = 13
          Caption = 'N&ome da Empresa'
          FocusControl = EdNomeEmp
        end
        inherited EdNome: TEdit
          Left = 53
          Top = 21
          Width = 179
          ParentShowHint = False
        end
        inherited ButBusca: TBitBtn
          Left = 688
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 1259
          TabOrder = 9
        end
        inherited EdCod: TEdit
          Left = 4
          Top = 21
          Width = 45
          CharCase = ecUpperCase
          ParentShowHint = False
        end
        inherited ButFiltro: TBitBtn
          Left = 225
          TabOrder = 7
        end
        inherited ButAltLin: TButton
          Left = 316
          TabOrder = 8
        end
        object EdNomeCart: TEdit
          Left = 315
          Top = 21
          Width = 179
          Height = 21
          CharCase = ecUpperCase
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnKeyPress = EdNomeKeyPress
        end
        object EdCartao: TEdit
          Left = 236
          Top = 21
          Width = 74
          Height = 21
          Hint = 
            'Digite o numero de cart'#227'o e o digito, ou n'#186' de cart'#227'o de importa' +
            #231#227'o para pesquisa.'
          CharCase = ecUpperCase
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdEmpID: TEdit
          Left = 497
          Top = 21
          Width = 50
          Height = 21
          CharCase = ecUpperCase
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdNomeEmp: TEdit
          Left = 551
          Top = 21
          Width = 133
          Height = 21
          CharCase = ecUpperCase
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnKeyPress = EdNomeKeyPress
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 1020
        Height = 390
        OnEditButtonClick = DBGrid1EditButtonClick
        OnExit = TabFichaExit
        Columns = <
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'CONV_ID'
            ReadOnly = False
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CARTAO_ID'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 233
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            PickList.Strings = (
              'S'
              'N')
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TITULAR'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODIGO'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DIGITO'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETITULAR'
            Width = 205
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_MES'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JAEMITIDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODCARTIMP'
            Width = 109
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARENTESCO'
            Width = 122
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_DEP'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_NASC'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Width = 98
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 462
        Width = 1020
      end
    end
    inherited TabFicha: TTabSheet
      OnExit = TabFichaExit
      inherited Panel2: TPanel
        Top = 449
        Width = 1020
        inherited btnFirstB: TSpeedButton
          Left = 584
        end
        inherited btnPriorB: TSpeedButton
          Left = 617
        end
        inherited btnNextB: TSpeedButton
          Left = 650
        end
        inherited btnLastB: TSpeedButton
          Left = 683
        end
        inherited ButGrava: TBitBtn
          Left = 728
          Width = 83
          TabOrder = 5
        end
        inherited ButCancela: TBitBtn
          Left = 812
          TabOrder = 6
        end
        object ButAtualCodImp: TBitBtn
          Left = 432
          Top = 4
          Width = 141
          Height = 25
          Caption = 'Atualizar Cod. Import.'
          TabOrder = 4
          Visible = False
          OnClick = ButAtualCodImpClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000CDD3D5CDD3D5
            B69290BA9391B99290B99290B99291B89291B89291B89291B89292B89292B892
            93BB9492A67F7BCDD3D5CDD3D5B5B5B5B7B7B7B6B6B6B6B6B6B7B7B7B6B6B6B6
            B6B6B6B6B6B7B7B7B7B7B7B7B7B7B8B8B8A6A6A6CDD3D5CDD3D5CDD3D5CDD3D5
            B7928DF6D6CCFAD3C4FBD1C4F9CFC0F8CDBDF9CCBBF8CBB8F6C8B5F6C6B2F5C4
            AFF8CBB2B38279CDD3D5CDD3D5B4B4B4E7E7E7E5E5E5E6E6E6E3E3E3E1E1E1E1
            E1E1DFDFDFDEDEDEDCDCDCDBDBDBDDDDDDABABABCDD3D5CDD3D5CDD3D5CDD3D5
            B79B95F6EDDDFFEFD6F2E4C3FFE7CBFFE8CCFEE1BCFFE0BCFFDFBAFFD9ACFFD6
            A5FFDBB3B88886CDD3D5CDD3D5B7B7B7EEEEEEEEEEEEE2E2E2EAEAEAEAEAEAE3
            E3E3E4E4E4E3E3E3DEDEDEDBDBDBE0E0E0B2B2B2CDD3D5CDD3D5CDD3D5CDD3D5
            BB9D97F6EDE0FFF3E967B956AECC8AA0C98051AE3D6EB651C6CC8EFFDCB9FFD7
            ACFFDBB8B88885CDD3D5CDD3D5BABABAEFEFEFF6F6F69F9F9FBBBBBBB6B6B691
            91919C9C9CBDBDBDE3E3E3DEDEDEE2E2E2B1B1B1CDD3D5CDD3D5CDD3D5CDD3D5
            BFA09AF7F0E6FFF6EF6EBD5E149B10009900009500009400039902B5C882FFDC
            B7FFDDBCB98A86CDD3D5CDD3D5BDBDBDF2F2F2F8F8F8A4A4A47777777070706E
            6E6E6E6E6E717171B7B7B7E2E2E2E4E4E4B2B2B2CDD3D5CDD3D5CDD3D5CDD3D5
            C3A59DF8F3EBFFF9F56EBE6100910000960053B243B4CE9154B1402FA522EDD7
            A9FFE1C4BB8B89CDD3D5CDD3D5C0C0C0F4F4F4FBFBFBA6A6A66D6D6D6F6F6F95
            9595BFBFBF939393828282D5D5D5E7E7E7B5B5B5CDD3D5CDD3D5CDD3D5CDD3D5
            C7A89FF9F6F0FFFCFB72C16703930203990334A62BA7CB88FFEADD7FBD63AEC9
            80FFE4CCBD8D8BCDD3D5CDD3D5C2C2C2F6F6F6FDFDFDA9A9A96F6F6F71717186
            8686BABABAF1F1F1A6A6A6B6B6B6EAEAEAB6B6B6CDD3D5CDD3D5CDD3D5CDD3D5
            CDADA1FBFAF8F5F4E5E9ECD5EFEEDAEFEAD4F7E6CCFFE7CFFFEDE0FFE7D3EFDE
            B8FFE3C8BE908CCDD3D5CDD3D5C5C5C5FAFAFAF0F0F0E6E6E6E9E9E9E7E7E7E7
            E7E7ECECECF2F2F2EDEDEDDCDCDCE9E9E9B7B7B7CDD3D5CDD3D5CDD3D5CDD3D5
            D2B2A3FFFDFFDAECCFA6D69AFFF8F3FFF5F083C36E68B75468B5539CC77CFFE9
            D2FFE5CDC1928FCDD3D5CDD3D5C8C8C8FEFEFEE4E4E4C6C6C6FAFAFAF9F9F9AD
            ADAD9D9D9D9C9C9CB4B4B4EDEDEDEBEBEBB9B9B9CDD3D5CDD3D5CDD3D5CDD3D5
            D6B7A6FEFFFFFCFDFA60BE5D98D08CFFF8F2CEE1B90B9C0B008C004BB241FFEF
            E2FFE5D0C19290CDD3D5CDD3D5CBCBCBFEFEFEFCFCFCA4A4A4BEBEBEFAFAFAD7
            D7D77676766B6B6B949494F3F3F3ECECECB9B9B9CDD3D5CDD3D5CDD3D5CDD3D5
            DABBA9FDFFFFFFFFFFBDE2B900980055B64D71C06617A01400910057B54AFFF4
            E7FFE5D2C48F8CCDD3D5CDD3D5CDCDCDFEFEFEFFFFFFD7D7D77070709B9B9BA9
            A9A97B7B7B6D6D6D999999F5F5F5EDEDEDB9B9B9CDD3D5CDD3D5CDD3D5CDD3D5
            E0BFABFEFFFFFFFFFFFFFFFF8CD08A02970200910000970019A2174EB142FFD6
            D9FFB6B2C58684CDD3D5CDD3D5D1D1D1FEFEFEFFFFFFFFFFFFBDBDBD7070706D
            6D6D6F6F6F7D7D7D949494EEEEEEE0E0E0B7B7B7CDD3D5CDD3D5CDD3D5CDD3D5
            E5C3AEFFFFFFFFFFFFFFFFFFFFFFFFD3E9CD92CF8AA4D597E1EDCF94B979D394
            90CF8D82B17C75CDD3D5CDD3D5D4D4D4FFFFFFFFFFFFFFFFFFFFFFFFE2E2E2BD
            BDBDC4C4C4E5E5E5ADADADC1C1C1BABABAA9A9A9CDD3D5CDD3D5CDD3D5CDD3D5
            E9C8B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBECCFD0CE8F
            5AD2986BC0A298CDD3D5CDD3D5D6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFDFDFDE4E4E4A9A9A9B2B2B2BDBDBDCDD3D5CDD3D5CDD3D5CDD3D5
            EECEB4FFFFFFFFFFFFFFFFFFFEFFFFFEFFFFFCFFFFFBFDFDFCFFFFDECDC9CE92
            6FD4B39DCACFD2CDD3D5CDD3D5DADADAFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFE
            FEFEFCFCFCFEFEFEDCDCDCB2B2B2C7C7C7D8D8D8CDD3D5CDD3D5CDD3D5CDD3D5
            EBC09EE7C4ADE7C4ADE5C2ABE5C2ABE4C1AAE3C0A9E2BFA8E5C2A9D0A694C6A5
            99CDD3D5CAD3D6CDD3D5CDD3D5D0D0D0D4D4D4D4D4D4D3D3D3D3D3D3D2D2D2D1
            D1D1D0D0D0D2D2D2C1C1C1BFBFBFDADADAD9D9D9CDD3D5CDD3D5}
          NumGlyphs = 2
        end
        object ButLimpaSenha: TBitBtn
          Left = 257
          Top = 4
          Width = 100
          Height = 25
          Caption = 'Limpar Senha'
          TabOrder = 3
          OnClick = ButLimpaSenhaClick
          Glyph.Data = {
            96050000424D9605000000000000960300002800000020000000100000000100
            08000000000000020000232E0000232E0000D800000000000000FFFFFF00CCFF
            FF0099FFFF0066FFFF0033FFFF0000FFFF00FFCCFF00CCCCFF0099CCFF0066CC
            FF0033CCFF0000CCFF00FF99FF00CC99FF009999FF006699FF003399FF000099
            FF00FF66FF00CC66FF009966FF006666FF003366FF000066FF00FF33FF00CC33
            FF009933FF006633FF003333FF000033FF00FF00FF00CC00FF009900FF006600
            FF003300FF000000FF00FFFFCC00CCFFCC0099FFCC0066FFCC0033FFCC0000FF
            CC00FFCCCC00CCCCCC0099CCCC0066CCCC0033CCCC0000CCCC00FF99CC00CC99
            CC009999CC006699CC003399CC000099CC00FF66CC00CC66CC009966CC006666
            CC003366CC000066CC00FF33CC00CC33CC009933CC006633CC003333CC000033
            CC00FF00CC00CC00CC009900CC006600CC003300CC000000CC00FFFF9900CCFF
            990099FF990066FF990033FF990000FF9900FFCC9900CCCC990099CC990066CC
            990033CC990000CC9900FF999900CC9999009999990066999900339999000099
            9900FF669900CC66990099669900666699003366990000669900FF339900CC33
            990099339900663399003333990000339900FF009900CC009900990099006600
            99003300990000009900FFFF6600CCFF660099FF660066FF660033FF660000FF
            6600FFCC6600CCCC660099CC660066CC660033CC660000CC6600FF996600CC99
            660099996600669966003399660000996600FF666600CC666600996666006666
            66003366660000666600FF336600CC3366009933660066336600333366000033
            6600FF006600CC00660099006600660066003300660000006600FFFF3300CCFF
            330099FF330066FF330033FF330000FF3300FFCC3300CCCC330099CC330066CC
            330033CC330000CC3300FF993300CC9933009999330066993300339933000099
            3300FF663300CC66330099663300666633003366330000663300FF333300CC33
            330099333300663333003333330000333300FF003300CC003300990033006600
            33003300330000003300FFFF0000CCFF000099FF000066FF000033FF000000FF
            0000FFCC0000CCCC000099CC000066CC000033CC000000CC0000FF990000CC99
            000099990000669900003399000000990000FF660000CC660000996600006666
            00003366000000660000FF330000CC3300009933000066330000333300000033
            0000FF000000CC00000099000000660000003300000000000000002D092D0000
            00000000000000000000012B072B0000000000000000000000002D35350B5F00
            000000000000000000002B2B2B2B2B000000000000000000000009033B350B5F
            0000000000000000000007002B2B072B000000000000000000002D010B5F350B
            5F0000000000000000000700002B2B2B2B0000000000000000000035010A3B35
            04350000000000000000002B00002B2B072B0000000000000000000035010A5F
            350A352D00000000000000002B00002B2B012B010000000000000000340A0909
            3B350A353B5F5F3400000000070000002B2B012B2B2B2B2B0000000035013501
            09350909090909345F0000002B002B00002B00000000002B2B00000009350935
            01030203020302030A340000012B012B0000010000000100072B000000000009
            340102020101010102350000000000012B00000000000000002B000000000000
            350102020335350902350000000000002B000000012B2B00002B000000000000
            350101013500013501350000000000002B0000002B00002B002B000000000000
            3501010135071101092E0000000000002B0000002B002B00012B000000000000
            2E0901012D5F010235000000000000002B0000002B2B00002B00000000000000
            00350901010109350000000000000000002B01000000012B0000000000000000
            00000A3535350A00000000000000000000002B2B2B2B2B000000}
          NumGlyphs = 2
        end
      end
      inherited Panel3: TPanel
        Width = 1020
        Height = 449
        object GroupBox1: TGroupBox
          Left = 2
          Top = 367
          Width = 1016
          Height = 80
          Align = alBottom
          Caption = 'Dados da '#218'ltima altera'#231#227'o'
          TabOrder = 1
          object Label10: TLabel
            Left = 254
            Top = 28
            Width = 71
            Height = 13
            Caption = 'Data Altera'#231#227'o'
            FocusControl = DBEdit8
          end
          object Label11: TLabel
            Left = 38
            Top = 28
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit9
          end
          object DBEdit8: TDBEdit
            Left = 254
            Top = 44
            Width = 81
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 38
            Top = 44
            Width = 193
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
          object Button1: TButton
            Left = 464
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Cadastrar'
            TabOrder = 0
            Visible = False
            OnClick = Button1Click
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 2
          Width = 1016
          Height = 365
          Align = alClient
          Caption = 'Dados do Cart'#227'o'
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 15
            Width = 45
            Height = 13
            Caption = 'Cart'#227'o ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 10
            Top = 55
            Width = 39
            Height = 13
            Caption = 'Conv ID'
            FocusControl = DBEdit2
          end
          object Label5: TLabel
            Left = 73
            Top = 15
            Width = 77
            Height = 13
            Caption = 'Nome do Cart'#227'o'
            FocusControl = DBEdit3
          end
          object Label6: TLabel
            Left = 378
            Top = 15
            Width = 46
            Height = 13
            Caption = 'N'#186' Cart'#227'o'
            FocusControl = DBEdit4
          end
          object Label7: TLabel
            Left = 520
            Top = 15
            Width = 19
            Height = 13
            Caption = 'Dig.'
            FocusControl = DBEdit5
          end
          object Label8: TLabel
            Left = 10
            Top = 95
            Width = 50
            Height = 13
            Caption = 'Limite M'#234's'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 387
            Top = 55
            Width = 78
            Height = 13
            Caption = 'C'#243'd. Importa'#231#227'o'
            FocusControl = DBEdit10
          end
          object Label14: TLabel
            Left = 98
            Top = 95
            Width = 54
            Height = 13
            Caption = 'Parentesco'
            FocusControl = DBEdit12
          end
          object Label15: TLabel
            Left = 451
            Top = 95
            Width = 82
            Height = 13
            Caption = 'Data Nascimento'
            FocusControl = DBEdit13
          end
          object Label13: TLabel
            Left = 99
            Top = 55
            Width = 75
            Height = 13
            Caption = 'Nome do Titular'
          end
          object ButPesq: TSpeedButton
            Left = 68
            Top = 70
            Width = 23
            Height = 22
            Caption = '...'
            Flat = True
            OnClick = ButPesqClick
          end
          object Label25: TLabel
            Left = 399
            Top = 95
            Width = 38
            Height = 13
            Caption = 'N'#186' Dep.'
            FocusControl = DBEdit14
          end
          object Label9: TLabel
            Left = 122
            Top = 135
            Width = 41
            Height = 13
            Caption = 'Liberado'
          end
          object Label26: TLabel
            Left = 210
            Top = 135
            Width = 20
            Height = 13
            Caption = 'CPF'
            FocusControl = DBEdit7
          end
          object Label27: TLabel
            Left = 346
            Top = 135
            Width = 16
            Height = 13
            Caption = 'RG'
            FocusControl = DBEdit15
          end
          object DBEdit1: TDBEdit
            Left = 10
            Top = 30
            Width = 57
            Height = 21
            TabStop = False
            CharCase = ecUpperCase
            DataField = 'CARTAO_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 10
            Top = 70
            Width = 57
            Height = 21
            CharCase = ecUpperCase
            DataField = 'CONV_ID'
            DataSource = DSCadastro
            TabOrder = 4
            OnExit = DBEdit2Exit
            OnKeyDown = DBEdit2KeyDown
          end
          object DBEdit3: TDBEdit
            Left = 74
            Top = 30
            Width = 297
            Height = 21
            CharCase = ecUpperCase
            DataField = 'NOME'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 378
            Top = 30
            Width = 133
            Height = 21
            TabStop = False
            CharCase = ecUpperCase
            DataField = 'CODIGO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 519
            Top = 30
            Width = 25
            Height = 21
            TabStop = False
            CharCase = ecUpperCase
            DataField = 'DIGITO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 3
          end
          object DBEdit6: TDBEdit
            Left = 10
            Top = 110
            Width = 79
            Height = 21
            CharCase = ecUpperCase
            DataField = 'LIMITE_MES'
            DataSource = DSCadastro
            TabOrder = 7
          end
          object DBEdit10: TDBEdit
            Left = 386
            Top = 70
            Width = 159
            Height = 21
            CharCase = ecUpperCase
            DataField = 'CODCARTIMP'
            DataSource = DSCadastro
            TabOrder = 6
          end
          object DBEdit12: TDBEdit
            Left = 97
            Top = 110
            Width = 295
            Height = 21
            CharCase = ecUpperCase
            DataField = 'PARENTESCO'
            DataSource = DSCadastro
            TabOrder = 8
          end
          object DBEdit13: TDBEdit
            Left = 451
            Top = 110
            Width = 93
            Height = 21
            CharCase = ecUpperCase
            DataField = 'DATA_NASC'
            DataSource = DSCadastro
            TabOrder = 10
          end
          object DBCheckBox1: TDBCheckBox
            Left = 10
            Top = 148
            Width = 97
            Height = 17
            Caption = 'Cart'#227'o de Titular'
            DataField = 'TITULAR'
            DataSource = DSCadastro
            TabOrder = 11
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object DBEdit11: TDBEdit
            Left = 98
            Top = 70
            Width = 281
            Height = 21
            TabStop = False
            CharCase = ecUpperCase
            DataField = 'NOMETITULAR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 5
          end
          object DBEdit14: TDBEdit
            Left = 399
            Top = 110
            Width = 44
            Height = 21
            CharCase = ecUpperCase
            DataField = 'NUM_DEP'
            DataSource = DSCadastro
            TabOrder = 9
          end
          object JvDBComboBox1: TJvDBComboBox
            Left = 122
            Top = 150
            Width = 81
            Height = 21
            DataField = 'LIBERADO'
            DataSource = DSCadastro
            Items.Strings = (
              'Sim'
              'N'#227'o '
              'Inativo')
            TabOrder = 12
            Values.Strings = (
              'S'
              'N'
              'I')
            ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
            ListSettings.OutfilteredValueFont.Color = clRed
            ListSettings.OutfilteredValueFont.Height = -11
            ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
            ListSettings.OutfilteredValueFont.Style = []
          end
          object DBEdit7: TDBEdit
            Left = 210
            Top = 150
            Width = 129
            Height = 21
            DataField = 'CPF'
            DataSource = DSCadastro
            TabOrder = 13
          end
          object DBEdit15: TDBEdit
            Left = 346
            Top = 150
            Width = 113
            Height = 21
            DataField = 'RG'
            DataSource = DSCadastro
            TabOrder = 14
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 176
            Width = 489
            Height = 177
            DataSource = dExcel
            TabOrder = 15
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
          end
        end
      end
    end
    object TabCC: TTabSheet [2]
      Caption = 'Conta Corrente (F12)'
      ImageIndex = 3
      OnShow = TabCCShow
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 94
        Width = 1020
        Height = 387
        Align = alClient
        DataSource = DSContaCorrente
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = PopupCOntaC
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = JvDBGrid2TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'DATA'
            Title.Caption = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AUTORIZACAO_ID'
            Title.Caption = 'Autoriza'#231#227'o'
            Width = 79
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DIGITO'
            Title.Caption = 'Dig'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HORA'
            Title.Caption = 'Hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DEBITO'
            Title.Caption = 'Debito'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDITO'
            Title.Caption = 'Cr'#233'dito'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CREDENCIADO'
            Title.Caption = 'Estabelecimento'
            Width = 284
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_FECHA_EMP'
            Title.Caption = 'Data Fechamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NF'
            Title.Caption = 'Num. NF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RECEITA'
            Title.Caption = 'Receita'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HISTORICO'
            Title.Caption = 'Hist'#243'rico'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Title.Caption = 'Operador'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_CANCELADO'
            Title.Caption = 'Valor Cancelado'
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1020
        Height = 94
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Label20: TLabel
          Left = 50
          Top = 15
          Width = 80
          Height = 13
          Caption = 'Nome do Cart'#227'o:'
        end
        object Label21: TLabel
          Left = 50
          Top = 34
          Width = 64
          Height = 13
          Caption = 'N'#186' do Cart'#227'o:'
        end
        object Label22: TLabel
          Left = 50
          Top = 53
          Width = 78
          Height = 13
          Caption = 'C'#243'd. Importa'#231#227'o'
        end
        object dbnome: TDBText
          Left = 144
          Top = 15
          Width = 45
          Height = 13
          AutoSize = True
          DataField = 'NOME'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object codigocar: TDBText
          Left = 144
          Top = 34
          Width = 57
          Height = 13
          AutoSize = True
          DataField = 'CODIGO'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DG: TDBText
          Left = 221
          Top = 34
          Width = 19
          Height = 13
          AutoSize = True
          DataField = 'DIGITO'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBText4: TDBText
          Left = 144
          Top = 53
          Width = 50
          Height = 13
          AutoSize = True
          DataField = 'CODCARTIMP'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label23: TLabel
          Left = 7
          Top = 75
          Width = 159
          Height = 13
          Caption = 'Autoriza'#231#245'es em aberto do cart'#227'o'
        end
        object Label24: TLabel
          Left = 213
          Top = 34
          Width = 5
          Height = 13
          Caption = '-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object TabSaldos: TTabSheet [3]
      Caption = '&Saldo Cart'#227'o/Conveniado'
      ImageIndex = 4
      OnShow = TabSaldosShow
      object Splitter1: TSplitter
        Left = 0
        Top = 49
        Width = 1020
        Height = 2
        Cursor = crVSplit
        Align = alTop
      end
      object Panel10: TPanel
        Left = 0
        Top = 601
        Width = 1020
        Height = 41
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 5
        object Label62: TLabel
          Left = 43
          Top = 14
          Width = 119
          Height = 13
          Caption = 'Fechamento selecionado'
        end
        object Panel16: TPanel
          Left = 23
          Top = 13
          Width = 17
          Height = 14
          BorderStyle = bsSingle
          Color = 12582911
          TabOrder = 0
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 1020
        Height = 49
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Button2: TButton
          Left = 16
          Top = 9
          Width = 75
          Height = 25
          Caption = '&Atualizar'
          TabOrder = 0
          OnClick = Button2Click
        end
      end
      object Panel12: TPanel
        Left = 0
        Top = 281
        Width = 1020
        Height = 18
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Saldo total do conveniado por fechamento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object GridSaldoConv: TDBGrid
        Left = 0
        Top = 69
        Width = 1020
        Height = 212
        Align = alTop
        DataSource = DSSaldoConv
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = GridSaldoConvCellClick
        OnDrawColumnCell = GridSaldoConvDrawColumnCell
        OnTitleClick = GridSaldoConvTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'FECHAMENTO'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'saldo_nconf'
            Title.Caption = 'Saldo N'#227'o Confirmado'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'saldo_conf'
            Title.Caption = 'Saldo Confirmado'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'total'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RESTANTE'
            Title.Caption = 'Restante'
            Visible = True
          end>
      end
      object Panel13: TPanel
        Left = 0
        Top = 51
        Width = 1020
        Height = 18
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 'Saldo separado por cart'#245'es e por fechamento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object GridSaldoCartao: TDBGrid
        Left = 0
        Top = 299
        Width = 1020
        Height = 302
        Align = alClient
        DataSource = DSSaldoCartao
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GridSaldoCartaoDrawColumnCell
        OnTitleClick = GridSaldoCartaoTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'FECHAMENTO'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'saldo_nconf'
            Title.Caption = 'Saldo N'#227'o Confirmado'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'saldo_conf'
            Title.Caption = 'Saldo Confirmado'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'total'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Visible = True
          end>
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 1020
      end
      inherited GridHistorico: TJvDBGrid
        Width = 1020
        Height = 434
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 1028
  end
  inherited panStatus2: TPanel
    Width = 1028
  end
  inherited DSCadastro: TDataSource
    Left = 636
    Top = 168
  end
  inherited PopupBut: TPopupMenu
    Left = 52
    Top = 296
  end
  inherited PopupGrid1: TPopupMenu
    Left = 108
    Top = 256
  end
  inherited QHistorico: TADOQuery
    Left = 628
    Top = 112
  end
  inherited DSHistorico: TDataSource
    Left = 668
    Top = 112
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterPost = QCadastroAfterPost
    OnPostError = QCadastroPostError
    SQL.Strings = (
      ' Select '
      'CARTOES_BEM_ESTAR.CARTAO_ID '
      '      , CARTOES_BEM_ESTAR.CONV_ID '
      '      , CARTOES_BEM_ESTAR.NOME '
      '      , CARTOES_BEM_ESTAR.LIBERADO '
      '      , CARTOES_BEM_ESTAR.CODIGO '
      '      , CARTOES_BEM_ESTAR.DIGITO '
      '      , CARTOES_BEM_ESTAR.DATA_NASC'
      '      , CARTOES_BEM_ESTAR.TITULAR '
      '      , CARTOES_BEM_ESTAR.JAEMITIDO '
      '      , CARTOES_BEM_ESTAR.APAGADO '
      '      , CARTOES_BEM_ESTAR.LIMITE_MES '
      '      , CARTOES_BEM_ESTAR.CODCARTIMP '
      '      , CARTOES_BEM_ESTAR.PARENTESCO '
      '       , CARTOES_BEM_ESTAR.NUM_DEP '
      '      , CARTOES_BEM_ESTAR.FLAG '
      '      , CARTOES_BEM_ESTAR.DTEMISSAO '
      '      , CARTOES_BEM_ESTAR.CPF '
      '      , CARTOES_BEM_ESTAR.RG '
      '      , CARTOES_BEM_ESTAR.VIA '
      '      , CARTOES_BEM_ESTAR.DTAPAGADO '
      '      , CARTOES_BEM_ESTAR.DTALTERACAO '
      '      , CARTOES_BEM_ESTAR.OPERADOR '
      '      , CARTOES_BEM_ESTAR.DTCADASTRO '
      '      , CARTOES_BEM_ESTAR.OPERCADASTRO '
      '      , CARTOES_BEM_ESTAR.CRED_ID '
      '      , CARTOES_BEM_ESTAR.ATIVO '
      '      , CONVENIADOS_BEM_ESTAR.titular as nometitular'
      '      , CARTOES_BEM_ESTAR.EMPRES_ID'
      '      , CARTOES_BEM_ESTAR.SENHA'
      ' from cartoes_bem_estar '
      
        'join conveniados_bem_estar on CONVENIADOS_BEM_ESTAR.conv_id = ca' +
        'rtoes_bem_estar.conv_id '
      'where cartao_id = 0')
    Left = 604
    Top = 168
    object QCadastroCONV_ID: TIntegerField
      DisplayLabel = 'Conv ID'
      FieldName = 'CONV_ID'
    end
    object QCadastroNOME: TStringField
      DisplayLabel = 'Nome do Cart'#227'o'
      FieldName = 'NOME'
      Size = 45
    end
    object QCadastroLIBERADO: TStringField
      DisplayLabel = 'Liberado'
      FieldName = 'LIBERADO'
      Size = 1
    end
    object QCadastroCODIGO: TIntegerField
      DisplayLabel = 'N'#186' Cart'#227'o'
      FieldName = 'CODIGO'
      Required = True
    end
    object QCadastroDIGITO: TSmallintField
      DisplayLabel = 'Digito'
      FieldName = 'DIGITO'
      DisplayFormat = '00'
    end
    object QCadastroLIMITE_MES: TFloatField
      DisplayLabel = 'Limite M'#234's'
      FieldName = 'LIMITE_MES'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '###,###,##0.00'
    end
    object QCadastroJAEMITIDO: TStringField
      DisplayLabel = 'J'#225' Emitido'
      FieldName = 'JAEMITIDO'
      Size = 1
    end
    object QCadastroDTALTERACAO: TDateTimeField
      DisplayLabel = 'Data de Altera'#231#227'o'
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
      EditMask = '!00/00/0000;0; '
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroCODCARTIMP: TStringField
      DisplayLabel = 'C'#243'digo de Importa'#231#227'o'
      DisplayWidth = 20
      FieldName = 'CODCARTIMP'
    end
    object QCadastroAPAGADO: TStringField
      DisplayLabel = 'Apagado'
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroTITULAR: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'TITULAR'
      Size = 1
    end
    object QCadastroPARENTESCO: TStringField
      DisplayLabel = 'Grau Parentesco'
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object QCadastroNUM_DEP: TIntegerField
      DisplayLabel = 'N'#186' Dep.'
      FieldName = 'NUM_DEP'
    end
    object QCadastroDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCadastroRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroFLAG: TStringField
      FieldName = 'FLAG'
      Size = 1
    end
    object QCadastroVIA: TIntegerField
      FieldName = 'VIA'
    end
    object QCadastroCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCadastroATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object QCadastroNOMETITULAR: TStringField
      FieldKind = fkLookup
      FieldName = 'NOMETITULAR'
      LookupDataSet = QConvAux
      LookupKeyFields = 'CONV_ID'
      LookupResultField = 'TITULAR'
      KeyFields = 'CONV_ID'
      Lookup = True
    end
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object QCadastroCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
  end
  object DSContaCorrente: TDataSource
    DataSet = Qcontacorrente
    Left = 592
    Top = 293
  end
  object DSCredenciado: TDataSource
    DataSet = QCredenciado
    Left = 592
    Top = 341
  end
  object PopupCOntaC: TPopupMenu
    Left = 174
    Top = 328
    object MenuItem2: TMenuItem
      Caption = 'Configura'#231#227'o da Grade'
    end
    object Exportarparaoexcel3: TMenuItem
      Caption = 'Exportar para o excel'
      OnClick = Exportarparaoexcel3Click
    end
  end
  object DSSaldoCartao: TDataSource
    DataSet = QSaldoCartao
    Left = 376
    Top = 376
  end
  object DSSaldoConv: TDataSource
    DataSet = QSaldoConv
    Left = 376
    Top = 296
  end
  object tExcel1: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\GaviaoPeixot' +
      'o.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    CursorType = ctStatic
    TableDirect = True
    TableName = 'AVIATION_1$'
    Left = 624
    Top = 464
    object tExcel1CHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object tExcel1N_CART_TIT: TFloatField
      FieldName = 'N_CART_TIT'
    end
    object tExcel1NOMEFUNCIONRIO: TWideStringField
      FieldName = 'NOME FUNCION'#193'RIO               '
      Size = 255
    end
    object tExcel1N_CART_DEP: TFloatField
      FieldName = 'N_CART_DEP '
    end
    object tExcel1NOMEDODEPENDENTE: TWideStringField
      FieldName = 'NOME DO DEPENDENTE             '
      Size = 255
    end
    object tExcel1N_CART: TWideStringField
      FieldName = 'N_CART'
      Size = 255
    end
  end
  object tExcel2: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\GaviaoPeixot' +
      'o.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    CursorType = ctStatic
    TableDirect = True
    TableName = 'AVIATION_2$'
    Left = 672
    Top = 464
    object tExcel2CHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object tExcel2N_CART_TIT: TFloatField
      FieldName = 'N_CART_TIT'
    end
    object tExcel2NOMEFUNCIONRIO: TWideStringField
      FieldName = 'NOME FUNCION'#193'RIO               '
      Size = 255
    end
    object tExcel2N_CART_DEP: TFloatField
      FieldName = 'N_CART_DEP '
    end
    object tExcel2NOMEDODEPENDENTE: TWideStringField
      FieldName = 'NOME DO DEPENDENTE             '
      Size = 255
    end
    object tExcel2N_CART: TWideStringField
      FieldName = 'N_CART'
      Size = 255
    end
  end
  object tExcel3: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\GaviaoPeixot' +
      'o.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    CursorType = ctStatic
    TableDirect = True
    TableName = 'AVIATION_3$'
    Left = 728
    Top = 464
    object tExcel3CHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object tExcel3N_CART_TIT: TFloatField
      FieldName = 'N_CART_TIT'
    end
    object tExcel3NOMEFUNCIONRIO: TWideStringField
      FieldName = 'NOME FUNCION'#193'RIO               '
      Size = 255
    end
    object tExcel3N_CART_DEP: TFloatField
      FieldName = 'N_CART_DEP '
    end
    object tExcel3NOMEDODEPENDENTE: TWideStringField
      FieldName = 'NOME DO DEPENDENTE             '
      Size = 255
    end
    object tExcel3N_CART: TWideStringField
      FieldName = 'N_CART'
      Size = 255
    end
  end
  object dExcel: TDataSource
    DataSet = tExcel1
    Left = 454
    Top = 465
  end
  object Qcontacorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'cartao_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select * from CONTACORRENTE_BEM_ESTAR '
      'where cartao_id = :cartao_id and baixa_conveniado <> '#39'S'#39)
    Left = 560
    Top = 296
    object QcontacorrenteAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QcontacorrenteCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QcontacorrenteCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QcontacorrenteCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QcontacorrenteDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QcontacorrenteDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QcontacorrenteHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object QcontacorrenteDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object QcontacorrenteDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object QcontacorrenteCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object QcontacorrenteVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object QcontacorrenteBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object QcontacorrenteFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object QcontacorrenteFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QcontacorrentePAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QcontacorrenteAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object QcontacorrenteOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QcontacorrenteDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QcontacorrenteDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QcontacorrenteDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object QcontacorrenteDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object QcontacorrenteHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object QcontacorrenteNF: TIntegerField
      FieldName = 'NF'
    end
    object QcontacorrenteDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object QcontacorrenteDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object QcontacorrenteDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object QcontacorrenteOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object QcontacorrenteOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object QcontacorrenteDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object QcontacorrenteOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object QcontacorrenteCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteNSU: TIntegerField
      FieldName = 'NSU'
    end
    object QcontacorrentePREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QcontacorrenteEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object QCredenciado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select cred_id, fantasia from credenciados')
    Left = 560
    Top = 336
    object QCredenciadocred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCredenciadofantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
  end
  object QSaldoConv: TADOQuery
    Connection = DMConexao.AdoCon
    OnCalcFields = QSaldoConvCalcFields
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'exec SALDO_PROXFECHA_CONV_COD_LIM_M :conv_id')
    Left = 320
    Top = 296
    object QSaldoConvsaldo_conf: TBCDField
      FieldName = 'saldo_conf'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoConvsaldo_nconf: TBCDField
      FieldName = 'saldo_nconf'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoConvFECHAMENTO: TWideStringField
      FieldName = 'FECHAMENTO'
      ReadOnly = True
      Size = 4000
    end
    object QSaldoConvtotal: TBCDField
      FieldName = 'total'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoConvRESTANTE: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'RESTANTE'
      DisplayFormat = '##0.00'
      currency = False
      Calculated = True
    end
  end
  object QSaldoCartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'exec SALDO_CARTAO_ADM_C :CONV_ID')
    Left = 328
    Top = 376
    object QSaldoCartaosaldo_conf: TBCDField
      FieldName = 'saldo_conf'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoCartaosaldo_nconf: TBCDField
      FieldName = 'saldo_nconf'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoCartaoFECHAMENTO: TWideStringField
      FieldName = 'FECHAMENTO'
      ReadOnly = True
      Size = 4000
    end
    object QSaldoCartaototal: TBCDField
      FieldName = 'total'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QSaldoCartaonome: TStringField
      FieldName = 'nome'
      Size = 58
    end
  end
  object qConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from conveniados'
      'where conveniados.grupo_conv_emp in (3,4,5)')
    Left = 544
    Top = 240
    object qConvCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object qConvEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object qConvBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object qConvGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object qConvCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object qConvSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object qConvTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object qConvCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object qConvLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object qConvLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object qConvFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object qConvAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object qConvDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
    object qConvCARGO: TStringField
      FieldName = 'CARGO'
      Size = 45
    end
    object qConvSETOR: TStringField
      FieldName = 'SETOR'
      Size = 45
    end
    object qConvCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object qConvRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object qConvLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object qConvLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object qConvAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object qConvCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object qConvDIGITO_CONTA: TStringField
      FieldName = 'DIGITO_CONTA'
      Size = 2
    end
    object qConvTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object qConvENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object qConvNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object qConvCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object qConvTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object qConvTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object qConvCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object qConvOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object qConvOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object qConvEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object qConvCESTABASICA: TBCDField
      FieldName = 'CESTABASICA'
      Precision = 15
      Size = 2
    end
    object qConvDTULTCESTA: TDateTimeField
      FieldName = 'DTULTCESTA'
    end
    object qConvSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object qConvTIPOSALARIO: TStringField
      FieldName = 'TIPOSALARIO'
      Size = 15
    end
    object qConvCOD_EMPRESA: TStringField
      FieldName = 'COD_EMPRESA'
      Size = 30
    end
    object qConvFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object qConvDTASSOCIACAO: TDateTimeField
      FieldName = 'DTASSOCIACAO'
    end
    object qConvDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object qConvDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object qConvOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qConvDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object qConvOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object qConvVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object qConvLIBERA_GRUPOSPROD: TStringField
      FieldName = 'LIBERA_GRUPOSPROD'
      FixedChar = True
      Size = 1
    end
    object qConvCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object qConvUSA_SALDO_DIF: TStringField
      FieldName = 'USA_SALDO_DIF'
      FixedChar = True
      Size = 1
    end
    object qConvABONO_MES: TBCDField
      FieldName = 'ABONO_MES'
      Precision = 15
      Size = 2
    end
    object qConvSALDO_RENOVACAO: TBCDField
      FieldName = 'SALDO_RENOVACAO'
      Precision = 15
      Size = 2
    end
    object qConvSALDO_ACUMULADO: TBCDField
      FieldName = 'SALDO_ACUMULADO'
      Precision = 15
      Size = 2
    end
    object qConvDATA_ATUALIZACAO_ACUMULADO: TDateTimeField
      FieldName = 'DATA_ATUALIZACAO_ACUMULADO'
    end
    object qConvCONSUMO_MES: TBCDField
      FieldName = 'CONSUMO_MES'
      Precision = 15
      Size = 2
    end
    object qConvCONSUMO_MES_1: TBCDField
      FieldName = 'CONSUMO_MES_1'
      Precision = 15
      Size = 2
    end
    object qConvCONSUMO_MES_2: TBCDField
      FieldName = 'CONSUMO_MES_2'
      Precision = 15
      Size = 2
    end
    object qConvCONSUMO_MES_3: TBCDField
      FieldName = 'CONSUMO_MES_3'
      Precision = 15
      Size = 2
    end
    object qConvCONSUMO_MES_4: TBCDField
      FieldName = 'CONSUMO_MES_4'
      Precision = 15
      Size = 2
    end
    object qConvPIS: TFloatField
      FieldName = 'PIS'
    end
    object qConvNOME_PAI: TStringField
      FieldName = 'NOME_PAI'
      Size = 45
    end
    object qConvnome_mae: TStringField
      FieldName = 'nome_mae'
      Size = 45
    end
    object qConvCART_TRAB_NUM: TIntegerField
      FieldName = 'CART_TRAB_NUM'
    end
    object qConvCART_TRAB_SERIE: TStringField
      FieldName = 'CART_TRAB_SERIE'
      Size = 10
    end
    object qConvREGIME_TRAB: TStringField
      FieldName = 'REGIME_TRAB'
      Size = 45
    end
    object qConvVENC_TOTAL: TBCDField
      FieldName = 'VENC_TOTAL'
      Precision = 15
      Size = 2
    end
    object qConvESTADO_CIVIL: TStringField
      FieldName = 'ESTADO_CIVIL'
      Size = 25
    end
    object qConvNUM_DEPENDENTES: TIntegerField
      FieldName = 'NUM_DEPENDENTES'
    end
    object qConvDATA_ADMISSAO: TDateTimeField
      FieldName = 'DATA_ADMISSAO'
    end
    object qConvDATA_DEMISSAO: TDateTimeField
      FieldName = 'DATA_DEMISSAO'
    end
    object qConvFIM_CONTRATO: TDateTimeField
      FieldName = 'FIM_CONTRATO'
    end
    object qConvDISTRITO: TStringField
      FieldName = 'DISTRITO'
      Size = 45
    end
    object qConvSALDO_DEVEDOR: TBCDField
      FieldName = 'SALDO_DEVEDOR'
      Precision = 15
      Size = 2
    end
    object qConvSALDO_DEVEDOR_FAT: TBCDField
      FieldName = 'SALDO_DEVEDOR_FAT'
      Precision = 15
      Size = 2
    end
    object QConvBAIRRO1: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QConvCIDADE1: TIntegerField
      FieldName = 'CIDADE'
    end
    object QConvESTADO1: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object qCard: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from cartoes')
    Left = 504
    Top = 240
    object qCardCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object qCardCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object qCardNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object qCardLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object qCardCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object qCardDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object qCardTITULAR: TStringField
      FieldName = 'TITULAR'
      FixedChar = True
      Size = 1
    end
    object qCardJAEMITIDO: TStringField
      FieldName = 'JAEMITIDO'
      FixedChar = True
      Size = 1
    end
    object qCardAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object qCardLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object qCardCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object qCardPARENTESCO: TStringField
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object qCardDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object qCardNUM_DEP: TIntegerField
      FieldName = 'NUM_DEP'
    end
    object qCardFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object qCardDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object qCardCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object qCardRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object qCardVIA: TIntegerField
      FieldName = 'VIA'
    end
    object qCardDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object qCardDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object qCardOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qCardDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object qCardOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object qCardCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qCardATIVO: TStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object QConvAux: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from conveniados_bem_estar'
      '')
    Left = 648
    Top = 240
    object QConvAuxCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QConvAuxCARTAO_BEM_ESTAR: TStringField
      FieldName = 'CARTAO_BEM_ESTAR'
      FixedChar = True
      Size = 1
    end
    object QConvAuxEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QConvAuxBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QConvAuxGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object QConvAuxCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QConvAuxSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QConvAuxTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QConvAuxCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QConvAuxLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 8
      Size = 2
    end
    object QConvAuxLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QConvAuxFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object QConvAuxAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QConvAuxDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
    object QConvAuxCARGO: TStringField
      FieldName = 'CARGO'
      Size = 45
    end
    object QConvAuxSETOR: TStringField
      FieldName = 'SETOR'
      Size = 45
    end
    object QConvAuxCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QConvAuxRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QConvAuxLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object QConvAuxLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object QConvAuxAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QConvAuxCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object QConvAuxDIGITO_CONTA: TStringField
      FieldName = 'DIGITO_CONTA'
      FixedChar = True
      Size = 2
    end
    object QConvAuxTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object QConvAuxENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QConvAuxNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QConvAuxCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QConvAuxTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QConvAuxTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QConvAuxCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QConvAuxOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QConvAuxOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QConvAuxEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QConvAuxDTULTCESTA: TDateTimeField
      FieldName = 'DTULTCESTA'
    end
    object QConvAuxSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object QConvAuxTIPOSALARIO: TStringField
      FieldName = 'TIPOSALARIO'
      Size = 15
    end
    object QConvAuxCOD_EMPRESA: TStringField
      FieldName = 'COD_EMPRESA'
      Size = 30
    end
    object QConvAuxFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QConvAuxBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QConvAuxCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QConvAuxESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object DSConvAux: TDataSource
    DataSet = QConvAux
    Left = 680
    Top = 240
  end
end
