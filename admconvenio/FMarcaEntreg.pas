unit FMarcaEntreg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, ToolEdit, CurrEdit,
  JvCombobox, JvExStdCtrls, JvEdit, JvValidateEdit, JvExMask, JvToolEdit,
  JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, DB,
  ADODB;

type
  TF_MarcaEntreg = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CBReceita: TComboBox;
    EDNF: TEdit;
//    data: TDateEdit;
    //debito: TCurrencyEdit;
    Label1: TLabel;
    Label6: TLabel;
    //credito: TCurrencyEdit;
    Label7: TLabel;
    EdHist: TEdit;
    Label8: TLabel;
    Bevel2: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    LabTitular: TLabel;
    LabEmpresa: TLabel;
    CBNFEntregue: TJvComboBox;
    LabNomeCartao: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    LabFornec: TLabel;
    edtNumPrescritor: TEdit;
    Label11: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    cbTpPres: TJvComboBox;
    edtNumReceita: TEdit;
    cbUF_OLD: TJvComboBox;
    lblUF: TLabel;
    debito: TJvValidateEdit;
    credito: TJvValidateEdit;
    data: TJvDateEdit;
    edtDtReceita: TJvDateEdit;
    cbUF: TDBLookupComboBox;
    QEstado: TADOQuery;
    DataSource1: TDataSource;
    DSEstado: TDataSource;
    procedure EDNFKeyPress(Sender: TObject; var Key: Char);
    procedure EDNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure edtDtReceitaExit(Sender: TObject);
    procedure edtDtReceitaEnter(Sender: TObject);
    procedure CBReceitaChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure creditoKeyPress(Sender: TObject; var Key: Char);
    procedure debitoKeyPress(Sender: TObject; var Key: Char);
    procedure dataKeyPress(Sender: TObject; var Key: Char);
    procedure edtDtReceitaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    DTANT : String;
    procedure MostrarItensRodape;
    { Public declarations }
  end;

var
  F_MarcaEntreg: TF_MarcaEntreg;

implementation

uses FEntregaNF, DM, StrUtils, cartao_util, UValidacao;

{$R *.dfm}

procedure TF_MarcaEntreg.MostrarItensRodape;
begin
  Label11.Visible          := CBReceita.ItemIndex = 0;
  edtNumPrescritor.Visible := CBReceita.ItemIndex = 0;
  Label17.Visible          := CBReceita.ItemIndex = 0;
  edtNumReceita.Visible    := CBReceita.ItemIndex = 0;
  Label16.Visible          := CBReceita.ItemIndex = 0;
  edtDtReceita.Visible     := CBReceita.ItemIndex = 0;
  lblUF.Visible            := CBReceita.ItemIndex = 0;
  cbUF.Visible             := CBReceita.ItemIndex = 0;
  Label18.Visible          := CBReceita.ItemIndex = 0;
  cbTpPres.Visible         := CBReceita.ItemIndex = 0;
end;

procedure TF_MarcaEntreg.EDNFKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#13,#8]) then key := #0;
end;

procedure TF_MarcaEntreg.EDNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then begin
   if (ActiveControl is TCustomComboBox) then
      if TCustomComboBox(ActiveControl).DroppedDown then begin
         TCustomComboBox(ActiveControl).DroppedDown := False;
         exit;
      end;
   Perform(WM_NextDLgCtl,0,0);
end;

end;

procedure TF_MarcaEntreg.FormCreate(Sender: TObject);
begin
  DMConexao.Config.Open;
  if DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'N' then
  begin
    CBNFEntregue.Color := clBtnFace;
    CBNFEntregue.ReadOnly := True;
    CBNFEntregue.TabStop := False;
  end;
  DMConexao.Config.Close;
  QEstado.Open;
end;

procedure TF_MarcaEntreg.edtDtReceitaExit(Sender: TObject);
begin
  if not fnDataValida(TMaskEdit(Sender).Text) then
   TMaskEdit(Sender).Text := DTANT;
end;

procedure TF_MarcaEntreg.edtDtReceitaEnter(Sender: TObject);
begin
  DTANT := TMaskEdit(Sender).Text;
end;

procedure TF_MarcaEntreg.CBReceitaChange(Sender: TObject);
begin
  MostrarItensRodape;
end;

procedure TF_MarcaEntreg.BitBtn1Click(Sender: TObject);
begin
  if CBReceita.ItemIndex = 0 then begin
    if Trim(edtNumPrescritor.Text) = '' then begin
      msgInf('Digite o n�mero do prescritor');
      edtNumPrescritor.SetFocus;
      Abort;
    end else if Trim(edtNumReceita.Text) = '' then begin
      msgInf('Digite o n�mero da receita');
      edtNumReceita.SetFocus;
      Abort;
    end else if not fnDataValida(edtDtReceita.Text) then begin
      msgInf('Digite uma data v�lida');
      edtDtReceita.SetFocus;
      Abort;
    end else if VarIsNull(cbUF.KeyValue) then begin
      msgInf('Escolha o UF');
      cbUF.SetFocus;
      Abort;
    end else if cbTpPres.ItemIndex < 0 then begin
      msgInf('Escolha o tipo de prescritor');
      cbTpPres.SetFocus;
      Abort;
    end;
    ModalResult := mrOk;
  end else
    ModalResult := mrOk;
end;

procedure TF_MarcaEntreg.creditoKeyPress(Sender: TObject; var Key: Char);
begin
  if key = '.' then key := ',';

  if Key = #13 then
    data.SetFocus;
end;

procedure TF_MarcaEntreg.debitoKeyPress(Sender: TObject; var Key: Char);
begin
  if key = '.' then key := ',';

  if Key = #13 then
    credito.SetFocus;
end;

procedure TF_MarcaEntreg.dataKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    begin
      if CBReceita.ItemIndex = 0 then
        edtNumPrescritor.SetFocus
      else
         BitBtn1.SetFocus;
    end;
end;

procedure TF_MarcaEntreg.edtDtReceitaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if Key = #13 then
      cbUF.SetFocus;
end;

end.

