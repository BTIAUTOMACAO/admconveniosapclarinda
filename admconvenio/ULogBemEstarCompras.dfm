inherited FLogBemEstarCompras: TFLogBemEstarCompras
  Left = 253
  Top = 125
  Caption = 'Painel de Confirma'#231#227'o de Pagamento Bem-Estar'
  ClientHeight = 445
  ClientWidth = 912
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 912
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 23
    Width = 912
    Height = 146
    Align = alTop
    TabOrder = 1
    object ButListaEmp: TButton
      Left = 640
      Top = 82
      Width = 113
      Height = 55
      Caption = 'Busca (F5)'
      TabOrder = 2
      OnClick = ButListaEmpClick
    end
    object GroupBox2: TGroupBox
      Left = 512
      Top = 14
      Width = 239
      Height = 59
      Caption = 'Selecione o per'#237'odo de Ades'#227'o'
      TabOrder = 1
      object datafin: TJvDateEdit
        Left = 124
        Top = 29
        Width = 107
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 1
      end
      object DataIni: TJvDateEdit
        Left = 4
        Top = 28
        Width = 107
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object GroupBox3: TGroupBox
      Left = 4
      Top = 14
      Width = 501
      Height = 115
      Caption = 'Digite os dados do Conveniado'
      TabOrder = 0
      object Label1: TLabel
        Left = 6
        Top = 22
        Width = 44
        Height = 13
        Caption = 'CONV ID'
      end
      object Label3: TLabel
        Left = 222
        Top = 22
        Width = 125
        Height = 13
        Caption = 'NOME DO CONVENIADO'
      end
      object Label4: TLabel
        Left = 222
        Top = 62
        Width = 133
        Height = 13
        Caption = 'STATUS DO PAGAMENTO'
      end
      object txtConvID: TEdit
        Left = 6
        Top = 38
        Width = 99
        Height = 21
        TabOrder = 0
      end
      object txtNome: TEdit
        Left = 222
        Top = 38
        Width = 267
        Height = 21
        TabOrder = 1
      end
      object cbbStatus: TComboBox
        Left = 224
        Top = 80
        Width = 89
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        Text = 'P - Pendente'
        Items.Strings = (
          'P - Pendente'
          'E - Efetuado'
          'TODOS')
      end
    end
  end
  object GridLogRecargaCantinex: TJvDBGrid [2]
    Left = 0
    Top = 169
    Width = 912
    Height = 223
    Align = alClient
    DataSource = DSLogCantinex
    DefaultDrawing = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = GridLogRecargaCantinexDblClick
    AutoAppend = False
    TitleButtons = True
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'LOG ID'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Title.Caption = 'TITULAR'
        Width = 298
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'conv_id'
        Title.Caption = 'CONV ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'data_recarga'
        Title.Caption = 'DATA SOLICITA'#199#195'O DO CART'#195'O'
        Width = 182
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor_recarga'
        Title.Caption = 'VALOR BRUTO R$'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor_taxa'
        Title.Caption = 'VALOR DE DESC. R$'
        Width = 117
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor_mensalidade'
        Title.Caption = 'VALOR MENSALIDADE R$'
        Visible = True
      end>
  end
  object Panel4: TPanel [3]
    Left = 0
    Top = 392
    Width = 912
    Height = 53
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 3
    DesignSize = (
      908
      49)
    object ButMarcaDesmEmp: TButton
      Left = 7
      Top = 9
      Width = 105
      Height = 25
      Caption = 'Marca/Desm.(F12)'
      TabOrder = 3
      OnClick = ButMarcaDesmEmpClick
    end
    object ButMarcaTodasEmp: TButton
      Left = 115
      Top = 9
      Width = 105
      Height = 25
      Caption = 'Marca Todos (F6)'
      TabOrder = 4
      OnClick = ButMarcaTodasEmpClick
    end
    object ButDesmarcaTodosEmp: TButton
      Left = 224
      Top = 9
      Width = 105
      Height = 25
      Caption = 'Desm. Todos (F7)'
      TabOrder = 5
      OnClick = ButDesmarcaTodosEmpClick
    end
    object btnGravar: TBitBtn
      Left = 344
      Top = 4
      Width = 137
      Height = 41
      Caption = '&Confirmar Pagamento'
      TabOrder = 0
      OnClick = btnGravarClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000CED5D8CCD3D6
        CCD3D6D0D8DDCDD6DACBD3D6CDD4D7CCD4D7CBD2D6CBD2D6CBD2D6CFD6D9D1D7
        DBCCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CCD4D7D1D6DAD4D8DBD4D8DBD4
        D8DBD3D7DAD3D7DAD3D7DAD6D9DCD1D6DACBD2D6CCD3D6CDD4D7CCD3D6CDD6DB
        CCD5D9B7B8B8A1A3A391949586898A8487888588897E818274777764696C949B
        A1D2D9DDCFD6DACAD1D5CAD1D5CBD2D6CDD4D8C8CBCDC5C6C7C3C3C2BCBBBBBB
        BABABBBABAB7B7B7B2B1B1ABABAABEC0C0CFD5DACBD3D7CBD2D5CED6DAB8B5B2
        9A8D82663E17A19080D0D5DABABBBDBFC1C3D5D8DBD3D6DAD0D4D98E88834F40
        2E787B80A4ABAFCDD4D8CBD2D7C7CBCCBBBBBC929191BEBDBDE6E6E6D8D8D8DB
        DBDBE8E8E8E8E8E8E7E7E7BCBCBC919191B0B1B1C1C4C6CDD3D8CFD8DD82674E
        5822005B2800AF9A82F1F1F1ECE8E6EBE8E4EAE5E1EAE6E1ECECEBE5CBAC804D
        0F19170E535658C9D1D5D6DBDEABABAA828282838382C2C2C2F6F6F6F1F1F1F1
        F1F1F0F0F0EFEFEFF3F3F3DFDFDF9797977C7C7CA09F9FD2D7DAD0D8DD816850
        874E0C754308A67A50E2B48EDAAD87D9AC85CE9453CD924FCE9554D093508856
        165F4101656356C5CFD7D6DBDEABABAB969696919191AFAFAFD0D0D0CCCCCCCC
        CCCCBDBDBDBCBCBCBEBEBEBDBDBD9C9C9C8E8E8EA5A4A4D1D6D9D0D8DD7F664F
        935D277A4B17A67849E9B388E1AC80E1AB80DCA36ED59550D38D3ED08E42925D
        1CA16A1E726B5EC1CCD5D6DBDEAAA9A99E9E9E969696AEAEAECFCFCFCBCBCBCA
        CACAC5C5C5BEBEBEB9B9B9B9B9B99F9F9FA5A5A5ABA9A9D0D5D8D0D9DD7F654F
        9360317D5020995B31B47B4BAC7543AD7543AE784AAA723DA2611DB1722A9A63
        22BC7D3781786EC1CCD4D6DBDEAAA9A9A0A0A09999999F9F9FB0B0B0ADADADAD
        ADADAEAEAEABABABA2A2A2A9A9A9A2A2A2B0B0B0B1B1B0D0D5D7D0D9DD7F654F
        935F2F7F4E1C7E58418BB6A87FB7A97FB5A87FB5A780B6AA80BFB57D90747D42
        03BC7D37867D73C1CDD5D6DBDEAAA9A99F9F9F9999999D9D9DD2D2D2D3D3D3D2
        D2D2D2D2D2D2D2D2D8D8D8BEBEBE909090B0B0B0B4B3B3D1D5D8D0D9DD7F654F
        935F2F7D4A16848B7AB3F6FD9FE5EFA0E5EEA0E5EEA0E5EEA4EFF593C8C77552
        1FBD7A31857D72C1CDD5D6DBDEAAA9A99F9F9F959595B9B9B9F8F8F8EEEEEEEF
        EFEFEFEFEFEFEFEFF4F4F4E0E0E09A9A9AAEAEAEB3B3B2D1D5D8D0D9DD7F654F
        925E2E8D590B92B698B9EAEEA9CDCCAACFCFA9CFCFA9CFCFACCECFACECF28F92
        5BBC7325847C72C2CDD5D6DBDEA9A9A99F9F9F9B9B9BD0D0D0F1F1F1E0E0E0E1
        E1E1E1E1E1E1E1E1E1E1E1F2F2F2BEBEBEA9A9A9B2B2B1D0D5D8D0D9DD7F644E
        925E2E9F7D429AC7B0B3E9EEA6D3D7A8D4D6A9D4D4A9D4D4ABD4D4A9EBEF99A4
        79BE7628877D72C2CDD5D6DBDEA9A9A99F9F9FAFAFAFDADADAF2F2F2E4E4E4E4
        E4E4E4E4E4E4E4E4E4E4E4F2F2F2C8C8C8ABABABB3B3B2D0D5D8D0D9DD7E634C
        925E2DAFA4A1A2D6DCAFF3EEAEE9DBABEDE8AAEFF0AAEFEFABEFEFA4F7F9A3BC
        B7C3803D8E8477C2CED5D6DBDDA9A8A89E9E9EC9C9C9E5E5E5F7F7F7EEEEEEF1
        F1F1F4F4F4F4F4F4F4F4F4F9F9F9D7D7D7B1B1B1B7B7B7D1D6D9D0D9DC7A5D49
        905828B7AEA7B9CAD4D1BFA4D08C3ABAA487A7B1B8A9AFB2ADADAFB1DBE2ACCA
        C6D2976A938B83C2CDD3D6DBDEA7A6A59C9C9CCECECEDFDFDFD8D8D8B8B8B8C8
        C8C8D2D2D2D0D0D0CECECEE8E8E8DFDFDFC0C0C0BBBBBAD0D5D8CDD5D9A9A6A4
        90745B8A6D38A79C84B3B7A8A5B58E9FBCA899C1B89AC0B69BBFB69DD1C78E95
        708C5E37A9A39DCDD6DBCCD4D7C0C4C6AFAFAEA9A9A8C4C3C3D3D3D3CFCFCFD5
        D5D5D9D9D9D8D8D8D8D8D8E2E2E2C0C0C0A4A3A2BFC2C2CDD4D8CCD2D6CED6DB
        C3CACFB4B4AD9D8E778D7D618C87718E856B9084679084678F84688C7F61A39D
        8DBAC0C1C8CED1CBD2D6CAD1D5CBD2D7D1D5D8C5C9CAB9BABAB3B2B2B8B8B7B7
        B6B6B6B6B6B7B6B6B7B6B6B4B3B3BCBEBFCED1D2D0D5D8C9D0D5CFD5D9CCD3D7
        CDD4D9D0D9E0CED6DBCDD3D7CED2D6CED2D6CED2D6CED2D6CED2D6CDD2D6CFD6
        DCCFD6DBCDD4D8CCD3D7CCD3D7CCD3D7CCD3D7CCD4D9D1D7DBD4D8DBD2D7DAD2
        D7DAD2D7DAD2D7DAD2D7DAD3D8DBCFD6DACCD3D7CCD3D7CED4D8}
      NumGlyphs = 2
    end
    object bntGerarPDF: TBitBtn
      Left = 768
      Top = 8
      Width = 92
      Height = 29
      Anchors = [akTop, akRight]
      Caption = '&Gerar PDF'
      TabOrder = 2
      Visible = False
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
        FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
        9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
        FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
        FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
        E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
        B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
        FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
        FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
        F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
        F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
        F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
        F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
        F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
        F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
        EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
        F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
        EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
        EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
        A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
        00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
        3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
        99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
        85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
        AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
        FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
        03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
        3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
        FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
        1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
        BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object btnCancelar: TBitBtn
      Left = 488
      Top = 4
      Width = 129
      Height = 41
      Caption = '&Cancelar Pagamento'
      TabOrder = 1
      OnClick = btnCancelarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5DAF77D8EE5314CD600
        21CC0021CC314CD67D8EE5D5DAF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9BA8EC1D3BD51739E42749F33254FF3254FF2749F31739E41D3BD59BA8
        ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9DAAED1031DA2A4CF33B5DFF4062FF43
        65FF4365FF4062FF3B5DFF2A4CF31031DA9DAAEDFFFFFFFFFFFFFFFFFFD7DCF8
        2644DC2F51F46385FE0021CC7B92FCFFFFFFFFFFFFB8C4FE6984FC3E60FD2F51
        F42644DCD7DCF8FFFFFFFFFFFF8697EC284AEA4062FC4365FF87A9FF0021CC7F
        90E5FFFFFFFFFFFFE4E8FE647EF84062FC284AEA8697ECFFFFFFFFFFFF445FE4
        4163F7385AF47088F54365FF87A9FF0021CC7F90E5FFFFFFFFFFFFB2BFF9385A
        F44163F7445FE4FFFFFFFFFFFF1E40E3587AFF2648E9FFFFFFA1B2FF4365FF87
        A9FF0021CC7F90E5FFFFFFFFFFFF2648E9587AFF1E40E3FFFFFFFFFFFF2446E8
        6183FF1C3EE2FFFFFFFFFFFFA1B2FF4365FF87A9FF0021CC7F90E5FFFFFF1C3E
        E26183FF2446E8FFFFFFFFFFFF546FF05C7EFB2D4FE5A8B5F2FFFFFFFFFFFFA1
        B2FF4365FF87A9FF0021CC5D74E62D4FE55C7EFB546FF0FFFFFFFFFFFF96A7F8
        4E70F74F71F0425CDEDEE2F9FFFFFFFFFFFFA1B2FF4365FF87A9FF0021CC4F71
        F04E70F796A7F8FFFFFFFFFFFFDEE4FE4E6DF8698BFD3F61E73D57DBA3AFEEFF
        FFFFFFFFFF536ADF4365FF87A9FF698BFD4E6DF8DEE4FEFFFFFFFFFFFFFFFFFF
        B2BFFD496BFA6F91FD5476ED2547DB0021CC0021CC2547DB5476ED6F91FD496B
        FAB2BFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3C1FE5775FD5F81FE7698FF87
        A9FF87A9FF7698FF5F81FE5775FDB3C1FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE0E6FF9FB0FF6783FF4365FF4365FF6783FF9FB0FFE0E6FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 148
    Top = 40
  end
  object QLogCantinex: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select '
      'LOG_ID ID,'
      'valor, '
      'log_recarga_cantinex.valor_taxa, '
      'valor_recarga, '
      'CONVERT(varchar,dt_cadastro,103) DATA_RECARGA, '
      'ORIGEM, '
      'log_recarga_cantinex.CONV_ID, '
      'CART.CODCARTIMP,'
      'CONV.TITULAR, '
      'EMPRESAS.NOME'
      'from log_recarga_cantinex'
      
        'INNER JOIN CONVENIADOS CONV ON log_recarga_cantinex.CONV_ID = CO' +
        'NV.CONV_ID '
      'INNER JOIN EMPRESAS ON EMPRESAS.EMPRES_ID = CONV.EMPRES_ID'
      'INNER JOIN CARTOES CART ON CART.CONV_ID = CONV.CONV_ID'
      'WHERE ORIGEM = '#39'PagSeguro'#39)
    Left = 336
    Top = 256
    object QLogCantinexCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QLogCantinexTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QLogCantinexLOG_ID: TIntegerField
      FieldName = 'LOG_ID'
    end
    object QLogCantinexDATA_SOLICITACAO: TDateField
      FieldName = 'DATA_SOLICITACAO'
    end
    object QLogCantinexBRUTO: TCurrencyField
      FieldName = 'BRUTO'
    end
    object QLogCantinexTAXA: TCurrencyField
      FieldName = 'TAXA'
    end
  end
  object DSLogCantinex: TDataSource
    DataSet = MLogRecargaCantinex
    Left = 368
    Top = 256
  end
  object MLogRecargaCantinex: TJvMemoryData
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'valor'
        DataType = ftCurrency
      end
      item
        Name = 'valor_taxa'
        DataType = ftCurrency
      end
      item
        Name = 'valor_recarga'
        DataType = ftCurrency
      end
      item
        Name = 'data_recarga'
        DataType = ftDate
      end
      item
        Name = 'origem'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'conv_id'
        DataType = ftInteger
      end
      item
        Name = 'titular'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end
      item
        Name = 'codcartimp'
        DataType = ftString
        Size = 20
      end>
    Left = 336
    Top = 288
    object MLogRecargaCantinexID: TIntegerField
      FieldName = 'ID'
    end
    object MLogRecargaCantinexvalor: TCurrencyField
      FieldName = 'valor'
    end
    object MLogRecargaCantinexvalor_taxa: TCurrencyField
      FieldName = 'valor_taxa'
    end
    object MLogRecargaCantinexvalor_recarga: TCurrencyField
      FieldName = 'valor_recarga'
    end
    object MLogRecargaCantinexdata_recarga: TDateField
      FieldName = 'data_recarga'
    end
    object MLogRecargaCantinexconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object MLogRecargaCantinextitular: TStringField
      FieldName = 'titular'
      Size = 50
    end
    object MLogRecargaCantinexmarcado: TBooleanField
      FieldName = 'marcado'
    end
    object MLogRecargaCantinexvalor_mensalidade: TCurrencyField
      FieldName = 'valor_mensalidade'
    end
  end
end
