object frmImportProdXls: TfrmImportProdXls
  Left = 299
  Top = 230
  Width = 367
  Height = 256
  Caption = 'Importa'#231#227'o de Produtos para Prog. Desc.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 17
    Width = 94
    Height = 13
    Caption = 'Caminho do arquivo'
  end
  object Label2: TLabel
    Left = 20
    Top = 73
    Width = 195
    Height = 13
    Caption = 'Programa onde ser'#225' gerada a importa'#231#227'o'
  end
  object BitBtn1: TSpeedButton
    Left = 20
    Top = 144
    Width = 321
    Height = 29
    Caption = 'Importa'#231#227'o de listagem de produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = BitBtn1Click
  end
  object Label3: TLabel
    Left = 56
    Top = 115
    Width = 287
    Height = 13
    Caption = 'Aten'#231#227'o leia a ajuda antes de efetuar a opera'#231#227'o.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ButAjuda: TSpeedButton
    Left = 20
    Top = 116
    Width = 32
    Height = 22
    Hint = 'Ajuda'
    Flat = True
    Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
      5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
      DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
      CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
      E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
      A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
      2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
      D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
      3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
      F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
      3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
      FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
      3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
      FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
      3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
      FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
      5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
      FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
      FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
      FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
      FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
      E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
      BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
      C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
      7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
      D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
      E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
      FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
      FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
      D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    OnClick = ButAjudaClick
  end
  object lblC: TLabel
    Left = 128
    Top = 16
    Width = 6
    Height = 13
    Caption = '0'
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 202
    Width = 351
    Height = 16
    Align = alBottom
    TabOrder = 1
  end
  object cbbPrograma: TJvDBLookupCombo
    Left = 20
    Top = 89
    Width = 321
    Height = 21
    LookupField = 'PROG_ID'
    LookupDisplay = 'NOME'
    LookupSource = DataSource1
    TabOrder = 2
  end
  object RichEdit1: TRichEdit
    Left = 280
    Top = 17
    Width = 53
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'Importa'#231#227'o de Produtos para Programas'
      ''
      
        '- Para realizar a importa'#231#227'o de um arquivo de produtos para um p' +
        'rograma'
      
        '  de descontos '#233' necess'#225'rio que o arquivo seja do tipo (.xls/.xl' +
        'sx) e '
      '  esteja no formato esperado pelo sistema.'
      ''
      '- Segue abaixo o formato para o arquivo.'
      '  Verifique se os nomes dos campos n'#227'o est'#227'o com espa'#231'os.'
      ''
      
        '   -------------------------------------------------------------' +
        '----------'
      
        '  | CAMPO         | TAMAM | FORMATO | DESCRICAO                 ' +
        '          |'
      
        '  |-------------------------------------------------------------' +
        '----------|'
      
        '  | BARRA         |  013  | Texto   | Codigo de barras do produt' +
        'o         |'
      
        '  | PRODUTO       |  045  | Texto   | Descricao do produto      ' +
        '          |'
      
        '  | BRUTO         |   -   | Real    | Valor unitario do produto ' +
        'Ex: 999,99|'
      
        '  | PERCENTUAL    |   -   | Real    | Percentual de Desconto Ex:' +
        ' 999,99   |'
      
        '  | QTD2          |   -   | Real    | Desconto para 2 produtos  ' +
        'Ex: 999,99|'
      
        '  | QTD3          |   -   | Real    | Desconto para 3 produtos  ' +
        'Ex: 999,99|'
      
        '  | VALE          |   -   | Real    | Vale Desconto no Produto E' +
        'x: 999,99 |'
      
        '   -------------------------------------------------------------' +
        '----------'
      ''
      '  Aten'#231#227'o: '
      '  1 - O nome da planilha deve ser "prod" sem as aspas.'
      
        '  2 - Para o nome do arquivo '#233' sugerido que n'#227'o contenha espa'#231'os' +
        '.'
      
        '  3 - Entende-se por nome do campo o conte'#250'do da primeira c'#233'lula' +
        ' de uma '
      '      coluna de uma planilha do excel.')
    ParentFont = False
    TabOrder = 3
    Visible = False
    WordWrap = False
  end
  object ckbVale: TCheckBox
    Left = 20
    Top = 178
    Width = 129
    Height = 16
    Caption = 'Inclui Vale Desconto'
    TabOrder = 4
  end
  object ckbQtd2: TCheckBox
    Left = 172
    Top = 178
    Width = 50
    Height = 17
    Caption = 'Qtd2'
    TabOrder = 6
  end
  object ckbQtd3: TCheckBox
    Left = 260
    Top = 178
    Width = 51
    Height = 17
    Caption = 'Qtd3'
    TabOrder = 5
  end
  object FilenameEdit1: TJvFilenameEdit
    Left = 20
    Top = 33
    Width = 321
    Height = 21
    Filter = 'Planilha do Excel (*.xls;*.xlsx)|*.xls;*.xlsx'
    TabOrder = 0
    Text = 'C:\AdmCartaoBella\'
  end
  object Table1: TADOTable
    ConnectionString = 
      'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=D:\William\ListaPr' +
      'od.xlsx;Extended Properties="Excel 12.0;HDR=YES;"'
    CursorType = ctStatic
    TableDirect = True
    TableName = 'prod$'
    Left = 180
    Top = 1
  end
  object DataSource1: TDataSource
    DataSet = AdoQry1
    Left = 252
    Top = 1
  end
  object AdoQry1: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT programas.prog_id, programas.nome FROM programas'
      'WHERE programas.apagado = '#39'N'#39
      'AND programas.dt_fim >= current_timestamp')
    Left = 224
    object AdoQry1prog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object AdoQry1nome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
end
