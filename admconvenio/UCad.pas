unit UCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, ExtCtrls, Grids, DBGrids, ComCtrls, Buttons,
  DB, {JvDBCtrl,} {JvMemDS,} JvDateTimePicker, JvDBDateTimePicker,
  Mask, JvToolEdit, Menus, JvDBLookup, UClassLog, strutils, JvExMask,
  JvExDBGrids, JvDBGrid, ADODB;

const INFORMACOES_OCORRENCIA_OBRIGATORIO = 'Informa��es da Ocorr�ncia devem ser preenchidas';

type
  TFCad = class(TForm)
    PageControl1: TPageControl;
    TabGrade: TTabSheet;
    TabFicha: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    ButBusca: TBitBtn;
    ButAtualiza: TBitBtn;
    DBGrid1: TJvDBGrid;
    DSCadastro: TDataSource;
    Panel2: TPanel;
    ButEdit: TBitBtn;
    ButInclui: TBitBtn;
    ButApaga: TBitBtn;
    Panel3: TPanel;
    EdCod: TEdit;
    TabHistorico: TTabSheet;
    PanelHistorico: TPanel;
    GridHistorico: TJvDBGrid;
    LabelDataini: TLabel;
    LabelDataFinal: TLabel;
    dataini: TJvDateEdit;
    datafin: TJvDateEdit;
    ButGrava: TBitBtn;
    ButCancela: TBitBtn;
    DBCampo: TComboBox;
    Label42: TLabel;
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    ButFiltro: TBitBtn;
    Minimizar1: TMenuItem;
    Barra: TStatusBar;
    PanStatus: TPanel;
    ButAltLin: TButton;
    PopupGrid1: TPopupMenu;
    panStatus2: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    btnFirstB: TSpeedButton;
    btnPriorB: TSpeedButton;
    btnNextB: TSpeedButton;
    btnLastB: TSpeedButton;
    btnHist: TBitBtn;
    btnFiltroDados: TSpeedButton;
    btnAlterLinear: TSpeedButton;
    btnFirstA: TSpeedButton;
    btnPriorA: TSpeedButton;
    btnNextA: TSpeedButton;
    btnLastA: TSpeedButton;
    btnFirstC: TSpeedButton;
    btnPriorC: TSpeedButton;
    btnNextC: TSpeedButton;
    btnLastC: TSpeedButton;
    QHistorico: TADOQuery;
    DSHistorico: TDataSource;
    QHistoricoLOG_ID: TIntegerField;
    QHistoricoJANELA: TStringField;
    QHistoricoCAMPO: TStringField;
    QHistoricoVALOR_ANT: TStringField;
    QHistoricoVALOR_POS: TStringField;
    QHistoricoOPERADOR: TStringField;
    QHistoricoOPERACAO: TStringField;
    QHistoricoDATA_HORA: TDateTimeField;
    QHistoricoCADASTRO: TStringField;
    QHistoricoID: TIntegerField;
    QHistoricoDETALHE: TStringField;
    QHistoricoMOTIVO: TStringField;
    QHistoricoSOLICITANTE: TStringField;
    QCadastro: TADOQuery;
    procedure ButEditClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure ButApagaClick(Sender: TObject);
    procedure ButGravaClick(Sender: TObject);
    procedure ButCancelaClick(Sender: TObject);
    procedure ButAtualizaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButhistClick(Sender: TObject);
    procedure TabHistoricoShow(Sender: TObject);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QCadastro_AfterScroll(DataSet: TDataSet);
    procedure QCadastro_BeforePost(DataSet: TDataSet);
    procedure EdNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QCadastro_BeforeEdit(DataSet: TDataSet);
    procedure QCadastro_BeforeInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure PopupButPopup(Sender: TObject);
    procedure Restaurar1Click(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure ButFiltroClick(Sender: TObject);
    procedure QCadastro_AfterOpen(DataSet: TDataSet);
    procedure TabFichaShow(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Minimizar1Click(Sender: TObject);
    procedure ButCloseClick(Sender: TObject);
    procedure PanStatusResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure datainiExit(Sender: TObject);
    procedure Exportar1Click(Sender: TObject);
    procedure ButAltLinClick(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure QCadastro_BeforeOpen(DataSet: TDataSet);
    procedure EdCodExit(Sender: TObject);
    procedure DSCadastroStateChange(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
    procedure btnHistClick(Sender: TObject);
    procedure btnFiltroDadosClick(Sender: TObject);
    procedure btnAlterLinearClick(Sender: TObject);
    procedure btnFirstBClick(Sender: TObject);
    procedure btnPriorBClick(Sender: TObject);
    procedure btnNextBClick(Sender: TObject);
    procedure btnLastBClick(Sender: TObject);
    procedure btnFirstCClick(Sender: TObject);
    procedure btnPriorCClick(Sender: TObject);
    procedure btnNextCClick(Sender: TObject);
    procedure btnLastCClick(Sender: TObject);
    procedure DSHistoricoDataChange(Sender: TObject; Field: TField);
    procedure QCadastroAfterOpen(DataSet: TDataSet);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroBeforeEdit(DataSet: TDataSet);
    procedure QCadastroBeforeInsert(DataSet: TDataSet);
    procedure QCadastroBeforeOpen(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    editagrade : string;
    FAcumularAutor: Boolean;
    LogCad : TLog;
    procedure Filtrar;
    function BuscaNomeCampo(DisplayName:String):String;
    function GetTextTitulo:String;
    procedure SetTextTitulo(Text:String);
    function GetTextStatus:String;
    procedure SetTextStatus(Text:String);
    procedure PesqLog;
    procedure ValidaColunas(Grade: TDBGrid);
    procedure CriarChapaExcluida;
    procedure TratarBloqueadosGrid;
  public
    { Public declarations }
    //Foi nescessario colocar as variaveis aqui porque elas se confundiam entre os forms.
    colocouMensagem : Boolean;
    chavepri, detalhe, janela, liberado_field : string;
    Alterar, Incluir, Excluir,flag, excluindo : Boolean;
    contadorDePosts : Integer;
    but, sep : TToolButton;
    procedure ShowForm(Sender:TObject);overload;
    function  GetExpressao(tabela:string):String;
    procedure HabilitarBotoes;
    property TextTitulo : String read GetTextTitulo write SetTextTitulo;
    property TextStatus : String read GetTextStatus write SetTextStatus;
  protected
    property AcumularAutor: Boolean read FAcumularAutor;
  end;


implementation

uses DM, UMenu, UColunas, CheckLst, UFiltro, UMenuArv,
  UTelaAltLinear, ToolEdit, UGeraLista, impressao, calculadora, UTipos,
  DateUtils, UDBGridHelper, cartao_util, UValidacao, uStringGridHelper,
  FOcorrencia, ConvUtils, ThreadLoadCadastro;

{$R *.dfm}

procedure TFCad.SetTextTitulo(Text:String);
begin
panTitulo.Caption := Text;
end;

function TFCad.GetTextTitulo:String;
begin
Result := panTitulo.Caption;
end;

procedure TFCad.SetTextStatus(Text:String);
begin
PanStatus2.Caption := Text;
end;

function TFCad.GetTextStatus:String;
begin
Result := PanStatus2.Caption;
end;

procedure TFCad.ButEditClick(Sender: TObject);
begin
QCadastro.Edit;
end;

procedure TFCad.ButIncluiClick(Sender: TObject);
begin
if TBitBtn(Sender).Enabled = False then Abort;
QCadastro.Append;
QCadastro.FieldByName('APAGADO').AsString := 'N';
PageControl1.ActivePage := TabFicha;
end;

procedure TFCad.ButApagaClick(Sender: TObject);
var S : String;
begin
  if (TBitBtn(Sender).Enabled = False) then Abort;
  if QCadastro.RecordCount <= 0 then
    begin
    MsgErro('N�o existe registro para ser apagado, ou n�o existe registro selecinoado!');
    Abort;
    end;
  if Excluir then
  begin
    if not QCadastro.IsEmpty then
    begin
      flag := False;
      if Application.MessageBox('Confirma a exclus�o deste registro?','Aten��o',MB_YESNO+MB_DEFBUTTON2+MB_ICONQUESTION) = IDYes then
      begin
        Try
          excluindo := True;
          flag := True;
          QCadastro.Edit;
          QCadastro.FieldByName('APAGADO').AsString := 'S';
          if Self.ClassNameIs('TFCadConv') then
          begin
            CriarChapaExcluida;
          end;
          QCadastro.Post;
          QCadastro.Requery();
        except
          QCadastro.Cancel;
          ShowMessage('N�o foi possivel excluir este registro.');
        end;
      end;
    end
  end
  else
  begin
    ShowMessage('Opera��o n�o permitida para o usu�rio.');
  end;
end;

procedure TFCad.CriarChapaExcluida;
var chapa_excluida : Extended;
begin
  // Rotina para nao duplicar chapas excluidas.
  chapa_excluida := Coalesce(DMConexao.ExecuteScalar(' Select min(chapa) from conveniados where apagado = ''S'' and chapa < 0 '));
  chapa_excluida := chapa_excluida - 1;
  QCadastro.FieldByName('CHAPA').Value := chapa_excluida;
end;

procedure TFCad.ButGravaClick(Sender: TObject);
begin
  if QCadastro.State in [dsInsert,dsEdit] then
    QCadastro.Post;
end;

procedure TFCad.ButCancelaClick(Sender: TObject);
begin
  if QCadastro.State in [dsInsert,dsEdit] then
    QCadastro.Cancel;
end;

procedure TFCad.ButAtualizaClick(Sender: TObject);
var mark : TBookmark;
begin
  if not QCadastro.IsEmpty then
  begin
    Screen.Cursor := crHourGlass;
    mark := QCadastro.GetBookmark;
    QCadastro.Refresh;
    QCadastro.GotoBookmark(mark);
    QCadastro.FreeBookmark(mark);
    Screen.Cursor := crDefault;
    DBGrid1.SetFocus;
  end;
end;

procedure TFCad.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
If Key = VK_F3 then PageControl1.ActivePage := TabGrade;
If Key = VK_F7 then PageControl1.ActivePage := TabFicha;
If Key = VK_F2 then ButEdit.Click;
If Key = VK_F5 then begin
   PageControl1.ActivePageIndex:= 1;
   ButInclui.Click;
end;
If Key = VK_F6 then ButApaga.Click;

if (Key in [vk_return,vk_escape]) and (not(ActiveControl is TDBGrid)) and (not(ActiveControl is TMemo)) and (not(ActiveControl is TDBMemo)) then begin
   if (ActiveControl is TJvDBLookupCombo) then begin
      if TJvDBLookupCombo(ActiveControl).ListVisible then begin
         if key = vk_return then
            TJvDBLookupCombo(ActiveControl).CloseUp(true)
          else
            TJvDBLookupCombo(ActiveControl).CloseUp(false);
         exit;
      end;
   end
   else if (ActiveControl is TCustomComboBox) then begin
      if TCustomComboBox(ActiveControl).DroppedDown then begin
         TCustomComboBox(ActiveControl).DroppedDown := False;
         exit;
      end;
   end;
   if key = vk_return then
      SelectNext(ActiveControl,true,true);
end;

If Key = vk_escape then
   if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
      close;
      
end;

procedure TFCad.EdCodKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then ButBusca.Click;
end;

procedure TFCad.FormCreate(Sender: TObject);
var i : integer;
    Qry : String;
    thread: CarregaQuery;
begin
  janela := Copy(Self.ClassName,2,length(Self.ClassName));
  PageControl1.ActivePageIndex := 0;
  //Lendo configura��o de edi��o da grade
  DMConexao.Config.Open;
  editagrade := DMConexao.ConfigEDIT_GRID.AsString;
  FAcumularAutor := (DMConexao.ConfigACUMULA_AUTOR_PROX_FECHA.AsString = 'S');
  DMConexao.Config.Close;
  LogCad := TLog.Create;
  LogCad.LogarQuery(QCadastro,janela,detalhe,Copy(Self.ClassName,2,length(Self.ClassName)),Operador.Nome,chavepri);
  Qry := QCadastro.SQL.Text;
  thread := CarregaQuery.Create(Qry);
  thread.FreeOnTerminate := True;

  //QCadastro.Open;
  DataIni.Date := date;
  DataFin.Date := date;
  //Rotina para a cria��o do bot�o na barra de tarefas do programa.
  but         := TToolButton.Create(self);
  sep         := TToolButton.Create(self);
  but.Name    := Name+'1';
  sep.Name    := Name+'2';
  but.Caption := Caption;
  but.Hint    := Caption;
  but.ShowHint := True;
  but.OnClick := ShowForm;
  but.PopupMenu := PopupBut;
  if FMenu.Barra.ButtonCount > 0 then
  begin
    FMenu.Barra.AutoSize := False;
    sep.Left := FMenu.Barra.Buttons[FMenu.Barra.ButtonCount-1].Left+1;
    but.Left := sep.Left+1;
  end;
  sep.Style   := tbsSeparator;
  FMenu.Barra.InsertControl(sep);
  FMenu.Barra.InsertControl(but);
  but.Click;
  if editagrade = 'S' then
  begin
    DBGrid1.ReadOnly := False;
    DBGrid1.Options := [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgConfirmDelete,dgCancelOnExit];
  end;
  for i := 0 to DBGrid1.Columns.Count - 1 do
    if CompareText(DBGrid1.Columns[i].FieldName,chavepri) = 0 then
    begin
      DBGrid1.Columns[i].ReadOnly := True;
      Break;
    end;
  TDBGridHelper.DBGridHelperTratrForm(Self,DMConexao.AdoQry,Operador.ID);
  TStringGridHelper.StringGridHelperTratrForm(Self);
  TratarBloqueadosGrid;
end;

procedure TFCad.TratarBloqueadosGrid;
begin
   if QCadastro.FindField('liberado') <> nil then
      liberado_field := 'liberado'
   else if QCadastro.FindField('liberada') <> nil then
      liberado_field := 'liberada';
end;

procedure TFCad.DBGrid1DblClick(Sender: TObject);
begin
if not QCadastro.IsEmpty then PageControl1.ActivePage := TabFicha;
end;

procedure TFCad.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if editagrade <> 'S' then begin
     case Key  of
        vk_return : DBGrid1DblClick(nil);
     end;
  end;
  if (ssCtrl in Shift) and (key = vk_delete) then begin
     MsgErro('Opsss, para apagar algum registro use o bot�o Apagar na aba Em Ficha.');
     Abort;
  end;
end;

procedure TFCad.ButhistClick(Sender: TObject);
var cadastro, field : string;
begin
  if QCadastro.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('mm/dd/yyyy 00:00:00',dataini.Date))+' and '+QuotedStr(FormatDateTime('mm/dd/yyyy 23:59:59',datafin.Date)));
  QHistorico.Sql.Add(' and ID = '+QCadastro.FieldByName(chavepri).AsString);
  QHistorico.Sql.Add(' and JANELA = '+QuotedStr(janela));
  if DBCampo.ItemIndex > 0 then begin
     cadastro := Copy(DBCampo.Text,1,Pos('.',DBCampo.Text)-1);
     field    := Copy(DBCampo.Text,Pos('.',DBCampo.Text)+1,Length(DBCampo.Text));
     QHistorico.Sql.Add(' and CAMPO = '+QuotedStr(field));
  end;
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFCad.TabHistoricoShow(Sender: TObject);
begin
dataini.SetFocus;
QHistorico.Close;
end;

procedure TFCad.GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  try
    if Pos(Field.FieldName,QHistorico.Sort) > 0 then begin
       if Pos(' Desc',QHistorico.Sort) > 0 then QHistorico.Sort := Field.FieldName
                                                   else QHistorico.Sort := Field.FieldName+' Desc';
    end
    else QHistorico.Sort := Field.FieldName;
  except
  end;     
end;

procedure TFCad.QCadastro_AfterScroll(DataSet: TDataSet);
begin
QHistorico.Close;
end;

procedure TFCad.QCadastro_BeforePost(DataSet: TDataSet);
var Cadastro : String;
begin
  Try
    if QCadastro.State = dsInsert then
    begin
      QCadastro.FieldByName('DTCADASTRO').AsDateTime := Now;
      QCadastro.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
      DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',QCadastro.FieldByName(chavepri).AsString,StringReplace(detalhe,': ','',[rfReplaceAll]),Self.Name);
    end;
    colocouMensagem := DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Altera��o',QCadastro.FieldByName(chavepri).AsString,'', '');

    DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',QCadastro.FieldByName(chavepri).AsString,'', '');
    if (DMConexao.config.IsEmpty) then
      DMConexao.Config.Open;
    if (DMConexao.ConfigMOSTRAR_TELA_OCORRENCIA.AsString = 'S') then begin
      FrmOcorrencia := TFrmOcorrencia.Create(Self);
      if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
        DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',StringReplace(detalhe,': ','',[rfReplaceAll]),FrmOcorrencia.edtSolicitante.Text, FrmOcorrencia.mmoMotivo.Text);
        FreeAndNil(FrmOcorrencia);
        colocouMensagem := True;
      end else begin
        colocouMensagem := False;
        FreeAndNil(FrmOcorrencia);
        MsgInf('Informa��es da Ocorr�ncia devem ser preenchidas');
        //DataSet.Cancel;
        Application.ProcessMessages;
        Exit;
        //raise Exception.Create(INFORMACOES_OCORRENCIA_OBRIGATORIO);
        //Abort;
      end;
    end else begin
      DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',StringReplace(detalhe,': ','',[rfReplaceAll]),'', '');
      colocouMensagem := True;
    end;
    QCadastro.FieldByName('DTALTERACAO').AsDateTime := Now;
    QCadastro.FieldByName('OPERADOR').AsString := Operador.Nome;
    if QCadastro.FieldByName('APAGADO').AsString = 'S' then
       QCadastro.FieldByName('DTAPAGADO').AsDateTime := Now;
  except on E:Exception do
    if e.Message = INFORMACOES_OCORRENCIA_OBRIGATORIO then
      msgInf(e.Message);
  end;
end;

procedure TFCad.EdNomeKeyPress(Sender: TObject; var Key: Char);
begin
if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then ButBusca.Click;
end;

procedure TFCad.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var i : integer;
begin
 if QCadastro.State in [dsInsert,dsEdit] then
    case Application.MessageBox('Deseja salvar as �ltimas altera��es?','Confirma��o',MB_YESNOCANCEL+MB_ICONQUESTION+MB_DEFBUTTON1) of
      IDYes : QCadastro.Post;
      IDNo  : QCadastro.Cancel;
      IDCANCEL : SysUtils.Abort;
    end;
 for i := 0 to ComponentCount - 1 do if Components[i] is TDataSet then TDataSet(Components[i]).Close;
end;


procedure TFCad.QCadastro_BeforeEdit(DataSet: TDataSet);
begin
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
  excluindo := False;
end;

procedure TFCad.QCadastro_BeforeInsert(DataSet: TDataSet);
begin
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCad.FormClose(Sender: TObject; var Action: TCloseAction);
var BlobStream: TStream;
begin
  LogCad.Free;
  Action := caFree;
end;

procedure TFCad.DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
   try
    if Pos(Field.FieldName,QCadastro.Sort) > 0 then begin
       if Pos(' DESC',QCadastro.Sort) > 0 then QCadastro.Sort := Field.FieldName
                                                  else QCadastro.Sort := Field.FieldName+' DESC';
    end
    else QCadastro.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCad.ShowForm(Sender:TObject);
var i : integer;
begin
  for i := 0 to FMenu.Barra.ButtonCount-1 do FMenu.Barra.Buttons[i].Down := False;
  if Self.WindowState <> wsMaximized then Self.WindowState := wsMaximized;
  Self.Show;
  TToolButton(Sender).Down := True;
end;

procedure TFCad.PopupButPopup(Sender: TObject);
begin
  Restaurar1.Enabled := WindowState <> wsMaximized;
  Minimizar1.Enabled := WindowState = wsMaximized;
end;

procedure TFCad.Restaurar1Click(Sender: TObject);
begin
Self.Show;
end;

procedure TFCad.FormDeactivate(Sender: TObject);
var i : integer;
begin
for i := 0 to FMenu.Barra.ButtonCount-1 do FMenu.Barra.Buttons[i].Down := False;
end;

procedure TFCad.ButFiltroClick(Sender: TObject);
var i : integer;
begin
   FFiltro := TFFiltro.Create(Self);
   FFiltro.janela := janela;
   FFiltro.QCampos.Open;
   for i := 0 to QCadastro.FieldCount-1 do begin
       if ( (UpperCase(QCadastro.Fields[i].FieldName) <> 'APAGADO') and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'SENHA'  )) then begin
          FFiltro.QCampos.Append;
          FFiltro.QCamposDescricao.AsString := QCadastro.Fields[i].DisplayName;
          FFiltro.QCamposTipo.AsString      := TipodoCampo(QCadastro.Fields[i].DataType);
          FFiltro.QCampos.Post;
       end;
   end;
   if Self.ClassNameIs('TFCadConv') or Self.ClassNameIs('TFCadCartoes') then begin
       FFiltro.QCampos.Append;    // Campos dinamicos.
       FFiltro.QCamposDescricao.AsString := 'D�bito total';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha.';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       //
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha. c/receita';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha. s/receita';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       //
   end;
   FFiltro.QCampos.First;
   FFiltro.QFiltro.Sql.Text := 'Select * from filtros where janela = '+QuotedStr(janela)+' order by descricao ';
   FFiltro.QFiltro.Open;
   if not FFiltro.QFiltro.IsEmpty then begin
      FFiltro.QFiltro.First;
      FFiltro.DBFiltro.KeyValue := FFiltro.QFiltroFILTRO_ID.AsInteger;
   end;
   FFiltro.ShowModal;
   if FFiltro.ModalResult = mrOk then begin
      Filtrar;
   end;
   FFiltro.Free;
end;

function TFCad.BuscaNomeCampo(DisplayName:String):String;
var i : integer;
begin
  for i := 0 to QCadastro.FieldCount - 1 do begin
     if DisplayName = QCadastro.Fields[i].DisplayName then begin
        Result := QCadastro.Fields[i].FieldName;
        Exit;
     end;
  end;
end;

procedure TFCad.Filtrar;
var sql, sql_old, sql_new, val1, val2, campo : string;
begin
  sql  := EmptyStr;
  if not FFiltro.QDetalhes.IsEmpty then begin
     FFiltro.QDetalhes.First;
     while not FFiltro.QDetalhes.Eof do begin
        val1 := EmptyStr;
        val2 := EmptyStr;
        if ( Self.ClassNameIs('TFCadConv')
           and (  (FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito total')
                or (FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha.' )
                or (FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha. c/receita')
                or (FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha. s/receita'))) then begin
           if ( FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito total') then
              campo := '(select sum(saldo_mes) from saldo_conv(conveniados.conv_id) )'
           else if ( FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha.') then
              campo := '(select saldo_mes from SALDO_PROXFECHA_CONV(conveniados.conv_id) )'
           else if ( FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha. c/receita') then
              campo := '(select saldo from SALDO_CONV_REC(conveniados.conv_id,"S") ) '
           else
              campo := '(select saldo from SALDO_CONV_REC(conveniados.conv_id,"N") ) '
        end
        else if Self.ClassNameIs('TFCadCartoes') then begin
              if  FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito total' then
                 campo := '(select sum(saldo_cart.saldo_mes) from saldo_cart(cartoes.conv_id)  where saldo_cart.cartao_id = cartoes.cartao_id)'
              else if FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha.' then
                 campo := '(select saldo from saldo_cart(cartoes.conv_id) where saldo_cart.cartao_id = cartoes.cartao_id and fechamento = (select * from get_prox_fecha(conveniados.empres_id)) )'
              else if FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha. c/receita' then
                 campo := '(select sum(contacorrente.debito-contacorrente.credito) from contacorrente where coalesce(contacorrente.baixa_conveniado,"N") <> "S" and contacorrente.cartao_id = cartoes.cartao_id ' + 'and contacorrente.receita = "S" and contacorrente.data_fecha_emp = (Select * from SALDO_PROXFECHA_CONV(conveniados.empres_id)) )'
              else if FFiltro.QDetalhesFIELD_NAME.AsString = 'D�bito pr�x. fecha. s/receita' then
                 campo := '(select sum(contacorrente.debito-contacorrente.credito) from contacorrente where coalesce(contacorrente.baixa_conveniado,"N") <> "S" and contacorrente.cartao_id = cartoes.cartao_id ' + 'and (coalesce(contacorrente.receita,"N") = "N") and contacorrente.data_fecha_emp = (Select * from SALDO_PROXFECHA_CONV(conveniados.empres_id)) )'
              else if FFiltro.QDetalhesFIELD_NAME.AsString = 'Nome Titular' then
                 campo := 'Conveniados.Titular'
              else if FFiltro.QDetalhesFIELD_NAME.AsString = 'Empres ID' then
                 campo := 'Conveniados.empres_id'
              else
                 campo := 'Cartoes.'+BuscaNomeCampo(FFiltro.QDetalhesFIELD_NAME.AsString); //Adicionei essa linha pq na query de cartoes tem um join e estava confundindo os campos.
        end
        else
           campo := BuscaNomeCampo(FFiltro.QDetalhesFIELD_NAME.AsString);
        if Pos(FFiltro.QDetalhesFIELD_TYPE.AsString,'ftFloat,ftCurrency,ftBCD') > 0 then begin
           val1 := StringReplace(FFiltro.QDetalhesVALOR1.AsString,',','.',[rfReplaceAll]);
           if Trim(FFiltro.QDetalhesVALOR2.AsString) <> '' then
              val2 := StringReplace(FFiltro.QDetalhesVALOR2.AsString,',','.',[rfReplaceAll]);
        end
        else if Pos(FFiltro.QDetalhesFIELD_TYPE.AsString,'ftDate,ftDateTime') > 0 then begin
           val1 := FormatDateTime('mm/dd/yyyy 00:00:00',StrToDate(FFiltro.QDetalhesVALOR1.AsString));
           val1 := QuotedStr(val1);
           {if IsValidDateTime(FFiltro.QDetalhesVALOR2.AsString) then begin
              val2 := FormatDateTime('mm/dd/yyyy 23:59:59',StrToDate(FFiltro.QDetalhesVALOR2.AsString));
              val2 := QuotedStr(val2);
           end;     }//comentado ariane
        end
        else if FFiltro.QDetalhesFIELD_TYPE.AsString = 'ftString' then begin
           val1 := QuotedStr(FFiltro.QDetalhesVALOR1.AsString);
           if Trim(FFiltro.QDetalhesVALOR2.AsString) <> '' then begin
              val2 := QuotedStr(FFiltro.QDetalhesVALOR2.AsString);
           end;
        end
        else begin //ftSmallint,ftInteger,ftWord
           val1 := FFiltro.QDetalhesVALOR1.AsString;
           if Trim(FFiltro.QDetalhesVALOR2.AsString) <> '' then
              val2 := FFiltro.QDetalhesVALOR2.AsString;
        end;

        if FFiltro.QDetalhesCONJUNCAO.AsString = 'E' then
           sql := sql+' and '+campo
        else if FFiltro.QDetalhesCONJUNCAO.AsString = 'OU' then
           sql := sql+' or '+campo
        else sql := sql+' '+campo;

        if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Igual (=)' then
           sql := sql+' = '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Maior Igual (>=)' then
           sql := sql+' >= '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Menor Igual (<=)' then
           sql := sql+' <= '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Maior (>)' then
           sql := sql+' > '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Menor (<)' then
           sql := sql+' < '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Entre (>= e <=)' then
           sql := sql+' >= '+val1+' and '+campo+' <= '+val2
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Iniciando Com' then
           sql := sql+' starting with '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Que Contenha' then
           sql := sql+' containing '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Diferente (<>)' then
           sql := sql+' <> '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Que n�o Contenha' then
           sql := sql+' not containing '+val1
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Terminando Com' then begin
           Insert('%',val1,2);
           sql := sql+' Like '+val1;
        end
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'Nulo' then
           sql := sql+' is null '
        else if FFiltro.QDetalhesCOMPARACAO_TIPO.AsString = 'N�o Nulo' then
           sql := sql+' is not null ';
        FFiltro.QDetalhes.Next;
     end;
     sql_old := QCadastro.SQL.Text;
     sql_new := Copy(sql_old,0,Pos('where',sql_old)-1);
     Insert(' where '+sql+' and apagado <> "S" ',sql_new,length(sql_new)+1);
     sql := sql_new;
     screen.Cursor := crHourGlass;
     try
       QCadastro.Close;
       QCadastro.SQL.Text := sql;
       QCadastro.Open;
     except
       on E:Exception do begin
          ShowMessage('Ocorreu algum problema durante a tentativa de filtrar o cadastro.'+#13+'Por favor verifique o filtro selecionado.'+#13+'Erro: '+E.Message);
          QCadastro.Close;
          QCadastro.SQL.Text := sql_old;
          QCadastro.Open;
       end;
     end;
     screen.Cursor := crDefault;
  end;
end;

procedure TFCad.QCadastro_AfterOpen(DataSet: TDataSet);
begin
Barra.SimpleText := ' Registros encontrados: '+IntToStr(QCadastro.RecordCount);
HabilitarBotoes;
end;

procedure TFCad.TabFichaShow(Sender: TObject);
begin
  if FindComponent('DBEdit2') is TDBEdit
  and TDBEdit(FindComponent('DBEdit2')).Enabled
  and TDBEdit(FindComponent('DBEdit2')).Visible then
  TDBEdit(FindComponent('DBEdit2')).SetFocus;
end;

procedure TFCad.SpeedButton5Click(Sender: TObject);
begin
Close;
end;

procedure TFCad.Minimizar1Click(Sender: TObject);
begin
Self.WindowState := wsMinimized;
end;

procedure TFCad.ButCloseClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCad.PanStatusResize(Sender: TObject);
begin
  panTitulo.Top:= 1;
  panTitulo.Left:= 1;
  panTitulo.Height:= 20;
  panTitulo.Width:= PanStatus.Width - 3;
  ButClose.Left := PanStatus.Width - (ButClose.Width+4);
end;

procedure TFCad.FormActivate(Sender: TObject);
begin
Self.WindowState := wsMaximized;
but.Down         := True;
if (ActiveControl = nil) and (PageControl1.ActivePageIndex = 0) then begin
   if EdCod.Showing then begin
      try
      EdCod.SetFocus;
      except
      end;
   end;
end;
end;

procedure TFCad.PesqLog;
begin
  DMConexao.AdoQry.Close;
  QHistorico.Close;
  DBCampo.Clear;
  if not QCadastro.IsEmpty then begin
     DMConexao.AdoQry.Close;
     DMConexao.AdoQry.Sql.Clear;
     DMConexao.AdoQry.Sql.Add(' Select distinct CADASTRO + ''.'' + CAMPO from logs ');
     DMConexao.AdoQry.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('dd/mm/yyyy 00:00:00',dataini.Date))+' and '+QuotedStr(FormatDateTime('dd/mm/yyyy 23:59:59',datafin.Date)));
     DMConexao.AdoQry.Sql.Add(' and ID = '+QCadastro.FieldByName(chavepri).AsString);
     DMConexao.AdoQry.Sql.Add(' and JANELA = '+QuotedStr(janela));
     DMConexao.AdoQry.Sql.Add('  order by 1');
     DMConexao.AdoQry.Sql.Text;
     DMConexao.AdoQry.Open;
     DMConexao.AdoQry.First;
     DBCampo.Items.Add('Todos os Campos');
     while not DMConexao.AdoQry.Eof do begin
        DBCampo.Items.Add(DMConexao.AdoQry.Fields[0].AsString);
        DMConexao.AdoQry.Next;
     end;
     DMConexao.AdoQry.Close;
     DBCampo.ItemIndex := 0;
  end;
end;

procedure TFCad.datainiExit(Sender: TObject);
begin
PesqLog;
end;

procedure TFCad.Exportar1Click(Sender: TObject);
begin
Grade_to_PlanilhaExcel(GridHistorico,'Hist�rico'); 
end;

procedure TFCad.ButAltLinClick(Sender: TObject);
var i : integer;
  campo, mov_id, tabela, sql, SqlWhere : string;
begin
  If QCadastro.IsEmpty then Exit;
  if not Operador.IsAdmin then
  begin
    ShowMessage('Esta opera��o � permitida somente para administradores.');
    Exit;
  end;
  DMConexao.AdoQry.Close;
  FTelaAltLinear := TFTelaAltLinear.Create(self);
  for i := 0 to QCadastro.Fields.Count - 1 do
  begin
    if IsEditableField(QCadastro.Fields[i],chavepri) then
    begin
      FTelaAltLinear.TempCampos.Append;
      FTelaAltLinear.TempCamposCampo.AsString     := QCadastro.Fields[i].DisplayName;
      FTelaAltLinear.TempCamposFieldName.AsString := QCadastro.Fields[i].FieldName;
      FTelaAltLinear.TempCamposTipo.AsVariant     := Variant(QCadastro.Fields[i].DataType);
      if IsNumericField(QCadastro.Fields[i].DataType) and QCadastro.Fields[i].IsNull then
        FTelaAltLinear.TempCamposValor.AsVariant    := 0
      else
        FTelaAltLinear.TempCamposValor.AsVariant    := QCadastro.Fields[i].AsVariant;
      FTelaAltLinear.TempCamposSize.AsInteger     := QCadastro.Fields[i].Size;
      FTelaAltLinear.TempCampos.Post;
    end;
  end;
  FTelaAltLinear.TempCampos.SortOnFields('Campo');
  FTelaAltLinear.TempCampos.First;
  FTelaAltLinear.DBCampos.KeyValue := FTelaAltLinear.TempCamposCampo.AsString;
  FTelaAltLinear.DBCamposChange(nil);
  FTelaAltLinear.dados := QCadastro;
  FTelaAltLinear.ShowModal;
  if FTelaAltLinear.ModalResult = mrOk then
  begin
    Self.BringToFront;
    if Application.MessageBox(PChar('Confirma esta altera��o?'+#13+'Todos os registros abertos ser�o afetados.'),'Aten��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDNo then
    begin
      FTelaAltLinear.Free;
      Exit;
    end;
    Screen.Cursor := crHourGlass;
    try
      FTelaAltLinear.ButExec.Enabled := False;
      FTelaAltLinear.Button2.Enabled := False;
      campo := FTelaAltLinear.TempCamposFieldName.AsString;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Text := 'Select gen_id(GEN_ALT_LINEAR_ID,1) from rdb$database ';
      DMConexao.AdoQry.Open;
      if DMConexao.AdoQry.Fields[0].asInteger = 1 then // primeira vez atualiza o generator
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := ' Select Max(mov_id) from BACK_ALT_LINEAR  ';
        DMConexao.AdoQry.Open;
        mov_id := IntToStr(DMConexao.AdoQry.Fields[0].AsInteger+1);
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := ' SET GENERATOR GEN_ALT_LINEAR_ID TO '+ mov_id;
        DMConexao.AdoQry.ExecSQL;
      end
      else
      begin
        mov_id := DMConexao.AdoQry.Fields[0].AsString;
        DMConexao.AdoQry.Close;
      end;
      tabela := copy(QCadastro.SQL.Text,Pos('from',QCadastro.SQL.Text)+5,50);
      tabela := UpperCase(copy(tabela,1,Pos(' ',tabela)-1));
      SqlWhere := copy(QCadastro.SQL.Text,Pos(' from ',LowerCase(QCadastro.SQL.Text)),length(QCadastro.SQL.Text));

      //Bloco para gavar back up altera��o linear;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' insert into BACK_ALT_LINEAR(MOV_ID,CHAVE_PRIM,TABELA,CAMPO,VALOR_OLD,DATA,OPERADOR,HISTORICO,CAMPO_CHAVE)');
      if Self.Name = 'FCadConv' then
        DMConexao.AdoQry.SQL.Add(' select '+mov_id+','+tabela+'.'+chavepri+',"'+tabela+'","'+tabela+'.'+campo+'",'+campo+','+FormatDataIB(date)+',"'+Operador.Nome+'","'+FTelaAltLinear.EdHistorico.Text+'","'+chavepri+'"' )
      else
        DMConexao.AdoQry.SQL.Add(' select '+mov_id+','+chavepri+',"'+tabela+'","'+campo+'",'+campo+','+FormatDataIB(date)+',"'+Operador.Nome+'","'+FTelaAltLinear.EdHistorico.Text+'","'+chavepri+'"' );
      DMConexao.AdoQry.SQL.Add( SqlWhere );
      DMConexao.AdoQry.ExecSQL;
      //Fim. Bloco para gavar back up altera��o linear;

      //Bloco para gravar logs;
      DMConexao.AdoQry.SQL.Text := ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                             ' OPERACAO, DATA_HORA, ID) ';
      DMConexao.AdoQry.SQL.Add(' Select gen_id(gen_log_id,1),"'+janela+'","'+
                         FTelaAltLinear.TempCamposCampo.AsString+'",');

      if IsFloatField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select str from floattostr2('+tabela+'.'+campo+',2)) ')
      else if IsDateTimeField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select RETORNO from datetostr('+tabela+'.'+campo+')) ')
      else
        DMConexao.AdoQry.SQL.Add(campo);
      DMConexao.AdoQry.SQL.Add(',');

      if IsFloatField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select str from floattostr2('+iif(FTelaAltLinear.ChkExpress.Checked,GetExpressao(tabela),FTelaAltLinear.NewValue)+',2)) ')
      else if IsDateTimeField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select RETORNO from datetostr('+FTelaAltLinear.NewValue+')) ')
      else
        DMConexao.AdoQry.SQL.Add(iif(FTelaAltLinear.ChkExpress.Checked,GetExpressao(tabela),FTelaAltLinear.NewValue));

      DMConexao.AdoQry.SQL.Add(',"'+Operador.Nome+'","Alt."||'+mov_id+',current_timestamp,'+
                         '"'+Copy(Self.ClassName,2,length(Self.ClassName))+'",'+
                         chavepri+',"'+detalhe+'"||'+chavepri);
      DMConexao.AdoQry.SQL.Add(SqlWhere);
      DMConexao.AdoQry.ExecSQL;
      //Fim. Bloco para gravar logs;

      //Bloco que efetua a altera��o linear
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' update '+tabela+' set '+campo+' = ');
      if FTelaAltLinear.ChkExpress.Checked then
        DMConexao.AdoQry.SQL.Add(GetExpressao(tabela))
      else
        DMConexao.AdoQry.SQL.Add(FTelaAltLinear.NewValue);
      DMConexao.AdoQry.SQL.Add(' , dtalteracao = cast(current_timestamp as date) ');
      DMConexao.AdoQry.SQL.Add(' , operador = '+QuotedStr(Operador.Nome));

      if not Self.ClassNameIs('TFCadCartoes') then
        SqlWhere := copy(SqlWhere,pos('where',SqlWhere), length(SqlWhere));

      if pos('order ',SqlWhere) > 0 then
        SqlWhere := copy(SqlWhere,1, pos('order',SqlWhere)-1);

      if Self.ClassNameIs('TFCadCartoes') then
      begin
        DMConexao.AdoQry.SQL.Add(' where cartao_id in ( select cartoes.cartao_id '+SqlWhere+' )' );
      end
      else
        DMConexao.AdoQry.SQL.Add(SqlWhere);
      try
        DMConexao.AdoQry.ExecSQL;
      except on E:Exception
        do
        begin
          if Pos(UpperCase('COLUMN UNKNOWN'), UpperCase(E.Message)) <> 0 then
          begin
            Self.BringToFront;
            MsgErro('O processo est� tentando usar um campo que n�o foi encontrado no banco de dados. '+sLineBreak+'Entre em contato com suporte do sistema.');
            if QCadastro.State = dsEdit then QCadastro.Cancel;
            QCadastro.EnableControls;
            Screen.Cursor := crDefault;
            FTelaAltLinear.Free;
            Exit;
          end;
        end;
      end;
      //Fim. Bloco que efetua a altera��o linear

      QCadastro.Refresh;
      Self.BringToFront;
      ShowMessage('Opera��o executada com sucesso!');
      Barra.SimpleText := ' Altera��o linear conclu�da com sucesso!';
    except on E:Exception
    do
      begin
        Self.BringToFront;
        ShowMessage('Ocorreu um erro durante a opera��o!. '+#13+'Erro: '+E.Message);
        Barra.SimpleText := ' Ocorreu um erro durante a opera��o.';
        If QCadastro.State = dsEdit then
          QCadastro.Cancel;
      end;
    end;
    QCadastro.EnableControls;
    Screen.Cursor := crDefault;
  end;
  FTelaAltLinear.Free;
  Screen.Cursor := crDefault;
end;

procedure TFCad.DBGrid1ColExit(Sender: TObject);
begin
  if editagrade = 'S' then if QCadastro.State = dsEdit then QCadastro.Post;
end;

procedure TFCad.QCadastro_BeforeOpen(DataSet: TDataSet);
begin
  Barra.SimpleText := ' Buscando dados aguarde... ';
end;

function TFCad.GetExpressao(tabela:string): String;
var i : integer; marka : TBookmark;
  express : string;
begin
  express := FTelaAltLinear.texto1.Text;
  FTelaAltLinear.TempCampos.First;
  while not FTelaAltLinear.TempCampos.Eof do
  begin
    express := StringReplace(express,'"'+FTelaAltLinear.TempCamposCampo.AsString+'"','coalesce('+tabela+'.'+FTelaAltLinear.TempCamposFieldName.AsString+',0)',[rfIgnoreCase,rfReplaceAll]);
    FTelaAltLinear.TempCampos.Next;
  end;
  Result := express
end;

procedure TFCad.EdCodExit(Sender: TObject);
var send : string;
begin
  send := (sender as TEdit).Text;
  if Trim(send) <> '' then
  begin
    if send[1] = ',' then Delete(send,1,1);
    if send[Length(send)] = ',' then delete(send,1,Length(send));
   (sender as TEdit).Text := send;
  end;
end;

procedure TFCad.ValidaColunas(Grade:TDBGrid);
var Dataset : TDataSet;
  i : Integer;
  cols : TList;
begin  //Verifica se alguma coluna foi apagada do sistema e deleta das configura��es da grade.
  Dataset := Grade.DataSource.DataSet;
  cols := TList.Create;
  for i := 0 to Grade.Columns.Count - 1 do
  begin
    if Dataset.FindField(Grade.Columns[i].FieldName) = nil then
    begin
      cols.add(Grade.Columns[i]);
    end;
  end;
  for i := 0 to cols.Count - 1 do TColumn(cols[i]).Free;
  cols.Free;
end;

procedure TFCad.HabilitarBotoes;
begin
  ButCancela.Enabled  := QCadastro.State in [dsEdit,dsInsert];
  ButGrava.Enabled    := QCadastro.State in [dsEdit,dsInsert];
  ButInclui.Enabled   := (QCadastro.State = dsBrowse) and Incluir;
  ButEdit.Enabled     := (QCadastro.State = dsBrowse) and Alterar and (not QCadastro.IsEmpty);
  ButApaga.Enabled    := (QCadastro.State = dsBrowse) and Excluir and (not QCadastro.IsEmpty);
  if FindComponent('ButNovoCod') <> nil then
    TWinControl(FindComponent('ButNovoCod')).Enabled := Alterar and (not QCadastro.IsEmpty);
  if FindComponent('ButLimpaSenha') <> nil then
    TWinControl(FindComponent('ButLimpaSenha')).Enabled := Alterar and (not QCadastro.IsEmpty);
end;

procedure TFCad.DSCadastroStateChange(Sender: TObject);
begin
   HabilitarBotoes;
end;

procedure TFCad.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if liberado_field <> '' then
  begin
    if QCadastro.FieldByName(liberado_field).AsString = 'N' then
    begin
      DBGrid1.Canvas.Font.Color := clRed; //Se estiver bloqueado fonte vermelha.
      if gdSelected in State then
      begin
        DBGrid1.Canvas.Brush.Color:= $00BFFFFF; //Se estiver selecionado fundo amarelo.
      end;
      DBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column,State);
    end;
  end;
  
end;

procedure TFCad.FormShow(Sender: TObject);
var i : integer;
begin
  Self.WindowState:= wsMaximized;
  Self.SetTextTitulo('  '+self.Caption);
{  for i := 0 to ComponentCount -1 do begin
    if Components[i] is TZReadOnlyQuery then
      TZReadOnlyQuery(Components[i]).Connection := DM1.ConnectionReadOnly;
  end;}
end;

procedure TFCad.Fechar1Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TFCad.DSCadastroDataChange(Sender: TObject; Field: TField);
begin
  btnFirstA.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Bof);
  btnFirstB.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Bof);
  btnPriorA.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Bof);
  btnPriorB.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Bof);
  btnNextA.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Eof);
  btnNextB.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Eof);
  btnLastA.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Eof);
  btnLastB.Enabled:= (QCadastro.State = dsBrowse) and not (QCadastro.Eof);
end;

procedure TFCad.btnHistClick(Sender: TObject);
var cadastro, field : string;
begin
  if QCadastro.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('dd/mm/yyyy 00:00:00',dataini.Date))+' and '+QuotedStr(FormatDateTime('dd/mm/yyyy 23:59:59',datafin.Date)));
  QHistorico.Sql.Add(' and ID = '+QCadastro.FieldByName(chavepri).AsString);
  QHistorico.Sql.Add(' and JANELA = '+QuotedStr(janela));
  if DBCampo.ItemIndex > 0 then begin
     cadastro := Copy(DBCampo.Text,1,Pos('.',DBCampo.Text)-1);
     field    := Copy(DBCampo.Text,Pos('.',DBCampo.Text)+1,Length(DBCampo.Text));
     QHistorico.Sql.Add(' and CAMPO = '+QuotedStr(field));
  end;
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFCad.btnFiltroDadosClick(Sender: TObject);
var i : integer;
begin
   FFiltro := TFFiltro.Create(Self);
   FFiltro.janela := janela;
   FFiltro.QCampos.Open;
   for i := 0 to QCadastro.FieldCount-1 do begin
       if ( (UpperCase(QCadastro.Fields[i].FieldName) <> 'APAGADO') and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'DTAPAGADO'  ) and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'FLAG'  ) and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'LIMITE_PROX_FECHAMENTO'  ) and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'CONV_ID_1'  ) and
            (UpperCase(QCadastro.Fields[i].FieldName) <> 'SENHA'  )) then begin
          FFiltro.QCampos.Append;
          FFiltro.QCamposDescricao.AsString := QCadastro.Fields[i].DisplayName;
          FFiltro.QCamposTipo.AsString      := TipodoCampo(QCadastro.Fields[i].DataType);
          FFiltro.QCampos.Post;
       end;
   end;
   if Self.ClassNameIs('TFCadConv') or Self.ClassNameIs('TFCadCartoes') then begin
       FFiltro.QCampos.Append;    // Campos dinamicos.
       FFiltro.QCamposDescricao.AsString := 'D�bito total';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha.';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       //
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha. c/receita';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       FFiltro.QCampos.Append;
       FFiltro.QCamposDescricao.AsString := 'D�bito pr�x. fecha. s/receita';
       FFiltro.QCamposTipo.AsString      := 'ftCurrency';
       FFiltro.QCampos.Post;
       //
   end;
   FFiltro.QCampos.First;
   FFiltro.QFiltro.Sql.Text := 'Select * from filtros where janela = '+QuotedStr(janela)+' order by descricao ';
   FFiltro.QFiltro.Open;
   if not FFiltro.QFiltro.IsEmpty then begin
      FFiltro.QFiltro.First;
      FFiltro.DBFiltro.KeyValue := FFiltro.QFiltroFILTRO_ID.AsInteger;
   end;
   FFiltro.ShowModal;
   if FFiltro.ModalResult = mrOk then begin
      Filtrar;
   end;
   FFiltro.Free;
end;

procedure TFCad.btnAlterLinearClick(Sender: TObject);
var i : integer;
  campo, mov_id, tabela, sql, SqlWhere : string;
begin
  If QCadastro.IsEmpty then Exit;
  if not Operador.IsAdmin then
  begin
    ShowMessage('Esta opera��o � permitida somente para administradores.');
    Exit;
  end;
  DMConexao.AdoQry.Close;
  FTelaAltLinear := TFTelaAltLinear.Create(self);
  for i := 0 to QCadastro.Fields.Count - 1 do
  begin
    if IsEditableField(QCadastro.Fields[i],chavepri) then
    begin
      FTelaAltLinear.TempCampos.Append;
      FTelaAltLinear.TempCamposCampo.AsString     := QCadastro.Fields[i].DisplayName;
      FTelaAltLinear.TempCamposFieldName.AsString := QCadastro.Fields[i].FieldName;
      FTelaAltLinear.TempCamposTipo.AsVariant     := Variant(QCadastro.Fields[i].DataType);
      if IsNumericField(QCadastro.Fields[i].DataType) and QCadastro.Fields[i].IsNull then
        FTelaAltLinear.TempCamposValor.AsVariant    := 0
      else
        FTelaAltLinear.TempCamposValor.AsVariant    := QCadastro.Fields[i].AsVariant;
      FTelaAltLinear.TempCamposSize.AsInteger     := QCadastro.Fields[i].Size;
      FTelaAltLinear.TempCampos.Post;
    end;
  end;
  FTelaAltLinear.TempCampos.SortOnFields('Campo');
  FTelaAltLinear.TempCampos.First;
  FTelaAltLinear.DBCampos.KeyValue := FTelaAltLinear.TempCamposCampo.AsString;
  FTelaAltLinear.DBCamposChange(nil);
  FTelaAltLinear.dados := QCadastro;
  FTelaAltLinear.ShowModal;
  if FTelaAltLinear.ModalResult = mrOk then
  begin
    Self.BringToFront;
    if Application.MessageBox(PChar('Confirma esta altera��o?'+#13+'Todos os registros abertos ser�o afetados.'),'Aten��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDNo then
    begin
      FTelaAltLinear.Free;
      Exit;
    end;
    Screen.Cursor := crHourGlass;
    try
      FTelaAltLinear.ButExec.Enabled := False;
      FTelaAltLinear.Button2.Enabled := False;
      campo := FTelaAltLinear.TempCamposFieldName.AsString;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Text := 'Select gen_id(GEN_ALT_LINEAR_ID,1) from rdb$database ';
      DMConexao.AdoQry.Open;
      if DMConexao.AdoQry.Fields[0].asInteger = 1 then // primeira vez atualiza o generator
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := ' Select Max(mov_id) from BACK_ALT_LINEAR  ';
        DMConexao.AdoQry.Open;
        mov_id := IntToStr(DMConexao.AdoQry.Fields[0].AsInteger+1);
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := ' SET GENERATOR GEN_ALT_LINEAR_ID TO '+ mov_id;
        DMConexao.AdoQry.ExecSQL;
      end
      else
      begin
        mov_id := DMConexao.AdoQry.Fields[0].AsString;
        DMConexao.AdoQry.Close;
      end;
      tabela := copy(QCadastro.SQL.Text,Pos('FROM',UpperCase(QCadastro.SQL.Text))+5,50);
      tabela := UpperCase(copy(tabela,1,Pos(' ',tabela)-1));
      SqlWhere := copy(QCadastro.SQL.Text,Pos(' from ',LowerCase(QCadastro.SQL.Text)),length(QCadastro.SQL.Text));

      //Bloco para gavar back up altera��o linear;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' insert into BACK_ALT_LINEAR(MOV_ID,CHAVE_PRIM,TABELA,CAMPO,VALOR_OLD,DATA,OPERADOR,HISTORICO,CAMPO_CHAVE)');
      if Self.Name = 'FCadConv' then
        DMConexao.AdoQry.SQL.Add(' select '+mov_id+','+tabela+'.'+chavepri+',"'+tabela+'","'+tabela+'.'+campo+'",'+campo+','+FormatDataIB(date)+',"'+Operador.Nome+'","'+FTelaAltLinear.EdHistorico.Text+'","'+chavepri+'"' )
      else
        DMConexao.AdoQry.SQL.Add(' select '+mov_id+','+chavepri+',"'+tabela+'","'+campo+'",'+campo+','+FormatDataIB(date)+',"'+Operador.Nome+'","'+FTelaAltLinear.EdHistorico.Text+'","'+chavepri+'"' );
      DMConexao.AdoQry.SQL.Add( SqlWhere );
      DMConexao.AdoQry.ExecSQL;
      //Fim. Bloco para gavar back up altera��o linear;

      //Bloco para gravar logs;
      DMConexao.AdoQry.SQL.Text := ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                             ' OPERACAO, DATA_HORA, ID) ';
      DMConexao.AdoQry.SQL.Add(' Select gen_id(gen_log_id,1),"'+janela+'","'+
                         FTelaAltLinear.TempCamposCampo.AsString+'",');

      if IsFloatField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select str from floattostr2('+tabela+'.'+campo+',2)) ')
      else if IsDateTimeField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select RETORNO from datetostr('+tabela+'.'+campo+')) ')
      else
        DMConexao.AdoQry.SQL.Add(campo);
      DMConexao.AdoQry.SQL.Add(',');

      if IsFloatField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select str from floattostr2('+iif(FTelaAltLinear.ChkExpress.Checked,GetExpressao(tabela),FTelaAltLinear.NewValue)+',2)) ')
      else if IsDateTimeField(TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant)) then
        DMConexao.AdoQry.SQL.Add(' (select RETORNO from datetostr('+FTelaAltLinear.NewValue+')) ')
      else
        DMConexao.AdoQry.SQL.Add(iif(FTelaAltLinear.ChkExpress.Checked,GetExpressao(tabela),FTelaAltLinear.NewValue));

      DMConexao.AdoQry.SQL.Add(',"'+Operador.Nome+'","Alt."||'+mov_id+',current_timestamp,'+
                         '"'+Copy(Self.ClassName,2,length(Self.ClassName))+'",'+
                         chavepri+',"'+detalhe+'"||'+chavepri);
      DMConexao.AdoQry.SQL.Add(SqlWhere);
      DMConexao.AdoQry.ExecSQL;
      //Fim. Bloco para gravar logs;

      //Bloco que efetua a altera��o linear
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' update '+tabela+' set '+campo+' = ');
      if FTelaAltLinear.ChkExpress.Checked then
        DMConexao.AdoQry.SQL.Add(GetExpressao(tabela))
      else
        DMConexao.AdoQry.SQL.Add(FTelaAltLinear.NewValue);
      DMConexao.AdoQry.SQL.Add(' , dtalteracao = cast(current_timestamp as date) ');
      DMConexao.AdoQry.SQL.Add(' , operador = '+QuotedStr(Operador.Nome));

      if not Self.ClassNameIs('TFCadCartoes') then
        SqlWhere := copy(SqlWhere,pos('WHERE',UpperCase(SqlWhere)), length(SqlWhere));

      if pos('order ',SqlWhere) > 0 then
        SqlWhere := copy(SqlWhere,1, pos('order',SqlWhere)-1);

      if Self.ClassNameIs('TFCadCartoes') then
      begin
        DMConexao.AdoQry.SQL.Add(' where cartao_id in ( select cartoes.cartao_id '+SqlWhere+' )' );
      end
      else
        DMConexao.AdoQry.SQL.Add(SqlWhere);
      try
        DMConexao.AdoQry.ExecSQL;
      except on E:Exception
        do
        begin
          if Pos(UpperCase('COLUMN UNKNOWN'), UpperCase(E.Message)) <> 0 then
          begin
            Self.BringToFront;
            MsgErro('O processo est� tentando usar um campo que n�o foi encontrado no banco de dados. '+sLineBreak+'Entre em contato com suporte do sistema.');
            if QCadastro.State = dsEdit then QCadastro.Cancel;
            QCadastro.EnableControls;
            Screen.Cursor := crDefault;
            FTelaAltLinear.Free;
            Exit;
          end;
        end;
      end;
      //Fim. Bloco que efetua a altera��o linear

      QCadastro.Refresh;
      Self.BringToFront;
      ShowMessage('Opera��o executada com sucesso!');
      Barra.SimpleText := ' Altera��o linear conclu�da com sucesso!';
    except on E:Exception
    do
      begin
        Self.BringToFront;
        ShowMessage('Ocorreu um erro durante a opera��o!. '+#13+'Erro: '+E.Message);
        Barra.SimpleText := ' Ocorreu um erro durante a opera��o.';
        If QCadastro.State = dsEdit then
          QCadastro.Cancel;
      end;
    end;
    QCadastro.EnableControls;
    Screen.Cursor := crDefault;
  end;
  FTelaAltLinear.Free;
  Screen.Cursor := crDefault;
end;

procedure TFCad.btnFirstBClick(Sender: TObject);
begin
  prVerfEAbreCon(QCadastro);
  QCadastro.First;
end;

procedure TFCad.btnPriorBClick(Sender: TObject);
begin
  prVerfEAbreCon(QCadastro);
  QCadastro.Prior;
end;

procedure TFCad.btnNextBClick(Sender: TObject);
begin
  prVerfEAbreCon(QCadastro);
  QCadastro.Next;
end;

procedure TFCad.btnLastBClick(Sender: TObject);
begin
  prVerfEAbreCon(QCadastro);
  QCadastro.Last;
end;

procedure TFCad.btnFirstCClick(Sender: TObject);
begin
  if QCadastro.IsEmpty then Exit
  else prVerfEAbreCon(QHistorico);
  if fnQtdRegistro('N�o h� hist�rico dispon�vel!', QHistorico) then Exit;
  btnHist.Click;
  QHistorico.First;
end;

procedure TFCad.btnPriorCClick(Sender: TObject);
begin
  if QCadastro.IsEmpty then Exit
  else prVerfEAbreCon(QHistorico);
  if fnQtdRegistro('N�o h� hist�rico dispon�vel!', QHistorico) then Exit;
  btnHist.Click;
  QHistorico.Prior;
end;

procedure TFCad.btnNextCClick(Sender: TObject);
begin
  if QCadastro.IsEmpty then Exit
  else prVerfEAbreCon(QHistorico);
  if fnQtdRegistro('N�o h� hist�rico dispon�vel!', QHistorico) then Exit;
  btnHist.Click;
  QHistorico.Next;
end;

procedure TFCad.btnLastCClick(Sender: TObject);
begin
  if QCadastro.IsEmpty then Exit
  else prVerfEAbreCon(QHistorico);
  if fnQtdRegistro('N�o h� hist�rico dispon�vel!', QHistorico) then Exit;
  btnHist.Click;
  QHistorico.Last;
end;

procedure TFCad.DSHistoricoDataChange(Sender: TObject; Field: TField);
begin
  btnFirstC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnPriorC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnNextC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Eof);
  btnLastC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico  .Eof);
end;

procedure TFCad.QCadastroAfterOpen(DataSet: TDataSet);
begin
Barra.SimpleText := ' Registros encontrados: '+IntToStr(QCadastro.RecordCount);
HabilitarBotoes;
end;

procedure TFCad.QCadastroAfterScroll(DataSet: TDataSet);
begin
QHistorico.Close;
end;

procedure TFCad.QCadastroBeforeEdit(DataSet: TDataSet);
begin
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
  excluindo := False;
end;

procedure TFCad.QCadastroBeforeInsert(DataSet: TDataSet);
begin
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCad.QCadastroBeforeOpen(DataSet: TDataSet);
begin
  Barra.SimpleText := ' Buscando dados aguarde... ';
end;

procedure TFCad.QCadastroBeforePost(DataSet: TDataSet);
var Cadastro : String;

begin
  contadorDePosts := contadorDeposts + 1;
  Try
   flag := False;
   if QCadastro.State = dsInsert then
      colocouMensagem := DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',QCadastro.FieldByName(chavepri).AsString,'','')
      //colocouMensagem := DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',Self.Caption,iif(Trim(QCadastro.FieldByName(chavepri).AsString) = '','NULL',QCadastro.FieldByName(chavepri).AsString),StringReplace(detalhe,': ','',[rfReplaceAll]),'', '')
   else
   begin
      colocouMensagem := DMConexao.GravaLogCad(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Altera��o',QCadastro.FieldByName(chavepri).AsString,'', '');
      flag := True;
   end;
    { QCadastro.FieldByName('DTALTERACAO').AsDateTime := Now;
    QCadastro.FieldByName('OPERADOR').AsString := Operador.Nome;
    if QCadastro.FieldByName('APAGADO').AsString = 'S' then
       QCadastro.FieldByName('DTAPAGADO').AsDateTime := Now;}
    if QCadastro.State = dsInsert then
      ShowMessage('Cadastro realizado com sucesso!')
    else
      ShowMessage('Altera��o realizada com sucesso!');

  except on E:Exception do
    if e.Message = INFORMACOES_OCORRENCIA_OBRIGATORIO then
      msgInf(e.Message);
  end;
end;




end.
