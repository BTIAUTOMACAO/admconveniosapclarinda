unit UGeraCartaoBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, StdCtrls, Buttons, JvExControls, JvDBLookup,
  Menus, ExtCtrls, InvokeRegistry, Rio, SOAPHTTPClient, ComCtrls;

const DEBUG: boolean = false;

type
  datas =array of TDateTime;
  TFGeraCartaBemEstar = class(TF1)
    Panel1: TPanel;
    Label1: TLabel;
    edtEmpr: TEdit;
    DBEmpresa: TJvDBLookupCombo;
    btnProcessar: TBitBtn;
    QEmpresa: TADOQuery;
    DSEmpresa: TDataSource;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresafantasia: TStringField;
    edtCodEmpresaDestino: TEdit;
    lkpEmpresasDestino: TJvDBLookupCombo;
    Label2: TLabel;
    QEmpresaBemEstar: TADOQuery;
    DSEmpresaBemEstar: TDataSource;
    QQtdCartoes: TADOQuery;
    DSQtdCartoes: TDataSource;
    QQtdCartoesQtdCartoes: TIntegerField;
    QDatasFechamento: TADOQuery;
    DSDatasFecha: TDataSource;
    QExecProcedure: TADOQuery;
    QConvByEmpresId: TADOQuery;
    DSConvByEmpresId: TDataSource;
    QCartoes: TADOQuery;
    DSCartoes: TDataSource;
    QCartoesnome: TStringField;
    QCartoesconv_id: TIntegerField;
    QCartoesempres_id: TIntegerField;
    QConveniado: TADOQuery;
    QGeraConveniado: TADOQuery;
    QEmpresaBemEstarempres_id: TIntegerField;
    QEmpresaBemEstarnome: TStringField;
    QEmpresaBemEstarfantasia: TStringField;
    QConveniadoCARTAO_BEM_ESTAR: TStringField;
    QConveniadoEMPRES_ID: TIntegerField;
    QConveniadoBANCO: TIntegerField;
    QConveniadoGRUPO_CONV_EMP: TIntegerField;
    QConveniadoSENHA: TStringField;
    QConveniadoTITULAR: TStringField;
    QConveniadoCONTRATO: TIntegerField;
    QConveniadoLIMITE_MES: TBCDField;
    QConveniadoLIBERADO: TStringField;
    QConveniadoFIDELIDADE: TStringField;
    QConveniadoAPAGADO: TStringField;
    QConveniadoDT_NASCIMENTO: TDateTimeField;
    QConveniadoCARGO: TStringField;
    QConveniadoSETOR: TStringField;
    QConveniadoCPF: TStringField;
    QConveniadoRG: TStringField;
    QConveniadoLIMITE_TOTAL: TBCDField;
    QConveniadoLIMITE_PROX_FECHAMENTO: TBCDField;
    QConveniadoAGENCIA: TStringField;
    QConveniadoCONTACORRENTE: TStringField;
    QConveniadoDIGITO_CONTA: TStringField;
    QConveniadoTIPOPAGAMENTO: TStringField;
    QConveniadoENDERECO: TStringField;
    QConveniadoNUMERO: TIntegerField;
    QConveniadoCEP: TStringField;
    QConveniadoTELEFONE1: TStringField;
    QConveniadoTELEFONE2: TStringField;
    QConveniadoCELULAR: TStringField;
    QConveniadoOBS1: TStringField;
    QConveniadoOBS2: TStringField;
    QConveniadoEMAIL: TStringField;
    QConveniadoDTULTCESTA: TDateTimeField;
    QConveniadoSALARIO: TBCDField;
    QConveniadoTIPOSALARIO: TStringField;
    QConveniadoCOD_EMPRESA: TStringField;
    QConveniadoFLAG: TStringField;
    Panel2: TPanel;
    QGetEmpTaxasPadrao: TADOQuery;
    QCredenciados: TADOQuery;
    QCredenciadosCRED_ID: TIntegerField;
    QCredenciadosNOME: TStringField;
    QCredenciadosSEG_ID: TIntegerField;
    QCredenciadosLIBERADO: TStringField;
    QCredenciadosCODACESSO: TIntegerField;
    QCredenciadosCOMISSAO: TBCDField;
    QCredenciadosBANCO: TIntegerField;
    QCredenciadosDIAFECHAMENTO1: TWordField;
    QCredenciadosDIAFECHAMENTO2: TWordField;
    QCredenciadosVENCIMENTO1: TWordField;
    QCredenciadosVENCIMENTO2: TWordField;
    QCredenciadosAPAGADO: TStringField;
    QCredenciadosPAGA_CRED_POR_ID: TIntegerField;
    QCredenciadosCONTRATO: TIntegerField;
    QCredenciadosCGC: TStringField;
    QCredenciadosINSCRICAOEST: TStringField;
    QCredenciadosFANTASIA: TStringField;
    QCredenciadosTELEFONE1: TStringField;
    QCredenciadosTELEFONE2: TStringField;
    QCredenciadosFAX: TStringField;
    QCredenciadosCONTATO: TStringField;
    QCredenciadosENVIAR_EMAIL: TStringField;
    QCredenciadosEMAIL: TStringField;
    QCredenciadosHOMEPAGE: TStringField;
    QCredenciadosPOSSUICOMPUTADOR: TStringField;
    QCredenciadosENDERECO: TStringField;
    QCredenciadosNUMERO: TIntegerField;
    QCredenciadosCEP: TStringField;
    QCredenciadosMAQUINETA: TStringField;
    QCredenciadosTIPOFECHAMENTO: TStringField;
    QCredenciadosNOVOCODACESSO: TIntegerField;
    QCredenciadosSENHA: TStringField;
    QCredenciadosAGENCIA: TStringField;
    QCredenciadosCONTACORRENTE: TStringField;
    QCredenciadosOBS1: TStringField;
    QCredenciadosOBS2: TStringField;
    QCredenciadosCORRENTISTA: TStringField;
    QCredenciadosCARTIMPRESSO: TStringField;
    QCredenciadosFORNCESTA: TStringField;
    QCredenciadosCPF: TStringField;
    QCredenciadosACEITA_PARC: TStringField;
    QCredenciadosGERA_LANC_CPMF: TStringField;
    QCredenciadosENCONTRO_CONTAS: TStringField;
    QCredenciadosDTAPAGADO: TDateTimeField;
    QCredenciadosDTALTERACAO: TDateTimeField;
    QCredenciadosOPERADOR: TStringField;
    QCredenciadosDTCADASTRO: TDateTimeField;
    QCredenciadosOPERCADASTRO: TStringField;
    QCredenciadosVALE_DESCONTO: TStringField;
    QCredenciadosCOMPLEMENTO: TStringField;
    QCredenciadosDIA_PAGTO: TIntegerField;
    QCredenciadosTX_DVV: TBCDField;
    QCredenciadosUTILIZA_AUTORIZADOR: TStringField;
    QCredenciadosUTILIZA_COMANDA: TStringField;
    QCredenciadosUTILIZA_RECARGA: TStringField;
    QCredenciadosgrupo_estab_id: TIntegerField;
    QCredenciadosEXIBIR_ONDE_COMPRAR: TStringField;
    QCredenciadosEXIBIR_DIRF: TStringField;
    HTTPRIO1: THTTPRIO;
    QCartoessenha: TStringField;
    QCartoescodcartimp: TStringField;
    QDatas: TADOQuery;
    QInserirTransacao: TADOQuery;
    QInserirAutor: TADOQuery;
    QInserirContaCorrente: TADOQuery;
    QCartoescartao_id: TIntegerField;
    QCartoescodigo: TIntegerField;
    QGetConveniados: TADOQuery;
    QGetConveniadosCONV_ID: TIntegerField;
    QGetConveniadosEMPRES_ID: TIntegerField;
    QGetConveniadosBANCO: TIntegerField;
    QGetConveniadosGRUPO_CONV_EMP: TIntegerField;
    QGetConveniadosCHAPA: TFloatField;
    QGetConveniadosSENHA: TStringField;
    QGetConveniadosTITULAR: TStringField;
    QGetConveniadosCONTRATO: TIntegerField;
    QGetConveniadosLIMITE_MES: TBCDField;
    QGetConveniadosLIBERADO: TStringField;
    QGetConveniadosFIDELIDADE: TStringField;
    QGetConveniadosAPAGADO: TStringField;
    QGetConveniadosDT_NASCIMENTO: TDateTimeField;
    QGetConveniadosCARGO: TStringField;
    QGetConveniadosSETOR: TStringField;
    QGetConveniadosCPF: TStringField;
    QGetConveniadosRG: TStringField;
    QGetConveniadosLIMITE_TOTAL: TBCDField;
    QGetConveniadosLIMITE_PROX_FECHAMENTO: TBCDField;
    QGetConveniadosAGENCIA: TStringField;
    QGetConveniadosCONTACORRENTE: TStringField;
    QGetConveniadosDIGITO_CONTA: TStringField;
    QGetConveniadosTIPOPAGAMENTO: TStringField;
    QGetConveniadosENDERECO: TStringField;
    QGetConveniadosNUMERO: TIntegerField;
    QGetConveniadosCEP: TStringField;
    QGetConveniadosTELEFONE1: TStringField;
    QGetConveniadosTELEFONE2: TStringField;
    QGetConveniadosCELULAR: TStringField;
    QGetConveniadosOBS1: TStringField;
    QGetConveniadosOBS2: TStringField;
    QGetConveniadosEMAIL: TStringField;
    QGetConveniadosCESTABASICA: TBCDField;
    QGetConveniadosDTULTCESTA: TDateTimeField;
    QGetConveniadosSALARIO: TBCDField;
    QGetConveniadosTIPOSALARIO: TStringField;
    QGetConveniadosCOD_EMPRESA: TStringField;
    QGetConveniadosFLAG: TStringField;
    QGetConveniadosDTASSOCIACAO: TDateTimeField;
    QGetConveniadosDTAPAGADO: TDateTimeField;
    QGetConveniadosDTALTERACAO: TDateTimeField;
    QGetConveniadosOPERADOR: TStringField;
    QGetConveniadosDTCADASTRO: TDateTimeField;
    QGetConveniadosOPERCADASTRO: TStringField;
    QGetConveniadosVALE_DESCONTO: TStringField;
    QGetConveniadosLIBERA_GRUPOSPROD: TStringField;
    QGetConveniadosCOMPLEMENTO: TStringField;
    QGetConveniadosUSA_SALDO_DIF: TStringField;
    QGetConveniadosABONO_MES: TBCDField;
    QGetConveniadosSALDO_RENOVACAO: TBCDField;
    QGetConveniadosSALDO_ACUMULADO: TBCDField;
    QGetConveniadosDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QGetConveniadosCONSUMO_MES_1: TBCDField;
    QGetConveniadosCONSUMO_MES_2: TBCDField;
    QGetConveniadosCONSUMO_MES_3: TBCDField;
    QGetConveniadosCONSUMO_MES_4: TBCDField;
    QGetConveniadosPIS: TFloatField;
    QGetConveniadosNOME_PAI: TStringField;
    QGetConveniadosnome_mae: TStringField;
    QGetConveniadosCART_TRAB_NUM: TIntegerField;
    QGetConveniadosCART_TRAB_SERIE: TStringField;
    QGetConveniadosREGIME_TRAB: TStringField;
    QGetConveniadosVENC_TOTAL: TBCDField;
    QGetConveniadosESTADO_CIVIL: TStringField;
    QGetConveniadosNUM_DEPENDENTES: TIntegerField;
    QGetConveniadosDATA_ADMISSAO: TDateTimeField;
    QGetConveniadosDATA_DEMISSAO: TDateTimeField;
    QGetConveniadosFIM_CONTRATO: TDateTimeField;
    QGetConveniadosDISTRITO: TStringField;
    QGetConveniadosSALDO_DEVEDOR: TBCDField;
    QGetConveniadosSALDO_DEVEDOR_FAT: TBCDField;
    QGetConveniadosSETOR_ID: TIntegerField;
    QGetConveniadosCARTAO_BEM_ESTAR: TStringField;
    QDataFechaEmp: TADOQuery;
    QDataFechaCred: TADOQuery;
    ProgressBar1: TProgressBar;
    lblStatus: TLabel;
    QConveniadoCHAPA: TFloatField;
    QGeraCartao: TADOQuery;
    QGetEmpTaxasPadraoTAXA_ID: TIntegerField;
    QGetEmpTaxasPadraoFAIXA_INICIO: TIntegerField;
    QGetEmpTaxasPadraoFAIXA_FIM: TIntegerField;
    QGetEmpTaxasPadraoTAXA_VALOR: TBCDField;
    QGetEmpTaxasPadraoDTCADASTRO: TDateTimeField;
    QGetEmpTaxasPadraoOPERCADASTRO: TStringField;
    QGetConveniadosENVIA_SMS: TStringField;
    QGetConveniadosNAO_GERA_CARTAO_TITULAR: TStringField;
    QGetConveniadosTAXA_SERVICO_CANTINA: TStringField;
    QGetConveniadosSENHA_CANTINEX: TStringField;
    QGeraCartaoCARTAO_ID: TIntegerField;
    QGeraCartaoCONV_ID: TIntegerField;
    QGeraCartaoNOME: TStringField;
    QGeraCartaoLIBERADO: TStringField;
    QGeraCartaoCODIGO: TIntegerField;
    QGeraCartaoDIGITO: TWordField;
    QGeraCartaoTITULAR: TStringField;
    QGeraCartaoJAEMITIDO: TStringField;
    QGeraCartaoAPAGADO: TStringField;
    QGeraCartaoLIMITE_MES: TBCDField;
    QGeraCartaoCODCARTIMP: TStringField;
    QGeraCartaoPARENTESCO: TStringField;
    QGeraCartaoDATA_NASC: TDateTimeField;
    QGeraCartaoNUM_DEP: TIntegerField;
    QGeraCartaoFLAG: TStringField;
    QGeraCartaoDTEMISSAO: TDateTimeField;
    QGeraCartaoCPF: TStringField;
    QGeraCartaoRG: TStringField;
    QGeraCartaoVIA: TIntegerField;
    QGeraCartaoDTAPAGADO: TDateTimeField;
    QGeraCartaoDTALTERACAO: TDateTimeField;
    QGeraCartaoOPERADOR: TStringField;
    QGeraCartaoDTCADASTRO: TDateTimeField;
    QGeraCartaoOPERCADASTRO: TStringField;
    QGeraCartaoCRED_ID: TIntegerField;
    QGeraCartaoATIVO: TStringField;
    QGeraCartaoEMPRES_ID: TIntegerField;
    QGeraCartaoSENHA: TStringField;
    QCredenciadosBAIRRO: TIntegerField;
    QCredenciadosCIDADE: TIntegerField;
    QCredenciadosESTADO: TIntegerField;
    QConveniadoCONV_ID: TIntegerField;
    QConveniadoBAIRRO: TIntegerField;
    QConveniadoCIDADE: TIntegerField;
    QConveniadoESTADO: TIntegerField;
    QGetConveniadosBAIRRO: TIntegerField;
    QGetConveniadosCIDADE: TIntegerField;
    QGetConveniadosESTADO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function GetQuantidadeCartoesEmpresa(empres_id : Integer): Integer ;
    function HasLimiteByConveniado (empres_id : Integer; conv_id : Integer): Boolean ;
    function GetLimiteMesByConveniado(conv_id : Integer) : Double ;
    procedure GerarCartaoBemEstar(conv_id : Integer);
    procedure btnProcessarClick(Sender: TObject);
    procedure GerarConveniado(conv_id : Integer);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure edtEmprChange(Sender: TObject);
    procedure DBEmpresaChange(Sender: TObject);
    procedure QGeraCartaoBeforePost(DataSet: TDataSet);
    function PegarAutorizacao(cartao : String; precoMensalidade : String) : String;
    function ErroConectarWebService(mensagem: String): Boolean;
    procedure acertarDecimal;
    function InsereTaxasConveniadoBemEstar (conv_id :Integer; conv_id_be : Integer ;
    empres_id : Integer; empres_id_be : Integer) : Boolean ;
    function MontadorSql(id : Integer; conv_id: Integer; conv_id_be: Integer; empres_id: Integer; empres_id_be: Integer ) : String ;
    procedure lkpEmpresasDestinoChange(Sender: TObject);
    procedure edtCodEmpresaDestinoChange(Sender: TObject);
    function GetQtdDependentes (conv_id : Integer) : Integer ;
    procedure AlterarCampoCartaoBemEstar(Conv_id : Integer);
    function InserirTransacoes(CodAcesso : Integer; CredId : Integer;
    CodCartao : String; CartaoID : Integer; CPF: String; Operador: String; DataHora: TDateTime; Valor: Currency; EmpresId: Integer ) : String;
    function InserirAutor(DataHora : TDateTime; Valor: String;DataFechaEmp: TDateTime; DataVencEmp: TDateTime;DataFechaFor: TDateTime; DataVencFor: TDateTime;
    ConvID : Integer; CredID: Integer; EmpresId: Integer;CartaoID : Integer) : String;//    function getDatasArray(dia : Integer) : datas;
    function InserirContaCorrente(Data : TDate; Hora: TTime; DataVenda: TDateTime; ConvID: Integer; CartaoID: Integer;
    CredID : Integer; DataVencEmp: TDateTime; DataFechaEmp: TDateTime; DataVencFor : TDateTime; DataFechaFor : TDateTime; Empres_ID : Integer ) : String;
    function RebuildQCartao() : String;

  private
    { Private declarations }
    FocoCodFor : Boolean;
  public
     Lanc_Webservice, Path_WebService: string;


  end;

var
  FGeraCartaBemEstar: TFGeraCartaBemEstar;
  GetLastTransID : Integer;
  GetLastAutorID : Integer;


implementation

uses cartao_util, DateUtils, URotinasTexto, DM, UMenu, uDatas, wsconvenio;

{$R *.dfm}

procedure TFGeraCartaBemEstar.FormCreate(Sender: TObject);
begin
  inherited;
  QEmpresa.Open;
  QEmpresaBemEstar.Open;
  QCredenciados.Open;
  FMenu.vFGeraCartaBemEstar := True;

  //DOING
  //inherited;
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
  Screen.Cursor := crHourGlass;

  DMConexao.Config.Open;  //Define se o foco ap�s o lan�amentos vai para o fornecedor.
  FocoCodFor := (DMConexao.ConfigFLANCAMENTOS_FOCO_FORNEC.AsString = 'S');
  if DMConexao.ConfigFLANCAMENTOS_AUTOR_WEBSERVICE.AsString = 'S' then
  begin
    Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString;
    if Path_WebService <> '' then begin
      Path_WebService := Path_WebService + 'wsconvenio.asmx?wsdl';
      HTTPRIO1.URL := Path_WebService;
      HTTPRIO1.WSDLLocation := Path_WebService;
      HTTPRIO1.Service := 'wsconvenio';
      HTTPRIO1.Port :=  'wsconvenioSoap';
    end else
      msgInf('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');
  end;
  //informar_emp := (DMConexao.ConfigINFORMA_EMP_LANC_IND.AsString = 'S');
  DMConexao.Config.Close;
    //qLogLancIndv.Open;
  HTTPRIO1.HTTPWebNode.ConnectTimeout := 120000; //em milesegundos
  HTTPRIO1.HTTPWebNode.ReceiveTimeout := 120000;
  HTTPRIO1.HTTPWebNode.SendTimeout    := 120000;




end;

procedure TFGeraCartaBemEstar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFGeraCartaBemEstar := False;
end;

//<summary>Esta function retorna a QUANTIDADE DE CART�ES ativos da EMPRESA </summary>
//<param name = "empres_id">empres_id</params>
//<returns>Quantidade de cart�es liberados por empresa</returns>
function TFGeraCartaBemEstar.GetQuantidadeCartoesEmpresa(empres_id : Integer): Integer ;
var QtdCartoes : Integer;
begin
  QQtdCartoes.Parameters[0].Value := empres_id;
  QQtdCartoes.Open;
  Result := QQtdCartoesQtdCartoes.AsInteger;
end;

//<summary>Esta function retorna LIMITE DO CONVENENIADO </summary>
//<param name = "conv_id">conv_id</params>
//<returns>(Double) Vlaro R$ do limite do conveniado</returns>
function TFGeraCartaBemEstar.GetLimiteMesByConveniado(conv_id : Integer) : Double;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT * FROM DBO.getLimite('+IntToStr(conv_id)+')');
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].Value;
  
end;

//<summary>Esta fun��o verifica se o conveniado te limite nos pr�ximo 12 meses havaliando se ele tem parcelas a vencer e etc.</summary>
function TFGeraCartaBemEstar.HasLimiteByConveniado (empres_id : Integer; conv_id : Integer): Boolean ;
var debito_prox_fecha : Currency;
    hasSaldoByMonth   : Boolean;
    sp : TADOStoredProc;
begin
  //Carrega Data do proximo fechamento + 11 meses seguinte
  QDatasFechamento.Close;
  QDatasFechamento.SQL.Clear;
  QDatasFechamento.SQL.Add('select top 12 DATA_FECHA from DIA_FECHA where EMPRES_ID = '+IntToStr(empres_id)+'');
  QDatasFechamento.SQL.Add(' AND DATA_FECHA >= GETDATE();');
  QDatasFechamento.Open;
  //fim da consulta

  QDatasFechamento.First;
  while not QDatasFechamento.Eof do
  begin
    QExecProcedure.Parameters[0].Value := conv_id;
    QExecProcedure.Parameters[1].Value := QDatasFechamento.Fields[0].Value;
    QExecProcedure.Open;
    debito_prox_fecha := QExecProcedure.Fields[3].AsCurrency;
    hasSaldoByMonth := debito_prox_fecha < GetLimiteMesByConveniado(conv_id);

    if hasSaldoByMonth = True then
      QDatasFechamento.Next

    else
    begin
      QDatasFechamento.Last;
      hasSaldoByMonth := False;
    end;
  end;

  Result := hasSaldoByMonth;
end;

function TFGeraCartaBemEstar.RebuildQCartao() : String;
var cmdText : String;
begin
  cmdText := ''
  +'SELECT '
  +'cart.nome, '
  +'cart.conv_id, '
  +'cart.senha, '
  +'cart.codcartimp, '
  +'conv.empres_id, '
  +'cart.cartao_id, '
  +'cart.codigo '
  +'FROM CARTOES cart INNER JOIN CONVENIADOS conv '
  +'ON cart.conv_id = conv.conv_id AND conv.empres_id = :empres_id '
  +'WHERE conv.apagado = ''N'' and conv.liberado = ''S''  ';

  Result := cmdText;

end;


procedure TFGeraCartaBemEstar.btnProcessarClick(Sender: TObject);
var QtdCartoesEmpresa,i : Integer;
begin
  inherited;
  lblStatus.Caption := 'Gerando aquivo...';
  Application.ProcessMessages;
  QCartoes.Close;
  QCartoes.SQL.Clear;
  QCartoes.SQL.Add(RebuildQCartao);
  ProgressBar1.Position := 0;
  QtdCartoesEmpresa := GetQuantidadeCartoesEmpresa(DBEmpresa.KeyValue);
  QCartoes.Parameters.ParamByName('empres_id').Value := DBEmpresa.KeyValue;
  QCartoes.Open;
  while not QCartoes.Eof do
  begin
    IF(HasLimiteByConveniado(DBEmpresa.KeyValue,QCartoesconv_id.AsInteger)) THEN
    BEGIN
      GerarConveniado(QCartoesconv_id.AsInteger);
      QCartoes.Next;
      ProgressBar1.Position := ((QCartoes.RecNo*100) div QCartoes.RecordCount);
      ProgressBar1.Refresh;
    END
    ELSE begin
      QCartoes.Next;
      ProgressBar1.Position := ((QCartoes.RecNo*100) div QCartoes.RecordCount);
      ProgressBar1.Refresh;
    end;


  end;
  lblStatus.Caption := 'Exporta��o concluida com sucesso!';
  Application.ProcessMessages;
  ProgressBar1.Position := 0;
  ProgressBar1.CleanupInstance;
  ProgressBar1.Refresh;


end;

procedure TFGeraCartaBemEstar.edtEmprKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFGeraCartaBemEstar.edtEmprChange(Sender: TObject);
begin
  inherited;
  QEmpresaBemEstar.Requery();
  if Trim(edtEmpr.Text) <> '' then begin
     if QEmpresa.Locate('empres_id',edtEmpr.Text,[]) then
        DBEmpresa.KeyValue := edtEmpr.Text
     else begin
        DBEmpresa.ClearValue;
     end;
  end
  else begin
    DBEmpresa.ClearValue;
  end;
end;

procedure TFGeraCartaBemEstar.DBEmpresaChange(Sender: TObject);
begin
  inherited;
  edtEmpr.Text := QEmpresaempres_id.AsString;
end;

//<summary>Esta procedure replica para a tabela CONVENIADOS_BEM_ESTAR o registro do conveniado passado por par�metro</summary>
//<param name="conv_id"> conv_id </param>
procedure TFGeraCartaBemEstar.GerarConveniado(conv_id : Integer);
var ConvIdBe,i,credID  : Integer;
    ValorTaxa : String;
    currValorTaxa : Currency;
    today, dataVencimento : TDateTime;

begin

  QGetConveniados.Close;
  QGetConveniados.Parameters[0].Value := conv_id;
  QGetConveniados.Open;

  CurrencyDecimals := 02;
  DMConexao.AdoCon.BeginTrans;
  TRY
    with QGeraConveniado do begin
      QGeraConveniado.Parameters.ParamByName('conv_id').Value   := conv_id;
      ConvIdBe := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SCONV_ID');
      QGeraConveniado.Parameters.ParamByName('CONV_ID_BE').Value := ConvIdBe;
      QGeraConveniado.Parameters.ParamByName('EMPRES_ID').Value := lkpEmpresasDestino.KeyValue;
      QGeraConveniado.ExecSQL;
    end;

      //CHAMADA DO M�TODO GERAR CARTAO
    GerarCartaoBemEstar(ConvIdBe);

    //CHAMADA DO M�TODO INSERE TAXAS_PADR�O VINCULADOS AO CONV_ID E CONV_ID_BE
    InsereTaxasConveniadoBemEstar(conv_id,ConvIdBe,DBEmpresa.KeyValue,lkpEmpresasDestino.KeyValue);

    //CHAMADA DO M�TODO ATUALIZA CAMPO "CARTAO_BEM_ESTAR" NA TABELA ONVENIADOS VALORES POSS�VEIS = 'S' OU 'N'
    AlterarCampoCartaoBemEstar(conv_id);

    //CHAMADA DO M�TODO PEGAR AUTORIZA��O PARAMS <'CARTAO_NUM'> ; <'PRECO_MENSALIDADE'>

  Except
    DMConexao.AdoCon.RollbackTrans;
    ShowMessage('Ocorreu algum erro durante a importa��o,tente novamente ou contacte o suporte.');
    Exit;
  END;
  DMConexao.ADOCon.CommitTrans;

  QDatas.Close;
  QDatas.SQL.Clear;
  QDatas.SQL.Add('EXEC getDatas12MesesApos '+QuotedStr(DateToStr(Now))+'');
  QDatas.SQL.Text;
  QDatas.Open;
  credID := DMConexao.ExecuteQuery('SELECT CRED_ID_BEM_ESTAR FROM CONFIG');
  while not QDatas.Eof do
  begin
    ValorTaxa := FormatFloat('#,##0.00',QGetEmpTaxasPadraoTAXA_VALOR.AsFloat);
    QInserirTransacao.SQL.Add(InserirTransacoes(QCredenciadosCODACESSO.AsInteger,credID,QCartoescodigo.AsString,QCartoescartao_id.AsInteger,QGetConveniadosCPF.AsString,Operador.Nome,QDatas.Fields[0].AsDateTime,StrToCurr(ValorTaxa),DBEmpresa.KeyValue));
    QInserirTransacao.ExecSQL;

    QDataFechaEmp.SQL.Add('select top(1)data_fecha,data_venc from DIA_FECHA where DATA_FECHA > '+QuotedStr(QDatas.Fields[0].AsString)+' and empres_id = '+DBEmpresa.KeyValue+'');
    QDataFechaEmp.Open;
    QDataFechaCred.SQL.Add('select top(1)data_fecha,data_venc from DIA_FECHA_CRED where DATA_FECHA > '+QuotedStr(QDatas.Fields[0].AsString)+' and cred_id = '+IntToStr(credID)+'');
    QDataFechaCred.Open;

    //Inserindo Autor
    //<params> DataHora : TDateTime; Valor: Currency; DataFechaEmp: TDateTime; DataFechaFor: TDateTime;
    //<params>DataVencFor: TDateTime;
    //<params>ConvID : Integer; CredID: Integer; EmpresId: Integer; CartaoID : Integer ) : String;
    QInserirAutor.SQL.Add(InserirAutor(QDatas.Fields[0].AsDateTime,ValorTaxa,QDataFechaEmp.Fields[0].AsDateTime,QDataFechaEmp.Fields[1].AsDateTime,QDataFechaCred.Fields[0].AsDateTime,QDataFechaCred.Fields[1].AsDateTime,QGetConveniadosCONV_ID.AsInteger,credID,DBEmpresa.KeyValue,QCartoescartao_id.AsInteger));
    QInserirAutor.ExecSQL;

    QInserirContaCorrente.SQL.Add(InserirContaCorrente(QDatas.Fields[0].AsDateTime,TimeOf(QDatas.Fields[0].Value),QDatas.Fields[0].AsDateTime,QGetConveniadosCONV_ID.AsInteger,QCartoescartao_id.AsInteger,credID,QDataFechaEmp.Fields[1].Value,QDataFechaEmp.Fields[0].Value,QDataFechaCred.Fields[1].Value,QDataFechaCred.Fields[0].Value,DBEmpresa.KeyValue));
    QInserirContaCorrente.ExecSQL;

    QDataFechaEmp.Close;
    QDataFechaEmp.SQL.Clear;
    QDataFechaCred.SQL.Clear;
    QDataFechaCred.Close;
    QInserirTransacao.Close;
    QInserirTransacao.SQL.Clear;
    QInserirAutor.Close;
    QInserirAutor.SQL.Clear;
    QInserirContaCorrente.Close;
    QInserirContaCorrente.SQL.Clear;

    QDatas.Next;
  end;


end;

procedure TFGeraCartaBemEstar.GerarCartaoBemEstar(conv_id : Integer);
begin
  QConveniado.Close;
  QConveniado.Parameters.ParamByName('conv_id').Value :=  conv_id;
  QConveniado.Open;
  QGeraCartao.SQL.Text;
  QGeraCartao.Open;
  QGeraCartao.Append;
  QGeraCartaoCONV_ID.AsInteger := conv_id;
  QGeraCartao.Post;
end;

procedure TFGeraCartaBemEstar.QGeraCartaoBeforePost(DataSet: TDataSet);
var codimp: string;
    qtdEncontrados : Integer;
begin
  inherited;
  //PREENCHE REGISTRO DA TABELA CART�ES COM DADOS DA TABELA CONVENIADOS
  QGeraCartaoCARTAO_ID.AsInteger   := DMConexao.ExecuteQuery('select next value for SCARTAO_ID');
  QGeraCartaoTITULAR.AsString      := UpperCase('S');
  QGeraCartaoNOME.AsString         := UpperCase(QConveniadoTITULAR.AsString);
  QGeraCartaoAPAGADO.AsString      := 'N';
  QGeraCartaoLIBERADO.AsString     := 'S';
  QGeraCartaoJAEMITIDO.AsString    := 'N';
  QGeraCartaoLIMITE_MES.AsFloat    := 0;
  QGeraCartaoEMPRES_ID.AsInteger   := lkpEmpresasDestino.KeyValue;
  QGeraCartaoCODIGO.AsInteger      := DMConexao.GeraCartao(QGeraCartaoCONV_ID.AsInteger);
  QGeraCartaoDIGITO.AsInteger      := DigitoCartao(QGeraCartaoCODIGO.AsFloat);

   DMConexao.AbrirCongis;
//   if DMConexao.ExecuteScalar(' SELECT coalesce(usa_cod_importacao,''N'') as usa_cod_importacao FROM empresas ' +
//                        ' where empres_id = (select empres_id from conveniados where conv_id = ' +
//                        QGeraCartaoCONV_ID.AsString + ')') = 'S' then begin
     repeat
//       if not IsTipoCreditoCantinex(DMConexao.ExecuteQuery('SELECT CONV.EMPRES_ID FROM CONVENIADOS CONV, '+
//              'CARTOES CART WHERE CONV.CONV_ID = CART.CONV_ID AND CART.CONV_ID = '+QGeraCartaoCONV_ID.AsString+'')) then
//       begin
//          codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigBIN_BEM_ESTAR.AsInteger));
//          DMConexao.AdoQry.Close;
//          DMConexao.AdoQry.SQL.Clear;
//          DMConexao.AdoQry.SQL.Add('SELECT BIN_BEM_ESTAR FROM CONFIG WHERE BIN_BEM_ESTAR = '+QuotedStr(codimp));
//          DMConexao.AdoQry.Open;
//       END
//       else
//       begin
          codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigBIN_BEM_ESTAR.AsInteger));
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT bin_cantina FROM CONFIG WHERE bin_cantina = '+QuotedStr(codimp));
          DMConexao.AdoQry.Open;
       //end;
       if(DMConexao.AdoQry.Fields[0].IsNull) then
       begin
         qtdEncontrados := -1
       end
       else
         qtdEncontrados := DMConexao.AdoQry.Fields[0].Value;
     until (qtdEncontrados = -1);
     QGeraCartaoCODCARTIMP.AsString := codimp;
   //end
//   else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
//      QGeraCartaoCODCARTIMP.AsString := QGeraCartaoCODIGO.AsString
//   else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then begin
//      if QGeraCartaoTITULAR.AsString = 'S' then
//         codimp := DMConexao.ObterCodCartImp
//      else
//         codimp := DMConexao.ObterCodCartImp(False);
//      QGeraCartaoCODCARTIMP.AsString := codimp;
//   end
//   else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
//      QGeraCartaoCODCARTIMP.AsString:= DMConexao.ObterCodCartImpMod1(QGeraCartaoCONV_ID.AsInteger,
//      DMConexao.ExecuteScalar('select empres_id from conveniados where conv_id = '+QGeraCartaoCONV_ID.AsString),
//      DMConexao.ExecuteScalar('select chapa from conveniados where conv_id = '+QGeraCartaoCONV_ID.AsString))
//   else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then begin
//      codimp := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
//      QGeraCartaoCODCARTIMP.AsString := codimp;
//   end;
   DMConexao.FecharConfigs;

   QGeraCartaoCODCARTIMP.AsString   := SoNumero(QGeraCartaoCODCARTIMP.AsString);
   QGeraCartao.FieldByName('DTCADASTRO').AsDateTime := Now;
   QGeraCartao.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
   
   if DMConexao.ConfigSENHA_CONV_ID.AsString = 'S' then
      QGeraCartaoSENHA.AsString := Crypt('E',QGeraCartaoSENHA.AsString,'BIGCOMPRAS')
   else
      QGeraCartaoSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
end;

function TFGeraCartaBemEstar.GetQtdDependentes (conv_id : Integer) : Integer ;
begin
  Result := DMConexao.ExecuteQuery('SELECT count(*) FROM cartoes WHERE conv_id = '+IntToStr(conv_id)+' ');
end;


procedure TFGeraCartaBemEstar.lkpEmpresasDestinoChange(Sender: TObject);
begin
  inherited;
  edtCodEmpresaDestino.Text := QEmpresaBemEstarempres_id.AsString;
end;

procedure TFGeraCartaBemEstar.edtCodEmpresaDestinoChange(Sender: TObject);
begin
  inherited;
  QEmpresaBemEstar.Requery();
  if Trim(edtCodEmpresaDestino.Text) <> '' then begin
     if QEmpresaBemEstar.Locate('empres_id',edtCodEmpresaDestino.Text,[]) then
        lkpEmpresasDestino.KeyValue := edtCodEmpresaDestino.Text
     else begin
        lkpEmpresasDestino.ClearValue;
     end;
  end
  else begin
    lkpEmpresasDestino.ClearValue;
  end;
end;

function TFGeraCartaBemEstar.InsereTaxasConveniadoBemEstar (conv_id :Integer; conv_id_be : Integer ;
                                            empres_id : Integer; empres_id_be : Integer) : Boolean ;
var NumDependentes, ID : Integer;
    SqlInsert : String;
begin
  NumDependentes := GetQtdDependentes(conv_id_be);
  ID := DMConexao.ExecuteScalar('SELECT NEXT VALUE FOR SCONVENIADOS_BEM_ESTAR_ID');

  //Retorna taxa bem estar padr�o verificando a faixa de dependentes
  QGetEmpTaxasPadrao.Parameters.ParamByName('qtdDependente').Value := NumDependentes;
  QGetEmpTaxasPadrao.Open;

  //Insere na tabela conveniados bem estar os dados de acordo com os dados passados por par�metro
  DMConexao.AdoQry.Close;
   DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(MontadorSql(ID,conv_id,conv_id_be,empres_id,empres_id_be));
  DMConexao.AdoQry.SQL.Text;
  DMConexao.AdoQry.ExecSQL;
end;

function TFGeraCartaBemEstar.MontadorSql(id : Integer; conv_id: Integer; conv_id_be: Integer; empres_id: Integer; empres_id_be: Integer ) : String ;
var SqlInsert : String;
    ValorTaxa : String;
begin
  ValorTaxa := CurrToStr(QGetEmpTaxasPadraoTAXA_VALOR.AsCurrency);
  ValorTaxa := StringReplace(ValorTaxa,',','.',[rfReplaceAll, rfIgnoreCase]);
  SqlInsert := ''
  +'INSERT INTO [dbo].[CONVENIADOS_BEM_ESTAR_HAS_EMP_TAXAS] '
  +'           ([ID] '
  +'           ,[CONV_ID] '
  +'           ,[CONV_ID_BE] '
  +'           ,[EMPRES_ID] '
  +'           ,[EMPRES_ID_BE] '
  +'           ,[TAXA_VALOR] '
  +'           ,[DTCADASTRO] '
  +'           ,[OPERCADASTRO] '
  +'           ,[LIBERADO] '
  +'           ,[TAXA_ID] '
  +'           ,[APAGADO]) '
  +'     VALUES '
  +'           ('+intToStr(ID)
  +'           ,'+intToStr(conv_id)
  +'           ,'+intToStr(conv_id_be)
  +'           ,'+intToStr(empres_id)
  +'           ,'+intToStr(empres_id_be)
  +'           ,'+ValorTaxa
  +'           ,'+QuotedStr(DateToStr(Date))
  +'           ,'+QuotedStr(Operador.Nome)
  +'           ,'+QuotedStr('S')
  +'           ,'+QGetEmpTaxasPadraoTAXA_ID.AsString
  +'           ,'+QuotedStr('N')+')';

  Result := SqlInsert;
end;

procedure TFGeraCartaBemEstar.acertarDecimal;
begin
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
end;

procedure TFGeraCartaBemEstar.AlterarCampoCartaoBemEstar(Conv_id : Integer);

begin
  DMConexao.ExecuteSql('UPDATE CONVENIADOS SET cartao_bem_estar = '+QuotedStr('S')+' WHERE CONV_ID = '+IntToStr(Conv_id)+'');
end;

function TFGeraCartaBemEstar.ErroConectarWebService(mensagem: String): Boolean;
begin
  Result := False;
  if Pos('A connection with the server could not be established',mensagem) > 0 then begin
    MsgErro('N�o foi poss�vel estabelecer uma conex�o com o servidor,' + sLineBreak +
      'verifique se a op��o "Caminho do Web Service" est� com um caminho v�lido ' + sLineBreak +
      'caso o problema persista entre em contato com o suporte t�cnico');
    Result := True;
  end;
end;

//function TFGeraCartaBemEstar.getDatasArray() : datas
//var dataAtual, getData : TDateTime;
//    dia, mes, ano : Word;
//    qtdMeses : Integer;
//    mesInicial,i : Integer;
//    datasGeradas : datas;
//
//begin
//  i := 1;
//  qtdMeses := 12;
//  mesInicial := mes;
//  dataAtual := Now;
//  DecodeDateTime(dataAtual,ano,mes,ano);
//  for i:=1 to qtdMeses do
//  begin
//    getData := EncodeDateTime(ano,mes,dia);
//    mes := mes + 1;
//    datasGeradas[i] :=  getData;
//  end;
//
//end;

function TFGeraCartaBemEstar.PegarAutorizacao(cartao : String; precoMensalidade : String) : String;
var aut, Retorno, valor, codigo, digito, get, senha, numAut, digAut, autForm : string;
    ptsFidel, transId, I,J, pIni, pFin : integer;
    Saldo : Currency;
    Fidelidade : Boolean;
    cred : TCredenciado;
    produto : TProduto;
    produtos : TProdutos;
    prescritor : TPrescritor;
    prescredProd : TPrescrProd;
    prescredsProd : TPrescrsProd;
    abrirTransacao : TAbrirTransacao; abrirTrasnacaoRet : TAbrirTransacaoRet;
    validProd : TValidarProdutos; valdProdRet : TValidarProdutosRet;
    FecharTransacao : TFecharTransacao;    FecharTransacaoRet : TFecharTransacaoRet;
    ConfirmarTransacao : TConfirmarTransacao; ConfirmarTransacaoRet : TConfirmarTransacaoRet;
    cancelarTrans : TCancelarTrasnacao;  cancelarTransRet : TCancelarTransacaoRet;
    bm : TBookmark;
    sl : TStrings;
begin
  if Trim(Path_WebService) = '' then begin
  		MsgErro('Caminho do webservice n�o encontrado, favor atualizar suas configura��es.');
	  	Exit;
	end;
  try
    if (DEBUG) then
       sl := TStringList.Create;
    try
    //MONTANDO O XML DE ABERTURA DA TRANSA��O
    cred := TCredenciado.Create(QCredenciadosCODACESSO.AsInteger, Crypt('D',QCredenciadosSENHA.AsString,'BIGCOMPRAS'));
    acertarDecimal;
	  abrirTransacao := TAbrirTransacao.Create(cred,cartao, '', 'ADM:'+Operador.Nome,Copy('N',1,1));
    acertarDecimal;
    try
      //get := validProd.getXML;
      if (DEBUG) then begin
        sl.add('ABRIR TRANSACAO');
        sl.Add(abrirTransacao.getXML);
      end;
      Retorno := (HTTPRIO1 as wsconvenioSoap).MAbrirTransacao(abrirTransacao.getXML);
      if (DEBUG) then begin
        sl.Add('RETORNO ABRIR TRANSACAO');
        sl.Add(Retorno);
      end;
    except
      on e:Exception do begin
		  	if not ErroConectarWebService(e.Message) then begin
//          reAut.Lines.Add('ERRO AO SOLICITAR TRANSA��O');
				  MsgErro('Erro ao abrir transa��o.'+sLineBreak+
  						'Erro: '+e.Message+sLineBreak+
	  					'Favor entrar em contato com o suporte t�cnico');
				  //lblAut.Caption := '';
          //edtDebito.Enabled := true;
          if (DEBUG) then begin
            sl.SaveToFile('logXmlWsConvenio.txt');
            sl.Free;
          end;
  				Exit;
	  		end else
		  		Exit;
			  end;
    end;
    abrirTrasnacaoRet := TAbrirTransacaoRet.Create(Retorno);

    if (abrirTrasnacaoRet.Status = 0) then begin
			Fidelidade:= abrirTrasnacaoRet.Fidelidade;
  		if Fidelidade then
	  	ptsFidel := abrirTrasnacaoRet.SaldoPontos;
		  transId := abrirTrasnacaoRet.TransId;
  	end
    else
    begin
      //reAut.Lines.Add('ERRO AO SOLICITAR TRANSA��O');
      MsgErro('N�o foi poss�vel abrir a transa��o.'+sLineBreak+
					'Motivo: '+abrirTrasnacaoRet.Mensagem+sLineBreak);
		  	//lblAut.Caption := '';
      //edtDebito.Enabled := true;
			exit;
  	end;

    produtos := TProdutos.Create(StrToInt(fnRemoveCasasDecimais((precoMensalidade))));
//    produto :=
//    TProduto.Create('''','''',1,
//    StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',precoMensalidade))),
//    StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',precoMensalidade))),
//    StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',precoMensalidade))),
//    0);
    //produtos.Add(produto);
    validProd := TValidarProdutos.Create(cred,cartao,'',transId,produtos);
    acertarDecimal;

    try
      get := validProd.getXML;
      if (DEBUG) then begin
        sl.Add('VALIDAR PRODUTOS');
        sl.Add(get);
      end;
      //reAut.Lines.Add('VALIDANDO TRANSA��O');
      Retorno := (HTTPRIO1 as wsconvenioSoap).MValidarProdutos(get);
      if (DEBUG) then begin
        sl.Add('RETORNO VALIDAR PRODUTOS');
        sl.Add(Retorno);
      end;
    except
      on e:Exception do
      begin
      if not ErroConectarWebService(e.Message) then begin
        //reAut.Lines.Add('ERRO AO VALIDAR TRANSA��O');
        MsgErro('Erro ao validar produtos.'+sLineBreak+
            'Erro: '+e.Message+sLineBreak+
        'Favor entrar em contato com o suporte t�cnico');
      //lblAut.Caption := '';
       //edtDebito.Enabled := true;
        if (DEBUG) then begin
          sl.SaveToFile('logXmlWsConvenio.txt');
          sl.Free;
       end;
        Exit;
      end else
        Exit;
      end;
    end;
    valdProdRet := TValidarProdutosRet.Create(Retorno);

    if valdProdRet.STATUS = 0 then begin
	  	  if Fidelidade then
  		    ptsFidel := ptsFidel + valdProdRet.PontosTrans;
   			transId := valdProdRet.TransId;
    end else begin
        //reAut.Lines.Add('ERRO AO VALIDAR TRANSA��O');
  		  MsgErro('N�o foi poss�vel validar produtos.'+sLineBreak+
  	  			'Motivo: '+valdProdRet.Mensagem+sLineBreak);
	  	  cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
 		  	try
        if (DEBUG) then begin
          sl.Add('CANCELAR TRANSACAO');
          sl.Add(cancelarTrans.getXML);
        end;
    		Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
        if (DEBUG) then begin
          sl.Add('RETORNO CANCELAR TRANSACAO');
          sl.Add(Retorno);
        end;
        //edtDebito.Enabled := true;
        acertarDecimal;
        acertarDecimal;
	    	except
  		    on e:Exception do	begin
  		  	  MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
	  		  	  	'Erro: '+e.Message+sLineBreak);
  	  	  	//lblAut.Caption := '';
            //edtDebito.Enabled := true;
            if (DEBUG) then begin
              sl.SaveToFile('logXmlWsConvenio.txt');
              sl.Free;
            end;
	  	  	  Exit;
 		  	  end;
    		end;
	    	cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
 		  	if cancelarTransRet.Status <> 0 then
  		  begin
  	  	MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	  	  		'Motivo: '+cancelarTransRet.Mensagem+sLineBreak);
   		  //lblAut.Caption := '';
        //edtDebito.Enabled := true;
    		exit;
	    	end;
  		  //lblAut.Caption := '';
 	  		exit;
  	  end;

//      Verificar este trecho
//      if (DMConexao.ExecuteScalar('SELECT COALESCE(ECOS.obrigar_senha,''N'') FROM EMP_CRED_OBRIGA_SENHA ECOS WHERE ECOS.EMPRES_ID = ' + cbbEmpr.KeyValue + ' AND ECOS.CRED_ID = ' + cbbEstab.KeyValue,'N') = 'S') then begin
//	  		senha := DMConexao.ExecuteScalar('SELECT senha FROM conveniados where apagado <> ''S'' and conv_id in (SELECT conv_id FROM cartoes card WHERE (card.codigo = ' +  qConvCODIGO.AsString + ' and digito = ' + qConvDIGITO.AsString + ') OR ( codcartimp = ''' + qConvCODCARTIMP.AsString + '''))');
//		  end else
//			  senha := '';
      get := '0';

      FecharTransacao := TFecharTransacao.Create(cred,cartao,'',transId,
		  cartao_util.iif(senha = '', '',Crypt('D',QCartoessenha.AsString,'BIGCOMPRAS')),
			1, produtos.ValorTotal,'1234');

      try
	  		get := FecharTransacao.getXML;
        if (DEBUG) then begin
          sl.Add('FECHAR TRANSACAO');
          sl.Add(get);
        end;
        acertarDecimal;
			  Retorno := (HTTPRIO1 as wsconvenioSoap).MFecharTransacao(get);
        acertarDecimal;
        if (DEBUG) then begin
          sl.Add('RETORNO FECHAR TRANSACAO');
          sl.Add(Retorno);
        end;
		  except
			  on e:Exception do
  			begin
	  		if not ErroConectarWebService(e.Message) then begin
			  	MsgErro('Erro ao validar produtos.'+sLineBreak+
				  		'Erro: '+e.Message+sLineBreak+
					  	'Favor entrar em contato com o suporte t�cnico');
//		  		lblAut.Caption := '';
//          edtDebito.Enabled := true;
          if (DEBUG) then begin
            sl.SaveToFile('logXmlWsConvenio.txt');
            sl.Free;
          end;
			  	Exit;
  			end else
	  			Exit;
		  	end;
  		end;

      FecharTransacaoRet := TFecharTransacaoRet.Create(Retorno);
		  if FecharTransacaoRet.Status = 0 then
  		begin
	  		if FecharTransacaoRet.CupomAut.Count = 0 then
	  	end
		  else
  		begin
		  	cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
        //reAut.Lines.Add('ERRO AO FECHAR TRANSA��O');
        MsgErro('Erro ao fechar transa��o.'+sLineBreak+
			     			'Erro: '+ 'Problema na autoriza��o: '+StringReplace(FecharTransacaoRet.Mensagem,''#$A' ',sLineBreak,[rfReplaceAll])+sLineBreak+FormatDateTime('dd/mm/yyyy hh:nn:ss',Now));
  			try
        acertarDecimal;
        acertarDecimal;
        if (DEBUG) then begin
          sl.Add('CANCELAR TRANSACAO');
          sl.Add(cancelarTrans.getXML);
        end;
	  		Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
        if (DEBUG) then begin
          sl.Add('RETORNO CANCELAR TRANSACAO');
          sl.Add(Retorno);
        end;
		  	except
  		  	on e:Exception do	begin
	  		  	if not ErroConectarWebService(e.Message) then begin
		    	  	MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
			    	  		'Erro: '+e.Message+sLineBreak);
		  		    //lblAut.Caption := '';
              //edtDebito.Enabled := true;
    	  			Exit;
	  		  	end else
	  	  	  	Exit;
  		  	end;
	  		end;
        acertarDecimal;
		  	cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
        acertarDecimal;
			  if cancelarTransRet.Status <> 0 then begin
          MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	    				'Motivo: '+cancelarTransRet.Mensagem+sLineBreak); //se n funcionar eh pq eh RET 1
		  	  //lblAut.Caption := '';
          //edtDebito.Enabled := true;
  			  exit;
	  		end;
      	//lblAut.Caption := '';
        //edtDebito.Enabled := true;
	      exit;
  		end;

      get := '0';

      ConfirmarTransacao:= TConfirmarTransacao.Create(cred,cartao,'',transId,StrToInt(get));
  		try
      if (DEBUG) then begin
        sl.Add('CONFIRMAR TRANSACAO');
        sl.Add(ConfirmarTransacao.getXML);
      end;
      acertarDecimal;
		  Retorno := (HTTPRIO1 as wsconvenioSoap).MConfirmarTransacao(ConfirmarTransacao.getXML);
      acertarDecimal;
      if (DEBUG) then begin
        sl.Add('RETORNO CONFIRMAR TRANSACAO');
        sl.Add(Retorno);
      end;

  		except
    		on e:Exception do	begin
		    	if not ErroConectarWebService(e.Message) then begin
  			    MsgErro('Erro ao confirmar transa��o.'+sLineBreak+
	  			    	'Erro: '+e.Message+sLineBreak+
		  			    'Favor entrar em contato com o suporte t�cnico');
//  	  		  lblAut.Caption := '';
//            edtDebito.Enabled := true;
    	  		Exit;
  	  	  end else
	      	  Exit;
    		end;
	  	end;
  		ConfirmarTransacaoRet := TConfirmarTransacaoRet.Create(Retorno);
	  	if ConfirmarTransacaoRet.Status <> 0 then	begin
       //reAut.Lines.Add('ERRO AO CONFIRMAR TRANSA��O');
  		  MsgErro('N�o foi poss�vel confirmar a transa��o.'+sLineBreak+
	  		  	'Motivo: '+ConfirmarTransacaoRet.Mensagem+sLineBreak);
  		  cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
    		try
	  	  	Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
  		  except
    			on e:Exception do	begin
      			if not ErroConectarWebService(e.Message) then begin
		  	    	MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
			  	    		'Erro: '+e.Message+sLineBreak);
//      				lblAut.Caption := '';
//              edtDebito.Enabled := true;
		      		Exit;
    	  		end else
		      		Exit;
    			end;
	    	end;
		    cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
  		  if cancelarTransRet.Status <> 0 then begin
  			  MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	  			  	'Motivo: '+cancelarTransRet.Mensagem+sLineBreak);
//  		  	lblAut.Caption := '';
//          edtDebito.Enabled := true;
	  		  exit;
  	  	end;
//		    lblAut.Caption := '';
    		exit;
	  	end;
      numAut := DMConexao.ExecuteScalar('SELECT AUTORIZACAO_ID FROM CONTACORRENTE WHERE TRANS_ID = ' + IntToStr(transId));
      digAut := DMConexao.ExecuteScalar('SELECT DIGITO FROM CONTACORRENTE WHERE TRANS_ID = ' + IntToStr(transId));

      finally
  		if abrirTransacao <> nil then
	  		FreeAndNil(abrirTransacao);
		  if abrirTrasnacaoRet <> nil then
  			FreeAndNil(abrirTrasnacaoRet);

		  if validProd <> nil then
  			FreeAndNil(validProd);
	  	if valdProdRet <> nil then
  			FreeAndNil(valdProdRet);

	  	if FecharTransacao <> nil then
		  	FreeAndNil(FecharTransacao);
  		if FecharTransacaoRet <> nil then
	  		FreeAndNil(FecharTransacao);

  		if ConfirmarTransacao <> nil then
	  		ConfirmarTransacao.Free;
		  if ConfirmarTransacaoRet <> nil then
			  ConfirmarTransacaoRet.Free;

	  	if cancelarTrans <> nil then
		  	FreeAndNil(cancelarTrans);
  		if cancelarTransRet <> nil then
	  		FreeAndNil(cancelarTransRet);
		end;
  finally
      if (DEBUG) then begin
        sl.SaveToFile('logXmlWsConvenio.txt');
        sl.Free;
      end;
  end;
end;

function TFGeraCartaBemEstar.InserirTransacoes(CodAcesso : Integer; CredId : Integer;
                                                  CodCartao : String; CartaoID : Integer; CPF: String; Operador: String; DataHora: TDateTime; Valor: Currency; EmpresId: Integer ) : String;
var strSql : String;
    ValorTaxa : string;
begin
  ValorTaxa := FormatFloat('#,##0.00',QGetEmpTaxasPadraoTAXA_VALOR.AsCurrency);
  ValorTaxa := StringReplace(ValorTaxa,',','.',[rfReplaceAll, rfIgnoreCase]);
  GetLastTransID := 0;
  GetLastTransID := DMConexao.ExecuteScalar('SELECT NEXT VALUE FOR STRANS_ID');
  strSql := ''
  +'INSERT INTO dbo.TRANSACOES '
  +'           (TRANS_ID '
  +'           ,CODACESSO '
  +'           ,SENHA '
  +'           ,CRED_ID '
  +'           ,CARTAO '
  +'           ,CARTAO_ID '
  +'           ,CPF '
  +'           ,OPERADOR '
  +'           ,DATAHORA '
  +'           ,ABERTA '
  +'           ,CONFIRMADA '
  +'           ,CANCELADO '
  +'           ,DTCANCELADO '
  +'           ,OPERCANCELADO '
  +'           ,VALOR '
  +'           ,PONTOS '
  +'           ,CUPOM '
  +'           ,VALE_ACUMULADO '
  +'           ,VALE_UTILIZADO '
  +'           ,DTCONFIRMADA '
  +'           ,NSU '
  +'           ,ENTREGA '
  +'           ,EMPRES_ID '
  +'           ,ACCEPTORTAN) '
  +'     VALUES '
  +'           ('+IntToStr(GetLastTransID)+' '
  +'           ,'+IntToStr(CodAcesso)+' '
  +'           ,'+''''''+''
  +'           ,'+IntToStr(CredID)+' '
  +'           ,'+QuotedStr(CodCartao)+''
  +'           ,'+IntToStr(CartaoId)+''
  +'           ,'+QuotedStr(CPF)+''
  +'           ,'+QuotedStr(Operador)+''
  +'           ,'+QuotedStr(DateTimeToStr(DataHora))+''
  +'           ,'+'''N'''+''
  +'           ,'+'''S'''+''
  +'           ,'+'''N'''+''
  +'           ,'+''''''+''
  +'           ,'+''''''+''
  +'           ,'+ValorTaxa+''
  +'           ,'+'0'+''
  +'           ,'+''''''+''
  +'           ,'+'0'+''
  +'           ,'+'0'+''
  +'           ,'+''''''+''
  +'           ,'+'0'+''
  +'           ,'+''''''+''
  +'           ,'+IntToStr(EmpresId)+''
  +'           ,'+'0'+')'
  ;
  Result := strSql;
end;

function TFGeraCartaBemEstar.InserirAutor(DataHora : TDateTime; Valor: String; DataFechaEmp: TDateTime;DataVencEmp: TDateTime; DataFechaFor: TDateTime; DataVencFor: TDateTime;
                                         ConvID : Integer; CredID: Integer; EmpresId: Integer; CartaoID : Integer ) : String;
var strSql : String;
ValorTaxa : string;
begin
  ValorTaxa := FormatFloat('#,##0.00',QGetEmpTaxasPadraoTAXA_VALOR.AsCurrency);
  ValorTaxa := StringReplace(ValorTaxa,',','.',[rfReplaceAll, rfIgnoreCase]);
  GetLastAutorID := 0;
  GetLastAutorID := DMConexao.ExecuteScalar('SELECT NEXT VALUE FOR SAUTORIZACAO_ID');
  strSql := ''
  +'INSERT INTO dbo.AUTOR_TRANSACOES '
  +'           (AUTOR_ID '
  +'           ,TRANS_ID '
  +'           ,DATAHORA '
  +'           ,DOC_FISCAL '
  +'           ,DIGITO '
  +'           ,VALOR '
  +'           ,HISTORICO '
  +'           ,FORMAPAGTO_ID '
  +'           ,RECEITA '
  +'           ,DATA_VENC_EMP '
  +'           ,DATA_FECHA_EMP '
  +'           ,DATA_VENC_FOR '
  +'           ,DATA_FECHA_FOR '
  +'           ,CONV_ID '
  +'           ,CRED_ID '
  +'           ,CARTAO_ID) '
  +'     VALUES '
  +'           ('+IntToStr(GetLastAutorID)+' '
  +'           ,'+IntToStr(GetLastTransID)+' '
  +'           ,'+QuotedStr(DateTimeToStr(DataHora))+' '
  +'           ,'+IntToStr(0)+' '
  +'           ,'+IntToStr(0)+' '
  +'           ,'+QuotedStr(ValorTaxa)+' '
  +'           ,'+QuotedStr('Autor. Mensalidade - Bem Estar')+' '
  +'           ,'+IntToStr(1)+' '
  +'           ,'+QuotedStr('N')+' '
  +'           ,'+QuotedStr(DateTimeToStr(DataVencEmp))+' '
  +'           ,'+QuotedStr(DateTimeToStr(DataFechaEmp))+' '
  +'           ,'+QuotedStr(DateTimeToStr(DataVencFor))+' '
  +'           ,'+QuotedStr(DateTimeToStr(DataFechaFor))+' '
  +'           ,'+IntToStr(ConvID)+' '
  +'           ,'+IntToStr(CredID)+' '
  +'           ,'+IntToStr(CartaoID)+') '
  ;
  Result := strSql;
END;

function TFGeraCartaBemEstar.InserirContaCorrente(Data : TDate; Hora: TTime; DataVenda: TDateTime; ConvID: Integer; CartaoID: Integer;
                                         CredID : Integer; DataVencEmp: TDateTime; DataFechaEmp: TDateTime; DataVencFor : TDateTime; DataFechaFor : TDateTime; Empres_ID : Integer ) : String;
var strSql : String;
ValorTaxa : string;
begin
ValorTaxa := FormatFloat('#,##0.00',QGetEmpTaxasPadraoTAXA_VALOR.AsCurrency);
ValorTaxa := StringReplace(ValorTaxa,',','.',[rfReplaceAll, rfIgnoreCase]);
  strSql := ''
  +'INSERT INTO CONTACORRENTE ( '
  +'   autorizacao_id '
  +'	,digito '
  +'	,data '
  +'	,hora '
  +'	,datavenda '
  +'	,conv_id '
  +'	,cartao_id '
  +'	,cred_id '
  +'	,debito '
  +'	,credito '
  +'	,operador '
  +'	,historico '
  +'	,autorizacao_id_canc '
  +'	,formapagto_id '
  +'	,baixa_conveniado '
  +'	,baixa_credenciado '
  +'	,nf '
  +'	,receita '
  +'	,data_venc_emp '
  +'	,data_fecha_emp '
  +'	,data_venc_for '
  +'	,data_fecha_for '
  +'	,trans_id '
  +'	,previamente_cancelada '
  +'	,nsu '
  +'	,empres_id '
  +'	) '
  +'VALUES ( '
  +'	'+intToStr(GetLastAutorID)+' '
  +'	,1 '
  +'	,'+QuotedStr(DateToStr(Data))+' '
  +'	,'+QuotedStr(TimeToStr(Hora))+' '
  +'	,'+QuotedStr(DateTimeToStr(DataVenda))+' '
  +'	,'+IntToStr(ConvID)+' '
  +'	,'+IntToStr(CartaoID)+' '
  +'	,'+IntToStr(CredID)+' '
  +'	,'+ValorTaxa+' '
  +'	,0.00 '
  +'	,''AUTOR.BTI'' '
  +'	,''Autor. Eletronica - �nica'' '
  +'	,NULL '
  +'	,1 '
  +'	,''N'' '
  +'	,''N'' '
  +'	,NULL '
  +'	,''N'' '
  +'	,'+QuotedStr(DateTimeToStr(DataVencEmp))+' '
  +'	,'+QuotedStr(DateTimeToStr(DataFechaEmp))+' '
  +'	,'+QuotedStr(DateTimeToStr(dataVencFor))+' '
  +'	,'+QuotedStr(DateTimeToStr(DataFechaFor))+' '
  +'	,'+intToStr(GetLastTransID)+' '
  +'	,''N'' '
  +'	,0 '
  +'	,'+intToStr(Empres_ID)+' '
  +'	)';

  Result := strSql;

end;

end.

