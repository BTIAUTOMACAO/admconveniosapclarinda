unit uCadModelosCartoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DBCtrls, StdCtrls, Menus, DB, ZDataset,
  ZAbstractRODataset, ZAbstractDataset, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls, ADODB;

type
  TFCadModelosCartoes = class(TFCad)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    dbEdtNm: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label54: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    QCadastroDESCRICAO: TStringField;
    QCadastroOBSERVACAO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroOPERADOR: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    edObs: TEdit;
    Label5: TLabel;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroMOD_CART_ID: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadModelosCartoes: TFCadModelosCartoes;

implementation

uses DM;

{$R *.dfm}

procedure TFCadModelosCartoes.FormCreate(Sender: TObject);
begin
  chavepri := 'mod_cart_id';
  detalhe  := 'Modelo Cart�o ID: ';
  inherited;

end;

procedure TFCadModelosCartoes.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select * from modelos_cartoes where coalesce(apagado,''N'') <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and mod_cart_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and descricao like '+QuotedStr('%'+EdNome.Text+'%'));
  if Trim(edObs.Text) <> '' then
     QCadastro.Sql.Add(' and observacao like '+QuotedStr('%'+edObs.Text+'%'));
  QCadastro.Sql.Add(' order by descricao ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then
  begin
    Self.TextStatus := 'Modelo Cart�o: ['+QCadastroMOD_CART_ID.AsString+'] - '+QCadastroDESCRICAO.AsString;
    DBGrid1.SetFocus;
  end
  else
    EdCod.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
  edObs.Clear;
end;

procedure TFCadModelosCartoes.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMODELO_CARTOES');
  DMConexao.AdoQry.Open;
  QCadastroMOD_CART_ID.ReadOnly := False;
  QCadastroMOD_CART_ID.AsInteger :=  DMConexao.AdoQry.Fields[0].Value;
end;

procedure TFCadModelosCartoes.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Modelo Cart�o: ['+QCadastroMOD_CART_ID.AsString+'] - '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadModelosCartoes.QCadastroAfterPost(DataSet: TDataSet);
begin
  inherited;
  QCadastro.Requery();
end;

end.
