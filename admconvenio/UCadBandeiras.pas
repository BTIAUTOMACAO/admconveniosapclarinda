unit UCadBandeiras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, Menus, DB, ZDataset, ZAbstractRODataset, ZAbstractDataset,
  StdCtrls, Mask, JvExMask, JvToolEdit, ComCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons, ExtCtrls, DBCtrls, ADODB;

type
  TFCadBandeiras = class(TFCad)
    QCadastroBAND_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroQTD_LIMITES: TIntegerField;
    QCadastroDESC_LIMITE_1: TStringField;
    QCadastroMIN_LIMITE_1: TFloatField;
    QCadastroMAX_LIMITE_1: TFloatField;
    QCadastroMIN_LIMITE_2: TFloatField;
    QCadastroMAX_LIMITE_2: TFloatField;
    QCadastroMIN_LIMITE_3: TFloatField;
    QCadastroMAX_LIMITE_3: TFloatField;
    QCadastroMIN_LIMITE_4: TFloatField;
    QCadastroMAX_LIMITE_4: TFloatField;
    QCadastroLIMITE_4: TFloatField;
    QCadastroLIMITE_1: TFloatField;
    QCadastroLIMITE_2: TFloatField;
    QCadastroLIMITE_3: TFloatField;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    dbCbLimites: TDBComboBox;
    tsSegLim1: TTabSheet;
    tsSegLim2: TTabSheet;
    tsSegLim3: TTabSheet;
    tsSegLim4: TTabSheet;
    Panel39: TPanel;
    GridCredObrigarSenha: TJvDBGrid;
    Panel38: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel12: TPanel;
    dsCadBand_Segmentos: TDataSource;
    dsSegmentos: TDataSource;
    Panel13: TPanel;
    btnIncProdBlack: TBitBtn;
    btnImportListaBlack: TBitBtn;
    Panel5: TPanel;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    Panel8: TPanel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel11: TPanel;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    btnApagaCadBand_Seg: TBitBtn;
    btnApagaCadBand_Seg2: TBitBtn;
    btnApagaCadBand_Seg3: TBitBtn;
    btnApagaCadBand_Seg4: TBitBtn;
    QCadastroDESC_LIMITE_2: TStringField;
    QCadastroDESC_LIMITE_3: TStringField;
    QCadastroDESC_LIMITE_4: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroOPERADOR: TStringField;
    QCadastroAPAGADO: TStringField;
    btnGravarCadBand_Seg: TBitBtn;
    btnGravarCadBand_Seg2: TBitBtn;
    btnGravarCadBand_Seg3: TBitBtn;
    btnGravarCadBand_Seg4: TBitBtn;
    Label22: TLabel;
    edQtdLimites: TEdit;
    pnlLimite1: TPanel;
    Label6: TLabel;
    DBEditDescLimite1: TDBEdit;
    Label7: TLabel;
    DBEditMinLimite1: TDBEdit;
    DBEditLimiteMax1: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBEditLimite1: TDBEdit;
    pnlLimite2: TPanel;
    DBEditDescLimite2: TDBEdit;
    Label10: TLabel;
    DBEditMinLimite2: TDBEdit;
    Label11: TLabel;
    DBEditLimiteMax2: TDBEdit;
    Label12: TLabel;
    DBEditLimite2: TDBEdit;
    Label13: TLabel;
    pnlLimite3: TPanel;
    Label14: TLabel;
    DBEditDescLimite3: TDBEdit;
    Label15: TLabel;
    DBEditMinLimite3: TDBEdit;
    Label16: TLabel;
    DBEditLimiteMax3: TDBEdit;
    Label17: TLabel;
    DBEditLimite3: TDBEdit;
    pnlLimite4: TPanel;
    DBEditDescLimite4: TDBEdit;
    Label18: TLabel;
    DBEditMinLimite4: TDBEdit;
    Label19: TLabel;
    DBEditLimiteMax4: TDBEdit;
    Label20: TLabel;
    DBEditLimite4: TDBEdit;
    Label21: TLabel;
    JvDBGrid1: TJvDBGrid;
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTAPAGADO: TDateTimeField;
    QSegmentos: TADOQuery;
    QSegmentosSEG_ID: TIntegerField;
    QSegmentosDESCRICAO: TStringField;
    QCadBand_Segmentos: TADOQuery;
    QCadBand_SegmentosBAND_ID: TIntegerField;
    QCadBand_SegmentosSEG_ID: TIntegerField;
    QCadBand_SegmentosCOD_LIMITE: TIntegerField;
    QCadBand_SegmentosSEG_ID_LKP: TIntegerField;
    QCadBand_SegmentosSEGMENTOS: TStringField;
    procedure tsSegLim1Show(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure btnIncProdBlackClick(Sender: TObject);
    procedure btnApagaCadBand_SegClick(Sender: TObject);
    procedure btnGravarCadBand_SegClick(Sender: TObject);
    procedure DSCadastroStateChange(Sender: TObject);
    procedure QCadBand_SegmentosBeforePost(DataSet: TDataSet);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    procedure dsCadBand_SegmentosStateChange(Sender: TObject);
    procedure dbCbLimitesChange(Sender: TObject);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QCadBand_SegmentosAfterInsert(DataSet: TDataSet);
    procedure GridCredObrigarSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure JvDBGrid2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure JvDBGrid3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure HabilitarTabSheets(Habilitar : Boolean);
    procedure mostrarBotoesApagarGravarSeg;
    procedure habilitaLimites;
    procedure addCadBand(aba : Integer);
    procedure abrirCadBand(aba : Integer);
    procedure mostrarAbas;
    function verfLimiteVazio(nomeBaseComp, mensagem : String) : Boolean;
    function permiteSalvarSegmento : Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadBandeiras: TFCadBandeiras;

implementation

uses cartao_util, UValidacao, DM;

{$R *.dfm}

procedure TFCadBandeiras.HabilitarTabSheets(Habilitar: Boolean);
var I: Integer;
begin
  for I := 0 to PageControl1.PageCount -1 do
    if not (PageControl1.Pages[I].PageIndex = PageControl1.ActivePageIndex) then
      PageControl1.Pages[I].Enabled := Habilitar;
end;


procedure TFCadBandeiras.mostrarBotoesApagarGravarSeg;
begin
  btnApagaCadBand_Seg.Enabled   := not QCadBand_Segmentos.IsEmpty;
  btnApagaCadBand_Seg2.Enabled  := not QCadBand_Segmentos.IsEmpty;
  btnApagaCadBand_Seg3.Enabled  := not QCadBand_Segmentos.IsEmpty;
  btnApagaCadBand_Seg4.Enabled  := not QCadBand_Segmentos.IsEmpty;
  btnGravarCadBand_Seg.Enabled  := QCadBand_Segmentos.State in [dsInsert,dsEdit];
  btnGravarCadBand_Seg2.Enabled := QCadBand_Segmentos.State in [dsInsert,dsEdit];
  btnGravarCadBand_Seg3.Enabled := QCadBand_Segmentos.State in [dsInsert,dsEdit];
  btnGravarCadBand_Seg4.Enabled := QCadBand_Segmentos.State in [dsInsert,dsEdit];
end;

procedure TFCadBandeiras.habilitaLimites;
begin
  pnlLimite1.Enabled := QCadastroQTD_LIMITES.Value >= 1;
  pnlLimite2.Enabled := QCadastroQTD_LIMITES.Value >= 2;
  pnlLimite3.Enabled := QCadastroQTD_LIMITES.Value >= 3;
  pnlLimite4.Enabled := QCadastroQTD_LIMITES.Value >= 4;
end;

function TFCadBandeiras.verfLimiteVazio(nomeBaseComp, mensagem : String) : Boolean;
var comp : TComponent;
begin
  Result := True;
  comp := FindComponent(nomeBaseComp);
  if comp <> nil then begin
    if Trim(TDBEdit(comp).Text) = '' then begin
      MsgInf(mensagem);
      TDBEdit(comp).SetFocus;
    end else
      Result := False;
  end;
end;

procedure TFCadBandeiras.abrirCadBand(aba: Integer);
begin
  QCadBand_Segmentos.Close;
  QCadBand_Segmentos.Parameters[0].Value := aba;
  QCadBand_Segmentos.Parameters[1].Value := QCadastroBAND_ID.Value;
  QCadBand_Segmentos.Open;
end;

procedure TFCadBandeiras.mostrarAbas;
var I : Integer;
begin
  inherited;
  if (QCadastro.Active) and (not QCadastro.IsEmpty) and (not (QCadastro.State = dsInsert))then begin
    for I := 2 to 5 do
      PageControl1.Pages[I].TabVisible := (QCadastroQTD_LIMITES.Value >= I-1);
  end else if (QCadastro.State = dsInsert) then begin
    for I := 2 to 5 do
      PageControl1.Pages[I].TabVisible := False;
  end;
end;

procedure TFCadBandeiras.addCadBand(aba: Integer);
begin
  QCadBand_Segmentos.Append;
  QCadBand_SegmentosBAND_ID.Value := QCadastroBAND_ID.Value;
  QCadBand_SegmentosCOD_LIMITE.Value := aba;
end;

function TFCadBandeiras.permiteSalvarSegmento : Boolean;
var c : integer;
begin
  c := DMConexao.ExecuteScalar('SELECT COUNT(BAND_ID) FROM BANDEIRAS_SEGMENTOS WHERE BAND_ID = '+ QCadastroBAND_ID.AsString + ' AND SEG_ID = '+QSegmentosSEG_ID.AsString,0);
  Result := c = 0;
end;

procedure TFCadBandeiras.tsSegLim1Show(Sender: TObject);
begin
  inherited;
  abrirCadBand(TTabSheet(Sender).Tag);
end;

procedure TFCadBandeiras.FormCreate(Sender: TObject);
var I : Integer;
begin
  chavepri := 'band_id';
  detalhe  := 'Bandeira ID: ';
  inherited;
  QSegmentos.Open;
  QCadastro.Open;
  for I := 2 to 5 do
    PageControl1.Pages[I].TabVisible := (QCadastro.Active) and (not QCadastro.IsEmpty) and (QCadastroQTD_LIMITES.Value >= I-1);
end;

procedure TFCadBandeiras.QCadastroAfterInsert(DataSet: TDataSet);
var  band_id : Integer;
begin
  inherited;
  band_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SBAND_ID');
  QCadastroBAND_ID.AsInteger          := band_id;
end;

procedure TFCadBandeiras.QCadastroBeforePost(DataSet: TDataSet);
var I : Integer;
begin
  inherited;
  if fnVerfCampoVazio('De uma descri��o para esta bandeira!',QCadastroDESCRICAO) then begin
    DBEdit2.SetFocus;
    Abort;
  end;
  if fnVerfCampoVazio('Selecione a quantidade limites ',QCadastroQTD_LIMITES) then begin
    dbCbLimites.SetFocus;
    Abort;
  end;

  for I := 1 to QCadastroQTD_LIMITES.Value do begin
    if (verfLimiteVazio('DBEditDescLimite' + IntToStr(I), 'De uma descri��o para o "Limite ' + IntToStr(I) + '"')) or
       (verfLimiteVazio('DBEditMinLimite' + IntToStr(I), 'Digite o valor m�nimo para o Limite ' + IntToStr(I) + '"')) or
       (verfLimiteVazio('DBEditLimiteMax' + IntToStr(I), 'Digite o valor m�ximo para o Limite ' + IntToStr(I) + '"')) or
       (verfLimiteVazio('DBEditLimite' + IntToStr(I), 'Digite o valor do Limite ' + IntToStr(I)+'"')) then
       Abort;
  end;
{  for I := 1 to QCadastroQTD_LIMITES.Value do begin
    if (QCadastro.FieldByName('MIN_LIMITE_'+IntToStr(I)).Value > QCadastro.FieldByName('MAX_LIMITE_'+IntToStr(I)).Value) then begin
       msgInf('Limite Minimo ' + IntToStr(I) + ' n�o deve ser maior que o Limite M�ximo ' + IntToStr(I));
       TDBEdit(FindComponent('DBEditMinLimite'+IntToStr(I))).SetFocus;;
       Abort;
    end;
    if (QCadastro.FieldByName('LIMITE_'+IntToStr(I)).Value < QCadastro.FieldByName('MIN_LIMITE_'+IntToStr(I)).Value) then begin
       msgInf('Limite ' + IntToStr(I) + ' n�o deve ser menor que o Limite M�nimo ' + IntToStr(I));
       TDBEdit(FindComponent('DBEditLimite'+IntToStr(I))).SetFocus;;
       Abort;
    end;
    if (QCadastro.FieldByName('LIMITE_'+IntToStr(I)).Value > QCadastro.FieldByName('MAX_LIMITE_'+IntToStr(I)).Value) then begin
       msgInf('Limite ' + IntToStr(I) + ' n�o deve ser maior que o Limite M�ximo ' + IntToStr(I));
       TDBEdit(FindComponent('DBEditLimite'+IntToStr(I))).SetFocus;
       Abort;
    end;
  end;}

  for I := QCadastroQTD_LIMITES.Value+1 to 4 do begin
    QCadastro.FieldByName('DESC_LIMITE_'+IntToStr(I)).AsString := '';
    QCadastro.FieldByName('MIN_LIMITE_'+IntToStr(I)).AsString := '';
    QCadastro.FieldByName('MAX_LIMITE_'+IntToStr(I)).AsString := '';
    QCadastro.FieldByName('LIMITE_'+IntToStr(I)).AsString := '';
  end;
end;

procedure TFCadBandeiras.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select * from bandeiras where coalesce(apagado,''N'') <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and band_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and descricao like '+QuotedStr('%'+EdNome.Text+'%'));
  if Trim(edQtdLimites.Text) <> '' then
     QCadastro.Sql.Add(' and qtd_limites = '+edQtdLimites.Text);
  QCadastro.Sql.Add(' order by descricao ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then
  begin
    Self.TextStatus := 'Bandeira: ['+QCadastroBAND_ID.AsString+'] - '+QCadastroDESCRICAO.AsString;
    DBGrid1.SetFocus;
  end
  else
    EdCod.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
  edQtdLimites.Clear;
end;

procedure TFCadBandeiras.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  mostrarAbas;
end;

procedure TFCadBandeiras.btnIncProdBlackClick(Sender: TObject);
begin
  inherited;
  QCadBand_Segmentos.Append;
  addCadBand(PageControl1.ActivePage.Tag);
end;

procedure TFCadBandeiras.btnApagaCadBand_SegClick(Sender: TObject);
begin
  inherited;
  QCadBand_Segmentos.Delete;
end;

procedure TFCadBandeiras.btnGravarCadBand_SegClick(Sender: TObject);
begin
  inherited;
  QCadBand_Segmentos.Post;
end;

procedure TFCadBandeiras.DSCadastroStateChange(Sender: TObject);
begin
  inherited;
  habilitaLimites;
end;

procedure TFCadBandeiras.QCadBand_SegmentosBeforePost(DataSet: TDataSet);
begin
  inherited;
  QCadBand_SegmentosSEG_ID.Value := QSegmentosSEG_ID.Value;
  if not permiteSalvarSegmento then begin
    MsgInf('O Estabelecimento '+ QSegmentosDESCRICAO.AsString + ' j� est� cadastrado!'+#13+
           'N�o � permitido ter o mesmo estabelemento cadastrado para a mesma bandeira.');
    Abort;
  end;
end;

procedure TFCadBandeiras.QCadastroAfterPost(DataSet: TDataSet);
begin
  inherited;
  mostrarAbas;
end;

procedure TFCadBandeiras.dsCadBand_SegmentosStateChange(Sender: TObject);
begin
  inherited;
  mostrarBotoesApagarGravarSeg;
  //HabilitarTabSheets(not (QCadBand_Segmentos.State in [dsInsert, dsEdit]));
end;

procedure TFCadBandeiras.dbCbLimitesChange(Sender: TObject);
begin
  inherited;
  pnlLimite1.Enabled := TDBComboBox(Sender).ItemIndex >= 0;
  pnlLimite2.Enabled := TDBComboBox(Sender).ItemIndex >= 1;
  pnlLimite3.Enabled := TDBComboBox(Sender).ItemIndex >= 2;
  pnlLimite4.Enabled := TDBComboBox(Sender).ItemIndex >= 3;
end;

procedure TFCadBandeiras.DSCadastroDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  mostrarBotoesApagarGravarSeg;
end;

procedure TFCadBandeiras.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  QCadastro.Close;
  QSegmentos.Close;
end;

procedure TFCadBandeiras.QCadBand_SegmentosAfterInsert(DataSet: TDataSet);
begin
  inherited;
  addCadBand(PageControl1.ActivePage.Tag);
end;

procedure TFCadBandeiras.GridCredObrigarSenhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  IF KEY = VK_ESCAPE then
    if QCadBand_Segmentos.State in [dsInsert, dsEdit] then begin
      QCadBand_Segmentos.Cancel;
      Abort;
    end;
  inherited;
end;

procedure TFCadBandeiras.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if not (ActiveControl is TJvDBGrid) then
    inherited;
end;

procedure TFCadBandeiras.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_ESCAPE then
    if QCadBand_Segmentos.State in [dsInsert, dsEdit] then begin
      QCadBand_Segmentos.Cancel;
      Abort;
    end;
  inherited;
end;

procedure TFCadBandeiras.JvDBGrid2KeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  IF KEY = VK_ESCAPE then
    if QCadBand_Segmentos.State in [dsInsert, dsEdit] then begin
      QCadBand_Segmentos.Cancel;
      Abort;
    end;
  inherited;
end;

procedure TFCadBandeiras.JvDBGrid3KeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  IF KEY = VK_ESCAPE then
    if QCadBand_Segmentos.State in [dsInsert, dsEdit] then begin
      QCadBand_Segmentos.Cancel;
      Abort;
    end;
  inherited;
end;

end.
