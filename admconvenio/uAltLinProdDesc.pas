unit uAltLinProdDesc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfAltLinProdDesc = class(TForm)
    Panel1: TPanel;
    cbbCampo: TComboBox;
    Label1: TLabel;
    edtNew: TEdit;
    Label2: TLabel;
    edtHist: TEdit;
    Label3: TLabel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure edtNewKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnOkClick(Sender: TObject);
  private
    procedure Validacao;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAltLinProdDesc: TfAltLinProdDesc;

implementation

uses cartao_util;

{$R *.dfm}

procedure TfAltLinProdDesc.Validacao;
begin
  if SoNumero(edtNew.Text) = '' then
  begin
    MsgInf('Insira o novo valor para o campo');
    edtNew.SetFocus;
    Exit;
  end
  else if edtHist.Text = '' then
  begin
    MsgInf('Insira um histórico para a alteração');
    edtHist.SetFocus;
    Exit;
  end;
end;

procedure TfAltLinProdDesc.edtNewKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
end;

procedure TfAltLinProdDesc.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key in [vk_return,vk_escape]) then
  begin
    if (ActiveControl is TCustomComboBox) then
    begin
      if TCustomComboBox(ActiveControl).DroppedDown then
      begin
        TCustomComboBox(ActiveControl).DroppedDown := False;
        exit;
      end;
    end;
    if key = vk_return then
    begin
      SelectNext(ActiveControl,true,true);
    end;
    if Key = vk_escape then
    begin
      if application.messagebox('Fechar a janela?','Confirmação',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
      begin
        Close;
      end;
    end;
  end;

end;

procedure TfAltLinProdDesc.btnOkClick(Sender: TObject);
begin
  Validacao;
end;

end.
