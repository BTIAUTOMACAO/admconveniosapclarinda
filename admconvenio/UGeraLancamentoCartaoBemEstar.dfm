inherited FGeraLancamentoCartaBemEstar: TFGeraLancamentoCartaBemEstar
  Left = 332
  Top = 213
  Caption = 'Lan'#231'amento de D'#233'bitos - Bem Estar'
  ClientHeight = 355
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    inherited panTitulo: TPanel
      Caption = 'Gerar Cart'#245'es'
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 760
    Height = 332
    Align = alClient
    TabOrder = 1
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 758
      Height = 240
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 32
        Top = 28
        Width = 151
        Height = 13
        Caption = 'Selecione a empresa de origem:'
      end
      object Label2: TLabel
        Left = 432
        Top = 52
        Width = 154
        Height = 13
        Caption = 'Selecione a empresa de destino:'
        Visible = False
      end
      object lblStatus: TLabel
        Left = 1
        Top = 204
        Width = 756
        Height = 13
        Align = alBottom
      end
      object Label3: TLabel
        Left = 35
        Top = 69
        Width = 85
        Height = 13
        Caption = 'Data Fechamento'
      end
      object lkpEmpresasDestino: TJvDBLookupCombo
        Left = 496
        Top = 68
        Width = 217
        Height = 21
        Hint = 'Empresas Bem Estar'
        LookupField = 'empres_id'
        LookupDisplay = 'nome'
        LookupSource = DSEmpresaBemEstar
        TabOrder = 3
        Visible = False
        OnChange = lkpEmpresasDestinoChange
      end
      object edtEmpr: TEdit
        Left = 32
        Top = 44
        Width = 57
        Height = 21
        TabOrder = 0
        OnChange = edtEmprChange
        OnKeyPress = edtEmprKeyPress
      end
      object DBEmpresa: TJvDBLookupCombo
        Left = 96
        Top = 44
        Width = 217
        Height = 21
        LookupField = 'empres_id'
        LookupDisplay = 'fantasia'
        LookupSource = DSEmpresa
        TabOrder = 1
        OnChange = DBEmpresaChange
        OnExit = DBEmpresaExit
      end
      object edtCodEmpresaDestino: TEdit
        Left = 432
        Top = 68
        Width = 57
        Height = 21
        TabOrder = 2
        Visible = False
        OnChange = edtCodEmpresaDestinoChange
      end
      object btnProcessar: TBitBtn
        Left = 35
        Top = 152
        Width = 278
        Height = 33
        Caption = 'Processar'
        TabOrder = 6
        OnClick = btnProcessarClick
      end
      object ProgressBar1: TProgressBar
        Left = 1
        Top = 217
        Width = 756
        Height = 22
        Align = alBottom
        TabOrder = 7
      end
      object FilenameEdit1: TJvFilenameEdit
        Left = 34
        Top = 124
        Width = 279
        Height = 21
        Filter = 'Planilha do Excel (*.xls;*.xlsx)|*.xls;*.xlsx'
        InitialDir = 'C:\'
        TabOrder = 5
        Text = 'C:\AdmCartaoBella\'
      end
      object lkpDataFechamento: TDBLookupComboBox
        Left = 32
        Top = 88
        Width = 281
        Height = 21
        KeyField = 'DATA_FECHA'
        ListField = 'DATA_FECHA'
        ListSource = dsData
        TabOrder = 4
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 644
    Top = 48
  end
  object QEmpresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome, case when fantasia is null'
      'then nome else fantasia end as fantasia'
      'from empresas where apagado <> '#39'S'#39' '
      'order by nome')
    Left = 504
    Top = 159
    object QEmpresaempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresafantasia: TStringField
      FieldName = 'fantasia'
      ReadOnly = True
      Size = 60
    end
  end
  object DSEmpresa: TDataSource
    DataSet = QEmpresa
    Left = 544
    Top = 159
  end
  object QEmpresaBemEstar: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome, case when fantasia is null'
      'then nome else fantasia end as fantasia'
      'from empresas where apagado <> '#39'S'#39' and tipo_credito = 5 '
      'order by nome')
    Left = 504
    Top = 199
    object QEmpresaBemEstarempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresaBemEstarnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresaBemEstarfantasia: TStringField
      FieldName = 'fantasia'
      ReadOnly = True
      Size = 60
    end
  end
  object DSEmpresaBemEstar: TDataSource
    DataSet = QEmpresaBemEstar
    Left = 544
    Top = 199
  end
  object QQtdCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'EMPRES_ID'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT COUNT(*) QtdCartoes FROM CARTOES INNER JOIN CONVENIADOS'
      
        'ON CARTOES.CONV_ID = CONVENIADOS.CONV_ID and CONVENIADOS.EMPRES_' +
        'ID = :EMPRES_ID')
    Left = 584
    Top = 160
    object QQtdCartoesQtdCartoes: TIntegerField
      FieldName = 'QtdCartoes'
      ReadOnly = True
    end
  end
  object DSQtdCartoes: TDataSource
    Left = 584
    Top = 199
  end
  object QDatasFechamento: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 504
    Top = 247
  end
  object DSDatasFecha: TDataSource
    DataSet = QDatasFechamento
    Left = 544
    Top = 247
  end
  object QExecProcedure: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'data_fecha'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      
        'EXEC SALDO_PROXFECHA_CONV @CONV = :conv_id, @data_fechamento = :' +
        'data_fecha')
    Left = 504
    Top = 279
  end
  object QConvByEmpresId: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 584
    Top = 247
  end
  object DSConvByEmpresId: TDataSource
    DataSet = QConvByEmpresId
    Left = 624
    Top = 247
  end
  object QCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        DataType = ftInteger
        Value = 0
      end>
    SQL.Strings = (
      'SELECT'
      'cart.nome, '
      'cart.conv_id,'
      'cart.senha,'
      'cart.codcartimp,'
      'conv.empres_id,'
      'cart.cartao_id,'
      'cart.codigo '
      'FROM CARTOES cart INNER JOIN CONVENIADOS conv'
      'ON cart.conv_id = conv.conv_id AND conv.empres_id = :empres_id '
      'WHERE conv.apagado = '#39'N'#39' and conv.liberado = '#39'S'#39)
    Left = 624
    Top = 160
    object QCartoesnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QCartoesconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QCartoesempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QCartoessenha: TStringField
      FieldName = 'senha'
      Size = 40
    end
    object QCartoescodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object QCartoescartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object QCartoescodigo: TIntegerField
      FieldName = 'codigo'
    end
  end
  object DSCartoes: TDataSource
    DataSet = QCartoes
    Left = 624
    Top = 200
  end
  object QConveniado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT '
      '       [CONV_ID]'
      '      ,[CARTAO_BEM_ESTAR]'
      '      ,[EMPRES_ID]'
      '      ,[BANCO]'
      '      ,[GRUPO_CONV_EMP]'
      '      ,[CHAPA]'
      '      ,[SENHA]'
      '      ,[TITULAR]'
      '      ,[CONTRATO]'
      '      ,[LIMITE_MES]'
      '      ,[LIBERADO]'
      '      ,[FIDELIDADE]'
      '      ,[APAGADO]'
      '      ,[DT_NASCIMENTO]'
      '      ,[CARGO]'
      '      ,[SETOR]'
      '      ,[CPF]'
      '      ,[RG]'
      '      ,[LIMITE_TOTAL]'
      '      ,[LIMITE_PROX_FECHAMENTO]'
      '      ,[AGENCIA]'
      '      ,[CONTACORRENTE]'
      '      ,[DIGITO_CONTA]'
      '      ,[TIPOPAGAMENTO]'
      '      ,[ENDERECO]'
      '      ,[NUMERO]'
      '      ,[BAIRRO]'
      '      ,[CIDADE]'
      '      ,[ESTADO]'
      '      ,[CEP]'
      '      ,[TELEFONE1]'
      '      ,[TELEFONE2]'
      '      ,[CELULAR]'
      '      ,[OBS1]'
      '      ,[OBS2]'
      '      ,[EMAIL]'
      '      ,[DTULTCESTA]'
      '      ,[SALARIO]'
      '      ,[TIPOSALARIO]'
      '      ,[COD_EMPRESA]'
      '      ,[FLAG]'
      '  FROM [dbo].[CONVENIADOS_BEM_ESTAR]'
      ' WHERE conv_id = :conv_id'
      '')
    Left = 544
    Top = 279
    object QConveniadoCARTAO_BEM_ESTAR: TStringField
      FieldName = 'CARTAO_BEM_ESTAR'
      FixedChar = True
      Size = 1
    end
    object QConveniadoEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QConveniadoBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QConveniadoGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object QConveniadoSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QConveniadoTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QConveniadoCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QConveniadoLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 8
      Size = 2
    end
    object QConveniadoLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QConveniadoFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object QConveniadoAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QConveniadoDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
    object QConveniadoCARGO: TStringField
      FieldName = 'CARGO'
      Size = 45
    end
    object QConveniadoSETOR: TStringField
      FieldName = 'SETOR'
      Size = 45
    end
    object QConveniadoCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QConveniadoRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QConveniadoLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object QConveniadoLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object QConveniadoAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QConveniadoCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object QConveniadoDIGITO_CONTA: TStringField
      FieldName = 'DIGITO_CONTA'
      FixedChar = True
      Size = 2
    end
    object QConveniadoTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object QConveniadoENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QConveniadoNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QConveniadoCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QConveniadoTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QConveniadoTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QConveniadoCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QConveniadoOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QConveniadoOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QConveniadoEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QConveniadoDTULTCESTA: TDateTimeField
      FieldName = 'DTULTCESTA'
    end
    object QConveniadoSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object QConveniadoTIPOSALARIO: TStringField
      FieldName = 'TIPOSALARIO'
      Size = 15
    end
    object QConveniadoCOD_EMPRESA: TStringField
      FieldName = 'COD_EMPRESA'
      Size = 30
    end
    object QConveniadoFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QConveniadoCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QConveniadoCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QConveniadoBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QConveniadoCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QConveniadoESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object QGeraConveniado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'CONV_ID_BE'
        Size = -1
        Value = Null
      end
      item
        Name = 'EMPRES_ID'
        Size = -1
        Value = Null
      end
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'INSERT INTO [dbo].[CONVENIADOS_BEM_ESTAR]'
      '           ([CONV_ID_BE]'
      '           ,[CARTAO_BEM_ESTAR]'
      '           ,[EMPRES_ID]'
      '           ,[BANCO]'
      '           ,[GRUPO_CONV_EMP]'
      '           ,[CHAPA]'
      '           ,[SENHA]'
      '           ,[TITULAR]'
      '           ,[CONTRATO]'
      '           ,[LIMITE_MES]'
      '           ,[LIBERADO]'
      '           ,[FIDELIDADE]'
      '           ,[APAGADO]'
      '           ,[DT_NASCIMENTO]'
      '           ,[CARGO]'
      '           ,[SETOR]'
      '           ,[CPF]'
      '           ,[RG]'
      '           ,[LIMITE_TOTAL]'
      '           ,[LIMITE_PROX_FECHAMENTO]'
      '           ,[AGENCIA]'
      '           ,[CONTACORRENTE]'
      '           ,[DIGITO_CONTA]'
      '           ,[TIPOPAGAMENTO]'
      '           ,[ENDERECO]'
      '           ,[NUMERO]'
      '           ,[BAIRRO]'
      '           ,[CIDADE]'
      '           ,[ESTADO]'
      '           ,[CEP]'
      '           ,[TELEFONE1]'
      '           ,[TELEFONE2]'
      '           ,[CELULAR]'
      '           ,[OBS1]'
      '           ,[OBS2]'
      '           ,[EMAIL]'
      '           ,[DTULTCESTA]'
      '           ,[SALARIO]'
      '           ,[TIPOSALARIO]'
      '           ,[COD_EMPRESA]'
      '           ,[FLAG])'
      '     '
      '       SELECT :CONV_ID_BE'
      '      ,'#39'S'#39
      '      ,:EMPRES_ID'
      '      ,coalesce([BANCO],0) AS BANCO'
      '      ,[GRUPO_CONV_EMP]'
      '      ,[CHAPA]'
      '      ,[SENHA]'
      '      ,[TITULAR]'
      '      ,[CONTRATO]'
      '      ,[LIMITE_MES]'
      '      ,[LIBERADO]'
      '      ,[FIDELIDADE]'
      '      ,[APAGADO]'
      '      ,COALESCE([DT_NASCIMENTO],1900-01-01) DT_NASCIMENTO'
      '      ,COALESCE([CARGO],'#39'PADRAO'#39') CARGO'
      '      ,[SETOR]'
      '      ,COALESCE([CPF],'#39'0'#39')'
      '      ,COALESCE([RG],'#39'0'#39')'
      '      ,[LIMITE_TOTAL]'
      '      ,[LIMITE_PROX_FECHAMENTO]'
      '      ,COALESCE([AGENCIA],'#39'0'#39') AGENCIA'
      '      ,COALESCE([CONTACORRENTE],'#39'0'#39') CONTACORRENTE'
      '      ,COALESCE([DIGITO_CONTA],'#39'0'#39') DIGITO_CONTA'
      '      ,[TIPOPAGAMENTO]'
      '      ,COALESCE([ENDERECO],'#39#39')'
      '      ,COALESCE([NUMERO],0)'
      '      ,COALESCE([BAIRRO],'#39#39')'
      '      ,COALESCE([CIDADE],'#39#39')'
      '      ,COALESCE([ESTADO],'#39#39')'
      '      ,COALESCE([CEP],'#39#39')'
      '      ,COALESCE([TELEFONE1],'#39#39')'
      '      ,COALESCE([TELEFONE2],'#39#39')'
      '      ,COALESCE([CELULAR],'#39#39')'
      '      ,COALESCE([OBS1],'#39#39')'
      '      ,COALESCE([OBS2],'#39#39')'
      '      ,COALESCE([EMAIL],'#39#39')'
      '      ,COALESCE([DTULTCESTA],1900-01-01)'
      '      ,COALESCE([SALARIO],0.00)'
      '      ,COALESCE([TIPOSALARIO],'#39#39')'
      '      ,COALESCE([COD_EMPRESA],'#39#39')'
      '      ,COALESCE([FLAG],'#39#39')'
      '  FROM [dbo].[CONVENIADOS] where conv_id = :conv_id'
      '  ')
    Left = 584
    Top = 279
  end
  object QGeraCartao: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = QGeraCartaoBeforePost
    Parameters = <>
    SQL.Strings = (
      'select * from cartoes where cartao_id = 0')
    Left = 624
    Top = 279
    object QGeraCartaoCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QGeraCartaoCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QGeraCartaoNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QGeraCartaoLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QGeraCartaoDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QGeraCartaoTITULAR: TStringField
      FieldName = 'TITULAR'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoJAEMITIDO: TStringField
      FieldName = 'JAEMITIDO'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QGeraCartaoCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object QGeraCartaoPARENTESCO: TStringField
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object QGeraCartaoDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object QGeraCartaoNUM_DEP: TIntegerField
      FieldName = 'NUM_DEP'
    end
    object QGeraCartaoFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object QGeraCartaoCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QGeraCartaoRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QGeraCartaoVIA: TIntegerField
      FieldName = 'VIA'
    end
    object QGeraCartaoDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QGeraCartaoDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QGeraCartaoOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QGeraCartaoDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QGeraCartaoOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QGeraCartaoCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QGeraCartaoATIVO: TStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object QGeraCartaoEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QGeraCartaoSENHA: TStringField
      FieldName = 'SENHA'
      Size = 45
    end
  end
  object QGetEmpTaxasPadrao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'qtdDependente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT  *  FROM EMP_TAXAS_BEM_ESTAR_PADRAO where :qtdDependente ' +
        ' between FAIXA_INICIO and FAIXA_FIM ')
    Left = 425
    Top = 48
    object QGetEmpTaxasPadraoTAXA_ID: TIntegerField
      FieldName = 'TAXA_ID'
    end
    object QGetEmpTaxasPadraoFAIXA_INICIO: TIntegerField
      FieldName = 'FAIXA_INICIO'
    end
    object QGetEmpTaxasPadraoFAIXA_FIM: TIntegerField
      FieldName = 'FAIXA_FIM'
    end
    object QGetEmpTaxasPadraoTAXA_VALOR: TBCDField
      FieldName = 'TAXA_VALOR'
      DisplayFormat = '#,##0.00'
      EditFormat = '#,##0.00'
      currency = True
      Precision = 6
      Size = 2
    end
    object QGetEmpTaxasPadraoDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QGetEmpTaxasPadraoOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
  end
  object QCredenciados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CREDENCIADOS WHERE CRED_ID = 2836')
    Left = 505
    Top = 120
    object QCredenciadosCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCredenciadosNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QCredenciadosSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object QCredenciadosLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCODACESSO: TIntegerField
      FieldName = 'CODACESSO'
    end
    object QCredenciadosCOMISSAO: TBCDField
      FieldName = 'COMISSAO'
      Precision = 6
      Size = 2
    end
    object QCredenciadosBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QCredenciadosDIAFECHAMENTO1: TWordField
      FieldName = 'DIAFECHAMENTO1'
    end
    object QCredenciadosDIAFECHAMENTO2: TWordField
      FieldName = 'DIAFECHAMENTO2'
    end
    object QCredenciadosVENCIMENTO1: TWordField
      FieldName = 'VENCIMENTO1'
    end
    object QCredenciadosVENCIMENTO2: TWordField
      FieldName = 'VENCIMENTO2'
    end
    object QCredenciadosAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosPAGA_CRED_POR_ID: TIntegerField
      FieldName = 'PAGA_CRED_POR_ID'
    end
    object QCredenciadosCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QCredenciadosCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object QCredenciadosINSCRICAOEST: TStringField
      FieldName = 'INSCRICAOEST'
      Size = 18
    end
    object QCredenciadosFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 58
    end
    object QCredenciadosTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QCredenciadosTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QCredenciadosFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QCredenciadosCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 58
    end
    object QCredenciadosENVIAR_EMAIL: TStringField
      FieldName = 'ENVIAR_EMAIL'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 40
    end
    object QCredenciadosHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 40
    end
    object QCredenciadosPOSSUICOMPUTADOR: TStringField
      FieldName = 'POSSUICOMPUTADOR'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCredenciadosNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCredenciadosCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCredenciadosMAQUINETA: TStringField
      FieldName = 'MAQUINETA'
      Size = 14
    end
    object QCredenciadosTIPOFECHAMENTO: TStringField
      FieldName = 'TIPOFECHAMENTO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosNOVOCODACESSO: TIntegerField
      FieldName = 'NOVOCODACESSO'
    end
    object QCredenciadosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCredenciadosAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QCredenciadosCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 15
    end
    object QCredenciadosOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QCredenciadosOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QCredenciadosCORRENTISTA: TStringField
      FieldName = 'CORRENTISTA'
      Size = 40
    end
    object QCredenciadosCARTIMPRESSO: TStringField
      FieldName = 'CARTIMPRESSO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosFORNCESTA: TStringField
      FieldName = 'FORNCESTA'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCredenciadosACEITA_PARC: TStringField
      FieldName = 'ACEITA_PARC'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosGERA_LANC_CPMF: TStringField
      FieldName = 'GERA_LANC_CPMF'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosENCONTRO_CONTAS: TStringField
      FieldName = 'ENCONTRO_CONTAS'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCredenciadosDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCredenciadosOPERADOR: TStringField
      FieldName = 'OPERADOR'
    end
    object QCredenciadosDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCredenciadosOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCredenciadosVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object QCredenciadosDIA_PAGTO: TIntegerField
      FieldName = 'DIA_PAGTO'
    end
    object QCredenciadosTX_DVV: TBCDField
      FieldName = 'TX_DVV'
      Precision = 15
      Size = 2
    end
    object QCredenciadosUTILIZA_AUTORIZADOR: TStringField
      FieldName = 'UTILIZA_AUTORIZADOR'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosUTILIZA_COMANDA: TStringField
      FieldName = 'UTILIZA_COMANDA'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosUTILIZA_RECARGA: TStringField
      FieldName = 'UTILIZA_RECARGA'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosgrupo_estab_id: TIntegerField
      FieldName = 'grupo_estab_id'
    end
    object QCredenciadosEXIBIR_ONDE_COMPRAR: TStringField
      FieldName = 'EXIBIR_ONDE_COMPRAR'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosEXIBIR_DIRF: TStringField
      FieldName = 'EXIBIR_DIRF'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QCredenciadosCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QCredenciadosESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object HTTPRIO1: THTTPRIO
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 544
    Top = 119
  end
  object QDatas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'datas'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'EXEC getDatas12MesesaApos :datas')
    Left = 24
    Top = 303
  end
  object QInserirTransacao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 72
    Top = 279
  end
  object QInserirAutor: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 104
    Top = 279
  end
  object QInserirContaCorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 136
    Top = 279
  end
  object QGetConveniados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * FROM CONVENIADOS WHERE CONV_ID = :conv_id')
    Left = 688
    Top = 199
    object QGetConveniadosCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QGetConveniadosEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QGetConveniadosBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QGetConveniadosGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object QGetConveniadosCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QGetConveniadosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QGetConveniadosTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QGetConveniadosCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QGetConveniadosLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
    object QGetConveniadosCARGO: TStringField
      FieldName = 'CARGO'
      Size = 45
    end
    object QGetConveniadosSETOR: TStringField
      FieldName = 'SETOR'
      Size = 45
    end
    object QGetConveniadosCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QGetConveniadosRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QGetConveniadosLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QGetConveniadosCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object QGetConveniadosDIGITO_CONTA: TStringField
      FieldName = 'DIGITO_CONTA'
      Size = 2
    end
    object QGetConveniadosTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QGetConveniadosNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QGetConveniadosCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QGetConveniadosTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QGetConveniadosTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QGetConveniadosCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object QGetConveniadosOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QGetConveniadosOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QGetConveniadosEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QGetConveniadosCESTABASICA: TBCDField
      FieldName = 'CESTABASICA'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosDTULTCESTA: TDateTimeField
      FieldName = 'DTULTCESTA'
    end
    object QGetConveniadosSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosTIPOSALARIO: TStringField
      FieldName = 'TIPOSALARIO'
      Size = 15
    end
    object QGetConveniadosCOD_EMPRESA: TStringField
      FieldName = 'COD_EMPRESA'
      Size = 30
    end
    object QGetConveniadosFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosDTASSOCIACAO: TDateTimeField
      FieldName = 'DTASSOCIACAO'
    end
    object QGetConveniadosDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QGetConveniadosDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QGetConveniadosOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QGetConveniadosDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QGetConveniadosOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QGetConveniadosVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosLIBERA_GRUPOSPROD: TStringField
      FieldName = 'LIBERA_GRUPOSPROD'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object QGetConveniadosUSA_SALDO_DIF: TStringField
      FieldName = 'USA_SALDO_DIF'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosABONO_MES: TBCDField
      FieldName = 'ABONO_MES'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosSALDO_RENOVACAO: TBCDField
      FieldName = 'SALDO_RENOVACAO'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosSALDO_ACUMULADO: TBCDField
      FieldName = 'SALDO_ACUMULADO'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosDATA_ATUALIZACAO_ACUMULADO: TDateTimeField
      FieldName = 'DATA_ATUALIZACAO_ACUMULADO'
    end
    object QGetConveniadosCONSUMO_MES_1: TBCDField
      FieldName = 'CONSUMO_MES_1'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosCONSUMO_MES_2: TBCDField
      FieldName = 'CONSUMO_MES_2'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosCONSUMO_MES_3: TBCDField
      FieldName = 'CONSUMO_MES_3'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosCONSUMO_MES_4: TBCDField
      FieldName = 'CONSUMO_MES_4'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosPIS: TFloatField
      FieldName = 'PIS'
    end
    object QGetConveniadosNOME_PAI: TStringField
      FieldName = 'NOME_PAI'
      Size = 45
    end
    object QGetConveniadosnome_mae: TStringField
      FieldName = 'nome_mae'
      Size = 45
    end
    object QGetConveniadosCART_TRAB_NUM: TIntegerField
      FieldName = 'CART_TRAB_NUM'
    end
    object QGetConveniadosCART_TRAB_SERIE: TStringField
      FieldName = 'CART_TRAB_SERIE'
      Size = 10
    end
    object QGetConveniadosREGIME_TRAB: TStringField
      FieldName = 'REGIME_TRAB'
      Size = 45
    end
    object QGetConveniadosVENC_TOTAL: TBCDField
      FieldName = 'VENC_TOTAL'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosESTADO_CIVIL: TStringField
      FieldName = 'ESTADO_CIVIL'
      Size = 25
    end
    object QGetConveniadosNUM_DEPENDENTES: TIntegerField
      FieldName = 'NUM_DEPENDENTES'
    end
    object QGetConveniadosDATA_ADMISSAO: TDateTimeField
      FieldName = 'DATA_ADMISSAO'
    end
    object QGetConveniadosDATA_DEMISSAO: TDateTimeField
      FieldName = 'DATA_DEMISSAO'
    end
    object QGetConveniadosFIM_CONTRATO: TDateTimeField
      FieldName = 'FIM_CONTRATO'
    end
    object QGetConveniadosDISTRITO: TStringField
      FieldName = 'DISTRITO'
      Size = 45
    end
    object QGetConveniadosSALDO_DEVEDOR: TBCDField
      FieldName = 'SALDO_DEVEDOR'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosSALDO_DEVEDOR_FAT: TBCDField
      FieldName = 'SALDO_DEVEDOR_FAT'
      Precision = 15
      Size = 2
    end
    object QGetConveniadosSETOR_ID: TIntegerField
      FieldName = 'SETOR_ID'
    end
    object QGetConveniadosCARTAO_BEM_ESTAR: TStringField
      FieldName = 'CARTAO_BEM_ESTAR'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosENVIA_SMS: TStringField
      FieldName = 'ENVIA_SMS'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosNAO_GERA_CARTAO_TITULAR: TStringField
      FieldName = 'NAO_GERA_CARTAO_TITULAR'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosTAXA_SERVICO_CANTINA: TStringField
      FieldName = 'TAXA_SERVICO_CANTINA'
      FixedChar = True
      Size = 1
    end
    object QGetConveniadosSENHA_CANTINEX: TStringField
      FieldName = 'SENHA_CANTINEX'
      Size = 40
    end
    object QGetConveniadosBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QGetConveniadosCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QGetConveniadosESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
  end
  object QDataFechaEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 264
    Top = 231
  end
  object QDataFechaCred: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 296
    Top = 231
  end
  object Table1: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Teste de Convers' +
      'ao.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    TableDirect = True
    TableName = 'Conv$'
    Left = 88
    Top = 128
  end
  object QCartaoVenda: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CONVENIADOS.CONV_ID, CONVENIADOS.EMPRES_ID, CARTAO_ID, CO' +
        'DCARTIMP, CODIGO '
      
        'FROM CONVENIADOS INNER JOIN CARTOES ON CARTOES.CONV_ID = CONVENI' +
        'ADOS.CONV_ID '
      'WHERE CHAPA =  20995 AND CONVENIADOS.EMPRES_ID = 2334 ')
    Left = 625
    Top = 120
    object QCartaoVendaCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCartaoVendaEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCartaoVendaCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QCartaoVendaCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object QCartaoVendaCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
  end
  object QAutorTrans: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select top(1) * from dia_fecha where EMPRES_ID =  :empres_id and' +
        ' data_fecha >= GETDATE()')
    Left = 368
    Top = 168
    object QAutorTransEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QAutorTransDATA_FECHA: TDateTimeField
      FieldName = 'DATA_FECHA'
    end
    object QAutorTransDATA_VENC: TDateTimeField
      FieldName = 'DATA_VENC'
    end
  end
  object QData: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'EMPRES_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select top 6 DATA_FECHA,DATA_VENC  from DIA_FECHA where EMPRES_I' +
        'D = :empres_id'
      'and data_fecha between (SELECT DATEADD(MM,-6,GETDATE())) '
      
        'and (SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA WHERE DIA_' +
        'FECHA.DATA_FECHA >= DATEADD(month,2,getdate()))'
      'order by DATA_FECHA desc')
    Left = 32
    Top = 200
    object QDataDATA_FECHA: TDateTimeField
      FieldName = 'DATA_FECHA'
    end
    object QDataDATA_VENC: TDateTimeField
      FieldName = 'DATA_VENC'
    end
  end
  object dsData: TDataSource
    DataSet = QData
    Left = 64
    Top = 200
  end
end
