object FRelCartaCircularizacaoSpani: TFRelCartaCircularizacaoSpani
  Left = 266
  Top = 161
  Width = 928
  Height = 480
  Caption = 'Relatorio Carta Circulariza'#231#227'o Spani'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 140
    Top = 32
    Width = 500
    Height = 321
    Caption = 'Saldo Devedor e Vendas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -14
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      500
      321)
    object Label3: TLabel
      Left = 244
      Top = 121
      Width = 69
      Height = 16
      Caption = 'Data Inicial:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 244
      Top = 161
      Width = 64
      Height = 16
      Caption = 'Data Final:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 49
      Top = 32
      Width = 104
      Height = 16
      Caption = 'Local do Arquivo:'
    end
    object Label2: TLabel
      Left = 56
      Top = 120
      Width = 108
      Height = 16
      Caption = 'Tipo de Relat'#243'rio:'
    end
    object JvDateEdit1: TJvDateEdit
      Left = 349
      Top = 116
      Width = 100
      Height = 24
      ShowNullDate = False
      TabOrder = 0
    end
    object JvDateEdit3: TJvDateEdit
      Left = 349
      Top = 156
      Width = 100
      Height = 24
      ShowNullDate = False
      TabOrder = 1
    end
    object Button1: TButton
      Left = 144
      Top = 232
      Width = 217
      Height = 25
      Caption = 'Emitir Relat'#243'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button1Click
    end
    object edtCaminho: TJvDirectoryEdit
      Left = 49
      Top = 53
      Width = 403
      Height = 24
      DialogKind = dkWin32
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 3
    end
    object ComboBox1: TComboBox
      Left = 56
      Top = 136
      Width = 145
      Height = 24
      ItemHeight = 16
      TabOrder = 4
      Items.Strings = (
        'Saldo Devedor'
        'Vendas')
    end
    object ProgressBar1: TProgressBar
      Left = 2
      Top = 303
      Width = 496
      Height = 16
      Align = alBottom
      TabOrder = 5
    end
  end
  object QBusca: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 96
    Top = 344
  end
end
