unit UPesqConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, StdCtrls, DB, ComCtrls, ULancamentos, ULancIndiv,
  ADODB;

type
  TFPesqConv = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DSConveniados: TDataSource;
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    StatusBar: TStatusBar;
    lblStatus: TLabel;
    QConveniados: TADOQuery;
    QConveniadostitular: TStringField;
    QConveniadoscodcartimp: TStringField;
    QConveniadoschapa: TFloatField;
    QConveniadosconv_id: TIntegerField;
    QConveniadoscartao_id: TIntegerField;
    QConveniadoscodigo: TIntegerField;
    QConveniadosdigito: TWordField;
    QConveniadosnome: TStringField;
    QConveniadoscartlib: TStringField;
    QConveniadosconvlib: TStringField;
    QConveniadossenha: TStringField;
    QConveniadosempresa: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QConveniadosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
      FLancamentos : TFLancamentos;
      FLancIndiv : TFLancIndiv;
  end;

var
  FPesqConv: TFPesqConv;

implementation

uses DM;

{$R *.dfm}

procedure TFPesqConv.Button1Click(Sender: TObject);
var codcartimp : Extended;
begin
 Screen.Cursor := crHourGlass;
 lblStatus.Caption := 'Pesquisando...';
 QConveniados.Close;
 QConveniados.SQL.Clear;
 QConveniados.SQL.Add(' Select conveniados.titular, cartoes.codcartimp, conveniados.chapa, conveniados.conv_id, ');
 QConveniados.SQL.Add(' cartoes.cartao_id, cartoes.codigo, cartoes.digito, cartoes.nome, ');
 QConveniados.SQL.Add(' cartoes.liberado as cartlib, conveniados.liberado as convlib, conveniados.senha, empresas.nome as empresa from conveniados ');
 QConveniados.SQL.Add(' join cartoes on conveniados.conv_id = cartoes.conv_id ');
 QConveniados.SQL.Add(' join empresas on empresas.empres_id = conveniados.empres_id ');
 QConveniados.SQL.Add(' where conveniados.apagado <> ''S'' and cartoes.apagado <> ''S'' ');
 if TryStrToFloat(Edit1.Text,codcartimp) then
   QConveniados.SQL.Add(' and cartoes.codcartimp = '''+Edit1.Text+'''')
 else
   QConveniados.SQL.Add(' and cartoes.nome like '+QuotedStr('%'+Edit1.Text+'%'));
 if FPesqConv.Tag = 0 then //Foi chamado pelo FLancamentos.
 begin
   if Trim(FLancamentos.EdEmpres_id.Text) <> '' then  //N�O TA CONSEGUINDO LER O EDEMPRES_ID
   begin
     QConveniados.SQL.Add(' and conveniados.empres_id = '+FLancamentos.EdEmpres_id.Text);
   end;
 end
 else if FPesqConv.Tag = 5 then //Foi chamado pelo FLancamentos.
 begin
    if Trim(FLancIndiv.edtEmpr.Text) <> '' then  //N�O TA CONSEGUINDO LER O EDEMPRES_ID
    begin
       QConveniados.SQL.Add(' and conveniados.empres_id = '+FLancIndiv.edtEmpr.Text);
    end;
 end;
 QConveniados.SQL.Add(' order by cartoes.nome ');
 QConveniados.Open;
 if not QConveniados.IsEmpty then DBGrid1.SetFocus else ShowMessage('Conveniado n�o encontrado.');
 lblStatus.Caption := '';
 Screen.Cursor := crDefault;
end;

procedure TFPesqConv.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then Button1.Click;
if key = vk_escape then Close;
end;

procedure TFPesqConv.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if not QConveniados.IsEmpty then if key = vk_return then FPesqConv.ModalResult := MrOk;
if Key = vk_escape then Edit1.SetFocus;
case Key  of
   vk_return : DBGrid1DblClick(nil);
   VK_LEFT : DBGrid1.Perform(WM_HSCROLL,0,0);
   VK_RIGHT : DBGrid1.Perform(WM_HSCROLL,1,0);
end;
if Key in [vk_left,vk_right] then Key := 0;
end;

procedure TFPesqConv.DBGrid1DblClick(Sender: TObject);
begin
if not QConveniados.IsEmpty then FPesqConv.ModalResult := MrOk;
end;

procedure TFPesqConv.FormShow(Sender: TObject);
begin
if Trim(Edit1.Text) <> EmptyStr then Button1.Click;
end;

procedure TFPesqConv.QConveniadosAfterOpen(DataSet: TDataSet);
begin
StatusBar.SimpleText := 'Registros encontrados: '+IntToStr(QConveniados.RecordCount);
end;

end.
