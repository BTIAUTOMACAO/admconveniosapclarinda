unit UGeraTaxa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, StdCtrls, Mask, JvToolEdit, ExtCtrls, Grids, DBGrids,
  {JvDBCtrl,} ComCtrls, DB, {JvMemDS,} Buttons, JvExMask, JvExDBGrids, JvDBGrid,
  ADODB, cartao_util;

type
  TFGeraTaxa = class(TF1)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    DBGridEmp: TJvDBGrid;
    TabConv: TTabSheet;
    Panel2: TPanel;
    ButMarcDesmConv: TButton;
    ButMarcaTodosConv: TButton;
    ButDesmTodosConv: TButton;
    ButProcessa: TButton;
    DBGridConv: TJvDBGrid;
    Bevel2: TBevel;
    DSEmpresas: TDataSource;
    DSConveniados: TDataSource;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodosEmp: TButton;
    ButDesmTodosEmp: TButton;
    Panel3: TPanel;
    TabSheet3: TTabSheet;
    SoComMov: TCheckBox;
    Panel4: TPanel;
    ButMarcaDesmFornec: TButton;
    ButMarcaTodosFornec: TButton;
    ButDesmTodosFornec: TButton;
    DSFornecedores: TDataSource;
    DBGridFornec: TJvDBGrid;
    Panel5: TPanel;
    ButAbreConv: TButton;
    SoCartaoLib: TCheckBox;
    SoConvLib: TCheckBox;
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    DataIni: TJvDateEdit;
    Datafin: TJvDateEdit;
    Baixapor: TRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    Bevel1: TBevel;
    Bevel3: TBevel;
    SoCartaoTitular: TCheckBox;
    QEmpresas: TADOQuery;
    QFornecedores: TADOQuery;
    QConveniados: TADOQuery;
    QContaCorr: TADOQuery;
    QEmpresasempres_id: TIntegerField;
    QEmpresasnome: TStringField;
    QEmpresasdatafin: TDateTimeField;
    QEmpresasdataini: TDateTimeField;
    QEmpresasmarcado: TStringField;
    QFornecedorescred_id: TIntegerField;
    QFornecedoresnome: TStringField;
    QFornecedoresfantasia: TStringField;
    QFornecedoresmarcado: TStringField;
    QConveniadosTitular: TStringField;
    QConveniadosConv_id: TIntegerField;
    QConveniadosEmpres_id: TIntegerField;
    QConveniadoslimite_mes: TBCDField;
    QConveniadossalario: TBCDField;
    QConveniadosempresa: TStringField;
    QConveniadoscartao_id: TIntegerField;
    QConveniadoscodigo: TIntegerField;
    QConveniadosdigito: TWordField;
    QConveniadosmarcado: TStringField;
    QContaCorrAUTORIZACAO_ID: TIntegerField;
    QContaCorrCARTAO_ID: TIntegerField;
    QContaCorrCONV_ID: TIntegerField;
    QContaCorrCRED_ID: TIntegerField;
    QContaCorrDIGITO: TWordField;
    QContaCorrDATA: TDateTimeField;
    QContaCorrHORA: TStringField;
    QContaCorrDATAVENDA: TDateTimeField;
    QContaCorrDEBITO: TBCDField;
    QContaCorrCREDITO: TBCDField;
    QContaCorrVALOR_CANCELADO: TBCDField;
    QContaCorrBAIXA_CONVENIADO: TStringField;
    QContaCorrBAIXA_CREDENCIADO: TStringField;
    QContaCorrENTREG_NF: TStringField;
    QContaCorrRECEITA: TStringField;
    QContaCorrCESTA: TStringField;
    QContaCorrCANCELADA: TStringField;
    QContaCorrDIGI_MANUAL: TStringField;
    QContaCorrTRANS_ID: TIntegerField;
    QContaCorrFORMAPAGTO_ID: TIntegerField;
    QContaCorrFATURA_ID: TIntegerField;
    QContaCorrPAGAMENTO_CRED_ID: TIntegerField;
    QContaCorrAUTORIZACAO_ID_CANC: TIntegerField;
    QContaCorrOPERADOR: TStringField;
    QContaCorrDATA_VENC_EMP: TDateTimeField;
    QContaCorrDATA_FECHA_EMP: TDateTimeField;
    QContaCorrDATA_VENC_FOR: TDateTimeField;
    QContaCorrDATA_FECHA_FOR: TDateTimeField;
    QContaCorrHISTORICO: TStringField;
    QContaCorrNF: TIntegerField;
    QContaCorrDATA_ALTERACAO: TDateTimeField;
    QContaCorrDATA_BAIXA_CONV: TDateTimeField;
    QContaCorrDATA_BAIXA_CRED: TDateTimeField;
    QContaCorrOPER_BAIXA_CONV: TStringField;
    QContaCorrOPER_BAIXA_CRED: TStringField;
    QContaCorrDATA_CONFIRMACAO: TDateTimeField;
    QContaCorrOPER_CONFIRMACAO: TStringField;
    QContaCorrCONFERIDO: TStringField;
    QContaCorrNSU: TIntegerField;
    QContaCorrPREVIAMENTE_CANCELADA: TStringField;
    QContaCorrEMPRES_ID: TIntegerField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure DBGridEmpDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridEmpDblClick(Sender: TObject);
    procedure DBGridEmpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButMarcaTodosEmpClick(Sender: TObject);
    procedure ButDesmTodosEmpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridConvDblClick(Sender: TObject);
    procedure DBGridConvKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButMarcDesmConvClick(Sender: TObject);
    procedure ButMarcaTodosConvClick(Sender: TObject);
    procedure ButDesmTodosConvClick(Sender: TObject);
    procedure DBGridConvDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure DBGridEmpTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure DBGridConvTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ButProcessaClick(Sender: TObject);
    procedure BaixaporClick(Sender: TObject);
    procedure DataIniChange(Sender: TObject);
    procedure DBGridFornecDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridFornecDblClick(Sender: TObject);
    procedure DBGridFornecKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButAbreConvClick(Sender: TObject);
    procedure ButMarcaDesmFornecClick(Sender: TObject);
    procedure ButMarcaTodosFornecClick(Sender: TObject);
    procedure ButDesmTodosFornecClick(Sender: TObject);
    procedure DBGridFornecTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure SoComMovClick(Sender: TObject);
    procedure SoCartaoLibClick(Sender: TObject);
    procedure SoConvLibClick(Sender: TObject);
    procedure TabConvHide(Sender: TObject);
  private
    { Private declarations }
    acumularAutor: Boolean;
  public
    { Public declarations }
    function FuncSel:Boolean;
  end;


implementation

uses ULancTaxa, DM, CurrEdit, UMenu, UTipos;

{$R *.dfm}

procedure TFGeraTaxa.ButListaEmpClick(Sender: TObject);
begin
  QEmpresas.Close;
  QEmpresas.Sql.Clear;
  QEmpresas.Sql.Add(' Select empresas.empres_id, empresas.nome, ');
  if Baixapor.ItemIndex = 0 then begin
     QEmpresas.Sql.Add(' (dia_fecha.data_fecha - 1) as datafin, ');
     QEmpresas.Sql.Add(' (select Max(dia_fecha.data_fecha) from dia_fecha where dia_fecha.data_fecha < '''+FormatDateTime('mm/dd/yyyy',Dataini.Date)+''' and dia_fecha.empres_id = empresas.empres_id ) as dataini ');
  end
  else begin
     QEmpresas.Sql.Add(' cast('''+FormatDateTime('mm/dd/yyyy',Dataini.Date)+''' as date) as dataini, ');
     QEmpresas.Sql.Add(' cast('''+FormatDateTime('mm/dd/yyyy',Datafin.Date)+''' as date) as datafin ');
  end;
  QEmpresas.Sql.Add(', ''N'' as marcado from empresas ');
  if Baixapor.ItemIndex = 0 then
     QEmpresas.Sql.Add(' join dia_fecha on dia_fecha.empres_id = empresas.empres_id ');
  QEmpresas.Sql.Add(' where empresas.apagado <> ''S'' ');
  if Baixapor.ItemIndex = 0 then
     QEmpresas.Sql.Add(' and dia_fecha.data_fecha = '''+FormatDateTime('mm/dd/yyyy',Dataini.Date)+'''');
  QEmpresas.Sql.Add(' order by empresas.nome ');
  QEmpresas.Open;
  PageControl1.ActivePageIndex := 0;
  if QEmpresas.IsEmpty then begin
     ShowMessage('N�o foi encontrada nenhuma empresa com fechamento nesta data.');
     DataIni.SetFocus;
  end
  else begin
     PageControl1.ActivePageIndex := 0;
     DBGridEmp.SetFocus;
  end;
end;

procedure TFGeraTaxa.DBGridEmpDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
If QEmpresasMarcado.AsString = 'S' then DBGridEmp.Canvas.Brush.Color:= $008080FF;
DBGridEmp.DefaultDrawDataCell(Rect,DBGridEmp.columns[datacol].field,State);
end;

procedure TFGeraTaxa.DBGridEmpDblClick(Sender: TObject);
begin
ButMarcaDesmEmp.Click;
end;

procedure TFGeraTaxa.DBGridEmpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then ButMarcaDesmEmp.Click;
end;

procedure TFGeraTaxa.ButMarcaDesmEmpClick(Sender: TObject);
begin
DMConexao.MarcaDesm(QEmpresas);
end;

procedure TFGeraTaxa.ButMarcaTodosEmpClick(Sender: TObject);
begin
DMConexao.MarcaDesmTodos(QEmpresas);
end;

procedure TFGeraTaxa.ButDesmTodosEmpClick(Sender: TObject);
begin
DMConexao.MarcaDesmTodos(QEmpresas,False);
end;

procedure TFGeraTaxa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
inherited;
  if Key = vk_F5 then ButListaEmp.Click;
  if key in [vk_f2,vk_f3,vk_f4] then begin
     if PageControl1.ActivePageIndex = 0 then begin
        if Key = vk_F2  then ButMarcaDesmEmp.click;
        if Key = vk_F3  then ButMarcaTodosEmp.Click;
        if Key = vk_F4  then ButDesmTodosEmp.Click;
     end;
     if PageControl1.ActivePageIndex = 1 then begin
        if Key = vk_F2  then ButMarcDesmConv.Click;
        if Key = vk_F3  then ButMarcaTodosConv.Click;
        if Key = vk_F4  then ButDesmTodosConv.Click;
        if key = vk_F7  then ButProcessa.Click;
     end;
     if PageControl1.ActivePageIndex = 2 then begin
        if Key = vk_F2  then ButMarcaTodosFornec.Click;
        if Key = vk_F3  then ButMarcaTodosFornec.Click;
        if Key = vk_F4  then ButDesmTodosFornec.Click;
     end;
  end;
end;

procedure TFGeraTaxa.DBGridConvDblClick(Sender: TObject);
begin
ButMarcDesmConv.Click;
end;

procedure TFGeraTaxa.DBGridConvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
if key = vk_return then ButMarcDesmConv.Click;
end;

procedure TFGeraTaxa.ButMarcDesmConvClick(Sender: TObject);
begin
DMConexao.MarcaDesm(QConveniados);
end;

procedure TFGeraTaxa.ButMarcaTodosConvClick(Sender: TObject);
begin
DMConexao.MarcaDesmTodos(QConveniados);
end;

procedure TFGeraTaxa.ButDesmTodosConvClick(Sender: TObject);
begin
DMConexao.MarcaDesmTodos(QConveniados,False);
end;

procedure TFGeraTaxa.DBGridConvDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
If QConveniadosmarcado.AsString = 'S' then DBGridConv.Canvas.Brush.Color:= $008080FF;
DBGridConv.DefaultDrawDataCell(Rect,DBGridConv.columns[datacol].field,State);
end;

procedure TFGeraTaxa.FormCreate(Sender: TObject);
begin
  inherited;
  DataIni.Date := Date;
  DataIniChange(nil);
  QFornecedores.Open;
  DMConexao.Config.Open;
  acumularAutor := (DMConexao.ConfigACUMULA_AUTOR_PROX_FECHA.AsString = 'S');
  DMConexao.Config.Close;
end;

procedure TFGeraTaxa.DBGridEmpTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
 //comentado ariane QEmpresas.SortedFields := Field.FieldName;
end;

procedure TFGeraTaxa.DBGridConvTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  //comentado ariane QConveniados.SortedFields := Field.FieldName;
end;

procedure TFGeraTaxa.ButProcessaClick(Sender: TObject);
var i : integer;
begin
 if QConveniados.IsEmpty then begin
    ShowMessage('Nenhum funcion�rio selecionado.');
    exit;
 end;
 if not FuncSel then begin
    ShowMessage('Nenhum funcion�rio selecionado.');
    exit;
 end;
 FLancTaxa := TFLancTaxa.Create(self);
 FLancTaxa.FGeraTaxa := Self;
 for i := 0 to QConveniados.Fields.Count - 1 do begin
     if (( QConveniados.Fields[i].FieldName = 'LIMITE_MES') or (QConveniados.Fields[i].FieldName = 'SALARIO')) then begin
       { FLancTaxa.TempCampos.Append;
        FLancTaxa.TempCamposCampo.AsString     := QConveniados.Fields[i].DisplayName;
        FLancTaxa.TempCamposFieldName.AsString := QConveniados.Fields[i].FieldName;
        FLancTaxa.TempCamposTipo.AsVariant     := Variant(QConveniados.Fields[i].DataType);
        FLancTaxa.TempCamposValor.AsVariant    := QConveniados.Fields[i].AsVariant;
        FLancTaxa.TempCamposSize.AsInteger     := QConveniados.Fields[i].Size;
        FLancTaxa.TempCampos.Post;  }//comentado ariane
     end;
 end;
 {FLancTaxa.TempCampos.SortOnFields('Campo'); }//comentado ariane
 if Baixapor.ItemIndex = 0 then FLancTaxa.Data.Date := Trunc(DataIni.Date)-1
                           else FLancTaxa.Data.Date := Datafin.Date;
 FLancTaxa.ShowModal;
 if FLancTaxa.ModalResult = mrOk then begin
    QConveniados.First;
    QContaCorr.Open;
    //ariane DMConexao.AdoCon.AutoCommit := False;
    while not QConveniados.Eof do begin
       if QConveniadosMARCADO.AsString = 'S' then begin
         { DMConexao.StoredProc1.ProcedureName       := 'PEGA_AUTORIZACAO_ID';
          DMConexao.StoredProc1.ExecProc;
          QContaCorr.Append;
          QContaCorrAUTORIZACAO_ID.AsInteger   := DMConexao.StoredProc1.Parameters.ParamValues .Params[0].AsInteger;
          QContaCorrDIGITO.AsInteger           := DigitoControle(QContaCorrAUTORIZACAO_ID.AsFloat);
          QContaCorrBAIXA_CONVENIADO.AsString  := 'N';
          QContaCorrBAIXA_CREDENCIADO.AsString := 'N';
          QContaCorrCARTAO_ID.AsInteger        := QConveniadoscartao_id.AsInteger;
          QContaCorrCONV_ID.AsInteger          := QConveniadosConv_id.AsInteger;
          if FLancTaxa.ChExpressao.Checked then begin
             if FLancTaxa.chExpressCredito.Checked then begin
                QContaCorrCREDITO.AsCurrency   := FLancTaxa.CalcExpressao;
                QContaCorrDEBITO.AsCurrency   := 0;
             end
             else begin  // Lan�amento calculado entra no d�bito.
                QContaCorrDEBITO.AsCurrency   := FLancTaxa.CalcExpressao;
                QContaCorrCREDITO.AsCurrency   := 0;
             end;
          end
          else begin
             QContaCorrCREDITO.AsCurrency      := FLancTaxa.Credito.Value;
             QContaCorrDEBITO.AsCurrency       := FLancTaxa.Debito.Value;
          end;
          QContaCorrCRED_ID.AsInteger          := FLancTaxa.QForneccred_id.AsInteger;
          QContaCorrDATA.AsDateTime            := FLancTaxa.Data.Date;
          //QContaCorrDATA_FECHA_EMP.AsDateTime  := Dm1.data_fecha_emp(FLancTaxa.Data.Date,QConveniadosempres_id.AsInteger);
          //QContaCorrDATA_VENC_EMP.AsDateTime   := Dm1.data_venc_emp(QContaCorrDATA_FECHA_EMP.AsDateTime,QConveniadosempres_id.AsInteger);
          QContaCorrHISTORICO.AsString         := FLancTaxa.EdHist.Text;
          QContaCorrOPERADOR.AsString          := Operador.Nome;
          QContaCorrDATA_ALTERACAO.AsDateTime  := Date;
          QContaCorrHORA.AsString              := FormatDateTime('hh:nn:ss',now);
          QContaCorr.Post;
          Application.ProcessMessages;   }     //comentado ariane arrumar depois ja que usa procedure
       end;
       QConveniados.Next;
    end;
    //DMConexao.AdoCon.Commit;
    //arianeDMConexao.Connection1.AutoCommit := True;
    QContaCorr.Close;
    ShowMessage('Taxas geradas com sucesso.');
 end;
 FLancTaxa.Free;
end;

function TFGeraTaxa.FuncSel:Boolean;
var marca : TBookmark;
begin
Result := False;
QConveniados.DisableControls;
marca := QConveniados.GetBookmark;
QConveniados.First;
while not QConveniados.Eof do begin
   if QConveniadosmarcado.AsString = 'S' then begin
      Result := True;
      Break;
   end;
   QConveniados.Next;
end;
QConveniados.GotoBookmark(marca);
QConveniados.FreeBookmark(marca);
QConveniados.EnableControls;
end;

procedure TFGeraTaxa.BaixaporClick(Sender: TObject);
begin

if Baixapor.ItemIndex = 0 then begin
   Label1.Caption   := 'Dia de Fechamento';
   Datafin.Visible    := False;
   ButListaEmp.Left := 416;
end
else begin
   Label1.Caption   := 'Per�odo - Data Inicial      Data Final';
   Datafin.Visible    := True;
   ButListaEmp.Left := 536;
end;

end;

procedure TFGeraTaxa.DataIniChange(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
  QEmpresas.Close;
  Datafin.Date := Trunc(IncMonth(DataIni.Date,1))-1;
end;

procedure TFGeraTaxa.DBGridFornecDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
If QFornecedoresMARCADO.AsString = 'S' then DBGridFornec.Canvas.Brush.Color:= $008080FF;
DBGridFornec.DefaultDrawDataCell(Rect,DBGridFornec.columns[datacol].field,State);
end;

procedure TFGeraTaxa.DBGridFornecDblClick(Sender: TObject);
begin
  inherited;
ButMarcaDesmFornec.Click;
end;

procedure TFGeraTaxa.DBGridFornecKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
if key = vk_return then ButMarcaDesmFornec.Click;
end;

procedure TFGeraTaxa.ButAbreConvClick(Sender: TObject);
var empresas, fornecedores : string;
begin
  if not QEmpresas.Active Then
    begin
    Showmessage('Selecione uma empresa para encontrar os conveniados');
    Abort;
    end;
  QEmpresas.First;
  while not QEmpresas.Eof do begin
     if QEmpresasMARCADO.AsString = 'S' then empresas := empresas + ',' + QEmpresasEMPRES_ID.AsString;
     QEmpresas.Next;
  end;
  if Trim(empresas) = '' then begin
     ShowMessage('Nenhuma empresa selecionada.');
     Exit;
  end;
  Delete(empresas,1,1);
  if SoComMov.Checked then begin
     QFornecedores.First;
     while not QFornecedores.Eof do begin
        if QFornecedoresMARCADO.AsString = 'S' then fornecedores := fornecedores + ',' + QFornecedoresCRED_ID.AsString;
        QFornecedores.Next;
     end;
     if Trim(fornecedores) <> '' then begin
        delete(fornecedores,1,1);
        if Application.MessageBox('Aten��o, o sistema listar� os conveniados com movimento no per�odo informado e nos fornecedores selecionado.','Aten��o',MB_OKCANCEL+MB_ICONWARNING) = IDCancel then Exit;
     end
     else if Application.MessageBox('Aten��o, o sistema listar� os conveniados com movimento no per�odo informado.','Aten��o',MB_OKCANCEL+MB_ICONWARNING) = IDCancel then Exit;
  end;
  Screen.Cursor := crHourGlass;
  QConveniados.Close;
  QConveniados.SQL.Clear;
  QConveniados.SQL.Add('  select distinct conveniados.Conv_id, conveniados.Titular, conveniados.Empres_id, ');
  QConveniados.SQL.Add('  conveniados.limite_mes, conveniados.salario, ');
  QConveniados.SQL.Add('  empresas.nome as empresa, cartoes.cartao_id, cartoes.codigo, ');
  QConveniados.SQL.Add('  cartoes.digito, ''N'' as marcado from conveniados ');
  QConveniados.SQL.Add('  join empresas on empresas.empres_id = conveniados.empres_id ');
  QConveniados.SQL.Add('  join cartoes on cartoes.conv_id = conveniados.conv_id ');
  if SoComMov.Checked then
     QConveniados.SQL.Add('  join contacorrente on contacorrente.conv_id = conveniados.conv_id ');
  QConveniados.SQL.Add('  where conveniados.apagado <> ''S'' and cartoes.apagado = ''N'' ');
  QConveniados.SQL.Add('  and conveniados.empres_id in ('+Empresas+')');

  if SoConvLib.Checked       then QConveniados.SQL.Add('  and conveniados.liberado = ''S'' ');
  if SoCartaoLib.Checked     then QConveniados.SQL.Add('  and cartoes.liberado = ''S'' ');
  if SoCartaoTitular.Checked then QConveniados.SQL.Add('  and cartoes.titular = ''S'' ');
  if SoComMov.Checked then begin
     QConveniados.SQL.Add('  and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
     if Baixapor.ItemIndex = 0 then
     begin
        if acumularAutor then
           QConveniados.SQL.Add(' and contacorrente.data_fecha_emp <= '+formatdataIB(DataIni.Date))
        else
           QConveniados.SQL.Add(' and contacorrente.data_fecha_emp = '+FormatDataIB(DataIni.Date));
     end
     else
       QConveniados.SQL.Add('  and contacorrente.data between '+formatdataIB(DataIni.Date)+' and '+FormatDataIB(Datafin.Date));
     if Trim(fornecedores) <> '' then
       QConveniados.SQL.Add('  and contacorrente.cred_id in ('+fornecedores+')');
  end;
  QConveniados.SQL.Add('  order by conveniados.titular ');
  QConveniados.SQL.Text;
  QConveniados.Open;
  if not QConveniados.IsEmpty then DBGridConv.SetFocus;
  Screen.Cursor := crDefault;
end;

procedure TFGeraTaxa.ButMarcaDesmFornecClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesm(QFornecedores);
end;

procedure TFGeraTaxa.ButMarcaTodosFornecClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesmTodos(QFornecedores);
end;

procedure TFGeraTaxa.ButDesmTodosFornecClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesmTodos(QFornecedores,False);
end;

procedure TFGeraTaxa.DBGridFornecTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  //comentado ariane QFornecedores.SortedFields := Field.FieldName;
end;

procedure TFGeraTaxa.SoComMovClick(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
end;

procedure TFGeraTaxa.SoCartaoLibClick(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
end;

procedure TFGeraTaxa.SoConvLibClick(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
end;

procedure TFGeraTaxa.TabConvHide(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
end;

end.
