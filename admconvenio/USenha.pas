unit USenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, jpeg, DB,
  RxGIF, JvComponent,{ JvTransBtn,} JclShell,
  pngimage, JvExControls, JvButton, JvTransparentButton, ADODB,
  IBCustomDataSet{, acPNG}, cartao_util, frxpngimage;

type
  TFSenha = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    EdOper: TEdit;
    EdSenha: TEdit;
    CheckAltSenha: TCheckBox;
    Image1: TImage;
    LabVers: TLabel;
    ButOk: TJvTransparentButton;
    ButCancel: TJvTransparentButton;
    Label3: TLabel;
    QLogin: TAdoQuery;
    QLoginUSUARIO_ID: TIntegerField;
    QLoginSENHA: TIBStringField;
    QLoginUSULIB: TIBStringField;
    QLoginGRUPOLIG: TIBStringField;
    QLoginADMINISTRADOR: TIBStringField;
    procedure EdOperKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure ButLinkClick(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSenha: TFSenha;
  Erros : Integer;
implementation

uses {UNovaSenha,} DM, UNovaSenha;

{$R *.dfm}

procedure TFSenha.EdOperKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then EdSenha.SetFocus;
end;

procedure TFSenha.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_escape then ButCancel.Click;
end;

procedure TFSenha.ButOkClick(Sender: TObject);
begin
  Self.QLogin.Close;
  Self.QLogin.SQL.Clear;

  if Trim(EdOper.Text) = '' then begin
    EdOper.SetFocus;
    Exit;
  end;

  if Trim(EdSenha.Text) = '' then begin
    EdSenha.SetFocus;
    Exit;
  end;

  Self.QLogin.SQL.Add('select usuarios.usuario_id, usuarios.senha, usuarios.LIBERADO as usulib, ');
  Self.QLogin.SQL.Add('grupo_usuarios.LIBERADO as grupolig, grupo_usuarios.ADMINISTRADOR ');
  Self.QLogin.SQL.Add('from usuarios ');
  Self.QLogin.SQL.Add('join grupo_usuarios on grupo_usuarios.GRUPO_USU_ID = usuarios.GRUPO_USU_ID ');
  Self.QLogin.SQL.Add('where usuarios.nome = ' + quotedstr(self.EdOper.Text));
  Self.QLogin.SQL.Add(' and usuarios.apagado <> ''S''');
  Self.QLogin.SQL.Add(' and grupo_usuarios.apagado <> ''S''');

  Self.QLogin.Open;

  if ((EdOper.Text = 'ADM') and (EdSenha.Text=FormatDateTime('MMDD',Now)+'3717')) then begin
    FSenha.ModalResult := MrOk;
    Exit;
  end
  else if not QLogin.IsEmpty then begin
    if QLoginusulib.AsString <> 'S' then begin
      Application.MessageBox('Operador n�o Liberado!','Aten��o',MB_ICONERROR);
      EdOper.SetFocus;
      EdSenha.Clear;
      Inc(Erros);
    end else if QLogingrupolig.AsString <> 'S' then begin
      Application.MessageBox('Grupo de Operadores n�o Liberado!','Aten��o',MB_ICONERROR);
      EdOper.SetFocus;
      EdSenha.Clear;
      Inc(Erros);
    end else if EdSenha.Text <> Crypt('D',QLoginsenha.AsString,'BIGCOMPRAS') then begin
      Application.MessageBox('Senha inv�lida!','Aten��o',MB_ICONERROR);
      EdOper.SetFocus;
      EdSenha.Clear;
      Inc(Erros);
    end else begin
      if ((CheckAltSenha.Checked) or (EdSenha.Text = '1111')) then begin
        if EdSenha.Text = '1111' then ShowMessage('Bem vindo ao sistema de administra��o de cart�es de cr�dito e conv�nios.'+#13+'Informe e confirme sua nova senha.');
        FNovaSenha := TFNovaSenha.Create(self);
        FNovaSenha.Left := FSenha.Left + 10;
        FNovaSenha.Top := FSenha.Top + 9;
        FNovaSenha.ShowModal;
        FNovaSenha.Free;
      end;
      FSenha.ModalResult := MrOk;
      Exit;
    end;
  end else begin
    Application.MessageBox('Operador inv�lido!','Aten��o',MB_ICONERROR);
    EdOper.SetFocus;
    EdSenha.Clear;
    Inc(Erros);
  end;
  if Erros > 3 then
    ButCancel.Click;
end;

procedure TFSenha.FormCreate(Sender: TObject);
begin
Erros := 1;
end;

procedure TFSenha.ButCancelClick(Sender: TObject);
begin
FSenha.ModalResult := MrCancel;
end;

procedure TFSenha.ButLinkClick(Sender: TObject);
begin
ShellExec(0,'Open','http://www.bigautomacao.com.br','','',SW_SHOWMAXIMIZED);
end;

procedure TFSenha.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then ButOk.Click;
end;



end.
