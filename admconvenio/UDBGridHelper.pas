unit UDBGridHelper;

interface

uses sysutils, grids, dbgrids, menus, Classes, Forms, Windows, strutils, db, DBTables, DBClient,
     math, comObj, dialogs, controls, Graphics, messages, TypInfo, Variants, Excelxp, JvMemoryDataset;
type
  THackDBgrid = class(TDBGrid);
  THackColumn = class(TColumn);

  TFormOnCloseMap = class(TComponent)
     private
        _Form : TForm;
        OldFormDestroy : TNotifyEvent;
        procedure GravarConfGrades(Sender: TObject);
     public
        procedure SetForm(Form:TForm);

  end;

  TDBGridHelper = class(TComponent)
  public
    procedure AddHelperToGrid(DBGrid:TDBGrid);
    procedure Grade_to_PlanilhaExcel(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='');
    procedure Grade_to_PlanilhaCalc(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='');
    class procedure DBGridHelperTratrForm(Form:TForm;Query:TDataSet;Oper_ID:Integer);
    class procedure AutoZebrarGrid(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    Grid:TDBGrid;
    Oper_id : Integer;
    Query : TDataSet;
    FormClass : String;
    GridKeyDown : TKeyEvent;
    GridColumnCellEvent : TDrawColumnCellEvent;
    procedure TratarPoupUp;
    function ItemMenuFactory(Caption,Name: String;AOwner: TPopupMenu;OnClickEvent:TNotifyEvent): TMenuItem;
    procedure ExportarExcel(Sender: TObject);
    procedure ExportarTexto(Sender: TObject);
    procedure Imprimir(Sender: TObject);
    procedure DBGridToTxt;
    procedure MostrarFormListagem;
    function CriarLinha(Titulo:Boolean=False):String;
    procedure ConfGrid;
    procedure ConfigurarGrade(Sender: TObject);
    procedure ObterConfigGrade;
    procedure SalvarConfigGrade;
    //Eventos
    procedure DBGridAutoScroll(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure CompararGridsCol(ColsSalvas: TDBGridColumns);
    function ColExiste(Cols: TDBGridColumns; Column:TColumn): Boolean;
    procedure TitleClick(Column: TColumn);

    procedure ZebrarGrid(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure LocateGrade;
    function ConvertLetra(num: integer): string;
    function FileName2URL(FileName: string): string;
    function MakePropertyValue(ooServer: Variant; PropName: string;
      PropValue: variant): variant;
    procedure ooDispatch(ooServer, ooDocument: Variant; ooCommand: string;
      ooParams: variant);
    function RetornaRange(left, top, right, botton: integer): string;
  end;

implementation

uses impressao, cartao_util, UNewGeraLista, UColunas, UPesqGrade, Excel_TLB;

{ TDBGridHelper }

procedure TDBGridHelper.TratarPoupUp;
var PopUp : TPopupMenu;
    //i : integer;
begin
   if Assigned(Grid.PopupMenu) then 
      PopUp := Grid.PopupMenu
   else
      PopUp := TPopupMenu.Create(nil);

   if PopUp.Items.Count > 0 then begin
      PopUp.Items.Add(ItemMenuFactory('-','Sep1' ,PopUp,nil));
   end;

   if (PopUp.Items.Find('Configurar Grade') = nil) then begin
      PopUp.Items.Add(ItemMenuFactory('Configurar Grade','ConfGrid',PopUp,ConfigurarGrade));
   end;

   PopUp.Items.Add(ItemMenuFactory('-','Sep2',PopUp,nil));

   if (PopUp.Items.Find('Exportar para Excel') = nil) then
      PopUp.Items.Add(ItemMenuFactory('Exportar para Excel','ExpExcel',PopUp,ExportarExcel));

   if (PopUp.Items.Find('Exportar para Texto') = nil) then
      PopUp.Items.Add(ItemMenuFactory('Exportar para Texto','ExpText',PopUp,ExportarTexto));

   if (PopUp.Items.Find('Imprimir',) = nil) then begin
      PopUp.Items.Add(ItemMenuFactory('-','Sep3',PopUp,nil));
      PopUp.Items.Add(ItemMenuFactory('Imprimir','Imp',PopUp,Imprimir));
   end;
   Grid.PopupMenu := PopUp;

end;

function TDBGridHelper.ItemMenuFactory(Caption,Name:String;AOwner:TPopupMenu;OnClickEvent:TNotifyEvent):TMenuItem;
begin
   Result := TMenuItem.Create(nil);
   Result.Caption := Caption;
   Result.OnClick := OnClickEvent;
   Result.Name    := Name;
end;

procedure TDBGridHelper.ConfigurarGrade(Sender: TObject);
begin
    try
       ConfGrid;
    except on e:Exception do
       MsgErro('Erro ao configurar grade, erro: '+e.message);
    end;
end;

procedure TDBGridHelper.ExportarExcel(Sender: TObject);
begin
//    try
       Grade_to_PlanilhaExcel((Screen.ActiveControl as TDBGrid),'',True);
//    except on e:Exception do
//       MsgErro('Erro ao exportar a grade, erro: '+e.message);
//    end;
end;

procedure TDBGridHelper.ExportarTexto(Sender: TObject);
begin
   try
      DBGridToTxt;
   except on e:Exception do
      MsgErro('Erro ao exportar para texto, erro: '+e.message);
   end;
end;

procedure TDBGridHelper.Imprimir(Sender: TObject);
begin
  MostrarFormListagem;
end;

procedure TDBGridHelper.MostrarFormListagem;
var FNewGeraLista : TFNewGeraLista;
begin
  FNewGeraLista := TFNewGeraLista.Create(Screen.ActiveForm);
  if Query is TJvMemoryData then
    MsgInf('bosta...');
  FNewGeraLista.Query := Query;
  FNewGeraLista.Dados := Grid.DataSource.DataSet;
  FNewGeraLista.Grade := Grid;

  FNewGeraLista.EdTitulo.Text := 'Impess�o - '+Screen.ActiveForm.Caption;
  FNewGeraLista.TelaAtual := Copy(Screen.ActiveForm.ClassName,2,length(Screen.ActiveForm.ClassName));
  SetLength(FNewGeraLista.widths, Grid.FieldCount);
  FNewGeraLista.CriarCampos;
  Grid.DataSource.DataSet.DisableControls;
  FNewGeraLista.ShowModal;
  FNewGeraLista.Free;
  Grid.DataSource.DataSet.EnableControls;
//  FreeAndNil(FNewGeraLista);
end;

Procedure TDBGridHelper.Grade_to_PlanilhaExcel(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='');
var
  Dados : Variant;  Range : String;
  i, linha, ColsCount: integer;
  Excel, WorkSheet: Variant;
  ColsVisible : TList;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
  DataSet : TDataSet;
begin
  try
    Excel := CreateOleObject('Excel.Application');
    Excel.Workbooks.Add;
    WorkSheet := Excel.ActiveSheet;
    if Nome <> '' then
       Excel.ActiveSheet.Name := Nome;
    if SalvarComo <> '' then
       Excel.ActiveSheet.SaveAs(SalvarComo, 56);
  except
    Grade_to_PlanilhaCalc(Grade,Nome,MostrarTitulos,SalvarComo);
    SysUtils.Abort;
  end;
  try
    Screen.Cursor := crHourGlass;
    DataSet := Grade.DataSource.DataSet;
    if DataSet.IsEmpty then begin
       MsgInf('N�o h� dados para serem exportados');
       Screen.Cursor := crDefault;
       Exit;
    end;
    ColsVisible := TList.Create;
    for i := 0 to Grade.Columns.Count-1 do begin
        if Grade.Columns[i].Visible then
           ColsVisible.Add(Grade.Columns[i]);
    end;
    ColsCount := ColsVisible.Count-1;
    Dados := VarArrayCreate([0, DataSet.RecordCount,0,ColsVisible.Count], varVariant);
    if MostrarTitulos then begin
       for i := 0 to ColsCount do begin
           Dados[0,i] := TColumn(ColsVisible[i]).Title.Caption;
       end;
    end;

    AfterScroll  := DataSet.AfterScroll;
    BeforeScroll := DataSet.BeforeScroll;
    DataSet.AfterScroll  := nil;
    DataSet.BeforeScroll := nil;
    DataSet.First;
    while not DataSet.Eof do begin
       for i :=0 to ColsCount do begin
          try
            if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
               Dados[DataSet.RecNo,i] := RoundTo(TColumn(ColsVisible[i]).Field.AsCurrency,-2)
            else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then begin
               if (TColumn(ColsVisible[i]).Field.AsString <> '') then
                  Dados[DataSet.RecNo,i] := TColumn(ColsVisible[i]).Field.AsDateTime
               else
                  Dados[DataSet.RecNo,i] := '';
            end else
               Dados[DataSet.RecNo,i] := TColumn(ColsVisible[i]).Field.DisplayText;
          except end;
       end;
       Grade.DataSource.DataSet.Next;
    end;

    for i :=0 to ColsCount do begin
        Range := WorkSheet.Cells[2, i+1].AddressLocal+':'+
                 WorkSheet.Cells[DataSet.Recno, i+1].AddressLocal;

        if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then begin
           WorkSheet.Range[Range].NumberFormat := '#.##0,00';
        end
        else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then begin
           WorkSheet.Range[Range].HorizontalAlignment := xlRight;
        end
        else if IsStringField(TColumn(ColsVisible[i]).Field.DataType) then
           WorkSheet.Range[Range].NumberFormat := '@';
    end;

    Range := 'A1:'+WorkSheet.Cells[DataSet.RecordCount+1, ColsCount+1].AddressLocal;
    WorkSheet.Range[Range].value := dados;

    WorkSheet.Columns.Autofit;

    DataSet.AfterScroll  := AfterScroll;
    DataSet.BeforeScroll := BeforeScroll;
    DataSet.First;
    Excel.Visible := True;
    Screen.Cursor := crDefault;
  except on E:Exception do  begin
       DataSet.AfterScroll  := AfterScroll;
       DataSet.BeforeScroll := BeforeScroll;
       DataSet.First;
       Screen.Cursor := crDefault;
       ShowMessage('Ocorreu um problema durande a gera��o do arquivo.'+#13+'Erro: '+E.Message);
    end;
  end;
end;


procedure TDBGridHelper.DBGridToTxt;
var Txt : TextFile; Arq : String;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
begin
   Arq := ExtractFilePath(Application.ExeName)+'Grade_'+FormatDateTime('hh_nn',now)+'.txt';
   AssignFile(Txt,Arq);
   Rewrite(Txt);
   Writeln(Txt,CriarLinha(True));
   AfterScroll  := Grid.DataSource.DataSet.AfterScroll;
   BeforeScroll := Grid.DataSource.DataSet.BeforeScroll;
   Grid.DataSource.DataSet.AfterScroll  := nil;
   Grid.DataSource.DataSet.BeforeScroll := nil;
   Grid.DataSource.DataSet.First;
   while not Grid.DataSource.DataSet.Eof do begin
      Writeln(Txt,CriarLinha);
      Grid.DataSource.DataSet.Next;
   end;
   CloseFile(Txt);
   Grid.DataSource.DataSet.AfterScroll  := AfterScroll;
   Grid.DataSource.DataSet.BeforeScroll := BeforeScroll;
   Grid.DataSource.DataSet.First;
   WinExec(PChar('notepad.exe '+Arq),SW_SHOWMAXIMIZED);
end;

function TDBGridHelper.CriarLinha(Titulo:Boolean=False):String;
var i,Tamanho : integer; Linha, Coluna :String;
begin
   Linha := EmptyStr;
   for i := 0 to pred(Grid.Columns.Count) do begin
       if not Grid.Columns[i].Visible then
          Continue;
       Tamanho := iif(Grid.Columns[i].Field.DisplayWidth > length(Grid.Columns[i].Title.Caption),Grid.Columns[i].Field.DisplayWidth,length(Grid.Columns[i].Title.Caption));
       if Titulo then
          Coluna := Grid.Columns[i].Title.Caption
       else
          Coluna := Grid.Columns[i].Field.DisplayText;

       if IsNumericField(Grid.Columns[i].Field.DataType) then
          Coluna := Direita(Coluna,' ',Tamanho)
       else begin
          case Grid.Columns[i].Title.Alignment of
             taLeftJustify  : Coluna := Preenche(Coluna,' ',Tamanho);
             taRightJustify : Coluna := Direita(Coluna,' ',Tamanho);
             taCenter       : Coluna := Centraliza(Coluna,' ',Tamanho);
          end;
       end;
       Linha := Linha +'  '+ Coluna;
   end;
   Result := Linha;
end;

procedure TDBGridHelper.AddHelperToGrid(DBGrid: TDBGrid);
var UsandoTitleClick : Boolean;
begin
   Grid := DBGrid;
   TratarPoupUp;
   GridKeyDown      := DBGrid.OnKeyDown;
   DBGrid.OnKeyDown := DBGridAutoScroll;
   GridColumnCellEvent := DBGrid.OnDrawColumnCell;
   DBGrid.OnDrawColumnCell := ZebrarGrid;

   UsandoTitleClick := IsPublishedProp(Grid,'OnTitleBtnClick') and (GetMethodProp(Grid,'OnTitleBtnClick').code <> nil);

   if (not Assigned(Grid.OnTitleClick) and (not UsandoTitleClick)) then
      Grid.OnTitleClick := TitleClick;
   ObterConfigGrade;
end;

procedure TDBGridHelper.ConfGrid;
var i,j  : integer;
begin
   FColunas := TFColunas.Create(nil);
   For i := 0 to Grid.Columns.Count-1 do begin
      if Grid.Columns[i].Field <> nil then begin
         FColunas.CheckList.Items.Append(Grid.Columns[i].Title.Caption);
         FColunas.CheckList.Checked[FColunas.CheckList.Count-1] := Grid.Columns[i].Visible;
      end
      else
         FColunas.CheckList.Items.Append(Grid.Columns[i].Title.Caption);

   end;
   FColunas.ShowModal;
   if FColunas.ModalResult = mrOk then begin
      for i := 0 to FColunas.CheckList.Items.Count-1 do begin
         if FColunas.CheckList.Items[i] = Grid.Columns[i].Title.Caption then begin
            Grid.Columns[i].Visible := FColunas.CheckList.Checked[i];
         end
         else begin
            for j := 0 to Grid.Columns.Count - 1 do begin
                if Grid.Columns[j].Title.Caption = FColunas.CheckList.Items[i] then begin
                   Grid.Columns[j].Visible := FColunas.CheckList.Checked[i];
                   Grid.Columns[j].Index   := i;
                   Break;
                end;
            end;
         end;
      end;
   end;
   Grid.Repaint;
   FColunas.Free;
end;

procedure TDBGridHelper.DBGridAutoScroll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Assigned(GridKeyDown) then
     GridKeyDown(Sender,Key,Shift);
  if dgRowSelect in TDBGrid(Sender).Options then begin
     case Key  of
        VK_LEFT  : begin
           TDBGrid(Sender).Perform(WM_HSCROLL,0,0);
           Key := 0;
        end;
        VK_RIGHT : begin
          TDBGrid(Sender).Perform(WM_HSCROLL,1,0);
          Key := 0;
        end;
     end;
  end;
  if (( ssCtrl in Shift ) and (Key = $46)) then
     LocateGrade;
end;

class procedure TDBGridHelper.DBGridHelperTratrForm(Form: TForm;Query:TDataSet;Oper_ID:Integer);
var i : Integer; Helper : TDBGridHelper; FormMap : TFormOnCloseMap;
begin
   for i := 0 to pred(Form.ComponentCount) do begin
       if Form.Components[i] is TDBGrid then begin
          Helper := TDBGridHelper.Create(Form);
          Helper.FormClass    := Form.ClassName;
          Helper.Query   := Query;
          Helper.Oper_id := Oper_id;
          Helper.AddHelperToGrid( (Form.Components[i] as TDBGrid) );
       end;
   end;
   FormMap := TFormOnCloseMap.Create(Form);
   FormMap.SetForm(Form);
end;

procedure TDBGridHelper.ObterConfigGrade;
var FormGradeName : String; SQL : TStrings;
Cols : TDBGridColumns;  BlobStream : TStream;
begin
{   if Query <> nil then begin
      Query.Close;
      FormGradeName := Copy(FormClass,2,length(FormClass))+Grid.Name; //Para manter codigo legado do ADM.
      Sql := TStringList.Create;
      Sql.Add(' Select COLUNAS from conf_grades where oper_id = '+inttostr(oper_id)+' and FORM_GRADE_NAME = "'+FormGradeName+'"');
      SetObjectProp(Query,'SQL', Sql);
      SetPropValue(Query,'RequestLive',True);
      Query.Open;
      if (not Query.isempty) and (not Query.Fields[0].IsNull) then begin
         try
            BlobStream := Query.CreateBlobStream(Query.Fields[0],bmRead);
            Cols := TDBGridColumns.Create(Grid,TColumn);
            Cols.LoadFromStream(BlobStream);
            CompararGridsCol(Cols);
            Grid.Columns := Cols;
            FreeAndNil(BlobStream);
         except end;
      end;
      Query.Close;
   end;}
end;

procedure TDBGridHelper.CompararGridsCol(ColsSalvas:TDBGridColumns);
var i,j : integer; New : TColumn;
begin
   //Verifica as colunas que existem na grade e nao existe no bd e cria-as.
   for i := 0 to Pred(Grid.Columns.Count) do begin
       if not ColExiste(ColsSalvas,Grid.Columns[i]) then begin
          New := ColsSalvas.Add;
          ColsSalvas[ColsSalvas.Count-1] := Grid.Columns[i];
       end;
   end;
end;

function TDBGridHelper.ColExiste(Cols:TDBGridColumns; Column:TColumn):Boolean;
var I : integer;
begin
Result := False;
for i := 0 to pred(Cols.Count) do
    if Cols[i].FieldName = Column.FieldName then begin
       Cols[i].Alignment       := Column.Alignment;
       Cols[i].ButtonStyle     := Column.ButtonStyle;
       Cols[i].Color           := Column.Color;
       Cols[i].DropDownRows    := Column.DropDownRows;
       Cols[i].Expanded        := Column.Expanded;
       Cols[i].Font.Assign(Column.Font);
       Cols[i].ImeMode         := Column.ImeMode;
       Cols[i].ImeName         := Column.ImeName;
       Cols[i].PickList        := Column.PickList;
       Cols[i].PopupMenu       := Column.PopupMenu;
       Cols[i].Title.Alignment := Column.Title.Alignment;
       Cols[i].Title.Caption   := Column.Title.Caption;
       Cols[i].Title.Color     := Column.Title.Color;
       Cols[i].Title.Font.Assign(Column.Title.Font);
       Result := True;
       Exit;
    end;
end;


procedure TDBGridHelper.SalvarConfigGrade;
var FormGradeName : String; SQL : TStrings;
BlobStream : TStream;  TempDS : TDataSource;
begin
try
   if Query <> nil then begin
      Query.Close;
//      FormGradeName := Copy(FormClass,2,length(FormClass))+Grid.Name; //Para manter codigo legado do ADM.
//      Sql := TStringList.Create;
//      Sql.Add(' Select COLUNAS, OPER_ID, FORM_GRADE_NAME, CONF_GRADE_ID ');
//      {$IFDEF SisBig}
//         Sql.Add(' ,lojas_leram ');
//      {$ENDIF}
//      Sql.Add(' from conf_grades where oper_id = '+inttostr(oper_id)+' and FORM_GRADE_NAME = "'+FormGradeName+'"');
//      SetObjectProp(Query,'SQL',Sql);
//      SetPropValue(Query,'RequestLive',True);
//      Query.Open;
//      if not Query.isempty then
//         Query.Edit
//      else begin
//         Query.Append;
//         Query.FieldByName('OPER_ID').AsInteger        := Oper_id;
//         Query.FieldByName('FORM_GRADE_NAME').AsString := FormGradeName;
//      end;
//      {$IFDEF SisBig}
//         Query.FieldByName('lojas_leram').AsString := '';
//      {$ENDIF}
//      BlobStream := Query.CreateBlobStream(Query.Fields[0],bmWrite);
//      //Tratamento por bug de stack overflow
//      TempDS := Grid.DataSource;
//      Grid.DataSource := nil;
//      Grid.Columns.SaveToStream(BlobStream);
//      Grid.DataSource := TempDS;
//      //************************************
//      BlobStream.Free;
//      Query.Post;
//      Query.Close;
      inherited;
   end;
except
end;

end;

procedure TDBGridHelper.ZebrarGrid(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var Grid : TDBGrid;
  L, R: Integer;
  marc: Boolean;
begin
  if not (TDBGrid(Sender).Name = 'GridBranco') then
  begin
    marc:= False;
    Grid := TDBGrid(Sender);
    if ((gdFocused in State) and (dgEditing in Grid.Options)) then
      Grid.Canvas.Font.Color:= clWindow
    else if ((Grid.DataSource.DataSet.FindField('LIBERADO') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADO').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('obrigaSenha') <> nil) and (Grid.DataSource.DataSet.FindField('obrigaSenha').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('LIBERADO') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADO').AsString = 'I') or
      (Grid.DataSource.DataSet.FindField('LIBERADA') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADA').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('ACEITA') <> nil) and (Grid.DataSource.DataSet.FindField('ACEITA').AsString = 'N') or
      //(Grid.DataSource.DataSet.FindField('USA_DESCONTO_ESPECIAL') <> nil) and (Grid.DataSource.DataSet.FindField('USA_DESCONTO_ESPECIAL').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('BAIXADO') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADO').AsString = 'S') or
      (Grid.DataSource.DataSet.FindField('DESCONTAR') <> nil) and (Grid.DataSource.DataSet.FindField('DESCONTAR').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('PARTICIPA') <> nil) and (Grid.DataSource.DataSet.FindField('PARTICIPA').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('BAIXADA') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADA').AsString = 'S')) then
    begin
      Grid.Canvas.Font.Color := clRed;
      marc:= True;
    end
    else
      Grid.Canvas.Font.Color := clBlack;
    with THackDBgrid(Sender) do
    begin
      if DataLink.ActiveRecord = Row -1 then
      begin
        Grid.Canvas.Brush.Color:= clBlack;
        Grid.Canvas.FillRect(Rect);
        if odd(Grid.DataSource.DataSet.RecNo) then
          if ((gdFocused in State) and (dgEditing in Grid.Options)) then
            Canvas.Brush.Color:= clNavy
          else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and ((Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') or (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean)) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite
        else
          if ((gdFocused in State) and (dgEditing in Grid.Options)) then
            Canvas.Brush.Color:= clNavy
          else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and ((Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') or (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean)) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $007676EB;
          end
          else
            Canvas.Brush.Color:= $0EEEEEE;
      end
      else
      begin
        if odd(Grid.DataSource.DataSet.RecNo) then
        begin
          if ((gdFocused in State) and (dgEditing in Grid.Options)) then
            Canvas.Brush.Color:= clNavy
          else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and ((Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') or (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean)) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite;
        end
        else if ((gdFocused in State) and (dgEditing in Grid.Options)) then
          Canvas.Brush.Color:= clNavy
        else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and ((Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') or (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean)) then
        begin
          if marc then Grid.Canvas.Font.Color := clWhite;
          Canvas.Brush.Color:= $007676EB;
        end
        else
          Canvas.Brush.Color:= $0EEEEEE;
        Grid.Canvas.FillRect(Rect);
      end;
    end;
    R:= Rect.Right; L:= Rect.Left;
    if Column.Index = 0 then L := L + 1;
    if Column.Index = Grid.Columns.Count -1 then R := R - 1;
    Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
    Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
  end;
{
begin
Grid.Canvas.Font.Color  := clBlack;
If not odd(Grid.DataSource.DataSet.RecNo) then
   Grid.Canvas.Brush.Color := $00FED3BA
else
   Grid.Canvas.Brush.Color:= clWhite;

if dgRowSelect in Grid.options then begin
   if THackDBgrid(Sender).DataLink.ActiveRecord = THackDBgrid(Sender).Row - 1 then begin
      Grid.Canvas.Brush.Color := clNavy;
      Grid.Canvas.Font.Color  := clWhite;
   end;
end
else begin
  if gdFocused in State then begin
     Grid.Canvas.Brush.Color := clNavy;
     Grid.Canvas.Font.Color  := clWhite;
  end;
end;
Grid.Canvas.FillRect(Rect);
Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
}
if Assigned(GridColumnCellEvent) then
   GridColumnCellEvent(Sender, Rect, DataCol, Column, State);

end;


procedure TDBGridHelper.LocateGrade;
var i : integer; FPesqGrade : TFPesqGrade;
begin
if not Grid.DataSource.DataSet.IsEmpty then begin
   FPesqGrade := TFPesqGrade.Create(Screen.ActiveForm);
   SetLength(FPesqGrade.campos,Grid.Columns.Count);
   for i := 0 to Grid.Columns.Count - 1 do begin
       FPesqGrade.CBCampos.Items.Add(Grid.Columns[i].Title.Caption);
       FPesqGrade.campos[i].index := i;
       FPesqGrade.campos[i].fieldname := Grid.Columns[i].FieldName;
   end;
   FPesqGrade.CBCampos.ItemIndex := 0;
   FPesqGrade.Grade := Grid;
   FPesqGrade.ShowModal;
   FPesqGrade.Free;
   Grid.SetFocus;
end;
end;


class procedure TDBGridHelper.AutoZebrarGrid(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var Grid : TDBGrid;
  L, R: Integer;
begin
{
  if not (TDBGrid(Sender).Name = 'GridBranco') then
  begin
    Grid := TDBGrid(Sender);
    if ((gdFocused in State) and (dgEditing in Grid.Options)) then
    begin
      Grid.Canvas.Font.Style:= [fsBold];
      Grid.Canvas.Font.Color:= clTeal;
    end
    else if ((Grid.DataSource.DataSet.FindField('LIBERADO') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADO').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('LIBERADA') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADA').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('BAIXADO') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADO').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('BAIXADA') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADA').AsString = 'N')) then
      Grid.Canvas.Font.Color := clRed
    else
      Grid.Canvas.Font.Color := clBlack;
    with THackDBgrid(Sender) do
    begin
      if DataLink.ActiveRecord = Row -1 then
      begin
        Grid.Canvas.Brush.Color:= clBlack;
        Grid.Canvas.FillRect(Rect);
        if odd(Grid.DataSource.DataSet.RecNo) then
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
            Canvas.Brush.Color:= $008080FF
          else
            Canvas.Brush.Color:= clWhite
        else
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
            Canvas.Brush.Color:= $007676EB
          else
            Canvas.Brush.Color:= $0EEEEEE;
      end
      else
      begin
        if odd(Grid.DataSource.DataSet.RecNo) then
        begin
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
            Canvas.Brush.Color:= $008080FF
          else
            Canvas.Brush.Color:= clWhite;
        end
        else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
          Canvas.Brush.Color:= $007676EB
        else
          Canvas.Brush.Color:= $0EEEEEE;
        Grid.Canvas.FillRect(Rect);
      end;
    end;
    R:= Rect.Right; L:= Rect.Left;
    if Column.Index = 0 then L := L + 1;
    if Column.Index = Grid.Columns.Count -1 then R := R - 1;
    Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
    Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
  end;
}

{var Grid : TDBGrid;
begin
Grid := TDBGrid(Sender);
Grid.Canvas.Font.Color  := clBlack;
If not odd(Grid.DataSource.DataSet.RecNo) then
   Grid.Canvas.Brush.Color := $00FED3BA
else
   Grid.Canvas.Brush.Color:= clWhite;


if dgRowSelect in Grid.options then begin
   if THackDBgrid(Sender).DataLink.ActiveRecord = THackDBgrid(Sender).Row - 1 then begin
      Grid.Canvas.Brush.Color := clNavy;
      Grid.Canvas.Font.Color  := clWhite;
   end;
end
else begin
  if gdFocused in State then begin
     Grid.Canvas.Brush.Color := clNavy;
     Grid.Canvas.Font.Color  := clWhite;
  end;
end;

if Grid.DataSource.DataSet.FindField('marcado') <> nil then begin
   if Grid.DataSource.DataSet.FindField('marcado').AsString = 'S' then begin
      Grid.Canvas.Brush.Color :=  $008080FF;
   end;
end;
Grid.Canvas.FillRect(Rect);
Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
}
end;

procedure TDBGridHelper.TitleClick(Column: TColumn);
begin
  if Column.Grid.DataSource.DataSet.ClassNameIs('TZQuery') then //AdmConvenios
     //arianeSortZQuery(Grid.DataSource.DataSet,Column.FieldName)
  else if Column.Grid.DataSource.DataSet.ClassNameIs('TClientDataSet') then
     ///arianeSortClientDataSetByFieldName(Column.FieldName, TClientDataSet(Grid.DataSource.DataSet), stAuto)
  else
  begin
  {
  // todo : acertar ordena�ao.
  }
  end;
end;

function TDBGridHelper.FileName2URL(FileName: string): string;
begin
  result:= '';
  if LowerCase(copy(FileName,1,8))<>'file:///' then
    result:= 'file:///';
  result:= result + StringReplace(FileName, '\', '/', [rfReplaceAll, rfIgnoreCase]);
end;

Procedure TDBGridHelper.Grade_to_PlanilhaCalc(Grade:TDBGrid;Nome:String='';MostrarTitulos:Boolean=True;SalvarComo:String='');
var
  OOoServer, OOoDesktop, VariantArr, OOoDocument: variant;
  Column, Sheets, Sheet, Range, Dados, args: variant;
  Linha : array of Variant;
  i, DataNum, ColsCount: integer;
  TargetCell, caminho : String;
  ColsVisible : TList;
  AfterScroll, BeforeScroll : TDataSetNotifyEvent;
  DataSet : TDataSet;
begin
  try
    OOoServer := CreateOleObject('com.sun.star.ServiceManager');
    OOoDesktop := OOoServer.createInstance('com.sun.star.frame.Desktop');
    VariantArr := VarArrayCreate([0, 2], varVariant);
    VariantArr[0] := MakePropertyValue(OOoServer, 'Hidden', true);
    VariantArr[1] := MakePropertyValue(OOoServer, 'Overwrite', True);
    VariantArr[2] := MakePropertyValue(OOoServer, 'OptimalWidth', True);
    OOoDocument := OOoDesktop.LoadComponentFromURL('private:factory/scalc', '_blank', 0, VariantArr);

    Sheets := OOoDocument.Sheets;
    Sheet := Sheets.getByIndex(0);

    if Nome <> '' then
    begin
      args:= VarArrayCreate([0, 0], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'Name',Nome);
      ooDispatch(OOoServer, OOoDocument, '.uno:RenameTable',args);
    end
    else
    begin
      args:= VarArrayCreate([0, 0], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'Name','Exportacao');
      ooDispatch(OOoServer, OOoDocument, '.uno:RenameTable',args);
    end;
  except
    ShowMessage('N�o foi possivel abrir o office!');
    SysUtils.Abort;
  end;
  try
    Screen.Cursor := crHourGlass;
    DataSet := Grade.DataSource.DataSet;
    if DataSet.IsEmpty then
    begin
      MsgInf('N�o h� dados para sem exportados');
      Exit;
    end;
    ColsVisible := TList.Create;
    for i := 0 to Grade.Columns.Count-1 do
    begin
      if Grade.Columns[i].Visible then
      begin
        ColsVisible.Add(Grade.Columns[i]);
      end;
    end;
    ColsCount := ColsVisible.Count-1;
    SetLength(Linha, ColsCount+1);
    Dados := VarArrayCreate([iif(MostrarTitulos,0,1),DataSet.RecordCount], varVariant);
    if MostrarTitulos then
    begin
      for i := 0 to ColsCount do
      begin
        Linha[i] := TColumn(ColsVisible[i]).Title.Caption;
      end;
      Dados[0] := Linha;
    end;
    AfterScroll  := DataSet.AfterScroll;
    BeforeScroll := DataSet.BeforeScroll;
    DataSet.AfterScroll  := nil;
    DataSet.BeforeScroll := nil;
    DataSet.First;
    while not DataSet.Eof do
    begin
      for i :=0 to ColsCount do
      begin
        try
          if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
          begin
            Linha[i] := RoundTo(TColumn(ColsVisible[i]).Field.AsFloat,-2);
          end
          else if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
          begin
            if TColumn(ColsVisible[i]).Field.IsNull then
            begin
              Linha[i]:= '';
            end
            else
            begin
              DataNum:= Trunc(TColumn(ColsVisible[i]).Field.AsDateTime);
              Linha[i] := DataNum;
            end;
          end
          else if not TColumn(ColsVisible[i]).Field.IsNull then
          begin
            Linha[i]:= TColumn(ColsVisible[i]).Field.AsVariant;
          end
          else
          begin
            Linha[i]:= '';
          end;
        except
        end;
      end;
      Dados[DataSet.RecNo] := Linha;
      Grade.DataSource.DataSet.Next;
    end;
    for i :=0 to ColsCount do
    begin
      args:= VarArrayCreate([0, 0], varVariant);
      if MostrarTitulos then
      begin
        TargetCell:= RetornaRange(i,1,i,DataSet.RecordCount+1);
      end
      else
      begin
        TargetCell:= RetornaRange(i,0,i,DataSet.RecordCount);
      end;
      if IsDateTimeField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',36);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end
      else if IsFloatField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',4);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end
      else if IsStringField(TColumn(ColsVisible[i]).Field.DataType) then
      begin
        args[0]:= MakePropertyValue(OOoServer, 'ToPoint',TargetCell);
        ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
        args[0]:= MakePropertyValue(OOoServer, 'NumberFormatValue',100);
        ooDispatch(OOoServer, OOoDocument, '.uno:NumberFormatValue',args);
      end;
    end;
    args[0]:= MakePropertyValue(OOoServer, 'ToPoint','$A$1');
    ooDispatch(OOoServer, OOoDocument, '.uno:GoToCell',args);
    Range := sheet.getCellRangeByPosition(0{esquerda},0{topo},ColsCount{direita},iif(MostrarTitulos,DataSet.RecordCount,DataSet.RecordCount-1){rodape});
    Range.setDataArray(dados);
    Column:= OOoDocument.GetCurrentController.GetActiveSheet.GetColumns;
    Column.OptimalWidth:=True;
    if SalvarComo <> '' then
    begin
      SalvarComo:= PChar(SalvarComo);
      caminho:= FileName2URL(SalvarComo);
      if pos('.xls',caminho)>0 then
         caminho:= copy(caminho,1,pos('.xls',caminho)-1);
      args:= VarArrayCreate([0, 1], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'URL',caminho+'.xls');
      args[1]:= MakePropertyValue(OOoServer, 'FilterName','MS Excel 97');
      ooDispatch(OOoServer, OOoDocument, '.uno:SaveAs',args);
    end
    else
    begin
      caminho:= FileName2URL(SalvarComo);
      args:= VarArrayCreate([0, 0], varVariant);
      args[0]:= MakePropertyValue(OOoServer, 'FilterName','MS Excel 97');
      ooDispatch(OOoServer, OOoDocument, '.uno:SaveAs',args);
    end;
    DataSet.AfterScroll  := AfterScroll;
    DataSet.BeforeScroll := BeforeScroll;
    DataSet.First;
    Screen.Cursor := crDefault;
    OOoDocument.getCurrentController.getFrame.getContainerWindow.setVisible(true);
    OOoDesktop:= Unassigned;
    OOoServer:= Unassigned;
  except on E:Exception
    do
    begin
      DataSet.AfterScroll  := AfterScroll;
      DataSet.BeforeScroll := BeforeScroll;
      DataSet.First;
      Screen.Cursor := crDefault;
      ShowMessage('Ocorreu um problema durande a gera��o do arquivo.'+#13+'Erro: '+E.Message);
    end;
  end;
end;

function TDBGridHelper.RetornaRange(left, top, right, botton: integer):string;
begin
  result:= '$'+ConvertLetra(left)+'$'+IntToStr(top)+':$'+ConvertLetra(right)+'$'+IntToStr(botton);
end;

function TDBGridHelper.ConvertLetra(num:integer):string;
begin
  case num of
    0: Result:= 'A';
    1: Result:= 'B';
    2: Result:= 'C';
    3: Result:= 'D';
    4: Result:= 'E';
    5: Result:= 'F';
    6: Result:= 'G';
    7: Result:= 'H';
    8: Result:= 'I';
    9: Result:= 'J';
    10: Result:= 'K';
    11: Result:= 'L';
    12: Result:= 'M';
    13: Result:= 'N';
    14: Result:= 'O';
    15: Result:= 'P';
    16: Result:= 'Q';
    17: Result:= 'R';
    18: Result:= 'S';
    19: Result:= 'T';
    20: Result:= 'U';
    21: Result:= 'V';
    22: Result:= 'W';
    23: Result:= 'X';
    24: Result:= 'Y';
    25: Result:= 'Z';
    26: Result:= 'AA';
    27: Result:= 'AB';
    28: Result:= 'AC';
    29: Result:= 'AD';
    30: Result:= 'AE';
    31: Result:= 'AF';
    32: Result:= 'AG';
    33: Result:= 'AH';
    34: Result:= 'AI';
    35: Result:= 'AJ';
    36: Result:= 'AK';
    37: Result:= 'AL';
    38: Result:= 'AM';
    39: Result:= 'AN';
    40: Result:= 'AO';
    41: Result:= 'AP';
    42: Result:= 'AQ';
    43: Result:= 'AR';
    44: Result:= 'AS';
    45: Result:= 'AT';
    46: Result:= 'AU';
    47: Result:= 'AV';
    48: Result:= 'AW';
    49: Result:= 'AX';
    50: Result:= 'AY';
    51: Result:= 'AZ';
    52: Result:= 'BA';
    53: Result:= 'BB';
    54: Result:= 'BC';
    55: Result:= 'BD';
    56: Result:= 'BE';
    57: Result:= 'BF';
    58: Result:= 'BG';
    59: Result:= 'BH';
    60: Result:= 'BI';
    61: Result:= 'BJ';
    62: Result:= 'BK';
    63: Result:= 'BL';
    64: Result:= 'BM';
    65: Result:= 'BN';
    66: Result:= 'BO';
    67: Result:= 'BP';
    68: Result:= 'BQ';
    69: Result:= 'BR';
    70: Result:= 'BS';
    71: Result:= 'BT';
    72: Result:= 'BU';
    73: Result:= 'BV';
    74: Result:= 'BW';
    75: Result:= 'BX';
    76: Result:= 'BY';
    77: Result:= 'BZ';
    78: Result:= 'CA';
    79: Result:= 'CB';
    80: Result:= 'CC';
    81: Result:= 'CD';
    82: Result:= 'CE';
    83: Result:= 'CF';
    84: Result:= 'CG';
    85: Result:= 'CH';
    86: Result:= 'CI';
    87: Result:= 'CJ';
    88: Result:= 'CK';
    89: Result:= 'CL';
    90: Result:= 'CM';
    91: Result:= 'CN';
    92: Result:= 'CO';
    93: Result:= 'CP';
    94: Result:= 'CQ';
    95: Result:= 'CR';
    96: Result:= 'CS';
    97: Result:= 'CT';
    98: Result:= 'CU';
    99: Result:= 'CV';
    100: Result:= 'CW';
    101: Result:= 'CX';
    102: Result:= 'CY';
    103: Result:= 'CZ';
  else
    Result:= 'DA';
  end;
end;

function TDBGridHelper.MakePropertyValue(ooServer: Variant; PropName:string; PropValue:variant): variant;
begin
  Result := ooServer.Bridge_GetStruct('com.sun.star.beans.PropertyValue');
  Result.Name := PropName;
  Result.Value := PropValue;
end;

procedure TDBGridHelper.ooDispatch(ooServer, ooDocument: Variant; ooCommand: string; ooParams: variant);
var
  ooDispatcher, ooFrame: variant;
begin
  if (VarIsEmpty(ooParams) or VarIsNull(ooParams)) then
  begin
    ooParams:= VarArrayCreate([0, -1], varVariant);
  end;
  ooFrame:= ooDocument.getCurrentController.getFrame;
  ooDispatcher:= ooServer.createInstance('com.sun.star.frame.DispatchHelper');
  ooDispatcher.executeDispatch(ooFrame, ooCommand, '', 0, ooParams );
end;

{ TFormOnCloseMap }

procedure TFormOnCloseMap.SetForm(Form: TForm);
begin
   _Form                := Form;
   Self.OldFormDestroy  := _Form.OnDestroy;
   _Form.OnDestroy      := GravarConfGrades;
end;

procedure TFormOnCloseMap.GravarConfGrades(Sender: TObject);
var i : integer;
begin
  for i := 0 to _Form.ComponentCount-1 do begin
      if (_Form.Components[i] is TDBGridHelper) then
         TDBGridHelper(_Form.Components[i]).SalvarConfigGrade;
  end;
  if Assigned(Self.OldFormDestroy) then
     Self.OldFormDestroy(_Form);
end;

initialization

finalization


end.
