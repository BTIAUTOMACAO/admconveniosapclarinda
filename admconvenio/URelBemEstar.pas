unit URelBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB;

type
  TFRelBemEstar = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    lbl1: TLabel;
    lbl2: TLabel;
    DataInicio: TJvDateEdit;
    DataFim: TJvDateEdit;
    btn1: TButton;
    QBusca: TADOQuery;
    intgrfldADOQuery1CONV_ID: TIntegerField;
    strngfldADOQuery1TITULAR: TStringField;
    intgrfldADOQuery1EMPRES_ID: TIntegerField;
    strngfldADOQuery1NOME_EMPRESA: TStringField;
    QBuscaDEBITO: TBCDField;
    QBuscaDATA_VENC_FATURA: TDateTimeField;
    procedure Button1Click(Sender: TObject);
    procedure SalvarXLS(Dados : TADOQuery; NomeArq : String);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelBemEstar: TFRelBemEstar;

implementation
uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;

{$R *.dfm}

procedure TFRelBemEstar.Button1Click(Sender: TObject);
begin
  
  QBusca.SQL.Clear;
  QBusca.SQL.Add('SELECT DISTINCT CC.CONV_ID, CONV.TITULAR, e.EMPRES_ID, e.NOME NOME_EMPRESA, DEBITO, DATA_VENC_EMP DATA_VENC_FATURA ');
  QBusca.SQL.Add('FROM CONTACORRENTE CC ');
  QBusca.SQL.Add('INNER JOIN EMPRESAS E ON CC.EMPRES_ID = E.EMPRES_ID ');
  QBusca.SQL.Add('INNER JOIN CONVENIADOS CONV ON CONV.CONV_ID = CC.CONV_ID ');
  QBusca.SQL.Add('WHERE DATA_VENC_EMP BETWEEN '''+ DataInicio.Text+''' and '''+DataFim.Text+''' ');
  QBusca.SQL.Add('AND CC.CRED_ID = 2939');
  QBusca.Open;

  SalvarXLS(QBusca, 'BemEstar_'+ StringReplace(FormatDateTime('dd/mm/yyyy',DataFim.Date),'/','',[rfReplaceAll]));

  ShowMessage('Efetuado Exporta��o com sucesso');
  SysUtils.Abort;
end;

procedure TFRelBemEstar.SalvarXLS(Dados : TADOQuery; NomeArq : String);
var excel: variant;
    i , coluna, linha: integer;
    valor: variant;
begin
   //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        excel.Workbooks.add(1);
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      try
        Screen.Cursor := crHourGlass;
        coluna := 0;
        linha  := 1;
        for i:=0 to Dados.FieldCount - 1  do begin
          inc(coluna);
          valor := Dados.Fields[i].DisplayLabel;
          excel.cells[1,coluna] := valor;
          excel.cells[linha,coluna].Font.bold := True;
        end;
        inc(linha);

        Dados.First;
        While not Dados.Eof do begin
          Coluna := 0;
          for i:= 0 to Dados.FieldCount-1 do begin
            inc(coluna);
            if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
              valor := Dados.Fields[i].AsFloat;
              if (Dados.Fields[i] is TBCDField) then
                excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)'
              else
              excel.cells[linha,coluna].NumberFormat := '#';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end else begin
              valor := Dados.Fields[i].AsString;
              excel.cells[linha,coluna].NumberFormat := '@';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
            end;
            excel.cells[linha,coluna].Value := valor;
          end;
          inc(linha);
          Dados.Next;
        end;
        excel.columns.AutoFit;
        excel.ActiveWorkBook.SaveAs(FileName:='C:\'+NomeArq,Password := '');
        excel.Visible := true;
        Screen.Cursor := crDefault;

    except on e:Exception do
        msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
        e.message);
      end;

end;

procedure TFRelBemEstar.btn1Click(Sender: TObject);
begin
  Self.Close;
end;

end.
