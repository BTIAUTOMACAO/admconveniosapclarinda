unit UFRealCartaCircularizacaoSpani;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB,
  ComCtrls;

type
  TFRelCartaCircularizacaoSpani = class(TForm)
    QBusca: TADOQuery;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    JvDateEdit1: TJvDateEdit;
    JvDateEdit3: TJvDateEdit;
    Button1: TButton;
    edtCaminho: TJvDirectoryEdit;
    ComboBox1: TComboBox;
    ProgressBar1: TProgressBar;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure SalvarXLS(Dados : TADOQuery; NomeArq : String; Relatorio : String);
    function ConverteMes(mes:String): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelCartaCircularizacaoSpani: TFRelCartaCircularizacaoSpani ;

implementation
  uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;
{$R *.dfm}

procedure TFRelCartaCircularizacaoSpani.Button1Click(Sender: TObject);
var anotitulo, mestitulo, data1, data2 : String;
begin

  if not DirectoryExists(ExtractFilePath(edtCaminho.Text)) then begin
    MsgErro('Caminho n�o encontrado!');
    edtCaminho.SetFocus;
    Abort;
  end
  else begin
    if (ComboBox1.Text <> 'Saldo Devedor') and (ComboBox1.Text <> 'Vendas') then
    begin
      MsgErro('Escolha um tipo de relat�rio v�lido!');
      Abort;
    end
    else begin

      if not IsDate(JvDateEdit1.Text) then
      begin
        MsgErro('Digite a data inicial!');
        JvDateEdit1.SetFocus;
        Abort;
      end;

      if not IsDate(JvDateEdit3.Text) then
      begin
        MsgErro('Digite a data final!');
        JvDateEdit3.SetFocus;
        Abort;
      end;

      if JvDateEdit1.Text > JvDateEdit3.Text then
      begin
        MsgErro('Data inicial maior que a data final!');
        JvDateEdit1.SetFocus;
        Abort;
      end;
    end;
  end;
  ProgressBar1.Position := 0;

  if ComboBox1.Text = 'Saldo Devedor' then
  begin
    QBusca.SQL.Clear;
    QBusca.SQL.Add('SELECT CRED.NOME,CRED.CGC,(SUM(DEBITO-CREDITO)-(SUM(DEBITO-CREDITO)*(CRED.COMISSAO/100))) SALDO_DEVEDOR');
    QBusca.SQL.Add('FROM CONTACORRENTE CC');
    QBusca.SQL.Add('INNER JOIN CREDENCIADOS CRED ON CRED.CRED_ID = CC.CRED_ID');
    QBusca.SQL.Add('WHERE CANCELADA = ''N'' AND DATA BETWEEN '''+JvDateEdit1.Text+''' AND '''+JvDateEdit3.Text+'''');
    QBusca.SQL.Add('AND CC.CRED_ID IN(select cred_id from CREDENCIADOS where NOME like ''%zaragoza%'')');
    QBusca.SQL.Add('GROUP BY CC.CRED_ID,CRED.COMISSAO,CRED.NOME,CRED.CGC');
    QBusca.Open;
    anotitulo := StringReplace(FormatDateTime('yyyy',JvDateEdit3.Date),'/','',[rfReplaceAll]);
    mestitulo := StringReplace(FormatDateTime('mm',JvDateEdit3.Date),'/','',[rfReplaceAll]);
    mestitulo := ConverteMes(mestitulo);
    SalvarXLS(QBusca, 'SALDO DEVEDOR SPANI - ' + UpperCase(mestitulo) + anotitulo, ComboBox1.Text);
  end
  else begin
    QBusca.SQL.Clear;
    QBusca.SQL.Add('SELECT CC.CRED_ID,CRED.NOME,CRED.CGC,DEBITO,NSU,CC.OPERADOR AS ');
    QBusca.SQL.Add('MEIO_DE_CAPTURA,concat(CONVERT(VARCHAR,DATA,103),'' '',HORA) DATA_HORA,CONVERT(VARCHAR,DATA,103) ');
    QBusca.SQL.Add('DATA,DATA');
    QBusca.SQL.Add('FROM CONTACORRENTE CC');
    QBusca.SQL.Add('INNER JOIN CREDENCIADOS CRED ON CRED.CRED_ID = CC.CRED_ID');
    QBusca.SQL.Add('WHERE CANCELADA = ''N'' AND DATA BETWEEN '''+JvDateEdit1.Text+''' AND '''+JvDateEdit3.Text+'''');
    QBusca.SQL.Add('AND CC.CRED_ID IN(select cred_id from CREDENCIADOS where NOME like ''%zaragoza%'')');
    QBusca.SQL.Add('ORDER BY CRED_ID, CC.DATA ASC');
    QBusca.Open;
    data1 := StringReplace(JvDateEdit1.Text,'/','-',[rfReplaceAll]);
    data2 := StringReplace(JvDateEdit3.Text,'/','-',[rfReplaceAll]);
    SalvarXLS(QBusca, 'RELAT�RIO DE VENDAS PLANT�O CARD ' + data1 + ' A ' + data2, ComboBox1.Text);
  end;

  MsgInf('Efetuado Exporta��o com sucesso');
  Screen.Cursor := crDefault;
  SysUtils.Abort;
end;

procedure TFRelCartaCircularizacaoSpani.SalvarXLS(Dados : TADOQuery; NomeArq : String; Relatorio : String);
var excel: variant;
    i , coluna, linha, cont: integer;
    valor: variant;
    mes, ano, dia: string;
begin
    ProgressBar1.Min := 0;
    ProgressBar1.Max := Dados.RecordCount;

    mes := StringReplace(FormatDateTime('mm',Date()),'/','',[rfReplaceAll]);

    mes := ConverteMes(mes);

    ano := StringReplace(FormatDateTime('yyyy',Date()),'/','',[rfReplaceAll]);
    dia := StringReplace(FormatDateTime('dd',Date()),'/','',[rfReplaceAll]);

   //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        excel.Workbooks.add(1);
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      if Relatorio = 'Saldo Devedor' then
      begin
        try
          Screen.Cursor := crHourGlass;



          coluna := 0;
          linha  := 13;

          excel.cells[1,1] := 'RPC REDE PLANTAO DE CONVENIOS EIRLI';
          excel.cells[2,1] := 'Rua Maraba, 100 - Pq. Industrial';
          excel.cells[3,1] := 'S�o Jos� dos Campos-SP';
          excel.cells[4,1] := '12235-780';
          excel.cells[6,1] := 'S�o Jose dos Campos, ' + dia + ' de ' + mes + ' de ' + ano;
          excel.Workbooks[1].WorkSheets[1].Range['A8:C8'].Merge; // Mescla c�lula

          excel.cells[8,1].Font.bold := True;
          excel.cells[8,1] := 'CIRCULARIZA��O DE CLIENTES';
          excel.cells[8,1].Font.size := 18;
          excel.cells[8,1].HorizontalAlignment := 3;

          excel.Workbooks[1].WorkSheets[1].Range['A10:C10'].Merge; // Mescla c�lula
          excel.cells[10,1] := 'Conforme solicita��o enviamos o saldo devedor dos estabelecimentos abaixo em ' + DateToStr(JvDateEdit3.Date);

          excel.Workbooks[1].WorkSheets[1].Range['A12:C12'].Merge; // Mescla c�lula
          excel.cells[12,1].Font.bold := True;
          excel.cells[12,1] := 'VENDAS ' + DateToStr(JvDateEdit1.Date) + ' a ' + DateToStr(JvDateEdit3.Date);
          excel.cells[12,1].Font.size := 18;
          excel.cells[12,1].HorizontalAlignment := 3;

          for i:=0 to Dados.FieldCount - 1  do begin
            inc(coluna);
            valor := Dados.Fields[i].DisplayLabel;
            excel.cells[13,coluna] := valor;
            excel.cells[linha,coluna].Font.bold := True;
          end;
          inc(linha);

          Dados.First;
          While not Dados.Eof do begin
            Coluna := 0;
            for i:= 0 to Dados.FieldCount-1 do begin
              inc(coluna);

              if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
                valor := Dados.Fields[i].AsFloat;

                if (Dados.Fields[i] is TBCDField) then begin
                  excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)';
                  excel.cells[linha,coluna].Style := 'Currency'
                end else begin
                excel.cells[linha,coluna].NumberFormat := '#';
                excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
                end;
              end else begin
                valor := Dados.Fields[i].AsString;
                excel.cells[linha,coluna].NumberFormat := '@';
                excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
              end;
              excel.cells[linha,coluna].Value := valor;
            end;
            inc(cont);
            inc(linha);
            Dados.Next;
            ProgressBar1.position := ProgressBar1.position + 1;
          end;

          excel.cells[linha,1].Font.bold := True;
          excel.cells[linha,1] := 'TOTAL';

          excel.cells[linha,3].Font.bold := True;
          Excel.WorkBooks[1].Sheets[1].Cells[linha,3].Formula:='=sum(C' + IntToStr(linha-cont)+':C' + IntToStr(linha-1) + ')';

          excel.cells[linha+4,1] := 'RPC REDE PLANTAO DE CONVENIOS EIRLI';
          excel.cells[linha+5,1] := 'CNPJ 07.277.393/0001-24';
          excel.cells[linha+6,1] := 'CELEANDRO RODOLFO DE FARIA';

          ProgressBar1.Position := 0;

          excel.columns.AutoFit;
          excel.ActiveWorkBook.SaveAs(FileName:=edtCaminho.Text + '\' + NomeArq,Password := '');
          excel.Visible := true;
          Screen.Cursor := crDefault;

      except on e:Exception do
          msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
          e.message);
        end;
   end
   else begin
      try
          Screen.Cursor := crHourGlass;

          coluna := 0;
          linha  := 1;

          for i:=0 to Dados.FieldCount - 1  do begin
            inc(coluna);
            valor := Dados.Fields[i].DisplayLabel;
            excel.cells[linha,coluna] := valor;
            excel.cells[linha,coluna].Font.bold := True;
          end;
          inc(linha);

          Dados.First;
          While not Dados.Eof do begin
            Coluna := 0;
            for i:= 0 to Dados.FieldCount-1 do begin
              inc(coluna);

              if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
                valor := Dados.Fields[i].AsFloat;

                if (Dados.Fields[i] is TBCDField) then begin
                  excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)';
                  excel.cells[linha,coluna].Style := 'Currency'
                end else begin
                excel.cells[linha,coluna].NumberFormat := '#';
                excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
                end;
              end else begin
                valor := Dados.Fields[i].AsString;
                excel.cells[linha,coluna].NumberFormat := '@';
                excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
              end;
              excel.cells[linha,coluna].Value := valor;
            end;
            inc(cont);
            inc(linha);
            Dados.Next;
            ProgressBar1.position := ProgressBar1.position + 1;
          end;
          ProgressBar1.Position := 0;
          excel.columns.AutoFit;
          excel.ActiveWorkBook.SaveAs(FileName:=edtCaminho.Text + '\' + NomeArq,Password := '');
          excel.Visible := true;
          Screen.Cursor := crDefault;

      except on e:Exception do
          msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
          e.message);
        end;
      end;
end;

Function TFRelCartaCircularizacaoSpani.ConverteMes(mes:String): String;
begin
  if mes = '01' then
      mes := 'janeiro';
    if mes = '02' then
      mes := 'fevereiro';
    if mes = '03' then
      mes := 'mar�o';
    if mes = '04' then
      mes := 'abril';
    if mes = '05' then
      mes := 'maio';
    if mes = '06' then
      mes := 'junho';
    if mes = '07' then
      mes := 'julho';
    if mes = '08' then
      mes := 'agosto';
    if mes = '09' then
      mes := 'setembro';
    if mes = '10' then
      mes := 'outubro';
    if mes = '11' then
      mes := 'novembro';
    if mes = '12' then
      mes := 'dezembro';

    Result := mes; //Retorno da fun��o
end;

end.
