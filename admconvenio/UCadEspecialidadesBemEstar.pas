unit UCadEspecialidadesBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ADODB, Menus, StdCtrls, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls,
  DBCtrls, JvExControls, JvDBLookup;

type
  TFCadEspecialidadesBemEstar = class(TFCad)
    QCadastroESPECIALIDADE_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit2: TDBEdit;
    QCadastroAPAGADO: TStringField;
    lblServico: TLabel;
    dsServico: TDataSource;
    jvdbSERV_ID: TJvDBLookupCombo;
    QCadastroSERV_ID: TIntegerField;
    QServico: TADOQuery;
    QServicoDESCRICAO: TStringField;
    QServicoSERV_ID: TIntegerField;
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure BuscaEspecialidade;
    procedure LimpaEdits;
    procedure QCadastroAfterRefresh(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButIncluiClick(Sender: TObject);
    procedure valida;
    procedure ButGravaClick(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FCadEspecialidadesBemEstar: TFCadEspecialidadesBemEstar;

implementation

uses DM, cartao_util, UMenu;

{$R *.dfm}

procedure TFCadEspecialidadesBemEstar.QCadastroAfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  {Recuperando o �ltimo ID do conveniado}
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SESPEC_ID_BEM_ESTAR');
  DMConexao.AdoQry.Open;
  QCadastroESPECIALIDADE_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  {Fim do bloco para recupera��o do ID}
  QCadastroLIBERADO.AsString          := 'S';

end;

procedure TFCadEspecialidadesBemEstar.FormCreate(Sender: TObject);
begin
  chavepri := 'ESPECIALIDADE_ID';
  detalhe := 'Especialidade ID: ';
  QCadastro.Open;
  QServico.Open;
  FMenu.vFCadEspecialidadesBemEstar := True;

  inherited;
end;

procedure TFCadEspecialidadesBemEstar.ButBuscaClick(Sender: TObject);
begin
  inherited;
  BuscaEspecialidade;
end;

//Paulinho Code, fazendo com que os campos sejam obrigat�rios.
procedure TFCadEspecialidadesBemEstar.Valida;
begin
  if QCadastroSERV_ID.IsNull then
  begin
    MsgInf('Selecione um servi�o');
    PageControl1.ActivePageIndex := 1; //0 � a aba "Em Grade" e um � a "Em Ficha"
    jvdbSERV_ID.SetFocus;
    Abort;
  end;
  if Trim(QCadastroDESCRICAO.AsString) = '' then
  begin
    MsgInf('Informe uma Descri��o');
    PageControl1.ActivePageIndex := 1;
    DBEdit2.SetFocus;
    Abort;
  end;
end;

procedure TFCadEspecialidadesBemEstar.BuscaEspecialidade;
begin
    QCadastro.Close;
    Screen.Cursor := crHourGlass;

    QCadastro.SQL.Clear;
    QCadastro.SQL.Add('select * from especialidades WHERE 1=1 ');
    if Trim(EdCod.Text) <> '' then begin
      QCadastro.Sql.Add(' and especialidade_id = '+EdCod.Text);
    end;
    if Trim(EdNome.Text) <> '' then begin
      QCadastro.Sql.Add(' and descricao like ''%'+EdNome.Text+'%''');
    end;
    Barra.Panels[0].Text := 'buscando conveniados';
    QCadastro.Open;
    Barra.Panels[0].Text := 'busca conclu�da conveniados';

    if not QCadastro.IsEmpty then
    begin
      DBGrid1.SetFocus;
      Self.TextStatus := '  Especialidade: ['+QCadastroESPECIALIDADE_ID.AsString+'] - '+QCadastroDESCRICAO.AsString
    end
    else
      EdCod.SetFocus;
    LimpaEdits;
    Screen.Cursor := crDefault;
end;

procedure TFCadEspecialidadesBemEstar.LimpaEdits;
begin
  EdCod.Clear;
  EdNome.Clear;
end;

procedure TFCadEspecialidadesBemEstar.QCadastroAfterRefresh(
  DataSet: TDataSet);
begin
  inherited;
   Self.TextStatus := '  Especialidade: ['+QCadastroESPECIALIDADE_ID.AsString+'] - '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadEspecialidadesBemEstar.QCadastroBeforePost(
  DataSet: TDataSet);
begin
  Valida;
  if QCadastro.State = dsInsert then begin
    QCadastroDTCADASTRO.Value   := Now;
    QCadastroOPERCADASTRO.Value := Operador.Nome;
  end;
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;

end;

procedure TFCadEspecialidadesBemEstar.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  if (Key = VK_F5) and (PageControl1.ActivePageIndex = 2) then begin ButInclui_Cartao.Click; Exit; end;
//  if (Key = VK_F6) and (PageControl1.ActivePageIndex = 2) then begin ButApaga_Cartao.Click;  Exit; end;
//  inherited;
//  if Key = VK_F8 then PageControl1.ActivePage := TabCartoes;
//  if Key = VK_F9 then PageControl1.ActivePage := TabFotos;
//  if Key = VK_F11 then PageControl1.ActivePage := TabSituacao;
//  if Key = VK_F12 then PageControl1.ActivePage := TabContaCorrente;
end;
procedure TFCadEspecialidadesBemEstar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFCadEspecialidadesBemEstar := False;
  //FormDestroy(FCadEspecialidadesBemEstar);
end;

procedure TFCadEspecialidadesBemEstar.ButIncluiClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;


procedure TFCadEspecialidadesBemEstar.ButGravaClick(Sender: TObject);
begin
  //Valida;
  inherited;

end;

end.
