inherited FrmLancamentoExtraAlimentacao: TFrmLancamentoExtraAlimentacao
  Left = 43
  Top = 119
  Caption = 'Lan'#231'amento de Complemento Alimenta'#231#227'o'
  ClientHeight = 575
  ClientWidth = 1276
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1276
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 23
    Width = 1276
    Height = 66
    Align = alTop
    Caption = 'Filtro de Dados'
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 19
      Width = 46
      Height = 13
      Caption = 'Empresas'
    end
    object Label60: TLabel
      Left = 416
      Top = 19
      Width = 82
      Height = 13
      Caption = 'Data Renova'#231#227'o'
    end
    object dbLkpEmpresas: TDBLookupComboBox
      Left = 8
      Top = 38
      Width = 385
      Height = 21
      KeyField = 'EMPRES_ID'
      ListField = 'NOME'
      ListFieldIndex = 1
      ListSource = dsEmpresas
      TabOrder = 0
      OnExit = dbLkpEmpresasExit
    end
    object dbDataRenovacao: TJvDateEdit
      Left = 416
      Top = 39
      Width = 107
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 1
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 169
    Width = 1276
    Height = 357
    Align = alClient
    TabOrder = 4
  end
  object pnlDadosRenovacao: TPanel [3]
    Left = 0
    Top = 89
    Width = 1276
    Height = 80
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object rgTipo: TRadioGroup
      Left = 10
      Top = 10
      Width = 187
      Height = 49
      Caption = 'Tipo'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Renova'#231#227'o'
        'Complemento')
      TabOrder = 0
      OnClick = rgTipoClick
    end
    object Button2: TButton
      Left = 1069
      Top = 15
      Width = 94
      Height = 43
      Caption = 'Confirmar'
      TabOrder = 4
      OnClick = Button2Click
    end
    object btnExcluir: TButton
      Left = 1173
      Top = 15
      Width = 108
      Height = 43
      Caption = 'Excluir Lan'#231'amento'
      TabOrder = 3
      OnClick = btnExcluirClick
    end
    object rgDetalheSAP: TRadioGroup
      Left = 218
      Top = 10
      Width = 551
      Height = 49
      Caption = 'Detalhe do Evento'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Recarga Financeiro Antecipado(Boleto Manual SAP)   '
        'Recarga Financeiro Programado no SAP')
      TabOrder = 1
    end
    object rbTipoComplemento: TRadioGroup
      Left = 786
      Top = 10
      Width = 255
      Height = 49
      Caption = 'Tipo do Complemento'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Pr'#243'xima Recarga'
        'Fatura Atual')
      TabOrder = 2
      Visible = False
    end
  end
  object dbGridAlim: TJvDBGrid [4]
    Left = 0
    Top = 169
    Width = 1276
    Height = 357
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsCredAlim
    DefaultDrawing = False
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    AutoAppend = False
    TitleButtons = True
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'conv_id'
        Title.Caption = 'Conv. ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Title.Caption = 'Titular'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'limite_mes'
        Title.Caption = 'Limite M'#234's'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'abono_mes'
        Title.Caption = 'Abono M'#234's'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'saldo_renovacao'
        Title.Caption = 'Saldo Renova'#231#227'o'
        Width = 100
        Visible = True
      end>
  end
  object pnlDados: TPanel [5]
    Left = 0
    Top = 526
    Width = 1276
    Height = 49
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    Visible = False
    DesignSize = (
      1276
      49)
    object Bevel1: TBevel
      Left = 186
      Top = 6
      Width = 2
      Height = 37
    end
    object Label57: TLabel
      Left = 292
      Top = 6
      Width = 86
      Height = 13
      Caption = 'Saldo Renova'#231#227'o'
    end
    object Label56: TLabel
      Left = 204
      Top = 6
      Width = 54
      Height = 13
      Caption = 'Abono M'#234's'
    end
    object Bevel3: TBevel
      Left = 512
      Top = 6
      Width = 2
      Height = 37
    end
    object Label85: TLabel
      Left = 688
      Top = 24
      Width = 64
      Height = 13
      Caption = 'Total Abono: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label88: TLabel
      Left = 840
      Top = 24
      Width = 119
      Height = 13
      Caption = 'Total Saldo Renova'#231#227'o: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 1272
      Top = 10
      Width = 57
      Height = 29
      Hint = 'Ajuda'
      Caption = 'Ajuda'
      Flat = True
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
        5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
        DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
        CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
        E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
        A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
        2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
        D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
        3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
        F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
        3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
        FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
        3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
        FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
        3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
        FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
        5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
        FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
        FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
        FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
        FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
        E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
        BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
        C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
        7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
        D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
        E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
        FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
        FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
        D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = SpeedButton1Click
    end
    object btnGravarConvAlim: TBitBtn
      Left = 6
      Top = 6
      Width = 80
      Height = 34
      Caption = '&Gravar'
      TabOrder = 0
      OnClick = btnGravarConvAlimClick
      Glyph.Data = {
        A6030000424DA603000000000000A60100002800000020000000100000000100
        08000000000000020000232E0000232E00005C00000000000000343434003535
        3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
        49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
        63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
        800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
        A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
        B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
        BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
        C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
        D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
        CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
        E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
        F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
        5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
        3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
        3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
        2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
        284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
        234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
        54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
        3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
        323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
        5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
        57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
        58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
        5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
        5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
        53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
        5B5B5B5B5B5B5B5B5B5B}
      NumGlyphs = 2
    end
    object btnCancelarConvAlim: TBitBtn
      Left = 90
      Top = 6
      Width = 80
      Height = 34
      Caption = '&Cancelar'
      TabOrder = 1
      Glyph.Data = {
        0E040000424D0E040000000000000E0200002800000020000000100000000100
        08000000000000020000232E0000232E000076000000000000000021CC001031
        DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
        DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
        FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
        F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
        F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
        FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
        E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
        ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
        FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
        C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
        CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
        D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
        E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
        E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
        F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
        75757575757575757575757575757575757575757575622F080000082F627575
        757575757575705E493434495E70757575757575753802030E11110E03023875
        7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
        7575757567354D5354555554534D35677575756307102A00337575442C151007
        63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
        367575604B545568345E7575745B544B607575171912301C3700317575401219
        1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
        057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
        0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
        217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
        3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
        65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
        757575756C566058483434485860566C75757575754324283237373228244375
        75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
        757575757575736A5D55555D6A73757575757575757575757575757575757575
        757575757575757575757575757575757575}
      NumGlyphs = 2
    end
    object edtSaldoRenovacao: TEdit
      Left = 292
      Top = 21
      Width = 93
      Height = 21
      TabOrder = 6
      Text = '0,00'
    end
    object edtAbonoMes: TEdit
      Left = 204
      Top = 21
      Width = 64
      Height = 21
      TabOrder = 5
      Text = '0,00'
    end
    object Button1: TButton
      Left = 398
      Top = 8
      Width = 94
      Height = 34
      Caption = 'Alterar p/ Todos'
      TabOrder = 4
      OnClick = Button1Click
    end
    object btnImportar: TBitBtn
      Left = 523
      Top = 7
      Width = 158
      Height = 34
      Hint = 'Importar'
      Anchors = [akLeft]
      Caption = 'Importar Planilha'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnImportarClick
      NumGlyphs = 2
    end
    object lblTotalRenovacao: TJvValidateEdit
      Left = 953
      Top = 24
      Width = 93
      Height = 18
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = True
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
    end
    object lblTotalAbono: TJvValidateEdit
      Left = 760
      Top = 24
      Width = 75
      Height = 18
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = True
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
    end
    object BitBtn1: TBitBtn
      Left = 1059
      Top = 7
      Width = 158
      Height = 34
      Hint = 'Importar'
      Anchors = [akLeft]
      Caption = 'Efetivar Recarga'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BitBtn1Click
      NumGlyphs = 2
    end
    object RichEdit1: TRichEdit
      Left = 1232
      Top = 16
      Width = 34
      Height = 25
      Lines.Strings = (
        'Importa'#231#227'o de Conveniados para cr'#233'dito alimenta'#231#227'o'
        ''
        '   Para realizar a importa'#231#227'o de um arquivo dos conveniados, '
        
          #233' necess'#225'rio que o arquivo seja do tipo (.xls) Excel 97-2003 ou ' +
          '(.xlxs) e'
        'esteja no formato esperado pelo sistema.'
        ''
        
          'Favor usar a Planilha "Importa'#231#227'o_Conveniados_Credito_Alimentaca' +
          'o.xlsx" na pasta ADMConv'#234'nio_Manuais para importar os conveniado' +
          's.'
        ''
        
          '   Obs.: Caso tenha algum erro na importa'#231#227'o ser'#225' informado na t' +
          'ela'
        ''
        '   Segue abaixo o formato para o arquivo.'
        ''
        
          '   Campo CESTA '#233' obrigat'#243'rio para as empresas 1550, 1552, 1553, ' +
          ' 1696.'
        ''
        '   Aten'#231#227'o: O nome da planilha deve ser "credito" (sem aspas)'
        ''
        
          '   Exemplo do formato da planilha, onde todos campos para atuali' +
          'za'#231#227'o'
        'foram selecionados:'
        ''
        
          'EMPRES_ID'#9'-'#9'CONV_ID'#9'  -'#9'TITULAR'#9'                -'#9'CESTA'#9'-'#9'VALOR_' +
          'RENOVA'#199#195'O'
        '2169'#9'                -'#9'1111'#9'  -'#9'JO'#195'O MARIA'#9'-'#9'100.00'#9'-'#9'200.00'
        '2169'#9'                -'#9'2222'#9'  -'#9'MARIA JOS'#201#9'-'#9'100.00'#9'-'#9'200.00'
        ''
        ''
        ''
        '_______\credito/_________'
        ''
        ''
        ''
        
          'Para maiores informa'#231#245'es acesse a pasta "ADMConv'#234'nio - Manuais" ' +
          'dentro do "pool" '
        
          'atrav'#233's do caminho \\servidor\pool\ADMConv'#234'nio_Manuais e abra o ' +
          'arquivo Importa'#231#227'o_Conveniados_Credito_Alimentacao.pdf '
        ' '
        ' ')
      TabOrder = 9
      Visible = False
      WordWrap = False
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 1052
    Top = 40
  end
  object dsEmpresas: TDataSource
    DataSet = qEmpresas
    Left = 992
    Top = 40
  end
  object qEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select emp.empres_id, emp.tipo_credito, CONCAT(emp.empres_id,'#39' -' +
        ' '#39',emp.nome) nome, emp.liberada, convert(varchar,'
      
        '(select top 1 DATA_FECHA from DIA_FECHA where EMPRES_ID=emp.EMPR' +
        'ES_ID and month(data_fecha) >= MONTH(GETDATE ()) and year(DATA_F' +
        'ECHA) = year(GETDATE ())),103) fechamento1 '
      'from empresas emp'
      'where emp.tipo_credito = 2 or emp.TIPO_CREDITO = 3'
      'and emp.liberada = '#39'S'#39' AND emp.APAGADO = '#39'N'#39
      ''
      ''
      ''
      ''
      '')
    Left = 1024
    Top = 40
    object qEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qEmpresasnome: TStringField
      FieldName = 'nome'
      ReadOnly = True
      Size = 75
    end
    object qEmpresasliberada: TStringField
      FieldName = 'liberada'
      FixedChar = True
      Size = 1
    end
    object qEmpresasfechamento1: TStringField
      FieldName = 'fechamento1'
      ReadOnly = True
      Size = 30
    end
    object qEmpresastipo_credito: TIntegerField
      FieldName = 'tipo_credito'
    end
  end
  object QCredAlim: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id_1'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'empres_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select'
      '    c.conv_id,'
      '    c.empres_id, '
      '    c.titular, '
      '    coalesce(c.limite_mes,0.00) limite_mes, '
      '    coalesce(alc.abono_valor,0.00) abono_mes, '
      '    coalesce(alc.renovacao_valor, 0.00) saldo_renovacao, '
      #9'alr.RENOVACAO_ID,'
      #9'alr.DATA_RENOVACAO,'
      #9'alr.TIPO_CREDITO'
      'from'
      '    conveniados c'
      
        #9'left join ALIMENTACAO_RENOVACAO alr on alr.EMPRES_ID = c.EMPRES' +
        '_ID and alr.DATA_RENOVACAO = (SELECT TOP 1 DATA_RENOVACAO FROM A' +
        'LIMENTACAO_RENOVACAO where EMPRES_ID = :empres_id_1 ORDER BY DAT' +
        'A_RENOVACAO ASC)'
      
        #9'left join ALIMENTACAO_RENOVACAO_CREDITOS alc on alc.RENOVACAO_I' +
        'D = alr.RENOVACAO_ID and alc.CONV_ID = c.CONV_ID'
      'where c.empres_id = :empres_id '
      'AND C.LIBERADO = '#39'S'#39' AND C.APAGADO = '#39'N'#39
      'order by c.titular')
    Left = 1080
    Top = 40
    object QCredAlimconv_id: TIntegerField
      FieldName = 'conv_id'
      ReadOnly = True
    end
    object QCredAlimempres_id: TIntegerField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object QCredAlimtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QCredAlimabono_mes: TBCDField
      FieldName = 'abono_mes'
      Precision = 18
      Size = 2
    end
    object QCredAlimsaldo_renovacao: TBCDField
      FieldName = 'saldo_renovacao'
      Precision = 18
      Size = 2
    end
    object QCredAlimlimite_mes: TBCDField
      FieldName = 'limite_mes'
      ReadOnly = True
      Precision = 15
      Size = 2
    end
    object QCredAlimRENOVACAO_ID: TIntegerField
      FieldName = 'RENOVACAO_ID'
    end
    object QCredAlimDATA_RENOVACAO: TWideStringField
      FieldName = 'DATA_RENOVACAO'
      Size = 10
    end
    object QCredAlimTIPO_CREDITO: TStringField
      FieldName = 'TIPO_CREDITO'
      FixedChar = True
      Size = 1
    end
  end
  object dsCredAlim: TDataSource
    DataSet = QCredAlim
    Left = 1113
    Top = 41
  end
  object qUpdate: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'abono_mes'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'saldo_renovacao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'update conveniados set abono_mes = :abono_mes, saldo_renovacao =' +
        ' :saldo_renovacao')
    Left = 1144
    Top = 40
  end
  object tExcel: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 1176
    Top = 40
  end
end
