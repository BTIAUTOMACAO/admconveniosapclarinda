unit UCadConv;

interface

uses
  Windows, Messages, SysUtils,Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,} uClassLog,
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls, Mask,
  JvDateTimePicker, JvDBDateTimePicker, JvToolEdit, Menus,
  ToolEdit, RXDBCtrl, {scap32_rt,}ExtDlgs, jpeg, CurrEdit, {JvDBComb,}
  XMLDoc, IdHTTP, JvExStdCtrls, JvCombobox, JvDBCombobox,
  JvExControls, JvDBLookup, JvExMask, JvExDBGrids, JvDBGrid, ADODB,
  JvValidateEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit, ClipBrd, JvDBControls, JvMemoryDataset;

type
  THackDBgrid = class(TDBGrid);
  TFCadConv = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox1: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    dbEdit2: TDBEdit;
    Label5: TLabel;
    dbEdtChapa: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit17: TDBEdit;
    Label20: TLabel;
    DBEdit18: TDBEdit;
    DBComboBox1: TDBComboBox;
    Label21: TLabel;
    DSBancos: TDataSource;
    DSEmpresa: TDataSource;
    Label22: TLabel;
    DBEdit16: TDBEdit;
    DBEdit19: TDBEdit;
    Label24: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdCodEmp: TEdit;
    EdNomeEmp: TEdit;
    Label32: TLabel;
    Label33: TLabel;
    EdCartao: TEdit;
    TabCartoes: TTabSheet;
    TabContaCorrente: TTabSheet;
    DSCartoes: TDataSource;
    DSSaldoConv: TDataSource;
    DSContaCorrente: TDataSource;
    DBBanco: TJvDBLookupCombo;
    DSTodasCompras: TDataSource;
    Bevel4: TBevel;
    Label41: TLabel;
    dbEdtEmail: TDBEdit;
    DSCredenciado: TDataSource;
    TabSheet3: TTabSheet;
    DSConvDetail: TDataSource;
    DBDateEdit3: TDBDateEdit;
    Label56: TLabel;
    EdChapa: TEdit;
    Label58: TLabel;
    DBEdit4: TDBEdit;
    PageCartoes: TPageControl;
    TabGradeCartoes: TTabSheet;
    TabHistCartoes: TTabSheet;
    DSHistCartoes: TDataSource;
    TabSaldos: TTabSheet;
    Panel11: TPanel;
    DSSaldoCartao: TDataSource;
    DSQGrupo_conv_emp: TDataSource;
    DBGrupo_conv_emp: TJvDBLookupCombo;
    Label64: TLabel;
    DSFotos: TDataSource;
    OpenFotos: TOpenPictureDialog;
    Splitter1: TSplitter;
    PageControl3: TPageControl;
    TabCC: TTabSheet;
    TabCChist: TTabSheet;
    DBEmpresa: TJvDBLookupCombo;
    DBEdit35: TDBEdit;
    Label73: TLabel;
    DSLimSeg: TDataSource;
    PopupLimSeg: TPopupMenu;
    AlteraoLineairdelimiteporSegmento1: TMenuItem;
    AlteraoLineardeSenha1: TMenuItem;
    TabSituacao: TTabSheet;
    DSSitFornBloq: TDataSource;
    DSSitSeg: TDataSource;
    DSSitGProd: TDataSource;
    TabProdutos: TTabSheet;
    pnlMovimentacaoAutor: TPanel;
    DSProdutos: TDataSource;
    popCancAutor: TPopupMenu;
    CancelaAutorizao1: TMenuItem;
    Panel24: TPanel;
    GridSaldConv: TDBGrid;
    Panel23: TPanel;
    DSPbm: TDataSource;
    DSValGrupDesc: TDataSource;
    DSProdTrans: TDataSource;
    TabOutras: TTabSheet;
    PageControl5: TPageControl;
    TabLimiteSeg: TTabSheet;
    TabProgDesc: TTabSheet;
    TabFildelidade: TTabSheet;
    TabFotos: TTabSheet;
    Panel8: TPanel;
    GroupBox6: TGroupBox;
    Label65: TLabel;
    Bevel1: TBevel;
    Button4: TButton;
    Panel19: TPanel;
    DBFoto: TImage;
    Button6: TButton;
    GroupBox7: TGroupBox;
    Button5: TButton;
    Panel18: TPanel;
    rgCam: TRadioGroup;
    TabTodasAsCompras: TTabSheet;
    Panel32: TPanel;
    Panel31: TPanel;
    Panel30: TPanel;
    DSProgram: TDataSource;
    DSFidelidade: TDataSource;
    grdFidelidel: TJvDBGrid;
    GroupBox5: TGroupBox;
    panFidelidade: TPanel;
    Label97: TLabel;
    lblTotalPontos: TLabel;
    Label99: TLabel;
    lblAPerder: TLabel;
    Label101: TLabel;
    lblTotalSaldo: TLabel;
    btnAtualizaSaldo: TBitBtn;
    Panel34: TPanel;
    Panel9: TPanel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    btnFirstE: TSpeedButton;
    btnPriorE: TSpeedButton;
    btnNextE: TSpeedButton;
    btnLastE: TSpeedButton;
    DatainiCartao: TJvDateEdit;
    DatafimCartao: TJvDateEdit;
    CBCamposHistCartao: TComboBox;
    btnVisulHistCart: TBitBtn;
    GridHistoricoCartao: TJvDBGrid;
    Panel35: TPanel;
    Panel5: TPanel;
    Label35: TLabel;
    Label36: TLabel;
    titular: TDBText;
    empresa: TDBText;
    JvDBGrid1: TJvDBGrid;
    Panel4: TPanel;
    Label96: TLabel;
    btnFirstD: TSpeedButton;
    btnPriorD: TSpeedButton;
    btnNextD: TSpeedButton;
    btnLastD: TSpeedButton;
    ButInclui_Cartao: TBitBtn;
    ButApaga_Cartao: TBitBtn;
    ButCancelCartao: TBitBtn;
    ButGravaCartao: TBitBtn;
    ButAtualizaCodImp: TBitBtn;
    btn2Via: TBitBtn;
    Panel36: TPanel;
    GridLimPorSeg: TJvDBGrid;
    Panel20: TPanel;
    btnGravaLimSeg: TBitBtn;
    btnCancelLimSeg: TBitBtn;
    Panel33: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Panel41: TPanel;
    Panel7: TPanel;
    Label37: TLabel;
    DBText2: TDBText;
    DBText1: TDBText;
    Label38: TLabel;
    btnAlteraCC: TBitBtn;
    JvDBGrid2: TJvDBGrid;
    Panel22: TPanel;
    DBGrid2: TDBGrid;
    Panel21: TPanel;
    LabTotProd: TLabel;
    Panel14: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    btnLastF: TSpeedButton;
    btnNextF: TSpeedButton;
    btnPriorF: TSpeedButton;
    btnFirstF: TSpeedButton;
    dataini1: TJvDateEdit;
    datafin1: TJvDateEdit;
    DBCampoCC: TComboBox;
    btnVisualHist: TBitBtn;
    JvDBGrid4: TJvDBGrid;
    Panel6: TPanel;
    Label55: TLabel;
    //data1: TDateEdit;
    //data2: TDateEdit;
    btnAbrirTodasCompras: TBitBtn;
    JvDBGrid3: TJvDBGrid;
    Panel15: TPanel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Shape3: TShape;
    Shape2: TShape;
    Shape1: TShape;
    Panel42: TPanel;
    Panel13: TPanel;
    GridSaldoCartao: TDBGrid;
    Panel10: TPanel;
    Label62: TLabel;
    Label63: TLabel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel12: TPanel;
    Panel43: TPanel;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    Label12: TLabel;
    lblLimite: TLabel;
    DBCheckBox1
    : TDBCheckBox;
    dbEdtLimite: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    lblSalario: TLabel;
    dbEdtSalario: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    SpeedButton1: TSpeedButton;
    Panel44: TPanel;
    Label40: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label57: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit40: TDBEdit;
    DBComboBox2: TDBComboBox;
    DBDateEdit1: TDBDateEdit;
    DBDateEdit2: TDBDateEdit;
    DBDateEdit4: TDBDateEdit;
    DBDateEdit5: TDBDateEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    Label93: TLabel;
    DBEdit10: TDBEdit;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit60: TDBEdit;
    btnLastG: TSpeedButton;
    btnNextG: TSpeedButton;
    btnPriorG: TSpeedButton;
    btnFirstG: TSpeedButton;
    tabTransacoes: TTabSheet;
    Splitter8: TSplitter;
    Panel28: TPanel;
    Splitter4: TSplitter;
    GroupBox16: TGroupBox;
    gridTransacao: TJvDBGrid;
    GroupBox18: TGroupBox;
    gridProdTran: TJvDBGrid;
    GroupBox17: TGroupBox;
    mmCupom: TDBMemo;
    Panel27: TPanel;
    Label95: TLabel;
    //datainiPbm: TDateEdit;
    //datafimPbm: TDateEdit;
    ckbSomenteValProgramas: TCheckBox;
    BitBtn1: TBitBtn;
    Splitter3: TSplitter;
    Panel38: TPanel;
    Panel29: TPanel;
    JvDBGrid88: TJvDBGrid;
    Panel37: TPanel;
    Panel26: TPanel;
    btnGravaPbm: TBitBtn;
    btnCancelPbm: TBitBtn;
    Panel25: TPanel;
    GridPbm: TJvDBGrid;
    Splitter2: TSplitter;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    EdBuscaCartao: TEdit;
    Label29: TLabel;
    tExcel1: TADOTable;
    tExcel2: TADOTable;
    tExcel3: TADOTable;
    tExcel1CHAPA: TFloatField;
    tExcel1N_CART_TIT: TFloatField;
    tExcel1NOMEFUNCIONRIO: TWideStringField;
    tExcel1N_CART_DEP: TFloatField;
    tExcel1NOMEDODEPENDENTE: TWideStringField;
    tExcel1N_CART: TWideStringField;
    tExcel2CHAPA: TFloatField;
    tExcel2N_CART_TIT: TFloatField;
    tExcel2NOMEFUNCIONRIO: TWideStringField;
    tExcel2N_CART_DEP: TFloatField;
    tExcel2NOMEDODEPENDENTE: TWideStringField;
    tExcel2N_CART: TWideStringField;
    tExcel3CHAPA: TFloatField;
    tExcel3N_CART_TIT: TFloatField;
    tExcel3NOMEFUNCIONRIO: TWideStringField;
    tExcel3N_CART_DEP: TFloatField;
    tExcel3NOMEDODEPENDENTE: TWideStringField;
    tExcel3N_CART: TWideStringField;
    btncad: TButton;
    PageControl4: TPageControl;
    TabSheet5: TTabSheet;
    GroupBox10: TGroupBox;
    Label66: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    lblAbono: TLabel;
    lblFech: TLabel;
    lblDemissao: TLabel;
    lblGastoCRec: TLabel;
    lblGastoSRec: TLabel;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit52: TDBEdit;
    dbSaldoMes: TDBEdit;
    dbEdtAbono: TDBEdit;
    dbEdtFech: TDBEdit;
    dbEdtDemissao: TDBEdit;
    dbEdtGastoCRec: TDBEdit;
    dbEdtGastoSRec: TDBEdit;
    GroupBox11: TGroupBox;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    GroupBox15: TGroupBox;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    Label108: TLabel;
    DBEdit59: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    dbCbUsaLimiteDif: TDBCheckBox;
    GroupBox3: TGroupBox;
    lblLimite1: TLabel;
    lblLimite2: TLabel;
    lblLimite3: TLabel;
    lblLimite4: TLabel;
    DSBandConv: TDataSource;
    edtLimite1: TDBEdit;
    edtLimite2: TDBEdit;
    edtLimite3: TDBEdit;
    edtLimite4: TDBEdit;
    dbEdtAbonoMes: TDBEdit;
    dbEdtSaldoRenovacao: TDBEdit;
    lblSaldoRenovacao: TLabel;
    lblAbonoMes: TLabel;
    dbEdtSaldoAcumulado: TDBEdit;
    lblSaldoAcumulado: TLabel;
    dbEdtAcumulado: TDBEdit;
    DSSituacao: TDataSource;
    dbEdtSaldoRest: TDBEdit;
    lblSaldoRest: TLabel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DSEstados: TDataSource;
    DSCidades: TDataSource;
    dbLkpCidades: TDBLookupComboBox;
    dbLkpEstados: TDBLookupComboBox;
    Label17: TLabel;
    datainiPbm: TJvDateEdit;
    datafimPbm: TJvDateEdit;
    data1: TJvDateEdit;
    data2: TJvDateEdit;
    DSBand: TDataSource;
    QBanco: TADOQuery;
    QBancocodigo: TIntegerField;
    QBancobanco: TStringField;
    QBand: TADOQuery;
    QBandband_id: TIntegerField;
    QBandqtd_limites: TIntegerField;
    QBandConv: TADOQuery;
    QBandConvCONV_ID: TIntegerField;
    QBandConvLIMITE_1: TBCDField;
    QBandConvLIMITE_2: TBCDField;
    QBandConvLIMITE_3: TBCDField;
    QBandConvLIMITE_4: TBCDField;
    QBandConvDTCADASTRO: TDateTimeField;
    QBandConvOPERCADASTRO: TStringField;
    QBandConvDTALTERACAO: TDateTimeField;
    QBandConvOPERADOR: TStringField;
    QBandConvDTAPAGADO: TDateTimeField;
    QBandConvAPAGADO: TStringField;
    QCartoes: TADOQuery;
    QCartoesCARTAO_ID: TIntegerField;
    QCartoesCONV_ID: TIntegerField;
    QCartoesNOME: TStringField;
    QCartoesLIBERADO: TStringField;
    QCartoesCODIGO: TIntegerField;
    QCartoesDIGITO: TWordField;
    QCartoesTITULAR: TStringField;
    QCartoesJAEMITIDO: TStringField;
    QCartoesAPAGADO: TStringField;
    QCartoesLIMITE_MES: TBCDField;
    QCartoesCODCARTIMP: TStringField;
    QCartoesPARENTESCO: TStringField;
    QCartoesDATA_NASC: TDateTimeField;
    QCartoesNUM_DEP: TIntegerField;
    QCartoesFLAG: TStringField;
    QCartoesDTEMISSAO: TDateTimeField;
    QCartoesCPF: TStringField;
    QCartoesRG: TStringField;
    QCartoesVIA: TIntegerField;
    QCartoesDTAPAGADO: TDateTimeField;
    QCartoesDTALTERACAO: TDateTimeField;
    QCartoesOPERADOR: TStringField;
    QCartoesDTCADASTRO: TDateTimeField;
    QCartoesOPERCADASTRO: TStringField;
    QCartoesCRED_ID: TIntegerField;
    QCartoesATIVO: TStringField;
    QCidades: TADOQuery;
    QCidadesCID_ID: TIntegerField;
    QCidadesESTADO_ID: TIntegerField;
    QCidadesNOME: TStringField;
    QContaCorrente: TADOQuery;
    QContaCorrenteAUTORIZACAO_ID: TIntegerField;
    QContaCorrenteCARTAO_ID: TIntegerField;
    QContaCorrenteCONV_ID: TIntegerField;
    QContaCorrenteCRED_ID: TIntegerField;
    QContaCorrenteDIGITO: TWordField;
    QContaCorrenteDATA: TDateTimeField;
    QContaCorrenteHORA: TStringField;
    QContaCorrenteDATAVENDA: TDateTimeField;
    QContaCorrenteDEBITO: TBCDField;
    QContaCorrenteCREDITO: TBCDField;
    QContaCorrenteVALOR_CANCELADO: TBCDField;
    QContaCorrenteBAIXA_CONVENIADO: TStringField;
    QContaCorrenteBAIXA_CREDENCIADO: TStringField;
    QContaCorrenteENTREG_NF: TStringField;
    QContaCorrenteRECEITA: TStringField;
    QContaCorrenteCESTA: TStringField;
    QContaCorrenteCANCELADA: TStringField;
    QContaCorrenteDIGI_MANUAL: TStringField;
    QContaCorrenteTRANS_ID: TIntegerField;
    QContaCorrenteFORMAPAGTO_ID: TIntegerField;
    QContaCorrenteFATURA_ID: TIntegerField;
    QContaCorrentePAGAMENTO_CRED_ID: TIntegerField;
    QContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    QContaCorrenteOPERADOR: TStringField;
    QContaCorrenteDATA_VENC_EMP: TDateTimeField;
    QContaCorrenteDATA_FECHA_EMP: TDateTimeField;
    QContaCorrenteDATA_VENC_FOR: TDateTimeField;
    QContaCorrenteDATA_FECHA_FOR: TDateTimeField;
    QContaCorrenteHISTORICO: TStringField;
    QContaCorrenteNF: TIntegerField;
    QContaCorrenteDATA_ALTERACAO: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CONV: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CRED: TDateTimeField;
    QContaCorrenteOPER_BAIXA_CONV: TStringField;
    QContaCorrenteOPER_BAIXA_CRED: TStringField;
    QContaCorrenteDATA_CONFIRMACAO: TDateTimeField;
    QContaCorrenteOPER_CONFIRMACAO: TStringField;
    QContaCorrenteCONFERIDO: TStringField;
    QContaCorrenteNSU: TIntegerField;
    QContaCorrentePREVIAMENTE_CANCELADA: TStringField;
    QContaCorrenteEMPRES_ID: TIntegerField;
    QConv: TADOQuery;
    QConvCONV_ID: TIntegerField;
    QConvEMPRES_ID: TIntegerField;
    QConvBANCO: TIntegerField;
    QConvGRUPO_CONV_EMP: TIntegerField;
    QConvCHAPA: TFloatField;
    QConvSENHA: TStringField;
    QConvTITULAR: TStringField;
    QConvCONTRATO: TIntegerField;
    QConvLIMITE_MES: TBCDField;
    QConvLIBERADO: TStringField;
    QConvFIDELIDADE: TStringField;
    QConvAPAGADO: TStringField;
    QConvDT_NASCIMENTO: TDateTimeField;
    QConvCARGO: TStringField;
    QConvSETOR: TStringField;
    QConvCPF: TStringField;
    QConvRG: TStringField;
    QConvLIMITE_TOTAL: TBCDField;
    QConvLIMITE_PROX_FECHAMENTO: TBCDField;
    QConvAGENCIA: TStringField;
    QConvCONTACORRENTE: TStringField;
    QConvDIGITO_CONTA: TStringField;
    QConvTIPOPAGAMENTO: TStringField;
    QConvENDERECO: TStringField;
    QConvNUMERO: TIntegerField;
    QConvCEP: TStringField;
    QConvTELEFONE1: TStringField;
    QConvTELEFONE2: TStringField;
    QConvCELULAR: TStringField;
    QConvOBS1: TStringField;
    QConvOBS2: TStringField;
    QConvEMAIL: TStringField;
    QConvCESTABASICA: TBCDField;
    QConvDTULTCESTA: TDateTimeField;
    QConvSALARIO: TBCDField;
    QConvTIPOSALARIO: TStringField;
    QConvCOD_EMPRESA: TStringField;
    QConvFLAG: TStringField;
    QConvDTASSOCIACAO: TDateTimeField;
    QConvDTAPAGADO: TDateTimeField;
    QConvDTALTERACAO: TDateTimeField;
    QConvOPERADOR: TStringField;
    QConvDTCADASTRO: TDateTimeField;
    QConvOPERCADASTRO: TStringField;
    QConvVALE_DESCONTO: TStringField;
    QConvLIBERA_GRUPOSPROD: TStringField;
    QConvCOMPLEMENTO: TStringField;
    QConvUSA_SALDO_DIF: TStringField;
    QConvABONO_MES: TBCDField;
    QConvSALDO_RENOVACAO: TBCDField;
    QConvSALDO_ACUMULADO: TBCDField;
    QConvDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QConvCONSUMO_MES: TBCDField;
    QConvDetail: TADOQuery;
    QConvDetailCONV_ID: TIntegerField;
    QConvDetailPIS: TFloatField;
    QConvDetailNOME_PAI: TStringField;
    QConvDetailNOME_MAE: TStringField;
    QConvDetailCART_TRAB_NUM: TIntegerField;
    QConvDetailCART_TRAB_SERIE: TStringField;
    QConvDetailREGIME_TRAB: TStringField;
    QConvDetailVENC_TOTAL: TBCDField;
    QConvDetailESTADO_CIVIL: TStringField;
    QConvDetailNUM_DEPENDENTES: TIntegerField;
    QConvDetailDATA_ADMISSAO: TDateTimeField;
    QConvDetailDATA_DEMISSAO: TDateTimeField;
    QConvDetailFIM_CONTRATO: TDateTimeField;
    QConvDetailDISTRITO: TStringField;
    QConvDetailSALDO_DEVEDOR: TBCDField;
    QConvDetailSALDO_DEVEDOR_FAT: TBCDField;
    QDataAdmisDemiss: TADOQuery;
    QDataAdmisDemissdata_admissao: TDateTimeField;
    QDataAdmisDemissdata_demissao: TDateTimeField;
    QDataAdmisDemissconv_id: TIntegerField;
    QEmpresa: TADOQuery;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresausa_cod_importacao: TStringField;
    QEmpresafantasia: TStringField;
    QEmpresafidelidade: TStringField;
    QEmpresaband_id: TIntegerField;
    QEstados: TADOQuery;
    QEstadosESTADO_ID: TIntegerField;
    QEstadosUF: TStringField;
    QFidelidade: TADOQuery;
    QFidelidadehist_id: TIntegerField;
    QFidelidadecred_id: TIntegerField;
    QFidelidadenome: TStringField;
    QFidelidadedatahora: TDateTimeField;
    QFidelidadetrans_id: TIntegerField;
    QFidelidadesaldo: TIntegerField;
    QFidelidadehistorico: TStringField;
    QFidelidadedataexpira: TDateTimeField;
    QFidelidaderesgate_id: TIntegerField;
    QFotos: TADOQuery;
    QFotosFOTO_ID: TIntegerField;
    QFotosCONV_ID: TIntegerField;
    QFotosFOTO: TStringField;
    QGrupo_conv_emp: TADOQuery;
    QGrupo_conv_empGRUPO_CONV_EMP_ID: TIntegerField;
    QGrupo_conv_empDESCRICAO: TStringField;
    QGrupo_conv_empEMPRES_ID: TIntegerField;
    QHistCartoes: TADOQuery;
    QHistCartoesLOG_ID: TIntegerField;
    QHistCartoesJANELA: TStringField;
    QHistCartoesCAMPO: TStringField;
    QHistCartoesVALOR_ANT: TStringField;
    QHistCartoesVALOR_POS: TStringField;
    QHistCartoesOPERADOR: TStringField;
    QHistCartoesOPERACAO: TStringField;
    QHistCartoesDATA_HORA: TDateTimeField;
    QHistCartoesCADASTRO: TStringField;
    QHistCartoesID: TIntegerField;
    QHistCartoesDETALHE: TStringField;
    QHistCartoesMOTIVO: TStringField;
    QHistCartoesSOLICITANTE: TStringField;
    QLimSeg: TADOQuery;
    QPbm: TADOQuery;
    QProdTrans: TADOQuery;
    QProdutos: TADOQuery;
    QProdutosMOV_ID: TIntegerField;
    QProdutosAUTORIZACAO_ID: TIntegerField;
    QProdutosQTDE: TIntegerField;
    QProdutosPRECO_UNI: TFloatField;
    QProdutosPRECO_TAB: TFloatField;
    QProdutosCANCELADO: TStringField;
    QProdutosCOMREC: TStringField;
    QProdutosPROD_ID: TIntegerField;
    QProdutosCRM: TStringField;
    QProdutosDATA_CADASTRO: TDateTimeField;
    QProdutosdescricao: TStringField;
    QProdutoscodinbs: TStringField;
    QPrograma: TADOQuery;
    QProgramaprod_id: TIntegerField;
    QProgramadescricao: TStringField;
    QProgramaprog_id: TIntegerField;
    QProgramacodbarras: TStringField;
    QProgramaprc_unit: TBCDField;
    QProgramaperc_desc: TBCDField;
    QProgramaqtd_max: TIntegerField;
    QProgramafabricante: TStringField;
    QProgramaobrig_receita: TStringField;
    QReceitaSemLimite: TADOQuery;
    QReceitaSemLimitereceita_sem_limite: TStringField;
    QSaldoCartao: TADOQuery;
    QSaldoConv: TADOQuery;
    QSitFornBloq: TADOQuery;
    QSitGProd: TADOQuery;
    QSitFornBloqcred_id: TIntegerField;
    QSitFornBloqnome: TStringField;
    QSitFornBloqsituacao: TStringField;
    QSitFornBloqliberado: TStringField;
    QSitSeg: TADOQuery;
    QSituacao: TADOQuery;
    QTodasCompras: TADOQuery;
    QValGrupDesc: TADOQuery;
    QCredenciado: TADOQuery;
    QCredenciadocred_id: TIntegerField;
    QCredenciadofantasia: TStringField;
    qProgram: TADOQuery;
    qProgramprod_id: TIntegerField;
    qProgramdescricao: TStringField;
    qProgramprog_id: TIntegerField;
    qProgramcodbarras: TStringField;
    qProgramprc_unit: TBCDField;
    qProgramperc_desc: TBCDField;
    qProgramqtd_max: TIntegerField;
    qProgramfabricante: TStringField;
    qProgramobrig_receita: TStringField;
    Qconveniado: TADOQuery;
    QconveniadoCONV_ID: TIntegerField;
    QconveniadoEMPRES_ID: TIntegerField;
    QconveniadoBANCO: TIntegerField;
    QconveniadoGRUPO_CONV_EMP: TIntegerField;
    QconveniadoCHAPA: TFloatField;
    QconveniadoSENHA: TStringField;
    QconveniadoTITULAR: TStringField;
    QconveniadoCONTRATO: TIntegerField;
    QconveniadoLIMITE_MES: TBCDField;
    QconveniadoLIBERADO: TStringField;
    QconveniadoFIDELIDADE: TStringField;
    QconveniadoAPAGADO: TStringField;
    QconveniadoDT_NASCIMENTO: TDateTimeField;
    QconveniadoCARGO: TStringField;
    QconveniadoSETOR: TStringField;
    QconveniadoCPF: TStringField;
    QconveniadoRG: TStringField;
    QconveniadoLIMITE_TOTAL: TBCDField;
    QconveniadoLIMITE_PROX_FECHAMENTO: TBCDField;
    QconveniadoAGENCIA: TStringField;
    QconveniadoCONTACORRENTE: TStringField;
    QconveniadoDIGITO_CONTA: TStringField;
    QconveniadoTIPOPAGAMENTO: TStringField;
    QconveniadoENDERECO: TStringField;
    QconveniadoNUMERO: TIntegerField;
    QconveniadoCEP: TStringField;
    QconveniadoTELEFONE1: TStringField;
    QconveniadoTELEFONE2: TStringField;
    QconveniadoCELULAR: TStringField;
    QconveniadoOBS1: TStringField;
    QconveniadoOBS2: TStringField;
    QconveniadoEMAIL: TStringField;
    QconveniadoCESTABASICA: TBCDField;
    QconveniadoDTULTCESTA: TDateTimeField;
    QconveniadoSALARIO: TBCDField;
    QconveniadoTIPOSALARIO: TStringField;
    QconveniadoCOD_EMPRESA: TStringField;
    QconveniadoFLAG: TStringField;
    QconveniadoDTASSOCIACAO: TDateTimeField;
    QconveniadoDTAPAGADO: TDateTimeField;
    QconveniadoDTALTERACAO: TDateTimeField;
    QconveniadoOPERADOR: TStringField;
    QconveniadoDTCADASTRO: TDateTimeField;
    QconveniadoOPERCADASTRO: TStringField;
    QconveniadoVALE_DESCONTO: TStringField;
    QconveniadoLIBERA_GRUPOSPROD: TStringField;
    QconveniadoCOMPLEMENTO: TStringField;
    QconveniadoUSA_SALDO_DIF: TStringField;
    QconveniadoABONO_MES: TBCDField;
    QconveniadoSALDO_RENOVACAO: TBCDField;
    QconveniadoSALDO_ACUMULADO: TBCDField;
    QconveniadoDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QconveniadoCONSUMO_MES: TBCDField;
    DSconveniado: TDataSource;
    QSituacaoconv_id: TIntegerField;
    QSituacaotitular: TStringField;
    QSituacaochapa: TFloatField;
    DS_SituacaoNew: TDataSource;
    QSituacao_Dif_999: TADOQuery;
    DS_Dif_999: TDataSource;
    DBEdit3: TDBEdit;
    Label98: TLabel;
    DBEdit15: TDBEdit;
    Label100: TLabel;
    DBEdit26: TDBEdit;
    Label102: TLabel;
    QSituacao_Dif_999consumo_mes_1: TBCDField;
    QSituacao_Dif_999consumo_mes_2: TBCDField;
    QSituacao_Dif_999consumo_mes_3: TBCDField;
    QSituacao_Dif_999limite_1: TBCDField;
    QSituacao_Dif_999limite_2: TBCDField;
    QSituacao_Dif_999LIMITE_3: TBCDField;
    QSituacao_new: TADOQuery;
    QSituacao_newconv_id: TIntegerField;
    QSituacao_newtitular: TStringField;
    QSituacao_newchapa: TFloatField;
    QSituacao_newlimite_1: TBCDField;
    QSituacao_newconsumo_mes_1: TBCDField;
    QSituacao_newsaldo_acumulado: TBCDField;
    QSituacao_newabono_mes: TBCDField;
    QSituacao_newsaldo_restante: TBCDField;
    QSituacao_newFechamento: TDateTimeField;
    QSituacao_newlimite_2: TBCDField;
    QSituacao_newlimite_3: TBCDField;
    QSituacao_newconsumo_mes_2: TBCDField;
    QSituacao_newconsumo_mes_3: TBCDField;
    QSituacao_newsaldo_restante2: TBCDField;
    QSituacao_newsaldo_restante3: TBCDField;
    lblAcumulado: TLabel;
    QSituacao_newCODCARTIMP: TStringField;
    QSituacao_newNOME_CARTAO: TStringField;
    QSituacao_newCOD_CARTAO: TIntegerField;
    QSituacao_newempres_id: TAutoIncField;
    QSituacao_newnome_empres: TStringField;
    QSituacao_newtodos_segmentos: TStringField;
    QSituacao_newaceita_parc: TStringField;
    QSituacao_newvenda_nome: TStringField;
    QSitGProdgrupo_prod_id: TIntegerField;
    QSitGProddescricao: TStringField;
    QSitGProdliberado: TStringField;
    QSitGProddesconto: TFloatField;
    QSitGProdpreco_fabrica: TStringField;
    QSitGProdremp_grupo_prod_id: TIntegerField;
    QSituacao_newliberado: TStringField;
    QSituacao_newemp_lib: TStringField;
    QSituacao_newCART_LIB: TStringField;
    QSituacao_newdemissao: TDateTimeField;
    QContaCorrenteCREDENCIADO: TStringField;
    lblFiltro: TLabel;
    lblFechaIni: TLabel;
    Label34: TLabel;
    DEdataini: TJvDateEdit;
    DEdatafim: TJvDateEdit;
    QSaldoConvsaldo_conf: TBCDField;
    QSaldoConvsaldo_nconf: TBCDField;
    QSaldoConvFECHAMENTO: TWideStringField;
    QSaldoConvfaturada: TStringField;
    QSaldoConvfatura_id: TIntegerField;
    QSaldoConvdata_fatura: TDateTimeField;
    QSaldoConvtipo: TStringField;
    QSaldoConvtotal: TBCDField;
    QSaldoCartaosaldo_conf: TBCDField;
    QSaldoCartaosaldo_nconf: TBCDField;
    QSaldoCartaosaldo_sem_receita: TBCDField;
    QSaldoCartaosaldo_com_receita: TBCDField;
    QSaldoCartaoFECHAMENTO: TWideStringField;
    QSaldoCartaofaturada: TStringField;
    QSaldoCartaofatura_id: TIntegerField;
    QSaldoCartaodata_fatura: TDateTimeField;
    QSaldoCartaotipo: TStringField;
    QSaldoCartaototal: TBCDField;
    QSaldoCartaoCODCARTIMP: TStringField;
    QSaldoCartaonome: TStringField;
    QCadastroCONV_ID: TIntegerField;
    QCadastroTITULAR: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroGRUPO_CONV_EMP: TIntegerField;
    QCadastroCARGO: TStringField;
    QCadastroCOD_EMPRESA: TStringField;
    QCadastroDT_NASCIMENTO: TDateTimeField;
    QCadastroCPF: TStringField;
    QCadastroRG: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroCEP: TStringField;
    QCadastroBANCO: TIntegerField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTACORRENTE: TStringField;
    QCadastroDIGITO_CONTA: TStringField;
    QCadastroTIPOPAGAMENTO: TStringField;
    QCadastroTELEFONE1: TStringField;
    QCadastroTELEFONE2: TStringField;
    QCadastroCELULAR: TStringField;
    QCadastroOBS1: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTULTCESTA: TDateTimeField;
    QCadastroDTASSOCIACAO: TDateTimeField;
    QCadastroEMAIL: TStringField;
    QCadastroLIMITE_MES: TBCDField;
    QCadastroLIMITE_TOTAL: TBCDField;
    QCadastroLIMITE_PROX_FECHAMENTO: TBCDField;
    QCadastroCESTABASICA: TBCDField;
    QCadastroSALARIO: TBCDField;
    QCadastroFIDELIDADE: TStringField;
    QCadastroCONTRATO: TIntegerField;
    QCadastroTIPOSALARIO: TStringField;
    QCadastroFLAG: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroAPAGADO: TStringField;
    QCadastroVALE_DESCONTO: TStringField;
    QCadastroLIBERA_GRUPOSPROD: TStringField;
    QCadastroCOMPLEMENTO: TStringField;
    QCadastroUSA_SALDO_DIF: TStringField;
    QCadastroABONO_MES: TBCDField;
    QCadastroSALDO_RENOVACAO: TBCDField;
    QCadastroSALDO_ACUMULADO: TBCDField;
    QCadastroDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QCadastroCHAPA: TFloatField;
    QCadastroDATA_ADMISSAO: TDateTimeField;
    QCadastroDATA_DEMISSAO: TDateTimeField;
    QCadastroNUM_DEPENDENTES: TIntegerField;
    QCadastroSALDO_DEVEDOR: TBCDField;
    QCadastroSALDO_DEVEDOR_FAT: TBCDField;
    QCadastroPIS: TFloatField;
    QCadastroNOME_PAI: TStringField;
    QCadastroNOME_MAE: TStringField;
    QCadastroCART_TRAB_NUM: TIntegerField;
    QCadastroCART_TRAB_SERIE: TStringField;
    QCadastroREGIME_TRAB: TStringField;
    QCadastroVENC_TOTAL: TBCDField;
    QCadastroFIM_CONTRATO: TDateTimeField;
    QCadastroDISTRITO: TStringField;
    QCadastroESTADO_CIVIL: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroBAND_ID: TIntegerField;
    QCadastroempresa: TStringField;
    QCadastroTIPO_CREDITO: TIntegerField;
    QCadastroQTD_Limites: TIntegerField;
    QValGrupDesctrans_id: TIntegerField;
    QValGrupDesccred_id: TIntegerField;
    QValGrupDescdatahora: TDateTimeField;
    QValGrupDescnome: TStringField;
    QValGrupDescconfirmada: TStringField;
    QValGrupDescpontos: TIntegerField;
    QValGrupDescvale_acumulado: TBCDField;
    QValGrupDescvale_utilizado: TBCDField;
    QValGrupDescbruto: TBCDField;
    QValGrupDescdescont: TBCDField;
    QValGrupDescliquido: TBCDField;
    QValGrupDesccupom: TStringField;
    QValGrupDescoperador: TStringField;
    QProdTransprog_id: TIntegerField;
    QProdTransnome: TStringField;
    QProdTranstrans_id: TIntegerField;
    QProdTransprod_id: TIntegerField;
    QProdTransdescricao: TStringField;
    QProdTranscodbarras: TStringField;
    QProdTransqtd_solic: TIntegerField;
    QProdTransqtd_aprov: TIntegerField;
    QProdTransprc_unitbru_rec: TBCDField;
    QProdTransprc_unit_env: TBCDField;
    QProdTransvlr_bru: TBCDField;
    QProdTransvlr_desc: TBCDField;
    QProdTransvlr_liq: TBCDField;
    QProdTransstatus: TIntegerField;
    QProdTranssituacao: TStringField;
    QProdTransvale_acumulado: TBCDField;
    QProdTransvale_utilizado: TBCDField;
    QProdTransgerou_pontos: TStringField;
    QPbmprog_id: TIntegerField;
    QPbmnome: TStringField;
    QPbmparticipa: TStringField;
    QPbmempres_id: TIntegerField;
    QLimSegseg_id: TIntegerField;
    QLimSegdescricao: TStringField;
    QLimSegporcent: TBCDField;
    QLimSegCONV_ID: TIntegerField;
    QLimSegEMPRES_ID: TIntegerField;
    QLimSegLimite: TBCDField;
    QLimSeglimite_cem_por_cento: TBCDField;
    QCadastroSETOR_ID: TIntegerField;
    QCadastroCONSUMO_MES_1: TBCDField;
    QCadastroCONSUMO_MES_3: TBCDField;
    QCadastroCONSUMO_MES_4: TBCDField;
    edtEmpr: TEdit;
    Label39: TLabel;
    QBuscaSeg_id: TADOQuery;
    QBuscaSeg_idseg_id: TIntegerField;
    btnAtualizar: TButton;
    CheckBox1: TCheckBox;
    MDTemp: TJvMemoryData;
    qTemp: TADOQuery;
    DSTemp: TDataSource;
    qTempconv_id: TIntegerField;
    MDTempconv_id: TIntegerField;
    Edit1: TEdit;
    Label89: TLabel;
    QCartoesEMPRES_ID: TIntegerField;
    lblTranferencia: TLabel;
    btnTransferencia: TButton;
    dbConvID: TEdit;
    QCadastroSETOR: TStringField;
    DBSetor: TJvDBLookupCombo;
    QEmp_Dpto: TADOQuery;
    DSEmp_Dpto: TDataSource;
    QEmp_DptoDEPT_ID: TIntegerField;
    QEmp_DptoDESCRICAO: TStringField;
    QEmp_DptoEMPRES_ID: TIntegerField;
    QEmp_DptoDPTO_APAGADO: TStringField;
    cbbLiberado: TComboBox;
    lbl1: TLabel;
    QCartoesSENHA: TStringField;
    BitBtn2: TBitBtn;
    dbLkpBairros: TDBLookupComboBox;
    btnAdicionaBairro: TBitBtn;
    QBairros: TADOQuery;
    QBairrosBAIRRO_ID: TAutoIncField;
    QBairrosCID_ID: TIntegerField;
    QBairrosDESCRICAO: TStringField;
    DSBairros: TDataSource;
    lblAddBairro: TLabel;
    QCadastroBAIRRO: TIntegerField;
    QCadastroCIDADE: TIntegerField;
    QCadastroESTADO: TIntegerField;
    QconveniadoBAIRRO: TIntegerField;
    QconveniadoCIDADE: TIntegerField;
    QconveniadoESTADO: TIntegerField;
    QCadastroCONSUMO_MES_2: TBCDField;
    QCartoesCVV: TStringField;
    QCadastroNOMEBAIRRO: TStringField;
    QCadastroNOMECIDADE: TStringField;
    QCadastroNOMEESTADO: TStringField;
    QEmpresausa_novo_cartao: TStringField;
    QCartoesJAEMITIDO_CARTAO_NOVO: TStringField;
    QCartoesDATA_EMISSAO_CARTAO_NOVO: TDateTimeField;
    QCartoesLOG_ID_CARTAO_NOVO: TIntegerField;
    btnVerSenha: TBitBtn;
    lbl2: TLabel;
    TabSheet4: TTabSheet;
    GroupBox12: TGroupBox;
    Label90: TLabel;
    Label94: TLabel;
    cbbserie: TDBComboBox;
    cbbModeloCartaoCantinex: TDBComboBox;
    QCadastromodelo_cartao_cantinex: TIntegerField;
    QCadastroserie: TStringField;
    QCartoesBLOQUEIO_ANALISE: TStringField;
    cbApagado: TDBCheckBox;
    QEmpresatipo_credito: TIntegerField;
    //QContaCorrenteCREDENCIADO: TStringField;
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure DSCartoesStateChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabCartoesShow(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DSCadastroStateChange(Sender: TObject);
    procedure QCartoesBeforePost(DataSet: TDataSet);
    procedure ButApagaClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure JvDBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure JvDBGrid3TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Exportarparaoexcel2Click(Sender: TObject);
    procedure Exportarparaoexcel3Click(Sender: TObject);
    procedure Exportarparaoexcel4Click(Sender: TObject);
    procedure TabFichaExit(Sender: TObject);
    procedure DBEdit11KeyPress(Sender: TObject; var Key: Char);
    procedure QConvDetailBeforePost(DataSet: TDataSet);
    procedure DSConvDetailStateChange(Sender: TObject);
    procedure ButGravaClick(Sender: TObject);
    procedure ButCancelaClick(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure DBEdit22KeyPress(Sender: TObject; var Key: Char);
    procedure DBComboBox2KeyPress(Sender: TObject; var Key: Char);
    procedure JvDBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ButInclui_CartaoClick(Sender: TObject);
    procedure TabHistCartoesHide(Sender: TObject);
    procedure TabHistCartoesShow(Sender: TObject);
    procedure DatainiCartaoExit(Sender: TObject);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QCartoesPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure GridSaldConvDrawColumnCell(Sender: TObject; const Rect: TRect;
    DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabSaldosShow(Sender: TObject);
    procedure GridSaldoCartaoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure QSaldoConvAfterScroll(DataSet: TDataSet);
    procedure GridSaldoCar(Column: TColumn);
    //procedure GridSaldConvTitleClick(Sender: TObject; ACol: Integer;
    //Field: TField);
    //procedure GridSaldConvTitleClick(Column: TColumn);
    procedure DBEmpresaExit(Sender: TObject);
    procedure QCadastroPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure QFotosAfterScroll(DataSet: TDataSet);
    procedure qContaCorrenteBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TabCChistShow(Sender: TObject);
    procedure TabContaCorrenteShow(Sender: TObject);
    procedure datafin1Exit(Sender: TObject);
    procedure TabCCResize(Sender: TObject);
    procedure JvDBGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButAtualizaCodImpClick(Sender: TObject);
    procedure QLimSegAfterPost(DataSet: TDataSet);
    procedure AlteraoLineairdelimiteporSegmento1Click(Sender: TObject);
    procedure AlteraoLineardeSenha1Click(Sender: TObject);
    procedure DSLimSegStateChange(Sender: TObject);
    procedure JvDBGrid2DblClick(Sender: TObject);
    procedure JvDBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid7DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TabProdutosShow(Sender: TObject);
    procedure TabProdutosHide(Sender: TObject);
    procedure QProdutosAfterOpen(DataSet: TDataSet);
    procedure CancelaAutorizao1Click(Sender: TObject);
    procedure JvDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DSPbmStateChange(Sender: TObject);
    procedure qPbmAfterPost(DataSet: TDataSet);
    procedure qPbmBeforePost(DataSet: TDataSet);
    procedure GridPbmColExit(Sender: TObject);
    procedure DSValGrupDescDataChange(Sender: TObject; Field: TField);
    procedure TabFichaShow(Sender: TObject);
    procedure GridLimPorSegColExit(Sender: TObject);
    procedure gridTransacaoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure QLimSegBeforeEdit(DataSet: TDataSet);
    procedure QLimSegBeforeInsert(DataSet: TDataSet);
    procedure qPbmBeforeInsert(DataSet: TDataSet);
    procedure qPbmBeforeEdit(DataSet: TDataSet);
    procedure DSContaCorrenteStateChange(Sender: TObject);
    procedure qContaCorrenteBeforeEdit(DataSet: TDataSet);
    procedure qContaCorrenteBeforeInsert(DataSet: TDataSet);
    procedure btn2ViaClick(Sender: TObject);
    procedure TabLimiteSegHide(Sender: TObject);
    procedure TabLimiteSegShow(Sender: TObject);
    procedure TabFotosExit(Sender: TObject);
    procedure TabFotosShow(Sender: TObject);
    procedure TabTodasAsComprasExit(Sender: TObject);
    procedure TabTodasAsComprasShow(Sender: TObject);
    procedure TabFildelidadeShow(Sender: TObject);
    procedure TabFildelidadeExit(Sender: TObject);
    procedure TabOutrasShow(Sender: TObject);
    procedure PageControl5Enter(Sender: TObject);
    procedure btnGravaLimSegClick(Sender: TObject);
    procedure ButApaga_CartaoClick(Sender: TObject);
    procedure btnCancelLimSegClick(Sender: TObject);
    procedure ButGravaCartaoClick(Sender: TObject);
    procedure ButCancelCartaoClick(Sender: TObject);
    procedure btnGravaPbmClick(Sender: TObject);
    procedure btnCancelPbmClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnAlteraCCClick(Sender: TObject);
    procedure btnAtualizaSaldoClick(Sender: TObject);
    procedure btnVisualHistClick(Sender: TObject);
    procedure btnAbrirTodasComprasClick(Sender: TObject);
    procedure btnFirstDClick(Sender: TObject);
    procedure btnPriorDClick(Sender: TObject);
    procedure btnNextDClick(Sender: TObject);
    procedure btnLastDClick(Sender: TObject);
    procedure DSCartoesDataChange(Sender: TObject; Field: TField);
    procedure btnFirstEClick(Sender: TObject);
    procedure btnPriorEClick(Sender: TObject);
    procedure btnNextEClick(Sender: TObject);
    procedure btnLastEClick(Sender: TObject);
    procedure btnVisulHistCartClick(Sender: TObject);
    procedure DSHistoricoDataChange(Sender: TObject; Field: TField);
    procedure DSHistCartoesDataChange(Sender: TObject; Field: TField);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnFirstCClick(Sender: TObject);
    procedure btnPriorCClick(Sender: TObject);
    procedure btnNextCClick(Sender: TObject);
    procedure btnLastCClick(Sender: TObject);
    procedure DSContaCorrenteDataChange(Sender: TObject; Field: TField);
    procedure tabTransacoesHide(Sender: TObject);
    procedure tabTransacoesShow(Sender: TObject);
    procedure TabProgDescHide(Sender: TObject);
    procedure TabProgDescShow(Sender: TObject);
    procedure dbEdtChapaKeyPress(Sender: TObject; var Key: Char);
    procedure QLimSegBeforePost(DataSet: TDataSet);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
    procedure btncadClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dbCbUsaLimiteDifClick(Sender: TObject);
    procedure DBEmpresaChange(Sender: TObject);
    procedure edtLimite1_Change(Sender: TObject);
    procedure DSBandConvDataChange(Sender: TObject; Field: TField);
    procedure qBandConvAfterPost(DataSet: TDataSet);
    procedure TabSituacaoHide(Sender: TObject);
    procedure TabSheet6Show(Sender: TObject);
    procedure DBMemo1KeyPress(Sender: TObject; var Key: Char);
    procedure DSEstadosDataChange(Sender: TObject; Field: TField);
    procedure edtEmprChange(Sender: TObject);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure FormDeactivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QconveniadoBeforePost(DataSet: TDataSet);
    procedure TabSituacaoShow(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    procedure QCadastroAfterRefresh(DataSet: TDataSet);
    procedure QCartoesAfterInsert(DataSet: TDataSet);
    procedure TabOutrasHide(Sender: TObject);
    procedure GridLimPorSegKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridSaldConvCellClick(Column: TColumn);
    procedure CheckBox1Click(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure btnTransferenciaClick(Sender: TObject);
    procedure dbConvIDKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure HabilitarBotaoPerfilDiretor(NomeDoBotao : TButton);
    procedure btnAdicionaBairroClick(Sender: TObject);
    procedure DSCidadesDataChange(Sender: TObject; Field: TField);
    procedure DBGrid1Enter(Sender: TObject);
    procedure QCartoesAfterPost(DataSet: TDataSet);
    procedure btnVerSenhaClick(Sender: TObject);
    procedure DBGrid1ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);




  private
    { Private declarations }
    LogCartoes, LogConvDetail, LogContaC : TLog;
    cred_id_baixa : integer;
    incluindo : Boolean;
    procedure LimpaEdits;
    procedure valida;
    function DigitaNovaSenha:string;
    function PegaLimitePadrao(emp:integer):currency;
    function PegaCartoesID:string;
    procedure PesqLogCartao;
    procedure ValidarCartoes;
    function CartaoValidado:Boolean;
    procedure PesqLogCC;
    procedure GeraLimitPorSeg(Conv: Integer; Seg:Integer; Porcent: Currency; seg_descr:String);
    function ExisteLimSeg(seg_id, conv_id: Integer): Boolean;
    procedure SomarProdutos;
    procedure atualizaCodImpTodasEmpresas;
    procedure acertaCupom;
    procedure buscaConveniado;
    procedure buscaConveniadoPassaId(conv_id : String);
    function CentralizaTexto(Texto:string;Tamanho:Integer=127;Preenchedor:string=' '): String;
    procedure carregaCupom;
    procedure CalcTotPtsFidel;
    procedure MostrarLimiteDiferencial(pageIndex : Integer);
    function IsFormaLimite(empres_id : Integer) : Integer;
    function VerificaEmpresaBloqueadaAtePagamento(empres_id : Integer) : Boolean;
    function VerificaModeloCartaoPorEmpresID(empres_id : Integer) : Integer ;
  public
    { Public declarations }
    procedure carregaimg;
    procedure carregaimgbmp(imgbmp: String);
    function receita_sem_limite(empres_id : Integer) : Boolean;
    procedure atualizaCodImp;
  end;
  TCustomDBGridCracker = class(TCustomDBGrid);

var
  FCadConv  : TFCadConv;
  Porcent,LimiteValor : Currency;
  SavePlace           : TBookmark;


implementation

uses DM, UMenu, DateUtils, UAltContaCor, USaldoMes, UTipos, Types, StrUtils, cartao_util, UDigitaSenha,
  USelTipoImp, ULimiteSeg, UAltLinSenha, UCancelaAutor, TypInfo,
  USQLMount, UValidacao, UComandosQuery, URotinasTexto, FOcorrencia,
  FCadBairro;

{$R *.dfm}
function TFCadConv.receita_sem_limite(empres_id : Integer) : Boolean;
begin
  QReceitaSemLimite.Parameters[0].DataType := ftInteger;
  qReceitaSemLimite.Close;
  QReceitaSemLimite.Parameters[0].value := empres_id;
  qReceitaSemLimite.Open;
  Result := qReceitaSemLimiteRECEITA_SEM_LIMITE.AsString = 'S';
end;

procedure TFCadConv.MostrarLimiteDiferencial(pageIndex : Integer);
var qtdLimites : Integer;
begin
  if dbCbUsaLimiteDif.Visible then begin
    GroupBox3.Visible := (dbCbUsaLimiteDif.Checked) and (pageIndex = 2);
    if GroupBox3.Visible then begin

      if not QCadastro.IsEmpty then
        qtdLimites := DMConexao.ExecuteQuery('SELECT qtd_limites from BANDEIRAS WHERE BAND_ID = (SELECT band_id FROM EMPRESAS WHERE EMPRES_ID = '+QCadastroEMPRES_ID.AsString+')');
      //QCadastro.Edit; //Colocando a Query Qcadastro em modo de edi��o
      //QSituacao_New.SQL.Clear;
      //QCadastroQTD_Limites.AsInteger := DMConexao.AdoQry.Fields[0].Value;


      lblLimite1.Visible   := qtdLimites >= 1;
      EdtLimite1.Visible   := qtdLimites >= 1;

      lblLimite2.Visible   := qtdLimites >= 2;
      EdtLimite2.Visible   := qtdLimites >= 2;
      GroupBox3.Width      := lblLimite2.Left + lblLimite2.Width + 10;

      lblLimite3.Visible   := qtdLimites >= 3;
      EdtLimite3.Visible   := qtdLimites >= 3;
      GroupBox3.Width      := lblLimite3.Left + lblLimite3.Width + 10;

      lblLimite4.Visible   := qtdLimites >= 4;
      EdtLimite4.Visible   := qtdLimites >= 4;
      GroupBox3.Width      := lblLimite4.Left + lblLimite4.Width + 10;
    end;
  end;
end;  

procedure TFCadConv.atualizaCodImp;
var codimp: string; fimCodImp: string;
begin
  DMConexao.Config.Open;
  prVerfEAbreCon(QCartoes);
  if QCartoes.RecordCount > 0 then
  begin
    fimCodImp := Copy(QCartoesCODCARTIMP.AsString, 13, 4);
    QCartoes.Edit;
    QCartoesJAEMITIDO.AsString := 'N';
    if QEmpresaUSA_COD_IMPORTACAO.AsString = 'S' then begin
      if QEmpresausa_novo_cartao.AsString = 'N' then begin
        if QEmpresatipo_credito.AsString = '4' then begin
          repeat
            codimp := RemoveCaracter(gerarCartaoCantinex(DMConexao.Configbin_cantina.AsString));
          until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));
        end
        else begin
          repeat
            codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
          until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));
        end;
      end else begin
        if QEmpresatipo_credito.AsString = '4' then begin
          repeat
            codimp := RemoveCaracter(gerarCartaoCantinex(DMConexao.Configbin_cantina.AsString));
          until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));
        end
        else begin
          repeat
            codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
          until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));
        end;
        QCartoesCVV.AsString := GerarCVV(codimp);
        QCartoesJAEMITIDO_CARTAO_NOVO.AsString := 'S';
        QCartoesDATA_EMISSAO_CARTAO_NOVO.AsDateTime := Now;
      end;
      QCartoesCODCARTIMP.AsString := codimp;
    end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
      QCartoesCODCARTIMP.AsString := QCartoesCODIGO.AsString
    else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
    begin
      if QCartoesTITULAR.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp
      else
        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp(False);
    end
    else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
      QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImpMod1(QCadastroCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QCadastroCHAPA.AsString)
    else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
      QCartoesCODCARTIMP.AsString :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
    if QCartoes.State in [dsEdit] then begin
      if (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '')) then begin
        //if (Crypt('D',QCartoesSENHA.AsString,'BIGCOMPRAS') = fimCodImp) then
        QCartoesSENHA.AsString := Crypt('E',Copy(QCartoesCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
        QCartoesDTALTERACAO.AsDateTime := Now;
        QCartoes.Post;
        DMConexao.ExecuteSql('INSERT INTO CARTOES_HISTORICO SELECT CARTAO_ID, CONV_ID, EMPRES_ID, CODCARTIMP, CVV, SENHA, GETDATE() FROM CARTOES WHERE CARTAO_ID = '  +  QCartoesCARTAO_ID.AsString);
      end;
    end;
  end else
    QCartoes.Close;
  DMConexao.Config.Close;
  MsgInf('Nova via gerada com sucesso!');
end;

procedure TFCadConv.ButBuscaClick(Sender: TObject);
begin
  inherited;
  buscaConveniado;
  
end;

procedure TFCadConv.ButIncluiClick(Sender: TObject);
VAR ultimo_Id: Integer;
begin
  inherited;
  PageControl2.ActivePage := TabSheet1;
  dbEdit2.SetFocus;
 
end;


procedure TFCadConv.DSCartoesStateChange(Sender: TObject);
begin
  ButCancelCartao.Enabled  := (QCartoes.State in [dsEdit,dsInsert]);
  ButGravaCartao.Enabled   := (QCartoes.State in [dsEdit,dsInsert]);
  ButInclui_Cartao.Enabled := (QCartoes.State = dsBrowse) and Incluir ;
  ButApaga_Cartao.Enabled  := (QCartoes.State = dsBrowse) and Excluir and (not QCartoes.IsEmpty);;
  inherited;
end;

procedure TFCadConv.LimpaEdits;
begin
  EdCod.Clear;
  EdBuscaCartao.Clear;
  EdNome.Clear;
  EdCodEmp.Clear;
  EdNomeEmp.Clear;
  EdCartao.Clear;
  EdChapa.Clear;
  DEdataini.Clear;
  DEdatafim.Clear;
  cbbLiberado.ItemIndex := 0;
end;

procedure TFCadConv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F5) and (PageControl1.ActivePageIndex = 2) then begin ButInclui_Cartao.Click; Exit; end;
  if (Key = VK_F6) and (PageControl1.ActivePageIndex = 2) then begin ButApaga_Cartao.Click;  Exit; end;
  inherited;
  if Key = VK_F8 then PageControl1.ActivePage := TabCartoes;
  if Key = VK_F9 then PageControl1.ActivePage := TabFotos;
  if Key = VK_F11 then PageControl1.ActivePage := TabSituacao;
  if Key = VK_F12 then PageControl1.ActivePage := TabContaCorrente;
end;

procedure TFCadConv.TabCartoesShow(Sender: TObject);
begin
  if not (QCadastro.State in [dsEdit, dsInsert]) then
  inherited;
  PageCartoes.TabIndex:= 0;
  if (QCartoes.Parameters.ParamByName('conv_id').Value <> QCadastroCONV_ID.AsInteger) or (not QCartoes.Active) then
  begin
    QCartoes.Close;
    if not QCadastro.IsEmpty then
    begin
      QCartoes.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
      QCartoes.Open;
    end;
  end
  else
    if not QCadastro.IsEmpty then
      QCartoes.Refresh;
end;

procedure TFCadConv.TabSheet2Show(Sender: TObject);
begin
  inherited;
  DBBanco.SetFocus;
end;

procedure TFCadConv.FormCreate(Sender: TObject);
begin
  chavepri := 'conv_id';
  detalhe := 'Conv ID: ';
  DMConexao.Config.Open;
  TabProgDesc.TabVisible:= (DMConexao.ConfigINTEGRA_SISTEMABIG.AsString = 'S');
  if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
    ButAtualizaCodImp.Visible:= True
  else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
    ButAtualizaCodImp.Visible:= True;
  if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
  begin
    //DBEmpresa.LookupDisplay := 'empres_id;fantasia';
    DBEmpresa.LookupDisplay := 'fantasia';
    Label32.Caption:= '&Nome Fantasia da Empresa';
    QCadastroempresa.LookupResultField:= 'fantasia';
  end
  else
  begin
    DBEmpresa.LookupDisplay := 'nome';
    Label32.Caption:= '&Raz�o/Nome Empresa';
    QCadastroempresa.LookupResultField:= 'nome';
    //QCadastroempresa.LookupResultField:= 'empres_id;nome';
  end;
  if DMConexao.ConfigUSA_PROG_DESC.AsString = 'S' then
  begin
    TabProgDesc.TabVisible:= True;
    tabTransacoes.TabVisible:= True;
  end
  else
  begin
    TabProgDesc.TabVisible:= False;
    tabTransacoes.TabVisible:= False;
  end;
  DMConexao.Config.Close;

  if Operador.IsAdmin then
  begin
    btnVerSenha.Visible := True;
    DBCheckBox1.Visible := True;
  end;
  //if Operador.Nome
  if (Operador.ID = 36) or (Operador.IsAdmin) or
    (Operador.ID = 5) or (Operador.ID = 80) or (Operador.ID = 86) or (Operador.ID = 69) or (Operador.ID = 3) or (Operador.ID = 11) or (Operador.ID = 49) or (Operador.ID = 34) or (Operador.ID = 72) or (Operador.ID = 91) or (Operador.ID = 96) or (Operador.ID = 94)
  then
  begin
    DBCheckBox1.Visible := True;
  end;
  inherited;

  LogCartoes := TLog.Create;
  LogCartoes.LogarQuery(QCartoes,'FCadCartoes','Cart�o ID:','FCadCartoes',Operador.Nome,'cartao_id');

  LogConvDetail:= TLog.Create;
  LogConvDetail.LogarQuery(QConvDetail,janela,'Conv ID: ','FCadConv',Operador.Nome,chavepri);

  LogContaC := TLog.Create;
  LogContaC.LogarQuery(Qcontacorrente,'CONTACORRENTE','Autoriza��o: ','Conta Corrente',Operador.Nome,'autorizacao_id');

  QCadastro.Open;
  try
    QEmpresa.Open;
  except
  end;
  QBanco.Open;
  qEstados.Open;
  QCidades.Parameters.ParamByName('ESTADO_ID').Value := 0;
  qCidades.Open;
  QBairros.Parameters.ParamByName('CID_ID').Value := 0;
  QBairros.Open;
  data1.date := date;
  data2.date := date;
  DatainiCartao.Date := Date;
  DatafimCartao.Date := Date;
  FMenu.vCadConv := True;
end;

procedure TFCadConv.FormDestroy(Sender: TObject);
begin
  inherited;
  QEmpresa.Close;
  QCartoes.Close;
  Qcontacorrente.Close;
  QBanco.Close;
  QEMP_DPTO.Close;
end;

procedure TFCadConv.DSCadastroStateChange(Sender: TObject);
begin
  inherited;
  //DSCadastroStateChange(Sender);
  AlteraoLineairdelimiteporSegmento1.Enabled:= Alterar;
  AlteraoLineardeSenha1.Enabled:= Alterar;
  CancelaAutorizao1.Enabled:= Alterar;
  //btn2via.Enabled:= Alterar;
end;

//DONE -O[SIDNEI] -C[ALTERAR STATUS CARTAO LIBERADO]:[FALTA IMPLEMENTAR A ALTERA��O
//DIRETA NO GRID]
procedure TFCadConv.QCartoesBeforePost(DataSet: TDataSet);
var qts : Integer;
    codimpconv: String;
begin
 inherited;
  qts:= DMConexao.ExecuteScalar(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and coalesce(ativo,''N'')=''S'' and coalesce(liberado,''N'')=''S'' and coalesce(titular,''S'')=''S'' and conv_id = '+QCadastroCONV_ID.AsString);
  QCartoesATIVO.AsString := 'S';
  if (qts > 1) then
  begin
    MsgErro('Aten��o � obrigat�rio que o conveniado tenha somente um cart�o de titular');
    QCartoesTITULAR.AsString := 'N';
  end;
  if Trim(QCartoesNOME.AsString) = EmptyStr then
  begin
    MsgInf('Informe o nome do cart�o.');
    JvDBGrid1.SetFocus;
    SysUtils.Abort;
  end;
  if Trim(QCartoesNOME.AsString) = EmptyStr then
  begin
    MsgInf('Informe o nome do cart�o.');
    JvDBGrid1.SetFocus;
    SysUtils.Abort;
  end;
  if (IsTipoCreditoCantinex(QCadastroEMPRES_ID.AsInteger)) and (IsFormaLimite (QCadastroEMPRES_ID.AsInteger) = 2) then
  begin
    if (QCartoesLIMITE_MES.Value = 0) and(QCartoesTITULAR.AsString = 'N') then
      begin
        MsgInf('Informe o limite m�s.');
        JvDBGrid1.SelectedIndex := 6;
        JvDBGrid1.SetFocus;
        SysUtils.Abort;
      end;
  end;
  if ((not QCartoesCODCARTIMP.IsNull) and (QCartoesCODCARTIMP.AsString <> '')) then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('select nome from cartoes where conv_id <> '+QCadastroCONV_ID.AsString+' and codcartimp = '+QuotedStr(QCartoesCODCARTIMP.AsString)+';');
    DMConexao.AdoQry.Open;
    codimpconv:= DMConexao.AdoQry.Fields[0].AsString;
    if (codimpconv <> '') then
    begin
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('DELETE FROM CONVENIADOS WHERE CONV_ID = '+QCadastroCONV_ID.AsString);
      DMConexao.AdoQry.ExecSQL;
      MsgInf('C�digo de importa��o ja em uso para o cart�o: '+sLineBreak+codimpconv+ 'Efetue novamente o cadastro do conveniado!');
      dbEdit2.SetFocus;
      //JvDBGrid1.SetFocus;
      QCartoes.Close;
      QCadastro.Close;
      QCadastro.Open;
      
      SysUtils.Abort;
    end;
  end;
  //QCartoesCARTAO_ID.Value := QCadastroCONV_ID.Value;
  QCartoesEMPRES_ID.AsInteger := QCadastroEMPRES_ID.AsInteger;
  QCartoesNOME.AsString := UpperCase(QCartoesNOME.AsString);
  QCartoesPARENTESCO.AsString := UpperCase(QCartoesPARENTESCO.AsString);
  QCartoesJAEMITIDO.AsString := UpperCase(QCartoesJAEMITIDO.AsString);
  if (QCartoesJAEMITIDO.AsString = '') then
  QCartoesJAEMITIDO.AsString := 'N'
  else if not (QCartoesJAEMITIDO.AsString[1] in ['S','N']) then
    QCartoesJAEMITIDO.AsString := 'N';

  if VerificaModeloCartaoPorEmpresID(QCadastroEMPRES_ID.AsInteger) = 8 then
  begin
     QCartoesLIBERADO.AsString := 'S';
  end
  else begin
    QCartoesLIBERADO.AsString := UpperCase(QCartoesLIBERADO.AsString);
    if (QCartoesLIBERADO.AsString = '') then
      QCartoesLIBERADO.AsString := 'N'
    else if not (QCartoesLIBERADO.AsString[1] in ['S','N','I']) then QCartoesLIBERADO.AsString := 'N';
  end;



  QCartoesTITULAR.AsString := UpperCase(QCartoesTITULAR.AsString);
  if (QCartoesTITULAR.AsString = '') then
    QCartoesTITULAR.AsString := 'S'
  else if not (QCartoesTITULAR.AsString[1] in ['S','N']) then QCartoesTITULAR.AsString := 'N';
  QCartoesCODCARTIMP.AsString:= SoNumero(QCartoesCODCARTIMP.AsString);
  //QCartoesDTALTERACAO.AsDateTime := Now;
  //QCartoesOPERADOR.AsString := Operador.Nome;
  if QCartoesAPAGADO.IsNull then QCartoesAPAGADO.AsString := 'N';
  if QCartoesTITULAR.IsNull then QCartoesTITULAR.AsString := 'N';
  if QCartoesAPAGADO.AsString = 'S' then
    QCartoesDTAPAGADO.AsDateTime := Now;
  if QCartoes.State = dsInsert then
  begin
    if DMConexao.ConfigSENHA_CONV_ID.AsString <> 'S' then
      QCartoesSENHA.AsString := Crypt('E',Copy(QCartoesCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
    //incluindo := True;
  end;
end;

function TFCadConv.IsFormaLimite(empres_id : Integer) : Integer;
begin
  Result := DMConexao.ExecuteScalar('SELECT forma_limite_id FROM empresas WHERE empres_id = '+IntToStr(empres_id)+'');
end;

procedure TFCadConv.Valida;
var mens, chapa : string; focar : TWinControl;
    qtdLimites : Integer;
    limite : string;
begin
  mens := '';
  if not (QCadastro.State in [dsInsert, dsEdit]) then
    Exit;
  if fnVerfCompVazioEmTabSheet('Informe o nome do conveniado.',dbEdit2     )                 then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o n� de chapa/matricula do conveniado.',dbEdtChapa)  then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o limite do conveniado.',dbEdtLimite)                then Abort;
  if fnVerfCompVazioEmTabSheet('Informe a empresa do conveniado.',DBEmpresa)                 then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o grupo do conveniado na empresa.',DBGrupo_conv_emp) then Abort;
  if fnVerfCompVazioEmTabSheet('Chapa Obrigat�ria'  ,dbEdtChapa)                             then Abort;
  if fnVerfCompVazioEmTabSheet('Empresa Obrigat�ria', DBEmpresa)                             then Abort;
  if QCadastroABONO_MES.AsCurrency < 0 then begin
    MsgInf('O valor de Abono n�o pode ser negativo');
    PageControl2.ActivePageIndex := 0;
    dbEdtAbonoMes.SetFocus;
    Abort;
  end;
  if QCadastroSALDO_RENOVACAO.AsCurrency < 0 then begin
    MsgInf('O valor de Saldo de Renova��o n�o pode ser negativo');
    PageControl2.ActivePageIndex := 0;
    dbEdtSaldoRenovacao.SetFocus;
    Abort;
  end;
  if QCadastroUSA_SALDO_DIF.AsString = 'S' then begin
    qtdLimites := DMConexao.ExecuteScalar('select qtd_limites from bandeiras where band_id = ' + QCadastroBAND_ID.AsString);
    if (qtdLimites >= 1) and ((qBandConvLIMITE_1.AsString = '') or (qBandConvLIMITE_1.AsCurrency < 0)) then begin
      MsgInf('Limite 1 deve ser preenchido e com valor maior que 0 (zero).');
      EdtLimite1.SetFocus;
      Abort;
    end;
    limite := fnRemoveCaracters('.',fnRemoveCaracters(',',edtLimite2.Text));
    if (qtdLimites >= 2) and ((qBandConvLIMITE_2.AsString = '') or (qBandConvLIMITE_2.AsCurrency < 0)) then begin
      MsgInf('Limite 2 deve ser preenchido e com valor maior que 0 (zero).');
      EdtLimite2.SetFocus;
      Abort;
    end;
    limite := fnRemoveCaracters('.',fnRemoveCaracters(',',edtLimite3.Text));
    if (qtdLimites >= 3) and ((qBandConvLIMITE_3.AsString = '') or (qBandConvLIMITE_3.AsCurrency < 0)) then begin
      MsgInf('Limite 3 deve ser preenchido e com valor maior que 0 (zero).');
      EdtLimite3.SetFocus;
      Abort;
    end;
    limite := fnRemoveCaracters('.',fnRemoveCaracters(',',edtLimite4.Text));
    if (qtdLimites >= 4) and ((qBandConvLIMITE_4.AsString = '') or (qBandConvLIMITE_4.AsCurrency <= 0)) then begin
      MsgInf('Limite 4 deve ser preenchido e com valor maior que 0 (zero).');
      EdtLimite4.SetFocus;
      Abort;
    end;
  end;
  if DBDateEdit2.Date = 0 then QCadastroDATA_DEMISSAO.Clear;
  if Trim(dbEdtEmail.Text) <> '' then
    if not fnIsEmail(dbEdtEmail.Text) then
      begin
      MsgErro('Email inv�lido!');
      dbEdtEmail.SetFocus;
      Abort;
      end;
  if length(QCadastro.FieldByName('CPF').AsString) > 0 then
  begin
    QCadastro.FieldByName('CPF').AsString:= SoNumero(Trim(QCadastro.FieldByName('CPF').AsString));
    if not ValidaCPF(DBEdit12.Text) then
    begin
      MsgErro('CPF Inv�lido!');
      DBEdit12.SetFocus;
      Exit;
    end;
  end;
  if QCadastro.State = dsEdit then
  begin

    if QCadastroEMPRES_ID.NewValue <> QCadastroEMPRES_ID.AsInteger then
    begin
      if DMConexao.ExecuteScalar('select grupo_conv_emp_id from grupo_conv_emp where grupo_conv_emp_id = '+QCadastroGRUPO_CONV_EMP.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString,0) = 0 then
      begin
        mens := 'Selecione um grupo v�lido para essa empresa!';
        focar:= DBGrupo_conv_emp;
      end;
    end;

  end;
  if QCadastro.State in [dsInsert] then
  begin
    //chapa:= DMConexao.ExecuteScalar('select titular from conveniados where chapa = '+QCadastroCHAPA.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString,'');
    DMConexao.AdoQry.SQL.Text := 'select titular from conveniados where chapa = '+QCadastroCHAPA.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString;
    DMConexao.AdoQry.Open;
    chapa:=  DMConexao.AdoQry.Fields[0].AsString;

    if (chapa <> '') then
    begin
      mens := 'Chapa ja em uso nessa empresa para o conveniado: '+sLineBreak+chapa;
      focar := dbEdtChapa;
    end;

    if(QCadastroEMPRES_ID.AsInteger = 455) then
    begin
      if(QCadastroDT_NASCIMENTO.AsDateTime = 0) then
      begin
        MsgInf('A data de nascimento � obrigat�ria!');
        DBDateEdit3.SetFocus;
        Abort;
      end;
    end;
  end;
  if QCadastroBANCO.IsNull then QCadastroBANCO.AsInteger := 0;
  if QCadastroCONTRATO.AsString = '' then QCadastroCONTRATO.AsInteger:= QCadastroCONV_ID.AsInteger;
  if mens <> '' then
  begin
    msginf(mens);
    PageControl2.ActivePageIndex := 0;
    focar.SetFocus;
    SysUtils.Abort;
  end;
end;

{procedure TFCadConv.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
                                          '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;
  if QCadastro.IsEmpty then Self.TextStatus := '';
  if not QCadastro.IsEmpty then
  begin
    QConvDetail.Close;
    QConvDetail.Sql.Text := ' select * from CONV_DETAIL  where conv_id = '+QCadastroCONV_ID.AsString;
    QConvDetail.Open;
    QGrupo_conv_emp.Close;
    QGrupo_conv_emp.Parameters.ParamByName('empres_id').Value := QCadastroEMPRES_ID.AsInteger;
    QGrupo_conv_emp.Open;
    DBEmpresa.DataField := '';
    DBEmpresa.KeyValue := QCadastroEMPRES_ID.Value;
    DBEmpresa.DataField := 'empres_id';
    //*************************************************************
  end;
  QCartoes.Close;
  Qcontacorrente.Close;
  QTodasCompras.Close;
  if QCadastroCIDADE.AsString <> '' then
    dbLkpCidades.KeyValue := UpperCase(QCadastroCIDADE.AsString);
end;  }

procedure TFCadConv.ButApagaClick(Sender: TObject);
begin
  if not QCadastro.IsEmpty then
  begin
    DMConexao.Config.Open;
    if DMConexao.Config.FieldByName('USA_NOVO_FECHAMENTO').AsString = 'S' then
    begin
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.Sql.Text := 'Select count(autorizacao_id) as num from contacorrente where baixa_conveniado <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString;
      DMConexao.AdoQry.Open;
    end
    else
    begin
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.Sql.Text := 'Select count(autorizacao_id) as num from contacorrente where conv_id = '+QCadastroCONV_ID.AsString;
      DMConexao.AdoQry.Open;
    end;
    DMConexao.Config.Close;
    if DMConexao.AdoQry.FieldByName('num').AsInteger > 0 then
    begin
       Application.MessageBox(PChar('N�o foi poss�vel executar esta opera��o!'+#13+'Existe movimenta��o em aberto na contacorrente deste conveniado.'),'Aten��o',MB_ICONINFORMATION+MB_OK);
       exit;
    end;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Text := 'Select count(cartao_id) as num from cartoes where coalesce(apagado,''N'') <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString;
    DMConexao.AdoQry.Open;
    if DMConexao.AdoQry.FieldByName('num').AsInteger > 0 then
    begin
      //Application.MessageBox(PChar('N�o foi poss�vel executar esta opera��o!'+#13+'Existem cart�es cadastrados a este conveniado.'),'Aten��o',MB_ICONINFORMATION+MB_OK);
      if MsgSimNao('Existem cart�es cadastrados a este conveniado.'+sLineBreak+'Deseja excluir o titular e todos os seus cart�es?') then
      begin
        QCartoes.Close;
        if not QCadastro.IsEmpty then
        begin
          QCartoes.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
          QCartoes.Open;
          QCartoes.First;
          while not QCartoes.Eof do
          begin
            QCartoes.Edit;
            if QCadastroAPAGADO.AsString = 'N' then
            begin
              QCartoesAPAGADO.AsString:= 'S';
              QCartoesDTAPAGADO.AsDateTime:= Now;
              DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'AUTOMATICO', 'EXCLUS�O CONVENIADO');
              QCartoes.Post;
            end;
            QCartoes.Next;
          end;
          QCartoes.Close;
        end;
      end
      else
      begin
        exit;
      end;
    end;
    inherited;
    DMConexao.AdoQry.Close;
  end;
end;

procedure TFCadConv.JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  try
    if Pos(Field.FieldName,QCartoes.Sort) > 0 then // Modificado Sidnei Sanches, anteriormete estava utilizado o sortedFields do componente Zeus
    begin
      if Pos(' Desc',QCartoes.Sort) > 0 then
        QCartoes.Sort := Field.FieldName
      else
        QCartoes.Sort := Field.FieldName+' Desc';
    end
    else
      QCartoes.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadConv.JvDBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  try
    if Pos(Field.FieldName,Qcontacorrente.Sort) > 0 then
    begin
      if Pos(' Desc',Qcontacorrente.Sort) > 0 then
        Qcontacorrente.Sort := Field.FieldName
      else
        Qcontacorrente.Sort := Field.FieldName+' Desc';
    end
    else
      Qcontacorrente.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadConv.JvDBGrid3TitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  try
    if Pos(Field.FieldName,QTodasCompras.Sort) > 0 then
    begin
      if Pos(' Desc',QTodasCompras.Sort) > 0 then
        QTodasCompras.Sort := Field.FieldName
      else
        QTodasCompras.Sort := Field.FieldName+' Desc';
    end
    else
      QTodasCompras.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadConv.Exportarparaoexcel2Click(Sender: TObject);
begin
  Grade_to_PlanilhaExcel(JvDBGrid1);
end;

procedure TFCadConv.Exportarparaoexcel3Click(Sender: TObject);
begin
  Grade_to_PlanilhaExcel(JvDBGrid2);
end;

procedure TFCadConv.Exportarparaoexcel4Click(Sender: TObject);
begin
  Grade_to_PlanilhaExcel(JvDBGrid3);
end;


procedure TFCadConv.TabFichaExit(Sender: TObject);
begin
  inherited;
  PageControl2.ActivePageIndex := 0;
  //QEMP_DPTO.Close;
  //QEMP_DPTO.Open;
end;

procedure TFCadConv.DBEdit11KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if key = #13 then PageControl2.ActivePageIndex := 1;
end;

procedure TFCadConv.QConvDetailBeforePost(DataSet: TDataSet);
begin
  QConvDetailCONV_ID.AsInteger := QCadastroCONV_ID.AsInteger;
  QConvDetailESTADO_CIVIL.AsString:= DBComboBox2.Text;
  DMConexao.Config.Open;
  if DMConexao.ConfigUSA_NOVO_FECHAMENTO.AsString = 'S' then
  begin
    if QConvDetail.State = dsInsert then
    begin
      if DBDateEdit2.Date > 0 then
      begin
        MsgInf('As autoriza��es com fechamentos posteriores ser�o transportadas para o pr�ximo fechamento em aberto!');
        DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select datafecha from '+
        ' get_prox_fecha_aberto(current_timestamp,'+QCadastroCONV_ID.AsString+
        ',0)) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString);
        QConvDetailSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Devedor',FormatDinBR(0),FormatDinBR(QConvDetailSALDO_DEVEDOR.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
        QConvDetailSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Dev. Fat.',FormatDinBR(0),FormatDinBR(QConvDetailSALDO_DEVEDOR_FAT.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
      end;
    end;
    if (QConvDetail.State = dsEdit) and (QConvDetailDATA_DEMISSAO.OldValue <> QConvDetailDATA_DEMISSAO.Value) then
    begin
      if not QConvDetailDATA_DEMISSAO.IsNull then
      begin
        QConvDetailSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString,0);
        QConvDetailSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString,0);
        MsgInf('As autoriza��es com fechamentos posteriores ser�o transportadas para o pr�ximo fechamento em aberto!');
        DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select datafecha from '+
        ' get_prox_fecha_aberto(current_timestamp,'+QCadastroCONV_ID.AsString+
        ',0)) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString);
      end
      else
      begin
        QConvDetailSALDO_DEVEDOR.AsCurrency:= 0;
        QConvDetailSALDO_DEVEDOR_FAT.AsCurrency:= 0;
      end;
    end;
  end;
  DMConexao.Config.Close;
  inherited;
end;

procedure TFCadConv.DSConvDetailStateChange(Sender: TObject);
begin
  inherited;
  if QConvDetail.State in [dsInsert,dsEdit] then
  begin
    //MsgInf('Query: '+QConvDetailESTADO_CIVIL.AsString+sLineBreak+'Combo: '+DBComboBox2.Text);
    if DBComboBox2.Text <> '' then QConvDetailESTADO_CIVIL.AsString:= DBComboBox2.Text;
    //if DBDateEdit4.Date > 0 then QConvDetailFIM_CONTRATO.AsDateTime:= DBDateEdit4.Date;
    //if DBDateEdit2.Date > 0 then QConvDetailDATA_DEMISSAO.AsDateTime:= DBDateEdit2.Date;
    //if DBDateEdit1.Date > 0 then QConvDetailDATA_ADMISSAO.AsDateTime:= DBDateEdit1.Date;
    //MsgInf('Query: '+QConvDetailESTADO_CIVIL.AsString+sLineBreak+'Combo: '+DBComboBox2.Text);
    prVerfEAbreCon(QCadastro);
    QCadastro.Edit;
    //MsgInf('Query: '+QConvDetailESTADO_CIVIL.AsString+sLineBreak+'Combo: '+DBComboBox2.Text);
  end;
end;
//Esta fun��o em por fim tratar o retorno dos campos num�ricos
//Caso o usu�rio n�o atribua valor ao campo o retorno deve ser 0
function VerificaConsistenciaNum(Edit: TDBEdit):Integer;
begin
  if(Edit.Text = '')then
    result := 0
    else result := StrToInt(Edit.Text);
end;

procedure TFCadConv.ButGravaClick(Sender: TObject);
var Item,teste : String;
begin
  if DBDateEdit2.Date = 0 then
  QCadastroDATA_DEMISSAO.IsNull;
  inherited;

end;

procedure TFCadConv.ButCancelaClick(Sender: TObject);
begin
  inherited;
    if QConvDetail.State in [dsInsert,dsEdit] then
      QConvDetail.Cancel;
end;

procedure TFCadConv.TabSheet3Show(Sender: TObject);
begin
  inherited;
  DBEdit28.SetFocus;
  if not QCadastro.FieldByName('DATA_DEMISSAO').IsNull then
  begin
    Label91.Visible:= True;
    DBEdit56.Visible:= True;
    Label92.Visible:= True;
    DBEdit57.Visible:= True;
  end
  else
  begin
    Label91.Visible:= False;
    DBEdit56.Visible:= False;
    Label92.Visible:= False;
    DBEdit57.Visible:= False;
  end;
  dbCbUsaLimiteDif.Visible := not (QCadastroBAND_ID.AsInteger = 999);
  if dbCbUsaLimiteDif.Visible then
    MostrarLimiteDiferencial(TTabSheet(Sender).PageIndex);
end;

procedure TFCadConv.DBEdit22KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
if key = #13 then PageControl2.ActivePageIndex := 2;
end;

procedure TFCadConv.DBComboBox2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if key = #13 then DBEdit34.SetFocus;
end;

procedure TFCadConv.JvDBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key  of
    VK_LEFT  : JvDBGrid2.Perform(WM_HSCROLL,0,0);
    VK_RIGHT : JvDBGrid2.Perform(WM_HSCROLL,1,0);
  end;
  if Key in [vk_left,vk_right] then Key := 0;
end;

procedure TFCadConv.JvDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var Grid : TDBGrid;
  L, R: Integer;
begin
  Grid := TDBGrid(Sender);
  Grid.Canvas.Font.Color := clBlack;
  with THackDBgrid(Sender) do
  begin
    if DataLink.ActiveRecord = Row -1 then
    begin
      Grid.Canvas.Brush.Color:= clBlack;
      Grid.Canvas.FillRect(Rect);
      if odd(Grid.DataSource.DataSet.RecNo) then
      begin
        if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'N')) then
          Canvas.Brush.Color:= $00E2FEFD
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'N') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00FDECF0
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00D2D2FF
        else
          Canvas.Brush.Color:= clWhite
      end
      else
      begin
        if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'N')) then
          Canvas.Brush.Color:= $00E2FEFD
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'N') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00FDECF0
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00D2D2FF
        else
          Canvas.Brush.Color:= $0EEEEEE;
      end
    end
    else
    begin
      if odd(Grid.DataSource.DataSet.RecNo) then
      begin
        if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'N')) then
          Canvas.Brush.Color:= $00E2FEFD
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'N') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00FDECF0
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00D2D2FF
        else
          Canvas.Brush.Color:= clWhite
      end
      else
      begin
        if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'N')) then
          Canvas.Brush.Color:= $00E2FEFD
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'N') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00FDECF0
        else if ((Grid.DataSource.DataSet.FindField('BAIXA_CONVENIADO').AsString = 'S') and (Grid.DataSource.DataSet.FindField('BAIXA_CREDENCIADO').AsString = 'S')) then
          Canvas.Brush.Color:= $00D2D2FF
        else
          Canvas.Brush.Color:= $0EEEEEE;
      end
    end;
  end;
  R:= Rect.Right; L:= Rect.Left;
  if Column.Index = 0 then L := L + 1;
  if Column.Index = Grid.Columns.Count -1 then R := R - 1;
  Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
  Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
end;

procedure TFCadConv.ButInclui_CartaoClick(Sender: TObject);
begin
  if QCadastro.State in [dsEdit, dsInsert] then
    begin
    msginf('Voc� j� est� em um processo de inclus�o! Por favor, termine esta inclus�o antes de iniciar outra!');
    Exit;
    end;
  //if (QCartoes.Parameters[0].Value = '') or (QCartoes.Parameters[0].Value = null) then
  if(QCartoes.Parameters[0].Value = null) then
    begin
    msginf('Um conveniado deve estar selecionado para inserir o registro!');
    Abort;
    end;
  if QCartoes.Active = False then
  QCartoes.Open;
  QCartoes.Append;
  QCartoes.FieldByName('VIA').AsInteger:= 1;
  inherited;
end;

procedure TFCadConv.TabHistCartoesHide(Sender: TObject);
begin
inherited;
  QHistCartoes.Close;
end;

procedure TFCadConv.TabHistCartoesShow(Sender: TObject);
begin
  inherited;
  DatainiCartao.SetFocus;
end;

procedure TFCadConv.PesqLogCartao;
var dataIni, dataFim : string;
begin
  DMConexao.AdoQry.Close;
  QHistCartoes.Close;
  CBCamposHistCartao.Clear;
  if not QCartoes.IsEmpty then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Clear;
    DMConexao.AdoQry.Sql.Add(' Select distinct CADASTRO+''.''+CAMPO from logs ');
    dataIni := FormatDateTime('dd/mm/yyyy 00:00:00',DatainiCartao.Date);
    dataFim := FormatDateTime('dd/mm/yyyy 23:59:59',DatafimCartao.Date);
    DMConexao.AdoQry.Sql.Add(' where data_hora between '+QuotedStr(dataIni)+' and '+QuotedStr(dataFim));
    DMConexao.AdoQry.Sql.Add(' and ID in ('+PegaCartoesID+')');
    DMConexao.AdoQry.Sql.Add(' and JANELA = '+ QuotedStr('FCadCartoes'));
    DMConexao.AdoQry.Sql.Add(' order by 1');
    DMConexao.AdoQry.Sql.Text;//mostra debug.
    DMConexao.AdoQry.Open;
    DMConexao.AdoQry.First;
    CBCamposHistCartao.Items.Add('Todos os Campos');
    while not DMConexao.AdoQry.Eof do
    begin
      CBCamposHistCartao.Items.Add(DMConexao.AdoQry.Fields[0].AsString);
      DMConexao.AdoQry.Next;
    end;
    DMConexao.AdoQry.Close;
    CBCamposHistCartao.ItemIndex := 0;
  end;
end;


function TFCadConv.PegaCartoesID: string;
var  marka : TBookmark;
begin
  Result := '';
  QCartoes.DisableControls;
  marka := QCartoes.GetBookmark;
  QCartoes.first;
  while not qcartoes.eof do
  begin
    Result := Result + ',' + QCartoesCARTAO_ID.AsString;
    qcartoes.next;
  end;
  delete(Result,1,1);
  qcartoes.GotoBookmark(marka);
  qcartoes.FreeBookmark(marka);
  qcartoes.EnableControls;
end;

procedure TFCadConv.DatainiCartaoExit(Sender: TObject);
begin
  inherited;
  PesqLogCartao;
end;

procedure TFCadConv.GridHistoricoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited GridHistoricoTitleBtnClick(Sender,Acol,Field);
end;

procedure TFCadConv.QCartoesPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  inherited;
  If Pos('Aten��o, este c�digo de cart�o j� est� sendo usado por outro conveniado.',E.Message) > 0 then
  begin
    Application.MessageBox('Aten��o, este c�digo de cart�o j� est� sendo usado por outro conveniado.'+#13+'Esta altera��o n�o foi gravada.','Erro',MB_ICONERROR);
    Action := daAbort;
  end;
end;

procedure TFCadConv.GridSaldConvDrawColumnCell(Sender: TObject; // Sidnei Sanches
 const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
  var R : integer;
begin
  inherited;
  with TCustomDBGridCracker(Sender) do
  begin
    if DataLink.ActiveRecord = Row - 1 then
    begin
      GridSaldConv.Canvas.Brush.Color := clBlack;
      GridSaldConv.Canvas.FillRect(Rect);
      Canvas.Brush.Color := $00BFFFFF;
    end
    else if ((QSaldoConvFECHAMENTO.AsDateTime > Date) and (QSaldoConvFECHAMENTO.AsDateTime <= date + DaysInMonth(date))) then
    begin
      GridSaldConv.Canvas.Brush.Color:= $00F5EFE2;
      GridSaldConv.Canvas.FillRect(Rect);
    end
    else
    begin
      GridSaldConv.Canvas.Brush.Color:= clWhite;
      GridSaldConv.Canvas.FillRect(Rect);
    end;
  end;
  GridSaldConv.Canvas.Font.Color  := clBlack;
  R := Rect.Right;
  if Column.Index = GridSaldConv.Columns.Count - 1 then R := R-1;
  GridSaldConv.Canvas.FillRect(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1));
  GridSaldConv.DefaultDrawColumnCell(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1), DataCol, Column, State);

end;

procedure TFCadConv.GridSaldoCartaoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
  var R : integer;  ativo : boolean;
begin
  inherited;
  with TCustomDBGridCracker(Sender) do
  begin
    ativo := False;
    if DataLink.ActiveRecord = Row - 1 then
    begin
      GridSaldoCartao.Canvas.Brush.Color := clBlack;
      GridSaldoCartao.Canvas.FillRect(Rect);
      ativo := True;
    end;
    If QSaldoCartaoFECHAMENTO.AsDateTime = QSaldoConvFECHAMENTO.AsDateTime then
      GridSaldoCartao.Canvas.Brush.Color := $00BFFFFF
    else if ((QSaldoCartaoFECHAMENTO.AsDateTime > Date) and (QSaldoCartaoFECHAMENTO.AsDateTime <= date + DaysInMonth(date))) then
      GridSaldoCartao.Canvas.Brush.Color:= $00F5EFE2
    else
      GridSaldoCartao.Canvas.Brush.Color:= clWhite;
    if not ativo then
      GridSaldoCartao.Canvas.FillRect(Rect);
  end;
  GridSaldoCartao.Canvas.Font.Color  := clBlack;
  R := Rect.Right;
  if Column.Index = GridSaldConv.Columns.Count - 1 then R := R-1;
  GridSaldoCartao.Canvas.FillRect(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1));
  GridSaldoCartao.DefaultDrawColumnCell(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1), DataCol, Column, State);

  end;


procedure TFCadConv.QSaldoConvAfterScroll(DataSet: TDataSet);
begin
  inherited;
  {GridSaldoCartao.Repaint;
  if not QSaldoConv.Eof then begin
    QSaldoConv
  end;   }
end;

//comentado Sidnei Sanches
procedure TFCadConv.TabSaldosShow(Sender: TObject);
begin
  inherited;
  QSaldoConv.Close;
  QSaldoCartao.Close;
  //if not QCadastro.IsEmpty then
  //begin
    //if not QCartoes.Active then TabCartoesShow(nil);
    Screen.Cursor := crHourGlass;
    QSaldoConv.Parameters[0].Value   := QCadastroCONV_ID.AsInteger;
    QSaldoConv.Parameters[1].Value   := null;
    QSaldoConv.Parameters[2].Value   := null;
    QSaldoConv.Open;
    Application.ProcessMessages;
    QSaldoCartao.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
    QSaldoCartao.Parameters[1].Value := null;
    QSaldoCartao.Parameters[2].Value := null;
    QSaldoCartao.Open;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
  //end;
end;

procedure TFCadConv.GridSaldoCar(Column: TColumn);
begin
   DMConexao.SortZQuery(Column.Field.DataSet,Column.FieldName);
end;


procedure TFCadConv.DBEmpresaExit(Sender: TObject);
begin
  inherited;
  if QCadastroEMPRES_ID.AsInteger <> QGrupo_conv_empGRUPO_CONV_EMP_ID.AsInteger then
  begin
    QGrupo_conv_emp.Close;
    QEMP_DPTO.Close;
    QGrupo_conv_emp.Parameters.ParamByName('empres_id').Value := QCadastroEMPRES_ID.AsInteger;
    QEMP_DPTO.Parameters.ParamByName('EMPRES_ID').Value       := QCadastroEMPRES_ID.AsInteger;
    QGrupo_conv_emp.Open;
    QEMP_DPTO.Open;
    if QGrupo_conv_emp.IsEmpty then
      Application.MessageBox('Essa empresa n�o possu� grupos cadastrados, por favor cadastre algum grupo.','Aten��o',MB_ICONINFORMATION+MB_OK)
    else
      if QCadastro.State = dsInsert then
        QCadastroGRUPO_CONV_EMP.AsInteger := QGrupo_conv_empGRUPO_CONV_EMP_ID.AsInteger;

  end;
  if QCadastroLIMITE_MES.AsCurrency = 0 then
    QCadastroLIMITE_MES.AsCurrency:= PegaLimitePadrao(StrToInt(DBEmpresa.KeyValue));
end;

procedure TFCadConv.QCadastroPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
  var mensagem : string;
begin
  inherited;
  if Pos('O cpf',E.Message) > 0 then
  begin
    mensagem := Copy(E.Message,Pos('O cpf',E.Message),Pos('#',E.Message)-Pos('O cpf',E.Message));
    msgInf(mensagem);
    Action := daAbort;
  end;
end;

procedure TFCadConv.carregaimgbmp(imgbmp: String);
begin
  If QFotosFOTO.IsNull then
  begin
    DBFoto.Picture.Bitmap.LoadFromFile(ExtractFilePath(Application.ExeName)+'imagemndisp.bmp');
    exit;
  end;
  DBFoto.Picture.Bitmap.LoadFromFile(imgbmp);
end;


procedure TFCadConv.carregaimg;
Var
  Img: TJPEGImage;
  Mem: TMemoryStream;
begin

end;


procedure TFCadConv.Button4Click(Sender: TObject);
Var
  Imagem: TFileStream;
begin

end;

procedure TFCadConv.Button6Click(Sender: TObject);
begin
  inherited;
  if fnVerfCampoVazio('Conv�nio deve ser selecionado antes de tentar deletar a foto!',QCadastroCONV_ID) then Abort;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('delete from fotos_conveniados where conv_id = '+QCadastroCONV_ID.AsString);
  DMConexao.AdoQry.ExecSQL;
  DMConexao.AdoQry.Close;
  QFotos.Refresh;
  DBFoto.Picture.Bitmap.LoadFromFile(ExtractFilePath(Application.ExeName)+'imagemndisp.bmp');
end;

procedure TFCadConv.QFotosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  carregaimg;
end;

procedure TFCadConv.qContaCorrenteBeforePost(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('DEBITO').IsNull then
    DataSet.FieldValues['DEBITO'] := 0;
  if DataSet.FieldByName('CREDITO').IsNull then
    DataSet.FieldValues['CREDITO'] := 0;
end;

procedure TFCadConv.ValidarCartoes;
var qts, qtdAtivos: integer;
begin
  if (not QCadastro.IsEmpty) then
  begin
    {if (not QCartoes.IsEmpty) and (not QCartoes.Locate('titular','S',[])) and (QCartoes.State <> dsInsert)  then
    begin
      PageControl1.ActivePage := TabCartoes;
      PageCartoes.ActivePageIndex := 0;
      raise Exception.Create('Aten��o � obrigat�rio que o conveniado tenha um cartao de titular');
    end;}
    qts       := DMConexao.ExecuteScalar(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and coalesce(ativo,''N'')=''S'' and coalsesce(liberado,''N'')=''S'' and coalesce(titular,''S'')=''S'' and conv_id = '+QCadastroCONV_ID.AsString,0);
    qtdAtivos := DMConexao.ExecuteScalar(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and coalesce(ativo,''N'')=''S'' and coalesce(liberado,''N'')=''S'' and conv_id = '+QCadastroCONV_ID.AsString,0);
    if qts > 1 then
    begin
      PageControl1.ActivePage := TabCartoes;
      PageCartoes.ActivePageIndex := 0;
      raise Exception.Create('Aten��o � obrigat�rio que o conveniado tenha somente um cart�o de titular');
    end
    else if (qts = 0) and (qtdAtivos > 0) then
    begin
      PageControl1.ActivePage := TabCartoes;
      PageCartoes.ActivePageIndex := 0;
      raise Exception.Create('Aten��o � obrigat�rio que o conveniado tenha um cart�o de titular');
    end;
  end;
end;

procedure TFCadConv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LogCartoes.Free;
  LogConvDetail.Free;
  LogContaC.Free;
  inherited;                       
  QLimSeg.Close;
  FMenu.vCadConv := False;
   FormDestroy(FCadConv);
end;

procedure TFCadConv.TabCChistShow(Sender: TObject);
begin
  inherited;
  dataini1.date := date;
  datafin1.date := date;
  dataini1.SetFocus;
end;

procedure TFCadConv.PesqLogCC;
var dataIni, dataFim : string;
begin
  DMConexao.AdoQry.Close;
  QHistCartoes.Close;
  DBCampoCC.Clear;
  if not Qcontacorrente.IsEmpty then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Clear;
    DMConexao.AdoQry.Sql.Add(' Select distinct CADASTRO+''.''+CAMPO from logs ');
    dataIni := FormatDateTime('dd/mm/yyyy 00:00:00',dataini1.Date);
    dataFim := FormatDateTime('dd/mm/yyyy 23:59:59',datafin1.Date);
    DMConexao.AdoQry.Sql.Add(' where data_hora between '+QuotedStr(dataIni)+' and '+QuotedStr(dataFim));
    DMConexao.AdoQry.Sql.Add(' and ID = '+QcontacorrenteAUTORIZACAO_ID.AsString);
    DMConexao.AdoQry.Sql.Add(' and JANELA = '+QuotedStr('CONTACORRENTE'));
    DMConexao.AdoQry.Sql.Add('  order by 1');
    DMConexao.AdoQry.Sql.Text;//mostra debug.
    DMConexao.AdoQry.Open;
    DMConexao.AdoQry.First;
    DBCampoCC.Items.Add('Todos os Campos');
    while not DMConexao.AdoQry.Eof do
    begin
      DBCampoCC.Items.Add(DMConexao.AdoQry.Fields[0].AsString);
      DMConexao.AdoQry.Next;
    end;
    DMConexao.AdoQry.Close;
    DBCampoCC.ItemIndex := 0;
  end;
end;

procedure TFCadConv.TabContaCorrenteShow(Sender: TObject);
begin
  inherited;
  PageControl3.ActivePageIndex := 0;
  QHistorico.Close;
  if QCadastro.IsEmpty then
  begin
    Qcontacorrente.Close;
  end
  else
  begin
    if (QContaCorrente.Parameters[0].Value <> QCadastroCONV_ID.AsInteger) or (not QContacorrente.Active) then
    begin
      QContaCorrente.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
      QContaCorrente.Open;
    end
    else
    begin
      Qcontacorrente.Requery();
    end;
  end;
  JvDBGrid2.SetFocus;
end;

procedure TFCadConv.datafin1Exit(Sender: TObject);
begin
  inherited;
  PesqLogCC;
end;

procedure TFCadConv.TabCCResize(Sender: TObject);
begin
  inherited;
  if not Qcontacorrente.IsEmpty then
    Qcontacorrente.Refresh;
end;

procedure TFCadConv.JvDBGrid3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key  of
    VK_LEFT : JvDBGrid3.Perform(WM_HSCROLL,0,0);
    VK_RIGHT : JvDBGrid3.Perform(WM_HSCROLL,1,0);
  end;
  if Key in [vk_left,vk_right] then Key := 0;
end;

procedure TFCadConv.ButAtualizaCodImpClick(Sender: TObject);
begin
  inherited;
  atualizaCodImp;
end;

function TFCadConv.PegaLimitePadrao(emp: integer): currency;
begin
  DMConexao.Query2.Close;
  DMConexao.Query2.SQL.Text:= ' select coalesce(limite_padrao,0) as limite_padrao from empresas where empres_id = '+IntToStr(emp);
  DMConexao.Query2.Open;
  if DMConexao.Query2.Fields[0].AsCurrency > 0 then
    Result:= DMConexao.Query2.Fields[0].AsCurrency
  else
    Result:= 0;
  DMConexao.Query2.Close;
end;

function TFCadConv.DigitaNovaSenha: string;
var senha : String;
begin
  FDigitaSenha := TFDigitaSenha.Create(self);
  FDigitaSenha.ShowModal;
  if FDigitaSenha.ModalResult = mrOk then
  begin
    senha := FDigitaSenha.edSenha.Text;
    Result:= senha;
  end
  else
    Result:= '';
  FDigitaSenha.Free;
end;


procedure TFCadConv.QLimSegAfterPost(DataSet: TDataSet);
var Sql, sqlQuery, vvelho, vnovo, convert: String;
    difPorcent : Double;
begin
  inherited;
  if Porcent > 100 then
  begin
    MsgInf('Valor de porcentagem inv�lida!');
    QLimSeg.Requery();
    Abort;
  end;

  if LimiteValor > QLimSeglimite_cem_por_cento.AsCurrency then
  begin
    MsgInf('Valor maior do que o limite permitido'+#13+'Insira um valor at� R$: '+QLimSeglimite_cem_por_cento.AsString);
    QLimSeg.Requery();
    Abort;
  end;

  if(QLimSegLimite.AsCurrency <> LimiteValor)then
  begin
    difPorcent := ((QLimSeglimite_cem_por_cento.AsCurrency - LimiteValor)*100)/QLimSeglimite_cem_por_cento.AsCurrency;
    Porcent    := 100 - difPorcent;
  end;
  if ((QLimSegporcent.Value <> Porcent) and  (QLimSegporcent.Value = 100)) then
  begin
    DecimalSeparator := '.';
    Sql := 'Insert into conv_lim_seg(conv_id,seg_id,porcent) values '+
           '('+QCadastroCONV_ID.AsString+','+QLimSegseg_id.AsString+','+ FloatToStr(Porcent)+')';
  end

  else if ((QLimSegporcent.Value <> 100) and (Porcent <> 100)) then
  begin
    Sql := 'Update conv_lim_seg set porcent = '+FloatToStr(Porcent)+' WHERE conv_id = '+
           ''+QCadastroCONV_ID.AsString+' and seg_id = '+QLimSegseg_id.AsString;
  end

  else
  begin
    Sql := 'Delete from conv_lim_seg WHERE conv_id = '+QCadastroCONV_ID.AsString+''+
           ' and seg_id = '+QLimSegseg_id.AsString+'';
  end;


  DMConexao.ExecuteSql(Sql);
  QLimSeg.Requery();
  QLimSeg.GotoBookmark(SavePlace);
  QLimSeg.FreeBookmark(SavePlace);
end;

function TFCadConv.ExisteLimSeg(seg_id, conv_id: Integer):Boolean;
begin
  if DMConexao.ExecuteScalar('select conv_id from conv_lim_seg where seg_id = '+IntToStr(seg_id)+
    ' and conv_id = '+IntToStr(conv_id),0) = 0 then
  begin
    Result:= false;
  end
  else
  begin
    Result:= true;
  end;
end;

procedure TFCadConv.AlteraoLineairdelimiteporSegmento1Click(
  Sender: TObject);
var seg_id: Integer;
  valor: Currency; seg_descr:String;
begin
  inherited;
  FLimiteSeg := TFLimiteSeg.Create(self);
  FLimiteSeg.ShowModal;
  if FLimiteSeg.ModalResult = mrOk then
  begin
    QCadastro.First;
    seg_id := FLimiteSeg.Segmento.KeyValue;
    valor := FLimiteSeg.edvalor.Value;
    seg_descr:= FLimiteSeg.seg_nome;
    while not QCadastro.Eof do
    begin
      GeraLimitPorSeg(QCadastroCONV_ID.AsInteger,seg_id,valor,seg_descr);
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  QCadastro.First;
  FLimiteSeg.Free;
end;

procedure TFCadConv.GeraLimitPorSeg(Conv:Integer;Seg:Integer;Porcent:Currency;seg_descr:String);
var Sql, sqlQuery, vvelho, vnovo: String;
  vrvelho: Currency;
begin
  vrvelho:= DMConexao.ExecuteScalar('select porcent from conv_lim_seg where conv_id = '+IntToStr(Conv)+' and seg_id = '+IntToStr(Seg),0);
  if vrvelho = 0 then
  begin
    if Porcent > 100 then
    begin
      MsgInf('Valor de porcentagem inv�lida!');
      Exit;
    end;
    sql :=  'insert into CONV_LIM_SEG (SEG_ID,CONV_ID,PORCENT) ';
    sql := sql + ' values('+IntToStr(Seg)+','+IntToStr(Conv)+','+FormatDimIB(Porcent)+')';
    sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
               ' OPERACAO, DATA_HORA, ID) values ('+
               ' gen_id(gen_log_id,1), "FCadConv", "Limite Seg.", "100.00", "'+FormatDimIB(Porcent)+'", "'+Operador.Nome+'"'+
               ', "Alt Linear", current_timestamp, "'+IntToStr(Conv)+'")';

//    DMConexao.ExecuteSql(sqlQuery);
  end
  else
  begin
    sql :=  'update CONV_LIM_SEG set PORCENT = '+FormatDimIB(Porcent);
    sql :=  sql + ' where CONV_ID = '+IntToStr(Conv)+ ' and SEG_ID = '+IntToStr(Seg);
    if Porcent > 100 then
    begin
      MsgInf('Valor de porcentagem inv�lida!');
      Exit;
    end;
    if Porcent <> vrvelho then
    begin
      vvelho:= FormatDimIB(vrvelho);
      vnovo := FormatDimIB(Porcent);
      sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                 ' OPERACAO, DATA_HORA, ID) values ('+
                 ' gen_id(gen_log_id,1), "FCadConv", "Limite Seg.", "'+vvelho+'", "'+vnovo+'", "'+Operador.Nome+'"'+
                 ', "Alt Linear", current_timestamp, "'+IntToStr(Conv)+'")';

//      DMConexao.ExecuteSql(sqlQuery);
    end;
  end;
  DMConexao.ExecuteSql(sql);
end;

procedure TFCadConv.AlteraoLineardeSenha1Click(Sender: TObject);
var index: integer;
begin
  inherited;
  index:= 0;
  FAltLinSenha := TFAltLinSenha.Create(self);
  FAltLinSenha.ShowModal;
  QCartoes.Close;
  QCartoes.Parameters.ParamByName('conv_id').Value := QCadastroCONV_ID.AsString;
  QCartoes.Open;
  if FAltLinSenha.ModalResult = mrOk then
  begin
    index:= FAltLinSenha.RadioGroup1.ItemIndex;
    QCadastro.First;
    while not QCadastro.Eof do
    begin
      if index = 0 then
      begin
        DMConexao.ExecuteSql('update cartoes set senha = "'+Crypt('E', Copy(QCartoesCODCARTIMP.AsString, 13, 4), 'BIGCOMPRAS')+'" where conv_id = '+QCadastroCONV_ID.AsString);
      end
      else
      begin
        DMConexao.ExecuteSql('update cartoes set senha = "'+Crypt('E', DigitaNovaSenha, 'BIGCOMPRAS')+'" where conv_id = '+QCadastroCONV_ID.AsString);
      end;
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  QCadastro.First;
  FAltLinSenha.Free;
end;

procedure TFCadConv.DSLimSegStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelLimSeg = ActiveControl) or (btnGravaLimSeg = ActiveControl) then
  begin
    GridLimPorSeg.SetFocus;
  end;
  btnCancelLimSeg.Enabled := QLimSeg.State in [dsEdit,dsInsert];
  btnGravaLimSeg.Enabled  := QLimSeg.State in [dsEdit,dsInsert];
end;

procedure TFCadConv.JvDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  btnAlteraCCClick(nil);
end;

procedure TFCadConv.JvDBGrid6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{  inherited;
  if QSitSegLIBERADO.AsString = 'N�O' then
  begin
    JvDBGrid6.Canvas.Font.Color := clRed;
    JvDBGrid6.DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;}
end;


procedure TFCadConv.JvDBGrid7DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{  inherited;
  if QSitGProdLIBERADO.AsString = 'N�O' then
  begin
    JvDBGrid7.Canvas.Font.Color := clRed;
    JvDBGrid7.DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;}
end;


procedure TFCadConv.TabProdutosShow(Sender: TObject);
begin
  inherited;
  if not Qcontacorrente.IsEmpty then
  begin
    QProdutos.Close;
    QProdutos.Parameters[0].Value := QcontacorrenteAUTORIZACAO_ID.AsInteger;
    QProdutos.Open;
  end;
end;

procedure TFCadConv.SomarProdutos;
var Atual : TBookmark; Soma : Currency;
begin
   Soma := 0;
   if QProdutos.Active and ( not QProdutos.IsEmpty ) then begin
      Atual := QProdutos.GetBookmark;
      QProdutos.DisableControls;
      QProdutos.First;

      while not QProdutos.Eof do begin
         Soma := Soma + ArredondaDin( QProdutosQTDE.AsInteger * QProdutosPRECO_UNI.AsCurrency);
         QProdutos.Next;
      end;
      QProdutos.GotoBookmark(Atual);
      QProdutos.FreeBookmark(Atual);
      QProdutos.EnableControls;
   end;
   LabTotProd.Caption := FormatDinBR(Soma);
   Application.ProcessMessages;
end;

procedure TFCadConv.TabProdutosHide(Sender: TObject);
begin
  inherited;
  QProdutos.Close;
end;

procedure TFCadConv.QProdutosAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SomarProdutos;
end;

procedure TFCadConv.CancelaAutorizao1Click(Sender: TObject);
begin
  inherited;
  if UpperCase(QcontacorrenteCANCELADA.AsString) = 'S' then
    begin
      msgInf('Valor j� cancelado!');
    Exit;
    end;
  if not Qcontacorrente.IsEmpty then
  begin
    if Qcontacorrente.FieldByName('VALOR_CANCELADO').Value > 0 then
    begin
      MsgInf('Autoriza��o ja possui cancelamento!');
      Exit;
    end;///////////////AQUI ADD UMA CHAVE A MAIS PARA FUNCIONAR O COMENT�RIO
    if (Qcontacorrente.FieldByName('DEBITO').AsCurrency-Qcontacorrente.FieldByName('CREDITO').AsCurrency)<=0 then
    begin
      MsgInf('Valor da Autoriza��o � negativo e n�o pode ser cancelado!');
      Exit;
    end;
    if Qcontacorrente.FieldByName('FATURA_ID').AsInteger > 0 then
    begin
      MsgInf('Autoriza��o ja faturada!');
      Exit;
    end;
    FCancelaAutor:= TFCancelaAutor.Create(nil);
    FCancelaAutor.cred_id:= Qcontacorrente.FieldByName('CRED_ID').AsInteger;
    FCancelaAutor.txtCred.Caption := QcontacorrenteCREDENCIADO.AsString;
    FCancelaAutor.autorizacao:= StrToInt(Qcontacorrente.FieldByName('AUTORIZACAO_ID').AsString+PadL(Qcontacorrente.FieldByName('DIGITO').AsString,2,'0'));
    if Qcontacorrente.FieldByName('TRANS_ID').IsNull then
      FCancelaAutor.transacao:= 0
    else
    FCancelaAutor.transacao:= Qcontacorrente.FieldByName('TRANS_ID').AsInteger;
    FCancelaAutor.txtAutor.Text:= Qcontacorrente.FieldByName('AUTORIZACAO_ID').AsString+PadL(Qcontacorrente.FieldByName('DIGITO').AsString,2,'0');
    FCancelaAutor.txtValor.Value:= FormatDinBR(Qcontacorrente.FieldByName('DEBITO').AsCurrency-Qcontacorrente.FieldByName('CREDITO').AsCurrency);
    //FCancelaAutor.txtConv.Caption:= QCadastro.FieldByName('CONV_ID').AsString + ' - ' + QCadastro.FieldByName('TITULAR').AsString;
    FCancelaAutor.txtEmp.Caption:= QEmpresa.FieldByName('EMPRES_ID').AsString + ' - ' + QEmpresa.FieldByName('NOME').AsString;
    FCancelaAutor.txtConv.Caption:= Qcontacorrente.FieldByName('CRED_ID').AsString + ' - ' + Qcontacorrente.FieldByName('CREDENCIADO').AsString;
    FCancelaAutor.txtData.Caption:= FormatDataBR(Qcontacorrente.FieldByName('DATA').AsDateTime);
    FCancelaAutor.txtValTot.Caption:= FormatDinBR(Qcontacorrente.FieldByName('DEBITO').AsCurrency-Qcontacorrente.FieldByName('CREDITO').AsCurrency);
    FCancelaAutor.ShowModal;
    if FCancelaAutor.ModalResult = mrOk then
      Qcontacorrente.Refresh;
    FCancelaAutor.Free;
  end;
end;

procedure TFCadConv.JvDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
//begin
//  inherited;
var Grid : TDBGrid;
  L, R: Integer;
  marc: Boolean;
begin
  if not (TDBGrid(Sender).Name = 'GridBranco') then
  begin
    marc:= False;
    Grid := TDBGrid(Sender);
    if ((Qcontacorrente.FieldByName('FATURA_ID').AsInteger > 0) and (JvDBGrid2.Columns[DataCol].FieldName = 'DATA_FECHA_EMP')) then
    begin
      Grid.Canvas.Font.Style:= [fsBold];
      Grid.Canvas.Font.Color:= clTeal;
    end
    else if ((Grid.DataSource.DataSet.FindField('LIBERADO') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADO').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('LIBERADA') <> nil) and (Grid.DataSource.DataSet.FindField('LIBERADA').AsString = 'N') or
      (Grid.DataSource.DataSet.FindField('BAIXADO') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADO').AsString = 'S') or
      (Grid.DataSource.DataSet.FindField('BAIXADA') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADA').AsString = 'S')) then
    begin
      Grid.Canvas.Font.Color := clRed;
      marc:= True;
    end
    else
      Grid.Canvas.Font.Color := clBlack;
    if ((Qcontacorrente.FieldByName('FATURA_ID').AsInteger > 0) and (JvDBGrid2.Columns[DataCol].FieldName = 'DATA_FECHA_EMP')) then
      JvDBGrid2.Canvas.Font.Style := [fsBold];
    with THackDBgrid(Sender) do
    begin
      if UpperCase(QcontacorrenteCANCELADA.AsString) = 'S' then
        Grid.Canvas.Font.Color := clRed
      else
      if Qcontacorrente.FieldByName('FATURA_ID').AsInteger > 0 then
        Grid.Canvas.Font.Color := clBlue
      else
        Grid.Canvas.Font.Color := clBlack;

      if DataLink.ActiveRecord = Row -1 then
      begin
        Grid.Canvas.Brush.Color:= clBlack;
        Grid.Canvas.FillRect(Rect);
        if odd(Grid.DataSource.DataSet.RecNo) then
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite
        else
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $007676EB;
          end
          else
            Canvas.Brush.Color:= $0EEEEEE;
      end
      else
      begin
        if odd(Grid.DataSource.DataSet.RecNo) then
        begin
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite;
        end
        else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsString = 'S') then
        begin
          if marc then Grid.Canvas.Font.Color := clWhite;
          Canvas.Brush.Color:= $007676EB;
        end
        else
          Canvas.Brush.Color:= $0EEEEEE;
        Grid.Canvas.FillRect(Rect);
      end;
    end;
    R:= Rect.Right; L:= Rect.Left;
    if Column.Index = 0 then L := L + 1;
    if Column.Index = Grid.Columns.Count -1 then R := R - 1;
    Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
    Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
  end;

end;

function TFCadConv.CartaoValidado:Boolean;
var qts, qtdAtivos: integer;
begin
  if (not QCadastro.IsEmpty) then
  begin
    qts       := DMConexao.ExecuteScalar(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and coalesce(ativo,''N'')=''S'' and coalesce(liberado,''N'')=''S'' and coalesce(titular,''S'')=''S'' and conv_id = '+QCadastroCONV_ID.AsString);
    qtdAtivos := DMConexao.ExecuteScalar(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and coalesce(ativo,''N'')=''S'' and coalesce(liberado,''N'')=''S'' and conv_id = '+QCadastroCONV_ID.AsString);
    if qts > 1 then
    begin
      MsgErro('Aten��o � obrigat�rio que o conveniado tenha somente um cart�o de titular');
      //raise Exception.Create('Aten��o � obrigat�rio que o conveniado tenha somente um cart�o de titular');
      Result:= False;
    end
//    else if (qts = 0) and (qtdAtivos > 0) then
//    begin
//      MsgErro('Aten��o � obrigat�rio que o conveniado tenha um cart�o de titular');
//      Result:= False;
//    end
    else
    begin
      Result:= True;
    end;
  end
  else
    Result:= True;
end;

procedure TFCadConv.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if not (QCadastro.State in [dsEdit, dsInsert]) then
    if ((PageControl1.ActivePageIndex = 2) and not (CartaoValidado)) then
    AllowChange:= False;
end;

procedure TFCadConv.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if not CartaoValidado then
  begin
    Abort;
  end;
  inherited;
end;
procedure TFCadConv.DSPbmStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelPbm = ActiveControl) or (btnGravaPbm = ActiveControl) then
    GridPbm.SetFocus;
  btnCancelPbm.Enabled  := qPbm.State in [dsEdit,dsInsert];
  btnGravaPbm.Enabled := qPbm.State in [dsEdit,dsInsert];
end;

procedure TFCadConv.qPbmAfterPost(DataSet: TDataSet);
begin
  inherited;
  if qPbm.FieldByName('PARTICIPA').OldValue <> UpperCase(qPbm.FieldByName('PARTICIPA').AsString) then
  begin
    if UpperCase(qPbm.FieldByName('PARTICIPA').AsString) = 'S' then
    begin
      DMConexao.ExecuteSql(' insert into prog_conv (prog_id, conv_id) values ('+qPbm.FieldByName('PROG_ID').AsString+', '+QCadastro.FieldByName('CONV_ID').AsString+') ');
      DMConexao.GravaLog('FCadConv','Pbm','',qPbm.FieldByName('PROG_ID').AsString+' - '+qPbm.FieldByName('NOME').AsString,Operador.Nome,'Inclus�o','Conv ID',Self.Name);
    end
    else
    begin
      DMConexao.ExecuteSql(' delete from prog_conv where prog_id = '+qPbm.FieldByName('PROG_ID').AsString+' and conv_id = '+QCadastro.FieldByName('CONV_ID').AsString);
      DMConexao.GravaLog('FCadConv','Pbm',qPbm.FieldByName('PROG_ID').AsString+' - '+qPbm.FieldByName('NOME').AsString, '',Operador.Nome,'Exclus�o','Conv ID',Self.Name);
    end;
  end;
end;

procedure TFCadConv.qPbmBeforePost(DataSet: TDataSet);
begin
  inherited;
  qPbm.FieldByName('PARTICIPA').AsString:= UpperCase(qPbm.FieldByName('PARTICIPA').AsString);
  if qPbm.FieldByName('PARTICIPA').AsString <> 'S' then
    qPbm.FieldByName('PARTICIPA').AsString:= 'N';
  if ((qPbm.FieldByName('PARTICIPA').AsString = 'N') and (qPbm.FieldByName('EMPRES_ID').AsInteger > 0)) then
  begin
    MsgErro('Essa programa n�o pode ser desvinculado por fazer parte da empresa!');
    qPbm.Cancel;
  end;
end;

procedure TFCadConv.GridPbmColExit(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsEdit] then
    qPbm.Post;
end;

procedure TFCadConv.acertaCupom;
var i: Integer;
begin
  for i:=0 to mmCupom.Lines.Count do
  begin
    if Length(mmCupom.Lines[i]) > 40 then
      mmCupom.Lines[i]:= copy(mmCupom.Lines[i],1,40);
    if Pos('<C>',mmCupom.Lines[i])>0 then
    begin
      mmCupom.Lines[i] := StringReplace(mmCupom.Lines[i],'<C>','',[rfIgnoreCase,rfReplaceAll]);
      mmCupom.Lines[i] := CentralizaTexto(mmCupom.Lines[i],40,' ');
    end;
  end;
end;

function TFCadConv.CentralizaTexto(Texto:string;Tamanho:Integer=127;Preenchedor:string=' '): String;
begin
  if Length(Texto) > Tamanho then
  begin
    Texto := Copy(Texto , 1, Tamanho);
  end;
  while Length(Texto) < Tamanho do
  begin
    if (Length(Texto) mod 2) = 0 then
    begin
      Texto := Texto + Preenchedor
    end
    else
    begin
      Texto := Preenchedor + Texto;
    end;
  end;
  Result := Texto;
end;

procedure TFCadConv.DSValGrupDescDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if qValGrupDesc.FieldByName('CUPOM').IsNull then
    GroupBox17.Visible:= False
  else
    GroupBox17.Visible:= True;
  acertaCupom;
end;

procedure TFCadConv.HabilitarBotaoPerfilDiretor(NomeDoBotao : TButton);
begin
  NomeDoBotao.Enabled := True;
end;

procedure TFCadConv.TabFichaShow(Sender: TObject);
begin
  inherited;
  PageControl2.TabIndex:= 0;
  lblAbonoMes.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  dbEdtAbonoMes.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  dbEdtSaldoRenovacao.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  lblSaldoRenovacao.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  dbEdtSaldoAcumulado.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  lblSaldoAcumulado.Visible := QCadastroTIPO_CREDITO.AsInteger in [2,3];
  if QCadastroAPAGADO.AsString = 'S' then
    cbApagado.Visible := True;

  if (QCadastroTIPO_CREDITO.AsInteger in [1,2]) then begin
    dbEdtSalario.Top  := dbEdtAbonoMes.Top;
    dbEdtSalario.Left := dbEdtAbonoMes.Left - 15 - dbEdtSalario.Width;
    lblSalario.Top    := lblAbonoMes.Top;
    lblSalario.Left   := dbEdtSalario.Left;

    dbEdtLimite.Top   := dbEdtSalario.Top;
    dbEdtLimite.Left  := dbEdtSalario.Left - 15 - dbEdtSalario.Width;
    lblLimite.Top     := lblSalario.Top;
    lblLimite.Left    := dbEdtLimite.Left;

    if (QCadastroTIPO_CREDITO.AsInteger in [2,3]) then begin
      lblTranferencia.Caption :=' Transfer�ncia de Saldo Acumulado do Conv. ID ' +  QCadastroCONV_ID.AsString + ' para o Conv.ID ';
      dbConvID.Visible := true;
      dbConvID.Text := '';
      lblTranferencia.Visible := true;
      GroupBox4.Height  := 140;
      btnTransferencia.Visible := true;
    end
    else begin
      dbConvID.Visible     := false;
      lblTranferencia.Visible := false;
      GroupBox4.Height  := 114;
      btnTransferencia.Visible := false;
      dbConvID.Text := '';
    end;
  end else begin
    lblLimite.Left       := 585;
    lblLimite.Top        := 11;
    dbEdtLimite.Left     := 585;
    dbEdtLimite.Top      := 26;
    lblSalario.Left      := 688;
    lblSalario.Top       := 11;
    dbEdtSalario.Left    := 688;
    dbEdtSalario.Top     := 26;
    GroupBox4.Height     := 60;
    dbConvID.Visible     := false;
    lblTranferencia.Visible := false;
  end;

end;

procedure TFCadConv.carregaCupom;
begin

end;

procedure TFCadConv.GridLimPorSegColExit(Sender: TObject);
begin
  inherited;

  if QLimSeg.State in [dsEdit] then
  begin
    Porcent        := GridLimPorSeg.Fields[2].Value;
    LimiteValor    := GridLimPorSeg.Fields[3].Value;
    QLimSeg.Post;
  end;
end;

procedure TFCadConv.gridTransacaoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var Grid : TDBGrid;
  L, R: Integer;
begin
  Grid := TDBGrid(Sender);
  if ((gdFocused in State) and (dgEditing in Grid.Options)) then
  begin
    Grid.Canvas.Font.Style:= [fsBold];
    Grid.Canvas.Font.Color:= clTeal;
  end
  else if Grid.DataSource.DataSet.FindField('CONFIRMADA').AsString = 'N' then
    Grid.Canvas.Font.Color := clRed
  else
    Grid.Canvas.Font.Color := clBlack;
  with THackDBgrid(Sender) do
  begin
    if DataLink.ActiveRecord = Row -1 then
    begin
      Grid.Canvas.Brush.Color:= clBlack;
      Grid.Canvas.FillRect(Rect);
      if odd(Grid.DataSource.DataSet.RecNo) then
        Canvas.Brush.Color:= clWhite
      else
        Canvas.Brush.Color:= $0EEEEEE;
    end
    else
    begin
      if odd(Grid.DataSource.DataSet.RecNo) then
        Canvas.Brush.Color:= clWhite
      else
        Canvas.Brush.Color:= $0EEEEEE;
    end;
  end;
  R:= Rect.Right; L:= Rect.Left;
  if Column.Index = 0 then L := L + 1;
  if Column.Index = Grid.Columns.Count -1 then R := R - 1;
  Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
  Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
end;

procedure TFCadConv.QLimSegBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.QLimSegBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.qPbmBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.qPbmBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.DSContaCorrenteStateChange(Sender: TObject);
begin
  inherited;
  btnAlteraCC.Enabled := Alterar and (not Qcontacorrente.IsEmpty);
end;

procedure TFCadConv.qContaCorrenteBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.qContaCorrenteBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     msginf('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadConv.btn2ViaClick(Sender: TObject);
var CodCartao,oldEmpresID, oldNome, oldTitular, oldParent, oldCpf, oldRg: string;
  oldDataNasc: TDate;
begin
  inherited;
  if QCartoes.IsEmpty then
  begin
    MsgErro('Selecione um cart�o para emitir a segunda via!');
    Exit;
  end;
  DMConexao.Config.Open;
  if (DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S') or
     (DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S') or
     (DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S') or
     (DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S') then
    CodCartao:= QCartoes.FieldByName('CODCARTIMP').AsString
  else
    CodCartao:= QCartoes.FieldByName('CODIGO').AsString + PadL(QCartoes.FieldByName('DIGITO').AsString,2,'0');
  if not (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Inclus�o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '')) then
    Abort;
  //if MsgSimNao('Confirma a emiss�o de segunda via do cart�o n� '+CodCartao+sLineBreak+'no nome de '+QCartoes.FieldByName('NOME').AsString) then
  //begin
    if DMConexao.ConfigEMITE_NOVO_CART_2VIA.AsString = 'S' then
    begin
      oldNome     := QCartoes.FieldByName('NOME').AsString;
      oldTitular  := QCartoes.FieldByName('TITULAR').AsString;
      oldParent   := QCartoes.FieldByName('PARENTESCO').AsString;
      oldCpf      := QCartoes.FieldByName('CPF').AsString;
      oldRg       := QCartoes.FieldByName('RG').AsString;
      oldEmpresID := QCartoes.FieldByName('EMPRES_ID').AsString;
      if QCartoes.FieldByName('DATA_NASC').Value > 0 then
        oldDataNasc:= QCartoes.FieldByName('DATA_NASC').AsDateTime;
      QCartoes.Edit;
      QCartoes.FieldByName('LIBERADO').AsString:= 'I';
      if oldTitular = 'S' then
        QCartoes.FieldByName('TITULAR').AsString:= 'N';
      QCartoes.Post;
      QCartoes.Append;
      QCartoes.FieldByName('NOME').AsString:= oldNome;
      QCartoes.FieldByName('LIBERADO').AsString:= 'S';
      QCartoes.FieldByName('TITULAR').AsString:= oldTitular;
      QCartoes.FieldByName('PARENTESCO').AsString:= oldParent;
      QCartoes.FieldByName('DTCADASTRO').AsDateTime:= Now;
      QCartoes.FieldByName('CPF').AsString:= oldCpf;
      QCartoes.FieldByName('RG').AsString:= oldRg;
      QCartoes.FieldByName('VIA').AsInteger:= 2;
      QCartoes.FieldByName('EMPRES_ID').AsInteger := StrToInt(oldEmpresID);

      if oldDataNasc > 0 then
        QCartoes.FieldByName('DATA_NASC').AsDateTime:= oldDataNasc;
      QCartoes.Post;
    end
    else
    begin
      QCartoes.Edit;
      QCartoes.FieldByName('JAEMITIDO').AsString:= 'N';
      QCartoes.FieldByName('VIA').AsInteger:= 2;
      QCartoes.Post;
      //atualizaCodImp;
    end;
  //end;
  MsgInf('O cart�o pode ser reimpresso com sucesso');
  DMConexao.Config.Close;
end;

procedure TFCadConv.TabLimiteSegHide(Sender: TObject);
begin
  inherited;
  QLimSeg.Close;
end;

procedure TFCadConv.TabLimiteSegShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QLimSeg.Close;
    QLimSeg.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
    QLimSeg.Open;
  end;
end;


procedure TFCadConv.TabFotosExit(Sender: TObject);
begin
 { inherited;
  CapFoto.Connected := False;
  QFotos.Close;  }
end;

procedure TFCadConv.TabFotosShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QFotos.Close;
    QFotos.SQL.Clear;
    QFotos.SQL.Add(' Select * from fotos_conveniados where conv_id = '+QCadastroCONV_ID.AsString);
    QFotos.Open;
    carregaimg;
  end;
end;

procedure TFCadConv.TabTodasAsComprasExit(Sender: TObject);
begin
  inherited;
  QTodasCompras.Close;
end;

procedure TFCadConv.TabTodasAsComprasShow(Sender: TObject);
begin
  inherited;
  data1.SetFocus;
end;

procedure TFCadConv.TabFildelidadeShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    panFidelidade.Visible:= False;
    if ((QCadastroFIDELIDADE.AsString = 'S') or (QEmpresaFIDELIDADE.AsString = 'S')) then
    begin
      panFidelidade.Visible:= False;
      qFidelidade.Close;
      qFidelidade.Parameters[0].Value:= QCadastro.FieldByName('CONV_ID').AsInteger;
      qFidelidade.Open;
      CalcTotPtsFidel;
    end
    else
    begin
      qFidelidade.Close;
      lblTotalPontos.Caption:= IntToStr(0);
      lblAPerder.Caption:= IntToStr(0);
      lblTotalSaldo.Caption:= IntToStr(0);
      panFidelidade.Visible:= True;
    end;
  end;
end;

procedure TFCadConv.TabFildelidadeExit(Sender: TObject);
begin
  inherited;
  if qFidelidade.Active then
    qFidelidade.Close;
end;

procedure TFCadConv.CalcTotPtsFidel;
var  Marca : TBookmark;
  Total, Aperder, Saldo: Integer;
begin
  Total:= 0;
  Aperder:= 0;
  Saldo:= 0;
  qFidelidade.DisableControls;
  Marca := qFidelidade.GetBookmark;
  qFidelidade.First;
  while not qFidelidade.eof do
  begin
    Total := Total + qFidelidade.FieldByName('SALDO').AsInteger;
    if ((qFidelidade.FieldByName('DATAEXPIRA').AsDateTime >= Now) and (qFidelidade.FieldByName('DATAEXPIRA').AsDateTime <= EndOfTheMonth(Now))) then
    begin
      Aperder := Aperder + qFidelidade.FieldByName('SALDO').AsInteger;
    end;
    qFidelidade.Next;
  end;
  lblTotalPontos.Caption:= IntToStr(Total);
  lblAPerder.Caption:= IntToStr(Aperder);
  lblTotalSaldo.Caption:= IntToStr(Total-Aperder);
  qFidelidade.GotoBookmark(Marca);
  qFidelidade.FreeBookmark(Marca);
  qFidelidade.EnableControls;
end;

procedure TFCadConv.TabOutrasShow(Sender: TObject);
begin
  inherited;
  PageControl5.TabIndex:= 0;
  QLimSeg.Parameters.ParamByName('conv_id1').Value   := QCadastroCONV_ID.Value;
  QLimSeg.Parameters.ParamByName('conv_id2').Value   := QCadastroCONV_ID.Value;
  QLimSeg.Parameters.ParamByName('empres_id1').Value := QCadastroEMPRES_ID.Value;
  QLimSeg.Parameters.ParamByName('empres_id2').Value := QCadastroEMPRES_ID.Value;
  QLimSeg.Open;
end;

procedure TFCadConv.PageControl5Enter(Sender: TObject);
begin
  inherited;
  TabLimiteSegShow(Self);
end;

procedure TFCadConv.btnGravaLimSegClick(Sender: TObject);
begin
  inherited;
  SavePlace := QLimSeg.GetBookmark;
  if TBitBtn(Sender).Enabled = False then Abort;
  if QLimSeg.State in [dsInsert,dsEdit] then
  begin
    Porcent        := GridLimPorSeg.Fields[2].Value;
    LimiteValor    := GridLimPorSeg.Fields[3].Value;
    QLimSeg.Post;
  end;
end;

procedure TFCadConv.ButApaga_CartaoClick(Sender: TObject);
begin
  inherited;
  if not QCartoes.IsEmpty then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Text := 'Select count(autorizacao_id) as num from contacorrente where cartao_id = '+QCartoesCARTAO_ID.AsString;
    DMConexao.AdoQry.Open;
    If DMConexao.AdoQry.FieldByName('num').AsInteger > 0 then
    begin
      Application.MessageBox('Cart�o possui movimenta��o de conta corrente.'+#13+'N�o ser� poss�vel exclu�-lo.','Aten��o',MB_OK+MB_DEFBUTTON1+MB_ICONINFORMATION);
    end
    else if DMConexao.ExecuteQuery(' select count(*) from cartoes where coalesce(apagado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString) = 1 then
    begin
      Application.MessageBox('O sistema n�o permite a exist�ncia de um titular sem um cart�o.'+sLineBreak+'Se desejar, exclua o titular e os cart�es ser�o excluidos automaticamente.','Aten��o',MB_OK+MB_DEFBUTTON1+MB_ICONINFORMATION);
    end
    else
    begin
      if Application.MessageBox('Confirma a exclus�o deste cart�o?','Confirma��o',mb_YesNO+MB_DEFBUTTON2+MB_ICONQUESTION) = IDYes then
      begin
        DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Inclus�o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '');
        QCartoes.edit;
        QCartoesAPAGADO.AsString := 'S';
        QCartoes.Post;
        QCartoes.Refresh;
      end;
    end;
    DMConexao.AdoQry.Close;
  end;
end;

procedure TFCadConv.btnCancelLimSegClick(Sender: TObject);
begin
  inherited;
  if QLimSeg.State in [dsInsert,dsEdit] then
  begin
    QLimSeg.Cancel;
  end;
end;

procedure TFCadConv.ButGravaCartaoClick(Sender: TObject);
begin
  if (Qcartoes.State in [dsedit, dsInsert]) and (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Inclus�o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '')) then
    QCartoes.Post;
    DMConexao.ExecuteSql('INSERT INTO CARTOES_HISTORICO SELECT CARTAO_ID, CONV_ID, EMPRES_ID, CODCARTIMP, CVV, SENHA, GETDATE() FROM CARTOES WHERE CARTAO_ID = '  + QCartoesCARTAO_ID.AsString);
  inherited;
  JvDBGrid1.SetFocus;
end;

procedure TFCadConv.ButCancelCartaoClick(Sender: TObject);
begin
  inherited;
  QCartoes.Cancel;
end;

procedure TFCadConv.btnGravaPbmClick(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsInsert,dsEdit] then qPbm.Post;
end;

procedure TFCadConv.btnCancelPbmClick(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsInsert,dsEdit] then qPbm.Cancel;
end;

procedure TFCadConv.BitBtn1Click(Sender: TObject);
begin
  if fnVerfCampoVazio('Voc� deve selecionar um conv�nio para fazar a consula!', QCadastroCONV_ID) then abort;
  inherited;
  qValGrupDesc.Close;
  qValGrupDesc.SQL.Clear;
//  qValGrupDesc
  qValGrupDesc.SQL.Add(' select t.trans_id, t.cred_id, t.datahora, cred.nome, t.confirmada, ');
  qValGrupDesc.SQL.Add(' COALESCE(t.pontos,0) AS pontos, COALESCE(t.vale_utilizado,0.00) AS vale_utilizado, ');
  qValGrupDesc.SQL.Add(' COALESCE(t.vale_acumulado,0.00) AS vale_acumulado, ');
  qValGrupDesc.SQL.Add(' coalesce(sum(pt.vlr_bru),t.valor,0.00) bruto, coalesce(sum(pt.vlr_desc),0.00) descont, ');
  qValGrupDesc.SQL.Add(' coalesce(sum(pt.vlr_liq),t.valor,0.00) liquido, t.cupom, t.operador ');
  qValGrupDesc.SQL.Add(' from transacoes t join credenciados cred on t.cred_id = cred.cred_id ');
  qValGrupDesc.SQL.Add(' left join prod_trans pt on t.trans_id = pt.trans_id ');
  if ckbSomenteValProgramas.Checked then
    qValGrupDesc.SQL.Add(' and pt.status = 0 ');
  qValGrupDesc.SQL.Add(' where t.cancelado <> ''S'' and t.cartao_id in (select cartao_id from cartoes ');
  qValGrupDesc.SQL.Add(' where conv_id = :conv) ');
  qValGrupDesc.SQL.Add(' and t.confirmada = ''S'' ');
  qValGrupDesc.SQL.Add(' and t.datahora between '+FormatDataIB(datainiPbm.Date,True,Inicial)+' and '+FormatDataIB(datafimPbm.Date,True,Final));
  qValGrupDesc.SQL.Add(' group by t.trans_id, t.cred_id, t.datahora, cred.nome, t.confirmada, t.cupom, ');
  qValGrupDesc.SQL.Add(' t.operador, t.cancelado, t.dtcancelado, t.opercancelado, t.valor, ');
  qValGrupDesc.SQL.Add(' t.pontos, t.vale_utilizado, t.vale_acumulado ');
  qValGrupDesc.SQL.Add(' order by t.datahora desc ');
  qValGrupDesc.Parameters[0].Value := QCadastro.FieldByName('CONV_ID').AsInteger;
  qValGrupDesc.Open;
  acertaCupom;
  if not qValGrupDesc.IsEmpty then
  begin
    carregaCupom;
    qProdTrans.Close;
    qProdTrans.SQL.Clear;
    qProdTrans.SQL.Add(' select pt.prog_id, coalesce(pr.nome,''GRUPO: ''+ gr.descricao) nome, t.trans_id, pt.prod_id, coalesce(pt.descricao,p.descricao,''PRODUTO NAO ENCONTRADO'') descricao, ');
    qProdTrans.SQL.Add(' pt.codbarras, pt.qtd_solic, pt.qtd_aprov, pt.prc_unitbru_rec ,pt.prc_unit_env, ');
    qProdTrans.SQL.Add(' pt.vlr_bru, pt.vlr_desc, pt.vlr_liq, pt.status, ');
    qProdTrans.SQL.Add(' case when pt.status = 0 then (case when pt.prog_id is null then ''GRUPO DE PRODUTO'' else ''PROGRAMA DE DESCONTO'' end) when pt.status = 1 then ''SEM DESCONTO'' ');
    qProdTrans.SQL.Add(' when pt.status = 2 then ''CODIGO DE BARRAS BLOQUEADO'' else ''DESCONTO PELO BIG MAIOR'' end situacao, ');
    qProdTrans.SQL.Add(' coalesce(pt.vale_acumulado,0.00) as vale_acumulado, ');
    qProdTrans.SQL.Add(' coalesce(pt.vale_utilizado,0.00) as vale_utilizado, ');
    qProdTrans.SQL.Add(' case when coalesce(pt.pontos,0) = 0 then ''N'' else ''S'' end as gerou_pontos ');
    qProdTrans.SQL.Add(' from transacoes t ');
    qProdTrans.SQL.Add(' join prod_trans pt on t.trans_id = pt.trans_id ');
    if ckbSomenteValProgramas.Checked then
      qProdTrans.SQL.Add(' and pt.status = 0 ');
    qProdTrans.SQL.Add(' join grupo_prod gr on pt.grupo_prod_id = gr.grupo_prod_id ');
    qProdTrans.SQL.Add(' left join produtos p on pt.prod_id = p.prod_id ');
    qProdTrans.SQL.Add(' left join programas pr on pt.prog_id = pr.prog_id ');
    qProdTrans.SQL.Add(' where t.cancelado <> ''S'' and t.cartao_id in (select cartao_id from cartoes ');
    qProdTrans.SQL.Add(' where conv_id = '+QCadastro.FieldByName('CONV_ID').AsString+') ');
    qProdTrans.SQL.Add(' and t.confirmada = ''S'' ');
    qProdTrans.SQL.Add(' and t.datahora between '+FormatDataIB(datainiPbm.Date,True,Inicial)+' and '+FormatDataIB(datafimPbm.Date,True,Final));
    qProdTrans.Open;
  end;
  gridTransacao.SetFocus;
end;

procedure TFCadConv.btnAlteraCCClick(Sender: TObject);
var datafecha : TDateTime;
  FAltContaCor : TFAltContaCor;
  teste : boolean;
  sql, qtdLimite : String;
  i,diferencaDeMes,seg_id, codLimite : Integer;
  data_fecha_emp_OLD,fechamento_atual, data_fecha_emp_value : TDateTime;
  dia_fechamento,dia_prox_fecha,mes_prox_fecha,ano_prox_fecha, mes_fechamento, ano_fechamento,oldAnoFechamento,oldMesFechamento,oldDiaFechamento, dia_atual, mes_atual, ano_atual : Word;
  consumo_mes, creditoOldVal, debitoOldVal : currency;
begin
  if not Qcontacorrente.IsEmpty then begin
    if QcontacorrenteFATURA_ID.AsInteger = 0 then begin

      FAltContaCor := TFAltContaCor.create(self);
      for i := 0 to FAltContaCor.ComponentCount -1 do
        if IsPublishedProp(FAltContaCor.Components[i],'DataSource') then
          SetObjectProp(FAltContaCor.Components[i],'DataSource',Self.DSContaCorrente);

      datafecha := QcontacorrenteDATA_FECHA_EMP.AsDateTime;
      FAltContaCor.ShowModal;
      if qContaCorrente.State = dsEdit then begin
        if FAltContaCor.ModalResult = mrOk then begin
          if (datafecha <> QcontacorrenteDATA_FECHA_EMP.AsDateTime) then begin
            teste := DMConexao.ValidarFechamentoAbertoConv(QcontacorrenteDATA_FECHA_EMP.AsDateTime,QCadastroCONV_ID.AsInteger,QCadastroEMPRES_ID.AsInteger);
            if not DMConexao.ValidarFechamentoAbertoConv(QcontacorrenteDATA_FECHA_EMP.AsDateTime,QCadastroCONV_ID.AsInteger,QCadastroEMPRES_ID.AsInteger) then begin
              Qcontacorrente.Cancel;
              MsgErro('Data de fechamento inv�lida para a empresa ou '+sLineBreak+
                      'essa data de fechamento foi faturada para esta empresa ou para este conveniado.'+sLineBreak+
                      'Altera��o Cancelada');
              Exit;
            end;
          end;
          DecodeDate(qContaCorrenteDATA_FECHA_EMP.AsDateTime,ano_fechamento, mes_fechamento, dia_fechamento);
					DecodeDate(now,ano_atual, mes_atual, dia_atual);
          data_fecha_emp_OLD := QContaCorrenteDATA_FECHA_EMP.OldValue;
          data_fecha_emp_value := QContaCorrenteDATA_FECHA_EMP.Value;
          DecodeDate(QContaCorrenteDATA_FECHA_EMP.OldValue,oldAnoFechamento,oldMesFechamento,oldDiaFechamento);

          if VarIsNull(qContaCorrenteCREDITO.OldValue)then
             creditoOldVal := 0
          else
             creditoOldVal := QContaCorrenteCREDITO.OldValue;

          if VarIsNull(QContaCorrenteDEBITO.OldValue)then
              debitoOldVal := 0
          else
              debitoOldVal := QContaCorrenteDEBITO.OldValue;

				  if (creditoOldVal <> QContaCorrenteCREDITO.Value)
				  or (debitoOldVal <> qContaCorrenteDEBITO.Value) or (mes_fechamento <> oldMesFechamento) then
          begin
            qContaCorrenteOPERADOR.AsString := Operador.Nome;
            qContaCorrenteDATA_ALTERACAO.AsDateTime := Date;
            qContaCorrente.Post;
            DMConexao.AtualizaComReceitaMovProd(QcontacorrenteAUTORIZACAO_ID.AsInteger, QcontacorrenteRECEITA.AsString);

            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT top 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA ');
            DMConexao.AdoQry.SQL.Add('WHERE DIA_FECHA.DATA_FECHA > CURRENT_TIMESTAMP');
            DMConexao.AdoQry.SQL.Add('AND DIA_FECHA.EMPRES_ID = '''+QCadastroEMPRES_ID.AsString+''' ORDER BY 1');
            DMConexao.AdoQry.Open;
            fechamento_atual := DMConexao.AdoQry.Fields[0].Value;


            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('select coalesce(qtd_limites,1) as qtd_limites from bandeiras where band_id = '+QCadastroBAND_ID.AsString+'');
            DMConexao.AdoQry.Open;
            qtdLimite := DMConexao.AdoQry.Fields[0].AsString;

            try
                DMConexao.AdoCon.BeginTrans;
                DMConexao.ExecuteSql('EXEC CONSUMO_MES_CONV ' + QCadastroEMPRES_ID.AsString + ',' + QCadastroBAND_ID.AsString + ',' + QCadastroTIPO_CREDITO.AsString + ',''' + DateToStr(fechamento_atual) + ''',' + qtdLimite + ',' + QCadastroCONV_ID.AsString);
                DMConexao.AdoCon.CommitTrans;

                DMConexao.AdoQry.SQL.Clear;
                DMConexao.AdoQry.SQL.Add('select consumo_mes_1, consumo_mes_2, consumo_mes_3, consumo_mes_4 from conveniados where conv_id = '+ QCadastroCONV_ID.AsString);
                DMConexao.AdoQry.Open;

                if QCadastroBAND_ID.AsInteger = 999 then
                  DMConexao.GravaLog('FCadConv','CONSUMO_MES_1',QCadastroCONSUMO_MES_1.AsString,DMConexao.AdoQry.Fields[0].AsString,Operador.Nome,'Altera��o',
                       QCadastroCONV_ID.AsString,'')
                else
                  begin
                      sql := 'select seg_id from CREDENCIADOS,contacorrente'+
                      ' where contacorrente.CRED_ID = CREDENCIADOS.CRED_ID'+
                      ' and credenciados.cred_id = '+QContaCorrenteCRED_ID.AsString;
                      QBuscaSeg_id.SQL.Clear;
                      QBuscaSeg_id.SQL.Add(sql);
                      QBuscaSeg_id.Open;
                      DMConexao.Q.SQL.Clear;
                      DMConexao.Q.SQL.Add('select coalesce(cod_limite,1) as cod_limite from bandeiras_segmentos where band_id = '+QCadastroBAND_ID.AsString+' and seg_id = '+ QBuscaSeg_idseg_id.AsString+'');
                      DMConexao.Q.Open;
                      codLimite := DMConexao.Q.Fields[0].AsInteger;
                      if codLimite = 1 then
                         DMConexao.GravaLog('FCadConv','CONSUMO_MES_1',QCadastroCONSUMO_MES_1.AsString,DMConexao.AdoQry.Fields[0].AsString,Operador.Nome,'Altera��o',
                            QCadastroCONV_ID.AsString,'')
                      else if codLimite = 2 then
                         DMConexao.GravaLog('FCadConv','CONSUMO_MES_2',QCadastroCONSUMO_MES_2.AsString,DMConexao.AdoQry.Fields[1].AsString,Operador.Nome,'Altera��o',
                            QCadastroCONV_ID.AsString,'')
                      else if codLimite = 3 then
                         DMConexao.GravaLog('FCadConv','CONSUMO_MES_3',QCadastroCONSUMO_MES_3.AsString,DMConexao.AdoQry.Fields[2].AsString,Operador.Nome,'Altera��o',
                            QCadastroCONV_ID.AsString,'')
                      else if codLimite = 4 then
                         DMConexao.GravaLog('FCadConv','CONSUMO_MES_4',QCadastroCONSUMO_MES_4.AsString,DMConexao.AdoQry.Fields[3].AsString,Operador.Nome,'Altera��o',
                          QCadastroCONV_ID.AsString,'');

                  end;
            except
                  on e:Exception do
                  begin
                    DMConexao.AdoCon.RollbackTrans;
                  end;
            end;
        end;
        if QCartoes.Active then
           QCartoes.Refresh;
        end
        else qContaCorrente.Cancel;
      end;
      FAltContaCor.Free;
    end
    else
      MsgInf('Autoriza��o faturada, n�o � poss�vel alterar!');
    JvDBGrid2.SetFocus;
  end;
end;

procedure TFCadConv.btnAtualizaSaldoClick(Sender: TObject);
var conv_id : integer;
    texto : String ;
begin
  inherited;
  Application.ProcessMessages;
  conv_id := QCadastroCONV_ID.Value;

  QSaldoConv.Close;
  QSaldoCartao.Close;
  //if not QCadastro.IsEmpty then
  //begin
    //if not QCartoes.Active then TabCartoesShow(nil);
    Screen.Cursor := crHourGlass;
    QSaldoConv.Parameters[0].Value   := conv_id;
    QSaldoCartao.Parameters[0].Value := conv_id;
    if((DEdataini.Text = '  /  /    ') and (DEdatafim.Text = '  /  /    '))then
    begin
      QSaldoConv.Parameters[1].Value   := null;
      QSaldoConv.Parameters[2].Value   := null;
      QSaldoCartao.Parameters[1].Value := null;
      QSaldoCartao.Parameters[2].Value := null;
      QSaldoConv.Open;
    end

    else
    if(DEdataini.Text = '  /  /    ') or (DEdatafim.Text = '  /  /    ') then
    begin
      if(DEdataini.Text = '  /  /    ')then
      begin
        ShowMessage('Necess�rio Escolher a Data Inicial');
        DEdataini.SetFocus;
      end
      else
        ShowMessage('Necess�rio Escolher a Data Final');
        DEdatafim.SetFocus;
    end

    else
    if(DEdatafim.Date < DEdataini.Date)then
    begin
      ShowMessage('A Data Final Deve ser Maior ou Igual a Data Inicial');
    end
    else
    begin
      QSaldoConv.Parameters[1].Value   := DEdataini.Text;
      QSaldoConv.Parameters[2].Value   := DEdatafim.Text;
      QSaldoCartao.Parameters[1].Value := DEdataini.Text;
      QSaldoCartao.Parameters[2].Value := DEdatafim.Text;
      QSaldoConv.Open;
      QSaldoCartao.Open;
      LimpaEdits();
    end;

    Application.ProcessMessages;
    //QSaldoCartao.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
    //QSaldoCartao.Open;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;

  //end;


  {if QSaldoConv.Active then//and QSaldoCartao.Active then
  begin
    QSaldoConv.Parameters.ParamByName('data_ini').Value := DEdataini.Text;
    QSaldoConv.Parameters.ParamByName('data_fim').Value := DEdatafim.Text;
    QSaldoConv.Parameters.ParamByName('conv_id').Value := conv_id;
    QSaldoConv.Open;
    QSaldoConv.Edit;
    QSaldoConv.Post;
    Screen.Cursor := crHourglass;
    //QSaldoConv.Refresh;
    Application.ProcessMessages;
    //QSaldoCartao.Refresh;
    //Application.ProcessMessages;
    //Screen.Cursor := crdefault;
  end; }
end;

procedure TFCadConv.btnVisualHistClick(Sender: TObject);
var cadastro, field: string;
var dataIni, dataFim : string;

begin
  if Qcontacorrente.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  dataIni := FormatDateTime('dd/mm/yyyy 00:00:00',dataini1.Date);
  dataFim := FormatDateTime('dd/mm/yyyy 23:59:59',datafin1.Date);
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(dataIni)+' and '+QuotedStr(dataFim));
  QHistorico.Sql.Add(' and ID = '+QcontacorrenteAUTORIZACAO_ID.AsString);
  QHistorico.Sql.Add(' and JANELA = '+QuotedStr('CONTACORRENTE')+'');
  if DBCampoCC.ItemIndex > 0 then
  begin
    cadastro := Copy(DBCampoCC.Text,1,Pos('.',DBCampoCC.Text)-1);
    field    := Copy(DBCampoCC.Text,Pos('.',DBCampoCC.Text)+1,Length(DBCampoCC.Text));
    QHistorico.Sql.Add(' and CAMPO = '+QuotedStr(field));
  end;
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFCadConv.btnAbrirTodasComprasClick(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    DMConexao.Config.Open;
    cred_id_baixa := DMConexao.ConfigCOD_CRED_BAIXA.AsInteger;
    DMConexao.Config.Close;
    QTodasCompras.Sql.Clear;
    QTodasCompras.Sql.Add(' select contacorrente.*, credenciados.fantasia from contacorrente ');
    QTodasCompras.Sql.Add(' join credenciados on (credenciados.cred_id = contacorrente.cred_id) ');
    QTodasCompras.Sql.Add(' where contacorrente.conv_id = '+QCadastroCONV_ID.AsString);
    QTodasCompras.Sql.Add(' and contacorrente.data between '+QuotedStr(DateTimeToStr(data1.Date))+' and '+ QuotedStr(DateTimeToStr(data2.Date)));
    //QTodasCompras.Sql.Add(' and contacorrente.data between FORMAT('+DateTimeToStr(data1.Date)+','+QuotedStr('dd/mm/yyyy')+') AND FORMAT('+DateTimeToStr(data2.Date)+','+QuotedStr('dd/mm/yyyy')+')');
    //FORMAT('+QuotedStr(DateToStr(data1.Date))+','+QuotedStr('dd/mm/yyyy')') and FORMAT('+QuotedStr(DateToStr(data2.Date))+','+QuotedStr('dd/mm/yyyy')')');
    QTodasCompras.Sql.Add(' order by contacorrente.data desc ');
    QTodasCompras.Open;
    JvDBGrid3.SetFocus;
  end;
end;

procedure TFCadConv.btnFirstDClick(Sender: TObject);
begin
  inherited;
  if QCartoes.Active = True then
    if QCartoes.RecordCount > 0 then
    QCartoes.First;
end;

procedure TFCadConv.btnPriorDClick(Sender: TObject);
begin
  inherited;
  if QCartoes.Active = True then
    if QCartoes.RecordCount > 0 then
    QCartoes.Prior;
end;

procedure TFCadConv.btnNextDClick(Sender: TObject);
begin
  inherited;
  if QCartoes.Active = True then
    if QCartoes.RecordCount > 0 then
    QCartoes.Next;
end;

procedure TFCadConv.btnLastDClick(Sender: TObject);
begin
  inherited;
  if QCartoes.Active = True then
    if QCartoes.RecordCount > 0 then
    QCartoes.Last;
end;

procedure TFCadConv.DSCartoesDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  btnFirstD.Enabled:= (QCartoes.State = dsBrowse) and not (QCartoes.Bof);
  btnPriorD.Enabled:= (QCartoes.State = dsBrowse) and not (QCartoes.Bof);
  btnNextD.Enabled:= (QCartoes.State = dsBrowse) and not (QCartoes.Eof);
  btnLastD.Enabled:= (QCartoes.State = dsBrowse) and not (QCartoes.Eof);
end;

procedure TFCadConv.btnFirstEClick(Sender: TObject);
begin
  inherited;
  QHistCartoes.First;
end;

procedure TFCadConv.btnPriorEClick(Sender: TObject);
begin
  inherited;
  QHistCartoes.Prior;
end;

procedure TFCadConv.btnNextEClick(Sender: TObject);
begin
  inherited;
  QHistCartoes.Next;
end;

procedure TFCadConv.btnLastEClick(Sender: TObject);
begin
  inherited;
  QHistCartoes.Last;
end;

procedure TFCadConv.btnVisulHistCartClick(Sender: TObject);
var cadastro, field : string;
begin
  if QCartoes.IsEmpty then msginf('N�o h� cart�es a serem pesquisados.')
  else
  begin
    QHistCartoes.Close;
    QHistCartoes.Sql.Clear;
    QHistCartoes.Sql.Add(' Select * from logs ');
    QHistCartoes.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('dd/mm/yyyy 00:00:00',DatainiCartao.Date))+' and '+QuotedStr(FormatDateTime('dd/mm/yyyy 23:59:59',DatafimCartao.Date)));
    QHistCartoes.Sql.Add(' and ID in ('+PegaCartoesID+')');
    QHistCartoes.Sql.Add(' and JANELA = ''FCadCartoes''');
    if DBCampo.ItemIndex > 0 then
    begin
      cadastro := Copy(CBCamposHistCartao.Text,1,Pos('.',CBCamposHistCartao.Text)-1);
      field    := Copy(CBCamposHistCartao.Text,Pos('.',CBCamposHistCartao.Text)+1,Length(CBCamposHistCartao.Text));
      QHistCartoes.Sql.Add(' and CAMPO = '+QuotedStr(field));
    end;
    QHistCartoes.Sql.Add(' order by data_hora desc ');
    QHistCartoes.Sql.Text;
    QHistCartoes.Open;
  end;
end;

procedure TFCadConv.DSHistoricoDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  btnFirstF.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnPriorF.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnNextF.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Eof);
  btnLastF.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Eof);
end;

procedure TFCadConv.DSHistCartoesDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnFirstE.Enabled:= (QHistCartoes.State = dsBrowse) and not (QHistCartoes.Bof);
  btnPriorE.Enabled:= (QHistCartoes.State = dsBrowse) and not (QHistCartoes.Bof);
  btnNextE.Enabled:= (QHistCartoes.State = dsBrowse) and not (QHistCartoes.Eof);
  btnLastE.Enabled:= (QHistCartoes.State = dsBrowse) and not (QHistCartoes.Eof);
end;

procedure TFCadConv.SpeedButton1Click(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: TXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  if (DBEdit11.Text = '') or (Length(SoNumero(DBEdit11.Text)) <> 8) then
  begin
    Application.MessageBox('CEP nulo ou inv�lido.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
    exit;
  end;
  Resposta   := TStringStream.Create('');
  TSConsulta := TStringList.Create;
  IdHTTP1:= TIdHTTP.Create(Self);
  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
  TSConsulta.Values['&cep'] := SoNumero(DBEdit11.Text);
  TSConsulta.Values['&formato'] := 'xml';
  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
  TSConsulta.Free;
  IdHTTP1.Free;
  XMLDocCEP:= TXMLDocument.Create(self);
  XMLDocCEP.Active := True;
  XMLDocCEP.Encoding := 'iso-8859-1';
  XMLDocCEP.LoadFromStream(Resposta);
  try
    try
      QCadastro.Edit;
      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
                                                 ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
      //QCadastro.FieldByName('BAIRRO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
      //QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
      //QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
    except
      msginf('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
    end;
  finally
    Resposta.Free;
    XMLDocCEP.Active := False;
    XMLDocCEP.Free;
  end;
  DBEdit10.SetFocus;
end;

procedure TFCadConv.btnFirstCClick(Sender: TObject);
begin
  inherited;
  if (QContaCorrente.Parameters[0].Value = QCadastroCONV_ID.AsInteger) and (QContacorrente.Active) then
    Qcontacorrente.First;
end;

procedure TFCadConv.btnPriorCClick(Sender: TObject);
begin
  inherited;
  if (QContaCorrente.Parameters[0].Value = QCadastroCONV_ID.AsInteger) and (QContacorrente.Active) then
  Qcontacorrente.Prior;
end;

procedure TFCadConv.btnNextCClick(Sender: TObject);
begin
  inherited;
  if (QContaCorrente.Parameters[0].Value = QCadastroCONV_ID.AsInteger) and (QContacorrente.Active) then
  Qcontacorrente.Next;
end;

procedure TFCadConv.btnLastCClick(Sender: TObject);
begin
  inherited;
  if (QContaCorrente.Parameters[0].Value = QCadastroCONV_ID.AsInteger) and (QContacorrente.Active) then
  Qcontacorrente.Last;
end;

procedure TFCadConv.DSContaCorrenteDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnFirstG.Enabled:= (Qcontacorrente.State = dsBrowse) and not (Qcontacorrente.Bof);
  btnPriorG.Enabled:= (Qcontacorrente.State = dsBrowse) and not (Qcontacorrente.Bof);
  btnNextG.Enabled:= (Qcontacorrente.State = dsBrowse) and not (Qcontacorrente.Eof);
  btnLastG.Enabled:= (Qcontacorrente.State = dsBrowse) and not (Qcontacorrente.Eof);
end;

procedure TFCadConv.tabTransacoesHide(Sender: TObject);
begin
  inherited;
  qProdTrans.Close;
  qValGrupDesc.Close;
  datainiPbm.Date := 0;
  datafimPbm.Date := 0;
end;

procedure TFCadConv.tabTransacoesShow(Sender: TObject);
begin
  inherited;
  datainiPbm.date := StartOfTheMonth(date);
  datafimPbm.date := IncDay(IncMonth(datainiPbm.date),-1);
  datainiPbm.SetFocus;
  qValGrupDesc.Close;
  qValGrupDesc.Parameters[0].Value := QCadastro.FieldByName('CONV_ID').AsInteger;
  qValGrupDesc.Open;
  qProdTrans.Close;
  qProdTrans.Parameters[0].Value := QCadastro.FieldByName('CONV_ID').AsInteger;
  qProdTrans.Open;
end;

procedure TFCadConv.TabProgDescHide(Sender: TObject);
begin
  inherited;
  qProgram.Close;
  qPbm.Close;

end;

procedure TFCadConv.TabProgDescShow(Sender: TObject);
begin
  inherited;
  qPbm.Close;
  qPbm.Parameters[0].Value:= QCadastro.FieldByName('CONV_ID').AsInteger;
  QPbm.Parameters[1].Value:= QCadastro.FielDByName('CONV_ID').AsInteger;
  qPbm.Open;
  qProgram.Open;
end;

procedure TFCadConv.dbEdtChapaKeyPress(Sender: TObject; var Key: Char);
begin
if not (key in ['0'..'9',',',#8,#13]) then Key := #0;
  inherited;
end;

procedure TFCadConv.QLimSegBeforePost(DataSet: TDataSet);
begin
  inherited;
  if QLimSegPORCENT.AsFloat < 0 then
    QLimSegPORCENT.AsFloat := 0;
end;

procedure TFCadConv.DSCadastroDataChange(Sender: TObject; Field: TField);
var B : Boolean;
begin
  inherited;
  if QCadastroEMPRES_ID.AsString <> '' then begin
    B := receita_sem_limite(qCadastroEMPRES_ID.AsInteger);
    lblGastocRec.Visible  := B;
    lblGastosRec.Visible  := B;
    dbEdtGastocRec.Visible := B;
    DBEdtGastosRec.Visible := B;
  end;
  if not QCadastroCONV_ID.IsNull then begin
    if (not (QCadastro.State in [dsInsert,dsEdit]) and (not (qBandConv.Modified) )) then begin
      qBandConv.Close;
      qBandConv.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
      qBandConv.Open;

    end;
  end;
end;

procedure TFCadConv.btncadClick(Sender: TObject);
begin
  inherited;
  qConv.Open;
  tExcel1.Open;
  tExcel1.First;
  while not tExcel1.Eof do begin
    if Trim(tExcel1NOMEDODEPENDENTE.AsString) = '' then begin
      if not (qConv.Locate('CHAPA',tExcel1CHAPA.AsInteger,[])) then begin
        qConv.Append;
        //QCadastroCONV_ID.
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Add('select max(conv_id) as conv_id + 1 from conveniados');
        DMConexao.AdoQry.Open;
        qConvCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qConvEMPRES_ID.AsInteger := 390;
        qConvBANCO.AsInteger := 0;
        qConvGRUPO_CONV_EMP.AsInteger := 1;
        qConvCHAPA.AsString := Trim(tExcel1CHAPA.AsString);
        qConvSENHA.AsString := '9C8F89FD6C';
        qConvTITULAR.AsString := TrimRight(tExcel1NOMEFUNCIONRIO.AsString);
        qConvCONTRATO.AsInteger := qConvCONV_ID.AsInteger;
        qConvLIMITE_MES.AsFloat := 200.00;
        qConvSETOR.AsString := '';
        qConvLIBERADO.AsString := 'S';
        qConvFIDELIDADE.AsString := 'N';
        qConvAPAGADO.AsString := 'N';
        //QCadastroDT_NASCIMENTO.AsString := ;
        qConvCPF.AsString := '';
        qConvTELEFONE1.AsString := '';
        qConvGRUPO_CONV_EMP.AsInteger := 3;
        qConvDTCADASTRO.AsString  := FormatDateTime('DD/MM/YYYY',now);
        qConvDTALTERACAO.AsString := FormatDateTime('DD/MM/YYYY',now);
        //CONVENIADOS.CONTRATO := conv_id
        qConv.Post;
      end;
    end;
    tExcel1.Next;
  end;

  tExcel2.Open;
  tExcel2.First;
  while not tExcel2.Eof do begin
    if Trim(tExcel2NOMEDODEPENDENTE.AsString) = '' then begin
      if not (qConv.Locate('CHAPA',tExcel2CHAPA.AsInteger,[])) then begin
        qConv.Append;
        //QCadastroCONV_ID.
        DMConexao.AdoQry.SQL.Add('select max(conv_id) as conv_id + 1 from conveniados');
        DMConexao.AdoQry.Open;
        qConvCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qConvEMPRES_ID.AsInteger := 390;
        qConvBANCO.AsInteger := 0;
        qConvGRUPO_CONV_EMP.AsInteger := 1;
        qConvCHAPA.AsString := Trim(tExcel2CHAPA.AsString);
        qConvSENHA.AsString := '9C8F89FD6C';
        qConvTITULAR.AsString := TrimRight(tExcel2NOMEFUNCIONRIO.AsString);
        qConvCONTRATO.AsInteger := qConvCONV_ID.AsInteger;
        qConvLIMITE_MES.AsFloat := 200.00;
        qConvSETOR.AsString := '';
        qConvLIBERADO.AsString := 'S';
        qConvFIDELIDADE.AsString := 'N';
        qConvAPAGADO.AsString := 'N';
        //QCadastroDT_NASCIMENTO.AsString := ;
        qConvCPF.AsString := '';
        qConvTELEFONE1.AsString := '';
        qConvGRUPO_CONV_EMP.AsInteger := 4;
        //CONVENIADOS.CONTRATO := conv_id
        qConv.Post;
      end;
    end;
    tExcel2.Next;
  end;

  tExcel3.Open;
  tExcel3.First;
  while not tExcel3.Eof do begin
    if Trim(tExcel3NOMEDODEPENDENTE.AsString) = '' then begin
      if not (qConv.Locate('CHAPA',tExcel3CHAPA.AsInteger,[])) then begin
        qConv.Append;
        //QCadastroCONV_ID.
        DMConexao.AdoQry.SQL.Add('select max(conv_id) as conv_id + 1 from conveniados');
        DMConexao.AdoQry.Open;
        qConvCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qConvEMPRES_ID.AsInteger := 390;
        qConvBANCO.AsInteger := 0;
        qConvGRUPO_CONV_EMP.AsInteger := 1;
        qConvCHAPA.AsString := Trim(tExcel3CHAPA.AsString);
        qConvSENHA.AsString := '9C8F89FD6C';
        qConvTITULAR.AsString := TrimRight(tExcel3NOMEFUNCIONRIO.AsString);
        qConvCONTRATO.AsInteger := qConvCONV_ID.AsInteger;
        qConvLIMITE_MES.AsFloat := 200.00;
        qConvSETOR.AsString := '';
        qConvLIBERADO.AsString := 'S';
        qConvFIDELIDADE.AsString := 'N';
        qConvAPAGADO.AsString := 'N';
        //QCadastroDT_NASCIMENTO.AsString := ;
        qConvCPF.AsString := '';
        qConvTELEFONE1.AsString := '';
        qConvGRUPO_CONV_EMP.AsInteger := 5;
        //CONVENIADOS.CONTRATO := conv_id
        qConv.Post;
      end;
    end;
    tExcel3.Next;
  end;
  showmessage('fim');
end;

procedure TFCadConv.Button1Click(Sender: TObject);
begin
  inherited;
  if validarCartao(QCartoesCODCARTIMP.AsString) then
    showmessage('OK')
  else
    showmessage('Car�o Inv�lido');
end;

procedure TFCadConv.dbCbUsaLimiteDifClick(Sender: TObject);
begin
  inherited;
  MostrarLimiteDiferencial(PageControl2.ActivePageIndex);
end;

procedure TFCadConv.edtLimite1_Change(Sender: TObject);
begin
  inherited;
  if not ( QCadastro.State in [dsEdit,dsInsert]) then
    QCadastro.Edit;
end;

procedure TFCadConv.DSBandConvDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (not (QCadastro.State in [dsInsert,dsEdit]) and (qBandConv.Modified)) then
    QCadastro.Edit;
end;

procedure TFCadConv.qBandConvAfterPost(DataSet: TDataSet);
begin
  inherited;
  QCadastro.Edit;
  QCadastroLIMITE_MES.AsCurrency := qBandConvLIMITE_1.AsCurrency + qBandConvLIMITE_2.AsCurrency + qBandConvLIMITE_3.AsCurrency + qBandConvLIMITE_4.AsCurrency;
  QCadastro.Post; 
end;

procedure TFCadConv.TabSituacaoHide(Sender: TObject);
begin
  {inherited;
  if QSituacaoNew.Active then
    QSituacaoNew.Close;
  if QSitFornBloq.Active then
    QSitFornBloq.Close;
  if QSitSeg.Active then
    QSitSeg.Close;
  if QSitGProd.Active then
    QSitGProd.Close; }
end;

procedure TFCadConv.TabSheet6Show(Sender: TObject);
begin
  inherited;
  Cursor := crHourGlass;
  QSitFornBloq.Close;
  QSitFornBloq.Parameters[0].Value:= QCadastroEMPRES_ID.AsInteger;
  QSitFornBloq.Open;

  QSitSeg.Close;
  QSitSeg.Parameters[0].Value:= QCadastroCONV_ID.AsInteger;
  QSitSeg.Open;

  QSitGProd.Close;
  QSitGProd.Parameters[0].Value:= QCadastroEMPRES_ID.AsInteger;
  QSitGProd.Open;
  Cursor := crDefault;
end;

procedure TFCadConv.DBMemo1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  Key := AnsiUpperCase( Key )[1];
end;

procedure TFCadConv.DSEstadosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not qCidades.Locate('NOME',dbLkpCidades.KeyValue,[])) then
  begin
    QCidades.Close;
    QCidades.Parameters.ParamByName('ESTADO_ID').Value := dbLkpEstados.KeyValue;
    QCidades.Open;
  end;
end;

procedure TFCadConv.FormShow(Sender: TObject);
begin
  inherited;
  //TabSaldos.TabVisible := False;
  //TabOutras.TabVisible := False;
end;


procedure TFCadConv.QconveniadoBeforePost(DataSet: TDataSet);
var mens, verifica_cpf : string;
    colocouMensagem : Boolean;
  SQL : TSqlMount;
begin
  //colocouMensagem := DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Altera��o',Self.Caption,iif(Trim(QCadastro.FieldByName(chavepri).AsString) = '','NULL',QCadastro.FieldByName(chavepri).AsString),StringReplace(detalhe,': ','',[rfReplaceAll]),'', '');
  //inherited;
  valida;
 {if (not colocouMensagem) then begin
    Abort;
  end;  }
  QconveniadoFIDELIDADE.AsString:= UpperCase(QconveniadoFIDELIDADE.AsString);
  if (QCadastroTIPO_CREDITO.AsInteger = 2) then
    QCadastroDATA_ATUALIZACAO_ACUMULADO.AsDateTime := now;
  if (QconveniadoFIDELIDADE.AsString = 'S') and (Qconveniado.State <> dsInsert) then
  begin
    if (QconveniadoFIDELIDADE.OldValue <> QconveniadoFIDELIDADE.Value) then
    begin
      if DMConexao.ExecuteScalar('select hist_id from fidel_historico where historico = ''PONTOS CADASTRO'' and conv_id = ' + QconveniadoCONV_ID.AsString,0) = 0 then
      begin
        DMConexao.AdoQry.Close;
        SQL := TSqlMount.Create(smtInsert,'FIDEL_HISTORICO');
        Sql.AddField('HIST_ID',DMConexao.getGeneratorValue('GEN_FIDEL_HISTORICO'),ftInteger);
        Sql.AddField('DATAHORA',Now,ftDateTime);
        Sql.AddField('CREDITO',DMConexao.ExecuteScalar('select distinct ptscadastro from fidel_config ',0),ftInteger);
        Sql.AddField('DEBITO',0,ftInteger);
        Sql.AddField('HISTORICO','PONTOS CADASTRO',ftString);
        Sql.AddField('CONV_ID',QCadastroCONV_ID.AsInteger,ftInteger);
        Sql.AddField('CANCELADO','N',ftString);
        DMConexao.AdoQry.SQL    := sql.GetSqlString;
        //DMConexao.AdoQry.Parameters := sql.GetParams;
        DMConexao.AdoQry.ExecSQL;
        SQL.Free;
      end;
    end;
  end;
  DMConexao.Config.Open;
  if (Qconveniado.State = dsEdit) and (QconveniadoTITULAR.OldValue <> QconveniadoTITULAR.AsString) and (DMConexao.ConfigALTERAR_TITULAR_NOME_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set nome = '+ QuotedStr(QconveniadoTITULAR.AsString)+'" where conv_id = '+QuotedStr(QconveniadoCONV_ID.AsString)+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  if (Qconveniado.State = dsEdit) and (QconveniadoLIBERADO.OldValue <> QconveniadoLIBERADO.AsString) and (DMConexao.ConfigALTERAR_TITULAR_LIBERADO_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set liberado = '+QuotedStr(QconveniadoLIBERADO.AsString)+' where conv_id = '+QconveniadoCONV_ID.AsString+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  verifica_cpf := DMConexao.ConfigVERIFICA_CPF_CONV.AsString;
  if (verifica_cpf[1] in ['C','P']) and (Trim(QconveniadoCPF.AsString) <> '') then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := 'Select conv_id from PESQUISA_CPF("'+QconveniadoCPF.AsString+'") where conv_id <> '+QconveniadoCONV_ID.AsString;
    DMConexao.AdoQry.Open;
    if not DMConexao.AdoQry.IsEmpty then
      while not DMConexao.AdoQry.Eof do
      begin
        mens := mens + DMConexao.AdoQry.Fields[0].AsString+sLineBreak;
        DMConexao.AdoQry.Next;
      end;
      DMConexao.AdoQry.Close;
      if Trim(mens) <> '' then
      begin
      if verifica_cpf = 'P' then
      begin
        if Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens+sLineBreak+'Deseja continuar o cadastro?'),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDNO then
          Sysutils.Abort;
      end
      else
      begin
        Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens),'Opera��o cancelada.',MB_OK+MB_ICONERROR);
        Sysutils.Abort;
      end;
    end;
  end;
  if Qconveniado.State = dsInsert then
    incluindo := True;
  if DMConexao.ConfigUSA_NOVO_FECHAMENTO.AsString = 'S' then
  begin
    if Qconveniado.State = dsInsert then
    begin
      if ((DBDateEdit2.Date > 0) and ((QConvDetailDATA_DEMISSAO.OldValue <> QConvDetailDATA_DEMISSAO.Value))) then
      begin
        if DMConexao.ConfigDEMISSAO_MOVE_AUTS.AsString = 'S' then
        begin
          MsgInf('As autoriza��es com fechamentos posteriores ser�o transportadas para o pr�ximo fechamento em aberto!');
          DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select datafecha from '+
          ' get_prox_fecha_aberto(current_timestamp,'+QconveniadoCONV_ID.AsString+
          ',0)) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' and conv_id = '+QconveniadoCONV_ID.AsString);
        end;
        QConvDetailSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Devedor',FormatDinBR(0),FormatDinBR(QCadastroSALDO_DEVEDOR.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
        QConvDetailSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Dev. Fat.',FormatDinBR(0),FormatDinBR(QCadastroSALDO_DEVEDOR_FAT.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
      end;
    end;
    if ((Qconveniado.State = dsEdit) and ((QCadastroDATA_DEMISSAO.OldValue <> QCadastroDATA_DEMISSAO.Value))) then
    begin
      if not QCadastroDATA_DEMISSAO.IsNull then
      begin
        QCadastroSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString,0);
        QCadastroSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString,0);
        if DMConexao.ConfigDEMISSAO_MOVE_AUTS.AsString = 'S' then
        begin
          if MsgSimNao('As autoriza��es com fechamentos posteriores a data atual '+sLineBreak+
                 'ser�o transportadas para o fechamento de '+FormatDataBR(DMConexao.ExecuteScalar('select datafecha from '+
                 'get_prox_fecha_aberto(current_timestamp,'+QCadastroCONV_ID.AsString+' , '+QCadastroEMPRES_ID.AsString+')'))+sLineBreak+
                 'correspondente ao pr�ximo fechamento n�o faturado da empresa'+sLineBreak+
                 'Confirma esta opera��o?') then
          begin
            DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select datafecha from '+
                           ' get_prox_fecha_aberto(current_timestamp,'+QCadastroCONV_ID.AsString+
                           ',0)) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' '+
                           ' and conv_id = '+ QCadastroCONV_ID.AsString + ' and data_fecha_emp > ' +
                           ' (select first 1 datafecha from get_prox_fecha_aberto(current_timestamp,' +
                             QCadastroCONV_ID.AsString + ',0))');
          end;
        end;
      end
      else
      begin
        QCadastroSALDO_DEVEDOR.AsCurrency:= 0;
        QCadastroSALDO_DEVEDOR_FAT.AsCurrency:= 0;
      end;
    end
  end;
  DMConexao.Config.Close; 


end;

function CalculaConsumoMes(val1: Double; val2: Double) : Double;
begin
  result := val1 + val2;
end;

procedure ExecutaQuery(Query : string ; Parametro : Variant);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(Query);
  DMConexao.AdoQry.Parameters[0].Value := Parametro;
  DMConexao.AdoQry.Open;
end;

function verificaTipoCredito(empres_id : Integer): Integer ;
var t1 : Integer;
begin
  ExecutaQuery('SELECT e.tipo_credito from empresas e where e.EMPRES_ID = :empres_id',empres_id);
  Result := DMConexao.AdoQry.Fields[0].AsInteger;
end;

procedure TFCadConv.buscaConveniado;
begin
  QCadastro.Close;
  if ((Trim(EdCod.Text) = '') and (Trim(EdNome.Text) = '') and (Trim(EdChapa.Text) = '') and (Trim(EdCodEmp.Text) = '')
    and (Trim(EdNomeEmp.text) ='') and (Trim(EdCartao.Text) = '')) then
  begin
     MsgInf('� necess�rio especificar um crit�rio de busca.');
     EdCod.SetFocus;
     Exit;
  end;
  Screen.Cursor := crHourGlass;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('select');
  QCadastro.SQL.Add(' distinct conveniados.CONV_ID,');
  QCadastro.SQL.Add('  conveniados.TITULAR,');
  QCadastro.SQL.Add('  conveniados.LIBERADO,');
  QCadastro.SQL.Add('  conveniados.EMPRES_ID,');
  QCadastro.SQL.Add('  conveniados.GRUPO_CONV_EMP,');
  QCadastro.SQL.Add('  conveniados.CARGO,');
  QCadastro.SQL.Add('  conveniados.SETOR,');
  QCadastro.SQL.Add('  conveniados.COD_EMPRESA,');
  QCadastro.SQL.Add('  conveniados.DT_NASCIMENTO,');
  QCadastro.SQL.Add('  conveniados.CPF,');
  QCadastro.SQL.Add('  conveniados.RG,');
  QCadastro.SQL.Add('  conveniados.ENDERECO,');
  QCadastro.SQL.Add('  conveniados.NUMERO,');
  QCadastro.SQL.Add('  conveniados.BAIRRO,');
  QCadastro.SQL.Add('  conveniados.CIDADE,');
  QCadastro.SQL.Add('  conveniados.ESTADO,');
  QCadastro.SQL.Add('  conveniados.CEP,');
  QCadastro.SQL.Add('  conveniados.BANCO,');
  QCadastro.SQL.Add('  conveniados.AGENCIA,');
  QCadastro.SQL.Add('  conveniados.CONTACORRENTE,');
  QCadastro.SQL.Add('  conveniados.DIGITO_CONTA,');
  QCadastro.SQL.Add('  conveniados.TIPOPAGAMENTO,');
  QCadastro.SQL.Add('  conveniados.TELEFONE1,');
  QCadastro.SQL.Add('  conveniados.TELEFONE2,');
  QCadastro.SQL.Add('  conveniados.CELULAR,');
  QCadastro.SQL.Add('  conveniados.OBS1,');
  QCadastro.SQL.Add('  conveniados.OBS2,');
  QCadastro.SQL.Add('  conveniados.DTCADASTRO,');
  QCadastro.SQL.Add('  conveniados.OPERCADASTRO,');
  QCadastro.SQL.Add('  conveniados.DTALTERACAO,');
  QCadastro.SQL.Add('  conveniados.OPERADOR,');
  QCadastro.SQL.Add('  conveniados.DTULTCESTA,');
  QCadastro.SQL.Add('  conveniados.DTASSOCIACAO,');
  QCadastro.SQL.Add('  conveniados.EMAIL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_MES,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_1,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_2,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_3,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_4,');
  QCadastro.SQL.Add('  conveniados.LIMITE_TOTAL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_PROX_FECHAMENTO,');
  QCadastro.SQL.Add('  conveniados.CESTABASICA,');
  QCadastro.SQL.Add('  conveniados.SALARIO,');
  QCadastro.SQL.Add('  conveniados.FIDELIDADE,');
  QCadastro.SQL.Add('  conveniados.CONTRATO,');
  QCadastro.SQL.Add('  conveniados.TIPOSALARIO,');
  QCadastro.SQL.Add('  conveniados.SETOR_ID,');
  QCadastro.SQL.Add('  conveniados.FLAG,');
  QCadastro.SQL.Add('  conveniados.DTAPAGADO,');
  QCadastro.SQL.Add('  conveniados.APAGADO,');
  QCadastro.SQL.Add('  conveniados.VALE_DESCONTO,');
  QCadastro.SQL.Add('  conveniados.LIBERA_GRUPOSPROD,');
  QCadastro.SQL.Add('  conveniados.COMPLEMENTO,');
  QCadastro.SQL.Add('  conveniados.USA_SALDO_DIF,');
  QCadastro.SQL.Add('  conveniados.ABONO_MES,');
  QCadastro.SQL.Add('  conveniados.SALDO_RENOVACAO,');
  QCadastro.SQL.Add('  conveniados.SALDO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.DATA_ATUALIZACAO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.CHAPA,');
  //FOI NECESS�RIO ACRESCENTAR A LINHA ABAIXO AP�S TER ACRESENTADO
  //O CONV_DETAIL_CONVID - DEVE SER ASSIM SEMPRE QUE ACRESENTADO
  //UM NOVO CAMPO NO DATASET.
  //QCadastro.SQL.ADD('  conveniados.CONV_ID,');
  QCadastro.SQL.Add('  conveniados.DATA_ADMISSAO,');
  QCadastro.SQL.Add('  conveniados.DATA_DEMISSAO,');
  QCadastro.SQL.Add('  conveniados.NUM_DEPENDENTES,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR_FAT,');
  QCadastro.SQL.Add('  conveniados.PIS,');
  QCadastro.SQL.Add('  conveniados.NOME_PAI,');
  QCadastro.SQL.Add('  conveniados.NOME_MAE,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_NUM,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_SERIE,');
  QCadastro.SQL.Add('  conveniados.REGIME_TRAB,');
  QCadastro.SQL.Add('  conveniados.VENC_TOTAL,');
  QCadastro.SQL.Add('  conveniados.MODELO_CARTAO_CANTINEX,');
  QCadastro.SQL.Add('  conveniados.SERIE,');
  QCadastro.SQL.Add('  conveniados.FIM_CONTRATO,');
  QCadastro.SQL.Add('  conveniados.DISTRITO,');
  QCadastro.SQL.Add('  conveniados.ESTADO_CIVIL,');
  QCadastro.SQL.Add('  empresas.FANTASIA,');
  QCadastro.SQL.Add('  empresas.BAND_ID,');
  QCadastro.SQL.Add('  empresas.nome empresa,');
  QCadastro.SQL.Add('  empresas.TIPO_CREDITO,');
  QCadastro.SQL.Add('  UPPER(ba.descricao) as NOMEBAIRRO,');
  QCadastro.SQL.Add('  UPPER(ci.nome) as NOMECIDADE,');
  QCadastro.SQL.Add('  es.UF as NOMEESTADO');
  QCadastro.SQL.Add('  from conveniados');
  //QCadastro.SQL.Add('left join conv_detail on conv_detail.conv_id = conveniados.conv_id');
  QCadastro.SQL.Add('  join empresas on empresas.empres_id = conveniados.empres_id and empresas.apagado <> ''S''');
  QCadastro.SQL.Add('  left join bairros ba on (ba.bairro_id = conveniados.bairro) ');
  QCadastro.SQL.Add('  left join cidades ci on (ci.cid_id = conveniados.cidade) ');
  QCadastro.SQL.Add('  left join estados es on (es.estado_id = conveniados.estado) ');
  QCadastro.SQL.Add('  join cartoes on cartoes.conv_id = conveniados.conv_id and cartoes.apagado = ''N''');
  QCadastro.SQL.Add('  where 1=1 ');
  if ((Trim(EdCartao.Text) <> '') or (Trim(EdNome.Text) <> '')) then
  begin
    QCadastro.Sql.Add(' and cartoes.nome like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
  end;
  if cbbLiberado.ItemIndex > 0 then
  begin
    if cbbLiberado.ItemIndex = 1 then
      QCadastro.SQL.Add(' and conveniados.liberado = ''S'' and conveniados.apagado <> ''S''')
    else if cbbLiberado.ItemIndex = 2 then
      QCadastro.SQL.Add(' and conveniados.liberado = ''N'' and conveniados.apagado <> ''S''')
        else begin
          QCadastro.SQL.Add(' and conveniados.apagado = ''S''');
        end;
  end
  else 
  begin
    QCadastro.SQL.Add(' and conveniados.apagado <> ''S''');
  end;

  if Trim(EdCod.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.conv_id in (' + fnRemoverCaracterSQLInjection(EdCod.Text) + ')');
  if Trim(EdBuscaCartao.Text) <> '' then
    QCadastro.SQL.Add(' and cartoes.cartao_id in (' + fnRemoverCaracterSQLInjection(EdBuscaCartao.Text) + ')');
  //if Trim(EdNome.Text) <> '' then
    //QCadastro.Sql.Add(' and conveniados.titular like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
    //QCadastro.Sql.Add(' and cartoes.nome like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
  if Trim(EdChapa.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.chapa in (' + fnRemoverCaracterSQLInjection(EdChapa.Text) + ')');
  if Trim(EdCodEmp.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.empres_id in (' + fnRemoverCaracterSQLInjection(EdCodEmp.Text) + ')');
  if Trim(EdNomeEmp.text) <>''then
  begin
    DMConexao.Config.Open;
    if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
      QCadastro.Sql.Add(' and empresas.fantasia like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''')
    else
      QCadastro.Sql.Add(' and empresas.nome like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''');
    DMConexao.Config.Close;
  end;
  if Trim(EdCartao.Text) <> '' then
  begin
    if (( Length(Trim(EdCartao.Text)) = 11) and ( DigitoCartao(StrToFloat(Copy(EdCartao.Text,1,9))) = StrToInt(Copy(EdCartao.Text,10,2)))) then
      QCadastro.Sql.Add(' and (cartoes.codigo in (' + Copy( fnRemoverCaracterSQLInjection(EdCartao.Text),1,9) + ')' + ' or cartoes.codcartimp in (''' + fnRemoverCaracterSQLInjection(EdCartao.Text) + ''')))')
    else begin
      EdCartao.Text := fnRemoverCaracterSQLInjection(StringReplace(EdCartao.Text,',','","',[]));
      QCadastro.Sql.Add(' and cartoes.codcartimp in ('''+fnRemoverCaracterSQLInjection(EdCartao.Text)+''')');
    end;
  end;
  QCadastro.Sql.Add(' order by conveniados.titular ');
  //clipBoard.AsText := QCadastro.SQL.Text;
  Barra.Panels[0].Text := 'buscando conveniados';
  QCadastro.Open;
  Barra.Panels[0].Text := 'busca de conveniados conclu�da';
  //QCadastro.EnableControls;
  if not QCadastro.IsEmpty then
  begin
    DBGrid1.SetFocus;
    Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
    '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;

  end
  else
    EdCod.SetFocus;
  LimpaEdits;

  Screen.Cursor := crDefault;
end;

procedure TFCadConv.DBEmpresaChange(Sender: TObject);
begin
  inherited;
  edtEmpr.Text := QEmpresaempres_id.AsString;

end;

procedure TFCadConv.edtEmprChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEmpr.Text) <> '' then begin
     if QEmpresa.Locate('empres_id',edtEmpr.Text,[]) then
        DBEmpresa.KeyValue := edtEmpr.Text
     else begin
        DBEmpresa.ClearValue;
        {AposEmpEstabNull;
        cds.First;
        while not cds.Eof do
          cds.Delete;}
     end;
  end
  else begin
    DBEmpresa.ClearValue;
    {AposEmpEstabNull;
    cds.First;
    while not cds.Eof do
      cds.Delete; }
  end;
end;

procedure TFCadConv.edtEmprKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFCadConv.FormDeactivate(Sender: TObject);
begin
  inherited;
end;

{DONE -O[SIDNEI] -C[Implementar filtro caso o conveniado tenha tipos diferentes de limites]
:TabSituacaoShow}
{DONE -O[SIDNEI] -C[Implementar Limite 2, Limite 3 ]
:TabSituacaoShow}
procedure TFCadConv.TabSituacaoShow(Sender: TObject);
var EmpresId, ConvId : string;
    tipoCredito, i : Integer;
    qtdLimites : Integer;
    usa_lim_dif : String;
begin
  inherited;
  EmpresId := QCadastroEMPRES_ID.AsString;
  ConvId := QCadastroCONV_ID.AsString;
  {Bloco para fazer atribui��o a QCadastroQTD_Limites
  A Function ExecutaQuery foi criado com o intuito de evitar repeti��o no preparo
  do componente ADOQUERY para rodar uma determinada Query}
  if(EmpresId <> '') then
  begin
    ExecutaQuery('SELECT qtd_limites from BANDEIRAS WHERE BAND_ID = (SELECT band_id FROM EMPRESAS WHERE EMPRES_ID = :EmpresId)',EmpresId);
    //QCadastro.Edit; //Colocando a Query Qcadastro em modo de edi��o
    QSituacao_New.SQL.Clear;
    //QCadastroQTD_Limites.AsInteger := DMConexao.AdoQry.Fields[0].Value;
    qtdLimites := DMConexao.AdoQry.Fields[0].Value;

    //Verifica se o conveniado usa saldo diferenciado
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT usa_saldo_dif from conveniados WHERE conv_id = '+ConvId);
    DMConexao.AdoQry.Open;
    usa_lim_dif := DMConexao.AdoQry.fields[0].Value;
    DMConexao.AdoQry.Close;

    QSituacao_New.SQL.Clear;
    QSituacao.Parameters.Clear;
    //INICIO DA QUERY DIN�MICA
    QSituacao_New.SQL.Add('SELECT C.conv_id, C.titular,C.chapa,');
    QSituacao_new.SQL.Add('case when coalesce(c.liberado, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as liberado,');
    QSituacao_new.SQL.Add(' (SELECT cd.data_demissao FROM conv_detail cd WHERE cd.conv_id = '+ConvId+') as demissao,');
    if(QCadastroBand_Id.AsInteger = 999)then
    // Todo retorno referente a limite_1,consumo_m�s_1 e saldo_restante_1
    //� tratado em um condicional tendo em vista que esses campos s�o em comum
    //tanto para band_id=1 como band_id = 999 o tratamento acontece na linha
    //4789,4799,4807 e 4823
    begin
      if(usa_lim_dif = 'S')Then
      begin
        //bc � referenete a tabela bandeiras conveniados onde fica contido os limites
        //de quem possui o limite diferenciado.
        QSituacao_new.SQL.Add(' bc.limite_1 as limite_1, coalesce(c.consumo_mes_1,0) as consumo_mes_1,');
        QSituacao_New.SQL.Add(' (SELECT (SELECT bc.limite_1 AS limite_1) + CASE WHEN e.tipo_credito in(2,3) THEN');
        QSituacao_New.SQL.Add(' coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = ''N'' THEN 0 else 0 END END - (SELECT coalesce(conv.CONSUMO_MES_1,0))');
        QSituacao_New.SQL.Add(' from conveniados conv join empresas e on e.EMPRES_ID = '+EmpresId+' AND CONV.CONV_ID = '+ConvId+' join BANDEIRAS_CONV bc ON bc.conv_id = conv.conv_id) AS saldo_restante,');
      end
      else
      begin
        QSituacao_New.SQL.Add(' c.limite_mes as limite_1, coalesce(c.consumo_mes_1,0) as consumo_mes_1,');
        QSituacao_New.SQL.Add(' (SELECT (SELECT conv.limite_mes AS limite_1) + CASE WHEN e.tipo_credito in(2,3) THEN');
        QSituacao_New.SQL.Add(' coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = ''N'' THEN 0 else 0 END END - (SELECT coalesce(conv.CONSUMO_MES_1,0))');
        QSituacao_New.SQL.Add(' from conveniados conv join empresas e on e.EMPRES_ID = '+EmpresId+' AND CONV.CONV_ID = '+ConvId+') AS saldo_restante,');
        //QSituacao_New.SQL.Add(' from conveniados WHERE conv_id = '+ConvId);
      end;
    end;
    QSituacao_New.SQL.Add(' coalesce(saldo_acumulado,0) as saldo_acumulado,coalesce(abono_mes,0) as abono_mes,');
    QSituacao_New.SQL.Add(' (SELECT top 1 data_fecha from dia_fecha WHERE data_fecha > CURRENT_TIMESTAMP and');
    QSituacao_New.SQL.Add(' EMPRES_ID = (SELECT conveniados.EMPRES_ID from conveniados where conv_id = '+ConvId+'))as fechamento,');

    //Calculando o valor da colunao saldo restante 1
    if(QCadastroBand_Id.AsInteger <> 999)then
    begin
      if(usa_lim_dif = 'S')Then
      begin
        QSituacao_New.SQL.Add(' (SELECT bc.limite_1 - coalesce(c.consumo_mes_1,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
        QSituacao_New.SQL.Add(' WHERE bc.CONV_ID = c.CONV_ID AND');
        QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante,');

        QSituacao_New.SQL.Add(' (SELECT bc.limite_2 - coalesce(c.consumo_mes_2,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
        QSituacao_New.SQL.Add(' WHERE bc.CONV_ID = c.CONV_ID AND');
        QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante2,');
         //Calculando o valor da colunao saldo restante 3
        QSituacao_New.SQL.Add(' (SELECT bc.limite_3 - coalesce(c.consumo_mes_3,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
        QSituacao_New.SQL.Add(' WHERE bc.CONV_ID = c.CONV_ID AND');
        QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante3,');

      end;
    end;
    if((QCadastroBand_Id.AsInteger <> 999) and (usa_lim_dif <> 'S'))then begin
      QSituacao_New.SQL.Add(' (SELECT b.limite_1 - coalesce(c.consumo_mes_1,0) from BANDEIRAS b, CONVENIADOS c');
      QSituacao_New.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+EmpresId+') AND');
      QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante,');
    end;

    if(usa_lim_dif <> 'S') then begin
      QSituacao_New.SQL.Add(' (SELECT b.limite_2 - coalesce(c.consumo_mes_2,0) from BANDEIRAS b, CONVENIADOS c');
      QSituacao_New.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+EmpresId+') AND');
      QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante2,');
       //Calculando o valor da colunao saldo restante 3
      QSituacao_New.SQL.Add(' (SELECT b.limite_3 - coalesce(c.consumo_mes_3,0) from BANDEIRAS b, CONVENIADOS c');
      QSituacao_New.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+EmpresId+') AND');
      QSituacao_New.SQL.Add(' c.conv_id = '+ConvId+') as saldo_restante3,');
    end;


    QSituacao_New.SQL.Add(' ca.codcartimp as CODCARTIMP,ca.nome AS NOME_CARTAO,ca.codigo as COD_CARTAO,');
    QSituacao_new.SQL.Add(' case when coalesce(ca.liberado, ''S'') = ''S'' then ''SIM'' else ''N�O'' end AS CART_LIB,');
    QSituacao_new.SQL.Add(' e.empres_id, e.nome as nome_empres,');
    QSituacao_new.SQL.Add(' case when coalesce(e.liberada, ''N'') = ''N'' OR coalesce(e.BLOQ_ATE_PGTO,''N'') = ''S'' then ''N�O'' else ''SIM'' END as emp_lib,');
    QSituacao_new.SQL.Add(' case when coalesce(e.todos_segmentos, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as todos_segmentos,');
    QSituacao_new.SQL.Add(' case when coalesce(e.aceita_parc, ''S'') = ''S'' OR');
    QSituacao_new.SQL.Add(' (select count(FORMAS_EMP_LIB.FORMA_ID) from FORMAS_EMP_LIB where FORMAS_EMP_LIB.EMP_ID = E.EMPRES_ID) > 1');
    QSituacao_new.SQL.Add(' then ''SIM'' else ''N�O'' end as aceita_parc,');
    QSituacao_new.SQL.Add(' case when coalesce(e.venda_nome, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as venda_nome');
    if(QCadastroBand_Id.AsInteger <> 999)then
    begin
      if(usa_lim_dif = 'S')Then begin
        QSituacao_new.SQL.Add(', coalesce(c.consumo_mes_1,0) as consumo_mes_1,coalesce(bc.limite_1,0) as limite_1,');
        QSituacao_New.SQL.Add(' coalesce(bc.limite_2,0) as limite_2, coalesce(bc.LIMITE_3,0) as limite_3');
      end
      else
      QSituacao_New.SQL.Add(', coalesce(b.limite_2,0) as limite_2, coalesce(b.LIMITE_3,0) as limite_3');
    end;
    if((QCadastroBand_Id.AsInteger <> 999) and (usa_lim_dif <> 'S')) then
    QSituacao_new.SQL.Add(', coalesce(c.consumo_mes_1,0) as consumo_mes_1,coalesce(b.limite_1,0) as limite_1');

    if(QCadastroBand_Id.AsInteger = 999) then begin
      QSituacao_New.SQL.Add(', coalesce(b.limite_2,0) as limite_2, coalesce(b.LIMITE_3,0) as limite_3');
    end;
    QSituacao_New.SQL.Add(', coalesce(c.consumo_mes_2,0) as consumo_mes_2,coalesce(c.consumo_mes_3,0) as consumo_mes_3');


     //QSituacao_new.SQL.Add(' INNER JOIN CONV_DETAIL AS cd ON cd.conv_id = '+ConvId);
     QSituacao_new.SQL.Add(' from CONVENIADOS as c INNER JOIN EMPRESAS AS e ON e.EMPRES_ID = c.empres_id');
     QSituacao_New.SQL.Add(' INNER JOIN CARTOES AS ca ON ca.CODIGO = (SELECT TOP 1 CA.CODIGO FROM CARTOES CA WHERE ca.conv_id = '+ConvId+' AND ca.TITULAR = ''S'')');
     QSituacao_New.SQL.Add(' INNER JOIN BANDEIRAS AS b ON b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+EmpresId+') AND c.conv_id = '+ConvId);
     if(usa_lim_dif = 'S') then
     QSituacao_New.SQL.Add(' INNER JOIN BANDEIRAS_CONV AS bc ON bc.conv_id = c.conv_id AND c.conv_id = '+ConvId);

     QSituacao_new.SQL.Text;
     //realinhando os DBEdits de acordo com o filtro aplicado
    //caso seja band_id 999 executa bloco abaixo
    tipoCredito := verificaTipoCredito(QCadastroEMPRES_ID.AsInteger);
    if(tipoCredito < 2)then
    begin
      lblSaldoRest.Left   := 224;
      dbEdtSaldoRest.Left := 224;
      lblFech.Left        := 332;
      dbEdtFech.Left      := 332;
      lblDemissao.Left    := 440;
      dbEdtDemissao.Left  := 440;
      lblAcumulado.Left   := 548;
      dbEdtAcumulado.Left := 548;
      dbEdtAbono.Left     := 656;
      lblAbono.Left       := 656;
      lblGastoCRec.Left   := 764;
      dbEdtGastoCRec.Left := 764;
      lblGastoSRec.Left   := 872;
      dbEdtGastoSRec.Left := 872;
    end
    //Empresas com tipo_credito > 2 organiza o DBEdit conforme abaixo
    else
    begin
      lblAcumulado.Left   := 224;
      dbEdtAcumulado.Left := 224;
      lblAbono.Left       := 332;
      dbEdtAbono.Left     := 332;
      lblSaldoRest.Left   := 440;
      dbEdtSaldoRest.Left := 440;
      lblFech.Left        := 548;
      dbEdtFech.Left      := 548;
      lblDemissao.Left    := 656;
      dbEdtDemissao.Left  := 656;
      lblGastoCRec.Left   := 764;
      dbEdtGastoCRec.Left := 764;
      lblGastoSRec.Left   := 872;
      dbEdtGastoSRec.Left := 872;
    end;

    if (VerificaEmpresaBloqueadaAtePagamento(QCadastroEMPRES_ID.AsInteger) = True) then
      lbl2.Visible := True
    else
      lbl2.Visible := False;

    QSituacao_New.Open;


     QSituacao_newconsumo_mes_2.Value;
     QSituacao_newsaldo_restante2.Value;
     QSituacao_newlimite_2.Value;
     QSituacao_newlimite_2.Value;
     QSituacao_newconv_id.Value;
     Label98.Visible  := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);
     Label100.Visible := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);
     Label102.Visible := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit3.Visible  := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit15.Visible := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit26.Visible := (qtdLimites >= 2) and (QCadastroBand_id.AsInteger <> 999);

     Label103.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);
     Label104.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);
     Label105.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit59.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit61.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit62.Visible := (qtdLimites >= 3) and (QCadastroBand_id.AsInteger <> 999);

     Label106.Visible := (qtdLimites>= 4) and (QCadastroBand_id.AsInteger <> 999);
     Label107.Visible := (qtdLimites >= 4) and (QCadastroBand_id.AsInteger <> 999);
     Label108.Visible := (qtdLimites >= 4) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit63.Visible := (qtdLimites >= 4) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit64.Visible := (qtdLimites >= 4) and (QCadastroBand_id.AsInteger <> 999);
     DBEdit65.Visible := (qtdLimites >= 4) and (QCadastroBand_id.AsInteger <> 999);

     dbEdtAcumulado.Visible := (QCadastroBand_id.AsInteger = 999) and (QCadastroTIPO_CREDITO.AsInteger in [2,3]);
     lblAcumulado.Visible   := (QCadastroBand_id.AsInteger = 999) and (QCadastroTIPO_CREDITO.AsInteger in [2,3]);
     dbEdtAbono.Visible     := (QCadastroBand_id.AsInteger = 999) and (QCadastroTIPO_CREDITO.AsInteger in [2,3]);
     lblAbono.Visible       := (QCadastroBand_id.AsInteger = 999) and (QCadastroTIPO_CREDITO.AsInteger in [2,3]);


     for i:=40 to ComponentCount-1 do
     if(Components[i] is TDBEdit) then
     begin
       if((TDBEdit(Components[i]).Text = 'SIM'))then
         begin
           TDBEdit(Components[i]).Font.Color := clNavy;
         end
       else if((TDBEdit(Components[i]).Text = 'N�O')) then
         begin
            TDBEdit(Components[i]).Font.Color := clRed;
         end;
     end;
     end;


end;

function TFCadConv.VerificaEmpresaBloqueadaAtePagamento(empres_id : Integer) : Boolean;
var bloq_ate_pgto : Variant;
begin
  bloq_ate_pgto := DMConexao.ExecuteScalar('select bloq_ate_pgto from empresas where empres_id = '+ IntToStr(empres_id));
  if bloq_ate_pgto = 'S' then
   Result := True
  else
    Result := False;
end;

procedure TFCadConv.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  {Recuperando o �ltimo ID do conveniado}
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONV_ID AS CONV_ID');
  DMConexao.AdoQry.Open;
  QCadastroCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  {Fim do bloco para recupera��o do ID}
  QCadastroLIBERADO.AsString          := 'S';
  QCadastroFIDELIDADE.AsString        := 'N';
  QCadastroLIMITE_MES.AsCurrency      := 0;
  QCadastroLIMITE_TOTAL.AsCurrency    := 0;
  QCadastroSALARIO.AsCurrency         := 0;
  QCadastroAPAGADO.AsString           := 'N';
  QCadastroVALE_DESCONTO.AsString     := 'N';
  QCadastroLIBERA_GRUPOSPROD.AsString := 'N';
  QCadastroUSA_SALDO_DIF.AsString     := 'N';
  QCadastroSALDO_RENOVACAO.AsCurrency := 0;
  QCadastroABONO_MES.AsCurrency       := 0;
  QCadastroSALDO_ACUMULADO.AsCurrency := 0;
  QCadastroCONSUMO_MES_1.AsCurrency     := 0;
  QCadastroCONSUMO_MES_2.AsCurrency     := 0;
  QCadastroCONSUMO_MES_3.AsCurrency     := 0;
  QCadastroCONSUMO_MES_4.AsCurrency     := 0;
  //QCadastroSENHA.AsString               := '0000';
  QCadastroCONTRATO.AsInteger := QCadastroCONV_ID.AsInteger;
  QCadastroOPERADOR.AsString  := Operador.Nome;
  QCadastroOPERCADASTRO.Value := Operador.Nome;
  QCadastroDTCADASTRO.Value := now;
end;

procedure TFCadConv.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
                                          '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;
  if QCadastro.IsEmpty then Self.TextStatus := '';
  if not QCadastro.IsEmpty then
  begin
    QConvDetail.Close;
    QConvDetail.Sql.Text := ' select * from CONV_DETAIL  where conv_id = '+QCadastroCONV_ID.AsString;
    QConvDetail.Open;
    QGrupo_conv_emp.Close;
    QGrupo_conv_emp.Parameters.ParamByName('empres_id').Value := QCadastroEMPRES_ID.AsInteger;
    QGrupo_conv_emp.Open;
    QEmp_Dpto.Close;
    QEmp_Dpto.Parameters.ParamByName('empres_id').Value := QCadastroEMPRES_ID.AsInteger;
    QEmp_Dpto.Open;
    DBEmpresa.DataField := '';
    DBEmpresa.KeyValue := QCadastroEMPRES_ID.Value;
    DBEmpresa.DataField := 'empres_id';
    //*************************************************************
  end;
  QCartoes.Close;
  Qcontacorrente.Close;
  QTodasCompras.Close;
  if QCadastroCIDADE.AsString <> '' then
    dbLkpCidades.KeyValue := UpperCase(QCadastroCIDADE.AsString);
end;

procedure TFCadConv.QCadastroBeforePost(DataSet: TDataSet);
var mens, verifica_cpf, param1, param2: string;
  SQL : TSqlMount;
  I: Integer;
begin
  valida;
  //QCadastroCONV_ID.ProviderFlags:=[pfInUpdate,pfInWhere];
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;
  QCadastroFIDELIDADE.AsString:= UpperCase(QCadastroFIDELIDADE.AsString);
  if (QCadastroTIPO_CREDITO.AsInteger = 2) then
    DMConexao.ExecuteSql('UPDATE conveniados SET data_atualizacao_acumulado = getdate() WHERE conv_id = '+QCadastroCONV_ID.AsString);
    //QCadastroDATA_ATUALIZACAO_ACUMULADO.AsDateTime := now;
  if (QCadastroFIDELIDADE.AsString = 'S') and (QCadastro.State <> dsInsert) then
  begin
    if (QCadastroFIDELIDADE.OldValue <> QCadastroFIDELIDADE.Value) then
    begin
      if DMConexao.ExecuteScalar('select hist_id from fidel_historico where historico = ''PONTOS CADASTRO'' and conv_id = ' + QCadastroCONV_ID.AsString,0) = 0 then
      begin
        DMConexao.AdoQry.Close;
        SQL := TSqlMount.Create(smtInsert,'FIDEL_HISTORICO');
        Sql.AddField('HIST_ID',DMConexao.getGeneratorValue('GEN_FIDEL_HISTORICO'),ftInteger);
        Sql.AddField('DATAHORA',Now,ftDateTime);
        Sql.AddField('CREDITO',DMConexao.ExecuteScalar('select distinct ptscadastro from fidel_config ',0),ftInteger);
        Sql.AddField('DEBITO',0,ftInteger);
        Sql.AddField('HISTORICO','PONTOS CADASTRO',ftString);
        Sql.AddField('CONV_ID',QCadastroCONV_ID.AsInteger,ftInteger);
        Sql.AddField('CANCELADO','N',ftString);
        DMConexao.AdoQry.SQL    := sql.GetSqlString;
        //DMConexao.AdoQry.Parameters := sql.GetParams;
        DMConexao.AdoQry.ExecSQL;
        SQL.Free;
      end;
    end;
  end;
  DMConexao.Config.Open;
  if (QCadastro.State = dsEdit) and (QCadastroTITULAR.OldValue <> QCadastroTITULAR.AsString) and (DMConexao.ConfigALTERAR_TITULAR_NOME_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set nome = '+ QuotedStr(QCadastroTITULAR.AsString)+'" where conv_id = '+QuotedStr(QCadastroCONV_ID.AsString)+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  if (QCadastro.State = dsEdit) and (QCadastroLIBERADO.OldValue <> QCadastroLIBERADO.AsString) and (DMConexao.ConfigALTERAR_TITULAR_LIBERADO_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set liberado = '+QuotedStr(QCadastroLIBERADO.AsString)+' where conv_id = '+QCadastroCONV_ID.AsString+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  verifica_cpf := DMConexao.ConfigVERIFICA_CPF_CONV.AsString;
  if (verifica_cpf[1] in ['C','P']) and (Trim(QCadastroCPF.AsString) <> '') then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := 'Select conv_id from PESQUISA_CPF("'+QCadastroCPF.AsString+'") where conv_id <> '+QCadastroCONV_ID.AsString;
    DMConexao.AdoQry.Open;
    if not DMConexao.AdoQry.IsEmpty then
      while not DMConexao.AdoQry.Eof do
      begin
        mens := mens + DMConexao.AdoQry.Fields[0].AsString+sLineBreak;
        DMConexao.AdoQry.Next;
      end;
      DMConexao.AdoQry.Close;
      if Trim(mens) <> '' then               
      begin
      if verifica_cpf = 'P' then
      begin
        if Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens+sLineBreak+'Deseja continuar o cadastro?'),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDNO then
          Sysutils.Abort;
      end
      else
      begin
        Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens),'Opera��o cancelada.',MB_OK+MB_ICONERROR);
        Sysutils.Abort;
      end;
    end;
  end;
  if QCadastro.State = dsInsert then
    incluindo := True;
  if DMConexao.ConfigUSA_NOVO_FECHAMENTO.AsString = 'S' then
  begin
    if QCadastro.State = dsInsert then
    begin
      if ((DBDateEdit2.Date > 0) and ((QCadastroDATA_DEMISSAO.OldValue <> QCadastroDATA_DEMISSAO.Value))) then
      begin
        if DMConexao.ConfigDEMISSAO_MOVE_AUTS.AsString = 'S' then
        begin
          MsgInf('As autoriza��es com fechamentos posteriores ser�o transportadas para o pr�ximo fechamento em aberto!');
          DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select datafecha from '+
          ' get_prox_fecha_aberto(current_timestamp,'+QCadastroCONV_ID.AsString+
          ',0)) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString);
        end;
        QCadastroSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Devedor',FormatDinBR(0),FormatDinBR(QCadastroSALDO_DEVEDOR.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
        QCadastroSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select sum(debito-credito) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString,0);
        //DMConexao.GravaLog('FCadConv','Saldo Dev. Fat.',FormatDinBR(0),FormatDinBR(QCadastroSALDO_DEVEDOR_FAT.AsCurrency),Operador.Nome,'Altera��o','Cadastro de Conveniados',QCadastroCONV_ID.AsString,'Conv ID: ',Self.Name);
      end;
    end;
    if ((QCadastro.State = dsEdit) and ((QCadastroDATA_DEMISSAO.OldValue <> QCadastroDATA_DEMISSAO.Value))) then
    begin
      if not QCadastroDATA_DEMISSAO.IsNull then
      begin
        //QCadastroSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select coalesce(sum(debito-credito),0) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString);
        if DMConexao.ConfigDEMISSAO_MOVE_AUTS.AsString = 'S' then
        begin

          if MsgSimNao('As autoriza��es com fechamentos posteriores a data atual '+sLineBreak+
                 'ser�o transportadas para o fechamento de '+FormatDataBR(DMConexao.ExecuteScalar('select min(data_fecha) from '+
                 'dia_fecha where data_fecha > GETDATE() AND empres_id = '+QCadastroEMPRES_ID.AsString+ 'and '+
                 'data_fecha not in (Select fechamento from fatura where apagado <> ''S'' and tipo = ''E'' and id = '+QCadastroEMPRES_ID.AsString+') and '+
                 'data_fecha not in (Select fechamento from fatura where apagado <> ''S'' and tipo = ''C'' and id = '+QCadastroCONV_ID.AsString+')'))+sLineBreak+
                 'correspondente ao pr�ximo fechamento n�o faturado da empresa'+sLineBreak+
                 'Confirma esta opera��o?') then
          begin
            param1 := QCadastroCONV_ID.AsString;
            param2 := QCadastroEMPRES_ID.AsString;
            DMConexao.ExecuteSql(' update contacorrente set data_fecha_emp = (select data_fecha from '+
                           ' getDataFecha('+param1+','+param2+')) where fatura_id = 0 and coalesce(baixa_conveniado,''N'') <> ''S'' '+
                           'and conv_id = '+QCadastroCONV_ID.AsString+' and data_fecha_emp > ' +
                           '(SELECT * FROM getDataFecha('+param1+','+param2+'))'
            );
            QCadastroSALDO_DEVEDOR.AsCurrency:= DMConexao.ExecuteScalar('select coalesce(sum(debito-credito),0) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and conv_id = '+QCadastroCONV_ID.AsString+' and DATA_FECHA_EMP = (select data_fecha from getDataFecha('+QCadastroCONV_ID.AsString+','+QCadastroEMPRES_ID.AsString+'))');
            QCadastroSALDO_DEVEDOR_FAT.AsCurrency:= DMConexao.ExecuteScalar('select coalesce(sum(debito-credito),0) from contacorrente where coalesce(baixa_conveniado,''N'')=''N'' and coalesce(fatura_id,0)>0 and conv_id = '+QCadastroCONV_ID.AsString);

          end;
        end;
        QCadastroLIBERADO.AsString := 'N';
        DBCheckBox1.Checked := false;

      end
      else
      begin
        if ((QCadastroDATA_DEMISSAO.Value = 0) and  (QCadastroDATA_DEMISSAO.OldValue <> null)) then  begin
          QCadastroSALDO_DEVEDOR.AsFloat:= 0;
          QCadastroSALDO_DEVEDOR_FAT.AsFloat:= 0;
          QCadastroLIBERADO.AsString := 'S';
          DBCheckBox1.Checked := true;
        end;
      end;
    end
  end;

  DMConexao.Config.Close;
end;

procedure TFCadConv.QCadastroAfterPost(DataSet: TDataSet);
var
  SQL : TSqlMount;
begin
  inherited;
  if incluindo then
  begin
    //Gybsom
    //TRECHO QUE ATRIBUI O STATUS DE TITULAR PARA O CART�O
    QCartoes.Close;
    QCartoes.Parameters[0].Value := QCadastroCONV_ID.AsInteger;
    Qcartoes.Open;
    QCartoes.Append;
    QCartoesNOME.AsString      := QCadastroTITULAR.AsString;
    QCartoesLIBERADO.AsString  := 'N';
    QCartoesTITULAR.AsString   := 'S';
    if QCadastroDT_NASCIMENTO.AsDateTime <> 0 then
      QCartoesDATA_NASC.AsDateTime    := QCadastroDT_NASCIMENTO.AsDateTime;
    QCartoesEMPRES_ID.AsString := QCadastroEMPRES_ID.AsString;
    if colocouMensagem then
      DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'AUTOMATICO', 'INCLUS�O CONVENIADO');
    QCartoes.Post;
    incluindo := False;
    DMConexao.Config.Open;
    if DMConexao.ConfigUSA_FIDELIDADE.AsString = 'SIM' then
    begin
      if ((QCadastroFIDELIDADE.AsString = 'S') or (DMConexao.ExecuteScalar('select fidelidade from empresas where empres_id = '+QCadastroEMPRES_ID.AsString,'N') = 'S')) then
      begin
        if DMConexao.ExecuteScalar('select hist_id from fidel_historico where historico = ''PONTOS CADASTRO'' and conv_id = ' + QCadastroCONV_ID.AsString,0) = 0 then
        begin
          DMConexao.AdoQry.Close;
          SQL := TSqlMount.Create(smtInsert,'FIDEL_HISTORICO');
          Sql.AddField('HIST_ID',DMConexao.getGeneratorValue('GEN_FIDEL_HISTORICO'),ftInteger);
          Sql.AddField('DATAHORA',Now,ftDateTime);
          Sql.AddField('CREDITO',DMConexao.ExecuteScalar('select distinct ptscadastro from fidel_config ',0),ftInteger);
          Sql.AddField('DEBITO',0,ftInteger);
          Sql.AddField('HISTORICO','PONTOS CADASTRO',ftString);
          Sql.AddField('CONV_ID',QCadastroCONV_ID.AsInteger,ftInteger);
          Sql.AddField('CANCELADO','N',ftString);
          DMConexao.AdoQry.SQL    := sql.GetSqlString;
          //DMConexao.AdoQry.Params := sql.GetParams;
          DMConexao.AdoQry.ExecSQL;
          SQL.Free;
        end;
      end;
    end;
  end;

  if Qcartoes.State = dsInsert then
    DMConexao.ExecuteSql('INSERT INTO CARTOES_HISTORICO SELECT CARTAO_ID, CONV_ID, EMPRES_ID, CODCARTIMP, CVV, SENHA, GETDATE() FROM CARTOES WHERE CARTAO_ID = '  + QCartoesCARTAO_ID.AsString);

  DMConexao.ExecuteSql('UPDATE CONVENIADOS SET USA_SALDO_DIF = ' + QuotedStr(QCadastroUSA_SALDO_DIF.AsString) + ' WHERE CONV_ID = ' + QCadastroCONV_ID.AsString);

  if qBandConv.State = dsInsert then begin
    qBandConvCONV_ID.Value := QCadastroCONV_ID.Value;
    qBandConvAPAGADO.AsString := 'N';
    qBandConv.Post;
  end;
  if qBandConv.State = dsEdit then begin
    if dbCbUsaLimiteDif.Checked then
      qBandConvAPAGADO.AsString := 'N'
    else
      qBandConvAPAGADO.AsString := 'S';

    qBandConv.Post;
  end;

end;
procedure TFCadConv.QCadastroAfterRefresh(DataSet: TDataSet);
begin
  inherited;
  Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
   '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;
end;

procedure TFCadConv.QCartoesAfterInsert(DataSet: TDataSet);
var titular : Boolean;
    codimp: string;
    //aux : Integer;
    qtdEncontrados : Integer;
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    //GYBSOM
    //Inicio da logica de verificar se ser� gerado cart�o novo e cart�o antigo ou apenas cart�o novo
    // CASO A EMPRESA J� TENHA "VIRADO", SER�O GERADOS APENAS CART�ES NOVOS
    if(QEmpresausa_novo_cartao.AsString = 'S') then
    begin
      titular := (QCartoes.RecordCount = 0);
      //RECUPERANDO O PR�XIMO ID DO CARTAO A SER INSERIDO
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCARTAO_ID AS CARTAO_ID');
      DMConexao.AdoQry.Open;
      QCartoesCARTAO_ID.AsInteger          := DMConexao.AdoQry.Fields[0].Value;
      //E RETORNANDO TAMB�M O VALOR DA SEQUENCE SCARTAO_NUM, PARA UTILIZAR COMO C�DIGO DA TABELA CARTOES
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCARTAO_NUM AS CODIGO');
      DMConexao.AdoQry.Open;
      QCartoesCODIGO.AsInteger             := DMConexao.AdoQry.Fields[0].Value;
      //POR FIM, RETORNA O VALOR DA SEQUENCE SLOG_NOVOS_CARTOES, PARA GRAVAR NO CAMPO LOG_ID_CARTAO_NOVO
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SLOG_NOVOS_CARTOES AS LOG_ID');
      DMConexao.AdoQry.Open;
      QCartoesLOG_ID_CARTAO_NOVO.AsInteger             := DMConexao.AdoQry.Fields[0].Value;
      DMConexao.AbrirCongis;
      if QEmpresaUSA_COD_IMPORTACAO.AsString = 'S' then begin
        repeat
          if VerificaModeloCartaoPorEmpresID(QCadastroEMPRES_ID.AsInteger) = 8 then
            codimp := RemoveCaracter(gerarCartaoCantinex(DMConexao.ConfigBIN_CANTINA.AsString))
          else
            codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
          //qtdEncontrados := DMConexao.ExecuteSql('SELECT CODCARTIMP FROM CARTOES_HISTORICO WHERE CODCARTIMP = '+QuotedStr(codimp));
        until (verificaCartaoExistente(codimp) AND verificaCartaoExistentehISTORICO(codimp));

        QCartoesCODCARTIMP.AsString := codimp;
        QCartoesCVV.AsString := GerarCVV(codimp);
      end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := QCartoesCODIGO.AsString
      else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
      begin
        if titular then
          codimp := DMConexao.ObterCodCartImp
        else
          codimp := DMConexao.ObterCodCartImp(False);
        QCartoesCODCARTIMP.AsString := codimp;
      end
      else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImpMod1(QCadastroCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QCadastroCHAPA.AsString)
      else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
      begin
        codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
        QCartoesCODCARTIMP.AsString := codimp;
      end;
      QCartoesCVV.AsString := GerarCVV(codimp);
      DMConexao.FecharConfigs;
      QCartoesDIGITO.AsInteger       := DigitoCartao(QCartoesCODIGO.AsFloat);
      QCartoesCONV_ID.AsInteger      := QCadastroCONV_ID.AsInteger;
      if VerificaModeloCartaoPorEmpresID(QCadastroEMPRES_ID.AsInteger) = 8 then
      begin
        QCartoesLIBERADO.AsString := 'S';
      end
      else begin
        QCartoesLIBERADO.AsString := 'N';
      end;
      QCartoesJAEMITIDO.AsString     := 'N';
      QCartoesJAEMITIDO_CARTAO_NOVO.AsString := 'S';
      QCartoesDATA_EMISSAO_CARTAO_NOVO.AsDateTime := Now;
      QCartoesAPAGADO.AsString       := 'N';
      QCartoesTITULAR.AsString       := 'N';
      QCartoesLIMITE_MES.Value       := QCadastroLIMITE_MES.Value;
      QCartoesDTCADASTRO.AsDateTime  := Now;
      QCartoesDTALTERACAO.AsDateTime := Now;
      QCartoesOPERADOR.AsString      := QCadastroOPERCADASTRO.AsString;
      QCartoesOPERCADASTRO.AsString  := QCadastroOPERCADASTRO.AsString;
      QCartoesSENHA.AsString := Crypt('E', Copy(QCartoesCODCARTIMP.AsString, 13, 4), 'BIGCOMPRAS');
      if titular then
      begin
        QCartoesNOME.AsString    := UpperCase(QCadastroTITULAR.AsString);
        QCartoesTITULAR.AsString := 'S';
      end;
      if TabCartoes.Visible then
        JvDBGrid1.SetFocus;
    end else begin   //CASO A EMPRESA AINDA N�O TENHA VIRADO, SER� GRAVADO O MODELO ANTIGO NA TABELA CART�ES E O NOVO NA TEMP
      titular := (QCartoes.RecordCount = 0);
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCARTAO_ID AS CARTAO_ID');
      DMConexao.AdoQry.Open;
      QCartoesCARTAO_ID.AsInteger          := DMConexao.AdoQry.Fields[0].Value;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCARTAO_NUM AS CODIGO');
      DMConexao.AdoQry.Open;
      QCartoesCODIGO.AsInteger             := DMConexao.AdoQry.Fields[0].Value;
      DMConexao.AbrirCongis;
      if QEmpresaUSA_COD_IMPORTACAO.AsString = 'S' then begin
        repeat
          codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
          //qtdEncontrados := DMConexao.ExecuteSql('SELECT CODCARTIMP FROM CARTOES_HISTORICO WHERE CODCARTIMP = '+QuotedStr(codimp));
        //until (qtdEncontrados > -1);
        until (verificaCartaoExistente(codimp) AND verificaCartaoExistentehISTORICO(codimp));
        QCartoesCODCARTIMP.AsString := codimp;
      end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := QCartoesCODIGO.AsString
      else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
      begin
        if titular then
          codimp := DMConexao.ObterCodCartImp
        else
          codimp := DMConexao.ObterCodCartImp(False);
        QCartoesCODCARTIMP.AsString := codimp;
      end
      else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImpMod1(QCadastroCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QCadastroCHAPA.AsString)
      else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
      begin
        codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
        QCartoesCODCARTIMP.AsString := codimp;
      end;
      QCartoesCVV.AsString := GerarCVV(codimp);
      DMConexao.FecharConfigs;
      QCartoesDIGITO.AsInteger               := DigitoCartao(QCartoesCODIGO.AsFloat);
      QCartoesCONV_ID.AsInteger              := QCadastroCONV_ID.AsInteger;
      QCartoesLIBERADO.AsString              := 'S';
      QCartoesJAEMITIDO.AsString             := 'N';
      QCartoesJAEMITIDO_CARTAO_NOVO.AsString := 'N';
      QCartoesAPAGADO.AsString               := 'N';
      QCartoesTITULAR.AsString               := 'N';
      QCartoesLIMITE_MES.Value               := QCadastroLIMITE_MES.Value;
      QCartoesDTCADASTRO.AsDateTime           := Now;
      QCartoesDTALTERACAO.AsDateTime         := Now;
      QCartoesOPERADOR.AsString              := QCadastroOPERCADASTRO.AsString;
      QCartoesOPERCADASTRO.AsString          := QCadastroOPERCADASTRO.AsString;
      QCartoesSENHA.AsString := Crypt('E', Copy(QCartoesCODCARTIMP.AsString, 13, 4), 'BIGCOMPRAS');
      if titular then
      begin
        QCartoesNOME.AsString    := UpperCase(QCadastroTITULAR.AsString);
        QCartoesTITULAR.AsString := 'S';
      end;
      if TabCartoes.Visible then
        JvDBGrid1.SetFocus;
    end;
  end;
end;

procedure TFCadConv.TabOutrasHide(Sender: TObject);
begin
  inherited;
  QLimSeg.Close;
end;

procedure TFCadConv.GridLimPorSegKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = vk_down ) or (key = vk_up) then
  begin
    if QLimSeg.State in [dsEdit] then
    begin
      Porcent        := GridLimPorSeg.Fields[2].Value;
      LimiteValor    := GridLimPorSeg.Fields[3].Value;
      SavePlace      := QLimSeg.GetBookmark;
      QLimSeg.Post;
    end;
  end;
end;

procedure TFCadConv.GridSaldConvCellClick(Column: TColumn);
begin
  inherited;

  while not QSaldoCartao.Eof do
  begin
    if(QSaldoConvFECHAMENTO.AsDateTime = QSaldoCartaoFECHAMENTO.AsDateTime) then
    begin
      GridSaldoCartao.Canvas.Brush.Color := $00BFFFFF;
    end;
    QSaldoCartao.Next;
  end;
  QSaldoCartao.First;

end;

procedure TFCadConv.CheckBox1Click(Sender: TObject);
begin
  inherited;
  if CheckBox1.Checked then
  begin
    btnAtualizar.Visible := true;
    Edit1.Visible := true;
    Label89.Visible := True;
    Edit1.Focused;
  end
  else begin
    btnAtualizar.Visible := false;
    Label89.Visible := false;
    Edit1.Visible := false;
  end;
end;

procedure TFCadConv.atualizaCodImpTodasEmpresas;
var codimp: string;
    linhas,empres_id,conv_id : Integer;
begin
  Screen.Cursor := crHourGlass;
  qTemp.Parameters[0].Value := StrToInt(Edit1.Text);
  qTemp.Open;
  qTemp.First;
  mdTemp.Open;
  mdTemp.EmptyTable;
  mdTemp.DisableControls;
  while not qTemp.Eof do
  begin
    mdTemp.Append;
    mdTempconv_id.AsInteger           := qTempconv_id.AsInteger;
    mdTemp.Post;
    qTemp.Next;
  end;
  mdTemp.Next;
  mdTemp.First;
  mdTemp.EnableControls;


  while not mdTemp.Eof do
  begin

    conv_id := mdTempconv_id.Value;
    buscaConveniadoPassaId(IntToStr(conv_id));
    QCartoes.Parameters[0].Value := mdTempconv_id.Value;
    QCartoes.Open;
    DMConexao.Config.Open;
    prVerfEAbreCon(QCartoes);
    if QCartoes.RecordCount > 0 then
    begin
      QCartoes.Edit;
      QCartoesJAEMITIDO.AsString := 'N';
      if QEmpresaUSA_COD_IMPORTACAO.AsString = 'S' then begin
        repeat
          codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
        until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));
        QCartoesCODCARTIMP.AsString := codimp;
      end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := QCartoesCODIGO.AsString
      else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
      begin
        if QCartoesTITULAR.AsString = 'S' then
          QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp
        else
          QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp(False);
      end
      else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImpMod1(QCadastroCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QCadastroCHAPA.AsString)
      else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
        QCartoesCODCARTIMP.AsString :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
      if QCartoes.State in [dsEdit] then begin
        //if (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',Self.Caption,QCartoes.FieldByName('CARTAO_ID').AsString,StringReplace(detalhe,': ','',[rfReplaceAll]),'', '')) then begin
          QCartoes.Post;

        //end;
      end;
    end
    else
    begin
      QCartoes.Close;
      DMConexao.Config.Close;
    end;
    mdTemp.Next;
  end;
  DMConexao.AdoQry.EnableControls;
  Screen.Cursor := crDefault;
  MsgInf('Novas vias geradas com sucesso!');
  buscaConveniadoPassaId('0');

end;

procedure TFCadConv.buscaConveniadoPassaId(conv_id : string);
begin
  QCadastro.Close;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('select');
  QCadastro.SQL.Add(' distinct conveniados.CONV_ID,');
  QCadastro.SQL.Add('  conveniados.TITULAR,');
  QCadastro.SQL.Add('  conveniados.LIBERADO,');
  QCadastro.SQL.Add('  conveniados.EMPRES_ID,');
  QCadastro.SQL.Add('  conveniados.GRUPO_CONV_EMP,');
  QCadastro.SQL.Add('  conveniados.CARGO,');
  QCadastro.SQL.Add('  conveniados.SETOR,');
  QCadastro.SQL.Add('  conveniados.COD_EMPRESA,');
  QCadastro.SQL.Add('  conveniados.DT_NASCIMENTO,');
  QCadastro.SQL.Add('  conveniados.CPF,');
  QCadastro.SQL.Add('  conveniados.RG,');
  QCadastro.SQL.Add('  conveniados.ENDERECO,');
  QCadastro.SQL.Add('  conveniados.NUMERO,');
  QCadastro.SQL.Add('  conveniados.BAIRRO,');
  QCadastro.SQL.Add('  conveniados.CIDADE,');
  QCadastro.SQL.Add('  conveniados.ESTADO,');
  QCadastro.SQL.Add('  conveniados.CEP,');
  QCadastro.SQL.Add('  conveniados.BANCO,');
  QCadastro.SQL.Add('  conveniados.AGENCIA,');
  QCadastro.SQL.Add('  conveniados.CONTACORRENTE,');
  QCadastro.SQL.Add('  conveniados.DIGITO_CONTA,');
  QCadastro.SQL.Add('  conveniados.TIPOPAGAMENTO,');
  QCadastro.SQL.Add('  conveniados.TELEFONE1,');
  QCadastro.SQL.Add('  conveniados.TELEFONE2,');
  QCadastro.SQL.Add('  conveniados.CELULAR,');
  QCadastro.SQL.Add('  conveniados.OBS1,');
  QCadastro.SQL.Add('  conveniados.OBS2,');
  QCadastro.SQL.Add('  conveniados.DTCADASTRO,');
  QCadastro.SQL.Add('  conveniados.OPERCADASTRO,');
  QCadastro.SQL.Add('  conveniados.DTALTERACAO,');
  QCadastro.SQL.Add('  conveniados.OPERADOR,');
  QCadastro.SQL.Add('  conveniados.DTULTCESTA,');
  QCadastro.SQL.Add('  conveniados.DTASSOCIACAO,');
  QCadastro.SQL.Add('  conveniados.EMAIL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_MES,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_1,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_2,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_3,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_4,');
  QCadastro.SQL.Add('  conveniados.LIMITE_TOTAL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_PROX_FECHAMENTO,');
  QCadastro.SQL.Add('  conveniados.CESTABASICA,');
  QCadastro.SQL.Add('  conveniados.SALARIO,');
  QCadastro.SQL.Add('  conveniados.FIDELIDADE,');
  QCadastro.SQL.Add('  conveniados.CONTRATO,');
  QCadastro.SQL.Add('  conveniados.TIPOSALARIO,');
  QCadastro.SQL.Add('  conveniados.SETOR_ID,');
  QCadastro.SQL.Add('  conveniados.FLAG,');
  QCadastro.SQL.Add('  conveniados.SENHA,');
  QCadastro.SQL.Add('  conveniados.DTAPAGADO,');
  QCadastro.SQL.Add('  conveniados.APAGADO,');
  QCadastro.SQL.Add('  conveniados.VALE_DESCONTO,');
  QCadastro.SQL.Add('  conveniados.LIBERA_GRUPOSPROD,');
  QCadastro.SQL.Add('  conveniados.COMPLEMENTO,');
  QCadastro.SQL.Add('  conveniados.USA_SALDO_DIF,');
  QCadastro.SQL.Add('  conveniados.ABONO_MES,');
  QCadastro.SQL.Add('  conveniados.SALDO_RENOVACAO,');
  QCadastro.SQL.Add('  conveniados.SALDO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.DATA_ATUALIZACAO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.CHAPA,');
  QCadastro.SQL.Add('  conveniados.DATA_ADMISSAO,');
  QCadastro.SQL.Add('  conveniados.DATA_DEMISSAO,');
  QCadastro.SQL.Add('  conveniados.NUM_DEPENDENTES,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR_FAT,');
  QCadastro.SQL.Add('  conveniados.PIS,');
  QCadastro.SQL.Add('  conveniados.NOME_PAI,');
  QCadastro.SQL.Add('  conveniados.NOME_MAE,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_NUM,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_SERIE,');
  QCadastro.SQL.Add('  conveniados.REGIME_TRAB,');
  QCadastro.SQL.Add('  conveniados.VENC_TOTAL,');
  QCadastro.SQL.Add('  conveniados.FIM_CONTRATO,');
  QCadastro.SQL.Add('  conveniados.DISTRITO,');
  QCadastro.SQL.Add('  conveniados.ESTADO_CIVIL,');
  QCadastro.SQL.Add('  empresas.FANTASIA,');
  QCadastro.SQL.Add('  empresas.BAND_ID,');
  QCadastro.SQL.Add('  empresas.nome empresa,');
  QCadastro.SQL.Add('  empresas.TIPO_CREDITO');
  QCadastro.SQL.Add('from conveniados');
  QCadastro.SQL.Add('join empresas on empresas.empres_id = conveniados.empres_id and empresas.apagado <> ''S''');
  QCadastro.SQL.Add(' where conv_id = ' + conv_id);
  QCadastro.Sql.Add('order by conveniados.titular');
  QCadastro.Open;
end;


procedure TFCadConv.btnAtualizarClick(Sender: TObject);
begin
  inherited;
  if(Edit1.Text = '') then
  begin
    MsgInf('Digite um c�digo de empresa v�lido!');
    Exit;
  end;
  if MsgSimNao('Voc� realmente deseja solicitar nova via de cart�es para todos os conveniados da empresa: '+Edit1.Text) then
  begin
    atualizaCodImpTodasEmpresas;
  end;


end;

procedure TFCadConv.btnTransferenciaClick(Sender: TObject);
var texto : string;
saldoAcumulado, novoSaldo : currency;
begin
  inherited;
  if dbConvID.Text <> '' then begin
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT TITULAR FROM CONVENIADOS WHERE CONV_ID = ' + dbConvID.Text);
      DMConexao.AdoQry.Open;
      if DMConexao.AdoQry.Fields[0].AsString = ''  then begin
        msginf('Conveniado n�o encontrado!');
        dbConvID.SetFocus;
      end
      else begin
          texto := 'Confirma a transfer�ncia do Saldo Acumulado para o conveniado ' + dbConvID.Text + ' - ' + DMConexao.AdoQry.Fields[0].AsString + '?';
          if Application.MessageBox(PAnsiChar(texto),'Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then begin

            {FrmOcorrencia := TFrmOcorrencia.Create(Self);
            if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
              Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
              Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
              FreeAndNil(FrmOcorrencia);
            end;     }


            saldoAcumulado := 0;
            novoSaldo:= 0;

            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add(' SELECT COALESCE(SALDO_ACUMULADO,0) FROM CONVENIADOS WHERE CONV_ID = ' + dbConvID.Text);
            DMConexao.AdoQry.Open;

            saldoAcumulado := DMConexao.AdoQry.Fields[0].AsCurrency;

            novoSaldo := saldoAcumulado + QCadastroSALDO_ACUMULADO.AsCurrency;

            texto := ' update conveniados set saldo_acumulado = ' + fnsubstituiString(',','.',CurrToStr(novoSaldo)) + ' where conv_id = ' + dbConvID.Text;
            DMConexao.ExecuteSql(texto);
             
            QCadastro.Edit;
            QCadastroSALDO_ACUMULADO.AsCurrency := 0;
            QCadastro.Post;

            //CONVENIADO ATUAL//
            //DMConexao.GravaLog('FCadConv','SALDO_ACUMULADO',QCadastroSALDO_ACUMULADO.AsString,'0.00',Operador.Nome,'Altera��o',
            //  'FCadConv',QCadastroCONV_ID.AsString,'Transf. de Saldo Acum. para o conv. ' + dbConvID.Text,Ocorrencia.Solicitante, Ocorrencia.Motivo);

            //NOVO CONVENIADO//
            DMConexao.GravaLog('FCadConv','SALDO_ACUMULADO', FormatDinBR(saldoAcumulado),FormatDinBR(novoSaldo),Operador.Nome,'Altera��o',
             dbConvID.Text,Ocorrencia.Solicitante, Ocorrencia.Motivo);

            dbConvID.SetFocus;

          end
          else
             dbConvID.SetFocus;

      end;

  end
  else begin
    msginf('Informe o Conv.ID para realizar a transfer�ncia do Saldo');
    dbConvID.SetFocus;
  end;
end;

procedure TFCadConv.dbConvIDKeyPress(Sender: TObject; var Key: Char);
begin
if not (key in ['0'..'9',',',#8,#13]) then Key := #0;
  inherited;
end;

procedure TFCadConv.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  QCidades.close;
  QBairros.close;
  QCidades.Parameters.ParamByName('ESTADO_ID').Value := QCadastroESTADO.Value;
  QBairros.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE.Value;
  QCidades.open;
  QBairros.open;
end;

procedure TFCadConv.BitBtn2Click(Sender: TObject);
var cartao_id,ItemSel : Integer;
var _isTitular : String;
begin
  inherited;
   cartao_id := JvDBGrid1.Columns.Items[0].Field.Value;
   if (fnVerfCompVazioEmTabSheet('� necess�rio carregar um conveniado para alterar a senha!',dbEdit2) = False)
    and(fnVerfCompVazioEmTabSheet('� necess�rio carregar um conveniado para alterar a senha!',titular) = False)then
    begin
      inherited;
      if not QCadastro.IsEmpty then
      begin
        ItemSel := TFSelTipoImp.AbrirJanela(['Alterar senha para os 4 �ltimos n�meros do Cart�o']);
        //Verifica se o cart�o selecionado pertence ao titular ou n�o
        QCartoes.Open;
        QCartoes.Edit;
        case ItemSel of
           0: QCartoesSENHA.AsString := Crypt('E',Copy(QCartoesCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
           1: QCartoesSENHA.AsString := Crypt('E',DigitaNovaSenha,'BIGCOMPRAS');
        end;
      end;
      DMConexao.ExecuteNonQuery('INSERT INTO LOG_SENHA VALUES('+QCartoesCARTAO_ID.AsString+','+QuotedStr(DateToStr(Date))+','+QuotedStr('ADMCON')+','+QuotedStr(Operador.Nome)+',''N'' )');
      if Application.MessageBox('Confirma esta opera��o?','Confirma��o',mb_yesno+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
      begin
        colocouMensagem := DMConexao.GravaLogCad(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName(chavepri).AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName(chavepri).AsString,'NULL','');
        QCartoes.Post;
      end
      else
        QCartoes.Cancel;
    end;
end;

procedure TFCadConv.btnAdicionaBairroClick(Sender: TObject);
var bairro : string; cidade : string; sql : string;
begin
  inherited;
  frmBairro := TfrmBairro.Create(Self);
  frmBairro.ShowModal;
  if frmBairro.ModalResult = mrOk then
  begin
    cidade := QBairros.Parameters.ParamByName('CID_ID').Value;
    bairro := frmBairro.txtBairro.Text;
    sql := 'insert into bairros(cid_id, descricao) values(' + cidade + ', ''' + bairro + ''')';
    DMConexao.ExecuteSql(sql);
    QBairros.Requery();
    ShowMessage('Bairro inserido com sucesso!');
  end;
end;

procedure TFCadConv.DSCidadesDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not qBairros.Locate('DESCRICAO',dbLkpBairros.KeyValue,[])) then
  begin
    QBairros.Close;
    QBairros.Parameters.ParamByName('CID_ID').Value := dbLkpCidades.KeyValue;
    QBairros.Open;
  end;
  btnAdicionaBairro.Enabled := true;
end;

procedure TFCadConv.DBGrid1Enter(Sender: TObject);
begin
  inherited;
  QCidades.close;
  QBairros.close;
  QCidades.Parameters.ParamByName('ESTADO_ID').Value := QCadastroESTADO.Value;
  QBairros.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE.Value;
  QCidades.open;
  QBairros.open;

end;

procedure TFCadConv.QCartoesAfterPost(DataSet: TDataSet);
var codimp: string;
cvv : string; logId : string;
qtdEncontrados : Integer;
begin
  inherited;
  if((QEmpresausa_novo_cartao.AsString = 'N') and (QCartoesJAEMITIDO_CARTAO_NOVO.AsString = 'N')) then
  begin
    if(QCartoesLOG_ID_CARTAO_NOVO.AsInteger = 0) then begin
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SLOG_NOVOS_CARTOES AS LOG_ID');
      DMConexao.AdoQry.Open;
      logId := DMConexao.AdoQry.Fields[0].Value;
      DMConexao.AdoQry.SQL.Clear;
    end;

    //Atualiza os dados de emiss�o na tabela cart�es
    DMConexao.ExecuteSql('UPDATE CARTOES SET JAEMITIDO_CARTAO_NOVO = ''S'', DATA_EMISSAO_CARTAO_NOVO = GETDATE(), LOG_ID_CARTAO_NOVO = ' + logId + ' WHERE CARTAO_ID = ' + QCartoesCARTAO_ID.AsString);

    //Clona a linha da CARTOES para a CARTOES_TEMP
    DMConexao.ExecuteSql('INSERT INTO CARTOES_TEMP SELECT * FROM CARTOES WHERE CARTAO_ID = '+QCartoesCARTAO_ID.AsString+'');

    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
      //qtdEncontrados := DMConexao.ExecuteSql('SELECT CODCARTIMP FROM CARTOES_HISTORICO WHERE CODCARTIMP = '+QuotedStr(codimp));
    until (verificaCartaoExistente(codimp) AND verificaCartaoExistenteHistorico(codimp));

    cvv := GerarCVV(codimp);

    DMConexao.ExecuteSql('UPDATE CARTOES_TEMP SET CODCARTIMP = ' + QuotedStr(codimp) + ', CVV = ' + QuotedStr(cvv) + ' WHERE CARTAO_ID = ' + QCartoesCARTAO_ID.AsString);
    DMConexao.ExecuteSql('INSERT INTO CARTOES_HISTORICO SELECT CARTAO_ID, CONV_ID, EMPRES_ID, CODCARTIMP, CVV, SENHA, GETDATE() FROM CARTOES_TEMP WHERE CARTAO_ID = '  +  QCartoesCARTAO_ID.AsString);
  end;
end;
  
procedure TFCadConv.btnVerSenhaClick(Sender: TObject);
var senha : String;
var cartao_id,ItemSel : Integer;
var _isTitular : String;
begin
  inherited;
    cartao_id := JvDBGrid1.Columns.Items[0].Field.Value;
   if (fnVerfCompVazioEmTabSheet('� necess�rio carregar um conveniado para verificar a senha!',dbEdit2) = False)
    and(fnVerfCompVazioEmTabSheet('� necess�rio carregar um conveniado para verificar a senha!',titular) = False)then
    begin
      inherited;
      if not QCadastro.IsEmpty then
      begin
        senha := Crypt('D', DMConexao.ExecuteQuery('select senha from cartoes where cartao_id = '+ IntToStr(cartao_id)),'BIGCOMPRAS');
        MsgInf(senha);
      end;
    end;
end;

function TFCadConv.VerificaModeloCartaoPorEmpresID(empres_id : Integer) : Integer;
begin
  Result := DMConexao.ExecuteQuery('select mod_cart_id from EMPRESAS where empres_id = '+ IntToStr(empres_id));
end;


procedure TFCadConv.DBGrid1ContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  inherited;
  if Operador.ID = 80 then
    Handled := True;
end;

end.
