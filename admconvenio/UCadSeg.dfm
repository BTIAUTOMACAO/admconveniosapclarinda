inherited FCadSeg: TFCadSeg
  Left = 368
  Top = 163
  Caption = 'Cadastro de Segmentos'
  ClientHeight = 519
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Height = 478
    ActivePage = TabGrade
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 359
        inherited EdNome: TEdit
          Top = 17
          TabOrder = 0
        end
        inherited ButBusca: TBitBtn
          Top = 17
          TabOrder = 1
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 701
        end
        inherited EdCod: TEdit
          TabOrder = 2
        end
      end
      inherited DBGrid1: TJvDBGrid
        Height = 359
        Columns = <
          item
            Color = clWhite
            Expanded = False
            FieldName = 'SEG_ID'
            Title.Caption = 'Segmento ID'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Nome'
            Width = 339
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Title.Caption = 'Data '#250'ltima altera'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Title.Caption = 'Operador'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 431
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 418
      end
      inherited Panel3: TPanel
        Height = 418
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 818
          Height = 75
          Align = alTop
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 21
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 104
            Top = 21
            Width = 28
            Height = 13
            Caption = 'Nome'
            FocusControl = dbEdtNm
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 37
            Width = 81
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'SEG_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object dbEdtNm: TDBEdit
            Left = 104
            Top = 37
            Width = 289
            Height = 21
            CharCase = ecUpperCase
            DataField = 'DESCRICAO'
            DataSource = DSCadastro
            TabOrder = 1
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 357
          Width = 818
          Height = 59
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 1
          object Label43: TLabel
            Left = 8
            Top = 15
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
          end
          object Label44: TLabel
            Left = 136
            Top = 15
            Width = 44
            Height = 13
            Caption = 'Operador'
          end
          object DBEdit12: TDBEdit
            Left = 8
            Top = 30
            Width = 113
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 136
            Top = 30
            Width = 126
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited GridHistorico: TJvDBGrid
        Height = 403
      end
    end
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from segmentos where seg_id = 0')
    object QCadastroSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object QCadastroDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 58
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
  end
end
