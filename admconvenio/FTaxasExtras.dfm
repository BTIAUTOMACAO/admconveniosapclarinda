object FrmTaxasExtras: TFrmTaxasExtras
  Left = 417
  Top = 143
  BorderStyle = bsDialog
  Caption = 'Taxas Extras'
  ClientHeight = 339
  ClientWidth = 715
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 715
    Height = 339
    Align = alClient
    BevelWidth = 3
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 117
      Width = 90
      Height = 13
      Caption = 'Descri'#231#227'o da Taxa'
    end
    object Label2: TLabel
      Left = 408
      Top = 117
      Width = 66
      Height = 13
      Caption = 'Valor da Taxa'
    end
    object lbl1: TLabel
      Left = 144
      Top = 69
      Width = 124
      Height = 13
      Caption = 'Nome do Estabelecimento'
    end
    object lbl2: TLabel
      Left = 8
      Top = 69
      Width = 114
      Height = 13
      Caption = 'C'#243'digo Estabelecimento'
    end
    object Label3: TLabel
      Left = 8
      Top = 13
      Width = 72
      Height = 13
      Caption = 'C'#243'digo do Lote'
    end
    object BitBtn1: TBitBtn
      Left = 280
      Top = 167
      Width = 260
      Height = 25
      Caption = '&Gravar'
      ModalResult = 1
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object edtValor: TCurrencyEdit
      Left = 408
      Top = 136
      Width = 121
      Height = 21
      AutoSize = False
      TabOrder = 1
    end
    object btnCancelar: TBitBtn
      Left = 8
      Top = 167
      Width = 260
      Height = 25
      Caption = '&Cancelar'
      ModalResult = 1
      TabOrder = 2
      OnClick = btnCancelarClick
    end
    object edtCredId: TEdit
      Left = 8
      Top = 88
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 3
      Text = 'edtCredId'
    end
    object edtCodLote: TEdit
      Left = 8
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 4
      Text = 'edtData'
    end
    object edtCredNome: TEdit
      Left = 144
      Top = 88
      Width = 385
      Height = 21
      Enabled = False
      TabOrder = 5
      Text = 'edtCredNome'
    end
    object edtDescricao: TEdit
      Left = 8
      Top = 136
      Width = 385
      Height = 21
      TabOrder = 6
    end
  end
end
