// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\bella\Projetos\Local\AdmConvenio\wsdl\SaleService.asmx.xml
// Encoding : UTF-8
// Version  : 1.0
// (29/04/2014 19:45:56 - 1.33.2.5)
// ************************************************************************ //

unit SaleService2;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns, Classes, TypInfo, dialogs;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:long            - "http://www.w3.org/2001/XMLSchema"
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"

  GetInstallmentOptions = class;                { "SaleService" }
  InstallmentOption    = class;                 { "SaleService" }
  IdentificadorEstab   = class;                 { "SaleService" }
  CupomVendaResponse   = class;                 { "SaleService" }
  CupomVendaRequest    = class;                 { "SaleService" }
  ReversalResponse     = class;                 { "SaleService" }
  SaleResponse         = class;                 { "SaleService" }
  SaleRequest          = class;                 { "SaleService" }
  BalanceResponse      = class;                 { "SaleService" }
  BalanceRequest       = class;                 { "SaleService" }
  CancellationConfirmation = class;             { "SaleService" }
  SaleConfirmation     = class;                 { "SaleService" }
  CancellationResponse = class;                 { "SaleService" }
  SaleInfo             = class;                 { "SaleService" }
  ReversalRequest      = class;                 { "SaleService" }
  CancellationRequest  = class;                 { "SaleService" }



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  GetInstallmentOptions = class(TRemotable)
  private
    FacceptorId: Integer;
    FcardNumber: WideString;
  published
    property acceptorId: Integer read FacceptorId write FacceptorId;
    property cardNumber: WideString read FcardNumber write FcardNumber;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  InstallmentOption = class(TRemotable)
  private
    FinstallmentsNumber: Integer;
    FinstallmentAmount: TXSDecimal;
  public
    destructor Destroy; override;
  published
    property installmentsNumber: Integer read FinstallmentsNumber write FinstallmentsNumber;
    property installmentAmount: TXSDecimal read FinstallmentAmount write FinstallmentAmount;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  IdentificadorEstab = class(TRemotable)
  private
    FacceptorId: Int64;
    FacceptorTan: Integer;
    FterminalId: WideString;
    FlocalTransactionDateTime: TXSDateTime;
  public
    destructor Destroy; override;
  published
    property acceptorId: Int64 read FacceptorId write FacceptorId;
    property acceptorTan: Integer read FacceptorTan write FacceptorTan;
    property terminalId: WideString read FterminalId write FterminalId;
    property localTransactionDateTime: TXSDateTime read FlocalTransactionDateTime write FlocalTransactionDateTime;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  CupomVendaResponse = class(IdentificadorEstab)
  private
    FcardNumber: WideString;
    Famount: TXSDecimal;
    FinstallmentsNumber: Integer;
    Fcupom: WideString;
  public
    destructor Destroy; override;
  published
    property cardNumber: WideString read FcardNumber write FcardNumber;
    property amount: TXSDecimal read Famount write Famount;
    property installmentsNumber: Integer read FinstallmentsNumber write FinstallmentsNumber;
    property cupom: WideString read Fcupom write Fcupom;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  CupomVendaRequest = class(IdentificadorEstab)
  private
    FcardNumber: WideString;
    Famount: TXSDecimal;
    FinstallmentsNumber: Integer;
  public
    destructor Destroy; override;
  published
    property cardNumber: WideString read FcardNumber write FcardNumber;
    property amount: TXSDecimal read Famount write Famount;
    property installmentsNumber: Integer read FinstallmentsNumber write FinstallmentsNumber;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  ReversalResponse = class(IdentificadorEstab)
  private
    FresponseCode: WideString;
    FissuerTan: Int64;
  published
    property responseCode: WideString read FresponseCode write FresponseCode;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  SaleResponse = class(IdentificadorEstab)
  private
    Famount: TXSDecimal;
    FresponseCode: WideString;
    FresponseMessage: WideString;
    FissuerTan: Int64;
    FsaldoRestante: TXSDecimal;
    FnomePortadorCartao: WideString;
  public
    destructor Destroy; override;
  published
    property amount: TXSDecimal read Famount write Famount;
    property responseCode: WideString read FresponseCode write FresponseCode;
    property responseMessage: WideString read FresponseMessage write FresponseMessage;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
    property saldoRestante: TXSDecimal read FsaldoRestante write FsaldoRestante;
    property nomePortadorCartao: WideString read FnomePortadorCartao write FnomePortadorCartao;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  SaleRequest = class(IdentificadorEstab)
  private
    FcardNumber: WideString;
    FcardSecurityCode: WideString;
    FcardExpirationYear: Integer;
    FcardExpirationMonth: Integer;
    FcardPassword: WideString;
    Famount: TXSDecimal;
    FinstallmentsNumber: Integer;
    FwithInterestCharges: Boolean;
    FacquiringType: Integer;
  public
    destructor Destroy; override;
  published
    property cardNumber: WideString read FcardNumber write FcardNumber;
    property cardSecurityCode: WideString read FcardSecurityCode write FcardSecurityCode;
    property cardExpirationYear: Integer read FcardExpirationYear write FcardExpirationYear;
    property cardExpirationMonth: Integer read FcardExpirationMonth write FcardExpirationMonth;
    property cardPassword: WideString read FcardPassword write FcardPassword;
    property amount: TXSDecimal read Famount write Famount;
    property installmentsNumber: Integer read FinstallmentsNumber write FinstallmentsNumber;
    property withInterestCharges: Boolean read FwithInterestCharges write FwithInterestCharges;
    property acquiringType: Integer read FacquiringType write FacquiringType;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  BalanceResponse = class(IdentificadorEstab)
  private
    FresponseMessage: WideString;
    FbalanceAmount: TXSDecimal;
    FresponseCode: WideString;
    FissuerTan: Int64;
  public
    destructor Destroy; override;
  published
    property responseMessage: WideString read FresponseMessage write FresponseMessage;
    property balanceAmount: TXSDecimal read FbalanceAmount write FbalanceAmount;
    property responseCode: WideString read FresponseCode write FresponseCode;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  BalanceRequest = class(IdentificadorEstab)
  private
    FcardNumber: WideString;
    FcardSecurityCode: WideString;
    FcardExpirationYear: Integer;
    FcardExpirationMonth: Integer;
    FcardPassword: WideString;
    FacquiringType: Integer;
  published
    property cardNumber: WideString read FcardNumber write FcardNumber;
    property cardSecurityCode: WideString read FcardSecurityCode write FcardSecurityCode;
    property cardExpirationYear: Integer read FcardExpirationYear write FcardExpirationYear;
    property cardExpirationMonth: Integer read FcardExpirationMonth write FcardExpirationMonth;
    property cardPassword: WideString read FcardPassword write FcardPassword;
    property acquiringType: Integer read FacquiringType write FacquiringType;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  CancellationConfirmation = class(IdentificadorEstab)
  private
    ForiginalTransactionAmmount: TXSDecimal;
    FresponseCode: WideString;
    FissuerTan: Int64;
  public
    destructor Destroy; override;
  published
    property originalTransactionAmmount: TXSDecimal read ForiginalTransactionAmmount write ForiginalTransactionAmmount;
    property responseCode: WideString read FresponseCode write FresponseCode;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  SaleConfirmation = class(IdentificadorEstab)
  private
    Famount: TXSDecimal;
    FresponseCode: WideString;
    FissuerTan: Int64;
  public
    destructor Destroy; override;
  published
    property amount: TXSDecimal read Famount write Famount;
    property responseCode: WideString read FresponseCode write FresponseCode;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  CancellationResponse = class(IdentificadorEstab)
  private
    FresponseCode: WideString;
    FresponseMessage: WideString;
    FissuerTan: Int64;
  published
    property responseCode: WideString read FresponseCode write FresponseCode;
    property responseMessage: WideString read FresponseMessage write FresponseMessage;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  SaleInfo = class(IdentificadorEstab)
  private
    FcardNumber: WideString;
    Famount: TXSDecimal;
    FwithInterestCharges: Boolean;
    FissuerTan: Int64;
  public
    destructor Destroy; override;
  published
    property cardNumber: WideString read FcardNumber write FcardNumber;
    property amount: TXSDecimal read Famount write Famount;
    property withInterestCharges: Boolean read FwithInterestCharges write FwithInterestCharges;
    property issuerTan: Int64 read FissuerTan write FissuerTan;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  ReversalRequest = class(IdentificadorEstab)
  private
    FsaleInfo: SaleInfo;
    FacquiringType: Integer;
  public
    destructor Destroy; override;
  published
    property saleInfo: SaleInfo read FsaleInfo write FsaleInfo;
    property acquiringType: Integer read FacquiringType write FacquiringType;
  end;



  // ************************************************************************ //
  // Namespace : SaleService
  // ************************************************************************ //
  CancellationRequest = class(IdentificadorEstab)
  private
    FsaleInfo: SaleInfo;
    FacquiringType: Integer;
  public
    destructor Destroy; override;
  published
    property saleInfo: SaleInfo read FsaleInfo write FsaleInfo;
    property acquiringType: Integer read FacquiringType write FacquiringType;
  end;


  // ************************************************************************ //
  // Namespace : SaleService
  // soapAction: SaleService/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : SaleServiceSoap
  // service   : SaleService
  // port      : SaleServiceSoap
  // URL       : http://localhost:1256/SaleService.asmx
  // ************************************************************************ //
  SaleServiceSoap = interface(IInvokable)
  ['{EB93767E-B41A-24EE-2C96-3FC57CA85A5F}']
    function  cancellationRequest(const request: CancellationRequest): CancellationResponse; stdcall;
    procedure confirm(const request: SaleConfirmation; const cartao: WideString); stdcall;
    procedure confirmCancellation(const request: CancellationConfirmation; const cardNumber: WideString); stdcall;
    function  getBalance(const request: BalanceRequest): BalanceResponse; stdcall;
    function  getInstallmentOptions(const request: GetInstallmentOptions): InstallmentOption; stdcall;
    function  saleRequest(const request: SaleRequest): SaleResponse; stdcall;
    function  reverse(const request: ReversalRequest): ReversalResponse; stdcall;
    function  getCupomVenda(const request: CupomVendaRequest): CupomVendaResponse; stdcall;
    function  MTeste(const xml: WideString): WideString; stdcall;
  end;

function GetSaleServiceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): SaleServiceSoap;


implementation

function GetSaleServiceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): SaleServiceSoap;
const
  defWSDL = 'C:\bella\Projetos\Local\AdmConvenio\wsdl\SaleService.asmx.xml';
  defURL  = 'http://localhost:1256/SaleService.asmx';
  defSvc  = 'SaleService';
  defPrt  = 'SaleServiceSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as SaleServiceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor InstallmentOption.Destroy;
begin
  if Assigned(FinstallmentAmount) then
    FinstallmentAmount.Free;
  inherited Destroy;
end;

destructor IdentificadorEstab.Destroy;
begin
  if Assigned(FlocalTransactionDateTime) then
    FlocalTransactionDateTime.Free;
  inherited Destroy;
end;

destructor CupomVendaResponse.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  inherited Destroy;
end;

destructor CupomVendaRequest.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  inherited Destroy;
end;

destructor SaleResponse.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  if Assigned(FsaldoRestante) then
    FsaldoRestante.Free;
  inherited Destroy;
end;

destructor SaleRequest.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  inherited Destroy;
end;

destructor BalanceResponse.Destroy;
begin
  if Assigned(FbalanceAmount) then
    FbalanceAmount.Free;
  inherited Destroy;
end;

destructor CancellationConfirmation.Destroy;
begin
  if Assigned(ForiginalTransactionAmmount) then
    ForiginalTransactionAmmount.Free;
  inherited Destroy;
end;

destructor SaleConfirmation.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  inherited Destroy;
end;

destructor SaleInfo.Destroy;
begin
  if Assigned(Famount) then
    Famount.Free;
  inherited Destroy;
end;

destructor ReversalRequest.Destroy;
begin
  if Assigned(FsaleInfo) then
    FsaleInfo.Free;
  inherited Destroy;
end;

destructor CancellationRequest.Destroy;
begin
  if Assigned(FsaleInfo) then
    FsaleInfo.Free;
  inherited Destroy;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(SaleServiceSoap), 'SaleService', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(SaleServiceSoap), 'SaleService/%operationName%');
  RemClassRegistry.RegisterXSClass(GetInstallmentOptions, 'SaleService', 'GetInstallmentOptions');
  RemClassRegistry.RegisterXSClass(InstallmentOption, 'SaleService', 'InstallmentOption');
  RemClassRegistry.RegisterXSClass(IdentificadorEstab, 'SaleService', 'IdentificadorEstab');
  RemClassRegistry.RegisterXSClass(CupomVendaResponse, 'SaleService', 'CupomVendaResponse');
  RemClassRegistry.RegisterXSClass(CupomVendaRequest, 'SaleService', 'CupomVendaRequest');
  RemClassRegistry.RegisterXSClass(ReversalResponse, 'SaleService', 'ReversalResponse');
  RemClassRegistry.RegisterXSClass(SaleResponse, 'SaleService', 'SaleResponse');
  RemClassRegistry.RegisterXSClass(SaleRequest, 'SaleService', 'SaleRequest');
  RemClassRegistry.RegisterXSClass(BalanceResponse, 'SaleService', 'BalanceResponse');
  RemClassRegistry.RegisterXSClass(BalanceRequest, 'SaleService', 'BalanceRequest');
  RemClassRegistry.RegisterXSClass(CancellationConfirmation, 'SaleService', 'CancellationConfirmation');
  RemClassRegistry.RegisterXSClass(SaleConfirmation, 'SaleService', 'SaleConfirmation');
  RemClassRegistry.RegisterXSClass(CancellationResponse, 'SaleService', 'CancellationResponse');
  RemClassRegistry.RegisterXSClass(SaleInfo, 'SaleService', 'SaleInfo');
  RemClassRegistry.RegisterXSClass(ReversalRequest, 'SaleService', 'ReversalRequest');
  RemClassRegistry.RegisterXSClass(CancellationRequest, 'SaleService', 'CancellationRequest');
  InvRegistry.RegisterInvokeOptions(TypeInfo(SaleServiceSoap), [ioDefault]);
  //InvRegistry.RegisterInvokeOptions(TypeInfo(SaleServiceSoap), [ioDefault]);
  //InvRegistry.RegisterInvokeOptions(TypeInfo(SaleServiceSoap), ioDefault);
  //, ioDocument, ioHasReturnParamNames, ioHasNamespace
end. 