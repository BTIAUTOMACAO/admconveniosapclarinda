unit uConvDependentes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, Grids, DBGrids, DB, JvExDBGrids, JvDBGrid, ADODB;

type
  TfrmConvDependentes = class(TForm)
    btnSair: TButton;
    lblTitular: TLabel;
    DSDependentes: TDataSource;
    JvDBGrid1: TJvDBGrid;
    QDependentes: TADOQuery;
    QDependentesnome: TStringField;
    QDependentescodcartimp: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSairClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConvDependentes: TfrmConvDependentes;

implementation

uses cartao_util, URotinasGrids, DM;

{$R *.dfm}

procedure TfrmConvDependentes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Self.Close;
end;

procedure TfrmConvDependentes.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmConvDependentes.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  Screen.Cursor := crHourGlass;
  SortZQuery(qDependentes,Field.FieldName);
  Screen.Cursor := crDefault;
end;

procedure TfrmConvDependentes.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  pintaGrid(Sender,Rect,DataCol,Column,State);
end;

end.
