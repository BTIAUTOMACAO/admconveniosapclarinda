unit URelNotaFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, JvExStdCtrls,
  JvCombobox, JvDBCombobox, Mask, JvExMask, JvToolEdit, DB, ADODB,
  JvExControls, JvDBLookup, Grids, DBGrids, JvExDBGrids, JvDBGrid, DBCtrls;

type
  TfrmRelNotaFiscal = class(TF1)
    pnlPanAbe: TPanel;
    bvl1: TBevel;
    btnPesquisar: TBitBtn;
    grp4: TGroupBox;
    cbbPagamento: TJvDBComboBox;
    grpDatas: TGroupBox;
    lblDe: TLabel;
    lblA: TLabel;
    dataIni: TJvDateEdit;
    dataFim: TJvDateEdit;
    grp1: TGroupBox;
    qSeg: TADOQuery;
    qSegSEG_ID: TIntegerField;
    qSegDESCRICAO: TStringField;
    qSegAPAGADO: TStringField;
    qSegDTALTERACAO: TDateTimeField;
    qSegOPERADOR: TStringField;
    qSegDTAPAGADO: TDateTimeField;
    qSegOPERCADASTRO: TStringField;
    qSegDTCADASTRO: TDateTimeField;
    dsSeg: TDataSource;
    bvl3: TBevel;
    grp2: TGroupBox;
    grdNota: TJvDBGrid;
    qNotaFsical: TADOQuery;
    dsNota: TDataSource;
    qNotaFsicalCRED_ID: TIntegerField;
    qNotaFsicalNOME: TStringField;
    qNotaFsicalCGC: TStringField;
    qNotaFsicalVALOR_PAGO: TFloatField;
    qNotaFsicalTOTAL: TFloatField;
    qNotaFsicalIMPOSTO: TFloatField;
    qNotaFsicalNF: TStringField;
    dbTaxa: TMaskEdit;
    cmbSegmento: TJvDBLookupCombo;
    procedure FormCreate(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure dbTaxaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure AbrirPagamentos;
    procedure SQLFactory;
    procedure SQLDestroyString;
  public
    { Public declarations }
  end;

var
  frmRelNotaFiscal: TfrmRelNotaFiscal;
  sql : string;

implementation

uses DM,cartao_util;

{$R *.dfm}

procedure TfrmRelNotaFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  cbbPagamento.SetFocus;
  qseg.Open;
end;

procedure TfrmRelNotaFiscal.AbrirPagamentos;
begin
    SQLFactory;
    qNotaFsical.Close;
    qNotaFsical.SQL.Clear;
    qNotaFsical.SQL.Add(sql);
    qNotaFsical.Open;
    if(qNotaFsical.IsEmpty)then
    begin
      MsgInf('Os crit�rios de busca aplicados n�o retornaram nenhum valor!');
      exit;
    end;

    SQLDestroyString;
end;

procedure TfrmRelNotaFiscal.SQLFactory;
begin
   // O from � baseado na tabela credenciados cred
    sql := 'SELECT PAG.CRED_ID, CRED.NOME, CRED.CGC, ROUND(SUM(PAG.VALOR_PAGO),2) AS VALOR_PAGO, ' +
      'ROUND(SUM(PAG.VALOR_PAGO) / ' + StringReplace(dbTaxa.Text,',','.',[rfReplaceAll]) + ',2) AS TOTAL, ' +
      'ROUND(SUM(PAG.VALOR_PAGO) / ' + StringReplace(dbTaxa.Text,',','.',[rfReplaceAll]) + ' - ROUND(SUM(PAG.VALOR_PAGO),2),2) AS IMPOSTO, '' '' AS NF '+
      'FROM PAGAMENTO_CRED PAG '+
      'INNER JOIN CREDENCIADOS CRED ON CRED.CRED_ID = PAG.CRED_ID '  +
      'WHERE PAG.DATA_COMPENSACAO BETWEEN ''' +  dataIni.Text + ''' AND ''' + dataFim.Text  + '''' +
      'AND PAG.IDENTIFICACAO_BANCARIA = ''' + IntToStr(cbbPagamento.ItemIndex) + ''' AND PAG.CANCELADO <> ''S'' ';
      if cmbSegmento.KeyValue > 0 then
         sql := sql + ' and CRED.seg_id = '+ cmbSegmento.KeyValue;

      sql := sql + 'GROUP BY PAG.CRED_ID, CRED.NOME, CRED.CGC ';

end;

procedure TfrmRelNotaFiscal.SQLDestroyString;
begin
  sql := '';
end;

procedure TfrmRelNotaFiscal.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  if Trim(cbbPagamento.Text) = '' then
  begin
      MsgInf('Necess�rio selecionar Origem do Pagamento!');
      cbbPagamento.SetFocus;
      Exit;
  end
  else if Trim(StringReplace(dataIni.Text,'/','',[rfReplaceAll])) = '' then
    begin
      MsgInf('Data Inicial n�o pode ser em branco!');
      dataIni.SetFocus;
      Exit;
    end
    else if Trim(StringReplace(dataFim.Text,'/','',[rfReplaceAll])) = '' then
      begin
        MsgInf('Data Final n�o pode ser em branco!');
        dataFim.SetFocus;
        Exit;
      end
      else if Trim(dbTaxa.Text) = '' then
        begin
          MsgInf('Imposto n�o pode ser em branco!');
          dbTaxa.SetFocus;
          Exit;
        end
         else
            AbrirPagamentos;
end;

procedure TfrmRelNotaFiscal.dbTaxaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (key in [#13]) then
    btnPesquisar.Click;
end;

end.
