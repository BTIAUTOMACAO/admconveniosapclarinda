unit UFiltro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, Grids, DBGrids, {JvDBCtrl, JvMemDS,} Buttons, DBCtrls, Mask,
  JvToolEdit, ToolEdit, CurrEdit, Menus,  JvMemoryDataset,
  JvExMask, JvExDBGrids, JvDBGrid, JvExStdCtrls, JvEdit, JvValidateEdit,
  ADODB;

type
   TipoComparacao = (Igual,Maior_Igual,Menor_Igual,Maior,Menor,Entre,Iniciando_Com,Que_Contenha,
                     Diferente,Que_nao_Contenha,Terminando_Com,Nulo,Nao_Nulo);

  TFFiltro = class(TForm)
    Panel1: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    but1: TSpeedButton;
    but2: TSpeedButton;
    ButInsert: TButton;
    ButDelete: TButton;
    ButOk: TButton;
    ButCancel: TButton;
    JvDBGrid2: TJvDBGrid;
    JvDBGrid3: TJvDBGrid;
    DBFiltro: TDBLookupComboBox;
    GroupBox1: TGroupBox;
    LabConj: TLabel;
    LabTipoComp: TLabel;
    DBTipoComp: TDBComboBox;
    DBConj: TDBComboBox;
    DSFiltro: TDataSource;
    DSDetalhes: TDataSource;
    QCampos: TJvMemoryData;
    QCamposDescricao: TStringField;
    QCamposTipo: TStringField;
    DSCampos: TDataSource;
    PopupMenu1: TPopupMenu;
    Excluircampo1: TMenuItem;
    ButSum: TButton;
    PanComps: TPanel;
    LabVal1: TLabel;
    LabVal2: TLabel;
    alfa1: TEdit;
    alfa2: TEdit;
    data2: TJvDateEdit;
    data1: TJvDateEdit;
    Float1: TJvValidateEdit;
    Float2: TJvValidateEdit;
    QDetalhes: TADOQuery;
    QFiltro: TADOQuery;
    QFiltroFILTRO_ID: TIntegerField;
    QFiltroDESCRICAO: TStringField;
    QFiltroJANELA: TStringField;
    QDetalhesID: TIntegerField;
    QDetalhesFILTRO_ID: TIntegerField;
    QDetalhesFIELD_NAME: TStringField;
    QDetalhesFIELD_TYPE: TStringField;
    QDetalhesCOMPARACAO_TIPO: TStringField;
    QDetalhesVALOR1: TStringField;
    QDetalhesVALOR2: TStringField;
    QDetalhesCONJUNCAO: TStringField;
    QDetalhesSUMARIO: TStringField;
    procedure QFiltroAfterScroll(DataSet: TDataSet);
    procedure Excluircampo1Click(Sender: TObject);
    procedure JvDBGrid2DblClick(Sender: TObject);
    procedure but1Click(Sender: TObject);
    procedure QDetalhesAfterScroll(DataSet: TDataSet);
    procedure DBTipoCompChange(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure ButInsertClick(Sender: TObject);
    procedure JvDBGrid3CellClick(Column: TColumn);
    procedure but2Click(Sender: TObject);
    procedure ButDeleteClick(Sender: TObject);
    procedure ButOkClick(Sender: TObject);
    procedure ButSumClick(Sender: TObject);
    procedure QDetalhesCalcFields(DataSet: TDataSet);
    procedure QDetalhesAfterDelete(DataSet: TDataSet);
    procedure QDetalhesAfterOpen(DataSet: TDataSet);
    procedure PanCompsExit(Sender: TObject);
    procedure JvDBGrid3Enter(Sender: TObject);
  private
    procedure MostrarCampos;
    procedure GravarDetalhes;
    function TipoCampo: TFieldType;
    { Private declarations }
  public
    { Public declarations }
    janela : string;
  end;

var
  FFiltro: TFFiltro;

implementation

uses DM, USumario, cartao_util;

{$R *.dfm}

procedure TFFiltro.QFiltroAfterScroll(DataSet: TDataSet);
begin
  QDetalhes.Close;
  QDetalhes.Parameters.ParamByName('filtro_id').Value := QFiltroFILTRO_ID.AsInteger;
  QDetalhes.Open;
end;

procedure TFFiltro.Excluircampo1Click(Sender: TObject);
begin
If not QDetalhes.IsEmpty then QDetalhes.Delete;
end;

procedure TFFiltro.JvDBGrid2DblClick(Sender: TObject);
begin
but1.Click;
end;

procedure TFFiltro.but1Click(Sender: TObject);
begin
if not QFiltro.IsEmpty then begin
   QDetalhes.Append;
   QDetalhesID.AsInteger        := DMConexao.getGeneratorValue('gen_filtro_det_id');
   QDetalhesFILTRO_ID.AsInteger := QFiltroFILTRO_ID.AsInteger;
   QDetalhesFIELD_NAME.AsString := QCamposDescricao.AsString;
   QDetalhesFIELD_TYPE.AsString := QCamposTipo.AsString;
   if QDetalhes.RecNo > 1 then QDetalhesCONJUNCAO.AsString  := 'E';
   QDetalhesCOMPARACAO_TIPO.AsString := 'Igual (=)';
   if IsFloatField(TipoCampo)    then QDetalhesVALOR1.AsString := '0,00';
   if IsIntegerField(TipoCampo)  then QDetalhesVALOR1.AsString := '0';
   if IsDateTimeField(TipoCampo) then QDetalhesVALOR1.AsString := FormatDateTime('dd/mm/yyyy',Date);
   QDetalhes.Post;
   JvDBGrid3.SetFocus;
end
else begin
   MsgInf('Crie um novo filtro!');
   ButInsert.SetFocus;
end;
end;

procedure TFFiltro.QDetalhesAfterScroll(DataSet: TDataSet);
begin
   MostrarCampos;
end;

procedure TFFiltro.DBTipoCompChange(Sender: TObject);
begin
 MostrarCampos;
end;

procedure TFFiltro.ButCancelClick(Sender: TObject);
begin
 FFiltro.ModalResult := mrCancel;
end;

procedure TFFiltro.ButInsertClick(Sender: TObject);
var
  NewFiltro: string;
  OK: Boolean;
begin
  NewFiltro := '';
  if InputQuery('Criando novo filtro','Descri��o',NewFiltro) then begin
     if Trim(NewFiltro) <> '' then begin
        NewFiltro := UpperCase(NewFiltro);
        if QFiltro.Locate('DESCRICAO',NewFiltro,[]) then begin
           ShowMessage('Descri��o de filtro j� exitente.'+#13+'Por favor altere a descri��o');
           ButInsert.Click;
           Exit;
        end;
        QFiltro.Append;
        QFiltroFILTRO_ID.AsInteger := DMConexao.getGeneratorValue('gen_filtro_id');
        QFiltroDESCRICAO.AsString := NewFiltro;
        QFiltroJANELA.AsString := janela;
        QFiltro.Post;
        DBFiltro.KeyValue := QFiltroFILTRO_ID.AsVariant;
        MostrarCampos;
        JvDBGrid2.SetFocus;
     end
     else
        ShowMessage('Descri��o do filtro obrigat�ria!');
  end;
end;

procedure TFFiltro.JvDBGrid3CellClick(Column: TColumn);
begin
if not QDetalhes.IsEmpty then
   MostrarCampos;
end;

procedure TFFiltro.but2Click(Sender: TObject);
begin
if not QDetalhes.IsEmpty then
   QDetalhes.Delete;
end;

procedure TFFiltro.ButDeleteClick(Sender: TObject);
begin
if not QFiltro.IsEmpty then begin
   if Application.MessageBox('Confirma a exclus�o deste filtro?','Confirma��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDYes then begin
      while not QDetalhes.Eof do QDetalhes.Delete;
      QFiltro.Delete;
      DBFiltro.KeyValue := QFiltroFILTRO_ID.AsInteger;
   end;
end;
end;

procedure TFFiltro.ButOkClick(Sender: TObject);
begin
   if not QFiltro.IsEmpty then begin
      GravarDetalhes;
      FFiltro.ModalResult := mrOk;
   end
   else
      MsgInf('Nenhum filtro selecionado');
end;

procedure TFFiltro.ButSumClick(Sender: TObject);
begin
   FSumario := TFSumario.Create(Self);
   FSumario.ShowModal;
   FSumario.Free;
end;

procedure TFFiltro.QDetalhesCalcFields(DataSet: TDataSet);
begin
if QDetalhesCOMPARACAO_TIPO.AsString = 'Entre (>= e <=)' then
   QDetalhesSUMARIO.AsString := 'Entre ('+QDetalhesVALOR1.AsString+' e '+QDetalhesVALOR2.AsString+')'
else if QDetalhesCOMPARACAO_TIPO.AsString = 'Iniciando Com' then
   QDetalhesSUMARIO.AsString := 'Iniciando com ('+QDetalhesVALOR1.AsString+')'
else if QDetalhesCOMPARACAO_TIPO.AsString = 'Que Contenha' then
   QDetalhesSUMARIO.AsString := 'Que Contenha ('+QDetalhesVALOR1.AsString+')'
else QDetalhesSUMARIO.AsString := QDetalhesCOMPARACAO_TIPO.AsString+' a '+QDetalhesVALOR1.AsString;
end;

procedure TFFiltro.QDetalhesAfterDelete(DataSet: TDataSet);
begin
 if QDetalhes.RecNo = 1 then begin
    QDetalhes.Edit;
    QDetalhesCONJUNCAO.Clear;
    QDetalhes.Post;
 end;
 QDetalhesAfterOpen(nil);
end;

procedure TFFiltro.QDetalhesAfterOpen(DataSet: TDataSet);
begin
MostrarCampos;
end;

procedure TFFiltro.MostrarCampos;
var tpField : TFieldType;
begin
   LabTipoComp.Visible  := not QDetalhes.IsEmpty;
   DBTipoComp.Visible   := not QDetalhes.IsEmpty;
   LabVal1.Visible      := not QDetalhes.IsEmpty;
   LabConj.Visible := QDetalhes.RecNo > 1;
   DBConj.Visible  := QDetalhes.RecNo > 1;
   if (DBTipoComp.ItemIndex = integer(Nulo)) or (DBTipoComp.ItemIndex = integer(nao_nulo)) then
      PanComps.Visible := False
   else begin
      PanComps.Visible := True;
      Float1.Visible  := IsNumericField(TipoCampo);
      data1.Visible   := IsDateTimeField(TipoCampo);
      alfa1.Visible   := IsStringField(TipoCampo);
      LabVal2.Visible := DBTipoComp.ItemIndex = Integer(Entre);
      data2.Visible   := data1.Visible  and LabVal2.Visible;
      alfa2.Visible   := alfa1.Visible  and LabVal2.Visible;
      Float2.Visible  := Float1.Visible and LabVal2.Visible;
      if QDetalhes.State <> dsInsert then begin
         tpField := TipoCampo;
         if IsNumericField(tpField) then begin
           if IsIntegerField(tpField) then begin
             Float1.DisplayFormat := dfInteger;
             Float2.DisplayFormat := dfInteger;
           end else if IsFloatField(tpField) then begin
             Float1.DisplayFormat := dfFloat;
             Float2.DisplayFormat := dfFloat;
           end;
           Float1.Value := Coalesce(QDetalhesVALOR1.AsVariant);
           Float2.Value := Coalesce(QDetalhesVALOR2.AsVariant);
         end
         else if IsDateTimeField(tpField) then begin
           data1.Text   := QDetalhesVALOR1.Text;
           data2.Text   := QDetalhesVALOR2.Text;
         end
         else begin
           alfa1.Text   := QDetalhesVALOR1.AsString;
           alfa2.Text   := QDetalhesVALOR2.AsString;
         end;
      end;
   end;
end;

procedure TFFiltro.GravarDetalhes;
begin
   QDetalhes.Edit;
   if IsNumericField(TipoCampo) then begin
     QDetalhesVALOR1.AsVariant := Float1.Value;
     QDetalhesVALOR2.AsVariant := Float2.Value;
   end
   else if IsDateTimeField(TipoCampo) then begin
     QDetalhesVALOR1.Text := data1.Text;
     QDetalhesVALOR2.Text := data2.Text;
   end
   else begin
     QDetalhesVALOR1.AsString := alfa1.Text;
     QDetalhesVALOR2.AsString := alfa2.Text;
   end;
   QDetalhes.Post;
end;

function TFFiltro.TipoCampo:TFieldType;
var teste : TFieldType;
begin
teste := TFieldType( StringToEnum(QDetalhesFIELD_TYPE.AsString, TypeInfo(TFieldType)) );
Result := teste;
end;

procedure TFFiltro.PanCompsExit(Sender: TObject);
begin
   GravarDetalhes;
end;

procedure TFFiltro.JvDBGrid3Enter(Sender: TObject);
begin
   MostrarCampos;
end;

end.

