inherited frmIdentificacaoBancariaRepasse: TfrmIdentificacaoBancariaRepasse
  Left = 250
  Top = 77
  Caption = 'Identifica'#231#227'o Banc'#225'ria'
  ClientHeight = 662
  ClientWidth = 980
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 980
    inherited panTitulo: TPanel
      Caption = '  Identifica'#231#227'o Banc'#225'ria'
    end
  end
  object pnlPanAbe: TPanel [1]
    Left = 0
    Top = 23
    Width = 980
    Height = 194
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object bvl1: TBevel
      Left = 777
      Top = 1
      Width = 7
      Height = 193
    end
    object txtFormaPgtoDoRepasse: TLabel
      Left = 204
      Top = 64
      Width = 203
      Height = 20
      Caption = 'txtFormaPgtoDoRepasse'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object txtDataDoPagamento: TLabel
      Left = 355
      Top = 88
      Width = 154
      Height = 20
      Caption = 'txtDataDoRepasse'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel
      Left = 203
      Top = 86
      Width = 146
      Height = 20
      Caption = 'Data do Repasse:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object bvl2: TBevel
      Left = 184
      Top = 0
      Width = 9
      Height = 113
    end
    object bvl3: TBevel
      Left = 1
      Top = 113
      Width = 776
      Height = 7
    end
    object grpDatas: TGroupBox
      Left = 458
      Top = 7
      Width = 243
      Height = 43
      Caption = 'Data da Baixa Fatura'
      TabOrder = 1
      object lblDe: TLabel
        Left = 2
        Top = 23
        Width = 14
        Height = 13
        Caption = 'De'
      end
      object lblA: TLabel
        Left = 125
        Top = 21
        Width = 7
        Height = 13
        Caption = #192
      end
      object dataIni: TJvDateEdit
        Left = 24
        Top = 16
        Width = 97
        Height = 21
        Enabled = False
        ShowNullDate = False
        TabOrder = 0
      end
      object DataFin: TJvDateEdit
        Left = 136
        Top = 16
        Width = 97
        Height = 21
        Enabled = False
        ShowNullDate = False
        TabOrder = 1
      end
    end
    object grp2: TGroupBox
      Left = 199
      Top = 9
      Width = 243
      Height = 43
      Caption = 'Data de Compensa'#231#227'o Fatura'
      TabOrder = 2
      object lbl2: TLabel
        Left = 2
        Top = 23
        Width = 14
        Height = 13
        Caption = 'De'
      end
      object dataCompensa: TJvDateEdit
        Left = 24
        Top = 16
        Width = 97
        Height = 21
        Enabled = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object grp3: TGroupBox
      Left = 7
      Top = 5
      Width = 171
      Height = 103
      Caption = 'N'#186' do Lote/Repasse'
      TabOrder = 0
      object txtCodLote: TEdit
        Left = 7
        Top = 16
        Width = 121
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnKeyPress = txtCodLoteKeyPress
      end
    end
    object btnConsultar: TBitBtn
      Left = 13
      Top = 55
      Width = 154
      Height = 44
      Caption = 'Consultar'
      TabOrder = 3
      OnClick = btnConsultarClick
      Glyph.Data = {
        7E090000424D7E0900000000000036000000280000001D0000001B0000000100
        1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
        A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
        EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
        AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
        596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
        A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
        D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
        C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
        89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
        D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
        C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
        EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
        F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
        BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
        FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
        EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
        99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
        FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
        D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
        BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
        FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
        C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
        ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
        F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
        FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
        C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
        FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
        BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
        CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400}
    end
    object grp1: TGroupBox
      Left = 7
      Top = 117
      Width = 442
      Height = 68
      Caption = 'Banco'
      TabOrder = 4
      object cmbBancosLotes: TJvDBLookupCombo
        Left = 7
        Top = 32
        Width = 410
        Height = 20
        Hint = 'Selecione a forma de pagamento para pesquisa'
        DropDownCount = 7
        DeleteKeyClear = False
        EmptyValue = '0'
        LookupField = 'cod_banco'
        LookupDisplay = 'descricao'
        LookupSource = dsBanco
        TabOrder = 0
      end
    end
    object btnFiltrar: TBitBtn
      Left = 459
      Top = 137
      Width = 126
      Height = 47
      Caption = 'Consultar'
      TabOrder = 5
      OnClick = btnFiltrarClick
      Glyph.Data = {
        7E090000424D7E0900000000000036000000280000001D0000001B0000000100
        1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
        A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
        EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
        AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
        596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
        A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
        D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
        C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
        89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
        D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
        C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
        EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
        F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
        BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
        FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
        EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
        99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
        FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
        D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
        BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
        FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
        C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
        ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
        F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
        FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
        C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
        FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
        BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
        CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400}
    end
    object grp4: TGroupBox
      Left = 591
      Top = 117
      Width = 178
      Height = 68
      Caption = 'Origem do Pagamento'
      TabOrder = 6
      object cbbPagamento: TJvDBComboBox
        Left = 9
        Top = 32
        Width = 160
        Height = 21
        DropDownCount = 4
        Items.Strings = (
          'BELLA'
          'CDC'
          'PRATICARD'
          'RPC')
        TabOrder = 0
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
        ListSettings.OutfilteredValueFont.Style = []
      end
    end
  end
  object pnl1: TPanel [2]
    Left = 0
    Top = 620
    Width = 980
    Height = 42
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    BorderStyle = bsSingle
    TabOrder = 2
    object bvl4: TBevel
      Left = 350
      Top = 4
      Width = 2
      Height = 29
    end
    object BtnMarcDesm: TButton
      Left = 7
      Top = 5
      Width = 105
      Height = 25
      Hint = 'Marcar ou desmarcar um fornecedor'
      Caption = 'Marca/Desm.(F11)'
      TabOrder = 3
      Visible = False
      OnClick = BtnMarcDesmClick
    end
    object BtnMarcaTodos: TButton
      Left = 118
      Top = 5
      Width = 105
      Height = 25
      Hint = 'Marcar todos os fornecedores'
      Caption = 'Marcar Todos(F8)'
      TabOrder = 1
      Visible = False
      OnClick = BtnMarcaTodosClick
    end
    object BtnDesmTodos: TButton
      Left = 232
      Top = 5
      Width = 105
      Height = 25
      Hint = 'Desmarcar todos os fornecedores'
      Caption = 'Desm. Todos (F9)'
      TabOrder = 2
      Visible = False
      OnClick = BtnDesmTodosClick
    end
    object btnGravar: TButton
      Left = 362
      Top = 5
      Width = 159
      Height = 25
      Caption = 'Gravar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnGravarClick
    end
  end
  object grdPgtoEstab: TJvDBGrid [3]
    Left = 0
    Top = 217
    Width = 980
    Height = 403
    Align = alClient
    DataSource = dsPgtoEstab
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = grdPgtoEstabDblClick
    AutoAppend = False
    TitleButtons = True
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'cred_id'
        Title.Caption = 'Cred. ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'
        Title.Caption = 'Nome'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'correntista'
        Title.Caption = 'Correntista'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'bruto'
        Title.Caption = 'Bruto R$'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'comissao'
        Title.Caption = 'Taxa %'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'comissao_adm'
        Title.Caption = 'Comiss'#227'o Adm. R$'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'tx_dvv'
        Title.Caption = 'DVV'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'taxa_extra'
        Title.Caption = 'Taxas Extras R$ '
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'taxas_avulsa'
        Title.Caption = 'Taxas Avulsas R$'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'liquido'
        Title.Caption = 'L'#237'quido/Repasse R$'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'codbanco'
        Title.Caption = 'Cod. Banco'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'agencia'
        Title.Caption = 'Ag'#234'ncia'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contacorrente'
        Title.Caption = 'Conta Corrente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome_banco'
        Title.Caption = 'Nome do Banco'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'identificacaoBancaria'
        Title.Caption = 'Origem Pagamento'
        Width = 175
        Visible = True
      end>
  end
  inherited PopupBut: TPopupMenu
    Left = 804
    Top = 40
  end
  object dsBanco: TDataSource
    DataSet = qBanco
    Left = 768
    Top = 79
  end
  object qPgtoPor: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from paga_cred_por WHERE paga_cred_por_id <> 1')
    Left = 808
    Top = 120
    object qPgtoPorPAGA_CRED_POR_ID: TIntegerField
      FieldName = 'PAGA_CRED_POR_ID'
    end
    object qPgtoPorDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
  end
  object qBanco: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'lote'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT cred.banco as cod_banco, B.BANCO as descricao'
      'FROM PAGAMENTO_CRED PAG'
      'INNER JOIN CREDENCIADOS CRED ON CRED.CRED_ID = PAG.CRED_ID'
      'LEFT JOIN BANCOS B ON B.CODIGO = CRED.BANCO'
      'WHERE LOTE_PAGAMENTO =:lote')
    Left = 808
    Top = 79
    object intgrfldBancocod_banco: TIntegerField
      FieldName = 'cod_banco'
    end
    object strngfldBancodescricao: TStringField
      FieldName = 'descricao'
      Size = 45
    end
  end
  object MDPagtoEstab: TJvMemoryData
    Active = True
    Filtered = True
    FieldDefs = <
      item
        Name = 'nome'
        DataType = ftString
        Size = 120
      end
      item
        Name = 'correntista'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cgc'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'comissao'
        DataType = ftCurrency
      end
      item
        Name = 'contacorrente'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'agencia'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'bruto'
        DataType = ftCurrency
      end
      item
        Name = 'taxa_extra'
        DataType = ftCurrency
      end
      item
        Name = 'comissao_adm'
        DataType = ftCurrency
      end
      item
        Name = 'total_retido_adm'
        DataType = ftCurrency
      end
      item
        Name = 'liquido'
        DataType = ftCurrency
      end
      item
        Name = 'cred_id'
        DataType = ftInteger
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end
      item
        Name = 'diafechamento1'
        DataType = ftInteger
      end
      item
        Name = 'vencimento1'
        DataType = ftInteger
      end
      item
        Name = 'codbanco'
        DataType = ftInteger
      end
      item
        Name = 'nome_banco'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'baixado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'atrasado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'tx_dvv'
        DataType = ftFloat
      end
      item
        Name = 'pagamento_cred_id'
        DataType = ftInteger
      end
      item
        Name = 'endereco'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'numero'
        DataType = ftInteger
      end
      item
        Name = 'cidade'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cep'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'estado'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'complemento'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'telefone1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'conta_cdc'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'taxas_avulsa'
        DataType = ftCurrency
      end
      item
        Name = 'conferencia'
        DataType = ftCurrency
      end
      item
        Name = 'identificacaoBancaria'
        DataType = ftString
        Size = 20
      end>
    Left = 872
    Top = 128
    object MDPagtoEstabnome: TStringField
      FieldName = 'nome'
      Size = 120
    end
    object MDPagtoEstabcorrentista: TStringField
      FieldName = 'correntista'
    end
    object MDPagtoEstabcgc: TStringField
      FieldName = 'cgc'
    end
    object MDPagtoEstabcomissao: TCurrencyField
      FieldName = 'comissao'
    end
    object MDPagtoEstabcontacorrente: TStringField
      FieldName = 'contacorrente'
    end
    object MDPagtoEstabagencia: TStringField
      FieldName = 'agencia'
    end
    object MDPagtoEstabbruto: TCurrencyField
      FieldName = 'bruto'
    end
    object MDPagtoEstabtaxa_extra: TCurrencyField
      FieldName = 'taxa_extra'
    end
    object MDPagtoEstabcomissao_adm: TCurrencyField
      FieldName = 'comissao_adm'
    end
    object MDPagtoEstabtotal_retido_adm: TCurrencyField
      FieldName = 'total_retido_adm'
    end
    object MDPagtoEstabliquido: TCurrencyField
      FieldName = 'liquido'
    end
    object MDPagtoEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object MDPagtoEstabmarcado: TBooleanField
      FieldName = 'marcado'
    end
    object MDPagtoEstabdiafechamento1: TIntegerField
      FieldName = 'diafechamento1'
    end
    object MDPagtoEstabvencimento1: TIntegerField
      FieldName = 'vencimento1'
    end
    object MDPagtoEstabcodbanco: TIntegerField
      FieldName = 'codbanco'
    end
    object MDPagtoEstabnome_banco: TStringField
      FieldName = 'nome_banco'
      Size = 50
    end
    object MDPagtoEstabbaixado: TStringField
      FieldName = 'baixado'
    end
    object MDPagtoEstabatrasado: TStringField
      FieldName = 'atrasado'
    end
    object MDPagtoEstabtx_dvv: TFloatField
      FieldName = 'tx_dvv'
    end
    object MDPagtoEstabpagamento_cred_id: TIntegerField
      FieldName = 'pagamento_cred_id'
    end
    object MDPagtoEstabendereco: TStringField
      FieldName = 'endereco'
      Size = 60
    end
    object MDPagtoEstabnumero: TIntegerField
      FieldName = 'numero'
    end
    object MDPagtoEstabcidade: TStringField
      FieldName = 'cidade'
      Size = 60
    end
    object MDPagtoEstabcep: TStringField
      FieldName = 'cep'
    end
    object MDPagtoEstabestado: TStringField
      FieldName = 'estado'
      Size = 2
    end
    object MDPagtoEstabcomplemento: TStringField
      FieldName = 'complemento'
      Size = 60
    end
    object MDPagtoEstabtelefone1: TStringField
      FieldName = 'telefone1'
    end
    object MDPagtoEstabconta_cdc: TStringField
      FieldName = 'conta_cdc'
    end
    object MDPagtoEstabtaxas_avulsa: TCurrencyField
      FieldName = 'taxas_avulsa'
    end
    object MDPagtoEstabconferencia: TCurrencyField
      FieldName = 'conferencia'
    end
    object MDPagtoEstabidentificacaoBancaria: TStringField
      FieldName = 'identificacaoBancaria'
    end
  end
  object dsPgtoEstab: TDataSource
    DataSet = MDPagtoEstab
    Left = 872
    Top = 64
  end
  object qPgtoEstab: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT pc.cred_id, cred.nome, cred.cgc, pc.valor_total as bruto,' +
        ' pc.per_comissao as comissao, pc.valor_comissao as comissao_adm,' +
        ' pc.valor_pago as liquido, pc.data_hora as data_hora_repasse,'
      
        'pc.taxas_fixas as taxa_extra, pc.taxas_variaveis as taxas_avulsa' +
        ', pc.TAXA_DVV as tx_dvv, pc.paga_cred_por_id, paga_cred_por.desc' +
        'ricao as paga_cred_por_descricao, bancos.banco as nome_banco, '
      
        'cred.correntista, cred.banco as codbanco, cred.agencia, cred.con' +
        'tacorrente, pc.lote_pagamento,PC.PAGAMENTO_CRED_ID,'
      
        'CASE PC.IDENTIFICACAO_BANCARIA WHEN '#39'0'#39' THEN '#39'BELLA'#39' ELSE CASE P' +
        'C.IDENTIFICACAO_BANCARIA WHEN '#39'1'#39' THEN  '#39'CDC'#39' ELSE CASE PC.IDENT' +
        'IFICACAO_BANCARIA WHEN '#39'2'#39' THEN '#39'PRATICARD'#39' ELSE '#39'RPC'#39' END END E' +
        'ND AS IDENTIFICACAO_BANCARIA   from pagamento_cred pc inner join' +
        ' credenciados'
      
        ' cred ON cred.cred_id = pc.cred_id inner join PAGA_CRED_POR on p' +
        'aga_cred_por.PAGA_CRED_POR_ID = pc.PAGA_CRED_POR_ID inner join B' +
        'ANCOS on BANCOS.CODIGO = cred.BANCO where pc.LOTE_PAGAMENTO = 87' +
        '9'
      'and cred.BANCO =1')
    Left = 936
    Top = 96
    object qPgtoEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qPgtoEstabnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qPgtoEstabcgc: TStringField
      FieldName = 'cgc'
      Size = 18
    end
    object qPgtoEstabbruto: TFloatField
      FieldName = 'bruto'
    end
    object qPgtoEstabcomissao: TFloatField
      FieldName = 'comissao'
    end
    object qPgtoEstabcomissao_adm: TFloatField
      FieldName = 'comissao_adm'
    end
    object qPgtoEstabliquido: TFloatField
      FieldName = 'liquido'
    end
    object qPgtoEstabdata_hora_repasse: TDateTimeField
      FieldName = 'data_hora_repasse'
    end
    object qPgtoEstabtaxa_extra: TFloatField
      FieldName = 'taxa_extra'
    end
    object qPgtoEstabtaxas_avulsa: TFloatField
      FieldName = 'taxas_avulsa'
    end
    object qPgtoEstabtx_dvv: TBCDField
      FieldName = 'tx_dvv'
      Precision = 8
      Size = 2
    end
    object qPgtoEstabpaga_cred_por_id: TIntegerField
      FieldName = 'paga_cred_por_id'
    end
    object qPgtoEstabpaga_cred_por_descricao: TStringField
      FieldName = 'paga_cred_por_descricao'
      Size = 40
    end
    object qPgtoEstabnome_banco: TStringField
      FieldName = 'nome_banco'
      Size = 45
    end
    object qPgtoEstabcorrentista: TStringField
      FieldName = 'correntista'
      Size = 40
    end
    object qPgtoEstabcodbanco: TIntegerField
      FieldName = 'codbanco'
    end
    object qPgtoEstabagencia: TStringField
      FieldName = 'agencia'
      Size = 7
    end
    object qPgtoEstabcontacorrente: TStringField
      FieldName = 'contacorrente'
      Size = 15
    end
    object qPgtoEstablote_pagamento: TIntegerField
      FieldName = 'lote_pagamento'
    end
    object qPgtoEstabPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object qPgtoEstabIDENTIFICACAO_BANCARIA: TStringField
      FieldName = 'IDENTIFICACAO_BANCARIA'
      ReadOnly = True
      Size = 9
    end
  end
end
