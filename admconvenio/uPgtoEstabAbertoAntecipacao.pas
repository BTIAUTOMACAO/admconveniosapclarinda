unit uPgtoEstabAbertoAntecipacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, DB, ZAbstractDataset, ZDataset,
  ZAbstractRODataset, Grids, DBGrids, {JvDBCtrl,} StdCtrls, Mask, ToolEdit,
  {JvLookup,} DateUtils, DBCtrls, ClassImpressao, JvExDBGrids, JvDBGrid,
  JvExControls, JvDBLookup, {ClipBrd,} uDatas, frxClass, frxDBSet,
  JvExMask, JvToolEdit, ADODB, JvMemoryDataset, RxMemDS, DBClient,
  JvDialogs, frxExportPDF, JvMenus, pngimage,SispagEntidades, frxpngimage;

type ConsultaPor = (PorAutor,PorConv,PorNada);

type
  TfrmPgtoEstabAbertoAntecipacao = class(TF1)
    PanAbe: TPanel;
    Bevel4: TBevel;
    cbbEstab: TJvDBLookupCombo;
    edtEstab: TEdit;
    btnConsultar: TButton;
    gpbDatas: TGroupBox;
    lblDe: TLabel;
    lblA: TLabel;
    //DataFin: TDateEdit;
    //DataIni: TDateEdit;
    ckbTodasDatas: TCheckBox;
    GroupBox2: TGroupBox;
    cbbPgtoPor: TJvDBLookupCombo;
    panFatura: TPanel;
    Label3: TLabel;
    edtFatura: TEdit;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel4: TPanel;
    Panel5: TPanel;
    GridDescontos: TJvDBGrid;
    Panel7: TPanel;
    Panel8: TPanel;
    grdPgtoEstab: TJvDBGrid;
    Panel2: TPanel;
    Bevel7: TBevel;
    BtnImprimir: TBitBtn;
    BtnEfetuar: TButton;
    BtnMarcDesm: TButton;
    BtnMarcaTodos: TButton;
    BtnDesmTodos: TButton;
    ckQuebraPagina: TCheckBox;
    Panel9: TPanel;
    Bevel1: TBevel;
    Panel10: TPanel;
    Label12: TLabel;
    LabLiq: TLabel;
    Label14: TLabel;
    LabBruto: TLabel;
    Label8: TLabel;
    LabDesc: TLabel;
    Panel3: TPanel;
    dsEmpr: TDataSource;
    dsEstab: TDataSource;
    dsPgtoEstab: TDataSource;
    dsPgtoDesc: TDataSource;
    dsPgtoPor: TDataSource;
    rdgAutoriz: TRadioGroup;
    dsTemp: TDataSource;
    rdgDataPor: TRadioGroup;
    cbAtrasadas: TCheckBox;
    edtDia: TEdit;
    dsBancos: TDataSource;
    dataIni: TJvDateEdit;
    DataFin: TJvDateEdit;
    qPgtoPor: TADOQuery;
    qEmpr: TADOQuery;
    qEstab: TADOQuery;
    qPgtoEstab: TADOQuery;
    qTemp: TADOQuery;
    qBancos: TADOQuery;
    qPgtoPorPAGA_CRED_POR_ID: TIntegerField;
    qPgtoPorDESCRICAO: TStringField;
    qEmprempres_id: TIntegerField;
    qEmprnome: TStringField;
    qEmprfantasia: TStringField;
    qEstabcred_id: TIntegerField;
    qEstabnome: TStringField;
    qEstabfantasia: TStringField;
    frxReport1: TfrxReport;
    frxBancos: TfrxDBDataset;
    frxPgtoEstab: TfrxDBDataset;
    MDPagtoEstab: TJvMemoryData;
    MDPagtoEstabnome: TStringField;
    MDPagtoEstabcorrentista: TStringField;
    MDPagtoEstabcgc: TStringField;
    MDPagtoEstabcomissao: TCurrencyField;
    MDPagtoEstabcontacorrente: TStringField;
    MDPagtoEstabagencia: TStringField;
    MDPagtoEstabbruto: TCurrencyField;
    MDPagtoEstabtaxa_extra: TCurrencyField;
    MDPagtoEstabcomissao_adm: TCurrencyField;
    MDPagtoEstabtotal_retido_adm: TCurrencyField;
    MDPagtoEstabliquido: TCurrencyField;
    MDPagtoEstabcred_id: TIntegerField;
    MDPagtoEstabmarcado: TBooleanField;
    MDPagtoEstabdiafechamento1: TIntegerField;
    MDPagtoEstabvencimento1: TIntegerField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Edit1: TEdit;
    dataCompensa: TJvDateEdit;
    MDPagtoEstabCODBANCO: TIntegerField;
    MDPagtoEstabNOME_BANCO: TStringField;
    MDPagtoEstabBAIXADO: TStringField;
    MDPgtoDesc: TRxMemoryData;
    MDPgtoDesccred_id: TIntegerField;
    MDPgtoDesctaxa_id: TIntegerField;
    MDPgtoDescdescricao: TStringField;
    MDPgtoDescvalor: TFloatField;
    MDPgtoDescmarcado: TBooleanField;
    CDSPagDesc: TClientDataSet;
    CDSPagDesccred_id: TIntegerField;
    CDSPagDesctaxa_id: TIntegerField;
    CDSPagDescdescricao: TStringField;
    CDSPagDescvalor: TFloatField;
    CDSPagDescmarcado: TBooleanField;
    rdgPagamentos: TRadioGroup;
    qBancoscodbanco: TIntegerField;
    qBancosbanco: TStringField;
    rbEstab: TRadioGroup;
    qPgtoEstabcred_id: TIntegerField;
    qPgtoEstabnome: TStringField;
    qPgtoEstabdiafechamento1: TWordField;
    qPgtoEstabvencimento1: TWordField;
    qPgtoEstabCORRENTISTA: TStringField;
    qPgtoEstabcgc: TStringField;
    qPgtoEstabCOMISSAO: TBCDField;
    qPgtoEstabCONTACORRENTE: TStringField;
    qPgtoEstabAGENCIA: TStringField;
    qPgtoEstabCODBANCO: TIntegerField;
    qPgtoEstabNOME_BANCO: TStringField;
    qPgtoEstabBAIXADO: TStringField;
    qPgtoEstabBRUTO: TBCDField;
    qPgtoEstabTAXA_EXTRA: TFloatField;
    qPgtoEstabCOMISSAO_ADM: TBCDField;
    qPgtoEstabTOTAL_RETIDO_ADM: TFloatField;
    qPgtoEstabLIQUIDO: TFloatField;
    qPgtoEstabATRASADO: TStringField;
    MDPagtoEstabATRASADO: TStringField;
    cbbSetManual: TCheckBox;
    QAux: TADOQuery;
    DSAux: TDataSource;
    Bevel2: TBevel;
    Label1: TLabel;
    bntGerarPDF: TBitBtn;
    frxPDFExport1: TfrxPDFExport;
    SD: TJvSaveDialog;
    popCancTaxa: TPopupMenu;
    CancelaAutorizao1: TMenuItem;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    btnTaxaBanco: TButton;
    edTaxaBanco: TEdit;
    Button1: TButton;
    MDPagtoEstabTX_DVV: TFloatField;
    qPgtoEstabTX_DVV: TFloatField;
    QLancCred: TADOQuery;
    BtnCancelarPag: TButton;
    MDPagtoEstabPAGAMENTO_CRED_ID: TIntegerField;
    qPgtoDesc: TADOQuery;
    qPgtoDescCRED_ID: TIntegerField;
    qPgtoDesctaxa_id: TIntegerField;
    qPgtoDescdescricao: TStringField;
    qPgtoDescvalor: TFloatField;
    qPgtoDescmarcado: TStringField;
    qPgtoDescid: TIntegerField;
    qPgtoDescTAXAS_PROX_PAG_ID_FK: TIntegerField;
    QPgtoEmpr: TADOQuery;
    DSPgtoEmp: TDataSource;
    frxReport2: TfrxReport;
    QDescontosDePagtosBaixados: TADOQuery;
    QDescontosDePagtosBaixadoscred_id: TIntegerField;
    QDescontosDePagtosBaixadostaxa_id: TIntegerField;
    QDescontosDePagtosBaixadosdt_desconto: TDateTimeField;
    QDescontosDePagtosBaixadostaxas_prox_pag_id_fk: TIntegerField;
    QDescontosDePagtosBaixadosdescricao: TStringField;
    QDescontosDePagtosBaixadosvalor: TBCDField;
    QDescontosDePagtosBaixadosmarcado: TStringField;
    DSTeste: TDataSource;
    qPgtoDescTIPO_RECEITA: TStringField;
    QDescontosDePagtosBaixadosid: TIntegerField;
    QDescontosDePagtosBaixadosTIPO_RECEITA: TStringField;
    JvPopupMenu1: TJvPopupMenu;
    ConferirNotas1: TMenuItem;
    LanarTaxas1: TMenuItem;
    Image13: TImage;
    Image4: TImage;
    GridLog: TJvDBGrid;
    CheckBox1: TCheckBox;
    qPgtoEstabENDERECO: TStringField;
    qPgtoEstabNUMERO: TIntegerField;
    qPgtoEstabCIDADE: TStringField;
    qPgtoEstabCEP: TStringField;
    qPgtoEstabESTADO: TStringField;
    qPgtoEstabCOMPLEMENTO: TStringField;
    qPgtoEstabTELEFONE1: TStringField;
    MDPagtoEstabENDERECO: TStringField;
    MDPagtoEstabNUMERO: TIntegerField;
    MDPagtoEstabCIDADE: TStringField;
    MDPagtoEstabCEP: TStringField;
    MDPagtoEstabESTADO: TStringField;
    MDPagtoEstabCOMPLEMENTO: TStringField;
    MDPagtoEstabTELEFONE1: TStringField;
    rgbVendas: TRadioGroup;
    Panel11: TPanel;
    QPgtoEmprtrans_id: TIntegerField;
    QPgtoEmprempres_id: TIntegerField;
    QPgtoEmprfantasia: TStringField;
    QPgtoEmprconv_id: TIntegerField;
    QPgtoEmprdata: TDateTimeField;
    QPgtoEmprDEBITO: TBCDField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtEmprChange(Sender: TObject);
    procedure edtEstabChange(Sender: TObject);
    procedure cbbEmprChange(Sender: TObject);
    procedure cbbEstabChange(Sender: TObject);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure edtEstabKeyPress(Sender: TObject; var Key: Char);
    procedure DataIniExit(Sender: TObject);
    procedure qPgtoEstabCalcFields(DataSet: TDataSet);
    procedure btnConsultarClick(Sender: TObject);
    procedure cbbPgtoPorChange(Sender: TObject);
    procedure qPgtoEmprCalcFields(DataSet: TDataSet);
    procedure BtnMarcDesmClick(Sender: TObject);
    procedure BtnMarcaTodosClick(Sender: TObject);
    procedure BtnDesmTodosClick(Sender: TObject);
    procedure grdPgtoEstabKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdPgtoEstabDblClick(Sender: TObject);
    procedure dsPgtoEstabStateChange(Sender: TObject);
    procedure BtnImprimirClick(Sender: TObject);
    procedure BtnEfetuarClick(Sender: TObject);
    procedure dataCompensaChange(Sender: TObject);
    procedure dsPgtoEstabDataChange(Sender: TObject; Field: TField);
    procedure dsPgtoPorDataChange(Sender: TObject; Field: TField);
    procedure rdgDataPorClick(Sender: TObject);
    procedure edtDiaKeyPress(Sender: TObject; var Key: Char);
    procedure qPgtoEstabAfterScroll(DataSet: TDataSet);
    procedure qPgtoEstabBeforeOpen(DataSet: TDataSet);
    procedure qPgtoEmprBeforeOpen(DataSet: TDataSet);
    procedure GridDescontosDblClick(Sender: TObject);
    procedure qPgtoEstabAfterOpen(DataSet: TDataSet);
    procedure rbEstabClick(Sender: TObject);
    procedure grdPgtoEstabTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure MDPagtoEstabAfterPost(DataSet: TDataSet);
    procedure CancelaAutorizao1Click(Sender: TObject);
    procedure btnTaxaBancoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edTaxaBancoKeyPress(Sender: TObject; var Key: Char);
    procedure edTaxaBancoEnter(Sender: TObject);
    procedure BtnCancelarPagClick(Sender: TObject);
    procedure edTaxaBancoExit(Sender: TObject);
    procedure dataCompensaEnter(Sender: TObject);
    procedure dataCompensaExit(Sender: TObject);
    procedure ConferirNotas1Click(Sender: TObject);
    procedure LanarTaxas1Click(Sender: TObject);
    procedure Image13Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure rdgPagamentosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    listaDeTaxas : TList;
    PgtoEstab_Sel : String;
    strPgtoDesc_Sel : String;
    TotalGeral : Currency;
    procedure Pgto_Sel;
    procedure PgtoDesc_Sel;
    procedure AbrirPagamentos;
    procedure SQLFactory;
    procedure SqlGetLancFuturos;
    procedure SQLDestroyString;
    procedure AbrirValoresEmpresas;
    procedure CarregaTabelaTemp;
    procedure AbrirValoresDescontos;
    procedure ZerarLabels;
    procedure HabilitarBotoes;
    procedure SomarMarcados;
    procedure CriarCabecalho(Imp: TImpres);
    procedure CriarCorpo(Imp: TImpres; ConsultaPor: ConsultaPor=PorNada);
    procedure CriarTitulo(Imp: TImpres);
    procedure ImprimePorConveniado(Imp: TImpres);
    procedure ImprimePorAutorizacao(Imp: TImpres);
    procedure GravarPagamento(dt : TDateTime; COD_LOG : Integer);
    procedure GravarPgtoCredAutors(Pgto: Integer);
    procedure GravarPgtoCredDesc(Pgto: Integer; CredId : String);
    procedure GravarPgtoCredDet(Pgto: Integer);
    procedure GravarPgtoCredRep(Pgto, EstID: Integer);
    procedure GravarLogDoPagamento(pagamento_id: Integer);
    function getValorTaxasExtras(cred_id : Integer ; data_compensa : TDateTime) : Currency;
    function CalculaBrutoContaCorrenteByCredId(cred_id : integer) : Currency;
    function ValidaValoresPagamento(cred_id : Integer; vl_bruto : Currency): Boolean;
    function GetIdGrupoUsuario(idUsuario : Integer) : Integer;
    function CalculaTaxaDVV(ValorTaxaDVV : Double; ValorBruto : Double ; ValorComissaoAdm : Double) : Double;
    //function GetValorBrutoContaCorrente(cred_id : integer; data_ini : datetime; data_fin : datetime) : Currency;
    procedure GravaLogFinanceiro(Operacao : String; Valor_Ant : String; Valor_Pos : String; Cred_id : Integer; Cod_Operacao : Integer);
    procedure SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
    function SetUpSacado(NomeSacado: String; Logradouro: String; NumeroRes: Integer;
            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ: String) : TSacado;
//    function SetUpCedente(NomeFavorecido: String; Logradouro: String; NumeroRes: Integer;
//            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) : TCedente;

    function SetUpFavorecido(CodigoTranmissao, Logradouro, Bairro, NumeroRes, CEP,
    Cidade, CodigoFavorecido, ComplementoRes, Telefone, NomeFavorecido,
    Agencia, AgenciaDigito, Conta, ContaDigito, Modalidade, Convenio, CNPJCPF,
    UF, CodBanco: string): TCedente;
    procedure TearDownSacado();


  public
    { Public declarations }
    taxa_do_banco_old : Double;
    isCadCredOpen     : Boolean;
    SList             : TStringList;
    Sacado            : TSacado;
    Cedente           : TCedente;
    Favorecido        : TCedente;
end;

function CarregaArquivoDeRemessa() : String;

procedure RollBackTaxaAvulsa(Cred_id: Integer; Descricao: string; ValorDaTaxa:
    Double; DataDoDesconto: TDateTime; TipoReceita: String);

var
  frmPgtoEstabAbertoAntecipacao: TfrmPgtoEstabAbertoAntecipacao;
  sql : string;
  vvelho, vnovo, sqlQuery   : String;
  SqlLancamentosFuturos     : String;
  MarcouTodos,flagMarcado   : Boolean;
  //vCadEstab                 : Boolean;
  DesmarcouTodos            : Boolean;
  codigo_pagamento          : Integer;
  primeiraVezEfetuaPgto     : Integer; //Esta vari�vel controla se � a primeira vez que o m�todo � acessado. 0 = sim /1 = n�o

implementation

uses DM, cartao_util, UDBGridHelper, USelTipoImp, USQLMount, UMenu, Math,
   FEntregaNF, UCadCred, URotinasTexto, UCodLoteEstorno;

{$R *.dfm}

procedure TfrmPgtoEstabAbertoAntecipacao.FormCreate(Sender: TObject);
begin
  inherited;
  DataIni.Date := Date;
  DataFin.Date := Date;
  HabilitarBotoes;
  qEmpr.Open;
  qEstab.Open;
  qPgtoPor.Open;
  cbbPgtoPor.KeyValue := 1;
  taxa_do_banco_old := 0;
  isCadCredOpen := False;
  FMenu.vFormRepasseAntecipacao := True;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qEmpr.Close;
  qEstab.Close;
  qPgtoPor.Close;
  if qPgtoDesc.Active then qPgtoDesc.Close;
  if qPgtoEmpr.Active then qPgtoEmpr.Close;
  if qPgtoEstab.Active then qPgtoEstab.Close;
  FMenu.vFormRepasseAntecipacao := True;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edtEmprChange(Sender: TObject);
begin
  inherited;
  {if Trim(edtEmpr.Text) <> '' then
  begin
    if qEmpr.Locate('empres_id',edtEmpr.Text,[]) then
      cbbEmpr.KeyValue := edtEmpr.Text
    else
      cbbEmpr.ClearValue;
  end
  else
    cbbEmpr.ClearValue; }
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edtEstabChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEstab.Text) <> '' then
  begin
    if qEstab.Locate('cred_id',edtEstab.Text,[]) then
      cbbEstab.KeyValue := edtEstab.Text
    else
      cbbEstab.ClearValue;
  end
  else
    cbbEstab.ClearValue;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.cbbEmprChange(Sender: TObject);
begin
  inherited;
  {if edtEmpr.Text <> cbbEmpr.KeyValue then
    edtEmpr.Text := string(cbbEmpr.KeyValue);}
end;

procedure TfrmPgtoEstabAbertoAntecipacao.cbbEstabChange(Sender: TObject);
begin
  inherited;
  if edtEstab.Text <> cbbEstab.KeyValue then
    edtEstab.Text := string(cbbEstab.KeyValue);
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edtEmprKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edtEstabKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TfrmPgtoEstabAbertoAntecipacao.DataIniExit(Sender: TObject);
begin
  inherited;
  if cbbPgtoPor.KeyValue = 2 then
    DataFin.Date := EndOfTheMonth(DataIni.Date)
  else if (DataIni.Date > DataFin.Date) then
    DataFin.Date := DataIni.Date;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEstabCalcFields(DataSet: TDataSet);
begin
  inherited;
  qPgtoEstab.FieldByName('LIQUIDO').AsCurrency:= qPgtoEstab.FieldByName('BRUTO').AsCurrency -
  (qPgtoEstab.FieldByName('COMISS_C').AsCurrency + qPgtoEstab.FieldByName('COMISS_R').AsCurrency +
  qPgtoEstab.FieldByName('TAXA').AsCurrency) + (qPgtoEstab.FieldByName('REPASSE').AsCurrency);
end;

procedure TfrmPgtoEstabAbertoAntecipacao.btnConsultarClick(Sender: TObject);
begin
  inherited;
  ZerarLabels;
  Screen.Cursor := crHourGlass;
  DMConexao.ExecuteSql('delete from taxas_repasse_temp');

  if (cbbEstab.DisplayEmpty <> cbbEstab.Text) and (string(cbbEstab.KeyValue) <> edtEstab.Text) then begin
    MsgInf('O c�digo do Estabelecimento n�o corresponde ao estabelecimento selecionado.'+sLineBreak+'Por favor, selecione o estabelecimento correto.');
    cbbEstab.SetFocus;
    Exit;
  end;
  if cbbPgtoPor.KeyValue = 0 then
    MsgInf('Selecione uma forma de pagamento de estabelecimento.');

  if (cbbPgtoPor.KeyValue = 3) and (edtDia.Text = '') then
  begin
    MsgInf('Digite um dia do fechamento');
    edtDia.SetFocus;
    Exit;
  end;

  if integer(cbbPgtoPor.KeyValue) >= 1 then
  begin
    AbrirPagamentos;
    btnTaxaBanco.Enabled := true;
    Button1.Enabled      := true;
  end
  else
    raise Exception.Create('Forma de pagamento de Fornecedores Inv�lida!');

  //BtnCancelarPag.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.ZerarLabels;
begin
  LabLiq.Caption   := '0,00';
  LabBruto.Caption := '0,00';
  //LabComis.Caption := '0,00';
  LabDesc.Caption  := '0,00';
  MarcouTodos := False;
  DesmarcouTodos := False;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.SQLDestroyString;
begin
  sql := '';
end;

///<summary>
///Esta procedure monta uma query na vari�vel global sql para que seja executada na a��o click do bot�o Buscar
///</summary>
///<returns>
/// SQLFactory retorna a variavel global = sql Tipo = String
///</returns>
///<param name = "sql">Retorna query dinamica do GRID principal financeiro</param>

procedure TfrmPgtoEstabAbertoAntecipacao.SQLFactory;
begin
   // O from � baseado na tabela credenciados cred
   sql := 'SELECT * FROM('                +
   'SELECT '                          +
   'cred.cred_id,'                    +
   'cred.nome,'                       +
   'cred.ENDERECO,'                   +
   'cred.NUMERO,'                     +
   'cred.CIDADE,'                     +
   'cred.CEP,'                        +
   'cred.ESTADO,'                     +
   'cred.COMPLEMENTO,'                +
   'cred.TELEFONE1,'                  +
   'cred.diafechamento1,'             +
   'cred.vencimento1,'                +
   'cred.CORRENTISTA,'                +
   'cred.cgc,'                        +
   'cred.COMISSAO,'                   +
   'cred.CONTACORRENTE,'              ;
   if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 2 then
     sql:= sql + 'cast(((coalesce(cred.TX_DVV,0.0) * 41)/100) as float) as TX_DVV ,'
   else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 10 then
     sql:= sql + 'cast(((coalesce(cred.TX_DVV,0.23) * 30)/100)as float) as TX_DVV, '
   else
     sql := sql + 'cast(((coalesce(cred.TX_DVV,0.23) * 30)/100)as float) as TX_DVV, ';
   sql := sql + 'cred.AGENCIA,'        +
   'coalesce(cc.BAIXA_CREDENCIADO,''N'') AS BAIXADO, ' +
   //'cc.PAGAMENTO_CRED_ID, '          +
   'cred.BANCO as CODBANCO,'           +
   'ba.BANCO AS NOME_BANCO,'           +
   'COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'+
   '0.0 AS TAXA_EXTRA,'+
   //'(SELECT COALESCE(SUM(t.valor),0)'+
   //'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA,'+
   //-----------------------------------------------------------------------------
   //--seleciona TAXA ADMINISTRADORA(COMISS�O)'+
   '(coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0)) as COMISSAO_ADM,'+
   //'-- fim da consulta TAXA ADMINISTRADORA(COMISS�O)'+
   //'-----------------------------------------------------------------------------'+
   //'--seleciona TOTAL RETIDO PELA ADMINISTRADORA'+
   '(coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100))'+
   '+'+
   '(SELECT COALESCE(SUM(t.valor),0)'+
   'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'+
   'AS TOTAL_RETIDO_ADM,'+
   //'-- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'+
   //'-----------------------------------------------------------------------------'+
   //'--seleciona o TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
   //'--(BRUTO - (TAXAS EXTRAS - COMISSAO))'+
   '(CASE '+
   '   WHEN (COALESCE(SUM(CC.DEBITO - CC.CREDITO), 0)) > 0 THEN '+
   '      ((COALESCE(SUM(CC.DEBITO - CC.CREDITO), 0)) - ((COALESCE((SUM(CC.debito - CC.credito)) * (cred.COMISSAO / 100), 0)) + (SELECT '+
   '       COALESCE(SUM(t.valor), 0) '+
   '              FROM TAXAS t, '+
   '              rel_taxa_cred rtc '+
   '              WHERE t.taxa_id = rtc.TAXA_ID '+
   '              AND rtc.cred_id = cred.CRED_ID) '+
   ')) ELSE (SELECT SUM(TAXAS_PROX_PAG.VALOR) FROM TAXAS_PROX_PAG WHERE TIPO_RECEITA = ''C'' AND CRED_ID = '+qEstabcred_id.AsString+') END) AS LIQUIDO '+

   //'--Fim da consulta TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
   //'-----------------------------------------------------------------------------'+
   ', ''N'' as ATRASADO'+
   ' from credenciados cred'+
   ' LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between :DATA_INICIAL and :DATA_FINAL';
    //RETORNA APENAS AS AUTORIZA��ES CONFIRMADAS
    if rdgAutoriz.ItemIndex = 1 then
    begin
      sql := sql+' AND cc.conferido = ''S'' AND cc.cancelada = ''N''';
    end;
    //RETORNA APENAS OS PAGAMENTOS CONFIRMADOS
    if rdgPagamentos.ItemIndex = 1 then
    begin
      sql := sql+' AND cc.BAIXA_CREDENCIADO = ''N''';
    end;

    if rdgPagamentos.ItemIndex = 2 then
    begin
      sql := sql+' AND cc.BAIXA_CREDENCIADO = ''S''';
    end;
    //Exclui empresas que est�o cadastrados com o tipo de pagamento se efeito financ.
    sql := sql+' AND cc.EMPRES_ID not in(SELECT EMPRES_ID FROM empresas WHERE TEM_VALOR_FINANCEIRO = ''N'')';
    sql := sql+' LEFT JOIN BANCOS ba ON ba.codigo = cred.banco';

    if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 4 then
    begin
      sql := sql+' WHERE cred.PAGA_CRED_POR_ID in (4,1) ';
    end

    else
       sql := sql+' WHERE cred.PAGA_CRED_POR_ID = '+qPgtoPorPAGA_CRED_POR_ID.AsString +' AND cred.APAGADO = ''N''';
    //FAZ COM QUE A BUSCA RETORNE APENAS CRED. QUE TIVERAM O RENDIMENTO BRUTO MAIOR QUE 0 E LIQUIDO MAIOR QUE 0
    //'AND (SELECT CC.DEBITO - CC.CREDITO) > 0';

    if rbEstab.ItemIndex = 1 then
      sql := sql+' AND CRED.SEG_ID = 14';
    if rbEstab.ItemIndex = 0 then
      sql := sql+' AND CRED.SEG_ID = 39';

    if edtEstab.Text <> '' then
    begin
      sql := sql+' AND cred.CRED_ID = '+qEstabcred_id.AsString;
    end;

    if rdgPagamentos.ItemIndex < 2 then begin
      sql := sql+' GROUP BY cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
      ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV,cred.ENDERECO, cred.NUMERO, cred.CIDADE, cred.CEP, cred.ESTADO, cred.COMPLEMENTO, cred.TELEFONE1';

    end
    else
    begin
      sql := sql+' GROUP BY CC.PAGAMENTO_CRED_ID, cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
      ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV,cred.ENDERECO, cred.NUMERO, cred.CIDADE, cred.CEP, cred.ESTADO, cred.COMPLEMENTO, cred.TELEFONE1';

    end;
    if cbAtrasadas.Checked = false then begin
      sql := sql + ' )A ';
      if rgbVendas.ItemIndex = 0 then
        sql := sql + ' where A.bruto > 0'
    end;
end;



///
///<summary>pocedure montadora da query.text que retorna os lan�amentos "extra" D�bito(D) ou Cr�dito(C) lan�ados do cadastro de Credenciados/taxas
///</summary>
///<returns>
///var SqlLancamentosFuturos : String
//</returns>
///<remarks>
///Esta procedure � acionada quando marcamos uma linha no Grid Principal o intuito � calcular os valores adiantados para pagamentos futuros. 
///</remarks>
procedure TfrmPgtoEstabAbertoAntecipacao.SqlGetLancFuturos;
begin
   // Esta query visa trazer as taxas cadastradas no m�dulo de credenciados
   // ela pode ser tanto um valor de desconto ou valor a acresentar como receita para o credenciado.
   {SqlLancamentosFuturos := 'SELECT case a.tipo_receita when(''D'') then a.lanc_futuros else a.lanc_futuros  end as valor,a.tipo_receita as tipo_receita FROM('      +
       'SELECT '                          +
       'cred.cred_id,'                    ;
       if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 2 then
         SqlLancamentosFuturos:= SqlLancamentosFuturos + 'cast(((coalesce(cred.TX_DVV,0.0) * 41)/100) as float) as TX_DVV ,'
       else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 10 then
         SqlLancamentosFuturos:= SqlLancamentosFuturos + 'cast(((coalesce(cred.TX_DVV,0.0) * 30)/100)as float) as TX_DVV, '
       else
         SqlLancamentosFuturos := SqlLancamentosFuturos + '0.0 as TX_DVV, ';
       SqlLancamentosFuturos := SqlLancamentosFuturos + 'cred.AGENCIA,'       +
       'cc.BAIXA_CREDENCIADO AS BAIXADO, '+
       'cred.BANCO as CODBANCO,'          +
       'ba.BANCO AS NOME_BANCO,'          +
       'taxas_prox_pag.valor as lanc_futuros,'+
       'taxas_prox_pag.tipo_receita as tipo_receita,'+
       'COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'+
       '0.0 AS TAXA_EXTRA,'+
       //'(SELECT COALESCE(SUM(t.valor),0)'+
       //'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA,'+
       //-----------------------------------------------------------------------------
       //--seleciona TAXA ADMINISTRADORA(COMISS�O)'+
       '(coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0)) as COMISSAO_ADM,'+
       //'-- fim da consulta TAXA ADMINISTRADORA(COMISS�O)'+
       //'-----------------------------------------------------------------------------'+
       //'--seleciona TOTAL RETIDO PELA ADMINISTRADORA'+
       '(coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100))'+
       '+'+
       '(SELECT COALESCE(SUM(t.valor),0)'+
       'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'+
       'AS TOTAL_RETIDO_ADM,'+
       //'-- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'+
       //'-----------------------------------------------------------------------------'+
       //'--seleciona o TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
       //'--(BRUTO - (TAXAS EXTRAS - COMISSAO))'+
       '((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0))'+
       '-'+
       '((coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0))+(SELECT COALESCE(SUM(t.valor),0)'+
       'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID))'+
       ') AS LIQUIDO'+
       //'--Fim da consulta TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
       //'-----------------------------------------------------------------------------'+
       ', ''N'' as ATRASADO'+
       ' from credenciados cred'+
       ' LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between '+QuotedStr(DateToStr(dataIni.Date))+' and '+QuotedStr(DateToStr(dataIni.Date));
               //RETORNA APENAS AS AUTORIZA��ES CONFIRMADAS
        if rdgAutoriz.ItemIndex = 1 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cc.conferido = ''S''';
        end;
        //RETORNA APENAS OS PAGAMENTOS CONFIRMADOS
        if rdgPagamentos.ItemIndex = 1 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cc.BAIXA_CREDENCIADO = ''N''';
        end;

        if rdgPagamentos.ItemIndex = 2 then
        begin
          sql := SqlLancamentosFuturos+' AND cc.BAIXA_CREDENCIADO = ''S''';
        end;

        SqlLancamentosFuturos := SqlLancamentosFuturos+' LEFT JOIN BANCOS ba ON ba.codigo = cred.banco';
        SqlLancamentosFuturos := SqlLancamentosFuturos+' LEFT JOIN taxas_prox_pag ON cred.cred_id = taxas_prox_pag.cred_id';
        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 6 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID';
          if rbEstab.ItemIndex = 1 then
            SqlLancamentosFuturos := SqlLancamentosFuturos+' AND CRED.SEG_ID = 14'
          else
            SqlLancamentosFuturos := SqlLancamentosFuturos+' AND CRED.SEG_ID = 39';
        end;

        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 7 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID and CRED.SEG_ID = 39';
        end;

        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 8 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SEG_ID = 39';
        end;

        SqlLancamentosFuturos := SqlLancamentosFuturos+' WHERE cred.PAGA_CRED_POR_ID = '+qPgtoPorPAGA_CRED_POR_ID.AsString;
        if edtEstab.Text <> '' then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cred.CRED_ID = '+qEstabcred_id.AsString;
        end;
        SqlLancamentosFuturos := SqlLancamentosFuturos+' GROUP BY cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
        ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV,taxas_prox_pag.valor,taxas_prox_pag.tipo_receita'+
        ' )A WHERE A.CRED_ID = '+MDPagtoEstabcred_id.AsString; }
        SqlLancamentosFuturos := ''
          +'SELECT taxas_prox_pag.valor AS valor, '
          +'                                   taxas_prox_pag.tipo_receita AS tipo_receita, '
          +'                                   cred.cred_id, '
          +'                                   TAXAS_PROX_PAG.TAXAS_PROX_PAG_ID '
          +'                                   FROM credenciados cred '
          +'                                   INNER JOIN taxas_prox_pag ON cred.cred_id = taxas_prox_pag.cred_id '
          +'                                   WHERE convert(varchar,data_do_desconto,103) = '+QuotedStr(DateToStr(dataCompensa.Date)) +' and cred.cred_id = '+MDPagtoEstabcred_id.AsString 
          +'                                   GROUP BY cred.CRED_ID '
          +'                                  ,taxas_prox_pag.tipo_receita,taxas_prox_pag.VALOR,taxas_prox_pag.taxas_prox_pag_id';
end;



function RetornaNumeros(pString:String):String;
var
  i : Integer;
  begin
    Result := '';
    for i := 0 to length(pString) do begin
      if Char(pString[i]) in ['0'..'9'] then
      Result := Result + pString[i];
    end;
  end;

function CarregaArquivoDeRemessa() : String;
//var
////  SispagItau: TSispagItau;
begin
//  SispagItau := TSispagItau.Create();
//  SispagItau.GerarRegistroHeader240(tipoInscDaEmpresa,ContaDigito,TipoDePgto,FinalidadeDoLote,HistoricoDeContaCorrente);
//  SispagItau.GerarRegistroDetalheSegmentoA();
//  SispagItau.GerarRegistroTraillerDeLote();
//  SispagItau.GerarTraillerDoArquivo240();
end;

procedure RollBackTaxaAvulsa(Cred_id: Integer; Descricao: string; ValorDaTaxa:
    Double; DataDoDesconto: TDateTime; TipoReceita: String);
var SQL : TSqlMount;
begin
  SQL := TSqlMount.Create(smtInsert,'TAXAS_PROX_PAG');
          SQL.ClearFields;
          SQL.AddField('TAXAS_PROX_PAG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR STAXAS_PROX_PAG_ID AS TAXAS_PROX_PAG_ID'),ftInteger);
          SQL.AddField('CRED_ID',Cred_id, ftInteger);
          SQL.AddField('DESCRICAO',Descricao, ftString);
          SQL.AddField('VALOR',ValorDaTaxa, ftFloat);
          SQL.AddField('TIPO_RECEITA',TipoReceita,ftString);
          SQL.AddField('DATA_DO_DESCONTO',DataDoDesconto,ftDateTime);

          DMConexao.Query1.SQL    := SQL.GetSqlString;
          DMConexao.Query1.SQL.Text;
          DMConexao.Query1.ExecSQL;
end;

//function SetUpSacado: TSacado;
//var
//  Sacado: TSacado;
//begin
//  Sacado := TSacado.Create();
//
//  Result := ;
//end;


procedure TfrmPgtoEstabAbertoAntecipacao.AbrirPagamentos;
var mes : integer;
  dia : string;
  CNPJsoNumero,imprimeCNPJ : String;
  i : Integer;
  lista_credID : TList;
  valor_taxa,total_adm,total,tx_dvv,ValorTaxaBancaria,TotalDeTaxasParaDescontar,TotalDeTaxasDescontou : Double;
begin
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >= 2) then
  begin
    SQLFactory; //Esta Procedure cria a query din�mica atribuindo valor a uma
    //vari�vel global chamada sql para passar como par�metro pro m�todo ADD da propriedade SQL
    qPgtoEstab.Close;
    qPgtoEstab.SQL.Clear;
    qPgtoEstab.SQL.Add(sql);
    qPgtoEstab.Open;
    if(qPgtoEstab.IsEmpty)then
    begin
      MDPagtoEstab.EmptyTable;
      qPgtoDesc.Close;
      qPgtoEmpr.Close;
      MsgInf('Os crit�rios de busca aplicados n�o retornaram nenhum valor!');
      exit;
    end;
    qPgtoEstab.First;
    MDPagtoEstab.Open;
    MDPagtoEstab.EmptyTable;
    MDPagtoEstab.DisableControls;
    while not qPgtoEstab.Eof do
    begin
      //if qPgtoEstabBRUTO.AsCurrency > 0.0 then begin
        MDPagtoEstab.Append;
        MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
        MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
        MDPagtoEstabENDERECO.AsString           := qPgtoEstabENDERECO.AsString;
        MDPagtoEstabCIDADE.AsString             := qPgtoEstabCIDADE.AsString;
        MDPagtoEstabNUMERO.AsInteger            := qPgtoEstabNUMERO.AsInteger;
        MDPagtoEstabCEP.AsString                := qPgtoEstabCEP.AsString;
        MDPagtoEstabESTADO.AsString             := qPgtoEstabESTADO.AsString;
        MDPagtoEstabCOMPLEMENTO.AsString        := qPgtoEstabCOMPLEMENTO.AsString;
        MDPagtoEstabTELEFONE1.AsString          := qPgtoEstabTELEFONE1.AsString;

        MDPagtoEstabdiafechamento1.AsInteger    := qPgtoEstabdiafechamento1.AsInteger;
        MDPagtoEstabvencimento1.AsInteger       := qPgtoEstabvencimento1.AsInteger;
        //MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        qPgtoEstabCORRENTISTA.AsString;
        //CNPJsoNumero := RetornaNumeros(qPgtoEstabCORRENTISTA.AsString);
        if qPgtoEstabCORRENTISTA.AsString <> '' then
        begin
          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
          end;
          if Length(CNPJsoNumero) = 14 then
          begin
            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
          end
          else
          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
        end
        else
        begin
          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        end;
        MDPagtoEstabcgc.AsString                := qPgtoEstabcgc.AsString;
        MDPagtoEstabcomissao.AsFloat            := qPgtoEstabCOMISSAO.AsFloat;
        MDPagtoEstabcontacorrente.AsString      := qPgtoEstabCONTACORRENTE.AsString;
        MDPagtoEstabagencia.AsString            := qPgtoEstabAGENCIA.AsString;
        MDPagtoEstabCODBANCO.AsInteger          := qPgtoEstabCODBANCO.AsInteger;
        MDPagtoEstabNOME_BANCO.AsString         := qPgtoEstabNOME_BANCO.AsString;
        MDPagtoEstabbruto.AsFloat               := qPgtoEstabBRUTO.AsFloat;
        MDPagtoEstabcomissao_adm.AsFloat        := qPgtoEstabCOMISSAO_ADM.AsFloat;

        //Preenche o grid para pagamentos pendentes
        if(rdgPagamentos.ItemIndex <> 2) then
        begin
          //Calcula e retorna taxa de DVV
          tx_dvv :=  CalculaTaxaDVV(qPgtoEstabTX_DVV.AsFloat, MDPagtoEstabbruto.AsFloat, MDPagtoEstabcomissao_adm.AsFloat);
           if tx_dvv = 0 then
              MDPagtoEstabTX_DVV.AsFloat            := 0.0
           else
              MDPagtoEstabTX_DVV.AsFloat          := tx_dvv;

          //Se for pagamento efetuado = 'N'
          if rdgPagamentos.ItemIndex <> 2 then begin
            //Se o banco n�o for BB, Ita� ou Bradesco
            if (NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3])) then
            begin
              ValorTaxaBancaria :=  StrToFloat(edTaxaBanco.Text);
            end
            else
              ValorTaxaBancaria := 0.0;
          end ;

          //Calcula o total de taxas para descontar o estabelecimento
          TotalDeTaxasParaDescontar             := MDPagtoEstabcomissao_adm.AsFloat + tx_dvv + ValorTaxaBancaria ;

          //total_adm := (qPgtoEstabBRUTO.AsFloat * (qPgtoEstabCOMISSAO.AsFloat/100)) + qPgtoEstabTAXA_EXTRA.AsFloat + MDPagtoEstabtaxa_extra.AsFloat;
          MDPagtoEstabtaxa_extra.AsFloat        := ValorTaxaBancaria;

          if qPgtoEstabBRUTO.AsFloat <> 0 then
            MDPagtoEstabliquido.AsFloat         := qPgtoEstabBRUTO.AsFloat - TotalDeTaxasParaDescontar
          else
            MDPagtoEstabliquido.AsFloat         := qPgtoEstabLIQUIDO.AsFloat;
        end

        //Preenche o grid principal caso os pagamentos sejam baixados = "S"
        else
        begin
          //Calcula e retorna taxa de DVV
          tx_dvv :=  CalculaTaxaDVV(qPgtoEstabTX_DVV.AsFloat, MDPagtoEstabbruto.AsFloat, MDPagtoEstabcomissao_adm.AsFloat);

          //Preenche a coluna com o valor do DVV
          if tx_dvv = 0 then
              MDPagtoEstabTX_DVV.AsFloat    := 0.0
           else
              MDPagtoEstabTX_DVV.AsFloat    := tx_dvv;

          //Calcula o total de taxas para descontar o estabelecimento
          TotalDeTaxasDescontou             := MDPagtoEstabcomissao_adm.AsFloat + tx_dvv + getValorTaxasExtras(MDPagtoEstabcred_id.AsInteger,dataCompensa.Date);

          //total_adm := (qPgtoEstabBRUTO.AsFloat * (qPgtoEstabCOMISSAO.AsFloat/100)) + qPgtoEstabTAXA_EXTRA.AsFloat + MDPagtoEstabtaxa_extra.AsFloat;
          MDPagtoEstabtaxa_extra.AsFloat    := getValorTaxasExtras(MDPagtoEstabcred_id.AsInteger,dataCompensa.Date);

          MDPagtoEstabliquido.AsFloat       := qPgtoEstabBRUTO.AsFloat - TotalDeTaxasDescontou;
        end;

        MDPagtoEstabBAIXADO.AsString        := qPgtoEstabBAIXADO.AsString;
        MDPagtoEstabATRASADO.AsString       := qPgtoEstabATRASADO.AsString;
        //MDPagtoEstabPAGAMENTO_CRED_ID.AsInteger := qPgtoEstabpagamento_cred_id.AsInteger;
        MDPagtoEstabmarcado.AsBoolean       := False;
        MDPagtoEstab.Post;
      //end;
      CNPJsoNumero := '';
      qPgtoEstab.Next;
    end;
    MDPagtoEstab.Next;
    MDPagtoEstab.First;
    MDPagtoEstab.EnableControls;
    //qPgtoEstab.Requery();  // adicionado hoe 06/01 - teste
    SQLDestroyString;

    //empres_sel := EmptyStr;
  end

end;

function TfrmPgtoEstabAbertoAntecipacao.getValorTaxasExtras(cred_id : Integer ; data_compensa : TDateTime) : Currency;
var vlTotal : Currency;
begin
 vlTotal := DMConexao.ExecuteQuery('select coalesce(sum(valor),0) as vl_total_taxas from  TAXAS_REPASSE where cred_id = '+IntToStr(cred_id)+' and DT_DESCONTO = '+QuotedStr(DateToStr(data_compensa))+'');
 Result := vlTotal;
end;

function TfrmPgtoEstabAbertoAntecipacao.CalculaTaxaDVV(ValorTaxaDVV : Double; ValorBruto : Double ; ValorComissaoAdm : Double) : Double;
begin
  Result := (ValorTaxaDVV * (ValorBruto - ValorComissaoAdm));
end;


procedure TfrmPgtoEstabAbertoAntecipacao.CarregaTabelaTemp;
begin

end;

procedure TfrmPgtoEstabAbertoAntecipacao.AbrirValoresDescontos;
var sql : TSqlMount;
    data : TDateTime;
    mes,ano : Integer;
    mesConvertido : Integer;
var taxa_id, cred_id,flag : Integer;

begin

  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA N�O POSSUI TAXAS EXTRAS ATRASADAS
  if MDPagtoEstabATRASADO.AsString = 'N' then
  begin
    if MDPagtoEstabBAIXADO.AsString = 'N' then
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Add('SELECT tr.ID id,rtc.CRED_ID,rtc.taxa_id,t.descricao,t.valor,coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK,t.TIPO_RECEITA AS TIPO_RECEITA ');
      qPgtoDesc.SQL.Add('FROM REL_TAXA_CRED rtc LEFT JOIN taxas_repasse_temp tr ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id');
      qPgtoDesc.SQL.Add(',taxas t where rtc.taxa_id = t.taxa_id');
      qPgtoDesc.SQL.Add(' and rtc.TAXA_ID not  in (select taxa_id from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString);
      qPgtoDesc.SQL.Add(' AND CONVERT(VARCHAR(4),YEAR(dt_desconto)) + '''' + CONVERT(VARCHAR(2),MONTH(dt_desconto)) ');
			qPgtoDesc.SQL.Add(' = CONVERT(VARCHAR(4),YEAR('+QuotedStr(DateToStr(dataCompensa.Date))+')) + '''' + CONVERT(VARCHAR(2),MONTH('+QuotedStr(DateToStr(dataCompensa.Date))+'))) ');
      qPgtoDesc.SQL.Add(' AND rtc.cred_id = '+MDPagtoEstabcred_id.AsString+'');
      qPgtoDesc.SQL.Add(' UNION ALL');
      qPgtoDesc.SQL.Add(' SELECT  TR.ID, TPP.CRED_ID, 0 AS TAXA_ID, TPP.descricao, TPP.valor, coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID,0) TAXAS_PROX_PAG_ID_FK,COALESCE(TPP.TIPO_RECEITA,'''') TIPO_RECEITA');
      qPgtoDesc.SQL.Add(' FROM TAXAS_PROX_PAG TPP LEFT JOIN taxas_repasse_temp tr ON TPP.TAXAS_PROX_PAG_ID = TR.TAXAS_PROX_PAG_ID_FK WHERE TPP.CRED_ID = '+MDPagtoEstabcred_id.AsString+'');
      qPgtoDesc.Sql.Add(' AND CONVERT(VARCHAR, (DATA_DO_DESCONTO), 103) = '+QuotedStr(DateToStr(dataCompensa.Date))+'');
      qPgtoDesc.SQL.Text;
      qPgtoDesc.Open;
    end
    //Se dados do grid principal forem BAIXADOS = 'S'
    else
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Text := QDescontosDePagtosBaixados.SQL.Text;

      qPgtoDesc.Parameters[0].Value := MDPagtoEstabcred_id.AsString;
      qPgtoDesc.Parameters[1].Value := DateToStr(dataCompensa.Date);

//      qPgtoDesc.SQL.Add('SELECT CONVERT(INT,next value for STAXAS_TEMP) id, tr.cred_id, tr.taxa_id, ');
//      qPgtoDesc.SQL.Add('CASE WHEN tr.taxa_id > 0 THEN t.descricao ');
//      qPgtoDesc.SQL.Add('WHEN tr.TAXA_ID = 0 then tr.HISTORICO end AS descricao, ');
//      qPgtoDesc.SQL.Add('CASE WHEN tr.taxa_id > 0 THEN t.VALOR WHEN tr.TAXA_ID = 0	THEN tr.VALOR	END AS VALOR, ');
//      qPgtoDesc.SQL.Add('coalesce(trt.marcado,''N'') marcado,coalesce(TR.TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK ');
//      qPgtoDesc.SQL.Add('from taxas_repasse tr LEFT JOIN taxas t ON t.taxa_id = tr.taxa_id ');
//      qPgtoDesc.SQL.Add('LEFT JOIN taxas_repasse_temp trt ON tr.cred_id = trt.cred_id and tr.taxa_id = trt.taxa_id');
//      qPgtoDesc.SQL.Add('WHERE tr.cred_id = '+MDPagtoEstabcred_id.AsString+' and tr.dt_desconto = '+QuotedStr(dataCompensa.Text)+'');
      qPgtoDesc.Open;
//        GridDescontos.DataSource         := nil;
//
//        GridDescontos.DataSource := DSTeste;
//        //GridDescontos.DataSource := dsPgtoDesc;
//
//        QDescontosDePagtosBaixados.Close;
//        QDescontosDePagtosBaixados.Parameters.ParamByName('cred_id').Value := MDPagtoEstabcred_id.AsString;
//        QDescontosDePagtosBaixados.Parameters.ParamByName('dt_desconto').Value := DateToStr(dataCompensa.Date);
//        QDescontosDePagtosBaixados.SQL.Text;
//        QDescontosDePagtosBaixados.Open;


        


    end;
  end
  
  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA POSSUI TAXAS EXTRAS ATRASADAS. CASO ENTRE NESTA CONDI��O BUSCA TAXA NA TABELA
  //TAXAS_REPASSE_ATRASADA
  else
  begin
    qPgtoDesc.Close;
    qPgtoDesc.SQL.Clear;
    qPgtoDesc.SQL.Add('SELECT tra.id id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
    qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
    qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
    //qPgtoDesc.SQL.Add('INNER JOIN taxas_repasse tr ON tr.taxa_id = tra.taxa_id AND TR.CRED_ID = TRA.CRED_ID');
    qPgtoDesc.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString+' AND tra.baixado = ''N''');
    //qPgtoDesc.SQL.Add('and tra.taxa_id not in (select taxa_id from taxas_repasse where cred_id = '+MDPagtoEstabcred_id.AsString+' and negociada = ''S'')');
    qPgtoDesc.Open;
  end;
end;


procedure TfrmPgtoEstabAbertoAntecipacao.AbrirValoresEmpresas;
begin
  qPgtoEmpr.Close;
  qPgtoEmpr.SQL.Clear;
  qPgtoEmpr.SQL.Add('select PAGAMENTO_CRED_ID, DATA_PGTO, DATA_COMPENSACAO, OPERADOR ');
  qPgtoEmpr.SQL.Add('from PAGAMENTO_CRED where cred_id = '+ cbbEstab.KeyValue);
  qPgtoEmpr.SQL.Add(' AND DATEPART(YYYY,DATA_PGTO) >= 2015 ');
  qPgtoEmpr.Open;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.cbbPgtoPorChange(Sender: TObject);
begin
  inherited;
  panFatura.Visible     := cbbPgtoPor.KeyValue = 1;
  ckbTodasDatas.Visible := cbbPgtoPor.KeyValue = 1;
  rdgAutoriz.Visible    := true;//cbbPgtoPor.KeyValue = 2;
  DataIni.Visible       := not (integer(cbbPgtoPor.KeyValue) in [3]);
  DataFin.Visible       := not (integer(cbbPgtoPor.KeyValue) in [3]);
  lblA.Visible          := not (integer(cbbPgtoPor.KeyValue) in [3]);
  cbAtrasadas.Visible   := not (integer(cbbPgtoPor.KeyValue) in [3]);
  edtDia.Visible        := integer(cbbPgtoPor.KeyValue) in [3];
  rbEstab.ItemIndex := -1;
  rbEstab.Enabled := true;
  if cbbPgtoPor.KeyValue = 3 then
    lblDe.Caption := 'Dia'
  //else if cbbPgtoPor.KeyValue = 4 then
    //lblDe.Caption := 'Dia repasse'
  else
    lblDe.Caption := 'De';

  if cbbPgtoPor.KeyValue = 1 then
  begin
    gpbDatas.Caption := 'Data da Baixa Fatura';
    rdgDataPor.Visible:= False;
  end else begin
    gpbDatas.Caption := 'Datas das Autoriza��es';
    ckbTodasDatas.Checked := False;
    rdgDataPor.Visible:= True;
  end;
  if integer(cbbPgtoPor.KeyValue) in[6,7] then begin
    rbEstab.Enabled := true;
    if integer(cbbPgtoPor.KeyValue) in[7] then
    begin
      //rbEstab.ItemIndex := 0;
      rbEstab.Enabled := true;
    end;
  end
  else
    rbEstab.Enabled := true;

  qPgtoEstab.Close;
  HabilitarBotoes;
  Application.ProcessMessages;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.HabilitarBotoes;
begin
  BtnMarcDesm.Enabled     := true;
  BtnMarcaTodos.Enabled   := true;
  BtnDesmTodos.Enabled    := true;
  BtnEfetuar.Enabled      := (not qPgtoEstab.IsEmpty) and (rdgPagamentos.ItemIndex <> 2);
  BtnCancelarPag.Enabled  := (not qPgtoEstab.IsEmpty) and (rdgPagamentos.ItemIndex = 2);
  BtnImprimir.Enabled     := not qPgtoEstab.IsEmpty;
  bntGerarPDF.Enabled     := not qPgtoEstab.IsEmpty;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEmprCalcFields(DataSet: TDataSet);
begin
  inherited;
  qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency:= qPgtoEmpr.FieldByName('BRUTO').AsCurrency -
  (qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency);
end;

/// <summary>
///  Esta procedure Marca e Desmarca linha do grid principal.
/// </summary>
///  <remarks>
///   - SqlGetLancFuturos - Retorna Lan�amentos Futuros
///  </remarks>
///  <remarks>
///   - Efetua o somat�rio dos taxas extras(lan�amentos futuros + Taxa Banc�ria + Taxas)
             /// - Lan�amentos futuros vindos da tabela taxas_prox_pag cadastro efetuado no fcred-> aba taxas
             /// - Taxa Banc�rio valor fixo de 3,90 para cred com banco <> (Itau, Bradesco, Brasil)
	           /// - Taxas Pr� cadastradas no fcred aba taxas vindas da tabela rel_taxa_cred
///  </remarks>
/// <remarks>
        /// - SQL TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP')
        ///	- Grava Na tabela 'TAXAS_REPASSE_TEMP' as taxas marcadas em tempo de execu��o
/// </remarks>

procedure TfrmPgtoEstabAbertoAntecipacao.BtnMarcDesmClick(Sender: TObject);
VAR SQL : TSqlMount;
var valor_taxa,valor_taxa_aux  : Currency;
    i : Integer;
    contador,contador_aux,contador_aux_debitos_futuros,taxaDeCreditoFixo : Double;
    contador_aux_cred_futuros,DebitosFuturos,CreditosFuturos : Double;
    get_lancamentos_cred,strSQL : String;
    _isTaxaExtra : Boolean;

begin
  inherited;
  if MDPagtoEstab.IsEmpty then Exit;
    MDPagtoEstab.Edit;
    //MUDA O STATUS 'MARCADO' DA LINHA DO GRID PRINCIPAL
    MDPagtoEstabmarcado.AsBoolean := not MDPagtoEstabmarcado.AsBoolean;
    MDPagtoEstab.Post;
    Pgto_Sel;
  //--------------SE ESTIVER MARCANDO--------------
  if MDPagtoEstabBAIXADO.AsString = 'S' then
  begin
    SqlGetLancFuturos;
    QLancCred.Close;
    QLancCred.SQL.Clear;
    QLancCred.SQL.Add(SqlLancamentosFuturos);
    QLancCred.Open;
    //Calcula o valor de d�bito e cr�dito lan�ado no formul�rio do credenciado
    //especificamente na aba taxas
    while not QLancCred.Eof do
    begin
      if QLancCred.Fields.FieldByName('tipo_receita').Value = 'D' then
        DebitosFuturos := DebitosFuturos + QLancCred.Fields[0].AsFloat
        else
        CreditosFuturos := CreditosFuturos + QLancCred.Fields[0].AsFloat;
      QLancCred.Next;
    end;
    //FIM DA CONTAGEM

    //Vari�vel contador � respons�vel por calcular o montante de taxas acumuladas para posteriormente confrontar com o valor bruto
    //Como regra de neg�cio ficou definido que se o credenciado possuir liquido < bruto desconsidera o desconto desta taxa para o credenciado
    contador := MDPagtoEstabcomissao_adm.AsCurrency;
    qPgtoDesc.First;
    MDPgtoDesc.Open;
    MDPgtoDesc.DisableControls;
    valor_taxa := 0;
    contador_aux := 0;

    //Ja incrementa a variavel contador com o valor de taxa banc�ria
    //caso o banco_id do credenciado seja diferente de 1,2,3
    if MDPagtoEstabCODBANCO.AsInteger > 3 then
      contador := contador + StrToFloat(edTaxaBanco.Text);
    while not qPgtoDesc.Eof do
    begin
      contador := contador + qPgtoDesc.Fields[3].AsFloat;
      if contador < MDPagtoEstabbruto.AsCurrency then
      begin
        contador_aux := contador_aux + qPgtoDescvalor.AsFloat; // conta o somatorio
        //das taxas extras em que o somat�rio � menor do que o Valor Bruto
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
        DMConexao.AdoQry.Open;
        // VARI�VEL VALOR_TAXA ARMAZENA O VALOR DAS TAXAS QUE ATENDEM A CONDI��O
        // CONTADOR < MDPagtoEstabbruto.AsCurrency(OU SEJA TAXAS_ADM + TAXA EXTRA)
        // DEVEM SER MENOR QUE O BRUTO PARA QUE POSSAMOS MARCAS AS TAXAS EXTRAS.
        valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
        SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
        SQL.ClearFields;
        sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
        sql.AddField('TAXAS_PROX_PAG_ID_FK',QLancCred.Fields.FieldByName('TAXAS_PROX_PAG_ID').Value,ftInteger);
        sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
        sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger);
        sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
        sql.AddField('MARCADO','S',ftString);
        DMConexao.Query1.SQL    := SQL.GetSqlString;
        DMConexao.Query1.ExecSQL;
      end;
//      else
//      begin
//        contador := contador - qPgtoDesc.Fields[3].AsFloat;
//      end;
      qPgtoDesc.Next;
    end;
  end
  else //Pagamentos Em aberto
  begin
    if(MDPagtoEstabmarcado.AsBoolean = True) then begin

      //--------------SE ESTIVER ATRASADO--------------
      if MDPagtoEstabATRASADO.Value = 'S' then
      begin
        contador := MDPagtoEstabcomissao_adm.AsCurrency;
        qPgtoDesc.SQL.Add('SELECT tra.id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
        qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
        qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
        qPgtoDesc.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString);
        qPgtoDesc.Open;
        qPgtoDesc.First;
        MDPgtoDesc.Open;
        MDPgtoDesc.DisableControls;
        valor_taxa := 0;
        contador_aux := 0;
        if MDPagtoEstabCODBANCO.AsInteger > 3 then
          contador := contador + StrToFloat(edTaxaBanco.Text);
        while not qPgtoDesc.Eof do
        begin
          contador := contador + qPgtoDesc.FieldValues['valor'];
          if contador < MDPagtoEstabbruto.AsCurrency then
          begin
            contador_aux := contador_aux + qPgtoDescvalor.AsFloat;
            valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
            SQL := TSqlMount.Create(smtUpdate,'TAXAS_REPASSE_ATRASADA');
            SQL.ClearFields;
            SQL.AddField('MARCADO','S',ftString);
            SQL.SetWhere('WHERE CRED_ID = '+MDPagtoEstabcred_id.AsString);
            DMConexao.Query1.SQL    := SQL.GetSqlString;
            DMConexao.Query1.ExecSQL;
          end;
          qPgtoDesc.Next;
        end;

      end

      //--------------SE N�O, N�O ESTIVER ATRASADO--------------
      else
      begin
        SqlGetLancFuturos;
        QLancCred.Close;
        QLancCred.SQL.Clear;
        QLancCred.SQL.Add(SqlLancamentosFuturos);
        QLancCred.Open;
        //Calcula o valor de d�bito e cr�dito lan�ado no formul�rio do credenciado
        //especificamente na aba taxas
        while not QLancCred.Eof do
        begin
          if QLancCred.Fields.FieldByName('tipo_receita').Value = 'D' then
            begin
              DebitosFuturos := DebitosFuturos + QLancCred.Fields[0].AsFloat;
              contador := contador + QLancCred.Fields[0].AsFloat;
            end
            else
              CreditosFuturos := CreditosFuturos + QLancCred.Fields[0].AsFloat;
          QLancCred.Next;
        end;
        //FIM DA CONTAGEM

        contador :=  contador + MDPagtoEstabcomissao_adm.AsCurrency + MDPagtoEstabTX_DVV.AsCurrency;
        qPgtoDesc.First;
        MDPgtoDesc.Open;
        MDPgtoDesc.DisableControls;
        valor_taxa                   := 0;
        contador_aux                 := 0;
        contador_aux_debitos_futuros := 0;
        contador_aux_cred_futuros    := 0;

        //Ja incrementa a variavel contador COM O VALOR DA TAXA BANC�RIA caso o banco_id do credenciado seja diferente de 1,2,3
        if MDPagtoEstabCODBANCO.AsInteger > 3 then
          if MDPagtoEstabliquido.AsFloat >= StrToFloat(edTaxaBanco.Text) then
            contador := contador + StrToFloat(edTaxaBanco.Text);

        //Busca as taxas FIXAS referentes ao Credenciado MDPagtoEstabcred_id(CRED_ID) e carrega o resultado em um TList para
        //compara��es.
        DMConexao.AdoQry.Close;
                  DMConexao.AdoQry.SQL.Clear;
                  DMConexao.AdoQry.SQL.Add('SELECT taxa_id FROM rel_taxa_cred WHERE cred_id = '+MDPagtoEstabcred_id.AsString+'');
                  DMConexao.AdoQry.Open;
        listaDeTaxas := TList.Create;
        while not DMConexao.AdoQry.Eof do
        begin
          listaDeTaxas.Add(Pointer(DMConexao.AdoQry.Fields[0].AsInteger));
          DMConexao.AdoQry.Next;
        end;

        while not qPgtoDesc.Eof do
        begin

          //Somat�rio taxas (contador) < Bruto ? : Executa cod abaixo
            listaDeTaxas.First;
            _isTaxaExtra := False;
            for i := 0 to listaDeTaxas.Count-1 do
            begin
              if qPgtoDesctaxa_id.AsInteger = Integer(listaDeTaxas[i]) then
              begin
                _isTaxaExtra := True;
                Break;
              end
              else
                DMConexao.AdoQry.Next;
            end;
            if _isTaxaExtra then
            begin
              //SOMA O VALOR DA TAXA FIXA AO CONTADOR CASO SEJA TAXA FIXA DO TIPO DEBITO.
              if qPgtoDescTIPO_RECEITA.AsString = 'D' then
                contador := contador + qPgtoDesc.Fields[3].AsFloat
              else
                //SOMA O VALOR DA TAXA FIXA AO CONTADOR CASO SEJA TAXA FIXA DO TIPO DEBITO.
                taxaDeCreditoFixo := taxaDeCreditoFixo + qPgtoDesc.Fields[3].AsFloat;
              if (contador < MDPagtoEstabbruto.AsCurrency) or (qPgtoDescTIPO_RECEITA.AsString = 'C') then
              begin
                if qPgtoDescTIPO_RECEITA.AsString = 'D' then
                begin
                  contador_aux := contador_aux + qPgtoDescvalor.AsFloat;
                  valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
                end;
                DMConexao.AdoQry.Close;
                DMConexao.AdoQry.SQL.Clear;
                DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
                DMConexao.AdoQry.Open;

                // VARI�VEL VALOR_TAXA ARMAZENA O VALOR DAS TAXAS QUE ATENDEM A CONDI��O
                // CONTADOR < MDPagtoEstabbruto.AsCurrency(OU SEJA TAXAS_ADM + TAXA EXTRA)
                // DEVEM SER MENOR QUE O BRUTO PARA QUE POSSAMOS MARCAS AS TAXAS EXTRAS.

                SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
                SQL.ClearFields;
                sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
                sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
                SQL.AddField('TAXAS_PROX_PAG_ID_FK', 0, ftInteger );
                sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger);
                sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
                sql.AddField('MARCADO','S',ftString);
                DMConexao.Query1.SQL    := SQL.GetSqlString;
                DMConexao.Query1.ExecSQL;
              end
              else
                 contador := contador - qPgtoDesc.Fields[3].AsFloat;
            end ;

          qPgtoDesc.Next;
        end;
        ///<summary>
        ///Regi�o que verifica se h� Lan�amento de Cr�dito e/ou D�bitos futuros
        ///caso positivo insere na tabela de taxas tempor�rias
        ///</summary>
        QLancCred.First;
        while not QLancCred.Eof do
        begin
            if QLancCred.Fields[1].Value = 'D' then
            begin
              //contador := contador + QLancCred.Fields.FieldByName('valor').Value;
              //contador_aux := contador_aux + QLancCred.Fields.FieldByName('valor').Value;
              valor_taxa := valor_taxa + QLancCred.Fields.FieldByName('valor').Value;
              contador_aux_debitos_futuros := valor_taxa
            end;

            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
            DMConexao.AdoQry.Open;

            SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
            SQL.ClearFields;
            sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
            sql.AddField('TAXAS_PROX_PAG_ID_FK',QLancCred.Fields.FieldByName('TAXAS_PROX_PAG_ID').Value,ftInteger);
            sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
            sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
            sql.AddField('MARCADO','S',ftString);
            DMConexao.Query1.SQL    := SQL.GetSqlString;
            DMConexao.Query1.ExecSQL;

          //end;
          QLancCred.Next;
        end;

      end;

      valor_taxa_aux := MDPagtoEstabtaxa_extra.AsCurrency;
      if contador < MDPagtoEstabbruto.AsCurrency then begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;

        //QUERY PARA SELECIONAR O VALOR TOTAL DAS TAXAS QUE N�O FORAM DESCONTADAS(BAIXADAS)
        //DURANTE O M�S DE CONSULTA
        if MDPagtoEstabATRASADO.Value = 'S' then
        begin
           DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
           DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
           DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE_ATRASADA tra where tra.baixado = ''S''))) ');
           //DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataIni.date))+'))))');
        //end;
           DMConexao.AdoQry.Open;
        end

        else
        begin
          //CONTROLE CASO O TIPO_PAGA_CRED = 4 OU 1
          //if(qPgtoPorPAGA_CRED_POR_ID.AsInteger <> 4) then
          //begin
            DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
            DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
            DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE tr ');
            DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataCompensa.date))+'))))');
          //end
          {else
          begin
            DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
            DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
            DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE tr ');
            DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataIni.date))+'))))');

          end;}
        end;
        DMConexao.AdoQry.Open;
        //VERIFICA SE O VALOR "ORIGINAL" DA TAXA EXTRA(VALOR_TAXA_AUX) � DIFERENTE
        //DO QUE O SOMADO(ATRIBUIDO A VARIAVEL VALOR_TAXA) NA BUSCA APLICANDO O FILTRO
        //DAS TAXAS QUE AINDA N�O FORAM BAIXADAS NO M�S PARA AQUELE DETERMINADO CREDENCIADO
        if (valor_taxa_aux <> valor_taxa) and (valor_taxa_aux <> StrToFloat(edTaxaBanco.Text)) then begin
          MDPagtoEstab.Edit;
          MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux + DebitosFuturos; //+ MDPagtoEstabTX_DVV.AsFloat;
          MDPagtoEstabliquido.AsCurrency    := ((MDPagtoEstabliquido.AsCurrency - MDPagtoEstabtaxa_extra.AsCurrency) + CreditosFuturos + taxaDeCreditoFixo);
        end
        else begin
          MDPagtoEstab.Edit;
          //MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + DMConexao.AdoQry.Fields[0].Value  + DebitosFuturos;//+ MDPagtoEstabTX_DVV.AsFloat;
          MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux + DebitosFuturos;//+ MDPagtoEstabTX_DVV.AsFloat;
          MDPagtoEstabliquido.AsCurrency    := ((MDPagtoEstabliquido.AsCurrency - contador_aux - DebitosFuturos) + CreditosFuturos + taxaDeCreditoFixo);
        end;
      end
      //else if contador_aux < MDPagtoEstabbruto.AsCurrency then
      //begin
      else begin
        if (valor_taxa_aux <> valor_taxa) and (contador_aux <> StrToFloat(edTaxaBanco.Text)) then begin
          MDPagtoEstab.Edit;
          MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux + contador_aux_debitos_futuros;
          MDPagtoEstabliquido.AsCurrency    := (MDPagtoEstabbruto.AsFloat -
                                                      (MDPagtoEstabtaxa_extra.AsCurrency +
                                                      MDPagtoEstabcomissao_adm.AsFloat +
                                                      MDPagtoEstabTX_DVV.AsFloat)
                                                + CreditosFuturos + taxaDeCreditoFixo
                                                );
        end
      end;
      //end;
    end

    //--------------SE ESTIVER DESMARCANDO--------------//
    //--------------------------------------------------//
    //--------------------------------------------------//
    else
    begin
      valor_taxa := 0;
      flagMarcado := false;
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      if MDPagtoEstabATRASADO.Value = 'S' then
      begin
        qPgtoDesc.SQL.Add('SELECT tra.id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
        qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
        qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
        qPgtoDesc.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString); qPgtoDesc.Open;
        qPgtoDesc.First;
        while not qPgtoDesc.Eof do
        begin
          valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
          SQL := TSqlMount.Create(smtUpdate,'TAXAS_REPASSE_ATRASADA');
          SQL.ClearFields;
          SQL.AddField('MARCADO','N',ftString);
          DMConexao.Query1.SQL    := SQL.GetSqlString;
          DMConexao.Query1.ExecSQL;
          qPgtoDesc.Next;
        end;
      end
      else
      begin
        qPgtoDesc.SQL.Add('SELECT tr.id,rtc.CRED_ID,rtc.taxa_id,t.descricao,t.valor,coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK, '''' AS TIPO_RECEITA ');
        qPgtoDesc.SQL.Add('FROM REL_TAXA_CRED rtc LEFT JOIN taxas_repasse_temp tr ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id');
        qPgtoDesc.SQL.Add(',taxas t where rtc.taxa_id = t.taxa_id');
        qPgtoDesc.SQL.Add(' and rtc.cred_id = '+MDPagtoEstabcred_id.AsString);
        qPgtoDesc.SQL.Add(' UNION ALL');
        qPgtoDesc.SQL.Add(' SELECT  TR.ID, TPP.CRED_ID, 0 AS TAXA_ID, TPP.descricao, TPP.valor, coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK,COALESCE(TPP.TIPO_RECEITA,'''') TIPO_RECEITA ');
        qPgtoDesc.SQL.Add(' FROM TAXAS_PROX_PAG TPP LEFT JOIN taxas_repasse_temp tr ON TPP.TAXAS_PROX_PAG_ID = TR.TAXAS_PROX_PAG_ID_FK WHERE TPP.CRED_ID = '+MDPagtoEstabcred_id.AsString+'');

        qPgtoDesc.Open;
        qPgtoDesc.First;
        valor_taxa := valor_taxa + StrToFloat(edTaxaBanco.Text) + MDPagtoEstabTX_DVV.AsFloat;
        while not qPgtoDesc.Eof do
        begin

          valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency + StrToFloat(edTaxaBanco.Text) + MDPagtoEstabTX_DVV.AsFloat;

          if qPgtoDesctaxa_id.AsInteger = 0 then
          begin
             DMConexao.ExecuteSql('DELETE FROM TAXAS_REPASSE_TEMP WHERE TAXAS_PROX_PAG_ID_FK = '+
            ''+qPgtoDescTAXAS_PROX_PAG_ID_FK.AsString+' AND CRED_ID = '+qPgtoDescCRED_ID.AsString);
          end
          else
          begin
            DMConexao.ExecuteSql('DELETE FROM TAXAS_REPASSE_TEMP WHERE TAXA_ID = '+
            ''+qPgtoDesctaxa_id.AsString+' AND CRED_ID = '+qPgtoDescCRED_ID.AsString);
          end;
          qPgtoDesc.Next;
        end;
      end;
      if valor_taxa > 0 then
      begin
        if MDPagtoEstabtaxa_extra.AsFloat > 0 then begin
          MDPagtoEstab.Edit;
          if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
          begin
            MDPagtoEstabtaxa_extra.AsCurrency := StrToFloat(edTaxaBanco.Text);
            MDPagtoEstabliquido.AsCurrency := MDPagtoEstabbruto.AsFloat - MDPagtoEstabcomissao_adm.AsFloat - StrToFloat(edTaxaBanco.Text) - MDPagtoEstabTX_DVV.AsFloat;
          end
          else
          begin
            MDPagtoEstabtaxa_extra.AsCurrency := 0;
            MDPagtoEstabliquido.AsCurrency := MDPagtoEstabbruto.AsFloat - MDPagtoEstabcomissao_adm.AsFloat - MDPagtoEstabTX_DVV.AsFloat;
          end;
        end;
      end;
    end;
  end;
  AbrirValoresDescontos;
  SomarMarcados;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.BtnMarcaTodosClick(Sender: TObject);
var marca                                                       : TBookmark;
var SQL                                                         : TSqlMount;
var contador,DebitosFuturos,CreditosFuturos                     : Double;
var contador_aux_debitos_futuros, contador_aux_creditos_futuros,taxaDeCreditoFixo : Double;
var valor_taxa,valor_taxa_aux,contador_aux                      : Currency;
var _isTaxaExtra                                                : Boolean;
var i                                                           : Integer;


begin
  inherited;
  Screen.Cursor := crHourGlass;
  BtnCancelarPag.Enabled := True;
  bntGerarPDF.Enabled   := not qPgtoEstab.IsEmpty;
  BtnEfetuar.Enabled    := rdgPagamentos.ItemIndex <> 2;
  BtnImprimir.Enabled   := true;
  BtnDesmTodos.Click;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  //POSICIONA O CURSOR DO GRID PRINCIPAL NO PRIMEIRO REGISTRO
  MDPagtoEstab.First;
  //Percorrentdo o DataSet do Grid Principal 
  while not MDPagtoEstab.Eof do
  begin
    //CONTROLE DE PAGAMENTOS J� EFETUADOS

     if MDPagtoEstabBAIXADO.AsString = 'S' then
     begin
        SqlGetLancFuturos;
        QLancCred.Close;
        QLancCred.SQL.Clear;
        QLancCred.SQL.Add(SqlLancamentosFuturos);
        QLancCred.Open;
        //Calcula o valor de d�bito e cr�dito lan�ado no formul�rio do credenciado
        //especificamente na aba taxas
        while not QLancCred.Eof do
        begin
          if QLancCred.Fields.FieldByName('tipo_receita').Value = 'D' then
            DebitosFuturos := DebitosFuturos + QLancCred.Fields[0].AsFloat
            else
            CreditosFuturos := CreditosFuturos + QLancCred.Fields[0].AsFloat;
          QLancCred.Next;
        end;
        //FIM DA CONTAGEM

        //MARCA A LINHA NO GRID PRINCIPAL
        MDPagtoEstab.Edit;
        MDPagtoEstabmarcado.AsBoolean := True;
        MDPagtoEstab.Post;

        //Vari�vel contador � respons�vel por calcular o montante de taxas acumuladas para posteriormente confrontar com o valor bruto
        //Como regra de neg�cio ficou definido que se o credenciado possuir liquido < bruto desconsidera o desconto desta taxa para o credenciado
        contador := MDPagtoEstabcomissao_adm.AsCurrency;
        qPgtoDesc.First;
        MDPgtoDesc.Open;
        MDPgtoDesc.DisableControls;
        valor_taxa := 0;
        contador_aux := 0;

        //Ja incrementa a variavel contador com o valor de taxa banc�ria
        //caso o banco_id do credenciado seja diferente de 1,2,3
        if MDPagtoEstabCODBANCO.AsInteger > 3 then
          contador := contador + StrToFloat(edTaxaBanco.Text);
        QDescontosDePagtosBaixados.Close;
        QDescontosDePagtosBaixados.Parameters.ParamByName('cred_id').Value := MDPagtoEstabcred_id.AsString;
        QDescontosDePagtosBaixados.Parameters.ParamByName('dt_desconto').Value := DateToStr(dataCompensa.Date);
        QDescontosDePagtosBaixados.SQL.Text;
        QDescontosDePagtosBaixados.Open;

        while not QDescontosDePagtosBaixados.Eof do
        begin
          contador := contador + qPgtoDesc.Fields[3].AsFloat;
          if contador < MDPagtoEstabbruto.AsCurrency then
          begin
            contador_aux := contador_aux + qPgtoDescvalor.AsFloat; // conta o somatorio
            //das taxas extras em que o somat�rio � menor do que o Valor Bruto
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
            DMConexao.AdoQry.Open;
            // VARI�VEL VALOR_TAXA ARMAZENA O VALOR DAS TAXAS QUE ATENDEM A CONDI��O
            // CONTADOR < MDPagtoEstabbruto.AsCurrency(OU SEJA TAXAS_ADM + TAXA EXTRA)
            // DEVEM SER MENOR QUE O BRUTO PARA QUE POSSAMOS MARCAS AS TAXAS EXTRAS.
            valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
            SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
            SQL.ClearFields;
            sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
            sql.AddField('TAXAS_PROX_PAG_ID_FK',QDescontosDePagtosBaixados.Fields.FieldByName('TAXAS_PROX_PAG_ID_FK').Value,ftInteger);
            sql.AddField('CRED_ID',QDescontosDePagtosBaixadosCRED_ID.AsInteger,ftInteger);
            sql.AddField('TAXA_ID',QDescontosDePagtosBaixadosTaxa_id.AsInteger,ftInteger);
            sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
            sql.AddField('MARCADO','S',ftString);
            DMConexao.Query1.SQL    := SQL.GetSqlString;
            DMConexao.Query1.ExecSQL;
          end;

          QDescontosDePagtosBaixados.Next;
        end;
     end
    //FIM DE CONTROLE DE PAGAMENTOS J� EFETUADOS


     else begin
        SqlGetLancFuturos;
        QLancCred.Close;
        QLancCred.SQL.Clear;
        QLancCred.SQL.Add(SqlLancamentosFuturos);
        QLancCred.Open;
        //Calcula o valor de d�bito e cr�dito lan�ado no formul�rio do credenciado
        //especificamente na aba taxas
        contador := 0.0;
        while not QLancCred.Eof do
        begin
          if QLancCred.Fields.FieldByName('tipo_receita').Value = 'D' then
          begin
            DebitosFuturos := DebitosFuturos + QLancCred.Fields[0].AsFloat;
            contador := contador + QLancCred.Fields[0].AsFloat;
          end
            else
              CreditosFuturos := CreditosFuturos + QLancCred.Fields[0].AsFloat;
          QLancCred.Next;
        end;
        //FIM DA CONTAGEM

        // FAZ A MARCA��O NO GRID PRINCIPAL
        MDPagtoEstab.Edit;
        MDPagtoEstabmarcado.AsBoolean := True;
        MDPagtoEstab.Post;

        //--------------SE N�O, N�OESTIVER ATRASADO--------------
        //CASO N�O ESTIVER ATRASADO O SELECT BUSCA REGISTROS NA TABELA TAXAS_REPASSE_TEMP
        if MDPagtoEstabATRASADO.AsString = 'N' then
        begin
          contador :=  contador + MDPagtoEstabcomissao_adm.AsCurrency + MDPagtoEstabTX_DVV.AsCurrency;

          //qPgtoDesc Busca taxas do credenciado incluindo os d�bitos e cr�ditos do pr�ximo pamento
          qPgtoDesc.Close;
          qPgtoDesc.SQL.Clear;
          qPgtoDesc.SQL.Add('SELECT tr.id,rtc.CRED_ID,rtc.taxa_id,t.descricao,t.valor,coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK,t.TIPO_RECEITA AS TIPO_RECEITA');
          qPgtoDesc.SQL.Add('FROM REL_TAXA_CRED rtc LEFT JOIN taxas_repasse_temp tr ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id');
          qPgtoDesc.SQL.Add(',taxas t where rtc.taxa_id = t.taxa_id');
          qPgtoDesc.SQL.Add('and rtc.TAXA_ID not in (select taxa_id from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString);
          qPgtoDesc.SQL.Add(' and datepart(mm,dt_desconto) = datepart(mm,'+QuotedStr(DateToStr(dataCompensa.Date))+') and datepart(YYYY,dt_desconto) = datepart(YYYY,'+QuotedStr(DateToStr(dataCompensa.Date))+'))  and rtc.cred_id = '+MDPagtoEstabcred_id.AsString);
          qPgtoDesc.SQL.Add(' UNION ALL');
          qPgtoDesc.SQL.Add(' SELECT  TR.ID, TPP.CRED_ID, 0 AS TAXA_ID, TPP.descricao, TPP.valor, coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK,COALESCE(TPP.TIPO_RECEITA,'''') TIPO_RECEITA');
          qPgtoDesc.SQL.Add(' FROM TAXAS_PROX_PAG TPP LEFT JOIN taxas_repasse_temp tr ON TPP.TAXAS_PROX_PAG_ID = TR.TAXAS_PROX_PAG_ID_FK WHERE TPP.CRED_ID = '+MDPagtoEstabcred_id.AsString+'');
          qPgtoDesc.Open;
          MDPgtoDesc.DisableControls;
          valor_taxa := 0;
          contador_aux := 0;
          if MDPagtoEstabCODBANCO.AsInteger > 3 then
            if MDPagtoEstabliquido.AsFloat >= StrToFloat(edTaxaBanco.Text) then
              contador := contador + StrToFloat(edTaxaBanco.Text);

          DMConexao.AdoQry.Close;
                      DMConexao.AdoQry.SQL.Clear;
                      DMConexao.AdoQry.SQL.Add('SELECT taxa_id FROM rel_taxa_cred WHERE cred_id = '+MDPagtoEstabcred_id.AsString+'');
                      DMConexao.AdoQry.Open;
            listaDeTaxas := TList.Create;
            while not DMConexao.AdoQry.Eof do
            begin
              listaDeTaxas.Add(Pointer(DMConexao.AdoQry.Fields[0].AsInteger));
              DMConexao.AdoQry.Next;
            end;

            if listaDeTaxas.Count <> 0 then
            begin
               while not qPgtoDesc.Eof do
               begin

                  //Somat�rio taxas (contador) < Bruto ? : Executa cod abaixo
                  listaDeTaxas.First;
                  _isTaxaExtra := False;
                  for i := 0 to listaDeTaxas.Count-1 do
                  begin
                    if qPgtoDesctaxa_id.AsInteger = Integer(listaDeTaxas[i]) then
                    begin
                      _isTaxaExtra := True;
                      Break;
                    end
                    else
                      DMConexao.AdoQry.Next;
                  end;
                  if _isTaxaExtra then
                  begin
                    //SOMA O VALOR DA TAXA FIXA AO CONTADOR.
                    //SOMA O VALOR DA TAXA FIXA AO CONTADOR CASO SEJA TAXA FIXA DO TIPO DEBITO.
                    if qPgtoDescTIPO_RECEITA.AsString = 'D' then
                      contador := contador + qPgtoDesc.Fields[3].AsFloat
                    else
                      taxaDeCreditoFixo := taxaDeCreditoFixo + qPgtoDesc.Fields[3].AsFloat;

                    if (contador < MDPagtoEstabbruto.AsCurrency) or (qPgtoDescTIPO_RECEITA.AsString = 'C') then
                    begin
                      if qPgtoDescTIPO_RECEITA.AsString = 'D' then
                      begin
                        contador_aux := contador_aux + qPgtoDescvalor.AsFloat;
                        valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
                      end;

                      DMConexao.AdoQry.Close;
                      DMConexao.AdoQry.SQL.Clear;
                      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
                      DMConexao.AdoQry.Open;

                      // VARI�VEL VALOR_TAXA ARMAZENA O VALOR DAS TAXAS QUE ATENDEM A CONDI��O
                      // CONTADOR < MDPagtoEstabbruto.AsCurrency(OU SEJA TAXAS_ADM + TAXA EXTRA)
                      // DEVEM SER MENOR QUE O BRUTO PARA QUE POSSAMOS MARCAS AS TAXAS EXTRAS.

                      SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
                      SQL.ClearFields;
                      sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
                      sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
                      SQL.AddField('TAXAS_PROX_PAG_ID_FK', 0, ftInteger );
                      sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger);
                      sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
                      sql.AddField('MARCADO','S',ftString);
                      DMConexao.Query1.SQL    := SQL.GetSqlString;
                      DMConexao.Query1.ExecSQL;
                    end
                    else
                       contador := contador - qPgtoDesc.Fields[3].AsFloat;
                  end ;

                  qPgtoDesc.Next;
               end;
            end;

          ///Regi�o que verifica se h� Lan�amento de Cr�dito e/ou D�bitos futuros(Taxas Avulsas)
          ///caso positivo insere na tabela de taxas tempor�rias
          QLancCred.First;
          while not QLancCred.Eof do
          begin
              if QLancCred.Fields[1].Value = 'D' then
              begin
    //            contador := contador + QLancCred.Fields.FieldByName('valor').Value;
    //            contador_aux := contador_aux + QLancCred.Fields.FieldByName('valor').Value;
                valor_taxa := valor_taxa + QLancCred.Fields.FieldByName('valor').Value;
                contador_aux_debitos_futuros := valor_taxa;
              end;

              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
              DMConexao.AdoQry.Open;

              SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
              SQL.ClearFields;
              sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
              sql.AddField('TAXAS_PROX_PAG_ID_FK',QLancCred.Fields.FieldByName('TAXAS_PROX_PAG_ID').Value,ftInteger);
              sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
              sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
              sql.AddField('MARCADO','S',ftString);
              DMConexao.Query1.SQL    := SQL.GetSqlString;
              DMConexao.Query1.ExecSQL;

            //end;
            QLancCred.Next;
          end;



          valor_taxa_aux := MDPagtoEstabtaxa_extra.AsCurrency;
          if contador < MDPagtoEstabbruto.AsCurrency then begin
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;

            //QUERY PARA SELECIONAR O VALOR TOTAL DAS TAXAS QUE N�O FORAM DESCONTADAS(BAIXADAS)
            //DURANTE O M�S DE CONSULTA
            if MDPagtoEstabATRASADO.Value = 'S' then
            begin
               DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
               DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
               DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE_ATRASADA tra where tra.baixado = ''S''))) ');
               //DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataIni.date))+'))))');
            //end;
               DMConexao.AdoQry.Open;
            end

            else
            begin
              //CONTROLE CASO O TIPO_PAGA_CRED = 4 OU 1
              //if(qPgtoPorPAGA_CRED_POR_ID.AsInteger <> 4) then
              //begin
                DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
                DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
                DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE tr ');
                DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataCompensa.date))+'))))');
              //end
              {else
              begin
                DMConexao.AdoQry.SQL.Add('select coalesce (sum(valor),0) from taxas t where taxa_id in ');
                DMConexao.AdoQry.SQL.Add('((select taxa_id from REL_TAXA_CRED where cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
                DMConexao.AdoQry.SQL.Add('taxa_id not in(select taxa_id from TAXAS_REPASSE tr ');
                DMConexao.AdoQry.SQL.Add('where tr.CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND datepart(MM,(select max(dt_desconto) from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString+')) = datepart(MM,'+QuotedStr(DateToStr(dataIni.date))+'))))');

              end;}
            end;
            DMConexao.AdoQry.Open;
            //VERIFICA SE O VALOR "ORIGINAL" DA TAXA EXTRA(VALOR_TAXA_AUX) � DIFERENTE
            //DO QUE O SOMADO(ATRIBUIDO A VARIAVEL VALOR_TAXA) NA BUSCA APLICANDO O FILTRO
            //DAS TAXAS QUE AINDA N�O FORAM BAIXADAS NO M�S PARA AQUELE DETERMINADO CREDENCIADO
            if (valor_taxa_aux <> valor_taxa) and (valor_taxa_aux <> StrToFloat(edTaxaBanco.Text)) then begin
                MDPagtoEstab.Edit;
                //MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + DMConexao.AdoQry.Fields[0].Value + DebitosFuturos; //+ MDPagtoEstabTX_DVV.AsFloat;
                MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux + DebitosFuturos; //+ MDPagtoEstabTX_DVV.AsFloat;
                MDPagtoEstabliquido.AsCurrency    := ((MDPagtoEstabliquido.AsCurrency - MDPagtoEstabtaxa_extra.AsCurrency) + CreditosFuturos + taxaDeCreditoFixo);
            end
            else begin
                MDPagtoEstab.Edit;
                MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux  + DebitosFuturos;//+ MDPagtoEstabTX_DVV.AsFloat;
                MDPagtoEstabliquido.AsCurrency    := ((MDPagtoEstabliquido.AsCurrency - contador_aux - DebitosFuturos) + CreditosFuturos + taxaDeCreditoFixo);
            end;



          end
            //else if contador_aux < MDPagtoEstabbruto.AsCurrency then
            //begin
          else begin
              if (valor_taxa_aux <> valor_taxa) and (contador_aux <> StrToFloat(edTaxaBanco.Text)) then begin
                MDPagtoEstab.Edit;
                MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + contador_aux + contador_aux_debitos_futuros;
                MDPagtoEstabliquido.AsCurrency    := (MDPagtoEstabbruto.AsFloat -
                                                            (MDPagtoEstabtaxa_extra.AsCurrency +
                                                            MDPagtoEstabcomissao_adm.AsFloat +
                                                            MDPagtoEstabTX_DVV.AsFloat)
                                                      + CreditosFuturos + taxaDeCreditoFixo
                                                      );
              end
          end;

        end
        //SE FOR UM PAGAMENTO ATRASADO
        else
        begin
          qPgtoDesc.Close;
          qPgtoDesc.SQL.Clear;
          qPgtoDesc.SQL.Add('SELECT tra.id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
          qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
          qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
          qPgtoDesc.SQL.Add('where tra.baixado = ''N'' and tra.cred_id = '+MDPagtoEstabcred_id.AsString);
          qPgtoDesc.Open;
          qPgtoDesc.First;
          MDPgtoDesc.Open;
          MDPgtoDesc.DisableControls;
          valor_taxa := 0;
          if MDPagtoEstabCODBANCO.AsInteger > 3 then
            contador := contador + StrToFloat(edTaxaBanco.Text);
          while not qPgtoDesc.Eof do
          begin
            contador := contador + qPgtoDesc.Fields[3].AsFloat;
            if contador < MDPagtoEstabbruto.AsCurrency then
            begin
              valor_taxa := valor_taxa + qPgtoDescvalor.AsCurrency;
              SQL := TSqlMount.Create(smtUpdate,'TAXAS_REPASSE_ATRASADA');
              SQL.ClearFields;
              SQL.AddField('MARCADO','S',ftString);
              DMConexao.Query1.SQL    := SQL.GetSqlString;
              DMConexao.Query1.ExecSQL;
            end;
            qPgtoDesc.Next;
          end;

          valor_taxa_aux := MDPagtoEstabtaxa_extra.AsCurrency;

          if contador < MDPagtoEstabbruto.AsCurrency then begin
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;

            DMConexao.AdoQry.SQL.Add('select coalesce(sum(valor),0) from taxas t inner join ');
            DMConexao.AdoQry.SQL.Add('TAXAS_REPASSE_ATRASADA tra ON t.TAXA_ID = tra.taxa_id');
            DMConexao.AdoQry.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString+' and tra.baixado = ''N''');
            //end;
            DMConexao.AdoQry.Open;
            //VERIFICA SE O VALOR "ORIGINAL" DA TAXA EXTRA � DIFERENTE DO QUE O SOMADO NA BUSCA APLICANDO O FILTRO
            //DAS TAXAS QUE AINDA N�O FORAM BAIXADAS NO M�S PARA AQUELE DETERMINADO CREDENCIADO
            if (valor_taxa_aux <> valor_taxa) and (valor_taxa_aux <> StrToFloat(edTaxaBanco.Text)) then begin
              MDPagtoEstab.Edit;
              MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + DMConexao.AdoQry.Fields[0].Value;
              MDPagtoEstabliquido.AsCurrency := MDPagtoEstabliquido.AsCurrency - MDPagtoEstabtaxa_extra.AsCurrency
            end
            else begin
              MDPagtoEstab.Edit;
              MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + DMConexao.AdoQry.Fields[0].Value;
              MDPagtoEstabliquido.AsCurrency := MDPagtoEstabliquido.AsCurrency - DMConexao.AdoQry.Fields[0].Value;

            end;
          end
          else if valor_taxa < MDPagtoEstabbruto.AsCurrency then
          begin

            if (valor_taxa_aux <> valor_taxa) and (valor_taxa <> StrToFloat(edTaxaBanco.Text)) then begin
              MDPagtoEstab.Edit;
              MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + valor_taxa;
              MDPagtoEstabliquido.AsCurrency := MDPagtoEstabliquido.AsCurrency - MDPagtoEstabtaxa_extra.AsCurrency;
            end

          end;

          //MDPagtoEstab.Edit;
          //MDPagtoEstabtaxa_extra.Value := valor_taxa;
        end;
        //reinicia as v�riaveis para que o somat�rio seja aplicado aos demais creds.
        CreditosFuturos := 0;
        DebitosFuturos  := 0;
     end;
    //fim do controle de pagamentos em aberto
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  Pgto_Sel;
  SomarMarcados;
  MarcouTodos := true;
  DesmarcouTodos := false;
  
end;

procedure TfrmPgtoEstabAbertoAntecipacao.BtnDesmTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  MDPagtoEstab.First;
  while not MDPagtoEstab.Eof do
  begin
    MDPagtoEstab.Edit;
    MDPagtoEstabmarcado.AsBoolean := False;
    MDPagtoEstab.Post;

    //REMOVE AS TACHAS DA TABELA TEMPOR�RIA AO DESMARCAR OS CREDENCIADOS
    DMConexao.ExecuteSql('DELETE FROM TAXAS_REPASSE_TEMP WHERE '+
        'CRED_ID = '+MDPagtoEstabcred_id.AsString);

    DMConexao.ExecuteSql('UPDATE taxas_repasse_atrasada SET marcado = ''N''');

    DMConexao.ExecuteSql('ALTER SEQUENCE STAXAS_TEMP RESTART WITH 1');

    MDPagtoEstab.Next;
  end;
  MDPgtoDesc.EmptyTable;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  Pgto_Sel;
  SomarMarcados;
  DesmarcouTodos := true;
  MarcouTodos := False;
  AbrirPagamentos;
  Screen.Cursor := crDefault;;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.SomarMarcados;
var
  marca : TBookmark;
  bruto, liqui, comis, desc : currency;
begin
  ZerarLabels;
  if MDPagtoEstab.Active and (not MDPagtoEstab.IsEmpty) then
  begin
    marca := MDPagtoEstab.GetBookmark;
    MDPagtoEstab.DisableControls;
    MDPagtoEstab.First;
    bruto := 0; liqui := 0;
    comis := 0; desc := 0;
    while not MDPagtoEstab.Eof do begin
      if MDPagtoEstab.FieldByName('MARCADO').AsBoolean = True then begin
        bruto := bruto + MDPagtoEstab.FieldByName('BRUTO').AsCurrency;
        comis := comis + MDPagtoEstab.FieldByName('COMISSAO').AsCurrency; //+ qPgtoEstab.FieldByName('COMISS_R').AsCurrency;
        desc  := desc  + MDPagtoEstab.FieldByName('TAXA_EXTRA').AsCurrency;
        liqui := liqui + MDPagtoEstab.FieldByName('LIQUIDO').AsCurrency;
      end;
      MDPagtoEstab.Next;
    end;
    MDPagtoEstab.GotoBookmark(marca);
    MDPagtoEstab.FreeBookmark(marca);
    MDPagtoEstab.EnableControls;
    LabLiq.Caption   := FormatDinBR(liqui);
    LabBruto.Caption := FormatDinBR(bruto);
    LabDesc.Caption  := FormatDinBR(desc);
    //LabComis.Caption := FormatDinBR(comis);
  end;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.grdPgtoEstabKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  GridScroll(Sender,Key,Shift);
  if key = vk_return then BtnMarcDesm.Click;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f11 : BtnMarcDesm.Click;
     vk_f8  : BtnMarcaTodos.Click;
     vk_f9  : BtnDesmTodos.Click;
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.grdPgtoEstabDblClick(Sender: TObject);
begin
  inherited;
  BtnMarcDesm.Click;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.dsPgtoEstabStateChange(Sender: TObject);
begin
  inherited;
  HabilitarBotoes;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.BtnImprimirClick(Sender: TObject);
var RegAtual : TBookmark;
  RegAtualEmp : TBookmark;
  RegAtualTax : TBookmark;
  dt : TDateTime;
  sl : TStringList;
  semana : integer;
  codigo_pagamento : Integer;
begin
  //ShowMessage(CDSPagDescmarcado.AsString);
  sl := TStringList.Create;
  sl.Clear;
  sl.Sorted := True;
  sl.Duplicates := dupIgnore;
  TotalGeral := 0;
  CDSPagDesc.DisableControls;
  //ShowMessage(CDSPagDescmarcado.AsString);
  if DMConexao.ContaMarcados(MDPagtoEstab) = 0 then
  begin
    MsgInf('N�o h� pagamento marcado !');
  end
  else
  begin
    if MsgSimNao('Confirma a vizualiza��o do relat�rio de pagamento?') then
  begin
      RegAtual := MDPagtoEstab.GetBookmark;
      RegAtualEmp := qPgtoEmpr.GetBookmark;
      qPgtoEmpr.DisableControls;
      MDPgtoDesc.DisableControls;
      MDPagtoEstab.First;
      dt := dataCompensa.Date;
      qBancos.Close;
      qBancos.SQL.Clear;
      qBancos.SQL.Add('Select');
      qBancos.SQL.Add('b.codigo as codbanco,');
      qBancos.SQL.Add('b.banco');
      qBancos.SQL.Add('from');
      qBancos.SQL.Add('bancos b');
      while not MDPagtoEstab.Eof do
      begin
        if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then
        begin
          if sl.Count < 1 then begin
            qBancos.SQL.Add('WHERE');
            sl.Add('CODIGO = ' + MDPagtoEstabCODBANCO.AsString)
          end else
            sl.Add('OR CODIGO = '+ MDPagtoEstabCODBANCO.AsString);
        end;
        if ((MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) and (MDPagtoEstabBAIXADO.AsString = 'N')) then
        begin
          //GravarPagamento(dt);
        end;
        MDPagtoEstab.Next;
      end;
      if sl.Count > 0 then begin
        qBancos.SQL.Add(sl.Text);
        qBancos.Open;
      end;
      frxReport2.Variables['tipoPagamento'] := QuotedStr(qPgtoPorDESCRICAO.AsString);
      frxReport2.Variables['dataIni']       := QuotedStr(DateToStr(dataIni.Date));
      frxReport2.Variables['dataFin']       := QuotedStr(DateToStr(DataFin.Date));
      frxReport2.Variables['dtcompensa']    := QuotedStr(DateToStr(dataCompensa.Date));
      //codigo_pagamento := DMConexao.ExecuteQuery('SELECT MAX(PAGAMENTO_ID) FROM LOG_PAGAMENTO_CREDENCIADO');
      //frxReport1.Variables['codAutorizacao'] := codigo_pagamento;
      semana := WeekOfTheMonth(dataIni.Date);
      {if qPgtoPorPAGA_CRED_POR_ID.AsInteger in [4] then begin
          frxReport1.Variables['semana'].Visible := False;
      end
      else }
      frxReport2.Variables['semana']        := QuotedStr(IntToStr(semana));
      if qPgtoPorPAGA_CRED_POR_ID.AsInteger in[6,7] then
      begin
        if rbEstab.ItemIndex = 0 then
          frxReport2.Variables['tipoEstab']  := QuotedStr(' - Supermercados')
          else
          frxReport2.Variables['tipoEstab']  := QuotedStr(' - Postos');
      end
      else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 8 then
        frxReport2.Variables['tipoEstab']  := QuotedStr(' - Supermercados')
      else
        frxReport2.Variables['tipoEstab']  := QuotedStr('');
      frxReport2.ShowReport;
  end;
  qPgtoEmpr.EnableControls;
end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.CriarTitulo(Imp: TImpres);
var linha: Integer;
begin
  DMConexao.Adm.Open;
  Imp.AddLinha(DMConexao.AdmFANTASIA.AsString);
  DMConexao.Adm.Close;
  Imp.AddLinha(imp.Centraliza('Relacao de Pagamentos'));
  Imp.AddLinha(' ');
  Imp.AddLinhaSeparadora('=');
  linha:= Imp.AddLinha('Estabelecimento: '+FormatFloat('000000',qPgtoEstab.FieldByName('CRED_ID').AsInteger));
  Imp.AddLinha('- '+qPgtoEstab.FieldByName('NOME').AsString,28,linha);
  Imp.AddLinha('CNPJ: '+qPgtoEstab.FieldByName('CGC').AsString,110,linha);
  linha:= Imp.AddLinha('Banco: '+qPgtoEstab.FieldByName('CODBANCO').AsString+' - '+qPgtoEstab.FieldByName('NOMEBANCO').AsString);
  Imp.AddLinha('Agencia: '+qPgtoEstab.FieldByName('AGENCIA').AsString+'   Conta Corrente: '+qPgtoEstab.FieldByName('CONTACORRENTE').AsString,52,linha);
  Imp.AddLinhaSeparadora('=');
  Imp.AddLinha(' ');
end;

procedure TfrmPgtoEstabAbertoAntecipacao.CriarCabecalho(Imp: TImpres);
var linha: Integer;
begin
  if cbbPgtoPor.KeyValue = 1 then
    linha := Imp.AddLinha('Empresa Conveniada                                               Fatura         Bruto    Percentual      Comissao       Liquido')
  else
    linha := Imp.AddLinha('Empresa Conveniada                                                              Bruto    Percentual      Comissao       Liquido');
  Imp.AddLinhaSeparadora();
end;

procedure TfrmPgtoEstabAbertoAntecipacao.CriarCorpo(Imp:TImpres; ConsultaPor: ConsultaPor);
var bru, per, com, tax, Liq: Currency; linha : integer;
  marca: TBookmark;
begin
  bru := 0; per := 0; com := 0; liq := 0; tax := 0;
  marca := qPgtoEmpr.GetBookmark;
  qPgtoEmpr.DisableControls;
  qPgtoEmpr.First;
  while not qPgtoEmpr.Eof do
  begin
    if ((ConsultaPor = PorNada) and (qPgtoEmpr.Bof)) or (ConsultaPor <> PorNada) then
      CriarCabecalho(Imp);
    linha := imp.AddLinha(Imp.Direita(qPgtoEmpr.FieldByName('EMPRES_ID').AsString,6) +' - '+qPgtoEmpr.FieldByName('NOME').AsString);
    if cbbPgtoPor.KeyValue = 1 then
      Imp.AddLinha(Imp.Direita(qPgtoEmpr.FieldByName('FATURA_ID').AsString,6,'0'),68,linha);
    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('BRUTO').AsCurrency),10),78,linha);
    if qPgtoEmpr.FieldByName('BRUTO').AsCurrency > 0 then
      Imp.AddLinha(Imp.Direita(FormatDinBR(((qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency)*100)/qPgtoEmpr.FieldByName('BRUTO').AsCurrency),10),92,linha)
    else
      Imp.AddLinha(Imp.Direita(FormatDinBR(0),10),92,linha);
    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency),10),106,linha);
    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency),10),120,linha);
    if ConsultaPor = PorConv then
      ImprimePorConveniado(Imp)
    else if ConsultaPor = PorAutor then
      ImprimePorAutorizacao(Imp);
    bru := bru + qPgtoEmpr.FieldByName('BRUTO').AsCurrency;
    com := com + qPgtoEmpr.FieldByName('COMISS_C').AsCurrency + qPgtoEmpr.FieldByName('COMISS_R').AsCurrency;
    liq := liq + qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency;
    qPgtoEmpr.Next;
  end;
  if bru = 0 then
    per := 0
  else
    per := (com*100)/bru;
  Imp.AddLinhaSeparadora('=');
  Imp.AddLinha('Total do Fornecedor                                                             Bruto    Percentual      Comissao       Liquido');
  linha := Imp.AddLinha(Imp.Direita(FormatDinBR(bru),10),78);
  Imp.AddLinha(Imp.Direita(FormatDinBR(per),10),92,linha);
  Imp.AddLinha(Imp.Direita(FormatDinBR(com),10),106,linha);
  Imp.AddLinha(Imp.Direita(FormatDinBR(liq),10),120,linha);
  qPgtoEmpr.GotoBookmark(marca);
  qPgtoEmpr.FreeBookmark(marca);
  qPgtoEmpr.EnableControls;
  Imp.SaltarLinhas(1);
  Imp.AddLinha('-------------------------------------------------------------');
  linha:= Imp.AddLinha('Valor Liquido');
  Imp.AddLinha(Imp.Direita(FormatDinBR(liq),10)+' (+)',50,linha);
  Imp.AddLinha('-------------------------------------------------------------');
  if (not qPgtoDesc.IsEmpty) or (qPgtoEstab.FieldByName('REPASSE').AsCurrency > 0.00) then
  begin
    marca := qPgtoDesc.GetBookmark;
    qPgtoDesc.DisableControls;
    qPgtoDesc.First;
    Imp.AddLinha('Descontos e Acr�cimos');
    while not qPgtoDesc.Eof do
    begin
      linha:= Imp.AddLinha(qPgtoDesc.FieldByName('DESCRICAO').AsString);
      Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoDesc.FieldByName('VALOR').AsCurrency),10)+' (-)',50,linha);
      tax:= tax + qPgtoDesc.FieldByName('VALOR').AsCurrency;
      qPgtoDesc.Next;
    end;
    qPgtoDesc.GotoBookmark(marca);
    qPgtoDesc.FreeBookmark(marca);
    qPgtoDesc.EnableControls;
    if qPgtoEstab.FieldByName('REPASSE').AsCurrency > 0.00 then
    begin
      linha:= Imp.AddLinha('REPASSE DE VENDAS');
      Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEstab.FieldByName('REPASSE').AsCurrency),10)+' (+)',50,linha);
    end;
    Imp.AddLinha('-------------------------------------------------------------');
  end;
  linha:= Imp.AddLinha('Valor a Pagar');
  Imp.AddLinha(Imp.Direita(FormatDinBR((liq-tax)+qPgtoEstab.FieldByName('REPASSE').AsCurrency),10)+' (=)',50,linha);
end;

procedure TfrmPgtoEstabAbertoAntecipacao.ImprimePorConveniado(Imp: TImpres);
var linha: Integer;
begin
  qTemp.Close;
  qTemp.SQL.Clear;
  qTemp.SQL.Add(' select ');
  qTemp.SQL.Add('   cv.conv_id, ');
  qTemp.SQL.Add('   cv.chapa, ');
  qTemp.SQL.Add('   cv.titular, ');
  qTemp.SQL.Add('   sum(cc.debito-cc.credito) valor ');
  qTemp.SQL.Add(' from contacorrente cc ');
  qTemp.SQL.Add(' join conveniados cv on cc.conv_id = cv.conv_id ');
  if cbbPgtoPor.KeyValue = 1 then
    qTemp.SQL.Add(' where cc.fatura_id = ' + qPgtoEmpr.FieldByName('FATURA_ID').AsString)
  else
    qTemp.SQL.Add(' where cc.data between ' + FormatDataIB(qPgtoEmpr.FieldByName('DTINI').AsDateTime) + ' and ' + FormatDataIB(qPgtoEmpr.FieldByName('DTFIM').AsDateTime));
  qTemp.SQL.Add(' and cc.baixa_credenciado = ''N'' ');
  qTemp.SQL.Add(' and cv.empres_id = ' + qPgtoEmpr.FieldByName('EMPRES_ID').AsString);
  qTemp.SQL.Add(' and cc.cred_id = ' + qPgtoEstab.FieldByName('CRED_ID').AsString);
  qTemp.SQL.Add(' group by cv.conv_id, cv.chapa, cv.titular ');
  qTemp.SQL.Add(' order by cv.titular, cv.chapa ');
  qTemp.Open;
  if not qTemp.IsEmpty then
  begin
    qTemp.First;
    Imp.AddLinha('-------------------------------------------------------------------------',57);
    Imp.AddLinha('| Nome do Conveniado                                                 Valor',56);
    while not qTemp.Eof do
    begin
      linha:= Imp.AddLinha('| '+FormatFloat('000000000000',qTemp.FieldByName('CHAPA').AsFloat),56);
      Imp.AddLinha('- '+qTemp.FieldByName('TITULAR').AsString,71,linha);
      Imp.AddLinha(Imp.Direita(FormatDinBR(qTemp.FieldByName('VALOR').AsCurrency),10),120,linha);
      qTemp.Next;
    end;
    Imp.SaltarLinhas(1);
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.ImprimePorAutorizacao(Imp: TImpres);
var linha: Integer;
begin
  qTemp.Close;
  qTemp.SQL.Clear;
  qTemp.SQL.Add(' select ');
  qTemp.SQL.Add('    cc.trans_id, ');
  qTemp.SQL.Add('    cc.autorizacao_id, ');
  qTemp.SQL.Add('    cc.digito, ');
  qTemp.SQL.Add('    cv.conv_id, ');
  qTemp.SQL.Add('    cv.chapa, ');
  qTemp.SQL.Add('    cv.titular, ');
  qTemp.SQL.Add('    cc.data, ');
  qTemp.SQL.Add('    cc.debito-cc.credito as valor ');
  qTemp.SQL.Add(' from contacorrente cc ');
  qTemp.SQL.Add(' join conveniados cv on cc.conv_id = cv.conv_id ');
  if cbbPgtoPor.KeyValue = 1 then
    qTemp.SQL.Add(' where cc.fatura_id = ' + qPgtoEmpr.FieldByName('FATURA_ID').AsString)
  else
    qTemp.SQL.Add(' where cc.data between ' + FormatDataIB(qPgtoEmpr.FieldByName('DTINI').AsDateTime) + ' and ' + FormatDataIB(qPgtoEmpr.FieldByName('DTFIM').AsDateTime));
  qTemp.SQL.Add(' and cc.baixa_credenciado = ''N'' ');
  qTemp.SQL.Add(' and cv.empres_id = ' + qPgtoEmpr.FieldByName('EMPRES_ID').AsString);
  qTemp.SQL.Add(' and cc.cred_id = ' + qPgtoEstab.FieldByName('CRED_ID').AsString);
  qTemp.SQL.Add(' order by cv.titular, cc.autorizacao_id, cc.trans_id ');
  qTemp.Open;
  if not qTemp.IsEmpty then
  begin
    qTemp.First;
    if (not qEmpr.Bof) and (qTemp.Bof) then
      CriarCabecalho(Imp);
    Imp.AddLinha('------------------------------------------------------------------------------------------------------------------',16);
    Imp.AddLinha('| Transacao    Autorizacao    Nome do Conveniado                                                 Data         Valor',15);
    while not qTemp.Eof do
    begin
      linha:= Imp.AddLinha('| '+Imp.Direita(qTemp.FieldByName('TRANS_ID').AsString,8),15);
      Imp.AddLinha(Imp.Direita(qTemp.FieldByName('AUTORIZACAO_ID').AsString,8),30,linha);
      Imp.AddLinha('-'+Imp.Direita(qTemp.FieldByName('DIGITO').AsString,2,'0'),38,linha);
      Imp.AddLinha(FormatFloat('000000000000',qTemp.FieldByName('CHAPA').AsFloat),45,linha);
      Imp.AddLinha('-'+qTemp.FieldByName('TITULAR').AsString,56,linha);
      Imp.AddLinha(FormatDataBR(qTemp.FieldByName('DATA').AsDateTime),106,linha);
      Imp.AddLinha(Imp.Direita(FormatDinBR(qTemp.FieldByName('VALOR').AsCurrency),10),120,linha);
      qTemp.Next;
    end;
    Imp.SaltarLinhas(1);
  end;
end;
//TODO -O[SIDNEI] -C[TfrmPgtoEstabAberto.BtnEfetuarClick]:[COMENTADO SIDNEI qPgtoEmpr precisa implementar]
procedure TfrmPgtoEstabAbertoAntecipacao.BtnEfetuarClick(Sender: TObject);
var RegAtual              : TBookmark;
  RegAtualEmp             : TBookmark;
  RegAtualTax             : TBookmark;
  dt                      : TDateTime;
  sl                      : TStringList;
  semana                  : Integer;
  SQL                     : TSqlMount;
  flagGravaLogAnteciapado : Boolean;
  valorBrutoDoPgto        : Currency;
  validouPagamento        : Boolean;
  qtdeRegistroBaixados    : Integer;
  LinhaTxtHeaderArquivo,LinhaTxtHeaderLote,LinhaTxtDetalhesSegmentoA,LinhaTxtTraillerLote,LinhaTxtTraillerDoArquivo : String;

  //objSisPagItau: TSispagItau;
  //objSacado : TSacado;
  //objFavorecido : TCedente;
begin
  sl := TStringList.Create;
  sl.Clear;
  sl.Sorted := True;
  sl.Duplicates := dupIgnore;
  TotalGeral := 0;
  CDSPagDesc.DisableControls;
  SList := TStringList.Create;
  if DMConexao.ContaMarcados(MDPagtoEstab) = 0 then
  begin
    MsgInf('N�o h� pagamento marcado !');
  end
  else
  begin
    if MsgSimNao('Confirma a realiza��o dos pagamentos em aberto no valor de '+LabLiq.Caption+' ?'+sLineBreak+'s� ser�o efetuados os pagamentos dos estabelecimentos n�o baixados!') then
    begin
      if(Date <= DataFin.Date)then
      begin
        GravaLogFinanceiro('ANTECIPOU PAG. FECHAMENTO EM ABERTO','',MDPagtoEstabbruto.AsString,MDPagtoEstabcred_id.AsInteger,4);
        flagGravaLogAnteciapado := True;
      end;
      RegAtual := MDPagtoEstab.GetBookmark;
      RegAtualEmp := qPgtoEmpr.GetBookmark;
      qPgtoEmpr.DisableControls;
      MDPgtoDesc.DisableControls;
      MDPagtoEstab.First;
      dt := dataCompensa.Date;
      qBancos.Close;
      qBancos.SQL.Clear;
      qBancos.SQL.Add('Select');
      qBancos.SQL.Add('b.codigo as codbanco,');
      qBancos.SQL.Add('b.banco');
      qBancos.SQL.Add('from');
      qBancos.SQL.Add('bancos b');
      primeiraVezEfetuaPgto := 0;
      //GERA NUMERO DO LOTE DE PAGAMENTO
      codigo_pagamento := DMConexao.ExecuteQuery('SELECT MAX(PAGAMENTO_ID) FROM LOG_PAGAMENTO_CREDENCIADO');
      while not MDPagtoEstab.Eof do
      begin
        if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then begin
          validouPagamento := ValidaValoresPagamento(MDPagtoEstabcred_id.AsInteger, MDPagtoEstabbruto.AsCurrency);
          //Rotina para baixar pagamentos INCONSISTENTES, Valores INCONSISTENTES entre o financeiro e conferencia de nota
          if(not validouPagamento) then
          begin
            if MsgSimNao('O valor bruto R$ '+MDPagtoEstabbruto.AsString+' calculado pelo m�dulo financeiro � diferente do valor bruto R$ '+CurrToStr(CalculaBrutoContaCorrenteByCredId(MDPagtoEstabcred_id.AsInteger))+' gerado na confer�ncia de notas. Deseja continuar ?') then
            begin
              GravaLogFinanceiro('LIBEROU PGTO, VALOR BRUTO INCONSIST.',MDPagtoEstabbruto.AsString,CurrToStr(CalculaBrutoContaCorrenteByCredId(MDPagtoEstabcred_id.AsInteger)),MDPagtoEstabcred_id.AsInteger,5);
              if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then
              begin
                if sl.Count < 1 then begin
                  qBancos.SQL.Add('WHERE');
                  sl.Add('CODIGO = ' + MDPagtoEstabCODBANCO.AsString)
                end else
                  sl.Add('OR CODIGO = '+ MDPagtoEstabCODBANCO.AsString);
              end;

              if ((MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) and (MDPagtoEstabBAIXADO.AsString = 'N')) then
              begin
                  If(flagGravaLogAnteciapado) Then
                    GravarPagamento(dt,4)
                  else
                    GravarPagamento(dt,3);
                  SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
                  SQL.ClearFields;
                  SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
                  SQL.AddField('DATAHORA',Now, ftDateTime);
                  SQL.AddField('OPERADOR',Operador.Nome, ftString);
                  SQL.AddField('OPERACAO','PAGAMENTO/REPASSE', ftString);
                  SQL.AddField('VALOR_ANT','',ftString);
                  SQL.AddField('VALOR_POS',MDPagtoEstabbruto.AsString,ftString);
                  SQL.AddField('CRED_ID',MDPagtoEstabcred_id.AsString,ftString);
                  SQL.AddField('COD_OPERACAO','3',ftString);
                  SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
                  SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
                  SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(DataFin.Text),ftDateTime);
                  DMConexao.Query1.SQL    := SQL.GetSqlString;
                  DMConexao.Query1.SQL.Text;
                  DMConexao.Query1.ExecSQL;
                  qtdeRegistroBaixados := qtdeRegistroBaixados + 1;
              end;
              MDPagtoEstab.Next;
            end
            else
            begin
              MDPagtoEstab.Next;
            end;
          end

          //Rotina para baixar pagamentos VALIDADOS, Valores CONSISTENTES entre o financeiro e conferencia de nota
          else
          begin
//            objSisPagItau := TSispagItau.Create;
//            objSacado     := TSacado.Create;
//            objFavorecido := TCedente.Create;
//
//
//            //Carregando ObjSacado
//            objSacado     := SetUpSacado('REDE_PLANTAO_DE_CONVENIO_S/S_LTDA_ME','Rua Olinda',830,'SAO_JOSE_DOS_CAMPOS','SP','12232380','0250','437506','07277393000124');
//            objSacado.CodBanco := '2';
//            //Carregando ObjFavorecido
//            objFavorecido := SetUpFavorecido('',MDPagtoEstabENDERECO.AsString,'',MDPagtoEstabNUMERO.AsString,
//                              MDPagtoEstabCEP.AsString,MDPagtoEstabCIDADE.AsString,MDPagtoEstabcred_id.AsString,MDPagtoEstabCOMPLEMENTO.AsString,
//                              MDPagtoEstabTELEFONE1.AsString,MDPagtoEstabnome.AsString,MDPagtoEstabagencia.AsString,'1',MDPagtoEstabcontacorrente.AsString,
//                              '1','','',MDPagtoEstabcgc.AsString,MDPagtoEstabESTADO.AsString,MDPagtoEstabCODBANCO.AsString);
//
//            //objFavorecido.CodBanco := MDPagtoEstabCODBANCO.AsString;
//            //Preenche Linha 1 e Linha 2 do arquivo
//            LinhaTxtHeaderArquivo := objSisPagItau.GerarRegistroHeader240(objSacado);
//            SalvarArquivo(LinhaTxtHeaderArquivo,'REMESSA_FINANC',FALSE,'.txt');
//
//            //Preenche a Linha 2 do arquivo Header do lote
//            LinhaTxtHeaderLote    := objSisPagItau.GeraHeaderDeLote(objSacado);
//            SalvarArquivo(LinhaTxtHeaderLote,'REMESSA_FINANC',False,'.txt');
//
//            //Preenche Linha 3 Segmento A Obrigat�rio
//            LinhaTxtDetalhesSegmentoA := objSisPagItau.GerarRegistroDetalheSegmentoA(objFavorecido,MDPagtoEstabliquido.AsCurrency);
//            SalvarArquivo(LinhaTxtDetalhesSegmentoA,'REMESSA_FINANC',False,'.txt');
//
//            LinhaTxtTraillerLote      := objSisPagItau.GerarRegistroTraillerDeLote(MDPagtoEstabliquido.AsCurrency);
//            SalvarArquivo(LinhaTxtTraillerLote,'REMESSA_FINANC',False,'.txt');
//
//            LinhaTxtTraillerDoArquivo := objSisPagItau.GerarTraillerDoArquivo240();
//            SalvarArquivo(LinhaTxtTraillerDoArquivo,'REMESSA_FINANC',False,'.txt');
//
//            SalvarArquivo('','Remessa_Financ',True,'.txt');

            if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then
            begin
              if sl.Count < 1 then begin
                qBancos.SQL.Add('WHERE');
                sl.Add('CODIGO = ' + MDPagtoEstabCODBANCO.AsString)
              end else
                sl.Add('OR CODIGO = '+ MDPagtoEstabCODBANCO.AsString);
            end;

            if ((MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) and (MDPagtoEstabBAIXADO.AsString = 'N')) then
            begin
                If(flagGravaLogAnteciapado) Then
                  GravarPagamento(dt,4)
                else
                  GravarPagamento(dt,3);
                SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
                SQL.ClearFields;
                SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
                SQL.AddField('DATAHORA',Now, ftDateTime);
                SQL.AddField('OPERADOR',Operador.Nome, ftString);
                SQL.AddField('OPERACAO','PAGAMENTO/REPASSE', ftString);
                SQL.AddField('VALOR_ANT','',ftString);
                SQL.AddField('VALOR_POS',MDPagtoEstabbruto.AsString,ftString);
                SQL.AddField('CRED_ID',MDPagtoEstabcred_id.AsString,ftString);
                SQL.AddField('COD_OPERACAO','3',ftString);
                SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
                SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
                SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(DataFin.Text),ftDateTime);
                DMConexao.Query1.SQL    := SQL.GetSqlString;
                DMConexao.Query1.SQL.Text;
                DMConexao.Query1.ExecSQL;
                qtdeRegistroBaixados := qtdeRegistroBaixados + 1;
            end;
          end;
        end;
        MDPagtoEstab.Next;
      end;
      if sl.Count > 0 then begin
        qBancos.SQL.Add(sl.Text);
        qBancos.Open;
      end;

      if qtdeRegistroBaixados > 0 then begin

        frxReport1.Variables['codAutorizacao'] := codigo_pagamento;
        frxReport1.Variables['tipoPagamento'] := QuotedStr(qPgtoPorDESCRICAO.AsString);
        frxReport1.Variables['dataIni']       := QuotedStr(DateToStr(dataIni.Date));
        frxReport1.Variables['dataFin']       := QuotedStr(DateToStr(DataFin.Date));
        frxReport1.Variables['dtcompensa']    := QuotedStr(DateToStr(dataCompensa.Date));
        semana := WeekOfTheMonth(dataIni.Date);
        frxReport1.Variables['semana']        := QuotedStr(IntToStr(semana));
        if qPgtoPorPAGA_CRED_POR_ID.AsInteger in[6,7] then
        begin
          if rbEstab.ItemIndex = 0 then
            frxReport1.Variables['tipoEstab']  := QuotedStr(' - Supermercados')
            else
            frxReport1.Variables['tipoEstab']  := QuotedStr(' - Postos');
        end
        else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 8 then
          frxReport1.Variables['tipoEstab']  := QuotedStr(' - Supermercado')
        else
          frxReport1.Variables['tipoEstab']  := QuotedStr('');
        frxReport1.ShowReport;
        SomarMarcados;
        MDPagtoEstab.GotoBookmark(RegAtual);
        qPgtoEmpr.GotoBookmark(RegAtualEmp);
        MDPagtoEstab.FreeBookmark(RegAtual);
        qPgtoEmpr.FreeBookmark(RegAtualEmp);
        qPgtoEmpr.EnableControls;
        MDPgtoDesc.EnableControls;
        sl.Free;
        MsgInf('Pagamentos efetuados com sucesso!');
      end;
    end
    else begin
      DMConexao.ExecuteSql('DELETE FROM TAXAS_REPASSE_TEMP');
    end;
    CDSPagDesc.EnableControls;
    ZerarLabels;
    DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET MARCADO = ''N''');
  end;
  flagGravaLogAnteciapado := False;
end;

//function TfrmPgtoEstabAberto.GetValorBrutoContaCorrente(cred_id : integer; data_ini : datetime; data_fin : datetime);
//begin
//
//end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravaLogFinanceiro(Operacao : String; Valor_Ant : String; Valor_Pos : String; Cred_id : Integer; Cod_Operacao : Integer);
var SQL : TSqlMount;
begin
  SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
          SQL.ClearFields;
          SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
          SQL.AddField('DATAHORA',Now, ftDateTime);
          SQL.AddField('OPERADOR',Operador.Nome, ftString);
          SQL.AddField('OPERACAO',Operacao, ftString);
          SQL.AddField('VALOR_ANT',Valor_Ant,ftString);
          SQL.AddField('VALOR_POS',Valor_Pos,ftString);
          SQL.AddField('CRED_ID',IntToStr(Cred_id),ftString);
          SQL.AddField('COD_OPERACAO',IntToStr(Cod_Operacao),ftString);
          SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
          SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
          SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(dataFin.Text),ftDateTime);
          DMConexao.Query1.SQL    := SQL.GetSqlString;
          DMConexao.Query1.SQL.Text;
          DMConexao.Query1.ExecSQL;
end;

function TfrmPgtoEstabAbertoAntecipacao.CalculaBrutoContaCorrenteByCredId(cred_id : integer) : Currency;
var SQL : String;
begin
  SQL :=  'SELECT coalesce(sum(cc.debito - cc.credito),0) Bruto '
        + 'FROM contacorrente cc '
        + 'INNER JOIN credenciados ON (credenciados.cred_id = cc.cred_id) '
        + 'WHERE cc.data BETWEEN '+QuotedStr(DateToStr(dataIni.Date))
	      +	' AND '+QuotedStr(DateToStr(DataFin.Date));
        if rdgAutoriz.ItemIndex = 1 then
        begin
          SQL := SQL + ' and CONFERIDO = ''S''';
        end;
	      SQL := SQL + ' AND cc.cred_id = '+IntToStr(cred_id);

  Result := DMConexao.ExecuteQuery(SQL);

end;

procedure TfrmPgtoEstabAbertoAntecipacao.dataCompensaChange(Sender: TObject);
var CurDate, PrimeiroDia, dataCompensaOld,myDate, dataCompensaChanged,dataParam : TDateTime;
begin
  inherited;
  if not cbbSetManual.Checked then
  begin

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 2)then begin
      dataCompensa.Text := DMConexao.ExecuteQuery('SELECT FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-14,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-8,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

     if(qPgtoPorPAGA_CRED_POR_ID.Value = 4)then
     begin
        if(DayOf(dataCompensa.Date) < 12) then
        begin
          myDate := EncodeDate(YearOf(dataCompensa.Date),MonthOf(dataCompensa.Date),11);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(dataIni.Date))));
        end
        else if (DayOf(dataCompensa.Date) > 26) then
        begin
          myDate := EncodeDate(YearOf((IncMonth(dataCompensa.Date,1))),MonthOf(IncMonth(dataCompensa.Date,1)),11);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(dataIni.Date))));

        end
        else
        begin
          myDate := EncodeDate(YearOf(dataCompensa.Date),MonthOf(dataCompensa.Date),26);
          dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),1));
          DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),15));
        end;
        dataCompensa.Text := DateToStr(myDate);
     end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 6) then
    begin
      if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
        dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-42,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
        DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-36,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
        dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-43,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
        DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-37,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');

      end;
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 7) then
    begin
      if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-57,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-51,(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 8) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(mm,-1,dateadd(dd,-day('+QuotedStr(DateToStr(dataCompensa.Date))+')+1,'+QuotedStr(DateToStr(dataCompensa.Date))+')),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('SELECT format(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'+QuotedStr(DateToStr(dataIni.Date))+')+1,0)),''dd/MM/yyyy'')');
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 9) then
    begin
      //dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');

    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 13) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-7,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
      DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-1,(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+')))),''dd/MM/yyyy'')');
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 12) then
    begin
       dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia10('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
       //dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia10(default),''dd/MM/yyyy'')');
       DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
       dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1)
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 11) then
    begin
      dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia14('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
      dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
    end;

    if(qPgtoPorPAGA_CRED_POR_ID.Value = 10) then
    begin
       dataCompensaOld := dataCompensa.Date;
       PrimeiroDia := StartOfTheMonth(dataCompensa.Date);
       dataCompensaChanged := StrToDate(DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')'));
       if dataCompensaOld <= dataCompensaChanged then
       begin
         DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
         dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
         dataCompensa.Text := DateToStr(dataCompensaChanged);
       end
       else begin
         PrimeiroDia := StartOfTheMonth(IncMonth(dataCompensaOld,1));
         DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
         dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
         dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')');
       end;
    end;
                                                                                         
    if(qPgtoPorPAGA_CRED_POR_ID.Value = 14) then
    begin
      DataFin.Date := IncDay(StrToDate(dataCompensa.Text),-1);
      dataIni.Date := IncDay(StrToDate(dataCompensa.Text),-1)
    end;

    if (qPgtoPorPAGA_CRED_POR_ID.Value = 15) then
    begin
      //dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
      DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');

    end;

  end
  else begin
  vnovo := dataCompensa.Text;
  
      sqlQuery:= ' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERACAO, VALOR_ANT, VALOR_POS, OPERADOR) '+
      ' values (next value for SLog_Financeiro,current_timestamp,'+QuotedStr('Setou manualmente a data de compensacao financeira')+','+QuotedStr(vvelho)+','+QuotedStr(vnovo)+','+QuotedStr(Operador.Nome)+')';
     DMConexao.ExecuteSql(sqlQuery);
     sqlQuery := '';
     vvelho   := '';
     vnovo    := '';
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.dsPgtoEstabDataChange(Sender: TObject; Field:
    TField);
    var strSql,comandText : string;
begin
  inherited;
  if not MDPagtoEstab.IsEmpty then
  begin
   AbrirValoresDescontos;
   Panel8.Caption := 'AUTORIZA��ES NO PER�ODO: '+dataIni.Text+ ' � ' +DataFin.Text;
   QPgtoEmpr.Close;
   QPgtoEmpr.SQL.Clear;
   comandText := 'Select '+
      ' cc.trans_id,' +
      ' cc.empres_id,' +
      ' e.fantasia,' +
      ' cc.conv_id,' +
      ' cc.data,' +
      ' cc.DEBITO' +
    ' from CONTACORRENTE cc' +
    ' inner join empresas e ON e.empres_id = cc.empres_id' +
    ' where cc.cred_id = '+MDPagtoEstabcred_id.AsString+'' +
    ' and cc.data between '''+dataIni.Text+''' and '''+DataFin.Text+''''+
    ' and cc.CREDITO = 0'+
    ' order by EMPRES_ID, DATA';

   QPgtoEmpr.SQL.Text := comandText;
   QPgtoEmpr.Open;
  end;
  //Screen.Cursor := crDefault;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.dsPgtoPorDataChange(Sender: TObject; Field:
    TField);
var data_ini_aux : TDateTime;
var Ultimodia,PrimeiroDia,CurDate : TDateTime;
var    data_i,myDate : TDate;
begin
  inherited;
  if (qPgtoPorPAGA_CRED_POR_ID.Value = 2)then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira(default),''dd/MM/yyyy'')');
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-14,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-8,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 4) then
  begin
    if(DayOf(Date) < 12) then
    begin
      myDate := EncodeDate(YearOf(Date),MonthOf(Date),11);
      dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),16));
      DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-2)),MonthOf(IncMonth(myDate,-2)),DayOf(EndOfTheMonth(IncMonth(myDate,-2)))));
    end
    else
    begin
      myDate := EncodeDate(YearOf(Date),MonthOf(Date),26);
      dataIni.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),1));
      DataFin.Text := DateToStr(EncodeDate(YearOf(IncMonth(myDate,-1)),MonthOf(IncMonth(myDate,-1)),15));
    end;
    dataCompensa.Text := DateToStr(myDate);
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 6) then
  begin
    if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-42,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-36,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 7) then
  begin
    if rbEstab.ItemIndex = 1 then
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end
      else
      begin
        dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getTercaFeira('+QuotedStr(dataCompensa.Text)+'),''dd/MM/yyyy'')');
      end;
    dataIni.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-57,(dbo.getTercaFeira(default)))),''dd/MM/yyyy'')');
    DataFin.Text := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-51,(dbo.getTercaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 8) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getDia30_CurMonth(default),''dd/MM/yyyy'')');
    dataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(mm,-1,dateadd(dd,-day('+QuotedStr(DateToStr(dataCompensa.Date))+')+1,'+QuotedStr(DateToStr(dataCompensa.Date))+')),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('SELECT format(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,'+QuotedStr(DateToStr(dataIni.Date))+')+1,0)),''dd/MM/yyyy'')');
  end;

  if (qPgtoPorPAGA_CRED_POR_ID.Value = 9) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(getdate(),''dd/MM/yyyy'')');
    DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-30,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 13) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getSegundaFeira(default),''dd/MM/yyyy'')');
    dataIni.Text      := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-7,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
    dataFin.Text      := DMConexao.ExecuteQuery('select FORMAT((dateadd(dd,-1,(dbo.getSegundaFeira(default)))),''dd/MM/yyyy'')');
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 12) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia10(default),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1)
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 11) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.getdia14(default),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 10) then
  begin
    PrimeiroDia := StartOfTheMonth(Now);
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(dbo.workday('''+DateToStr(PrimeiroDia)+'''),''dd/MM/yyyy'')');
    DataFin.Date := StartOfTheMonth(dataCompensa.Date) -1;
    dataIni.Date := StartOfTheMonth(StartOfTheMonth(dataCompensa.Date) -1);
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 14) then
  begin
    dataCompensa.Text := DateToStr(IncDay(now,1));
    DataFin.Date := Now;
    dataIni.Date := Now;
  end;

  if(qPgtoPorPAGA_CRED_POR_ID.Value = 15) then
  begin
    dataCompensa.Text := DMConexao.ExecuteQuery('select FORMAT(getdate(),''dd/MM/yyyy'')');
    DataIni.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
    DataFin.Text      := DMConexao.ExecuteQuery('select format(dateadd(day,-10,'+QuotedStr(DateToStr(dataCompensa.Date))+'),''dd/MM/yyyy'')');
  end;


end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravarPagamento(dt : TDateTime; COD_LOG : Integer);
var pagamento_id : integer;
  SQL : TSqlMount;
begin
  if dt = 0 then begin
    msgInf('Selecione uma data v�lida para efetuar o pagamento!');
    Abort;
  end;
  pagamento_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SPAGAMENTO_CRED');
  try
    DMConexao.Query1.Close;
    SQL := TSqlMount.Create(smtInsert,'PAGAMENTO_CRED');
    Sql.AddField('PAGAMENTO_CRED_ID',pagamento_id,ftInteger);
    Sql.AddField('CRED_ID',MDPagtoEstabcred_id.AsInteger,ftInteger);
    sql.AddField('DATA_PGTO',Today,ftDateTime);
    sql.AddField('LOTE_PAGAMENTO', codigo_pagamento, ftInteger);
    sql.AddField('COD_OPERACAO_LOG',COD_LOG, ftInteger);
    sql.AddField('VALOR_TOTAL',ArredondaDin(MDPagtoEstabbruto.AsCurrency),ftFloat);
    sql.AddField('PER_COMISSAO',ArredondaDin(MDPagtoEstabcomissao.AsFloat),ftFloat);
    sql.AddField('VALOR_COMISSAO',ArredondaDin(MDPagtoEstabcomissao_adm.AsFloat),ftFloat);
    sql.AddField('PER_COMISSAO_R',ArredondaDin(0.0),ftFloat);
    sql.AddField('VALOR_COMISSAO_R',ArredondaDin(0.0),ftFloat);
    sql.AddField('TAXAS_VARIAVEIS',ArredondaDin(0.0),ftFloat);
    sql.AddField('REPASSE',ArredondaDin(0.0),ftFloat);
    sql.AddField('VALOR_PAGO',ArredondaDin(MDPagtoEstabliquido.AsFloat),ftFloat);
    sql.AddField('OPERADOR',Operador.Nome,ftString);
    sql.AddField('DATA_HORA',Now,ftDateTime);
    sql.AddField('TAXAS_FIXAS',0,ftFloat);
    sql.AddField('TAXA_BANCARIA',ArredondaDin(StrToCurr(edTaxaBanco.Text)),ftFloat);
    sql.AddField('PAGA_CRED_POR_ID',cbbPgtoPor.KeyValue,ftInteger);
    sql.AddField('DATA_COMPENSACAO',dt,ftDateTime);
    sql.AddField('DATA_INI_PGTO',dataIni.Date,ftDateTime);
    sql.AddField('DATA_FIN_PGTO',DataFin.Date,ftDateTime);
    sql.AddField('TAXA_DVV',ArredondaDin(MDPagtoEstabTX_DVV.AsFloat),ftFloat);
    sql.AddField('APAGADO','N',ftString);
    sql.AddField('ANTECIPADO','S', ftString);
    if (cbbPgtoPor.KeyValue = 2) and (rdgAutoriz.ItemIndex = 0) then
      sql.AddField('SO_CONFIRMADAS','S',ftString)
    else
      sql.AddField('SO_CONFIRMADAS','N',ftString);
    DMConexao.Query1.SQL    := SQL.GetSqlString;
    DMConexao.Query1.SQL.Text;
    Screen.Cursor := crHourGlass;
    DMConexao.AdoCon.BeginTrans;
    DMConexao.Query1.ExecSQL;

    if primeiraVezEfetuaPgto = 0 then
    begin
      GravarLogDoPagamento(DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLOG_PAGAMENTOID'));
      primeiraVezEfetuaPgto := primeiraVezEfetuaPgto + 1;
    end;
    //GravarPgtoCredDet(pagamento_id);
    GravarPgtoCredDesc(pagamento_id,MDPagtoEstabcred_id.AsString);
    GravarPgtoCredAutors(pagamento_id);

    MDPagtoEstab.Edit;
    MDPagtoEstabBAIXADO.AsString:= 'S';
    MDPagtoEstab.Post;
    DMConexao.AdoCon.CommitTrans;
    Screen.Cursor := crDefault;
    //AbrirPagamentos;
  except
    on e:Exception do
    begin
      DMConexao.AdoCon.RollbackTrans;
      Screen.Cursor := crDefault;
      MsgErro('Um erro ocorreu durante o pagamento, erro: '+e.Message+sLineBreak+'Pagamento para o estabelecimento '+MDPagtoEstabnome.AsString+' n�o efetuado.');
    end;
  end;
  sql.Free;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravarLogDoPagamento(pagamento_id: Integer);
var SQL : TSqlMount;
begin
  SQL := TSqlMount.Create(smtInsert,'LOG_PAGAMENTO_CREDENCIADO');
  SQL.ClearFields;
  SQL.AddField('PAGAMENTO_ID',pagamento_id,ftInteger);
  SQL.AddField('DATA_GERACAO',Now, ftDateTime);
  SQL.AddField('OPERADOR',Operador.Nome, ftString);
  DMConexao.Query1.SQL    := SQL.GetSqlString;
  DMConexao.Query1.SQL.Text;
  DMConexao.Query1.ExecSQL;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravarPgtoCredDet(Pgto: Integer);
var SQL : TSqlMount;
begin
   SQL := TSqlMount.Create(smtInsert,'PAGAMENTO_CRED_DET');
   qPgtoEmpr.First;
   while not qPgtoEmpr.Eof do begin
      sql.ClearFields;
      sql.AddField('PAGAMENTO_CRED_ID',Pgto,ftInteger);
      sql.AddField('ID',MDPagtoEstab.FieldByName('EMPRES_ID').AsInteger,ftInteger);
      if cbbPgtoPor.KeyValue = 1 then
         sql.AddField('FATURA_ID',MDPagtoEstab.FieldByName('FATURA_ID').AsInteger,ftInteger)
      else begin
         sql.AddField('DATA_INI',DataIni.Date,ftDateTime);
         sql.AddField('DATA_FIN',DataFin.Date,ftDateTime);
      end;
      sql.AddField('VALOR',ArredondaDin(MDPagtoEstab.FieldByName('BRUTO').AsFloat),ftFloat);
      sql.AddField('PER_COMISSAO',ArredondaDin(MDPagtoEstab.FieldByName('COMISSAO').AsFloat),ftFloat);
      sql.AddField('VAL_COMISSAO',ArredondaDin(MDPagtoEstab.FieldByName('COMISSAO_ADM').AsFloat),ftFloat);
      sql.AddField('APAGADO','N',ftString);
      DMConexao.Query1.SQL    := sql.GetSqlString;
      DMConexao.Query1.ExecSQL;
      qPgtoEmpr.Next;
   end;
   SQL.Free;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravarPgtoCredDesc(Pgto: Integer; CredId: String);
var sql : TSqlMount;
var sql2 : TSqlMount;
var diferencaMes : Integer;
begin
  if MDPagtoEstabATRASADO.Value = 'N' then
  begin
    //GRAVA REGISTRO NA TABELA TAXAS_REPASSE INCLUIDO TANTO TAXAS FIXAS E TAXAS LAN�ADAS PARA O PAGAMENTO ATUAL(TAXAS_PROX_PAG)
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT * FROM TAXAS_REPASSE_TEMP WHERE CRED_ID = '+CredId+'');
    DMConexao.AdoQry.Open;

    qPgtoDesc.First;
    while NOT qPgtoDesc.Eof do
    begin

      DMConexao.AdoQry.First;
      while(not DMConexao.AdoQry.Eof) do begin
        if((qPgtoDesctaxa_id.Value = DMConexao.AdoQry.Fields[1].Value) or (
                      (qPgtoDescTAXAS_PROX_PAG_ID_FK.Value <> 0 ) and (qPgtoDescTAXAS_PROX_PAG_ID_FK.Value = DMConexao.AdoQry.Fields[5].Value) )       ) then
        begin
          DMConexao.ExecuteSql
                    ('INSERT INTO TAXAS_REPASSE '+
                    '(CRED_ID, '+
                    'TAXA_ID, '+
                    'DT_DESCONTO, '+
                    'NEGOCIADA, '+
                    'TAXAS_PROX_PAG_ID_FK, '+
                    'HISTORICO, '+
                    'VALOR, ' +
                    'TIPO_DESCONTO,PAGAMENTO_CRED_ID,COD_LOTE_PAGAMENTO) '+
                        'VALUES(' +
                        MDPagtoEstabcred_id.AsString + ', ' +
                        qPgtoDesctaxa_id.AsString +', '+
                        QuotedStr(dataCompensa.Text) + ', ' +
                        QuotedStr('N') + ', ' +
                        qPgtoDescTAXAS_PROX_PAG_ID_FK.AsString+ ', '+
                        QuotedStr(qPgtoDescdescricao.AsString)+ ', '+
                        StringReplace((FloatToStr(qPgtoDescvalor.AsFloat)),',','.',[rfReplaceAll, rfIgnoreCase]) +
                        ', '+ QuotedStr(qPgtoDescTIPO_RECEITA.AsString) +' ,' + IntToStr(Pgto) + ' ,' + IntToStr(codigo_pagamento) +
                        ')'
                    );
          //Exclui taxas avulsas no Form de Credenciados --> Taxas
          DMConexao.ExecuteSql('DELETE FROM TAXAS_PROX_PAG WHERE TAXAS_PROX_PAG_ID = '+qPgtoDescTAXAS_PROX_PAG_ID_FK.AsString+' AND CONVERT(VARCHAR,DATA_DO_DESCONTO,103) = '+QuotedStr(DateToStr(dataCompensa.Date)));
          Break;
        end;
        DMConexao.AdoQry.Next;
      end;


      qPgtoDesc.Next;
    end;
    if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
    begin
        DMConexao.ExecuteSql
            ('INSERT INTO TAXAS_REPASSE '+
            '(CRED_ID, '+
            'TAXA_ID, '+
            'DT_DESCONTO, '+
            'NEGOCIADA, '+
            'TAXAS_PROX_PAG_ID_FK, '+
            'HISTORICO, '+
            'VALOR, TIPO_DESCONTO, PAGAMENTO_CRED_ID,COD_LOTE_PAGAMENTO) '+
                'VALUES(' +
                MDPagtoEstabcred_id.AsString + ', ' +
                '-1' +', '+
                QuotedStr(dataCompensa.Text) + ', ' +
                QuotedStr('N') + ', ' +
                '0'+ ', '+
                QuotedStr('TAXA BANCARIA')+ ', '+
                StringReplace((edTaxaBanco.Text),',','.',[rfReplaceAll, rfIgnoreCase]) +
                ', '+''''''+', '+IntToStr(Pgto)+' , '+IntToStr(codigo_pagamento)+'' +
                ')'
            );
    end;

  end

  else
  begin
    DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET BAIXADO = ''S'' WHERE ' +
    'CRED_ID = '+MDPagtoEstabcred_id.AsString+' AND MARCADO = ''S''');
  end;
end;


procedure TfrmPgtoEstabAbertoAntecipacao.GravarPgtoCredRep(Pgto, EstID: Integer);
var sql : TSqlMount;
begin
  if qPgtoEstab.FieldByName('REPASSE').AsCurrency > 0 then
  begin
    qTemp.Close;
    qTemp.SQL.Clear;
    qTemp.SQL.Add(' select cred_id, empres_id, fatura_id, dtini, dtfim, percent_r, comiss_r from fbtemp ');
    qTemp.SQL.Add(' where cred_id_r = ' + IntToStr(EstID));
    qTemp.Open;
    SQL := TSqlMount.Create(smtInsert,'PAGAMENTO_CRED_REP');
    qTemp.First;
    while not qTemp.Eof do
    begin
      SQL.ClearFields;
      SQL.AddField('PAGAMENTO_CRED_ID',Pgto,ftInteger);
      SQL.AddField('EMPRES_ID',qTemp.FieldByName('EMPRES_ID').AsInteger,ftInteger);
      SQL.AddField('CRED_ID',qTemp.FieldByName('CRED_ID').AsInteger,ftInteger);
      SQL.AddField('PERCENT_R',ArredondaDin(qTemp.FieldByName('PERCENT_R').AsCurrency),ftFloat);
      SQL.AddField('REPASSE',ArredondaDin(qTemp.FieldByName('COMISS_R').AsCurrency),ftFloat);
      if cbbPgtoPor.KeyValue = 1 then
        sql.AddField('FATURA_ID',qTemp.FieldByName('FATURA_ID').AsInteger,ftInteger)
      else
      begin
        sql.AddField('DATAINI',qTemp.FieldByName('DTINI').AsDateTime,ftDateTime);
        sql.AddField('DATAFIN',qTemp.FieldByName('DTFIM').AsDateTime,ftDateTime);
      end;
      DMConexao.Query1.SQL    := SQL.GetSqlString;
      //DMConexao.Query1.Params := SQL.GetParams; -- Comentado Sidnei
      DMConexao.Query1.ExecSQL;
      qTemp.Next;
    end;
    SQL.Free;
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.GravarPgtoCredAutors(Pgto: Integer);
var ids, campo : string;
begin
  if cbbPgtoPor.KeyValue = 1 then
    campo := 'fatura_id'
  else
    campo := 'empres_id';
//  qPgtoEmpr.First;
//  while not qPgtoEmpr.Eof do
//  begin
//    ids := ids+','+qPgtoEmpr.FieldByName(campo).AsString;
//    qPgtoEmpr.Next;
//  end;
//  Delete(ids,1,1);
  DMConexao.Query1.SQL.Clear;
  DMConexao.Query1.SQL.Add(' update contacorrente set pagamento_cred_id = '+IntToStr(Pgto));
  DMConexao.Query1.SQL.Add(' , baixa_credenciado = ''S'' where cred_id = '+MDPagtoEstabcred_id.AsString);
  DMConexao.Query1.SQL.Add(' and coalesce(baixa_credenciado,''N'') = ''N'' and coalesce(pagamento_cred_id,0) = 0 ');
  if cbbPgtoPor.KeyValue = 1 then
    DMConexao.Query1.SQL.Add(' and fatura_id in ('+ids+')')
  else begin
    if MDPagtoEstabATRASADO.AsString = 'N' THEN
       DMConexao.Query1.SQL.Add(' and data between '+FormatDataIB(DataIni.Date)+' and '+FormatDataIB(DataFin.Date))
    else
       DMConexao.Query1.SQL.Add(' and data between ''10/11/2014'' and '+ QuotedStr(DateToStr(IncDay(dataIni.Date, -1))));
    if rdgAutoriz.ItemIndex = 1 then
      DMConexao.Query1.SQL.Add('and CONFERIDO = ''S''');
  end;
  DMConexao.Query1.ExecSQL;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.rdgDataPorClick(Sender: TObject);
begin
  inherited;
  if rdgDataPor.ItemIndex = 0 then
    gpbDatas.Caption := 'Datas das Autoriza��es'
  else
    gpbDatas.Caption := 'Per�odo de Fechamento de Empresas';
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edtDiaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#32,#8]) then
    Key := #0;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.Pgto_Sel;
var marca : TBookmark;
begin
  PgtoEstab_Sel := EmptyStr;
  marca := MDPagtoEstab.GetBookMark;
  MDPagtoEstab.DisableControls;
  MDPagtoEstab.First;
  while not MDPagtoEstab.eof do begin
    if MDPagtoEstabMarcado.AsBoolean then PgtoEstab_Sel := PgtoEstab_Sel + ','+MDPagtoEstabcred_id.AsString;
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  if PgtoEstab_Sel <> '' then PgtoEstab_Sel := Copy(PgtoEstab_Sel,2,Length(PgtoEstab_Sel));
  MDPagtoEstab.EnableControls;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.PgtoDesc_Sel;
var marca : TBookmark;
begin
  strPgtoDesc_Sel := EmptyStr;
  marca := CDSPagDesc.GetBookmark;
  CDSPagDesc.DisableControls;
  CDSPagDesc.First;
  while not CDSPagDesc.Eof do begin
    if CDSPagDescMarcado.AsBoolean then strPgtoDesc_Sel := strPgtoDesc_Sel + ','+CDSPagDesccred_id.AsString;
    CDSPagDesc.Next;
  end;
  CDSPagDesc.GotoBookmark(marca);
  CDSPagDesc.FreeBookmark(marca);
  if strPgtoDesc_Sel  <> '' then strPgtoDesc_Sel  := Copy(strPgtoDesc_Sel ,2,Length(strPgtoDesc_Sel ));
  CDSPagDesc.EnableControls;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEstabAfterScroll(DataSet: TDataSet);
begin
  if ((qPgtoEstab.FieldByName('MARCADO').AsString = 'S') and (qPgtoEstab.FieldByName('BAIXADO').AsString = 'S')) then
    TotalGeral := TotalGeral + qPgtoEstabLIQUIDO.AsCurrency;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEstabBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >=2) then
  begin
    if cbAtrasadas.Checked then
    begin
      qPgtoEstab.Parameters[0].Value := dataIni.Text;
      qPgtoEstab.Parameters[1].Value := dataFin.Text;
      qPgtoEstab.Parameters[2].Value := '10/11/2014';
      qPgtoEstab.Parameters[3].Value := IncDay(dataIni.Date, -1);
    end
    else begin
      qPgtoEstab.Parameters[0].Value := dataIni.Text;
      qPgtoEstab.Parameters[1].Value := DataFin.Text;
    end;
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEmprBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if MDPagtoEstabATRASADO.AsString = 'S' then
  begin
     qPgtoEmpr.Parameters[0].Value := '10/11/2014';
     qPgtoEmpr.Parameters[1].Value := IncDay(dataIni.Date, -1);
  end
  else
  begin
     qPgtoEmpr.Parameters[0].Value := dataIni.Text;
     qPgtoEmpr.Parameters[1].Value := DataFin.Text;
  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.GridDescontosDblClick(Sender: TObject);
var marca,marca_desc               : TBookmark;
    taxa_id,id,taxa_prox_pag_id_fk : Integer;
    valor_desconto                 : Currency;
    cred_id,cred_id_sel            : Variant;
    sql                            : TSqlMount;
begin
  inherited;

   if MDPagtoEstabmarcado.AsBoolean = False then
   begin
     MsgInf('Selecione um credenciado!');
     Exit;
   end;


   cred_id := GridDescontos.Fields[2].Value;
   taxa_id := GridDescontos.Fields[3].Value;
   valor_desconto := GridDescontos.Fields[1].AsCurrency;
   taxa_prox_pag_id_fk := GridDescontos.Fields[6].Value;
   //SE TACHA FOR ATRASADA
   if MDPagtoEstabATRASADO.AsString = 'S' then
   begin
     if GridDescontos.Fields[4].Value = 'S' then
     begin
       id := GridDescontos.Fields[5].Value;
       DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET MARCADO = ''N'' WHERE CRED_ID = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
       MDPagtoEstab.Edit;
       MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency - valor_desconto;
       MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency + valor_desconto;
       MDPagtoEstab.Post;
     end
     else
     begin
        id := GridDescontos.Fields[5].Value;
        DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET MARCADO = ''S'' WHERE CRED_ID = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
        MDPagtoEstab.Edit;
        MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + valor_desconto;
        MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency - valor_desconto;
        MDPagtoEstab.Post;
     end;
   end

   else
   begin
     //Se Marcado = 'S' fa�a:
     if GridDescontos.Fields[4].Value = 'S' then
     begin
       if taxa_id <> 0 then
       begin
         DMConexao.ExecuteSql('DELETE FROM taxas_repasse_temp WHERE cred_id = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
       end
       else begin
         MsgInf('N�o � permitido remover taxas avulsas por aqui. Remova-a pelo cadastro de credenciados');
         Exit;
         //DMConexao.ExecuteSql('DELETE FROM taxas_repasse_temp WHERE cred_id = '+IntToStr(cred_id)+' and taxas_prox_pag_id_fk = '+IntToStr(taxa_prox_pag_id_fk));
       end;
       MDPagtoEstab.Edit;
       MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency - valor_desconto;
       MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency + valor_desconto;
       MDPagtoEstab.Post;
     end
     else
     begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
        DMConexao.AdoQry.Open;
        SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
        SQL.ClearFields;
        sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
        sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
        if taxa_id <> 0 then
          sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger)
        else begin
          qPgtoDesc.Requery();
          sql.AddField('TAXAS_PROX_PAG_ID_FK',qPgtoDescTAXAS_PROX_PAG_ID_FK.AsInteger,ftInteger);
        end;
        sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
        sql.AddField('MARCADO','S',ftString);
        DMConexao.Query1.SQL    := SQL.GetSqlString;
        DMConexao.Query1.ExecSQL;

        MDPagtoEstab.Edit;
        MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + valor_desconto;
        MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency - valor_desconto;
        MDPagtoEstab.Post;
     end;
     end;
   SomarMarcados;
   AbrirValoresDescontos;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.qPgtoEstabAfterOpen(DataSet: TDataSet);
var diferencaMes : Integer;
begin
  inherited;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.rbEstabClick(Sender: TObject);
var dataPosto,dataMercado : TDate;
begin
  inherited;
  if rbEstab.ItemIndex = 1 then
  begin
    dataCompensa.SetFocus;
    dataPosto := IncDay(dataCompensa.Date,-1);
    dataCompensa.Date := dataPosto;
  end
  else
  begin
    dataCompensa.SetFocus;
    dataMercado := IncDay(dataCompensa.Date,+1);
    dataCompensa.Date := dataMercado;
  end;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.grdPgtoEstabTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
var
  i : Integer;
  imprimeCNPJ,CNPJsoNumero : String;
  total_adm,tx_dvv : Double;
begin
  inherited;
  try
    if Pos(Field.FieldName,qPgtoEstab.Sort) > 0 then begin
       if Pos(' DESC',qPgtoEstab.Sort) > 0 then qPgtoEstab.Sort := Field.FieldName
                                                  else qPgtoEstab.Sort := Field.FieldName+' DESC';
    end
    else qPgtoEstab.Sort := Field.FieldName;
    except
   end;

   qPgtoEstab.First;
   MDPagtoEstab.Open;
   MDPagtoEstab.EmptyTable;
   MDPagtoEstab.DisableControls;

   while not qPgtoEstab.Eof do begin
      MDPagtoEstab.Append;
      MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
      MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
        MDPagtoEstabdiafechamento1.AsInteger    := qPgtoEstabdiafechamento1.AsInteger;
        MDPagtoEstabvencimento1.AsInteger       := qPgtoEstabvencimento1.AsInteger;
        //MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        qPgtoEstabCORRENTISTA.AsString;
        //CNPJsoNumero := RetornaNumeros(qPgtoEstabCORRENTISTA.AsString);
        if qPgtoEstabCORRENTISTA.AsString <> '' then
        begin
          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
          end;
          if Length(CNPJsoNumero) = 14 then
          begin
            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
          end
          else
          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
        end
        else
        begin
          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        end;

        MDPagtoEstabcgc.AsString                := qPgtoEstabcgc.AsString;
        MDPagtoEstabcomissao.AsFloat            := qPgtoEstabCOMISSAO.AsFloat;
        MDPagtoEstabcontacorrente.AsString      := qPgtoEstabCONTACORRENTE.AsString;
        MDPagtoEstabagencia.AsString            := qPgtoEstabAGENCIA.AsString;
        MDPagtoEstabCODBANCO.AsInteger          := qPgtoEstabCODBANCO.AsInteger;
        MDPagtoEstabNOME_BANCO.AsString         := qPgtoEstabNOME_BANCO.AsString;
        MDPagtoEstabbruto.AsFloat               := qPgtoEstabBRUTO.AsFloat;
        if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
        //if MDPagtoEstabCODBANCO.Value in[1,2,3] then
        begin
          MDPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat + StrToFloat(edTaxaBanco.Text);
          //MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat - StrToFloat(edTaxaBanco.Text);
        end
        else
        begin
          MDPagtoEstabtaxa_extra.AsFloat := qPgtoEstabTAXA_EXTRA.AsFloat;
        end;
        MDPagtoEstabcomissao_adm.AsFloat        := qPgtoEstabCOMISSAO_ADM.AsFloat;
        total_adm := (qPgtoEstabBRUTO.AsFloat * (qPgtoEstabCOMISSAO.AsFloat/100)) + qPgtoEstabTAXA_EXTRA.AsFloat + MDPagtoEstabtaxa_extra.AsFloat;
        MDPagtoEstabtotal_retido_adm.AsFloat    := total_adm;
        if total_adm > qPgtoEstabBRUTO.AsFloat then begin
          MDPagtoEstabliquido.AsFloat           := qPgtoEstabBRUTO.AsFloat - qPgtoEstabCOMISSAO_ADM.AsFloat;
        end
        else
        MDPagtoEstabliquido.AsFloat             := qPgtoEstabBRUTO.AsFloat - total_adm;
        if(qPgtoPorPAGA_CRED_POR_ID.AsInteger in [2,10]) then
        begin
          tx_dvv := qPgtoEstabTX_DVV.AsFloat * MDPagtoEstabliquido.AsFloat;
          MDPagtoEstabliquido.AsFloat           := MDPagtoEstabliquido.AsFloat - (tx_dvv);
          MDPagtoEstabTX_DVV.AsFloat            := tx_dvv;
        end
        else
        MDPagtoEstabTX_DVV.AsFloat              := 0.0;
        MDPagtoEstabBAIXADO.AsString            := qPgtoEstabBAIXADO.AsString;
        MDPagtoEstabATRASADO.AsString           := qPgtoEstabATRASADO.AsString;
        MDPagtoEstabmarcado.AsBoolean           := False;
        MDPagtoEstab.Post;
      qPgtoEstab.Next;
   end;
   MDPagtoEstab.First;
   MDPagtoEstab.EnableControls;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BtnImprimir.Click;
    frxReport1.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.MDPagtoEstabAfterPost(DataSet: TDataSet);
var diferencaMes : Integer;
begin
  inherited;

  {DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add('SELECT MAX(tr.DT_DESCONTO),rtc.taxa_id,rtc.cred_id from taxas_repasse tr');
  DMConexao.Q.SQL.Add(' inner join rel_taxa_cred rtc ON rtc.cred_id = tr.CRED_ID and tr.CRED_ID = '+MDPagtoEstabcred_id.AsString);
  DMConexao.Q.SQL.Add(' WHERE tr.taxa_id in');
  DMConexao.Q.SQL.Add(' (SELECT taxa_id from rel_taxa_cred WHERE cred_id = '+MDPagtoEstabcred_id.AsString+')');
  DMConexao.Q.SQL.Add(' group by rtc.taxa_id,rtc.cred_id');
  DMConexao.Q.Open;}
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQl.Add('SELECT MAX(tr.DT_DESCONTO) AS DT_DESCONTO,TAXA_ID FROM TAXAS_REPASSE AS TR WHERE  CRED_ID = '+MDPagtoEstabcred_id.AsString);
  DMConexao.Q.SQL.Add('GROUP BY TR.TAXA_ID');
  DMConexao.Q.Open;
  if not DMConexao.Q.IsEmpty then
  begin
    while not DMConexao.Q.Eof do
    begin
      diferencaMes := MonthsBetween(dataIni.Date,DMConexao.Q.Fields[0].Value);
      if (diferencaMes >= 2) AND (MDPagtoEstabATRASADO.AsString = 'S') then
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT * FROM taxas_repasse_atrasada WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
        DMConexao.AdoQry.SQL.Add('mes_referencia = '+IntToStr(diferencaMes)+' and taxa_id = '+DMConexao.Q.Fields[1].AsString);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          DMConexao.ExecuteSql('Insert into taxas_repasse_atrasada(id,cred_id,taxa_id,dt_referencia,marcado,baixado,mes_referencia)'+
                               'values ( next value for STAXAS_TEMP,'+MDPagtoEstabcred_id.AsString+','+DMConexao.Q.Fields[1].AsString+','''+DateToStr(IncMonth(dataIni.Date,-1))+''',''N'',''N'','+IntToStr(diferencaMes)+')');
        end;
      end;
      DMConexao.Q.Next;
    end;

  end;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.CancelaAutorizao1Click(Sender: TObject);
VAR SQL : TSqlMount;
    data : TDateTime;
begin
  inherited;
  if Application.MessageBox('Confirma o cancelamento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    //                   janela           ,campo     , vr_ant                                   , vr_novo                             , operador    , operacao  , cadastro                      , id                                                    , detalhe                                                ,)
    DMConexao.GravaLog('FPgtoEstabAberto','Negociada', 'N', 'S',Operador.Nome,'Exclus�o',qPgtoDesc.FieldByName('TAXA_ID').AsString,Self.Name);
  end;

  DMConexao.ExecuteSql('delete from taxas_repasse_atrasada WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and taxa_id = '+qPgtoDesctaxa_id.AsString);
  SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE');
  SQL.ClearFields;
  sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
  sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger);
  sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
  sql.AddField('NEGOCIADA','S',ftString);
  DMConexao.Query1.SQL    := SQL.GetSqlString;
  DMConexao.Query1.ExecSQL;



end;

procedure TfrmPgtoEstabAbertoAntecipacao.btnTaxaBancoClick(Sender: TObject);
VAR SQL    : TSqlMount;
    strSQL,sqlQuery: String;

begin
  inherited;
  MDPagtoEstab.First;
  if(vvelho <> vnovo)then
   begin
      sqlQuery:= ' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERACAO, VALOR_ANT, VALOR_POS, OPERADOR) '+
                 ' values (next value for SLog_Financeiro,current_timestamp, '+QuotedStr('Alterou taxa banc�ria')+','+StringReplace(vvelho,',','.',[rfIgnoreCase])+','+StringReplace(vnovo,',','.',[rfIgnoreCase])+', '+QuotedStr(Operador.Nome)+')';
      DMConexao.ExecuteSql(sqlQuery);
    end;
  while not MDPagtoEstab.Eof do
  begin
    if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
    begin
      MDPagtoEstab.Edit;
      MDPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat + StrToFloat(edTaxaBanco.Text) - taxa_do_banco_old;
      MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat - StrToFloat(edTaxaBanco.Text) + taxa_do_banco_old;
      MDPagtoEstab.Post;
    end;
    MDPagtoEstab.Next;

  end;
  vvelho    := '';
  vnovo     := '';
  sqlQuery  := '';

end;

procedure TfrmPgtoEstabAbertoAntecipacao.Button1Click(Sender: TObject);
var sqlQuery : String;
begin
  inherited;
  MDPagtoEstab.First;
  while not MDPagtoEstab.Eof do
  begin
    if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
    begin
      MDPagtoEstab.Edit;
      MDPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat - StrToFloat(edTaxaBanco.Text);
      MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat + StrToFloat(edTaxaBanco.Text);
      MDPagtoEstab.Post;
    end;
    MDPagtoEstab.Next;
    sqlQuery:= ' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERACAO, OPERADOR) '+
               ' values (next value for SLog_Financeiro,current_timestamp, '+QuotedStr('Removeu taxa banc�ria')+','+QuotedStr(Operador.Nome)+')';
    DMConexao.ExecuteSql(sqlQuery);


  end;
  sqlQuery  := '';

end;

procedure TfrmPgtoEstabAbertoAntecipacao.edTaxaBancoKeyPress(Sender: TObject;
  var Key: Char);
var
  Texto, Texto2: string;
  contador : Integer;
  i: byte;
begin
  inherited;
  if (Key in ['0'..'9',chr(vk_back)]) then
   begin
      
      // limito a 23 caracteres sen�o haver� um erro na fun��o StrToInt64()
      if (key in ['0'..'9']) and (Length(Trim(TEdit(Sender).Text))>23) then
         key := #0;

      // pego somente os caracteres de 0 a 9, ignorando a pontua��o
      Texto2 := '0';
      Texto := Trim(TEdit(Sender).Text)+Key;
      for i := 1 to Length(Texto) do
         if Texto[i] in ['0'..'9'] then
            Texto2 := Texto2 + Texto[i];

      // se foi pressionado BACKSPACE (�nica tecla v�lida, fora os n�meros)
      // apago o �ltimo caractere da string
      if key = chr(vk_back) then
         Delete(Texto2,Length(Texto2),1);

      // formato o texto que depois ser� colocado no Edit
      Texto2 := FormatFloat('#,0.00',StrToInt64(Texto2)/100);

      // preencho os espa�os � esquerda, de modo a deixar o texto
      // alinhado � direita (gambiarra)
      repeat
         Texto2 := ' '+Texto2
      until Canvas.TextWidth(Texto2) >= TEdit(Sender).Width;

      // atribuo a string � propriedade Text do Edit
      TEdit(Sender).Text := Texto2;

      // posiciono o cursor no fim do texto
      TEdit(Sender).SelStart := Length(Texto2);

   end;

   Key := #0;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edTaxaBancoEnter(Sender: TObject);
begin
  inherited;
  taxa_do_banco_old := 0;
  taxa_do_banco_old := StrToFloat(edTaxaBanco.Text);
  vvelho :=  edTaxaBanco.Text;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.BtnCancelarPagClick(Sender: TObject);
VAR SQL : TSqlMount;
    qtdDeCancelamentos : Integer;
    codigoLoteDoCancelamento : String;
begin
  inherited;
  if DMConexao.ContaMarcados(MDPagtoEstab) = 0 then
  begin
    MsgInf('N�o h� pagamento marcado !');
  end
  else
  begin
    if MsgSimNao('Confirma o cancelamento dos pagamentos ?'+sLineBreak+' Este processo reverter� os pagamentos e suas respectivas taxas para Pendente!') then
    begin
       FCodLoteEstorno  := TFCodLoteEstorno.Create(Self);
       FCodLoteEstorno.ShowModal;
       if FCodLoteEstorno.ModalResult = mrOk then
       begin
          codigoLoteDoCancelamento := FCodLoteEstorno.txtCodLote.Text;

          MDPagtoEstab.First;
          while not MDPagtoEstab.Eof do
          begin
            Screen.Cursor := crHourGlass;
            if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) and (MDPagtoEstabBAIXADO.Value = 'S') then
            begin
              DMConexao.ExecuteSql('UPDATE contacorrente SET baixa_credenciado = ''N'',pagamento_cred_id = 0 WHERE data between '+
              ''+QuotedStr(dataIni.Text)+' and '+QuotedStr(DataFin.Text)+' and cred_id = '+ MDPagtoEstabcred_id.AsString);

              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('SELECT HISTORICO,VALOR,DT_DESCONTO,TIPO_DESCONTO FROM TAXAS_REPASSE WHERE CRED_ID = '+MDPagtoEstabcred_id.AsString +
                                      ' AND DT_DESCONTO = '+QuotedStr(dataCompensa.Text)+' AND COD_LOTE_PAGAMENTO = '+codigoLoteDoCancelamento+' AND TAXAS_PROX_PAG_ID_FK <> 0');
              DMConexao.AdoQry.Open;
              DMConexao.AdoQry.First;
              WHILE NOT DMConexao.AdoQry.Eof DO
              BEGIN
                RollBackTaxaAvulsa(MDPagtoEstabcred_id.AsInteger,DMConexao.AdoQry.Fields[0].AsString,DMConexao.AdoQry.Fields[1].AsCurrency, DMConexao.AdoQry.Fields[2].AsDateTime, DMConexao.AdoQry.Fields[3].asString);
                DMConexao.AdoQry.Next;
              END;
              DMConexao.ExecuteSql('DELETE from TAXAS_REPASSE WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and dt_desconto = '+QuotedStr(dataCompensa.Text)+' and negociada = ''N'' and cod_lote_pagamento = '+codigoLoteDoCancelamento+'');

              DMConexao.ExecuteSql('UPDATE pagamento_cred SET data_cancelamento = '+QuotedStr(DateToStr(Date))+', cancelado = ''S'' , operador_cancelamento = '+QuotedStr(Operador.Nome)+' ,cod_operacao_log = 1 WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' AND data_compensacao = '+QuotedStr(dataCompensa.Text)+'');
              //PERSISTE DADOS DO CANCENLAMENTO NA TABELA LOG_FINANCEIRO
              SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
              SQL.ClearFields;
              SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
              SQL.AddField('DATAHORA',Now, ftDateTime);
              SQL.AddField('OPERADOR',Operador.Nome, ftString);
              SQL.AddField('OPERACAO','CANCELAMENTO DE REPASSE', ftString);
              SQL.AddField('VALOR_ANT','PGTO_EFETUADO',ftString);
              SQL.AddField('VALOR_POS','PGTO_CANCELADO',ftString);
              SQL.AddField('CRED_ID',MDPagtoEstabcred_id.AsString,ftString);
              SQL.AddField('COD_OPERACAO','1',ftString);
              SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
              SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
              SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(dataFin.Text),ftDateTime);
              DMConexao.Query1.SQL    := SQL.GetSqlString;
              DMConexao.Query1.SQL.Text;
              DMConexao.Query1.ExecSQL;
            end;
            MDPagtoEstab.Next;
          end;
       end;

      Screen.Cursor := crDefault;
      MsgInf('Cancelamento realizado com sucesso, A(s) autoriza��o(��es) est�(�o) dispon�vel(eis) para Pagamento.');
    end;

  end;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.edTaxaBancoExit(Sender: TObject);
begin
  inherited;
  vnovo :=  edTaxaBanco.Text;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.dataCompensaEnter(Sender: TObject);
var str : String;
begin
  inherited;
  str:= DateToStr(dataCompensa.Date);
  //vvelho := TimeToStr
  vvelho := str;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.dataCompensaExit(Sender: TObject);
var str : String;
begin
  inherited;
  str:= DateToStr(dataCompensa.Date);
  vnovo := str;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.ConferirNotas1Click(Sender: TObject);
Var FormConfNotas : TF_EntregaNF;
begin
  inherited;
  FormConfNotas := TF_EntregaNF.Create(Self);
  FormConfNotas.dataini.Date := dataIni.Date;
  FormConfNotas.datafin.Date := DataFin.Date;
  FormConfNotas.Show;
  FormConfNotas.BringToFront;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.LanarTaxas1Click(Sender: TObject);
Var FormCredTaxas : TFCadCred;
    objMenu : TFMenu;
begin
  inherited;

  if not FMenu.vCadEstab then
  begin
    FormCredTaxas := TFCadCred.Create(Self);
    FormCredTaxas.EdCod.Text := IntToStr(grdPgtoEstab.Columns[0].Field.AsInteger);
    FormCredTaxas.Incluir := True;
    FormCredTaxas.Alterar := True;
    FormCredTaxas.Excluir := True;

    FormCredTaxas.Show;
    FormCredTaxas.BringToFront;
  end
  else
  begin
    MsgInf('A tela de "Cadastro de Estabelecimentos" j� est� aberta.');
  end;


end;

procedure TfrmPgtoEstabAbertoAntecipacao.Image13Click(Sender: TObject);
begin
  inherited;
  ConferirNotas1.Click;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.Image4Click(Sender: TObject);
begin
  inherited;
  LanarTaxas1.Click;

end;

procedure TfrmPgtoEstabAbertoAntecipacao.rdgPagamentosClick(Sender: TObject);
begin
  inherited;
  BtnEfetuar.Enabled := rdgPagamentos.ItemIndex <> 2;
  BtnCancelarPag.Enabled := rdgPagamentos.ItemIndex = 2;
end;

function TfrmPgtoEstabAbertoAntecipacao.ValidaValoresPagamento(cred_id : Integer; vl_bruto : Currency): Boolean;
var valorBrutoDoPgto : Currency;
begin
    //Confirma valor bruto financeiro com o valor bruto ContaCorrente(confer�ncia de nota).
    valorBrutoDoPgto := CalculaBrutoContaCorrenteByCredId(cred_id);
    result := valorBrutoDoPgto = vl_bruto;
end;

procedure TfrmPgtoEstabAbertoAntecipacao.SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
begin
  if termino = false then
    SList.Add(Linha);

  if Termino = true then
  begin
    if SList.Count > 0 then begin
      if UpperCase(ExtractFileExt('C:\Users\sidnei.BELLA-RPC\Desktop')) <>  Formato then begin
        SList.SaveToFile('C:\Users\sidnei.BELLA-RPC\Desktop' + '\' + NomeArq + formato);
        AbrirArquivo(ExtractFilePath('C:\Users\sidnei.BELLA-RPC\Desktop'),ExtractFileName('C:\Users\sidnei.BELLA-RPC\Desktop')+ formato,fmOpenRead);
      end else begin
        SList.SaveToFile('C:\Users\sidnei.BELLA-RPC\Desktop');
        AbrirArquivo(ExtractFilePath('C:\Users\sidnei.BELLA-RPC\Desktop'),ExtractFileName('C:\Users\sidnei.BELLA-RPC\Desktop'),fmOpenRead);
      end;
    end;
  end;
end;

//Este m�todo atribui valores �s propriedades da objeto sacado.
function TfrmPgtoEstabAbertoAntecipacao.SetUpSacado(NomeSacado: String; Logradouro: String; NumeroRes: Integer;
            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) : TSacado;
begin
  Sacado := TSacado.Create();
  Sacado.EnderecoEmpresa := Logradouro;
  Sacado.Numero          := NumeroRes;
  Sacado.Cidade          := Cidade;
  Sacado.Estado          := Estado;
  Sacado.CEP             := CEP;
  Sacado.Agencia         := Agencia;
  Sacado.Conta           := Conta;
  Sacado.CPFCNPJ         := CPFCNPJ;
  Sacado.Nome            := NomeSacado;
  Sacado.CodBanco        := '341';
  result := Sacado;
end;

function TfrmPgtoEstabAbertoAntecipacao.SetUpFavorecido(CodigoTranmissao, Logradouro, Bairro, NumeroRes, CEP,
    Cidade, CodigoFavorecido, ComplementoRes, Telefone, NomeFavorecido,
    Agencia, AgenciaDigito, Conta, ContaDigito, Modalidade, Convenio, CNPJCPF,
    UF, CodBanco: string): TCedente;
begin
  Favorecido := TCedente.Create();
  Favorecido.CodigoTransmissao              := CodigoTranmissao;
  Favorecido.Logradouro                     := Logradouro;
  Favorecido.Bairro                         := Bairro;
  Favorecido.NumeroRes                      := NumeroRes;
  Favorecido.CEP                            := CEP;
  Favorecido.Cidade                         := Cidade;
  Favorecido.CodigoCedente                  := CodigoFavorecido;
  Favorecido.Complemento                    := ComplementoRes;
  Favorecido.Telefone                       := Telefone;
  Favorecido.NomeCedente                    := NomeFavorecido;
  Favorecido.Agencia                        := Agencia;
  Favorecido.AgenciaDigito                  := AgenciaDigito;
  Favorecido.Conta                          := Conta;
  Favorecido.ContaDigito                    := ContaDigito;
  Favorecido.Modalidade                     := Modalidade;
  Favorecido.Convenio                       := Convenio;
  Favorecido.CNPJCPF                        := CNPJCPF;
  Favorecido.UF                             := UF;
  Favorecido.CodBanco                       := CodBanco;
  Result := Favorecido;
end;



//function TfrmPgtoEstabAberto.SetUpCedente(NomeFavorecido: String; Logradouro: String; NumeroRes: Integer;
//            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) TCedente;
//begin
//
//end;

//Este M�todo Destr�i o Objeto TSacado.Sacado e Apaga o Ponteiro da mem�ria.
procedure TfrmPgtoEstabAbertoAntecipacao.TearDownSacado();
begin
  Sacado.Free;
  Sacado := Nil;
  //Sacado.Destroy;
end;

function TfrmPgtoEstabAbertoAntecipacao.GetIdGrupoUsuario(idUsuario : Integer) : Integer;
begin
  result := DMConexao.ExecuteQuery('select grupo_usu_id from usuarios where usuario_id = '+IntToStr(idUsuario)+'');
end;

procedure TfrmPgtoEstabAbertoAntecipacao.FormShow(Sender: TObject);
var IdGrupoUsuario : Integer;
begin
  inherited;
  IdGrupoUsuario := GetIdGrupoUsuario(Operador.ID);
  //Verifica se o usu�rio � administrador ou Diretoria (7) � o valor fixo para o grupo Diretoria
  //Diretoria Perfil do Sr. Fernando
  if (Operador.IsAdmin) or (IdGrupoUsuario = 7) then
  begin
    BtnCancelarPag.Visible := True;
  end;
  rbEstab.ItemIndex := -1;
end;

end.
