unit UImpCobreBem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, DB, ZAbstractRODataset, ZDataset, ComObj,
  JvExControls, JvDBLookup, ADODB;
  {JvLookup;}

type
  TFImpCobreBem = class(TForm)
    DSSacado: TDataSource;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btnGerar: TButton;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    btnCancelar: TButton;
    DSAdm: TDataSource;
    DSFatura: TDataSource;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DSBanco: TDataSource;
    DSConta: TDataSource;
    Banco: TJvDBLookupCombo;
    Conta: TJvDBLookupCombo;
    Label3: TLabel;
    Label4: TLabel;
    QSacado: TADOQuery;
    QSacadonome: TStringField;
    QSacadodoc: TStringField;
    QSacadoendereco: TStringField;
    QSacadobairro: TStringField;
    QSacadocidade: TStringField;
    QSacadoestado: TStringField;
    QSacadocep: TStringField;
    QFatura: TADOQuery;
    QFaturaFATURA_ID: TIntegerField;
    QFaturaID: TIntegerField;
    QFaturaDATA_FATURA: TDateTimeField;
    QFaturaDATA_VENCIMENTO: TDateTimeField;
    QFaturaVALOR: TFloatField;
    QFaturaFECHAMENTO: TDateTimeField;
    QFaturaBAIXADA: TStringField;
    QFaturaTIPO: TStringField;
    QFaturaSO_CONFIRMADAS: TStringField;
    QFaturaOPERADOR: TStringField;
    QFaturaDESC_EMPRESA: TFloatField;
    QFaturaPRE_BAIXA: TStringField;
    QFaturaAPAGADO: TStringField;
    QFaturaHORA_FATURA: TStringField;
    QFaturaDATA_BAIXA: TDateTimeField;
    QFaturaOBS: TStringField;
    QFaturaDTAPAGADO: TDateTimeField;
    QFaturaDATA_PRE_BAIXA: TDateTimeField;
    QBanco: TADOQuery;
    QBancocodigo: TIntegerField;
    QBancobanco: TStringField;
    QConta: TADOQuery;
    QContaconta_id: TIntegerField;
    QContaagencia: TStringField;
    QContacontacorrente: TStringField;
    QContataxa_debito: TBCDField;
    QContaseq_remessa: TIntegerField;
    QContadescricao: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGerarClick(Sender: TObject);
    procedure BancoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AbreFatura(fat: integer);
    procedure CarregaSacado(sac: integer);
  end;

var
  FImpCobreBem: TFImpCobreBem;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFImpCobreBem.FormCreate(Sender: TObject);
begin
  DMConexao.Adm.Open;
  QBanco.Open;
end;

procedure TFImpCobreBem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMConexao.Adm.Close;
  QFatura.Close;
  QSacado.Close;
end;

procedure TFImpCobreBem.AbreFatura(fat: integer);
begin
  QFatura.Close;
  QFatura.Parameters.ParamByName('fatura').Value := fat;
  QFatura.Open;
  CarregaSacado(QFaturaID.AsInteger);
end;

procedure TFImpCobreBem.CarregaSacado(sac: integer);
begin
  QSacado.Close;
  QSacado.SQL.Clear;
  if QFaturaTIPO.AsString = 'E' then begin
     QSacado.SQL.Add(' select emp.nome as nome, emp.cgc as doc, emp.endereco, emp.bairro, ');
     QSacado.SQL.Add(' emp.cidade, emp.estado, emp.cep from empresas emp ');
     QSacado.SQL.Add(' where emp.empres_id = '+IntToStr(sac));
  end
  else begin
     QSacado.SQL.Add(' select cv.titular as nome, cv.cpf as doc, cv.endereco, cv.bairro, ');
     QSacado.SQL.Add(' cv.cidade, cv.estado, cv.cep from conveniados cv ');
     QSacado.SQL.Add(' where cv.conv_id = '+IntToStr(sac));
  end;
  QSacado.Open;
end;

procedure TFImpCobreBem.btnGerarClick(Sender: TObject);
var
   CobreBemX: Variant;
   Boleto: Variant;
begin
     CobreBemX := CreateOleObject('CobreBemX.ContaCorrente');
     CobreBemX.ArquivoLicenca := 'c:\CobreBemX\Licencas\237-09.conf';
     CobreBemX.CodigoAgencia := QContaAGENCIA.AsString;
     CobreBemX.NumeroContaCorrente := QContaCONTACORRENTE.AsString;
     CobreBemX.CodigoCedente := '007';
     CobreBemX.InicioNossoNumero := '007';
     CobreBemX.FimNossoNumero := '007';
     CobreBemX.ProximoNossoNumero := '007';
     CobreBemX.PadroesBoleto.PadroesBoletoImpresso.ArquivoLogotipo := 'c:\CobreBemX\Imagens\237.jpg';
     CobreBemX.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras := 'c:\CobreBemX\Imagens\';

     Boleto := CobreBemX.DocumentosCobranca.Add;
     Boleto.NumeroDocumento := 'F'+FormatFloat('000000',QFaturaFATURA_ID.AsFloat);
     Boleto.NomeSacado := QSacadoNOME.AsString;
     Boleto.CPFSacado := QSacadoDOC.AsString;
     Boleto.EnderecoSacado := QSacadoENDERECO.AsString;
     Boleto.BairroSacado := QSacadoBAIRRO.AsString;
     Boleto.CidadeSacado := QSacadoCIDADE.AsString;
     Boleto.EstadoSacado := QSacadoESTADO.AsString;
     Boleto.CepSacado := QSacadoCEP.AsString;
     Boleto.DataDocumento := FormatDataBR(Now);
     Boleto.DataVencimento  := FormatDataBR(QFaturaDATA_VENCIMENTO.AsDateTime);
     Boleto.ValorDocumento := FormatDimIB(QFaturaVALOR.AsCurrency);
     Boleto.PadroesBoleto.Demonstrativo := '';
     Boleto.PadroesBoleto.InstrucoesCaixa := 'Ap�s vencimento, cobrar multa de R$ 0,77 e perman�ncia di�ria de R$ 0,12<br>Protestar ap�s 5 dias do Vencimento<br>Central de Informa��es convenios (19) 3451-3338';

     CobreBemX.ImprimeBoletos;

     CobreBemX := Unassigned;
end;

procedure TFImpCobreBem.BancoExit(Sender: TObject);
begin
  QConta.Close;
  QConta.Parameters.ParamByName('banco').Value := QBancoCODIGO.AsInteger;
  QConta.Open;
end;

end.
