unit UTipos;

interface

uses
  Sysutils, dateutils;


type
  TOperador = record
    Nome    : string;
    ID      : Integer;
    IsAdmin : Boolean;
  end;

  TOcorrencia = record
    Solicitante    : string;
    Motivo         : string;
  end;

  TAutorizacao = record
    Autor_id : string;
    dig      : string;
    Debito   : Currency;
    Credito  : Currency;  
  end;

  TPeriodo = record
    DataIni : TDateTime;
    DataFin : TDateTime;
    Mes,Ano : Word;
    Valor   : Currency;
  end;

  TValores = record
    VlrBrutConf: Currency;
    VlrLiquConf: Currency;
    VlrBrutNConf: Currency;
    VlrLiquNConf: Currency;
  end;

  TBarras = record
    barras: String;
    INBS: String; 
  end;

  TPeriodos = array of TPeriodo;

  TIntegerArray = array of Integer;

  function QuebraEmPeriodos(dataini,datafin:TDateTime):TPeriodos;
  function ArrayIntegerToString(value : TIntegerArray ; const separador : String = ','): string;

implementation

  {TPeriodos }
  function QuebraEmPeriodos(dataini,datafin:TDateTime):TPeriodos;
  var i, x, anos : integer; iniper :TDateTime;
  begin
      anos := YearOf(datafin)-YearOf(dataini);
      x := ( (MonthOf(datafin)+(12*anos)) - MonthOf(dataini) ) +1;
      SetLength(Result,x);
      iniper := dataini;
      for i := 1 to x do begin
          Result[i-1].DataIni := iniper;
          if datafin <= EndOfTheMonth(iniper) then
             Result[i-1].DataFin := datafin
          else
             Result[i-1].DataFin := EndOfTheMonth(iniper);
          Result[i-1].Mes := MonthOf(Result[i-1].DataFin);
          Result[i-1].Ano := YearOf(Result[i-1].DataFin);
          iniper := IncDay(Result[i-1].DataFin);
      end;
  end;

  { TIntegerArray }
  function ArrayIntegerToString(value : TIntegerArray ; const separador : String) : string;
  var I, tam : Integer;
  begin
    Result := '';
    tam := Length(value);
    for I := 0 to tam -1 do begin
      if (I = tam-1) then
        Result := Result + IntToStr(value[I])
      else
        Result := Result + IntToStr(value[I]) + separador;
    end;
  end;

end.
