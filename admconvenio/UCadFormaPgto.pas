unit UCadFormaPgto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,}
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, {JvMemDS,} Mask, JvToolEdit, math,
  Menus, JvExMask, JvExDBGrids, JvDBGrid, ADODB;

type
  TFCadFormaPgto = class(TFCad)
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    Pe1: TDBEdit;
    Label7: TLabel;
    Pe2: TDBEdit;
    Label8: TLabel;
    Pe3: TDBEdit;
    Label9: TLabel;
    Pe4: TDBEdit;
    Label10: TLabel;
    Pe5: TDBEdit;
    Label11: TLabel;
    Pe6: TDBEdit;
    Label12: TLabel;
    Pe7: TDBEdit;
    Label13: TLabel;
    Pe8: TDBEdit;
    Label14: TLabel;
    Pe9: TDBEdit;
    Label15: TLabel;
    Pe10: TDBEdit;
    Label16: TLabel;
    Pe11: TDBEdit;
    Label17: TLabel;
    Pe12: TDBEdit;
    Label18: TLabel;
    Pe13: TDBEdit;
    Label19: TLabel;
    Pe14: TDBEdit;
    Label20: TLabel;
    Pe15: TDBEdit;
    Label21: TLabel;
    Pe16: TDBEdit;
    Label22: TLabel;
    Pe17: TDBEdit;
    Label23: TLabel;
    Pe18: TDBEdit;
    Label24: TLabel;
    Pr1: TDBEdit;
    Label25: TLabel;
    Pr2: TDBEdit;
    Label26: TLabel;
    Pr3: TDBEdit;
    Label27: TLabel;
    Pr4: TDBEdit;
    Label28: TLabel;
    Pr5: TDBEdit;
    Label29: TLabel;
    Pr6: TDBEdit;
    Label30: TLabel;
    Pr7: TDBEdit;
    Label31: TLabel;
    Pr8: TDBEdit;
    Label32: TLabel;
    Pr9: TDBEdit;
    Label33: TLabel;
    Pr10: TDBEdit;
    Label34: TLabel;
    Pr11: TDBEdit;
    Label35: TLabel;
    Pr12: TDBEdit;
    Label36: TLabel;
    Pr13: TDBEdit;
    Label37: TLabel;
    Pr14: TDBEdit;
    Label38: TLabel;
    Pr15: TDBEdit;
    Label39: TLabel;
    Pr16: TDBEdit;
    Label40: TLabel;
    Pr17: TDBEdit;
    Label41: TLabel;
    Pr18: TDBEdit;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    QCadastroFORMA_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroPARCELAS: TSmallintField;
    QCadastroPRAZO01: TSmallintField;
    QCadastroPRAZO02: TSmallintField;
    QCadastroPRAZO03: TSmallintField;
    QCadastroPRAZO04: TSmallintField;
    QCadastroPRAZO05: TSmallintField;
    QCadastroPRAZO06: TSmallintField;
    QCadastroPRAZO07: TSmallintField;
    QCadastroPRAZO08: TSmallintField;
    QCadastroPRAZO09: TSmallintField;
    QCadastroPRAZO10: TSmallintField;
    QCadastroPRAZO11: TSmallintField;
    QCadastroPRAZO12: TSmallintField;
    QCadastroPERCENT01: TFloatField;
    QCadastroPERCENT02: TFloatField;
    QCadastroPERCENT03: TFloatField;
    QCadastroPERCENT04: TFloatField;
    QCadastroPERCENT05: TFloatField;
    QCadastroPERCENT06: TFloatField;
    QCadastroPERCENT07: TFloatField;
    QCadastroPERCENT08: TFloatField;
    QCadastroPERCENT09: TFloatField;
    QCadastroPERCENT10: TFloatField;
    QCadastroPERCENT11: TFloatField;
    QCadastroPERCENT12: TFloatField;
    QCadastroLIBERADO: TStringField;
    QCadastroPRAZO13: TSmallintField;
    QCadastroPRAZO14: TSmallintField;
    QCadastroPRAZO15: TSmallintField;
    QCadastroPRAZO16: TSmallintField;
    QCadastroPRAZO17: TSmallintField;
    QCadastroPRAZO18: TSmallintField;
    QCadastroPERCENT13: TFloatField;
    QCadastroPERCENT14: TFloatField;
    QCadastroPERCENT15: TFloatField;
    QCadastroPERCENT16: TFloatField;
    QCadastroPERCENT17: TFloatField;
    QCadastroPERCENT18: TFloatField;
    QCadastroAPAGADO: TStringField;
    QCadastroOPERADOR: TStringField;
    GroupBox1: TGroupBox;
    DBEdit12: TDBEdit;
    Label43: TLabel;
    DBEdit11: TDBEdit;
    Label44: TLabel;
    DBCheckBox2: TDBCheckBox;
    QCadastroFORCA_DIA: TStringField;
    SpeedButton1: TSpeedButton;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Pe19: TDBEdit;
    Pe20: TDBEdit;
    Pe21: TDBEdit;
    Pe22: TDBEdit;
    Pe23: TDBEdit;
    Pe24: TDBEdit;
    Pr19: TDBEdit;
    Pr20: TDBEdit;
    Pr21: TDBEdit;
    Pr22: TDBEdit;
    Pr23: TDBEdit;
    Pr24: TDBEdit;
    QCadastroPRAZO19: TSmallintField;
    QCadastroPRAZO20: TSmallintField;
    QCadastroPRAZO21: TSmallintField;
    QCadastroPRAZO22: TSmallintField;
    QCadastroPRAZO23: TSmallintField;
    QCadastroPRAZO24: TSmallintField;
    QCadastroPERCENT19: TFloatField;
    QCadastroPERCENT20: TFloatField;
    QCadastroPERCENT21: TFloatField;
    QCadastroPERCENT22: TFloatField;
    QCadastroPERCENT23: TFloatField;
    QCadastroPERCENT24: TFloatField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure CalculaParcelas;
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure ButIncluiClick(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private
    { Private declarations }
    procedure LiberaParcelas;
  public
    { Public declarations }
  end;

var
  FCadFormaPgto: TFCadFormaPgto;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFCadFormaPgto.FormCreate(Sender: TObject);
begin
  chavepri := 'forma_id';
  detalhe  := 'Forma ID';
  inherited;
end;

procedure TFCadFormaPgto.LiberaParcelas;
var i : integer;
begin
   for i :=  0 to ComponentCount - 1 do begin
     if Components[i] is TDBEdit then begin
        if ((Pos('Pr',TDBEdit(Components[i]).Name) > 0) or (Pos('Pe',TDBEdit(Components[i]).Name) > 0)) then
           if StrToInt(SoNumero(TDBEdit(Components[i]).Name)) in [1..QCadastroPARCELAS.AsInteger] then
              TDBEdit(Components[i]).Enabled := True
           else TDBEdit(Components[i]).Enabled := False;
     end;
   end;
end;

procedure TFCadFormaPgto.QCadastroAfterScroll(DataSet: TDataSet);
begin
  LiberaParcelas;
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Forma de Pagamento: '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadFormaPgto.CalculaParcelas;
var i : Integer;
perc, dias, totper : double;
begin
If QCadastroPARCELAS.AsInteger > 0 then begin
   totper := 0;
   dias := 0;
   perc := Trunc((100/QCadastroPARCELAS.AsInteger)*100)/100;
   for i := 1 to QCadastroPARCELAS.AsInteger do begin
       QCadastro.FieldByName('PERCENT'+FormatFloat('00',i)).AsFloat := perc;
       QCadastro.FieldByName('PRAZO'+PadL(IntToStr(i),2,'0')).AsFloat := dias;
       dias := dias+30;
       totper := totper+QCadastro.FieldByName('PERCENT'+FormatFloat('00',i)).AsFloat;
   end;
   for i := QCadastroPARCELAS.AsInteger + 1 to 24 do begin
       QCadastro.FieldByName('PERCENT'+FormatFloat('00',i)).Clear;
       QCadastro.FieldByName('PRAZO'+PadL(IntToStr(i),2,'0')).Clear;
   end;
   if totper < 100 then begin
      totper := 100-totper;
      QCadastroPERCENT01.AsFloat := QCadastroPERCENT01.AsFloat + totper;
   end;
end;
end;

procedure TFCadFormaPgto.QCadastroBeforePost(DataSet: TDataSet);
var i : integer;
totper : double;
begin
  totper := 0;
  for i := 1 to QCadastroPARCELAS.AsInteger do begin
      totper := totper+QCadastro.FieldByName('PERCENT'+FormatFloat('00',i)).AsFloat;
  end;
  if ArredondaDin(totper) <> 100 then begin
     ShowMessage('A soma dos percentuais para esta forma de pagamento n�o totalizam 100%'+#13+'Por favor informe corretamente os percentuais');
     SysUtils.Abort;
  end;
  //Regras para valida��o para que o dia da parcela seja igual o dia da venda.
  if QCadastroFORCA_DIA.AsString = 'S' then begin
     If ((QCadastroPARCELAS.AsInteger = 1) and (QCadastroPRAZO01.AsInteger <> 30)) then begin
        ShowMessage('Para for�ar o dia do fechamento igual ao dia da compra o Prazo 1 tem que ser igual a 30(trinta).');
        SysUtils.Abort;
     end;
     for i := 1 to QCadastroPARCELAS.AsInteger do begin
        if QCadastro.FieldByName('PRAZO'+FormatFloat('00',i)).AsInteger > 0 then
           if QCadastro.FieldByName('PRAZO'+FormatFloat('00',i)).AsInteger mod 30 <> 0 then begin
              ShowMessage('Para for�ar o dia do fechamento igual ao dia da compra os Prazos devem ter intervalos de 30(trinta) dias.');
              SysUtils.Abort;
           end;
     end;
  end;
  inherited;
end;

procedure TFCadFormaPgto.ButIncluiClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := TabFicha;
  DBEdit2.SetFocus;
end;

procedure TFCadFormaPgto.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  if Trim(EdCod.Text) <> '' then QCadastro.Sql.Text := ' select * from formaspagto where apagado <> ''S'' and forma_id in ('+EdCod.Text+')'
  else if Trim(EdNome.Text) <> '' then QCadastro.SQL.Text := ' select * from formaspagto where apagado <> ''S'' and descricao like '+QuotedStr('%'+EdNome.Text+'%')+' order by descricao '
  else QCadastro.Sql.Text := ' select * from formaspagto where apagado <> ''S'' ';
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus else EdCod.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
end;

procedure TFCadFormaPgto.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  QCadastroAPAGADO.AsString   := 'N';
  QCadastroLIBERADO.AsString  := 'S';
  QCadastroFORCA_DIA.AsString := 'N';
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SFORMA_ID AS FORMA_ID');
  DMConexao.AdoQry.Open;
  QCadastroFORMA_ID.AsInteger := DMConexao.AdoQry.FieldByName('FORMA_ID').AsInteger;
end;

procedure TFCadFormaPgto.DBEdit3Exit(Sender: TObject);
begin
  CalculaParcelas;
  LiberaParcelas;
  inherited;
end;

procedure TFCadFormaPgto.DBEdit3KeyPress(Sender: TObject; var Key: Char);
begin
  if DBEdit3.Text <> '' then
  begin
    Try
      if StrToInt(DBEdit3.Text) > 24 then DBEdit3.Text := '24';
    except end;
    CalculaParcelas;
    LiberaParcelas;
  end;
  inherited;
end;

procedure TFCadFormaPgto.SpeedButton1Click(Sender: TObject);
var Mensagem : String;
begin
  inherited;
  Mensagem :=  ' Este campo informa para o programa se as autoriza��es referentes '+#13+
               ' � vendas parceladas ser�o lan�adas no mesmo dia da venda, por ex:'+#13+
               ' se o campo n�o estiver marcado, e uma venda for efetuada em tr�s '+#13+
               ' vezes no dia 23, sendo � vista e 30 e 60 dias o sistema somar� os dias '+#13+
               ' podendo acontecer de uma parcela cair no dia 22 ou outra dia dependendo '+#13+
               ' do m�s, com o campo marcado somente o m�s ser� modificado, sendo que se '+#13+
               ' a venda foi feita no dia 23/06, entao teremos a vista 23/06, a segunda 23/07 '+#13+
               ' e a terceira 23/08.  obs: Para a primeira parcela ser � vista o prazo 1 tem '+#13+
               ' ser igual a 0(zero).';
  Application.MessageBox(PChar(Mensagem),'Informa��o do Sistema',mb_ok+MB_ICONINFORMATION);
end;

procedure TFCadFormaPgto.TabFichaShow(Sender: TObject);
begin
  inherited;
  QCadastro.Open;
end;

end.
