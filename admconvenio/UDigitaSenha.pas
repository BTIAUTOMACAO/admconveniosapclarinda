unit UDigitaSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFDigitaSenha = class(TForm)
    edSenha: TEdit;
    btnOk: TBitBtn;
    Label1: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDigitaSenha: TFDigitaSenha;

implementation

uses cartao_util;

{$R *.dfm}

procedure TFDigitaSenha.btnOkClick(Sender: TObject);
begin
  if edSenha.Text = '' then
  begin
    MsgErro('Digite a nova senha!');
    edSenha.SetFocus;
    Exit;
  end;
end;

procedure TFDigitaSenha.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    btnOk.Click;
  end;
end;

end.
