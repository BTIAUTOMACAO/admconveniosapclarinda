unit UMenuArv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ActnList;

type
  TFMenuArv = class(TForm)
    TreeView1: TTreeView;
    procedure TreeView1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMenuArv: TFMenuArv;

implementation

uses UMenu;

{$R *.dfm}

procedure TFMenuArv.TreeView1Click(Sender: TObject);
begin
if TreeView1.Selected.Data <> nil then begin
   TAction(TreeView1.Selected.Data).Execute;
   WindowState := wsMinimized;
end;
end;

procedure TFMenuArv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_f12 then windowstate := wsminimized;
if key = VK_Escape then close;
end;

procedure TFMenuArv.TreeView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then TreeView1Click(nil);
end;

procedure TFMenuArv.FormShow(Sender: TObject);
begin
TreeView1.SetFocus;
end;

end.
