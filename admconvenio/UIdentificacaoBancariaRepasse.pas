unit UIdentificacaoBancariaRepasse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, JvExControls, JvDBLookup,
  StdCtrls, Mask, JvExMask, JvToolEdit, DB, JvMemoryDataset, ADODB, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, JvExStdCtrls, JvCombobox, JvDBCombobox;

type
  TfrmIdentificacaoBancariaRepasse = class(TF1)
    pnlPanAbe: TPanel;
    bvl1: TBevel;
    txtFormaPgtoDoRepasse: TLabel;
    txtDataDoPagamento: TLabel;
    Label1: TLabel;
    bvl2: TBevel;
    bvl3: TBevel;
    grpDatas: TGroupBox;
    lblDe: TLabel;
    lblA: TLabel;
    dataIni: TJvDateEdit;
    DataFin: TJvDateEdit;
    grp2: TGroupBox;
    lbl2: TLabel;
    dataCompensa: TJvDateEdit;
    grp3: TGroupBox;
    txtCodLote: TEdit;
    btnConsultar: TBitBtn;
    grp1: TGroupBox;
    btnFiltrar: TBitBtn;
    pnl1: TPanel;
    bvl4: TBevel;
    BtnMarcDesm: TButton;
    BtnMarcaTodos: TButton;
    BtnDesmTodos: TButton;
    btnGravar: TButton;
    grdPgtoEstab: TJvDBGrid;
    dsBanco: TDataSource;
    qPgtoPor: TADOQuery;
    qPgtoPorPAGA_CRED_POR_ID: TIntegerField;
    qPgtoPorDESCRICAO: TStringField;
    qBanco: TADOQuery;
    intgrfldBancocod_banco: TIntegerField;
    strngfldBancodescricao: TStringField;
    MDPagtoEstab: TJvMemoryData;
    MDPagtoEstabnome: TStringField;
    MDPagtoEstabcorrentista: TStringField;
    MDPagtoEstabcgc: TStringField;
    MDPagtoEstabcomissao: TCurrencyField;
    MDPagtoEstabcontacorrente: TStringField;
    MDPagtoEstabagencia: TStringField;
    MDPagtoEstabbruto: TCurrencyField;
    MDPagtoEstabtaxa_extra: TCurrencyField;
    MDPagtoEstabcomissao_adm: TCurrencyField;
    MDPagtoEstabtotal_retido_adm: TCurrencyField;
    MDPagtoEstabliquido: TCurrencyField;
    MDPagtoEstabcred_id: TIntegerField;
    MDPagtoEstabmarcado: TBooleanField;
    MDPagtoEstabdiafechamento1: TIntegerField;
    MDPagtoEstabvencimento1: TIntegerField;
    MDPagtoEstabcodbanco: TIntegerField;
    MDPagtoEstabnome_banco: TStringField;
    MDPagtoEstabbaixado: TStringField;
    MDPagtoEstabatrasado: TStringField;
    MDPagtoEstabtx_dvv: TFloatField;
    MDPagtoEstabpagamento_cred_id: TIntegerField;
    MDPagtoEstabendereco: TStringField;
    MDPagtoEstabnumero: TIntegerField;
    MDPagtoEstabcidade: TStringField;
    MDPagtoEstabcep: TStringField;
    MDPagtoEstabestado: TStringField;
    MDPagtoEstabcomplemento: TStringField;
    MDPagtoEstabtelefone1: TStringField;
    MDPagtoEstabconta_cdc: TStringField;
    MDPagtoEstabtaxas_avulsa: TCurrencyField;
    MDPagtoEstabconferencia: TCurrencyField;
    dsPgtoEstab: TDataSource;
    qPgtoEstab: TADOQuery;
    qPgtoEstabcred_id: TIntegerField;
    qPgtoEstabnome: TStringField;
    qPgtoEstabcgc: TStringField;
    qPgtoEstabbruto: TFloatField;
    qPgtoEstabcomissao: TFloatField;
    qPgtoEstabcomissao_adm: TFloatField;
    qPgtoEstabliquido: TFloatField;
    qPgtoEstabdata_hora_repasse: TDateTimeField;
    qPgtoEstabtaxa_extra: TFloatField;
    qPgtoEstabtaxas_avulsa: TFloatField;
    qPgtoEstabtx_dvv: TBCDField;
    qPgtoEstabpaga_cred_por_id: TIntegerField;
    qPgtoEstabpaga_cred_por_descricao: TStringField;
    qPgtoEstabnome_banco: TStringField;
    qPgtoEstabcorrentista: TStringField;
    qPgtoEstabcodbanco: TIntegerField;
    qPgtoEstabagencia: TStringField;
    qPgtoEstabcontacorrente: TStringField;
    qPgtoEstablote_pagamento: TIntegerField;
    cmbBancosLotes: TJvDBLookupCombo;
    qPgtoEstabPAGAMENTO_CRED_ID: TIntegerField;
    grp4: TGroupBox;
    cbbPagamento: TJvDBComboBox;
    qPgtoEstabIDENTIFICACAO_BANCARIA: TStringField;
    MDPagtoEstabidentificacaoBancaria: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure txtCodLoteKeyPress(Sender: TObject; var Key: Char);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure grdPgtoEstabDblClick(Sender: TObject);
    procedure BtnMarcDesmClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnMarcaTodosClick(Sender: TObject);
    procedure BtnDesmTodosClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure CarregarDadosDetalheDoRepasse;
    procedure AbrirPagamentos;
    procedure SQLFactory;
    procedure SQLDestroyString;
    procedure Pgto_Sel;
    procedure SomarMarcados;
  public
    { Public declarations }
  end;

var
  frmIdentificacaoBancariaRepasse: TfrmIdentificacaoBancariaRepasse;
  sql : string;
  MarcouTodos,flagMarcado   : Boolean;
  DesmarcouTodos            : Boolean;
  PgtoEstab_Sel : String;

implementation

uses UMenu, DM, cartao_util;

{$R *.dfm}

procedure TfrmIdentificacaoBancariaRepasse.FormCreate(Sender: TObject);
begin
  inherited;
  FMenu.vFormIdenticaoBancaria := True;
  qPgtoPor.Open;
end;

procedure TfrmIdentificacaoBancariaRepasse.txtCodLoteKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  if (key in [#13]) then
    BtnConsultar.Click;
end;

procedure TfrmIdentificacaoBancariaRepasse.btnConsultarClick(
  Sender: TObject);
begin
  inherited;
  CarregarDadosDetalheDoRepasse;
end;

procedure TfrmIdentificacaoBancariaRepasse.CarregarDadosDetalheDoRepasse;
var loteRepasse : String;
begin

  loteRepasse := txtCodLote.Text;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Text := 'SELECT ' +
  'TOP(1)DATA_COMPENSACAO,DATA_INI_PGTO, DATA_FIN_PGTO, DATA_HORA, PAGA_CRED_POR.DESCRICAO '+
  'FROM PAGAMENTO_CRED '+
  'INNER JOIN PAGA_CRED_POR on PAGA_CRED_POR.PAGA_CRED_POR_ID = PAGAMENTO_CRED.PAGA_CRED_POR_ID '+
  'WHERE LOTE_PAGAMENTO = '+ loteRepasse + ' AND REPASSE_AUTORIZADO = ''S''';

  DMConexao.AdoQry.Open;
  if Not DMConexao.AdoQry.IsEmpty then
    begin
      dataCompensa.Date := DMConexao.AdoQry.Fields[0].AsDateTime;
      dataIni.Date  := DMConexao.AdoQry.Fields[1].AsDateTime;
      DataFin.Date  := DMConexao.AdoQry.Fields[2].AsDateTime;

      Label1.Visible := True;
      txtFormaPgtoDoRepasse.Visible := True;
      txtDataDoPagamento.Visible := True;

      txtDataDoPagamento.Caption := DMConexao.AdoQry.Fields[3].AsString;
      txtFormaPgtoDoRepasse.Caption := DMConexao.AdoQry.Fields[4].AsString;

      qBanco.Close;
      qBanco.Parameters[0].Value := loteRepasse;
      qBanco.Open;
      cmbBancosLotes.SetFocus;
     end
   else
    begin
      Label1.Visible := False;
      txtFormaPgtoDoRepasse.Visible := False;
      txtDataDoPagamento.Visible := False;
      MsgInf('Lote n�o encontrado!');
    end;
end;

procedure TfrmIdentificacaoBancariaRepasse.btnFiltrarClick(
  Sender: TObject);
begin
  inherited;
  if Trim(txtCodLote.Text) <> '' then
  begin
      AbrirPagamentos;
  end
end;

procedure TfrmIdentificacaoBancariaRepasse.AbrirPagamentos;
var mes : integer;
  dia : string;
  CNPJsoNumero,imprimeCNPJ : String;
  i : Integer;
  lista_credID : TList;
  valor_taxa,total_adm,total,tx_dvv,ValorTaxaBancaria,TotalDeTaxasParaDescontar,TotalDeTaxasDescontou : Double;
begin
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >= 2) then
  begin
    SQLFactory; //Esta Procedure cria a query din�mica atribuindo valor a uma
    //vari�vel global chamada sql para passar como par�metro pro m�todo ADD da propriedade SQL
    qPgtoEstab.Close;
    qPgtoEstab.SQL.Clear;
    qPgtoEstab.SQL.Add(sql);
    qPgtoEstab.Open;
    if(qPgtoEstab.IsEmpty)then
    begin
      MDPagtoEstab.EmptyTable;
      BtnMarcDesm.Visible := False;
      BtnMarcaTodos.Visible := False;
      BtnDesmTodos.Visible := False;
      btnGravar.Enabled := False;
      MsgInf('Os crit�rios de busca aplicados n�o retornaram nenhum valor!');
      exit;
    end;
    qPgtoEstab.First;
    MDPagtoEstab.Open;
    MDPagtoEstab.EmptyTable;
    MDPagtoEstab.DisableControls;
    while not qPgtoEstab.Eof do
    begin
        MDPagtoEstab.Append;
        MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
        MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
        qPgtoEstabCORRENTISTA.AsString;
        if qPgtoEstabCORRENTISTA.AsString <> '' then
        begin
          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
          end;
          if Length(CNPJsoNumero) = 14 then
          begin
            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
          end
          else
          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
        end
        else
        begin
          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        end;
        MDPagtoEstabcgc.AsString                         := qPgtoEstabcgc.AsString;
        MDPagtoEstabcomissao.AsFloat                     := qPgtoEstabCOMISSAO.AsFloat;
        MDPagtoEstabcontacorrente.AsString               := qPgtoEstabCONTACORRENTE.AsString;
        MDPagtoEstabagencia.AsString                     := qPgtoEstabAGENCIA.AsString;
        MDPagtoEstabCODBANCO.AsInteger                   := qPgtoEstabCODBANCO.AsInteger;
        MDPagtoEstabNOME_BANCO.AsString                  := qPgtoEstabNOME_BANCO.AsString;
        MDPagtoEstabbruto.AsFloat                        := qPgtoEstabBRUTO.AsFloat;
        MDPagtoEstabcomissao_adm.AsFloat                 := qPgtoEstabCOMISSAO_ADM.AsFloat;
        MDPagtoEstabtaxas_avulsa.AsCurrency              := qPgtoEstabtaxas_avulsa.AsCurrency;
        MDPagtoEstabTX_DVV.AsFloat                       := qPgtoEstabtx_dvv.AsCurrency;
        MDPagtoEstabtaxa_extra.AsCurrency                := qPgtoEstabtaxa_extra.AsCurrency;
        MDPagtoEstabliquido.AsFloat                      := qPgtoEstabliquido.AsCurrency;
        MDPagtoEstabpagamento_cred_id.AsInteger          := qPgtoEstabPAGAMENTO_CRED_ID.AsInteger;
        MDPagtoEstabidentificacaoBancaria.AsString       := qPgtoEstabIDENTIFICACAO_BANCARIA.AsString;
        MDPagtoEstabmarcado.AsBoolean                    := False;
        MDPagtoEstab.Post;
      CNPJsoNumero := '';
      qPgtoEstab.Next;
    end;
    MDPagtoEstab.Next;
    MDPagtoEstab.First;
    MDPagtoEstab.EnableControls;

    BtnMarcDesm.Visible := True;
    BtnMarcaTodos.Visible := True;
    BtnDesmTodos.Visible := True;
    btnGravar.Enabled := True;
    SQLDestroyString;


  end

end;


procedure TfrmIdentificacaoBancariaRepasse.SQLFactory;
begin
   // O from � baseado na tabela credenciados cred
    sql := 'SELECT ' +
   'pc.cred_id, '+
   'cred.nome, '+
   'cred.cgc, '+
   'pc.valor_total as bruto, ' +
   'pc.per_comissao as comissao, ' +
   'pc.valor_comissao as comissao_adm, ' +
   'pc.valor_pago as liquido, ' +
   'pc.data_hora as data_hora_repasse, ' +
   'pc.taxas_fixas as taxa_extra, ' +
   'pc.taxas_variaveis as taxas_avulsa, ' +
   'pc.TAXA_DVV as tx_dvv, ' +
   'pc.paga_cred_por_id, ' +
   'paga_cred_por.descricao as paga_cred_por_descricao, ' +
   'bancos.banco as nome_banco, ' +
   'cred.correntista, ' +
   'cred.banco as codbanco, '+
   'cred.agencia, ' +
   'cred.contacorrente, ' +
   'pc.lote_pagamento,PC.PAGAMENTO_CRED_ID, '+
   'CASE PC.IDENTIFICACAO_BANCARIA WHEN ''0'' THEN ''BELLA'' ELSE CASE PC.IDENTIFICACAO_BANCARIA WHEN ''1'' THEN ' +
   '''CDC'' ELSE CASE PC.IDENTIFICACAO_BANCARIA WHEN ''2'' THEN ''PRATICARD'' ELSE  CASE PC.IDENTIFICACAO_BANCARIA WHEN ''3'' THEN ''RPC'' else '' '' END END END END ' +
   ' AS IDENTIFICACAO_BANCARIA ' +
   'from pagamento_cred pc '+
   'inner join credenciados cred ON cred.cred_id = pc.cred_id '+
   'inner join PAGA_CRED_POR on paga_cred_por.PAGA_CRED_POR_ID = pc.PAGA_CRED_POR_ID '+
   'inner join BANCOS on BANCOS.CODIGO = cred.BANCO ';
   sql := sql + 'where pc.LOTE_PAGAMENTO = '+txtCodLote.Text;
   sql := sql + ' AND CRED.BANCO = '+cmbBancosLotes.KeyValue;
   sql := sql + ' AND pc.cancelado <> ''S'' and repasse_autorizado = ''S'' ' ;
end;

procedure TfrmIdentificacaoBancariaRepasse.SQLDestroyString;
begin
  sql := '';
end;



procedure TfrmIdentificacaoBancariaRepasse.grdPgtoEstabDblClick(
  Sender: TObject);
begin
  inherited;
  BtnMarcDesm.Click;
end;

procedure TfrmIdentificacaoBancariaRepasse.BtnMarcDesmClick(
  Sender: TObject);
begin
  inherited;
  if MDPagtoEstab.IsEmpty then Exit;
    MDPagtoEstab.Edit;
    //MUDA O STATUS 'MARCADO' DA LINHA DO GRID PRINCIPAL
    MDPagtoEstabmarcado.AsBoolean := not MDPagtoEstabmarcado.AsBoolean;
    MDPagtoEstab.Post;
    Pgto_Sel;
end;


procedure TfrmIdentificacaoBancariaRepasse.Pgto_Sel;
var marca : TBookmark;
begin
  PgtoEstab_Sel := EmptyStr;
  marca := MDPagtoEstab.GetBookMark;
  MDPagtoEstab.DisableControls;
  MDPagtoEstab.First;
  while not MDPagtoEstab.eof do begin
    if MDPagtoEstabMarcado.AsBoolean then PgtoEstab_Sel := PgtoEstab_Sel + ','+MDPagtoEstabcred_id.AsString;
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  if PgtoEstab_Sel <> '' then PgtoEstab_Sel := Copy(PgtoEstab_Sel,2,Length(PgtoEstab_Sel));
  MDPagtoEstab.EnableControls;
end;


procedure TfrmIdentificacaoBancariaRepasse.SomarMarcados;
var
  marca : TBookmark;
  bruto, liqui, comis, desc : currency;
begin
  if MDPagtoEstab.Active and (not MDPagtoEstab.IsEmpty) then
  begin
    marca := MDPagtoEstab.GetBookmark;
    MDPagtoEstab.DisableControls;
    MDPagtoEstab.First;

    MDPagtoEstab.GotoBookmark(marca);
    MDPagtoEstab.FreeBookmark(marca);
    MDPagtoEstab.EnableControls;

  end;

end;


procedure TfrmIdentificacaoBancariaRepasse.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f11 : BtnMarcDesm.Click;
     vk_f8  : BtnMarcaTodos.Click;
     vk_f9  : BtnDesmTodos.Click;
  end;
end;

procedure TfrmIdentificacaoBancariaRepasse.BtnMarcaTodosClick(
  Sender: TObject);
  var marca                                                       : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  BtnDesmTodos.Click;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  //POSICIONA O CURSOR DO GRID PRINCIPAL NO PRIMEIRO REGISTRO
  MDPagtoEstab.First;
  //Percorrentdo o DataSet do Grid Principal
  while not MDPagtoEstab.Eof do
  begin
    //MARCA A LINHA NO GRID PRINCIPAL
    MDPagtoEstab.Edit;
    MDPagtoEstabmarcado.AsBoolean := True;
    MDPagtoEstab.Post;
    //fim do controle de pagamentos em aberto
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  MarcouTodos := true;
  DesmarcouTodos := false;
end;

procedure TfrmIdentificacaoBancariaRepasse.BtnDesmTodosClick(
  Sender: TObject);
  var marca                                                       : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  MDPagtoEstab.First;
  while not MDPagtoEstab.Eof do
  begin
    MDPagtoEstab.Edit;
    MDPagtoEstabmarcado.AsBoolean := False;
    MDPagtoEstab.Post;
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  DesmarcouTodos := true;
  MarcouTodos := False;
  AbrirPagamentos;
  Screen.Cursor := crDefault;;
end;

procedure TfrmIdentificacaoBancariaRepasse.btnGravarClick(Sender: TObject);
var RegAtual              : TBookmark;
begin
   inherited;
   if cbbPagamento.Text <> '' then
   begin
        if MsgSimNao('Confirma que o pagamento foi realizado atr�ves da Conta ' +  cbbPagamento.Text + '?') then
        begin
          RegAtual := MDPagtoEstab.GetBookmark;
          MDPagtoEstab.First;
          while not MDPagtoEstab.Eof do
          begin
            MDPagtoEstabcred_id.AsString;
            if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then
                DMConexao.ExecuteSql('UPDATE PAGAMENTO_CRED SET IDENTIFICACAO_BANCARIA = ''' + IntToStr(cbbPagamento.ItemIndex) + ''' WHERE CRED_ID =' + MDPagtoEstab.FieldByName('CRED_ID').AsString + ' AND PAGAMENTO_CRED_ID = ' + MDPagtoEstab.FieldByName('PAGAMENTO_CRED_ID').AsString + ' AND LOTE_PAGAMENTO = ' + txtCodLote.Text + '');
          MDPagtoEstab.Next;
          end;

          MDPagtoEstab.GotoBookmark(RegAtual);
          MsgInf('Atualiza��o realizada com Sucesso!');
          AbrirPagamentos;
       end;
   end
   else
      begin
          MsgInf('Necess�rio selecionar a Origem do Pagamento');
          cbbPagamento.SetFocus;
      end;
end;

procedure TfrmIdentificacaoBancariaRepasse.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFormIdenticaoBancaria := false;
end;

end.
