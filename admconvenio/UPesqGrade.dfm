object FPesqGrade: TFPesqGrade
  Left = 312
  Top = 203
  ActiveControl = CBCampos
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Localizar'
  ClientHeight = 184
  ClientWidth = 296
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 296
    Height = 184
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 2
      Width = 92
      Height = 13
      Caption = 'Selecione o Campo'
    end
    object Label2: TLabel
      Left = 8
      Top = 40
      Width = 58
      Height = 13
      Caption = 'Procurar por'
    end
    object Bevel1: TBevel
      Left = 0
      Top = 144
      Width = 368
      Height = 2
    end
    object Label3: TLabel
      Left = 8
      Top = 160
      Width = 43
      Height = 13
      Caption = 'F1 - Help'
    end
    object Button1: TBitBtn
      Left = 131
      Top = 150
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 6
      Visible = False
      OnClick = Button1Click
    end
    object EdLoca: TEdit
      Left = 7
      Top = 54
      Width = 274
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
    end
    object CBCampos: TComboBox
      Left = 8
      Top = 16
      Width = 273
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 131
      Top = 150
      Width = 75
      Height = 25
      Caption = '&Ok'
      TabOrder = 4
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 206
      Top = 150
      Width = 75
      Height = 25
      Caption = '&Fechar'
      TabOrder = 5
      OnClick = BitBtn2Click
    end
    object RGDirec: TRadioGroup
      Left = 145
      Top = 77
      Width = 135
      Height = 64
      Caption = 'Dire'#231#227'o da pesquisa'
      ItemIndex = 0
      Items.Strings = (
        'Para Baixo'
        'Para Cima')
      TabOrder = 3
    end
    object RGTipo: TRadioGroup
      Left = 3
      Top = 77
      Width = 140
      Height = 64
      Caption = 'Tipo da pesquisa'
      ItemIndex = 0
      Items.Strings = (
        'Iniciando com'
        'Contendo'
        'Exatamente igual')
      TabOrder = 2
    end
  end
end
