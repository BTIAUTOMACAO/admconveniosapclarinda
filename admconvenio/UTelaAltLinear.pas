unit UTelaAltLinear;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ToolEdit, Mask, CurrEdit, {JvLookup,}
  RxMemDS, JvExControls, JvDBLookup, JvExMask, JvToolEdit, JvExStdCtrls,
  JvEdit, JvValidateEdit, cartao_util;

type
  TFTelaAltLinear = class(TForm)
    Panel1: TPanel;
    Bevel1: TBevel;
    ButExec: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    texto1: TEdit;
    //valor1: TCurrencyEdit;
    //date1: TDateEdit;
    Label3: TLabel;
    EdHistorico: TEdit;
    ChkExpress: TCheckBox;
    DSTempCampos: TDataSource;
    DBCampos: TJvDBLookupCombo;
    valor1: TJvValidateEdit;
    date1: TJvDateEdit;
    TempCampos: TRxMemoryData;
    TempCamposCampo: TStringField;
    TempCamposTipo: TStringField;
    TempCamposFieldName: TStringField;
    TempCamposValor: TVariantField;
    TempCamposSize: TIntegerField;
    procedure Button2Click(Sender: TObject);
    procedure ButExecClick(Sender: TObject);
    procedure valor1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure texto1KeyPress(Sender: TObject; var Key: Char);
    procedure ChkExpressClick(Sender: TObject);
    procedure DBCamposChange(Sender: TObject);
    procedure DBCamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure date1KeyPress(Sender: TObject; var Key: Char);
    procedure EdHistoricoKeyPress(Sender: TObject; var Key: Char);
  private
    Nvalue : Variant;
    function GetNewValue:Variant;
    procedure SetNewValue(const Value: Variant);
    { Private declarations }
  public
    { Public declarations }
    dados : TDataSet;
    function GetValuesAsDateTime:TDateTime;
    function GetValuesAsString:String;
    function GetValuesAsInteger:Integer;
    function GetValuesAsFloat:Double;
    property NewValue: Variant read GetNewValue write SetNewValue;
  end;

var
  FTelaAltLinear: TFTelaAltLinear;
  
implementation

uses UExpression, DateUtils;

{$R *.dfm}

procedure TFTelaAltLinear.Button2Click(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

procedure TFTelaAltLinear.ButExecClick(Sender: TObject);
begin
 if Trim(EdHistorico.Text) = '' then begin
    ShowMessage('Hist�rico da atualiza��o obrigat�rio.');
    EdHistorico.SetFocus;
    Exit;
 end;
 if texto1.Visible then
    NewValue := QuotedStr( texto1.Text )
 else if date1.Visible then
   NewValue := FormatDataIB(date1.Date)
 else begin
   if Trunc(valor1.Value) <> valor1.Value then
      NewValue := FormatDimIB(valor1.Value)
   else
      NewValue := FloatToStr(valor1.Value);
 end;
 ModalResult := mrOk;
end;

procedure TFTelaAltLinear.valor1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltLinear.ComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltLinear.texto1KeyPress(Sender: TObject; var Key: Char);
begin

if TFieldType(TempCamposTipo.AsVariant) in [ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint] then begin
   if not (Key in ['0'..'9',#13,#8]) then key := #0;
end;

if key = #13 then
  EdHistorico.SetFocus;

end;

procedure TFTelaAltLinear.ChkExpressClick(Sender: TObject);
var marka : TBookmark;
begin
if ChkExpress.Checked then begin
   FExpression := TFExpression.Create(Self);
   marka := TempCampos.GetBookmark;
   TempCampos.DisableControls;
   TempCampos.First;
   while not TempCampos.eof do begin
      if IsNumericField(TFieldType(TempCamposTipo.AsVariant))  then begin
         FExpression.Campos.Items.Add(TempCamposCampo.AsString);
      end;
      TempCampos.Next
   end;
   TempCampos.GotoBookmark(marka);
   TempCampos.FreeBookmark(marka);
   TempCampos.EnableControls;
   FExpression.ShowModal;
   if FExpression.ModalResult = mrOk then begin
      texto1.Text := FExpression.Memo1.Text;
      EdHistorico.SetFocus;
   end else ChkExpress.Checked := False;
   FExpression.Free;
end;
texto1.Hide;
valor1.Hide;
if ((TFieldType(TempCamposTipo.AsVariant) in [ftFloat, ftCurrency, ftBCD, ftFMTBcd]) and (not ChkExpress.Checked)) then begin
   valor1.Visible := True;
end
else texto1.Visible := True;
end;

procedure TFTelaAltLinear.DBCamposChange(Sender: TObject);
begin
texto1.Visible     := False;
date1.Visible      := False;
valor1.Visible     := False;
ChkExpress.Checked := False;
ChkExpress.Visible := False;
if TFieldType(TempCamposTipo.AsVariant) in [ftString,ftMemo,ftFixedChar, ftWideString,
                                            ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint]  then begin
   texto1.Visible := True;
   texto1.MaxLength := TempCamposSize.AsInteger;
end;
if TFieldType(TempCamposTipo.AsVariant) in [ftFloat, ftCurrency, ftBCD,ftFMTBcd] then begin
   valor1.Visible := True;
end;
if TFieldType(TempCamposTipo.AsVariant) in [ftDate, ftDateTime] then begin
   date1.Visible := True;
end;
if TFieldType(TempCamposTipo.AsVariant) in [ftFloat, ftCurrency, ftBCD,ftFMTBcd, ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint] then begin
   ChkExpress.Visible := True;
end;

//if date1.Visible = true then
//  date1.SetFocus
//else
//  texto1.SetFocus;

end;

procedure TFTelaAltLinear.DBCamposKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if not DBCampos.IsDropDown then if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltLinear.FormCreate(Sender: TObject);
begin
  TempCampos.Open;
  date1.Date := Today
end;

function TFTelaAltLinear.GetNewValue: Variant;
begin
   Result := Nvalue;
end;

procedure TFTelaAltLinear.SetNewValue(const Value: Variant);
begin
   Nvalue := Value;
end;

function TFTelaAltLinear.GetValuesAsDateTime: TDateTime;
begin
   if IsDateTimeField(TFieldType(TempCamposTipo.AsVariant)) then
      Result := date1.Date
   else
      raise Exception.Create('Tipo do Campo n�o � Data.');
end;

function TFTelaAltLinear.GetValuesAsFloat: Double;
begin
   if IsFloatField(TFieldType(TempCamposTipo.AsVariant)) then
      Result := valor1.Value
   else
      raise Exception.Create('Tipo do Campo n�o � Num�rico decimal.');
end;

function TFTelaAltLinear.GetValuesAsInteger: Integer;
begin
   if IsIntegerField(TFieldType(TempCamposTipo.AsVariant)) then
      Result := Trunc(valor1.Value)
   else
      raise Exception.Create('Tipo do Campo n�o � Num�rico inteito.');
end;

function TFTelaAltLinear.GetValuesAsString: String;
begin
   if IsStringField(TFieldType(TempCamposTipo.AsVariant)) then
      Result := texto1.Text
   else
      raise Exception.Create('Tipo do Campo n�o � Texto.');  
end;

procedure TFTelaAltLinear.date1KeyPress(Sender: TObject; var Key: Char);
begin
    if key = #13 then
      EdHistorico.SetFocus;
end;

procedure TFTelaAltLinear.EdHistoricoKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key = #13 then
      ButExec.SetFocus;
end;

end.
