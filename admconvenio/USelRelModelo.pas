unit USelRelModelo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, Menus;

type
  TFSelRelModelo = class(TForm)
    DBGrid1: TDBGrid;
    PopupMenu1: TPopupMenu;
    AlterarDescriodoModelo1: TMenuItem;
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure AlterarDescriodoModelo1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSelRelModelo: TFSelRelModelo;

implementation

uses UNewGeraLista, DB, cartao_util;

{$R *.dfm}

procedure TFSelRelModelo.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then ModalResult := mrOk;
if key = vk_escape then ModalResult := mrCancel;
end;

procedure TFSelRelModelo.DBGrid1DblClick(Sender: TObject);
begin
ModalResult := mrOk;
end;

procedure TFSelRelModelo.AlterarDescriodoModelo1Click(Sender: TObject);
var ok : boolean;
S : string;
begin
S := FNewGeraLista.Query.FieldByName('DESCRICAO').AsString;
ok := InputQuery('Altera��o da descri��o do modelo.','Informe a nova descri��o',S);
if ok then
   if Trim(S) <> '' then begin
      FNewGeraLista.Query.Edit;
      FNewGeraLista.Query.FieldByName('DESCRICAO').AsString := S;
      FNewGeraLista.Query.Post;
   end else ShowMessage('Descri��o obrigat�ria.');

end;

end.
