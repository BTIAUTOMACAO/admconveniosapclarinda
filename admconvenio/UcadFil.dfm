object FCadFil: TFCadFil
  Left = 311
  Top = 224
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Cadastro de Filial'
  ClientHeight = 449
  ClientWidth = 920
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poMainFormCenter
  ShowHint = True
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    920
    449)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 24
    Width = 913
    Height = 425
    ActivePage = tsGradeFilial
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object tsGradeFilial: TTabSheet
      Caption = 'Em Grade'
      DesignSize = (
        905
        397)
      object Panel2: TPanel
        Left = 0
        Top = 325
        Width = 905
        Height = 72
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          905
          72)
        object Label14: TLabel
          Left = 1
          Top = 3
          Width = 33
          Height = 13
          Caption = '&C'#243'digo'
          FocusControl = EdCod
        end
        object Label38: TLabel
          Left = 70
          Top = 3
          Width = 28
          Height = 13
          Caption = 'Nome'
        end
        object ButBusca: TBitBtn
          Left = 267
          Top = 15
          Width = 81
          Height = 24
          Hint = 'Buscar cadastro'
          Caption = '&Buscar'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = ButBuscaClick
          Glyph.Data = {
            AE030000424DAE03000000000000AE0100002800000020000000100000000100
            08000000000000020000232E0000232E00005E00000000000000512600005157
            61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
            7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
            9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
            BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
            BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
            CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
            D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
            DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
            E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
            ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
            F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
            FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
            09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
            0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
            23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
            065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
            5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
            5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
            5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
            5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
            5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
            5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
            5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
            5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
            5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
            5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
            5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
            5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
          NumGlyphs = 2
        end
        object ButAtualiza: TBitBtn
          Left = 919
          Top = 45
          Width = 81
          Height = 25
          Hint = 'Atualiza a visualiza'#231#227'o dos dados'
          Anchors = [akTop, akRight]
          Caption = '&Atualizar'
          TabOrder = 2
          Visible = False
          Glyph.Data = {
            B2030000424DB203000000000000B20100002800000020000000100000000100
            08000000000000020000232E0000232E00005F000000000000000D7D0D001785
            1700278F27003B9D3B00469F46004A9D4A004FAB4F0052AC52005BAC5B0064B9
            640069BC69006DB96D0076C576007FC47F007AC87A0086BD860086BE8600B7B8
            B900BABBBC0086C0860080CD80008DCF8D008AD48A008CD58C0092C6920090C8
            900097DC97009DE09D00B1E3B100B3E0B300B4EEB400BBE3BB00B7F0B700BCF3
            BC00BFC0C100C6C7C800C9CACA00CDCECE00CECFCF00C2DFC200D0D0D100D5D5
            D600D6D6D700DADADB00DBDBDC00DCDCDC00DEDFDF00DFDFE000C4E0C400C6E7
            C600C5EAC500C9E3C900CCE4CC00CBEECB00C7FAC700C9FBC900D4ECD400D6ED
            D600D3FFD300DEF1DE00E1E1E100E2E2E200E6E6E600E8E8E800EBEBEB00ECEC
            EC00EEEEEE00EEEFEF00EFEFF000E1F4E100E4F6E400E5F6E500E2FFE200ECF5
            EC00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F4F5F500F5F5F500F6F6
            F600F7F7F700F0F8F000F2FBF200F1FFF100F4FFF400F8F8F800F9F9F900FAFA
            FA00FBFBFB00F8FCF800FDFDFD00FEFEFE00FFFFFF005E5E5E5E5E5E5E5E1010
            5E5E5E5E5E5E5E5E5E5E5E5E5E5E2D2D5E5E5E5E5E5E5E5E5E5E5E5E5E5E010C
            015E5E5E5E5E5E5E5E5E5E5E5E5E122B125E5E5E5E5E5E5E491304020202021A
            0E025E5E5E5E5E5E592D24222222223E2D225E5E5E5E5E34080E1A2020202020
            201A035E5E5E5E4A282D3E4A4A4A4A4A4A3E235E5E5E5B0B21565E5E5E5E5E48
            485E065E5E5E5D2A4C5D5E5E5E5E5E5A5A5E255E5E5E381E5E3617090909095E
            5E095E5E5E5E4E435E503D292929295E5E295E5E5E5E31550E1F3B5E5E5E0E5E
            0E5E5E5E270F4A5D2D42525E5E5E2D5E2D5E5E5E422C3216455E5E5E5E5E1616
            5E5E5E5E05004B3D575E5E5E5E5E3D3D5E5E5E5E2411351C5E5E5E5E01015E5E
            5E5E5E3001014D415E5E5E5E12125E5E5E5E5E42121247465E5E5E020E025E5E
            5E3318020E0458585E5E5E222D225E5E5E442F222D245E5E5E5E03141A030303
            03070A1A20195E5E5E5E232E3E23232323262A3E4A2F5E5E5E061B2020202020
            2020375E0B535E5E5E253F4A4A4A4A4A4A4A515E2A5A5E5E5E095E48485E5E5E
            5E5E3A0D395E5E5E5E295E5A5A5E5E5E5E5E582D4F5E5E5E5E5E0E5E5E0E0E0E
            0E151D545E5E5E5E5E5E2D5E5E2D2D2D2D3C405C5E5E5E5E5E5E5E165E165E5E
            5E5E5E5E5E5E5E5E5E5E5E3D5E3D5E5E5E5E5E5E5E5E5E5E5E5E5E5E1A1A5E5E
            5E5E5E5E5E5E5E5E5E5E5E5E3E3E5E5E5E5E5E5E5E5E}
          NumGlyphs = 2
        end
        object EdCod: TEdit
          Left = 1
          Top = 18
          Width = 64
          Height = 21
          Hint = 'Busca por c'#243'digo'
          TabOrder = 1
        end
        object EdNome: TEdit
          Left = 70
          Top = 18
          Width = 179
          Height = 21
          Hint = 'Busca pelo nome fantasia'
          CharCase = ecUpperCase
          TabOrder = 3
        end
      end
      object jvdbgrd1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 729
        Height = 321
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = DSFilial
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = jvdbgrd1CellClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'FILIAL_ID'
            Width = 341
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Width = 600
            Visible = True
          end>
      end
    end
    object tsCadastroFilial: TTabSheet
      Caption = 'Em Ficha'
      ImageIndex = 1
      object grp1: TGroupBox
        Left = 0
        Top = 16
        Width = 1345
        Height = 161
        Caption = 'Dados Cadastrais'
        TabOrder = 0
        object Label3: TLabel
          Left = 120
          Top = 15
          Width = 66
          Height = 13
          Caption = 'Nome da Filial'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label1: TLabel
          Left = 504
          Top = 15
          Width = 96
          Height = 13
          Caption = 'C'#243'digo do Conv'#234'nio'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label2: TLabel
          Left = 8
          Top = 55
          Width = 82
          Height = 13
          Caption = 'Tipo da Inscri'#231#227'o'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label4: TLabel
          Left = 104
          Top = 55
          Width = 98
          Height = 13
          Caption = 'N'#250'mero da Inscri'#231#227'o'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label5: TLabel
          Left = 400
          Top = 55
          Width = 39
          Height = 13
          Caption = 'Ag'#234'ncia'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label6: TLabel
          Left = 528
          Top = 55
          Width = 71
          Height = 13
          Caption = 'Conta Corrente'
          FocusControl = dbedtFILIAL_NOME
        end
        object Label13: TLabel
          Left = 8
          Top = 15
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = dbedtFILIAL_ID
        end
        object lbl1: TLabel
          Left = 744
          Top = 55
          Width = 147
          Height = 13
          Caption = 'N'#250'mero Sequencial do Arquivo'
          FocusControl = dbedtFILIAL_NOME
        end
        object dbedtFILIAL_NOME: TDBEdit
          Left = 120
          Top = 32
          Width = 377
          Height = 21
          DataField = 'FILIAL_NOME'
          DataSource = DSCadastro
          TabOrder = 0
        end
        object dbedtCODIGO_CONVENIO: TDBEdit
          Left = 504
          Top = 32
          Width = 329
          Height = 21
          DataField = 'CODIGO_CONVENIO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object dbedtTIPO_INSCRICAO: TDBEdit
          Left = 8
          Top = 72
          Width = 41
          Height = 21
          DataField = 'TIPO_INSCRICAO'
          DataSource = DSCadastro
          TabOrder = 2
        end
        object dbedtCONTACORRENTE: TDBEdit
          Left = 528
          Top = 72
          Width = 201
          Height = 21
          DataField = 'CONTACORRENTE'
          DataSource = DSCadastro
          TabOrder = 3
        end
        object dbedtNUMERO_INSCRICAO: TDBEdit
          Left = 104
          Top = 72
          Width = 281
          Height = 21
          DataField = 'NUMERO_INSCRICAO'
          DataSource = DSCadastro
          TabOrder = 4
        end
        object dbedtAGENCIA: TDBEdit
          Left = 400
          Top = 72
          Width = 121
          Height = 21
          DataField = 'AGENCIA'
          DataSource = DSCadastro
          TabOrder = 5
        end
        object dbedtFILIAL_ID: TDBEdit
          Left = 8
          Top = 30
          Width = 81
          Height = 21
          Hint = 'C'#243'digo do fornecedor'
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          DataField = 'FILIAL_ID'
          DataSource = DSCadastro
          PopupMenu = PopupAlteraID
          ReadOnly = True
          TabOrder = 6
        end
        object dbedtSEQUENCIA_ARQUIVO: TDBEdit
          Left = 744
          Top = 72
          Width = 121
          Height = 21
          DataField = 'SEQUENCIA_ARQUIVO'
          DataSource = DSCadastro
          TabOrder = 7
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 176
        Width = 1345
        Height = 385
        Caption = 'Endere'#231'o'
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 15
          Width = 46
          Height = 13
          Caption = 'Endere'#231'o'
          FocusControl = dbedtENDERECO
        end
        object Label8: TLabel
          Left = 392
          Top = 15
          Width = 37
          Height = 13
          Caption = 'N'#250'mero'
          FocusControl = dbedtENDERECO
        end
        object Label9: TLabel
          Left = 504
          Top = 15
          Width = 64
          Height = 13
          Caption = 'Complemento'
          FocusControl = dbedtENDERECO
        end
        object Label10: TLabel
          Left = 8
          Top = 55
          Width = 21
          Height = 13
          Caption = 'CEP'
          FocusControl = dbedtENDERECO
        end
        object Label11: TLabel
          Left = 152
          Top = 55
          Width = 33
          Height = 13
          Caption = 'Cidade'
          FocusControl = dbedtENDERECO
        end
        object Label12: TLabel
          Left = 536
          Top = 55
          Width = 33
          Height = 13
          Caption = 'Estado'
          FocusControl = dbedtENDERECO
        end
        object dbedtENDERECO: TDBEdit
          Left = 8
          Top = 32
          Width = 377
          Height = 21
          DataField = 'ENDERECO'
          DataSource = DSCadastro
          TabOrder = 0
        end
        object dbedtENDERECO1: TDBEdit
          Left = 392
          Top = 32
          Width = 105
          Height = 21
          DataField = 'ENDERECO_NUMERO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object dbedtCIDADE: TDBEdit
          Left = 152
          Top = 72
          Width = 377
          Height = 21
          DataField = 'CIDADE'
          DataSource = DSCadastro
          TabOrder = 2
        end
        object dbedtCEP: TDBEdit
          Left = 8
          Top = 72
          Width = 137
          Height = 21
          DataField = 'CEP'
          DataSource = DSCadastro
          TabOrder = 3
        end
        object dbedtCOMPLEMENTO: TDBEdit
          Left = 504
          Top = 32
          Width = 257
          Height = 21
          DataField = 'COMPLEMENTO'
          DataSource = DSCadastro
          TabOrder = 4
        end
        object dbedtESTADO: TDBEdit
          Left = 536
          Top = 72
          Width = 89
          Height = 21
          DataField = 'ESTADO'
          DataSource = DSCadastro
          TabOrder = 5
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 365
        Width = 905
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object BitBtn1: TBitBtn
          Left = 2
          Top = 4
          Width = 85
          Height = 25
          Hint = 'Editar registro'
          Caption = 'Editar (F2)'
          TabOrder = 0
          Glyph.Data = {
            E6020000424DE602000000000000E60000002800000020000000100000000100
            08000000000000020000232E0000232E00002C00000000000000323232000054
            0000116E110065656500323298003232CC003265CC004C7FE5006565DD006565
            FF0021872100329832004CB24C0065CC65006598FF008787870098989800A9A9
            A900A8AAAA00B2B3B400B5B6B700BABABA00BBBCBD00BCBDBE0098BEFF00BFBF
            C000C3C4C400C9CACA00CCCCCC00CDCECE00CECFCF00D2D3D300D4D5D500D8D9
            D900DCDCDC00DDDDDD00DFDFE000CCE5FF00E2E3E300EAEAEB00EBEBEB00F2F2
            F200F7F7F700FFFFFF002B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
            2B2B2B2B2B2B2B2B2B2B2B000004062B2B2B2B2B2B2B2B2B2B2B2B1212141B2B
            2B2B2B2B2B2B2B2B2B2B2B00180E0E012B2B2B2B2B2B2B2B2B2B2B1228242412
            2B2B2B2B2B2B2B2B2B2B2B072518010A012B2B2B2B2B2B2B2B2B2B202A281216
            122B2B2B2B2B2B2B2B2B2B0E25020B0A0A012B2B2B2B2B2B2B2B2B242A131A16
            16122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E1A
            1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E
            1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B1621
            1E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16
            211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B
            16211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A032B2B2B2B2B2B2B2B
            2B16211E1A1616192B2B2B2B2B2B2B2B2B2B0A0D0C0B0F15032B2B2B2B2B2B2B
            2B2B16211E1A1D26192B2B2B2B2B2B2B2B2B2B0A0D111C0F052B2B2B2B2B2B2B
            2B2B2B162122271D172B2B2B2B2B2B2B2B2B2B2B10231108052B2B2B2B2B2B2B
            2B2B2B2B2029221E172B2B2B2B2B2B2B2B2B2B2B2B1009092B2B2B2B2B2B2B2B
            2B2B2B2B2B201F1F2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
            2B2B2B2B2B2B2B2B2B2B}
          NumGlyphs = 2
        end
        object BitBtn2: TBitBtn
          Left = 87
          Top = 4
          Width = 85
          Height = 25
          Hint = 'Incluir registro'
          Caption = 'Incluir (F5)'
          TabOrder = 1
          OnClick = BitBtn2Click
          Glyph.Data = {
            6A030000424D6A030000000000006A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004D00000000000000CC986500CE9A
            6700D19D6A00D4A06D00D7A37000DBA77400DFAB7800E3AF7C00E6B27F00E8B4
            8100ECB88500F0BC8900F4C08D00F7C39000FAC79300FDCA9600FFCC9800E8CF
            B600EBD4BD00FFD0A000FFD6AB00FFD9B200FFDCB800CFCFD000D0D0D100D1D2
            D200D2D3D300D4D5D500D6D6D700D7D8D800D9DADA00DADADB00DBDBDC00DCDD
            DD00DEDFDF00ECD9C600EED9C500F1DECB00FFE2C300FFE6CC00F3E1D000FFEE
            DD00FFEFDF00E0E0E100E2E2E200E3E4E400E4E5E500E5E5E600E8E8E800E9EA
            EA00EAEAEB00EBEBEB00ECECEC00EEEEEE00FFF0E100FFF1E300FFF3E600FFF4
            E900FFF6EC00F0F0F000F2F2F200F3F3F300F7F7F700FFF7F000FFF9F300FFFA
            F600F8F8F800F9F9F900FAFAFA00FBFBFB00FFFCF900FFFDFB00FCFCFC00FDFD
            FD00FFFEFD00FEFEFE00FFFFFF004C4C2311122425282825241211234C4C4C4C
            353133353B3C3C3B353331354C4C4C4C0129292929292929292929014C4C4C4C
            183E3E3E3E3E3E3E3E3E3E184C4C4C4C022A2A2A2A2A2A2A2A2A2A024C4C4C4C
            193E3E3E3E3E3E3E3E3E3E194C4C4C4C0336363636363636363636034C4C4C4C
            1A424242424242424242421A4C4C4C4C0437373737373737373737044C4C4C4C
            1B434343434343434343431B4C4C4C4C0538383838383838383838054C4C4C4C
            1C444444444444444444441C4C4C4C4C0639393939393939393939064C4C4C4C
            1D444444444444444444441D4C4C4C4C073A3A3A3A3A3A3A3A3A3A074C4C4C4C
            1E444444444444444444441E4C4C4C4C093F3F3F3F3F3F3F3F3F3F094C4C4C4C
            2045454545454545454545204C4C4C4C0A404040404040404040400A4C4C4C4C
            2148484848484848484848214C4C4C4C0B414141414141414141410B4C4C4C4C
            2249494949494949494949224C4C4C4C0C464646464646464646460C4C4C4C4C
            2B4B4B4B4B4B4B4B4B4B4B2B4C4C4C4C0D47474747474747080000004C4C4C4C
            2C4C4C4C4C4C4C4C1F1717174C4C4C4C0E4A4A4A4A4A4A4A104C004C4C4C4C4C
            2D4C4C4C4C4C4C4C2F4C174C4C4C4C4C0F4C4C4C4C4C4C4C10004C4C4C4C4C4C
            2E4C4C4C4C4C4C4C2F174C4C4C4C4C4C1513141626272726104C4C4C4C4C4C4C
            343032353B3D3D3B2F4C4C4C4C4C}
          NumGlyphs = 2
        end
        object BitBtn3: TBitBtn
          Left = 172
          Top = 4
          Width = 85
          Height = 25
          Hint = 'Apagar registro'
          Caption = 'Apagar (F6)'
          TabOrder = 2
          OnClick = BitBtn3Click
          Glyph.Data = {
            82030000424D8203000000000000820100002800000020000000100000000100
            08000000000000020000232E0000232E000053000000000000000021CC000324
            CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
            F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
            FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
            FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
            FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
            CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
            D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
            F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
            FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
            E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
            F400FFFFFF005252525252525252525252525252525252525252525252525252
            525252525252525216002552525252525225001652525252341A4A5252525252
            524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
            4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
            1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
            2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
            30471E4C52525252523B05211212121221053B5252525252524C244731313131
            47244C525252525252523C0617131317063C52525252525252524D2636323236
            264D52525252525252523D0714141414073D52525252525252524E2733333333
            274E525252525252523E08151521211515083E5252525252524F283535474735
            35284F52525252523F091818210909211818093F525252524F29434347292947
            4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
            4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
            2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
            512E472E4852525223104452525252525244102352525252492F515252525252
            52512F4952525252525252525252525252525252525252525252525252525252
            525252525252}
          NumGlyphs = 2
          Spacing = 2
        end
        object BitBtn4: TBitBtn
          Left = 639
          Top = 4
          Width = 85
          Height = 25
          Hint = 'Gravar registro'
          Caption = '&Gravar'
          TabOrder = 3
          OnClick = BitBtn4Click
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object BitBtn5: TBitBtn
          Left = 724
          Top = 4
          Width = 85
          Height = 25
          Hint = 'Cancelar altera'#231#227'o'
          Caption = '&Cancelar'
          TabOrder = 4
          OnClick = BitBtn5Click
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
      end
    end
  end
  object PanStatus: TPanel
    Left = 0
    Top = 0
    Width = 920
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Locked = True
    ParentFont = False
    TabOrder = 1
    object panTitulo: TPanel
      Left = 0
      Top = 1
      Width = 1361
      Height = 20
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '  Cadastro de Dados da Filial para Arquivo SICREDI'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object ButClose: TSpeedButton
        Left = 1340
        Top = 1
        Width = 20
        Height = 18
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = ButCloseClick
      end
    end
  end
  object DSCadastro: TDataSource
    DataSet = QCadastro
    Left = 604
    Top = 368
  end
  object PopupBut: TPopupMenu
    Left = 584
    Top = 417
    object Restaurar1: TMenuItem
      Caption = 'Restaurar'
      Enabled = False
    end
    object Minimizar1: TMenuItem
      Caption = 'Minimizar'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Fechar1: TMenuItem
      Caption = 'Fechar'
      ShortCut = 32883
    end
  end
  object PopupGrid1: TPopupMenu
    Left = 524
    Top = 417
  end
  object PopupCC: TPopupMenu
    Left = 453
    Top = 464
    object MenuItem1: TMenuItem
      Caption = 'Configura'#231#227'o da Grade'
    end
    object Exportarparaoexcel2: TMenuItem
      Caption = 'Exportar para o excel'
    end
  end
  object PopupDesconto: TPopupMenu
    Left = 380
    Top = 464
    object MenuItem2: TMenuItem
      Caption = 'Configura'#231#227'o da Grade'
    end
    object MenuItem3: TMenuItem
      Caption = 'Exportar para o excel'
    end
  end
  object PopCodAcesso: TPopupMenu
    Left = 524
    Top = 465
    object ColocarCodAcessoManual1: TMenuItem
      Caption = 'Alterar C'#243'd Acesso'
    end
  end
  object PopupAlteraID: TPopupMenu
    Left = 384
    Top = 417
    object AlterarIddoFornecedor1: TMenuItem
      Caption = 'Alterar Id do Estabelecimento'
    end
    object DuplicarEstabelecimentoalterandoID1: TMenuItem
      Caption = 'Duplicar Estabelecimento alterando ID'
      Visible = False
    end
  end
  object PopupComissaoEmpr: TPopupMenu
    Left = 452
    Top = 417
    object AltLineardeComissoporEmpresa1: TMenuItem
      Caption = 'Alt. Linear de Comiss'#227'o por Empresa'
      Visible = False
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 588
    Top = 465
    object este1: TMenuItem
      Caption = 'Alt. Linear de Comiss'#227'o por Empresa'
    end
  end
  object DSEstados: TDataSource
    DataSet = QEstados
    Left = 593
    Top = 533
  end
  object QEstados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT ESTADO_ID, UF, ICMS_ESTADUAL FROM ESTADOS'
      'ORDER BY UF')
    Left = 596
    Top = 499
    object QEstadosESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object QEstadosUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object QEstadosICMS_ESTADUAL: TFloatField
      FieldName = 'ICMS_ESTADUAL'
    end
  end
  object QCadastro: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT cc.*, UPPER(ba.descricao) as NOMEBAIRRO, UPPER(ci.nome) a' +
        's NOMECIDADE, '
      'es.UF as NOMEESTADO FROM credenciados cc inner join '
      'bairros ba on (ba.bairro_id = cc.bairro) inner join cidades ci'
      
        'on (ci.cid_id = cc.cidade) inner join estados es on (es.estado_i' +
        'd = cc.estado) WHERE cred_id = 0')
    Left = 604
    Top = 337
    object QCadastroFILIAL_ID: TIntegerField
      FieldName = 'FILIAL_ID'
    end
    object QCadastroBANCO_ID: TIntegerField
      FieldName = 'BANCO_ID'
    end
    object QCadastroFILIAL_NOME: TStringField
      FieldName = 'FILIAL_NOME'
      Size = 30
    end
    object QCadastroCODIGO_CONVENIO: TStringField
      FieldName = 'CODIGO_CONVENIO'
    end
    object QCadastroENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 30
    end
    object QCadastroCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 15
    end
    object QCadastroCIDADE: TStringField
      FieldName = 'CIDADE'
    end
    object QCadastroESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object QCadastroTIPO_INSCRICAO: TIntegerField
      FieldName = 'TIPO_INSCRICAO'
    end
    object QCadastroNUMERO_INSCRICAO: TStringField
      FieldName = 'NUMERO_INSCRICAO'
      Size = 14
    end
    object QCadastroAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 6
    end
    object QCadastroCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 13
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QCadastroENDERECO_NUMERO: TStringField
      FieldName = 'ENDERECO_NUMERO'
      Size = 5
    end
    object ADORegraQCadastroSEQUENCIA_ARQUIVO: TIntegerField
      FieldName = 'SEQUENCIA_ARQUIVO'
    end
  end
  object QOcorrencias: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM credenciados_atendimento where atendimento_id = 0')
    Left = 448
    Top = 352
    object QOcorrenciasatendimento_id: TIntegerField
      FieldName = 'atendimento_id'
    end
    object QOcorrenciasnome_solicitante: TStringField
      FieldName = 'nome_solicitante'
      Size = 100
    end
    object QOcorrenciastel_solictante: TStringField
      FieldName = 'tel_solictante'
      Size = 30
    end
    object QOcorrenciasmotivo: TStringField
      FieldName = 'motivo'
      Size = 50
    end
    object QOcorrenciasdesc_atendimento: TStringField
      FieldName = 'desc_atendimento'
      Size = 2000
    end
    object QOcorrenciasdata_atendimento: TDateTimeField
      FieldName = 'data_atendimento'
    end
    object QOcorrenciascred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QOcorrenciasoperador: TStringField
      FieldName = 'operador'
      Size = 100
    end
    object QOcorrenciasSTATUS_ID: TIntegerField
      FieldName = 'STATUS_ID'
    end
  end
  object DSOcorrencias: TDataSource
    DataSet = QOcorrencias
    Left = 480
    Top = 352
  end
  object PopupPOS: TPopupMenu
    Left = 352
    Top = 225
    object MenuItem4: TMenuItem
      Caption = 'Transferir POS para outro estabelecimento'
    end
    object MenuItem5: TMenuItem
      Visible = False
    end
  end
  object DSFilial: TDataSource
    DataSet = QFilial
    Left = 652
    Top = 344
  end
  object QFilial: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT cc.*, UPPER(ba.descricao) as NOMEBAIRRO, UPPER(ci.nome) a' +
        's NOMECIDADE, '
      'es.UF as NOMEESTADO FROM credenciados cc inner join '
      'bairros ba on (ba.bairro_id = cc.bairro) inner join cidades ci'
      
        'on (ci.cid_id = cc.cidade) inner join estados es on (es.estado_i' +
        'd = cc.estado) WHERE cred_id = 0')
    Left = 684
    Top = 345
    object QFilialFILIAL_ID: TIntegerField
      FieldName = 'FILIAL_ID'
    end
    object QFilialDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
    end
  end
end
