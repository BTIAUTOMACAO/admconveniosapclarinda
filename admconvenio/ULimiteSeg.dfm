object FLimiteSeg: TFLimiteSeg
  Left = 427
  Top = 281
  Width = 402
  Height = 195
  Caption = 'Altera'#231#227'o Linear por Segmento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 132
    Height = 13
    Caption = 'Selecione o Segmento:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 108
    Top = 72
    Width = 176
    Height = 13
    Caption = 'Limite para esse segmento. (%)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 116
    Width = 386
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btnGravar: TJvSpeedButton
      Left = 52
      Top = 8
      Width = 137
      Height = 25
      Caption = '&Gravar'
      Flat = True
      ModalResult = 1
    end
    object btnCancelar: TJvSpeedButton
      Left = 204
      Top = 8
      Width = 137
      Height = 25
      Caption = '&Cancelar'
      Flat = True
      ModalResult = 2
    end
  end
  object Segmento: TJvDBLookupCombo
    Left = 16
    Top = 32
    Width = 361
    Height = 21
    LookupField = 'SEG_ID'
    LookupDisplay = 'DESCRICAO'
    TabOrder = 1
    OnExit = SegmentoExit
  end
  object edvalor: TJvValidateEdit
    Left = 106
    Top = 89
    Width = 183
    Height = 21
    CriticalPoints.MaxValueIncluded = False
    CriticalPoints.MinValueIncluded = False
    DisplayFormat = dfFloat
    DecimalPlaces = 2
    EditText = '100,00'
    MaxValue = 100.000000000000000000
    TabOrder = 2
  end
  object DSSeg: TDataSource
    Left = 328
    Top = 64
  end
  object Qseg: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select seg_id, descricao from segmentos'
      'where apagado <> '#39'S'#39
      'order by descricao')
    Left = 24
    Top = 72
    object QsegSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object Qsegdescricao: TStringField
      FieldName = 'descricao'
      Size = 58
    end
  end
end
