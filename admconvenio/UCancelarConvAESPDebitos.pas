unit UCancelarConvAESPDebitos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, ComCtrls, Menus, Buttons, ExtCtrls, DB, ADODB, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, StdCtrls,math,StrUtils, Mask, DBCtrls;

type
  TfrmCancelarConvAESPDebitos = class(TF1)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Panel2: TPanel;
    QConvBemEstar: TADOQuery;
    DSConvBemEstar: TDataSource;
    QConvBemEstartitular: TStringField;
    QConvBemEstarconv_id: TIntegerField;
    QConvBemEstarempres_id: TIntegerField;
    QConvBemEstarliberado: TStringField;
    QConvBemEstarvalor: TBCDField;
    txtCodEmpresa: TEdit;
    cbCancelarAutor: TCheckBox;
    Label2: TLabel;
    QTransacao: TADOQuery;
    txtConvID: TEdit;
    Label3: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure QConvBemEstarAfterPost(DataSet: TDataSet);
    procedure txtCodEmpresaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
  private
    function BuscaConvDescontaTaxa(convID: integer):boolean;
  public
    { Public declarations }
    oldValue : String;
    newValue : String;
    valor : string;
  end;

var
  frmCancelarConvAESPDebitos: TfrmCancelarConvAESPDebitos;

implementation

uses cartao_util, URotinasTexto, URotinasGrids, DM, UMenu;

{$R *.dfm}

procedure TfrmCancelarConvAESPDebitos.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if (txtCodEmpresa.Text = '') and (txtConvID.Text = '') then
  begin
    MsgInf('Digite um c�digo para pesquisa!');
    txtCodEmpresa.SetFocus;
    Exit;
  end;
  QConvBemEstar.Close;
  QConvBemEstar.SQL.Clear;
  QConvBemEstar.SQL.Add('select conv.titular, cbe.conv_id, cbe.empres_id, coalesce(cbe.liberado,''N'') liberado, ');
  QConvBemEstar.SQL.Add('cbe.valor valor ');
  QConvBemEstar.SQL.Add('from CONV_AESP_ODONTO cbe ');
  QConvBemEstar.SQL.Add('INNER JOIN conveniados CONV on conv.CONV_ID = cbe.CONV_ID ');
  QConvBemEstar.SQL.Add('where 0 = 0  ');
  if txtCodEmpresa.Text <> '' then
  begin
    QConvBemEstar.SQL.Add(' and cbe.empres_id = '+txtCodEmpresa.Text);
  end;
  if txtConvID.Text <> '' then
  begin
    QConvBemEstar.SQL.Add(' and (cbe.conv_id = '+txtConvID.Text+' or conv.chapa = '+txtConvID.Text+')') ;
  end;
  QConvBemEstar.SQL.Add(' order by conv.titular');
  //QConvBemEstar.Parameters[0].Value := txtCodEmpresa.Text;
  QConvBemEstar.Open;
  if QConvBemEstar.IsEmpty then
    MsgInf('N�o foi encontrado nenhum registro');

  txtCodEmpresa.Clear;
  txtConvID.Clear;
  txtCodEmpresa.SetFocus;

end;

procedure TfrmCancelarConvAESPDebitos.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Check: Integer;
  R: TRect;
begin
  inherited;
  if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
    Exit;

  // Desenha um checkbox no dbgrid
  if Column.FieldName = 'liberado' then
  begin
    TDBGrid(Sender).Canvas.FillRect(Rect);
 
    if ((Sender as TDBGrid).DataSource.Dataset.FieldByName('liberado').AsString = 'S') then
      Check := DFCS_CHECKED
    else
      Check := 0;

    R := Rect;
    InflateRect(R, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(TDBGrid(Sender).Canvas.Handle, R, DFC_BUTTON,
      DFCS_BUTTONCHECK or Check);
  end;

end;

procedure TfrmCancelarConvAESPDebitos.JvDBGrid1DblClick(Sender: TObject);
VAR A,valorColuna : String;
var coluna : Integer;
begin
  if ((Sender as TDBGrid).DataSource.DataSet.IsEmpty) then
    Exit;

  coluna := JvDBGrid1.SelectedIndex;
  valorColuna := JvDBGrid1.SelectedField.AsString;

  //verifica se a coluna do clique foi a coluna "LIBERADO"
  valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',JvDBGrid1.Columns.Items[4].Field.AsCurrency));
  //COMENTADO APENAS PARA LIBERAR VERS�O PARA EDILMA
  //  if((valor = '0.0') or (JvDBGrid1.Columns.Items[4].Field.AsCurrency = 0)) then
//  begin
//    MsgInf('O valor de desconto n�o pode estar zerado!');
//    Exit;
//  end;
  if(coluna = 3)then
  begin
    oldValue := QConvBemEstar.Fields.FieldByName('liberado').AsString;
    QConvBemEstar.Edit;
    QConvBemEstar.Fields.FieldByName('liberado').AsString :=
    ifThen(QConvBemEstar.Fields.FieldByName('liberado').AsString = 'N','S','N');
    newValue := QConvBemEstar.Fields.FieldByName('liberado').AsString;
    valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',JvDBGrid1.Columns.Items[4].Field.AsCurrency));
    QConvBemEstar.Post;
  end;


end;

procedure TfrmCancelarConvAESPDebitos.QConvBemEstarAfterPost(
  DataSet: TDataSet);
  var sql, sqlLog: String;
  cadastro, detalhe,dataFechaEmp,sqlQuery: string;
  qtdDiasParaFechamento : Integer;
  reg_atual : TBookmark;
begin
  inherited;
if ((newValue = 'N') or (newValue = 'n')) then
begin
    if BuscaConvDescontaTaxa(QConvBemEstarconv_id.AsInteger) then
    begin
      sql := 'DELETE FROM CONV_AESP_ODONTO  WHERE CONV_ID = '+QConvBemEstarconv_id.AsString
    end;
  if sql <> '' then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sql;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;
    sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
             ' OPERACAO, DATA_HORA, ID) values ('+
             ' next value for slog_id, ''FCancelarConAespOdonto'', ''Cobranca'', ''S'',''N'' , '+QuotedStr(Operador.Nome)+
             ', ''AESP ODONTO'', current_timestamp, '+QConvBemEstarconv_id.AsString+')';
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sqlQuery;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;

    //1� VERIRIFICAR SE FALTA MAIS DO QUE 7 DIAS PARA O FECHAMENTO DA EMPRESA
    qtdDiasParaFechamento := DMConexao.ExecuteScalar('SELECT DATEDIFF(dd,GETDATE(),(SELECT TOP(1) DATA_FECHA FROM DIA_FECHA WHERE EMPRES_ID = '+QConvBemEstarempres_id.AsString+' AND DATA_FECHA > GETDATE()))');

    //2� ALTERAR PARAMETROS TRANSACAO(OPERADORCANCELADO DTCANCELADO), E CONTACORRENTE (CANCELADO = 'S')
    if cbCancelarAutor.Checked then
    begin
      QTransacao.Close;
      QTransacao.SQL.Clear;
      QTransacao.SQL.Add('SELECT AUTORIZACAO_ID,DEBITO,DATA,TRANS_ID,DATA_FECHA_EMP FROM CONTACORRENTE WHERE CONV_ID = '+QConvBemEstarconv_id.AsString + ' AND CRED_ID = 4955 AND CANCELADA = ''N'' and DATA_FECHA_EMP > GETDATE()');
      QTransacao.Open;
      while(not QTransacao.Eof) do
      begin
        //ALTERAR PARAMETROS CANCELADO TRANSACAO
        DMConexao.ExecuteNonQuery('UPDATE TRANSACOES SET OPERCANCELADO = '+QuotedStr(Operador.Nome) + ',CANCELADO = ''S'', DTCANCELADO =  '+QuotedStr(DateToStr(now))+ ' WHERE TRANS_ID = '+QTransacao.Fields[3].AsString);
        DMConexao.ExecuteNonQuery('UPDATE CONTACORRENTE SET VALOR_CANCELADO = '+ valor + ', CREDITO = '+valor+'  ,CANCELADA = ''S'' WHERE TRANS_ID = '+QTransacao.Fields[3].AsString);

        dataFechaEmp := DMConexao.ExecuteScalar('SELECT TOP(1) DATA_FECHA FROM DIA_FECHA WHERE EMPRES_ID = '+QConvBemEstarempres_id.AsString + ' and data_fecha > getdate()');
        if dataFechaEmp = QTransacao.Fields[4].AsString then
          DMConexao.ExecuteNonQuery('UPDATE conveniados SET consumo_mes_1 = consumo_mes_1 - '+ valor + 'WHERE CONV_ID = '+QConvBemEstarconv_id.AsString);

        QTransacao.Next;
      end;
    end;

  end;


  QConvBemEstar.DisableControls;
  reg_atual := QConvBemEstar.GetBookmark;
  QConvBemEstar.Requery();
  if not QConvBemEstar.IsEmpty then
  begin
    QConvBemEstar.GotoBookmark(reg_atual);
    QConvBemEstar.freebookmark(reg_atual);
  end;

  QConvBemEstar.EnableControls;
  newValue := '';
  oldValue := '';
  valor := '';
  cbCancelarAutor.Checked := True;
end;
end ;

function TfrmCancelarConvAESPDebitos.BuscaConvDescontaTaxa(convID: integer):boolean;
var teste : Variant;
begin
  {if}teste := DMConexao.ExecuteScalar(' select conv_id from CONV_AESP_ODONTO where CONV_ID  = '+
    IntToStr(convID));// = 0 then
    if(teste = null) then
    begin
      Result:= False
    end
    else
      Result:= True;
end;

procedure TfrmCancelarConvAESPDebitos.txtCodEmpresaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then BitBtn1.Click;
end;

procedure TfrmCancelarConvAESPDebitos.FormCreate(Sender: TObject);
begin
  inherited;
  txtCodEmpresa.SetFocus;
end;

procedure TfrmCancelarConvAESPDebitos.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QConvBemEstar.Sort) > 0 then begin
     if Pos(' DESC',QConvBemEstar.Sort) > 0 then QConvBemEstar.Sort := Field.FieldName
                                                  else QConvBemEstar.Sort := Field.FieldName+' DESC';
  end
  else QConvBemEstar.Sort := Field.FieldName;
  except
  end;
end;

end.
