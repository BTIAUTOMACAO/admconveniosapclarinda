unit FCadBairro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, Buttons, Mask, DB, ADODB;

type
  TfrmBairro = class(TForm)
    lblCadBairro: TLabel;
    txtBairro: TEdit;
    btnCancela: TBitBtn;
    BitBtn1: TBitBtn;
    procedure btnAddBairroClick(Sender: TObject);
    
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBairro: TfrmBairro;

implementation

{$R *.dfm}

procedure Valida;
    begin
      //Nome
      if Trim(frmBairro.txtBairro.Text) = '' then
      begin
        ShowMessage('Informe o nome do Bairro!');
        //.ActivePageIndex := 0;
        frmBairro.txtBairro.SetFocus;
        Abort;
      end;
      ShowMessage('Tudo ok!');
    end;
procedure TfrmBairro.btnAddBairroClick(Sender: TObject);
begin
  Valida;
end;

end.
