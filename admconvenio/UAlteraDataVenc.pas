unit UAlteraDataVenc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ToolEdit, DB,
  ZAbstractRODataset, ZDataset, JvExMask, JvToolEdit, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit;

type
  TFAlteraDataVenc = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
//    data: TDateEdit;
    Bevel1: TBevel;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LabFatura: TLabel;
    LabNome: TLabel;
    LabData: TLabel;
    LabValor: TLabel;
    edObs: TEdit;
    Label6: TLabel;
    data: TJvDateEdit;
    procedure btnGravarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    dtvenc: TDate;
    obs: string;
    { Public declarations }
  end;

var
  FAlteraDataVenc: TFAlteraDataVenc;

implementation

uses DM, cartao_util, UFatura, UMenu;

{$R *.dfm}

procedure TFAlteraDataVenc.btnGravarClick(Sender: TObject);
begin
  if MsgSimNao('Deseja alterar os dados da fatura?') then begin
     if edObs.Text <> obs then
        DMConexao.GravaLog('FFatura','Observa��o',obs,edObs.Text,Operador.Nome,'Altera��o',LabFatura.Caption,Self.Name);
     if data.Date <> dtvenc then
        DMConexao.GravaLog('FFatura','Data de Vencimento',FormatDataBR(dtvenc),data.Text,Operador.Nome,'Altera��o',LabFatura.Caption,Self.Name);
     DMConexao.Q.Close;
     DMConexao.Q.SQL.Clear;
     DMConexao.Q.SQL.Add(' update fatura set fatura.data_vencimento = '+ FormatDataIB(data.Date));
     DMConexao.Q.SQL.Add(' ,fatura.obs = '''+edObs.Text+'''');
     DMConexao.Q.SQL.Add(' where fatura.fatura_id = '+LabFatura.Caption);
     DMConexao.Q.ExecSQL;
  end;
end;

procedure TFAlteraDataVenc.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFAlteraDataVenc.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = vk_return then
      SelectNext(ActiveControl,true,true);
end;

end.
