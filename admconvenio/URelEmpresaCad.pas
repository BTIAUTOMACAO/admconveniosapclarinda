unit URelEmpresaCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  U1,Menus, Buttons, ExtCtrls, Grids,Mask,ToolEdit, ClassImpressao,frxGradient, frxDBSet,
  Dialogs, frxClass, frxExportPDF, DB,DBClient, ADODB,
  JvMemoryDataset, StdCtrls, JvExControls, JvDBLookup,
  DBGrids, JvExDBGrids, JvDBGrid, ComCtrls, JvExMask, JvToolEdit,
  Printers, JvLabel, JvExComCtrls, JvStatusBar;

type
  TFRelEmpresaCad = class(TF1)
    Panel1: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    Minimizar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    GroupBox2: TGroupBox;
    datafin: TJvDateEdit;
    DataIni: TJvDateEdit;
    MCredenciados: TJvMemoryData;
    frxCredenciados: TfrxReport;
    QCredenciados: TADOQuery;
    dsCredenciados: TDataSource;
    dbCredenciado: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    QCredenciadoscred_id: TIntegerField;
    QCredenciadosnome: TStringField;
    QCredenciadosfantasia: TStringField;
    QCredenciadosendereco: TStringField;
    QCredenciadosnumero: TIntegerField;
    QCredenciadosbairro: TStringField;
    QCredenciadoscidade: TStringField;
    QCredenciadostelefone1: TStringField;
    QCredenciadostelefone2: TStringField;
    QCredenciadosdtcadastro: TDateTimeField;
    MCredenciadoscred_id: TIntegerField;
    MCredenciadosnome: TStringField;
    MCredenciadosfantasia: TStringField;
    MCredenciadosendereco: TStringField;
    MCredenciadosnumero: TIntegerField;
    MCredenciadosbairro: TStringField;
    MCredenciadoscidade: TStringField;
    MCredenciadostelefone1: TStringField;
    MCredenciadostelefone2: TStringField;
    MCredenciadosdtcadastro: TDateTimeField;
    MCredenciadosmarcado: TBooleanField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    BitBtn1: TBitBtn;
    bntGerarPDF: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    JvStatusBar1: TJvStatusBar;
    lbStatus: TJvLabel;
    procedure ButListaEmpClick(Sender: TObject);
    procedure QCredenciadosBeforeOpen(DataSet: TDataSet);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelEmpresaCad: TFRelEmpresaCad;

implementation

uses cartao_util;

{$R *.dfm}

procedure TFRelEmpresaCad.ButListaEmpClick(Sender: TObject);
begin
  lbStatus.Caption := 'Aguarde enquanto efetuamos a pesquisa...';
  Screen.Cursor := crHourGlass;
  QCredenciados.Close;
  QCredenciados.Open;
  if QCredenciados.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum credenciado cadastrado no per�odo selecionado.');
    DataIni.SetFocus;
  end;
  QCredenciados.First;
  MCredenciados.Open;
  MCredenciados.EmptyTable;
  MCredenciados.DisableControls;
  while not QCredenciados.Eof do
  begin
    MCredenciados.Append;
    MCredenciadoscred_id.AsInteger       := QCredenciadoscred_id.AsInteger;
    MCredenciadosnome.AsString           := QCredenciadosnome.AsString;
    MCredenciadosfantasia.AsString       := QCredenciadosfantasia.AsString;
    MCredenciadosendereco.AsString       := QCredenciadosendereco.AsString;
    MCredenciadosnumero.AsInteger        := QCredenciadosnumero.AsInteger;
    MCredenciadosbairro.AsString         := QCredenciadosbairro.AsString;
    MCredenciadoscidade.AsString         := QCredenciadoscidade.AsString;
    MCredenciadostelefone1.AsString      := QCredenciadostelefone1.AsString;
    MCredenciadostelefone2.AsString      := QCredenciadostelefone2.AsString;
    MCredenciadosdtcadastro.AsDateTime   := QCredenciadosdtcadastro.AsDateTime;
    MCredenciadosmarcado.AsBoolean       := False;

    QCredenciados.Next;
  end;
  MCredenciados.First;
  MCredenciados.EnableControls;
  lbStatus.Caption := '';
  Screen.Cursor := crDefault;

end;

procedure TFRelEmpresaCad.QCredenciadosBeforeOpen(DataSet: TDataSet);
begin

  if((DataIni.Date = 0)) then
  begin
     MsgInf('A data inicial n�o pode estar vazia'); DataIni.SetFocus; Exit;
  end;

  if((DataIni.Date > datafin.Date) and (datafin.Date <> 0))then
  begin
     MsgInf('A data inicial n�o pode ser maior do que a data final');
     DataIni.SetFocus;
     Exit;
  end;


  QCredenciados.Parameters[0].Value := DataIni.Date;
  QCredenciados.Parameters[1].Value := datafin.Date;
end;

procedure TFRelEmpresaCad.JvDBGrid1DblClick(Sender: TObject);
begin
  ButMarcaDesmEmpClick(nil);
end;

procedure TFRelEmpresaCad.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;

  if MCredenciados.IsEmpty then Exit;
     MCredenciados.Edit;
     MCredenciadosmarcado.AsBoolean := not MCredenciadosmarcado.AsBoolean;
     MCredenciados.Post;
     //Empresas_Sel;
end;

procedure TFRelEmpresaCad.BitBtn1Click(Sender: TObject);
begin
   frxCredenciados.Variables['dataIni'] := QuotedStr(DateToStr(DataIni.Date));
   frxCredenciados.Variables['dataFin'] := QuotedStr(DateToStr(datafin.Date));

   frxCredenciados.ShowReport();
end;

end.
