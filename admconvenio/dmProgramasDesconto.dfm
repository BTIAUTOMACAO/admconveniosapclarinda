object dmProgramasDesconto1: TdmProgramasDesconto1
  OldCreateOrder = False
  Left = 441
  Top = 208
  Height = 150
  Width = 273
  object DSProgramaDesconto: TDataSource
    DataSet = QProgramaDesconto
    Left = 48
    Top = 56
  end
  object QProgramaDesconto: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cred_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      
        'p.prog_id, p.dt_inicio, p.dt_fim, p.verifica_valor, pe.empres_id' +
        ', pc.cred_id, pc.obriga_desconto'
      'from programas p'
      'join prog_empr pe on pe.prog_id = p.prog_id'
      'join prog_cred pc on pc.prog_id = p.prog_id'
      'where p.apagado <> '#39'S'#39
      'and pe.empres_id = :empres_id'
      'and pc.cred_id = :cred_id'
      'order by p.prog_id')
    Left = 48
    Top = 16
    object QProgramaDescontoprog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object QProgramaDescontodt_inicio: TDateTimeField
      FieldName = 'dt_inicio'
    end
    object QProgramaDescontodt_fim: TDateTimeField
      FieldName = 'dt_fim'
    end
    object QProgramaDescontoverifica_valor: TStringField
      FieldName = 'verifica_valor'
      FixedChar = True
      Size = 1
    end
    object QProgramaDescontoempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QProgramaDescontocred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QProgramaDescontoobriga_desconto: TStringField
      FieldName = 'obriga_desconto'
      FixedChar = True
      Size = 1
    end
  end
end
