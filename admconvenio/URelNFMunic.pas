unit URelNFMunic;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, JvExMask, JvToolEdit, DB, ADODB;

type
  TFRelNFMunic = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    ComboBox1: TComboBox;
    Label3: TLabel;
    DataInicio: TJvDateEdit;
    DataFinal: TJvDateEdit;
    CheckSpani: TCheckBox;
    CheckEstrela: TCheckBox;
    QBusca: TADOQuery;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SalvarXLS(Dados : TADOQuery; NomeArq : String; mes: String; ano: String; filial: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelNFMunic: TFRelNFMunic;

implementation
  uses DM, URotinasTexto, cartao_util, UValidacao, DateUtils,  FileCtrl, ComObj, StrUtils ,
  uDatas;
{$R *.dfm}

procedure TFRelNFMunic.Button2Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TFRelNFMunic.Button1Click(Sender: TObject);
var filial,teste: String;
mes, ano: String;
begin
  if ComboBox1.ItemIndex <> -1 then
  begin
    if ComboBox1.ItemIndex = 0 then
      filial := '1'
    else if ComboBox1.ItemIndex = 1 then
      filial := '2'
    else
      filial := '3';

    QBusca.SQL.Clear;
    QBusca.SQL.Add('select ocrd.u_es_id COD, opch.CardName NOME, cred.CGC CNPJ,');
    QBusca.SQL.Add('SUM(cast(((DocTotal*cred.taxa_nf_prefeitura)/100) as numeric(8,2))) AS SUM_TAXA');
    QBusca.SQL.Add('from [SAP].[SBO_BELLA_GOLD].[DBO].[opch]');
    QBusca.SQL.Add('inner join [SAP].[SBO_BELLA_GOLD].[DBO].[crd7] on opch.cardcode = crd7.cardcode');
    QBusca.SQL.Add('inner join [SAP].[SBO_BELLA_GOLD].[DBO].[ocrd] on ocrd.cardcode = opch.cardcode');
    QBusca.SQL.Add('inner join CREDENCIADOS cred on cred.CRED_ID = ocrd.u_es_id');
    QBusca.SQL.Add('where DocDate between '''+DataInicio.Text+''' and '''+DataFinal.Text+''' and BPLId = '+filial);

    //23/09/21 - adicionado para impedir a select de buscar transa��es duplicadas
    QBusca.SQL.Add('and canceled = ''N''');

    if CheckSpani.Checked then begin
      QBusca.SQL.Add('and cred.CRED_ID IN(');
      QBusca.SQL.Add('1440, 1923, 1924, 2263, 2501, 2752, 3040, 3189, 3341, 3759, 3760, 3784,');
      QBusca.SQL.Add('3805, 3806, 3807, 3832, 3835, 3848, 3849, 3850, 3876, 3910, 4044, 4611, 4963, 4964,');
      QBusca.SQL.Add('1425, 1424, 1423, 1422, 1421, 1420, 1419, 1418, 1417, 5121,5122,5167,5168)');
    end
    else begin
      QBusca.SQL.Add('and cred.CRED_ID NOT IN(');
      QBusca.SQL.Add('1440, 1923, 1924, 2263, 2501, 2752, 3040, 3189, 3341, 3759, 3760, 3784,');
      QBusca.SQL.Add('3805, 3806, 3807, 3832, 3835, 3848, 3849, 3850, 3876, 3910, 4044, 4611, 4963, 4964,');
      QBusca.SQL.Add('1425, 1424, 1423, 1422, 1421, 1420, 1419, 1418, 1417,5121,5122,5167,5168)');
      end;
      
    if CheckEstrela.Checked then begin
      QBusca.SQL.Add('and cred.CRED_ID IN ( SELECT CRED_ID FROM CREDENCIADOS WHERE FANTASIA LIKE ''%SETE ESTRELAS%'' OR NOME LIKE ''%SETE ESTRELAS%'')');
    end
    else begin
      QBusca.SQL.Add('and cred.CRED_ID NOT IN ( SELECT CRED_ID FROM CREDENCIADOS WHERE FANTASIA LIKE ''%SETE ESTRELAS%'' OR NOME LIKE ''%SETE ESTRELAS%'')');
    end;

    QBusca.SQL.Add('GROUP BY ocrd.u_es_id,opch.CardName,CRED.CGC');
    QBusca.SQL.Add('ORDER BY ocrd.u_es_id');
    teste := QBusca.SQL.text;
    QBusca.Open;

    if ComboBox1.ItemIndex = 0 then
      filial := 'RPC'
    else if ComboBox1.ItemIndex = 1 then
      filial := 'CDC'
    else
      filial := 'PratiCard';

    mes := StringReplace(FormatDateTime('mm',DataFinal.Date),'/','',[rfReplaceAll]);
    ano := StringReplace(FormatDateTime('yyyy',DataFinal.Date),'/','',[rfReplaceAll]);
    if mes = '01' then
      mes := 'JANEIRO';
    if mes = '02' then
      mes := 'FEVEREIRO';
    if mes = '03' then
      mes := 'MARCO';
    if mes = '04' then
      mes := 'ABRIL';
    if mes = '05' then
      mes := 'MAIO';
    if mes = '06' then
      mes := 'JUNHO';
    if mes = '07' then
      mes := 'JULHO';
    if mes = '08' then
      mes := 'AGOSTO';
    if mes = '09' then
      mes := 'SETEMBRO';
    if mes = '10' then
      mes := 'OUTUBRO';
    if mes = '11' then
      mes := 'NOVEMBRO';
    if mes = '12' then
      mes := 'DEZEMBRO';
    SalvarXLS(QBusca, 'NF - '+ filial + ' - ' + mes + ' ' + ano, mes, ano, filial);
    ShowMessage('Efetuado Exporta��o com sucesso');
    Screen.Cursor := crDefault;
    SysUtils.Abort;
  end;
  if ComboBox1.ItemIndex = -1 then 
    MsgInf('Selecione uma Filial!');

end;

procedure TFRelNFMunic.SalvarXLS(Dados : TADOQuery; NomeArq : String; mes: String; ano: String; filial: String);
var excel: variant;
    i , coluna, linha: integer;
    valor: variant;
begin
   //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        excel.Workbooks.add(1);
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      try
        Screen.Cursor := crHourGlass;
        coluna := 0;
        linha  := 2;


        excel.cells[1,2] := 'NOTAS FISCAIS M�S - '+mes+' '+ano+' - '+filial + ' | ADM DE CART�O DE CONV�NIO REF A '+ DataInicio.Text +' A '+ DataFinal.Text;
        excel.cells[1,2].Font.bold := True;
        excel.cells[1,2].Font.size := 14;
        excel.cells[1,2].Interior.Color := clAqua;
        excel.Workbooks[1].WorkSheets[1].Range['A1:H1'].Merge; // Mescla c�lula  

        for i:=0 to Dados.FieldCount - 1  do begin
          inc(coluna);
          valor := Dados.Fields[i].DisplayLabel;
          excel.cells[2,coluna] := valor;
          excel.cells[linha,coluna].Font.bold := True;
        end;
        inc(linha);

        Dados.First;
        While not Dados.Eof do begin
          Coluna := 0;
          for i:= 0 to Dados.FieldCount-1 do begin
            inc(coluna);
            if (Dados.Fields[i] is TIntegerField) or (Dados.Fields[i] is TFloatField) or (Dados.Fields[i] is TBCDField)then begin
              valor := Dados.Fields[i].AsFloat;
              if (Dados.Fields[i] is TBCDField) then
                excel.cells[linha,coluna].NumberFormat := '#.##0,00_);(#.##0,00)'
              else
              excel.cells[linha,coluna].NumberFormat := '#';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end else begin
              valor := Dados.Fields[i].AsString;
              excel.cells[linha,coluna].NumberFormat := '@';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
            end;
            excel.cells[linha,coluna].Value := valor;
          end;
          inc(linha);
          Dados.Next;
        end;
        excel.columns.AutoFit;
        excel.ActiveWorkBook.SaveAs(FileName:='C:\Users\Public\'+NomeArq,Password := '');
        excel.Visible := true;
        Screen.Cursor := crDefault;

    except on e:Exception do
        msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
        e.message);
      end;
end;

end.
