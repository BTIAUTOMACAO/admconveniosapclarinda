unit UValidacao;

interface

uses Windows, SysUtils, Dialogs, Controls, DBCtrls, ComCtrls, Classes, DB,
      JvDBLookup, Forms, cartao_util;

function versaoOffice: Integer;
function IsCurrency(s: string): Boolean;
function IsStrANumber(const S: string): Boolean;
function fnIsEmail(const S : String): Boolean;
function fnVerfCompVazioEmTabSheet(MensagemErro : String ; Componente : TComponent) : Boolean;
function fnVerfCampoVazio(Mensagem : String ; Campo : TField) : Boolean;
function fnQtdRegistro(Mensagem : String ; DataSet : TDataSet) : Boolean;
function fnDataValida(Dado: string): Boolean;
procedure prVerfEAbreCon(DataSet : TDataSet);


implementation

function versaoOffice: Integer;
var
  mTexto: AnsiString;
begin
  if DirectoryExists('C:\Arquivos de programas\Microsoft Office\OFFICE9') then
  begin
    Result := 9;
    Exit;
  end;
  if DirectoryExists('C:\Arquivos de programas\Microsoft Office\OFFICE10') then
  begin
    Result := 10;
    Exit;
  end;
  if DirectoryExists('C:\Arquivos de programas\Microsoft Office\OFFICE11') then
  begin
    Result := 11;
    Exit;
  end;
  if DirectoryExists('C:\Arquivos de programas\Microsoft Office\OFFICE12') then
  begin
    Result := 12;
    Exit;
  end;
  if DirectoryExists('C:\Arquivos de programas\Microsoft Office\OFFICE13') then
  begin
    Result := 13;
    Exit;
  end;
  if DirectoryExists('C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013') then
  begin
    Result := 13;
    Exit;
  end;
end;

function IsCurrency(s: string): Boolean;
begin
  try
    StrToCurr(s);
    result := True;
  except
    result := False;
  end;
end;


function IsStrANumber(const S: string): Boolean;
var
  P: PChar;
begin
  P      := PChar(S);
  Result := False;
  while P^ <> #0 do
  begin
    if not (P^ in ['0'..'9']) then Exit;
    Inc(P);
  end;
  Result := True;
end;

function fnIsEmail(const S : String): Boolean;
begin
Result := (Pos('@',S) > 0) and (Pos('.',S) > 0) and (Pos('@',S) < LastDelimiter('.',S)) and (S[Pos('@',S)+1] <> '.') and (S[Length(S)] <> '.');
end;

function fnVerfCompVazioEmTabSheet(MensagemErro: String;
  Componente: TComponent): Boolean;
var
I : Integer;
Control, ControlComp : TWinControl;
Vazio : Boolean;
begin
I := 0;
Control     := TWinControl.Create(nil);
ControlComp := TWinControl.Create(nil);
Result := False;

if Componente is TDBEdit then
  begin
  Vazio := Trim(TDBEdit(Componente).Text) = '';
  ControlComp := TDBEdit(Componente);
  end;

if Componente is TJvDBLookupCombo then
  begin
  Vazio := Trim(TJvDBLookupCombo(Componente).Text) = '';
  ControlComp := TJvDBLookupCombo(Componente);
  end;

if Vazio then
  begin
  Result := True;
  MsgErro(MensagemErro);
  while not (Control is TTabSheet) do
    begin
    if I = 0 then
      Control := ControlComp.Parent
    else
      Control := Control.Parent;
    I := I + 1;
    end;
  TPageControl(TTabSheet(Control).Parent).ActivePageIndex := TTabSheet(Control).PageIndex;
  ControlComp.SetFocus;
  end;
end;

function fnVerfCampoVazio(Mensagem: String; Campo: TField): Boolean;
begin
  Result := False;
  if Trim(Campo.AsString) = '' then
    begin
    Result := True;
    MsgInf(Mensagem);
    end;
end;

function fnQtdRegistro(Mensagem : String ; DataSet : TDataSet) : Boolean;
begin
  Result := DataSet.RecordCount <= 0;
  if (Result) and (Mensagem <> '')then MsgErro(Mensagem);
end;

function fnDataValida(Dado: string): Boolean;
begin
  try
    FormatDateTime('dd/mm/yyyy',StrToDate(Dado));
    Result := True;
  except
    Result := False;
  end;
end;


procedure prVerfEAbreCon(DataSet : TDataSet);
begin
if DataSet.Active = False then
  DataSet.Open;
end;

end.
