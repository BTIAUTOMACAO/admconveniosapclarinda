object FAltContaCor: TFAltContaCor
  Left = 495
  Top = 218
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Altera'#231#227'o de Conta Corrente'
  ClientHeight = 321
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 270
    Width = 447
    Height = 51
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 1
    object ButSalvar: TBitBtn
      Left = 263
      Top = 9
      Width = 80
      Height = 29
      Caption = '&Salvar'
      TabOrder = 0
      OnClick = ButSalvarClick
    end
    object ButCancel: TBitBtn
      Left = 351
      Top = 9
      Width = 80
      Height = 29
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = ButCancelClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 447
    Height = 270
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 0
    object Label1: TLabel
      Left = 26
      Top = 5
      Width = 56
      Height = 13
      Caption = 'Autoriza'#231#227'o'
    end
    object Label2: TLabel
      Left = 147
      Top = 5
      Width = 27
      Height = 13
      Caption = 'Digito'
    end
    object Label3: TLabel
      Left = 26
      Top = 48
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object Label4: TLabel
      Left = 148
      Top = 48
      Width = 23
      Height = 13
      Caption = 'Hora'
    end
    object Label5: TLabel
      Left = 26
      Top = 93
      Width = 78
      Height = 13
      Caption = 'Estabelecimento'
    end
    object Label6: TLabel
      Left = 26
      Top = 137
      Width = 31
      Height = 13
      Caption = 'D'#233'bito'
    end
    object Label7: TLabel
      Left = 138
      Top = 137
      Width = 33
      Height = 13
      Caption = 'Cr'#233'dito'
    end
    object Label8: TLabel
      Left = 26
      Top = 180
      Width = 14
      Height = 13
      Caption = 'NF'
    end
    object Label9: TLabel
      Left = 26
      Top = 224
      Width = 41
      Height = 13
      Caption = 'Hist'#243'rico'
    end
    object Label10: TLabel
      Left = 306
      Top = 224
      Width = 44
      Height = 13
      Caption = 'Operador'
    end
    object Label11: TLabel
      Left = 138
      Top = 180
      Width = 37
      Height = 13
      Caption = 'Receita'
    end
    object Label12: TLabel
      Left = 191
      Top = 180
      Width = 43
      Height = 13
      Caption = 'Entregue'
    end
    object Bevel1: TBevel
      Left = 227
      Top = 64
      Width = 2
      Height = 19
    end
    object Label13: TLabel
      Left = 244
      Top = 47
      Width = 112
      Height = 13
      Caption = 'Data Fechamento Emp.'
    end
    object DBEdit1: TDBEdit
      Left = 26
      Top = 21
      Width = 110
      Height = 21
      DataField = 'AUTORIZACAO_ID'
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 26
      Top = 109
      Width = 385
      Height = 21
      DataField = 'CREDENCIADO'
      Enabled = False
      ReadOnly = True
      TabOrder = 5
    end
    object DBEdit3: TDBEdit
      Left = 147
      Top = 21
      Width = 33
      Height = 21
      DataField = 'DIGITO'
      Enabled = False
      ReadOnly = True
      TabOrder = 1
    end
    object DBEdit5: TDBEdit
      Left = 148
      Top = 64
      Width = 65
      Height = 21
      DataField = 'HORA'
      Enabled = False
      ReadOnly = True
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 26
      Top = 240
      Width = 273
      Height = 21
      CharCase = ecUpperCase
      DataField = 'HISTORICO'
      TabOrder = 11
    end
    object DBEdit7: TDBEdit
      Left = 26
      Top = 196
      Width = 100
      Height = 21
      DataField = 'NF'
      TabOrder = 8
    end
    object DBEdit8: TDBEdit
      Left = 306
      Top = 240
      Width = 121
      Height = 21
      DataField = 'OPERADOR'
      Enabled = False
      ReadOnly = True
      TabOrder = 12
    end
    object DBEdit9: TDBEdit
      Left = 138
      Top = 153
      Width = 100
      Height = 21
      DataField = 'CREDITO'
      TabOrder = 7
    end
    object DBEdit10: TDBEdit
      Left = 26
      Top = 153
      Width = 100
      Height = 21
      DataField = 'DEBITO'
      TabOrder = 6
    end
    object JvDBDateEdit1: TJvDBDateEdit
      Left = 26
      Top = 64
      Width = 113
      Height = 21
      DataField = 'DATA'
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 2
    end
    object DBComboBox1: TDBComboBox
      Left = 138
      Top = 196
      Width = 47
      Height = 21
      Style = csDropDownList
      DataField = 'RECEITA'
      ItemHeight = 13
      Items.Strings = (
        'S'
        'N')
      TabOrder = 9
    end
    object DBComboEntregue: TDBComboBox
      Left = 191
      Top = 196
      Width = 47
      Height = 21
      Style = csDropDownList
      DataField = 'ENTREG_NF'
      ItemHeight = 13
      Items.Strings = (
        'S'
        'N')
      TabOrder = 10
    end
    object JvDBDateEdit2: TJvDBDateEdit
      Left = 245
      Top = 63
      Width = 113
      Height = 21
      DataField = 'DATA_FECHA_EMP'
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
    end
  end
end
