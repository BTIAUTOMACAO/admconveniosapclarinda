inherited FCadFormaPgto: TFCadFormaPgto
  Left = 120
  Top = 167
  Caption = 'Cadastro de Formas de Pagamento'
  ClientHeight = 514
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Height = 473
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 354
        inherited EdNome: TEdit
          TabOrder = 1
        end
        inherited ButBusca: TBitBtn
          TabOrder = 2
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          TabOrder = 3
        end
        inherited EdCod: TEdit
          TabOrder = 0
        end
      end
      inherited DBGrid1: TJvDBGrid
        Height = 354
        Columns = <
          item
            Expanded = False
            FieldName = 'FORMA_ID'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Title.Caption = 'Liberada'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARCELAS'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT01'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO01'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT02'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO02'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT03'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO03'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT04'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO04'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT05'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO05'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT06'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO06'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT07'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO07'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT08'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO08'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT09'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO09'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT10'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO10'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT11'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO11'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT12'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO12'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT13'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO13'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT14'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO14'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT15'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO15'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT16'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO16'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT17'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO17'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENT18'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRAZO18'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Title.Caption = 'Data da '#218'ltima Altera'#231#227'o'
            Width = 124
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 426
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 413
        TabOrder = 1
      end
      inherited Panel3: TPanel
        Height = 413
        TabOrder = 0
        object Label3: TLabel
          Left = 19
          Top = 4
          Width = 43
          Height = 13
          Caption = 'Forma ID'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 88
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 67
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 1'
          FocusControl = Pe1
        end
        object Label7: TLabel
          Left = 187
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 2'
          FocusControl = Pe2
        end
        object Label8: TLabel
          Left = 309
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 3'
          FocusControl = Pe3
        end
        object Label9: TLabel
          Left = 430
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 4'
          FocusControl = Pe4
        end
        object Label10: TLabel
          Left = 551
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 5'
          FocusControl = Pe5
        end
        object Label11: TLabel
          Left = 670
          Top = 90
          Width = 46
          Height = 13
          Caption = 'Percent 6'
          FocusControl = Pe6
        end
        object Label12: TLabel
          Left = 67
          Top = 145
          Width = 46
          Height = 13
          Caption = 'Percent 7'
          FocusControl = Pe7
        end
        object Label13: TLabel
          Left = 187
          Top = 145
          Width = 46
          Height = 13
          Caption = 'Percent 8'
          FocusControl = Pe8
        end
        object Label14: TLabel
          Left = 309
          Top = 145
          Width = 46
          Height = 13
          Caption = 'Percent 9'
          FocusControl = Pe9
        end
        object Label15: TLabel
          Left = 430
          Top = 145
          Width = 52
          Height = 13
          Caption = 'Percent 10'
          FocusControl = Pe10
        end
        object Label16: TLabel
          Left = 551
          Top = 145
          Width = 52
          Height = 13
          Caption = 'Percent 11'
          FocusControl = Pe11
        end
        object Label17: TLabel
          Left = 670
          Top = 145
          Width = 52
          Height = 13
          Caption = 'Percent 12'
          FocusControl = Pe12
        end
        object Label18: TLabel
          Left = 67
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 13'
          FocusControl = Pe13
        end
        object Label19: TLabel
          Left = 187
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 14'
          FocusControl = Pe14
        end
        object Label20: TLabel
          Left = 309
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 15'
          FocusControl = Pe15
        end
        object Label21: TLabel
          Left = 430
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 16'
          FocusControl = Pe16
        end
        object Label22: TLabel
          Left = 551
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 17'
          FocusControl = Pe17
        end
        object Label23: TLabel
          Left = 670
          Top = 203
          Width = 52
          Height = 13
          Caption = 'Percent 18'
          FocusControl = Pe18
        end
        object Label24: TLabel
          Left = 19
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 1'
          FocusControl = Pr1
        end
        object Label25: TLabel
          Left = 139
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 2'
          FocusControl = Pr2
        end
        object Label26: TLabel
          Left = 260
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 3'
          FocusControl = Pr3
        end
        object Label27: TLabel
          Left = 381
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 4'
          FocusControl = Pr4
        end
        object Label28: TLabel
          Left = 502
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 5'
          FocusControl = Pr5
        end
        object Label29: TLabel
          Left = 621
          Top = 90
          Width = 36
          Height = 13
          Caption = 'Prazo 6'
          FocusControl = Pr6
        end
        object Label30: TLabel
          Left = 19
          Top = 145
          Width = 36
          Height = 13
          Caption = 'Prazo 7'
          FocusControl = Pr7
        end
        object Label31: TLabel
          Left = 139
          Top = 145
          Width = 36
          Height = 13
          Caption = 'Prazo 8'
          FocusControl = Pr8
        end
        object Label32: TLabel
          Left = 260
          Top = 145
          Width = 36
          Height = 13
          Caption = 'Prazo 9'
          FocusControl = Pr9
        end
        object Label33: TLabel
          Left = 381
          Top = 145
          Width = 42
          Height = 13
          Caption = 'Prazo 10'
          FocusControl = Pr10
        end
        object Label34: TLabel
          Left = 502
          Top = 145
          Width = 42
          Height = 13
          Caption = 'Prazo 11'
          FocusControl = Pr11
        end
        object Label35: TLabel
          Left = 621
          Top = 145
          Width = 42
          Height = 13
          Caption = 'Prazo 12'
          FocusControl = Pr12
        end
        object Label36: TLabel
          Left = 19
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 13'
          FocusControl = Pr13
        end
        object Label37: TLabel
          Left = 139
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 14'
          FocusControl = Pr14
        end
        object Label38: TLabel
          Left = 260
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 15'
          FocusControl = Pr15
        end
        object Label39: TLabel
          Left = 381
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 16'
          FocusControl = Pr16
        end
        object Label40: TLabel
          Left = 502
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 17'
          FocusControl = Pr17
        end
        object Label41: TLabel
          Left = 621
          Top = 203
          Width = 42
          Height = 13
          Caption = 'Prazo 18'
          FocusControl = Pr18
        end
        object Label5: TLabel
          Left = 19
          Top = 44
          Width = 56
          Height = 13
          Caption = 'N'#186' Parcelas'
          FocusControl = DBEdit3
        end
        object SpeedButton1: TSpeedButton
          Left = 424
          Top = 56
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
            5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
            DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
            CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
            E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
            A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
            2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
            D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
            3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
            F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
            3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
            FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
            3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
            FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
            3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
            FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
            5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
            FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
            FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
            FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
            FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
            E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
            BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
            C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
            7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
            D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
            E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
            FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
            FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
            D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          OnClick = SpeedButton1Click
        end
        object Label45: TLabel
          Left = 67
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 19'
          FocusControl = Pe19
        end
        object Label46: TLabel
          Left = 187
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 20'
          FocusControl = Pe20
        end
        object Label47: TLabel
          Left = 309
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 21'
          FocusControl = Pe21
        end
        object Label48: TLabel
          Left = 430
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 22'
          FocusControl = Pe22
        end
        object Label49: TLabel
          Left = 551
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 23'
          FocusControl = Pe23
        end
        object Label50: TLabel
          Left = 670
          Top = 263
          Width = 52
          Height = 13
          Caption = 'Percent 24'
          FocusControl = Pe24
        end
        object Label51: TLabel
          Left = 19
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 19'
          FocusControl = Pr19
        end
        object Label52: TLabel
          Left = 139
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 20'
          FocusControl = Pr20
        end
        object Label53: TLabel
          Left = 260
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 21'
          FocusControl = Pr21
        end
        object Label54: TLabel
          Left = 381
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 22'
          FocusControl = Pr22
        end
        object Label55: TLabel
          Left = 502
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 23'
          FocusControl = Pr23
        end
        object Label56: TLabel
          Left = 621
          Top = 263
          Width = 42
          Height = 13
          Caption = 'Prazo 24'
          FocusControl = Pr24
        end
        object DBEdit1: TDBEdit
          Left = 19
          Top = 20
          Width = 57
          Height = 21
          TabStop = False
          Color = clBtnFace
          DataField = 'FORMA_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 52
        end
        object DBCheckBox1: TDBCheckBox
          Left = 89
          Top = 60
          Width = 73
          Height = 17
          Caption = 'Liberada'
          DataField = 'LIBERADO'
          DataSource = DSCadastro
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBEdit2: TDBEdit
          Left = 88
          Top = 20
          Width = 313
          Height = 21
          CharCase = ecUpperCase
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 0
        end
        object Pe1: TDBEdit
          Left = 67
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT01'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 5
        end
        object Pe2: TDBEdit
          Left = 187
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT02'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 7
        end
        object Pe3: TDBEdit
          Left = 309
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT03'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 9
        end
        object Pe4: TDBEdit
          Left = 430
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT04'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 11
        end
        object Pe5: TDBEdit
          Left = 551
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT05'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 13
        end
        object Pe6: TDBEdit
          Left = 670
          Top = 107
          Width = 52
          Height = 21
          DataField = 'PERCENT06'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 15
        end
        object Pe7: TDBEdit
          Left = 67
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT07'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 17
        end
        object Pe8: TDBEdit
          Left = 187
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT08'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 19
        end
        object Pe9: TDBEdit
          Left = 309
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT09'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 21
        end
        object Pe10: TDBEdit
          Left = 430
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT10'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 23
        end
        object Pe11: TDBEdit
          Left = 551
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT11'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 25
        end
        object Pe12: TDBEdit
          Left = 670
          Top = 162
          Width = 52
          Height = 21
          DataField = 'PERCENT12'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 27
        end
        object Pe13: TDBEdit
          Left = 67
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT13'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 29
        end
        object Pe14: TDBEdit
          Left = 187
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT14'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 31
        end
        object Pe15: TDBEdit
          Left = 309
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT15'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 33
        end
        object Pe16: TDBEdit
          Left = 430
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT16'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 35
        end
        object Pe17: TDBEdit
          Left = 551
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT17'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 37
        end
        object Pe18: TDBEdit
          Left = 670
          Top = 220
          Width = 52
          Height = 21
          DataField = 'PERCENT18'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 39
        end
        object Pr1: TDBEdit
          Left = 19
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO01'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 4
        end
        object Pr2: TDBEdit
          Left = 139
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO02'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 6
        end
        object Pr3: TDBEdit
          Left = 260
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO03'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 8
        end
        object Pr4: TDBEdit
          Left = 381
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO04'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 10
        end
        object Pr5: TDBEdit
          Left = 502
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO05'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 12
        end
        object Pr6: TDBEdit
          Left = 621
          Top = 107
          Width = 42
          Height = 21
          DataField = 'PRAZO06'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 14
        end
        object Pr7: TDBEdit
          Left = 19
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO07'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 16
        end
        object Pr8: TDBEdit
          Left = 139
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO08'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 18
        end
        object Pr9: TDBEdit
          Left = 260
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO09'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 20
        end
        object Pr10: TDBEdit
          Left = 381
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO10'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 22
        end
        object Pr11: TDBEdit
          Left = 502
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO11'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 24
        end
        object Pr12: TDBEdit
          Left = 621
          Top = 162
          Width = 42
          Height = 21
          DataField = 'PRAZO12'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 26
        end
        object Pr13: TDBEdit
          Left = 19
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO13'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 28
        end
        object Pr14: TDBEdit
          Left = 139
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO14'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 30
        end
        object Pr15: TDBEdit
          Left = 260
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO15'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 32
        end
        object Pr16: TDBEdit
          Left = 381
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO16'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 34
        end
        object Pr17: TDBEdit
          Left = 502
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO17'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 36
        end
        object Pr18: TDBEdit
          Left = 621
          Top = 220
          Width = 42
          Height = 21
          DataField = 'PRAZO18'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 38
        end
        object DBEdit3: TDBEdit
          Left = 19
          Top = 60
          Width = 57
          Height = 21
          DataField = 'PARCELAS'
          DataSource = DSCadastro
          TabOrder = 1
          OnExit = DBEdit3Exit
          OnKeyPress = DBEdit3KeyPress
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 342
          Width = 764
          Height = 69
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 53
          object Label43: TLabel
            Left = 17
            Top = 20
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
            FocusControl = DBEdit12
          end
          object Label44: TLabel
            Left = 141
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit11
          end
          object DBEdit12: TDBEdit
            Left = 16
            Top = 36
            Width = 113
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 139
            Top = 36
            Width = 126
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
        end
        object DBCheckBox2: TDBCheckBox
          Left = 168
          Top = 60
          Width = 253
          Height = 17
          Hint = 'Consulte a ajuda do campo.'
          Caption = 'For'#231'ar dia de vencimento igual ao dia da compra.'
          DataField = 'FORCA_DIA'
          DataSource = DSCadastro
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object Pe19: TDBEdit
          Left = 67
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT19'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 41
        end
        object Pe20: TDBEdit
          Left = 187
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT20'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 43
        end
        object Pe21: TDBEdit
          Left = 309
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT21'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 45
        end
        object Pe22: TDBEdit
          Left = 430
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT22'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 47
        end
        object Pe23: TDBEdit
          Left = 551
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT23'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 49
        end
        object Pe24: TDBEdit
          Left = 670
          Top = 280
          Width = 52
          Height = 21
          DataField = 'PERCENT24'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 51
        end
        object Pr19: TDBEdit
          Left = 19
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO19'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 40
        end
        object Pr20: TDBEdit
          Left = 139
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO20'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 42
        end
        object Pr21: TDBEdit
          Left = 260
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO21'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 44
        end
        object Pr22: TDBEdit
          Left = 381
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO22'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 46
        end
        object Pr23: TDBEdit
          Left = 502
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO23'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 48
        end
        object Pr24: TDBEdit
          Left = 621
          Top = 280
          Width = 42
          Height = 21
          DataField = 'PRAZO24'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 50
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited GridHistorico: TJvDBGrid
        Height = 398
      end
    end
  end
  inherited DSCadastro: TDataSource
    Left = 556
    Top = 61
  end
  inherited PopupBut: TPopupMenu
    Left = 332
    Top = 72
  end
  inherited PopupGrid1: TPopupMenu
    Left = 236
    Top = 72
  end
  inherited QHistorico: TADOQuery
    Left = 612
    Top = 85
  end
  inherited DSHistorico: TDataSource
    Left = 644
    Top = 85
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from formaspagto where forma_id = 0 ')
    Left = 524
    Top = 61
    object QCadastroFORMA_ID: TIntegerField
      DisplayLabel = 'Forma ID'
      FieldName = 'FORMA_ID'
      Required = True
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 60
    end
    object QCadastroPARCELAS: TSmallintField
      DisplayLabel = 'Parcelas'
      FieldName = 'PARCELAS'
      Required = True
    end
    object QCadastroPRAZO01: TSmallintField
      DisplayLabel = 'Prazo 1'
      FieldName = 'PRAZO01'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO02: TSmallintField
      DisplayLabel = 'Prazo 2'
      FieldName = 'PRAZO02'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO03: TSmallintField
      DisplayLabel = 'Prazo 3'
      FieldName = 'PRAZO03'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO04: TSmallintField
      DisplayLabel = 'Prazo 4'
      FieldName = 'PRAZO04'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO05: TSmallintField
      DisplayLabel = 'Prazo 5'
      FieldName = 'PRAZO05'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO06: TSmallintField
      DisplayLabel = 'Prazo 6'
      FieldName = 'PRAZO06'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO07: TSmallintField
      DisplayLabel = 'Prazo 7'
      FieldName = 'PRAZO07'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO08: TSmallintField
      DisplayLabel = 'Prazo 8'
      FieldName = 'PRAZO08'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO09: TSmallintField
      DisplayLabel = 'Prazo 9'
      FieldName = 'PRAZO09'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO10: TSmallintField
      DisplayLabel = 'Prazo 10'
      FieldName = 'PRAZO10'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO11: TSmallintField
      DisplayLabel = 'Prazo 11'
      FieldName = 'PRAZO11'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO12: TSmallintField
      DisplayLabel = 'Prazo 12'
      FieldName = 'PRAZO12'
      DisplayFormat = '00'
    end
    object QCadastroPERCENT01: TFloatField
      DisplayLabel = 'Percent 1'
      FieldName = 'PERCENT01'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT02: TFloatField
      DisplayLabel = 'Percent 2'
      FieldName = 'PERCENT02'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT03: TFloatField
      DisplayLabel = 'Percent 3'
      FieldName = 'PERCENT03'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT04: TFloatField
      DisplayLabel = 'Percent 4'
      FieldName = 'PERCENT04'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT05: TFloatField
      DisplayLabel = 'Percent 5'
      FieldName = 'PERCENT05'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT06: TFloatField
      DisplayLabel = 'Percent 6'
      FieldName = 'PERCENT06'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT07: TFloatField
      DisplayLabel = 'Percent 7'
      FieldName = 'PERCENT07'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT08: TFloatField
      DisplayLabel = 'Percent 8'
      FieldName = 'PERCENT08'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT09: TFloatField
      DisplayLabel = 'Percent 9'
      FieldName = 'PERCENT09'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT10: TFloatField
      DisplayLabel = 'Percent 10'
      FieldName = 'PERCENT10'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT11: TFloatField
      DisplayLabel = 'Percent 11'
      FieldName = 'PERCENT11'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT12: TFloatField
      DisplayLabel = 'Percent 12'
      FieldName = 'PERCENT12'
      DisplayFormat = '##0.00'
    end
    object QCadastroLIBERADO: TStringField
      DisplayLabel = 'Liberado'
      FieldName = 'LIBERADO'
      Required = True
      Size = 1
    end
    object QCadastroPRAZO13: TSmallintField
      DisplayLabel = 'Prazo 13'
      FieldName = 'PRAZO13'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO14: TSmallintField
      DisplayLabel = 'Prazo 14'
      FieldName = 'PRAZO14'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO15: TSmallintField
      DisplayLabel = 'Prazo 15'
      FieldName = 'PRAZO15'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO16: TSmallintField
      DisplayLabel = 'Prazo 16'
      FieldName = 'PRAZO16'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO17: TSmallintField
      DisplayLabel = 'Prazo 17'
      FieldName = 'PRAZO17'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO18: TSmallintField
      DisplayLabel = 'Prazo 18'
      FieldName = 'PRAZO18'
      DisplayFormat = '00'
    end
    object QCadastroPERCENT13: TFloatField
      DisplayLabel = 'Percent 13'
      FieldName = 'PERCENT13'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT14: TFloatField
      DisplayLabel = 'Percent 14'
      FieldName = 'PERCENT14'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT15: TFloatField
      DisplayLabel = 'Percent 15'
      FieldName = 'PERCENT15'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT16: TFloatField
      DisplayLabel = 'Percent 16'
      FieldName = 'PERCENT16'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT17: TFloatField
      DisplayLabel = 'Percent 17'
      FieldName = 'PERCENT17'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT18: TFloatField
      DisplayLabel = 'Percent 18'
      FieldName = 'PERCENT18'
      DisplayFormat = '##0.00'
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroFORCA_DIA: TStringField
      DisplayLabel = 'For'#231'a Dia'
      FieldName = 'FORCA_DIA'
      Size = 1
    end
    object QCadastroPRAZO19: TSmallintField
      DisplayLabel = 'Prazo 19'
      FieldName = 'PRAZO19'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO20: TSmallintField
      DisplayLabel = 'Prazo 20'
      FieldName = 'PRAZO20'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO21: TSmallintField
      DisplayLabel = 'Prazo 21'
      FieldName = 'PRAZO21'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO22: TSmallintField
      DisplayLabel = 'Prazo 22'
      FieldName = 'PRAZO22'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO23: TSmallintField
      DisplayLabel = 'Prazo 23'
      FieldName = 'PRAZO23'
      DisplayFormat = '00'
    end
    object QCadastroPRAZO24: TSmallintField
      DisplayLabel = 'Prazo 24'
      FieldName = 'PRAZO24'
      DisplayFormat = '00'
    end
    object QCadastroPERCENT19: TFloatField
      DisplayLabel = 'Percent 19'
      FieldName = 'PERCENT19'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT20: TFloatField
      DisplayLabel = 'Percent 20'
      FieldName = 'PERCENT20'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT21: TFloatField
      DisplayLabel = 'Percent 21'
      FieldName = 'PERCENT21'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT22: TFloatField
      DisplayLabel = 'Percent 22'
      FieldName = 'PERCENT22'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT23: TFloatField
      DisplayLabel = 'Percent 23'
      FieldName = 'PERCENT23'
      DisplayFormat = '##0.00'
    end
    object QCadastroPERCENT24: TFloatField
      DisplayLabel = 'Percent 24'
      FieldName = 'PERCENT24'
      DisplayFormat = '##0.00'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
  end
end
