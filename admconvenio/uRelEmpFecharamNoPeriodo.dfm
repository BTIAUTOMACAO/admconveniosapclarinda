inherited frmRelEmpFecharamNoPeriodo: TfrmRelEmpFecharamNoPeriodo
  Left = 73
  Top = 51
  Caption = 'Relat'#243'rio de Empresas que Fecharam no Per'#237'odo'
  ClientHeight = 472
  ClientWidth = 816
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 816
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 113
    Width = 816
    Height = 359
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '&Empresas'
      object Panel3: TPanel
        Left = 0
        Top = 277
        Width = 808
        Height = 54
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 0
        DesignSize = (
          804
          50)
        object ButMarcDesm_Emp: TButton
          Left = 7
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F11)'
          TabOrder = 0
          OnClick = ButMarcDesm_EmpClick
        end
        object ButMarcaTodos_Emp: TButton
          Left = 115
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F8)'
          TabOrder = 1
          OnClick = ButMarcaTodos_EmpClick
        end
        object ButDesmTodos_Emp: TButton
          Left = 224
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F9)'
          TabOrder = 2
          OnClick = ButDesmTodos_EmpClick
        end
        object btnVisualizar: TBitBtn
          Left = 607
          Top = 8
          Width = 93
          Height = 29
          Anchors = [akTop, akRight]
          Caption = '&Visualizar'
          Enabled = False
          TabOrder = 3
          OnClick = btnVisualizarClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000C8D0D4C8D0D4
            CAD2D6CBD4D8BDC4C8B1B6B8A0A3A5A6ABAEBFC0C4B7ADB1B9BABDBEC4C6C1C9
            CDCBD4D9CAD3D7CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CECECEC3C3C3B5B5B5BB
            BBBBCECECEC1C1C1C8C8C8CECECED2D2D2CDD3D5CDD3D5CDD3D5CAD2D6CCD4D9
            C3CBCFB2B6B9B5B6B7D2D1D1AAA9A88F8D8D7763636A57578B8989AEAFAEBCBD
            BDB7BBBDC1C8CCCBD3D7CDD3D5CDD3D5D4D4D4C4C4C4C4C4C4DADADABABABAA4
            A4A48A8A8A808080A1A1A1BEBEBEC9C9C9C7C7C7CDD3D5CDD3D5C9D2D6B6BBC0
            ADAEB0CFCFCFECECECECECECB6B6B69796965552523737374B4B4B7070709F9E
            9EB7B6B6ADADAEB9BFC2CDD3D5C9C9C9BEBEBED9D9D9F0F0F0F0F0F0C5C5C5AB
            ABAB7676765F5F5F6F6F6F8D8D8DB1B1B1C5C5C5BDBDBDCACACABCC3C6BEBFBF
            F4F4F3F7F7F7E0E0E0BDBDBD8888888181819A9A9B9696968080807474746565
            65837E80A6A7A8BEC6CACDCDCDCBCBCBF5F5F5F9F9F9E6E6E6CACACAA0A0A09A
            9A9AAEAEAEABABAB9999999090908484849A9A9AB8B8B8D0D0D0B9BEC1DAD9D8
            E8E8E9BABABA9E9E9EA8A8A89C9C9C7D7D7D7474748080809393939D9D9D999B
            9A899B8EADB5B4C8CED4CACACAE0E0E0EDEDEDC8C8C8B1B1B1B9B9B9B0B0B097
            9797909090999999A9A9A9B1B1B1AEAEAEA8A8A8C0C0C0CDD3D5B4B9BC969595
            A8A8A9B1B1B1C6C6C6CBCBCBD2D2D2C0C0C0B3B3B3A5A5A59292928989898688
            8765816D949F9CCBD0D7C6C6C6AAAAAAB9B9B9C1C1C1D1D1D1D5D5D5DBDBDBCD
            CDCDC2C2C2B7B7B7A8A8A8A1A1A19F9F9F8F8F8FADADADCDD3D5A9AEB1A1A0A0
            DCDCDCD2D2D2C7C7C7D2D2D2C4C4C4CACACAC0C0C0BABABAC3C3C3BBBBBBB7B6
            B6ABA6A9A1A2A4C5CFD3BDBDBDB3B3B3E3E3E3DBDBDBD2D2D2DBDBDBD0D0D0D5
            D5D5CDCDCDC8C8C8CFCFCFC9C9C9C5C5C5BABABAB5B5B5CDD3D5BEC5C9BEC0C1
            CCCBCBC0C0C0C4C4C4C0C0C1E6E6E6F2F2F2E6E6E6DCDCDCCDCECEBBBBBBB9B8
            B8B4B4B3BCC1C3C7D0D4CFCFCFCCCCCCD5D5D5CDCDCDD0D0D0CDCDCDEBEBEBF5
            F5F5EBEBEBE3E3E3D7D7D7C9C9C9C6C6C6C2C2C2CCCCCCCDD3D5CBD3D7C5CDD1
            B5BABCA5A6A6CFD4D6AFB3B6B0B5BAC7CACECDCFD1DADBDCE4E2E1D9D8D7B8B8
            B8A7ABADC7CED3CAD2D6CDD3D5CDD3D5C6C6C6B7B7B7DBDBDBC2C2C2C4C4C4D5
            D5D5D8D8D8E2E2E2E8E8E8E0E0E0C6C6C6BBBBBBCDD3D5CDD3D5CAD2D6C9D1D5
            CDD7DBB0B2B3EEDBCBDFCAB1C3B4A6BBB2A8BBB9B5BCB4B4B19E9DB2B3B5B3B9
            BCC7CFD4CAD3D7C8D0D4CDD3D5CDD3D5DCDCDCC1C1C1E3E3E3D3D3D3C3C3C3C1
            C1C1C6C6C6C6C6C6B9B9B9C2C2C2C5C5C5D7D7D7CDD3D5CDD3D5CAD2D6C8D0D4
            C9D3D8BAB3B5E0A984FFD49FFFCF9EFFD4A6FDD6AED9B19AAD9FA3C6D1D6CAD3
            D7CAD2D6C8D0D4C9D1D5CDD3D5CDD3D5DADADAC5C5C5C1C1C1D8D8D8D8D8D8DB
            DBDBDDDDDDC7C7C7B7B7B7D7D7D7DADADACDD3D5CDD3D5CDD3D5CAD2D6C8D0D4
            C9D3D9B7A4A3F7CDA6FFD7AFFFD6AEFFD5AEFED7AEBE9488BBB3B9CCD8DDC8D0
            D4C8D0D4C8D0D4C9D1D5CDD3D5CDD3D5DADADABDBDBDD8D8D8DFDFDFDEDEDEDE
            DEDEDEDEDEB5B5B5C5C5C5DDDDDDCDD3D5CDD3D5CDD3D5CDD3D5CAD2D6C9D1D5
            C5CFD5BB9E97FCE6CBFFE3CAFFE3CAFFE6CCF1D5BFAF9293C5CDD2C9D1D5C8D0
            D4C8D0D4C8D0D4C9D1D5CDD3D5CDD3D5D7D7D7BABABAE9E9E9EAEAEAEAEAEAEA
            EAEAE0E0E0B3B3B3D6D6D6CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CAD2D6CCD8DC
            B9B5B8E2D0C8FFFCEFFFFBEDFFFAECFFFFF5D0B2ABAF9798CBD7DBC8D0D4C8D0
            D4C8D0D4C8D0D4C9D1D5CDD3D5DCDCDCC5C5C5DDDDDDF8F8F8F8F8F8F7F7F7FB
            FBFBCACACAB5B5B5CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CCD5D9C0C5C8
            B19697D9C4C2DECECEDECDCCDECECCDECCCCBA9D9DBEBABFCAD3D8C8D0D4C8D0
            D4C8D0D4C8D0D4C9D1D5CDD3D5D0D0D0B6B6B6D7D7D7DEDEDEDDDDDDDDDDDDDD
            DDDDBCBCBCCACACACDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CCD4D8C4C8CC
            BBB3B6BCB3B5BBB3B6BBB3B5BBB3B5BBB1B3C1BFC2CAD4D8C9D1D5C9D1D5C9D1
            D5C9D1D5C9D1D5CAD2D6CDD3D5D3D3D3C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5
            C5C5CDCDCDCDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5CDD3D5}
          NumGlyphs = 2
        end
        object btnGerarPDF: TBitBtn
          Left = 699
          Top = 8
          Width = 96
          Height = 29
          Anchors = [akTop, akRight]
          Caption = '&Gerar PDF'
          Enabled = False
          TabOrder = 4
          OnClick = btnGerarPDFClick
          Glyph.Data = {
            F6060000424DF606000000000000360000002800000018000000180000000100
            180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
            FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
            FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
            9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
            FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
            FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
            E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
            B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
            FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
            FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
            F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
            F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
            F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
            F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
            F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
            EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
            F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
            EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
            EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
            A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
            00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
            3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
            99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
            85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
            AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
            FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
            03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
            3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
            FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
            1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
            BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        end
      end
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 808
        Height = 277
        Align = alClient
        DataSource = dsEmp
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = JvDBGrid1TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NOME'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_FECHA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_VENC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TOTAL'
            Visible = True
          end>
      end
    end
  end
  object Panel7: TPanel [2]
    Left = 0
    Top = 23
    Width = 816
    Height = 90
    Align = alTop
    Caption = 'Panel4'
    TabOrder = 2
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 814
      Height = 88
      Align = alTop
      Caption = 'Filtro de Dados'
      TabOrder = 0
      object Label2: TLabel
        Left = 167
        Top = 18
        Width = 93
        Height = 13
        Caption = 'Dia de Fechamento'
      end
      object Bevel1: TBevel
        Left = 160
        Top = 17
        Width = 2
        Height = 57
      end
      object Label1: TLabel
        Left = 408
        Top = 19
        Width = 144
        Height = 13
        Caption = 'Responsavel pelo fechamento'
      end
      object ButListaEmp: TButton
        Left = 648
        Top = 35
        Width = 137
        Height = 23
        Caption = 'Listar Empresas (F5)'
        TabOrder = 5
        OnClick = ButListaEmpClick
      end
      object DataFecha: TJvDateEdit
        Left = 167
        Top = 34
        Width = 103
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 2
      end
      object por_dia_fecha: TRadioButton
        Left = 8
        Top = 24
        Width = 137
        Height = 17
        Caption = 'Por dia de Fechamento'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = por_dia_fechaClick
      end
      object por_periodo: TRadioButton
        Left = 8
        Top = 48
        Width = 129
        Height = 17
        Caption = 'Por Per'#237'odo'
        TabOrder = 1
        OnClick = por_dia_fechaClick
      end
      object datafin: TJvDateEdit
        Left = 282
        Top = 34
        Width = 107
        Height = 21
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 3
        Visible = False
      end
      object dbLkpOperadores: TDBLookupComboBox
        Left = 408
        Top = 34
        Width = 145
        Height = 21
        KeyField = 'NOME'
        ListField = 'NOME'
        ListFieldIndex = 1
        ListSource = dsOperadores
        TabOrder = 4
      end
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupButPopup
    Left = 456
    Top = 172
    object MenuItem1: TMenuItem
      Caption = 'Restaurar'
      Enabled = False
      OnClick = Restaurar1Click
    end
    object MenuItem2: TMenuItem
      Caption = 'Minimizar'
      OnClick = Minimizar1Click
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object MenuItem4: TMenuItem
      Caption = 'Fechar'
      OnClick = Fechar1Click
    end
  end
  object frxReport1: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41108.697881944400000000
    ReportOptions.LastChange = 41891.492103530090000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var c : Integer;                                   '
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MasterData1.visible := <dbEmpresas."MARCADO"> = True;         ' +
        '  '
      
        '  if <dbEmpresas."MARCADO"> =  True then begin                  ' +
        '           '
      '    if (c mod 2 = 0) then begin'
      
        '      grMaster.BeginColor := $00FFFFFF;                         ' +
        '                        '
      '      grMaster.EndColor   := $00FFFFFF;  '
      '    end else begin'
      '      grMaster.BeginColor := $00D2D2D2;'
      '      grMaster.EndColor   := $00D2D2D2;                        '
      '    end;                    '
      '    inc(c);'
      '  end;                  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 456
    Top = 31
    Datasets = <
      item
        DataSet = dbEmpresas
        DataSetName = 'dbEmpresas'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'Periodo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object PageHeader1: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Periodo: TfrxMemoView
          Align = baCenter
          Top = 22.677180000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Per'#195#173'odo: [Periodo]')
          ParentFont = False
        end
        object Titulo: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Relat'#195#179'rio de Empresas que fecharam no per'#195#173'odo')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 154.960730000000000000
        Width = 718.110700000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = dbEmpresas
        DataSetName = 'dbEmpresas'
        RowCount = 0
        object grMaster: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          EndColor = clWhite
          Style = gsHorizontal
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = clWhite
        end
        object dbEmpresasNOME: TfrxMemoView
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbEmpresas."EMPRES_ID"] - [dbEmpresas."NOME"]')
          ParentFont = False
        end
        object dbEmpresasDATA_FECHA: TfrxMemoView
          Left = 408.189240000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'DATA_FECHA'
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbEmpresas."DATA_FECHA"]')
          ParentFont = False
        end
        object dbEmpresasDATA_VENC: TfrxMemoView
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'DATA_VENC'
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbEmpresas."DATA_VENC"]')
          ParentFont = False
        end
        object dbEmpresasTOTAL: TfrxMemoView
          Align = baRight
          Left = 604.724800000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[dbEmpresas."TOTAL"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        object Page2: TfrxMemoView
          Left = 593.386210000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 646.520100000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '/')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          Left = 657.638220000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[TotalPages#]')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 60.472480000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
        object Time: TfrxMemoView
          Left = 139.842610000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Time]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data/Hora:')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 15.118120000000000000
        Top = 117.165430000000000000
        Width = 718.110700000000000000
        object Gradient2: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          EndColor = clSilver
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = 14737632
        end
        object mmEmpresa: TfrxMemoView
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Empresa')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Fechamento')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 498.897960000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Vencimento')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 680.315400000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Total')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 15.118120000000000000
        Top = 192.756030000000000000
        Width = 718.110700000000000000
        object Gradient3: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          BeginColor = clSilver
          EndColor = clWhite
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = 14671839
        end
        object Memo4: TfrxMemoView
          Align = baRight
          Left = 604.724800000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Total: [SUM(<dbEmpresas."TOTAL">,MasterData1)]')
          ParentFont = False
        end
      end
    end
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 512
    Top = 71
  end
  object dbEmpresas: TfrxDBDataset
    UserName = 'dbEmpresas'
    CloseDataSource = False
    DataSet = MEmpresas
    BCDToCurrency = False
    Left = 456
    Top = 72
  end
  object sd: TSaveDialog
    Filter = '.pdf|.pdf'
    Left = 744
    Top = 304
  end
  object frxPDFExport1: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Title = 'Extrato Empresa'
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 512
    Top = 31
  end
  object dsEmp: TDataSource
    DataSet = MEmpresas
    OnDataChange = dsEmpDataChange
    Left = 78
    Top = 209
  end
  object dsOperadores: TDataSource
    DataSet = qOperadores
    Left = 118
    Top = 209
  end
  object qEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'dataIni'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'dataFin'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'dataFIni'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'dataFFin'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'operador'
        DataType = ftString
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  emp.empres_id,'
      '  emp.nome,'
      '  df.data_fecha,'
      '  df.data_venc,'
      '  sum(cc.debito - cc.credito) as total'
      'from contacorrente cc'
      'join conveniados c on c.conv_id = cc.conv_id'
      'join empresas emp on emp.empres_id = c.empres_id'
      'join dia_fecha df on df.empres_id = emp.empres_id'
      'where cc.data_fecha_emp between :dataIni and :dataFin'
      'and df.data_fecha between :dataFIni and :dataFFin'
      
        'and coalesce(emp.responsavel_fechamento,'#39#39') = coalesce(:operador' +
        ',emp.responsavel_fechamento,'#39#39')'
      'group by emp.empres_id, emp.nome, df.data_fecha, df.data_venc')
    Left = 76
    Top = 297
    object qEmpempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qEmpnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEmpdata_fecha: TDateTimeField
      FieldName = 'data_fecha'
    end
    object qEmpdata_venc: TDateTimeField
      FieldName = 'data_venc'
    end
    object qEmptotal: TBCDField
      FieldName = 'total'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object qOperadores: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select '#39'<TODOS>'#39' NOME from USUARIOS'
      'union'
      'select distinct NOME from USUARIOS'
      'where apagado <> '#39'S'#39' and liberado = '#39'S'#39' order by nome')
    Left = 108
    Top = 297
    object qOperadoresNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
  end
  object MEmpresas: TJvMemoryData
    FieldDefs = <
      item
        Name = 'EMPRES_ID'
        DataType = ftInteger
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DATA_FECHA'
        DataType = ftDateTime
      end
      item
        Name = 'DATA_VENC'
        DataType = ftDateTime
      end
      item
        Name = 'TOTAL'
        DataType = ftFloat
      end
      item
        Name = 'MARCADO'
        DataType = ftBoolean
      end>
    Left = 76
    Top = 265
    object MEmpresasNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 60
    end
    object MEmpresasDATA_FECHA: TDateTimeField
      DisplayLabel = 'Data Fechamento'
      FieldName = 'DATA_FECHA'
    end
    object MEmpresasDATA_VENC: TDateTimeField
      DisplayLabel = 'Data Vencimento'
      FieldName = 'DATA_VENC'
    end
    object MEmpresasTOTAL: TFloatField
      DisplayLabel = 'Total'
      FieldName = 'TOTAL'
      DisplayFormat = '##0.00'
    end
    object MEmpresasMARCADO: TBooleanField
      FieldName = 'MARCADO'
    end
    object MEmpresasEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
end
