object FSaldoMes: TFSaldoMes
  Left = 278
  Top = 162
  Width = 316
  Height = 335
  Caption = 'Atualiza'#231#227'o do cadastro de conveniados.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 300
    Height = 141
    Align = alTop
    Caption = 'Campo Saldo M'#234's'
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 91
      Width = 191
      Height = 13
      Caption = 'Data Inicial                              Data Final'
    end
    object ChSaldoMes: TCheckBox
      Left = 16
      Top = 19
      Width = 153
      Height = 17
      Caption = 'Atualizar este campo'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object RBFecha: TRadioButton
      Left = 16
      Top = 46
      Width = 185
      Height = 17
      Caption = 'Atualizar por data de fechamento'
      TabOrder = 1
      OnClick = RBFechaClick
    end
    object RBPeriodo: TRadioButton
      Left = 16
      Top = 65
      Width = 121
      Height = 15
      Caption = 'Atualizar por per'#237'odo'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = RBFechaClick
    end
    object datainiSM: TJvDateEdit
      Left = 16
      Top = 107
      Width = 121
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
    end
    object datafinSM: TJvDateEdit
      Left = 159
      Top = 107
      Width = 121
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 141
    Width = 300
    Height = 112
    Align = alTop
    Caption = 'Campo Saldo Total'
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 44
      Width = 214
      Height = 13
      Caption = 'Informe o per'#237'odo para a soma do saldo total:'
    end
    object Label2: TLabel
      Left = 16
      Top = 64
      Width = 191
      Height = 13
      Caption = 'Data Inicial                              Data Final'
    end
    object ChSaldoTotal: TCheckBox
      Left = 16
      Top = 20
      Width = 153
      Height = 17
      Caption = 'Atualizar este campo'
      TabOrder = 0
    end
    object datafinST: TJvDateEdit
      Left = 159
      Top = 80
      Width = 121
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 2
    end
    object datainiST: TJvDateEdit
      Left = 16
      Top = 80
      Width = 121
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 253
    Width = 300
    Height = 44
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 2
    object Butatua: TButton
      Left = 112
      Top = 16
      Width = 88
      Height = 25
      Caption = '&Atualizar'
      ModalResult = 1
      TabOrder = 0
    end
    object ButCancel: TButton
      Left = 200
      Top = 16
      Width = 88
      Height = 25
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
