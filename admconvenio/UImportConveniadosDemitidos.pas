unit UImportConveniadosDemitidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, Mask, ToolEdit, ExtCtrls, Buttons,
  DB, ZAbstractRODataset, ZDataset, {JvLookup,} ADODB, ZStoredProcedure,
  ZSqlUpdate, ZAbstractDataset, JvToolEdit, ShellApi, JvExControls,
  JvDBLookup, JvExMask, frxGradient, frxClass, frxDBSet, frxExportPDF;

type
  TFImportConveniadosDemitidos = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Bevel1: TBevel;
    //FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    RichEdit1: TRichEdit;
    ProgressBar1: TProgressBar;
    Empresas: TJvDBLookupCombo;
    DSEmpresas: TDataSource;
    Table1: TADOTable;
    FilenameEdit1: TJvFilenameEdit;
    QConv: TADOQuery;
    QCartao: TADOQuery;
    QCartaoCARTAO_ID: TIntegerField;
    QCartaoCONV_ID: TIntegerField;
    QCartaoNOME: TStringField;
    QCartaoLIBERADO: TStringField;
    QCartaoCODIGO: TIntegerField;
    QCartaoDIGITO: TWordField;
    QCartaoTITULAR: TStringField;
    QCartaoJAEMITIDO: TStringField;
    QCartaoAPAGADO: TStringField;
    QCartaoLIMITE_MES: TBCDField;
    QCartaoCODCARTIMP: TStringField;
    QCartaoPARENTESCO: TStringField;
    QCartaoDATA_NASC: TDateTimeField;
    QCartaoNUM_DEP: TIntegerField;
    QCartaoFLAG: TStringField;
    QCartaoDTEMISSAO: TDateTimeField;
    QCartaoCPF: TStringField;
    QCartaoRG: TStringField;
    QCartaoVIA: TIntegerField;
    QCartaoDTAPAGADO: TDateTimeField;
    QCartaoDTALTERACAO: TDateTimeField;
    QCartaoOPERADOR: TStringField;
    QCartaoDTCADASTRO: TDateTimeField;
    QCartaoOPERCADASTRO: TStringField;
    QCartaoCRED_ID: TIntegerField;
    QCartaoATIVO: TStringField;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasUSA_COD_IMPORTACAO: TStringField;
    QSaldo: TADOQuery;
    QSaldosaldo_conf: TBCDField;
    QSaldosaldo_nconf: TBCDField;
    QSaldoFECHAMENTO: TWideStringField;
    QSaldofaturada: TStringField;
    QSaldofatura_id: TIntegerField;
    QSaldodata_fatura: TDateTimeField;
    QSaldotipo: TStringField;
    QSaldototal: TBCDField;
    QConvCHAPA: TFloatField;
    QConvTITULAR: TStringField;
    QConvCODCARTIMP: TStringField;
    QConvSALDO_DEVEDOR: TBCDField;
    sd: TSaveDialog;
    frxReport1: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    dbConveniados: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    SaveDialog1: TSaveDialog;
    QConvEMPRES_ID: TIntegerField;
    QConvNOME: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButAjudaClick(Sender: TObject);
    procedure EmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImportConveniadosDemitidos: TFImportConveniadosDemitidos;

implementation

uses DM, UChangeLog, UMenu, cartao_util, UValidacao,URotinasTexto;

{$R *.dfm}

procedure TFImportConveniadosDemitidos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end;
end;

procedure TFImportConveniadosDemitidos.FormCreate(Sender: TObject);
begin
  QEmpresas.Open;
  FilenameEdit1.Text:= '"'+FMenu.GetPersonalFolder+'\"';
end;

procedure TFImportConveniadosDemitidos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresas.Close;
end;

procedure TFImportConveniadosDemitidos.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFImportConveniadosDemitidos.EmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
    Perform(WM_NEXTDLGCTL,0,0);
  end;
end;

procedure TFImportConveniadosDemitidos.Button1Click(Sender: TObject);
var path, nome, chapa, sql, conveniados : String;
  prox_fechamento : TDateTime;
  conv_id, qtdImp, qtdErr, grupo_conv_emp : Integer;
  saldo_devedor, saldo_faturado, saldo : Double;
  lista : TStringlist;
  erro: Boolean;
begin
  qtdImp:= 0; qtdErr:= 0;
  erro:= False;
  screen.Cursor  := crHourGlass;
  if Empresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  try
    path := '';
    if versaoOffice < 12 then
    begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end
    else
    begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
    Table1.Active := False;
    Table1.ConnectionString := path;
    Table1.TableName:= 'Plan1$';
    Table1.Active := True;
  except on E:Exception do
    ShowMessage('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  ProgressBar1.Position := 0;
  Button1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
  QConv.Open;
  QCartao.Open;
  lista := TStringList.Create;
  conveniados := '';
  while not Table1.Eof do
  begin
    conveniados := conveniados + Table1.FieldByName('CONV_ID').AsString + ',';
    //OBTEM O SALDO DEVEDOR
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT Coalesce(SUM(debito-credito),0) FROM contacorrente WHERE COALESCE(baixa_conveniado,''N'')=''N'' AND conv_id =' + Table1.FieldByName('CONV_ID').AsString);
    DMConexao.AdoQry.Open;
    saldo_devedor := DMConexao.AdoQry.Fields[0].Value;

    //OBTEM O SALDO DEVEDOR FATURADO
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT coalesce(SUM(debito-credito),0) FROM contacorrente WHERE COALESCE(baixa_conveniado,''N'')=''N'' AND COALESCE(fatura_id,0)>0 AND conv_id =' + Table1.FieldByName('CONV_ID').AsString);
    DMConexao.AdoQry.Open;
    saldo_faturado := DMConexao.AdoQry.Fields[0].Value;

    //OBTEM O PROXIMO FECHAMENTO
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add(' select min(data_fecha) from dia_fecha where data_fecha > '+QuotedStr(FormatDateTime('dd/mm/yyyy',Date)));
    DMConexao.AdoQry.SQL.Add(' and empres_id = ' + Empresas.KeyValue + 'and data_fecha not in ');
    DMConexao.AdoQry.SQL.Add(' (Select fechamento from fatura where apagado <> ''S'' and tipo = ''E'' and id = ' + Empresas.KeyValue + ')');
    DMConexao.AdoQry.SQL.Add(' and data_fecha not in (Select fechamento from fatura where apagado <> ''S'' and tipo = ''C'' and id = ' + Table1.FieldByName('CONV_ID').AsString+ ')');
    DMConexao.AdoQry.Open;
    prox_fechamento := DMConexao.AdoQry.Fields[0].Value;

    try
      DMConexao.AdoCon.BeginTrans;
      sql := '';
      sql := 'UPDATE CONVENIADOS SET data_demissao =' + QuotedStr(FormatDateTime('dd/mm/yyyy',Date));
      sql := sql + ', saldo_devedor =' + fnsubstituiString(',','.',FloatToStr(saldo_devedor));
      sql := sql + ',  saldo_devedor_fat =' + fnsubstituiString(',','.',FloatToStr(saldo_faturado));
      sql := sql + ', liberado = ''N''';
      sql := sql + ' WHERE conv_id = ' + Table1.FieldByName('CONV_ID').AsString;
      DMConexao.ExecuteSql(sql);

      sql := 'UPDATE contacorrente SET data_fecha_emp = '+QuotedStr(FormatDateTime('dd/mm/yyyy',prox_fechamento))+' WHERE fatura_id = 0';
      sql := sql + ' AND COALESCE(baixa_conveniado,''N'') <> ''S'' AND conv_id =' + Table1.FieldByName('CONV_ID').AsString;
      sql := sql + ' and data_fecha_emp > '+QuotedStr(FormatDateTime('dd/mm/yyyy',prox_fechamento));
      DMConexao.ExecuteSql(sql);
      DMConexao.AdoCon.CommitTrans;

      //OBTEM O VALOR DAS COMPRAS FUTURAS
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT (SELECT coalesce(sum(case when coalesce(cc.entreg_nf,''N'')  = ''S'' then cc.debito-cc.credito else 0 end),0))+');
      DMConexao.AdoQry.SQL.Add(' coalesce(sum(case when coalesce(cc.entreg_nf,''N'') <> ''S'' then cc.debito-cc.credito else 0 end),0)as total ');
      DMConexao.AdoQry.SQL.Add(' from DIA_FECHA ');
      DMConexao.AdoQry.SQL.Add(' join CONTACORRENTE cc on cc.CONV_ID = ' + Table1.FieldByName('CONV_ID').AsString);
      DMConexao.AdoQry.SQL.Add(' and cc.DATA_FECHA_EMP = DIA_FECHA.DATA_FECHA ');
      DMConexao.AdoQry.SQL.Add(' Where DIA_FECHA.EMPRES_ID = cc.empres_id AND ');
      DMConexao.AdoQry.SQL.Add(' DIA_FECHA.DATA_FECHA > current_timestamp ');
      DMConexao.AdoQry.Open;

      saldo := DMConexao.AdoQry.Fields[0].Value;

      DMConexao.AdoCon.BeginTrans;
      sql := '';
      sql := 'UPDATE CONVENIADOS SET saldo_devedor =' + fnsubstituiString(',','.',FloatToStr(saldo));
      sql := sql + ' WHERE conv_id = ' + Table1.FieldByName('CONV_ID').AsString;
      DMConexao.ExecuteSql(sql);
      DMConexao.AdoCon.CommitTrans;


    except
      on E:Exception do
      begin
        DMConexao.AdoCon.RollbackTrans;
        erro:= True;
        qtdErr:= qtdErr + 1;
      end;
    end;
    Table1.Next;
    Application.ProcessMessages;
    ProgressBar1.Position := ProgressBar1.Position +1;
  end;
  Button1.Enabled := True;
  screen.Cursor := crDefault;
  Table1.Close;

  QConv.Close;
  QConv.SQL.Clear;
  QConv.SQL.Add(' Select');
  QConv.SQL.Add(' CONV.CHAPA,CONV.TITULAR, CART.CODCARTIMP, COALESCE(CONV.SALDO_DEVEDOR,0.00) AS SALDO_DEVEDOR, CONV.EMPRES_ID, EMP.NOME');
  QConv.SQL.Add(' from CONVENIADOS CONV');
  QConv.SQL.Add(' inner join CARTOES CART ON CART.CONV_ID = CONV.CONV_ID');
  QConv.SQL.Add(' inner join EMPRESAS EMP ON EMP.EMPRES_ID = CONV.EMPRES_ID');
  QConv.SQL.Add(' WHERE CART.APAGADO = ''N'' AND CART.TITULAR = ''S''');
  QConv.SQL.Add(' AND CONV.CONV_ID IN ('+ COPY(conveniados,0,LENGTH(conveniados) -1)+ ')');
  QConv.SQL.Add(' ORDER BY CONV.TITULAR');
  QConv.Open;

  if QConv.IsEmpty then
    begin
      MsgInf('N�o foi .');
      abort;
    end;
  sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      frxReport1.ShowReport;
      frxReport1.Export(frxPDFExport1);
  end;

  lista.SaveToFile(FMenu.GetPersonalFolder+'\erros.txt');
  lista.Free;
  ProgressBar1.Position := 0;
  if erro then
  begin
    MsgInf('A Importa��o foi efetuada, por�m, ocorreram '+IntToStr(qtdErr)+' erro(s) na importa��o. ');
  end
  else
  begin
    MsgInf('Importa��o Realizada com Sucesso.');
  end;
end;

end.
