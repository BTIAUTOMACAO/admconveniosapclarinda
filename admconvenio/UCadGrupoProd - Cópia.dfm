inherited FCadGrupoEstab: TFCadGrupoEstab
  Left = 143
  Top = 106
  Caption = 'Cadastro de Grupo de Estabelecimentos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = TabFicha
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 701
        end
      end
      inherited DBGrid1: TJvDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'GRUPO_PROD_ID'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel3: TPanel
        object Label3: TLabel
          Left = 40
          Top = 24
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 96
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = dbEdtDesc
        end
        object DBEdit1: TDBEdit
          Left = 40
          Top = 40
          Width = 49
          Height = 21
          TabStop = False
          Color = clBtnFace
          DataField = 'GRUPO_PROD_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 0
        end
        object dbEdtDesc: TDBEdit
          Left = 96
          Top = 40
          Width = 618
          Height = 21
          CharCase = ecUpperCase
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 403
          Width = 764
          Height = 72
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 2
          object Label5: TLabel
            Left = 24
            Top = 20
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
            FocusControl = DBEdit3
          end
          object Label6: TLabel
            Left = 157
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit4
          end
          object DBEdit3: TDBEdit
            Left = 24
            Top = 36
            Width = 113
            Height = 21
            DataField = 'DTCADASTRO'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 160
            Top = 36
            Width = 161
            Height = 21
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from grupo_prod where grupo_prod_id = 0')
    object QCadastroGRUPO_PROD_ID: TIntegerField
      FieldName = 'GRUPO_PROD_ID'
      ReadOnly = True
      Required = True
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Required = True
      Size = 60
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroVALE_MAXIMO: TFloatField
      FieldName = 'VALE_MAXIMO'
    end
    object QCadastroVALE_PERC: TFloatField
      FieldName = 'VALE_PERC'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
end
