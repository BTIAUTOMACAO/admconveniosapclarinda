inherited FRelExtratoConv: TFRelExtratoConv
  Left = 57
  Top = -8
  ActiveControl = DataFecha
  Caption = 'Extrato do Conveniado'
  ClientHeight = 746
  ClientWidth = 1301
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox [0]
    Left = 0
    Top = 23
    Width = 1301
    Height = 89
    Align = alTop
    Caption = 'Selecione dia de fechamento ou per'#237'odo'
    TabOrder = 1
    object Label2: TLabel
      Left = 200
      Top = 20
      Width = 93
      Height = 13
      Caption = 'Dia de Fechamento'
    end
    object Bevel1: TBevel
      Left = 184
      Top = 16
      Width = 2
      Height = 66
    end
    object ButListaEmp: TButton
      Left = 320
      Top = 34
      Width = 137
      Height = 24
      Caption = 'Listar Empresas (F5)'
      TabOrder = 1
      OnClick = ButListaEmpClick
    end
    object DataFecha: TJvDateEdit
      Left = 202
      Top = 36
      Width = 107
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 2
    end
    object CheckBaixados: TCheckBox
      Left = 200
      Top = 66
      Width = 97
      Height = 16
      Caption = 'Incluir Baixados'
      TabOrder = 5
    end
    object por_dia_fecha: TRadioButton
      Left = 24
      Top = 24
      Width = 137
      Height = 17
      Caption = 'Por dia de Fechamento'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = por_dia_fechaClick
    end
    object por_periodo: TRadioButton
      Left = 24
      Top = 48
      Width = 129
      Height = 17
      Caption = 'Por Per'#237'odo'
      TabOrder = 4
      OnClick = por_periodoClick
    end
    object datafin: TJvDateEdit
      Left = 322
      Top = 36
      Width = 107
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
      Visible = False
    end
    object CheckEmp: TCheckBox
      Left = 320
      Top = 66
      Width = 193
      Height = 17
      Caption = 'Somente empresas com movimento'
      TabOrder = 6
    end
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 112
    Width = 1301
    Height = 634
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = '&Empresas'
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1293
        Height = 508
        Align = alClient
        DataSource = DSEmpresas
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        OnKeyDown = JvDBGrid1KeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = JvDBGrid1TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'Empres ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dataini'
            Title.Caption = 'Data Fecha'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'datafin'
            Title.Caption = 'Data Venc'
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 508
        Width = 1293
        Height = 46
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 1
        object ButMarcaDesmEmp: TButton
          Left = 7
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F12)'
          TabOrder = 0
          OnClick = ButMarcaDesmEmpClick
        end
        object ButMarcaTodasEmp: TButton
          Left = 115
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F6)'
          TabOrder = 1
          OnClick = ButMarcaTodasEmpClick
        end
        object ButDesmarcaTodosEmp: TButton
          Left = 224
          Top = 9
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F7)'
          TabOrder = 2
          OnClick = ButDesmarcaTodosEmpClick
        end
      end
      object pnlOutrasOpcoes: TPanel
        Left = 0
        Top = 554
        Width = 1293
        Height = 52
        Align = alBottom
        BevelOuter = bvSpace
        BorderStyle = bsSingle
        TabOrder = 2
        object rgpNFEntregue: TRadioGroup
          Left = 6
          Top = 2
          Width = 479
          Height = 41
          Caption = 'Selecione uma op'#231#227'o p/ Entrega NF'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Todas as autoriza'#231#245'es'
            'Somente Confirmadas'
            'Somente N'#227'o Confirmadas')
          TabOrder = 0
          OnClick = rgpNFEntregueClick
        end
        object Button2: TButton
          Left = 656
          Top = 17
          Width = 161
          Height = 25
          Caption = '&Visualizar Titulares >>'
          TabOrder = 1
          OnClick = Button2Click
        end
      end
    end
    object TabConv: TTabSheet
      Caption = '&Titulares'
      ImageIndex = 2
      OnShow = TabConvShow
      object Panel1: TPanel
        Left = 0
        Top = 444
        Width = 1293
        Height = 162
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 1
        DesignSize = (
          1289
          158)
        object Label1: TLabel
          Left = 7
          Top = 54
          Width = 121
          Height = 13
          Caption = 'Mensagem para o Extrato'
        end
        object Bevel3: TBevel
          Left = 0
          Top = 46
          Width = 1289
          Height = 2
          Anchors = [akLeft, akTop, akRight]
        end
        object lblEntregaNF: TLabel
          Left = 670
          Top = 23
          Width = 69
          Height = 13
          Caption = 'Entrega NF:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ButMarcDesmConv: TButton
          Left = 1
          Top = 19
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F11)'
          TabOrder = 3
          OnClick = ButMarcDesmConvClick
        end
        object ButMarcaTodosConv: TButton
          Left = 109
          Top = 19
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F8)'
          TabOrder = 4
          OnClick = ButMarcaTodosConvClick
        end
        object ButDesmTodosConv: TButton
          Left = 218
          Top = 19
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F9)'
          TabOrder = 5
          OnClick = ButDesmTodosConvClick
        end
        object BitBtn1: TBitBtn
          Left = 487
          Top = 8
          Width = 92
          Height = 29
          Caption = '&Visualizar'
          TabOrder = 1
          OnClick = BitBtn1Click
          Glyph.Data = {
            E6040000424DE604000000000000360000002800000014000000140000000100
            180000000000B0040000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFC6CED2A8ADAFB0B1B2A8A6A6868585919394A69698987879A29A9AB4B5B5
            B2B3B4B2B7BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B8BAAAAB
            ACC2C2C2E7E6E6DADADAA0A1A19994945B4A494A4141606060858585AEAEADC2
            C1C1ACADAEB1B5B7FFFFFFFFFFFFFFFFFFBAC1C5A6A7A9B8B8B8E5E5E5F1F1F1
            EAEAEACECECE9999999696965858583939394545454F4F4F6E6E6E969595BAB9
            B9B7B7B7A1A2A4FFFFFFFFFFFFAEB0B1DBDBDAFAFAFAF3F3F3EDEDEDCDCDCDA2
            A2A27D7D7D8F8F8FA6A6A6A2A2A28A8A8A6F6F6F6D6D6D5C5C5C7171719A9B9B
            ACB2B4FFFFFFFFFFFFC3C3C2FFFFFFF2F2F2D0D0D09595959999999E9E9E7878
            787070707171718080809C9C9CAEAEAEA7A7A79090908F8B8DB1B0B2B7BEC1FF
            FFFFFFFFFFAFAFAED5D5D5939393959595C0C0C0C3C3C3C8C8C8BFBFBFA2A2A2
            9191918787877777776F6F6F828282A3A4A36AA27B7FA08BBBBDC5FFFFFFFFFF
            FF7F7F7E939393CDCDCDD7D7D7C7C7C7C1C1C1DADADAC5C5C5CDCDCDC9C9C9C2
            C2C2BDBDBDB5B5B59B9B9B7B7B7B757173848385B6BDC0FFFFFFFFFFFF979695
            F0F0F0D2D2D2C6C6C6C2C2C2DBDBDBBEBEBEC7C7C7C8C8C8B8B8B8B0B0B0BDBD
            BDBDBDBDC1C1C1CFCFCFC7C7C7A3A4A4B4BABEFFFFFFFFFFFFB3B7B8CBCBCBC6
            C6C6C3C3C3CCCCCCB8B8B8DDDDDDF5F5F5F2F2F2E9E9E9DFDFDFD4D4D4BFBFBF
            B1B1B1B1B1B1B1B0B0BBBDBEC1C9CDFFFFFFFFFFFFC4CCCFBDC2C5A5A7A8A8A8
            A8C3C4C4B5B7B8B0B1B1D1D1D1E0E0E0E1E1E1E6E6E6E9EAEAE9E9E9DFDFDFC0
            BFBF9D9E9EBBC1C5CBD3D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFFFE
            CEC7C1A9ADB0A1A5AA9EA1A4A6A8AAB6B7B9C2B6B6C1B7B7B3B4B4A7A9AABBC2
            C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1AFB0DEB094FED5A5F4
            CBA2EECAA7ECD2B7E3D3C2D6CBC1AB8D8DAAA5A7BEC6CAFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8A8A8E3AC86FFD2A1FFCE9EFFCF
            9FFFD0A0FFD1A2F0C09BAD8D8DCAD5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFB49A95FDD5ADFFD7B0FFD6B0FFD6B0FFD6B0
            FFDDB4C39A8DAF9495FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFB79188FFECD0FFE3C9FFE3C9FFE3C9FFE4CAFDE3C9B0
            8B89BEBCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFB2A7AADABBAFFFEFDBFFEBD8FFEBD8FFEBD8FFF2DEDCBFB4AA8383FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA
            A2A3FFFFF8FFFFF8FFFFF8FFFFF8FFFFF9FFFFFEC4A5A1AB9394FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA89799B99797CBB0
            B0CAB0B0CAB0B0CAB0B0CAB1B0C9ADACB59999C2C2C6FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C1C5BDBABDBBB8BBBBB8BB
            BBB8BBBBB8BBBBB8BBBBB7BAC3C5C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF}
        end
        object TipoRel: TRadioGroup
          Left = 1
          Top = 93
          Width = 1287
          Height = 64
          Align = alBottom
          Caption = 'Modelo do Relat'#243'rio'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Seq'#252'encial'
            'Um por folha'
            'Dois por folha'
            'Quatro por folha'
            'Quatro por folhaTotaliza'#231#227'o por Fornec.'
            'Totaliza'#231#227'o por Fornecedor (Seq'#252'encial)'
            'Vers'#227'o para confer'#234'ncia sem fornecedor'
            'Vers'#227'o para confer'#234'ncia sem fornecedor com Tot. Conv.')
          TabOrder = 11
          OnClick = TipoRelClick
        end
        object ChKMostraChapa: TCheckBox
          Left = 325
          Top = 19
          Width = 161
          Height = 17
          Caption = 'Mostrar chapa no lugar do ID'
          TabOrder = 6
        end
        object EdMsg: TEdit
          Left = 7
          Top = 69
          Width = 490
          Height = 21
          TabOrder = 9
        end
        object ckNaoAgruparPorEmpresa: TCheckBox
          Left = 511
          Top = 80
          Width = 291
          Height = 17
          Caption = 'Ordenar por Nome de Titular desconsiderando Empresa'
          TabOrder = 10
          Visible = False
          OnClick = ckNaoAgruparPorEmpresaClick
        end
        object ckQuebraPagina: TCheckBox
          Left = 511
          Top = 64
          Width = 195
          Height = 17
          Caption = 'Imprimir uma Empresa por P'#225'gina'
          Checked = True
          State = cbChecked
          TabOrder = 8
        end
        object CKHistAuts: TCheckBox
          Left = 511
          Top = 48
          Width = 145
          Height = 17
          Caption = 'Exibir Hist'#243'rico das Auts.'
          TabOrder = 7
          Visible = False
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 322
          Height = 16
          BevelOuter = bvLowered
          Color = clWindow
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 2
            Width = 144
            Height = 13
            Caption = 'Valor Total dos Titulares:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblTotal: TLabel
            Left = 288
            Top = 3
            Width = 26
            Height = 13
            Alignment = taRightJustify
            Caption = '0,00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object bntGerarPDF: TBitBtn
          Left = 578
          Top = 8
          Width = 90
          Height = 29
          Caption = '&Gerar PDF'
          TabOrder = 2
          OnClick = bntGerarPDFClick
          Glyph.Data = {
            F6060000424DF606000000000000360000002800000018000000180000000100
            180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
            FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
            FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
            9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
            FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
            FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
            E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
            B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
            FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
            FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
            F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
            F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
            F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
            F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
            F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
            EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
            F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
            EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
            EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
            A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
            00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
            3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
            99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
            85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
            AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
            FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
            03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
            3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
            FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
            1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
            BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        end
      end
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1293
        Height = 444
        Hint = 
          'Todas as ordena'#231#245'es seguir'#227'o a ordem de empresa e depois a ordem' +
          ' do campo clicado.'
        Align = alClient
        DataSource = DSConv
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid2DblClick
        OnKeyDown = JvDBGrid2KeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = JvDBGrid2TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'conv_id'
            Title.Caption = 'Conv ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'chapa'
            Title.Caption = 'Chapa'
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titular'
            Width = 327
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'total'
            Title.Caption = 'Valor'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'Empres ID'
            Width = 60
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'E&stabelecimentos'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 428
        Width = 779
        Height = 40
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 0
        object but_MarcDem_For: TButton
          Left = 64
          Top = 6
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F2)'
          TabOrder = 0
          OnClick = but_MarcDem_ForClick
        end
        object But_MarcaT_For: TButton
          Left = 290
          Top = 6
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F3)'
          TabOrder = 1
          OnClick = But_MarcaT_ForClick
        end
        object But_DesmT_For: TButton
          Left = 490
          Top = 6
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F4)'
          TabOrder = 2
          OnClick = But_DesmT_ForClick
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 779
        Height = 46
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 1
        object but_Abre_For: TButton
          Left = 10
          Top = 8
          Width = 120
          Height = 25
          Caption = '&Abrir Estabelecimentos'
          TabOrder = 0
          OnClick = but_Abre_ForClick
        end
        object But_Fecha_For: TButton
          Left = 131
          Top = 8
          Width = 132
          Height = 25
          Caption = '&Fechar Estabelecimentos'
          TabOrder = 1
          OnClick = But_Fecha_ForClick
        end
      end
      object DBGrid1: TJvDBGrid
        Left = 0
        Top = 46
        Width = 779
        Height = 382
        Align = alClient
        DataSource = DSFornec
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        OnKeyDown = DBGrid1KeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = DBGrid1TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'cred_id'
            Title.Caption = 'Estab. ID'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'fantasia'
            Width = 292
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'segmento'
            Width = 272
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Width = 306
            Visible = True
          end>
      end
    end
  end
  inherited Panel6: TPanel
    Width = 1301
  end
  inherited PopupBut: TPopupMenu
    Left = 212
    Top = 152
  end
  object DSEmpresas: TDataSource
    DataSet = MEmpresas
    Left = 76
    Top = 393
  end
  object DSConv: TDataSource
    DataSet = MConv
    Left = 36
    Top = 393
  end
  object dbContaC: TfrxDBDataset
    UserName = 'dbContaC'
    CloseDataSource = False
    DataSet = cdsContaC
    BCDToCurrency = False
    Left = 632
    Top = 312
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 720
    Top = 311
  end
  object frxConv: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41129.730737118100000000
    ReportOptions.LastChange = 41457.849011319400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'var c : Integer = 0; cFolha : Integer = 0;                      ' +
        '                                                             '
      '    seeFooter : Boolean = False;'
      
        '    totalConfDeb, totalNConfDeb : Double = 0;                   ' +
        '                                             '
      '    totalConfCred, totalNConfCred  : Double = 0;'
      '    totalConvDeb, totalConvCred : Double = 0;'
      
        '    totalConfMaster, totalNConfMaster, totalConvMaster : Double ' +
        '= 0;        '
      '      '
      'procedure Header1OnBeforePrint(Sender: TfrxComponent);'
      'begin  '
      
        '  Header1.Visible := (<dbConv."CONV_ID"> = <conv>) and (<dbConv.' +
        '"EMPRES_ID"> = <emp>);'
      'end;'
      ''
      'procedure SubdetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '//  showmessage('#39'Conv_id = '#39' + inttostr(<dbContaC."CONV_ID">) + ' +
        #39' Conv_id 2 '#39'+ inttostr(<dbConv."CONV_ID">));'
      
        '//  showmessage('#39'Empres_id = '#39' + inttostr(<dbContaC."EMPRES_ID">' +
        ') + '#39' Empres_id 2 '#39'+ inttostr(<dbConv."EMPRES_ID">));      '
      
        '  SubdetailData1.Visible := (<dbContaC."CONV_ID"> = <dbConv."CON' +
        'V_ID">)'
      
        '         and (<dbContaC."EMPRES_ID"> = <dbEmpresas."EMPRES_ID">)' +
        ';'
      '  seeFooter := true;                                '
      '  if SubdetailData1.Visible then begin'
      
        '    if c mod 2 = 0 then begin                                   ' +
        '                                           '
      '      gradientSD.BeginColor := clWhite;'
      '      gradientSD.EndColor   := clWhite;'
      '      gradientSD.Color      := clWhite;'
      '    end else begin'
      '      gradientSD.BeginColor := clSilver;'
      '      gradientSD.EndColor   := clSilver;'
      '      gradientSD.Color      := clSilver;        '
      '    end;'
      '    c := c + 1;        '
      '  end;            '
      'end;'
      ''
      'procedure DetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  DetailData1.Visible := (<dbConv."EMPRES_ID"> = <dbEmpresas."EM' +
        'PRES_ID">)  '
      '                     and (not (<TpRel> in [6,7]) ) ;'
      'end;'
      ''
      'procedure dbContaCDEBITOOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <dbContaC."ENTREG_NF"> = '#39'S'#39' then begin'
      
        '    totalConfDeb := totalConfDeb + <dbContaC."DEBITO">;         ' +
        '                                                              '
      '  end else begin'
      
        '    totalNConfDeb := totalNConfDeb + <dbContaC."DEBITO">;       ' +
        '          '
      '  end;      '
      'end;'
      ''
      'procedure dbContaCCREDITOOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <dbContaC."ENTREG_NF"> = '#39'S'#39' then begin'
      
        '    totalConfCred := totalConfCred + <dbContaC."CREDITO">;      ' +
        '                                                                ' +
        ' '
      '  end else begin'
      
        '    totalNConfCred := totalNConfCred + <dbContaC."CREDITO">;    ' +
        '             '
      '  end;'
      'end;'
      ''
      'procedure footerSDOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  footerSD.Visible := ((seeFooter) and ((totalConfDeb <> 0)     ' +
        '                 '
      '         or (totalNConfDeb  <> 0) or (totalConfCred  <> 0)'
      
        '         or (totalNConfCred <> 0))) and (<TpRel> <> 6);         ' +
        '                                       '
      
        '  if not (<TpRel> = 7) then begin                               ' +
        '                    '
      
        '    mmoTotalConf.Text  := '#39'Totalizacao confirmadas:     '#39' + Form' +
        'at('#39'%2.2m'#39', [totalConfDeb]) + '#39'               '#39' + Format('#39'%2.2m'#39 +
        ', [totalConfCred]);  '
      
        '    mmoTotalNConf.Text := '#39'Totalizacao nao confirmadas: '#39' + Form' +
        'at('#39'%2.2m'#39', [totalNConfDeb]) + '#39'               '#39' + Format('#39'%2.2m' +
        #39', [totalNConfCred]);  '
      
        '    mmoTotalConv.Text  := '#39'Totalizacao conveniado:      '#39' + Form' +
        'at('#39'%2.2m'#39', [totalConvDeb]) + '#39'               '#39' + Format('#39'%2.2m'#39 +
        ', [totalConvCred]);        '
      
        '    mmoTotalPagar.Text := '#39'Total a Pagar: '#39' + Format('#39'%2.2m'#39', [t' +
        'otalConvDeb - totalConvCred]);  '
      '  end else if (<TpRel> <> 6) then begin'
      '    //setando valores                                         '
      
        '    mmoTotalConf.Text  := '#39'Totalizacao confirmadas: '#39' + Format('#39 +
        '%2.2m'#39', [totalConfDeb - totalConfCred]);'
      
        '    mmoTotalNConf.Text := '#39'Totalizacao nao confirmadas: '#39' + Form' +
        'at('#39'%2.2m'#39', [totalNConfDeb - totalNConfCred]);'
      
        '    mmoTotalConv.Text  := '#39'Totalizacao do conveniado: '#39' + Format' +
        '('#39'%2.2m'#39', [totalConvDeb - totalConvCred]);        '
      '  end;            '
      'end;'
      ''
      'procedure footerSDOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  c := 0;'
      
        '  totalConfMaster  := totalConfMaster  + totalConfDeb  - totalCo' +
        'nfCred;                                     '
      
        '  totalNConfMaster := totalNConfMaster + totalNConfDeb - totalNC' +
        'onfCred;                                  '
      
        '  totalConvMaster  := totalConvMaster  + totalConvDeb  - totalCo' +
        'nvCred;'
      
        '  //showmessage('#39' conf '#39' + floattostr(totalConfMaster)  + '#39' Ncon' +
        'f '#39' + floattostr(totalNConfMaster) + '#39' Conv '#39' + floattostr(total' +
        'NConfMaster));      '
      '  seeFooter      := False;                                 '
      '  totalConfDeb   := 0; '
      '  totalNConfDeb  := 0;'
      '  totalConfCred  := 0;'
      '  totalNConfCred := 0;'
      '  totalConvDeb   := 0;   '
      '  totalConvCred  := 0;'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MasterData1.StartNewPage := <empPorPag>;    '
      'end;'
      ''
      'procedure Page1OnBeforePrint(Sender: TfrxComponent);'
      
        'begin                                                           ' +
        '          '
      '  if (<TpRel> = 7) then begin'
      
        '    //posi'#231#227'o dos labels                                        ' +
        '                   '
      '    mmoTotalConf.Top  := 0;'
      '    mmoTotalNConf.Top := 0;'
      '    mmoTotalConv.Top  := 0;      '
      '    //tamanho dos labels'
      '    mmoTotalConf.Width  := footerSD.Width / 3;'
      '    mmoTotalNConf.Width := footerSD.Width / 3;'
      '    mmoTotalConv.Width  := footerSD.Width / 3;      '
      
        '    mmoTotalPagar.Visible := False;                             ' +
        '                            '
      '    FooterSD.Height := mensagem.Height * 2;'
      
        '    //alinhando os mesmos em suas devidas posi'#231#245'es              ' +
        '                                                                ' +
        '                                                                ' +
        '                                   '
      '    mmoTotalConf.Left  := 0;'
      
        '    mmoTotalNConf.Left := mmoTotalConf.Left + mmoTotalConf.Width' +
        ';'
      
        '    mmoTotalConv.Left  := mmoTotalNConf.Left + mmoTotalNConf.Wid' +
        'th;            '
      '  end;                  '
      'end;'
      ''
      'procedure Footer1OnBeforePrint(Sender: TfrxComponent);'
      'begin             '
      
        '  mmoTotalConfDD.Text  := '#39'Totalizacao confirmadas: '#39' + Format('#39 +
        '%2.2m'#39', [totalConfMaster]);'
      
        '  mmoTotalNConfDD.Text := '#39'Totalizacao nao confirmadas: '#39' + Form' +
        'at('#39'%2.2m'#39', [totalNConfMaster]);'
      
        '  mmoTotalConvDD.Text  := '#39'Totalizacao do conveniado: '#39' + Format' +
        '('#39'%2.2m'#39', [totalConvMaster]);  '
      'end;'
      ''
      'procedure Footer1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  totalConfMaster  := 0;                  '
      '  totalNConfMaster := 0;                   '
      '  totalConvMaster  := 0;                                 '
      'end;'
      ''
      'procedure SubdetailData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  totalConvDeb  := totalConfDeb  + totalNConfDeb;'
      '  totalConvCred := totalConfCred + totalNConfCred;    '
      
        '  //totalConfMaster  := totalConfDeb - totalConfCred;           ' +
        '                          '
      
        '  //totalNConfMaster := totalNConfDeb - totalNConfCred;         ' +
        '                         '
      '  //totalConvMaster  := totalConvDeb - totalConvCred;'
      
        '  //showmessage('#39' conf '#39' + floattostr(totalConfMaster)  + '#39' Ncon' +
        'f '#39' + floattostr(totalNConfMaster) + '#39' Conv '#39' + floattostr(total' +
        'NConfMaster));                                  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxConvGetValue
    OnPreview = frxConvPreview
    OnClosePreview = frxConvClosePreview
    Left = 664
    Top = 240
    Datasets = <
      item
        DataSet = dbContaC
        DataSetName = 'dbContaC'
      end
      item
        DataSet = dbConv
        DataSetName = 'dbConv'
      end
      item
        DataSet = dbEmpresas
        DataSetName = 'dbEmpresas'
      end
      item
        DataSet = dbFornec
        DataSetName = 'dbFornec'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'TpRel'
        Value = Null
      end
      item
        Name = 'CHAPA'
        Value = Null
      end
      item
        Name = 'empPorPag'
        Value = Null
      end
      item
        Name = 'estabelecimentos'
        Value = Null
      end
      item
        Name = 'qtdFolha'
        Value = Null
      end
      item
        Name = 'mensagem'
        Value = Null
      end
      item
        Name = 'qtdRec'
        Value = Null
      end
      item
        Name = 'conv'
        Value = Null
      end
      item
        Name = 'emp'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Style = fsDash
      PrintOnPreviousPage = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object ReportTitle1: TfrxReportTitle
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object mmoTitulo: TfrxMemoView
          Align = baClient
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Extrato de Conveniados')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = dbEmpresas
        DataSetName = 'dbEmpresas'
        RowCount = 0
        StartNewPage = True
        object Gradient4: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          BeginColor = 15724527
          EndColor = 15724527
          Style = gsHorizontal
          Frame.Typ = [ftTop]
          Color = 15724527
        end
        object Memo1: TfrxMemoView
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Empresa:')
          ParentFont = False
        end
        object dbEmpresasNome: TfrxMemoView
          Left = 154.960730000000000000
          Width = 374.173470000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Nome'
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            '[dbEmpresas."Nome"]')
          ParentFont = False
        end
        object dbEmpresasdata_ini: TfrxMemoView
          Align = baRight
          Left = 540.472897401574800000
          Width = 83.149606299212600000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DATAINI'
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            '[dbEmpresas."DATAINI"]')
          ParentFont = False
        end
        object dbEmpresasdata_fin: TfrxMemoView
          Align = baRight
          Left = 634.961093700787400000
          Width = 83.149606299212600000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DATAFIN'
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            '[dbEmpresas."DATAFIN"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baRight
          Left = 623.622503700787400000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            'a')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 68.031540000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = dbEmpresas
          DataSetName = 'dbEmpresas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            '[dbEmpresas."EMPRES_ID"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 430.866420000000000000
        Width = 718.110700000000000000
        object Gradient5: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          EndColor = clWhite
          Style = gsHorizontal
          Frame.Typ = [ftTop]
          Frame.TopLine.Width = 2.000000000000000000
          Color = clWhite
        end
        object Memo5: TfrxMemoView
          Left = 60.472480000000000000
          Width = 56.692913390000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
        object Time: TfrxMemoView
          Left = 117.165430000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Time]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data/Hora:')
          ParentFont = False
        end
        object Page2: TfrxMemoView
          Align = baRight
          Left = 657.638220000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page]')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          Align = baRight
          Left = 691.653990000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[TotalPages#]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baRight
          Left = 684.094930000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '/')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        Height = 18.897640240000000000
        Top = 188.976500000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        DataSet = dbConv
        DataSetName = 'dbConv'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Titular :')
          ParentFont = False
        end
        object dbConvCONV_ID: TfrxMemoView
          Left = 52.913420000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = dbConv
          DataSetName = 'dbConv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[IIF(<CHAPA>,<dbConv."CHAPA">,<dbConv."CONV_ID">)]')
          ParentFont = False
        end
        object dbConvTITULAR: TfrxMemoView
          Left = 143.622140000000000000
          Width = 574.488560000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TITULAR'
          DataSet = dbConv
          DataSetName = 'dbConv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[dbConv."TITULAR"]')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        OnAfterPrint = 'SubdetailData1OnAfterPrint'
        OnBeforePrint = 'SubdetailData1OnBeforePrint'
        DataSet = dbContaC
        DataSetName = 'dbContaC'
        RowCount = 0
        object gradientSD: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          EndColor = clWhite
          Style = gsHorizontal
          Frame.Typ = [ftBottom]
          Color = clWhite
        end
        object dbContaCDATA: TfrxMemoView
          Left = -1.000000000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'DATA'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."DATA"]')
          ParentFont = False
        end
        object dbContaCAUTORIZACAO_ID: TfrxMemoView
          Left = 60.472480000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'AUTORIZACAO_ID'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."AUTORIZACAO_ID"]')
          ParentFont = False
        end
        object dbContaCNOME: TfrxMemoView
          Left = 124.724490000000000000
          Width = 188.976500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOME'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."NOME"]')
          ParentFont = False
        end
        object dbContaCFORNECEDOR: TfrxMemoView
          Left = 314.039580000000000000
          Width = 204.094488190000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'FORNECEDOR'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."FORNECEDOR"]')
          ParentFont = False
        end
        object dbContaCDEBITO: TfrxMemoView
          Left = 574.488560000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'dbContaCDEBITOOnBeforePrint'
          ShowHint = False
          DataField = 'DEBITO'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."DEBITO"]')
          ParentFont = False
        end
        object dbContaCCREDITO: TfrxMemoView
          Left = 631.181510000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'dbContaCCREDITOOnBeforePrint'
          ShowHint = False
          DataField = 'CREDITO'
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dbContaC."CREDITO"]')
          ParentFont = False
        end
        object dbContaCENTREG_NF: TfrxMemoView
          Left = 687.874460000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[iif(<dbContaC."ENTREG_NF"> = '#39'S'#39','#39'Sim'#39','#39'N'#195#163'o'#39')]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 517.795610000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = dbContaC
          DataSetName = 'dbContaC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[iif(<dbContaC."RECEITA"> = '#39'S'#39','#39'Sim'#39','#39'N'#195#163'o'#39')]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 22.677180000000000000
        Top = 143.622140000000000000
        Width = 718.110700000000000000
        OnAfterPrint = 'Footer1OnAfterPrint'
        OnBeforePrint = 'Footer1OnBeforePrint'
        object Gradient1: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          ShowHint = False
          BeginColor = 13750737
          EndColor = 13750737
          Style = gsHorizontal
          Color = 13750737
        end
        object mmoTotalConfDD: TfrxMemoView
          Top = 3.000000000000000000
          Width = 239.244094490000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mmoTotalNConfDD: TfrxMemoView
          Left = 239.110390000000000000
          Top = 3.000000000000000000
          Width = 239.244094490000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mmoTotalConvDD: TfrxMemoView
          Left = 478.220780000000000000
          Top = 3.000000000000000000
          Width = 239.622047240000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 15.118120000000000000
        Top = 230.551330000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'Header1OnBeforePrint'
        object Gradient3: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          ShowHint = False
          EndColor = clWhite
          Style = gsHorizontal
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 2.000000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Frame.BottomLine.Width = 1.000000000000000000
          Color = clWhite
        end
        object Memo6: TfrxMemoView
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 60.472480000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Autoriza'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 124.944960000000000000
          Width = 188.976377950000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Nome Cart'#195#163'o')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 314.039580000000000000
          Width = 204.094497950000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Estabelecimento')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 574.488560000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'D'#195#169'bito')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 631.181510000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Cr'#195#169'dito')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 687.874460000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Ent')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 517.795610000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Receita')
          ParentFont = False
        end
      end
      object footerSD: TfrxFooter
        Height = 64.252010000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        OnAfterPrint = 'footerSDOnAfterPrint'
        OnBeforePrint = 'footerSDOnBeforePrint'
        object Gradient2: TfrxGradientView
          Align = baClient
          Width = 718.110700000000000000
          Height = 64.252010000000000000
          ShowHint = False
          EndColor = clWhite
          Style = gsHorizontal
          Frame.Typ = [ftBottom]
          Frame.BottomLine.Style = fsDouble
          Color = clWhite
        end
        object mmoTotalConf: TfrxMemoView
          Width = 302.362204720000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mmoTotalNConf: TfrxMemoView
          Top = 26.456710000000000000
          Width = 302.362204720000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mmoTotalConv: TfrxMemoView
          Left = 415.748300000000000000
          Width = 302.362204720000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mmoTotalPagar: TfrxMemoView
          Left = 415.748300000000000000
          Top = 26.456710000000000000
          Width = 302.362204720000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object mensagem: TfrxMemoView
          Align = baBottom
          Top = 45.354360000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Memo.UTF8 = (
            '[mensagem]')
          ParentFont = False
        end
      end
    end
  end
  object dbConv: TfrxDBDataset
    UserName = 'dbConv'
    CloseDataSource = False
    DataSet = cdsConv
    BCDToCurrency = False
    Left = 528
    Top = 312
  end
  object dbEmpresas: TfrxDBDataset
    UserName = 'dbEmpresas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EMPRES_ID=EMPRES_ID'
      'NOME=NOME'
      'DATAFIN=DATAFIN'
      'DATAINI=DATAINI')
    DataSet = cdsEmpresas
    BCDToCurrency = False
    Left = 464
    Top = 312
  end
  object dbFornec: TfrxDBDataset
    UserName = 'dbFornec'
    CloseDataSource = False
    DataSet = cdsFornec
    BCDToCurrency = False
    Left = 576
    Top = 312
  end
  object DSFornec: TDataSource
    DataSet = MFornec
    Left = 120
    Top = 393
  end
  object cdsEmpresas: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterScroll = cdsEmpresasAfterScroll
    Left = 196
    Top = 240
    object cdsEmpresasEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
      Required = True
    end
    object cdsEmpresasNOME: TStringField
      FieldName = 'NOME'
      Required = True
      Size = 60
    end
    object cdsEmpresasDATAFIN: TDateTimeField
      FieldName = 'DATAFIN'
      Required = True
    end
    object cdsEmpresasDATAINI: TDateTimeField
      FieldName = 'DATAINI'
    end
  end
  object cdsFornec: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 164
    Top = 240
    object cdsFornecCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
      Required = True
    end
    object cdsFornecFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 58
    end
    object cdsFornecNOME: TStringField
      FieldName = 'NOME'
      Required = True
      Size = 60
    end
    object cdsFornecSEGMENTO: TStringField
      FieldName = 'SEGMENTO'
      Required = True
      Size = 58
    end
  end
  object cdsConv: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterScroll = cdsConvAfterScroll
    Left = 100
    Top = 240
    object cdsConvTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object cdsConvTITULAR: TStringField
      FieldName = 'TITULAR'
      Required = True
      Size = 58
    end
    object cdsConvCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object cdsConvCHAPA: TFloatField
      FieldName = 'CHAPA'
      Required = True
    end
    object cdsConvEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object cdsContaC: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    Left = 132
    Top = 240
    object cdsContaCCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object cdsContaCEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object cdsContaCDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object cdsContaCCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object cdsContaCDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object cdsContaCAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object cdsContaCENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      Size = 1
    end
    object cdsContaCDIGITO: TIntegerField
      FieldName = 'DIGITO'
    end
    object cdsContaCCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object cdsContaCFORNECEDOR: TStringField
      FieldName = 'FORNECEDOR'
      Size = 58
    end
    object cdsContaCNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object cdsContaCHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object cdsContaCNF: TIntegerField
      FieldName = 'NF'
    end
    object cdsContaCRECEITA: TStringField
      DisplayLabel = 'Receita'
      FieldName = 'RECEITA'
      Size = 1
    end
  end
  object dsCdsEmpresas: TDataSource
    DataSet = cdsEmpresas
    Left = 372
    Top = 377
  end
  object dsCdsFornec: TDataSource
    DataSet = cdsFornec
    Left = 376
    Top = 345
  end
  object dsCdsContaC: TDataSource
    DataSet = cdsContaC
    Left = 416
    Top = 345
  end
  object dsCdsConv: TDataSource
    DataSet = cdsConv
    Left = 376
    Top = 409
  end
  object frxPDFExport1: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Title = 'Extrato Empresa'
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 512
    Top = 31
  end
  object sd: TSaveDialog
    Filter = '.pdf|.pdf'
    Left = 592
    Top = 32
  end
  object QConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select Sum(Contacorrente.debito-contacorrente.credito) as total,'
      'Conveniados.titular, Conveniados.conv_id, Conveniados.chapa,'
      'Conveniados.empres_id'
      ' from contacorrente'
      'join conveniados on conveniados.conv_id = contacorrente.conv_id'
      'where contacorrente.data between '#39'05/01/2004'#39' and '#39'06/01/2004'#39
      'and conveniados.empres_id = 1'
      
        'group by Conveniados.titular, Conveniados.conv_id, Conveniados.c' +
        'hapa, '
      'Conveniados.empres_id'
      'having Sum(Contacorrente.debito-contacorrente.credito) > 0'
      '')
    Left = 36
    Top = 360
    object QConvtotal: TBCDField
      FieldName = 'total'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object QConvtitular: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'titular'
      Size = 58
    end
    object QConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QConvchapa: TFloatField
      FieldName = 'chapa'
    end
    object QConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object QContaC: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select  contacorrente.debito,contacorrente.credito, coalesce(con' +
        'tacorrente.receita,'#39'N'#39') receita,'
      
        'contacorrente.data, contacorrente.autorizacao_id, contacorrente.' +
        'entreg_nf, contacorrente.digito, '
      
        'contacorrente.cred_id, credenciados.fantasia as fornecedor, cart' +
        'oes.nome, contacorrente.historico, '
      
        'contacorrente.nf from contacorrente join credenciados on credenc' +
        'iados.cred_id = contacorrente.cred_id '
      'join cartoes on cartoes.cartao_id = contacorrente.cartao_id '
      'join conveniados on conveniados.conv_id = cartoes.conv_id'
      
        'where contacorrente.conv_id = :conv_id and contacorrente.empres_' +
        'id = :empres_id '
      'order by contacorrente.data '
      '')
    Left = 36
    Top = 304
    object QContaCdebito: TBCDField
      FieldName = 'debito'
      Precision = 15
      Size = 2
    end
    object QContaCcredito: TBCDField
      FieldName = 'credito'
      Precision = 15
      Size = 2
    end
    object QContaCreceita: TStringField
      FieldName = 'receita'
      ReadOnly = True
      Size = 1
    end
    object QContaCdata: TDateTimeField
      FieldName = 'data'
    end
    object QContaCautorizacao_id: TIntegerField
      FieldName = 'autorizacao_id'
    end
    object QContaCentreg_nf: TStringField
      FieldName = 'entreg_nf'
      FixedChar = True
      Size = 1
    end
    object QContaCdigito: TWordField
      FieldName = 'digito'
    end
    object QContaCcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QContaCfornecedor: TStringField
      FieldName = 'fornecedor'
      Size = 58
    end
    object QContaCnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QContaChistorico: TStringField
      FieldName = 'historico'
      Size = 80
    end
    object QContaCnf: TIntegerField
      FieldName = 'nf'
    end
  end
  object QFornec: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select credenciados.cred_id, credenciados.fantasia, credenciados' +
        '.nome, segmentos.descricao as segmento from credenciados'
      'join segmentos on segmentos.seg_id = credenciados.seg_id'
      'where credenciados.apagado <> '#39'S'#39
      'order by credenciados.fantasia'
      '')
    Left = 116
    Top = 360
    object QForneccred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QFornecfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object QFornecnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QFornecsegmento: TStringField
      FieldName = 'segmento'
      Size = 58
    end
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        ' Select empresas.empres_id, empresas.nome, (dia_fecha.data_fecha' +
        ' - 1) as datafin, '
      ' (dia_fecha.data_fecha - 1) as dataini'
      ' from empresas '
      ' join dia_fecha on dia_fecha.empres_id = empresas.empres_id '
      ' where dia_fecha.data_fecha = '#39'01/01/2004'#39
      ' order by empresas.nome')
    Left = 76
    Top = 360
    object QEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasdatafin: TDateTimeField
      FieldName = 'datafin'
      ReadOnly = True
    end
    object QEmpresasdataini: TDateTimeField
      FieldName = 'dataini'
      ReadOnly = True
    end
  end
  object MConv: TJvMemoryData
    FieldDefs = <
      item
        Name = 'conv_id'
        DataType = ftInteger
      end
      item
        Name = 'titular'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'total'
        DataType = ftFloat
      end
      item
        Name = 'chapa'
        DataType = ftInteger
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end>
    Left = 36
    Top = 424
    object MConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object MConvtitular: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'titular'
      Size = 60
    end
    object MConvtotal: TFloatField
      FieldName = 'total'
      DisplayFormat = '##0.00'
    end
    object MConvchapa: TIntegerField
      FieldName = 'chapa'
    end
    object MConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object MConvmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
  object MEmpresas: TJvMemoryData
    FieldDefs = <
      item
        Name = 'empres_id'
        DataType = ftInteger
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'datafin'
        DataType = ftDateTime
      end
      item
        Name = 'dataini'
        DataType = ftDateTime
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end>
    Left = 76
    Top = 424
    object MEmpresasempres_id: TIntegerField
      DisplayLabel = 'Empresa ID'
      FieldName = 'empres_id'
    end
    object MEmpresasnome: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'nome'
      Size = 60
    end
    object MEmpresasdatafin: TDateTimeField
      FieldName = 'datafin'
    end
    object MEmpresasdataini: TDateTimeField
      FieldName = 'dataini'
    end
    object MEmpresasmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
  object MFornec: TJvMemoryData
    FieldDefs = <
      item
        Name = 'cred_id'
        DataType = ftInteger
      end
      item
        Name = 'fantasia'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'segmento'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end>
    Left = 116
    Top = 424
    object MForneccred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object MFornecfantasia: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'fantasia'
      Size = 60
    end
    object MFornecnome: TStringField
      DisplayLabel = 'Raz'#227'o'
      FieldName = 'nome'
      Size = 60
    end
    object MFornecsegmento: TStringField
      DisplayLabel = 'Segmento'
      FieldName = 'segmento'
      Size = 30
    end
    object MFornecmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
end
