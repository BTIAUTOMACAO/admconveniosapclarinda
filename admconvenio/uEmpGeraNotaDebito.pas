unit uEmpGeraNotaDebito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, DBClient, frxClass, frxExportPDF,
  frxDBSet, frxGradient, DB, ZAbstractDataset, ZDataset, JvMemoryDataset,
  ZAbstractRODataset, DBCtrls, StdCtrls, Mask, JvExMask, JvToolEdit, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, ComCtrls,ShellApi, ADODB;

type
  TfrmEmpGeraNotaDebito = class(TF1)
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    ButMarcDesm_Emp: TButton;
    ButMarcaTodos_Emp: TButton;
    ButDesmTodos_Emp: TButton;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Bevel1: TBevel;
    Label1: TLabel;
    ButListaEmp: TButton;
    DataFecha: TJvDateEdit;
    por_dia_fecha: TRadioButton;
    por_periodo: TRadioButton;
    datafin: TJvDateEdit;
    dbLkpOperadores: TDBLookupComboBox;
    frxReport1: TfrxReport;
    frxGradientObject1: TfrxGradientObject;
    dbEmpresas: TfrxDBDataset;
    sd: TSaveDialog;
    frxPDFExport1: TfrxPDFExport;
    dsEmp: TDataSource;
    dsOperadores: TDataSource;
    qEmp: TADOQuery;
    qEmpempres_id: TAutoIncField;
    qEmpnome: TStringField;
    qEmpdata_fecha: TDateTimeField;
    qEmpdata_venc: TDateTimeField;
    qEmptotal: TBCDField;
    qOperadores: TADOQuery;
    qOperadoresNOME: TStringField;
    MEmpresas: TJvMemoryData;
    MEmpresasNOME: TStringField;
    MEmpresasDATA_FECHA: TDateTimeField;
    MEmpresasDATA_VENC: TDateTimeField;
    MEmpresasTOTAL: TFloatField;
    MEmpresasMARCADO: TBooleanField;
    MEmpresasEMPRES_ID: TIntegerField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dsEmpDataChange(Sender: TObject; Field: TField);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure por_dia_fechaClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
  private
    empres_sel : String;
    procedure selecionaTipoData(rb : TRadioButton);
    procedure Empresas_Sel;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmpGeraNotaDebito: TfrmEmpGeraNotaDebito;

implementation

uses cartao_util, DM;

{$R *.dfm}

procedure TfrmEmpGeraNotaDebito.ButListaEmpClick(Sender: TObject);
var dtIni, dtFim : string;
begin
  inherited;
  if(Pos(' ',DataFecha.Text) > 0) then begin
    MsgInf('Selecione a data de Fechamento');
    DataFecha.SetFocus;
  end else if(por_periodo.Checked) and (Pos(' ',datafin.Text) > 0) then begin
    MsgInf('Selecione a data final de Fechamento');
    datafin.SetFocus;
  end else begin
    qEmp.Close;
    qEmp.SQL.Clear;
    qEmp.SQL.Add(' select emp.empres_id, emp.empres_id, emp.nome, df.data_fecha, df.data_fecha, df.data_venc, sum(cc.debito - cc.credito) as total ');
    qEmp.SQL.Add(' from contacorrente cc ');
    qEmp.SQL.Add(' join conveniados c on c.conv_id = cc.conv_id ');
    qEmp.SQL.Add(' join empresas emp on emp.empres_id = c.empres_id ');
    qEmp.SQL.Add(' join dia_fecha df on df.empres_id = emp.empres_id ');
    dtIni := FormatDateTime('dd/mm/yyyy 00:00:00',StrToDateTime(DataFecha.Text));
    if por_periodo.Checked then begin
      dtFim := FormatDateTime('dd/mm/yyyy 23:59:00',StrToDateTime(DataFin.Text));
    end else begin
      dtFim := FormatDateTime('dd/mm/yyyy 23:59:00',StrToDateTime(DataFecha.Text));
    end;
    qEmp.SQL.Add(' where cc.data_fecha_emp between '''+dtIni+''' and '''+dtFim+'''');
    qEmp.SQL.Add(' and df.data_fecha between '''+dtIni+''' and '''+dtFim+'''');
    if (dbLkpOperadores.KeyValue <> '<TODOS>') then
      qEmp.SQL.Add(' and emp.responsavel_fechamento = '''+qOperadoresNOME.AsString+'''');
    qEmp.SQL.Add(' group by emp.empres_id, emp.nome, df.data_fecha, df.data_venc ');
    qEmp.SQL.Add(' order by emp.nome');
    qEmp.SQL.Text;
    qEmp.Open;
    if qEmp.IsEmpty then begin
     MsgInf('N�o foi encontrada nenhuma Empresa.');
     MEmpresas.EmptyTable;
     exit;
    end;

    qEmp.First;

    MEmpresas.Open;
    MEmpresas.EmptyTable;
    MEmpresas.DisableControls;
    while not qEmp.Eof do begin
        MEmpresas.Append;
        MEmpresasempres_id.AsInteger := qEmpempres_id.AsInteger;
        MEmpresasnome.AsString := qEmpnome.AsString;
        MEmpresasdata_fecha.AsDateTime := qEmpdata_fecha.AsDateTime;
        MEmpresasdata_venc.AsDateTime := qEmpdata_venc.AsDateTime;
        MEmpresastotal.AsFloat := qEmptotal.AsFloat;
        MEmpresasMarcado.AsBoolean := False;
        MEmpresas.Post;
        qEmp.Next;
    end;
    MEmpresas.First;
    MEmpresas.EnableControls;
    empres_sel := EmptyStr;

  end;
end;

procedure TfrmEmpGeraNotaDebito.btnVisualizarClick(Sender: TObject);
begin
  inherited;
  //frxReport1.Variables['Periodo'] := QuotedStr(DataFecha.Text +' � ' + iif(por_periodo.Checked,datafin.Text,DataFecha.Text));
  frxReport1.ShowReport;
end;

procedure TfrmEmpGeraNotaDebito.dsEmpDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnVisualizar.Enabled := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
  //btnGerarExcel.Enabled := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
  btnGerarPDF.Enabled   := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
end;

procedure TfrmEmpGeraNotaDebito.btnGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    btnVisualizar.Click;
    frxReport1.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TfrmEmpGeraNotaDebito.FormCreate(Sender: TObject);
begin
  inherited;
  qOperadores.Open;
  dbLkpOperadores.KeyValue := '<TODOS>';
end;

procedure TfrmEmpGeraNotaDebito.selecionaTipoData(rb: TRadioButton);
begin
  por_dia_fecha.Checked := rb = por_dia_fecha;
  datafin.Visible   := not (rb = por_dia_fecha);
  if rb = por_dia_fecha then begin
    label2.Caption    := 'Dia de Fechamento';
  end else begin
    label2.Caption    := 'Per�odo do Fechamento';
  end;
  por_periodo.Checked := not (rb = por_dia_fecha);
  DataFecha.SetFocus;
end;

procedure TfrmEmpGeraNotaDebito.por_dia_fechaClick(Sender: TObject);
begin
  inherited;
  selecionaTipoData(TRadioButton(Sender));
end;

procedure TfrmEmpGeraNotaDebito.ButMarcDesm_EmpClick(
  Sender: TObject);
begin
  inherited;
  if MEmpresas.IsEmpty then Exit;
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := not MEmpresasMarcado.AsBoolean;
    MEmpresas.Post;
    Empresas_Sel;
end;

procedure TfrmEmpGeraNotaDebito.Empresas_Sel;
var marca : TBookmark;
begin
  empres_sel := EmptyStr;
  marca := MEmpresas.GetBookmark;
  MEmpresas.DisableControls;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    if MEmpresasMarcado.AsBoolean then empres_sel := empres_sel + ','+MEmpresasempres_id.AsString;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  if empres_sel <> '' then empres_sel := Copy(empres_sel,2,Length(empres_sel));
  MEmpresas.EnableControls;
end;

procedure TfrmEmpGeraNotaDebito.ButMarcaTodos_EmpClick(
  Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := true;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
end;

procedure TfrmEmpGeraNotaDebito.ButDesmTodos_EmpClick(
  Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresas.IsEmpty then Exit;
  MEmpresas.DisableControls;
  marca := MEmpresas.GetBookmark;
  MEmpresas.First;
  while not MEmpresas.eof do begin
    MEmpresas.Edit;
    MEmpresasMarcado.AsBoolean := false;
    MEmpresas.Post;
    MEmpresas.Next;
  end;
  MEmpresas.GotoBookmark(marca);
  MEmpresas.FreeBookmark(marca);
  MEmpresas.EnableControls;
  Empresas_Sel;
end;

procedure TfrmEmpGeraNotaDebito.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm_EmpClick(nil);
end;

procedure TfrmEmpGeraNotaDebito.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (key = VK_F8) then
    ButMarcaTodos_Emp.Click
  else if (key = VK_F9) then
    ButDesmTodos_Emp.Click
  else if (key = VK_F11) then
    ButMarcDesm_Emp.Click
  else if (key = VK_F5) then
    ButListaEmp.Click
end;

procedure TfrmEmpGeraNotaDebito.JvDBGrid1TitleBtnClick(
  Sender: TObject; ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,qEmp.Sort) > 0 then begin
       if Pos(' DESC',qEmp.Sort) > 0 then qEmp.Sort := Field.FieldName
                                                  else qEmp.Sort := Field.FieldName+' DESC';
  end
  else qEmp.Sort := Field.FieldName;
  except
  end;

  qEmp.First;

  MEmpresas.Open;
  MEmpresas.EmptyTable;
  MEmpresas.DisableControls;
  while not qEmp.Eof do begin
      MEmpresas.Append;
      MEmpresasempres_id.AsInteger := qEmpempres_id.AsInteger;
      MEmpresasnome.AsString := qEmpnome.AsString;
      MEmpresasdata_fecha.AsDateTime := qEmpdata_fecha.AsDateTime;
      MEmpresasdata_venc.AsDateTime := qEmpdata_venc.AsDateTime;
      MEmpresastotal.AsFloat := qEmptotal.AsFloat;
      MEmpresasMarcado.AsBoolean := False;
      MEmpresas.Post;
      qEmp.Next;
  end;
  MEmpresas.First;
  MEmpresas.EnableControls;
  empres_sel := EmptyStr;


end;

end.
