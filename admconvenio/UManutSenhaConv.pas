unit UManutSenhaConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ADODB, Menus, StdCtrls, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls,
  DBCtrls, JvExControls, JvDBLookup, ToolEdit, RXDBCtrl;

type
  TFManutSenhaConv = class(TFCad)
    EdBuscaCartao: TEdit;
    Label29: TLabel;
    EdChapa: TEdit;
    Label56: TLabel;
    EdCodEmp: TEdit;
    Label31: TLabel;
    EdNomeEmp: TEdit;
    Label32: TLabel;
    EdCartao: TEdit;
    Label33: TLabel;
    QCadastroCONV_ID: TIntegerField;
    QCadastroTITULAR: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroGRUPO_CONV_EMP: TIntegerField;
    QCadastroCARGO: TStringField;
    QCadastroSETOR: TStringField;
    QCadastroCOD_EMPRESA: TStringField;
    QCadastroDT_NASCIMENTO: TDateTimeField;
    QCadastroCPF: TStringField;
    QCadastroRG: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroCEP: TStringField;
    QCadastroBANCO: TIntegerField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTACORRENTE: TStringField;
    QCadastroDIGITO_CONTA: TStringField;
    QCadastroTIPOPAGAMENTO: TStringField;
    QCadastroTELEFONE1: TStringField;
    QCadastroTELEFONE2: TStringField;
    QCadastroCELULAR: TStringField;
    QCadastroOBS1: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTULTCESTA: TDateTimeField;
    QCadastroDTASSOCIACAO: TDateTimeField;
    QCadastroEMAIL: TStringField;
    QCadastroLIMITE_MES: TBCDField;
    QCadastroCONSUMO_MES_1: TBCDField;
    QCadastroCONSUMO_MES_2: TBCDField;
    QCadastroCONSUMO_MES_3: TBCDField;
    QCadastroCONSUMO_MES_4: TBCDField;
    QCadastroLIMITE_TOTAL: TBCDField;
    QCadastroLIMITE_PROX_FECHAMENTO: TBCDField;
    QCadastroCESTABASICA: TBCDField;
    QCadastroSALARIO: TBCDField;
    QCadastroFIDELIDADE: TStringField;
    QCadastroCONTRATO: TIntegerField;
    QCadastroTIPOSALARIO: TStringField;
    QCadastroFLAG: TStringField;
    QCadastroSENHA: TStringField;
    QCadastroSETOR_ID: TIntegerField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroAPAGADO: TStringField;
    QCadastroVALE_DESCONTO: TStringField;
    QCadastroLIBERA_GRUPOSPROD: TStringField;
    QCadastroCOMPLEMENTO: TStringField;
    QCadastroUSA_SALDO_DIF: TStringField;
    QCadastroABONO_MES: TBCDField;
    QCadastroSALDO_RENOVACAO: TBCDField;
    QCadastroSALDO_ACUMULADO: TBCDField;
    QCadastroDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QCadastroCHAPA: TFloatField;
    QCadastroDATA_ADMISSAO: TDateTimeField;
    QCadastroDATA_DEMISSAO: TDateTimeField;
    QCadastroNUM_DEPENDENTES: TIntegerField;
    QCadastroSALDO_DEVEDOR: TBCDField;
    QCadastroSALDO_DEVEDOR_FAT: TBCDField;
    QCadastroPIS: TFloatField;
    QCadastroNOME_PAI: TStringField;
    QCadastroNOME_MAE: TStringField;
    QCadastroCART_TRAB_NUM: TIntegerField;
    QCadastroCART_TRAB_SERIE: TStringField;
    QCadastroREGIME_TRAB: TStringField;
    QCadastroVENC_TOTAL: TBCDField;
    QCadastroFIM_CONTRATO: TDateTimeField;
    QCadastroDISTRITO: TStringField;
    QCadastroESTADO_CIVIL: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroBAND_ID: TIntegerField;
    QCadastroempresa: TStringField;
    QCadastroTIPO_CREDITO: TIntegerField;
    QEmpresa: TADOQuery;
    DSEmpresa: TDataSource;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresausa_cod_importacao: TStringField;
    QEmpresafantasia: TStringField;
    QEmpresafidelidade: TStringField;
    QEmpresaband_id: TIntegerField;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label30: TLabel;
    Label58: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label93: TLabel;
    Label39: TLabel;
    DBEdit1: TDBEdit;
    dbEdit2: TDBEdit;
    dbEdtChapa: TDBEdit;
    DBEdit5: TDBEdit;
    DBDateEdit3: TDBDateEdit;
    DBEdit4: TDBEdit;
    DBEmpresa: TJvDBLookupCombo;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit10: TDBEdit;
    dbLkpCidades: TDBLookupComboBox;
    dbLkpEstados: TDBLookupComboBox;
    edtEmpr: TEdit;
    DBSetor: TJvDBLookupCombo;
    ButLimpaSenha: TBitBtn;
    grdDependentes: TJvDBGrid;
    GroupBox1: TGroupBox;
    QCartao: TADOQuery;
    DSCartao: TDataSource;
    QCartaoCONV_ID: TIntegerField;
    QCartaoNOME: TStringField;
    QCartaoCODCARTIMP: TStringField;
    Label16: TLabel;
    DBEdit3: TDBEdit;
    Label17: TLabel;
    DBEdit9: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit6: TDBEdit;
    QCadastroBAIRRO: TIntegerField;
    QCadastroCIDADE: TIntegerField;
    QCadastroESTADO: TIntegerField;
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButLimpaSenhaClick(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure TabFichaExit(Sender: TObject);
    procedure DBEmpresaChange(Sender: TObject);
    procedure HabilitarBotaoPerfilDiretor(NomeDoBotao : TButton);


    private
    function DigitaNovaSenha: string;

    public
    procedure buscaConveniado;

  
  end;

var
  FManutSenhaConv: TFManutSenhaConv;

implementation

uses
DM, UClassLog, DateUtils, cartao_util, URotinasTexto, UMenu, UValidacao,
  USelTipoImp, UDigitaSenha;

{$R *.dfm}


function TFManutSenhaConv.DigitaNovaSenha: string;
var senha : String;
begin
  FDigitaSenha := TFDigitaSenha.Create(self);
  FDigitaSenha.ShowModal;
  if FDigitaSenha.ModalResult = mrOk then
  begin
    senha := FDigitaSenha.edSenha.Text;
    Result:= senha;
  end
  else
    Result:= '';
  FDigitaSenha.Free;
end;


procedure TFManutSenhaConv.HabilitarBotaoPerfilDiretor(NomeDoBotao : TButton);
begin
  NomeDoBotao.Enabled := True;
end;

procedure TFManutSenhaConv.FormDestroy(Sender: TObject);
begin
  inherited;
  QEmpresa.Close;
end;

procedure TFManutSenhaConv.buscaConveniado;
begin
  QCadastro.Close;
  if ((Trim(EdCod.Text) = '') and (Trim(EdNome.Text) = '') and (Trim(EdChapa.Text) = '') and (Trim(EdCodEmp.Text) = '')
    and (Trim(EdNomeEmp.text) ='') and (Trim(EdCartao.Text) = '')) then
  begin
     MsgInf('� necess�rio especificar um crit�rio de busca.');
     EdCod.SetFocus;
     Exit;
  end;
  Screen.Cursor := crHourGlass;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('select');
  QCadastro.SQL.Add(' distinct conveniados.CONV_ID,');
  QCadastro.SQL.Add('  conveniados.TITULAR,');
  QCadastro.SQL.Add('  conveniados.LIBERADO,');
  QCadastro.SQL.Add('  conveniados.EMPRES_ID,');
  QCadastro.SQL.Add('  conveniados.GRUPO_CONV_EMP,');
  QCadastro.SQL.Add('  conveniados.CARGO,');
  QCadastro.SQL.Add('  conveniados.SETOR,');
  QCadastro.SQL.Add('  conveniados.COD_EMPRESA,');
  QCadastro.SQL.Add('  conveniados.DT_NASCIMENTO,');
  QCadastro.SQL.Add('  conveniados.CPF,');
  QCadastro.SQL.Add('  conveniados.RG,');
  QCadastro.SQL.Add('  conveniados.ENDERECO,');
  QCadastro.SQL.Add('  conveniados.NUMERO,');
  QCadastro.SQL.Add('  conveniados.BAIRRO,');
  QCadastro.SQL.Add('  conveniados.CIDADE,');
  QCadastro.SQL.Add('  conveniados.ESTADO,');
  QCadastro.SQL.Add('  conveniados.CEP,');
  QCadastro.SQL.Add('  conveniados.BANCO,');
  QCadastro.SQL.Add('  conveniados.AGENCIA,');
  QCadastro.SQL.Add('  conveniados.CONTACORRENTE,');
  QCadastro.SQL.Add('  conveniados.DIGITO_CONTA,');
  QCadastro.SQL.Add('  conveniados.TIPOPAGAMENTO,');
  QCadastro.SQL.Add('  conveniados.TELEFONE1,');
  QCadastro.SQL.Add('  conveniados.TELEFONE2,');
  QCadastro.SQL.Add('  conveniados.CELULAR,');
  QCadastro.SQL.Add('  conveniados.OBS1,');
  QCadastro.SQL.Add('  conveniados.OBS2,');
  QCadastro.SQL.Add('  conveniados.DTCADASTRO,');
  QCadastro.SQL.Add('  conveniados.OPERCADASTRO,');
  QCadastro.SQL.Add('  conveniados.DTALTERACAO,');
  QCadastro.SQL.Add('  conveniados.OPERADOR,');
  QCadastro.SQL.Add('  conveniados.DTULTCESTA,');
  QCadastro.SQL.Add('  conveniados.DTASSOCIACAO,');
  QCadastro.SQL.Add('  conveniados.EMAIL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_MES,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_1,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_2,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_3,');
  QCadastro.SQL.Add('  conveniados.CONSUMO_MES_4,');
  QCadastro.SQL.Add('  conveniados.LIMITE_TOTAL,');
  QCadastro.SQL.Add('  conveniados.LIMITE_PROX_FECHAMENTO,');
  QCadastro.SQL.Add('  conveniados.CESTABASICA,');
  QCadastro.SQL.Add('  conveniados.SALARIO,');
  QCadastro.SQL.Add('  conveniados.FIDELIDADE,');
  QCadastro.SQL.Add('  conveniados.CONTRATO,');
  QCadastro.SQL.Add('  conveniados.TIPOSALARIO,');
  QCadastro.SQL.Add('  conveniados.SETOR_ID,');
  QCadastro.SQL.Add('  conveniados.FLAG,');
  QCadastro.SQL.Add('  conveniados.SENHA,');
  QCadastro.SQL.Add('  conveniados.DTAPAGADO,');
  QCadastro.SQL.Add('  conveniados.APAGADO,');
  QCadastro.SQL.Add('  conveniados.VALE_DESCONTO,');
  QCadastro.SQL.Add('  conveniados.LIBERA_GRUPOSPROD,');
  QCadastro.SQL.Add('  conveniados.COMPLEMENTO,');
  QCadastro.SQL.Add('  conveniados.USA_SALDO_DIF,');
  QCadastro.SQL.Add('  conveniados.ABONO_MES,');
  QCadastro.SQL.Add('  conveniados.SALDO_RENOVACAO,');
  QCadastro.SQL.Add('  conveniados.SALDO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.DATA_ATUALIZACAO_ACUMULADO,');
  QCadastro.SQL.Add('  conveniados.CHAPA,');
  //FOI NECESS�RIO ACRESCENTAR A LINHA ABAIXO AP�S TER ACRESENTADO
  //O CONV_DETAIL_CONVID - DEVE SER ASSIM SEMPRE QUE ACRESENTADO
  //UM NOVO CAMPO NO DATASET.
  //QCadastro.SQL.ADD('  conveniados.CONV_ID,');
  QCadastro.SQL.Add('  conveniados.DATA_ADMISSAO,');
  QCadastro.SQL.Add('  conveniados.DATA_DEMISSAO,');
  QCadastro.SQL.Add('  conveniados.NUM_DEPENDENTES,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR,');
  QCadastro.SQL.Add('  conveniados.SALDO_DEVEDOR_FAT,');
  QCadastro.SQL.Add('  conveniados.PIS,');
  QCadastro.SQL.Add('  conveniados.NOME_PAI,');
  QCadastro.SQL.Add('  conveniados.NOME_MAE,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_NUM,');
  QCadastro.SQL.Add('  conveniados.CART_TRAB_SERIE,');
  QCadastro.SQL.Add('  conveniados.REGIME_TRAB,');
  QCadastro.SQL.Add('  conveniados.VENC_TOTAL,');
  QCadastro.SQL.Add('  conveniados.FIM_CONTRATO,');
  QCadastro.SQL.Add('  conveniados.DISTRITO,');
  QCadastro.SQL.Add('  conveniados.ESTADO_CIVIL,');
  QCadastro.SQL.Add('  empresas.FANTASIA,');
  QCadastro.SQL.Add('  empresas.BAND_ID,');
  QCadastro.SQL.Add('  empresas.nome empresa,');
  QCadastro.SQL.Add('  empresas.TIPO_CREDITO');
  QCadastro.SQL.Add('from conveniados');
  //QCadastro.SQL.Add('left join conv_detail on conv_detail.conv_id = conveniados.conv_id');
  QCadastro.SQL.Add('join empresas on empresas.empres_id = conveniados.empres_id and empresas.apagado <> ''S''');
  if ((Trim(EdCartao.Text) <> '') or (Trim(EdNome.Text) <> '')) then
  begin
    QCadastro.SQL.Add('join cartoes on cartoes.conv_id = conveniados.conv_id and cartoes.apagado = ''N''');
    QCadastro.Sql.Add(' and cartoes.nome like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
    QCadastro.SQL.Add('where conveniados.apagado <> ''S''');
  end;
  if Trim(EdCod.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.conv_id in (' + fnRemoverCaracterSQLInjection(EdCod.Text) + ')');
  if Trim(EdBuscaCartao.Text) <> '' then
    QCadastro.SQL.Add(' and cartoes.cartao_id in (' + fnRemoverCaracterSQLInjection(EdBuscaCartao.Text) + ')');
  //if Trim(EdNome.Text) <> '' then
    //QCadastro.Sql.Add(' and conveniados.titular like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
    //QCadastro.Sql.Add(' and cartoes.nome like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
  if Trim(EdChapa.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.chapa in (' + fnRemoverCaracterSQLInjection(EdChapa.Text) + ')');
  if Trim(EdCodEmp.Text) <> '' then
    QCadastro.Sql.Add(' and conveniados.empres_id in (' + fnRemoverCaracterSQLInjection(EdCodEmp.Text) + ')');
  if Trim(EdNomeEmp.text) <>''then
  begin
    DMConexao.Config.Open;
    if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
      QCadastro.Sql.Add(' and empresas.fantasia like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''')
    else
      QCadastro.Sql.Add(' and empresas.nome like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''');
    DMConexao.Config.Close;
  end;
  if Trim(EdCartao.Text) <> '' then
  begin
    if (( Length(Trim(EdCartao.Text)) = 11) and ( DigitoCartao(StrToFloat(Copy(EdCartao.Text,1,9))) = StrToInt(Copy(EdCartao.Text,10,2)))) then
      QCadastro.Sql.Add(' and (cartoes.codigo in (' + Copy( fnRemoverCaracterSQLInjection(EdCartao.Text),1,9) + ')' + ' or cartoes.codcartimp in (''' + fnRemoverCaracterSQLInjection(EdCartao.Text) + ''')))')
    else begin
      EdCartao.Text := fnRemoverCaracterSQLInjection(StringReplace(EdCartao.Text,',','","',[]));
      QCadastro.Sql.Add(' and cartoes.codcartimp in ('''+fnRemoverCaracterSQLInjection(EdCartao.Text)+''')');
    end;
  end;
  QCadastro.Sql.Add('order by conveniados.titular');
  //clipBoard.AsText := QCadastro.SQL.Text;
  Barra.Panels[0].Text := 'buscando conveniados';
  QCadastro.Open;
  Barra.Panels[0].Text := 'busca conclu�da conveniados';
  //QCadastro.EnableControls;
  if not QCadastro.IsEmpty then
  begin
    DBGrid1.SetFocus;
    Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
    '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;
  end
  else
    EdCod.SetFocus;
  //LimpaEdits;
  Screen.Cursor := crDefault;
end;


procedure TFManutSenhaConv.ButBuscaClick(Sender: TObject);
begin
  inherited;
  buscaConveniado;
end;

procedure TFManutSenhaConv.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONV_ID AS CONV_ID');
  DMConexao.AdoQry.Open;
  QCadastroCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  {Fim do bloco para recupera��o do ID}
  QCadastroLIBERADO.AsString          := 'S';
  QCadastroFIDELIDADE.AsString        := 'N';
  QCadastroLIMITE_MES.AsCurrency      := 0;
  QCadastroLIMITE_TOTAL.AsCurrency    := 0;
  QCadastroSALARIO.AsCurrency         := 0;
  QCadastroAPAGADO.AsString           := 'N';
  QCadastroVALE_DESCONTO.AsString     := 'N';
  QCadastroLIBERA_GRUPOSPROD.AsString := 'N';
  QCadastroUSA_SALDO_DIF.AsString     := 'N';
  QCadastroSALDO_RENOVACAO.AsCurrency := 0;
  QCadastroABONO_MES.AsCurrency       := 0;
  QCadastroSALDO_ACUMULADO.AsCurrency := 0;
  QCadastroCONSUMO_MES_1.AsCurrency     := 0;
  QCadastroCONSUMO_MES_2.AsCurrency     := 0;
  QCadastroCONSUMO_MES_3.AsCurrency     := 0;
  QCadastroCONSUMO_MES_4.AsCurrency     := 0;
  //QCadastroCONTRATO.AsInteger := QCadastroCONV_ID.AsInteger;
  //QCadastroOPERCADASTRO.Value := Operador.Nome;
  //QCadastroDTCADASTRO.Value := now;
end;

procedure TFManutSenhaConv.FormCreate(Sender: TObject);
begin

  chavepri := 'conv_id';
  detalhe  := 'Conv ID: ';
  DMConexao.Config.Open;
  if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
  begin
    DBEmpresa.LookupDisplay := 'empres_id;fantasia';
    DBEmpresa.LookupDisplay := 'fantasia';
    Label32.Caption:= '&Nome Fantasia da Empresa';
    QCadastroempresa.LookupResultField:= 'fantasia';
  end
  else
  begin
    DBEmpresa.LookupDisplay := 'nome';
    Label32.Caption:= '&Raz�o/Nome Empresa';
    QCadastroempresa.LookupResultField:= 'nome';
    //QCadastroempresa.LookupResultField:= 'empres_id;nome';
  end;
  DMConexao.Config.Close;
  inherited;
  QCadastro.Open;
  try
    QEmpresa.Open;
  except
  end;

  FMenu.vManutSenhaConv := True;
end;

procedure TFManutSenhaConv.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vManutSenhaConv := False;
  FormDestroy(FManutSenhaConv);

end;

procedure TFManutSenhaConv.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  QCartao.Close;
  QCartao.Parameters.ParamByName('conv_id').Value := QCadastroCONV_ID.AsInteger;
  QCartao.Open;
  edtEmpr.Text := QCadastroEMPRES_ID.AsString;
  
end;

procedure TFManutSenhaConv.ButLimpaSenhaClick(Sender: TObject);
var ItemSel : Integer;
begin
  inherited;
  if (fnVerfCompVazioEmTabSheet('Chapa Obrigat�ria',dbEdtChapa) = False)
  and(fnVerfCompVazioEmTabSheet('Empresa Obrigat�ria',DBEmpresa) = False)then
  begin
    inherited;
    if not QCadastro.IsEmpty then
    begin
      ItemSel := TFSelTipoImp.AbrirJanela(['Alterar senha para 1111','Alterar senha para n�mero do Conv. ID','Digitar a senha nova']);
      QCadastro.Open;
      QCadastro.edit;
      case ItemSel of
        0: QCadastroSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
        1: QCadastroSENHA.AsString := Crypt('E',IntToStr(QCadastroCONV_ID.AsInteger),'BIGCOMPRAS');
        2: QCadastroSENHA.AsString := Crypt('E',DigitaNovaSenha,'BIGCOMPRAS');
      end;
      if Application.MessageBox('Confirma esta opera��o?','Confirma��o',mb_yesno+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
        QCadastro.Post
      else
        QCadastro.Cancel;
    end;
  end;
end;

procedure TFManutSenhaConv.TabFichaShow(Sender: TObject);
var 
i:integer;
begin
  HabilitarBotaoPerfilDiretor(ButLimpaSenha);
  for i := 0 to componentCount - 1 do
  begin
    if (Components[i] is tEdit) then
      (Components[i] as TEdit).ReadOnly:= true;

   if (Components[i] is TDBEdit) then
      (Components[i] as TDBEdit).ReadOnly:= true;

   if (Components[i] is TJvDBLookupCombo) then
      (Components[i] as TJvDBLookupCombo).ReadOnly:= true;
  end;
end;

procedure TFManutSenhaConv.TabFichaExit(Sender: TObject);
var
i:integer;
begin
  inherited;
  for i := 0 to componentCount - 1 do
  begin
    if (Components[i] is tEdit) then
      (Components[i] as TEdit).ReadOnly:= false;

   if (Components[i] is TDBEdit) then
      (Components[i] as TDBEdit).ReadOnly:= false;

   if (Components[i] is TJvDBLookupCombo) then
      (Components[i] as TJvDBLookupCombo).ReadOnly:= false;
  end;
end;
procedure TFManutSenhaConv.DBEmpresaChange(Sender: TObject);
begin
  inherited;
  edtEmpr.Text := QCadastroEMPRES_ID.AsString;
end;

end.

