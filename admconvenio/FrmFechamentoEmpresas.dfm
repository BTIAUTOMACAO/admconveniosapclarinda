object FrmFechamentoEmpresa: TFrmFechamentoEmpresa
  Left = 258
  Top = 46
  Width = 908
  Height = 652
  Caption = 'FrmFechamentoEmpresa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 913
    Height = 41
    Alignment = taLeftJustify
    Caption = 'Par'#226'metros para resultado da pesquisa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object GBPesquisa: TGroupBox
    Left = 0
    Top = 56
    Width = 913
    Height = 185
    Caption = 'Pesquisa'
    TabOrder = 1
    DesignSize = (
      913
      185)
    object Label2: TLabel
      Left = 32
      Top = 24
      Width = 100
      Height = 13
      Caption = 'Data de fechamento:'
    end
    object Label3: TLabel
      Left = 304
      Top = 24
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label4: TLabel
      Left = 304
      Top = 72
      Width = 39
      Height = 13
      Caption = 'Cart'#245'es:'
    end
    object Label5: TLabel
      Left = 24
      Top = 96
      Width = 127
      Height = 13
      Caption = 'Respons'#225'vel Fechamento:'
    end
    object EDataFechamento: TJvDateEdit
      Left = 31
      Top = 41
      Width = 98
      Height = 21
      Hint = 'Data inicial da baixa da fatura'
      AutoSelect = False
      ShowNullDate = False
      TabOrder = 0
    end
    object CBEmpMovim: TCheckBox
      Left = 24
      Top = 72
      Width = 209
      Height = 17
      Caption = 'Somente empresas com movimento'
      TabOrder = 1
    end
    object EEmpres_id: TEdit
      Left = 304
      Top = 40
      Width = 73
      Height = 21
      TabOrder = 2
      OnExit = EEmpres_idExit
    end
    object CBTodos: TCheckBox
      Left = 304
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Todos'
      TabOrder = 3
    end
    object CBDrogaBella: TCheckBox
      Left = 304
      Top = 112
      Width = 97
      Height = 17
      Caption = 'Droga Bella'
      TabOrder = 4
    end
    object CBPlantaoCard: TCheckBox
      Left = 304
      Top = 136
      Width = 97
      Height = 17
      Caption = 'Plant'#227'o Card'
      TabOrder = 5
    end
    object CBMaxCard: TCheckBox
      Left = 304
      Top = 160
      Width = 97
      Height = 17
      Caption = 'Max Card'
      TabOrder = 6
    end
    object CBNatal: TCheckBox
      Left = 400
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Natal'
      TabOrder = 7
    end
    object CBAlimentacao: TCheckBox
      Left = 400
      Top = 112
      Width = 97
      Height = 17
      Caption = 'Alimenta'#231#227'o'
      TabOrder = 8
    end
    object CBRefeicao: TCheckBox
      Left = 400
      Top = 136
      Width = 97
      Height = 17
      Caption = 'Refei'#231#227'o'
      TabOrder = 9
    end
    object CBBemEstar: TCheckBox
      Left = 400
      Top = 160
      Width = 97
      Height = 17
      Caption = 'Bem Estar'
      TabOrder = 10
    end
    object CBCombustivel: TCheckBox
      Left = 488
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Combustivel'
      TabOrder = 11
    end
    object BtnPesquisar: TButton
      Left = 760
      Top = 88
      Width = 75
      Height = 25
      Caption = 'Pesquisar'
      TabOrder = 12
      OnClick = BtnPesquisarClick
    end
    object lkpOperador: TDBLookupComboBox
      Left = 26
      Top = 116
      Width = 199
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      KeyField = 'RESPONSAVEL_FECHAMENTO'
      ListField = 'RESPONSAVEL_FECHAMENTO'
      ListSource = dsOperador
      TabOrder = 13
    end
    object EEmpFantasia: TEdit
      Left = 408
      Top = 40
      Width = 257
      Height = 21
      Enabled = False
      TabOrder = 14
    end
  end
  object DBGPesqEmp: TDBGrid
    Left = 0
    Top = 248
    Width = 913
    Height = 184
    DataSource = DSPesqEmp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DSPesqEmp: TDataSource
    DataSet = QPesqEmp
    Left = 720
    Top = 208
  end
  object QPesqEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      ''
      '')
    Left = 780
    Top = 209
    object QPesqEmpEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QPesqEmpNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QPesqEmpDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QPesqEmpDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QPesqEmpTOTAL: TBCDField
      FieldName = 'TOTAL'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object QOperador: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct RESPONSAVEL_FECHAMENTO from EMPRESAS where RESPO' +
        'NSAVEL_FECHAMENTO is not null'
      '')
    Left = 72
    Top = 200
  end
  object dsOperador: TDataSource
    DataSet = QOperador
    Left = 32
    Top = 200
  end
  object DSEmpId: TDataSource
    Left = 296
    Top = 48
  end
  object QEmpId: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select empres_id,fantasia from empresas where empres_id = :empre' +
        's_id'
      '')
    Left = 344
    Top = 48
    object QEmpIdempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpIdfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
  end
end
