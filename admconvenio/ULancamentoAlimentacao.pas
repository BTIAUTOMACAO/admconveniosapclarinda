unit ULancamentoAlimentacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DBCtrls, DB, ADODB,
  frxClass, frxGradient, frxExportPDF, frxDBSet, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvMemoryDataset, JvExStdCtrls, JvEdit,
  JvValidateEdit, Mask, JvExMask, JvToolEdit;

type
  TfrmLancamentoAlimentacao = class(TF1)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbLkpEmpresas: TDBLookupComboBox;
    PageControl1: TPageControl;
    pnlDadosRenovacao: TPanel;
    Label60: TLabel;
    dbDataRenovacao: TJvDateEdit;
    rgTipo: TRadioGroup;
    Button2: TButton;
    btnExcluir: TButton;
    dbGridAlim: TJvDBGrid;
    pnlDados: TPanel;
    Bevel1: TBevel;
    Label57: TLabel;
    Label56: TLabel;
    Bevel3: TBevel;
    Label85: TLabel;
    Label88: TLabel;
    btnGravarConvAlim: TBitBtn;
    btnCancelarConvAlim: TBitBtn;
    edtSaldoRenovacao: TEdit;
    edtAbonoMes: TEdit;
    Button1: TButton;
    btnImportar: TBitBtn;
    lblTotalRenovacao: TJvValidateEdit;
    lblTotalAbono: TJvValidateEdit;
    dsEmpresas: TDataSource;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    QCredAlim: TADOQuery;
    QCredAlimconv_id: TIntegerField;
    QCredAlimempres_id: TIntegerField;
    QCredAlimtitular: TStringField;
    QCredAlimlimite_mes: TBCDField;
    QCredAlimabono_mes: TBCDField;
    QCredAlimsaldo_renovacao: TBCDField;
    QCredAlimRENOVACAO_ID: TIntegerField;
    QCredAlimDATA_RENOVACAO: TWideStringField;
    QCredAlimTIPO_CREDITO: TStringField;
    dsCredAlim: TDataSource;
    qUpdate: TADOQuery;
    tExcel: TADOTable;
    btnLimpar: TButton;
    qEmpresasliberada: TStringField;
    qEmpresasfechamento1: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure dbLkpEmpresasExit(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnGravarConvAlimClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
  private
    conv_sel : String;

    { Private declarations }
  public
    tSaldoRenovacao, tLimiteMes, tAbonoMes : currency;
  SavePlace : TBookmark;
  procedure InsereAlimentacaoRenovacao;
    procedure InsereAlimRenovacaoCreditos;
    { Public declarations }
  end;

var
  frmLancamentoAlimentacao: TfrmLancamentoAlimentacao;

implementation

uses DM, cartao_util, UMenu, URotinasTexto, UValidacao;

{$R *.dfm}

procedure TfrmLancamentoAlimentacao.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  //pnlDadosRenovacao.Visible := false;
  //dbGridAlim.Visible := false;
  //pnlDados.Visible := false;
  dbLkpEmpresas.SetFocus;
end;

procedure TfrmLancamentoAlimentacao.dbLkpEmpresasExit(
  Sender: TObject);
var valorAbono, valorRenovacao: Currency;
begin
  inherited;
  if qEmpresasliberada.Text = 'S' then begin
    valorAbono := 0;
    valorRenovacao := 0;
    pnlDadosRenovacao.Visible := true;
    pnlDados.Visible := true;

    qCredAlim.Close;
    qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
    QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
    qCredAlim.Open;



        if not qCredAlimDATA_RENOVACAO.IsNull then
    begin

      if QCredAlimTIPO_CREDITO.AsString = 'R' then  begin
        rgTipo.ItemIndex := 0;
        dbDataRenovacao.Text :=  qEmpresasfechamento1.AsString;
      end else begin
        rgTipo.ItemIndex := 1;
        dbDataRenovacao.Text :=  FormataDataSql(qCredAlimDATA_RENOVACAO.AsString);
       end;

      while not qCredAlim.Eof do
      begin
        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
        qCredAlim.Next;
      end;

      qCredAlim.First;
      dbGridAlim.Visible := true;
    end
    else
    begin
      dbDataRenovacao.Text := '';
      dbGridAlim.Visible := false;
    end;

    lblTotalAbono.Value := valorAbono;
    lblTotalRenovacao.Value := valorRenovacao;
    edtSaldoRenovacao.Text := '0,00';
    edtAbonoMes.Text := '0,00';
    rgTipo.SetFocus;
  end else begin
     Msginf('A empresa '+ qEmpresasnome.AsString +' n�o esta liberada: ');
  end;

  end;


//end
procedure TfrmLancamentoAlimentacao.Button2Click(Sender: TObject);
begin
  inherited;
  InsereAlimentacaoRenovacao;
end;

procedure TfrmLancamentoAlimentacao.InsereAlimentacaoRenovacao;
var tmp, sql : String;
    dc : char;
    dia_renovacao, mes_renovacao, ano_renovacao, dia_atual, mes_atual, ano_atual : Word;
    renovacao_id : integer;
begin
  if dbDataRenovacao.Date <> 0 then
  begin
    //verifica se data de renovacao e maior que a data atual//
    if dbDataRenovacao.Date <= Date then begin
      msginf('Data de Renova��o deve ser maior que a data atual!');
      Abort;
    end
    else
    //verifica se existe registro na registro_alimentacao se tiver verifica se data de renovacao foi alterada se sim faz update na tabela alimentacao_renovacao//
    if (not qCredAlimDATA_RENOVACAO.IsNull) AND (rgTipo.ItemIndex = 0) then begin
      renovacao_id := QCredAlimRENOVACAO_ID.AsInteger;
      DecodeDate(StrToDate(FormataDataSql(QCredAlimDATA_RENOVACAO.AsString)),ano_renovacao, mes_renovacao, dia_renovacao);
      DecodeDate(dbDataRenovacao.Date,ano_atual, mes_atual, dia_atual);
      if ((dbDataRenovacao.Date <> StrToDate(FormataDataSql(QCredAlimDATA_RENOVACAO.AsString))) and (mes_renovacao = mes_atual)) then begin
        if Application.MessageBox('J� existe Data de Renova��o para este m�s. Deseja alterar a Data?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
        begin
          try
            Screen.Cursor := crHourGlass;
            sql := 'UPDATE ALIMENTACAO_RENOVACAO SET DATA_RENOVACAO =  '+ QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ', TIPO_CREDITO = ';
            case rgTipo.ItemIndex of
              0: sql := sql + '''R''';
            else
              sql := sql + '''C''';
            end;
            sql := sql + ' WHERE RENOVACAO_ID = '+ QCredAlimRENOVACAO_ID.AsString;

            DMConexao.AdoCon.BeginTrans;
            DMConexao.ExecuteSql(sql);
            DMConexao.AdoCon.CommitTrans;
          except
            on e:Exception do
            begin
             DMConexao.AdoCon.RollbackTrans;
             Screen.Cursor := crDefault;
             MsgErro('Um erro ocorreu durante a atualiza��o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
             abort;
            end;
           end;
        end
        else
        begin
          dbDataRenovacao.Text := QCredAlimDATA_RENOVACAO.AsVariant;
          Abort;
        end;
      end;
    end
    else
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SRENOVACAO_ID AS RENOVACAO_ID');
          DMConexao.AdoQry.Open;
          renovacao_id := DMConexao.AdoQry.Fields[0].Value;

          sql := 'INSERT INTO ALIMENTACAO_RENOVACAO(RENOVACAO_ID, EMPRES_ID, DATA_RENOVACAO, TIPO_CREDITO, DATA_LANCAMENTO) VALUES('+ inttostr(renovacao_id);
          sql := sql + ',' + qCredAlimEMPRES_ID.AsString + ',' + QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ',';

          case rgTipo.ItemIndex of
            0: sql := sql + '''R'',';
          else
            sql := sql + '''C'',';
          end;

          sql := sql + QuotedStr(FormatDateTime('dd/mm/yyyy',Date) +  ' ' + FormatDateTime('hh:mm:ss',Time))  + ')';

          DMConexao.ExecuteSql(sql);
          DMConexao.AdoCon.CommitTrans;

        except
          on e:Exception do
          begin
             DMConexao.AdoCon.RollbackTrans;
             Screen.Cursor := crDefault;
             MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
             abort;
          end;
      end;
    end;
                                                                   

    qCredAlim.Close;
    qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
    QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
    qCredAlim.Open;

    DMConexao.GravaLog('FCadEmp','DATA_RENOVACAO',FormataDataSql(QCredAlimDATA_RENOVACAO.AsString),FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Inclus�o',
        dbLkpEmpresas.KeyValue,'');

    msginf('Atualiza��o realizada com sucesso');

    dbGridAlim.Visible := true;
    pnlDadosRenovacao.Visible := true;
    pnlDados.Visible := true;
    dbGridAlim.Visible := true;
    dbGridAlim.SetFocus;
    Screen.Cursor := crDefault;

  end
  else
    begin
      MsgInf('Necess�rio escolher a Data de Renova��o');
      dbDataRenovacao.SetFocus;
    end;
end;

procedure TfrmLancamentoAlimentacao.Button1Click(Sender: TObject);
begin
  inherited;
  InsereAlimRenovacaoCreditos
end;

procedure TfrmLancamentoAlimentacao.InsereAlimRenovacaoCreditos;
var
  dc : char;
  valorAbono, valorRenovacao: Currency;
begin
    valorAbono := 0;
    valorRenovacao := 0;

    if Application.MessageBox('Confirma a inclus�o/altera��o do Abono/Saldo Renova��o?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      Screen.Cursor := crHourGlass;
      dc := DecimalSeparator;
      Application.ProcessMessages;

      try
        DMConexao.AdoCon.BeginTrans;

        //DELETA OS REGISTROS DA TABELA ALIMENTACAO_RENOVACAO_CREDITOS//
        DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+qCredAlimRENOVACAO_ID.AsString);
        QCredAlim.First;
        while not QCredAlim.Eof do begin
          qUpdate.SQL.Clear;
          qUpdate.SQL.Text := 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + qCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',edtSaldoRenovacao.Text) + ',' + fnsubstituiString(',','.',edtAbonoMes.Text) + ','+QuotedStr(DateTimeToStr(Now))+')';
          qUpdate.ExecSQL;

          valorAbono := valorAbono + STRTOCURR(edtAbonoMes.Text);
          valorRenovacao := valorRenovacao + STRTOCURR(edtSaldoRenovacao.Text);

          QCredAlim.Next;
        end;

        DMConexao.AdoCon.CommitTrans;

        msginf('Atualiza��o realizada com sucesso!');

      except
        on e:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
          Screen.Cursor := crDefault;
          abort;
        end;
      end;

      Screen.Cursor := crDefault;
      qCredAlim.Requery;
      DecimalSeparator := dc;

      lblTotalAbono.Value := valorAbono;
      lblTotalRenovacao.Value := valorRenovacao;
      edtSaldoRenovacao.Text := '0,00';
      edtAbonoMes.Text := '0,00';
      Button1.SetFocus;

    end;

end;

procedure TfrmLancamentoAlimentacao.btnGravarConvAlimClick(
  Sender: TObject);
var saldoRenoOld, abonoMesOld, limiteMesOld : currency;
incluir : boolean;
valorAbono, valorRenovacao: Currency;
begin
  if Application.MessageBox('Confirma a altera��o das informa��es?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    valorAbono := 0;
    valorRenovacao := 0;

    tLimiteMes := StrtoCurr(dbGridAlim.Fields[2].Text);
    tAbonoMes := StrtoCurr(dbGridAlim.Fields[3].Text);
    tSaldoRenovacao := StrtoCurr(dbGridAlim.Fields[4].Text);

    SavePlace := QCredAlim.GetBookmark;
    try
      DMConexao.AdoCon.BeginTrans;
      // Altera��o de Limite M�s
      if VarIsNull(QCredAlimLIMITE_MES.OldValue) then
        limiteMesOld := 0
      else
        limiteMesOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if (limiteMesOld <> tLimiteMes) then
      begin
        DMConexao.GravaLog('FCadConv','LIMITE_MES',FormatDinBR(limiteMesOld),FormatDinBR(tLimiteMes),Operador.Nome,'Altera��o',
                            QCredAlimCONV_ID.AsString,'');
        qUpdate.SQL.Text := ' update conveniados set limite_mes = ' + fnsubstituiString(',','.',CurrToStr(tLimiteMes)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString;
        DMConexao.ExecuteSql(qUpdate.SQL.Text);
      end;

      Screen.Cursor := crHourGlass;

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE CONV_ID = '+ QCredAlimCONV_ID.AsString);
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then
        incluir := false
      else
        incluir := true;

      // Altera��o de Abono M�s
      if VarIsNull(QCredAlimABONO_MES.OldValue) then
        abonoMesOld := 0
      else
        abonoMesOld := QCredAlimABONO_MES.OldValue;

      // Altera��o de Saldo Renova��o
      if VarIsNull(QCredAlimSALDO_RENOVACAO.OldValue) then
        saldoRenoOld := 0
      else
        saldoRenoOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if ((abonoMesOld <> tAbonoMes) or (saldoRenoOld <> tSaldoRenovacao))  then
      begin
        if incluir = true then begin
            qUpdate.SQL.Text := 'insert into alimentacao_renovacao_creditos(renovacao_id, conv_id, renovacao_valor, abono_valor, data_alteracao) values('+ qCredAlimRENOVACAO_ID.AsString + ',' + QCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ',' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) +',' +QuotedStr(DateTimeToStr(Now))+')';
            DMConexao.ExecuteSql(qUpdate.SQL.Text);
          end
        else
          begin
            qUpdate.SQL.Text := ' update alimentacao_renovacao_creditos set abono_valor = ' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) + ', renovacao_valor = ' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString + ' and renovacao_id = ' + qCredAlimRENOVACAO_ID.AsString;
            DMConexao.ExecuteSql(qUpdate.SQL.Text);
          end;
      end;

      DMConexao.AdoCon.CommitTrans;

    except
      on e:Exception do
      begin
         DMConexao.AdoCon.RollbackTrans;
         Screen.Cursor := crDefault;
         MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
         abort;
       end;
    end;
  end;

  QCredAlim.Requery;
  QCredAlim.GotoBookmark(SavePlace);
  QCredAlim.FreeBookmark(SavePlace);
  qCredAlim.First;
  while not qCredAlim.Eof do
    begin
        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
        qCredAlim.Next;
    end;
  qCredAlim.First;
  lblTotalAbono.Value := valorAbono;
  lblTotalRenovacao.Value := valorRenovacao;
  
  Screen.Cursor := crDefault;
  tSaldoRenovacao := 0;
  tAbonoMes := 0;
  tLimiteMes := 0;
end;

procedure TfrmLancamentoAlimentacao.btnImportarClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId : String;
path : String;
valorAbono, valorRenovacao: Currency;
erro: Boolean;
begin
  inherited;

  valorAbono := 0;
  valorRenovacao := 0;
  try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
     OD.Free;
    tExcel.Close;
    tExcel.ConnectionString := path;
    tExcel.TableName:= 'credito$';
    try
      tExcel.Open;
    except
      MsgErro('N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "credito" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    btnImportar.Caption := 'Importar';
    Abort;
  end;

  if qCredAlimEMPRES_ID.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
  begin
    if not DMConexao.AdoCon.InTransaction then
      QCredAlim.Requery();
      renovacaoId := QCredAlimRENOVACAO_ID.AsString;
      DMConexao.AdoCon.BeginTrans;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+ QuotedStr(renovacaoId));
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then begin
        msginf('Valores anteriores j� lan�ados ser�o sobrepostos pelos valores da planilha');
      end;

      DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+renovacaoId);
      Screen.Cursor := crHourGlass;
      while not tExcel.Eof do begin
      try
        if tExcel.fieldByName('CONV_ID').AsString <> '' then begin
          qUpdate.SQL.Clear;
          qUpdate.SQL.Text := 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString) + ',0.00, '+QuotedStr(DateTimeToStr(Now))+')';
          DMConexao.ExecuteSql(qUpdate.SQL.Text);

          valorRenovacao := valorRenovacao + tExcel.fieldByName('VALOR RENOVA�AO').AsCurrency;
        end;
      except on E:Exception do begin
          MsgErro('Erro ao incluir cr�ditos');
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          tExcel.Close;
          abort;
        end;
      end;
      tExcel.Next;
      Application.ProcessMessages;
    end;

    lblTotalAbono.Value := 0;
    lblTotalRenovacao.Value := valorRenovacao;
    DMConexao.AdoCon.CommitTrans;
    QCredAlim.Requery;
    Screen.Cursor := crDefault;
    if tExcel.State in [dsEdit, dsInsert] then
      tExcel.Close;

    msginf('Importa��o realizada com sucesso!');

  end
  else
     msginf('Planilha n�o pertence a empresa '+qCredAlimEMPRES_ID.AsString + '. Verifique o arquivo.');


end;

procedure TfrmLancamentoAlimentacao.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Confirma o Lan�amento da Recarga?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
   try
        DMConexao.GravaLog('FCadEmp','RENOVACA_ID',QCredAlimRENOVACAO_ID.AsString,FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Inclus�o',
        dbLkpEmpresas.KeyValue,'');

        Screen.Cursor := crHourGlass;

        DMConexao.AdoCon.BeginTrans;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(' SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA WHERE DIA_FECHA.DATA_FECHA > CONVERT(DATE,CURRENT_TIMESTAMP) AND DIA_FECHA.EMPRES_ID = ' + QCredAlimEMPRES_ID.AsString);
        DMConexao.AdoQry.Open;

        DMConexao.ExecuteSql('EXEC VERIFICA_SALDO_RENOVACAO ' + QCredAlimEMPRES_ID.AsString + ',' + QCredAlimTIPO_CREDITO.AsString + ',''' +  DMConexao.AdoQry.Fields[0].AsString + '''');
        DMConexao.AdoCon.CommitTrans;

        Screen.Cursor := crDefault;
        msginf('Recarga realizada com sucesso!');

        qCredAlim.Close;
        qEmpresas.Open;
        pnlDadosRenovacao.Visible := false;
        dbGridAlim.Visible := false;
        pnlDados.Visible := false;
        dbLkpEmpresas.SetFocus;
      except
        on e:Exception do
        begin
          Screen.Cursor := crDefault;
          MsgErro('Erro ao efetivar lan�amento de cr�ditos');
          DMConexao.AdoCon.RollbackTrans;
         end;
   end;
   end;
end;

procedure TfrmLancamentoAlimentacao.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if not qCredAlimDATA_RENOVACAO.IsNull then
  begin
    if Application.MessageBox('Confirma a exclus�o do lan�amento de cr�dito empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      DMConexao.AdoCon.BeginTrans;
      try
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;
        DMConexao.AdoCon.CommitTrans;

        DMConexao.GravaLog('FCadEmp','RENOVACAO_ID',qCredAlimRENOVACAO_ID.AsString,FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Exclusao',
        dbLkpEmpresas.KeyValue,'');

        qCredAlim.Close;
        qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
        QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
        qCredAlim.Open;

        dbDataRenovacao.Text := '';
        dbGridAlim.Visible := false;
        pnlDados.Visible := false;
        pnlDadosRenovacao.Visible := False;
        rgTipo.ItemIndex := 0;
        dbLkpEmpresas.KeyValue :=0;
        dbLkpEmpresas.SetFocus;
      except
        on E:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          MsgErro('Erro ao excluir Lan�amento. Erro: '+E.Message+sLineBreak+'Opera��o Cancelada!');
        end;
      end;
    end;
    end
    else
      begin
         msginf('N�o existe lan�amentos a serem exclu�dos!');
         dbDataRenovacao.SetFocus;
      end;

end;

procedure TfrmLancamentoAlimentacao.btnLimparClick(Sender: TObject);
begin
  inherited;
  qEmpresas.Close;
  qEmpresas.Open;
  dbLkpEmpresas.KeyValue :=0;
  //pnlDadosRenovacao.Visible := false;
  //dbGridAlim.Visible := false;
  //pnlDados.Visible := false;
  dbLkpEmpresas.SetFocus;
end;

end.
