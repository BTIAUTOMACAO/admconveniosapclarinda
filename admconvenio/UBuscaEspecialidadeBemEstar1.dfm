object FBuscaEspecialidade1: TFBuscaEspecialidade1
  Left = 589
  Top = 151
  Width = 403
  Height = 480
  Caption = 'FBuscaEspecialidade1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Locked = True
    ParentFont = False
    TabOrder = 0
    object panTitulo: TPanel
      Left = 1
      Top = 1
      Width = 784
      Height = 20
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Localizar Especialidade'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object ButClose: TSpeedButton
        Left = 764
        Top = 1
        Width = 20
        Height = 18
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 381
    Width = 395
    Height = 72
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      395
      72)
    object btnFiltroDados: TSpeedButton
      Left = 146
      Top = 44
      Width = 89
      Height = 25
      Caption = '&Filtro de dados'
      Flat = True
      NumGlyphs = 2
    end
    object btnAlterLinear: TSpeedButton
      Left = 240
      Top = 44
      Width = 89
      Height = 25
      Caption = '&Altera'#231#227'o linear'
      Flat = True
    end
    object btnFirstA: TSpeedButton
      Left = 1
      Top = 44
      Width = 33
      Height = 25
      Flat = True
      Glyph.Data = {
        1E030000424D1E030000000000001E0100002800000020000000100000000100
        08000000000000020000232E0000232E00003A00000000000000986532009B68
        35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
        6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABB00BBBC
        BD00BDBEBE00BFBFC000D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
        9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C1C2C300C4C5C500C5C6
        C700C8C9C900CBCBCC00CBCCCD00CCCDCD00CECFCF00D0D0D100D3D4D400D5D5
        D600DBDBDC00DCDCDC00DCDDDD00DFDFE000FFE3C700FFE5CC00E1E1E100E6E6
        E600E7E7E700E8E9E900EBEBEB00EEEEEE00EFEFF000F2F2F200F3F3F300FFFF
        FF00393939393939393939393939393939393939393939393939393939393939
        3939393939393939393939393939393939393939393939393939393939393939
        3939393939393939000939393939390009393939393939391024393939393910
        24393939393939010F0139393939010F01393939393939112A1139393939112A
        11393939393902151902393939021519023939393939122A3112393939122A31
        123939393903161A1A03393903161A1A0339393939132C3232133939132C3232
        1339393904171B1B1B043904171B1B1B043939391F2D3333331F391F2D333333
        1F393905181C1C1C1C050D181C1C1C1C05393920303434343420283034343434
        203939062F1D1D1D1D060E2F1D1D1D1D06393921383535353521293835353535
        21393939072F1E1E1E0739072F1E1E1E07393939223836363622392238363636
        2239393939082F2E2E083939082F2E2E08393939392338373723393923383737
        2339393939390A2F2F0A3939390A2F2F0A393939393925383825393939253838
        253939393939390B2F0B393939390B2F0B393939393939263826393939392638
        26393939393939390C1439393939390C1439393939393939272B393939393927
        2B39393939393939393939393939393939393939393939393939393939393939
        3939393939393939393939393939393939393939393939393939393939393939
        3939}
      NumGlyphs = 2
    end
    object btnPriorA: TSpeedButton
      Left = 34
      Top = 44
      Width = 33
      Height = 25
      Flat = True
      Glyph.Data = {
        32030000424D3203000000000000320100002800000020000000100000000100
        08000000000000020000232E0000232E00003F00000000000000986532009A67
        34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
        5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
        BE00BEBFBF00BFC0C100D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
        9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C1C2
        C300C3C4C400C5C5C600C7C8C800C9CACA00CBCBCC00CDCECE00CECFCF00D0D0
        D100D1D1D200DCDCDC00DCDDDD00DDDEDE00DEDFDF00FFE1C200FFE3C800FFE5
        CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00ECECEC00EEEE
        EE00EFEFF000F1F1F100F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
        3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
        000A3E3E3E3E3E3E3E3E3E3E3E3E3E3E10273E3E3E3E3E3E3E3E3E3E3E3E3E01
        0F013E3E3E3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E0215
        1A023E3E3E3E3E3E3E3E3E3E3E3E122B34123E3E3E3E3E3E3E3E3E3E3E03161B
        1B033E3E3E3E3E3E3E3E3E3E3E132D3535133E3E3E3E3E3E3E3E3E3E04171C1C
        1C043E3E3E3E3E3E3E3E3E3E212E363636213E3E3E3E3E3E3E3E3E05181D1D1D
        1D053E3E3E3E3E3E3E3E3E223237373737223E3E3E3E3E3E3E3E06191E1E1E1E
        1E063E3E3E3E3E3E3E3E23333838383838233E3E3E3E3E3E3E3E07311F1F1F1F
        1F073E3E3E3E3E3E3E3E243D3939393939243E3E3E3E3E3E3E3E3E0831202020
        20083E3E3E3E3E3E3E3E3E253D3A3A3A3A253E3E3E3E3E3E3E3E3E3E09312F2F
        2F093E3E3E3E3E3E3E3E3E3E263D3B3B3B263E3E3E3E3E3E3E3E3E3E3E0B3130
        300B3E3E3E3E3E3E3E3E3E3E3E273D3C3C273E3E3E3E3E3E3E3E3E3E3E3E0C31
        310C3E3E3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E3E0D
        310D3E3E3E3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E3E
        0E143E3E3E3E3E3E3E3E3E3E3E3E3E3E2A2C3E3E3E3E3E3E3E3E3E3E3E3E3E3E
        3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
      NumGlyphs = 2
    end
    object btnNextA: TSpeedButton
      Left = 67
      Top = 44
      Width = 33
      Height = 25
      Flat = True
      Glyph.Data = {
        32030000424D3203000000000000320100002800000020000000100000000100
        08000000000000020000232E0000232E00003F00000000000000986532009A67
        34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
        5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
        BE00BEBFBF00BFBFC000D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
        9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C0C1
        C200C2C3C300C5C5C600C6C7C800C9CACA00CBCBCC00CCCDCD00CECFCF00D0D0
        D100D1D1D200DBDBDC00DCDCDC00DCDDDD00DEDFDF00FFE1C200FFE3C800FFE5
        CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00EBEBEB00EDED
        ED00EFEFF000F0F0F000F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
        3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E0A003E3E3E3E
        3E3E3E3E3E3E3E3E3E3E27103E3E3E3E3E3E3E3E3E3E3E3E3E3E010F013E3E3E
        3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E3E021A15023E3E
        3E3E3E3E3E3E3E3E3E3E12342C123E3E3E3E3E3E3E3E3E3E3E3E031B1B16033E
        3E3E3E3E3E3E3E3E3E3E1335352D133E3E3E3E3E3E3E3E3E3E3E041C1C1C1704
        3E3E3E3E3E3E3E3E3E3E213636362E213E3E3E3E3E3E3E3E3E3E051D1D1D1D18
        053E3E3E3E3E3E3E3E3E223737373732223E3E3E3E3E3E3E3E3E061E1E1E1E1E
        19063E3E3E3E3E3E3E3E23383838383833233E3E3E3E3E3E3E3E071F1F1F1F1F
        31073E3E3E3E3E3E3E3E2439393939393D243E3E3E3E3E3E3E3E082020202031
        083E3E3E3E3E3E3E3E3E253A3A3A3A3D253E3E3E3E3E3E3E3E3E092F2F2F3109
        3E3E3E3E3E3E3E3E3E3E263B3B3B3D263E3E3E3E3E3E3E3E3E3E0B3030310B3E
        3E3E3E3E3E3E3E3E3E3E273C3C3D273E3E3E3E3E3E3E3E3E3E3E0C31310C3E3E
        3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E0D310D3E3E3E
        3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E140E3E3E3E3E
        3E3E3E3E3E3E3E3E3E3E2D2A3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
        3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
      NumGlyphs = 2
    end
    object btnLastA: TSpeedButton
      Left = 100
      Top = 44
      Width = 33
      Height = 25
      Flat = True
      Glyph.Data = {
        1E030000424D1E030000000000001E0100002800000020000000100000000100
        08000000000000020000232E0000232E00003A00000000000000986532009B68
        35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
        6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABA00BABB
        BC00BCBDBE00BEBFBF00D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
        9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C0C1C200C3C4C400C5C5
        C600C7C8C800CACACB00CBCBCC00CBCCCD00CDCECE00CFCFD000D2D3D300D4D5
        D500DADADB00DBDBDC00DCDDDD00DEDFDF00FFE3C700FFE5CC00E1E1E100E5E5
        E600E7E7E700E8E9E900EAEAEB00EDEDED00EFEFF000F1F1F100F3F3F300FFFF
        FF00393939393939393939393939393939393939393939393939393939393939
        3939393939393939393939393939393939393939393939393939393939393939
        3939390900393939393909003939393939393924103939393939241039393939
        393939010F0139393939010F01393939393939112A1139393939112A11393939
        3939390219150239393902191502393939393912312A1239393912312A123939
        393939031A1A16033939031A1A1603393939391332322C1339391332322C1339
        393939041B1B1B170439041B1B1B17043939391F3333332D1F391F3333332D1F
        393939051C1C1C1C180D051C1C1C1C1805393920343434343028203434343430
        203939061D1D1D1D2F0E061D1D1D1D2F06393921353535353829213535353538
        213939071E1E1E2F0739071E1E1E2F0739393922363636382239223636363822
        393939082E2E2F083939082E2E2F083939393923373738233939233737382339
        3939390A2F2F0A3939390A2F2F0A393939393925383825393939253838253939
        3939390B2F0B393939390B2F0B39393939393926382639393939263826393939
        393939140C3939393939140C393939393939392B2739393939392B2739393939
        3939393939393939393939393939393939393939393939393939393939393939
        3939393939393939393939393939393939393939393939393939393939393939
        3939}
      NumGlyphs = 2
    end
    object Label1: TLabel
      Left = 1
      Top = 3
      Width = 55
      Height = 13
      Caption = 'Cod. Espec'
      FocusControl = EdCod
    end
    object Label2: TLabel
      Left = 79
      Top = 3
      Width = 48
      Height = 13
      Caption = '&Descricao'
      FocusControl = EdNome
    end
    object ButAtualiza: TBitBtn
      Left = 286
      Top = 45
      Width = 81
      Height = 25
      Hint = 'Atualiza a visualiza'#231#227'o dos dados'
      Anchors = [akTop, akRight]
      Caption = '&Atualizar'
      TabOrder = 2
      Visible = False
      Glyph.Data = {
        B2030000424DB203000000000000B20100002800000020000000100000000100
        08000000000000020000232E0000232E00005F000000000000000D7D0D001785
        1700278F27003B9D3B00469F46004A9D4A004FAB4F0052AC52005BAC5B0064B9
        640069BC69006DB96D0076C576007FC47F007AC87A0086BD860086BE8600B7B8
        B900BABBBC0086C0860080CD80008DCF8D008AD48A008CD58C0092C6920090C8
        900097DC97009DE09D00B1E3B100B3E0B300B4EEB400BBE3BB00B7F0B700BCF3
        BC00BFC0C100C6C7C800C9CACA00CDCECE00CECFCF00C2DFC200D0D0D100D5D5
        D600D6D6D700DADADB00DBDBDC00DCDCDC00DEDFDF00DFDFE000C4E0C400C6E7
        C600C5EAC500C9E3C900CCE4CC00CBEECB00C7FAC700C9FBC900D4ECD400D6ED
        D600D3FFD300DEF1DE00E1E1E100E2E2E200E6E6E600E8E8E800EBEBEB00ECEC
        EC00EEEEEE00EEEFEF00EFEFF000E1F4E100E4F6E400E5F6E500E2FFE200ECF5
        EC00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F4F5F500F5F5F500F6F6
        F600F7F7F700F0F8F000F2FBF200F1FFF100F4FFF400F8F8F800F9F9F900FAFA
        FA00FBFBFB00F8FCF800FDFDFD00FEFEFE00FFFFFF005E5E5E5E5E5E5E5E1010
        5E5E5E5E5E5E5E5E5E5E5E5E5E5E2D2D5E5E5E5E5E5E5E5E5E5E5E5E5E5E010C
        015E5E5E5E5E5E5E5E5E5E5E5E5E122B125E5E5E5E5E5E5E491304020202021A
        0E025E5E5E5E5E5E592D24222222223E2D225E5E5E5E5E34080E1A2020202020
        201A035E5E5E5E4A282D3E4A4A4A4A4A4A3E235E5E5E5B0B21565E5E5E5E5E48
        485E065E5E5E5D2A4C5D5E5E5E5E5E5A5A5E255E5E5E381E5E3617090909095E
        5E095E5E5E5E4E435E503D292929295E5E295E5E5E5E31550E1F3B5E5E5E0E5E
        0E5E5E5E270F4A5D2D42525E5E5E2D5E2D5E5E5E422C3216455E5E5E5E5E1616
        5E5E5E5E05004B3D575E5E5E5E5E3D3D5E5E5E5E2411351C5E5E5E5E01015E5E
        5E5E5E3001014D415E5E5E5E12125E5E5E5E5E42121247465E5E5E020E025E5E
        5E3318020E0458585E5E5E222D225E5E5E442F222D245E5E5E5E03141A030303
        03070A1A20195E5E5E5E232E3E23232323262A3E4A2F5E5E5E061B2020202020
        2020375E0B535E5E5E253F4A4A4A4A4A4A4A515E2A5A5E5E5E095E48485E5E5E
        5E5E3A0D395E5E5E5E295E5A5A5E5E5E5E5E582D4F5E5E5E5E5E0E5E5E0E0E0E
        0E151D545E5E5E5E5E5E2D5E5E2D2D2D2D3C405C5E5E5E5E5E5E5E165E165E5E
        5E5E5E5E5E5E5E5E5E5E5E3D5E3D5E5E5E5E5E5E5E5E5E5E5E5E5E5E1A1A5E5E
        5E5E5E5E5E5E5E5E5E5E5E5E3E3E5E5E5E5E5E5E5E5E}
      NumGlyphs = 2
    end
    object ButFiltro: TBitBtn
      Left = 416
      Top = 44
      Width = 91
      Height = 25
      Hint = 'Filtrar dados do cadastro'
      Caption = '&Filtro de dados'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = False
    end
    object ButAltLin: TButton
      Left = 510
      Top = 44
      Width = 91
      Height = 25
      Hint = 'Altera'#231#227'o linear da dados da grade'
      Caption = 'Altera'#231#227'o Linear'
      TabOrder = 1
      Visible = False
    end
    object ButBusca: TBitBtn
      Left = 290
      Top = 14
      Width = 81
      Height = 25
      Hint = 'Buscar cadastro'
      Caption = '&Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        AE030000424DAE03000000000000AE0100002800000020000000100000000100
        08000000000000020000232E0000232E00005E00000000000000512600005157
        61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
        7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
        9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
        BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
        BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
        CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
        D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
        DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
        E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
        ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
        F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
        FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
        09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
        0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
        23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
        065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
        5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
        5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
        5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
        5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
        5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
        5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
        5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
        5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
        5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
        5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
        5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
        5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
      NumGlyphs = 2
    end
    object EdNome: TEdit
      Left = 78
      Top = 18
      Width = 161
      Height = 21
      Hint = 'Busca por nome da especialiadade'
      CharCase = ecUpperCase
      TabOrder = 4
    end
    object EdCod: TEdit
      Left = 1
      Top = 18
      Width = 64
      Height = 21
      Hint = 'Busca por c'#243'digo da especialidade'
      AutoSelect = False
      CharCase = ecUpperCase
      TabOrder = 5
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 23
    Width = 395
    Height = 358
    Align = alClient
    TabOrder = 2
  end
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 23
    Width = 395
    Height = 358
    Align = alClient
    DataSource = DSCadastro
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'ESPECIALIDADE_ID'
        Title.Caption = 'Cod. Especialidade'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Liberado'
        Width = 175
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LIBERADO'
        Visible = True
      end>
  end
  object QCadastro: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM ESPECIALIDADES WHERE ESPECIALIDADE_ID = 0')
    Left = 192
    Top = 192
    object QCadastroESPECIALIDADE_ID: TIntegerField
      FieldName = 'ESPECIALIDADE_ID'
    end
    object QCadastroDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
  end
  object DSCadastro: TDataSource
    DataSet = QCadastro
    Left = 224
    Top = 192
  end
end
