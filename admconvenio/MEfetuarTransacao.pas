
{*************************************************************************************************************}
{                                                                                                             }
{                                              XML Data Binding                                               }
{                                                                                                             }
{         Generated on: 14/06/2010 16:57:17                                                                   }
{       Generated from: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MEfetuarTransacao.xsd   }
{   Settings stored in: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MEfetuarTransacao.xdb   }
{                                                                                                             }
{*************************************************************************************************************}

unit MEfetuarTransacao;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTAbrirTransacaoParam = interface;
  IXMLTCredenciado = interface;
  IXMLTValidarProdutosParam = interface;
  IXMLTProdutos = interface;
  IXMLTProduto = interface;
  IXMLTProdutoList = interface;
  IXMLTFecharTransacaoParam = interface;
  IXMLTPrescrsprod = interface;
  IXMLTPrescrprod = interface;
  IXMLTPrescritor = interface;
  IXMLTConfirmarTransacaoParam = interface;
  IXMLTCancelarTransacaoParam = interface;
  IXMLTAbrirTransacaoRet = interface;
  IXMLTValidarProdutosRet = interface;
  IXMLTProdsval = interface;
  IXMLTProdval = interface;
  IXMLTFecharTransacaoRet = interface;
  IXMLTCupomaut = interface;
  IXMLTCupompd = interface;
  IXMLTConfirmarTransacaoRet = interface;
  IXMLTCancelarTransacaoRet = interface;

{ IXMLTAbrirTransacaoParam }

  IXMLTAbrirTransacaoParam = interface(IXMLNode)
    ['{7C137645-F4FA-4829-AA4B-C953C97D4248}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_OPERADOR: WideString;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_OPERADOR(Value: WideString);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property CARTAO: WideString read Get_CARTAO write Set_CARTAO;
    property CPF: WideString read Get_CPF write Set_CPF;
    property OPERADOR: WideString read Get_OPERADOR write Set_OPERADOR;
  end;

{ IXMLTCredenciado }

  IXMLTCredenciado = interface(IXMLNode)
    ['{D198EF41-FCFC-404B-85ED-612F165A52A6}']
    { Property Accessors }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
    { Methods & Properties }
    property CODACESSO: LongWord read Get_CODACESSO write Set_CODACESSO;
    property SENHA: WideString read Get_SENHA write Set_SENHA;
  end;

{ IXMLTValidarProdutosParam }

  IXMLTValidarProdutosParam = interface(IXMLNode)
    ['{918FEB7F-0C04-4F95-80FC-24B853BEFA57}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTProdutos;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property CARTAO: WideString read Get_CARTAO write Set_CARTAO;
    property CPF: WideString read Get_CPF write Set_CPF;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property PRODUTOS: IXMLTProdutos read Get_PRODUTOS;
  end;

{ IXMLTProdutos }

  IXMLTProdutos = interface(IXMLNode)
    ['{833F69CB-B0E4-485F-A69C-28F2F0AF48BD}']
    { Property Accessors }
    function Get_PRODUTO: IXMLTProdutoList;
    function Get_VALORTOTAL: LongWord;
    procedure Set_VALORTOTAL(Value: LongWord);
    { Methods & Properties }
    property PRODUTO: IXMLTProdutoList read Get_PRODUTO;
    property VALORTOTAL: LongWord read Get_VALORTOTAL write Set_VALORTOTAL;
  end;

{ IXMLTProduto }

  IXMLTProduto = interface(IXMLNode)
    ['{B06DBAEF-1BDF-432B-A953-238043A08D5F}']
    { Property Accessors }
    function Get_CODBARRAS: WideString;
    function Get_DESCRICAO: WideString;
    function Get_QTDE: LongWord;
    function Get_PRCUNITBRU: LongWord;
    function Get_PRCUNITLIQ: LongWord;
    function Get_PRCFABRICA: LongWord;
    function Get_GRUPO: Integer;
    procedure Set_CODBARRAS(Value: WideString);
    procedure Set_DESCRICAO(Value: WideString);
    procedure Set_QTDE(Value: LongWord);
    procedure Set_PRCUNITBRU(Value: LongWord);
    procedure Set_PRCUNITLIQ(Value: LongWord);
    procedure Set_PRCFABRICA(Value: LongWord);
    procedure Set_GRUPO(Value: Integer);
    { Methods & Properties }
    property CODBARRAS: WideString read Get_CODBARRAS write Set_CODBARRAS;
    property DESCRICAO: WideString read Get_DESCRICAO write Set_DESCRICAO;
    property QTDE: LongWord read Get_QTDE write Set_QTDE;
    property PRCUNITBRU: LongWord read Get_PRCUNITBRU write Set_PRCUNITBRU;
    property PRCUNITLIQ: LongWord read Get_PRCUNITLIQ write Set_PRCUNITLIQ;
    property PRCFABRICA: LongWord read Get_PRCFABRICA write Set_PRCFABRICA;
    property GRUPO: Integer read Get_GRUPO write Set_GRUPO;
  end;

{ IXMLTProdutoList }

  IXMLTProdutoList = interface(IXMLNodeCollection)
    ['{97EDDCE0-A2D5-46E4-B64C-1F4FB8123052}']
    { Methods & Properties }
    function Add: IXMLTProduto;
    function Insert(const Index: Integer): IXMLTProduto;
    function Get_Item(Index: Integer): IXMLTProduto;
    property Items[Index: Integer]: IXMLTProduto read Get_Item; default;
  end;

{ IXMLTFecharTransacaoParam }

  IXMLTFecharTransacaoParam = interface(IXMLNode)
    ['{713D7CB0-B6A4-4328-B926-6C10E0F4EF5B}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTPrescrsprod;
    function Get_FORMAPAGTO: LongWord;
    function Get_VALORAUT: LongWord;
    function Get_SENHA_CARTAO: WideString;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_FORMAPAGTO(Value: LongWord);
    procedure Set_VALORAUT(Value: LongWord);
    procedure Set_SENHA_CARTAO(Value: WideString);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property CARTAO: WideString read Get_CARTAO write Set_CARTAO;
    property CPF: WideString read Get_CPF write Set_CPF;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property PRODUTOS: IXMLTPrescrsprod read Get_PRODUTOS;
    property FORMAPAGTO: LongWord read Get_FORMAPAGTO write Set_FORMAPAGTO;
    property VALORAUT: LongWord read Get_VALORAUT write Set_VALORAUT;
    property SENHA_CARTAO: WideString read Get_SENHA_CARTAO write Set_SENHA_CARTAO;
  end;

{ IXMLTPrescrsprod }

  IXMLTPrescrsprod = interface(IXMLNodeCollection)
    ['{4DB3AABD-A347-4BC0-AD2C-FF5924359DAF}']
    { Property Accessors }
    function Get_PRODUTO(Index: Integer): IXMLTPrescrprod;
    { Methods & Properties }
    function Add: IXMLTPrescrprod;
    function Insert(const Index: Integer): IXMLTPrescrprod;
    property PRODUTO[Index: Integer]: IXMLTPrescrprod read Get_PRODUTO; default;
  end;

{ IXMLTPrescrprod }

  IXMLTPrescrprod = interface(IXMLNode)
    ['{5676E9F2-15E1-429C-8148-556173C116C6}']
    { Property Accessors }
    function Get_CODBARRAS: WideString;
    function Get_PRESCRITOR: IXMLTPrescritor;
    procedure Set_CODBARRAS(Value: WideString);
    { Methods & Properties }
    property CODBARRAS: WideString read Get_CODBARRAS write Set_CODBARRAS;
    property PRESCRITOR: IXMLTPrescritor read Get_PRESCRITOR;
  end;

{ IXMLTPrescritor }

  IXMLTPrescritor = interface(IXMLNode)
    ['{F0D3EE31-5DC7-498C-A468-59C478663311}']
    { Property Accessors }
    function Get_TIPOPRESCRITOR: LongWord;
    function Get_UFPRESCRITOR: WideString;
    function Get_NUMPRESCRITOR: WideString;
    procedure Set_TIPOPRESCRITOR(Value: LongWord);
    procedure Set_UFPRESCRITOR(Value: WideString);
    procedure Set_NUMPRESCRITOR(Value: WideString);
    { Methods & Properties }
    property TIPOPRESCRITOR: LongWord read Get_TIPOPRESCRITOR write Set_TIPOPRESCRITOR;
    property UFPRESCRITOR: WideString read Get_UFPRESCRITOR write Set_UFPRESCRITOR;
    property NUMPRESCRITOR: WideString read Get_NUMPRESCRITOR write Set_NUMPRESCRITOR;
  end;

{ IXMLTConfirmarTransacaoParam }

  IXMLTConfirmarTransacaoParam = interface(IXMLNode)
    ['{F49657CF-EAC9-4256-8402-8D7781471B71}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_DOCFISCAL: LongWord;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_DOCFISCAL(Value: LongWord);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property CARTAO: WideString read Get_CARTAO write Set_CARTAO;
    property CPF: WideString read Get_CPF write Set_CPF;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property DOCFISCAL: LongWord read Get_DOCFISCAL write Set_DOCFISCAL;
  end;

{ IXMLTCancelarTransacaoParam }

  IXMLTCancelarTransacaoParam = interface(IXMLNode)
    ['{CB92AD3D-39FB-45EC-82A3-306AF938D4CC}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_TRANSID: LongWord;
    function Get_OPERADOR: WideString;
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_OPERADOR(Value: WideString);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property OPERADOR: WideString read Get_OPERADOR write Set_OPERADOR;
  end;

{ IXMLTAbrirTransacaoRet }

  IXMLTAbrirTransacaoRet = interface(IXMLNode)
    ['{05F34ED8-A3B4-4EDE-8CF8-7EA1226AD170}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_NOMECREDENCIADO: WideString;
    function Get_NOMECONVENIADO: WideString;
    function Get_PROG_DESC: Boolean;
    function Get_FIDELIDADE: Boolean;
    function Get_SALDO_PONTOS: LongWord;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_NOMECREDENCIADO(Value: WideString);
    procedure Set_NOMECONVENIADO(Value: WideString);
    procedure Set_PROG_DESC(Value: Boolean);
    procedure Set_FIDELIDADE(Value: Boolean);
    procedure Set_SALDO_PONTOS(Value: LongWord);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property NOMECREDENCIADO: WideString read Get_NOMECREDENCIADO write Set_NOMECREDENCIADO;
    property NOMECONVENIADO: WideString read Get_NOMECONVENIADO write Set_NOMECONVENIADO;
    property PROG_DESC: Boolean read Get_PROG_DESC write Set_PROG_DESC;
    property FIDELIDADE: Boolean read Get_FIDELIDADE write Set_FIDELIDADE;
    property SALDO_PONTOS: LongWord read Get_SALDO_PONTOS write Set_SALDO_PONTOS;
  end;

{ IXMLTValidarProdutosRet }

  IXMLTValidarProdutosRet = interface(IXMLNode)
    ['{D40988C2-E5C9-4872-B00F-81184C644381}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTProdsval;
    function Get_PONTOSTRANS: Integer;
    function Get_MSGFID: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_PONTOSTRANS(Value: Integer);
    procedure Set_MSGFID(Value: WideString);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property PRODUTOS: IXMLTProdsval read Get_PRODUTOS;
    property PONTOSTRANS: Integer read Get_PONTOSTRANS write Set_PONTOSTRANS;
    property MSGFID: WideString read Get_MSGFID write Set_MSGFID;
  end;

{ IXMLTProdsval }

  IXMLTProdsval = interface(IXMLNodeCollection)
    ['{FA4BFF90-22DC-49AF-B408-C3F4D462BBB3}']
    { Property Accessors }
    function Get_PRODUTO(Index: Integer): IXMLTProdval;
    { Methods & Properties }
    function Add: IXMLTProdval;
    function Insert(const Index: Integer): IXMLTProdval;
    property PRODUTO[Index: Integer]: IXMLTProdval read Get_PRODUTO; default;
  end;

{ IXMLTProdval }

  IXMLTProdval = interface(IXMLNode)
    ['{FAC5775E-60FA-47C9-ACB9-3E9D9E158E37}']
    { Property Accessors }
    function Get_CODBARRAS: WideString;
    function Get_STATUSPROD: LongWord;
    function Get_QTDE: LongWord;
    function Get_PRCUNIT: LongWord;
    function Get_VLRBRU: LongWord;
    function Get_PERCDESC: LongWord;
    function Get_VLRDESC: LongWord;
    function Get_VLRLIQ: LongWord;
    function Get_NOMEPROGRAMA: WideString;
    function Get_OBRIGRECEITA: Boolean;
    function Get_PONTOSPROD: Integer;
    procedure Set_CODBARRAS(Value: WideString);
    procedure Set_STATUSPROD(Value: LongWord);
    procedure Set_QTDE(Value: LongWord);
    procedure Set_PRCUNIT(Value: LongWord);
    procedure Set_VLRBRU(Value: LongWord);
    procedure Set_PERCDESC(Value: LongWord);
    procedure Set_VLRDESC(Value: LongWord);
    procedure Set_VLRLIQ(Value: LongWord);
    procedure Set_NOMEPROGRAMA(Value: WideString);
    procedure Set_OBRIGRECEITA(Value: Boolean);
    procedure Set_PONTOSPROD(Value: Integer);
    { Methods & Properties }
    property CODBARRAS: WideString read Get_CODBARRAS write Set_CODBARRAS;
    property STATUSPROD: LongWord read Get_STATUSPROD write Set_STATUSPROD;
    property QTDE: LongWord read Get_QTDE write Set_QTDE;
    property PRCUNIT: LongWord read Get_PRCUNIT write Set_PRCUNIT;
    property VLRBRU: LongWord read Get_VLRBRU write Set_VLRBRU;
    property PERCDESC: LongWord read Get_PERCDESC write Set_PERCDESC;
    property VLRDESC: LongWord read Get_VLRDESC write Set_VLRDESC;
    property VLRLIQ: LongWord read Get_VLRLIQ write Set_VLRLIQ;
    property NOMEPROGRAMA: WideString read Get_NOMEPROGRAMA write Set_NOMEPROGRAMA;
    property OBRIGRECEITA: Boolean read Get_OBRIGRECEITA write Set_OBRIGRECEITA;
    property PONTOSPROD: Integer read Get_PONTOSPROD write Set_PONTOSPROD;
  end;

{ IXMLTFecharTransacaoRet }

  IXMLTFecharTransacaoRet = interface(IXMLNode)
    ['{29385CB1-8ED5-44C5-A6BD-30F2351C8EA4}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_CUPOMAUT: IXMLTCupomaut;
    function Get_CUPOMPD: IXMLTCupompd;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
    property CUPOMAUT: IXMLTCupomaut read Get_CUPOMAUT;
    property CUPOMPD: IXMLTCupompd read Get_CUPOMPD;
  end;

{ IXMLTCupomaut }

  IXMLTCupomaut = interface(IXMLNodeCollection)
    ['{4BC98754-2D79-421D-BC10-44D14E445EA6}']
    { Property Accessors }
    function Get_LINHA(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const LINHA: WideString): IXMLNode;
    function Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
    property LINHA[Index: Integer]: WideString read Get_LINHA; default;
  end;

{ IXMLTCupompd }

  IXMLTCupompd = interface(IXMLNodeCollection)
    ['{7EE153A3-AA81-4CF8-A16F-21ADFE321EEB}']
    { Property Accessors }
    function Get_LINHA(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const LINHA: WideString): IXMLNode;
    function Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
    property LINHA[Index: Integer]: WideString read Get_LINHA; default;
  end;

{ IXMLTConfirmarTransacaoRet }

  IXMLTConfirmarTransacaoRet = interface(IXMLNode)
    ['{DB97EBF7-D670-4E9F-B16C-932071113C43}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
    property TRANSID: LongWord read Get_TRANSID write Set_TRANSID;
  end;

{ IXMLTCancelarTransacaoRet }

  IXMLTCancelarTransacaoRet = interface(IXMLNode)
    ['{C9787C17-5A1E-4CF3-B81C-BB25D173CCD5}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
  end;

{ Forward Decls }

  TXMLTAbrirTransacaoParam = class;
  TXMLTCredenciado = class;
  TXMLTValidarProdutosParam = class;
  TXMLTProdutos = class;
  TXMLTProduto = class;
  TXMLTProdutoList = class;
  TXMLTFecharTransacaoParam = class;
  TXMLTPrescrsprod = class;
  TXMLTPrescrprod = class;
  TXMLTPrescritor = class;
  TXMLTConfirmarTransacaoParam = class;
  TXMLTCancelarTransacaoParam = class;
  TXMLTAbrirTransacaoRet = class;
  TXMLTValidarProdutosRet = class;
  TXMLTProdsval = class;
  TXMLTProdval = class;
  TXMLTFecharTransacaoRet = class;
  TXMLTCupomaut = class;
  TXMLTCupompd = class;
  TXMLTConfirmarTransacaoRet = class;
  TXMLTCancelarTransacaoRet = class;

{ TXMLTAbrirTransacaoParam }

  TXMLTAbrirTransacaoParam = class(TXMLNode, IXMLTAbrirTransacaoParam)
  protected
    { IXMLTAbrirTransacaoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_OPERADOR: WideString;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_OPERADOR(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCredenciado }

  TXMLTCredenciado = class(TXMLNode, IXMLTCredenciado)
  protected
    { IXMLTCredenciado }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
  end;

{ TXMLTValidarProdutosParam }

  TXMLTValidarProdutosParam = class(TXMLNode, IXMLTValidarProdutosParam)
  protected
    { IXMLTValidarProdutosParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTProdutos;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProdutos }

  TXMLTProdutos = class(TXMLNode, IXMLTProdutos)
  private
    FPRODUTO: IXMLTProdutoList;
  protected
    { IXMLTProdutos }
    function Get_PRODUTO: IXMLTProdutoList;
    function Get_VALORTOTAL: LongWord;
    procedure Set_VALORTOTAL(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProduto }

  TXMLTProduto = class(TXMLNode, IXMLTProduto)
  protected
    { IXMLTProduto }
    function Get_CODBARRAS: WideString;
    function Get_DESCRICAO: WideString;
    function Get_QTDE: LongWord;
    function Get_PRCUNITBRU: LongWord;
    function Get_PRCUNITLIQ: LongWord;
    function Get_PRCFABRICA: LongWord;
    function Get_GRUPO: Integer;
    procedure Set_CODBARRAS(Value: WideString);
    procedure Set_DESCRICAO(Value: WideString);
    procedure Set_QTDE(Value: LongWord);
    procedure Set_PRCUNITBRU(Value: LongWord);
    procedure Set_PRCUNITLIQ(Value: LongWord);
    procedure Set_PRCFABRICA(Value: LongWord);
    procedure Set_GRUPO(Value: Integer);
  end;

{ TXMLTProdutoList }

  TXMLTProdutoList = class(TXMLNodeCollection, IXMLTProdutoList)
  protected
    { IXMLTProdutoList }
    function Add: IXMLTProduto;
    function Insert(const Index: Integer): IXMLTProduto;
    function Get_Item(Index: Integer): IXMLTProduto;
  end;

{ TXMLTFecharTransacaoParam }

  TXMLTFecharTransacaoParam = class(TXMLNode, IXMLTFecharTransacaoParam)
  protected
    { IXMLTFecharTransacaoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTPrescrsprod;
    function Get_FORMAPAGTO: LongWord;
    function Get_VALORAUT: LongWord;
    function Get_SENHA_CARTAO: WideString;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_FORMAPAGTO(Value: LongWord);
    procedure Set_VALORAUT(Value: LongWord);
    procedure Set_SENHA_CARTAO(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTPrescrsprod }

  TXMLTPrescrsprod = class(TXMLNodeCollection, IXMLTPrescrsprod)
  protected
    { IXMLTPrescrsprod }
    function Get_PRODUTO(Index: Integer): IXMLTPrescrprod;
    function Add: IXMLTPrescrprod;
    function Insert(const Index: Integer): IXMLTPrescrprod;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTPrescrprod }

  TXMLTPrescrprod = class(TXMLNode, IXMLTPrescrprod)
  protected
    { IXMLTPrescrprod }
    function Get_CODBARRAS: WideString;
    function Get_PRESCRITOR: IXMLTPrescritor;
    procedure Set_CODBARRAS(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTPrescritor }

  TXMLTPrescritor = class(TXMLNode, IXMLTPrescritor)
  protected
    { IXMLTPrescritor }
    function Get_TIPOPRESCRITOR: LongWord;
    function Get_UFPRESCRITOR: WideString;
    function Get_NUMPRESCRITOR: WideString;
    procedure Set_TIPOPRESCRITOR(Value: LongWord);
    procedure Set_UFPRESCRITOR(Value: WideString);
    procedure Set_NUMPRESCRITOR(Value: WideString);
  end;

{ TXMLTConfirmarTransacaoParam }

  TXMLTConfirmarTransacaoParam = class(TXMLNode, IXMLTConfirmarTransacaoParam)
  protected
    { IXMLTConfirmarTransacaoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_CARTAO: WideString;
    function Get_CPF: WideString;
    function Get_TRANSID: LongWord;
    function Get_DOCFISCAL: LongWord;
    procedure Set_CARTAO(Value: WideString);
    procedure Set_CPF(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_DOCFISCAL(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCancelarTransacaoParam }

  TXMLTCancelarTransacaoParam = class(TXMLNode, IXMLTCancelarTransacaoParam)
  protected
    { IXMLTCancelarTransacaoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_TRANSID: LongWord;
    function Get_OPERADOR: WideString;
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_OPERADOR(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTAbrirTransacaoRet }

  TXMLTAbrirTransacaoRet = class(TXMLNode, IXMLTAbrirTransacaoRet)
  protected
    { IXMLTAbrirTransacaoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_NOMECREDENCIADO: WideString;
    function Get_NOMECONVENIADO: WideString;
    function Get_PROG_DESC: Boolean;
    function Get_FIDELIDADE: Boolean;
    function Get_SALDO_PONTOS: LongWord;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_NOMECREDENCIADO(Value: WideString);
    procedure Set_NOMECONVENIADO(Value: WideString);
    procedure Set_PROG_DESC(Value: Boolean);
    procedure Set_FIDELIDADE(Value: Boolean);
    procedure Set_SALDO_PONTOS(Value: LongWord);
  end;

{ TXMLTValidarProdutosRet }

  TXMLTValidarProdutosRet = class(TXMLNode, IXMLTValidarProdutosRet)
  protected
    { IXMLTValidarProdutosRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_PRODUTOS: IXMLTProdsval;
    function Get_PONTOSTRANS: Integer;
    function Get_MSGFID: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
    procedure Set_PONTOSTRANS(Value: Integer);
    procedure Set_MSGFID(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProdsval }

  TXMLTProdsval = class(TXMLNodeCollection, IXMLTProdsval)
  protected
    { IXMLTProdsval }
    function Get_PRODUTO(Index: Integer): IXMLTProdval;
    function Add: IXMLTProdval;
    function Insert(const Index: Integer): IXMLTProdval;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProdval }

  TXMLTProdval = class(TXMLNode, IXMLTProdval)
  protected
    { IXMLTProdval }
    function Get_CODBARRAS: WideString;
    function Get_STATUSPROD: LongWord;
    function Get_QTDE: LongWord;
    function Get_PRCUNIT: LongWord;
    function Get_VLRBRU: LongWord;
    function Get_PERCDESC: LongWord;
    function Get_VLRDESC: LongWord;
    function Get_VLRLIQ: LongWord;
    function Get_NOMEPROGRAMA: WideString;
    function Get_OBRIGRECEITA: Boolean;
    function Get_PONTOSPROD: Integer;
    procedure Set_CODBARRAS(Value: WideString);
    procedure Set_STATUSPROD(Value: LongWord);
    procedure Set_QTDE(Value: LongWord);
    procedure Set_PRCUNIT(Value: LongWord);
    procedure Set_VLRBRU(Value: LongWord);
    procedure Set_PERCDESC(Value: LongWord);
    procedure Set_VLRDESC(Value: LongWord);
    procedure Set_VLRLIQ(Value: LongWord);
    procedure Set_NOMEPROGRAMA(Value: WideString);
    procedure Set_OBRIGRECEITA(Value: Boolean);
    procedure Set_PONTOSPROD(Value: Integer);
  end;

{ TXMLTFecharTransacaoRet }

  TXMLTFecharTransacaoRet = class(TXMLNode, IXMLTFecharTransacaoRet)
  protected
    { IXMLTFecharTransacaoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    function Get_CUPOMAUT: IXMLTCupomaut;
    function Get_CUPOMPD: IXMLTCupompd;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCupomaut }

  TXMLTCupomaut = class(TXMLNodeCollection, IXMLTCupomaut)
  protected
    { IXMLTCupomaut }
    function Get_LINHA(Index: Integer): WideString;
    function Add(const LINHA: WideString): IXMLNode;
    function Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCupompd }

  TXMLTCupompd = class(TXMLNodeCollection, IXMLTCupompd)
  protected
    { IXMLTCupompd }
    function Get_LINHA(Index: Integer): WideString;
    function Add(const LINHA: WideString): IXMLNode;
    function Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTConfirmarTransacaoRet }

  TXMLTConfirmarTransacaoRet = class(TXMLNode, IXMLTConfirmarTransacaoRet)
  protected
    { IXMLTConfirmarTransacaoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_TRANSID: LongWord;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    procedure Set_TRANSID(Value: LongWord);
  end;

{ TXMLTCancelarTransacaoRet }

  TXMLTCancelarTransacaoRet = class(TXMLNode, IXMLTCancelarTransacaoRet)
  protected
    { IXMLTCancelarTransacaoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
  end;

implementation

{ TXMLTAbrirTransacaoParam }

procedure TXMLTAbrirTransacaoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  inherited;
end;

function TXMLTAbrirTransacaoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTAbrirTransacaoParam.Get_CARTAO: WideString;
begin
  Result := ChildNodes['CARTAO'].Text;
end;

procedure TXMLTAbrirTransacaoParam.Set_CARTAO(Value: WideString);
begin
  ChildNodes['CARTAO'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoParam.Get_CPF: WideString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTAbrirTransacaoParam.Set_CPF(Value: WideString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoParam.Get_OPERADOR: WideString;
begin
  Result := ChildNodes['OPERADOR'].Text;
end;

procedure TXMLTAbrirTransacaoParam.Set_OPERADOR(Value: WideString);
begin
  ChildNodes['OPERADOR'].NodeValue := Value;
end;

{ TXMLTCredenciado }

function TXMLTCredenciado.Get_CODACESSO: LongWord;
begin
  Result := ChildNodes['CODACESSO'].NodeValue;
end;

procedure TXMLTCredenciado.Set_CODACESSO(Value: LongWord);
begin
  ChildNodes['CODACESSO'].NodeValue := Value;
end;

function TXMLTCredenciado.Get_SENHA: WideString;
begin
  Result := ChildNodes['SENHA'].Text;
end;

procedure TXMLTCredenciado.Set_SENHA(Value: WideString);
begin
  ChildNodes['SENHA'].NodeValue := Value;
end;

{ TXMLTValidarProdutosParam }

procedure TXMLTValidarProdutosParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  RegisterChildNode('PRODUTOS', TXMLTProdutos);
  inherited;
end;

function TXMLTValidarProdutosParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTValidarProdutosParam.Get_CARTAO: WideString;
begin
  Result := ChildNodes['CARTAO'].Text;
end;

procedure TXMLTValidarProdutosParam.Set_CARTAO(Value: WideString);
begin
  ChildNodes['CARTAO'].NodeValue := Value;
end;

function TXMLTValidarProdutosParam.Get_CPF: WideString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTValidarProdutosParam.Set_CPF(Value: WideString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTValidarProdutosParam.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTValidarProdutosParam.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTValidarProdutosParam.Get_PRODUTOS: IXMLTProdutos;
begin
  Result := ChildNodes['PRODUTOS'] as IXMLTProdutos;
end;

{ TXMLTProdutos }

procedure TXMLTProdutos.AfterConstruction;
begin
  RegisterChildNode('PRODUTO', TXMLTProduto);
  FPRODUTO := CreateCollection(TXMLTProdutoList, IXMLTProduto, 'PRODUTO') as IXMLTProdutoList;
  inherited;
end;

function TXMLTProdutos.Get_PRODUTO: IXMLTProdutoList;
begin
  Result := FPRODUTO;
end;

function TXMLTProdutos.Get_VALORTOTAL: LongWord;
begin
  Result := ChildNodes['VALORTOTAL'].NodeValue;
end;

procedure TXMLTProdutos.Set_VALORTOTAL(Value: LongWord);
begin
  ChildNodes['VALORTOTAL'].NodeValue := Value;
end;

{ TXMLTProduto }

function TXMLTProduto.Get_CODBARRAS: WideString;
begin
  Result := ChildNodes['CODBARRAS'].Text;
end;

procedure TXMLTProduto.Set_CODBARRAS(Value: WideString);
begin
  ChildNodes['CODBARRAS'].NodeValue := Value;
end;

function TXMLTProduto.Get_DESCRICAO: WideString;
begin
  Result := ChildNodes['DESCRICAO'].Text;
end;

procedure TXMLTProduto.Set_DESCRICAO(Value: WideString);
begin
  ChildNodes['DESCRICAO'].NodeValue := Value;
end;

function TXMLTProduto.Get_QTDE: LongWord;
begin
  Result := ChildNodes['QTDE'].NodeValue;
end;

procedure TXMLTProduto.Set_QTDE(Value: LongWord);
begin
  ChildNodes['QTDE'].NodeValue := Value;
end;

function TXMLTProduto.Get_PRCUNITBRU: LongWord;
begin
  Result := ChildNodes['PRCUNITBRU'].NodeValue;
end;

procedure TXMLTProduto.Set_PRCUNITBRU(Value: LongWord);
begin
  ChildNodes['PRCUNITBRU'].NodeValue := Value;
end;

function TXMLTProduto.Get_PRCUNITLIQ: LongWord;
begin
  Result := ChildNodes['PRCUNITLIQ'].NodeValue;
end;

procedure TXMLTProduto.Set_PRCUNITLIQ(Value: LongWord);
begin
  ChildNodes['PRCUNITLIQ'].NodeValue := Value;
end;

function TXMLTProduto.Get_PRCFABRICA: LongWord;
begin
  Result := ChildNodes['PRCFABRICA'].NodeValue;
end;

procedure TXMLTProduto.Set_PRCFABRICA(Value: LongWord);
begin
  ChildNodes['PRCFABRICA'].NodeValue := Value;
end;

function TXMLTProduto.Get_GRUPO: Integer;
begin
  Result := ChildNodes['GRUPO'].NodeValue;
end;

procedure TXMLTProduto.Set_GRUPO(Value: Integer);
begin
  ChildNodes['GRUPO'].NodeValue := Value;
end;

{ TXMLTProdutoList }

function TXMLTProdutoList.Add: IXMLTProduto;
begin
  Result := AddItem(-1) as IXMLTProduto;
end;

function TXMLTProdutoList.Insert(const Index: Integer): IXMLTProduto;
begin
  Result := AddItem(Index) as IXMLTProduto;
end;
function TXMLTProdutoList.Get_Item(Index: Integer): IXMLTProduto;
begin
  Result := List[Index] as IXMLTProduto;
end;

{ TXMLTFecharTransacaoParam }

procedure TXMLTFecharTransacaoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  RegisterChildNode('PRODUTOS', TXMLTPrescrsprod);
  inherited;
end;

function TXMLTFecharTransacaoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTFecharTransacaoParam.Get_CARTAO: WideString;
begin
  Result := ChildNodes['CARTAO'].Text;
end;

procedure TXMLTFecharTransacaoParam.Set_CARTAO(Value: WideString);
begin
  ChildNodes['CARTAO'].NodeValue := Value;
end;

function TXMLTFecharTransacaoParam.Get_CPF: WideString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTFecharTransacaoParam.Set_CPF(Value: WideString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTFecharTransacaoParam.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTFecharTransacaoParam.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTFecharTransacaoParam.Get_PRODUTOS: IXMLTPrescrsprod;
begin
  Result := ChildNodes['PRODUTOS'] as IXMLTPrescrsprod;
end;

function TXMLTFecharTransacaoParam.Get_FORMAPAGTO: LongWord;
begin
  Result := ChildNodes['FORMAPAGTO'].NodeValue;
end;

procedure TXMLTFecharTransacaoParam.Set_FORMAPAGTO(Value: LongWord);
begin
  ChildNodes['FORMAPAGTO'].NodeValue := Value;
end;

function TXMLTFecharTransacaoParam.Get_VALORAUT: LongWord;
begin
  Result := ChildNodes['VALORAUT'].NodeValue;
end;

procedure TXMLTFecharTransacaoParam.Set_VALORAUT(Value: LongWord);
begin
  ChildNodes['VALORAUT'].NodeValue := Value;
end;

function TXMLTFecharTransacaoParam.Get_SENHA_CARTAO: WideString;
begin
  Result := ChildNodes['SENHA_CARTAO'].NodeValue;
end;

procedure TXMLTFecharTransacaoParam.Set_SENHA_CARTAO(Value: WideString);
begin
  ChildNodes['SENHA_CARTAO'].NodeValue := Value;
end;

{ TXMLTPrescrsprod }

procedure TXMLTPrescrsprod.AfterConstruction;
begin
  RegisterChildNode('PRODUTO', TXMLTPrescrprod);
  ItemTag := 'PRODUTO';
  ItemInterface := IXMLTPrescrprod;
  inherited;
end;

function TXMLTPrescrsprod.Get_PRODUTO(Index: Integer): IXMLTPrescrprod;
begin
  Result := List[Index] as IXMLTPrescrprod;
end;

function TXMLTPrescrsprod.Add: IXMLTPrescrprod;
begin
  Result := AddItem(-1) as IXMLTPrescrprod;
end;

function TXMLTPrescrsprod.Insert(const Index: Integer): IXMLTPrescrprod;
begin
  Result := AddItem(Index) as IXMLTPrescrprod;
end;

{ TXMLTPrescrprod }

procedure TXMLTPrescrprod.AfterConstruction;
begin
  RegisterChildNode('PRESCRITOR', TXMLTPrescritor);
  inherited;
end;

function TXMLTPrescrprod.Get_CODBARRAS: WideString;
begin
  Result := ChildNodes['CODBARRAS'].Text;
end;

procedure TXMLTPrescrprod.Set_CODBARRAS(Value: WideString);
begin
  ChildNodes['CODBARRAS'].NodeValue := Value;
end;

function TXMLTPrescrprod.Get_PRESCRITOR: IXMLTPrescritor;
begin
  Result := ChildNodes['PRESCRITOR'] as IXMLTPrescritor;
end;

{ TXMLTPrescritor }

function TXMLTPrescritor.Get_TIPOPRESCRITOR: LongWord;
begin
  Result := ChildNodes['TIPOPRESCRITOR'].NodeValue;
end;

procedure TXMLTPrescritor.Set_TIPOPRESCRITOR(Value: LongWord);
begin
  ChildNodes['TIPOPRESCRITOR'].NodeValue := Value;
end;

function TXMLTPrescritor.Get_UFPRESCRITOR: WideString;
begin
  Result := ChildNodes['UFPRESCRITOR'].Text;
end;

procedure TXMLTPrescritor.Set_UFPRESCRITOR(Value: WideString);
begin
  ChildNodes['UFPRESCRITOR'].NodeValue := Value;
end;

function TXMLTPrescritor.Get_NUMPRESCRITOR: WideString;
begin
  Result := ChildNodes['NUMPRESCRITOR'].Text;
end;

procedure TXMLTPrescritor.Set_NUMPRESCRITOR(Value: WideString);
begin
  ChildNodes['NUMPRESCRITOR'].NodeValue := Value;
end;

{ TXMLTConfirmarTransacaoParam }

procedure TXMLTConfirmarTransacaoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  inherited;
end;

function TXMLTConfirmarTransacaoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTConfirmarTransacaoParam.Get_CARTAO: WideString;
begin
  Result := ChildNodes['CARTAO'].Text;
end;

procedure TXMLTConfirmarTransacaoParam.Set_CARTAO(Value: WideString);
begin
  ChildNodes['CARTAO'].NodeValue := Value;
end;

function TXMLTConfirmarTransacaoParam.Get_CPF: WideString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTConfirmarTransacaoParam.Set_CPF(Value: WideString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTConfirmarTransacaoParam.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTConfirmarTransacaoParam.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTConfirmarTransacaoParam.Get_DOCFISCAL: LongWord;
begin
  Result := ChildNodes['DOCFISCAL'].NodeValue;
end;

procedure TXMLTConfirmarTransacaoParam.Set_DOCFISCAL(Value: LongWord);
begin
  ChildNodes['DOCFISCAL'].NodeValue := Value;
end;

{ TXMLTCancelarTransacaoParam }

procedure TXMLTCancelarTransacaoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  inherited;
end;

function TXMLTCancelarTransacaoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTCancelarTransacaoParam.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTCancelarTransacaoParam.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTCancelarTransacaoParam.Get_OPERADOR: WideString;
begin
  Result := ChildNodes['OPERADOR'].Text;
end;

procedure TXMLTCancelarTransacaoParam.Set_OPERADOR(Value: WideString);
begin
  ChildNodes['OPERADOR'].NodeValue := Value;
end;

{ TXMLTAbrirTransacaoRet }

function TXMLTAbrirTransacaoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTAbrirTransacaoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTAbrirTransacaoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTAbrirTransacaoRet.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_NOMECREDENCIADO: WideString;
begin
  Result := ChildNodes['NOMECREDENCIADO'].Text;
end;

procedure TXMLTAbrirTransacaoRet.Set_NOMECREDENCIADO(Value: WideString);
begin
  ChildNodes['NOMECREDENCIADO'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_NOMECONVENIADO: WideString;
begin
  Result := ChildNodes['NOMECONVENIADO'].Text;
end;

procedure TXMLTAbrirTransacaoRet.Set_NOMECONVENIADO(Value: WideString);
begin
  ChildNodes['NOMECONVENIADO'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_PROG_DESC: Boolean;
begin
  Result := ChildNodes['PROG_DESC'].NodeValue;
end;

procedure TXMLTAbrirTransacaoRet.Set_PROG_DESC(Value: Boolean);
begin
  ChildNodes['PROG_DESC'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_FIDELIDADE: Boolean;
begin
  Result := ChildNodes['FIDELIDADE'].NodeValue;
end;

procedure TXMLTAbrirTransacaoRet.Set_FIDELIDADE(Value: Boolean);
begin
  ChildNodes['FIDELIDADE'].NodeValue := Value;
end;

function TXMLTAbrirTransacaoRet.Get_SALDO_PONTOS: LongWord;
begin
  Result := ChildNodes['SALDO_PONTOS'].NodeValue;
end;

procedure TXMLTAbrirTransacaoRet.Set_SALDO_PONTOS(Value: LongWord);
begin
  ChildNodes['SALDO_PONTOS'].NodeValue := Value;
end;

{ TXMLTValidarProdutosRet }

procedure TXMLTValidarProdutosRet.AfterConstruction;
begin
  RegisterChildNode('PRODUTOS', TXMLTProdsval);
  inherited;
end;

function TXMLTValidarProdutosRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTValidarProdutosRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTValidarProdutosRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTValidarProdutosRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

function TXMLTValidarProdutosRet.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTValidarProdutosRet.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTValidarProdutosRet.Get_PRODUTOS: IXMLTProdsval;
begin
  Result := ChildNodes['PRODUTOS'] as IXMLTProdsval;
end;

function TXMLTValidarProdutosRet.Get_PONTOSTRANS: Integer;
begin
  Result := ChildNodes['PONTOSTRANS'].NodeValue;
end;

procedure TXMLTValidarProdutosRet.Set_PONTOSTRANS(Value: Integer);
begin
  ChildNodes['PONTOSTRANS'].NodeValue := Value;
end;

function TXMLTValidarProdutosRet.Get_MSGFID: WideString;
begin
  Result := ChildNodes['MSGFID'].Text;
end;

procedure TXMLTValidarProdutosRet.Set_MSGFID(Value: WideString);
begin
  ChildNodes['MSGFID'].NodeValue := Value;
end;

{ TXMLTProdsval }

procedure TXMLTProdsval.AfterConstruction;
begin
  RegisterChildNode('PRODUTO', TXMLTProdval);
  ItemTag := 'PRODUTO';
  ItemInterface := IXMLTProdval;
  inherited;
end;

function TXMLTProdsval.Get_PRODUTO(Index: Integer): IXMLTProdval;
begin
  Result := List[Index] as IXMLTProdval;
end;

function TXMLTProdsval.Add: IXMLTProdval;
begin
  Result := AddItem(-1) as IXMLTProdval;
end;

function TXMLTProdsval.Insert(const Index: Integer): IXMLTProdval;
begin
  Result := AddItem(Index) as IXMLTProdval;
end;

{ TXMLTProdval }

function TXMLTProdval.Get_CODBARRAS: WideString;
begin
  Result := ChildNodes['CODBARRAS'].Text;
end;

procedure TXMLTProdval.Set_CODBARRAS(Value: WideString);
begin
  ChildNodes['CODBARRAS'].NodeValue := Value;
end;

function TXMLTProdval.Get_STATUSPROD: LongWord;
begin
  Result := ChildNodes['STATUSPROD'].NodeValue;
end;

procedure TXMLTProdval.Set_STATUSPROD(Value: LongWord);
begin
  ChildNodes['STATUSPROD'].NodeValue := Value;
end;

function TXMLTProdval.Get_QTDE: LongWord;
begin
  Result := ChildNodes['QTDE'].NodeValue;
end;

procedure TXMLTProdval.Set_QTDE(Value: LongWord);
begin
  ChildNodes['QTDE'].NodeValue := Value;
end;

function TXMLTProdval.Get_PRCUNIT: LongWord;
begin
  Result := ChildNodes['PRCUNIT'].NodeValue;
end;

procedure TXMLTProdval.Set_PRCUNIT(Value: LongWord);
begin
  ChildNodes['PRCUNIT'].NodeValue := Value;
end;

function TXMLTProdval.Get_VLRBRU: LongWord;
begin
  Result := ChildNodes['VLRBRU'].NodeValue;
end;

procedure TXMLTProdval.Set_VLRBRU(Value: LongWord);
begin
  ChildNodes['VLRBRU'].NodeValue := Value;
end;

function TXMLTProdval.Get_PERCDESC: LongWord;
begin
  Result := ChildNodes['PERCDESC'].NodeValue;
end;

procedure TXMLTProdval.Set_PERCDESC(Value: LongWord);
begin
  ChildNodes['PERCDESC'].NodeValue := Value;
end;

function TXMLTProdval.Get_VLRDESC: LongWord;
begin
  Result := ChildNodes['VLRDESC'].NodeValue;
end;

procedure TXMLTProdval.Set_VLRDESC(Value: LongWord);
begin
  ChildNodes['VLRDESC'].NodeValue := Value;
end;

function TXMLTProdval.Get_VLRLIQ: LongWord;
begin
  Result := ChildNodes['VLRLIQ'].NodeValue;
end;

procedure TXMLTProdval.Set_VLRLIQ(Value: LongWord);
begin
  ChildNodes['VLRLIQ'].NodeValue := Value;
end;

function TXMLTProdval.Get_NOMEPROGRAMA: WideString;
begin
  Result := ChildNodes['NOMEPROGRAMA'].Text;
end;

procedure TXMLTProdval.Set_NOMEPROGRAMA(Value: WideString);
begin
  ChildNodes['NOMEPROGRAMA'].NodeValue := Value;
end;

function TXMLTProdval.Get_OBRIGRECEITA: Boolean;
begin
  Result := ChildNodes['OBRIGRECEITA'].NodeValue;
end;

procedure TXMLTProdval.Set_OBRIGRECEITA(Value: Boolean);
begin
  ChildNodes['OBRIGRECEITA'].NodeValue := Value;
end;

function TXMLTProdval.Get_PONTOSPROD: Integer;
begin
  Result := ChildNodes['PONTOSPROD'].NodeValue;
end;

procedure TXMLTProdval.Set_PONTOSPROD(Value: Integer);
begin
  ChildNodes['PONTOSPROD'].NodeValue := Value;
end;

{ TXMLTFecharTransacaoRet }

procedure TXMLTFecharTransacaoRet.AfterConstruction;
begin
  RegisterChildNode('CUPOMAUT', TXMLTCupomaut);
  RegisterChildNode('CUPOMPD', TXMLTCupompd);
  inherited;
end;

function TXMLTFecharTransacaoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTFecharTransacaoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTFecharTransacaoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTFecharTransacaoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

function TXMLTFecharTransacaoRet.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTFecharTransacaoRet.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

function TXMLTFecharTransacaoRet.Get_CUPOMAUT: IXMLTCupomaut;
begin
  Result := ChildNodes['CUPOMAUT'] as IXMLTCupomaut;
end;

function TXMLTFecharTransacaoRet.Get_CUPOMPD: IXMLTCupompd;
begin
  Result := ChildNodes['CUPOMPD'] as IXMLTCupompd;
end;

{ TXMLTCupomaut }

procedure TXMLTCupomaut.AfterConstruction;
begin
  ItemTag := 'LINHA';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTCupomaut.Get_LINHA(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLTCupomaut.Add(const LINHA: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := LINHA;
end;

function TXMLTCupomaut.Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := LINHA;
end;

{ TXMLTCupompd }

procedure TXMLTCupompd.AfterConstruction;
begin
  ItemTag := 'LINHA';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTCupompd.Get_LINHA(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLTCupompd.Add(const LINHA: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := LINHA;
end;

function TXMLTCupompd.Insert(const Index: Integer; const LINHA: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := LINHA;
end;

{ TXMLTConfirmarTransacaoRet }

function TXMLTConfirmarTransacaoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTConfirmarTransacaoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTConfirmarTransacaoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTConfirmarTransacaoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

function TXMLTConfirmarTransacaoRet.Get_TRANSID: LongWord;
begin
  Result := ChildNodes['TRANSID'].NodeValue;
end;

procedure TXMLTConfirmarTransacaoRet.Set_TRANSID(Value: LongWord);
begin
  ChildNodes['TRANSID'].NodeValue := Value;
end;

{ TXMLTCancelarTransacaoRet }

function TXMLTCancelarTransacaoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTCancelarTransacaoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTCancelarTransacaoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTCancelarTransacaoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

end. 