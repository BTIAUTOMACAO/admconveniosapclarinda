unit UClassLog;

interface

uses
  SysUtils, Types, Classes, Variants, Forms, Windows, Buttons, DB,
  DateUtils, StrUtils, ADODB;


type
   TLog = Class(TObject)
   Private
      //Connex  : TIBConnection;
      //QGrava : TZQuery;
      //QGrava : T;
      Connex  : TADOConnection;
      QGrava : TAdoQuery;
      DSet   : TDataSet;
      Alterando : Boolean;
      Janela,Detalhe,Cadastro, ChavePri, Operador : String;
      FieldsValues : array of Variant;
      FieldsCount  : Integer;
      CodLojasLeram, CodFisica, CodUsuario : String;
      OriginalBeforeEdit: TDataSetNotifyEvent;
      OriginalAfterPost: TDataSetNotifyEvent;
      OriginalBeforePost: TDataSetNotifyEvent;
      procedure BeforeEdit(DataSet: TDataSet);
      procedure AfterPost(DataSet: TDataSet);
      procedure BeforePost(DataSet: TDataSet);
      procedure GravaLog(FieldIndex:Integer);
      procedure FormataValores(out old, new: string; FieldIndex: Integer);
    procedure GravaBarraProd(Prod: Integer; OldValue, NewValue: Variant);
    procedure GravaConvDetail(Conv: Integer; Campo: String; OldValue,
      NewValue: Variant);
   public
      destructor Destroy;override;
      constructor Create;
      function NaoLogar(FieldName:String;FieldType:TFieldKind):Boolean;
      Procedure LogarQuery(DataSet:TDataSet;Janela,Detalhe,Cadastro,Operador,ChavePrimaria:String);
      procedure FormatarValores(out old, new: String;OldValue,NewValue:Variant;FieldType: TFieldType);
      Procedure LogarCampo(JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, OPERACAO, CADASTRO, DETALHE, MOTIVO, SOLICITANTE :String; ID :Integer);

   end;

   TSimpleField = Class(TObject)
   public
      Tipo   : TFieldType;
      Kind   : TFieldKind;
      Valor  : Variant;
      Nome   : String;
      Titulo : String;
   end;

   TTableLog = Class(TObject)
   private
      QTable  : TAdoQuery;
      Connex  : TAdoConnection;
      _Table  : String;
      _Fields : String;
      _Where  : String;
      _FilFis : Integer;
      IFields : array of TSimpleField;
      CodLojasLeram, CodFisica,  CodUsuario: String;
      function ExtractTableName(UpdateStr:String):String;
      function ExtractFieldsName(UpdateStr:String):String;
      procedure ClearArray;
   public
      procedure SetTableName(TableName:String);
      function  GetTableName:String;
      procedure SetFields(Fields:string);
      function  GetFields:String;
      procedure SetTableAndFields(UpdateSql:String);
      procedure SetWhere(WhereStr:String);
      procedure Iniciar;
      procedure CompararLogar(ChavePri:Integer);
      procedure SetTitulos;
      constructor Create;
      destructor Destroy;
   end;
   function CoalesceField(Field:TField):Variant;

implementation

uses DM, cartao_util, FOcorrencia;

{ TLog }

procedure TLog.AfterPost(DataSet: TDataSet);
var i : Integer;
begin
if Alterando then begin
   For i := 0 to FieldsCount - 1 do
       if CoalesceField(DSet.Fields[i]) <> FieldsValues[i] then
          GravaLog(i);
end;
if Assigned(OriginalAfterPost) then
   OriginalAfterPost(DataSet);
end;

procedure TLog.BeforePost(DataSet: TDataSet);
begin
Alterando := (DSet.State = dsEdit);
if Assigned(OriginalBeforePost) then
   OriginalBeforePost(DataSet);
end;

procedure TLog.BeforeEdit(DataSet: TDataSet);
var i : integer;
begin
for i := 0 to FieldsCount - 1 do
   FieldsValues[i] := CoalesceField(DSet.Fields[i]);
if Assigned(OriginalBeforeEdit) then
   OriginalBeforeEdit(DataSet);
end;

function Tlog.NaoLogar(FieldName:String;FieldType:TFieldKind):Boolean;
begin
   Result := False;
   FieldName := LowerCase(FieldName);
   if AnsiContainsText('DTALTERACAO,OPERADOR,DATA_ALTERACAO,MARCADO,FLAG',FieldName) or (FieldType = fkCalculated) then
      Result := True;
end;

procedure TLog.GravaLog(FieldIndex:Integer);
var old, new, campo : string; filial, chave : integer;
solicitante, motivo: string;
begin
   if NaoLogar(DSet.Fields[FieldIndex].FieldName,DSet.Fields[FieldIndex].FieldKind) then exit;
   FormataValores(old,new,FieldIndex);
   chave := DSet.fieldbyname(ChavePri).AsInteger;
   campo := DSet.Fields[FieldIndex].DisplayLabel;
   LogarCampo(Janela,campo,old,new,Operador,'Altera��o',Cadastro,Detalhe+IntToStr(chave), Ocorrencia.Motivo, Ocorrencia.Solicitante, chave);
   if ((Janela = 'FCadConv') and (AnsiContainsText('PIS,NOME_PAI,NOME_MAE,CART_TRAB_NUM,CART_TRAB_SERIE,REGIME_TRAB,VENC_TOTAL,ESTADO_CIVIL,NUM_DEPENDENTES,DATA_ADMISSAO,DATA_DEMISSAO,FIM_CONTRATO,DISTRITO,SALDO_DEVEDOR,SALDO_DEVEDOR_FAT',DSet.Fields[FieldIndex].FieldName) and (DSet.Fields[FieldIndex].FieldName <> 'ESTADO'))) then
   begin
     GravaConvDetail(Chave, DSet.Fields[FieldIndex].FieldName, DSet.Fields[FieldIndex].OldValue, DSet.Fields[FieldIndex].NewValue);
   end;
   if ((Janela = 'FCadProduto2') and (AnsiContainsText('BARRAS',DSet.Fields[FieldIndex].FieldName))) then
   begin
     GravaBarraProd(chave, DSet.Fields[FieldIndex].OldValue, DSet.Fields[FieldIndex].NewValue);
   end;
{
var old, new, campo : string; filial, chave : integer;
begin
   if NaoLogar(DSet.Fields[FieldIndex].FieldName,DSet.Fields[FieldIndex].FieldKind) then  exit;
   FormataValores(old,new,FieldIndex);
   chave := DSet.fieldbyname(ChavePri).AsInteger;
   campo := DSet.Fields[FieldIndex].DisplayLabel;
   LogarCampo(Janela,campo,old,new,Operador,'Altera��o',Cadastro,Detalhe+IntToStr(chave),chave);
}
end;

procedure TLog.GravaConvDetail(Conv: Integer; Campo: String; OldValue, NewValue: Variant);
begin
  //Q.RequestLive:= True;
  DMConexao.AdoQry.SQL.Text := 'select * from conv_detail where conv_id = '+IntToStr(Conv);
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.RecordCount = 0 then
  begin
    DMConexao.AdoQry.Append;
    DMConexao.AdoQry.FieldByName('CONV_ID').AsInteger:= Conv;
    DMConexao.AdoQry.Post;
  end;
  DMConexao.AdoQry.Edit;
  DMConexao.AdoQry.FieldByName(Campo).Value:= NewValue;
  DMConexao.AdoQry.Post;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.Fields.Clear;
  DMConexao.AdoQry.SQL.Clear;
end;

procedure TLog.GravaBarraProd(Prod: Integer; OldValue, NewValue: Variant);
  var Q, Q2 : TAdoQuery;
  Id: Integer;
begin
  Q := TAdoQuery.Create(nil);
  Q.Connection := Connex;
  //Q.RequestLive:= True;
  Q.SQL.Text := 'select * from barras where prod_id = '+IntToStr(Prod);
  Q.Open;
  if Q.RecordCount = 0 then
  begin
    Q2 := TAdoQuery.Create(nil);
    Q2.Connection := Connex;
    Q2.SQL.Text := 'select gen_id(gen_barras_id,1) from rdb$database ';
    Q2.Open;
    Id:= Q2.Fields[0].AsInteger;
    Q2.Free;
    Q.Append;
    Q.FieldByName('PROD_ID').AsInteger:= Prod;
    Q.Post;
  end;
  Q.Edit;
  Q.FieldByName('BARRAS').Value:= NewValue;
  Q.Post;
  Q.Free;
end;

procedure TLog.FormataValores(out old, new : string;FieldIndex:Integer);
begin
  FormatarValores(old,new,fieldsvalues[FieldIndex],DSet.Fields[FieldIndex].AsVariant,DSet.Fields[FieldIndex].DataType);
end;

procedure TLog.FormatarValores(out old, new:String;OldValue,NewValue:Variant;FieldType:TFieldType);
begin
   case FieldType of
      ftSmallint, ftInteger, ftWord :
      begin
         if OldValue = null then OldValue := 0;
         old := IntToStr(OldValue);
         if NewValue = null then NewValue := 0;
         new := IntToStr(NewValue);
      end;
      ftFloat, ftCurrency, ftBCD :
      begin
         if OldValue = null then OldValue := 0;
         old := FormatDinBR(OldValue);
         if NewValue = null then NewValue := 0;
         new := FormatDinBR(NewValue);
      end;
      ftDate, ftDateTime, ftTimeStamp :
      begin
         if OldValue = null then
            old := ''
         else
            old := FormatDateTime('dd/mm/yyyy hh:nn:ss',OldValue);
         if NewValue = null then
            new := ''
         else
           new := FormatDateTime('dd/mm/yyyy hh:nn:ss',NewValue);
      end;
      ftTime :
      begin
         if OldValue = null then
            old := ''
         else
            old := FormatDateTime('hh:nn:ss',OldValue);
         if NewValue = null then
            new := ''
         else
           new := FormatDateTime('hh:nn:ss',NewValue);
      end;
      else begin
         if OldValue = null then
            old := ''
         else
            old := string(OldValue);
         if NewValue = null then
            new := ''
         else
           new := string(NewValue);
      end;
   end;
end;

destructor TLog.Destroy;
begin
  DSet.BeforeEdit := OriginalBeforeEdit;
  DSet.AfterPost  := OriginalAfterPost;
  DSet.BeforePost := OriginalBeforePost;
  Dset := nil;
  QGrava.Free;
  inherited Destroy;
end;

procedure TLog.LogarQuery(DataSet:TDataSet;Janela,Detalhe,Cadastro,Operador,ChavePrimaria:String);
begin
   DSet := DataSet;

   OriginalBeforeEdit  := DSet.BeforeEdit;
   OriginalAfterPost   := DSet.AfterPost;
   OriginalBeforePost  := DSet.BeforePost;

   DSet.BeforeEdit     := BeforeEdit;
   DSet.AfterPost      := AfterPost;
   DSet.BeforePost     := BeforePost;

   FieldsCount         := DSet.FieldCount;
   SetLength(FieldsValues,FieldsCount);
   Self.Janela   := Janela;
   Self.Detalhe  := Detalhe;
   Self.Cadastro := Cadastro;
   Self.Operador := Operador;
   ChavePri      := ChavePrimaria;
   
   if Trim(ChavePrimaria) = '' then
      raise Exception.Create('Erro ao mapear dataset, informe a Chave primaria');

end;

procedure TLog.LogarCampo(JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, OPERACAO, CADASTRO, DETALHE, MOTIVO, SOLICITANTE :String; ID :Integer);
var
IdLog: Integer;
begin
   VALOR_ANT := COPY(VALOR_ANT,1,200);
   VALOR_POS := COPY(VALOR_POS,1,200);

   Self.QGrava.Close;
   Self.QGrava.SQL.Clear;
   Self.QGrava.SQL.Add(' insert into logs (LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, OPERACAO, CADASTRO, ID, DETALHE, DATA_HORA, MOTIVO, SOLICITANTE) values (NEXT VALUE FOR SLOG_ID,');
   Self.QGrava.SQL.Add(' ''' + JANELA + ''',''' +copy(CAMPO,1,20)+''','''+VALOR_ANT+''',' );
   self.QGrava.SQL.Add(' '''+VALOR_POS+''','''+OPERADOR+''','''+OPERACAO+''', NULL'+','+inttostr(ID)+',NULL'+',current_timestamp,'''+MOTIVO+''','''+SOLICITANTE+''' )');
   QGrava.ExecSQL;
end;

constructor TLog.Create;
begin
 inherited;
 Connex := DMConexao.AdoCon;

 QGrava := TAdoQuery.Create(Connex);
 QGrava.Connection := Connex;
end;

{ TTableLog }

function TTableLog.ExtractFieldsName(UpdateStr: String): String;
var temp, f : String; i : integer; pegar : Boolean;
begin
   UpdateStr := LowerCase(UpdateStr);
   temp := Copy(UpdateStr,pos('set',UpdateStr)+4,length(UpdateStr));
   temp := Trim( Copy(temp,1,Pos('where',temp)-1) );
   pegar := true;
   for i := 1 to length(temp) do begin
       if pegar and ( temp[i] <> '=') then
          f := f+temp[i]
       else if temp[i] = '=' then begin
          _Fields := _Fields + f + ',';
          f := '';
          pegar := false;
       end
       else if temp[i] = ',' then
          pegar := true;
   end;
   if _Fields[length(_Fields)] = ',' then
      _Fields[length(_Fields)] := ' ';
end;

function TTableLog.ExtractTableName(UpdateStr: String): String;
var temp : String;
begin
   temp := Trim(AnsiReplaceText(UpdateStr,'update',''));
   Result := Trim(Copy(temp,1,pos(' ',temp)));
end;

function TTableLog.GetFields: String;
begin
Result := _Fields;
end;

function TTableLog.GetTableName: String;
begin
Result := _Table;
end;

procedure TTableLog.Iniciar;
var i : integer;
begin
QTable.Close;
QTable.Sql.Text := 'Select '+_Fields+' from '+_Table+' '+_Where;
QTable.Open;

SetLength(IFields,QTable.FieldCount);
for i := 0 to pred(QTable.FieldCount) do begin
    IFields[i]        := TSimpleField.Create;
    IFields[i].Tipo   := QTable.Fields[i].DataType;
    IFields[i].Valor  := QTable.Fields[i].AsVariant;
    IFields[i].Nome   := QTable.Fields[i].FieldName;
    IFields[i].Kind   := QTable.Fields[i].FieldKind;
end;
QTable.Close;
SetTitulos;
end;

procedure TTableLog.CompararLogar(ChavePri:Integer);
var i : integer; new, old : String;
Log : TLog;
begin
SetTitulos;
QTable.Open;
SetLength(IFields,QTable.FieldCount);
for i := 0 to pred(QTable.FieldCount) do begin
    if IFields[i].Valor <> QTable.FieldByName(IFields[i].Nome).AsVariant then begin
       Log := TLog.Create;
       if not Log.NaoLogar(IFields[i].Nome,IFields[i].Kind) then begin
            Log.FormatarValores(old,new,IFields[i].Valor,QTable.Fields[i].AsVariant,QTable.Fields[i].DataType);
       end;
    end;
end;
QTable.Close;
end;

procedure TTableLog.SetFields(Fields: string);
begin
  _Fields := Fields;
end;

procedure TTableLog.SetTableAndFields(UpdateSql: String);
begin
 _Table  := ExtractTableName(UpdateSql);
 _Fields := ExtractFieldsName(UpdateSql);
end;

procedure TTableLog.SetTableName(TableName: String);
begin
  _Table := TableName;
end;

procedure TTableLog.SetWhere(WhereStr: String);
begin
 _Where := WhereStr;
end;

procedure TTableLog.SetTitulos;
 var i : integer;
begin
 for i := 0 to pred(length(IFields)) do
     IFields[i].Titulo := IFields[i].Titulo;
end;

procedure TTableLog.ClearArray;
var i : integer;
begin
  for i := 0 to pred(length(IFields)) do
     if Assigned(IFields[i]) then
        IFields[i].Free;
end;

destructor TTableLog.Destroy;
begin
   ClearArray;
   QTable.Free;
   inherited;
end;

constructor TTableLog.Create;
begin
   inherited Create;
end;

function CoalesceField(Field:TField):Variant;
begin
   Result := Field.Value;
   if Field.Value = Null then begin
      if Field is TNumericField then
         Result := 0
      else if Field is TStringField then
         Result := EmptyStr;
   end
end;

end.
