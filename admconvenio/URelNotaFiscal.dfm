inherited frmRelNotaFiscal: TfrmRelNotaFiscal
  Left = 231
  Top = 70
  Caption = 'Nota Fiscal'
  ClientHeight = 746
  ClientWidth = 1151
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1151
    inherited panTitulo: TPanel
      Caption = '  Nota Fiscal'
    end
  end
  object pnlPanAbe: TPanel [1]
    Left = 0
    Top = 23
    Width = 1151
    Height = 98
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object bvl1: TBevel
      Left = 968
      Top = 1
      Width = 2
      Height = 80
    end
    object bvl3: TBevel
      Left = 1
      Top = 81
      Width = 968
      Height = 2
    end
    object btnPesquisar: TBitBtn
      Left = 848
      Top = 25
      Width = 105
      Height = 47
      Caption = 'Gerar Nota Fiscal'
      TabOrder = 0
      OnClick = btnPesquisarClick
    end
    object grp4: TGroupBox
      Left = 7
      Top = 6
      Width = 146
      Height = 68
      Caption = 'Origem do Pagamento'
      TabOrder = 1
      object cbbPagamento: TJvDBComboBox
        Left = 9
        Top = 32
        Width = 128
        Height = 21
        Hint = 'Origem do Pagament'
        DropDownCount = 4
        Items.Strings = (
          'BELLA'
          'CDC'
          'PRATICARD'
          'RPC')
        TabOrder = 0
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
        ListSettings.OutfilteredValueFont.Style = []
      end
    end
    object grpDatas: TGroupBox
      Left = 162
      Top = 6
      Width = 243
      Height = 67
      Caption = 'Per'#237'odo'
      TabOrder = 2
      object lblDe: TLabel
        Left = 2
        Top = 36
        Width = 14
        Height = 13
        Caption = 'De'
      end
      object lblA: TLabel
        Left = 125
        Top = 36
        Width = 7
        Height = 13
        Caption = #192
      end
      object dataIni: TJvDateEdit
        Left = 24
        Top = 32
        Width = 97
        Height = 21
        Hint = 'Data Inicial'
        ShowNullDate = False
        TabOrder = 0
      end
      object dataFim: TJvDateEdit
        Left = 136
        Top = 32
        Width = 97
        Height = 21
        Hint = 'Data Final'
        ShowNullDate = False
        TabOrder = 1
      end
    end
    object grp1: TGroupBox
      Left = 410
      Top = 6
      Width = 243
      Height = 67
      Caption = 'Segmento'
      TabOrder = 3
      object cmbSegmento: TJvDBLookupCombo
        Left = 10
        Top = 30
        Width = 223
        Height = 21
        DropDownWidth = 350
        DisplayEmpty = 'Todos Segmentos'
        EmptyValue = '0'
        FieldsDelimiter = #0
        LookupField = 'SEG_ID'
        LookupDisplay = 'DESCRICAO'
        LookupSource = dsSeg
        TabOrder = 0
      end
    end
    object grp2: TGroupBox
      Left = 658
      Top = 6
      Width = 183
      Height = 67
      Caption = 'Taxa'
      TabOrder = 4
      object dbTaxa: TMaskEdit
        Left = 8
        Top = 30
        Width = 152
        Height = 21
        TabOrder = 0
        OnKeyPress = dbTaxaKeyPress
      end
    end
  end
  object grdNota: TJvDBGrid [2]
    Left = 0
    Top = 121
    Width = 1151
    Height = 625
    Align = alClient
    DataSource = dsNota
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'CRED_ID'
        Title.Caption = 'Cred. ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Nome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CGC'
        Title.Caption = 'CNPJ'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR_PAGO'
        Title.Caption = 'Valor Pago'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Caption = 'Total de Vendas'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IMPOSTO'
        Title.Caption = 'Taxa'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF'
        Width = 100
        Visible = True
      end>
  end
  inherited PopupBut: TPopupMenu
    Left = 1084
    Top = 40
  end
  object qSeg: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from segmentos '
      'where apagado <> '#39'S'#39' order by descricao')
    Left = 1000
    Top = 39
    object qSegSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object qSegDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 58
    end
    object qSegAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object qSegDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object qSegOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qSegDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object qSegOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object qSegDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
  end
  object dsSeg: TDataSource
    DataSet = qSeg
    Left = 1048
    Top = 39
  end
  object qNotaFsical: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT PAG.CRED_ID, CRED.NOME, CRED.CGC, ROUND(SUM(PAG.VALOR_PAG' +
        'O),2) AS VALOR_PAGO, ROUND(SUM(PAG.VALOR_PAGO) / 0.995,2) AS TOT' +
        'AL,'
      
        'ROUND(ROUND(SUM(PAG.VALOR_PAGO) / 0.995,2) - ROUND(SUM(PAG.VALOR' +
        '_PAGO),2),2) AS IMPOSTO, '#39#39' AS NF'
      'FROM PAGAMENTO_CRED PAG'
      'INNER JOIN CREDENCIADOS CRED ON CRED.CRED_ID = PAG.CRED_ID'
      'WHERE PAG.DATA_COMPENSACAO BETWEEN '#39'01/03/2017'#39' AND '#39'02/03/2017'#39
      'AND PAG.IDENTIFICACAO_BANCARIA = 1'
      'GROUP BY PAG.CRED_ID, CRED.NOME, CRED.CGC')
    Left = 1056
    Top = 87
    object qNotaFsicalCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qNotaFsicalNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object qNotaFsicalCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object qNotaFsicalVALOR_PAGO: TFloatField
      FieldName = 'VALOR_PAGO'
      ReadOnly = True
    end
    object qNotaFsicalTOTAL: TFloatField
      FieldName = 'TOTAL'
      ReadOnly = True
    end
    object qNotaFsicalIMPOSTO: TFloatField
      FieldName = 'IMPOSTO'
      ReadOnly = True
    end
    object qNotaFsicalNF: TStringField
      FieldName = 'NF'
      ReadOnly = True
      Size = 1
    end
  end
  object dsNota: TDataSource
    DataSet = qNotaFsical
    Left = 1088
    Top = 87
  end
end
