unit UManutAutors;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, {JvLookup,} Mask, ToolEdit,
  Grids, DBGrids, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  {JvDBCtrl,} ZSqlUpdate, uclasslog, JvExDBGrids, JvDBGrid, JvExControls,
  JvDBLookup, JvExMask, JvToolEdit, ADODB, JvMemoryDataset;

type
  TFManutAutors = class(TF1)
    Panel1: TPanel;
    Panel2: TPanel;
    ButMarcaTodos: TButton;
    ButDesmTodos: TButton;
    ButMarcDesm: TButton;
    DBFornecedor: TJvDBLookupCombo;
    EdCred: TEdit;
    Label1: TLabel;
    ChkFornec: TCheckBox;
    Bevel1: TBevel;
    RGAutors: TRadioGroup;
    RGdatas: TRadioGroup;
    GroupBox1: TGroupBox;
    //Dataini: TDateEdit;
    //Datafin: TDateEdit;
    LabDatas: TLabel;
    Bevel3: TBevel;
    DBGrid1: TJvDBGrid;
    DSQuery1: TDataSource;
    DSEmpresa: TDataSource;
    dscrCredenciado: TDataSource;
    ButAlterar: TButton;
    Panel3: TPanel;
    Label3: TLabel;
    LabTotCred: TLabel;
    Label5: TLabel;
    LabTotal: TLabel;
    Label7: TLabel;
    LabTotDeb: TLabel;
    Label4: TLabel;
    Bevel4: TBevel;
    Label2: TLabel;
    DBEmpresa: TJvDBLookupCombo;
    ChkEmp: TCheckBox;
    EdEmp: TEdit;
    BitBtn1: TBitBtn;
    Label6: TLabel;
    cbNListCanc: TCheckBox;
    datafin: TJvDateEdit;
    dataini: TJvDateEdit;
    Query1: TADOQuery;
    Query1autorizacao_id: TIntegerField;
    Query1trans_id: TIntegerField;
    Query1debito: TBCDField;
    Query1credito: TBCDField;
    Query1data: TDateTimeField;
    Query1digito: TWordField;
    Query1data_fecha_emp: TDateTimeField;
    Query1historico: TStringField;
    Query1operador: TStringField;
    Query1hora: TStringField;
    Query1obs1: TStringField;
    Query1liberado: TStringField;
    Query1chapa: TFloatField;
    Query1entreg_nf: TStringField;
    Query1titular: TStringField;
    Query1conv_id: TIntegerField;
    Query1nf: TIntegerField;
    Query1receita: TStringField;
    Query1data_alteracao: TDateTimeField;
    Query1empres_id: TAutoIncField;
    Query1nome: TStringField;
    Query1Marcado: TStringField;
    Query1cred_id: TIntegerField;
    Query1fornecedor: TStringField;
    qConv: TADOQuery;
    Credenciado: TADOQuery;
    Empresa: TADOQuery;
    Empresaempres_id: TAutoIncField;
    Empresanome: TStringField;
    CredenciadoCred_id: TIntegerField;
    Credenciadonome: TStringField;
    Credenciadocomissao: TBCDField;
    Credenciadofantasia: TStringField;
    MDQuery1: TJvMemoryData;
    DataSource1: TDataSource;
    MDQuery1autorizacao_id: TIntegerField;
    MDQuery1trans_id: TIntegerField;
    MDQuery1debito: TFloatField;
    MDQuery1credito: TFloatField;
    MDQuery1data: TDateField;
    MDQuery1digito: TIntegerField;
    MDQuery1data_fecha_emp: TDateField;
    MDQuery1historico: TStringField;
    MDQuery1operador: TStringField;
    MDQuery1obs1: TStringField;
    MDQuery1liberado: TStringField;
    MDQuery1entrega_nf: TStringField;
    MDQuery1titular: TStringField;
    MDQuery1conv_id: TIntegerField;
    MDQuery1nf: TIntegerField;
    MDQuery1receita: TStringField;
    MDQuery1data_alteracao: TDateField;
    MDQuery1empres_id: TIntegerField;
    MDQuery1nome: TStringField;
    MDQuery1marcado: TBooleanField;
    MDQuery1cred_id: TIntegerField;
    MDQuery1fornecedor: TStringField;
    MDQuery1hora: TStringField;
    MDQuery1chapa: TFloatField;
    qConvconsumo_mes_1: TBCDField;
    qConvconsumo_mes_2: TBCDField;
    qConvconsumo_mes_3: TBCDField;
    qConvconsumo_mes_4: TBCDField;
    qConvlimite_mes: TBCDField;
    qConvsaldo_acumulado: TBCDField;
    QBuscaSeg_id: TADOQuery;
    QBuscaSeg_idseg_id: TIntegerField;
    Query1TIPO_CREDITO: TIntegerField;
    Query1BAND_ID: TIntegerField;
    Query1DATA_VENC_EMP: TDateTimeField;
    MDQuery1data_venc_emp: TDateField;
    procedure ChkFornecClick(Sender: TObject);
    procedure ChkEmpClick(Sender: TObject);
    procedure RGdatasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdCredChange(Sender: TObject);
    procedure EdEmpChange(Sender: TObject);
    procedure EdCredKeyPress(Sender: TObject; var Key: Char);
    procedure EdEmpKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure ButMarcDesmClick(Sender: TObject);
    procedure ButMarcaTodosClick(Sender: TObject);
    procedure ButDesmTodosClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ButAlterarClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBEmpresaChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Autorizacao_Sel;


  private
    Query1_Sel : String;
    LogQuery1 : TLog;
    procedure Somar;


    { Private declarations }
  public
    { Public declarations }



  end;

implementation

uses DM, UTelaAltLinear, UMenu, UTipos, cartao_util, FOcorrencia;

{$R *.dfm}

procedure TFManutAutors.ChkFornecClick(Sender: TObject);
begin
  inherited;
  EdCred.Enabled := not ChkFornec.Checked;
  DBFornecedor.Enabled := not ChkFornec.Checked;
  if EdCred.Enabled then
     EdCred.SetFocus;
end;

procedure TFManutAutors.ChkEmpClick(Sender: TObject);
begin
  inherited;
  EdEmp.Enabled := not ChkEmp.Checked;
  DBEmpresa.Enabled := not ChkEmp.Checked;
  if EdEmp.Enabled then
     EdEmp.SetFocus;
end;

procedure TFManutAutors.RGdatasClick(Sender: TObject);
begin
  inherited;
  if RGdatas.ItemIndex = 0 then begin
     LabDatas.Caption := 'Data Fechamento';
     Datafin.Visible := False;
  end
  else if RGdatas.ItemIndex = 1 then begin
     LabDatas.Caption := 'Periodo De                         A';
     Datafin.Visible := True;
  end;
end;

procedure TFManutAutors.FormCreate(Sender: TObject);
begin
  inherited;
  DMConexao.Config.Open;
  RGAutors.Visible := DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'S';
  DMConexao.Config.Close;
  LogQuery1 := TLog.Create;
  LogQuery1.LogarQuery(Query1,'CONTACORRENTE','Autoriza��o: ','Conta Corrente',Operador.Nome,'autorizacao_id');
  dataini.date := date;
  datafin.date := date;
  Empresa.Open;
  Credenciado.Open;
end;

procedure TFManutAutors.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFManutAutors.EdCredChange(Sender: TObject);
begin
  inherited;
  if Trim(EdCred.Text) <> '' then begin
     if Credenciado.Locate('cred_id',EdCred.Text,[]) then
        DBFornecedor.KeyValue := EdCred.Text
     else
     DBFornecedor.ClearValue;
  end;
end;

procedure TFManutAutors.EdEmpChange(Sender: TObject);
begin
  inherited;
  if Trim(EdEmp.Text) <> '' then begin
     if Empresa.Locate('empres_id',EdEmp.Text,[]) then
        DBEmpresa.KeyValue := EdEmp.Text
     else
     DBEmpresa.ClearValue;
  end;
end;

procedure TFManutAutors.EdCredKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TFManutAutors.EdEmpKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key); 
end;

procedure TFManutAutors.BitBtn1Click(Sender: TObject);
begin
  inherited;
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add(' select distinct contacorrente.autorizacao_id, contacorrente.trans_id, contacorrente.debito, contacorrente.credito, ');
  Query1.SQL.Add(' contacorrente.data_fecha_emp, contacorrente.data_venc_emp, contacorrente.data, contacorrente.digito, ');
  Query1.SQL.Add(' contacorrente.historico, contacorrente.operador, contacorrente.hora, ');
  Query1.SQL.Add(' contacorrente.entreg_nf, conveniados.titular, conveniados.conv_id, ');
  Query1.SQL.Add(' conveniados.obs1, conveniados.liberado, conveniados.chapa, ');
  Query1.SQL.Add(' contacorrente.nf, contacorrente.receita, contacorrente.data_alteracao, ');
  Query1.SQL.Add(' empresas.empres_id, empresas.nome, ''N'' as Marcado,');
  Query1.SQL.Add(' contacorrente.cred_id, (Select nome from credenciados where cred_id = contacorrente.cred_id) as fornecedor, empresas.TIPO_CREDITO, EMPRESAS.BAND_ID ');
  Query1.SQL.Add(' from contacorrente join conveniados on (conveniados.conv_id = contacorrente.conv_id) ');
  Query1.SQL.Add(' join empresas on (empresas.empres_id = conveniados.empres_id) join cartoes on (cartoes.conv_id = contacorrente.conv_id) ');
  Query1.SQL.Add(' where coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' and coalesce(contacorrente.fatura_id,0) = 0 ');
  if cbNListCanc.Checked then
    Query1.SQL.Add(' and coalesce(contacorrente.cancelada,''N'')=''N'' and coalesce(contacorrente.credito,0) = 0 ');
  if not ChkFornec.Checked then
     Query1.SQL.Add(' and contacorrente.cred_id = '+DBFornecedor.KeyValue);
  if not ChkEmp.Checked then
     Query1.SQL.Add(' and conveniados.empres_id = '+DBEmpresa.KeyValue);
  if RGAutors.Visible then begin
     case RGAutors.ItemIndex of
         1 : Query1.SQL.Add(' and coalesce(contacorrente.entreg_nf,''N'') = ''S'' ');
         2 : Query1.SQL.Add(' and coalesce(contacorrente.entreg_nf,''N'') = ''N'' ');
     end;
  end;
  case RGdatas.ItemIndex of
        0 : Query1.SQL.Add(' and contacorrente.data_fecha_emp = '+formatdateIB(Dataini.Date));
        1 : Query1.SQL.Add(' and contacorrente.data between '+formatdateIB(Dataini.Date)+' and '+formatdateIB(Datafin.Date));
  end;
  Screen.Cursor := crHourGlass;
  Query1.Open;
  Query1.First;
  MDQuery1.Open;
  MDQuery1.EmptyTable;
  MDQuery1.DisableControls;
  while not Query1.Eof do begin
    MDQuery1.Append;
    MDQuery1autorizacao_id.Value    := Query1autorizacao_id.AsInteger;
    MDQuery1trans_id.Value          := Query1trans_id.AsInteger;
    MDQuery1debito.Value            := Query1debito.Value;
    MDQuery1credito.Value           := Query1credito.Value;
    MDQuery1data.Value              := Query1data.Value;
    MDQuery1digito.Value            := Query1digito.Value;
    MDQuery1data_fecha_emp.Value    := Query1data_fecha_emp.Value;
    MDQuery1data_venc_emp.Value     := Query1data_venc_emp.Value;
    MDQuery1historico.Value         := Query1historico.Value;
    MDQuery1operador.Value          := Query1operador.Value;
    MDQuery1hora.Value              := Query1hora.Value;
    MDQuery1obs1.Value              := Query1obs1.Value;
    MDQuery1liberado.Value          := Query1liberado.Value;
    MDQuery1chapa.Value             := Query1chapa.Value;
    MDQuery1entrega_nf.Value        := Query1entreg_nf.Value;
    MDQuery1titular.Value           := Query1titular.Value;
    MDQuery1conv_id.Value           := Query1conv_id.Value;
    MDQuery1nf.Value                := Query1nf.Value;
    MDQuery1receita.Value           := Query1receita.Value;
    MDQuery1data_alteracao.Value    := Query1data_alteracao.Value;
    MDQuery1empres_id.Value         := Query1empres_id.Value;
    MDQuery1nome.Value              := Query1nome.Value;
    MDQuery1marcado.Value           := False;
    MDQuery1cred_id.Value           := Query1cred_id.Value;
    MDQuery1fornecedor.Value        := Query1fornecedor.Value;
    MDQuery1obs1.Value              := Query1obs1.Value;
    MDQuery1.Post;
    Query1.Next;
  end;
  //Query1.Close;
  MDQuery1.First;
  MDQuery1.EnableControls;
  Query1_sel := EmptyStr;
  Screen.Cursor := crDefault;
  if MDQuery1.IsEmpty then
     MsgInf('Nenhuma autoriza��o encontrada!')
  else
     DBGrid1.SetFocus;

   
end;



procedure TFManutAutors.Somar;
var m : TBookmark; tot, deb, cre : Currency;
begin
   MDQuery1.DisableControls;
   m := MDQuery1.GetBookmark;
   MDQuery1.First;
   deb := 0; cre := 0;
   while not MDQuery1.Eof do begin
      if MDQuery1MARCADO.AsBoolean = true then begin
         deb := deb + MDQuery1DEBITO.AsCurrency;
         cre := cre + MDQuery1CREDITO.AsCurrency;
      end;
      MDQuery1.Next;
   end;
   tot := deb-cre;
   MDQuery1.GotoBookmark(m);
   MDQuery1.FreeBookmark(m);
   MDQuery1.EnableControls;
   LabTotal.Caption   := FormatDinBR(tot);
   LabTotCred.Caption := FormatDinBR(cre);
   LabTotDeb.Caption  := FormatDinBR(deb);
   Application.ProcessMessages;
end;

procedure TFManutAutors.Autorizacao_Sel;
var marca : TBookmark;
begin
  Query1_Sel := EmptyStr;
  marca := MDQuery1.GetBookmark;
  MDQuery1.DisableControls;
  MDQuery1.First;
  while not MDQuery1.eof do begin
    if MDQuery1Marcado.AsBoolean then Query1_sel := Query1_sel + ','+MDQuery1CRed_id.AsString;
    MDQuery1.Next;
  end;
  MDQuery1.GotoBookmark(marca);
  MDQuery1.FreeBookmark(marca);
  if Query1_sel <> '' then Query1_sel := Copy(Query1_sel,2,Length(Query1_sel));
  MDQuery1.EnableControls;
end;

procedure TFManutAutors.ButMarcDesmClick(Sender: TObject);
begin
  if MDQuery1.IsEmpty then Exit;
  MDQuery1.Edit;
  MDQuery1marcado.AsBoolean := not MDQuery1marcado.AsBoolean;
  MDQuery1.Post;
  Autorizacao_Sel;
  Somar;
end;

procedure TFManutAutors.ButMarcaTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  if MDQuery1.IsEmpty then Exit;
  MDQuery1.DisableControls;
  marca := MDQuery1.GetBookmark;
  MDQuery1.First;
  while not MDQuery1.eof do begin
    MDQuery1.Edit;
    MDQuery1Marcado.AsBoolean := true;
    MDQuery1.Post;
    MDQuery1.Next;
  end;
  MDQuery1.GotoBookmark(marca);
  MDQuery1.FreeBookmark(marca);
  MDQuery1.EnableControls;
  Autorizacao_Sel;
  Somar;
end;

procedure TFManutAutors.ButDesmTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  if MDQuery1.IsEmpty then Exit;
  MDQuery1.DisableControls;
  marca := MDQuery1.GetBookmark;
  MDQuery1.First;
  while not MDQuery1.eof do begin
    MDQuery1.Edit;
    MDQuery1Marcado.AsBoolean := false;
    MDQuery1.Post;
    MDQuery1.Next;
  end;
  MDQuery1.GotoBookmark(marca);
  MDQuery1.FreeBookmark(marca);
  MDQuery1.EnableControls;
  Autorizacao_Sel;
  Somar;
end;

procedure TFManutAutors.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f11 : ButMarcDesm.Click;
     vk_f8  : ButMarcaTodos.Click;
     vk_f9  : ButDesmTodos.Click;
  end;    
end;

procedure TFManutAutors.DBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
    if Pos(Field.FieldName,Query1.Sort) > 0 then begin
       if Pos(' DESC',Query1.Sort) > 0 then Query1.Sort := Field.FieldName
                                                  else Query1.Sort := Field.FieldName+' DESC';
    end
    else Query1.Sort := Field.FieldName;
    except
    end;

  MDQuery1.First;
  MDQuery1.Open;
  MDQuery1.EmptyTable;
  MDQuery1.DisableControls;
  while not Query1.Eof do begin
    MDQuery1.Append;
    MDQuery1autorizacao_id.Value := Query1autorizacao_id.AsInteger;
    MDQuery1trans_id.Value       := Query1trans_id.AsInteger;
    MDQuery1debito.Value         := Query1debito.Value;
    MDQuery1credito.Value        := Query1credito.Value;
    MDQuery1data.Value           := Query1data.Value;
    MDQuery1digito.Value         := Query1digito.Value;
    MDQuery1data_fecha_emp.Value := Query1data_fecha_emp.Value;
    MDQuery1data_venc_emp.Value  := Query1data_venc_emp.Value;
    MDQuery1historico.Value      := Query1historico.Value;
    MDQuery1operador.Value       := Query1operador.Value;
    MDQuery1hora.Value           := Query1hora.Value;
    MDQuery1obs1.Value           := Query1obs1.Value;
    MDQuery1liberado.Value       := Query1liberado.Value;
    MDQuery1chapa.Value          := Query1chapa.Value;
    MDQuery1entrega_nf.Value     := Query1entreg_nf.Value;
    MDQuery1titular.Value        := Query1titular.Value;
    MDQuery1conv_id.Value        := Query1conv_id.Value;
    MDQuery1nf.Value             := Query1nf.Value;
    MDQuery1receita.Value        := Query1receita.Value;
    MDQuery1data_alteracao.Value := Query1data_alteracao.Value;
    MDQuery1empres_id.Value      := Query1empres_id.Value;
    MDQuery1nome.Value           := Query1nome.Value;
    MDQuery1marcado.Value        := False;
    MDQuery1cred_id.Value        := Query1cred_id.Value;
    MDQuery1fornecedor.Value     := Query1fornecedor.Value;
    MDQuery1obs1.Value              := Query1obs1.Value;
    MDQuery1.Post;
    Query1.Next;
  end;
  MDQuery1.First;
  MDQuery1.EnableControls;
  Query1_sel := EmptyStr;
   DBGrid1.SetFocus;
  
end;

procedure TFManutAutors.ButAlterarClick(Sender: TObject);
var i : integer; TipoCampo : TFieldType;
  data: TDateTime;
  qtdLimite, band_id, tipo_credito, sql : String;
  codLimite : Integer;
begin
  inherited;
  if DMConexao.ContaMarcados(MDQuery1) = 0 then
     MsgInf('N�o h� autoriza��es selecionadas.')
  else begin
     FTelaAltLinear := TFTelaAltLinear.Create(self);
     for i := 0 to MDQuery1.Fields.Count - 1 do begin
        //if IsEditableField(MDQuery1.Fields[i],'autorizacao_id') then begin
        if ((MDQuery1.Fields[i].FieldName = 'data_fecha_emp') or (MDQuery1.Fields[i].FieldName = 'debito') or (MDQuery1.Fields[i].FieldName = 'credito'))then begin
           FTelaAltLinear.TempCampos.Append;
           FTelaAltLinear.TempCamposCampo.AsString     := MDQuery1.Fields[i].DisplayName;
           FTelaAltLinear.TempCamposFieldName.AsString := MDQuery1.Fields[i].FieldName;
           FTelaAltLinear.TempCamposTipo.AsVariant     := Variant(MDQuery1.Fields[i].DataType);
           if IsNumericField(MDQuery1.Fields[i].DataType) and MDQuery1.Fields[i].IsNull then
              FTelaAltLinear.TempCamposValor.AsVariant    := 0
           else
              FTelaAltLinear.TempCamposValor.AsVariant    := MDQuery1.Fields[i].AsVariant;
           FTelaAltLinear.TempCamposSize.AsInteger     := MDQuery1.Fields[i].Size;
           FTelaAltLinear.TempCampos.Post;
        end;
     end;
     FTelaAltLinear.TempCampos.SortOnFields('Campo');
     FTelaAltLinear.TempCampos.First;
     FTelaAltLinear.DBCampos.KeyValue := FTelaAltLinear.TempCamposCampo.AsString;
     FTelaAltLinear.DBCamposChange(nil);
     FTelaAltLinear.ShowModal;
     if FTelaAltLinear.ModalResult = mrOk then begin
        TipoCampo := TFieldType(FTelaAltLinear.TempCamposTipo.AsVariant);
        MDQuery1.First;
        Query1.First;

        FrmOcorrencia := TFrmOcorrencia.Create(Self);
        if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
          Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
          Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
          FreeAndNil(FrmOcorrencia);
        end;


        while not MDQuery1.Eof do begin
           if MDQuery1MARCADO.AsString = 'True' then begin
              try
                 //MDQuery1.Edit;
                 Query1.Edit;
                 if IsIntegerField(TipoCampo) then
                    Query1.FieldByName(FTelaAltLinear.TempCamposFieldName.AsString).AsInteger  := FTelaAltLinear.GetValuesAsInteger
                 else if IsFloatField(TipoCampo) then
                    Query1.FieldByName(FTelaAltLinear.TempCamposFieldName.AsString).AsFloat    := FTelaAltLinear.GetValuesAsFloat
                 else if IsDateTimeField(TipoCampo) then begin
                    //if UpperCase(FTelaAltLinear.TempCamposFieldName.AsString) = 'DATA' then begin
                    //  DMConexao.ExecuteSql('UPDATE TRANSACOES SET DATAHORA = ' + QuotedStr(formatDateTime('dd.mm.yyyy hh:nn:ss',FTelaAltLinear.GetValuesAsDateTime)) + ' where trans_id = ' + MDQuery1TRANS_ID.AsString);
                    //  DMConexao.ExecuteSql('UPDATE AUTOR_TRANSACOES SET DATAHORA = ' + QuotedStr(formatDateTime('dd.mm.yyyy hh:nn:ss',FTelaAltLinear.GetValuesAsDateTime)) + ' where autor_id = ' + MDQuery1AUTORIZACAO_ID.AsString);
                    //  DMConexao.ExecuteSql('UPDATE CONTACORRENTE SET DATA = ' + QuotedStr(formatDateTime('dd.mm.yyyy',FTelaAltLinear.GetValuesAsDateTime)) + ' , DATAVENDA = ' + QuotedStr(FormatDateTime('dd.mm.yyyy hh:nn:ss',FTelaAltLinear.GetValuesAsDateTime)) + ' where autorizacao_id = ' + MDQuery1AUTORIZACAO_ID.AsString);
                    //end;
                    if UpperCase(FTelaAltLinear.TempCamposFieldName.AsString) = 'DATA_FECHA_EMP' then
                       if not (DMConexao.ValidarFechamentoAbertoConv(FTelaAltLinear.GetValuesAsDateTime,MDQuery1CONV_ID.AsInteger,MDQuery1EMPRES_ID.AsInteger)) then
                          raise Exception.Create('A Data Fechamento '+FormatDataBR(FTelaAltLinear.GetValuesAsDateTime)+' � inv�lida para a empresa ou '+sLineBreak+
                                                 'essa data de fechamento foi faturada para esta empresa ou para este conveniado.');
                    Query1.FieldByName(FTelaAltLinear.TempCamposFieldName.AsString).AsDateTime := FTelaAltLinear.GetValuesAsDateTime;
                    //ATUALIZA A DATA DE VENCIMENTO DA EMPRESA TAMBEM NA CONTACORRENTE//
                    DMConexao.AdoQry.Close;
                    DMConexao.AdoQry.SQL.Clear;
                    DMConexao.AdoQry.SQL.Add('SELECT TOP 1 DIA_FECHA.DATA_VENC FROM DIA_FECHA');
                    DMConexao.AdoQry.SQL.Add('WHERE DIA_FECHA.DATA_FECHA = '+QuotedStr(formatDateTime('dd.mm.yyyy hh:nn:ss',FTelaAltLinear.GetValuesAsDateTime))+'');
                    DMConexao.AdoQry.SQL.Add('AND DIA_FECHA.EMPRES_ID = '+ MDQuery1EMPRES_ID.AsString + 'ORDER BY 1');
                    DMConexao.AdoQry.Open;
                    Query1DATA_VENC_EMP.AsDateTime := DMConexao.AdoQry.Fields[0].Value;

                    DMConexao.ExecuteSql('Update autor_transacoes set data_venc_emp = '+QuotedStr(formatDateTime('dd.mm.yyyy hh:nn:ss',Query1DATA_VENC_EMP.AsDateTime))+', data_fecha_emp = '+QuotedStr(formatDateTime('dd.mm.yyyy hh:nn:ss',FTelaAltLinear.GetValuesAsDateTime))+' WHERE AUTOR_ID = '+ MDQuery1autorizacao_id.AsString+'');

                 end else
                    Query1.FieldByName(FTelaAltLinear.TempCamposFieldName.AsString).AsString   := FTelaAltLinear.GetValuesAsString;



                 Query1OPERADOR.AsString := Operador.Nome;
                 Query1DATA_ALTERACAO.AsDateTime := Now;
                 Query1HISTORICO.AsString :=  FTelaAltLinear.EdHistorico.Text;
                 Query1.Post;


                 DMConexao.AdoQry.Close;
                 DMConexao.AdoQry.SQL.Clear;
                 DMConexao.AdoQry.SQL.Add('SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA');
                 DMConexao.AdoQry.SQL.Add('WHERE DIA_FECHA.DATA_FECHA > CURRENT_TIMESTAMP');
                 DMConexao.AdoQry.SQL.Add('AND DIA_FECHA.EMPRES_ID = '+ MDQuery1EMPRES_ID.AsString + 'ORDER BY 1');
                 DMConexao.AdoQry.Open;
                 data := DMConexao.AdoQry.Fields[0].Value;

                 DMConexao.AdoQry.Close;
                 DMConexao.AdoQry.SQL.Clear;
                 DMConexao.AdoQry.SQL.Add('SELECT BAND_ID, TIPO_CREDITO FROM EMPRESAS');
                 DMConexao.AdoQry.SQL.Add('WHERE EMPRES_ID = '+ MDQuery1EMPRES_ID.AsString + 'ORDER BY 1');
                 DMConexao.AdoQry.Open;

                 band_id := DMConexao.AdoQry.Fields[0].Value;
                 tipo_credito := DMConexao.AdoQry.Fields[0].Value;


                 DMConexao.AdoQry.SQL.Clear;
                 DMConexao.AdoQry.SQL.Add('select coalesce(qtd_limites,1) as qtd_limites from bandeiras where band_id = '+band_id+'');
                 DMConexao.AdoQry.Open;
                 qtdLimite := DMConexao.AdoQry.Fields[0].AsString;

                 qConv.Close;
                 qConv.Parameters.ParamByName('CONV_ID').Value := MDQuery1CONV_ID.AsInteger;
                 qConv.Open;

                 try
                    DMConexao.AdoCon.BeginTrans;
                    DMConexao.ExecuteSql('EXEC CONSUMO_MES_CONV ' + MDQuery1EMPRES_ID.AsString + ',' + band_id + ',' + tipo_credito + ',''' + DateToStr(data) + ''',' + qtdLimite + ',' + MDQuery1CONV_ID.AsString);
                    DMConexao.AdoCon.CommitTrans;

                    DMConexao.AdoQry.SQL.Clear;
                    DMConexao.AdoQry.SQL.Add('select consumo_mes_1, consumo_mes_2, consumo_mes_3, consumo_mes_4 from conveniados where conv_id = '+ MDQuery1CONV_ID.AsString);
                    DMConexao.AdoQry.Open;

                    if band_id = '999' then
                      DMConexao.GravaLog('FCadConv','CONSUMO_MES_1',qConvCONSUMO_MES_1.AsString,DMConexao.AdoQry.Fields[0].AsString,Operador.Nome,'Altera��o',
                        MDQuery1CONV_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo)
                    else
                      begin
                       sql := 'select seg_id from CREDENCIADOS,contacorrente'+
                          ' where contacorrente.CRED_ID = CREDENCIADOS.CRED_ID'+
                          ' and credenciados.cred_id = '+MDQuery1CRED_ID.AsString;
                          QBuscaSeg_id.SQL.Clear;
                          QBuscaSeg_id.SQL.Add(sql);
                          QBuscaSeg_id.Open;
                          DMConexao.Q.SQL.Clear;
                          DMConexao.Q.SQL.Add('select coalesce(cod_limite,1) as cod_limite from bandeiras_segmentos where band_id = '+band_id+' and seg_id = '+ QBuscaSeg_idseg_id.AsString+'');
                          DMConexao.Q.Open;
                          codLimite := DMConexao.Q.Fields[0].AsInteger;

                          if codLimite = 1 then
                            DMConexao.GravaLog('FCadConv','CONSUMO_MES_1',qConvCONSUMO_MES_1.AsString,DMConexao.AdoQry.Fields[0].AsString,Operador.Nome,'Altera��o',
                              MDQuery1CONV_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo)
                          else if codLimite = 2 then
                            DMConexao.GravaLog('FCadConv','CONSUMO_MES_2',qConvCONSUMO_MES_2.AsString,DMConexao.AdoQry.Fields[1].AsString,Operador.Nome,'Altera��o',
                              MDQuery1CONV_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo)
                          else if codLimite = 3 then
                            DMConexao.GravaLog('FCadConv','CONSUMO_MES_3',qConvCONSUMO_MES_3.AsString,DMConexao.AdoQry.Fields[2].AsString,Operador.Nome,'Altera��o',
                              MDQuery1CONV_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo)
                          else if codLimite = 4 then
                            DMConexao.GravaLog('FCadConv','CONSUMO_MES_4',qConvCONSUMO_MES_4.AsString,DMConexao.AdoQry.Fields[3].AsString,Operador.Nome,'Altera��o',
                            MDQuery1CONV_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo);

                       end;

                  except
                      on e:Exception do
                      begin
                        DMConexao.AdoCon.RollbackTrans;
                      end;
                  end;

              except
                 on e : Exception do begin
                    MDQuery1.Cancel;
                    MsgErro('Ocorreu um problema, erro: '+e.Message+sLineBreak+'A autoriza��o n� '+MDQuery1AUTORIZACAO_ID.AsString+MDQuery1DIGITO.DisplayText+' n�o foi alterada.');
                    if not MsgSimNao('Deseja continuar com as altera��es?') then
                       Break;
                 end;
              end;
           end;
           MDQuery1.Next;
           Query1.Next;
        end;

        Screen.Cursor := crHourGlass;
        Query1.Requery;
        Query1.First;
        MDQuery1.Open;
        MDQuery1.EmptyTable;
        MDQuery1.DisableControls;
        while not Query1.Eof do begin
          MDQuery1.Append;
          MDQuery1autorizacao_id.Value    := Query1autorizacao_id.AsInteger;
          MDQuery1trans_id.Value          := Query1trans_id.AsInteger;
          MDQuery1debito.Value            := Query1debito.Value;
          MDQuery1credito.Value           := Query1credito.Value;
          MDQuery1data.Value              := Query1data.Value;
          MDQuery1digito.Value            := Query1digito.Value;
          MDQuery1data_fecha_emp.Value    := Query1data_fecha_emp.Value;
          MDQuery1data_venc_emp.Value     := Query1data_venc_emp.Value;
          MDQuery1historico.Value         := Query1historico.Value;
          MDQuery1operador.Value          := Query1operador.Value;
          MDQuery1hora.Value              := Query1hora.Value;
          MDQuery1obs1.Value              := Query1obs1.Value;
          MDQuery1liberado.Value          := Query1liberado.Value;
          MDQuery1chapa.Value             := Query1chapa.Value;
          MDQuery1entrega_nf.Value        := Query1entreg_nf.Value;
          MDQuery1titular.Value           := Query1titular.Value;
          MDQuery1conv_id.Value           := Query1conv_id.Value;
          MDQuery1nf.Value                := Query1nf.Value;
          MDQuery1receita.Value           := Query1receita.Value;
          MDQuery1data_alteracao.Value    := Query1data_alteracao.Value;
          MDQuery1empres_id.Value         := Query1empres_id.Value;
          MDQuery1nome.Value              := Query1nome.Value;
          MDQuery1marcado.Value           := False;
          MDQuery1cred_id.Value           := Query1cred_id.Value;
          MDQuery1fornecedor.Value        := Query1fornecedor.Value;
          MDQuery1obs1.Value              := Query1obs1.Value;
          MDQuery1.Post;
          Query1.Next;
        end;
        MDQuery1.First;
        MDQuery1.EnableControls;
        Query1_sel := EmptyStr;
        Screen.Cursor := crDefault;

        Somar;

     end;
     FTelaAltLinear.Free;
  end;
end;

procedure TFManutAutors.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  try
    GridScroll(Sender,Key,Shift);
    if key = vk_return then ButMarcDesm.Click;
  except
  end;
end;

procedure TFManutAutors.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Empresa.Close;
  Credenciado.Close;
  LogQuery1.Free;
end;

procedure TFManutAutors.DBEmpresaChange(Sender: TObject);
begin
  inherited;
  if EdEmp.Text <> DBEmpresa.KeyValue then
     EdEmp.Text := string(DBEmpresa.KeyValue); 
end;

procedure TFManutAutors.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm.Click;
end;

end.
