inherited FLancamentoDebitoMesAnterior: TFLancamentoDebitoMesAnterior
  Left = 321
  Top = 139
  Caption = 'Lan'#231'amento de D'#233'bitos do M'#234's Anterior'
  ClientHeight = 344
  ClientWidth = 711
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 711
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 23
    Width = 711
    Height = 242
    Align = alTop
    Caption = 'Filtro de Dados'
    TabOrder = 1
    DesignSize = (
      711
      242)
    object Label2: TLabel
      Left = 113
      Top = 72
      Width = 176
      Height = 13
      Caption = 'Selecione o arquivo para importa'#231#227'o:'
    end
    object Label5: TLabel
      Left = -641
      Top = 25
      Width = 22
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Cod.'
    end
    object lbl4: TLabel
      Left = 112
      Top = 24
      Width = 103
      Height = 13
      Caption = 'Selecione a Empresa:'
    end
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 58
      Height = 13
      Caption = 'Empresa ID:'
    end
    object lbl1: TLabel
      Left = 16
      Top = 72
      Width = 23
      Height = 13
      Caption = 'M'#234's:'
    end
    object Label3: TLabel
      Left = 16
      Top = 128
      Width = 88
      Height = 13
      Caption = 'Data Fechamento:'
      Visible = False
    end
    object Label4: TLabel
      Left = 176
      Top = 128
      Width = 85
      Height = 13
      Caption = 'Data Vencimento:'
      Visible = False
    end
    object FilenameEdit1: TJvFilenameEdit
      Left = 112
      Top = 92
      Width = 289
      Height = 21
      Filter = 'Planilha do Excel (*.xls;*.xlsx)|*.xls;*.xlsx'
      InitialDir = 'C:\'
      TabOrder = 2
      Text = 'C:\AdmCartaoBella\'
    end
    object Button1: TButton
      Left = 268
      Top = 208
      Width = 131
      Height = 25
      Caption = '&Executar Importa'#231#227'o'
      TabOrder = 3
      OnClick = Button1Click
    end
    object EdEmp_ID: TEdit
      Left = 16
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 0
    end
    object dblkpEmpresas: TJvDBLookupCombo
      Left = 112
      Top = 40
      Width = 289
      Height = 21
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'EMPRES_ID;NOME'
      LookupDisplayIndex = 1
      LookupSource = dsEmpresas
      TabOrder = 1
      OnEnter = jvdblkpcmb1Enter
    end
    object cbbMes: TComboBox
      Left = 16
      Top = 92
      Width = 81
      Height = 21
      ItemHeight = 13
      TabOrder = 4
      Text = 'Selecione'
      Items.Strings = (
        'JANEIRO'
        'FEVEREIRO'
        'MARCO'
        'ABRIL'
        'MAIO'
        'JUNHO'
        'JULHO'
        'AGOSTO'
        'SETEMBRO'
        'OUTUBRO'
        'NOVEMBRO'
        'DEZEMBRO')
    end
    object JvDBLCData: TJvDBLookupCombo
      Left = 16
      Top = 144
      Width = 145
      Height = 21
      LookupField = 'DATA_FECHA'
      LookupSource = DsData
      TabOrder = 5
      Visible = False
      OnEnter = JvDBLCDataEnter
      OnExit = JvDBLCDataExit
    end
    object JvDBLookupCombo1: TJvDBLookupCombo
      Left = 176
      Top = 144
      Width = 145
      Height = 21
      Enabled = False
      LookupField = 'DATA_VENC'
      LookupDisplay = 'DATA_VENC'
      LookupDisplayIndex = 1
      LookupSource = DsDataVenc
      TabOrder = 6
      Visible = False
    end
    object CBUnificada: TCheckBox
      Left = 296
      Top = 120
      Width = 105
      Height = 17
      Caption = 'Planilha unificada'
      TabOrder = 7
      Visible = False
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 265
    Width = 711
    Height = 79
    Align = alClient
    TabOrder = 2
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 62
      Width = 709
      Height = 16
      Align = alBottom
      TabOrder = 0
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 300
  end
  object dsEmpresas: TDataSource
    DataSet = qEmpresas
    Left = 550
    Top = 57
  end
  object qEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select empres_id, CONCAT(empres_id,'#39' - '#39',nome) nome from empresa' +
        's'
      'where LIBERADA = '#39'S'#39' and APAGADO = '#39'N'#39)
    Left = 492
    Top = 57
    object qEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object Table1: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Teste de Convers' +
      'ao.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    TableDirect = True
    TableName = 'Conv$'
    Left = 440
    Top = 56
  end
  object DsData: TDataSource
    DataSet = qData
    Left = 24
    Top = 199
  end
  object qData: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Value = Null
      end>
    SQL.Strings = (
      'with CTE_R as('
      
        'SELECT DATA_FECHA,DATA_VENC, ROW_NUMBER() OVER(ORDER BY DATA_FEC' +
        'HA) as RowNum'
      'FROM DIA_FECHA'
      'WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = :empres_id)'
      'select * from CTE_R')
    Left = 72
    Top = 199
  end
  object DsDataVenc: TDataSource
    DataSet = qDataVenc
    Left = 168
    Top = 199
  end
  object qDataVenc: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'data_fecha'
        Attributes = [paSigned]
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'with CTE_R as('
      
        #9'SELECT DATA_FECHA,DATA_VENC, ROW_NUMBER() OVER(ORDER BY DATA_FE' +
        'CHA) as RowNum'
      #9'FROM DIA_FECHA'
      '    WHERE DATA_FECHA > GETDATE()- 30 AND EMPRES_ID = :empres_id'
      '    )'
      '    select DATA_VENC from CTE_R where DATA_FECHA = :data_fecha'
      '')
    Left = 216
    Top = 199
  end
end
