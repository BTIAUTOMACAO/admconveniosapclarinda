unit UAltOperEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ToolEdit, Mask, CurrEdit, {JvLookup,}
  RxMemDS, JvExControls, JvDBLookup, JvExMask, JvToolEdit, JvExStdCtrls,
  JvEdit, JvValidateEdit, cartao_util, ADODB;

type
  TFTelaAltOperador = class(TForm)
    cbOperador: TPanel;
    Bevel1: TBevel;
    ButExec: TButton;
    Label1: TLabel;
    QOperador: TADOQuery;
    DSOperador: TDataSource;
    Operador_old: TEdit;
    Label2: TLabel;
    QCadastro: TADOQuery;
    DSCadastro: TDataSource;
    cbOper: TJvDBLookupCombo;
    QOperadornome: TStringField;
    QOperadorusuario_id: TIntegerField;
    QCadastroNOME: TStringField;
    QCadastroUSUARIO_ID: TIntegerField;
    procedure Button2Click(Sender: TObject);
    procedure valor1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdHistoricoKeyPress(Sender: TObject; var Key: Char);
    procedure ButExecClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Nvalue : Variant;
    { Private declarations }
  public
    { Public declarations }
    dados : TDataSet;

  end;

var
  FTelaAltOperador: TFTelaAltOperador;
  
implementation

uses UExpression, DateUtils, DM, UCadOper;

{$R *.dfm}

procedure TFTelaAltOperador.Button2Click(Sender: TObject);
begin
 QOperador.Close;
 ModalResult := mrCancel;
 //DMConexao.AdoCon.RollbackTrans;

end;

procedure TFTelaAltOperador.valor1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltOperador.ComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltOperador.DBCamposKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//if not DBCampos.IsDropDown then if key = vk_return then perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFTelaAltOperador.EdHistoricoKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key = #13 then
      ButExec.SetFocus;
end;

procedure TFTelaAltOperador.ButExecClick(Sender: TObject);
begin

  DMConexao.ExecuteSql('UPDATE empresas SET responsavel_fechamento = (SELECT NOME FROM USUARIOS WHERE USUARIO_ID = '+IntToStr(cbOper.KeyValue)+') where responsavel_fechamento like ''%'+Operador_old.Text+'%''');
  QOperador.Close;
  MsgInf('Transferência de resposabilidade das empresas entre os operadores realizada com sucesso.');
  ModalResult := mrOk;
end;

procedure TFTelaAltOperador.FormShow(Sender: TObject);
begin
  QCadastro.Open;
  QOperador.Open;
  cbOper.keyvalue := 0; 
end;

end.
