unit UCadConv1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCadConv, ActnList, DB, ADODB, ExtDlgs, Menus, ExtCtrls,
  StdCtrls, JvExMask, JvToolEdit, DBCtrls, JvExControls, JvDBLookup,
  ToolEdit, RXDBCtrl, Mask, ComCtrls, Grids, DBGrids, JvExDBGrids,
  JvDBGrid, Buttons;

type
  TFCadConv1 = class(TFCadConv)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadConv1: TFCadConv1;

implementation

{$R *.dfm}

end.
