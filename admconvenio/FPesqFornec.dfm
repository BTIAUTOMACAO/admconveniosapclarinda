object F_PesqFornec: TF_PesqFornec
  Left = 235
  Top = 100
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Pesquisa de Estabelecimentos'
  ClientHeight = 370
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object panel: TPanel
    Left = 0
    Top = 0
    Width = 387
    Height = 57
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 188
      Height = 13
      Hint = 'Informe a descri'#231#227'o do fornecedor e tecle enter.'
      Caption = 'Informe a descri'#231#227'o do estabelecimento'
      ParentShowHint = False
      ShowHint = True
    end
    object Edit1: TEdit
      Left = 8
      Top = 24
      Width = 365
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnKeyDown = Edit1KeyDown
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 57
    Width = 387
    Height = 313
    Align = alClient
    DataSource = dsrcPesqFornec
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'CRED_ID'
        Title.Caption = 'C'#243'digo'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Title.Caption = 'Nome'
        Width = 304
        Visible = True
      end>
  end
  object dsrcPesqFornec: TDataSource
    Left = 104
    Top = 176
  end
  object FDataSetConsulta: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 104
    Top = 144
  end
end
