object FAnexarBoleto: TFAnexarBoleto
  Left = 715
  Top = 233
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Anexar o Boleto da Empresa'
  ClientHeight = 258
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 242
    Width = 395
    Height = 16
    Align = alBottom
    TabOrder = 0
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 242
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 1
    object lbl3: TLabel
      Left = 17
      Top = 56
      Width = 171
      Height = 13
      Caption = 'Selecione o arquivo a ser carregado'
    end
    object lbl4: TLabel
      Left = 16
      Top = 8
      Width = 100
      Height = 13
      Caption = 'Selecione a Empresa'
    end
    object btnButAjuda: TSpeedButton
      Left = 3
      Top = 207
      Width = 32
      Height = 22
      Hint = 'Ajuda'
      Flat = True
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
        5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
        DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
        CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
        E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
        A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
        2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
        D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
        3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
        F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
        3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
        FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
        3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
        FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
        3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
        FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
        5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
        FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
        FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
        FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
        FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
        E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
        BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
        C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
        7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
        D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
        E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
        FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
        FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
        D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = btnButAjudaClick
    end
    object lbl5: TLabel
      Left = 38
      Top = 213
      Width = 287
      Height = 13
      Caption = 'Aten'#231#227'o leia a ajuda antes de efetuar a opera'#231#227'o.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bvl1: TBevel
      Left = 0
      Top = 164
      Width = 379
      Height = 2
    end
    object lbl1: TLabel
      Left = 17
      Top = 104
      Width = 100
      Height = 13
      Caption = 'Data do Fechamento'
    end
    object lbl2: TLabel
      Left = 192
      Top = 104
      Width = 97
      Height = 13
      Caption = 'Data de Vencimento'
    end
    object btn1: TButton
      Left = 124
      Top = 181
      Width = 131
      Height = 25
      Caption = '&Carregar Boleto'
      TabOrder = 4
      OnClick = btn1Click
    end
    object redt1: TRichEdit
      Left = 4
      Top = 112
      Width = 53
      Height = 25
      Lines.Strings = (
        'Importante:'
        '    Para realizar o carregamento de um boleto, '#233' necessario'
        '    que o mesmo se encontre no formato pdf, que '#233
        '    o formato aguardado pelo sistema.'
        ''
        
          'Obs.: Caso o boleto anexado n'#227'o seja referente ao '#250'ltimo fechame' +
          'nto da empresa,'
        
          '    tome o devido cuidado de selecionar a data correta no campo ' +
          '"DATA DE FECHAMENTO".')
      TabOrder = 5
      Visible = False
      WordWrap = False
    end
    object dblkpEmpresas: TJvDBLookupCombo
      Left = 16
      Top = 24
      Width = 358
      Height = 21
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'EMPRES_ID;NOME'
      LookupDisplayIndex = 1
      LookupSource = DSEmpresas
      TabOrder = 0
      OnChange = dblkpEmpresasChange
      OnKeyPress = dblkpEmpresasKeyPress
    end
    object FilenameEdit1: TJvFilenameEdit
      Left = 18
      Top = 76
      Width = 353
      Height = 21
      Filter = 'Arquivo PDF(*.pdf)|*.pdf'
      InitialDir = 'C:\'
      TabOrder = 1
      Text = 'C:\'
      OnKeyPress = dblkpEmpresasKeyPress
    end
    object DBDateEditDATA_VENCIMENTO: TDBDateEdit
      Left = 192
      Top = 120
      Width = 121
      Height = 21
      DataField = 'DATA_VENCIMENTO'
      DataSource = dsFaturaVencimento
      NumGlyphs = 2
      TabOrder = 3
    end
    object dblkpFechamento: TJvDBLookupCombo
      Left = 16
      Top = 120
      Width = 105
      Height = 21
      LookupField = 'FATURA_ID'
      LookupDisplay = 'FECHAMENTO'
      LookupSource = dsFaturaFechamento
      TabOrder = 2
      OnChange = dblkpFechamentoChange
    end
  end
  object DSEmpresas: TDataSource
    DataSet = QEmpresas
    Left = 264
    Top = 48
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  empres_id, '
      '  nome, '
      '  USA_COD_IMPORTACAO '
      'from empresas '
      'where apagado <> '#39'S'#39' '
      'order by empres_id')
    Left = 232
    Top = 48
    object QEmpresasempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasUSA_COD_IMPORTACAO: TStringField
      FieldName = 'USA_COD_IMPORTACAO'
      FixedChar = True
      Size = 1
    end
  end
  object qBoletos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'FATURA_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM BOLETOS WHERE FATURA_ID = :FATURA_ID')
    Left = 72
    Top = 152
    object qBoletosBOLETO_ID: TAutoIncField
      FieldName = 'BOLETO_ID'
      ReadOnly = True
    end
    object qBoletosCAMINHO_ARQUIVO: TStringField
      FieldName = 'CAMINHO_ARQUIVO'
      Size = 100
    end
    object qBoletosDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
    end
    object QBoletosFATURA_ID1: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qBoletosDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object qBoletosOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 50
    end
  end
  object qFaturaFechamento: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 3  FATURA_ID, FECHAMENTO FROM FATURA WHERE ID = :id'
      'AND APAGADO = '#39'N'#39
      'ORDER BY FATURA_ID DESC')
    Left = 136
    Top = 152
    object QFaturaFechamentoFATURA_ID3: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qFaturaFechamentoFECHAMENTO: TDateTimeField
      FieldName = 'FECHAMENTO'
    end
  end
  object dsFaturaFechamento: TDataSource
    DataSet = qFaturaFechamento
    Left = 168
    Top = 152
  end
  object qFaturaVencimento: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'FATURA_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DATA_VENCIMENTO FROM FATURA WHERE FATURA_ID = :FATURA_ID')
    Left = 232
    Top = 152
    object qFaturaVencimentoDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
    end
  end
  object dsFaturaVencimento: TDataSource
    DataSet = qFaturaVencimento
    Left = 264
    Top = 152
  end
  object dlgAbrePDF: TOpenDialog
    DefaultExt = 'pdf'
    Filter = 'Arquivos PDF (*.pdf)|*.pdf'
    Left = 276
    Top = 283
  end
end
