unit uRepassesCancelados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, DB, ZAbstractDataset, ZDataset,
  ZAbstractRODataset, Grids, DBGrids, {JvDBCtrl,} StdCtrls, Mask, ToolEdit,
  {JvLookup,} DateUtils, DBCtrls, ClassImpressao, JvExDBGrids, JvDBGrid,
  JvExControls, JvDBLookup, {ClipBrd,} uDatas, frxClass, frxDBSet,
  JvExMask, JvToolEdit, ADODB, JvMemoryDataset, RxMemDS, DBClient,
  JvDialogs, frxExportPDF, JvMenus, pngimage,SispagEntidades;

type ConsultaPor = (PorAutor,PorConv,PorNada);

type
  TfrmRepassesCancelados = class(TF1)
    PanAbe: TPanel;
    gpbDatas: TGroupBox;
    lblDe: TLabel;
    lblA: TLabel;
    Panel1: TPanel;
    Splitter1: TSplitter;
    grdPgtoEstab: TJvDBGrid;
    Panel2: TPanel;
    Bevel7: TBevel;
    BtnImprimir: TBitBtn;
    BtnMarcDesm: TButton;
    BtnMarcaTodos: TButton;
    BtnDesmTodos: TButton;
    Panel9: TPanel;
    Bevel1: TBevel;
    Panel10: TPanel;
    Label12: TLabel;
    LabLiq: TLabel;
    Label14: TLabel;
    LabBruto: TLabel;
    Label8: TLabel;
    LabDesc: TLabel;
    Panel3: TPanel;
    dsEmpr: TDataSource;
    dsEstab: TDataSource;
    dsPgtoEstab: TDataSource;
    dsPgtoDesc: TDataSource;
    dsPgtoPor: TDataSource;
    dsTemp: TDataSource;
    dsBancos: TDataSource;
    qPgtoPor: TADOQuery;
    qEmpr: TADOQuery;
    qEstab: TADOQuery;
    qTemp: TADOQuery;
    qBancos: TADOQuery;
    qPgtoPorPAGA_CRED_POR_ID: TIntegerField;
    qPgtoPorDESCRICAO: TStringField;
    qEmprempres_id: TIntegerField;
    qEmprnome: TStringField;
    qEmprfantasia: TStringField;
    qEstabcred_id: TIntegerField;
    qEstabnome: TStringField;
    qEstabfantasia: TStringField;
    frxReport1: TfrxReport;
    frxBancos: TfrxDBDataset;
    frxPgtoEstab: TfrxDBDataset;
    MDPagtoEstab: TJvMemoryData;
    MDPagtoEstabnome: TStringField;
    MDPagtoEstabcorrentista: TStringField;
    MDPagtoEstabcgc: TStringField;
    MDPagtoEstabcomissao: TCurrencyField;
    MDPagtoEstabcontacorrente: TStringField;
    MDPagtoEstabagencia: TStringField;
    MDPagtoEstabbruto: TCurrencyField;
    MDPagtoEstabtaxa_extra: TCurrencyField;
    MDPagtoEstabcomissao_adm: TCurrencyField;
    MDPagtoEstabtotal_retido_adm: TCurrencyField;
    MDPagtoEstabliquido: TCurrencyField;
    MDPagtoEstabcred_id: TIntegerField;
    MDPagtoEstabmarcado: TBooleanField;
    MDPagtoEstabdiafechamento1: TIntegerField;
    MDPagtoEstabvencimento1: TIntegerField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    MDPagtoEstabCODBANCO: TIntegerField;
    MDPagtoEstabNOME_BANCO: TStringField;
    MDPagtoEstabBAIXADO: TStringField;
    MDPgtoDesc: TRxMemoryData;
    MDPgtoDesccred_id: TIntegerField;
    MDPgtoDesctaxa_id: TIntegerField;
    MDPgtoDescdescricao: TStringField;
    MDPgtoDescvalor: TFloatField;
    MDPgtoDescmarcado: TBooleanField;
    CDSPagDesc: TClientDataSet;
    CDSPagDesccred_id: TIntegerField;
    CDSPagDesctaxa_id: TIntegerField;
    CDSPagDescdescricao: TStringField;
    CDSPagDescvalor: TFloatField;
    CDSPagDescmarcado: TBooleanField;
    qBancoscodbanco: TIntegerField;
    qBancosbanco: TStringField;
    MDPagtoEstabATRASADO: TStringField;
    QAux: TADOQuery;
    DSAux: TDataSource;
    Bevel2: TBevel;
    bntGerarPDF: TBitBtn;
    frxPDFExport1: TfrxPDFExport;
    SD: TJvSaveDialog;
    popCancTaxa: TPopupMenu;
    CancelaAutorizao1: TMenuItem;
    MDPagtoEstabTX_DVV: TFloatField;
    QLancCred: TADOQuery;
    MDPagtoEstabPAGAMENTO_CRED_ID: TIntegerField;
    qPgtoDesc: TADOQuery;
    qPgtoDescCRED_ID: TIntegerField;
    qPgtoDesctaxa_id: TIntegerField;
    qPgtoDescdescricao: TStringField;
    qPgtoDescvalor: TFloatField;
    qPgtoDescmarcado: TStringField;
    qPgtoDescid: TIntegerField;
    qPgtoDescTAXAS_PROX_PAG_ID_FK: TIntegerField;
    QPgtoEmpr: TADOQuery;
    DSPgtoEmp: TDataSource;
    intgrfldQPgtoEmprPAGAMENTO_CRED_ID: TIntegerField;
    QPgtoEmprDATA_COMPENSACAO: TDateTimeField;
    QPgtoEmprOPERADOR: TStringField;
    frxReport2: TfrxReport;
    QDescontosDePagtosBaixados: TADOQuery;
    QDescontosDePagtosBaixadoscred_id: TIntegerField;
    QDescontosDePagtosBaixadostaxa_id: TIntegerField;
    QDescontosDePagtosBaixadosdt_desconto: TDateTimeField;
    QDescontosDePagtosBaixadostaxas_prox_pag_id_fk: TIntegerField;
    QDescontosDePagtosBaixadosdescricao: TStringField;
    QDescontosDePagtosBaixadosvalor: TBCDField;
    QDescontosDePagtosBaixadosmarcado: TStringField;
    DSTeste: TDataSource;
    qPgtoDescTIPO_RECEITA: TStringField;
    QDescontosDePagtosBaixadosid: TIntegerField;
    QDescontosDePagtosBaixadosTIPO_RECEITA: TStringField;
    JvPopupMenu1: TJvPopupMenu;
    ConferirNotas1: TMenuItem;
    LanarTaxas1: TMenuItem;
    QPgtoEmprDESCRICAO: TStringField;
    QPgtoEmprLOTE_PAGAMENTO: TIntegerField;
    QPgtoEmprCOD_OPERACAO_LOG: TIntegerField;
    QPgtoEmprDATA_HORA: TDateTimeField;
    MDPagtoEstabENDERECO: TStringField;
    MDPagtoEstabNUMERO: TIntegerField;
    MDPagtoEstabCIDADE: TStringField;
    MDPagtoEstabCEP: TStringField;
    MDPagtoEstabESTADO: TStringField;
    MDPagtoEstabCOMPLEMENTO: TStringField;
    MDPagtoEstabTELEFONE1: TStringField;
    MDPagtoEstabCONTA_CDC: TStringField;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    GridLog: TJvDBGrid;
    Panel8: TPanel;
    Panel5: TPanel;
    GridDescontos: TJvDBGrid;
    MDPagtoEstabtaxas_avulsa: TCurrencyField;
    qPgtoEstab: TADOQuery;
    qPgtoEstabcred_id: TIntegerField;
    qPgtoEstabnome: TStringField;
    qPgtoEstabcgc: TStringField;
    qPgtoEstabbruto: TFloatField;
    qPgtoEstabcomissao: TFloatField;
    qPgtoEstabcomissao_adm: TFloatField;
    qPgtoEstabliquido: TFloatField;
    qPgtoEstabdata_hora_repasse: TDateTimeField;
    qPgtoEstabtaxa_extra: TFloatField;
    qPgtoEstabtaxas_avulsa: TFloatField;
    qPgtoEstabtx_dvv: TBCDField;
    qPgtoEstabpaga_cred_por_id: TIntegerField;
    qPgtoEstabpaga_cred_por_descricao: TStringField;
    qPgtoEstabnome_banco: TStringField;
    qPgtoEstabcorrentista: TStringField;
    qPgtoEstabcodbanco: TIntegerField;
    qPgtoEstabagencia: TStringField;
    qPgtoEstabcontacorrente: TStringField;
    qPgtoEstablote_pagamento: TIntegerField;
    txtFormaPgtoDoRepasse: TLabel;
    txtDataDoPagamento: TLabel;
    BitBtnConsultar: TBitBtn;
    dataIni: TJvDateEdit;
    dataFin: TJvDateEdit;
    dataCompensa: TJvDateEdit;
    Label1: TLabel;
    BtnCancelarPag: TButton;
    GroupBox2: TGroupBox;
    ckbMercado: TCheckBox;
    ckbPosto: TCheckBox;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    txtDataCompensacao: TJvDateEdit;
    GroupBox5: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    txtDataIniCompensacao: TJvDateEdit;
    txtDataFinCompensacao: TJvDateEdit;
    GroupBox6: TGroupBox;
    cbbPgtoPor: TJvDBLookupCombo;
    GroupBox7: TGroupBox;
    txtCodLote: TEdit;
    GroupBox8: TGroupBox;
    Label2: TLabel;
    Label7: TLabel;
    txtDataIniPagamento: TJvDateEdit;
    txtDataFinPagamento: TJvDateEdit;
    qPgtoEstabdata_cancelamento: TDateTimeField;
    qPgtoEstaboperador_cancelamento: TStringField;
    MDPagtoEstabdata_cancelamento: TDateTimeField;
    MDPagtoEstaboperador_cancelamento: TStringField;
    MDPagtoEstablote_pagamento: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtEmprChange(Sender: TObject);
    procedure cbbEmprChange(Sender: TObject);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure edtEstabKeyPress(Sender: TObject; var Key: Char);
    procedure qPgtoEstabCalcFields(DataSet: TDataSet);
    procedure btnConsultarClick(Sender: TObject);
    procedure cbbPgtoPorChange(Sender: TObject);
    procedure qPgtoEmprCalcFields(DataSet: TDataSet);
    procedure BtnMarcDesmClick(Sender: TObject);
    procedure BtnMarcaTodosClick(Sender: TObject);
    procedure BtnDesmTodosClick(Sender: TObject);
    procedure LimparTela();
    procedure grdPgtoEstabKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdPgtoEstabDblClick(Sender: TObject);
    procedure dsPgtoEstabStateChange(Sender: TObject);
    procedure BtnImprimirClick(Sender: TObject);
    procedure dsPgtoEstabDataChange(Sender: TObject; Field: TField);
    procedure edtDiaKeyPress(Sender: TObject; var Key: Char);
    procedure qPgtoEstabAfterScroll(DataSet: TDataSet);
    procedure qPgtoEmprBeforeOpen(DataSet: TDataSet);
    procedure GridDescontosDblClick(Sender: TObject);
    procedure qPgtoEstabAfterOpen(DataSet: TDataSet);
    procedure rbEstabClick(Sender: TObject);
    procedure grdPgtoEstabTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure MDPagtoEstabAfterPost(DataSet: TDataSet);
    procedure CancelaAutorizao1Click(Sender: TObject);
    procedure btnTaxaBancoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edTaxaBancoKeyPress(Sender: TObject; var Key: Char);
    procedure BtnCancelarPagClick(Sender: TObject);
    procedure edTaxaBancoExit(Sender: TObject);
    procedure dataCompensaEnter(Sender: TObject);
    procedure dataCompensaExit(Sender: TObject);
    procedure ConferirNotas1Click(Sender: TObject);
    procedure LanarTaxas1Click(Sender: TObject);
    procedure Image13Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure GridLogDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure rdgPagamentosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtnConsultarClick(Sender: TObject);
    procedure txtCodLoteKeyPress(Sender: TObject; var Key: Char);
    procedure ckbMercadoClick(Sender: TObject);
    procedure ckbPostoClick(Sender: TObject);
    procedure grdPgtoEstabDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);

  private
    { Private declarations }
    listaDeTaxas : TList;
    PgtoEstab_Sel : String;
    strPgtoDesc_Sel : String;
    TotalGeral : Currency;
    procedure Pgto_Sel;
    procedure PgtoDesc_Sel;
    procedure AbrirPagamentos;
    procedure SQLFactory;
    procedure SqlGetLancFuturos;
    procedure SQLDestroyString;
    procedure AbrirValoresEmpresas;
    procedure CarregaTabelaTemp;
    procedure AbrirValoresDescontos;
    procedure ZerarLabels;
    procedure HabilitarBotoes;
    procedure SomarMarcados;
    procedure CriarCabecalho(Imp: TImpres);
    procedure CriarCorpo(Imp: TImpres; ConsultaPor: ConsultaPor=PorNada);
    procedure CriarTitulo(Imp: TImpres);
    procedure ImprimePorConveniado(Imp: TImpres);
    procedure ImprimePorAutorizacao(Imp: TImpres);
    function getValorTaxasExtras(cred_id : Integer ; data_compensa : TDateTime) : Currency;
    function CalculaBrutoContaCorrenteByCredId(cred_id : integer) : Currency;
    function ValidaValoresPagamento(cred_id : Integer; vl_bruto : Currency): Boolean;
    function GetIdGrupoUsuario(idUsuario : Integer) : Integer;
    function GetNumeroLoteDoRepasse(cred_id : String; data_compensa : String) : Integer;
    function CalculaTaxaDVV(ValorTaxaDVV : Double; ValorBruto : Double ; ValorComissaoAdm : Double) : Double;
    //function GetValorBrutoContaCorrente(cred_id : integer; data_ini : datetime; data_fin : datetime) : Currency;
    procedure GravaLogFinanceiro(Operacao : String; Valor_Ant : String; Valor_Pos : String; Cred_id : Integer; Cod_Operacao : Integer);
    procedure SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
    procedure CarregarDadosDetalheDoRepasse(codigoDoLote : String);
    function SetUpSacado(NomeSacado: String; Logradouro: String; NumeroRes: Integer;
            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ: String) : TSacado;
//    function SetUpCedente(NomeFavorecido: String; Logradouro: String; NumeroRes: Integer;
//            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) : TCedente;

    function SetUpFavorecido(CodigoTranmissao, Logradouro, Bairro, NumeroRes, CEP,
    Cidade, CodigoFavorecido, ComplementoRes, Telefone, NomeFavorecido,
    Agencia, AgenciaDigito, Conta, ContaDigito, Modalidade, Convenio, CNPJCPF,
    UF, CodBanco: string): TCedente;
    procedure TearDownSacado();


  public
    { Public declarations }
    taxa_do_banco_old : Double;
    isCadCredOpen     : Boolean;
    SList             : TStringList;
    Sacado            : TSacado;
    Cedente           : TCedente;
    Favorecido        : TCedente;
end;

function CarregaArquivoDeRemessa() : String;

procedure RollBackTaxaAvulsa(Cred_id: Integer; Descricao: string; ValorDaTaxa:
    Double; DataDoDesconto: TDateTime; TipoReceita: String);

var
  frmRepassesCancelados: TfrmRepassesCancelados;
  sql : string;
  vvelho, vnovo, sqlQuery   : String;
  SqlLancamentosFuturos     : String;
  MarcouTodos,flagMarcado   : Boolean;
  //vCadEstab                 : Boolean;
  DesmarcouTodos            : Boolean;
  codigo_pagamento          : Integer;
  primeiraVezEfetuaPgto     : Integer; //Esta vari�vel controla se � a primeira vez que o m�todo � acessado. 0 = sim /1 = n�o

implementation

uses DM, cartao_util, UDBGridHelper, USelTipoImp, USQLMount, UMenu, Math,
  FEntregaNF, UCadCred, URotinasTexto, UCodLoteEstorno,
  USenhaDoAdm, StrUtils;

{$R *.dfm}

procedure TfrmRepassesCancelados.FormCreate(Sender: TObject);
begin
  inherited;
  HabilitarBotoes;
//  dataIni.Date := Null;
//  DataFin.Date := Null;
//  dataCompensa.Date := Null;
  qEmpr.Open;
  qEstab.Open;
  qPgtoPor.Open;
  //cbbPgtoPor.KeyValue := 1;
  FMenu.vFormRepassesCancelados := True;
  taxa_do_banco_old := 0;
  isCadCredOpen := False;
  txtFormaPgtoDoRepasse.Caption := '';
  txtDataDoPagamento.Caption := '';
  txtCodLote.SetFocus;
end;

procedure TfrmRepassesCancelados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qEmpr.Close;
  qEstab.Close;
  qPgtoPor.Close;
  if qPgtoDesc.Active then qPgtoDesc.Close;
  if qPgtoEmpr.Active then qPgtoEmpr.Close;
  if qPgtoEstab.Active then qPgtoEstab.Close;
  FMenu.vFormRepassesCancelados := False;
end;

procedure TfrmRepassesCancelados.edtEmprChange(Sender: TObject);
begin
  inherited;
  {if Trim(edtEmpr.Text) <> '' then
  begin
    if qEmpr.Locate('empres_id',edtEmpr.Text,[]) then
      cbbEmpr.KeyValue := edtEmpr.Text
    else
      cbbEmpr.ClearValue;
  end
  else
    cbbEmpr.ClearValue; }
end;


procedure TfrmRepassesCancelados.cbbEmprChange(Sender: TObject);
begin
  inherited;
  {if edtEmpr.Text <> cbbEmpr.KeyValue then
    edtEmpr.Text := string(cbbEmpr.KeyValue);}
end;


procedure TfrmRepassesCancelados.edtEmprKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TfrmRepassesCancelados.edtEstabKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;


procedure TfrmRepassesCancelados.qPgtoEstabCalcFields(DataSet: TDataSet);
begin
  inherited;
  qPgtoEstab.FieldByName('LIQUIDO').AsCurrency:= qPgtoEstab.FieldByName('BRUTO').AsCurrency -
  (qPgtoEstab.FieldByName('COMISS_C').AsCurrency + qPgtoEstab.FieldByName('COMISS_R').AsCurrency +
  qPgtoEstab.FieldByName('TAXA').AsCurrency) + (qPgtoEstab.FieldByName('REPASSE').AsCurrency);
end;

procedure TfrmRepassesCancelados.btnConsultarClick(Sender: TObject);
begin
  inherited;
  ZerarLabels;
  Screen.Cursor := crHourGlass;

//  if (cbbEstab.DisplayEmpty <> cbbEstab.Text) and (string(cbbEstab.KeyValue) <> edtEstab.Text) then begin
//    MsgInf('O c�digo do Estabelecimento n�o corresponde ao estabelecimento selecionado.'+sLineBreak+'Por favor, selecione o estabelecimento correto.');
//    cbbEstab.SetFocus;
//    Exit;
//  end;
//  if cbbPgtoPor.KeyValue = 0 then
//    MsgInf('Selecione uma forma de pagamento de estabelecimento.');

//  if (cbbPgtoPor.KeyValue = 3) and (edtDia.Text = '') then
//  begin
//    MsgInf('Digite um dia do fechamento');
//    edtDia.SetFocus;
//    Exit;
//  end;

  //if integer(cbbPgtoPor.KeyValue) >= 1 then
  //begin
  AbrirPagamentos;


  //end
  //else
    //raise Exception.Create('Forma de pagamento de Fornecedores Inv�lida!');

  //BtnCancelarPag.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TfrmRepassesCancelados.ZerarLabels;
begin
  LabLiq.Caption   := '0,00';
  LabBruto.Caption := '0,00';
  //LabComis.Caption := '0,00';
  LabDesc.Caption  := '0,00';
  MarcouTodos := False;
  DesmarcouTodos := False;
end;

procedure TfrmRepassesCancelados.CarregarDadosDetalheDoRepasse(codigoDoLote : String);
var loteRepasse : String;
begin
  if not MDPagtoEstab.IsEmpty then
  begin
    loteRepasse := txtCodLote.Text;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Text := 'SELECT ' +
    'TOP(1)DATA_COMPENSACAO,DATA_INI_PGTO, DATA_FIN_PGTO, DATA_HORA, PAGA_CRED_POR.DESCRICAO '+
    'FROM PAGAMENTO_CRED '+
    'INNER JOIN PAGA_CRED_POR on PAGA_CRED_POR.PAGA_CRED_POR_ID = PAGAMENTO_CRED.PAGA_CRED_POR_ID '+
    'WHERE LOTE_PAGAMENTO = '+ codigoDoLote;

    DMConexao.AdoQry.Open;
    dataCompensa.Date := DMConexao.AdoQry.Fields[0].AsDateTime;
    dataIni.Date  := DMConexao.AdoQry.Fields[1].AsDateTime;
    DataFin.Date  := DMConexao.AdoQry.Fields[2].AsDateTime;
    txtDataDoPagamento.Caption := DMConexao.AdoQry.Fields[3].AsString;
    txtFormaPgtoDoRepasse.Caption := DMConexao.AdoQry.Fields[4].AsString;
  end;


end;

procedure TfrmRepassesCancelados.SQLDestroyString;
begin
  sql := '';
end;

///<summary>
///Esta procedure monta uma query na vari�vel global sql para que seja executada na a��o click do bot�o Buscar
///</summary>
///<returns>
/// SQLFactory retorna a variavel global = sql Tipo = String
///</returns>
///<param name = "sql">Retorna query dinamica do GRID principal financeiro</param>

procedure TfrmRepassesCancelados.SQLFactory;
var _dataCompensa : String;
begin
   // O from � baseado na tabela credenciados cred

   sql := 'SELECT ' +
   'pc.cred_id, '+
   'cred.nome, '+
   'cred.cgc, '+
   'pc.valor_total as bruto, ' +
   'pc.per_comissao as comissao, ' +
   'pc.valor_comissao as comissao_adm, ' +
   'pc.valor_pago as liquido, ' +
   'pc.data_hora as data_hora_repasse, ' +
   'pc.taxas_fixas as taxa_extra, ' +
   'pc.taxas_variaveis as taxas_avulsa, ' +
   'pc.TAXA_DVV as tx_dvv, ' +
   'pc.paga_cred_por_id, ' +
   'paga_cred_por.descricao as paga_cred_por_descricao, ' +
   'bancos.banco as nome_banco, ' +
   'cred.correntista, ' +
   'cred.banco as codbanco, '+
   'cred.agencia, ' +
   'cred.contacorrente, ' +
   'pc.lote_pagamento, '+
   'pc.data_cancelamento, '+
   'pc.operador_cancelamento '+
   'from pagamento_cred pc '+
   'inner join credenciados cred ON cred.cred_id = pc.cred_id '+
   'inner join PAGA_CRED_POR on paga_cred_por.PAGA_CRED_POR_ID = pc.PAGA_CRED_POR_ID '+
   'inner join BANCOS on BANCOS.CODIGO = cred.BANCO where pc.cancelado = ''S''';
   if txtCodLote.Text <> '' then
      sql := sql + ' AND pc.LOTE_PAGAMENTO = '+txtCodLote.Text;

   _dataCompensa := Trim(StringReplace(txtDataCompensacao.Text,'/','',[rfReplaceAll]));
   if _dataCompensa <> '' then
      sql := sql + ' AND pc.DATA_COMPENSACAO = ' +QuotedStr(DateToStr(dataCompensa.Date));

   if Trim(StringReplace(txtDataIniCompensacao.Text,'/','',[rfReplaceAll])) <> '' then
      sql := sql + ' AND pc.DATA_COMPENSACAO between ' +QuotedStr(DateToStr(txtDataIniCompensacao.Date)) + ' and ' + QuotedStr(DateToStr(txtDataFinCompensacao.Date));

   if Trim(StringReplace(txtDataIniPagamento.Text,'/','',[rfReplaceAll])) <> '' then
      sql := sql + ' AND pc.DATA_COMPENSACAO between ' +QuotedStr(DateToStr(dataCompensa.Date)) + ' and  ' + QuotedStr(DateToStr(txtDataIniCompensacao.Date));
 //   if StringReplace(txtDataIni.Text,'/','') <> '' then
//      ''

end;

///
///<summary>pocedure montadora da query.text que retorna os lan�amentos "extra" D�bito(D) ou Cr�dito(C) lan�ados do cadastro de Credenciados/taxas
///</summary>
///<returns>
///var SqlLancamentosFuturos : String
//</returns>
///<remarks>
///Esta procedure � acionada quando marcamos uma linha no Grid Principal o intuito � calcular os valores adiantados para pagamentos futuros.
///</remarks>
procedure TfrmRepassesCancelados.SqlGetLancFuturos;
begin
   // Esta query visa trazer as taxas cadastradas no m�dulo de credenciados
   // ela pode ser tanto um valor de desconto ou valor a acresentar como receita para o credenciado.
   {SqlLancamentosFuturos := 'SELECT case a.tipo_receita when(''D'') then a.lanc_futuros else a.lanc_futuros  end as valor,a.tipo_receita as tipo_receita FROM('      +
       'SELECT '                          +
       'cred.cred_id,'                    ;
       if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 2 then
         SqlLancamentosFuturos:= SqlLancamentosFuturos + 'cast(((coalesce(cred.TX_DVV,0.0) * 41)/100) as float) as TX_DVV ,'
       else if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 10 then
         SqlLancamentosFuturos:= SqlLancamentosFuturos + 'cast(((coalesce(cred.TX_DVV,0.0) * 30)/100)as float) as TX_DVV, '
       else
         SqlLancamentosFuturos := SqlLancamentosFuturos + '0.0 as TX_DVV, ';
       SqlLancamentosFuturos := SqlLancamentosFuturos + 'cred.AGENCIA,'       +
       'cc.BAIXA_CREDENCIADO AS BAIXADO, '+
       'cred.BANCO as CODBANCO,'          +
       'ba.BANCO AS NOME_BANCO,'          +
       'taxas_prox_pag.valor as lanc_futuros,'+
       'taxas_prox_pag.tipo_receita as tipo_receita,'+
       'COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'+
       '0.0 AS TAXA_EXTRA,'+
       //'(SELECT COALESCE(SUM(t.valor),0)'+
       //'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA,'+
       //-----------------------------------------------------------------------------
       //--seleciona TAXA ADMINISTRADORA(COMISS�O)'+
       '(coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0)) as COMISSAO_ADM,'+
       //'-- fim da consulta TAXA ADMINISTRADORA(COMISS�O)'+
       //'-----------------------------------------------------------------------------'+
       //'--seleciona TOTAL RETIDO PELA ADMINISTRADORA'+
       '(coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100))'+
       '+'+
       '(SELECT COALESCE(SUM(t.valor),0)'+
       'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'+
       'AS TOTAL_RETIDO_ADM,'+
       //'-- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'+
       //'-----------------------------------------------------------------------------'+
       //'--seleciona o TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
       //'--(BRUTO - (TAXAS EXTRAS - COMISSAO))'+
       '((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0))'+
       '-'+
       '((coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),0))+(SELECT COALESCE(SUM(t.valor),0)'+
       'FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_ID and rtc.cred_id = cred.CRED_ID))'+
       ') AS LIQUIDO'+
       //'--Fim da consulta TOTAL L�QUIDO QUE A ADMINISTRADORA DEVE PAGAR PARA O CREDENCIADO'+
       //'-----------------------------------------------------------------------------'+
       ', ''N'' as ATRASADO'+
       ' from credenciados cred'+
       ' LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.data between '+QuotedStr(DateToStr(dataIni.Date))+' and '+QuotedStr(DateToStr(dataIni.Date));
               //RETORNA APENAS AS AUTORIZA��ES CONFIRMADAS
        if rdgAutoriz.ItemIndex = 1 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cc.conferido = ''S''';
        end;
        //RETORNA APENAS OS PAGAMENTOS CONFIRMADOS
        if rdgPagamentos.ItemIndex = 1 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cc.BAIXA_CREDENCIADO = ''N''';
        end;

        if rdgPagamentos.ItemIndex = 2 then
        begin
          sql := SqlLancamentosFuturos+' AND cc.BAIXA_CREDENCIADO = ''S''';
        end;

        SqlLancamentosFuturos := SqlLancamentosFuturos+' LEFT JOIN BANCOS ba ON ba.codigo = cred.banco';
        SqlLancamentosFuturos := SqlLancamentosFuturos+' LEFT JOIN taxas_prox_pag ON cred.cred_id = taxas_prox_pag.cred_id';
        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 6 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID';
          if rbEstab.ItemIndex = 1 then
            SqlLancamentosFuturos := SqlLancamentosFuturos+' AND CRED.SEG_ID = 14'
          else
            SqlLancamentosFuturos := SqlLancamentosFuturos+' AND CRED.SEG_ID = 39';
        end;

        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 7 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID and CRED.SEG_ID = 39';
        end;

        if qPgtoPorPAGA_CRED_POR_ID.AsInteger = 8 then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SEG_ID = 39';
        end;

        SqlLancamentosFuturos := SqlLancamentosFuturos+' WHERE cred.PAGA_CRED_POR_ID = '+qPgtoPorPAGA_CRED_POR_ID.AsString;
        if edtEstab.Text <> '' then
        begin
          SqlLancamentosFuturos := SqlLancamentosFuturos+' AND cred.CRED_ID = '+qEstabcred_id.AsString;
        end;
        SqlLancamentosFuturos := SqlLancamentosFuturos+' GROUP BY cred.CRED_ID,cc.BAIXA_CREDENCIADO,CRED.NOME,cred.diafechamento1,cred.vencimento1,cred.CORRENTISTA,cred.cgc,'+
        ' cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,cred.COMISSAO,cred.BANCO,cred.TX_DVV,taxas_prox_pag.valor,taxas_prox_pag.tipo_receita'+
        ' )A WHERE A.CRED_ID = '+MDPagtoEstabcred_id.AsString; }
        SqlLancamentosFuturos := ''
          +'SELECT taxas_prox_pag.valor AS valor, '
          +'                                   taxas_prox_pag.tipo_receita AS tipo_receita, '
          +'                                   cred.cred_id, '
          +'                                   TAXAS_PROX_PAG.TAXAS_PROX_PAG_ID '
          +'                                   FROM credenciados cred '
          +'                                   INNER JOIN taxas_prox_pag ON cred.cred_id = taxas_prox_pag.cred_id '
          +'                                   WHERE convert(varchar,data_do_desconto,103) = '+QuotedStr(DateToStr(dataCompensa.Date)) +' and cred.cred_id = '+MDPagtoEstabcred_id.AsString 
          +'                                   GROUP BY cred.CRED_ID '
          +'                                  ,taxas_prox_pag.tipo_receita,taxas_prox_pag.VALOR,taxas_prox_pag.taxas_prox_pag_id';
end;



function RetornaNumeros(pString:String):String;
var
  i : Integer;
  begin
    Result := '';
    for i := 0 to length(pString) do begin
      if Char(pString[i]) in ['0'..'9'] then
      Result := Result + pString[i];
    end;
  end;

function CarregaArquivoDeRemessa() : String;
//var
////  SispagItau: TSispagItau;
begin
//  SispagItau := TSispagItau.Create();
//  SispagItau.GerarRegistroHeader240(tipoInscDaEmpresa,ContaDigito,TipoDePgto,FinalidadeDoLote,HistoricoDeContaCorrente);
//  SispagItau.GerarRegistroDetalheSegmentoA();
//  SispagItau.GerarRegistroTraillerDeLote();
//  SispagItau.GerarTraillerDoArquivo240();
end;

procedure RollBackTaxaAvulsa(Cred_id: Integer; Descricao: string; ValorDaTaxa:
    Double; DataDoDesconto: TDateTime; TipoReceita: String);
var SQL : TSqlMount;
begin
  SQL := TSqlMount.Create(smtInsert,'TAXAS_PROX_PAG');
          SQL.ClearFields;
          SQL.AddField('TAXAS_PROX_PAG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR STAXAS_PROX_PAG_ID AS TAXAS_PROX_PAG_ID'),ftInteger);
          SQL.AddField('CRED_ID',Cred_id, ftInteger);
          SQL.AddField('DESCRICAO',Descricao, ftString);
          SQL.AddField('VALOR',ValorDaTaxa, ftFloat);
          SQL.AddField('TIPO_RECEITA',TipoReceita,ftString);
          SQL.AddField('DATA_DO_DESCONTO',DataDoDesconto,ftDateTime);

          DMConexao.Query1.SQL    := SQL.GetSqlString;
          DMConexao.Query1.SQL.Text;
          DMConexao.Query1.ExecSQL;
end;

//function SetUpSacado: TSacado;
//var
//  Sacado: TSacado;
//begin
//  Sacado := TSacado.Create();
//
//  Result := ;
//end;


procedure TfrmRepassesCancelados.AbrirPagamentos;
var mes : integer;
  dia : string;
  CNPJsoNumero,imprimeCNPJ : String;
  i : Integer;
  lista_credID : TList;
  valor_taxa,total_adm,total,tx_dvv,ValorTaxaBancaria,TotalDeTaxasParaDescontar,TotalDeTaxasDescontou : Double;
begin
  if(qPgtoPorPAGA_CRED_POR_ID.AsInteger >= 2) then
  begin
    SQLFactory; //Esta Procedure cria a query din�mica atribuindo valor a uma
    //vari�vel global chamada sql para passar como par�metro pro m�todo ADD da propriedade SQL
    qPgtoEstab.Close;
    qPgtoEstab.SQL.Clear;
    qPgtoEstab.SQL.Add(sql);
    qPgtoEstab.Open;
    if(qPgtoEstab.IsEmpty)then
    begin
      MDPagtoEstab.EmptyTable;
      qPgtoDesc.Close;
      qPgtoEmpr.Close;
      MsgInf('Os crit�rios de busca aplicados n�o retornaram nenhum valor!');
      exit;
    end;
    qPgtoEstab.First;
    MDPagtoEstab.Open;
    MDPagtoEstab.EmptyTable;
    MDPagtoEstab.DisableControls;
    while not qPgtoEstab.Eof do
    begin
      //if qPgtoEstabBRUTO.AsCurrency > 0.0 then begin
        MDPagtoEstab.Append;
        MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
        MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
        //MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        qPgtoEstabCORRENTISTA.AsString;
        //CNPJsoNumero := RetornaNumeros(qPgtoEstabCORRENTISTA.AsString);
        if qPgtoEstabCORRENTISTA.AsString <> '' then
        begin
          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
          end;
          if Length(CNPJsoNumero) = 14 then
          begin
            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
          end
          else
          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
        end
        else
        begin
          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
        end;
        MDPagtoEstabcgc.AsString                    := qPgtoEstabcgc.AsString;
        MDPagtoEstabcomissao.AsFloat                := qPgtoEstabCOMISSAO.AsFloat;
        MDPagtoEstabcontacorrente.AsString          := qPgtoEstabCONTACORRENTE.AsString;
        MDPagtoEstabagencia.AsString                := qPgtoEstabAGENCIA.AsString;
        MDPagtoEstabCODBANCO.AsInteger              := qPgtoEstabCODBANCO.AsInteger;
        MDPagtoEstabNOME_BANCO.AsString             := qPgtoEstabNOME_BANCO.AsString;
        MDPagtoEstabbruto.AsFloat                   := qPgtoEstabBRUTO.AsFloat;
        MDPagtoEstabcomissao_adm.AsFloat            := qPgtoEstabCOMISSAO_ADM.AsFloat;
        MDPagtoEstabtaxas_avulsa.AsCurrency         := qPgtoEstabtaxas_avulsa.AsCurrency;
        MDPagtoEstabTX_DVV.AsFloat                  := qPgtoEstabtx_dvv.AsCurrency;
        MDPagtoEstabtaxa_extra.AsCurrency           := qPgtoEstabtaxa_extra.AsCurrency;
        MDPagtoEstabliquido.AsFloat                 := qPgtoEstabliquido.AsCurrency;
        MDPagtoEstablote_pagamento.AsInteger        := qPgtoEstablote_pagamento.AsInteger;
        MDPagtoEstabdata_cancelamento.AsDateTime       := qPgtoEstabdata_cancelamento.AsDateTime;
        MDPagtoEstaboperador_cancelamento.AsString  := qPgtoEstaboperador_cancelamento.AsString;
        MDPagtoEstabmarcado.AsBoolean               := False;
        MDPagtoEstab.Post;
      //end;
      CNPJsoNumero := '';
      qPgtoEstab.Next;
    end;
    MDPagtoEstab.Next;
    MDPagtoEstab.First;
    MDPagtoEstab.EnableControls;
    //qPgtoEstab.Requery();  // adicionado hoe 06/01 - teste
    SQLDestroyString;

    //empres_sel := EmptyStr;
  end

end;

function TfrmRepassesCancelados.getValorTaxasExtras(cred_id : Integer ; data_compensa : TDateTime) : Currency;
var vlTotal : Currency;
begin
 vlTotal := DMConexao.ExecuteQuery('select coalesce(sum(valor),0) as vl_total_taxas from  TAXAS_REPASSE where cred_id = '+IntToStr(cred_id)+' and DT_DESCONTO = '+QuotedStr(DateToStr(data_compensa))+'');
 Result := vlTotal;
end;

function TfrmRepassesCancelados.CalculaTaxaDVV(ValorTaxaDVV : Double; ValorBruto : Double ; ValorComissaoAdm : Double) : Double;
begin
  Result := (ValorTaxaDVV * (ValorBruto - ValorComissaoAdm));
end;


procedure TfrmRepassesCancelados.CarregaTabelaTemp;
begin

end;

procedure TfrmRepassesCancelados.AbrirValoresDescontos;
var sql : TSqlMount;
    data : TDateTime;
    mes,ano : Integer;
    mesConvertido : Integer;
var taxa_id, cred_id,flag : Integer;

begin

  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA N�O POSSUI TAXAS EXTRAS ATRASADAS
  if MDPagtoEstabATRASADO.AsString = 'N' then
  begin
    if MDPagtoEstabBAIXADO.AsString = 'N' then
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Add('SELECT tr.ID id,rtc.CRED_ID,rtc.taxa_id,t.descricao,t.valor,coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK,t.TIPO_RECEITA AS TIPO_RECEITA ');
      qPgtoDesc.SQL.Add('FROM REL_TAXA_CRED rtc LEFT JOIN taxas_repasse_temp tr ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id');
      qPgtoDesc.SQL.Add(',taxas t where rtc.taxa_id = t.taxa_id');
      qPgtoDesc.SQL.Add(' and rtc.TAXA_ID not  in (select taxa_id from TAXAS_REPASSE where CRED_ID = '+MDPagtoEstabcred_id.AsString);
      qPgtoDesc.SQL.Add(' AND CONVERT(VARCHAR(4),YEAR(dt_desconto)) + '''' + CONVERT(VARCHAR(2),MONTH(dt_desconto)) ');
			qPgtoDesc.SQL.Add(' = CONVERT(VARCHAR(4),YEAR('+QuotedStr(DateToStr(dataCompensa.Date))+')) + '''' + CONVERT(VARCHAR(2),MONTH('+QuotedStr(DateToStr(dataCompensa.Date))+'))) ');
      qPgtoDesc.SQL.Add(' AND rtc.cred_id = '+MDPagtoEstabcred_id.AsString+'');
      qPgtoDesc.SQL.Add(' UNION ALL');
      qPgtoDesc.SQL.Add(' SELECT  TR.ID, TPP.CRED_ID, 0 AS TAXA_ID, TPP.descricao, TPP.valor, coalesce(tr.marcado,''N'') marcado, coalesce(TAXAS_PROX_PAG_ID,0) TAXAS_PROX_PAG_ID_FK,COALESCE(TPP.TIPO_RECEITA,'''') TIPO_RECEITA');
      qPgtoDesc.SQL.Add(' FROM TAXAS_PROX_PAG TPP LEFT JOIN taxas_repasse_temp tr ON TPP.TAXAS_PROX_PAG_ID = TR.TAXAS_PROX_PAG_ID_FK WHERE TPP.CRED_ID = '+MDPagtoEstabcred_id.AsString+'');
      qPgtoDesc.Sql.Add(' AND CONVERT(VARCHAR, (DATA_DO_DESCONTO), 103) = '+QuotedStr(DateToStr(dataCompensa.Date))+'');
      qPgtoDesc.SQL.Text;
      qPgtoDesc.Open;
    end
    //Se dados do grid principal forem BAIXADOS = 'S'
    else
    begin
      qPgtoDesc.Close;
      qPgtoDesc.SQL.Clear;
      qPgtoDesc.SQL.Text := QDescontosDePagtosBaixados.SQL.Text;

      qPgtoDesc.Parameters[0].Value := MDPagtoEstabcred_id.AsString;
      qPgtoDesc.Parameters[1].Value := DateToStr(dataCompensa.Date);

//      qPgtoDesc.SQL.Add('SELECT CONVERT(INT,next value for STAXAS_TEMP) id, tr.cred_id, tr.taxa_id, ');
//      qPgtoDesc.SQL.Add('CASE WHEN tr.taxa_id > 0 THEN t.descricao ');
//      qPgtoDesc.SQL.Add('WHEN tr.TAXA_ID = 0 then tr.HISTORICO end AS descricao, ');
//      qPgtoDesc.SQL.Add('CASE WHEN tr.taxa_id > 0 THEN t.VALOR WHEN tr.TAXA_ID = 0	THEN tr.VALOR	END AS VALOR, ');
//      qPgtoDesc.SQL.Add('coalesce(trt.marcado,''N'') marcado,coalesce(TR.TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_PAG_ID_FK ');
//      qPgtoDesc.SQL.Add('from taxas_repasse tr LEFT JOIN taxas t ON t.taxa_id = tr.taxa_id ');
//      qPgtoDesc.SQL.Add('LEFT JOIN taxas_repasse_temp trt ON tr.cred_id = trt.cred_id and tr.taxa_id = trt.taxa_id');
//      qPgtoDesc.SQL.Add('WHERE tr.cred_id = '+MDPagtoEstabcred_id.AsString+' and tr.dt_desconto = '+QuotedStr(dataCompensa.Text)+'');
      qPgtoDesc.Open;
//        GridDescontos.DataSource         := nil;
//
//        GridDescontos.DataSource := DSTeste;
//        //GridDescontos.DataSource := dsPgtoDesc;
//
//        QDescontosDePagtosBaixados.Close;
//        QDescontosDePagtosBaixados.Parameters.ParamByName('cred_id').Value := MDPagtoEstabcred_id.AsString;
//        QDescontosDePagtosBaixados.Parameters.ParamByName('dt_desconto').Value := DateToStr(dataCompensa.Date);
//        QDescontosDePagtosBaixados.SQL.Text;
//        QDescontosDePagtosBaixados.Open;


        


    end;
  end
  
  //VERIFICA SE O CREDENCIADO DA LINHA CLICADA POSSUI TAXAS EXTRAS ATRASADAS. CASO ENTRE NESTA CONDI��O BUSCA TAXA NA TABELA
  //TAXAS_REPASSE_ATRASADA
  else
  begin
    qPgtoDesc.Close;
    qPgtoDesc.SQL.Clear;
    qPgtoDesc.SQL.Add('SELECT tra.id id,tra.cred_id, tra.taxa_id,coalesce(tra.marcado,''N'') marcado,');
    qPgtoDesc.SQL.Add('t.descricao,t.valor FROM taxas_repasse_atrasada tra ');
    qPgtoDesc.SQL.Add('INNER JOIN taxas t ON tra.taxa_id = t.taxa_id ');
    //qPgtoDesc.SQL.Add('INNER JOIN taxas_repasse tr ON tr.taxa_id = tra.taxa_id AND TR.CRED_ID = TRA.CRED_ID');
    qPgtoDesc.SQL.Add('where tra.cred_id = '+MDPagtoEstabcred_id.AsString+' AND tra.baixado = ''N''');
    //qPgtoDesc.SQL.Add('and tra.taxa_id not in (select taxa_id from taxas_repasse where cred_id = '+MDPagtoEstabcred_id.AsString+' and negociada = ''S'')');
    qPgtoDesc.Open;
  end;
end;


procedure TfrmRepassesCancelados.AbrirValoresEmpresas;
begin
//  qPgtoEmpr.Close;
//  qPgtoEmpr.SQL.Clear;
//  qPgtoEmpr.SQL.Add('select PAGAMENTO_CRED_ID, DATA_PGTO, DATA_COMPENSACAO, OPERADOR ');
//  qPgtoEmpr.SQL.Add('from PAGAMENTO_CRED where cred_id = '+ cbbEstab.KeyValue);
//  qPgtoEmpr.SQL.Add(' AND DATEPART(YYYY,DATA_PGTO) >= 2015 ');
//  qPgtoEmpr.Open;
end;

procedure TfrmRepassesCancelados.cbbPgtoPorChange(Sender: TObject);
begin
  inherited;
  HabilitarBotoes;

end;

procedure TfrmRepassesCancelados.HabilitarBotoes;
begin
  BtnMarcDesm.Enabled     := true;
  BtnMarcaTodos.Enabled   := true;
  BtnDesmTodos.Enabled    := true;
  //BtnEfetuar.Enabled      := (not qPgtoEstab.IsEmpty) and (rdgPagamentos.ItemIndex <> 2);
  //BtnCancelarPag.Enabled  := (not qPgtoEstab.IsEmpty) and (rdgPagamentos.ItemIndex = 2);
  BtnImprimir.Enabled     := not qPgtoEstab.IsEmpty;
  bntGerarPDF.Enabled     := not qPgtoEstab.IsEmpty;
  BtnCancelarPag.Enabled  := not qPgtoEstab.IsEmpty;
end;

procedure TfrmRepassesCancelados.qPgtoEmprCalcFields(DataSet: TDataSet);
begin
  inherited;
  qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency:= qPgtoEmpr.FieldByName('BRUTO').AsCurrency -
  (qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency);
end;

/// <summary>
///  Esta procedure Marca e Desmarca linha do grid principal.
/// </summary>
///  <remarks>
///   - SqlGetLancFuturos - Retorna Lan�amentos Futuros
///  </remarks>
///  <remarks>
///   - Efetua o somat�rio dos taxas extras(lan�amentos futuros + Taxa Banc�ria + Taxas)
             /// - Lan�amentos futuros vindos da tabela taxas_prox_pag cadastro efetuado no fcred-> aba taxas
             /// - Taxa Banc�rio valor fixo de 3,90 para cred com banco <> (Itau, Bradesco, Brasil)
	           /// - Taxas Pr� cadastradas no fcred aba taxas vindas da tabela rel_taxa_cred
///  </remarks>
/// <remarks>
        /// - SQL TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP')
        ///	- Grava Na tabela 'TAXAS_REPASSE_TEMP' as taxas marcadas em tempo de execu��o
/// </remarks>

procedure TfrmRepassesCancelados.BtnMarcDesmClick(Sender: TObject);
VAR SQL : TSqlMount;
var valor_taxa,valor_taxa_aux  : Currency;
    i : Integer;
    contador,contador_aux,contador_aux_debitos_futuros,taxaDeCreditoFixo : Double;
    contador_aux_cred_futuros,DebitosFuturos,CreditosFuturos : Double;
    get_lancamentos_cred,strSQL : String;
    _isTaxaExtra : Boolean;

begin
  inherited;
  if MDPagtoEstab.IsEmpty then Exit;
    MDPagtoEstab.Edit;
    //MUDA O STATUS 'MARCADO' DA LINHA DO GRID PRINCIPAL
    MDPagtoEstabmarcado.AsBoolean := not MDPagtoEstabmarcado.AsBoolean;
    MDPagtoEstab.Post;
    Pgto_Sel;
  //--------------SE ESTIVER MARCANDO--------------

    SomarMarcados;
end;

procedure TfrmRepassesCancelados.BtnMarcaTodosClick(Sender: TObject);
var marca                                                       : TBookmark;
var SQL                                                         : TSqlMount;
var contador,DebitosFuturos,CreditosFuturos                     : Double;
var contador_aux_debitos_futuros, contador_aux_creditos_futuros,taxaDeCreditoFixo : Double;
var valor_taxa,valor_taxa_aux,contador_aux                      : Currency;
var _isTaxaExtra                                                : Boolean;
var i                                                           : Integer;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  //BtnCancelarPag.Enabled := True;
  bntGerarPDF.Enabled   := not qPgtoEstab.IsEmpty;
  //BtnEfetuar.Enabled    := rdgPagamentos.ItemIndex <> 2;
  BtnImprimir.Enabled   := true;
  BtnCancelarPag.Enabled := True;
  BtnDesmTodos.Click;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  //POSICIONA O CURSOR DO GRID PRINCIPAL NO PRIMEIRO REGISTRO
  MDPagtoEstab.First;
  //Percorrentdo o DataSet do Grid Principal 
  while not MDPagtoEstab.Eof do
  begin
    //MARCA A LINHA NO GRID PRINCIPAL
    MDPagtoEstab.Edit;
    MDPagtoEstabmarcado.AsBoolean := True;
    MDPagtoEstab.Post;
    //fim do controle de pagamentos em aberto
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  Pgto_Sel;
  SomarMarcados;
  MarcouTodos := true;
  DesmarcouTodos := false;

end;

procedure TfrmRepassesCancelados.BtnDesmTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  if MDPagtoEstab.IsEmpty then Exit;
  MDPagtoEstab.DisableControls;
  marca := MDPagtoEstab.GetBookmark;
  MDPagtoEstab.First;
  while not MDPagtoEstab.Eof do
  begin
    MDPagtoEstab.Edit;
    MDPagtoEstabmarcado.AsBoolean := False;
    MDPagtoEstab.Post;

    //REMOVE AS TACHAS DA TABELA TEMPOR�RIA AO DESMARCAR OS CREDENCIADOS
    DMConexao.ExecuteSql('DELETE FROM TAXAS_REPASSE_TEMP WHERE '+
        'CRED_ID = '+MDPagtoEstabcred_id.AsString);

    DMConexao.ExecuteSql('UPDATE taxas_repasse_atrasada SET marcado = ''N''');

    DMConexao.ExecuteSql('ALTER SEQUENCE STAXAS_TEMP RESTART WITH 1');

    MDPagtoEstab.Next;
  end;
  MDPgtoDesc.EmptyTable;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  MDPagtoEstab.EnableControls;
  Pgto_Sel;
  SomarMarcados;
  DesmarcouTodos := true;
  MarcouTodos := False;
  AbrirPagamentos;
  Screen.Cursor := crDefault;;
end;

procedure TfrmRepassesCancelados.SomarMarcados;
var
  marca : TBookmark;
  bruto, liqui, comis, desc : currency;
begin
  ZerarLabels;
  if MDPagtoEstab.Active and (not MDPagtoEstab.IsEmpty) then
  begin
    marca := MDPagtoEstab.GetBookmark;
    MDPagtoEstab.DisableControls;
    MDPagtoEstab.First;
    bruto := 0; liqui := 0;
    comis := 0; desc := 0;
    while not MDPagtoEstab.Eof do begin
      if MDPagtoEstab.FieldByName('MARCADO').AsBoolean = True then begin
        bruto := bruto + MDPagtoEstab.FieldByName('BRUTO').AsCurrency;
        comis := comis + MDPagtoEstab.FieldByName('COMISSAO').AsCurrency; //+ qPgtoEstab.FieldByName('COMISS_R').AsCurrency;
        desc  := desc  + MDPagtoEstab.FieldByName('TAXA_EXTRA').AsCurrency;
        liqui := liqui + MDPagtoEstab.FieldByName('LIQUIDO').AsCurrency;
      end;
      MDPagtoEstab.Next;
    end;
    MDPagtoEstab.GotoBookmark(marca);
    MDPagtoEstab.FreeBookmark(marca);
    MDPagtoEstab.EnableControls;
    LabLiq.Caption   := FormatDinBR(liqui);
    LabBruto.Caption := FormatDinBR(bruto);
    LabDesc.Caption  := FormatDinBR(desc);
    //LabComis.Caption := FormatDinBR(comis);
  end;

end;

procedure TfrmRepassesCancelados.grdPgtoEstabKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  GridScroll(Sender,Key,Shift);
  if key = vk_return then BtnMarcDesm.Click;
end;

procedure TfrmRepassesCancelados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f11 : BtnMarcDesm.Click;
     vk_f8  : BtnMarcaTodos.Click;
     vk_f9  : BtnDesmTodos.Click;
  end;
end;

procedure TfrmRepassesCancelados.grdPgtoEstabDblClick(Sender: TObject);
begin
  inherited;
  BtnMarcDesm.Click;
end;

procedure TfrmRepassesCancelados.dsPgtoEstabStateChange(Sender: TObject);
begin
  inherited;
  HabilitarBotoes;

end;

procedure TfrmRepassesCancelados.BtnImprimirClick(Sender: TObject);
var RegAtual : TBookmark;
  RegAtualEmp : TBookmark;
  RegAtualTax : TBookmark;
  dt : TDateTime;
  sl : TStringList;
  semana : integer;
  codigo_pagamento, lotePagamentoDoCredenciado : Integer;
  memo : TfrxMemoView;

begin
  //ShowMessage(CDSPagDescmarcado.AsString);
  sl := TStringList.Create;
  sl.Clear;
  sl.Sorted := True;
  sl.Duplicates := dupIgnore;
  TotalGeral := 0;
  CDSPagDesc.DisableControls;
  //ShowMessage(CDSPagDescmarcado.AsString);
  if DMConexao.ContaMarcados(MDPagtoEstab) = 0 then
  begin
    MsgInf('N�o h� pagamento marcado !');
  end
  else
  begin
    if MsgSimNao('Confirma a vizualiza��o do relat�rio de pagamento?') then
  begin
      RegAtual := MDPagtoEstab.GetBookmark;
      RegAtualEmp := qPgtoEmpr.GetBookmark;
      qPgtoEmpr.DisableControls;
      MDPgtoDesc.DisableControls;
      MDPagtoEstab.First;
      dt := dataCompensa.Date;
      qBancos.Close;
      qBancos.SQL.Clear;
      qBancos.SQL.Add('Select');
      qBancos.SQL.Add('b.codigo as codbanco,');
      qBancos.SQL.Add('b.banco');
      qBancos.SQL.Add('from');
      qBancos.SQL.Add('bancos b');
      while not MDPagtoEstab.Eof do
      begin
        if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) then
        begin
          if sl.Count < 1 then begin
            qBancos.SQL.Add('WHERE');
            sl.Add('CODIGO = ' + MDPagtoEstabCODBANCO.AsString)
          end else
            sl.Add('OR CODIGO = '+ MDPagtoEstabCODBANCO.AsString);
        end;
        if ((MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true) and (MDPagtoEstabBAIXADO.AsString = 'N')) then
        begin
          //GravarPagamento(dt);
        end;
        MDPagtoEstab.Next;
      end;
      if sl.Count > 0 then begin
        qBancos.SQL.Add(sl.Text);
        qBancos.Open;
      end;
      frxReport2.Variables['tipoPagamento'] := QuotedStr(txtFormaPgtoDoRepasse.Caption);
      frxReport2.Variables['dataIni']       := QuotedStr(DateToStr(dataIni.Date));
      frxReport2.Variables['dataFin']       := QuotedStr(DateToStr(DataFin.Date));
      frxReport2.Variables['dtcompensa']    := QuotedStr(DateToStr(dataCompensa.Date));
      frxReport2.Variables['dataRepasse']   := QuotedStr(txtDataDoPagamento.Caption);
      frxReport2.Variables['codAutorizacao'] := QuotedStr(txtCodLote.Text);

      if (ckbMercado.Checked) then
      begin
        frxReport2.Variables['tipoEstab']  := QuotedStr(' - Mercados');
      end
      else if (ckbPosto.Checked) then
      begin
        frxReport2.Variables['tipoEstab']  := QuotedStr(' - Postos');
      end
      else
      begin
        frxReport2.Variables['tipoEstab']  := QuotedStr('');
      end;


      frxReport2.ShowReport;
  end;
  qPgtoEmpr.EnableControls;
end;
end;

function TfrmRepassesCancelados.GetNumeroLoteDoRepasse(cred_id : String; data_compensa : String) : Integer;
var commandText : String;
    lotePagamentoDoCredenciado : Integer;
begin
  commandText := '' +
  'Select LOTE_PAGAMENTO ' +
  'from PAGAMENTO_CRED ' +
  'where cred_id = '+QuotedStr(cred_id)+' and ' +
  ' CANCELADO = ''N'' and ' +
	'DATA_COMPENSACAO = '+QuotedStr(data_compensa);


  lotePagamentoDoCredenciado := DMConexao.ExecuteQuery(commandText);

  Result := lotePagamentoDoCredenciado;
end;

procedure TfrmRepassesCancelados.CriarTitulo(Imp: TImpres);
var linha: Integer;
begin
  DMConexao.Adm.Open;
  Imp.AddLinha(DMConexao.AdmFANTASIA.AsString);
  DMConexao.Adm.Close;
  Imp.AddLinha(imp.Centraliza('Relacao de Pagamentos'));
  Imp.AddLinha(' ');
  Imp.AddLinhaSeparadora('=');
  linha:= Imp.AddLinha('Estabelecimento: '+FormatFloat('000000',qPgtoEstab.FieldByName('CRED_ID').AsInteger));
  Imp.AddLinha('- '+qPgtoEstab.FieldByName('NOME').AsString,28,linha);
  Imp.AddLinha('CNPJ: '+qPgtoEstab.FieldByName('CGC').AsString,110,linha);
  linha:= Imp.AddLinha('Banco: '+qPgtoEstab.FieldByName('CODBANCO').AsString+' - '+qPgtoEstab.FieldByName('NOMEBANCO').AsString);
  Imp.AddLinha('Agencia: '+qPgtoEstab.FieldByName('AGENCIA').AsString+'   Conta Corrente: '+qPgtoEstab.FieldByName('CONTACORRENTE').AsString,52,linha);
  Imp.AddLinhaSeparadora('=');
  Imp.AddLinha(' ');
end;

procedure TfrmRepassesCancelados.CriarCabecalho(Imp: TImpres);
var linha: Integer;
begin
//  if cbbPgtoPor.KeyValue = 1 then
//    linha := Imp.AddLinha('Empresa Conveniada                                               Fatura         Bruto    Percentual      Comissao       Liquido')
//  else
//    linha := Imp.AddLinha('Empresa Conveniada                                                              Bruto    Percentual      Comissao       Liquido');
//  Imp.AddLinhaSeparadora();
end;

procedure TfrmRepassesCancelados.CriarCorpo(Imp:TImpres; ConsultaPor: ConsultaPor);
var bru, per, com, tax, Liq: Currency; linha : integer;
  marca: TBookmark;
begin
//  bru := 0; per := 0; com := 0; liq := 0; tax := 0;
//  marca := qPgtoEmpr.GetBookmark;
//  qPgtoEmpr.DisableControls;
//  qPgtoEmpr.First;
//  while not qPgtoEmpr.Eof do
//  begin
//    if ((ConsultaPor = PorNada) and (qPgtoEmpr.Bof)) or (ConsultaPor <> PorNada) then
//      CriarCabecalho(Imp);
//    linha := imp.AddLinha(Imp.Direita(qPgtoEmpr.FieldByName('EMPRES_ID').AsString,6) +' - '+qPgtoEmpr.FieldByName('NOME').AsString);
//    if cbbPgtoPor.KeyValue = 1 then
//      Imp.AddLinha(Imp.Direita(qPgtoEmpr.FieldByName('FATURA_ID').AsString,6,'0'),68,linha);
//    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('BRUTO').AsCurrency),10),78,linha);
//    if qPgtoEmpr.FieldByName('BRUTO').AsCurrency > 0 then
//      Imp.AddLinha(Imp.Direita(FormatDinBR(((qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency)*100)/qPgtoEmpr.FieldByName('BRUTO').AsCurrency),10),92,linha)
//    else
//      Imp.AddLinha(Imp.Direita(FormatDinBR(0),10),92,linha);
//    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('COMISS_C').AsCurrency+qPgtoEmpr.FieldByName('COMISS_R').AsCurrency),10),106,linha);
//    Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency),10),120,linha);
//    if ConsultaPor = PorConv then
//      ImprimePorConveniado(Imp)
//    else if ConsultaPor = PorAutor then
//      ImprimePorAutorizacao(Imp);
//    bru := bru + qPgtoEmpr.FieldByName('BRUTO').AsCurrency;
//    com := com + qPgtoEmpr.FieldByName('COMISS_C').AsCurrency + qPgtoEmpr.FieldByName('COMISS_R').AsCurrency;
//    liq := liq + qPgtoEmpr.FieldByName('LIQUIDO').AsCurrency;
//    qPgtoEmpr.Next;
//  end;
//  if bru = 0 then
//    per := 0
//  else
//    per := (com*100)/bru;
//  Imp.AddLinhaSeparadora('=');
//  Imp.AddLinha('Total do Fornecedor                                                             Bruto    Percentual      Comissao       Liquido');
//  linha := Imp.AddLinha(Imp.Direita(FormatDinBR(bru),10),78);
//  Imp.AddLinha(Imp.Direita(FormatDinBR(per),10),92,linha);
//  Imp.AddLinha(Imp.Direita(FormatDinBR(com),10),106,linha);
//  Imp.AddLinha(Imp.Direita(FormatDinBR(liq),10),120,linha);
//  qPgtoEmpr.GotoBookmark(marca);
//  qPgtoEmpr.FreeBookmark(marca);
//  qPgtoEmpr.EnableControls;
//  Imp.SaltarLinhas(1);
//  Imp.AddLinha('-------------------------------------------------------------');
//  linha:= Imp.AddLinha('Valor Liquido');
//  Imp.AddLinha(Imp.Direita(FormatDinBR(liq),10)+' (+)',50,linha);
//  Imp.AddLinha('-------------------------------------------------------------');
//  if (not qPgtoDesc.IsEmpty) or (qPgtoEstab.FieldByName('REPASSE').AsCurrency > 0.00) then
//  begin
//    marca := qPgtoDesc.GetBookmark;
//    qPgtoDesc.DisableControls;
//    qPgtoDesc.First;
//    Imp.AddLinha('Descontos e Acr�cimos');
//    while not qPgtoDesc.Eof do
//    begin
//      linha:= Imp.AddLinha(qPgtoDesc.FieldByName('DESCRICAO').AsString);
//      Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoDesc.FieldByName('VALOR').AsCurrency),10)+' (-)',50,linha);
//      tax:= tax + qPgtoDesc.FieldByName('VALOR').AsCurrency;
//      qPgtoDesc.Next;
//    end;
//    qPgtoDesc.GotoBookmark(marca);
//    qPgtoDesc.FreeBookmark(marca);
//    qPgtoDesc.EnableControls;
//    if qPgtoEstab.FieldByName('REPASSE').AsCurrency > 0.00 then
//    begin
//      linha:= Imp.AddLinha('REPASSE DE VENDAS');
//      Imp.AddLinha(Imp.Direita(FormatDinBR(qPgtoEstab.FieldByName('REPASSE').AsCurrency),10)+' (+)',50,linha);
//    end;
//    Imp.AddLinha('-------------------------------------------------------------');
//  end;
//  linha:= Imp.AddLinha('Valor a Pagar');
//  Imp.AddLinha(Imp.Direita(FormatDinBR((liq-tax)+qPgtoEstab.FieldByName('REPASSE').AsCurrency),10)+' (=)',50,linha);
end;

procedure TfrmRepassesCancelados.ImprimePorConveniado(Imp: TImpres);
var linha: Integer;
begin
//  qTemp.Close;
//  qTemp.SQL.Clear;
//  qTemp.SQL.Add(' select ');
//  qTemp.SQL.Add('   cv.conv_id, ');
//  qTemp.SQL.Add('   cv.chapa, ');
//  qTemp.SQL.Add('   cv.titular, ');
//  qTemp.SQL.Add('   sum(cc.debito-cc.credito) valor ');
//  qTemp.SQL.Add(' from contacorrente cc ');
//  qTemp.SQL.Add(' join conveniados cv on cc.conv_id = cv.conv_id ');
//  if cbbPgtoPor.KeyValue = 1 then
//    qTemp.SQL.Add(' where cc.fatura_id = ' + qPgtoEmpr.FieldByName('FATURA_ID').AsString)
//  else
//    qTemp.SQL.Add(' where cc.data between ' + FormatDataIB(qPgtoEmpr.FieldByName('DTINI').AsDateTime) + ' and ' + FormatDataIB(qPgtoEmpr.FieldByName('DTFIM').AsDateTime));
//  qTemp.SQL.Add(' and cc.baixa_credenciado = ''N'' ');
//  qTemp.SQL.Add(' and cv.empres_id = ' + qPgtoEmpr.FieldByName('EMPRES_ID').AsString);
//  qTemp.SQL.Add(' and cc.cred_id = ' + qPgtoEstab.FieldByName('CRED_ID').AsString);
//  qTemp.SQL.Add(' group by cv.conv_id, cv.chapa, cv.titular ');
//  qTemp.SQL.Add(' order by cv.titular, cv.chapa ');
//  qTemp.Open;
//  if not qTemp.IsEmpty then
//  begin
//    qTemp.First;
//    Imp.AddLinha('-------------------------------------------------------------------------',57);
//    Imp.AddLinha('| Nome do Conveniado                                                 Valor',56);
//    while not qTemp.Eof do
//    begin
//      linha:= Imp.AddLinha('| '+FormatFloat('000000000000',qTemp.FieldByName('CHAPA').AsFloat),56);
//      Imp.AddLinha('- '+qTemp.FieldByName('TITULAR').AsString,71,linha);
//      Imp.AddLinha(Imp.Direita(FormatDinBR(qTemp.FieldByName('VALOR').AsCurrency),10),120,linha);
//      qTemp.Next;
//    end;
//    Imp.SaltarLinhas(1);
//  end;
end;

procedure TfrmRepassesCancelados.ImprimePorAutorizacao(Imp: TImpres);
var linha: Integer;
begin
//  qTemp.Close;
//  qTemp.SQL.Clear;
//  qTemp.SQL.Add(' select ');
//  qTemp.SQL.Add('    cc.trans_id, ');
//  qTemp.SQL.Add('    cc.autorizacao_id, ');
//  qTemp.SQL.Add('    cc.digito, ');
//  qTemp.SQL.Add('    cv.conv_id, ');
//  qTemp.SQL.Add('    cv.chapa, ');
//  qTemp.SQL.Add('    cv.titular, ');
//  qTemp.SQL.Add('    cc.data, ');
//  qTemp.SQL.Add('    cc.debito-cc.credito as valor ');
//  qTemp.SQL.Add(' from contacorrente cc ');
//  qTemp.SQL.Add(' join conveniados cv on cc.conv_id = cv.conv_id ');
//  if cbbPgtoPor.KeyValue = 1 then
//    qTemp.SQL.Add(' where cc.fatura_id = ' + qPgtoEmpr.FieldByName('FATURA_ID').AsString)
//  else
//    qTemp.SQL.Add(' where cc.data between ' + FormatDataIB(qPgtoEmpr.FieldByName('DTINI').AsDateTime) + ' and ' + FormatDataIB(qPgtoEmpr.FieldByName('DTFIM').AsDateTime));
//  qTemp.SQL.Add(' and cc.baixa_credenciado = ''N'' ');
//  qTemp.SQL.Add(' and cv.empres_id = ' + qPgtoEmpr.FieldByName('EMPRES_ID').AsString);
//  qTemp.SQL.Add(' and cc.cred_id = ' + qPgtoEstab.FieldByName('CRED_ID').AsString);
//  qTemp.SQL.Add(' order by cv.titular, cc.autorizacao_id, cc.trans_id ');
//  qTemp.Open;
//  if not qTemp.IsEmpty then
//  begin
//    qTemp.First;
//    if (not qEmpr.Bof) and (qTemp.Bof) then
//      CriarCabecalho(Imp);
//    Imp.AddLinha('------------------------------------------------------------------------------------------------------------------',16);
//    Imp.AddLinha('| Transacao    Autorizacao    Nome do Conveniado                                                 Data         Valor',15);
//    while not qTemp.Eof do
//    begin
//      linha:= Imp.AddLinha('| '+Imp.Direita(qTemp.FieldByName('TRANS_ID').AsString,8),15);
//      Imp.AddLinha(Imp.Direita(qTemp.FieldByName('AUTORIZACAO_ID').AsString,8),30,linha);
//      Imp.AddLinha('-'+Imp.Direita(qTemp.FieldByName('DIGITO').AsString,2,'0'),38,linha);
//      Imp.AddLinha(FormatFloat('000000000000',qTemp.FieldByName('CHAPA').AsFloat),45,linha);
//      Imp.AddLinha('-'+qTemp.FieldByName('TITULAR').AsString,56,linha);
//      Imp.AddLinha(FormatDataBR(qTemp.FieldByName('DATA').AsDateTime),106,linha);
//      Imp.AddLinha(Imp.Direita(FormatDinBR(qTemp.FieldByName('VALOR').AsCurrency),10),120,linha);
//      qTemp.Next;
//    end;
//    Imp.SaltarLinhas(1);
//  end;
end;

procedure TfrmRepassesCancelados.GravaLogFinanceiro(Operacao : String; Valor_Ant : String; Valor_Pos : String; Cred_id : Integer; Cod_Operacao : Integer);
var SQL : TSqlMount;
begin
  SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
          SQL.ClearFields;
          SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
          SQL.AddField('DATAHORA',Now, ftDateTime);
          SQL.AddField('OPERADOR',Operador.Nome, ftString);
          SQL.AddField('OPERACAO',Operacao, ftString);
          SQL.AddField('VALOR_ANT',Valor_Ant,ftString);
          SQL.AddField('VALOR_POS',Valor_Pos,ftString);
          SQL.AddField('CRED_ID',IntToStr(Cred_id),ftString);
          SQL.AddField('COD_OPERACAO',IntToStr(Cod_Operacao),ftString);
          SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
          SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
          SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(dataFin.Text),ftDateTime);
          DMConexao.Query1.SQL    := SQL.GetSqlString;
          DMConexao.Query1.SQL.Text;
          DMConexao.Query1.ExecSQL;
end;

function TfrmRepassesCancelados.CalculaBrutoContaCorrenteByCredId(cred_id : integer) : Currency;
var SQL : String;
begin
  SQL :=  'SELECT coalesce(sum(cc.debito - cc.credito),0) Bruto '
        + 'FROM contacorrente cc '
        + 'INNER JOIN credenciados ON (credenciados.cred_id = cc.cred_id) '
        + 'WHERE cc.data BETWEEN '+QuotedStr(DateToStr(dataIni.Date))
	      +	' AND '+QuotedStr(DateToStr(DataFin.Date));
//        if rdgAutoriz.ItemIndex = 1 then
//        begin
//          SQL := SQL + ' and CONFERIDO = ''S''';
//        end;
	      SQL := SQL + ' AND cc.cred_id = '+IntToStr(cred_id);

  Result := DMConexao.ExecuteQuery(SQL);

end;


procedure TfrmRepassesCancelados.dsPgtoEstabDataChange(Sender: TObject; Field:
    TField);
    var strSql : string;
begin
  inherited;
//  if not MDPagtoEstab.IsEmpty then
//  begin
//    if trim(txtCodLote.Text) = '' then
      CarregarDadosDetalheDoRepasse(MDPagtoEstablote_pagamento.AsString);
//  end;
  //Screen.Cursor := crDefault;

end;

procedure TfrmRepassesCancelados.edtDiaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#32,#8]) then
    Key := #0;
end;

procedure TfrmRepassesCancelados.Pgto_Sel;
var marca : TBookmark;
begin
  PgtoEstab_Sel := EmptyStr;
  marca := MDPagtoEstab.GetBookMark;
  MDPagtoEstab.DisableControls;
  MDPagtoEstab.First;
  while not MDPagtoEstab.eof do begin
    if MDPagtoEstabMarcado.AsBoolean then PgtoEstab_Sel := PgtoEstab_Sel + ','+MDPagtoEstabcred_id.AsString;
    MDPagtoEstab.Next;
  end;
  MDPagtoEstab.GotoBookmark(marca);
  MDPagtoEstab.FreeBookmark(marca);
  if PgtoEstab_Sel <> '' then PgtoEstab_Sel := Copy(PgtoEstab_Sel,2,Length(PgtoEstab_Sel));
  MDPagtoEstab.EnableControls;
end;

procedure TfrmRepassesCancelados.PgtoDesc_Sel;
var marca : TBookmark;
begin
  strPgtoDesc_Sel := EmptyStr;
  marca := CDSPagDesc.GetBookmark;
  CDSPagDesc.DisableControls;
  CDSPagDesc.First;
  while not CDSPagDesc.Eof do begin
    if CDSPagDescMarcado.AsBoolean then strPgtoDesc_Sel := strPgtoDesc_Sel + ','+CDSPagDesccred_id.AsString;
    CDSPagDesc.Next;
  end;
  CDSPagDesc.GotoBookmark(marca);
  CDSPagDesc.FreeBookmark(marca);
  if strPgtoDesc_Sel  <> '' then strPgtoDesc_Sel  := Copy(strPgtoDesc_Sel ,2,Length(strPgtoDesc_Sel ));
  CDSPagDesc.EnableControls;
end;

procedure TfrmRepassesCancelados.qPgtoEstabAfterScroll(DataSet: TDataSet);
begin
  if ((qPgtoEstab.FieldByName('MARCADO').AsString = 'S') and (qPgtoEstab.FieldByName('BAIXADO').AsString = 'S')) then
    TotalGeral := TotalGeral + qPgtoEstabLIQUIDO.AsCurrency;
end;


procedure TfrmRepassesCancelados.qPgtoEmprBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if MDPagtoEstabATRASADO.AsString = 'S' then
  begin
     qPgtoEmpr.Parameters[0].Value := '10/11/2014';
     qPgtoEmpr.Parameters[1].Value := IncDay(dataIni.Date, -1);
  end
  else
  begin
     qPgtoEmpr.Parameters[0].Value := dataIni.Text;
     qPgtoEmpr.Parameters[1].Value := DataFin.Text;
  end;
end;

procedure TfrmRepassesCancelados.GridDescontosDblClick(Sender: TObject);
var marca,marca_desc               : TBookmark;
    taxa_id,id,taxa_prox_pag_id_fk : Integer;
    valor_desconto                 : Currency;
    cred_id,cred_id_sel            : Variant;
    sql                            : TSqlMount;
begin
  inherited;

   if MDPagtoEstabmarcado.AsBoolean = False then
   begin
     MsgInf('Selecione um credenciado!');
     Exit;
   end;


   cred_id := GridDescontos.Fields[2].Value;
   taxa_id := GridDescontos.Fields[3].Value;
   valor_desconto := GridDescontos.Fields[1].AsCurrency;
   taxa_prox_pag_id_fk := GridDescontos.Fields[6].Value;
   //SE TACHA FOR ATRASADA
   if MDPagtoEstabATRASADO.AsString = 'S' then
   begin
     if GridDescontos.Fields[4].Value = 'S' then
     begin
       id := GridDescontos.Fields[5].Value;
       DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET MARCADO = ''N'' WHERE CRED_ID = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
       MDPagtoEstab.Edit;
       MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency - valor_desconto;
       MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency + valor_desconto;
       MDPagtoEstab.Post;
     end
     else
     begin
        id := GridDescontos.Fields[5].Value;
        DMConexao.ExecuteSql('UPDATE TAXAS_REPASSE_ATRASADA SET MARCADO = ''S'' WHERE CRED_ID = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
        MDPagtoEstab.Edit;
        MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + valor_desconto;
        MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency - valor_desconto;
        MDPagtoEstab.Post;
     end;
   end

   else
   begin
     //Se Marcado = 'S' fa�a:
     if GridDescontos.Fields[4].Value = 'S' then
     begin
       if taxa_id <> 0 then
       begin
         DMConexao.ExecuteSql('DELETE FROM taxas_repasse_temp WHERE cred_id = '+IntToStr(cred_id)+' and taxa_id = '+IntToStr(taxa_id));
       end
       else begin
         MsgInf('N�o � permitido remover taxas avulsas por aqui. Remova-a pelo cadastro de credenciados');
         Exit;
         //DMConexao.ExecuteSql('DELETE FROM taxas_repasse_temp WHERE cred_id = '+IntToStr(cred_id)+' and taxas_prox_pag_id_fk = '+IntToStr(taxa_prox_pag_id_fk));
       end;
       MDPagtoEstab.Edit;
       MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency - valor_desconto;
       MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency + valor_desconto;
       MDPagtoEstab.Post;
     end
     else
     begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXAS_TEMP');
        DMConexao.AdoQry.Open;
        SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE_TEMP');
        SQL.ClearFields;
        sql.AddField('ID',DMConexao.AdoQry.Fields[0].AsInteger,ftInteger);
        sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
        if taxa_id <> 0 then
          sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger)
        else begin
          qPgtoDesc.Requery();
          sql.AddField('TAXAS_PROX_PAG_ID_FK',qPgtoDescTAXAS_PROX_PAG_ID_FK.AsInteger,ftInteger);
        end;
        sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
        sql.AddField('MARCADO','S',ftString);
        DMConexao.Query1.SQL    := SQL.GetSqlString;
        DMConexao.Query1.ExecSQL;

        MDPagtoEstab.Edit;
        MDPagtoEstabtaxa_extra.AsCurrency := MDPagtoEstabtaxa_extra.AsCurrency + valor_desconto;
        MDPagtoEstabliquido.AsCurrency    := MDPagtoEstabliquido.AsCurrency - valor_desconto;
        MDPagtoEstab.Post;
     end;
     end;
   SomarMarcados;
   AbrirValoresDescontos;
end;

procedure TfrmRepassesCancelados.qPgtoEstabAfterOpen(DataSet: TDataSet);
var diferencaMes : Integer;
begin
  inherited;
end;

procedure TfrmRepassesCancelados.rbEstabClick(Sender: TObject);
var dataPosto,dataMercado : TDate;
begin
  inherited;
//  if rbEstab.ItemIndex = 1 then
//  begin
//    dataCompensa.SetFocus;
//    dataPosto := IncDay(dataCompensa.Date,-1);
//    dataCompensa.Date := dataPosto;
//  end
//  else
//  begin
//    dataCompensa.SetFocus;
//    dataMercado := IncDay(dataCompensa.Date,+1);
//    dataCompensa.Date := dataMercado;
//  end;

end;

procedure TfrmRepassesCancelados.grdPgtoEstabTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
var
  i : Integer;
  imprimeCNPJ,CNPJsoNumero : String;
  total_adm,tx_dvv : Double;
begin
  inherited;
  //try
//    if Pos(Field.FieldName,qPgtoEstab.Sort) > 0 then begin
//       if Pos(' DESC',qPgtoEstab.Sort) > 0 then qPgtoEstab.Sort := Field.FieldName
//                                                  else qPgtoEstab.Sort := Field.FieldName+' DESC';
//    end
//    else qPgtoEstab.Sort := Field.FieldName;
//    except
//   end;
//
//   qPgtoEstab.First;
//   MDPagtoEstab.Open;
//   MDPagtoEstab.EmptyTable;
//   MDPagtoEstab.DisableControls;
//
//   while not qPgtoEstab.Eof do begin
//      MDPagtoEstab.Append;
//      MDPagtoEstabcred_id.AsInteger           := qPgtoEstabcred_id.AsInteger;
//      MDPagtoEstabnome.AsString               := qPgtoEstabnome.AsString;
//        //MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
//        qPgtoEstabCORRENTISTA.AsString;
//        //CNPJsoNumero := RetornaNumeros(qPgtoEstabCORRENTISTA.AsString);
//        if qPgtoEstabCORRENTISTA.AsString <> '' then
//        begin
//          for i := 0 to length(qPgtoEstabCORRENTISTA.AsString) do begin
//            if Char(qPgtoEstabCORRENTISTA.AsString[i]) in ['0'..'9'] then
//            CNPJsoNumero := CNPJsoNumero + qPgtoEstabCORRENTISTA.AsString[i];
//          end;
//          if Length(CNPJsoNumero) = 14 then
//          begin
//            imprimeCNPJ := copy(CNPJsoNumero, 1, 2) + '.' + copy(CNPJsoNumero, 3, 3) + '.' + copy(CNPJsoNumero, 6, 3) + '.' + copy(CNPJsoNumero, 9, 4) + '-' + copy(CNPJsoNumero, 13, 2);
//            MDPagtoEstabcorrentista.AsString := imprimeCNPJ
//          end
//          else
//          MDPagtoEstabcorrentista.AsString := qPgtoEstabCORRENTISTA.AsString;
//        end
//        else
//        begin
//          MDPagtoEstabcorrentista.AsString        := qPgtoEstabCORRENTISTA.AsString;
//        end;
//
//        MDPagtoEstabcgc.AsString                := qPgtoEstabcgc.AsString;
//        MDPagtoEstabcomissao.AsFloat            := qPgtoEstabCOMISSAO.AsFloat;
//        MDPagtoEstabcontacorrente.AsString      := qPgtoEstabCONTACORRENTE.AsString;
//        MDPagtoEstabagencia.AsString            := qPgtoEstabAGENCIA.AsString;
//        MDPagtoEstabCODBANCO.AsInteger          := qPgtoEstabCODBANCO.AsInteger;
//        MDPagtoEstabNOME_BANCO.AsString         := qPgtoEstabNOME_BANCO.AsString;
//        MDPagtoEstabbruto.AsFloat               := qPgtoEstabBRUTO.AsFloat;
//        if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
//        //if MDPagtoEstabCODBANCO.Value in[1,2,3] then
//        begin
//         //DPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat + StrToFloat(edTaxaBanco.Text);
//          //MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat - StrToFloat(edTaxaBanco.Text);
//        end
//        else
//        begin
//          MDPagtoEstabtaxa_extra.AsFloat := qPgtoEstabTAXA_EXTRA.AsFloat;
//        end;
//        MDPagtoEstabcomissao_adm.AsFloat        := qPgtoEstabCOMISSAO_ADM.AsFloat;
//        total_adm := (qPgtoEstabBRUTO.AsFloat * (qPgtoEstabCOMISSAO.AsFloat/100)) + qPgtoEstabTAXA_EXTRA.AsFloat + MDPagtoEstabtaxa_extra.AsFloat;
//        MDPagtoEstabtotal_retido_adm.AsFloat    := total_adm;
//        if total_adm > qPgtoEstabBRUTO.AsFloat then begin
//          MDPagtoEstabliquido.AsFloat           := qPgtoEstabBRUTO.AsFloat - qPgtoEstabCOMISSAO_ADM.AsFloat;
//        end
//        else
//        MDPagtoEstabliquido.AsFloat             := qPgtoEstabBRUTO.AsFloat - total_adm;
//        if(qPgtoPorPAGA_CRED_POR_ID.AsInteger in [2,10]) then
//        begin
//          tx_dvv := qPgtoEstabTX_DVV.AsFloat * MDPagtoEstabliquido.AsFloat;
//          MDPagtoEstabliquido.AsFloat           := MDPagtoEstabliquido.AsFloat - (tx_dvv);
//          MDPagtoEstabTX_DVV.AsFloat            := tx_dvv;
//        end
//        else
//        MDPagtoEstabTX_DVV.AsFloat              := 0.0;
//        MDPagtoEstabBAIXADO.AsString            := qPgtoEstabBAIXADO.AsString;
//        MDPagtoEstabATRASADO.AsString           := qPgtoEstabATRASADO.AsString;
//        MDPagtoEstabmarcado.AsBoolean           := False;
//        MDPagtoEstab.Post;
//      qPgtoEstab.Next;
//   end;
//   MDPagtoEstab.First;
//   MDPagtoEstab.EnableControls;
end;

procedure TfrmRepassesCancelados.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BtnImprimir.Click;
    frxReport1.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;

end;

procedure TfrmRepassesCancelados.MDPagtoEstabAfterPost(DataSet: TDataSet);
var diferencaMes : Integer;
begin
  inherited;

  {DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQL.Add('SELECT MAX(tr.DT_DESCONTO),rtc.taxa_id,rtc.cred_id from taxas_repasse tr');
  DMConexao.Q.SQL.Add(' inner join rel_taxa_cred rtc ON rtc.cred_id = tr.CRED_ID and tr.CRED_ID = '+MDPagtoEstabcred_id.AsString);
  DMConexao.Q.SQL.Add(' WHERE tr.taxa_id in');
  DMConexao.Q.SQL.Add(' (SELECT taxa_id from rel_taxa_cred WHERE cred_id = '+MDPagtoEstabcred_id.AsString+')');
  DMConexao.Q.SQL.Add(' group by rtc.taxa_id,rtc.cred_id');
  DMConexao.Q.Open;}
  DMConexao.Q.Close;
  DMConexao.Q.SQL.Clear;
  DMConexao.Q.SQl.Add('SELECT MAX(tr.DT_DESCONTO) AS DT_DESCONTO,TAXA_ID FROM TAXAS_REPASSE AS TR WHERE  CRED_ID = '+MDPagtoEstabcred_id.AsString);
  DMConexao.Q.SQL.Add('GROUP BY TR.TAXA_ID');
  DMConexao.Q.Open;
  if not DMConexao.Q.IsEmpty then
  begin
    while not DMConexao.Q.Eof do
    begin
      diferencaMes := MonthsBetween(dataIni.Date,DMConexao.Q.Fields[0].Value);
      if (diferencaMes >= 2) AND (MDPagtoEstabATRASADO.AsString = 'S') then
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT * FROM taxas_repasse_atrasada WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and ');
        DMConexao.AdoQry.SQL.Add('mes_referencia = '+IntToStr(diferencaMes)+' and taxa_id = '+DMConexao.Q.Fields[1].AsString);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          DMConexao.ExecuteSql('Insert into taxas_repasse_atrasada(id,cred_id,taxa_id,dt_referencia,marcado,baixado,mes_referencia)'+
                               'values ( next value for STAXAS_TEMP,'+MDPagtoEstabcred_id.AsString+','+DMConexao.Q.Fields[1].AsString+','''+DateToStr(IncMonth(dataIni.Date,-1))+''',''N'',''N'','+IntToStr(diferencaMes)+')');
        end;
      end;
      DMConexao.Q.Next;
    end;

  end;

end;

procedure TfrmRepassesCancelados.CancelaAutorizao1Click(Sender: TObject);
VAR SQL : TSqlMount;
    data : TDateTime;
begin
  inherited;
  if Application.MessageBox('Confirma o cancelamento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    //                   janela           ,campo     , vr_ant                                   , vr_novo                             , operador    , operacao  , cadastro                      , id                                                    , detalhe                                                ,)
    DMConexao.GravaLog('FPgtoEstabAberto','Negociada', 'N', 'S',Operador.Nome,'Exclus�o',qPgtoDesc.FieldByName('TAXA_ID').AsString,Self.Name);
  end;

  DMConexao.ExecuteSql('delete from taxas_repasse_atrasada WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and taxa_id = '+qPgtoDesctaxa_id.AsString);
  SQL := TSqlMount.Create(smtInsert,'TAXAS_REPASSE');
  SQL.ClearFields;
  sql.AddField('CRED_ID',qPgtoDescCRED_ID.AsInteger,ftInteger);
  sql.AddField('TAXA_ID',qPgtoDesctaxa_id.AsInteger,ftInteger);
  sql.AddField('DT_DESCONTO',FormatDateTime('dd/mm/yyyy hh:nn:ss',Now),ftDateTime);
  sql.AddField('NEGOCIADA','S',ftString);
  DMConexao.Query1.SQL    := SQL.GetSqlString;
  DMConexao.Query1.ExecSQL;



end;

procedure TfrmRepassesCancelados.btnTaxaBancoClick(Sender: TObject);
VAR SQL    : TSqlMount;
    strSQL,sqlQuery: String;

begin


end;

procedure TfrmRepassesCancelados.Button1Click(Sender: TObject);
var sqlQuery : String;
begin
//  inherited;
//  MDPagtoEstab.First;
//  while not MDPagtoEstab.Eof do
//  begin
//    if MDPagtoEstabmarcado.AsBoolean = true then
//    begin
//      if MDPagtoEstabCONTA_CDC.AsString = 'S' then
//      begin
//        if (NOT (MDPagtoEstabCODBANCO.Value IN [2S
//          MDPagtoEstab.Edit;
//          MDPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat - StrToFloat(edTaxaBanco.Text);
//          MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat + StrToFloat(edTaxaBanco.Text);
//          MDPagtoEstab.Post;
//        end
//      end
//      else if NOT (MDPagtoEstabCODBANCO.Value IN [1,2,3]) then
//      begin
//        MDPagtoEstab.Edit;
//        MDPagtoEstabtaxa_extra.AsFloat := MDPagtoEstabtaxa_extra.AsFloat - StrToFloat(edTaxaBanco.Text);
//        MDPagtoEstabliquido.AsFloat := MDPagtoEstabliquido.AsFloat + StrToFloat(edTaxaBanco.Text);
//        MDPagtoEstab.Post;
//      end;
//    end;
//    MDPagtoEstab.Next;
//    sqlQuery:= ' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERACAO, OPERADOR) '+
//               ' values (next value for SLog_Financeiro,current_timestamp, '+QuotedStr('Removeu taxa banc�ria')+','+QuotedStr(Operador.Nome)+')';
//    DMConexao.ExecuteSql(sqlQuery);
//
//
//  end;
//  sqlQuery  := '';

end;

procedure TfrmRepassesCancelados.edTaxaBancoKeyPress(Sender: TObject;
  var Key: Char);
var
  Texto, Texto2: string;
  contador : Integer;
  i: byte;
begin
  inherited;
  if (Key in ['0'..'9',chr(vk_back)]) then
   begin
      
      // limito a 23 caracteres sen�o haver� um erro na fun��o StrToInt64()
      if (key in ['0'..'9']) and (Length(Trim(TEdit(Sender).Text))>23) then
         key := #0;

      // pego somente os caracteres de 0 a 9, ignorando a pontua��o
      Texto2 := '0';
      Texto := Trim(TEdit(Sender).Text)+Key;
      for i := 1 to Length(Texto) do
         if Texto[i] in ['0'..'9'] then
            Texto2 := Texto2 + Texto[i];

      // se foi pressionado BACKSPACE (�nica tecla v�lida, fora os n�meros)
      // apago o �ltimo caractere da string
      if key = chr(vk_back) then
         Delete(Texto2,Length(Texto2),1);

      // formato o texto que depois ser� colocado no Edit
      Texto2 := FormatFloat('#,0.00',StrToInt64(Texto2)/100);

      // preencho os espa�os � esquerda, de modo a deixar o texto
      // alinhado � direita (gambiarra)
      repeat
         Texto2 := ' '+Texto2
      until Canvas.TextWidth(Texto2) >= TEdit(Sender).Width;

      // atribuo a string � propriedade Text do Edit
      TEdit(Sender).Text := Texto2;

      // posiciono o cursor no fim do texto
      TEdit(Sender).SelStart := Length(Texto2);

   end;

   Key := #0;
end;

procedure TfrmRepassesCancelados.BtnCancelarPagClick(Sender: TObject);
VAR SQL : TSqlMount;
    qtdDeCancelamentos : Integer;
    codigoLoteDoCancelamento : String;
begin
  inherited;
  if DMConexao.ContaMarcados(MDPagtoEstab) = 0 then
  begin
    MsgInf('N�o h� pagamento marcado !');
  end
  else
  begin
    if MsgSimNao('Confirma o cancelamento dos pagamentos ?'+sLineBreak+' Este processo reverter� os pagamentos e suas respectivas taxas para Pendente!') then
    begin
       FSenhaDoAdm  := TFSenhaDoAdm.Create(Self);
       FSenhaDoAdm.ShowModal;
       if FSenhaDoAdm.ModalResult = mrOk then
       begin
          if (FSenhaDoAdm.txtSenha.Text = '5291') THEN
          begin
            MDPagtoEstab.First;
            while not MDPagtoEstab.Eof do
            begin
              codigoLoteDoCancelamento := FSenhaDoAdm.txtSenha.Text;
              Screen.Cursor := crHourGlass;
              if (MDPagtoEstab.FieldByName('MARCADO').AsBoolean = true)  then
              begin
                //Faz manuten��o na contacorrente retornando o status baixa_credenciado = S para baixa_credenciado = N
                DMConexao.ExecuteSql('UPDATE contacorrente SET baixa_credenciado = ''N'',pagamento_cred_id = 0 WHERE data between '+
                ''+QuotedStr(dataIni.Text)+' and '+QuotedStr(DataFin.Text)+' and lote_repasse_cred = '+codigoLoteDoCancelamento+' and cred_id = '+ MDPagtoEstabcred_id.AsString);

                //ESTORNA TAXAS AVUSAS
                DMConexao.AdoQry.Close;
                DMConexao.AdoQry.SQL.Clear;
                DMConexao.AdoQry.SQL.Add('SELECT HISTORICO,VALOR,DT_DESCONTO,TIPO_DESCONTO FROM TAXAS_REPASSE WHERE CRED_ID = '+MDPagtoEstabcred_id.AsString +
                                        ' AND DT_DESCONTO = '+QuotedStr(dataCompensa.Text)+' AND COD_LOTE_PAGAMENTO = '+codigoLoteDoCancelamento+' AND TAXAS_PROX_PAG_ID_FK <> 0');

                DMConexao.AdoQry.Open;
                DMConexao.AdoQry.First;
                While not DMConexao.AdoQry.Eof do
                begin
                  RollBackTaxaAvulsa(MDPagtoEstabcred_id.AsInteger,DMConexao.AdoQry.Fields[0].AsString,DMConexao.AdoQry.Fields[1].AsCurrency, DMConexao.AdoQry.Fields[2].AsDateTime, DMConexao.AdoQry.Fields[3].asString);
                  DMConexao.AdoQry.Next;
                end;
                DMConexao.ExecuteSql('DELETE from TAXAS_REPASSE WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' and dt_desconto = '+QuotedStr(dataCompensa.Text)+' and negociada = ''N'' and cod_lote_pagamento = '+codigoLoteDoCancelamento+'');
                // FIM DO ESTORNO DE TAXAS AVULSAS

                DMConexao.ExecuteSql('UPDATE pagamento_cred SET data_cancelamento = '+QuotedStr(DateToStr(Date))+', cancelado = ''S'' , operador_cancelamento = '+QuotedStr(Operador.Nome)+' ,cod_operacao_log = 1 WHERE cred_id = '+MDPagtoEstabcred_id.AsString+' AND data_compensacao = '+QuotedStr(dataCompensa.Text)+'');

                //PERSISTE DADOS DO CANCENLAMENTO NA TABELA LOG_FINANCEIRO
                SQL := TSqlMount.Create(smtInsert,'LOG_FINANCEIRO');
                SQL.ClearFields;
                SQL.AddField('LOG_ID',DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLog_Financeiro'),ftInteger);
                SQL.AddField('DATAHORA',Now, ftDateTime);
                SQL.AddField('OPERADOR',Operador.Nome, ftString);
                SQL.AddField('OPERACAO','CANCELAMENTO DE REPASSE', ftString);
                SQL.AddField('VALOR_ANT','PGTO_EFETUADO',ftString);
                SQL.AddField('VALOR_POS','PGTO_CANCELADO',ftString);
                SQL.AddField('CRED_ID',MDPagtoEstabcred_id.AsString,ftString);
                SQL.AddField('COD_OPERACAO','1',ftString);
                SQL.AddField('DATA_COMPENSACAO',StrToDateTime(dataCompensa.Text),ftDateTime);
                SQL.AddField('DATA_INI_AUTORIZACAO',StrToDateTime(dataIni.Text),ftDateTime);
                SQL.AddField('DATA_FIN_AUTORIZACAO',StrToDateTime(dataFin.Text),ftDateTime);
                DMConexao.Query1.SQL    := SQL.GetSqlString;
                DMConexao.Query1.SQL.Text;
                DMConexao.Query1.ExecSQL;
              end;
              MDPagtoEstab.Next;
            end;
            Screen.Cursor := crDefault;
            MsgInf('Cancelamento realizado com sucesso, A(s) autoriza��o(��es) est�(�o) dispon�vel(eis) para Pagamento.');
          end
          else
            begin
              Application.MessageBox('Senha inv�lida! Contate o Administrador do Sistema.','Aten��o',MB_ICONERROR);
            end;

       end;


    end;

  end;
end;

procedure TfrmRepassesCancelados.edTaxaBancoExit(Sender: TObject);
begin
//  inherited;
//  vnovo :=  edTaxaBanco.Text;
end;

procedure TfrmRepassesCancelados.dataCompensaEnter(Sender: TObject);
var str : String;
begin
  inherited;
  str:= DateToStr(dataCompensa.Date);
  //vvelho := TimeToStr
  vvelho := str;
end;

procedure TfrmRepassesCancelados.dataCompensaExit(Sender: TObject);
var str : String;
begin
  inherited;
  str:= DateToStr(dataCompensa.Date);
  vnovo := str;
end;

procedure TfrmRepassesCancelados.ConferirNotas1Click(Sender: TObject);
Var FormConfNotas : TF_EntregaNF;
begin
  inherited;
  FormConfNotas := TF_EntregaNF.Create(Self);
  FormConfNotas.dataini.Date := dataIni.Date;
  FormConfNotas.datafin.Date := DataFin.Date;
  FormConfNotas.Show;
  FormConfNotas.BringToFront;

end;

procedure TfrmRepassesCancelados.LanarTaxas1Click(Sender: TObject);
Var FormCredTaxas : TFCadCred;
    objMenu : TFMenu;
begin
  inherited;

  if not FMenu.vCadEstab then
  begin
    FormCredTaxas := TFCadCred.Create(Self);
    FormCredTaxas.EdCod.Text := IntToStr(grdPgtoEstab.Columns[0].Field.AsInteger);
    FormCredTaxas.Incluir := True;
    FormCredTaxas.Alterar := True;
    FormCredTaxas.Excluir := True;

    FormCredTaxas.Show;
    FormCredTaxas.BringToFront;
  end
  else
  begin
    MsgInf('A tela de "Cadastro de Estabelecimentos" j� est� aberta.');
  end;


end;

procedure TfrmRepassesCancelados.Image13Click(Sender: TObject);
begin
  inherited;
  ConferirNotas1.Click;

end;

procedure TfrmRepassesCancelados.Image4Click(Sender: TObject);
begin
  inherited;
  LanarTaxas1.Click;

end;

procedure TfrmRepassesCancelados.GridLogDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if not QPgtoEmpr.IsEmpty then
  begin
    if QPgtoEmprCOD_OPERACAO_LOG.AsInteger = 1 then
    begin
      GridLog.Canvas.Font.Color := clRed; //Se estiver bloqueado fonte vermelha.
      if gdSelected in State then
      begin
        GridLog.Canvas.Brush.Color:= $00BFFFFF; //Se estiver selecionado fundo amarelo.
      end;
      GridLog.DefaultDrawColumnCell(Rect,DataCol,Column,State);
    end;
    if QPgtoEmprCOD_OPERACAO_LOG.AsInteger = 4 then
    begin
      GridLog.Canvas.Font.Color := clBlue; //Se estiver bloqueado fonte vermelha.
      if gdSelected in State then
      begin
        GridLog.Canvas.Brush.Color:= $00BFFFFF; //Se estiver selecionado fundo amarelo.
      end;
      GridLog.DefaultDrawColumnCell(Rect,DataCol,Column,State);
    end;

  end;
end;

procedure TfrmRepassesCancelados.rdgPagamentosClick(Sender: TObject);
begin
//  inherited;
//  BtnEfetuar.Enabled := rdgPagamentos.ItemIndex <> 2;
//  BtnCancelarPag.Enabled := rdgPagamentos.ItemIndex = 2;
end;

function TfrmRepassesCancelados.ValidaValoresPagamento(cred_id : Integer; vl_bruto : Currency): Boolean;
var valorBrutoDoPgto : Currency;
begin
    //Confirma valor bruto financeiro com o valor bruto ContaCorrente(confer�ncia de nota).
    valorBrutoDoPgto := CalculaBrutoContaCorrenteByCredId(cred_id);
    result := valorBrutoDoPgto = vl_bruto;
end;

procedure TfrmRepassesCancelados.SalvarArquivo(Linha : String; NomeArq : String; Termino : Boolean; Formato : String);
begin
  if termino = false then
    SList.Add(Linha);

  if Termino = true then
  begin
    if SList.Count > 0 then begin
      if UpperCase(ExtractFileExt('C:\Users\sidnei.BELLA-RPC\Desktop')) <>  Formato then begin
        SList.SaveToFile('C:\Users\sidnei.BELLA-RPC\Desktop' + '\' + NomeArq + formato);
        AbrirArquivo(ExtractFilePath('C:\Users\sidnei.BELLA-RPC\Desktop'),ExtractFileName('C:\Users\sidnei.BELLA-RPC\Desktop')+ formato,fmOpenRead);
      end else begin
        SList.SaveToFile('C:\Users\sidnei.BELLA-RPC\Desktop');
        AbrirArquivo(ExtractFilePath('C:\Users\sidnei.BELLA-RPC\Desktop'),ExtractFileName('C:\Users\sidnei.BELLA-RPC\Desktop'),fmOpenRead);
      end;
    end;
  end;
end;

//Este m�todo atribui valores �s propriedades da objeto sacado.
function TfrmRepassesCancelados.SetUpSacado(NomeSacado: String; Logradouro: String; NumeroRes: Integer;
            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) : TSacado;
begin
  Sacado := TSacado.Create();
  Sacado.EnderecoEmpresa := Logradouro;
  Sacado.Numero          := NumeroRes;
  Sacado.Cidade          := Cidade;
  Sacado.Estado          := Estado;
  Sacado.CEP             := CEP;
  Sacado.Agencia         := Agencia;
  Sacado.Conta           := Conta;
  Sacado.CPFCNPJ         := CPFCNPJ;
  Sacado.Nome            := NomeSacado;
  Sacado.CodBanco        := '341';
  result := Sacado;
end;

function TfrmRepassesCancelados.SetUpFavorecido(CodigoTranmissao, Logradouro, Bairro, NumeroRes, CEP,
    Cidade, CodigoFavorecido, ComplementoRes, Telefone, NomeFavorecido,
    Agencia, AgenciaDigito, Conta, ContaDigito, Modalidade, Convenio, CNPJCPF,
    UF, CodBanco: string): TCedente;
begin
  Favorecido := TCedente.Create();
  Favorecido.CodigoTransmissao              := CodigoTranmissao;
  Favorecido.Logradouro                     := Logradouro;
  Favorecido.Bairro                         := Bairro;
  Favorecido.NumeroRes                      := NumeroRes;
  Favorecido.CEP                            := CEP;
  Favorecido.Cidade                         := Cidade;
  Favorecido.CodigoCedente                  := CodigoFavorecido;
  Favorecido.Complemento                    := ComplementoRes;
  Favorecido.Telefone                       := Telefone;
  Favorecido.NomeCedente                    := NomeFavorecido;
  Favorecido.Agencia                        := Agencia;
  Favorecido.AgenciaDigito                  := AgenciaDigito;
  Favorecido.Conta                          := Conta;
  Favorecido.ContaDigito                    := ContaDigito;
  Favorecido.Modalidade                     := Modalidade;
  Favorecido.Convenio                       := Convenio;
  Favorecido.CNPJCPF                        := CNPJCPF;
  Favorecido.UF                             := UF;
  Favorecido.CodBanco                       := CodBanco;
  Result := Favorecido;
end;



//function TfrmPgtoEstabAberto.SetUpCedente(NomeFavorecido: String; Logradouro: String; NumeroRes: Integer;
//            Cidade: String; Estado: String; CEP: String; Agencia: String; Conta: String; CPFCNPJ : String ) TCedente;
//begin
//
//end;

//Este M�todo Destr�i o Objeto TSacado.Sacado e Apaga o Ponteiro da mem�ria.
procedure TfrmRepassesCancelados.TearDownSacado();
begin
  Sacado.Free;
  Sacado := Nil;
  //Sacado.Destroy;
end;

function TfrmRepassesCancelados.GetIdGrupoUsuario(idUsuario : Integer) : Integer;
begin
  result := DMConexao.ExecuteQuery('select grupo_usu_id from usuarios where usuario_id = '+IntToStr(idUsuario)+'');
end;

procedure TfrmRepassesCancelados.FormShow(Sender: TObject);
var IdGrupoUsuario : Integer;
begin
  inherited;
  IdGrupoUsuario := GetIdGrupoUsuario(Operador.ID);
  //Verifica se o usu�rio � administrador ou Diretoria (7) � o valor fixo para o grupo Diretoria
  //Diretoria Perfil do Sr. Fernando
  if (Operador.IsAdmin) or (IdGrupoUsuario = 7) then
  begin
    //nCancelarPag.Visible := True;
  end;
  //Estab.ItemIndex := -1;
end;

procedure TfrmRepassesCancelados.BitBtnConsultarClick(Sender: TObject);
begin
  inherited;
  ZerarLabels;
  LimparTela;
  Screen.Cursor := crHourGlass;
  AbrirPagamentos;
  //CarregarDadosDetalheDoRepasse;
  Screen.Cursor := crDefault;
end;

procedure TfrmRepassesCancelados.LimparTela();
var i : Integer;
begin
//  for i := frmRepassesCancelados.ComponentCount -1 downto 0 do
//  begin
//    if (frmRepassesCancelados.Components[i] is TEdit) then
//       (frmRepassesCancelados.Components[i] as TEdit).text := '';
//  end;
  dataCompensa.Text := '';
  dataIni.Text := '';
  dataFin.Text := '';
  txtFormaPgtoDoRepasse.Caption := '';
  txtDataDoPagamento.Caption := '';
  ckbMercado.Checked := False;
  ckbPosto.Checked := False;
end;
procedure TfrmRepassesCancelados.txtCodLoteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if (key in [#13]) then
    BitBtnConsultar.Click;
end;

procedure TfrmRepassesCancelados.ckbMercadoClick(Sender: TObject);
begin
  inherited;
  if ckbMercado.Checked then
    ckbPosto.Checked := False;
end;

procedure TfrmRepassesCancelados.ckbPostoClick(Sender: TObject);
begin
  inherited;
  if ckbPosto.Checked then
    ckbMercado.Checked := False;
end;

procedure TfrmRepassesCancelados.grdPgtoEstabDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  Canvas.Brush.Color := clRed;
end;

end.
