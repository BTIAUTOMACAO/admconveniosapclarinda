object FDigitaSenha: TFDigitaSenha
  Left = 477
  Top = 357
  Width = 206
  Height = 161
  Caption = 'Nova Senha'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 18
    Top = 17
    Width = 165
    Height = 20
    Caption = 'Digite a nova senha!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edSenha: TEdit
    Left = 16
    Top = 56
    Width = 169
    Height = 21
    TabOrder = 0
  end
  object btnOk: TBitBtn
    Left = 64
    Top = 96
    Width = 80
    Height = 25
    TabOrder = 1
    OnClick = btnOkClick
    Kind = bkOK
  end
end
