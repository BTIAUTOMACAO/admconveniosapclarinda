object FColunas: TFColunas
  Left = 378
  Top = 170
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Configura'#231#227'o da Grade'
  ClientHeight = 333
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 264
    Width = 315
    Height = 69
    Align = alBottom
    TabOrder = 0
    object ButAcima: TButton
      Left = 10
      Top = 8
      Width = 70
      Height = 25
      Caption = 'A&cima'
      TabOrder = 0
      OnClick = ButAcimaClick
    end
    object ButAbaixo: TButton
      Left = 80
      Top = 8
      Width = 70
      Height = 25
      Caption = 'A&baixo'
      TabOrder = 1
      OnClick = ButAbaixoClick
    end
    object ButOk: TButton
      Left = 169
      Top = 8
      Width = 70
      Height = 25
      Caption = '&Ok'
      TabOrder = 2
      OnClick = ButOkClick
    end
    object ButCancel: TButton
      Left = 239
      Top = 8
      Width = 70
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 3
      OnClick = ButCancelClick
    end
    object Button1: TButton
      Left = 59
      Top = 37
      Width = 100
      Height = 25
      Caption = '&Marca Todos'
      TabOrder = 4
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 159
      Top = 37
      Width = 100
      Height = 25
      Caption = '&Desmarca Todos'
      TabOrder = 5
      OnClick = Button2Click
    end
  end
  object CheckList: TCheckListBox
    Left = 0
    Top = 0
    Width = 315
    Height = 264
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
end
