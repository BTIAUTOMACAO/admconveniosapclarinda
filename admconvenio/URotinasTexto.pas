unit URotinasTexto;

interface

uses Windows, SysUtils, Classes, Forms, ShellApi, ComCtrls,
  Graphics, Controls, JvRichEdit;


type TCaracteres = (crNUM, CrLetrasMai, CrLetrasMin, CrLetra,CrParaNomeArq);
type TDirecaoLetra = (dlDireita, dlEsquerda);


//function 
function tamanhoPalavraEntreCaracters(caracter, palavra : String) : Integer;
function iif(Condicao : Boolean; seVerdade, seMentira : String) : String;
function LastPos(SubStr, S: string): Integer;
function getExtremesElementsXML(pXML, pTag : String) : String;
//function fnRemoveCaracters(Caracters , Valor : String) : String;
function getElementXML(pXML, pTag : String) : String;
procedure SubstCarct(var SBusca : String ; SSubst : String ; Pos : Integer);
procedure AbrirArquivo(Caminho, NomeArquivo : String ; Mode : Cardinal);
procedure SalvaLogErro(var SL : TStringList ; Caminho, NomeArquivo : String);
procedure AddCabecalhoErro(DataHora : String ; var Arquivo : TStringList);
procedure CarregaLogErros(var SL : TStringList ; Caminho , NomeArquivo : String);
function fnRemoveCasasDecimais(Value : String) : String;
function IsDate(S: string): Boolean;
function IsTime(S: string): Boolean;
function IsInteger(S: String): Boolean;
function IsFloat(S: String):Boolean;
function ArquivoExiste(Caminho, NomeArquivo : String) : Boolean;
function Count(ValorBusca, Valor : String) : Integer;
function fnMascarar(Valor, Mascara : String) : String;
function fnAddCaracter(Caracters, Valor : String; const Posicao : Integer; const Direita : Boolean = False ) : String;
function fnRemoveCaractersQueNaoSejam(S : String ; QueNaoSejam : TCaracteres) : String;
function fnRemoveCaractersEsquerda(Carcacters , Valor : String) : String;
function fnRemoveCaractersDireita(Caracters , Valor : String) : String;
function fnRemoveCaracters(Caracters , Valor : String) : String;
function fnInverter(Valor : String) : String;
function fnSubstituiString(ProcurarPor, SubstituirPor, NoValor : String) : String;
function fnCompletarCom(Valor : String ; TamanhoMaximo : Integer ; Caracter : String = '0'; Direita : Boolean = False) : String;
procedure split(var sl : TStrings; frase : String ; caracter : String = ' ');
procedure mudarEstiloFonteRichText(var re : TRichEdit; textoSelecionado: String; FontStyle : TFontStyles; Cor : TColor = clBlack; Size : Integer = 8);
function fnRemoverCaracterParaSalvarEmArq(s : string) : String;
function fnRemoverCaracterSQLInjection(s : string) : String;

function RemoveAcentos(sEntrada: string): string;

implementation

uses ComObj;


function tamanhoPalavraEntreCaracters(caracter, palavra : String) : Integer;
var p1,p2 : integer;
begin
  p1 := Pos(caracter, palavra);
  Result := 0;
  if p1 > 0 then begin
    p2 := Pos(caracter,Copy(palavra,p1+1,length(palavra)-p1));
    Result := p1 - p2;
  end else
    Result := 0;
end;

function iif(Condicao : Boolean; seVerdade, seMentira : String) : String;
begin
  if (Condicao) then
    Result := seVerdade
  else
    Result := seMentira;
end;


function LastPos(SubStr, S: string): Integer;
var
  Found, Len, Pos: integer;
begin
  Pos := Length(S);
  Len := Length(SubStr);
  Found := 0;
  while (Pos > 0) and (Found = 0) do
  begin
    if Copy(S, Pos, Len) = SubStr then
      Found := Pos;
    Dec(Pos); 
  end; 
  Result := Found;
end;

function getExtremesElementsXML(pXML, pTag : String) : String;
var pTagFecha, retorno : String;
  pIni,pfin : Integer;
begin
  pTag := '<'+pTag+'>';
  Result := '';
  pTagFecha := '</'+Copy(pTag,2,Length(pTag)-1);
  pIni := Pos(pTag,pXML);
  pfin := LastPos(pTagFecha,pXML) + Length(pTagFecha);
  retorno := Copy(pXML, pIni,pfin-pIni);
  Result := retorno;
end;


function getElementXML(pXML, pTag : String) : String;
var pTagFecha, retorno : String;
  pIni : Integer;
begin
  pTag := '<'+pTag+'>';
  Result := '';
  pTagFecha := '</'+Copy(pTag,2,Length(pTag)-1);
  pIni := Pos(pTag,pXML)+Length(pTag);
  retorno := Copy(pXML, pIni,Pos(pTagFecha,pXML)-pIni);
  Result := retorno;
end;

procedure AbrirArquivo(Caminho, NomeArquivo : String ; Mode : Cardinal);
begin
  if Caminho[Length(Caminho)] <> '\' then
    Caminho := Caminho + '\';
  ShellExecute(Screen.ActiveForm.Handle, nil, Pchar(Caminho + NomeArquivo), nil, nil, SW_SHOWNORMAL);
end;

procedure SalvaLogErro(var SL : TStringList ; Caminho, NomeArquivo : String);
begin
if Caminho[Length(Caminho)] <> '\' then
  Caminho := Caminho + '\';
SL.SaveToFile(Caminho + NomeArquivo);
end;

procedure CarregaLogErros(var SL : TStringList ; Caminho , NomeArquivo : String);
begin
if Caminho[Length(Caminho)] <> '\' then
  Caminho := Caminho + '\';
if FileExists(Caminho + NomeArquivo) then
  SL.LoadFromFile(Caminho + NomeArquivo);
end;

function fnRemoveCasasDecimais(Value : String) : String;
var s : String;
begin
  s := StringReplace(Value,',','',[rfIgnoreCase,rfReplaceAll]);
  s := StringReplace(s,'.','',[rfIgnoreCase,rfReplaceAll]);
  Result := s;
end;

procedure AddCabecalhoErro(DataHora : String ; var Arquivo : TStringList);
begin
//Adiciono na parte de cima o cabe�alho e logo em seguida o Arquivo
if Pos(DataHora, Arquivo.Text) <= 0 then
  Arquivo.Text := '----------------'+#13+'---'+DataHora+'---'+#13+'----------------' + #13+ Arquivo.Text;
end;


function IsInteger(S: String): Boolean;
begin
  try
    Result := True;
    StrToInt(S);
  except on E: EConvertError do
    Result := False;
  end;
end;

function fnRemoveCaracters(Caracters , Valor : String) : String;
begin
  while Pos(Caracters,Valor) > 0 do
    Delete(Valor,Pos(Caracters,Valor),Length(Caracters));
  Result := Valor;
end;

function IsFloat(S: String):Boolean;
begin
  try
    StrToFloat(S);
    Result := True;
  except
    Result := False;
  end;
end;

function IsDate(S: string): Boolean;
var
  dt: TDateTime;
begin
  Result := True;
  try
    dt := StrToDate(S);
  except
    Result := False;
  end;
end;

function IsTime(S: string): Boolean;
var
  dt: TDateTime;
begin
  Result := True;
  try
    dt := StrToTime(S);
  except
    Result := False;
  end;
end;

function ArquivoExiste(Caminho, NomeArquivo : String) : Boolean;
begin
  if Caminho[Length(Caminho)] <> '\' then
    Caminho := Caminho + '\';
  Result := FileExists(Caminho+NomeArquivo);
end;

function Count(ValorBusca, Valor : String) : Integer;
begin
Result := 0;
while Pos(ValorBusca,Valor) > 0 do
  begin
  Delete(Valor,Pos(ValorBusca,Valor),Length(ValorBusca));
  Result := Result + 1;
  end;
end;

function fnMascarar(Valor, Mascara : String) : String;
var I,C : Integer;
begin
  C := 1;
  Result := '';
  //while Length(Valor) > Count('#',Mascara) do
  //  Mascara := Mascara + '#';
  for I := 1 to Length(Mascara)  do begin
    if Mascara[I] = '#' then begin
      Result := Result + Valor[C];
      C := C + 1;
    end else
      Result := Result + Mascara[I]
  end;
end;

function fnAddCaracter(Caracters, Valor : String; const Posicao : Integer; const Direita : Boolean = False) : String;
begin
  if not Direita then
    Insert(Caracters, Valor, Posicao)
  else
    Insert(Caracters, Valor, Length(Valor)+Posicao);
  Result := Valor;
end;

function fnRemoveCaractersQueNaoSejam(S : String ; QueNaoSejam : TCaracteres ) : String;
const NUMEROS_TXT       : set of '0'..'9' = ['0'..'9'];
const LETRAS_MAIUSCULAS : set of 'A'..'Z' = ['A'..'Z'];
const LETRAS_MINUSCULAS : set of 'a'..'z' = ['a'..'z'];
const LETRAS            : set of 'A'..'z' = ['A'..'z'];
var I : Integer;
begin
  case QueNaoSejam of
  crNUM:
    for I := 1 to Length(S) do
      if not (S[I] in NUMEROS_TXT) then Delete(S,Pos(S[I],S),1);
  crLetrasMai:
    for I := 1 to Length(S) do
      if not (S[I] in LETRAS_MAIUSCULAS) then Delete(S,Pos(S[I],S),1);
  crLetrasMin:
    for I := 1 to Length(S) do
      if not (S[I] in LETRAS_MINUSCULAS) then Delete(S,Pos(S[I],S),1);
  crLetra:
    for I := 1 to Length(S) do
      if not (S[I] in LETRAS) then Delete(S,Pos(S[I],S),1);
  end;
  Result := S;
end;

function fnRemoveCaractersEsquerda(Carcacters , Valor : String) : String;
begin
  while Copy(Valor,1,Length(Carcacters)) = Carcacters do
    Delete(Valor,1,Length(Carcacters));
  Result := Valor;
end;

function fnRemoveCaractersDireita(Caracters , Valor : String) : String;
begin
  while Copy(Valor,Length(Valor),Length(Caracters)) = Caracters do
    Delete(Valor,Length(Valor),Length(Caracters));
  Result := Valor;
end;

function fnInverter(Valor : String) : String;
var C : Integer;
begin
  C := Length(Valor);
  while C >= 1 do
    begin
    Result := Result + Valor[C];
    C := C - 1;
    end;
end;

procedure SubstCarct(var SBusca : String ; SSubst : String ; Pos : Integer);
begin
  if Pos > Length(SBusca) then
    exit
  else if Pos = Length(SBusca) then
    SBusca := Copy(SBusca, 1 ,Pos-1) + SSubst
  else
    SBusca := Copy(SBusca, 1 ,Pos-1) + SSubst + Copy(SBusca,Pos+1,Length(SBusca));
end;

function fnSubstituiString(ProcurarPor, SubstituirPor, NoValor : String) : String;
begin
  while Pos(ProcurarPor,NoValor) > 0 do begin
    Insert(SubstituirPor,NoValor,Pos(ProcurarPor,NoValor));
    Delete(NoValor,Pos(ProcurarPor,NoValor),1);
  end;
  Result := NoValor;
end;

///<summary>A fun��o fnCompletarCom funciona como um montador de string</summary>
///<params Valor=""></params>
function fnCompletarCom(Valor : String ; TamanhoMaximo : Integer ; Caracter : String; Direita : Boolean) : String;
begin
  if Length(Valor) >= TamanhoMaximo then
    Result := Copy(Valor,1,TamanhoMaximo)
  else begin       while Length(Valor) < TamanhoMaximo do
      Valor := fnAddCaracter(Caracter,Valor,1,Direita);
    Result := Valor;
  end;
end;

procedure split(var SL : TStrings; frase : String ; caracter : String);
var pCaracter : Integer;
begin
  pCaracter := SL.Count;
  while(Pos(caracter,frase) > 0) do begin
    pCaracter := Pos(caracter,frase)-1; //-1 pq eu n quero pegar o caracter delimitador
    SL.Add(Copy(frase,1,pCaracter));
    Delete(frase,1,pCaracter+1); //+1 pq eu quero deletar o caracter delimitador
  end;
  SL.Add(frase);
end;

procedure mudarEstiloFonteRichText(var re : TRichEdit; textoSelecionado: String; FontStyle : TFontStyles; Cor : TColor; Size : Integer);
var pi : Integer;
begin
  pi := Pos(textoSelecionado, re.Text);
  if pi > 0 then begin
    re.SelStart := pi -1;
    re.SelLength := Length(textoSelecionado) -1;
    re.SelAttributes.Size := Size;
    re.SelAttributes.Color := Cor;
    re.SelAttributes.Style := FontStyle;
    //re.SelText := textoSelecionado;
  end;
end;

function fnRemoverCaracterParaSalvarEmArq(s : string) : String;
begin
  s := StringReplace(s, '/','',[rfReplaceAll]);
  s := StringReplace(s, '\','',[rfReplaceAll]);
  s := StringReplace(s, ':','',[rfReplaceAll]);
  s := StringReplace(s, '*','',[rfReplaceAll]);
  s := StringReplace(s, '?','',[rfReplaceAll]);
  s := StringReplace(s, '"','',[rfReplaceAll]);
  s := StringReplace(s, '<','',[rfReplaceAll]);
  s := StringReplace(s, '>','',[rfReplaceAll]);
  s := StringReplace(s, '|','',[rfReplaceAll]);
  Result := s;
end;

function fnRemoverCaracterSQLInjection(s : string) : String;
begin
  s := StringReplace(s, '''', '',[rfReplaceAll]);
  s := StringReplace(s, '"', '',[rfReplaceAll]);
  s := StringReplace(s, ';', '',[rfReplaceAll]);
  Result := s;
end;

function RemoveAcentos(sEntrada: string): string;
Const ComAcento = '����������������������������������������������';
      SemAcento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
      Var I: Integer;
begin
  For I := 1 To Length(sENTRADA) Do
  If Pos(sENTRADA[I], ComAcento) <> 0 Then
  sENTRADA[I] := SemAcento[Pos(sENTRADA[I], ComAcento)];
  Result := sENTRADA;
end;



end.

