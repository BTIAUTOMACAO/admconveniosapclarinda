inherited FCadAluno: TFCadAluno
  Left = 359
  Top = 131
  Caption = 'Cadastro de Aluno - Cantinex'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = TabGrade
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
      end
      inherited Barra: TStatusBar
        Panels = <
          item
            Width = 50
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel3: TPanel
        object PageControl2: TPageControl
          Left = 2
          Top = 2
          Width = 818
          Height = 455
          ActivePage = TabSheet1
          Align = alClient
          Style = tsFlatButtons
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = '&Dados do Conveniado'
            object GroupBox4: TGroupBox
              Left = 0
              Top = 257
              Width = 810
              Height = 97
              Align = alTop
              Caption = 'Outras informa'#231#245'es'
              TabOrder = 1
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 0
              Width = 810
              Height = 257
              Align = alTop
              Caption = 'Principal'
              TabOrder = 0
              object Label3: TLabel
                Left = 10
                Top = 15
                Width = 39
                Height = 13
                Caption = 'Conv ID'
                FocusControl = DBEdit1
              end
              object Label4: TLabel
                Left = 81
                Top = 15
                Width = 37
                Height = 13
                Caption = 'Titular'
                FocusControl = dbEdit2
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label5: TLabel
                Left = 527
                Top = 15
                Width = 37
                Height = 13
                Caption = 'Chapa'
                FocusControl = dbEdtChapa
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label30: TLabel
                Left = 98
                Top = 95
                Width = 49
                Height = 13
                Caption = 'Empresa'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label10: TLabel
                Left = 616
                Top = 55
                Width = 27
                Height = 13
                Caption = 'Bairro'
              end
              object Label9: TLabel
                Left = 134
                Top = 55
                Width = 54
                Height = 13
                Caption = 'Logradouro'
                FocusControl = DBEdit7
              end
              object Label11: TLabel
                Left = 461
                Top = 55
                Width = 33
                Height = 13
                Caption = 'Cidade'
              end
              object Label13: TLabel
                Left = 7
                Top = 55
                Width = 30
                Height = 13
                Caption = 'C.E.P.'
              end
              object Label12: TLabel
                Left = 410
                Top = 55
                Width = 14
                Height = 13
                Caption = 'UF'
              end
              object Label14: TLabel
                Left = 448
                Top = 135
                Width = 20
                Height = 13
                Caption = 'CPF'
                FocusControl = DBEdit12
              end
              object Label15: TLabel
                Left = 583
                Top = 135
                Width = 16
                Height = 13
                Caption = 'RG'
                FocusControl = DBEdit13
              end
              object SpeedButton1: TSpeedButton
                Left = 104
                Top = 70
                Width = 22
                Height = 21
                Flat = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFD9B28CCF9D6CD4A577D9AD83DEB68FE2BD9932
                  65984C7FB24C7FB24C7FB24C7FB2326598CF9D6CD9B28CFFFFFFFFFFFFD6A26F
                  FFEEDDFFFFFFFFEEDDFFFFFFFFEEDD376A9D6598CC6598CC6598CC6598CC376A
                  9DFFEEDDD6A26FFFFFFFFFFFFFE5B17FFFEFDF988776FFEFDF988776FFEFDF3E
                  71A474A4D474A4D474A4D474A4D43E71A4FFEFDFE5B17FFFFFFFFFFFFFF5C18E
                  FFF0E2CCBAA9FFF0E2CCBAA9FFF0E2477AAD6598CC0032658BB5DF8BB5DF477A
                  ADFFF0E2F5C18EFFFFFFFFFFFFFFCC98FFF2E5FFFFFFFFF2E5FFFFFFFFF2E550
                  83B6A5C8ECA5C8ECA5C8ECA5C8EC5083B6FFF2E5FFCC98FFFFFF7FB2E5CC9865
                  FFF4E8988776FFF4E8988776FFF4E8749DC7A3C5E9BBD9F7BBD9F7A3C5E9749D
                  C7FFF4E8CC98657FB2E50065CC0065CCCCBAA9CCBAA9FFF7F0CCBAA9FFF7F0AF
                  C4DB84AEDACCE5FFCCE5FF84AEDAAFC4DBCCBAA90065CC0065CC0069D00098FF
                  0065CCCCBAA9FFF9F4FFF9F4FFF9F4EFEFF0B1C9E06598CC6598CCB1C9E0C1B7
                  AC0065CC0098FF0069D07FB7EA006FD60C9EFF0065CCCCBAA9FFFBF7FFFBF7FF
                  FBF7FFFBF7FFFBF7FFFBF7CCBAA90065CC0C9EFF006FD67FB7EAFFFFFF7FBBEE
                  0077DE1DA7FF0065CCCCBAA9FFFDFAFFFDFAFFFDFAFFFDFACCBAA90065CC1DA7
                  FF0077DE7FBBEEFFFFFFFFFFFFFFFFFF326598007FE532B1FF0065CCCCBAA9FF
                  FEFDFFFEFDCCBAA90065CC32B1FF007FE57FBFF2FFFFFFFFFFFFFFFFFFFFFFFF
                  3C6FA26598CC0086ED47BCFF0065CCCCBAA9CCBAA90065CC47BCFF0086ED7FC2
                  F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4C7FB198BEE64C7FB1008EF559C5FF00
                  65CC0065CC59C5FF008EF57FC6FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  5B8EC1CCE5FF5B8EC17FC9FD0094FB65CCFF65CCFF0094FB7FC9FDFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6598CC6598CC6598CCFFFFFF7FCBFF00
                  98FF0098FF7FCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              object Label93: TLabel
                Left = 364
                Top = 55
                Width = 12
                Height = 13
                Caption = 'N'#186
                FocusControl = DBEdit10
              end
              object Label39: TLabel
                Left = 10
                Top = 95
                Width = 66
                Height = 13
                Caption = 'Cod. Empresa'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object lblAddBairro: TLabel
                Left = 918
                Top = 57
                Width = 19
                Height = 13
                Caption = 'Add'
              end
              object Label6: TLabel
                Left = 9
                Top = 135
                Width = 73
                Height = 13
                Caption = 'Nome do Pai'
                FocusControl = DBEdit3
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label7: TLabel
                Left = 9
                Top = 177
                Width = 34
                Height = 13
                Caption = 'Limite'
                FocusControl = DBEdit4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label8: TLabel
                Left = 186
                Top = 175
                Width = 69
                Height = 13
                Caption = 'S'#233'rie do Aluno'
              end
              object Label17: TLabel
                Left = 269
                Top = 175
                Width = 84
                Height = 13
                Caption = 'Modelo do Cartao'
              end
              object Label29: TLabel
                Left = 97
                Top = 177
                Width = 81
                Height = 13
                Caption = 'Taxa Recarga'
                FocusControl = DBEdit4
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBEdit1: TDBEdit
                Left = 10
                Top = 30
                Width = 65
                Height = 21
                Hint = 'C'#243'digo do conveniado'
                TabStop = False
                Color = clBtnFace
                DataField = 'CONV_ID'
                DataSource = DSCadastro
                ReadOnly = True
                TabOrder = 0
              end
              object dbEdit2: TDBEdit
                Left = 81
                Top = 30
                Width = 440
                Height = 21
                Hint = 'Nome do titular'
                CharCase = ecUpperCase
                DataField = 'TITULAR'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object dbEdtChapa: TDBEdit
                Left = 527
                Top = 30
                Width = 126
                Height = 21
                Hint = 'Chapa/Matricula do titular na empresa'
                DataField = 'CHAPA'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBEmpresa: TJvDBLookupCombo
                Left = 96
                Top = 110
                Width = 338
                Height = 21
                Hint = 'Nome da empresa'
                DisplayAllFields = True
                DataField = 'EMPRES_ID'
                DataSource = DSCadastro
                EmptyValue = '0'
                LookupField = 'EMPRES_ID'
                LookupDisplay = 'nome'
                LookupDisplayIndex = -1
                TabOrder = 11
              end
              object DBEdit7: TDBEdit
                Left = 134
                Top = 70
                Width = 227
                Height = 21
                Hint = 'Endere'#231'o do titular'
                CharCase = ecUpperCase
                DataField = 'ENDERECO'
                DataSource = DSCadastro
                TabOrder = 4
              end
              object DBEdit11: TDBEdit
                Left = 7
                Top = 70
                Width = 96
                Height = 21
                Hint = 'Cep'
                DataField = 'CEP'
                DataSource = DSCadastro
                TabOrder = 3
              end
              object DBEdit12: TDBEdit
                Left = 448
                Top = 150
                Width = 129
                Height = 21
                Hint = 'CPF'
                DataField = 'CPF_RESPONSAVEL'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 13
              end
              object DBEdit13: TDBEdit
                Left = 583
                Top = 150
                Width = 97
                Height = 21
                Hint = 'Registro Geral'
                DataField = 'RG_RESPONSAVEL'
                DataSource = DSCadastro
                TabOrder = 14
              end
              object DBEdit10: TDBEdit
                Left = 360
                Top = 70
                Width = 42
                Height = 21
                DataField = 'NUMERO'
                DataSource = DSCadastro
                TabOrder = 5
              end
              object dbLkpCidades: TDBLookupComboBox
                Left = 456
                Top = 70
                Width = 161
                Height = 21
                DataField = 'CIDADE'
                DataSource = DSCadastro
                KeyField = 'CID_ID'
                ListField = 'NOME'
                TabOrder = 7
              end
              object dbLkpEstados: TDBLookupComboBox
                Left = 408
                Top = 70
                Width = 45
                Height = 21
                DataField = 'ESTADO'
                DataSource = DSCadastro
                KeyField = 'ESTADO_ID'
                ListField = 'UF'
                TabOrder = 6
              end
              object edtEmpr: TEdit
                Left = 10
                Top = 110
                Width = 79
                Height = 21
                TabOrder = 10
                OnChange = edtEmprChange
                OnKeyPress = edtEmprKeyPress
              end
              object dbLkpBairros: TDBLookupComboBox
                Left = 621
                Top = 70
                Width = 300
                Height = 21
                DataField = 'BAIRRO'
                DataSource = DSCadastro
                KeyField = 'BAIRRO_ID'
                ListField = 'DESCRICAO'
                NullValueKey = 46
                TabOrder = 8
              end
              object btnAdicionaBairro: TBitBtn
                Left = 920
                Top = 72
                Width = 17
                Height = 17
                Hint = 'Adiciona Bairro|Bot'#227'o que adiciona um novo bairro '#224' listagem.'
                Enabled = False
                TabOrder = 9
                Glyph.Data = {
                  5A030000424D5A030000000000005A0100002800000020000000100000000100
                  08000000000000020000232E0000232E00004900000000000000117611001379
                  1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
                  37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
                  4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
                  6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
                  7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
                  840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
                  C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
                  D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
                  DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
                  4848484848484848484848484848484848484848484848484848484848484848
                  484848480C00000C484848484848484848484848302323304848484848484848
                  4848484801161601484848484848484848484848243A3A244848484848484848
                  4848484802181802484848484848484848484848253C3C254848484848484848
                  48484848031A1A03484848484848484848484848263D3D264848484848484848
                  48484848041B1B04484848484848484848484848273F3F274848484848484813
                  0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
                  1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
                  2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
                  0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
                  484848480D21210D484848484848484848484848324444324848484848484848
                  4848484810282810484848484848484848484848354545354848484848484848
                  4848484812292912484848484848484848484848374646374848484848484848
                  48484848152A2A15484848484848484848484848394747394848484848484848
                  484848481E16161E484848484848484848484848423A3A424848484848484848
                  484848484848484848484848484848484848484848484848484848484848}
                NumGlyphs = 2
              end
              object DBEdit3: TDBEdit
                Left = 9
                Top = 150
                Width = 424
                Height = 21
                Hint = 'Nome do titular'
                CharCase = ecUpperCase
                DataField = 'RESPONSAVEL'
                DataSource = DSCadastro
                TabOrder = 12
              end
              object DBEdit4: TDBEdit
                Left = 9
                Top = 194
                Width = 80
                Height = 21
                Hint = 'Limite do titular'
                DataField = 'LIMITE_MES'
                DataSource = DSCadastro
                TabOrder = 16
              end
              object DBCheckBox2: TDBCheckBox
                Left = 361
                Top = 193
                Width = 80
                Height = 17
                Hint = 'Titular liberado para compra'
                Caption = 'Liberado'
                DataField = 'LIBERADO'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 15
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBComboBox2: TDBComboBox
                Left = 186
                Top = 195
                Width = 71
                Height = 21
                Hint = 'S'#233'rie do Aluno na escola.'
                Style = csDropDownList
                CharCase = ecLowerCase
                DataField = 'SERIE'
                DataSource = DSCadastro
                ItemHeight = 13
                Items.Strings = (
                  '1o.'
                  '2o.'
                  '3o.'
                  '4o.'
                  '5o.'
                  '6o.'
                  '7o.'
                  '8o.'
                  '9o.')
                TabOrder = 18
              end
              object DBComboBox3: TDBComboBox
                Left = 266
                Top = 195
                Width = 87
                Height = 21
                Hint = 'Modelo do cart'#227'o que o aluno escolheu.'
                Style = csDropDownList
                CharCase = ecUpperCase
                DataField = 'MODELO_CARTAO'
                DataSource = DSCadastro
                ItemHeight = 13
                Items.Strings = (
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
                TabOrder = 19
              end
              object txtTaxaRecarga: TDBEdit
                Left = 97
                Top = 194
                Width = 80
                Height = 21
                Hint = 'Porcentagem aplicada no desconto da taxa de recarga'
                DataField = 'TAXA_RECARGA'
                DataSource = DSCadastro
                TabOrder = 17
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 364
              Width = 810
              Height = 60
              Align = alBottom
              Caption = 'Informa'#231#245'es Adicionais/Dados da '#250'ltima altera'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object Label27: TLabel
                Left = 8
                Top = 15
                Width = 141
                Height = 13
                Caption = 'Data da Altera'#231#227'o / Operador'
                FocusControl = DBEdit23
              end
              object Label28: TLabel
                Left = 242
                Top = 15
                Width = 138
                Height = 13
                Caption = 'Data do Cadastro / Operador'
                FocusControl = DBEdit24
              end
              object DBEdit23: TDBEdit
                Left = 8
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTALTERACAO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 0
              end
              object DBEdit24: TDBEdit
                Left = 242
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 2
              end
              object DBEdit25: TDBEdit
                Left = 351
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 3
              end
              object DBEdit60: TDBEdit
                Left = 118
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERADOR'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 1
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Dados &Adicionais'
            ImageIndex = 1
            object GroupBox1: TGroupBox
              Left = 0
              Top = 128
              Width = 810
              Height = 137
              Align = alTop
              Caption = 'Observa'#231#245'es'
              TabOrder = 2
              object Label25: TLabel
                Left = 10
                Top = 20
                Width = 28
                Height = 13
                Caption = 'OBS1'
              end
              object Label26: TLabel
                Left = 331
                Top = 20
                Width = 28
                Height = 13
                Caption = 'OBS2'
              end
              object DBMemo1: TDBMemo
                Left = 10
                Top = 33
                Width = 311
                Height = 89
                Hint = 'Observa'#231#227'o'
                DataField = 'OBS1'
                DataSource = DSCadastro
                ScrollBars = ssBoth
                TabOrder = 0
              end
              object DBMemo2: TDBMemo
                Left = 338
                Top = 33
                Width = 311
                Height = 89
                Hint = 'Observa'#231#227'o'
                DataField = 'OBS2'
                DataSource = DSCadastro
                ScrollBars = ssBoth
                TabOrder = 1
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 810
              Height = 64
              Align = alTop
              Caption = 'Dados Banc'#225'rios'
              TabOrder = 0
              object Label18: TLabel
                Left = 10
                Top = 15
                Width = 31
                Height = 13
                Caption = 'Banco'
              end
              object Label19: TLabel
                Left = 269
                Top = 15
                Width = 39
                Height = 13
                Caption = 'Ag'#234'ncia'
                FocusControl = DBEdit17
              end
              object Label20: TLabel
                Left = 345
                Top = 15
                Width = 71
                Height = 13
                Caption = 'Conta Corrente'
                FocusControl = DBEdit18
              end
              object Label73: TLabel
                Left = 535
                Top = 33
                Width = 3
                Height = 13
                Caption = '-'
              end
              object DBEdit17: TDBEdit
                Left = 269
                Top = 30
                Width = 69
                Height = 21
                Hint = 'Ag'#234'ncia'
                CharCase = ecUpperCase
                DataField = 'AGENCIA'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit18: TDBEdit
                Left = 345
                Top = 30
                Width = 185
                Height = 21
                Hint = 'Conta corrente'
                CharCase = ecUpperCase
                DataField = 'CONTACORRENTE'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBBanco: TJvDBLookupCombo
                Left = 10
                Top = 30
                Width = 253
                Height = 21
                Hint = 'Banco do titular'
                DataField = 'BANCO'
                DataSource = DSCadastro
                LookupField = 'codigo'
                LookupDisplay = 'banco'
                TabOrder = 0
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 64
              Width = 810
              Height = 64
              Align = alTop
              Caption = 'Telefone/Internet'
              TabOrder = 1
              object Label22: TLabel
                Left = 10
                Top = 15
                Width = 51
                Height = 13
                Caption = 'Telefone 1'
                FocusControl = DBEdit16
              end
              object Bevel4: TBevel
                Left = 127
                Top = 15
                Width = 2
                Height = 37
              end
              object DBEdit16: TDBEdit
                Left = 10
                Top = 30
                Width = 113
                Height = 21
                Hint = 'Telefone (1)'
                DataField = 'TELEFONE'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object dbEdtEmail: TDBEdit
                Left = 135
                Top = 30
                Width = 271
                Height = 21
                Hint = 'E-mail'
                CharCase = ecLowerCase
                DataField = 'EMAIL'
                DataSource = DSCadastro
                TabOrder = 1
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = '&CANTINEX'
            ImageIndex = 3
            object GroupBox12: TGroupBox
              Left = 0
              Top = 0
              Width = 810
              Height = 64
              Align = alTop
              Caption = 'Dados do Aluno'
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterRefresh = QCadastroAfterRefresh
    SQL.Strings = (
      'SELECT '
      'CONVENIADOS_CANTINEX.CONV_ID,'
      'CONVENIADOS_CANTINEX.EMPRES_ID,'
      'CONVENIADOS_CANTINEX.CHAPA,'
      'CONVENIADOS_CANTINEX.SENHA,'
      'CONVENIADOS_CANTINEX.TITULAR,'
      'CONVENIADOS_CANTINEX.LIMITE_MES,'
      'CONVENIADOS_CANTINEX.CONSUMO_DIARIO,'
      'CONVENIADOS_CANTINEX.CONSUMO_MES,'
      'CONVENIADOS_CANTINEX.LIBERADO,'
      'CONVENIADOS_CANTINEX.APAGADO,'
      'CONVENIADOS_CANTINEX.RESPONSAVEL,'
      'CONVENIADOS_CANTINEX.RG_RESPONSAVEL,'
      'CONVENIADOS_CANTINEX.CPF_RESPONSAVEL,'
      'CONVENIADOS_CANTINEX.ENDERECO,'
      'CONVENIADOS_CANTINEX.NUMERO,'
      'CONVENIADOS_CANTINEX.BAIRRO,'
      'CONVENIADOS_CANTINEX.CIDADE,'
      'CONVENIADOS_CANTINEX.ESTADO,'
      'CONVENIADOS_CANTINEX.CEP,'
      'CONVENIADOS_CANTINEX.TELEFONE,'
      'CONVENIADOS_CANTINEX.EMAIL,'
      'CONVENIADOS_CANTINEX.SERIE,'
      'CONVENIADOS_CANTINEX.MODELO_CARTAO,'
      'CONVENIADOS_CANTINEX.CONSULTA_LIMITE,'
      'CONVENIADOS_CANTINEX.DTCADASTRO,'
      'CONVENIADOS_CANTINEX.DTALTERACAO,'
      'CONVENIADOS_CANTINEX.OBS1,'
      'CONVENIADOS_CANTINEX.OBS2,'
      'CONVENIADOS_CANTINEX.OPERADOR,'
      'CONVENIADOS_CANTINEX.BANCO,'
      'CONVENIADOS_CANTINEX.AGENCIA,'
      'CONVENIADOS_CANTINEX.CONTACORRENTE,'
      'CONVENIADOS_CANTINEX.TAXA_RECARGA,'
      'CONVENIADOS_CANTINEX.OPERCADASTRO,'
      'EMPRESAS_CANTINEX.FANTASIA,'
      'EMPRESAS_CANTINEX.NOME EMPRESA'
      'FROM CONVENIADOS_CANTINEX'
      
        'JOIN EMPRESAS_CANTINEX ON EMPRESAS_CANTINEX.EMPRES_ID = CONVENIA' +
        'DOS_CANTINEX.EMPRES_ID AND EMPRESAS_CANTINEX.APAGADO <> '#39'S'#39
      'WHERE CONVENIADOS_CANTINEX.CONV_ID = 0')
    object QCadastroCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCadastroCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 6
      Size = 2
    end
    object QCadastroCONSUMO_DIARIO: TBCDField
      FieldName = 'CONSUMO_DIARIO'
      Precision = 6
      Size = 2
    end
    object QCadastroCONSUMO_MES: TBCDField
      FieldName = 'CONSUMO_MES'
      Precision = 6
      Size = 2
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Size = 58
    end
    object QCadastroTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QCadastroRG_RESPONSAVEL: TStringField
      FieldName = 'RG_RESPONSAVEL'
      Size = 13
    end
    object QCadastroCPF_RESPONSAVEL: TStringField
      FieldName = 'CPF_RESPONSAVEL'
      Size = 14
    end
    object QCadastroENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object QCadastroNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCadastroBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QCadastroCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QCadastroESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCadastroTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 14
    end
    object QCadastroEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 50
    end
    object QCadastroSERIE: TStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object QCadastroMODELO_CARTAO: TIntegerField
      FieldName = 'MODELO_CARTAO'
    end
    object QCadastroCONSULTA_LIMITE: TStringField
      FieldName = 'CONSULTA_LIMITE'
      FixedChar = True
      Size = 1
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOBS1: TStringField
      FieldName = 'OBS1'
      Size = 100
    end
    object QCadastroOBS2: TStringField
      FieldName = 'OBS2'
      Size = 100
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 50
    end
    object QCadastroBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QCadastroAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QCadastroCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 10
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroTAXA_RECARGA: TFloatField
      FieldName = 'TAXA_RECARGA'
    end
    object QCadastroEMPRESA: TStringField
      FieldName = 'EMPRESA'
      Size = 50
    end
  end
  object QEmpresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select empres_id, nome, coalesce(usa_cod_importacao,'#39'N'#39') as usa_' +
        'cod_importacao,'
      'case when fantasia is null'
      'then nome else fantasia end as fantasia,'
      'coalesce(fidelidade,'#39'N'#39') as fidelidade,'
      'band_id,'
      'usa_novo_cartao'
      'from empresas where apagado <> '#39'S'#39' order by nome')
    Left = 532
    Top = 161
    object QEmpresaempres_id: TIntegerField
      FieldName = 'empres_id'
      KeyFields = 'empres_id'
      LookupCache = True
    end
    object QEmpresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresausa_cod_importacao: TStringField
      FieldName = 'usa_cod_importacao'
      ReadOnly = True
      Size = 1
    end
    object QEmpresafantasia: TStringField
      FieldName = 'fantasia'
      ReadOnly = True
      Size = 60
    end
    object QEmpresafidelidade: TStringField
      FieldName = 'fidelidade'
      ReadOnly = True
      Size = 1
    end
    object QEmpresaband_id: TIntegerField
      FieldName = 'band_id'
    end
    object QEmpresausa_novo_cartao: TStringField
      FieldName = 'usa_novo_cartao'
      FixedChar = True
      Size = 1
    end
  end
  object DSEmpresa: TDataSource
    DataSet = QEmpresa
    Left = 532
    Top = 194
  end
  object QConvDetail: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from CONV_DETAIL  where conv_id = 0')
    Left = 452
    Top = 281
    object QConvDetailCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QConvDetailPIS: TFloatField
      FieldName = 'PIS'
    end
    object QConvDetailNOME_PAI: TStringField
      FieldName = 'NOME_PAI'
      Size = 45
    end
    object QConvDetailNOME_MAE: TStringField
      FieldName = 'NOME_MAE'
      Size = 45
    end
    object QConvDetailCART_TRAB_NUM: TIntegerField
      FieldName = 'CART_TRAB_NUM'
    end
    object QConvDetailCART_TRAB_SERIE: TStringField
      FieldName = 'CART_TRAB_SERIE'
      Size = 10
    end
    object QConvDetailREGIME_TRAB: TStringField
      FieldName = 'REGIME_TRAB'
      Size = 45
    end
    object QConvDetailVENC_TOTAL: TBCDField
      FieldName = 'VENC_TOTAL'
      Precision = 15
      Size = 2
    end
    object QConvDetailESTADO_CIVIL: TStringField
      FieldName = 'ESTADO_CIVIL'
      Size = 25
    end
    object QConvDetailNUM_DEPENDENTES: TIntegerField
      FieldName = 'NUM_DEPENDENTES'
    end
    object QConvDetailDATA_ADMISSAO: TDateTimeField
      FieldName = 'DATA_ADMISSAO'
    end
    object QConvDetailDATA_DEMISSAO: TDateTimeField
      FieldName = 'DATA_DEMISSAO'
    end
    object QConvDetailFIM_CONTRATO: TDateTimeField
      FieldName = 'FIM_CONTRATO'
    end
    object QConvDetailDISTRITO: TStringField
      FieldName = 'DISTRITO'
      Size = 45
    end
    object QConvDetailSALDO_DEVEDOR: TBCDField
      FieldName = 'SALDO_DEVEDOR'
      Precision = 15
      Size = 2
    end
    object QConvDetailSALDO_DEVEDOR_FAT: TBCDField
      FieldName = 'SALDO_DEVEDOR_FAT'
      Precision = 15
      Size = 2
    end
  end
  object QContaCorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from contacorrente '
      'where conv_id = :conv_id and baixa_conveniado <> '#39'S'#39
      'order by data desc, hora desc')
    Left = 492
    Top = 281
    object QContaCorrenteAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QContaCorrenteCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QContaCorrenteCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QContaCorrenteCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QContaCorrenteDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QContaCorrenteDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QContaCorrenteHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object QContaCorrenteDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object QContaCorrenteDEBITO: TBCDField
      FieldName = 'DEBITO'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteCREDITO: TBCDField
      FieldName = 'CREDITO'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      DisplayFormat = '#,##0.00'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object QContaCorrenteFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object QContaCorrenteFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QContaCorrentePAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object QContaCorrenteOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QContaCorrenteDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QContaCorrenteDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QContaCorrenteDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object QContaCorrenteDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object QContaCorrenteHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object QContaCorrenteNF: TIntegerField
      FieldName = 'NF'
    end
    object QContaCorrenteDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object QContaCorrenteDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object QContaCorrenteDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object QContaCorrenteOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object QContaCorrenteOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object QContaCorrenteDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object QContaCorrenteOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object QContaCorrenteCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteNSU: TIntegerField
      FieldName = 'NSU'
    end
    object QContaCorrentePREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QContaCorrenteCREDENCIADO: TStringField
      FieldKind = fkLookup
      FieldName = 'CREDENCIADO'
      LookupKeyFields = 'cred_id'
      LookupResultField = 'fantasia'
      KeyFields = 'CRED_ID'
      Size = 120
      Lookup = True
    end
  end
  object DSContaCorrente: TDataSource
    DataSet = QContaCorrente
    Left = 523
    Top = 282
  end
end
