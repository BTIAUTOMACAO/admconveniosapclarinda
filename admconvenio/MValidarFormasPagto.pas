
{***************************************************************************************************************}
{                                                                                                               }
{                                               XML Data Binding                                                }
{                                                                                                               }
{         Generated on: 14/06/2010 11:49:31                                                                     }
{       Generated from: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MValidarFormasPagto.xsd   }
{   Settings stored in: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MValidarFormasPagto.xdb   }
{                                                                                                               }
{***************************************************************************************************************}

unit MValidarFormasPagto;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTValidarFormasPagtoParam = interface;
  IXMLTCredenciado = interface;
  IXMLTValidarFormasPagtoRet = interface;
  IXMLTFormaPagto = interface;
  IXMLTFormaPagtoList = interface;
  IXMLTParcelas = interface;
  IXMLTParcela = interface;

{ IXMLTValidarFormasPagtoParam }

  IXMLTValidarFormasPagtoParam = interface(IXMLNode)
    ['{A56C9035-B0AD-4F10-B9DC-54C328C4C350}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_EMPRES_ID: LongWord;
    procedure Set_EMPRES_ID(Value: LongWord);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property EMPRES_ID: LongWord read Get_EMPRES_ID write Set_EMPRES_ID;
  end;

{ IXMLTCredenciado }

  IXMLTCredenciado = interface(IXMLNode)
    ['{BB104612-4A4D-471B-A5B2-585E115377E4}']
    { Property Accessors }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
    { Methods & Properties }
    property CODACESSO: LongWord read Get_CODACESSO write Set_CODACESSO;
    property SENHA: WideString read Get_SENHA write Set_SENHA;
  end;

{ IXMLTValidarFormasPagtoRet }

  IXMLTValidarFormasPagtoRet = interface(IXMLNode)
    ['{5DAAEC81-5CAA-40A0-BDCD-EA1FDC158861}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_FORMAPAGTO: IXMLTFormaPagtoList;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
    property FORMAPAGTO: IXMLTFormaPagtoList read Get_FORMAPAGTO;
  end;

{ IXMLTFormaPagto }

  IXMLTFormaPagto = interface(IXMLNode)
    ['{2924B08A-AA9A-4B72-A739-64C1214FB5C3}']
    { Property Accessors }
    function Get_CODIGO: LongWord;
    function Get_DESCRICAO: WideString;
    function Get_PARCELAS: IXMLTParcelas;
    procedure Set_CODIGO(Value: LongWord);
    procedure Set_DESCRICAO(Value: WideString);
    { Methods & Properties }
    property CODIGO: LongWord read Get_CODIGO write Set_CODIGO;
    property DESCRICAO: WideString read Get_DESCRICAO write Set_DESCRICAO;
    property PARCELAS: IXMLTParcelas read Get_PARCELAS;
  end;

{ IXMLTFormaPagtoList }

  IXMLTFormaPagtoList = interface(IXMLNodeCollection)
    ['{CF6C8985-24A6-41A4-B00C-5E4DA039C255}']
    { Methods & Properties }
    function Add: IXMLTFormaPagto;
    function Insert(const Index: Integer): IXMLTFormaPagto;
    function Get_Item(Index: Integer): IXMLTFormaPagto;
    property Items[Index: Integer]: IXMLTFormaPagto read Get_Item; default;
  end;

{ IXMLTParcelas }

  IXMLTParcelas = interface(IXMLNodeCollection)
    ['{F14D3ED3-3E04-406E-986C-53D519BC2919}']
    { Property Accessors }
    function Get_PARCELA(Index: Integer): IXMLTParcela;
    { Methods & Properties }
    function Add: IXMLTParcela;
    function Insert(const Index: Integer): IXMLTParcela;
    property PARCELA[Index: Integer]: IXMLTParcela read Get_PARCELA; default;
  end;

{ IXMLTParcela }

  IXMLTParcela = interface(IXMLNode)
    ['{469589A8-4B3D-4620-8A4B-57D876EBE34A}']
    { Property Accessors }
    function Get_VENCIMENTO: WideString;
    function Get_PERCENTUAL: LongWord;
    procedure Set_VENCIMENTO(Value: WideString);
    procedure Set_PERCENTUAL(Value: LongWord);
    { Methods & Properties }
    property VENCIMENTO: WideString read Get_VENCIMENTO write Set_VENCIMENTO;
    property PERCENTUAL: LongWord read Get_PERCENTUAL write Set_PERCENTUAL;
  end;

{ Forward Decls }

  TXMLTValidarFormasPagtoParam = class;
  TXMLTCredenciado = class;
  TXMLTValidarFormasPagtoRet = class;
  TXMLTFormaPagto = class;
  TXMLTFormaPagtoList = class;
  TXMLTParcelas = class;
  TXMLTParcela = class;

{ TXMLTValidarFormasPagtoParam }

  TXMLTValidarFormasPagtoParam = class(TXMLNode, IXMLTValidarFormasPagtoParam)
  protected
    { IXMLTValidarFormasPagtoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_EMPRES_ID: LongWord;
    procedure Set_EMPRES_ID(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCredenciado }

  TXMLTCredenciado = class(TXMLNode, IXMLTCredenciado)
  protected
    { IXMLTCredenciado }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
  end;

{ TXMLTValidarFormasPagtoRet }

  TXMLTValidarFormasPagtoRet = class(TXMLNode, IXMLTValidarFormasPagtoRet)
  private
    FFORMAPAGTO: IXMLTFormaPagtoList;
  protected
    { IXMLTValidarFormasPagtoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    function Get_FORMAPAGTO: IXMLTFormaPagtoList;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTFormaPagto }

  TXMLTFormaPagto = class(TXMLNode, IXMLTFormaPagto)
  protected
    { IXMLTFormaPagto }
    function Get_CODIGO: LongWord;
    function Get_DESCRICAO: WideString;
    function Get_PARCELAS: IXMLTParcelas;
    procedure Set_CODIGO(Value: LongWord);
    procedure Set_DESCRICAO(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTFormaPagtoList }

  TXMLTFormaPagtoList = class(TXMLNodeCollection, IXMLTFormaPagtoList)
  protected
    { IXMLTFormaPagtoList }
    function Add: IXMLTFormaPagto;
    function Insert(const Index: Integer): IXMLTFormaPagto;
    function Get_Item(Index: Integer): IXMLTFormaPagto;
  end;

{ TXMLTParcelas }

  TXMLTParcelas = class(TXMLNodeCollection, IXMLTParcelas)
  protected
    { IXMLTParcelas }
    function Get_PARCELA(Index: Integer): IXMLTParcela;
    function Add: IXMLTParcela;
    function Insert(const Index: Integer): IXMLTParcela;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTParcela }

  TXMLTParcela = class(TXMLNode, IXMLTParcela)
  protected
    { IXMLTParcela }
    function Get_VENCIMENTO: WideString;
    function Get_PERCENTUAL: LongWord;
    procedure Set_VENCIMENTO(Value: WideString);
    procedure Set_PERCENTUAL(Value: LongWord);
  end;

implementation

{ TXMLTValidarFormasPagtoParam }

procedure TXMLTValidarFormasPagtoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  inherited;
end;

function TXMLTValidarFormasPagtoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTValidarFormasPagtoParam.Get_EMPRES_ID: LongWord;
begin
  Result := ChildNodes['EMPRES_ID'].NodeValue;
end;

procedure TXMLTValidarFormasPagtoParam.Set_EMPRES_ID(Value: LongWord);
begin
  ChildNodes['EMPRES_ID'].NodeValue := Value;
end;

{ TXMLTCredenciado }

function TXMLTCredenciado.Get_CODACESSO: LongWord;
begin
  Result := ChildNodes['CODACESSO'].NodeValue;
end;

procedure TXMLTCredenciado.Set_CODACESSO(Value: LongWord);
begin
  ChildNodes['CODACESSO'].NodeValue := Value;
end;

function TXMLTCredenciado.Get_SENHA: WideString;
begin
  Result := ChildNodes['SENHA'].Text;
end;

procedure TXMLTCredenciado.Set_SENHA(Value: WideString);
begin
  ChildNodes['SENHA'].NodeValue := Value;
end;

{ TXMLTValidarFormasPagtoRet }

procedure TXMLTValidarFormasPagtoRet.AfterConstruction;
begin
  RegisterChildNode('FORMAPAGTO', TXMLTFormaPagto);
  FFORMAPAGTO := CreateCollection(TXMLTFormaPagtoList, IXMLTFormaPagto, 'FORMAPAGTO') as IXMLTFormaPagtoList;
  inherited;
end;

function TXMLTValidarFormasPagtoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTValidarFormasPagtoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTValidarFormasPagtoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTValidarFormasPagtoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

function TXMLTValidarFormasPagtoRet.Get_FORMAPAGTO: IXMLTFormaPagtoList;
begin
  Result := FFORMAPAGTO;
end;

{ TXMLTFormaPagto }

procedure TXMLTFormaPagto.AfterConstruction;
begin
  RegisterChildNode('PARCELAS', TXMLTParcelas);
  inherited;
end;

function TXMLTFormaPagto.Get_CODIGO: LongWord;
begin
  Result := ChildNodes['CODIGO'].NodeValue;
end;

procedure TXMLTFormaPagto.Set_CODIGO(Value: LongWord);
begin
  ChildNodes['CODIGO'].NodeValue := Value;
end;

function TXMLTFormaPagto.Get_DESCRICAO: WideString;
begin
  Result := ChildNodes['DESCRICAO'].Text;
end;

procedure TXMLTFormaPagto.Set_DESCRICAO(Value: WideString);
begin
  ChildNodes['DESCRICAO'].NodeValue := Value;
end;

function TXMLTFormaPagto.Get_PARCELAS: IXMLTParcelas;
begin
  Result := ChildNodes['PARCELAS'] as IXMLTParcelas;
end;

{ TXMLTFormaPagtoList }

function TXMLTFormaPagtoList.Add: IXMLTFormaPagto;
begin
  Result := AddItem(-1) as IXMLTFormaPagto;
end;

function TXMLTFormaPagtoList.Insert(const Index: Integer): IXMLTFormaPagto;
begin
  Result := AddItem(Index) as IXMLTFormaPagto;
end;
function TXMLTFormaPagtoList.Get_Item(Index: Integer): IXMLTFormaPagto;
begin
  Result := List[Index] as IXMLTFormaPagto;
end;

{ TXMLTParcelas }

procedure TXMLTParcelas.AfterConstruction;
begin
  RegisterChildNode('PARCELA', TXMLTParcela);
  ItemTag := 'PARCELA';
  ItemInterface := IXMLTParcela;
  inherited;
end;

function TXMLTParcelas.Get_PARCELA(Index: Integer): IXMLTParcela;
begin
  Result := List[Index] as IXMLTParcela;
end;

function TXMLTParcelas.Add: IXMLTParcela;
begin
  Result := AddItem(-1) as IXMLTParcela;
end;

function TXMLTParcelas.Insert(const Index: Integer): IXMLTParcela;
begin
  Result := AddItem(Index) as IXMLTParcela;
end;

{ TXMLTParcela }

function TXMLTParcela.Get_VENCIMENTO: WideString;
begin
  Result := ChildNodes['VENCIMENTO'].Text;
end;

procedure TXMLTParcela.Set_VENCIMENTO(Value: WideString);
begin
  ChildNodes['VENCIMENTO'].NodeValue := Value;
end;

function TXMLTParcela.Get_PERCENTUAL: LongWord;
begin
  Result := ChildNodes['PERCENTUAL'].NodeValue;
end;

procedure TXMLTParcela.Set_PERCENTUAL(Value: LongWord);
begin
  ChildNodes['PERCENTUAL'].NodeValue := Value;
end;

end. 