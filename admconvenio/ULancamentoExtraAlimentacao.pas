unit ULancamentoExtraAlimentacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DBCtrls, DB, ADODB,
  frxClass, frxGradient, frxExportPDF, frxDBSet, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvMemoryDataset, JvExStdCtrls, JvEdit,
  JvValidateEdit, Mask, JvExMask, JvToolEdit, DateUtils;

type
  TFrmLancamentoExtraAlimentacao = class(TF1)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbLkpEmpresas: TDBLookupComboBox;
    PageControl1: TPageControl;
    pnlDadosRenovacao: TPanel;
    rgTipo: TRadioGroup;
    Button2: TButton;
    btnExcluir: TButton;
    dbGridAlim: TJvDBGrid;
    pnlDados: TPanel;
    Bevel1: TBevel;
    Label57: TLabel;
    Label56: TLabel;
    Bevel3: TBevel;
    Label85: TLabel;
    Label88: TLabel;
    btnGravarConvAlim: TBitBtn;
    btnCancelarConvAlim: TBitBtn;
    edtSaldoRenovacao: TEdit;
    edtAbonoMes: TEdit;
    Button1: TButton;
    btnImportar: TBitBtn;
    lblTotalRenovacao: TJvValidateEdit;
    lblTotalAbono: TJvValidateEdit;
    BitBtn1: TBitBtn;
    dsEmpresas: TDataSource;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    QCredAlim: TADOQuery;
    dsCredAlim: TDataSource;
    qUpdate: TADOQuery;
    tExcel: TADOTable;
    rgDetalheSAP: TRadioGroup;
    rbTipoComplemento: TRadioGroup;
    qEmpresasliberada: TStringField;
    qEmpresasfechamento1: TStringField;
    Label60: TLabel;
    dbDataRenovacao: TJvDateEdit;
    qEmpresastipo_credito: TIntegerField;
    QCredAlimconv_id: TIntegerField;
    QCredAlimempres_id: TIntegerField;
    QCredAlimtitular: TStringField;
    QCredAlimlimite_mes: TBCDField;
    QCredAlimabono_mes: TBCDField;
    QCredAlimsaldo_renovacao: TBCDField;
    QCredAlimRENOVACAO_ID: TIntegerField;
    QCredAlimDATA_RENOVACAO: TWideStringField;
    QCredAlimTIPO_CREDITO: TStringField;
    SpeedButton1: TSpeedButton;
    RichEdit1: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure dbLkpEmpresasExit(Sender: TObject);
//    procedure tsAlimentacaoShow;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnGravarConvAlimClick(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure dbLkpEmpresasEnter(Sender: TObject);
    procedure rgTipoClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
//    procedure FormShow(Sender: TObject);
//    procedure Label2Click(Sender: TObject);
  private
    conv_sel : String;

    { Private declarations }
  public
    tSaldoRenovacao, tLimiteMes, tAbonoMes : currency;
  SavePlace : TBookmark;
  procedure InsereAlimentacaoRenovacao;
    procedure InsereAlimRenovacaoCreditos;
    { Public declarations }
  end;

var
  FrmLancamentoExtraAlimentacao: TFrmLancamentoExtraAlimentacao;

implementation

uses DM, cartao_util, UMenu, URotinasTexto, UValidacao, UChangeLog;

{$R *.dfm}

procedure TFrmLancamentoExtraAlimentacao.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  pnlDadosRenovacao.Visible := false;
  dbGridAlim.Visible := false;
  pnlDados.Visible := false;
  dbDataRenovacao.Visible := false;
  Label60.Visible := False;
  dbLkpEmpresas.SetFocus;

//  EdEmp_ID.SetFocus;
end;

procedure TFrmLancamentoExtraAlimentacao.dbLkpEmpresasExit(
  Sender: TObject);
  var valorAbono, valorRenovacao: Currency;
  var data_atual, data_fecha: TDateTime ;
begin
  inherited;

    if qEmpresasliberada.Text = 'S' then begin

      pnlDadosRenovacao.Visible := true;
      pnlDados.Visible := true;
      dbDataRenovacao.Visible := true;
      Label60.Visible := true;
      valorAbono := 0;
      valorRenovacao := 0;


      qCredAlim.Close;
      qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
      QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
      qCredAlim.Open;

      //tsAlimentacaoShow();

      if not qCredAlimDATA_RENOVACAO.IsNull then begin
          dbGridAlim.Visible := true;
//          DMConexao.ExecuteSql('select RENOVACAO_ID from ALIMENTACAO_RENOVACAO where EMPRES_ID=' + QCredAlimempres_id.AsString);
//
//          if not DMConexao.AdoQry.Fields[0].IsNull then begin
//          DMConexao.AdoCon.BeginTrans;
//          DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString);
//
//          DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString);
//          DMConexao.AdoCon.CommitTrans;
//        end;
        //qCredAlim.Close;
        //qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
        //QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
        //qCredAlim.Open;

        //dbGridAlim.Visible := false;
        //rgTipo.ItemIndex := 0
        if QCredAlimTIPO_CREDITO.AsString = 'R' then begin
          rgTipo.ItemIndex := 0;
          rgDetalheSAP.ItemIndex := 1;
        end else begin
          rgTipo.ItemIndex := 1;
          rgDetalheSAP.ItemIndex := 0;
        end;

        //dbGridAlim.Visible := true;
        //Panel43.Visible := true;

        while not qCredAlim.Eof do
        begin
          valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
          valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
          qCredAlim.Next;
        end;

          qCredAlim.First;

      end;

      lblTotalAbono.Value := valorAbono;
      lblTotalRenovacao.Value := valorRenovacao;
      edtSaldoRenovacao.Text := '0,00';
      edtAbonoMes.Text := '0,00';

       if qEmpresastipo_credito.AsString = '2' then
       begin
          rgTipo.ItemIndex := 0;
          rgDetalheSAP.ItemIndex := 1;

          data_atual := StrToDateTime(FormatDateTime('dd/mm/yyyy', Date)) ;
          //data_fecha := StrToDateTime(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date));
          data_fecha := StrToDateTime(qEmpresasfechamento1.AsString);


//          if data_fecha = 0 then begin
//              dbDataRenovacao.Text :=  qEmpresasfechamento1.AsString;
          if data_fecha < data_atual then begin

//          BitBtn1.Visible := true;

            DMConexao.Query1.Close;
            DMConexao.Query1.SQL.Clear;
            DMConexao.Query1.SQL.Add(' select convert(varchar, (select top 1 DATA_FECHA ');
            DMConexao.Query1.SQL.Add(' from DIA_FECHA where EMPRES_ID='+ QuotedStr(qEmpresasempres_id.AsString));
            DMConexao.Query1.SQL.Add(' and month(data_fecha) > MONTH(GETDATE ()) ');
            DMConexao.Query1.SQL.Add(' and year(DATA_FECHA) >= year(GETDATE ())),103) ');
            DMConexao.Query1.SQL.Add(' fechamento1  from empresas emp  where emp.tipo_credito = 2 or emp.TIPO_CREDITO = 3 ');
            DMConexao.Query1.SQL.Add(' and emp.liberada=' + QuotedStr('S') + ' AND emp.APAGADO=' + QuotedStr('N'));
            DMConexao.Query1.Open;

            dbDataRenovacao.Text := DMConexao.Query1.Fields[0].AsString;
          end else begin
              dbDataRenovacao.Text :=  qEmpresasfechamento1.AsString;
          end;



        end else begin
        rgTipo.ItemIndex := 1;
        rgDetalheSAP.ItemIndex := 1;
//      dbDataRenovacao.Text :=  FormataDataSql(qCredAlimDATA_RENOVACAO.AsString);
        dbDataRenovacao.Text :=  DateToStr(Date());
       end;

      //dbDataRenovacao.Date := Date;
      //dbDataRenovacao.Enabled := false;
      Button2.SetFocus;
    end else begin
     Msginf('A empresa '+ qEmpresasnome.AsString +' n�o esta liberada: ');
      qEmpresas.Close;
      qEmpresas.Open;
      QCredAlim.Close;
      pnlDadosRenovacao.Visible := false;
      dbGridAlim.Visible := false;
      pnlDados.Visible := false;
      dbDataRenovacao.Visible := false;
      Label60.Visible := False;
      dbLkpEmpresas.SetFocus;
    end;

end;

procedure TFrmLancamentoExtraAlimentacao.Button2Click(Sender: TObject);
  var data_atual, data_renovacao: TDateTime ;
begin
  inherited;
  data_atual := StrToDateTime(FormatDateTime('dd/mm/yyyy', Date)) ;
  data_renovacao := StrToDateTime(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date));

  if data_renovacao >=  data_atual then begin
      InsereAlimentacaoRenovacao;
  end else begin
    MsgInf('A Data de Renova��o n�o pode ser menor que a data atual!');
    qEmpresas.Close;
    qEmpresas.Open;
    QCredAlim.Close;
//    pnlDadosRenovacao.Visible := false;
//    dbGridAlim.Visible := false;
    pnlDados.Visible := false;
//    dbDataRenovacao.Visible := false;
    Label60.Visible := False;
    dbLkpEmpresas.SetFocus;
  end;

end;

procedure TFrmLancamentoExtraAlimentacao.InsereAlimentacaoRenovacao;
var sql,sqlSAP,detalheEvento,tipoComplementoSAP : String;
    renovacao_id : integer;
begin
//    if dbDataRenovacao.text <  DateToStr(Now) then begin

        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SRENOVACAO_ID AS RENOVACAO_ID');
          DMConexao.AdoQry.Open;
          renovacao_id := DMConexao.AdoQry.Fields[0].Value;

          sql := 'INSERT INTO ALIMENTACAO_RENOVACAO(RENOVACAO_ID, EMPRES_ID, DATA_RENOVACAO, TIPO_CREDITO, DATA_LANCAMENTO) VALUES('+ inttostr(renovacao_id);
          sqlSAP := 'INSERT INTO ALIMENTACAO_RENOVACAO_SAP(RENOVACAO_ID, EMPRES_ID, DATA_RENOVACAO,TIPO_CREDITO,TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,DATA_LANCAMENTO) VALUES('+ inttostr(renovacao_id);
          sql := sql + ',' + inttostr(dbLkpEmpresas.KeyValue) + ',' + QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ',';
          sqlSAP := sqlSAP + ',' + inttostr(dbLkpEmpresas.KeyValue) + ',' + QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ',';


          case rgDetalheSAP.ItemIndex of
            0: begin
                  detalheEvento := 'RFI'
               end;
          else
            detalheEvento := 'RFP';
          end;

          case rbTipoComplemento.ItemIndex of
            0: begin
                  tipoComplementoSAP := 'PR'
               end;
          else
            tipoComplementoSAP := 'FA';
          end;

          case rgTipo.ItemIndex of
          0: begin
              sql := sql + '''R'',' ;
              sqlSAP := sqlSAP + '''R'',''RE'','+QuotedStr(detalheEvento) +',NULL,';
             end;
          else
            sql := sql + '''C'',';
            sqlSAP := sqlSAP + '''C'',';
            sqlSAP := sqlSAP + '''CR'','+QuotedStr(detalheEvento)+','+ QuotedStr(tipoComplementoSAP)+',';
            //sqlSAP := sqlSAP + ''+detalheEvento+''+',';
          end;


          sql := sql + QuotedStr(FormatDateTime('dd/mm/yyyy',Date) +  ' ' + FormatDateTime('hh:mm:ss',Time))  + ')';
          sqlSAP := sqlSAP + QuotedStr(FormatDateTime('dd/mm/yyyy',Date) +  ' ' + FormatDateTime('hh:mm:ss',Time))  + ')';

          DMConexao.ExecuteSql(sql);
          DMConexao.ExecuteSql(sqlSAP);
          DMConexao.AdoCon.CommitTrans;

        except
          on e:Exception do
          begin
             DMConexao.AdoCon.RollbackTrans;
             Screen.Cursor := crDefault;
             MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
             abort;
          end;
        end;

    qCredAlim.Close;
    qCredAlim.Parameters.Items[0].Value := dbLkpEmpresas.KeyValue;
    QCredAlim.Parameters.Items[1].Value := dbLkpEmpresas.KeyValue;
    qCredAlim.Open;

    DMConexao.GravaLog('FCadEmp','DATA_RENOVACAO',FormataDataSql(QCredAlimDATA_RENOVACAO.AsString),FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Inclus�o',
        dbLkpEmpresas.KeyValue,'');

    msginf('Atualiza��o realizada com sucesso');

    dbGridAlim.Visible := true;
    dbGridAlim.SetFocus;
    Screen.Cursor := crDefault;


end;

procedure TFrmLancamentoExtraAlimentacao.Button1Click(Sender: TObject);
begin
  inherited;
  InsereAlimRenovacaoCreditos
end;

procedure TFrmLancamentoExtraAlimentacao.InsereAlimRenovacaoCreditos;
var
  dc : char;
  valorAbono, valorRenovacao: Currency;
  detalheEvento,sqlSAP,complemento,empres_id : string;
begin
    valorAbono := 0;
    valorRenovacao := 0;

    if Application.MessageBox('Confirma a inclus�o/altera��o do Abono/Saldo Renova��o?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      Screen.Cursor := crHourGlass;
      dc := DecimalSeparator;
      Application.ProcessMessages;

      try
        DMConexao.AdoCon.BeginTrans;

        //DELETA OS REGISTROS DA TABELA ALIMENTACAO_RENOVACAO_CREDITOS//
        DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+qCredAlimRENOVACAO_ID.AsString);
        DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP WHERE RENOVACAO_ID = '+qCredAlimRENOVACAO_ID.AsString);
        QCredAlim.First;
        while not QCredAlim.Eof do begin
          qUpdate.SQL.Clear;
          qUpdate.SQL.Text := 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + qCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',edtSaldoRenovacao.Text) + ',' + fnsubstituiString(',','.',edtAbonoMes.Text) + ','+QuotedStr(DateTimeToStr(Now))+')';
          qUpdate.ExecSQL;

          case rgDetalheSAP.ItemIndex of
            0: begin
                  detalheEvento := 'RFI'
               end;
          else
            detalheEvento := 'RFP';
          end;
          case rbTipoComplemento.ItemIndex of
            0: begin
                  complemento := 'PR'
               end;
          else
            complemento := 'FA';
          end;

          sqlSAP := '';
          sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO,TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID, DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + qCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',edtSaldoRenovacao.Text) + ',' + fnsubstituiString(',','.',edtAbonoMes.Text)+','+QuotedStr(DateTimeToStr(Now))+',';

          case rgTipo.ItemIndex of
            0: begin
              sqlSAP := sqlSAP + '''RE'','+QuotedStr(detalheEvento)+',NULL';
            end;
          else
            sqlSAP := sqlSAP + '''CR'',';
            sqlSAP := sqlSAP + QuotedStr(detalheEvento) + ','+ QuotedStr(complemento);
          end;

          empres_id := dbLkpEmpresas.KeyValue;
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empres_id + ' and DATA_FECHA >= getdate()');
          DMConexao.AdoQry.Open;

          sqlSAP := sqlSAP + ', ' + empres_id + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

          sqlSAP := sqlSAP + ')';
          DMConexao.ExecuteSql(sqlSAP);

          valorAbono := valorAbono + STRTOCURR(edtAbonoMes.Text);
          valorRenovacao := valorRenovacao + STRTOCURR(edtSaldoRenovacao.Text);

          QCredAlim.Next;
        end;

        DMConexao.AdoCon.CommitTrans;

        msginf('Atualiza��o realizada com sucesso!');

      except
        on e:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
          Screen.Cursor := crDefault;
          abort;
        end;
      end;

      Screen.Cursor := crDefault;
      qCredAlim.Requery;
      DecimalSeparator := dc;

      lblTotalAbono.Value := valorAbono;
      lblTotalRenovacao.Value := valorRenovacao;
      edtSaldoRenovacao.Text := '0,00';
      edtAbonoMes.Text := '0,00';
      Button1.SetFocus;

    end;

end;

procedure TFrmLancamentoExtraAlimentacao.btnGravarConvAlimClick(
  Sender: TObject);
var saldoRenoOld, abonoMesOld, limiteMesOld : currency;
incluir : boolean;
valorAbono, valorRenovacao: Currency;
detalheEvento,complemento,sqlSAP,empres_id : string;
begin
  if Application.MessageBox('Confirma a altera��o das informa��es?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    valorAbono := 0;
    valorRenovacao := 0;

    tLimiteMes := StrtoCurr(dbGridAlim.Fields[2].Text);
    tAbonoMes := StrtoCurr(dbGridAlim.Fields[3].Text);
    tSaldoRenovacao := StrtoCurr(dbGridAlim.Fields[4].Text);

    SavePlace := QCredAlim.GetBookmark;
    try
      DMConexao.AdoCon.BeginTrans;
      // Altera��o de Limite M�s
      if VarIsNull(QCredAlimLIMITE_MES.OldValue) then
        limiteMesOld := 0
      else
        limiteMesOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if (limiteMesOld <> tLimiteMes) then
      begin
        DMConexao.GravaLog('FCadConv','LIMITE_MES',FormatDinBR(limiteMesOld),FormatDinBR(tLimiteMes),Operador.Nome,'Altera��o',
                            QCredAlimCONV_ID.AsString,'');
        qUpdate.SQL.Text := ' update conveniados set limite_mes = ' + fnsubstituiString(',','.',CurrToStr(tLimiteMes)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString;
        DMConexao.ExecuteSql(qUpdate.SQL.Text);
      end;

      Screen.Cursor := crHourGlass;

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE CONV_ID = '+ QCredAlimCONV_ID.AsString);
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then
        incluir := false
      else
        incluir := true;

      // Altera��o de Abono M�s
      if VarIsNull(QCredAlimABONO_MES.OldValue) then
        abonoMesOld := 0
      else
        abonoMesOld := QCredAlimABONO_MES.OldValue;

      // Altera��o de Saldo Renova��o
      if VarIsNull(QCredAlimSALDO_RENOVACAO.OldValue) then
        saldoRenoOld := 0
      else
        saldoRenoOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if ((abonoMesOld <> tAbonoMes) or (saldoRenoOld <> tSaldoRenovacao))  then
      begin
        if incluir = true then begin
            qUpdate.SQL.Text := 'insert into alimentacao_renovacao_creditos(renovacao_id, conv_id, renovacao_valor, abono_valor, data_alteracao) values('+ qCredAlimRENOVACAO_ID.AsString + ',' + QCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ',' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) +',' +QuotedStr(DateTimeToStr(Now))+')';
            DMConexao.ExecuteSql(qUpdate.SQL.Text);
        end
        else
          begin
            qUpdate.SQL.Text := ' update alimentacao_renovacao_creditos set abono_valor = ' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) + ', renovacao_valor = ' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString + ' and renovacao_id = ' + qCredAlimRENOVACAO_ID.AsString;
            DMConexao.ExecuteSql(qUpdate.SQL.Text);
          end;
        //SAP
        case rgDetalheSAP.ItemIndex of
          0: begin
                detalheEvento := 'RFI'
             end;
        else
          detalheEvento := 'RFP';
        end;

        case rbTipoComplemento.ItemIndex of
          0: begin
                complemento := 'PR'
             end;
        else
          complemento := 'FA';
        end;


        qUpdate.SQL.Text := 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO,TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID, DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES('+ qCredAlimRENOVACAO_ID.AsString + ',' + QCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ',' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) +',' +QuotedStr(DateTimeToStr(Now))+',';

        case rgTipo.ItemIndex of
          0: begin
            qUpdate.SQL.Text := qUpdate.SQL.Text + '''RE'','+QuotedStr(detalheEvento)+',NULL';
          end;
        else
          qUpdate.SQL.Text := qUpdate.SQL.Text + '''CR'',';
          qUpdate.SQL.Text := qUpdate.SQL.Text + QuotedStr(detalheEvento) +','+ QuotedStr(complemento);
        end;

        empres_id := dbLkpEmpresas.KeyValue;
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empres_id + ' and DATA_FECHA >= getdate()');
        DMConexao.AdoQry.Open;

        qUpdate.SQL.Text := qUpdate.SQL.Text + ', ' + empres_id + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

        qUpdate.SQL.Text := qUpdate.SQL.Text + ')';
        DMConexao.ExecuteSql(qUpdate.SQL.Text);

        valorAbono := valorAbono + STRTOCURR(edtAbonoMes.Text);
        valorRenovacao := valorRenovacao + STRTOCURR(edtSaldoRenovacao.Text);

        QCredAlim.Next;
        //Fim SAP
      end;

      DMConexao.AdoCon.CommitTrans;

    except
      on e:Exception do
      begin
         DMConexao.AdoCon.RollbackTrans;
         Screen.Cursor := crDefault;
         MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
         abort;
       end;
    end;
  end;

  QCredAlim.Requery;
  QCredAlim.GotoBookmark(SavePlace);
  QCredAlim.FreeBookmark(SavePlace);
  qCredAlim.First;
  while not qCredAlim.Eof do
    begin
        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
        qCredAlim.Next;
    end;
  qCredAlim.First;
  lblTotalAbono.Value := valorAbono;
  lblTotalRenovacao.Value := valorRenovacao;
  
  Screen.Cursor := crDefault;
  tSaldoRenovacao := 0;
  tAbonoMes := 0;
  tLimiteMes := 0;
end;

procedure TFrmLancamentoExtraAlimentacao.btnImportarClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId,detalheEvento,complemento,sqlSAP,empresId,valorAbonoString,valorRenovacaoString : String;
nome : PAnsiChar;
path : String;
valorAbono, valorRenovacao: Currency;
erro: Boolean;
begin
  inherited;

  valorAbono := 0;
  valorRenovacao := 0;
  valorAbonoString := '0';
  valorRenovacaoString := '0';
  try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
      OD.Free;
      tExcel.Active := False;
      //tExcel.Close;
      tExcel.ConnectionString := path;
      tExcel.TableName:= 'credito$';
      //tExcel.Active := True;
    try
      tExcel.Open;
    except
      MsgErro('N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "credito" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    btnImportar.Caption := 'Importar';
    Abort;
  end;

  if qCredAlimEMPRES_ID.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
  begin
    if not DMConexao.AdoCon.InTransaction then
      QCredAlim.Requery();
      renovacaoId := QCredAlimRENOVACAO_ID.AsString;
      DMConexao.AdoCon.BeginTrans;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+ QuotedStr(renovacaoId));
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then begin
        msginf('Valores anteriores j� lan�ados ser�o sobrepostos pelos valores da planilha');
      end;

      DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+renovacaoId);
      DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP WHERE RENOVACAO_ID =  '+ renovacaoId);
      Screen.Cursor := crHourGlass;
      while not tExcel.Eof do begin
      try
        if tExcel.fieldByName('CONV_ID').AsString <> '' then begin

          nome := PChar('O Conv-ID: '+tExcel.fieldByName('CONV_ID').AsString + ' est� bloqueado e n�o receber� a Recarga/Complemento!'#13' '#13'Deseja pular o conveniado e continuar o lan�amento?');

          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add(' SELECT CONV_ID FROM CONVENIADOS WHERE CONV_ID  = '+tExcel.fieldByName('CONV_ID').AsString+' AND LIBERADO = ''S'' AND APAGADO = ''N''');
          DMConexao.AdoQry.Open;
          if DMConexao.AdoQry.Fields[0].AsString <> '' then begin

//            if DMConexao.AdoQry.Fields[0].AsString = '' then begin
//
//            msginf('ULancamentoExtraAlimentacao_735 - Por favor, verificar Status do Conveniado de Conv. ID - ' + tExcel.fieldByName('CONV_ID').AsString);
//            DMConexao.AdoCon.RollbackTrans;
//            Screen.Cursor := crDefault;
//            tExcel.Close;
//            abort;

//            try
              qUpdate.SQL.Clear;
              if tExcel.fieldByName('VALOR RENOVA�AO').AsString <> '' then begin
                  qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
                  ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString) + ', ');
              end else begin
                    //valorRenovacaoString := fnsubstituiString(',','.',CurrToStr(valorRenovacao));
                    qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
                     ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + valorRenovacaoString + ', ');
              end;
              //qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
              // ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString) + ', ');
              if(QCredAlimempres_id.AsInteger = 1550) or (QCredAlimempres_id.AsInteger = 1552) or (QCredAlimempres_id.AsInteger = 1553) or (QCredAlimempres_id.AsInteger = 1696) then begin

                if tExcel.fieldByName('CESTA').AsString <> '' then begin
                    qUpdate.SQL.Add(fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) +  ',');
                end else begin
                    //valorAbonoString := fnsubstituiString(',','.',CurrToStr(valorAbono));
                    qUpdate.SQL.Add(valorAbonoString + ',');
                end;
              end
              else
              begin
                qUpdate.SQL.Add('0.00, ');
              end;
              qUpdate.SQL.Add(QuotedStr(DateTimeToStr(Now))+')');
              DMConexao.ExecuteSql(qUpdate.SQL.Text);






              case rgDetalheSAP.ItemIndex of
                0: begin
                      detalheEvento := 'RFI'
                   end;
              else
                detalheEvento := 'RFP';
              end;

              case rbTipoComplemento.ItemIndex of
                0: begin
                      complemento := 'PR'
                   end;
              else
                complemento := 'FA';
              end;

              sqlSAP := '';

              if tExcel.fieldByName('VALOR RENOVA�AO').AsString <> '' then begin
                sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
               ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString )+',';
               end else begin
                //valorRenovacaoString := fnsubstituiString(',','.',CurrToStr(valorRenovacao));
                sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
               ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + valorRenovacaoString +',';
              end;

              if(QCredAlimempres_id.AsInteger = 1550) or (QCredAlimempres_id.AsInteger = 1552) or (QCredAlimempres_id.AsInteger = 1553) or (QCredAlimempres_id.AsInteger = 1696) then
              begin
                if tExcel.fieldByName('CESTA').AsString <> '' then begin
                    sqlSAP := sqlSAP + fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) + ',' + QuotedStr(DateTimeToStr(Now))+',';
                    //qUpdate.SQL.Add(fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) +  ',');
                end else begin
                    //valorAbonoString := fnsubstituiString(',','.',CurrToStr(valorAbono));
                    sqlSAP := sqlSAP + (valorAbonoString + ',') + QuotedStr(DateTimeToStr(Now))+',';
                end;
                //sqlSAP := sqlSAP + fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) + ',' + QuotedStr(DateTimeToStr(Now))+',';
              end
              else begin
                sqlSAP := sqlSAP + '0.00, '+QuotedStr(DateTimeToStr(Now))+',';
              end;



              case rgTipo.ItemIndex of
              0: begin
                  sqlSAP := sqlSAP + '''RE'','+QuotedStr(detalheEvento)+',NULL';
                end;

              else
                sqlSAP := sqlSAP + '''CR'',';
                sqlSAP := sqlSAP + QuotedStr(detalheEvento)+','+QuotedStr(complemento);
              end;
              empresId := dbLkpEmpresas.KeyValue;
              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empresId + ' and DATA_FECHA >= getdate()');
              DMConexao.AdoQry.Open;
              sqlSAP := sqlSAP + ', ' + empresId + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

              sqlSAP := sqlSAP + ')';
              DMConexao.ExecuteSql(sqlSAP);


              valorRenovacao := valorRenovacao + tExcel.fieldByName('VALOR RENOVA�AO').AsCurrency;
//              except on E:Exception do begin
//              MsgErro('Erro ao incluir creditos');
//              DMConexao.AdoCon.RollbackTrans;
//              Screen.Cursor := crDefault;
//              tExcel.Close;
//              abort;
//            end;
//            end;
          end else begin

             if Application.MessageBox(nome,'Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then begin

             end else begin
//              msginf('ULancamentoExtraAlimentacao_735 - Por favor, verificar Status do Conveniado de Conv. ID - ' + tExcel.fieldByName('CONV_ID').AsString);
              DMConexao.AdoCon.RollbackTrans;
              Screen.Cursor := crDefault;
              tExcel.Close;
              abort;
              end;
          end;

        end;
      except on E:Exception do begin
          MsgErro('Erro ao incluir cr�ditos');
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          tExcel.Close;
          abort;
        end;
      end;
      tExcel.Next;
      Application.ProcessMessages;
    end;

    lblTotalAbono.Value := 0;
    lblTotalRenovacao.Value := valorRenovacao;
    DMConexao.AdoCon.CommitTrans;
    QCredAlim.Requery;
    Screen.Cursor := crDefault;
    if tExcel.State in [dsEdit, dsInsert] then
      tExcel.Close;

    msginf('Importa��o realizada com sucesso!');

  end
  else
     msginf('Planilha n�o pertence a empresa '+qCredAlimEMPRES_ID.AsString + '. Verifique o arquivo.');


end;

procedure TFrmLancamentoExtraAlimentacao.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Confirma o Lan�amento da Recarga?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
   try
        DMConexao.GravaLog('FCadEmp','RENOVACA_ID',QCredAlimRENOVACAO_ID.AsString,FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Inclus�o',
        dbLkpEmpresas.KeyValue,'');

        Screen.Cursor := crHourGlass;

        DMConexao.AdoCon.BeginTrans;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(' SELECT TOP 1 DIA_FECHA.DATA_FECHA FROM DIA_FECHA WHERE DIA_FECHA.DATA_FECHA > CONVERT(DATE,CURRENT_TIMESTAMP) AND DIA_FECHA.EMPRES_ID = ' + QCredAlimEMPRES_ID.AsString);
        DMConexao.AdoQry.Open;
        try
          DMConexao.ExecuteSql('EXEC VERIFICA_SALDO_RENOVACAO ' + QCredAlimEMPRES_ID.AsString + ',' + QCredAlimTIPO_CREDITO.AsString + ',''' +  DMConexao.AdoQry.Fields[0].AsString + '''');
        except on e : exception do
          MsgInf(e.Message);
        end;
        DMConexao.AdoCon.CommitTrans;

        Screen.Cursor := crDefault;
        msginf('Recarga realizada com sucesso!');

        DMConexao.ExecuteSql('select RENOVACAO_ID from ALIMENTACAO_RENOVACAO where EMPRES_ID=' + QCredAlimempres_id.AsString);

          if not DMConexao.AdoQry.Fields[0].IsNull then begin
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString);

          DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString);
          DMConexao.AdoCon.CommitTrans;
        end;

        qCredAlim.Close;
        qEmpresas.Open;
        pnlDadosRenovacao.Visible := false;
        dbGridAlim.Visible := false;
        pnlDados.Visible := false;
        dbDataRenovacao.Visible:= false;
        Label60.Visible := False;
        dbLkpEmpresas.SetFocus;
      except
        on e:Exception do
        begin
          Screen.Cursor := crDefault;
          MsgErro('Erro ao efetivar lan�amento de cr�ditos');
          DMConexao.AdoCon.RollbackTrans;
         end;
   end;
   end;
end;

procedure TFrmLancamentoExtraAlimentacao.dbLkpEmpresasEnter(
  Sender: TObject);
begin
//  inherited;
//      if EdEmp_ID.Text = '' then
//       dbLkpEmpresas.KeyValue := 0
//  else if QEmpresas.Locate('empres_id',EdEmp_ID.Text,[]) then  begin
//       dbLkpEmpresas.KeyValue := EdEmp_ID.Text;
//       dbLkpEmpresas.SetFocus;
//  end else
//       dbLkpEmpresas.KeyValue := 0;

end;

procedure TFrmLancamentoExtraAlimentacao.rgTipoClick(Sender: TObject);
  var data, data1: String ;
begin
  inherited;
  rbTipoComplemento.ItemIndex := -1;
  data := DateToStr(Now);
  data1 :=  qEmpresasfechamento1.AsString;

  if rgTipo.ItemIndex = 1 then
  begin
    rbTipoComplemento.Visible := True;
//    BitBtn1.Visible := True;
  end else  begin
    rbTipoComplemento.Visible := False;
//    BitBtn1.Visible := false;
  end;
end;

//procedure TFrmLancamentoExtraAlimentacao.FormShow(Sender: TObject);
//begin
//  inherited;
//  EdEmp_ID.SetFocus;
//end;

procedure TFrmLancamentoExtraAlimentacao.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if not qCredAlimDATA_RENOVACAO.IsNull then
  begin
    if Application.MessageBox('Confirma a exclus�o do lan�amento de cr�dito empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      DMConexao.AdoCon.BeginTrans;
      try
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO_SAP WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;
        DMConexao.AdoCon.CommitTrans;

        DMConexao.GravaLog('FCadEmp','RENOVACAO_ID',qCredAlimRENOVACAO_ID.AsString,FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Exclusao',
        qEmpresasEMPRES_ID.AsString,'');

        qCredAlim.Close;
        qCredAlim.Parameters.Items[0].Value := qEmpresasEMPRES_ID.Value;
        QCredAlim.Parameters.Items[1].Value := qEmpresasEMPRES_ID.Value;
        qCredAlim.Open;

        dbDataRenovacao.Text := '';
        dbGridAlim.Visible := false;
        //Panel43.Visible := false;
        rgTipo.ItemIndex := 0;
        dbDataRenovacao.SetFocus;
      except
        on E:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          MsgErro('Erro ao excluir Lan�amento. Erro: '+E.Message+sLineBreak+'Opera��o Cancelada!');
        end;
      end;

    end;
    end
    else
      begin
         msginf('N�o existe lan�amentos a serem exclu�dos!');
         dbDataRenovacao.SetFocus;
      end;
end;

procedure TFrmLancamentoExtraAlimentacao.SpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;


//procedure TFrmLancamentoExtraAlimentacao.tsAlimentacaoShow();
//var valorAbono, valorRenovacao: Currency;
//begin
//  inherited;
//
//    valorAbono := 0;
//    valorRenovacao := 0;
//
//    qCredAlim.Close;
//    qCredAlim.Parameters.Items[0].Value := qEmpresasempres_id.Value;
//    QCredAlim.Parameters.Items[1].Value := qEmpresasempres_id.Value;
//    qCredAlim.Open;
//    rgTipo.ItemIndex := 0;
//    if not qCredAlimDATA_RENOVACAO.IsNull then
//    begin
//
//      if QCredAlimTIPO_CREDITO.AsString = 'R' then begin
//        rgTipo.ItemIndex := 0;
//      end else begin
//        rgTipo.ItemIndex := 1;
//      end;
//
//      dbGridAlim.Visible := true;
//      pnlDadosRenovacao.Visible := true;
//      pnlDados.Visible := true;
//      dbDataRenovacao.Visible := true;
//      Label60.Visible := true;
//
//      while not qCredAlim.Eof do
//      begin
//        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
//        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
//        qCredAlim.Next;
//      end;
//
//      qCredAlim.First;
//    end
//    else
//    begin
//      dbDataRenovacao.Text := '';
//      dbGridAlim.Visible := false;
//      dbGridAlim.Visible := false;
//      pnlDadosRenovacao.Visible := false;
//      pnlDados.Visible := false;
//      dbDataRenovacao.Visible := false;
//      Label60.Visible := false;
//    end;
//  //end;
//
//  lblTotalAbono.Value := valorAbono;
//  lblTotalRenovacao.Value := valorRenovacao;
//  edtSaldoRenovacao.Text := '0,00';
//  edtAbonoMes.Text := '0,00';
//
//  //lblTitulo.Caption := 'Conveniados da empresa: '+ QCadastroNOME.AsString;
//  //dbDataRenovacao.SetFocus;
//
//  Msginf('A empresa '+ qEmpresasnome.AsString +' n�o esta liberada: ');
//      qEmpresas.Close;
//      qEmpresas.Open;
//      QCredAlim.Close;
//      pnlDadosRenovacao.Visible := false;
//      dbGridAlim.Visible := false;
//      pnlDados.Visible := false;
//      dbDataRenovacao.Visible := false;
//      Label60.Visible := False;
//      dbLkpEmpresas.SetFocus;
//
//end;




end.


