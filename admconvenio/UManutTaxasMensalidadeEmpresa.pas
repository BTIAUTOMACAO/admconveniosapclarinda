unit UManutTaxasMensalidadeEmpresa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ADODB, Menus, StdCtrls, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls,
  JvExControls, JvDBLookup, DBCtrls;

type
  TFCadManutTaxasMensalidadeEmpresa = class(TFCad)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    GroupBox2: TGroupBox;
    JvDBGrid1: TJvDBGrid;
    Label8: TLabel;
    DBEmpresa: TJvDBLookupCombo;
    QEmpresa: TADOQuery;
    DSEmpresa: TDataSource;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresausa_cod_importacao: TStringField;
    QEmpresafantasia: TStringField;
    QEmpresafidelidade: TStringField;
    QEmpresaband_id: TIntegerField;
    edtEmpr: TEdit;
    DBCheckBox1: TDBCheckBox;
    QCadastroTAXA_ID: TIntegerField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroFAIXA_INICIO: TIntegerField;
    QCadastroFAIXA_FIM: TIntegerField;
    QCadastroTAXA_VALOR: TBCDField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroAPAGADO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure edtEmprChange(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Valida();
    procedure DBEmpresaChange(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadManutTaxasMensalidadeEmpresa: TFCadManutTaxasMensalidadeEmpresa;

implementation

uses UMenu, DM, UValidacao;

{$R *.dfm}

procedure TFCadManutTaxasMensalidadeEmpresa.FormCreate(Sender: TObject);
begin
  chavepri := 'taxa_id';
  detalhe := 'ID: ';
  QCadastro.Open;
  try
    QEmpresa.Open;
  except
  end;
  FMenu.vFCadManutTaxasMensalidadeEmpresa := True;
  inherited;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.edtEmprChange(Sender: TObject);
begin
   if Trim(edtEmpr.Text) <> '' then begin
       if QEmpresa.Locate('empres_id',edtEmpr.Text,[]) then
          DBEmpresa.KeyValue := edtEmpr.Text
       else begin
          DBEmpresa.ClearValue;
          {AposEmpEstabNull;
          cds.First;
          while not cds.Eof do
            cds.Delete;}
       end;
   end
   else begin
      DBEmpresa.ClearValue;
      {AposEmpEstabNull;
      cds.First;
      while not cds.Eof do
        cds.Delete; }
   end;

end;

procedure TFCadManutTaxasMensalidadeEmpresa.ButBuscaClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  QCadastro.Close;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('SELECT * FROM EMP_TAXAS_BEM_ESTAR');
  QCadastro.Open;
  Screen.Cursor := crDefault;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.QCadastroAfterInsert(
  DataSet: TDataSet);
begin
  {Recuperando o �ltimo ID do conveniado}
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_EMP');
  DMConexao.AdoQry.Open;
  QCadastroTAXA_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;

  {Fim do bloco para recupera��o do ID}
//  QCadastroLIBERADO.AsString          := 'S';

end;

procedure TFCadManutTaxasMensalidadeEmpresa.QCadastroBeforePost(
  DataSet: TDataSet);
begin
  if QCadastro.State in [dsInsert, dsEdit] then
    valida;
  if QCadastro.State = dsInsert then begin
    QCadastroDTCADASTRO.Value   := Now;
    QCadastroOPERCADASTRO.Value := Operador.Nome;
  end;
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;

end;

procedure TFCadManutTaxasMensalidadeEmpresa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFCadManutTaxasMensalidadeEmpresa := False;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.Valida();
begin
  if not (QCadastro.State in [dsInsert, dsEdit]) then
    Exit;

  if fnVerfCompVazioEmTabSheet('Empresa Obrigat�ria.',DBEmpresa)                             then Abort;
  if fnVerfCompVazioEmTabSheet('Informe a faixa inicial.',DBEdit2)                           then Abort;
  if fnVerfCompVazioEmTabSheet('Informe a faixa final.',DBEdit3)                             then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o valor da taxa.',DBEdit4)                           then Abort;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.DBEmpresaChange(
  Sender: TObject);
begin
  inherited;
  edtEmpr.Text := QEmpresaempres_id.AsString;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.TabFichaShow(Sender: TObject);
begin
  inherited;
  edtEmpr.SetFocus;
//  if not QEmpresaempres_id.IsNull then
//  begin
//    edtEmpr.Text := QEmpresaempres_id.AsString;
//  end;
end;

procedure TFCadManutTaxasMensalidadeEmpresa.ButIncluiClick(
  Sender: TObject);
begin
  inherited;
  edtEmpr.Text := '';
end;

procedure TFCadManutTaxasMensalidadeEmpresa.QCadastroAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.IsEmpty then begin
    DBEmpresa.DataField := '';
    DBEmpresa.KeyValue := QCadastroEMPRES_ID.Value;
    DBEmpresa.DataField := 'empres_id';
  end;
end;

end.
