unit UNewGeraLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, DB, ComCtrls, Buttons, strutils,
  ClassImpressao,  dbgrids, TypInfo, QuickRpt, Printers, JvExStdCtrls,
  JvEdit;


type TipoSoma = (Conveniado,Cartao);

  TFNewGeraLista = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    CheckList: TCheckListBox;
    Button1: TButton;
    Button2: TButton;
    ButAbaixo: TButton;
    ButAcima: TButton;
    EdTitulo: TJvEdit;
    ChTotlinhas: TCheckBox;
    ListPrev: TListBox;
    Barra: TStatusBar;
    ButGeraLista: TBitBtn;
    Panel3: TPanel;
    Label4: TLabel;
    lbname: TLabel;
    lbdescri: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbtipo: TLabel;
    Label5: TLabel;
    lbtamanho: TLabel;
    AbreMod: TBitBtn;
    SalvaMod: TBitBtn;
    Bevel1: TBevel;
    DSRels: TDataSource;
    SalvaModComo: TBitBtn;
    Panel4: TPanel;
    Label6: TLabel;
    labmodelo: TLabel;
    ProgressBar1: TProgressBar;
    Label10: TLabel;
    RichEdit1: TRichEdit;
    Button3: TButton;
    SaveDialog1: TSaveDialog;
    SaveDialog2: TSaveDialog;
    cbTamFonte: TComboBox;
    btnAlEsq: TBitBtn;
    btnAlCenter: TBitBtn;
    btnAlDir: TBitBtn;
    cbFontes: TComboBox;
    Label3: TLabel;
    cbFonteEstilo: TCheckBox;
    lblRodape: TLabel;
    cbRodape: TComboBox;
    Label11: TLabel;
    cd: TColorDialog;
    pnlCorPar: TPanel;
    btnCorPar: TButton;
    btnCorImpar: TButton;
    pnlCorImpar: TPanel;
    edttamanho: TEdit;
    procedure ButAcimaClick(Sender: TObject);
    procedure ButAbaixoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckListClickCheck(Sender: TObject);
    procedure CheckListKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTituloEnter(Sender: TObject);
    procedure EdTituloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTituloClick(Sender: TObject);
    procedure CheckListClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AbreModClick(Sender: TObject);
    procedure SalvaModComoClick(Sender: TObject);
    procedure SalvaModClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButGeraListaClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbTamFonteChange(Sender: TObject);
    procedure btnAlEsqClick(Sender: TObject);
    procedure btnAlCenterClick(Sender: TObject);
    procedure btnAlDirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbFontesChange(Sender: TObject);
    procedure cbFonteEstiloClick(Sender: TObject);
    procedure cbRodapeChange(Sender: TObject);
    procedure pnlCorParClick(Sender: TObject);
    procedure btnCorParClick(Sender: TObject);
    procedure btnCorImparClick(Sender: TObject);
    procedure edttamanhoKeyPress(Sender: TObject; var Key: Char);
    procedure edttamanhoChange(Sender: TObject);
  private
    { Private declarations }
    procedure MudarCorPanel(pnl: TPanel);
    procedure MudaEstilo(Aplicar : Boolean);
    procedure setstatustext(texto:string);
    property statustext : string write setstatustext;
    procedure MontarColunas;
    function tipocampo(tipo:TFieldType):string;
    procedure importamodelo;
    procedure salvamodelo;
    procedure salvamodelocomo;
    function  criamodelo:string;
    procedure validarCheckList;
    function CalcularDebito(TpSoma:tiposoma;ID:Integer;Receita:Char):Currency;
    function GetCampoEspecificoCartao(Campo:String;var Valor: String): Boolean;
    procedure CriarCabecalho(Imp:TImpres);
    function CriarCampoAuxiliar(FieldName, DisplayName:String): TField;
    function SelectItemByFieldName(FieldName:String):TField;
    function CalcularDebitoTotal(Tipo:TipoSoma;Conv_ID,Cart_ID:Integer):Currency;
    function CalcularDebitoProxFecha(Tipo:TipoSoma;Conv_ID,Cart_ID:Integer):Currency;

  public
    { Public declarations }
    Dados  : TDataSet;
    Query  : TDataSet;
    Grade  : TDBGrid;
    Selecteds : TList;
    widths : array of Integer;
    TelaAtual: String;
    procedure CriarCampos;
  end;

var
  FNewGeraLista: TFNewGeraLista;


implementation

uses impressao, DateUtils, USelRelModelo, UChangeLog, UExporta,
  cartao_util, {qRelSimples,} URotinasGrids{, qRelSimples};

{$R *.dfm}

procedure TFNewGeraLista.MudarCorPanel(pnl: TPanel);
begin
  if cd.Execute then
    pnl.Color := cd.Color;
end;

procedure TFNewGeraLista.MudaEstilo(Aplicar : Boolean);
begin
  if Aplicar then begin
    cbFontes.Font.Name   := cbFontes.Text;
    cbFontes.Font.Size   := StrToInt(cbTamFonte.Text);
    cbTamFonte.Font.Name := cbFontes.Text;
    cbTamFonte.Font.Size := StrToInt(cbTamFonte.Text);
  end else begin
    cbFontes.Font.Name   := 'MS Sans Serif';
    cbTamFonte.Font.Name := 'MS Sans Serif';
    cbFontes.Font.Size   := 8;
    cbTamFonte.Font.Size := 8;
  end;
end;

procedure TFNewGeraLista.setstatustext(texto:string);
begin
  barra.Panels[0].Text := texto;
end;

procedure TFNewGeraLista.ButAcimaClick(Sender: TObject);
var i : integer;
begin
  if CheckList.ItemIndex > 0 then begin
    i := CheckList.ItemIndex;
    CheckList.Items.Move(CheckList.ItemIndex,CheckList.ItemIndex-1);
    CheckList.Selected[i-1] := True;
    CheckList.SetFocus;
  end;
  MontarColunas;
end;

procedure TFNewGeraLista.ButAbaixoClick(Sender: TObject);
var i : integer;
begin
  if CheckList.ItemIndex < CheckList.Items.Count-1 then begin
    i := CheckList.ItemIndex;
    CheckList.Items.Move(CheckList.ItemIndex,CheckList.ItemIndex+1);
    CheckList.Selected[i+1] := True;
    CheckList.SetFocus;
  end;
  MontarColunas;
end;

procedure TFNewGeraLista.Button1Click(Sender: TObject);
var i : integer;
begin
  For i := 0 to CheckList.Items.Count-1 do
    CheckList.Checked[i] := True;
  MontarColunas;
end;

procedure TFNewGeraLista.Button2Click(Sender: TObject);
var i : integer;
begin
  For i := 0 to CheckList.Items.Count-1 do
    CheckList.Checked[i] := False;
  MontarColunas;
end;

procedure TFNewGeraLista.CheckListClickCheck(Sender: TObject);
begin
  MontarColunas;
end;

procedure TFNewGeraLista.MontarColunas;
{var i, Tamanho : integer;
    Campo : TField;  }
begin
  {//ALTERAR AQUI!! A APRESENTA��O DOS DADOS...
  Tamanho := 0;
  Selecteds.Clear;
  qrRelSimples := TqrRelSimples.Create(Self);
  for i := 0 to CheckList.Count - 1 do begin
    if CheckList.Checked[i] then begin
      Campo := TField(CheckList.Items.Objects[i]);
      if Campo.Size >= 45 then
        Tamanho := Tamanho + TamanhoCelulaEmPixel(Campo.Size-15,cbFontes.Text,strtoint(cbTamFonte.text))
      else if Campo.Size >= 35 then
        Tamanho := Tamanho + TamanhoCelulaEmPixel(Campo.Size-10,cbFontes.Text,strtoint(cbTamFonte.text))
      else
        Tamanho := Tamanho + TamanhoCelulaEmPixel(Campo.Size,cbFontes.Text,strtoint(cbTamFonte.text));
      //Tamanho := iif(Campo.DisplayWidth > length(Campo.DisplayLabel),Campo.DisplayWidth,length(Campo.DisplayLabel));
      //Tamanho := iif(Tamanho > length(Campo.FieldName),Tamanho,length(Campo.FieldName));
      //if IsNumericField(Campo.DataType) then begin
         //EdColun.Text := EdColun.Text + '  ' + Direita(Campo.DisplayLabel,' ',Tamanho);
      //end
      //else begin
         //EdColun.Text := EdColun.Text + '  ' + Preenche(Campo.DisplayLabel,' ',Tamanho);
      //end;
      //Campo.DisplayWidth := Tamanho; //define para ser usado posteriormente.
      Selecteds.Add(Campo);
    end;
  end;
  if qrRelSimples.QRBand1.Width < Tamanho then begin
    qrRelSimples.Page.Orientation := poLandscape;
    if qrRelSimples.QRBand1.Width < Tamanho then
      MsgInf('Aten��o sua listagem estourou o numero m�ximo da folha,'+#13+' � poss�vel que alguns dados n�o sejam apresentados corretamente.');
  end;
  FreeAndNil(qrRelSimples); }//comentado ariane
end;

procedure TFNewGeraLista.CheckListKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = vk_up   then if ssCtrl in Shift then begin
   Key := 0;
   ButAcima.Click;
end;
if Key = vk_down then if ssCtrl in Shift then begin
   Key := 0;
   ButAbaixo.Click;
end;

end;

procedure TFNewGeraLista.EdTituloEnter(Sender: TObject);
begin
  statustext := 'Col '+IntToStr(TEdit(Sender).SelStart);
end;

procedure TFNewGeraLista.EdTituloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  statustext := 'Col '+IntToStr(TEdit(Sender).SelStart);
end;

procedure TFNewGeraLista.EdTituloClick(Sender: TObject);
begin
  statustext := 'Col '+IntToStr(TEdit(Sender).SelStart);
end;

procedure TFNewGeraLista.CheckListClick(Sender: TObject);
var F : TField;
begin
  F := TField(CheckList.Items.Objects[CheckList.ItemIndex]);
  lblRodape.Visible := (F is TNumericField) or (F is TIntegerField);
  cbRodape.Visible  := (F is TNumericField) or (F is TIntegerField);
  lbdescri.Caption  :=  F.DisplayLabel;
  lbtamanho.Caption := IntToStr(F.DisplayWidth);
  edtTamanho.Text   := IntToStr(F.DisplayWidth);
  lbtipo.Caption    := tipocampo(F.DataType);
  lbname.Caption    := F.FieldName;
  case F.Tag of
    1: cbRodape.ItemIndex := 1;
    2: cbRodape.ItemIndex := 2;
    3: cbRodape.ItemIndex := 3;
    4: cbRodape.ItemIndex := 4;
    else cbRodape.ItemIndex := 0;
  end;
end;

procedure TFNewGeraLista.cbRodapeChange(Sender: TObject);
var F : TField;
begin
  F := TField(CheckList.Items.Objects[CheckList.ItemIndex]);
  if cbRodape.Text = 'SOMA'        then F.Tag := 1
  else if cbRodape.Text = 'M�DIA'  then F.Tag := 2
  else if cbRodape.Text = 'M�XIMO' then F.Tag := 3
  else if cbRodape.Text = 'M�NIMO' then F.Tag := 4
  else F.Tag := 0;
end;

function TFNewGeraLista.tipocampo(tipo:TFieldType):string;
begin
  if tipo in [ftString,ftMemo,ftFixedChar, ftWideString]          then Result := 'Texto';
  if tipo in [ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint] then Result := 'Num�rico Inteiro';
  if tipo in [ftFloat, ftCurrency, ftBCD,ftFMTBcd]                then Result := 'Num�rico Decimal';
  if tipo in [ftDate, ftDateTime]                                 then Result := 'Data';
end;

procedure TFNewGeraLista.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_escape then Close;
  if Key = vk_F1 then begin
    FChangeLog := TFChangeLog.Create(Self);
    FChangeLog.Caption := 'Ajuda para Configura��o de Listagens.';
    FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
    FChangeLog.ShowModal;
    FChangeLog.Free;
  end;
end;

procedure TFNewGeraLista.AbreModClick(Sender: TObject);
var Sql : TStrings;
begin
  Query.Close;
  Sql := TStringList.Create;
  Sql.Add(' Select * from conf_rels where cadastro = "'+TelaAtual+'"');
  SetObjectProp(Query,'SQL',Sql);
  SetPropValue(Query,'RequestLive',True);
  DSRels.DataSet := Query;
  Query.Open;
  if not Query.IsEmpty then begin
    FSelRelModelo := TFSelRelModelo.Create(self);
    FSelRelModelo.ShowModal;
    if FSelRelModelo.ModalResult = mrOk then
      ImportaModelo;
    FSelRelModelo.Free;
    SalvaMod.Enabled := True;
    labmodelo.Caption := Query.FieldByName('DESCRICAO').AsString;
  end else
    MsgInf('N�o h� modelos de listagens salvos para esta Grade.');
  Query.Close;
end;

procedure TFNewGeraLista.importamodelo;
var i : integer;
    campo : string;
    Lista : TStringList;
begin
  CriarCampos;
  Lista                 := TStringList.Create;
  Lista.Text            := Query.FieldByName('LAYOUT').AsString;
  EdTitulo.Text         := Lista.Values['titulo'];
  ChTotlinhas.Checked   := StrToBool(Lista.Values['ChTotlinhas']);
  pnlCorPar.Color       := StringToColor(Lista.Values['CorLinhaPar']);
  pnlCorImpar.Color     := StringToColor(Lista.Values['CorLinhaImpar']);
  cbfontes.itemindex    := StrToInt(Lista.Values['EstiloFonte']);
  cbTamFonte.itemindex  := StrToInt(Lista.Values['TamanhoFonte']);
  cbFonteEstilo.Checked := StrToBool(Lista.Values['AplicarFonteEstilo']);
  Selecteds.Clear;
  for i := 0 to pred(Lista.Count) do begin
     if AnsiContainsText(Lista[i],'campo=') then begin
        try
        campo := copy(Lista[i],Pos('=',Lista[i])+1,(Pos('#',Lista[i])-(Pos('=',Lista[i])+1)));
        Selecteds.Add(SelectItemByFieldName(campo));
        SelectItemByFieldName(campo).Tag := StrToInt(Copy(Lista[i],Pos('#',Lista[i])+1,Length(Lista[i])));
        except
        end;
     end;
  end;
  Lista.Free;
end;

procedure TFNewGeraLista.salvamodelo;
var Sql : TStrings;
begin
   Query.Close;
   SQL := TStringList.Create;
   Sql.Add(' Select * from conf_rels where cadastro = "'+TelaAtual+'" and descricao = "'+labmodelo.Caption+'"');
   SetObjectProp(Query,'SQL',Sql);
   SetPropValue(Query,'RequestLive',True);
   Query.Open;
   Query.Edit;
   Query.FieldByName('LAYOUT').AsString    := criamodelo;
   Query.Post;
   Query.Close;
   MsgInf('Modelo Salvo com Sucesso!')
end;

procedure TFNewGeraLista.salvamodelocomo;
var
  NewModelo: string; Sql : TStrings;
  OK: Boolean;
begin
ok := InputQuery('Novo Modelo','Informe a descri��o do novo modelo.',NewModelo);
if ok then begin
   if Trim(NewModelo) = '' then begin
      msgInf('Descri��o do modelo obrigat�ria.');
      Exit;
   end;
   Query.Close;
   SetPropValue(Query,'RequestLive',True);
   SQL := TStringList.Create;
   Sql.Add(' Select * from conf_rels where cadastro = "'+TelaAtual+'"');
   SetObjectProp(Query,'SQL',Sql);
   SetPropValue(Query,'RequestLive',True);
   Query.Open;
   Query.Append;
   Query.FieldByName('CADASTRO').AsString  := TelaAtual;
   Query.FieldByName('DESCRICAO').AsString := NewModelo;
   Query.FieldByName('LAYOUT').AsString    := criamodelo;
   Query.Post;
   Query.Close;
   labmodelo.Caption := NewModelo;
end;

end;

function TFNewGeraLista.criamodelo:string;
var lista : TStringList;
i : integer;
begin
lista := TStringList.Create;
for i := 0 to CheckList.Items.Count - 1 do
   if CheckList.Checked[i] then
     lista.Add('campo='+TField(CheckList.Items.Objects[i]).FieldName+'#'+inttostr(TField(CheckList.Items.Objects[i]).Tag));
lista.Add('titulo='+EdTitulo.Text);
lista.Add('ChTotlinhas='+BoolToStr(ChTotlinhas.Checked));
lista.Add('CorLinhaPar='+ColorToString(pnlCorPar.color));
lista.Add('CorLinhaImpar='+ColorToString(pnlCorImpar.color));
lista.Add('EstiloFonte='+IntToStr(cbfontes.itemindex));
lista.Add('TamanhoFonte='+IntToStr(cbTamFonte.itemindex));
lista.Add('AplicarFonteEstilo='+BoolToStr(cbFonteEstilo.Checked));
Result := lista.Text;
lista.Free;
end;

procedure TFNewGeraLista.SalvaModComoClick(Sender: TObject);
begin
salvamodelocomo;
SalvaMod.Enabled := True;
end;

procedure TFNewGeraLista.SalvaModClick(Sender: TObject);
begin
salvamodelo;
end;

procedure TFNewGeraLista.FormClose(Sender: TObject; var Action: TCloseAction);
var i : Integer;
begin
  Query.Close;
  Selecteds.Free;
  for i := 0 to Length(widths)-1 do begin
    grade.Fields[i].DisplayWidth := widths[i];
  end;
  Grade.DataSource.DataSet.EnableControls;
end;

procedure TFNewGeraLista.ButGeraListaClick(Sender: TObject);
{var
  I,J : Integer;
  SL : TStringList;    }
begin
    {validarCheckList;
    Barra.Panels[1].Text := 'Gerando listagem....';
    Barra.Repaint;
    if not dados.IsEmpty then begin
       Screen.Cursor := crHourGlass;
       SL := TStringList.Create;
       for I := 0 to dados.FieldCount-1 do begin
         if dados.Fields[I].Visible then
           SL.Append('S;')
         else
           SL.Append('N;');
       dados.Fields[I].Visible := False;
       end;
       for I := 0 to Grade.Columns.Count-1 do
         for J := 0 to CheckList.Count-1 do
           if CheckList.Items[J] = Grade.Columns[I].Title.Caption then begin
             dados.FieldByName(Grade.Columns[I].FieldName).Visible := (Grade.Columns[I].Visible) and (CheckList.Checked[J]);
             Break;
           end;
       qrRelSimples := TqrRelSimples.Create(Self);
       //Dados.DisableControls;
       qrRelSimples.MontaRelSimples(dados,ChTotlinhas.Checked,EdTitulo.Text,cbFontes.Text,strtoint(cbTamFonte.text),EdTitulo.Alignment, pnlCorImpar.Color, pnlCorPar.Color);
       Screen.Cursor := crDefault;
       qrRelSimples.Preview;
       //qrRelSimples.Destroy;
       FreeAndNil(qrRelSimples);
       //Dados.EnableControls;
       for I := 0 to SL.Count-1 do
         dados.Fields[I].Visible := SL.Strings[I] = 'S';
       FreeAndNil(SL);
    end else
      MsgErro(' N�o h� dados para serem exibidos. ');  }//comentado ariane
end;

procedure TFNewGeraLista.CriarCampos;
var i : integer;
begin
   CheckList.Clear;
   For i := 0 to Grade.Columns.Count-1 do begin
      if not AnsiContainsText('apagado,senha',Grade.Columns[i].Field.FieldName) then begin
         Grade.Columns[i].Field.DisplayLabel := Grade.Columns[i].Title.Caption; //S� para usar o DisplayLabel com seguran�a
         CheckList.Items.AddObject(Grade.Columns[i].Title.Caption,Grade.Columns[i].Field);
         widths[i] := Grade.Columns[i].Field.DisplayWidth;
      end;
   end;
   if AnsiContainsText('FCadConv,FCadCartoes', TelaAtual) then begin
      CheckList.Items.AddObject('D�bito total',CriarCampoAuxiliar('DebTotal','D�bito total'));
      CheckList.Items.AddObject('D�b Pr�x.fecha.',CriarCampoAuxiliar('DebFecha','D�b Pr�x.fecha.'));
      CheckList.Items.AddObject('Pr�x.fecha.c/rec',CriarCampoAuxiliar('DebFechaComRec','Pr�x.fecha.c/rec'));
      CheckList.Items.AddObject('Pr�x.fecha.s/rec',CriarCampoAuxiliar('DebFechaSemRec','Pr�x.fecha.s/rec'));
   end;
end;

function TFNewGeraLista.CalcularDebito(TpSoma:tiposoma;ID:Integer;Receita:Char):currency;
var SQL : TStrings;
begin
   Result := 0;
   SQL := TStringList.Create;
   case TpSoma of
     Conveniado:
        SQL.Add('Select saldo from  SALDO_CONV_REC('+ IntToStr(ID)+',"'+Receita+'"  )');
     Cartao: begin
        SQL.Add('select sum(contacorrente.debito-contacorrente.credito) from contacorrente where coalesce(contacorrente.baixa_conveniado,''N'') <> ''S''');
        SQL.Add(' and contacorrente.cartao_id = ' + IntToStr(ID));
        SQL.Add(' and contacorrente.receita = ''' + Receita + '''');
        SQL.Add(' and contacorrente.data_fecha_emp = (select *  GET_PROX_FECHA('+Dados.FieldByName('empres_id').AsString + ')');
     end;
   end;
   Query.Close;
   SetObjectProp(Query,'SQL',SQL);
   Query.Open;
   Result := Query.Fields[0].AsCurrency;
   Query.Close;
end;


procedure TFNewGeraLista.Button3Click(Sender: TObject);
var i : integer;
linha, valor, relname : string;
marka : TBookmark;
Lista : TStringList;
Campo : TField;
begin
validarCheckList;
FExporta := TFExporta.create(self);
FExporta.ShowModal;
if FExporta.ModalResult = mrOk then begin
   Screen.Cursor := crHourGlass;
   Barra.Panels[1].Text := 'Gerando arquivo....';
   Barra.Repaint;
   Lista := TStringList.Create;
   if FExporta.ckprimlinha.Checked then begin
      linha := '';
      for i := 0 to pred(Selecteds.Count) do begin
          if FExporta.CheckBox1.Checked then
             valor := TField(Selecteds[i]).FieldName+FExporta.EdChar.Text
          else begin
             if IsNumericField(TField(Selecteds[i]).DataType) then
                valor := Direita(TField(Selecteds[i]).FieldName,' ',TField(Selecteds[i]).DisplayWidth)
             else
                valor := Preenche(TField(Selecteds[i]).FieldName,' ',TField(Selecteds[i]).DisplayWidth);
             valor := valor + ' ';
          end;
          linha := linha + valor;
      end;
      Lista.Add(linha);
   end;
   if not dados.IsEmpty then begin
      marka := dados.GetBookmark;
      //dados.DisableControls;
      dados.First;
      ProgressBar1.Position := 0;
      ProgressBar1.Max      := DaDos.RecordCount;
      while not dados.Eof do begin
          linha := '';
          for i := 0 to Selecteds.Count - 1 do begin
             Campo := TField(Selecteds[i]);
             if not GetCampoEspecificoCartao(Campo.FieldName,valor) then begin
                if Campo.DataType in [ftDate,ftDateTime] then begin
                   FExporta.edmaskdata.Text := StringReplace(FExporta.edmaskdata.Text,'a','y',[rfReplaceAll]);
                   valor := FormatDateTime(FExporta.edmaskdata.Text,Campo.AsDateTime);
                end
                else if IsFloatField(Campo.DataType) then
                   valor := FormatFloat('#####0.00', Campo.AsFloat)
                else
                   valor := Campo.AsString;
             end
             else begin
                valor := StringReplace(valor,'.','',[rfReplaceAll]);
             end;
             valor := Trim(valor);
             if IsFloatField(Campo.DataType) then begin
                case FExporta.RGDet.ItemIndex of
                    0 : valor := SoNumero(valor);
                    1 : valor := StringReplace(valor,DecimalSeparator,',',[rfReplaceAll]);
                    2 : valor := StringReplace(valor,DecimalSeparator,'.',[rfReplaceAll]);
                end;
             end
             else if IsStringField(Campo.DataType) and FExporta.CKAspas.Checked then
                valor := '"'+valor+'"';
             if FExporta.CheckBox1.Checked then
                valor := valor+FExporta.EdChar.Text
             else begin
                if IsNumericField(Campo.DataType) then
                   valor := Direita(valor,' ',Campo.DisplayWidth)
                else
                   valor := Preenche(valor,' ',Campo.DisplayWidth);
                valor := valor + ' ';   
             end;
             linha := linha + valor;
          end;
          if FExporta.CheckBox1.Checked then
             linha := Trim(linha);
          Lista.Add(linha);
          dados.Next;
          ProgressBar1.Position := ProgressBar1.Position + 1;
          Application.ProcessMessages;
      end;
      dados.GotoBookmark(marka);
      dados.FreeBookmark(marka);
      //dados.EnableControls;
      if SaveDialog1.Execute then
         lista.SaveToFile(SaveDialog1.FileName);
      Lista.Free;
      Screen.Cursor := crDefault;
      Barra.Panels[1].Text := 'Listagem conclu�da!';
      Barra.Repaint;
   end
   else msgInf(' N�o h� dados para serem exibidos. ');
end;

end;

procedure TFNewGeraLista.validarCheckList;
var i : integer;
begin
for i := 0 to pred(CheckList.Items.Count) do begin
   if CheckList.Checked[i] then
      exit;
end;
MsgErro('Selecione algum campo!');
CheckList.SetFocus;
Abort;
end;


function  TFNewGeraLista.GetCampoEspecificoCartao(Campo:String;var Valor:String):Boolean;
var _valor : Currency;
begin
   Result := True;
   _valor := 0;
   if Campo = 'DebTotal' then begin // campos adicionais para conveniados.
      if TelaAtual = 'FCadConv' then
         _valor := CalcularDebitoTotal(Conveniado,dados.FieldByName('conv_id').AsInteger,0)
      else
         _valor := CalcularDebitoTotal(Cartao,dados.FieldByName('conv_id').AsInteger,dados.FieldByName('cartao_id').AsInteger);
   end
   else if Campo = 'DebFecha' then begin // campos adicionais para conveniados.
      if TelaAtual = 'FCadConv' then    // soma de conveniados.
         _valor := CalcularDebitoProxFecha(Conveniado,dados.FieldByName('conv_id').AsInteger,0)
      else
         _valor := CalcularDebitoProxFecha(Cartao,dados.FieldByName('conv_id').AsInteger,dados.FieldByName('cartao_id').AsInteger)
   end
   else if Campo = 'DebFechaComRec' then begin // campos adicionais para conveniados.
      if TelaAtual = 'FCadConv' then    // soma de conveniados.
        _valor := CalcularDebito(Conveniado, dados.FieldByName('conv_id').AsInteger, 'S')
      else
        _valor := CalcularDebito(Cartao, dados.FieldByName('cartao_id').AsInteger, 'S');
   end
   else if campo = 'DebFechaSemRec' then begin // campos adicionais para conveniados.
      if TelaAtual = 'FCadConv' then    // soma de conveniados.
        _valor := CalcularDebito(Conveniado, dados.FieldByName('conv_id').AsInteger, 'N')
      else
        _valor := CalcularDebito(Cartao, dados.FieldByName('cartao_id').AsInteger, 'N');
   end
   else
      Result := False;
   if Result then begin
      Valor := Direita(FormatDinBR(_valor),' ',10);
   end;
end;


function TFNewGeraLista.CriarCampoAuxiliar(FieldName,DisplayName:String):TField;
begin
   Result              := TCurrencyField.Create(Self);
   Result.DisplayLabel := DisplayName;
   Result.FieldName    := FieldName;
   Result.Size         := 0;
   TNumericField(Result).DisplayFormat := '###,###,##0.00';
end;

procedure TFNewGeraLista.CriarCabecalho(Imp:TImpres);
begin
end;

procedure TFNewGeraLista.FormShow(Sender: TObject);
begin
  Selecteds := TList.Create;
  //SetLength(widths,Grade.FieldCount);  
end;

function TFNewGeraLista.SelectItemByFieldName(FieldName:String):TField;
var i : integer;
begin
  for i := 0 to Pred(CheckList.Items.Count) do
    if TField(CheckList.Items.Objects[i]).FieldName = FieldName then begin
       CheckList.Checked[i] := True;
       Result := TField(CheckList.Items.Objects[i]);
    end;
end;

function TFNewGeraLista.CalcularDebitoTotal(Tipo:TipoSoma;Conv_ID,Cart_ID:Integer):Currency;
var Sql : TStrings;
begin
try
   Result := 0;
   Sql := TStringList.Create;
   if Tipo = Conveniado then    // soma de conveniados.
      SQL.Text := ' select sum(saldo_mes) from saldo_conv(' +inttostr(Conv_ID)+ ')'
   else // soma de cartoes.
      SQL.Text := ' select sum(saldo_mes) from saldo_cart(' + inttostr(conv_id)+ ') where saldo_cart.cartao_id = '+inttostr(Cart_ID);
   Query.Close;
   SetObjectProp(Query,'SQL',Sql);
   Query.Open;
   if Query.IsEmpty then
      Result := 0
   else
      Result := Query.Fields[0].AsCurrency;
   Query.Close;
except on E:Exception do
   raise Exception.Create('Erro ao calcular debito total, erro: '+e.Message);
end;

end;

function TFNewGeraLista.CalcularDebitoProxFecha(Tipo:TipoSoma;Conv_ID,Cart_ID:Integer):Currency;
var Sql : TStrings; DataFecha : String;
begin
try
   Result := 0;
   Sql := TStringList.Create;
   Sql.Text := 'Select datafecha from get_prox_fecha((select empres_id from conveniados where conv_id = '+inttostr(Conv_ID)+'))';
   Query.Close;
   SetObjectProp(Query,'SQL',Sql);
   Query.Open;
   DataFecha := FormatDataIB(Query.Fields[0].AsDateTime);
   if Tipo = Conveniado then    // soma de conveniados.
      SQL.Text := ' select saldo_mes from saldo_conv(' +inttostr(Conv_ID)+ ') where fechamento = '+DataFecha
   else // soma de cartoes.
      SQL.Text := ' select saldo_mes from saldo_cart(' + inttostr(conv_id)+ ') where fechamento = '+DataFecha+' and saldo_cart.cartao_id = '+inttostr(Cart_ID);
   Query.Close;
   SetObjectProp(Query,'SQL',Sql);
   Query.Open;
   if Query.IsEmpty then
      Result := 0
   else
      Result := Query.Fields[0].AsCurrency;
   Query.Close;
except on E:Exception do
   raise Exception.Create('Erro ao calcular debito prox. fecha, erro: '+e.Message);
end;

end;

procedure TFNewGeraLista.cbTamFonteChange(Sender: TObject);
begin
  MontarColunas;
  if cbFonteEstilo.Checked then
    MudaEstilo(cbFonteEstilo.Checked);
end;

procedure TFNewGeraLista.btnAlEsqClick(Sender: TObject);
begin
  EdTitulo.Alignment := taLeftJustify;
end;

procedure TFNewGeraLista.btnAlCenterClick(Sender: TObject);
begin
  EdTitulo.Alignment := taCenter;
end;

procedure TFNewGeraLista.btnAlDirClick(Sender: TObject);
begin
  EdTitulo.Alignment := taRightJustify;
end;

procedure TFNewGeraLista.FormCreate(Sender: TObject);
var I : Integer;
begin
  cbFontes.Items := Screen.Fonts;
  for I := 0 to cbFontes.Items.Count-1 do
    if cbFontes.Text = 'Courier New' then
      Break;
end;

procedure TFNewGeraLista.cbFontesChange(Sender: TObject);
begin
  if cbFonteEstilo.Checked then
    MudaEstilo(cbFonteEstilo.Checked);
end;

procedure TFNewGeraLista.cbFonteEstiloClick(Sender: TObject);
begin
  MudaEstilo(cbFonteEstilo.Checked);
end;

procedure TFNewGeraLista.pnlCorParClick(Sender: TObject);
begin
  MudarCorPanel(TPanel(Sender));
end;

procedure TFNewGeraLista.btnCorParClick(Sender: TObject);
begin
  MudarCorPanel(pnlCorPar);
end;

procedure TFNewGeraLista.btnCorImparClick(Sender: TObject);
begin
  MudarCorPanel(pnlCorImpar);
end;

procedure TFNewGeraLista.edttamanhoKeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFNewGeraLista.edttamanhoChange(Sender: TObject);
begin
  if TEdit(Sender).Text = '' then
    TEdit(Sender).Text := '0';
  TField(CheckList.Items.Objects[CheckList.ItemIndex]).DisplayWidth := StrToInt(TEdit(Sender).Text);
end;

end.
