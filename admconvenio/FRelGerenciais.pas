unit FRelGerenciais;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, ADODB, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  Buttons, ComCtrls, ExtCtrls, JvExMask, JvToolEdit,frxGradient,ClassImpressao,
  U1,Menus, Printers, JvMemoryDataset, frxClass, frxDBSet;

type
  TFGerenciais = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    GroupBox2: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    BitBtn1: TBitBtn;
    bntGerarPDF: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    QCredenciados: TADOQuery;
    dsCredenciados: TDataSource;
    Label1: TLabel;
    edtDias: TEdit;
    Label2: TLabel;
    cbOrdena: TComboBox;
    QCredenciadoscred_id: TIntegerField;
    QCredenciadosnome: TStringField;
    QCredenciadosfantasia: TStringField;
    QCredenciadosTELEFONE1: TStringField;
    QCredenciadosemail: TStringField;
    Panel1: TPanel;
    txtResult: TLabel;
    MDCredenciados: TJvMemoryData;
    MDCredenciadoscred_id: TIntegerField;
    MDCredenciadosnome: TStringField;
    MDCredenciadosfantasia: TStringField;
    MDCredenciadoscidade: TStringField;
    MDCredenciadosemail: TStringField;
    MDCredenciadostelefone1: TStringField;
    MDCredenciadosbairro: TStringField;
    MDCredenciadosmarcado: TBooleanField;
    frxReport1: TfrxReport;
    dbCredenciados: TfrxDBDataset;
    QCredenciadosBAIRRO: TStringField;
    QCredenciadoscidade: TStringField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure QCredenciadosBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure QCredenciadosAfterOpen(DataSet: TDataSet);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGerenciais: TFGerenciais;

implementation

uses cartao_util;

{$R *.dfm}

procedure TFGerenciais.ButListaEmpClick(Sender: TObject);
var ordenaPor : String;
begin

  if edtDias.Text = '' then
  begin
    MsgInf('Voc� deve preencher a quantidade de dias desejada');
    Exit;
    edtDias.SetFocus;
  end;
  txtResult.Caption := 'Foram encontrados: ';
  txtResult.Visible := False;
  QCredenciados.Close;
  QCredenciados.SQL.Clear;
  QCredenciados.SQL.Add('SELECT distinct cred.cred_id as cred_id,cred.nome,cred.fantasia, UPPER(ba.descricao) as BAIRRO, UPPER(ci.nome) as cidade,');
  QCredenciados.SQL.Add('cred.TELEFONE1, cred.email');
  QCredenciados.SQL.Add('FROM credenciados cred');
  QCredenciados.SQL.Add('INNER JOIN bairros ba on (ba.bairro_id = cred.bairro) ');
  QCredenciados.SQL.Add('INNER JOIN cidades ci on (ci.cid_id = cred.cidade) ');
  QCredenciados.SQL.Add('where cred.CRED_ID NOT in ');
  QCredenciados.SQL.Add('(select CRED_ID from CONTACORRENTE where data between dateadd(dd,-:dias,getdate()) and getdate())');
  QCredenciados.SQL.Add('and apagado = ''N'' AND LIBERADO = ''S''');
  if cbOrdena.ItemIndex = 2 then
    ordenaPor := 'fantasia'
  else if cbOrdena.ItemIndex = 1 then
    ordenaPor := 'nome'
  else
    ordenaPor := 'cred_id';
  QCredenciados.SQL.Add('ORDER BY '+ordenaPor+'');
  QCredenciados.Open;

  txtResult.Caption := txtResult.Caption + InttoStr(QCredenciados.RecordCount) + ' registros.';
  txtResult.Visible := True;

  QCredenciados.First;
  MDCredenciados.Open;
  MDCredenciados.EmptyTable;
  MDCredenciados.DisableControls;
  while(not QCredenciados.Eof) do
  begin
    MDCredenciados.Append;
    MDCredenciadoscred_id.AsInteger  := QCredenciadoscred_id.AsInteger;
    MDCredenciadosnome.AsString      := QCredenciadosnome.AsString;
    MDCredenciadosfantasia.AsString   := QCredenciadosfantasia.AsString;
    MDCredenciadosbairro.AsString    := QCredenciadosBAIRRO.AsString;
    MDCredenciadoscidade.AsString    := QCredenciadoscidade.AsString;
    MDCredenciadosemail.AsString     := QCredenciadosemail.AsString;
    MDCredenciadostelefone1.AsString := QCredenciadosTELEFONE1.AsString;
    MDCredenciadosmarcado.AsBoolean  := False;
    QCredenciados.Next
  end;
  MDCredenciados.First;
  MDCredenciados.EnableControls;
end;

procedure TFGerenciais.QCredenciadosBeforeOpen(DataSet: TDataSet);
begin

  QCredenciados.Parameters[0].Value := StrToInt(edtDias.Text);

end;

procedure TFGerenciais.FormShow(Sender: TObject);
begin
     edtDias.SetFocus;
end;

procedure TFGerenciais.QCredenciadosAfterOpen(DataSet: TDataSet);
begin
  if QCredenciados.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum credenciado sem vendas no per�odo selecionado.');
    edtDias.SetFocus;
  end;
end;

procedure TFGerenciais.ButMarcaDesmEmpClick(Sender: TObject);
begin
   if MDCredenciados.IsEmpty then Exit;
   MDCredenciados.Edit;
   MDCredenciadosmarcado.AsBoolean := not MDCredenciadosmarcado.AsBoolean;
   MDCredenciados.Post;
end;

procedure TFGerenciais.JvDBGrid1DblClick(Sender: TObject);
begin
  ButMarcaDesmEmpClick(nil);
end;

procedure TFGerenciais.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MDCredenciados.IsEmpty then Exit;
  MDCredenciados.DisableControls;
  marca := MDCredenciados.GetBookmark;
  MDCredenciados.First;
  while not MDCredenciados.eof do begin
    MDCredenciados.Edit;
    MDCredenciadosMarcado.AsBoolean := true;
    MDCredenciados.Post;
    MDCredenciados.Next;
  end;
  MDCredenciados.GotoBookmark(marca);
  MDCredenciados.FreeBookmark(marca);
  MDCredenciados.EnableControls;
end;

procedure TFGerenciais.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MDCredenciados.IsEmpty then Exit;
  MDCredenciados.DisableControls;
  marca := MDCredenciados.GetBookmark;
  MDCredenciados.First;
  while not MDCredenciados.eof do begin
    MDCredenciados.Edit;
    MDCredenciadosMarcado.AsBoolean := false;
    MDCredenciados.Post;
    MDCredenciados.Next;
  end;
  MDCredenciados.GotoBookmark(marca);
  MDCredenciados.FreeBookmark(marca);
  MDCredenciados.EnableControls;
  //Empresas_Sel;
end;

procedure TFGerenciais.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_F5  then if PageControl1.ActivePageIndex = 0 then ButListaEmp.Click;
  if key = vk_F12 then if PageControl1.ActivePageIndex = 0 then ButMarcaDesmEmp.Click;
  if key = vk_F6  then if PageControl1.ActivePageIndex = 0 then ButMarcaTodasEmp.Click;
  if key = vk_F7  then if PageControl1.ActivePageIndex = 0 then ButDesmarcaTodosEmp.Click;
end;

procedure TFGerenciais.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin

  try
    if Pos(Field.FieldName,QCredenciados.Sort) > 0 then begin
    if Pos(' DESC',QCredenciados.Sort) > 0 then QCredenciados.Sort := Field.FieldName
                            else QCredenciados.Sort := Field.FieldName+' DESC';
    end
    else QCredenciados.Sort := Field.FieldName;
    except
    end;

    QCredenciados.First;

    MDCredenciados.Open;
    MDCredenciados.EmptyTable;
    MDCredenciados.DisableControls;
    while not QCredenciados.Eof do begin
        MDCredenciados.Append;
        MDCredenciadoscred_id.AsInteger  := QCredenciadoscred_id.AsInteger;
        MDCredenciadosnome.AsString      := QCredenciadosnome.AsString;
        MDCredenciadosfantasia.AsString   := QCredenciadosfantasia.AsString;
        MDCredenciadosbairro.AsString    := QCredenciadosBAIRRO.AsString;
        MDCredenciadoscidade.AsString    := QCredenciadoscidade.AsString;
        MDCredenciadosemail.AsString     := QCredenciadosemail.AsString;
        MDCredenciadostelefone1.AsString := QCredenciadosTELEFONE1.AsString;
        MDCredenciadosmarcado.AsBoolean  := False;
        MDCredenciados.Post;
        QCredenciados.Next;
    end;
    MDCredenciados.First;
    MDCredenciados.EnableControls;
    //conv_sel := EmptyStr;


end;

procedure TFGerenciais.BitBtn1Click(Sender: TObject);
begin
  frxReport1.Variables['qtDias'] := edtDias.Text;
  frxReport1.ShowReport;
end;

end.
