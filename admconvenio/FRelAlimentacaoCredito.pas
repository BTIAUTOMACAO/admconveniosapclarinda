unit FRelAlimentacaoCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, frxClass, frxExportPDF, frxGradient, frxDBSet, DBCtrls,
  StdCtrls, Mask, JvExMask, JvToolEdit, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons, ComCtrls, Menus, ZDataset, DB,
  ZAbstractRODataset, ZAbstractDataset, ShellApi, ADODB, JvMemoryDataset;

type
  TFrmRelAlimentacaoCredito = class(TF1)
    frxReport1: TfrxReport;
    dbConveniados: TfrxDBDataset;
    dbData: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    dsConv: TDataSource;
    GroupBox1: TGroupBox;
    qConv: TADOQuery;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    dbLbpFechamentos: TDBLookupComboBox;
    lblFechamento: TLabel;
    dsFechamentos: TDataSource;
    qFechamentos: TADOQuery;
    qFechamentosdata_fechamento: TStringField;
    qConvRENOVACAO: TBCDField;
    qConvABONO: TBCDField;
    qConvTOTAL: TBCDField;
    qConvNOME: TStringField;
    qConvEMPRES_ID: TIntegerField;
    qConvDATA_RENOVACAO: TWideStringField;
    rgTipo: TRadioGroup;
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dsConvDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    conv_sel : String;
  public
    { Public declarations }
  end;

var
  
  FrmRelAlimentacaoCredito: TFrmRelAlimentacaoCredito;

implementation

uses DM, cartao_util;

{$R *.dfm}



procedure TFrmRelAlimentacaoCredito.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm_EmpClick(nil);
end;

procedure TFrmRelAlimentacaoCredito.ButMarcDesm_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesm(QConv);
end;

procedure TFrmRelAlimentacaoCredito.ButMarcaTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv);
end;

procedure TFrmRelAlimentacaoCredito.ButDesmTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv,False);
end;

procedure TFrmRelAlimentacaoCredito.btnGerarPDFClick(Sender: TObject);
begin
  inherited;
  if(dbLbpFechamentos.KeyValue = Null) then begin
    MsgInf('Selecione uma Data de Renova��o.');
    dbLbpFechamentos.SetFocus;
  end
  else
  begin
    sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      btnVisualizar.Click;
      frxReport1.Export(frxPDFExport1);
    end;
  end;
end;

procedure TFrmRelAlimentacaoCredito.btnVisualizarClick(Sender: TObject);
begin
  inherited;
  case rgTipo.ItemIndex of
      0:  BEGIN
          qConv.Close;
          qConv.SQL.Clear;
          qConv.SQL.Add(' SELECT COALESCE(SUM(AC.RENOVACAO_VALOR),0.00) AS RENOVACAO, COALESCE(SUM(AC.ABONO_VALOR),0.00) AS ABONO,');
          qConv.SQL.Add(' COALESCE(SUM(AC.RENOVACAO_VALOR + AC.ABONO_VALOR),0.00) AS TOTAL, EMP.NOME, AR.EMPRES_ID, ');
          qConv.SQL.Add(' convert(varchar,AR.DATA_RENOVACAO ,103) as data_renovacao');
          qConv.SQL.Add(' FROM ALIMENTACAO_RENOVACAO AR, EMPRESAS EMP, ALIMENTACAO_RENOVACAO_CREDITOS AC');
          qConv.SQL.Add(' WHERE CONVERT(DATE,AR.DATA_LANCAMENTO) = CONVERT(DATE,CURRENT_TIMESTAMP) AND ');
          qConv.SQL.Add(' AR.EMPRES_ID = EMP.EMPRES_ID AND AC.RENOVACAO_ID = AR.RENOVACAO_ID ');
          qConv.SQL.Add(' GROUP BY EMP.NOME, AR.EMPRES_ID,AR.DATA_RENOVACAO ');
          qConv.SQL.Text;
          qConv.Open;
          END;
  else
          if(dbLbpFechamentos.KeyValue = Null) then begin
             MsgInf('Selecione uma Data de Renova��o!');
             dbLbpFechamentos.SetFocus;
             abort;
          end else begin
            qConv.Close;
            qConv.SQL.Clear;
            qConv.SQL.Add(' SELECT COALESCE(SUM(AH.RENOVACAO_VALOR),0.00) AS RENOVACAO, COALESCE(SUM(AH.ABONO_VALOR),0.00) AS ABONO,');
            qConv.SQL.Add(' COALESCE(SUM(AH.RENOVACAO_VALOR + AH.ABONO_VALOR),0.00) AS TOTAL, EMP.NOME, AH.EMPRES_ID, ');
            qConv.SQL.Add(' convert(varchar,AH.DATA_RENOVACAO ,103) as data_renovacao');
            qConv.SQL.Add(' FROM ALIMENTACAO_HISTORICO AH, EMPRESAS EMP');
            qConv.SQL.Add(' WHERE AH.DATA_RENOVACAO = ''' + dbLbpFechamentos.KeyValue + '''');
            qConv.SQL.Add(' AND AH.EMPRES_ID = EMP.EMPRES_ID ');
            qConv.SQL.Add(' GROUP BY EMP.NOME, AH.EMPRES_ID, AH.DATA_RENOVACAO ');
            qConv.SQL.Text;
            qConv.Open;
          END;
  end;

    if qConv.IsEmpty then
    begin
      MsgInf('N�o foi encontrado nenhum lan�amento.');
      dbLbpFechamentos.SetFocus;
      abort;
    end;
    frxReport1.ShowReport;


end;

procedure TFrmRelAlimentacaoCredito.dsConvDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnGerarPDF.Enabled   := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
end;

procedure TFrmRelAlimentacaoCredito.FormCreate(Sender: TObject);
begin
  inherited;
  qFechamentos.Open;
  rgTipo.SetFocus;
end;

procedure TFrmRelAlimentacaoCredito.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {if (key = VK_F8) then
    Dm1.MarcaDesmTodos(qConv)
  else if (key = VK_F9) then
    Dm1.MarcaDesmTodos(qConv,False)
  else if (key = VK_F11) then
    Dm1.MarcaDesm(qConv);   }
end;


end.
