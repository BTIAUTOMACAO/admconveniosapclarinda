unit UBuscaConvBemEstarPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, ExtCtrls, ComCtrls, Grids,
  DBGrids, JvExDBGrids, JvDBGrid;

type
  TFBuscaConvPadrao = class(TForm)
    JvDBGrid1: TJvDBGrid;
    PageControl1: TPageControl;
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ButBusca: TBitBtn;
    EdNome: TEdit;
    EdCod: TEdit;
    grdConveniados: TJvDBGrid;
    QConv: TADOQuery;
    DSConv: TDataSource;
    Label3: TLabel;
    EdCodEmpresa: TEdit;
    EdNomeFantasia: TEdit;
    Label4: TLabel;
    btnOk: TBitBtn;
    btnCancelar: TBitBtn;
    QConvconv_id: TIntegerField;
    QConvempres_id: TIntegerField;
    QConvtitular: TStringField;
    QConvfantasia: TStringField;
    procedure BuscaConveniados;
    procedure ButBuscaClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCodKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBuscaConvPadrao: TFBuscaConvPadrao;

implementation

uses URotinasTexto, UManutTaxasMensalidadesBenificiario, cartao_util;

{$R *.dfm}

procedure TFBuscaConvPadrao.BuscaConveniados;
begin
  Screen.Cursor := crHourGlass;
  QConv.Close;
  QConv.SQL.Clear;
  QConv.SQL.Add('SELECT conv.conv_id, conv.titular, conv.empres_id, empresas.fantasia ');
  QConv.SQL.Add('FROM conveniados conv inner join empresas ON empresas.empres_id = conv.empres_id WHERE conv.apagado = ''N''');
  if Trim(EdCod.Text) <> '' then begin
    QConv.Sql.Add(' and conv.conv_id =' + fnRemoverCaracterSQLInjection(EdCod.Text));
  end;
  if Trim(EdNome.Text) <> '' then
    QConv.Sql.Add(' and conv.titular like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');

  QConv.Open;

  Screen.Cursor := crDefault;
end;
procedure TFBuscaConvPadrao.ButBuscaClick(Sender: TObject);
begin
  
  BuscaConveniados;
  if not QConv.IsEmpty then
    btnOk.SetFocus;
end;

procedure TFBuscaConvPadrao.btnOkClick(Sender: TObject);
begin
  if QConv.RecordCount = 0 then
  begin
    MsgInf('N�o existe conveniado selecionado. Execute uma consulta v�lida.');
    EdCod.SetFocus;
    Abort;
  end
  else
    ModalResult := mrOk;
end;

procedure TFBuscaConvPadrao.FormShow(Sender: TObject);
begin
  QConv.Open;
end;

procedure TFBuscaConvPadrao.FormCreate(Sender: TObject);
begin
  //EdCod.SetFocus;
end;

procedure TFBuscaConvPadrao.EdCodKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then ButBusca.Click;
end;

end.
