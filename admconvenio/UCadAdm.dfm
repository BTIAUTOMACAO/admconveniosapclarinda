inherited FCadAdm: TFCadAdm
  Left = 266
  Top = 127
  Caption = 'Cadastro da Administradora'
  ClientHeight = 435
  ClientWidth = 815
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 815
    Height = 394
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 275
        Width = 807
        inherited Label2: TLabel
          Width = 40
          Caption = '&Fantasia'
        end
        inherited EdNome: TEdit
          TabOrder = 1
        end
        inherited ButBusca: TBitBtn
          TabOrder = 2
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 723
          TabOrder = 3
        end
        inherited EdCod: TEdit
          TabOrder = 0
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 807
        Height = 275
        Columns = <
          item
            Expanded = False
            FieldName = 'ADM_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RAZAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANTASIA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INSC_EST'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENDERECO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BAIRRO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CIDADE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 347
        Width = 807
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 334
        Width = 807
        TabOrder = 1
        inherited ButEdit: TBitBtn
          Left = 3
        end
        inherited ButInclui: TBitBtn
          Left = 88
        end
        inherited ButApaga: TBitBtn
          Left = 173
        end
      end
      inherited Panel3: TPanel
        Width = 807
        Height = 334
        TabOrder = 0
      end
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 807
        Height = 334
        ActivePage = TabSheet1
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 2
        object TabSheet1: TTabSheet
          Caption = 'Dados da Administradora'
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 799
            Height = 303
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 243
              Width = 799
              Height = 60
              Align = alBottom
              Caption = 'Informa'#231#245'es sobre altera'#231#227'o do cadastro'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object Label19: TLabel
                Left = 8
                Top = 15
                Width = 141
                Height = 13
                Caption = 'Data da Altera'#231#227'o / Operador'
                FocusControl = DBEdit10
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label20: TLabel
                Left = 232
                Top = 15
                Width = 138
                Height = 13
                Caption = 'Data do Cadastro / Operador'
                FocusControl = DBEdit16
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object DBEdit10: TDBEdit
                Left = 8
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTALTERACAO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
              end
              object DBEdit16: TDBEdit
                Left = 232
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
              object DBEdit17: TDBEdit
                Left = 342
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
              end
              object DBEdit18: TDBEdit
                Left = 118
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERADOR'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
              end
            end
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 799
              Height = 243
              Align = alClient
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 1
              object Label3: TLabel
                Left = 8
                Top = 10
                Width = 33
                Height = 13
                Caption = 'C'#243'digo'
                FocusControl = DBEdit1
              end
              object Label4: TLabel
                Left = 64
                Top = 10
                Width = 63
                Height = 13
                Caption = 'Raz'#227'o Social'
                FocusControl = dbEdtRazSocial
              end
              object Label5: TLabel
                Left = 8
                Top = 50
                Width = 71
                Height = 13
                Caption = 'Nome Fantasia'
                FocusControl = DBEdit3
              end
              object Label6: TLabel
                Left = 384
                Top = 50
                Width = 21
                Height = 13
                Caption = 'Cnpj'
                FocusControl = DBEdit4
              end
              object Label7: TLabel
                Left = 528
                Top = 50
                Width = 87
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual'
                FocusControl = DBEdit5
              end
              object Label8: TLabel
                Left = 8
                Top = 130
                Width = 46
                Height = 13
                Caption = 'Endere'#231'o'
                FocusControl = DBEdit6
              end
              object Label9: TLabel
                Left = 325
                Top = 130
                Width = 27
                Height = 13
                Caption = 'Bairro'
                FocusControl = DBEdit7
              end
              object Label10: TLabel
                Left = 445
                Top = 130
                Width = 33
                Height = 13
                Caption = 'Cidade'
                FocusControl = DBEdit8
              end
              object Label11: TLabel
                Left = 512
                Top = 90
                Width = 21
                Height = 13
                Caption = 'CEP'
                FocusControl = edtCep
              end
              object Label12: TLabel
                Left = 596
                Top = 130
                Width = 14
                Height = 13
                Caption = 'UF'
              end
              object Label15: TLabel
                Left = 8
                Top = 90
                Width = 42
                Height = 13
                Caption = 'Telefone'
                FocusControl = DBEdit13
              end
              object Label16: TLabel
                Left = 144
                Top = 90
                Width = 62
                Height = 13
                Caption = 'Respons'#225'vel'
                FocusControl = DBEdit14
              end
              object Label18: TLabel
                Left = 269
                Top = 130
                Width = 12
                Height = 13
                Caption = 'N'#186
                FocusControl = DBEdit15
              end
              object SpeedButton1: TSpeedButton
                Left = 617
                Top = 105
                Width = 23
                Height = 21
                Flat = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFD9B28CCF9D6CD4A577D9AD83DEB68FE2BD9932
                  65984C7FB24C7FB24C7FB24C7FB2326598CF9D6CD9B28CFFFFFFFFFFFFD6A26F
                  FFEEDDFFFFFFFFEEDDFFFFFFFFEEDD376A9D6598CC6598CC6598CC6598CC376A
                  9DFFEEDDD6A26FFFFFFFFFFFFFE5B17FFFEFDF988776FFEFDF988776FFEFDF3E
                  71A474A4D474A4D474A4D474A4D43E71A4FFEFDFE5B17FFFFFFFFFFFFFF5C18E
                  FFF0E2CCBAA9FFF0E2CCBAA9FFF0E2477AAD6598CC0032658BB5DF8BB5DF477A
                  ADFFF0E2F5C18EFFFFFFFFFFFFFFCC98FFF2E5FFFFFFFFF2E5FFFFFFFFF2E550
                  83B6A5C8ECA5C8ECA5C8ECA5C8EC5083B6FFF2E5FFCC98FFFFFF7FB2E5CC9865
                  FFF4E8988776FFF4E8988776FFF4E8749DC7A3C5E9BBD9F7BBD9F7A3C5E9749D
                  C7FFF4E8CC98657FB2E50065CC0065CCCCBAA9CCBAA9FFF7F0CCBAA9FFF7F0AF
                  C4DB84AEDACCE5FFCCE5FF84AEDAAFC4DBCCBAA90065CC0065CC0069D00098FF
                  0065CCCCBAA9FFF9F4FFF9F4FFF9F4EFEFF0B1C9E06598CC6598CCB1C9E0C1B7
                  AC0065CC0098FF0069D07FB7EA006FD60C9EFF0065CCCCBAA9FFFBF7FFFBF7FF
                  FBF7FFFBF7FFFBF7FFFBF7CCBAA90065CC0C9EFF006FD67FB7EAFFFFFF7FBBEE
                  0077DE1DA7FF0065CCCCBAA9FFFDFAFFFDFAFFFDFAFFFDFACCBAA90065CC1DA7
                  FF0077DE7FBBEEFFFFFFFFFFFFFFFFFF326598007FE532B1FF0065CCCCBAA9FF
                  FEFDFFFEFDCCBAA90065CC32B1FF007FE57FBFF2FFFFFFFFFFFFFFFFFFFFFFFF
                  3C6FA26598CC0086ED47BCFF0065CCCCBAA9CCBAA90065CC47BCFF0086ED7FC2
                  F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4C7FB198BEE64C7FB1008EF559C5FF00
                  65CC0065CC59C5FF008EF57FC6FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  5B8EC1CCE5FF5B8EC17FC9FD0094FB65CCFF65CCFF0094FB7FC9FDFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6598CC6598CC6598CCFFFFFF7FCBFF00
                  98FF0098FF7FCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                OnClick = SpeedButton1Click
              end
              object DBEdit1: TDBEdit
                Left = 8
                Top = 25
                Width = 49
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'ADM_ID'
                DataSource = DSCadastro
                ReadOnly = True
                TabOrder = 0
              end
              object dbEdtRazSocial: TDBEdit
                Left = 64
                Top = 25
                Width = 577
                Height = 21
                CharCase = ecUpperCase
                DataField = 'RAZAO'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 65
                Width = 369
                Height = 21
                CharCase = ecUpperCase
                DataField = 'FANTASIA'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBEdit4: TDBEdit
                Left = 384
                Top = 65
                Width = 137
                Height = 21
                CharCase = ecUpperCase
                DataField = 'CNPJ'
                DataSource = DSCadastro
                TabOrder = 3
              end
              object DBEdit5: TDBEdit
                Left = 528
                Top = 65
                Width = 113
                Height = 21
                CharCase = ecUpperCase
                DataField = 'INSC_EST'
                DataSource = DSCadastro
                TabOrder = 4
              end
              object DBEdit6: TDBEdit
                Left = 8
                Top = 145
                Width = 254
                Height = 21
                CharCase = ecUpperCase
                DataField = 'ENDERECO'
                DataSource = DSCadastro
                TabOrder = 8
              end
              object DBEdit7: TDBEdit
                Left = 325
                Top = 145
                Width = 113
                Height = 21
                CharCase = ecUpperCase
                DataField = 'BAIRRO'
                DataSource = DSCadastro
                TabOrder = 10
              end
              object DBEdit8: TDBEdit
                Left = 445
                Top = 145
                Width = 145
                Height = 21
                CharCase = ecUpperCase
                DataField = 'CIDADE'
                DataSource = DSCadastro
                TabOrder = 11
              end
              object edtCep: TDBEdit
                Left = 512
                Top = 105
                Width = 104
                Height = 21
                CharCase = ecUpperCase
                DataField = 'CEP'
                DataSource = DSCadastro
                TabOrder = 7
              end
              object DBEdit13: TDBEdit
                Left = 8
                Top = 105
                Width = 129
                Height = 21
                CharCase = ecUpperCase
                DataField = 'FONE'
                DataSource = DSCadastro
                TabOrder = 5
              end
              object DBEdit14: TDBEdit
                Left = 144
                Top = 105
                Width = 361
                Height = 21
                CharCase = ecUpperCase
                DataField = 'RESPONSAVEL'
                DataSource = DSCadastro
                TabOrder = 6
              end
              object DBEdit15: TDBEdit
                Left = 269
                Top = 145
                Width = 49
                Height = 21
                DataField = 'NUMERO'
                DataSource = DSCadastro
                TabOrder = 9
              end
              object cbbUF: TJvDBComboBox
                Left = 597
                Top = 145
                Width = 44
                Height = 21
                DataField = 'UF'
                DataSource = DSCadastro
                Items.Strings = (
                  'AC'
                  'AL'
                  'AM'
                  'AP'
                  'BA'
                  'CE'
                  'DF'
                  'ES'
                  'GO'
                  'MA'
                  'MG'
                  'MS'
                  'MT'
                  'PA'
                  'PB'
                  'PE'
                  'PI'
                  'PR'
                  'RJ'
                  'RN'
                  'RO'
                  'RR'
                  'RS'
                  'SC'
                  'SE'
                  'SP'
                  'TO')
                TabOrder = 12
                Values.Strings = (
                  'AC'
                  'AL'
                  'AM'
                  'AP'
                  'BA'
                  'CE'
                  'DF'
                  'ES'
                  'GO'
                  'MA'
                  'MG'
                  'MS'
                  'MT'
                  'PA'
                  'PB'
                  'PE'
                  'PI'
                  'PR'
                  'RJ'
                  'RN'
                  'RO'
                  'RR'
                  'RS'
                  'SC'
                  'SE'
                  'SP'
                  'TO')
                ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                ListSettings.OutfilteredValueFont.Color = clRed
                ListSettings.OutfilteredValueFont.Height = -11
                ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
                ListSettings.OutfilteredValueFont.Style = []
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Gera'#231#227'o de Boleto Banc'#225'rio'
          ImageIndex = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 799
            Height = 303
            Align = alClient
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object Label17: TLabel
              Left = 8
              Top = 10
              Width = 71
              Height = 13
              Caption = 'Conta Corrente'
            end
            object JvDBLookupCombo1: TJvDBLookupCombo
              Left = 8
              Top = 25
              Width = 369
              Height = 21
              DataField = 'CONTA_ID'
              DataSource = DSCadastro
              DisplayEmpty = 'Escolha uma conta'
              EmptyValue = '0'
              LookupField = 'CONTA_ID'
              LookupDisplay = 'NOME_CONTA'
              LookupSource = dsContas
              TabOrder = 0
            end
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 807
      end
      inherited GridHistorico: TJvDBGrid
        Width = 807
        Height = 319
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 815
  end
  inherited panStatus2: TPanel
    Width = 815
  end
  inherited DSCadastro: TDataSource
    Top = 128
  end
  inherited PopupBut: TPopupMenu
    Left = 724
    Top = 80
  end
  inherited PopupGrid1: TPopupMenu
    Left = 724
    Top = 32
  end
  inherited DSHistorico: TDataSource
    Top = 129
  end
  object dsContas: TDataSource [8]
    DataSet = qContas
    Left = 528
    Top = 129
  end
  object qContas: TADOQuery [9]
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select c.conta_id, b.banco +'#39' - '#39'+nome_convenio as nome_conta fr' +
        'om contas_bancarias c'
      'join bancos b on b.codigo = c.cod_banco'
      'where coalesce(c.carteira,'#39#39')<>'#39#39)
    Left = 532
    Top = 161
    object qContasconta_id: TIntegerField
      FieldName = 'conta_id'
    end
    object qContasnome_conta: TStringField
      FieldName = 'nome_conta'
      ReadOnly = True
      Size = 78
    end
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from administradora where  adm_id = 0')
    object QCadastroADM_ID: TIntegerField
      DisplayLabel = 'Adm. ID'
      FieldName = 'ADM_ID'
    end
    object QCadastroRAZAO: TStringField
      DisplayLabel = 'Raz'#227'o'
      FieldName = 'RAZAO'
      Size = 45
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroCONTA_ID: TIntegerField
      DisplayLabel = 'Conta ID'
      FieldName = 'CONTA_ID'
    end
    object QCadastroFANTASIA: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'FANTASIA'
      Size = 45
    end
    object QCadastroCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QCadastroINSC_EST: TStringField
      DisplayLabel = 'Insc. Estadual'
      FieldName = 'INSC_EST'
      Size = 15
    end
    object QCadastroENDERECO: TStringField
      DisplayLabel = 'Endereco'
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCadastroNUMERO: TIntegerField
      DisplayLabel = 'Numero'
      FieldName = 'NUMERO'
    end
    object QCadastroBAIRRO: TStringField
      DisplayLabel = 'Bairro'
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QCadastroCIDADE: TStringField
      DisplayLabel = 'Cidade'
      FieldName = 'CIDADE'
      Size = 35
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 10
    end
    object QCadastroUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object QCadastroRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Size = 50
    end
    object QCadastroFONE: TStringField
      FieldName = 'FONE'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroDTALTERACAO: TDateTimeField
      DisplayLabel = 'Dt. Altera'#231#227'o'
      FieldName = 'DTALTERACAO'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroVENCEDIA: TIntegerField
      FieldName = 'VENCEDIA'
    end
    object QCadastroCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
  end
end
