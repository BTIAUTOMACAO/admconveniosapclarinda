inherited FCadBandeiras: TFCadBandeiras
  Left = 313
  Top = 209
  Caption = 'Cadastro de Bandeiras'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = tsSegLim4
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        inherited Label2: TLabel
          Width = 48
          Caption = '&Descri'#231#227'o'
        end
        object Label22: TLabel [8]
          Left = 349
          Top = 3
          Width = 55
          Height = 13
          Caption = '&Qtd. Limites'
          FocusControl = EdNome
        end
        inherited EdNome: TEdit
          Hint = 'Busca por descri'#231#227'o'
        end
        inherited ButBusca: TBitBtn
          Left = 492
          TabOrder = 2
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          TabOrder = 3
        end
        inherited EdCod: TEdit
          TabOrder = 4
        end
        inherited ButFiltro: TBitBtn
          TabOrder = 5
        end
        inherited ButAltLin: TButton
          TabOrder = 6
        end
        object edQtdLimites: TEdit
          Left = 350
          Top = 18
          Width = 73
          Height = 21
          Hint = 'Busca por Quantidade de Limite'
          TabOrder = 1
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
      end
      inherited DBGrid1: TJvDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'BAND_ID'
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QTD_LIMITES'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESC_LIMITE_1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIN_LIMITE_1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAX_LIMITE_1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESC_LIMITE_2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIN_LIMITE_2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAX_LIMITE_2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESC_LIMITE_3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIN_LIMITE_3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAX_LIMITE_3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESC_LIMITE_4'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MIN_LIMITE_4'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAX_LIMITE_4'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_4'
            Visible = True
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel3: TPanel
        object Label3: TLabel
          Left = 10
          Top = 15
          Width = 47
          Height = 13
          Caption = 'Band ID'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 80
          Top = 14
          Width = 58
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = DBEdit2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 10
          Top = 63
          Width = 68
          Height = 13
          Caption = 'Qtd. Limites'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 10
          Top = 30
          Width = 65
          Height = 21
          Hint = 'C'#243'digo do conveniado'
          TabStop = False
          Color = clBtnFace
          DataField = 'BAND_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 80
          Top = 30
          Width = 671
          Height = 21
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object dbCbLimites: TDBComboBox
          Left = 8
          Top = 79
          Width = 129
          Height = 21
          DataField = 'QTD_LIMITES'
          DataSource = DSCadastro
          DropDownCount = 4
          ItemHeight = 13
          Items.Strings = (
            '1'
            '2'
            '3'
            '4')
          TabOrder = 2
          OnChange = dbCbLimitesChange
        end
        object pnlLimite1: TPanel
          Left = 160
          Top = 64
          Width = 595
          Height = 41
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 3
          object Label6: TLabel
            Left = 2
            Top = 1
            Width = 106
            Height = 13
            Caption = 'Descri'#231#227'o Limite 1'
            FocusControl = DBEditDescLimite1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 153
            Top = 1
            Width = 90
            Height = 13
            Caption = 'Limite M'#237'nimo 1'
            FocusControl = DBEditMinLimite1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 305
            Top = 1
            Width = 91
            Height = 13
            Caption = 'Limite M'#225'ximo 1'
            FocusControl = DBEditLimiteMax1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 457
            Top = 1
            Width = 45
            Height = 13
            Caption = 'Limite 1'
            FocusControl = DBEditLimite1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBEditDescLimite1: TDBEdit
            Left = 2
            Top = 17
            Width = 133
            Height = 21
            DataField = 'DESC_LIMITE_1'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEditMinLimite1: TDBEdit
            Left = 153
            Top = 17
            Width = 134
            Height = 21
            DataField = 'MIN_LIMITE_1'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEditLimiteMax1: TDBEdit
            Left = 305
            Top = 17
            Width = 134
            Height = 21
            DataField = 'MAX_LIMITE_1'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEditLimite1: TDBEdit
            Left = 457
            Top = 17
            Width = 134
            Height = 21
            DataField = 'LIMITE_1'
            DataSource = DSCadastro
            TabOrder = 3
          end
        end
        object pnlLimite2: TPanel
          Left = 160
          Top = 104
          Width = 595
          Height = 41
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 4
          object Label10: TLabel
            Left = 2
            Top = 1
            Width = 87
            Height = 13
            Caption = 'Descri'#231#227'o Limite 2'
            FocusControl = DBEditDescLimite2
          end
          object Label11: TLabel
            Left = 154
            Top = 1
            Width = 74
            Height = 13
            Caption = 'Limite M'#237'nimo 2'
            FocusControl = DBEditMinLimite2
          end
          object Label12: TLabel
            Left = 306
            Top = 1
            Width = 75
            Height = 13
            Caption = 'Limite M'#225'ximo 2'
            FocusControl = DBEditLimiteMax2
          end
          object Label13: TLabel
            Left = 458
            Top = 1
            Width = 36
            Height = 13
            Caption = 'Limite 2'
            FocusControl = DBEditLimite2
          end
          object DBEditDescLimite2: TDBEdit
            Left = 2
            Top = 17
            Width = 134
            Height = 21
            DataField = 'DESC_LIMITE_2'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEditMinLimite2: TDBEdit
            Left = 154
            Top = 17
            Width = 134
            Height = 21
            DataField = 'MIN_LIMITE_2'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEditLimiteMax2: TDBEdit
            Left = 306
            Top = 17
            Width = 134
            Height = 21
            DataField = 'MAX_LIMITE_2'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEditLimite2: TDBEdit
            Left = 458
            Top = 17
            Width = 134
            Height = 21
            DataField = 'LIMITE_2'
            DataSource = DSCadastro
            TabOrder = 3
          end
        end
        object pnlLimite3: TPanel
          Left = 160
          Top = 144
          Width = 593
          Height = 41
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 5
          object Label14: TLabel
            Left = 2
            Top = 2
            Width = 87
            Height = 13
            Caption = 'Descri'#231#227'o Limite 3'
            FocusControl = DBEditDescLimite3
          end
          object Label15: TLabel
            Left = 154
            Top = 2
            Width = 74
            Height = 13
            Caption = 'Limite M'#237'nimo 3'
            FocusControl = DBEditMinLimite3
          end
          object Label16: TLabel
            Left = 306
            Top = 2
            Width = 75
            Height = 13
            Caption = 'Limite M'#225'ximo 3'
            FocusControl = DBEditLimiteMax3
          end
          object Label17: TLabel
            Left = 458
            Top = 2
            Width = 36
            Height = 13
            Caption = 'Limite 3'
            FocusControl = DBEditLimite3
          end
          object DBEditDescLimite3: TDBEdit
            Left = 2
            Top = 18
            Width = 134
            Height = 21
            DataField = 'DESC_LIMITE_3'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEditMinLimite3: TDBEdit
            Left = 154
            Top = 18
            Width = 134
            Height = 21
            DataField = 'MIN_LIMITE_3'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEditLimiteMax3: TDBEdit
            Left = 306
            Top = 18
            Width = 134
            Height = 21
            DataField = 'MAX_LIMITE_3'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEditLimite3: TDBEdit
            Left = 458
            Top = 18
            Width = 134
            Height = 21
            DataField = 'LIMITE_3'
            DataSource = DSCadastro
            TabOrder = 3
          end
        end
        object pnlLimite4: TPanel
          Left = 160
          Top = 184
          Width = 593
          Height = 41
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 6
          object Label18: TLabel
            Left = 2
            Top = 2
            Width = 87
            Height = 13
            Caption = 'Descri'#231#227'o Limite 4'
            FocusControl = DBEditDescLimite4
          end
          object Label19: TLabel
            Left = 154
            Top = 2
            Width = 74
            Height = 13
            Caption = 'Limite M'#237'nimo 4'
            FocusControl = DBEditMinLimite4
          end
          object Label20: TLabel
            Left = 306
            Top = 2
            Width = 75
            Height = 13
            Caption = 'Limite M'#225'ximo 4'
            FocusControl = DBEditLimiteMax4
          end
          object Label21: TLabel
            Left = 458
            Top = 2
            Width = 36
            Height = 13
            Caption = 'Limite 4'
            FocusControl = DBEditLimite4
          end
          object DBEditDescLimite4: TDBEdit
            Left = 2
            Top = 18
            Width = 134
            Height = 21
            DataField = 'DESC_LIMITE_4'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEditMinLimite4: TDBEdit
            Left = 154
            Top = 18
            Width = 134
            Height = 21
            DataField = 'MIN_LIMITE_4'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBEditLimiteMax4: TDBEdit
            Left = 306
            Top = 18
            Width = 134
            Height = 21
            DataField = 'MAX_LIMITE_4'
            DataSource = DSCadastro
            TabOrder = 2
          end
          object DBEditLimite4: TDBEdit
            Left = 458
            Top = 18
            Width = 134
            Height = 21
            DataField = 'LIMITE_4'
            DataSource = DSCadastro
            TabOrder = 3
          end
        end
      end
    end
    object tsSegLim1: TTabSheet [2]
      Tag = 1
      Caption = 'Segmentos Lim. 1'
      ImageIndex = 3
      OnShow = tsSegLim1Show
      object Panel39: TPanel
        Left = 0
        Top = 0
        Width = 768
        Height = 509
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object GridCredObrigarSenha: TJvDBGrid
          Left = 2
          Top = 33
          Width = 764
          Height = 443
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsCadBand_Segmentos
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = GridCredObrigarSenhaKeyDown
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SEG_ID_LKP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEGMENTOS'
              Title.Caption = 'Segmento'
              Width = 448
              Visible = True
            end>
        end
        object Panel13: TPanel
          Left = 2
          Top = 2
          Width = 764
          Height = 31
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object btnIncProdBlack: TBitBtn
            Tag = 1
            Left = 3
            Top = 3
            Width = 702
            Height = 25
            Caption = '&Incluir Segmento'
            TabOrder = 0
            OnClick = btnIncProdBlackClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
          object btnImportListaBlack: TBitBtn
            Left = 707
            Top = 3
            Width = 75
            Height = 25
            BiDiMode = bdRightToLeft
            Caption = 'Import. Lista'
            Enabled = False
            ParentBiDiMode = False
            TabOrder = 1
            Visible = False
          end
        end
        object Panel38: TPanel
          Left = 2
          Top = 476
          Width = 764
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object btnApagaCadBand_Seg: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Apagar'
            TabOrder = 0
            OnClick = btnApagaCadBand_SegClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
          end
          object btnGravarCadBand_Seg: TBitBtn
            Left = 624
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Gravar registro'
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = btnGravarCadBand_SegClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
        end
      end
    end
    object tsSegLim2: TTabSheet [3]
      Tag = 2
      Caption = 'Segmentos Lim. 2'
      ImageIndex = 4
      OnShow = tsSegLim1Show
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 768
        Height = 509
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 476
          Width = 764
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object btnApagaCadBand_Seg2: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Apagar'
            TabOrder = 0
            OnClick = btnApagaCadBand_SegClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
          end
          object btnGravarCadBand_Seg2: TBitBtn
            Left = 624
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Gravar registro'
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = btnGravarCadBand_SegClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 2
          Width = 764
          Height = 31
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object BitBtn7: TBitBtn
            Tag = 2
            Left = 3
            Top = 3
            Width = 702
            Height = 25
            Caption = '&Incluir Segmento'
            TabOrder = 0
            OnClick = btnIncProdBlackClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
          object BitBtn8: TBitBtn
            Left = 707
            Top = 3
            Width = 75
            Height = 25
            BiDiMode = bdRightToLeft
            Caption = 'Import. Lista'
            Enabled = False
            ParentBiDiMode = False
            TabOrder = 1
            Visible = False
          end
        end
        object JvDBGrid1: TJvDBGrid
          Left = 2
          Top = 33
          Width = 764
          Height = 443
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsCadBand_Segmentos
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = JvDBGrid1KeyDown
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SEG_ID_LKP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEGMENTOS'
              Title.Caption = 'Segmento'
              Width = 448
              Visible = True
            end>
        end
      end
    end
    object tsSegLim3: TTabSheet [4]
      Tag = 3
      Caption = 'Segmentos Lim. 3'
      ImageIndex = 5
      OnShow = tsSegLim1Show
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 768
        Height = 509
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel9: TPanel
          Left = 2
          Top = 476
          Width = 764
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object btnApagaCadBand_Seg3: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Apagar'
            TabOrder = 0
            OnClick = btnApagaCadBand_SegClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
          end
          object btnGravarCadBand_Seg3: TBitBtn
            Left = 624
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Gravar registro'
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = btnGravarCadBand_SegClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
        end
        object Panel8: TPanel
          Left = 2
          Top = 2
          Width = 764
          Height = 31
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object BitBtn9: TBitBtn
            Tag = 3
            Left = 3
            Top = 3
            Width = 702
            Height = 25
            Caption = '&Incluir Segmento'
            TabOrder = 0
            OnClick = btnIncProdBlackClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
          object BitBtn10: TBitBtn
            Left = 707
            Top = 3
            Width = 75
            Height = 25
            BiDiMode = bdRightToLeft
            Caption = 'Import. Lista'
            Enabled = False
            ParentBiDiMode = False
            TabOrder = 1
            Visible = False
          end
        end
        object JvDBGrid2: TJvDBGrid
          Left = 2
          Top = 33
          Width = 764
          Height = 443
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsCadBand_Segmentos
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = JvDBGrid2KeyDown
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SEG_ID_LKP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEGMENTOS'
              Title.Caption = 'Segmento'
              Width = 448
              Visible = True
            end>
        end
      end
    end
    object tsSegLim4: TTabSheet [5]
      Tag = 4
      Caption = 'Segmentos Lim. 4'
      ImageIndex = 6
      OnShow = tsSegLim1Show
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 768
        Height = 509
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel12: TPanel
          Left = 2
          Top = 476
          Width = 764
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object btnApagaCadBand_Seg4: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Apagar'
            TabOrder = 0
            OnClick = btnApagaCadBand_SegClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
          end
          object btnGravarCadBand_Seg4: TBitBtn
            Left = 624
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Gravar registro'
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = btnGravarCadBand_SegClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 2
          Width = 764
          Height = 31
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object BitBtn11: TBitBtn
            Tag = 4
            Left = 3
            Top = 3
            Width = 702
            Height = 25
            Caption = '&Incluir Segmento'
            TabOrder = 0
            OnClick = btnIncProdBlackClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
          object BitBtn12: TBitBtn
            Left = 707
            Top = 3
            Width = 75
            Height = 25
            BiDiMode = bdRightToLeft
            Caption = 'Import. Lista'
            Enabled = False
            ParentBiDiMode = False
            TabOrder = 1
          end
        end
        object JvDBGrid3: TJvDBGrid
          Left = 2
          Top = 33
          Width = 764
          Height = 443
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsCadBand_Segmentos
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = JvDBGrid3KeyDown
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'SEG_ID_LKP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SEGMENTOS'
              Title.Caption = 'Segmento'
              Width = 448
              Visible = True
            end>
        end
      end
    end
  end
  inherited DSCadastro: TDataSource
    Left = 36
    Top = 424
  end
  inherited PopupBut: TPopupMenu
    Left = 604
    Top = 408
  end
  inherited PopupGrid1: TPopupMenu
    Left = 676
    Top = 408
  end
  inherited QHistorico: TADOQuery
    Left = 300
    Top = 376
  end
  inherited DSHistorico: TDataSource
    Left = 300
    Top = 424
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterPost = QCadastroAfterPost
    SQL.Strings = (
      'SELECT'
      '  BAND_ID, DESCRICAO,  QTD_LIMITES,'
      '  DESC_LIMITE_1, MIN_LIMITE_1, MAX_LIMITE_1, LIMITE_1,'
      '  DESC_LIMITE_2, MIN_LIMITE_2, MAX_LIMITE_2, LIMITE_2,'
      '  DESC_LIMITE_3, MIN_LIMITE_3, MAX_LIMITE_3, LIMITE_3,'
      '  DESC_LIMITE_4, MIN_LIMITE_4, MAX_LIMITE_4, LIMITE_4,'
      
        '  DTCADASTRO, OPERCADASTRO, DTALTERACAO, OPERADOR, APAGADO, DTAP' +
        'AGADO'
      'FROM BANDEIRAS'
      'WHERE BAND_ID = 0')
    Left = 36
    Top = 376
    object QCadastroBAND_ID: TIntegerField
      DisplayLabel = 'C'#243'd. Bandeira'
      FieldName = 'BAND_ID'
      Required = True
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Required = True
      Size = 60
    end
    object QCadastroQTD_LIMITES: TIntegerField
      DisplayLabel = 'Qtd. Limites'
      FieldName = 'QTD_LIMITES'
      Required = True
    end
    object QCadastroDESC_LIMITE_1: TStringField
      DisplayLabel = 'Descri'#231#227'o Limite 1'
      FieldName = 'DESC_LIMITE_1'
      Required = True
      Size = 60
    end
    object QCadastroMIN_LIMITE_1: TFloatField
      DisplayLabel = 'M'#237'nimo Limite 1'
      FieldName = 'MIN_LIMITE_1'
      Required = True
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroMAX_LIMITE_1: TFloatField
      DisplayLabel = 'Limite M'#225'ximo 1'
      FieldName = 'MAX_LIMITE_1'
      Required = True
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroLIMITE_1: TFloatField
      DisplayLabel = 'Limite 1'
      FieldName = 'LIMITE_1'
      Required = True
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroDESC_LIMITE_2: TStringField
      FieldName = 'DESC_LIMITE_2'
      Size = 60
    end
    object QCadastroMIN_LIMITE_2: TFloatField
      DisplayLabel = 'M'#237'nimo Limite 2'
      FieldName = 'MIN_LIMITE_2'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroMAX_LIMITE_2: TFloatField
      DisplayLabel = 'Limite M'#225'ximo 2'
      FieldName = 'MAX_LIMITE_2'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroLIMITE_2: TFloatField
      DisplayLabel = 'Limite 2'
      FieldName = 'LIMITE_2'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroDESC_LIMITE_3: TStringField
      FieldName = 'DESC_LIMITE_3'
      Size = 60
    end
    object QCadastroMIN_LIMITE_3: TFloatField
      DisplayLabel = 'M'#237'nimo Limite 3'
      FieldName = 'MIN_LIMITE_3'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroMAX_LIMITE_3: TFloatField
      DisplayLabel = 'Limite M'#225'ximo 3'
      FieldName = 'MAX_LIMITE_3'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroLIMITE_3: TFloatField
      DisplayLabel = 'Limite 3'
      FieldName = 'LIMITE_3'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroDESC_LIMITE_4: TStringField
      FieldName = 'DESC_LIMITE_4'
      Size = 60
    end
    object QCadastroMIN_LIMITE_4: TFloatField
      DisplayLabel = 'M'#237'nimo Limite 4'
      FieldName = 'MIN_LIMITE_4'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroMAX_LIMITE_4: TFloatField
      DisplayLabel = 'Limite M'#225'ximo 4'
      FieldName = 'MAX_LIMITE_4'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroLIMITE_4: TFloatField
      DisplayLabel = 'Limite 4'
      FieldName = 'LIMITE_4'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 10
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Required = True
      Size = 1
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object dsCadBand_Segmentos: TDataSource
    DataSet = QCadBand_Segmentos
    OnStateChange = dsCadBand_SegmentosStateChange
    OnDataChange = DSCadastroDataChange
    Left = 180
    Top = 424
  end
  object dsSegmentos: TDataSource
    DataSet = QSegmentos
    OnDataChange = DSHistoricoDataChange
    Left = 388
    Top = 424
  end
  object QSegmentos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'SEG_ID,'
      'DESCRICAO'
      'FROM'
      'SEGMENTOS'
      'ORDER BY'
      'DESCRICAO'
      '')
    Left = 384
    Top = 376
    object QSegmentosSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object QSegmentosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 58
    end
  end
  object QCadBand_Segmentos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'COD_LIMITE'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'BAND_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT'
      'bs.BAND_ID,'
      'bs.SEG_ID,'
      'bs.COD_LIMITE'
      'FROM'
      'BANDEIRAS_SEGMENTOS bs'
      'WHERE'
      'bs.COD_LIMITE = :COD_LIMITE'
      'AND bs.BAND_ID = :BAND_ID')
    Left = 184
    Top = 384
    object QCadBand_SegmentosBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object QCadBand_SegmentosSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object QCadBand_SegmentosCOD_LIMITE: TIntegerField
      FieldName = 'COD_LIMITE'
    end
    object QCadBand_SegmentosSEG_ID_LKP: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SEG_ID_LKP'
      LookupDataSet = QSegmentos
      LookupKeyFields = 'SEG_ID'
      LookupResultField = 'SEG_ID'
      KeyFields = 'SEG_ID'
      Lookup = True
    end
    object QCadBand_SegmentosSEGMENTOS: TStringField
      FieldKind = fkLookup
      FieldName = 'SEGMENTOS'
      LookupDataSet = QSegmentos
      LookupKeyFields = 'SEG_ID'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'SEG_ID'
      Lookup = True
    end
  end
end
