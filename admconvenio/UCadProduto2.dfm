inherited FCadProduto2: TFCadProduto2
  Left = 330
  Top = 25
  Caption = 'Cadastro de Produtos'
  ClientHeight = 602
  ClientWidth = 943
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object btn1: TSpeedButton [0]
    Left = 96
    Top = 297
    Width = 23
    Height = 22
    Flat = True
    Glyph.Data = {
      A6030000424DA603000000000000A60100002800000020000000100000000100
      08000000000000020000232E0000232E00005C00000000000000343434003535
      3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
      49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
      63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
      800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
      A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
      B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
      BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
      C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
      D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
      CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
      E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
      F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
      5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
      3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
      3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
      2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
      284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
      234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
      54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
      3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
      323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
      5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
      57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
      58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
      5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
      5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
      53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
      5B5B5B5B5B5B5B5B5B5B}
    NumGlyphs = 2
    OnClick = btnGravaBarraClick
  end
  object btn2: TSpeedButton [1]
    Left = 120
    Top = 297
    Width = 23
    Height = 22
    Flat = True
    Glyph.Data = {
      0E040000424D0E040000000000000E0200002800000020000000100000000100
      08000000000000020000232E0000232E000076000000000000000021CC001031
      DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
      DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
      FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
      F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
      F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
      FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
      E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
      ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
      FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
      C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
      CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
      D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
      E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
      E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
      F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
      75757575757575757575757575757575757575757575622F080000082F627575
      757575757575705E493434495E70757575757575753802030E11110E03023875
      7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
      7575757567354D5354555554534D35677575756307102A00337575442C151007
      63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
      367575604B545568345E7575745B544B607575171912301C3700317575401219
      1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
      057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
      0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
      217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
      3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
      65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
      757575756C566058483434485860566C75757575754324283237373228244375
      75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
      757575757575736A5D55555D6A73757575757575757575757575757575757575
      757575757575757575757575757575757575}
    NumGlyphs = 2
    OnClick = btnCancelBarraClick
  end
  object SpeedButton1: TSpeedButton [2]
    Left = 104
    Top = 305
    Width = 23
    Height = 22
    Flat = True
    Glyph.Data = {
      A6030000424DA603000000000000A60100002800000020000000100000000100
      08000000000000020000232E0000232E00005C00000000000000343434003535
      3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
      49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
      63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
      800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
      A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
      B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
      BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
      C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
      D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
      CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
      E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
      F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
      5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
      3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
      3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
      2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
      284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
      234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
      54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
      3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
      323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
      5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
      57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
      58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
      5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
      5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
      53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
      5B5B5B5B5B5B5B5B5B5B}
    NumGlyphs = 2
    OnClick = btnGravaBarraClick
  end
  object SpeedButton2: TSpeedButton [3]
    Left = 128
    Top = 305
    Width = 23
    Height = 22
    Flat = True
    Glyph.Data = {
      0E040000424D0E040000000000000E0200002800000020000000100000000100
      08000000000000020000232E0000232E000076000000000000000021CC001031
      DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
      DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
      FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
      F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
      F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
      FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
      E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
      ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
      FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
      C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
      CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
      D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
      E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
      E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
      F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
      75757575757575757575757575757575757575757575622F080000082F627575
      757575757575705E493434495E70757575757575753802030E11110E03023875
      7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
      7575757567354D5354555554534D35677575756307102A00337575442C151007
      63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
      367575604B545568345E7575745B544B607575171912301C3700317575401219
      1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
      057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
      0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
      217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
      3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
      65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
      757575756C566058483434485860566C75757575754324283237373228244375
      75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
      757575757575736A5D55555D6A73757575757575757575757575757575757575
      757575757575757575757575757575757575}
    NumGlyphs = 2
    OnClick = btnCancelBarraClick
  end
  object dbDesconto: TLabel [4]
    Left = 680
    Top = 170
    Width = 61
    Height = 13
    Caption = 'Pre'#231'o 19%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited PageControl1: TPageControl
    Width = 943
    Height = 561
    ActivePage = tsRegraNegocio
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 442
        Width = 935
        DesignSize = (
          935
          72)
        inherited Label2: TLabel
          Left = 79
        end
        object Label10: TLabel [2]
          Left = 341
          Top = 3
          Width = 81
          Height = 13
          Caption = 'C'#243'digo de Barras'
        end
        object Label7: TLabel [9]
          Left = 468
          Top = 3
          Width = 67
          Height = 13
          Caption = 'Principio Ativo'
        end
        inherited EdNome: TEdit
          Left = 79
          TabOrder = 4
        end
        inherited ButBusca: TBitBtn
          Left = 623
          TabOrder = 2
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 836
          TabOrder = 10
        end
        inherited EdCod: TEdit
          TabOrder = 3
        end
        inherited ButFiltro: TBitBtn
          Left = 744
          Top = 12
          TabOrder = 0
        end
        inherited ButAltLin: TButton
          Left = 838
          Top = 12
          TabOrder = 1
        end
        object edBarras: TEdit
          Left = 341
          Top = 18
          Width = 121
          Height = 21
          TabOrder = 5
          OnKeyPress = EdCodKeyPress
        end
        object cbbPAtivo: TJvDBLookupCombo
          Left = 468
          Top = 18
          Width = 145
          Height = 21
          EmptyValue = '0'
          LookupField = 'PAT_ID'
          LookupDisplay = 'PATIVO'
          LookupSource = dsPAtivo
          TabOrder = 6
        end
        object btnLab: TBitBtn
          Left = 524
          Top = 44
          Width = 80
          Height = 25
          Caption = 'Fabricantes'
          TabOrder = 9
          OnClick = btnLabClick
        end
        object btnGrupos: TBitBtn
          Left = 439
          Top = 44
          Width = 80
          Height = 25
          Caption = 'Grupos'
          TabOrder = 8
          OnClick = btnGruposClick
        end
        object btnClasse: TBitBtn
          Left = 354
          Top = 44
          Width = 80
          Height = 25
          Caption = 'Classes'
          TabOrder = 7
          OnClick = btnClasseClick
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 935
        Height = 442
        Columns = <
          item
            Expanded = False
            FieldName = 'PROD_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Width = 350
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'REGMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRECO_VND'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRECO_CUSTO'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODBARRAS'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APRES'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CLAS_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CLASSE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'SCLAS_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODINBS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FAM_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GENERICO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO_PROD_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'LAB_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LABOR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'LIBCIP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LISTA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'REGMS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VIGENCIA'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 514
        Width = 935
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 501
        Width = 935
      end
      inherited Panel3: TPanel
        Width = 935
        Height = 501
        object Label3: TLabel
          Left = 16
          Top = 10
          Width = 39
          Height = 13
          Caption = 'Prod. ID'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 96
          Top = 10
          Width = 58
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = DBEdit2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 592
          Top = 50
          Width = 92
          Height = 13
          Caption = 'Pre'#231'o de Venda'
          FocusControl = DBEdit3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 688
          Top = 50
          Width = 88
          Height = 13
          Caption = 'Pre'#231'o de Custo'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 16
          Top = 90
          Width = 53
          Height = 13
          Caption = 'Laborat'#243'rio'
        end
        object Label9: TLabel
          Left = 656
          Top = 10
          Width = 98
          Height = 13
          Caption = 'C'#243'digo de Barras'
          FocusControl = DBEdit6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 520
          Top = 90
          Width = 226
          Height = 13
          Caption = 'Familia (Grupo de Produto do Guia da Farmacia)'
        end
        object Label12: TLabel
          Left = 16
          Top = 130
          Width = 66
          Height = 13
          Caption = 'Principio ativo'
        end
        object Label13: TLabel
          Left = 256
          Top = 130
          Width = 85
          Height = 13
          Caption = 'Classe do produto'
        end
        object Label14: TLabel
          Left = 520
          Top = 130
          Width = 106
          Height = 13
          Caption = 'Sub classe do produto'
        end
        object Label15: TLabel
          Left = 16
          Top = 170
          Width = 46
          Height = 13
          Caption = 'Tipo Lista'
        end
        object Label16: TLabel
          Left = 232
          Top = 170
          Width = 67
          Height = 13
          Caption = 'Tipo de Pre'#231'o'
        end
        object Label17: TLabel
          Left = 240
          Top = 210
          Width = 81
          Height = 13
          Caption = 'C'#243'digo de Barras'
        end
        object Label18: TLabel
          Left = 312
          Top = 50
          Width = 66
          Height = 13
          Caption = 'Apresenta'#231#227'o'
          FocusControl = DBEdit5
        end
        object Label19: TLabel
          Left = 16
          Top = 50
          Width = 28
          Height = 13
          Caption = 'Nome'
          FocusControl = DBEdit7
        end
        object Label20: TLabel
          Left = 256
          Top = 90
          Width = 84
          Height = 13
          Caption = 'Grupo de Produto'
        end
        object btnIncBarra: TSpeedButton
          Left = 240
          Top = 297
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            5A030000424D5A030000000000005A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004900000000000000117611001379
            1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
            37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
            4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
            6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
            7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
            840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
            C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
            D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
            DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
            4848484848484848484848484848484848484848484848484848484848484848
            484848480C00000C484848484848484848484848302323304848484848484848
            4848484801161601484848484848484848484848243A3A244848484848484848
            4848484802181802484848484848484848484848253C3C254848484848484848
            48484848031A1A03484848484848484848484848263D3D264848484848484848
            48484848041B1B04484848484848484848484848273F3F274848484848484813
            0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
            1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
            2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
            0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
            484848480D21210D484848484848484848484848324444324848484848484848
            4848484810282810484848484848484848484848354545354848484848484848
            4848484812292912484848484848484848484848374646374848484848484848
            48484848152A2A15484848484848484848484848394747394848484848484848
            484848481E16161E484848484848484848484848423A3A424848484848484848
            484848484848484848484848484848484848484848484848484848484848}
          NumGlyphs = 2
          OnClick = btnIncBarraClick
        end
        object btnDelBarra: TSpeedButton
          Left = 264
          Top = 297
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            BE020000424DBE02000000000000BE0000002800000020000000100000000100
            08000000000000020000232E0000232E000022000000000000000526CF000D2E
            D4001335DB001436D9001D3FDE002446E2002A4CE6002F51F0004059D9004769
            FF004D6FFF005476FF005B7DFF006183FF006587FF00728CFF00B8B9B900BABB
            BC00BEBFBF0087A9FF00C1C2C300C4C5C500C5C6C700C8C9C900C9CACA00D1D2
            D200D3D4D400D6D6D700D7D8D800D9DADA00DBDBDC00DCDDDD00E5E5E600FFFF
            FF00212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121210800010304050606050403010008212117101112141516161514121110
            172121020B0B0B0B0B0B0B0B0B0B0B0B022121121B1B1B1B1B1B1B1B1B1B1B1B
            1221210713131313131313131313131307212118202020202020202020202020
            1821210F090A0B0C0D0E0E0D0C0B0A090F21211F191A1B1C1D1E1E1D1C1B1A19
            1F21212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121212121212121212121212121212121212121212121212121212121212121
            2121}
          NumGlyphs = 2
          OnClick = btnDelBarraClick
        end
        object btnGravaBarra: TSpeedButton
          Left = 344
          Top = 297
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
          OnClick = btnGravaBarraClick
        end
        object btnCancelBarra: TSpeedButton
          Left = 368
          Top = 297
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
          OnClick = btnCancelBarraClick
        end
        object Label21: TLabel
          Left = 680
          Top = 170
          Width = 61
          Height = 13
          Caption = 'Pre'#231'o 19%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 16
          Top = 25
          Width = 73
          Height = 21
          TabStop = False
          Color = clBtnFace
          DataField = 'PROD_ID'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 96
          Top = 25
          Width = 553
          Height = 21
          CharCase = ecUpperCase
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 592
          Top = 65
          Width = 90
          Height = 21
          DataField = 'PRECO_VND'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object DBEdit4: TDBEdit
          Left = 688
          Top = 65
          Width = 89
          Height = 21
          DataField = 'PRECO_CUSTO'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object JvDBLookupCombo1: TJvDBLookupCombo
          Left = 16
          Top = 105
          Width = 233
          Height = 21
          DataField = 'LAB_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          LookupField = 'LAB_ID'
          LookupDisplay = 'NOMELAB'
          LookupSource = dsFaboratorio
          TabOrder = 7
        end
        object DBEdit6: TDBEdit
          Left = 656
          Top = 25
          Width = 121
          Height = 21
          DataField = 'CODBARRAS'
          DataSource = DSCadastro
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object JvDBLookupCombo2: TJvDBLookupCombo
          Left = 520
          Top = 105
          Width = 257
          Height = 21
          DataField = 'FAM_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          LookupField = 'FAM_ID'
          LookupDisplay = 'FAMILIA'
          LookupSource = dsFamilia
          TabOrder = 9
        end
        object JvDBLookupCombo3: TJvDBLookupCombo
          Left = 16
          Top = 145
          Width = 233
          Height = 21
          DataField = 'PAT_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          LookupField = 'PAT_ID'
          LookupDisplay = 'PATIVO'
          LookupSource = dsPAtivo
          TabOrder = 10
        end
        object JvDBLookupCombo4: TJvDBLookupCombo
          Left = 256
          Top = 145
          Width = 257
          Height = 21
          DataField = 'CLAS_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          LookupField = 'CLAS_ID'
          LookupDisplay = 'CLASSE'
          LookupSource = dsClasse
          TabOrder = 11
        end
        object JvDBLookupCombo5: TJvDBLookupCombo
          Left = 520
          Top = 145
          Width = 257
          Height = 21
          DataField = 'SCLAS_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          LookupField = 'SCLAS_ID'
          LookupDisplay = 'SUBCLASSE'
          LookupSource = dsSClasse
          TabOrder = 12
        end
        object JvDBComboBox1: TJvDBComboBox
          Left = 16
          Top = 185
          Width = 209
          Height = 21
          DataField = 'LISTA'
          DataSource = DSCadastro
          Items.Strings = (
            'POSITIVO'
            'NEGATIVO'
            'NEUTRO')
          TabOrder = 13
          Values.Strings = (
            'P'
            'N'
            'U')
          ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
          ListSettings.OutfilteredValueFont.Color = clRed
          ListSettings.OutfilteredValueFont.Height = -11
          ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
          ListSettings.OutfilteredValueFont.Style = []
        end
        object JvDBComboBox2: TJvDBComboBox
          Left = 232
          Top = 185
          Width = 433
          Height = 21
          DataField = 'LIBCIP'
          DataSource = DSCadastro
          Items.Strings = (
            'MEDICAMENTO CONTROLADO (COM PRECO MAXIMO AO CONSUMIDOR)'
            'MEDICAMENTO LIBERADO (SEM PRECO MAXIMO AO CONSUMIDOR)'
            
              'MEDICAMENTO COM AUMENTO DE PRECOS LIBERADO PARA OS LABORAOTIRIOS' +
              ' PELO GOVERNO (COM PRECO MAXIMO AO CONSUMIDOR)'
            'PRODUTO CORRELATO (SEM PRECO MAXIMO AO CONSUMIDOR)'
            'MEDICAMENTO HOSPITALAR (SEM PRECO MAXIMO AO CONSUMIDOR)')
          TabOrder = 14
          Values.Strings = (
            'M'
            'L'
            'X'
            'O'
            'H')
          ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
          ListSettings.OutfilteredValueFont.Color = clRed
          ListSettings.OutfilteredValueFont.Height = -11
          ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
          ListSettings.OutfilteredValueFont.Style = []
        end
        object grdBarra: TJvDBGrid
          Left = 232
          Top = 225
          Width = 239
          Height = 72
          Ctl3D = False
          DataSource = dsBarras
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          TabOrder = 16
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'BARRAS'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CODINBS'
              Visible = True
            end>
        end
        object DBEdit5: TDBEdit
          Left = 312
          Top = 65
          Width = 273
          Height = 21
          DataField = 'APRES'
          DataSource = DSCadastro
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 16
          Top = 65
          Width = 289
          Height = 21
          DataField = 'NOME'
          DataSource = DSCadastro
          TabOrder = 3
        end
        object JvDBLookupCombo7: TJvDBLookupCombo
          Left = 256
          Top = 105
          Width = 257
          Height = 21
          DataField = 'GRUPO_PROD_ID'
          DataSource = DSCadastro
          DisplayEmpty = 'NAO RELACIONADO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          LookupField = 'GRUPO_PROD_ID'
          LookupDisplay = 'DESCRICAO'
          LookupSource = dsGrupo
          ParentFont = False
          TabOrder = 8
        end
        object dbALT_LIBERADA: TCheckBox
          Left = 16
          Top = 224
          Width = 193
          Height = 17
          Caption = 'Altera'#231#227'o de Pre'#231'o Liberada'
          TabOrder = 15
        end
        object dbValor: TCurrencyEdit
          Left = 672
          Top = 184
          Width = 105
          Height = 21
          AutoSize = False
          DisplayFormat = '###,###,##0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 17
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 935
      end
      inherited GridHistorico: TJvDBGrid
        Width = 935
        Height = 486
        Columns = <
          item
            Expanded = False
            FieldName = 'DETALHE'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_HORA'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CADASTRO'
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAMPO'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_ANT'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_POS'
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERACAO'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MOTIVO'
            Width = 305
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SOLICITANTE'
            Visible = True
          end>
      end
    end
    object tsRegraNegocio: TTabSheet
      Caption = 'Regra de Neg'#243'cio'
      ImageIndex = 3
      OnEnter = tsRegraNegocioEnter
      object lbl1: TLabel
        Left = 8
        Top = 96
        Width = 34
        Height = 13
        Caption = 'Regras'
      end
      object Label22: TLabel
        Left = 272
        Top = 96
        Width = 46
        Height = 13
        Caption = 'Desconto'
      end
      object Label23: TLabel
        Left = 8
        Top = 10
        Width = 39
        Height = 13
        Caption = 'Prod. ID'
        FocusControl = DBEdit8
      end
      object lbl2: TLabel
        Left = 544
        Top = 48
        Width = 28
        Height = 13
        Caption = 'Pre'#231'o'
      end
      object lbl3: TLabel
        Left = 8
        Top = 50
        Width = 98
        Height = 13
        Caption = 'C'#243'digo de Barras'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl4: TLabel
        Left = 144
        Top = 50
        Width = 83
        Height = 13
        Caption = 'Nome do Produto'
        FocusControl = DBEdit8
      end
      object cbbRegra: TComboBox
        Left = 8
        Top = 112
        Width = 233
        Height = 21
        ItemHeight = 13
        TabOrder = 1
        Text = 'SELECIONE A REGRA'
      end
      object btnGravarRegra: TBitBtn
        Left = 6
        Top = 220
        Width = 85
        Height = 25
        Hint = 'Gravar registro'
        Caption = '&Gravar'
        TabOrder = 3
        OnClick = btnGravarRegraClick
        Glyph.Data = {
          A6030000424DA603000000000000A60100002800000020000000100000000100
          08000000000000020000232E0000232E00005C00000000000000343434003535
          3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
          49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
          63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
          800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
          A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
          B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
          BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
          C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
          D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
          CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
          E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
          F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
          5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
          3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
          3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
          2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
          284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
          234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
          54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
          3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
          323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
          5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
          57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
          58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
          5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
          5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
          53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
          5B5B5B5B5B5B5B5B5B5B}
        NumGlyphs = 2
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 25
        Width = 73
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'PROD_ID'
        DataSource = DSCadastro
        Enabled = False
        TabOrder = 0
      end
      object edtDesconto: TCurrencyEdit
        Left = 272
        Top = 112
        Width = 121
        Height = 21
        AutoSize = False
        DisplayFormat = '###,###,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object edtValor: TCurrencyEdit
        Left = 544
        Top = 64
        Width = 121
        Height = 21
        AutoSize = False
        DisplayFormat = '###,###,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object edtBarras: TEdit
        Left = 8
        Top = 64
        Width = 121
        Height = 21
        TabOrder = 5
        OnChange = edtBarrasChange
      end
      object edtNome: TEdit
        Left = 144
        Top = 64
        Width = 377
        Height = 21
        TabOrder = 6
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 943
    inherited panTitulo: TPanel
      Width = 945
      inherited ButClose: TSpeedButton
        Left = 924
      end
    end
  end
  inherited panStatus2: TPanel
    Width = 943
  end
  inherited DSCadastro: TDataSource
    Left = 972
    Top = 592
  end
  inherited PopupBut: TPopupMenu
    Left = 428
    Top = 496
  end
  inherited PopupGrid1: TPopupMenu
    Left = 468
    Top = 496
  end
  inherited QHistorico: TADOQuery
    Left = 1012
    Top = 552
  end
  inherited DSHistorico: TDataSource
    Left = 1012
    Top = 592
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'SELECT * FROM produtos'
      'where apagado <> '#39'S'#39
      'and prod_id = 0')
    Left = 972
    Top = 552
    object QCadastroPROD_ID: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'PROD_ID'
      Required = True
    end
    object QCadastroCODINBS: TStringField
      DisplayLabel = 'Cod. INBS'
      FieldName = 'CODINBS'
      Size = 13
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 90
    end
    object QCadastroSN: TStringField
      FieldName = 'SN'
      Size = 7
    end
    object QCadastroFLAGNOME: TStringField
      DisplayLabel = 'Flag Nome'
      FieldName = 'FLAGNOME'
      Size = 1
    end
    object QCadastroMT: TStringField
      FieldName = 'MT'
      Size = 2
    end
    object QCadastroPRECO_FINAL: TFloatField
      DisplayLabel = 'Pre'#231'o Final'
      FieldName = 'PRECO_FINAL'
      DisplayFormat = '###,###,##0.00'
    end
    object QCadastroPRECO_SEM_DESCONTO: TFloatField
      DisplayLabel = 'Pre'#231'o Sem Desconto'
      FieldName = 'PRECO_SEM_DESCONTO'
      DisplayFormat = '###,###,##0.00'
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroENVIADO_FARMACIA: TStringField
      DisplayLabel = 'Enviado Farmacia'
      FieldName = 'ENVIADO_FARMACIA'
      Size = 1
    end
    object QCadastroDIGITADO_SITE: TStringField
      DisplayLabel = 'Digitado Site'
      FieldName = 'DIGITADO_SITE'
      Size = 1
    end
    object QCadastroCOD_ABC: TIntegerField
      DisplayLabel = 'C'#243'd. ABC'
      FieldName = 'COD_ABC'
    end
    object QCadastroLIBCIP: TStringField
      DisplayLabel = 'Lib. Cip.'
      FieldName = 'LIBCIP'
      Size = 1
    end
    object QCadastroNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 45
    end
    object QCadastroAPRES: TStringField
      DisplayLabel = 'Apres'
      FieldName = 'APRES'
      Size = 45
    end
    object QCadastroPRECO_VND: TFloatField
      DisplayLabel = 'Pre'#231'o de Venda'
      FieldName = 'PRECO_VND'
      DisplayFormat = '###,###,##0.00'
    end
    object QCadastroPRECO_CUSTO: TFloatField
      DisplayLabel = 'Pre'#231'o de Custo'
      FieldName = 'PRECO_CUSTO'
      DisplayFormat = '###,###,##0.00'
    end
    object QCadastroPRINCIPIO: TStringField
      DisplayLabel = 'Principio Ativo'
      FieldName = 'PRINCIPIO'
      Size = 130
    end
    object QCadastroUNID_CAIXA: TIntegerField
      DisplayLabel = 'Unid. Caixa'
      FieldName = 'UNID_CAIXA'
    end
    object QCadastroCLASSE_PRODUTO: TStringField
      DisplayLabel = 'Classe Prod.'
      FieldName = 'CLASSE_PRODUTO'
      Size = 1
    end
    object QCadastroIPI: TFloatField
      FieldName = 'IPI'
    end
    object QCadastroLISTA: TStringField
      DisplayLabel = 'Lista'
      FieldName = 'LISTA'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroCOD_GUIA: TIntegerField
      DisplayLabel = 'C'#243'd. Guia'
      FieldName = 'COD_GUIA'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroFAM_ID: TIntegerField
      DisplayLabel = 'Fam. ID'
      FieldName = 'FAM_ID'
    end
    object QCadastroPAT_ID: TIntegerField
      DisplayLabel = 'PAtiv. ID'
      FieldName = 'PAT_ID'
    end
    object QCadastroCLAS_ID: TIntegerField
      DisplayLabel = 'Classe ID'
      FieldName = 'CLAS_ID'
    end
    object QCadastroSCLAS_ID: TIntegerField
      DisplayLabel = 'Sub. Classe ID'
      FieldName = 'SCLAS_ID'
    end
    object QCadastroREGMS: TStringField
      DisplayLabel = 'Reg. MS'
      FieldName = 'REGMS'
      Size = 15
    end
    object QCadastroGENERICO: TStringField
      DisplayLabel = 'Gen'#233'rico'
      FieldName = 'GENERICO'
      Size = 1
    end
    object QCadastroPORTARIA: TStringField
      DisplayLabel = 'Portaria'
      FieldName = 'PORTARIA'
      Size = 10
    end
    object QCadastroGRUPO_PROD_ID: TIntegerField
      DisplayLabel = 'Grupo ID.'
      FieldName = 'GRUPO_PROD_ID'
    end
    object QCadastroLAB_ID: TIntegerField
      DisplayLabel = 'Lab. ID'
      FieldName = 'LAB_ID'
    end
    object QCadastroFABR_ID: TIntegerField
      FieldName = 'FABR_ID'
    end
    object QCadastroCODBARRAS: TStringField
      DisplayLabel = 'C'#243'digo de Barras'
      FieldName = 'CODBARRAS'
      Size = 13
    end
    object QCadastroLABORA: TStringField
      FieldName = 'LABORA'
      Size = 30
    end
    object QCadastroDATA: TDateTimeField
      FieldName = 'DATA'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroVIGENCIA: TDateTimeField
      FieldName = 'VIGENCIA'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object dsFaboratorio: TDataSource
    DataSet = qLaboratorio
    Left = 1228
    Top = 593
  end
  object dsPAtivo: TDataSource
    DataSet = qPAtivo
    Left = 1052
    Top = 593
  end
  object dsFamilia: TDataSource
    DataSet = qFamilia
    Left = 1092
    Top = 593
  end
  object dsClasse: TDataSource
    DataSet = qClasse
    Left = 1132
    Top = 593
  end
  object dsSClasse: TDataSource
    DataSet = qSClasse
    Left = 1180
    Top = 593
  end
  object dsBarras: TDataSource
    DataSet = qBarras
    OnStateChange = dsBarrasStateChange
    Left = 1268
    Top = 593
  end
  object dsGrupo: TDataSource
    DataSet = qGrupo
    Left = 1309
    Top = 592
  end
  object qPAtivo: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from pativo')
    Left = 1052
    Top = 553
    object qPAtivoPAT_ID: TIntegerField
      FieldName = 'PAT_ID'
    end
    object qPAtivoPATIVO: TStringField
      FieldName = 'PATIVO'
      Size = 80
    end
  end
  object qFamilia: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from familias')
    Left = 1092
    Top = 553
    object qFamiliaFAM_ID: TIntegerField
      FieldName = 'FAM_ID'
    end
    object qFamiliaFAMILIA: TStringField
      FieldName = 'FAMILIA'
      Size = 50
    end
  end
  object qClasse: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from classe')
    Left = 1132
    Top = 553
    object qClasseCLAS_ID: TIntegerField
      FieldName = 'CLAS_ID'
    end
    object qClasseCLASSE: TStringField
      FieldName = 'CLASSE'
      Size = 50
    end
  end
  object qSClasse: TADOQuery
    Connection = DMConexao.AdoCon
    DataSource = dsClasse
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'select * from subclasse')
    Left = 1172
    Top = 553
    object qSClasseSCLAS_ID: TIntegerField
      FieldName = 'SCLAS_ID'
    end
    object qSClasseSUBCLASSE: TStringField
      FieldName = 'SUBCLASSE'
      Size = 50
    end
    object qSClasseCLAS_ID: TIntegerField
      FieldName = 'CLAS_ID'
    end
  end
  object qLaboratorio: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from laboratorios')
    Left = 1228
    Top = 553
    object qLaboratorioLAB_ID: TIntegerField
      FieldName = 'LAB_ID'
    end
    object qLaboratorioNOMELAB: TStringField
      FieldName = 'NOMELAB'
      Size = 50
    end
    object qLaboratorioNOMEFANT: TStringField
      FieldName = 'NOMEFANT'
    end
  end
  object qBarras: TADOQuery
    Connection = DMConexao.AdoCon
    AfterInsert = qBarrasAfterInsert
    DataSource = DSCadastro
    Parameters = <
      item
        Name = 'prod_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select * from barras where prod_id = :prod_id')
    Left = 1268
    Top = 553
    object qBarrasBARRAS_ID: TIntegerField
      FieldName = 'BARRAS_ID'
    end
    object qBarrasBARRAS: TStringField
      FieldName = 'BARRAS'
      Size = 13
    end
    object qBarrasCODINBS: TStringField
      FieldName = 'CODINBS'
      Size = 13
    end
    object qBarrasPROD_ID: TIntegerField
      FieldName = 'PROD_ID'
    end
  end
  object qGrupo: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select grupo_prod_id, descricao from grupo_prod where apagado <>' +
        ' '#39'S'#39)
    Left = 1308
    Top = 553
    object qGrupogrupo_prod_id: TIntegerField
      FieldName = 'grupo_prod_id'
    end
    object qGrupodescricao: TStringField
      FieldName = 'descricao'
      Size = 60
    end
  end
  object ADORegraNegocio: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 76
    Top = 457
    object ADORegraNegocioPROG_ID: TIntegerField
      FieldName = 'PROG_ID'
    end
    object ADORegraNegocioNOME: TStringField
      FieldName = 'NOME'
      Size = 200
    end
  end
  object dsRegra: TDataSource
    DataSet = ADORegraNegocio
    Left = 36
    Top = 457
  end
  object qAutorizador: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM produtos'
      'where apagado <> '#39'S'#39
      'and prod_id = 0')
    Left = 36
    Top = 408
    object qAutorizadorPROD_CODIGO: TStringField
      FieldName = 'PROD_CODIGO'
      Size = 30
    end
    object qAutorizadorPROD_DESCR: TStringField
      FieldName = 'PROD_DESCR'
      Size = 50
    end
    object qAutorizadorPRE_VALOR: TFloatField
      FieldName = 'PRE_VALOR'
    end
    object qAutorizadorPROD_AUT_ID: TIntegerField
      FieldName = 'PROD_AUT_ID'
    end
  end
  object dsAutorizador: TDataSource
    DataSet = qAutorizador
    Left = 84
    Top = 408
  end
end
