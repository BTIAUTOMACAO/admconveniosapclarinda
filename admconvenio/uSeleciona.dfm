object fSeleciona: TfSeleciona
  Left = 249
  Top = 168
  Width = 456
  Height = 333
  Caption = 'fSeleciona'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 209
    Top = 0
    Width = 20
    Height = 264
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 3
      Top = 91
      Width = 23
      Height = 22
      Caption = '>'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 3
      Top = 115
      Width = 23
      Height = 22
      Caption = '<'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 3
      Top = 147
      Width = 23
      Height = 22
      Caption = '>>'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton3Click
    end
    object SpeedButton4: TSpeedButton
      Left = 3
      Top = 171
      Width = 23
      Height = 22
      Caption = '<<'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton4Click
    end
  end
  object Panel3: TPanel
    Left = 229
    Top = 0
    Width = 211
    Height = 264
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 211
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 5
        Top = 7
        Width = 90
        Height = 13
        Caption = 'Itens Selecionados'
      end
    end
    object Panel8: TPanel
      Left = 0
      Top = 24
      Width = 211
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object JvDBGrid2: TJvDBGrid
        Left = 0
        Top = 0
        Width = 209
        Height = 238
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsTempSel
        FixedColor = clBackground
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid2DblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 180
            Visible = True
          end>
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 264
    Width = 440
    Height = 31
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object btnOk: TBitBtn
      Left = 131
      Top = 3
      Width = 90
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      Glyph.Data = {
        46030000424D4603000000000000460100002800000020000000100000000100
        08000000000000020000232E0000232E00004400000000000000117611001379
        1300177D17001C821C0021872100278D27002C922C0032983200379D37003CA2
        3C0040A6400043A9430054BA540058BE58005DC35D0063CA63006AD16A0071D8
        710078DF78007EE57E00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD00BEBF
        BF009BCE9B0083EA830087EE8700A1D4A100BFC0C100C2C3C300C5C5C600C6C7
        C800C8C9C900CACACB00C3DDC300C4DDC400C5DEC500D1D1D200D2D3D300D5D5
        D600D7D8D800DADADB00DDDEDE00C6E0C600C7E1C700C9E2C900CAE4CA00CCE5
        CC00CDE6CD00CEE8CE00CFE9CF00D0E9D000E0E0E100E2E2E200E2E3E300E4E5
        E500E5E5E600E7E7E700EDEDED00EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1
        F100F2F2F200FFFFFF0043434343434343434343434343434343434343434343
        4343434343434343434343434343240000244343434343434343434343433C14
        143C434343434343434343434325010C0C0125434343434343434343433C1527
        27153C43434343434343434326020D0D0D0D022643434343434343433D162828
        2828163D434343434343432D030E0E1C1C0E0E032D4343434343433D1729293B
        3B2929173D434343434343040F0F1C04041C0F0F042E4343434343182A2A3B18
        183B2A2A183E434343434305101C052F2F051C1010052F43434343192B3B193F
        3F193B2B2B193F43434343061C0630434330061C111106304343431E3B1E3F43
        433F1E3B2C2C1E3F4343430707314343434331071C1212073143431F1F404343
        4343401F3B36361F4043431A3243434343434332081C13130843433741434343
        43434341203B383820434343434343434343434333091C1B0943434343434343
        4343434341213B3A21434343434343434343434343340A1C0A43434343434343
        434343434342223B2243434343434343434343434343350B0B43434343434343
        4343434343434223234343434343434343434343434343351D43434343434343
        4343434343434342394343434343434343434343434343434343434343434343
        4343434343434343434343434343434343434343434343434343434343434343
        43434343434343434343}
      NumGlyphs = 2
    end
    object btnCancel: TBitBtn
      Left = 227
      Top = 3
      Width = 90
      Height = 25
      Caption = 'Cancelar'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        0E040000424D0E040000000000000E0200002800000020000000100000000100
        08000000000000020000232E0000232E000076000000000000000021CC001031
        DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
        DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
        FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
        F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
        F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
        FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
        E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
        ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
        FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
        C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
        CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
        D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
        E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
        E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
        F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
        75757575757575757575757575757575757575757575622F080000082F627575
        757575757575705E493434495E70757575757575753802030E11110E03023875
        7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
        7575757567354D5354555554534D35677575756307102A00337575442C151007
        63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
        367575604B545568345E7575745B544B607575171912301C3700317575401219
        1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
        057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
        0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
        217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
        3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
        65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
        757575756C566058483434485860566C75757575754324283237373228244375
        75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
        757575757575736A5D55555D6A73757575757575757575757575757575757575
        757575757575757575757575757575757575}
      NumGlyphs = 2
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 209
    Height = 264
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 209
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 5
        Width = 49
        Height = 13
        Caption = 'Busca Por'
      end
      object edTexto: TEdit
        Left = 85
        Top = 20
        Width = 122
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 1
        OnChange = edTextoChange
      end
      object cbbBuscaPor: TComboBox
        Left = 8
        Top = 20
        Width = 70
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = 'C'#243'digo'
        Items.Strings = (
          'C'#243'digo'
          'Nome')
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 48
      Width = 209
      Height = 216
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 207
        Height = 214
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsTemp
        FixedColor = clBackground
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'CODIGO'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 125
            Visible = True
          end>
      end
    end
  end
  object qTemp: TRxMemoryData
    FieldDefs = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 60
      end>
    Left = 152
    Top = 100
    object qTempCODIGO: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'CODIGO'
    end
    object qTempNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 60
    end
  end
  object qTempSel: TRxMemoryData
    FieldDefs = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 60
      end>
    Left = 256
    Top = 100
    object qTempSelCODIGO: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'CODIGO'
    end
    object qTempSelNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 60
    end
  end
  object dsTempSel: TDataSource
    DataSet = qTempSel
    Left = 256
    Top = 144
  end
  object dsTemp: TDataSource
    DataSet = qTemp
    Left = 152
    Top = 144
  end
  object Q: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 256
    Top = 184
  end
end
