object FGeraCartConvNovo: TFGeraCartConvNovo
  Left = 291
  Top = 234
  Width = 926
  Height = 744
  ActiveControl = chImpCartEsp
  Caption = '[NOVOS CART'#213'ES] Gera'#231#227'o de Arquivo para cart'#227'o de conveniados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 910
    Height = 233
    Align = alTop
    Caption = 'Sele'#231#227'o.'
    TabOrder = 0
    object ButPesConv: TSpeedButton
      Left = 1017
      Top = 49
      Width = 23
      Height = 22
      Caption = '...'
      Enabled = False
      Visible = False
      OnClick = ButPesConvClick
    end
    object Bevel2: TBevel
      Left = 302
      Top = 14
      Width = 2
      Height = 147
    end
    object Label5: TLabel
      Left = 312
      Top = 96
      Width = 119
      Height = 13
      Caption = 'Filtro de Status do Cart'#227'o'
    end
    object Label6: TLabel
      Left = 764
      Top = 16
      Width = 69
      Height = 13
      Caption = 'Modelo Cart'#227'o'
    end
    object DBText1: TDBText
      Left = 765
      Top = 35
      Width = 60
      Height = 16
      AutoSize = True
      DataField = 'DESCRICAO'
      DataSource = dsModelosCartoes
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 312
      Top = 56
      Width = 25
      Height = 13
      Caption = 'Setor'
    end
    object DBEmpresa: TJvDBLookupCombo
      Left = 311
      Top = 33
      Width = 444
      Height = 21
      Enabled = False
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'EMPRES_ID;NOME'
      LookupDisplayIndex = 1
      LookupSource = DSEmpresa
      TabOrder = 4
      OnEnter = DBEmpresaEnter
      OnKeyUp = DBEmpresaKeyUp
    end
    object chempresa: TCheckBox
      Left = 311
      Top = 16
      Width = 161
      Height = 17
      Caption = 'Selecionar Empresa'
      TabOrder = 2
      OnClick = chempresaClick
    end
    object ChNomeCart: TCheckBox
      Left = 664
      Top = 128
      Width = 138
      Height = 17
      Caption = 'Selecionar um cart'#227'o'
      TabOrder = 10
      Visible = False
      OnClick = ChNomeCartClick
    end
    object EdNomeCart: TEdit
      Left = 663
      Top = 146
      Width = 251
      Height = 21
      CharCase = ecUpperCase
      Enabled = False
      TabOrder = 14
      Visible = False
    end
    object chSoNaoEmit: TCheckBox
      Left = 534
      Top = 10
      Width = 169
      Height = 17
      Caption = 'Somento Cart'#245'es n'#227'o Emitidos'
      Checked = True
      State = cbChecked
      TabOrder = 0
      Visible = False
    end
    object ButBusca: TButton
      Left = 665
      Top = 102
      Width = 89
      Height = 25
      Caption = '&Pesquisar'
      TabOrder = 7
      OnClick = ButBuscaClick
    end
    object chConvLib: TCheckBox
      Left = 310
      Top = 130
      Width = 177
      Height = 17
      Caption = 'Somente Conv'#234'niados Liberados'
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    object chCartLib: TCheckBox
      Left = 488
      Top = 97
      Width = 151
      Height = 17
      Caption = 'Somente Cart'#245'es Liberados'
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    object CkUsarCodImp: TCheckBox
      Left = 488
      Top = 147
      Width = 137
      Height = 17
      Caption = 'Usar Cod Importa'#231#227'o'
      TabOrder = 15
    end
    object chSoTitular: TCheckBox
      Left = 488
      Top = 114
      Width = 172
      Height = 17
      Caption = 'Somente Cart'#245'es de Titulares'
      TabOrder = 9
      OnClick = chSoTitularClick
    end
    object panTodasasdatas: TPanel
      Left = 8
      Top = 33
      Width = 288
      Height = 73
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 3
      object Label3: TLabel
        Left = 6
        Top = 49
        Width = 27
        Height = 13
        Caption = 'Inicial'
        Enabled = False
      end
      object Label4: TLabel
        Left = 148
        Top = 49
        Width = 22
        Height = 13
        Caption = 'Final'
        Enabled = False
      end
      object rdgDatas: TRadioGroup
        Left = 4
        Top = 2
        Width = 279
        Height = 34
        Caption = 'Busca por...'
        Columns = 2
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Data cadastro'
          'Data emiss'#227'o')
        TabOrder = 0
      end
      object DataIni: TJvDateEdit
        Left = 38
        Top = 43
        Width = 103
        Height = 21
        Hint = 'Data inicial para os dados da pesquisa'
        DefaultToday = True
        Enabled = False
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 1
      end
      object Datafim: TJvDateEdit
        Left = 176
        Top = 43
        Width = 105
        Height = 21
        Hint = 'Data final para os dados da pesquisa'
        DefaultToday = True
        Enabled = False
        NumGlyphs = 2
        ShowNullDate = False
        TabOrder = 2
      end
    end
    object ckbTodasdatas: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Todas as datas'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = ckbTodasdatasClick
    end
    object cbbFiltroCartao: TJvDBComboBox
      Left = 311
      Top = 109
      Width = 170
      Height = 21
      Items.Strings = (
        'N'#227'o emitidos'
        'Apenas emitidos'
        'Todos')
      TabOrder = 8
      Values.Strings = (
        '0'
        '1'
        '2')
      ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
      ListSettings.OutfilteredValueFont.Color = clRed
      ListSettings.OutfilteredValueFont.Height = -11
      ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
      ListSettings.OutfilteredValueFont.Style = []
    end
    object chSoDependentes: TCheckBox
      Left = 488
      Top = 130
      Width = 177
      Height = 17
      Caption = 'Somente Cart'#245'es de Depentente'
      TabOrder = 12
      OnClick = chSoDependentesClick
    end
    object lkDepartamento: TJvDBLookupCombo
      Left = 311
      Top = 72
      Width = 444
      Height = 21
      LookupField = 'dept_id'
      LookupDisplay = 'descricao'
      LookupDisplayIndex = 1
      LookupSource = DSDepart
      TabOrder = 5
      OnEnter = lkDepartamentoEnter
    end
    object Panel1: TPanel
      Left = 2
      Top = 188
      Width = 906
      Height = 43
      Align = alBottom
      Caption = 'NOVOS CART'#213'ES'
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
    end
    object cbbOrdenaSetor: TCheckBox
      Left = 310
      Top = 146
      Width = 177
      Height = 17
      Caption = 'Ordena por setor'
      Checked = True
      State = cbChecked
      TabOrder = 13
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 600
    Width = 910
    Height = 105
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 2
    object Label1: TLabel
      Left = 165
      Top = 42
      Width = 104
      Height = 13
      Caption = 'Salvar Arquivo como: '
    end
    object Bevel1: TBevel
      Left = 326
      Top = 11
      Width = 2
      Height = 20
    end
    object Label2: TLabel
      Left = 8
      Top = 41
      Width = 84
      Height = 13
      Caption = 'Modelo do Cart'#227'o'
    end
    object ButMarcDesm: TButton
      Left = 7
      Top = 8
      Width = 105
      Height = 25
      Caption = 'Marca/Desm.(F11)'
      TabOrder = 1
      OnClick = ButMarcDesmClick
    end
    object ButMarcaTodos: TButton
      Left = 112
      Top = 8
      Width = 105
      Height = 25
      Caption = 'Marca Todos (F8)'
      TabOrder = 2
      OnClick = ButMarcaTodosClick
    end
    object ButDesmTodos: TButton
      Left = 217
      Top = 8
      Width = 105
      Height = 25
      Caption = 'Desm. Todos (F9)'
      TabOrder = 3
      OnClick = ButDesmTodosClick
    end
    object ButProcessa: TButton
      Left = 632
      Top = 54
      Width = 124
      Height = 25
      Caption = 'Gerar Arquivo (F7)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = ButProcessaClick
    end
    object chMarcaCart: TCheckBox
      Left = 416
      Top = 50
      Width = 177
      Height = 17
      Caption = 'Marcar cart'#245'es como j'#225' emitidos.'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object Barra: TStatusBar
      Left = 1
      Top = 79
      Width = 904
      Height = 21
      Panels = <>
      SimplePanel = True
    end
    object Button1: TButton
      Left = 332
      Top = 8
      Width = 105
      Height = 25
      Caption = 'Excluir Marcados'
      TabOrder = 4
      OnClick = Button1Click
    end
    object RGTipoArq: TRadioGroup
      Left = 791
      Top = 0
      Width = 227
      Height = 36
      Caption = 'Exportar Arquivo Como:'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Arquivo Texto (txt)'
        'Arquivo Excel (xls)')
      TabOrder = 0
      OnClick = RGTipoArqClick
    end
    object cbModelo: TComboBox
      Left = 8
      Top = 57
      Width = 145
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 9
      Text = 'DB Titular (com matricula)'
      Items.Strings = (
        'DB Titular (com matricula)'
        'DB Dependente (com matricula)'
        'DB Titular (sem matricula)'
        'DB Dependente (sem matricula)')
    end
    object ckAbreExcel: TCheckBox
      Left = 656
      Top = 34
      Width = 97
      Height = 17
      Caption = 'Abrir Excel'
      TabOrder = 6
      Visible = False
    end
    object chImpCartEsp: TCheckBox
      Left = 416
      Top = 66
      Width = 209
      Height = 17
      Caption = 'Imprimir numero do cartao com espacos'
      TabOrder = 11
    end
    object Filename: TJvFilenameEdit
      Left = 164
      Top = 57
      Width = 241
      Height = 21
      Filter = 'Excel 97-2003|*.xls|Texto|*.txt'
      TabOrder = 10
      Text = 'C:\cartoes.txt'
      OnExit = FilenameExit
    end
    object chkGeraArqPorSetor: TCheckBox
      Left = 416
      Top = 34
      Width = 201
      Height = 17
      Caption = 'Gerar Arquivo com Quebra por Setor'
      TabOrder = 5
    end
  end
  object DBGrid1: TJvDBGrid
    Left = 0
    Top = 233
    Width = 910
    Height = 367
    Hint = 'Clique sobre o t'#237'tulo do campo desejado para ordenar.'
    Align = alClient
    DataSource = DataSource1
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -9
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    AutoAppend = False
    TitleButtons = True
    OnTitleBtnClick = DBGrid1TitleBtnClick
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'cartao'
        Title.Caption = 'C'#243'digo Cart'#227'o'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 236
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Titular'
        Width = 232
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Empresa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'modelo_cartoes'
        Title.Caption = 'Modelo do Cart.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'conv_id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATACADASTRO'
        Title.Caption = 'Cadastro'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'setor'
        Title.Caption = 'Setor'
        Visible = True
      end>
  end
  object DataSource1: TDataSource
    DataSet = Tempcartoes
    Left = 128
    Top = 304
  end
  object Tempcartoes: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Nome'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Empresa'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Marcado'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Titular'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Cidade'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'CARTAOTITU'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'cartao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cartao_id'
        DataType = ftInteger
      end
      item
        Name = 'DATACADASTRO'
        DataType = ftDateTime
      end
      item
        Name = 'CRED_ID'
        DataType = ftInteger
      end
      item
        Name = 'Empres_id'
        DataType = ftInteger
      end
      item
        Name = 'setor'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'setor_id'
        DataType = ftInteger
      end
      item
        Name = 'modelo_cartoes'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CVV'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Via'
        DataType = ftInteger
      end>
    Left = 128
    Top = 272
    object TempcartoesNome: TStringField
      FieldName = 'Nome'
      Size = 45
    end
    object TempcartoesEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 45
    end
    object TempcartoesTitular: TStringField
      FieldName = 'Titular'
      Size = 45
    end
    object TempcartoesCidade: TStringField
      FieldName = 'Cidade'
      Size = 45
    end
    object TempcartoesCARTAOTITU: TStringField
      FieldName = 'CARTAOTITU'
      Size = 1
    end
    object Tempcartoescartao: TStringField
      FieldName = 'cartao'
    end
    object Tempcartoescartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object TempcartoesMarcado: TStringField
      FieldName = 'Marcado'
      Size = 1
    end
    object Tempcartoesconv_id: TIntegerField
      DisplayLabel = 'Conv. ID'
      FieldName = 'conv_id'
    end
    object TempcartoesDATACADASTRO: TDateTimeField
      FieldName = 'DATACADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TempcartoesCRED_ID: TIntegerField
      DisplayLabel = 'Estab. ID'
      FieldName = 'CRED_ID'
    end
    object TempcartoesCHAPA: TFloatField
      FieldName = 'CHAPA'
      Required = True
    end
    object TempcartoesCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object TempcartoesEmpres_id: TIntegerField
      FieldName = 'Empres_id'
    end
    object Tempcartoessetor: TStringField
      FieldName = 'setor'
    end
    object Tempcartoessetor_id: TIntegerField
      FieldName = 'setor_id'
    end
    object Tempcartoesmodelo_cartoes: TStringField
      FieldName = 'modelo_cartoes'
    end
    object TempcartoesCVV: TStringField
      FieldName = 'CVV'
    end
    object TempcartoesVia: TIntegerField
      FieldName = 'Via'
    end
  end
  object DSEmpresa: TDataSource
    DataSet = QEmpresas
    OnDataChange = DSEmpresaDataChange
    Left = 64
    Top = 400
  end
  object dsModelosCartoes: TDataSource
    DataSet = qModelosCartoes
    Left = 104
    Top = 400
  end
  object QCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'Select cartoes.nome, cartoes.titular as cartaotitu, cartoes.codc' +
        'artimp,cartoes.via,cartoes.cvv,'
      
        'conveniados.titular, conveniados.empres_id, conveniados.cidade, ' +
        'conveniados.chapa,'
      'conveniados.setor,conveniados.setor_id,'
      'codcartimp as cartao, cartoes.cartao_id, cartoes.dtcadastro,'
      
        'empresas.nomecartao as empresa,MODELOS_CARTOES.DESCRICAO modelo_' +
        'cartoes, cartoes.conv_id from cartoes'
      'join  conveniados on conveniados.conv_id = cartoes.conv_id'
      'join  empresas    on empresas.empres_id  = conveniados.empres_id'
      
        'join  MODELOS_CARTOES on EMPRESAS.MOD_CART_ID = MODELOS_CARTOES.' +
        'MOD_CART_ID'
      'where cartoes.apagado <> '#39'S'#39)
    Left = 24
    Top = 368
    object QCartoesnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QCartoescartaotitu: TStringField
      FieldName = 'cartaotitu'
      FixedChar = True
      Size = 1
    end
    object QCartoescodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object QCartoestitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QCartoeschapa: TFloatField
      FieldName = 'chapa'
    end
    object QCartoescartao: TStringField
      FieldName = 'cartao'
    end
    object QCartoescartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object QCartoesdtcadastro: TDateTimeField
      FieldName = 'dtcadastro'
    end
    object QCartoesempresa: TStringField
      FieldName = 'empresa'
      Size = 45
    end
    object QCartoesconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QCartoesempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QCartoessetor: TStringField
      FieldName = 'setor'
      Size = 45
    end
    object QCartoessetor_id: TIntegerField
      FieldName = 'setor_id'
    end
    object QCartoesmodelo_cartoes: TStringField
      FieldName = 'modelo_cartoes'
    end
    object QCartoesvia: TIntegerField
      FieldName = 'via'
    end
    object QCartoescvv: TStringField
      FieldName = 'cvv'
      FixedChar = True
      Size = 3
    end
    object QCartoescidade: TIntegerField
      FieldName = 'cidade'
    end
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      'Select empres_id, nome, mod_cart_id from empresas where '
      'apagado <> '#39'S'#39' '
      'order by nome')
    Left = 64
    Top = 368
    object QEmpresasempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasmod_cart_id: TIntegerField
      FieldName = 'mod_cart_id'
    end
  end
  object qModelosCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    DataSource = DSEmpresa
    Parameters = <
      item
        Name = 'mod_cart_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select'
      '  *'
      'from modelos_cartoes'
      'where mod_cart_id = :mod_cart_id')
    Left = 104
    Top = 368
    object qModelosCartoesMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
    object qModelosCartoesDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
    end
    object qModelosCartoesOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 50
    end
    object qModelosCartoesAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object qModelosCartoesDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object qModelosCartoesDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object qModelosCartoesDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object qModelosCartoesOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qModelosCartoesOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
  end
  object QUpdate: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 144
    Top = 368
  end
  object QLotesCartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CARTOES_LOTES')
    Left = 184
    Top = 368
    object QLotesCartaoSEQ_LOTE: TIntegerField
      FieldName = 'SEQ_LOTE'
    end
    object QLotesCartaoDATA: TWideStringField
      FieldName = 'DATA'
      Size = 10
    end
    object QLotesCartaoHORA: TWideStringField
      FieldName = 'HORA'
      Size = 16
    end
    object QLotesCartaoNOME_ARQ: TStringField
      FieldName = 'NOME_ARQ'
      Size = 40
    end
    object QLotesCartaoEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QLotesCartaosetor_id: TIntegerField
      FieldName = 'setor_id'
    end
  end
  object QLotesCartaoDetails: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from CARTOES_LOTES_DETALHE')
    Left = 224
    Top = 368
    object QLotesCartaoDetailsSEQ_LOTE: TIntegerField
      FieldName = 'SEQ_LOTE'
    end
    object QLotesCartaoDetailsCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
  end
  object QDepart: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT empres_id, descricao, dept_id FROM emp_dptos WHERE empres' +
        '_id = :empres_id')
    Left = 488
    Top = 288
    object QDepartempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QDepartdescricao: TStringField
      FieldName = 'descricao'
      Size = 150
    end
    object QDepartdept_id: TIntegerField
      FieldName = 'dept_id'
    end
  end
  object DSDepart: TDataSource
    DataSet = QDepart
    Left = 520
    Top = 288
  end
end
