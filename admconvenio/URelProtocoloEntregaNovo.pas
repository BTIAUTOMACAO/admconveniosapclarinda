unit URelProtocoloEntregaNovo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, Grids, DBGrids, {JvDBCtrl, ClipBrd,}
  StdCtrls, ComCtrls, Mask, JvToolEdit, DB, {JvMemDS,} ZAbstractRODataset,
  ZDataset, ToolEdit, JvCombobox, ClassImpressao, JvMemoryDataset,
  JvExDBGrids, JvDBGrid, JvExMask, frxClass, frxGradient, frxDBSet,
  ZAbstractDataset, Printers, DBClient, frxExportPDF, ShellApi, ADODB,
  JvExControls, JvDBLookup, RxMemDS;

type
  TFRelProtocoloEntregaNovo = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    DataIni: TJvDateEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    datafin: TJvDateEdit;
    Panel4: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    DSLotesCartao: TDataSource;
    GroupBox2: TGroupBox;
    MLotes: TJvMemoryData;
    MLotesseq_lote: TIntegerField;
    MLotesempres_id: TIntegerField;
    MLotesmarcado: TBooleanField;
    MLotesdata: TDateTimeField;
    MLotesnome_arq: TStringField;
    QLotesCartao: TADOQuery;
    MLotesfantasia: TStringField;
    frxProtocolo: TfrxReport;
    cdsLote: TClientDataSet;
    dbLote: TfrxDBDataset;
    BitBtn2: TBitBtn;
    cdsEmpresa: TClientDataSet;
    dbEmpresas: TfrxDBDataset;
    dsCdsEmpresas: TDataSource;
    dsCdsLote: TDataSource;
    QEmpresas: TADOQuery;
    dsEmpresas: TDataSource;
    QEmpresasnome: TStringField;
    QEmpresasfantasia: TStringField;
    QEmpresasband_id: TIntegerField;
    QEmpCombo: TADOQuery;
    dsEmpCombo: TDataSource;
    QEmpresasendereco: TStringField;
    QCartao: TADOQuery;
    dsCartao: TDataSource;
    dbCartao: TfrxDBDataset;
    cdsCartao: TClientDataSet;
    cdsLoteSeq_lote: TIntegerField;
    cdsLoteData: TDateField;
    cdsLoteNome_arq: TStringField;
    cdsLoteEmpres_id: TIntegerField;
    cdsLoteFantasia: TStringField;
    cdsCartaoChapa: TFloatField;
    cdsCartaoCodCartao: TStringField;
    cdsCartaoSeq_Lote: TIntegerField;
    cdsCartaoTitular: TStringField;
    cdsCartaoDependente: TStringField;
    cdsEmpresaNome: TStringField;
    cdsEmpresaFantasia: TStringField;
    cdsEmpresaBand_id: TIntegerField;
    cdsEmpresaEndereco: TStringField;
    dsCdsCartao: TDataSource;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    DBEmpresa: TJvDBLookupCombo;
    EdEmp_ID: TEdit;
    QLotesCartaoseq_lote: TIntegerField;
    QLotesCartaoDATA: TWideStringField;
    QLotesCartaonome_arq: TStringField;
    QLotesCartaohora: TWideStringField;
    QLotesCartaofantasia: TStringField;
    QLotesCartaoEMPRES_ID: TIntegerField;
    QCartaochapa: TFloatField;
    QCartaocodcartimp: TStringField;
    QCartaoseq_lote: TAutoIncField;
    QCartaotitular: TStringField;
    QCartaodependente: TStringField;
    QCartaoempres_id: TIntegerField;
    QCartaonome: TStringField;
    QCartaotipo_credito: TIntegerField;
    QCartaoFANTASIA: TStringField;
    QCartaoendereco: TStringField;
    QCartaoempres_fornece_cartao: TStringField;
    QCartaoCOLUMN1: TStringField;
    bntGerarPDF: TBitBtn;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    QCartaodescricao: TStringField;
    QCartaosetor_id: TIntegerField;
    QCartaosetor: TStringField;
    QLotesCartaoDESCRICAO: TStringField;
    MLotessetor: TStringField;
    grpOrdena: TGroupBox;
    chkOrdenaSetor: TCheckBox;
    GroupBox4: TGroupBox;
    lkpModeloCartao: TJvDBLookupCombo;
    DSModelos: TDataSource;
    QModelos: TADOQuery;
    QModelosMOD_CART_ID: TIntegerField;
    QModelosDESCRICAO: TStringField;
    QModelosOBSERVACAO: TStringField;
    QModelosAPAGADO: TStringField;
    QModelosDTAPAGADO: TDateTimeField;
    QModelosDTALTERACAO: TDateTimeField;
    QModelosDTCADASTRO: TDateTimeField;
    QModelosOPERADOR: TStringField;
    QModelosOPERCADASTRO: TStringField;
    Panel1: TPanel;
    lbl1: TLabel;
    cbbLiberado: TComboBox;
    procedure ButListaEmpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure carregaDataSet;
    procedure EdEmp_IDChange(Sender: TObject);
    procedure EdEmp_IDKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure frxProtocoloBeforePrint(Sender: TfrxReportComponent);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);


  private
    { Private declarations }
    campo_ord : string;
    desc : Boolean;
    campo_ord2 : string;
    desc2 : Boolean;
    linha : integer;
    lpage : integer;
    empres_sel : String;
    bVisualizaTit, filtrarEntrNF, acumularAutor : Boolean;
    procedure BuscaLotes;
    

  public
    { Public declarations }
    sl : TStrings;
    fornec_sel, conv_sel, conveniados : String;
    MostrarRelatorio : Boolean;
    Marcados : TStringList;
    myList   : TList;
    lastRec  : boolean;
    recConv  : Integer;
    qtdConv  : Integer;
    FiltraporSetor : Boolean;
    procedure Empresas_Sel;
    //procedure carregarDataSet;

  end;

var
  FRelProtocoloEntregaNovo: TFRelProtocoloEntregaNovo;
  flagIsPDF : Boolean = False;

implementation

uses DM, impressao, Math, cartao_util;

{$R *.dfm}


procedure TFRelProtocoloEntregaNovo.ButListaEmpClick(Sender: TObject);
var aux : TDateTime;
begin
  inherited;
  BuscaLotes;
end;

procedure TFRelProtocoloEntregaNovo.FormCreate(Sender: TObject);
begin
  inherited;
  QEmpCombo.Open;
  cdsEmpresa.CreateDataSet;
  cdsCartao.CreateDataSet;
  cdsLote.CreateDataSet;
end;

procedure TFRelProtocoloEntregaNovo.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  if MLotes.IsEmpty then Exit;
    MLotes.Edit;
    MLotesMarcado.AsBoolean := not MLotesMarcado.AsBoolean;
    MLotes.Post;
    Empresas_Sel;
end;

procedure TFRelProtocoloEntregaNovo.Empresas_Sel;
var marca : TBookmark;
begin
  empres_sel := EmptyStr;
  marca := MLotes.GetBookmark;
  MLotes.DisableControls;
  MLotes.First;
  while not MLotes.eof do begin
    if MLotesMarcado.AsBoolean then empres_sel := empres_sel + ','+MLotesempres_id.AsString;
    MLotes.Next;
  end;
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  if empres_sel <> '' then empres_sel := Copy(empres_sel,2,Length(empres_sel));
  MLotes.EnableControls;
end;

procedure TFRelProtocoloEntregaNovo.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
   if MLotes.IsEmpty then Exit;
  MLotes.DisableControls;
  marca := MLotes.GetBookmark;
  MLotes.First;
  while not MLotes.eof do begin
    MLotes.Edit;
    MLotesMarcado.AsBoolean := false;
    MLotes.Post;
    MLotes.Next;
  end;
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  MLotes.EnableControls;
  Empresas_Sel;
end;

procedure TFRelProtocoloEntregaNovo.BitBtn2Click(Sender: TObject);
var x,hasDepartamento : Boolean;
    size,aux : Integer;
    sqlConcat : String;
var b : TBookmark;
var posMarcado : TBookmark;
var array_lotes,array_empres : array of Integer;
begin
  b := MLotes.GetBookmark;
  posMarcado := MLotes.GetBookmark;
  MLotes.Open;
  MLotes.First;
  size := 0;
  while not MLotes.Eof do begin

    if(MLotesmarcado.AsBoolean = True)then
    begin
    //i := i+1 ;
      SetLength(array_lotes,size+1);
      SetLength(array_empres,size+1);
      array_lotes[size]  := MLotesseq_lote.AsInteger;
      array_empres[size] := MLotesempres_id.AsInteger;
      size := size+1;
    end;
    MLotes.Next;
  end;
  if((QLotesCartao.RecordCount>=1) and (size = 0))then
  begin
    MsgInf('Voc� precisa selecionar pelo menos uma linha para exibir o relat�rio.');
    exit;
  end;
  QCartao.Close;
  QCartao.SQL.Clear;
  QCartao.SQL.Add('SELECT conv.chapa, cart.codcartimp,cl.seq_lote,b.descricao, coalesce(EMP_DPTOS.DEPT_ID,-1) setor_id, coalesce(EMP_DPTOS.DESCRICAO,'''') setor,');
  QCartao.SQL.Add('conv.TITULAR as titular,case cart.TITULAR when ''N'' then cart.nome else '''' end as dependente,e.nome,e.tipo_credito, ');
  QCartao.SQL.Add('e.empres_id,e.FANTASIA,CONCAT(e.endereco,'', '',e.numero,'', '',UPPER(ba.descricao),'','',CHAR(13)+CHAR(10),UPPER(ci.nome),'' - '',es.uf, '' / CEP: '', e.cep) as endereco, ');
  QCartao.SQL.Add('CASE e.mod_cart_id when 1 then ''DROGABELLA'' else ''PLANT�O CARD'' end as empres_fornece_cartao, ');
  QCartao.SQL.Add('CASE mc.DESCRICAO WHEN ''DB - Drogabella'' then ''DROGABELLA'' WHEN ''RPC - Plantao Card'' then ''PLANT�O CARD'' WHEN ''AL - ALIMENTA��O'' then ''PLANT�O CARD ALIMENTA��O'' ');
  QCartao.SQL.Add('when ''RF - Refei��o'' then ''PLANT�O CARD REFEI��O'' END ');
  QCartao.SQL.Add('from CONVENIADOS conv ');
  QCartao.SQL.Add('inner join CARTOES_TEMP cart ON conv.CONV_ID = cart.CONV_ID ');
  QCartao.SQL.Add('inner join bandeiras b ON b.band_id = (SELECT band_id from empresas WHERE empres_id = conv.empres_id)');
  QCartao.SQL.Add('inner join CARTOES_LOTES_DETALHE cld ON CART.CARTAO_ID in(cld.CARTAO_ID) ');
  QCartao.SQL.Add('inner join CARTOES_LOTES cl ON cl.SEQ_LOTE = cld.SEQ_LOTE and cl.SEQ_LOTE IN(');
  //TRECHO PARA PREENCHER O IN DA CLAUSULA DINAMICAMENTE
  //PREENCHE DE ACORDO COM AS LINHAS SELECIONADAS
  if(size = 0)THEN
      QCartao.SQL.Add(QuotedStr(IntToStr(array_lotes[0]))+')')
  else
  begin
      aux := 0;
      sqlConcat := '';
      while aux < size do
      begin
        if(aux = 0)then
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + IntToStr(array_lotes[aux]);
              //sqlConcat := IntToStr(array_lotes[aux]);
              Inc(aux);
          end
        else
          begin
              //sqlConcat := sqlConcat+','+ IntToStr(array_lotes[aux]);
              QCartao.SQL.Text := QCartao.SQL.Text + ','+IntToStr(array_lotes[aux]);
              Inc(aux);
          end;

       end;
       QCartao.SQL.Text := QCartao.SQL.Text + ')';

  end;
  QCartao.SQL.Text;
  QCartao.SQL.Add('inner join EMPRESAS e ON e.EMPRES_ID = conv.EMPRES_ID AND e.empres_id in(');
  if(size = 0)THEN
      QCartao.SQL.Add(QuotedStr(IntToStr(array_empres[0]))+')')
  else
  begin
      aux := 0;
      sqlConcat := '';
      while aux < size do
      begin
        if(aux = 0)then
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + IntToStr(array_empres[aux]);
              //sqlConcat := IntToStr(array_lotes[aux]);
              Inc(aux);
          end
        else
          begin
              //sqlConcat := sqlConcat+','+ IntToStr(array_lotes[aux]);
              QCartao.SQL.Text := QCartao.SQL.Text + ','+IntToStr(array_empres[aux]);
              Inc(aux);
          end;

       end;
       QCartao.SQL.Text := QCartao.SQL.Text + ')';

  end;
  //QCartao.SQL.Text;
  //QCartao.SQL.Text := QCartao.SQL.Text + ')';
  QCartao.SQL.Add('left join BAIRROS BA on (ba.bairro_id = e.bairro) ');
  QCartao.SQL.Add('left join CIDADES CI on (ci.cid_id = e.cidade) ');
  QCartao.SQL.Add('left join ESTADOS ES on (es.estado_id = e.estado) ');  
  QCartao.SQL.Add('inner join MODELOS_CARTOES mc ON mc.mod_cart_id = e.mod_cart_id');
  QCartao.SQL.Add('left join EMP_DPTOS ON EMP_DPTOS.DEPT_ID = cl.setor_id');
  QCartao.SQL.Add(' WHERE conv.apagado = ''N''');
  if cbbLiberado.ItemIndex > 0 then
  begin
    if cbbLiberado.ItemIndex = 1 then
      QCartao.SQL.Add(' and conv.liberado = ''S''')
    else
      QCartao.SQL.Add(' and conv.liberado = ''N''')
  end;
  QCartao.SQL.Add(' group by conv.TITULAR,conv.CHAPA,cart.CODCARTIMP,cl.SEQ_LOTE,cart.NOME,cart.TITULAR,e.nome,e.FANTASIA,b.descricao,');
  QCartao.SQL.Add('e.empres_id,e.endereco,e.numero,e.cep, ba.descricao, ci.nome, es.uf,e.MOD_CART_ID,mc.DESCRICAO,e.tipo_credito,EMP_DPTOS.DEPT_ID,EMP_DPTOS.DESCRICAO');
  MLotes.First;
  hasDepartamento := False;
  while not MLotes.Eof do
  begin
    if MLotesmarcado.AsBoolean = True then begin
      if MLotessetor.AsString <> '' then begin
        hasDepartamento := True;
        MLotes.Last;
      end;
    end;
    MLotes.Next;
  end;
  if hasDepartamento = False or chkOrdenaSetor.Checked = False then begin
     QCartao.SQL.Add('order by empres_id,conv.TITULAR');
     hasDepartamento := False;
  end
     else
        QCartao.SQL.Add('order by dept_id,conv.TITULAR');
  QCartao.SQL.Text;
  QCartao.Open;

  //if flagIsPDF = False then        //flagIsPDF � uma vari�vel para verividar se o usu�rio est� gerando PDF
  //begin
    if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

        FiltraPorSetor := True;
              frxProtocolo.ScriptText.Clear;
              frxProtocolo.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');
    end
    else begin

        FiltraPorSetor := False;
        frxProtocolo.ScriptText.Clear;
        frxProtocolo.ScriptText.Add('begin' + #13 +
                                   'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                   'end.');
    end;

    if flagIsPDF = False then        //flagIsPDF � uma vari�vel para verividar se o usu�rio est� gerando PDF
    begin
      frxProtocolo.ShowReport();
    end;

  flagIsPDF := False;
  MLotes.GotoBookmark(posMarcado);

end;

procedure TFRelProtocoloEntregaNovo.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    flagIsPDF := True;
    BitBtn2.Click;
    frxProtocolo.PrepareReport();
    frxProtocolo.Export(frxPDFExport1);
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TFRelProtocoloEntregaNovo.carregaDataSet;
var b : TBookmark;
begin
  b := MLotes.GetBookmark;
  MLotes.DisableControls;
  MLotes.Open;
  MLotes.First;
  while not QEmpresas.Eof do begin
    if (MLotesmarcado.asBoolean = True) then begin
      cdsLote.Edit;
      cdsLote.Append;
      cdsLoteseq_lote.asInteger      := MLotesseq_lote.value;
      cdsLoteempres_id.Value         := MLotesempres_id.Value;
      cdsLotedata.Value              := MLotesdata.Value;
      cdsLoteNome_arq.Value          := MLotesnome_arq.Value;
      cdsLote.Post;
    end;
    MLotes.Next;
  end;
  //Carregando os cart�es
  //QCartao.Parameters.ParamByName('num_lote').Value := 19;
  QCartao.Open;

  //ShowMessage(QCartaotitular.AsString);
  cdsCartao.Edit;
  While not QCartao.Eof do begin
    cdsCartaoChapa.AsFloat       := QCartaochapa.AsFloat;
    cdsCartaoCodCartao.AsString  := QCartaocodcartimp.AsString;
    cdsCartaoSeq_Lote.AsInteger  := QCartaoseq_lote.AsInteger;
    cdsCartaoTitular.AsString    := QCartaotitular.AsString;
    cdsCartaoDependente.AsString := QCartaodependente.AsString;
    cdsCartao.Post;
    QCartao.Next
  end;
  //carregando Empresas
  QEmpresas.Parameters.ParamByName('empres_id').Value := 45;
  QEmpresas.Open;
  cdsEmpresa.Edit;
  cdsEmpresaNome.AsString     := QEmpresasnome.AsString;
  cdsEmpresaFantasia.AsString := QEmpresasfantasia.AsString;
  cdsEmpresaBand_id.AsInteger := QEmpresasband_id.AsInteger;
  cdsEmpresaEndereco.AsString := QEmpresasendereco.AsString;
  cdsEmpresa.Post;
  
end;

procedure TFRelProtocoloEntregaNovo.EdEmp_IDChange(Sender: TObject);
begin
  inherited;
  if Sender = EdEmp_ID then begin
     if EdEmp_ID.Text = '' then
        DBEmpresa.ClearValue
     else if QEmpCombo.Locate('empres_id',EdEmp_ID.Text,[]) then
       DBEmpresa.KeyValue := EdEmp_ID.Text
     else
       DBEmpresa.ClearValue;
  end;
end;

procedure TFRelProtocoloEntregaNovo.EdEmp_IDKeyPress(Sender: TObject; var Key:
    Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFRelProtocoloEntregaNovo.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcaDesmEmpClick(nil);
end;

procedure TFRelProtocoloEntregaNovo.JvDBGrid1KeyDown(Sender: TObject; var Key:
    Word; Shift: TShiftState);
begin
  inherited;
  if key = vk_return then ButMarcaDesmEmpClick(nil);
end;

procedure TFRelProtocoloEntregaNovo.BuscaLotes;
begin
  QLotesCartao.Close;
  QLotesCartao.SQL.Clear;
  QLotesCartao.SQL.Add('SELECT cld.seq_lote, FORMAT(data,''dd/MM/yyyy'') as DATA, nome_arq,hora,');
  QLotesCartao.SQL.Add('e.fantasia,e.EMPRES_ID, EMP_DPTOS.descricao');
  QLotesCartao.SQL.Add('FROM cartoes_lotes cl');
  QLotesCartao.SQL.Add(' left join EMP_DPTOS ON EMP_DPTOS.DEPT_ID = cl.setor_id');
  QLotesCartao.SQL.Add(' INNER JOIN cartoes_lotes_detalhe cld ON cld.SEQ_LOTE = cl.SEQ_LOTE');
  QLotesCartao.SQL.Add(' INNER JOIN EMPRESAS e ON cld.empres_id = e.empres_id');
  QLotesCartao.SQL.Add(' WHERE ');

  if((DataIni.Date = 0) and (DBEmpresa.KeyValue = Null)) then
  begin
     MsgInf('A data inicial n�o pode estar vazia'); DataIni.SetFocus; Exit;
  end;

  if((DataIni.Date > datafin.Date) and (datafin.Date <> 0))then
  begin
     MsgInf('A data inicial n�o pode ser maior do que a data final');
     DataIni.SetFocus;
     Exit;
  end;

  if((DataIni.Date <> 0) or (datafin.Date <> 0)) then
  begin

     if datafin.Date <> 0 then
     begin
        QLotesCartao.SQL.Add('DATA  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataFin.Date)));
     end
     else
     QLotesCartao.SQL.Add('DATA  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',Date)));


    {if(rbTermo.Checked)then
       QLotesCartao.SQL.Add('and e.tipo_credito >1')
       else
          QLotesCartao.SQL.Add('and e.tipo_credito <= 1'); }
    if(DBEmpresa.KeyValue <> Null)then
    begin
       QLotesCartao.SQL.Add('and e.empres_id = '+DBEmpresa.KeyValue);
    end;

    if(lkpModeloCartao.KeyValue <> null) then
    begin
      QLotesCartao.SQL.Add('and e.mod_cart_id = ' + lkpModeloCartao.KeyValue);
    end;
  end
  else
  begin
    QLotesCartao.SQL.Add('e.empres_id = '+DBEmpresa.KeyValue);
  end;
  QLotesCartao.SQL.Add('group by e.empres_id,cld.SEQ_LOTE,cl.data,cl.hora,cl.NOME_ARQ,e.FANTASIA,EMP_DPTOS.DESCRICAO');
  QLotesCartao.SQL.Add('ORDER BY DATA ASC');
  QLotesCartao.Open;
  if QLotesCartao.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum lote com os filtros aplicados.');
    DataIni.SetFocus;
  end;
  QLotesCartao.First;
  MLotes.Open;
  MLotes.EmptyTable;
  MLotes.DisableControls;
  while not QLotesCartao.Eof do begin
      MLotes.Append;
      MLotesSeq_Lote.AsInteger  := QLotesCartaoSEQ_LOTE.AsInteger;
      MLotesempres_id.AsInteger := QLotesCartaoEMPRES_ID.AsInteger;
      MLotesdata.AsDateTime     := QLotesCartaodata.AsDateTime;
      MLotesnome_arq.Value      := QLotesCartaoNOME_ARQ.Value;
      MLotesfantasia.AsString   := QLotesCartaofantasia.AsString;
      MLotessetor.AsString      := QLotesCartaoDESCRICAO.AsString;
      MLotesMarcado.AsBoolean := False;
      MLotes.Post;
      QLotesCartao.Next;
  end;
  MLotes.First;
  MLotes.EnableControls;
  empres_sel := EmptyStr;
end;

procedure TFRelProtocoloEntregaNovo.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MLotes.IsEmpty then Exit;
  MLotes.DisableControls;
  marca := MLotes.GetBookmark;
  MLotes.First;
  while not MLotes.eof do begin
    MLotes.Edit;
    MLotesmarcado.AsBoolean := true;
    MLotes.Post;
    MLotes.Next;
  end;
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  MLotes.EnableControls;
  Empresas_Sel;
end;

procedure TFRelProtocoloEntregaNovo.FormShow(Sender: TObject);
begin
  QModelos.Open;
end;

procedure TFRelProtocoloEntregaNovo.frxProtocoloBeforePrint(Sender:
    TfrxReportComponent);
begin
  inherited;
  if FiltraporSetor then
  begin
    FiltraPorSetor := True;
              frxProtocolo.ScriptText.Clear;
              frxProtocolo.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');
  end
  else
  begin
    FiltraPorSetor := True;
              frxProtocolo.ScriptText.Clear;
              frxProtocolo.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                         'end.');

    TfrxMemoView(frxProtocolo.FindObject('Cartaosetor')).Visible := False;
  end;
end;

end.

