unit UAnexarBoletoAlimentacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, Mask, ToolEdit, ExtCtrls, Buttons,
  DB, ZAbstractRODataset, ZDataset, {JvLookup,} ADODB, ZStoredProcedure,
  ZSqlUpdate, ZAbstractDataset, JvToolEdit, ShellApi, JvExControls,
  JvDBLookup, JvExMask, dblookup, RXDBCtrl, CurrEdit;

type
  TFAnexarBoletoAlimentacao = class(TForm)
    ProgressBar1: TProgressBar;
    DSEmpresas: TDataSource;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasUSA_COD_IMPORTACAO: TStringField;
    qBoletos: TADOQuery;
    qBoletosCAMINHO_ARQUIVO: TStringField;
    qBoletosDATA_VENCIMENTO: TDateTimeField;
    pnl1: TPanel;
    lbl3: TLabel;
    lbl4: TLabel;
    btnButAjuda: TSpeedButton;
    lbl5: TLabel;
    bvl1: TBevel;
    lbl1: TLabel;
    lbl2: TLabel;
    btn1: TButton;
    dblkpEmpresas: TJvDBLookupCombo;
    FilenameEdit1: TJvFilenameEdit;
    dlgAbrePDF: TOpenDialog;
    qBoletosDATA_ALTERACAO: TDateTimeField;
    qBoletosOPERADOR: TStringField;
    Label1: TLabel;
    edtValor: TCurrencyEdit;
    dtpMesReferencia: TDateTimePicker;
    redt1: TRichEdit;
    dtpDataVencimento: TDateTimePicker;
    qBoletosEMPRES_ID: TIntegerField;
    qBoletosVALOR: TBCDField;
    qBoletosMES_REFERENCIA: TStringField;
    qBoletosBOLETO_ID: TIntegerField;
    Label5: TLabel;
    EdEmp_ID: TEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnButAjudaClick(Sender: TObject);
    procedure dblkpEmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure btn1Click(Sender: TObject);
    procedure LimparTela();
    procedure dblkpEmpresasEnter(Sender: TObject);

  public
    { Public declarations }
  end;

var
  FAnexarBoletoAlimentacao: TFAnexarBoletoAlimentacao;

implementation

uses DM, UChangeLog, UMenu, cartao_util, UValidacao, Math, StrUtils;

{$R *.dfm}

procedure TFAnexarBoletoAlimentacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end;
end;

procedure TFAnexarBoletoAlimentacao.FormCreate(Sender: TObject);
var year,month,Day : Word;
begin
  QEmpresas.Open;
  qBoletos.Open;
  dtpDataVencimento.Date := Now;
  dtpMesReferencia.Date := Now;
  DecodeDate(Now,Year, Month, Day);
  if((month = 1) and (Day > 28)) then
    dtpMesReferencia.Date := StrToDate('28/02/'+IntToStr(year));

  dtpMesReferencia.Format := 'MM/yyyy';
  FilenameEdit1.Text := '"\\tsclient\Z\"';
end;

procedure TFAnexarBoletoAlimentacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresas.Close;
  qBoletos.Close;
end;

procedure TFAnexarBoletoAlimentacao.btnButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Carregamento de Boletos';
  FChangeLog.RichEdit1.Lines := redt1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFAnexarBoletoAlimentacao.dblkpEmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
    Perform(WM_NEXTDLGCTL,0,0);
  end;
end;

procedure TFAnexarBoletoAlimentacao.btn1Click(Sender: TObject);
var arquivoPDF : string;
    caminhoAno, caminhoMes, caminhoCompleto, caminhoRelativo, mesReferencia, mes, ano : string;
begin
  screen.Cursor  := crHourGlass;
  if dblkpEmpresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa.');
    screen.Cursor := crDefault;
    Exit;
  end;

  if ExtractFileExt(FilenameEdit1.FileName) <> '.pdf' then
  begin
    ShowMessage('Selecione o arquivo a ser carregado.');
    screen.Cursor := crDefault;
    Exit;
  end;

//  if edtValor.Text = '' then
//  begin
//    ShowMessage('Preencha o valor do boleto.');
//    screen.Cursor := crDefault;
//    Exit;
//  end;

  if MsgSimNao('Deseja realmente anexar o boleto selecionado' + sLineBreak + '� empresa ' + dblkpEmpresas.Text + '?', False) then
  begin

    mesReferencia := RightStr(datetostr(dtpMesReferencia.Date),7);

    qBoletos.Close;
    qBoletos.Parameters.ParamByName('EMPRES_ID').Value := dblkpEmpresas.KeyValue;
    qBoletos.Open;

    if ((qBoletosMES_REFERENCIA.AsString = RightStr(datetostr(dtpMesReferencia.Date),7)) and (qBoletosVALOR.AsString = edtValor.Text)) then
      qBoletos.Edit
    else begin
      qBoletos.Insert;

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SBOLETOAL_ID AS BOLETO_ID');
      DMConexao.AdoQry.Open;

      qBoletosBOLETO_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
    end;

    arquivoPDF := FilenameEdit1.FileName;
    //DecodeDate(Now, ano, mes, dia);

    mes := LeftStr(mesReferencia,2);
    ano := RightStr(mesReferencia,4);

    caminhoAno := 'C:\Dados\Web\webEmpresas\FaturasAlimentacao\' + ano;
    if Length(mes) < 2 then
      caminhoMes := '0' + mes
    else
      caminhoMes := mes;

    if not DirectoryExists(caminhoAno) then
      CreateDir(caminhoAno);

    if not DirectoryExists(caminhoAno + '\' + caminhoMes) then
      CreateDir(caminhoAno + '\' + caminhoMes);

      //aqui tbm
    caminhoCompleto := caminhoAno + '\' + caminhoMes + '\' + IntToStr(qBoletosBOLETO_ID.AsInteger) + '.pdf';
    caminhoRelativo := 'FATURASALIMENTACAO\' + ano + '\' + caminhoMes + '\' + IntToStr(qBoletosBOLETO_ID.AsInteger) + '.pdf';

    if CopyFile(PChar(arquivoPDF), PChar(caminhoCompleto), False) then begin
      qBoletosEMPRES_ID.AsInteger := dblkpEmpresas.KeyValue;
      qBoletosMES_REFERENCIA.Text := mesReferencia;
      qBoletosCAMINHO_ARQUIVO.Text := caminhoRelativo;
      qBoletosVALOR.Value := edtValor.Value;
      qBoletosDATA_VENCIMENTO.AsDateTime := dtpDataVencimento.Date;
      qBoletosOPERADOR.AsString := Operador.Nome;
      qBoletosDATA_ALTERACAO.AsDateTime := Now;
      qBoletos.Post;

      ShowMessage('Boleto carregado com sucesso!');
    end else
      ShowMessage('O Boleto n�o foi carregado. Tente novamente!');
  end
  else
    ShowMessage('O Boleto n�o foi carregado!');
  LimparTela();
end;

procedure TFAnexarBoletoAlimentacao.LimparTela();
begin
  FilenameEdit1.Clear;
  FilenameEdit1.Text := '"\\tsclient\Z\"';
  dblkpEmpresas.ResetField;
  edtValor.Clear;
  //dtpDataVencimento.Date := Now;
  //dtpMesReferencia.Date := Now;
end;

procedure TFAnexarBoletoAlimentacao.dblkpEmpresasEnter(Sender: TObject);
begin
  if EdEmp_ID.Text = '' then
       dblkpEmpresas.ClearValue
     else if QEmpresas.Locate('empres_id',EdEmp_ID.Text,[]) then  begin
       dblkpEmpresas.KeyValue := EdEmp_ID.Text;
       dblkpEmpresas.SetFocus;
       //lblStatus.Caption := '';
     end
     else
       dblkpEmpresas.ClearValue;
end;



end.
