object FSelecionaBanco: TFSelecionaBanco
  Left = 257
  Top = 232
  Width = 358
  Height = 143
  BorderIcons = []
  Caption = 'Selecionar Banco'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 17
    Width = 139
    Height = 13
    Caption = 'Selecione o Banco de Dados'
  end
  object Button1: TButton
    Left = 88
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Selecionar'
    ModalResult = 1
    TabOrder = 1
  end
  object CBBanco: TJvDBLookupCombo
    Left = 16
    Top = 32
    Width = 305
    Height = 21
    LookupField = 'CODIGO'
    LookupDisplay = 'BANCO'
    LookupSource = DSBanco
    TabOrder = 0
  end
  object Button2: TButton
    Left = 176
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 2
  end
  object DSBanco: TDataSource
    DataSet = QBanco
    Left = 296
    Top = 56
  end
  object QBanco: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select CONCAT(codigo,'#39'-'#39',banco) as banco, codigo,'
      'layout from bancos'
      'where coalesce(apagado,'#39'N'#39') <> '#39'S'#39
      'and layout is not null')
    Left = 264
    Top = 56
    object QBancobanco: TStringField
      FieldName = 'banco'
      ReadOnly = True
      Size = 58
    end
    object QBancocodigo: TIntegerField
      FieldName = 'codigo'
    end
    object QBancolayout: TMemoField
      FieldName = 'layout'
      BlobType = ftMemo
    end
  end
end
