object FSelecionaBD: TFSelecionaBD
  Left = 345
  Top = 354
  Width = 345
  Height = 119
  BorderIcons = []
  Caption = 'Selecionar Banco de Dados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 9
    Width = 139
    Height = 13
    Caption = 'Selecione o Banco de Dados'
  end
  object CBBanco: TComboBox
    Left = 16
    Top = 24
    Width = 313
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnKeyPress = CBBancoKeyPress
  end
  object Button1: TButton
    Left = 136
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Selecionar'
    ModalResult = 1
    TabOrder = 1
  end
end
