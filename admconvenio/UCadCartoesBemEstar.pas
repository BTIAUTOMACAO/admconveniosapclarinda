{
 Autor : Gustavo Andr� Tenan
 Cadastro de Cart�es
 Data �ltima altera��o:  01/04/2005

 Obs: Qualquer alteral�ao na tabela de dados dos cartoes o componente de update dever� ser atualizado
      para que o campo nome do titular n�o v� para sql.
}


unit UCadCartoesBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, Menus, DB, ZDataset, {JvMemDS,} ZAbstractRODataset,
  ZAbstractDataset, Buttons, StdCtrls, Mask, JvToolEdit, ComCtrls, Grids,
  DBGrids, {JvDBCtrl,} DBCtrls, ExtCtrls, Provider, DBClient, ZSqlUpdate,
  {JvDBComb,} JvExStdCtrls, JvCombobox, JvDBCombobox, JvExMask, JvExDBGrids,
  JvDBGrid, ADODB;

type
  TFCadCartoesBemEstar = class(TFCad)
    GroupBox1: TGroupBox;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    Label11: TLabel;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdNomeCart: TEdit;
    EdCartao: TEdit;
    Label18: TLabel;
    EdEmpID: TEdit;
    Label19: TLabel;
    EdNomeEmp: TEdit;
    ButPesq: TSpeedButton;
    DBCheckBox1: TDBCheckBox;
    TabCC: TTabSheet;
    JvDBGrid2: TJvDBGrid;
    Panel4: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dbnome: TDBText;
    codigocar: TDBText;
    DG: TDBText;
    DBText4: TDBText;
    Label23: TLabel;
    Label24: TLabel;
    DSContaCorrente: TDataSource;
    DSCredenciado: TDataSource;
    QCadastroCONV_ID: TIntegerField;
    QCadastroNOME: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroCODIGO: TIntegerField;
    QCadastroDIGITO: TSmallintField;
    QCadastroLIMITE_MES: TFloatField;
    QCadastroJAEMITIDO: TStringField;
    QCadastroOPERADOR: TStringField;
    QCadastroCODCARTIMP: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroTITULAR: TStringField;
    QCadastroPARENTESCO: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    PopupCOntaC: TPopupMenu;
    MenuItem2: TMenuItem;
    Exportarparaoexcel3: TMenuItem;
    DBEdit11: TDBEdit;
    QCadastroNUM_DEP: TIntegerField;
    Label25: TLabel;
    DBEdit14: TDBEdit;
    DSSaldoCartao: TDataSource;
    DSSaldoConv: TDataSource;
    TabSaldos: TTabSheet;
    Panel10: TPanel;
    Label62: TLabel;
    Panel16: TPanel;
    Panel11: TPanel;
    Button2: TButton;
    Panel12: TPanel;
    GridSaldoConv: TDBGrid;
    Splitter1: TSplitter;
    Panel13: TPanel;
    GridSaldoCartao: TDBGrid;
    ButAtualCodImp: TBitBtn;
    Label9: TLabel;
    JvDBComboBox1: TJvDBComboBox;
    QCadastroCPF: TStringField;
    QCadastroRG: TStringField;
    Label26: TLabel;
    DBEdit7: TDBEdit;
    Label27: TLabel;
    DBEdit15: TDBEdit;
    Button1: TButton;
    tExcel1: TADOTable;
    tExcel1CHAPA: TFloatField;
    tExcel1N_CART_TIT: TFloatField;
    tExcel1NOMEFUNCIONRIO: TWideStringField;
    tExcel1N_CART_DEP: TFloatField;
    tExcel1NOMEDODEPENDENTE: TWideStringField;
    tExcel1N_CART: TWideStringField;
    tExcel2: TADOTable;
    tExcel2CHAPA: TFloatField;
    tExcel2N_CART_TIT: TFloatField;
    tExcel2NOMEFUNCIONRIO: TWideStringField;
    tExcel2N_CART_DEP: TFloatField;
    tExcel2NOMEDODEPENDENTE: TWideStringField;
    tExcel2N_CART: TWideStringField;
    tExcel3: TADOTable;
    tExcel3CHAPA: TFloatField;
    tExcel3N_CART_TIT: TFloatField;
    tExcel3NOMEFUNCIONRIO: TWideStringField;
    tExcel3N_CART_DEP: TFloatField;
    tExcel3NOMEDODEPENDENTE: TWideStringField;
    tExcel3N_CART: TWideStringField;
    dExcel: TDataSource;
    DBGrid2: TDBGrid;
    QCadastroFLAG: TStringField;
    QCadastroVIA: TIntegerField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroCRED_ID: TIntegerField;
    QCadastroATIVO: TStringField;
    QCadastroDTEMISSAO: TDateTimeField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    Qcontacorrente: TADOQuery;
    QcontacorrenteAUTORIZACAO_ID: TIntegerField;
    QcontacorrenteCARTAO_ID: TIntegerField;
    QcontacorrenteCONV_ID: TIntegerField;
    QcontacorrenteCRED_ID: TIntegerField;
    QcontacorrenteDIGITO: TWordField;
    QcontacorrenteDATA: TDateTimeField;
    QcontacorrenteHORA: TStringField;
    QcontacorrenteDATAVENDA: TDateTimeField;
    QcontacorrenteDEBITO: TBCDField;
    QcontacorrenteCREDITO: TBCDField;
    QcontacorrenteVALOR_CANCELADO: TBCDField;
    QcontacorrenteBAIXA_CONVENIADO: TStringField;
    QcontacorrenteBAIXA_CREDENCIADO: TStringField;
    QcontacorrenteENTREG_NF: TStringField;
    QcontacorrenteRECEITA: TStringField;
    QcontacorrenteCESTA: TStringField;
    QcontacorrenteCANCELADA: TStringField;
    QcontacorrenteDIGI_MANUAL: TStringField;
    QcontacorrenteTRANS_ID: TIntegerField;
    QcontacorrenteFORMAPAGTO_ID: TIntegerField;
    QcontacorrenteFATURA_ID: TIntegerField;
    QcontacorrentePAGAMENTO_CRED_ID: TIntegerField;
    QcontacorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    QcontacorrenteOPERADOR: TStringField;
    QcontacorrenteDATA_VENC_EMP: TDateTimeField;
    QcontacorrenteDATA_FECHA_EMP: TDateTimeField;
    QcontacorrenteDATA_VENC_FOR: TDateTimeField;
    QcontacorrenteDATA_FECHA_FOR: TDateTimeField;
    QcontacorrenteHISTORICO: TStringField;
    QcontacorrenteNF: TIntegerField;
    QcontacorrenteDATA_ALTERACAO: TDateTimeField;
    QcontacorrenteDATA_BAIXA_CONV: TDateTimeField;
    QcontacorrenteDATA_BAIXA_CRED: TDateTimeField;
    QcontacorrenteOPER_BAIXA_CONV: TStringField;
    QcontacorrenteOPER_BAIXA_CRED: TStringField;
    QcontacorrenteDATA_CONFIRMACAO: TDateTimeField;
    QcontacorrenteOPER_CONFIRMACAO: TStringField;
    QcontacorrenteCONFERIDO: TStringField;
    QcontacorrenteNSU: TIntegerField;
    QcontacorrentePREVIAMENTE_CANCELADA: TStringField;
    QcontacorrenteEMPRES_ID: TIntegerField;
    QCredenciado: TADOQuery;
    QCredenciadocred_id: TIntegerField;
    QCredenciadofantasia: TStringField;
    QSaldoConv: TADOQuery;
    QSaldoCartao: TADOQuery;
    qConv: TADOQuery;
    qCard: TADOQuery;
    qCardCARTAO_ID: TIntegerField;
    qCardCONV_ID: TIntegerField;
    qCardNOME: TStringField;
    qCardLIBERADO: TStringField;
    qCardCODIGO: TIntegerField;
    qCardDIGITO: TWordField;
    qCardTITULAR: TStringField;
    qCardJAEMITIDO: TStringField;
    qCardAPAGADO: TStringField;
    qCardLIMITE_MES: TBCDField;
    qCardCODCARTIMP: TStringField;
    qCardPARENTESCO: TStringField;
    qCardDATA_NASC: TDateTimeField;
    qCardNUM_DEP: TIntegerField;
    qCardFLAG: TStringField;
    qCardDTEMISSAO: TDateTimeField;
    qCardCPF: TStringField;
    qCardRG: TStringField;
    qCardVIA: TIntegerField;
    qCardDTAPAGADO: TDateTimeField;
    qCardDTALTERACAO: TDateTimeField;
    qCardOPERADOR: TStringField;
    qCardDTCADASTRO: TDateTimeField;
    qCardOPERCADASTRO: TStringField;
    qCardCRED_ID: TIntegerField;
    qCardATIVO: TStringField;
    qConvCONV_ID: TIntegerField;
    qConvEMPRES_ID: TIntegerField;
    qConvBANCO: TIntegerField;
    qConvGRUPO_CONV_EMP: TIntegerField;
    qConvCHAPA: TFloatField;
    qConvSENHA: TStringField;
    qConvTITULAR: TStringField;
    qConvCONTRATO: TIntegerField;
    qConvLIMITE_MES: TBCDField;
    qConvLIBERADO: TStringField;
    qConvFIDELIDADE: TStringField;
    qConvAPAGADO: TStringField;
    qConvDT_NASCIMENTO: TDateTimeField;
    qConvCARGO: TStringField;
    qConvSETOR: TStringField;
    qConvCPF: TStringField;
    qConvRG: TStringField;
    qConvLIMITE_TOTAL: TBCDField;
    qConvLIMITE_PROX_FECHAMENTO: TBCDField;
    qConvAGENCIA: TStringField;
    qConvCONTACORRENTE: TStringField;
    qConvDIGITO_CONTA: TStringField;
    qConvTIPOPAGAMENTO: TStringField;
    qConvENDERECO: TStringField;
    qConvNUMERO: TIntegerField;
    qConvCEP: TStringField;
    qConvTELEFONE1: TStringField;
    qConvTELEFONE2: TStringField;
    qConvCELULAR: TStringField;
    qConvOBS1: TStringField;
    qConvOBS2: TStringField;
    qConvEMAIL: TStringField;
    qConvCESTABASICA: TBCDField;
    qConvDTULTCESTA: TDateTimeField;
    qConvSALARIO: TBCDField;
    qConvTIPOSALARIO: TStringField;
    qConvCOD_EMPRESA: TStringField;
    qConvFLAG: TStringField;
    qConvDTASSOCIACAO: TDateTimeField;
    qConvDTAPAGADO: TDateTimeField;
    qConvDTALTERACAO: TDateTimeField;
    qConvOPERADOR: TStringField;
    qConvDTCADASTRO: TDateTimeField;
    qConvOPERCADASTRO: TStringField;
    qConvVALE_DESCONTO: TStringField;
    qConvLIBERA_GRUPOSPROD: TStringField;
    qConvCOMPLEMENTO: TStringField;
    qConvUSA_SALDO_DIF: TStringField;
    qConvABONO_MES: TBCDField;
    qConvSALDO_RENOVACAO: TBCDField;
    qConvSALDO_ACUMULADO: TBCDField;
    qConvDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    qConvCONSUMO_MES: TBCDField;
    qConvCONSUMO_MES_1: TBCDField;
    qConvCONSUMO_MES_2: TBCDField;
    qConvCONSUMO_MES_3: TBCDField;
    qConvCONSUMO_MES_4: TBCDField;
    qConvPIS: TFloatField;
    qConvNOME_PAI: TStringField;
    qConvnome_mae: TStringField;
    qConvCART_TRAB_NUM: TIntegerField;
    qConvCART_TRAB_SERIE: TStringField;
    qConvREGIME_TRAB: TStringField;
    qConvVENC_TOTAL: TBCDField;
    qConvESTADO_CIVIL: TStringField;
    qConvNUM_DEPENDENTES: TIntegerField;
    qConvDATA_ADMISSAO: TDateTimeField;
    qConvDATA_DEMISSAO: TDateTimeField;
    qConvFIM_CONTRATO: TDateTimeField;
    qConvDISTRITO: TStringField;
    qConvSALDO_DEVEDOR: TBCDField;
    qConvSALDO_DEVEDOR_FAT: TBCDField;
    QConvAux: TADOQuery;
    DSConvAux: TDataSource;
    QCadastroNOMETITULAR: TStringField;
    QSaldoConvsaldo_conf: TBCDField;
    QSaldoConvsaldo_nconf: TBCDField;
    QSaldoConvFECHAMENTO: TWideStringField;
    QSaldoConvtotal: TBCDField;
    QSaldoCartaosaldo_conf: TBCDField;
    QSaldoCartaosaldo_nconf: TBCDField;
    QSaldoCartaoFECHAMENTO: TWideStringField;
    QSaldoCartaototal: TBCDField;
    QSaldoCartaonome: TStringField;
    QSaldoConvRESTANTE: TCurrencyField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroSENHA: TStringField;
    QCadastroDATA_NASC: TDateTimeField;
    ButLimpaSenha: TBitBtn;
    QCadastroCARTAO_ID: TIntegerField;
    QConvAuxCONV_ID: TIntegerField;
    QConvAuxCARTAO_BEM_ESTAR: TStringField;
    QConvAuxEMPRES_ID: TIntegerField;
    QConvAuxBANCO: TIntegerField;
    QConvAuxGRUPO_CONV_EMP: TIntegerField;
    QConvAuxCHAPA: TFloatField;
    QConvAuxSENHA: TStringField;
    QConvAuxTITULAR: TStringField;
    QConvAuxCONTRATO: TIntegerField;
    QConvAuxLIMITE_MES: TBCDField;
    QConvAuxLIBERADO: TStringField;
    QConvAuxFIDELIDADE: TStringField;
    QConvAuxAPAGADO: TStringField;
    QConvAuxDT_NASCIMENTO: TDateTimeField;
    QConvAuxCARGO: TStringField;
    QConvAuxSETOR: TStringField;
    QConvAuxCPF: TStringField;
    QConvAuxRG: TStringField;
    QConvAuxLIMITE_TOTAL: TBCDField;
    QConvAuxLIMITE_PROX_FECHAMENTO: TBCDField;
    QConvAuxAGENCIA: TStringField;
    QConvAuxCONTACORRENTE: TStringField;
    QConvAuxDIGITO_CONTA: TStringField;
    QConvAuxTIPOPAGAMENTO: TStringField;
    QConvAuxENDERECO: TStringField;
    QConvAuxNUMERO: TIntegerField;
    QConvAuxCEP: TStringField;
    QConvAuxTELEFONE1: TStringField;
    QConvAuxTELEFONE2: TStringField;
    QConvAuxCELULAR: TStringField;
    QConvAuxOBS1: TStringField;
    QConvAuxOBS2: TStringField;
    QConvAuxEMAIL: TStringField;
    QConvAuxDTULTCESTA: TDateTimeField;
    QConvAuxSALARIO: TBCDField;
    QConvAuxTIPOSALARIO: TStringField;
    QConvAuxCOD_EMPRESA: TStringField;
    QConvAuxFLAG: TStringField;
    QConvBAIRRO1: TIntegerField;
    QConvCIDADE1: TIntegerField;
    QConvESTADO1: TIntegerField;
    QConvAuxBAIRRO: TIntegerField;
    QConvAuxCIDADE: TIntegerField;
    QConvAuxESTADO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure ButGravaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure DBEdit2Exit(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure TabCCShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButPesqClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure JvDBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Exportarparaoexcel3Click(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure QCadastroPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure TabFichaExit(Sender: TObject);
    procedure DBGrid1EditButtonClick(Sender: TObject);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    procedure QCadastroBeforeEdit(DataSet: TDataSet);
    procedure GridSaldoConvDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure GridSaldoCartaoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure GridSaldoConvTitleClick(Column: TColumn);
    procedure GridSaldoCartaoTitleClick(Column: TColumn);
    procedure TabSaldosShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure QSaldoConvAfterScroll(DataSet: TDataSet);
    procedure ButAtualCodImpClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure QSaldoConvCalcFields(DataSet: TDataSet);
    procedure GridSaldoConvCellClick(Column: TColumn);
    procedure ButLimpaSenhaClick(Sender: TObject);
    function  DigitaNovaSenha: string;
  private
    { Private declarations }
    Conv_id : Integer; //variavel usada para saber se o conv_id foi alterado para atualizar o nome do titular.
    procedure valida;
  public
    { Public declarations }
  end;
  TCustomDBGridCracker = class(TCustomDBGrid);

var
  FCadCartoesBemEstar: TFCadCartoesBemEstar;

implementation

uses DM, UPesqConv, StrUtils, cartao_util, UMenu, UValidacao, USelTipoImp,
  UDigitaSenha;

{$R *.dfm}

procedure TFCadCartoesBemEstar.FormCreate(Sender: TObject);
begin
  chavepri := 'cartao_id';
  detalhe  := 'Cart�o ID: ';
  inherited;
  DMConexao.Config.Open;
  ButAtualCodImp.Visible:= (DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S');
  DMConexao.Config.Close;
end;

procedure TFCadCartoesBemEstar.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Nome: '+QCadastroNOME.AsString;
end;

procedure TFCadCartoesBemEstar.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  if ((Trim(EdCod.Text) = '') and (Trim(EdNomeCart.Text) = '') and (Trim(EdCartao.Text) = '') and (Trim(EdNome.Text) = '')
    and (Trim(EdEmpID.text) ='') and (Trim(EdNomeEmp.Text) = '')) then
  begin
     MsgInf('� necess�rio especificar um crit�rio de busca.');
     EdCod.SetFocus;
     Exit;
  end;
  Screen.Cursor := crHourGlass;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select *');
  QCadastro.Sql.Add(' from cartoes ');
  QCadastro.Sql.Add(' join conveniados on conveniados.conv_id = cartoes.conv_id ');
  QCadastro.Sql.Add(' where cartoes.apagado <> ''S'' and conveniados.apagado <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and conveniados.conv_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
    QCadastro.Sql.Add(' and cartoes.conv_id in (select conv_id from conveniados where apagado <> ''S'' and titular like '+QuotedStr('%'+EdNome.Text+'%')+')');

  if Trim(EdCartao.Text) <> '' then begin
     if (( Length(Trim(EdCartao.Text)) = 11) and
       ( DigitoCartao(StrToFloat(Copy(EdCartao.Text,1,9))) = StrToInt(Copy(EdCartao.Text,10,2)))) then
       QCadastro.Sql.Add(' and codigo in ('+Copy(EdCartao.Text,1,9)+')')
     else begin
       EdCartao.Text := StringReplace(EdCartao.Text,',',''',''',[]);
       QCadastro.Sql.Add(' and codcartimp in ('+QuotedStr(EdCartao.Text)+')');
     end;
  end;
  if Trim(EdNomeCart.Text) <> '' then
     QCadastro.Sql.Add(' and nome like '+QuotedStr('%'+EdNomeCart.Text+'%'));
  if Trim(EdEmpID.Text) <> '' then
     QCadastro.Sql.Add(' and cartoes.empres_id in ('+EdEmpID.Text+')');
  if Trim(EdNomeEmp.Text) <> '' then
     QCadastro.Sql.Add(' and cartoes.conv_id in (select conv_id from conveniados where apagado <> ''S'' and empres_id in (select empres_id from empresas where nome like '+QuotedStr('%'+EdNomeEmp.Text+'%')+' ))');
  QCadastro.Sql.Add(' order by nome ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus else EdCod.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
  EdCartao.Clear;
  EdNomeCart.Clear;
  EdEmpID.Clear;
  EdNomeEmp.Clear;
  Screen.Cursor := crDefault;
end;

procedure TFCadCartoesBemEstar.ButIncluiClick(Sender: TObject);
begin
  inherited;
  QCadastroAPAGADO.AsString      := 'N';
  QCadastroLIBERADO.AsString     := 'S';
  QCadastroTITULAR.AsString      := 'N';
  QCadastroJAEMITIDO.AsString    := 'N';
  QCadastroLIMITE_MES.AsFloat    := 0;
  DBEdit3.SetFocus;
end;

procedure TFCadCartoesBemEstar.ButGravaClick(Sender: TObject);
begin
  valida;

  inherited

  {else
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('update cartoes set cartoes.NOME = '+QuotedStr(QCadastroNOME.AsString));
  DMConexao.AdoQry.SQL.Add(' WHERE CARTAO_ID = '+QCadastroCARTAO_ID.AsString);
  DMConexao.AdoQry.ExecSQL;}
end;

procedure TFCadCartoesBemEstar.valida;
begin
  if Trim(QCadastroNOME.AsString) = '' then begin
     ShowMessage('Informe o nome para o cart�o.');
     DBEdit3.SetFocus;
     Abort;
  end;
  if Trim(QCadastroCONV_ID.AsString) = '' then begin
     ShowMessage('Informe o titular para o cart�o.');
     DBEdit2.SetFocus;
     Abort;
  end;
end;

procedure TFCadCartoesBemEstar.QCadastroAfterInsert(DataSet: TDataSet);
begin
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Text:='SELECT NEXT VALUE FOR SCARTAO_ID';
  DMConexao.AdoQry.Open;
  QCadastroCARTAO_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  QCadastroDTEMISSAO.AsDateTime:= Now;
  Conv_id := 0;
  DMConexao.AdoQry.Close;
end;

procedure TFCadCartoesBemEstar.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  if Trim(QCadastroCONV_ID.AsString) <> '' then begin
     DMConexao.AdoQry.Close;
     DMConexao.AdoQry.SQL.Clear;
     DMConexao.AdoQry.Sql.Text := 'Select titular from conveniados where apagado <> ''S'' and conv_id = '+QCadastroCONV_ID.AsString;
     DMConexao.AdoQry.Open;
     if DMConexao.AdoQry.IsEmpty then begin
        ShowMessage('Conveniado n�o encontrado.');
        DBEdit2.Clear;
        if QCadastro.State in [dsInsert,dsEdit] then QCadastroCONV_ID.Clear;
     end;
     DMConexao.AdoQry.Close;
  end;
end;

procedure TFCadCartoesBemEstar.QCadastroBeforePost(DataSet: TDataSet);
var codimp: string;
    qtdEncontrados : Integer;
begin
  //VER AQUI
  QCadastroTITULAR.AsString      := UpperCase(QCadastroTITULAR.AsString);
  if UpperCase(QCadastroTITULAR.AsString) <> 'S' then QCadastroTITULAR.AsString := 'N';
  QCadastroLIBERADO.AsString     := UpperCase(QCadastroLIBERADO.AsString);
  if ((QCadastroLIBERADO.AsString <> 'S') and (QCadastroLIBERADO.AsString <> 'I')) then
    QCadastroLIBERADO.AsString := 'N';
  //QCadastroJAEMITIDO.AsString    := UpperCase(QCadastroJAEMITIDO.AsString);
  QCadastroJAEMITIDO.AsString := 'N';
  //if QCadastroJAEMITIDO.AsString <> 'S' then QCadastroJAEMITIDO.AsString := 'N';
  QCadastroEMPRES_ID.AsInteger := DMConexao.ExecuteQuery('SELECT EMPRES_ID '+
                                      'FROM CONVENIADOS WHERE conv_id = '+QCadastroCONV_ID.AsString+'');

  if QCadastro.State = dsInsert then begin
     QCadastroCODIGO.AsInteger := DMConexao.GeraCartao(QCadastroCONV_ID.AsInteger);
     QCadastroDIGITO.AsInteger := DigitoCartao(QCadastroCODIGO.AsFloat);
     DMConexao.AbrirCongis;
     if DMConexao.ExecuteScalar(' SELECT coalesce(usa_cod_importacao,''N'') as usa_cod_importacao FROM empresas ' +
                          ' where empres_id = (select empres_id from conveniados where conv_id = ' +
                          QCadastroCONV_ID.AsString + ')') = 'S' then begin
       repeat

        codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigBIN_BEM_ESTAR.AsInteger));
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT BIN_BEM_ESTAR FROM CONFIG WHERE BIN_BEM_ESTAR = '+QuotedStr(codimp));
        DMConexao.AdoQry.Open;


         if(DMConexao.AdoQry.Fields[0].IsNull) then
         begin
           qtdEncontrados := -1
         end
         else
           qtdEncontrados := DMConexao.AdoQry.Fields[0].Value;
         //qtdEncontrados := DMConexao.ExecuteScalar('SELECT COD_CARD_BIN FROM CONFIG WHERE COD_CARD_BIN = '+QuotedStr(codimp));
       until (qtdEncontrados = -1);
       QCadastroCODCARTIMP.AsString := codimp;
     end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
        QCadastroCODCARTIMP.AsString := QCadastroCODIGO.AsString
     else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then begin
        if QCadastroTITULAR.AsString = 'S' then
           codimp := DMConexao.ObterCodCartImp
        else
           codimp := DMConexao.ObterCodCartImp(False);
        QCadastroCODCARTIMP.AsString := codimp;
     end
     else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
        QCadastroCODCARTIMP.AsString:= DMConexao.ObterCodCartImpMod1(QCadastroCONV_ID.AsInteger,
        DMConexao.ExecuteScalar('select empres_id from conveniados_bem_estar where conv_id = '+QCadastroCONV_ID.AsString),
        DMConexao.ExecuteScalar('select chapa from conveniados_bem_estar where conv_id = '+QCadastroCONV_ID.AsString))
     else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then begin
        codimp := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
        QCadastroCODCARTIMP.AsString := codimp;
     end;
     DMConexao.FecharConfigs;
  end;

  QCadastroCODCARTIMP.AsString   := SoNumero(QCadastroCODCARTIMP.AsString);
  if QCadastro.State = dsInsert then
  begin
    QCadastro.FieldByName('DTCADASTRO').AsDateTime := Now;
    QCadastro.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
    QCadastroEMPRES_ID.Value   := DMConexao.ExecuteQuery('select empres_id from conveniados_bem_estar where conv_id = '+QCadastroCONV_ID.AsString);
  end;
  QCadastro.FieldByName('DTALTERACAO').AsDateTime := Now;
  QCadastro.FieldByName('OPERADOR').AsString := Operador.Nome;
  if QCadastro.FieldByName('APAGADO').AsString = 'S' then
     QCadastro.FieldByName('DTAPAGADO').AsDateTime := Now;

  if QCadastro.State = dsInsert then
  begin
    if DMConexao.ConfigSENHA_CONV_ID.AsString = 'S' then
      QCadastroSENHA.AsString := Crypt('E',QCadastroSENHA.AsString,'BIGCOMPRAS')
    else
      QCadastroSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
  end;
  inherited;
//  if (not colocouMensagem) then begin
//    Abort;
//  end;
end;

procedure TFCadCartoesBemEstar.TabCCShow(Sender: TObject);
begin
  inherited;
  Qcontacorrente.Close;
  if not QCadastro.IsEmpty then begin
     Qcontacorrente.Parameters[0].Value := QCadastroCARTAO_ID.AsInteger;
     Qcontacorrente.Open;
  end;
end;

procedure TFCadCartoesBemEstar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_F12 then PageControl1.ActivePage := TabCC;
end;

procedure TFCadCartoesBemEstar.ButPesqClick(Sender: TObject);
begin
  inherited;
  FPesqConv := TFPesqConv.Create(self);
  FPesqConv.Tag := 1;
  FPesqConv.ShowModal;
  if FPesqConv.ModalResult = mrOk then begin
     if not ( QCadastro.State in [dsInsert,dsEdit]) then QCadastro.Edit;
     QCadastroCONV_ID.AsString := FPesqConv.QConveniadosconv_id.AsString;
  end;
  FPesqConv.Free;
  if PageControl1.ActivePage = TabFicha then
     DBEdit2.SetFocus
  else
     if PageControl1.ActivePage = TabGrade then DBGrid1.SetFocus;
end;

procedure TFCadCartoesBemEstar.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = vk_return then if Trim(DBEdit2.Text) = '' then ButPesq.Click;
end;

procedure TFCadCartoesBemEstar.JvDBGrid2TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  DMConexao.SortZQuery(Field.DataSet,Field.FieldName);
end;

procedure TFCadCartoesBemEstar.Exportarparaoexcel3Click(Sender: TObject);
begin
  inherited;
Grade_to_PlanilhaExcel(JvDBGrid2);
end;

procedure TFCadCartoesBemEstar.TabFichaShow(Sender: TObject);
begin
  inherited;
  //DBEdit3.SetFocus;
  QCadastro.Open;
  DBEdit3.SetFocus;
end;

procedure TFCadCartoesBemEstar.QCadastroPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  If Pos('Aten��o, este c�digo de cart�o j� est� sendo usado por outro conveniado.',E.Message) > 0 then begin
     Application.MessageBox('Aten��o, este c�digo de cart�o j� est� sendo usado por outro conveniado.'+#13+'Esta altera��o n�o foi gravada.','Erro',MB_ICONERROR);
     Action := daAbort;
  end;
end;

procedure TFCadCartoesBemEstar.TabFichaExit(Sender: TObject);
begin
  inherited;
  if (QCadastro.State in [dsInsert,dsEdit]) and (QCadastroCARTAO_ID.AsInteger > 0) then begin
     QCadastro.post;
  end;
end;

procedure TFCadCartoesBemEstar.DBGrid1EditButtonClick(Sender: TObject);
begin
  inherited;
  if DBGrid1.SelectedField.FieldName = 'CONV_ID' then begin
     ButPesq.Click;
  end;
end;

procedure TFCadCartoesBemEstar.QCadastroAfterPost(DataSet: TDataSet);
var conv_id, codigo : String;
begin

  inherited;
  //QCadastro.Close;
  //QCadastro.Open;
  conv_id := QCadastroCONV_ID.AsString;
  codigo  := DBEdit4.Text;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select *');
  QCadastro.Sql.Add(' from cartoes_bem_estar ');
  QCadastro.Sql.Add(' join conveniados_bem_estar conveniados on conveniados.conv_id = cartoes_bem_estar.conv_id ');
  QCadastro.Sql.Add(' where cartoes_bem_estar.apagado <> ''S'' and conveniados.apagado <> ''S'' ');
  QCadastro.SQL.Add(' and cartoes_bem_estar.conv_id = '+conv_id+' and codigo = '+codigo);
  QCadastro.Open;
  QCadastro.SQL.Text;
  QCadastro.Requery();
end;

procedure TFCadCartoesBemEstar.QCadastroBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  Conv_id := QCadastroCONV_ID.AsInteger;
end;

procedure TFCadCartoesBemEstar.GridSaldoConvDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var R : integer;
begin
  inherited;
with TCustomDBGridCracker(Sender) do begin
   if DataLink.ActiveRecord = Row - 1 then begin
      GridSaldoConv.Canvas.Brush.Color := clBlack;
      GridSaldoConv.Canvas.FillRect(Rect);
      Canvas.Brush.Color := $00BFFFFF;
   end
   else begin
      GridSaldoConv.Canvas.Brush.Color:= clWhite;
      GridSaldoConv.Canvas.FillRect(Rect);
   end;
end;
GridSaldoConv.Canvas.Font.Color  := clBlack;
R := Rect.Right;
if Column.Index = GridSaldoConv.Columns.Count - 1 then R := R-1;
GridSaldoConv.Canvas.FillRect(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1));
GridSaldoConv.DefaultDrawColumnCell(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1), DataCol, Column, State);
end;

procedure TFCadCartoesBemEstar.GridSaldoCartaoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var R : integer;
begin
inherited;
with TCustomDBGridCracker(Sender) do begin
   if DataLink.ActiveRecord = Row - 1 then begin
      GridSaldoCartao.Canvas.Brush.Color := clBlack;
      GridSaldoCartao.Canvas.FillRect(Rect);
      If QSaldoCartaoFECHAMENTO.AsDateTime = QSaldoConvFECHAMENTO.AsDateTime then
         GridSaldoCartao.Canvas.Brush.Color := $00BFFFFF
      else
         GridSaldoCartao.Canvas.Brush.Color:= clWhite;
   end;
   end;
   {else begin
      If QSaldoCartaoFECHAMENTO.AsDateTime = QSaldoConvFECHAMENTO.AsDateTime then
         GridSaldoCartao.Canvas.Brush.Color := $00BFFFFF
      else
         GridSaldoCartao.Canvas.Brush.Color:= clWhite;
      GridSaldoCartao.Canvas.FillRect(Rect);
   end;
end; }
GridSaldoCartao.Canvas.Font.Color  := clBlack;
R := Rect.Right;
if Column.Index = GridSaldoConv.Columns.Count - 1 then R := R-1;
GridSaldoCartao.Canvas.FillRect(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1));
GridSaldoCartao.DefaultDrawColumnCell(Classes.Rect(Rect.Left,Rect.top+1,R,Rect.Bottom-1), DataCol, Column, State);
end;

procedure TFCadCartoesBemEstar.GridSaldoConvTitleClick(Column: TColumn);
begin
  DMConexao.SortZQuery(Column.Field.DataSet,Column.FieldName);
end;

procedure TFCadCartoesBemEstar.GridSaldoCartaoTitleClick(Column: TColumn);
begin
  DMConexao.SortZQuery(Column.Field.DataSet,Column.FieldName);
end;

//TODO -O[SIDNEI] -C[TFCadCartoes.TabSaldosShow]:[COMENTADO SIDNEI]
procedure TFCadCartoesBemEstar.TabSaldosShow(Sender: TObject);
begin
  inherited;
  QSaldoConv.Close;
  QSaldoCartao.Close;
  if not QCadastro.IsEmpty then begin
     QSaldoConv.Parameters[0].Value := QCadastroCONV_ID.Value;
     QSaldoCartao.Parameters[0].Value := QCadastroCONV_ID.Value;
     QSaldoConv.Open;
     QSaldoCartao.Open;
  end;
  {QSaldoConv.Close;
  QSaldoCartao.Close;
  if not QCadastro.IsEmpty then begin
     Screen.Cursor := crHourGlass;
     QSaldoConv.Params[0].AsInteger   := QCadastroCONV_ID.AsInteger;
     QSaldoConv.Open;
     Application.ProcessMessages;
     QSaldoCartao.Params[0].AsInteger := QCadastroCONV_ID.AsInteger;
     QSaldoCartao.Params[1].AsInteger := QCadastroCARTAO_ID.AsInteger;
     QSaldoCartao.Open;
     Application.ProcessMessages;
     Screen.Cursor := crDefault;
  end; }
end;

procedure TFCadCartoesBemEstar.Button2Click(Sender: TObject);
begin
  inherited;
  Application.ProcessMessages;
  if QSaldoConv.Active and QSaldoCartao.Active then begin
     Screen.Cursor := crHourglass;
     QSaldoConv.Requery();
     Application.ProcessMessages;
     QSaldoCartao.Requery();
     Application.ProcessMessages;
     Screen.Cursor := crdefault;
  end;
end;

procedure TFCadCartoesBemEstar.QSaldoConvAfterScroll(DataSet: TDataSet);
begin
  inherited;
  GridSaldoCartao.Repaint;
end;

procedure TFCadCartoesBemEstar.ButAtualCodImpClick(Sender: TObject);
begin
  inherited;
  QCadastro.Edit;
  //VER AQUI
  QCadastroCODCARTIMP.AsString:= DMConexao.ObterCodCartImp;
end;

procedure TFCadCartoesBemEstar.Button1Click(Sender: TObject);
var I : Integer;
begin
  inherited;
  I := 0;
  DMConexao.AdoCon.BeginTrans;
  qConv.Open;
  qcard.Open;
  tExcel1.Open;
  tExcel1.First;
  while not qConv.Eof do begin
    dExcel.DataSet := tExcel1;
    tExcel1.Filter := 'CHAPA LIKE '+QuotedStr(qConvCHAPA.AsString);
    tExcel1.Filtered := True;
//    showmessage('teste');
    while not tExcel1.Eof do begin
      if not (qCard.Locate('CODCARTIMP',Trim(tExcel1N_CART.AsString),[])) then begin
        try
        qcard.Append;
        //QCadastroCONV_ID.
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text:='SELECT NEXT VALUE FOR SCARTAOBEMESTAR_ID';
        DMConexao.AdoQry.Open;
        qCardCARTAO_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qCardCONV_ID.AsInteger := qConvCONV_ID.AsInteger;
        qCardCODIGO.AsInteger  := tExcel1N_CART.AsInteger;
        qCardDIGITO.AsInteger  := DigitoCartao(qCardCODIGO.AsFloat);
        if TrimRight(tExcel1NOMEDODEPENDENTE.AsString) = '' then begin
          qCardNOME.AsString := TrimRight(tExcel1NOMEFUNCIONRIO.AsString);
          qCardPARENTESCO.AsString := 'TITULAR';
          qCardTITULAR.AsString := 'S';
        end else begin
          qCardNOME.AsString := TrimRight(tExcel1NOMEDODEPENDENTE.AsString);
          qCardTITULAR.AsString := 'N';
        end;
        qCardCODCARTIMP.AsInteger := tExcel1N_CART.AsInteger;
        qCardLIBERADO.AsString := 'S';
        qCardATIVO.AsString := 'S';
        qCardJAEMITIDO.AsString := 'S';
        qCardAPAGADO.AsString := 'N';
        qCardLIMITE_MES.AsFloat := 200.00;
        qCardDTALTERACAO.AsString := FormatDateTime('DD/MM/YYYY',now);
        qCardDTCADASTRO.AsString := FormatDateTime('DD/MM/YYYY',now);;
        qcard.Post;
        except
          I := I+1;
        end;
      end;
      tExcel1.Next;
    end;
    qConv.Next;
  end;


  qConv.First;
  qcard.Open;
  tExcel2.Open;
  tExcel2.First;
  while not qConv.Eof do begin
    dExcel.DataSet := tExcel2;
    tExcel2.Filter := 'CHAPA LIKE '+QuotedStr(qConvCHAPA.AsString);
    tExcel2.Filtered := True;
//    showmessage('teste');
    while not tExcel2.Eof do begin
      if not (qCard.Locate('CODCARTIMP',Trim(tExcel2N_CART.AsString),[])) then begin
        try
        qcard.Append;
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text:='SELECT NEXT VALUE FOR SCARTAOBEMESTAR_ID';
        DMConexao.AdoQry.Open;
        qCardCARTAO_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qCardCONV_ID.AsInteger := qConvCONV_ID.AsInteger;
        qCardCODIGO.AsInteger  := tExcel2N_CART.AsInteger;
        qCardDIGITO.AsInteger  := DigitoCartao(qCardCODIGO.AsFloat);
        if TrimRight(tExcel2NOMEDODEPENDENTE.AsString) = '' then begin
          qCardNOME.AsString := TrimRight(tExcel2NOMEFUNCIONRIO.AsString);
          qCardPARENTESCO.AsString := 'TITULAR';
          qCardTITULAR.AsString := 'S';
        end else begin
          qCardNOME.AsString := TrimRight(tExcel2NOMEDODEPENDENTE.AsString);
          qCardTITULAR.AsString := 'N';
        end;
        qCardCODCARTIMP.AsInteger := tExcel2N_CART.AsInteger;
        qCardLIBERADO.AsString := 'S';
        qCardATIVO.AsString := 'S';
        qCardJAEMITIDO.AsString := 'S';
        qCardAPAGADO.AsString := 'N';
        qCardLIMITE_MES.AsFloat := 200.00;
        qCardDTALTERACAO.AsDateTime := now;
        qCardDTCADASTRO.AsDateTime := now;
        qcard.Post;
        except
          I := I+1;
        end;
      end;
      tExcel2.Next;
    end;
    qConv.Next;
  end;

  qConv.First;
  qcard.Open;
  tExcel3.Open;
  tExcel3.First;
  while not qConv.Eof do begin
    dExcel.DataSet := tExcel3;
    tExcel3.Filter := 'CHAPA LIKE '+QuotedStr(qConvCHAPA.AsString);
    tExcel3.Filtered := True;
//    showmessage('teste');
    while not tExcel3.Eof do begin
      if not (qCard.Locate('CODCARTIMP',Trim(tExcel3N_CART.AsString),[])) then begin
        try
        qcard.Append;
        //QCadastroCONV_ID.
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text:='SELECT NEXT VALUE FOR SCARTAOBEMESTAR_ID';
        DMConexao.AdoQry.Open;
        qCardCARTAO_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
        qCardCONV_ID.AsInteger := qConvCONV_ID.AsInteger;
        qCardCODIGO.AsInteger  := tExcel3N_CART.AsInteger;
        qCardDIGITO.AsInteger  := DigitoCartao(qCardCODIGO.AsFloat);
        if TrimRight(tExcel3NOMEDODEPENDENTE.AsString) = '' then begin
          qCardNOME.AsString := TrimRight(tExcel3NOMEFUNCIONRIO.AsString);
          qCardPARENTESCO.AsString := 'TITULAR';
          qCardTITULAR.AsString := 'S';
        end else begin
          qCardNOME.AsString := TrimRight(tExcel3NOMEDODEPENDENTE.AsString);
          qCardTITULAR.AsString := 'N';
        end;
        qCardCODCARTIMP.AsInteger := tExcel3N_CART.AsInteger;
        qCardLIBERADO.AsString := 'S';
        qCardATIVO.AsString := 'S';
        qCardJAEMITIDO.AsString := 'S';
        qCardAPAGADO.AsString := 'N';
        qCardLIMITE_MES.AsFloat := 200.00;
        qCardDTALTERACAO.AsDateTime := now;
        qCardDTCADASTRO.AsDateTime := now;
        qcard.Post;
        except
          I := I+1;
        end;
      end;
      tExcel3.Next;
    end;
    qConv.Next;
  end;
  showmessage('fim');
  DMConexao.AdoCon.CommitTrans
end;

procedure TFCadCartoesBemEstar.QSaldoConvCalcFields(DataSet: TDataSet);
VAR limiteConveniado : Currency;
begin
  inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT LIMITE_MES FROM CONVENIADOS_BEM_ESTAR WHERE CONV_ID = '+QCadastroCONV_ID.AsString);
  DMConexao.AdoQry.Open;
  limiteConveniado := DMConexao.AdoQry.Fields[0].Value;
  QSaldoConvRESTANTE.Value := limiteConveniado - QSaldoConvtotal.Value;
  DMConexao.AdoQry.Close;
end;



procedure TFCadCartoesBemEstar.GridSaldoConvCellClick(Column: TColumn);
var teste : TDateTime;
    i     : Integer;
begin
  inherited;
  while not QSaldoCartao.Eof do
  begin
    if(QSaldoConvFECHAMENTO.AsDateTime = QSaldoCartaoFECHAMENTO.AsDateTime) then
    begin

         GridSaldoCartao.Canvas.Brush.Color := $00BFFFFF;

         Abort;

    end;

    QSaldoCartao.Next;
  end;
  QSaldoCartao.First;

  {teste := QSaldoConvFECHAMENTO.AsDateTime;

  for i := 0 to GridSaldoCartao.FieldCount do
  begin
    if(QSaldoConvFECHAMENTO.AsDateTime = GridSaldoCartao.r)
  end; }
end;

///<summary>
///
///</summary>
///<param name="Mensagem">Tipo do Par�metro  = String</param>
///<param name="Component">Tipo do Par�metro = TComponent</param>
///<returns>Boolean</returns>
///<remarks>
///   <para>
///         A Function fnVerfCompVazioEmTabSheet � oriunda da unit UValida��o
///   </para>
///</remarks>

procedure TFCadCartoesBemEstar.ButLimpaSenhaClick(Sender: TObject);
var ItemSel : Integer;
begin
  //fnVerfCompVazioEmTabSheet oriunda da Unit UValida��o. Para usar esta function � necess�rio usar a Unit UValidacao
  //<param name="Mensagem">Tipo do Par�metro  = String</param>
  //<param name="Component">Tipo do Par�metro = TComponent</param>
  //<returns>Boolean</returns>
  if (fnVerfCompVazioEmTabSheet('Chapa Obrigat�ria',DBEdit1) = False)
  and(fnVerfCompVazioEmTabSheet('Empresa Obrigat�ria',DBEdit2) = False)then
  begin
    inherited;
    if not QCadastro.IsEmpty then
    begin
      ItemSel := TFSelTipoImp.AbrirJanela(['Alterar senha para 1111','Alterar senha para n�mero do Conv. ID','Digitar a senha nova']);
      QCadastro.Open;
      QCadastro.edit;
      case ItemSel of
        0: QCadastroSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
        1: QCadastroSENHA.AsString := Crypt('E',IntToStr(QCadastroCONV_ID.AsInteger),'BIGCOMPRAS');
        2: QCadastroSENHA.AsString := Crypt('E',DigitaNovaSenha,'BIGCOMPRAS');
      end;
      if Application.MessageBox('Confirma esta opera��o?','Confirma��o',mb_yesno+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
        QCadastro.Post
      else
        QCadastro.Cancel;
    end;
  end;
end;

function TFCadCartoesBemEstar.DigitaNovaSenha: string;
var senha : String;
begin
  FDigitaSenha := TFDigitaSenha.Create(self);
  FDigitaSenha.ShowModal;
  if FDigitaSenha.ModalResult = mrOk then
  begin
    senha := FDigitaSenha.edSenha.Text;
    Result:= senha;
  end
  else
    Result:= '';
  FDigitaSenha.Free;
end;

end.
