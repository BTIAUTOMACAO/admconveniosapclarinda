unit UCadSeg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,}
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls, Mask,
  {JvMemDS,} JvToolEdit, Menus, JvExMask, JvExDBGrids, JvDBGrid, ADODB;

type
  TFCadSeg = class(TFCad)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    dbEdtNm: TDBEdit;
    GroupBox2: TGroupBox;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    Label43: TLabel;
    Label44: TLabel;
    QCadastroSEG_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ButApagaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadSeg: TFCadSeg;

implementation

uses DM, UValidacao,UMenu;

{$R *.dfm}

procedure TFCadSeg.ButBuscaClick(Sender: TObject);
begin
  inherited;
  if ((Trim(EdCod.Text) <> '') or (Trim(EdNome.Text) <> '')) then begin
     QCadastro.Close;
     if Trim(EdCod.Text) <> '' then QCadastro.Sql.Text := 'Select * from segmentos where coalesce(apagado,''N'') <> ''S'' and seg_id in ('+EdCod.Text+') order by seg_id '
                               else QCadastro.Sql.Text := 'Select * from segmentos where coalesce(apagado,''N'') <> ''S'' and descricao like ''%'+EdNome.Text+'%'' order by descricao ';
  end
  else QCadastro.Sql.Text := 'Select * from segmentos where coalesce(apagado,''N'') <> ''S'' order by seg_id ';
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus;
  edcod.Clear;
  EdNome.Clear;
end;

procedure TFCadSeg.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtNm.SetFocus;
end;

procedure TFCadSeg.FormCreate(Sender: TObject);
begin
  chavepri := 'seg_id';
  detalhe  := 'Segmento ID: ';
  inherited;
  QCadastro.Open;
end;

procedure TFCadSeg.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Segmento: '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadSeg.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SSEG_ID AS SEG_ID');
  DMConexao.AdoQry.Open;
  QCadastroSEG_ID.AsInteger := DMConexao.AdoQry.FieldByName('SEG_ID').AsInteger;
  QCadastroAPAGADO.AsString := 'N';
  QCadastroDTCADASTRO.AsString := DateTimeToStr(Now);
  QCadastroOPERCADASTRO.AsString := Operador.Nome;
end;

procedure TFCadSeg.ButGravaClick(Sender: TObject);
begin
  if fnVerfCompVazioEmTabSheet('Nome obrigatório!', dbEdtNm) then abort;
  inherited;
end;

procedure TFCadSeg.QCadastroBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;
end;
procedure TFCadSeg.GridHistoricoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  try
    if Pos(Field.FieldName,QHistorico.Sort) > 0 then
    begin
      if Pos(' DESC',QHistorico.Sort) > 0 then
        QHistorico.Sort := Field.FieldName
      else
        QHistorico.Sort := Field.FieldName+' DESC';
    end
    else
      QHistorico.Sort := Field.FieldName;
  except
  end;

end;

procedure TFCadSeg.ButApagaClick(Sender: TObject);
var seg_id : string;
begin
  seg_id := QCadastroSEG_ID.AsString;
  inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('UPDATE SEGMENTOS SET DTAPAGADO = CURRENT_TIMESTAMP WHERE SEG_ID = '+seg_id+'');
  DMConexao.AdoQry.ExecSQL;
  DMConexao.AdoQry.Close;
  QCadastro.Requery;
end;

end.

