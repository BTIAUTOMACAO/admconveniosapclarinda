unit uProdutos;

interface

uses SysUtils, Classes;

type

  TBarras = class(TObject)
  private
    FBarras : String;
    FINBS : String;
    procedure setBarra(const Value: String);
    procedure setINBS(const Value: String);
  public
    property Barras : String read FBarras write setBarra;
    property INBS : String read FINBS write setINBS;
    constructor Create(Barras, INBS : String);
  end;

  TProduto = class(TObject)
  private
    FCod : Integer;
    FGrupo : Integer;
    FDescricao : String;
    FBarras : TBarras;
    FPercentualDesconto: Currency;
    FObrigaDesconto : Boolean;
    FPrecoUnitario : Currency;
    FNomeLab : String;
    FQtdMax : Integer;
    FPrecoTabela: Currency;
    procedure setCod(const Value: Integer);
    procedure setGrupo(const Value : Integer);
    procedure setDescricao(const Value: String);
    procedure setBarras(const Value: TBarras);
    procedure SetPercentualDesconto(const Value: Currency);
    procedure setObrigaDesconto(const Value : Boolean);
    procedure setPrecoUnitario(const Value: Currency);
    procedure setNomeLab(const Value: String);
    procedure setQtdMax(const Value: Integer);
    procedure CopiarProduto(prod: TProduto);
  public
    property Cod : Integer read FCod;
    property Grupo : Integer read FGrupo;
    property Descricao : String read FDescricao;
    property Barras : TBarras read FBarras;
    property PrecoUnitario : Currency read FPrecoUnitario;
    property PrecoNomeLab : String read FNomeLab;
    property PercentualDesconto : Currency read FPercentualDesconto write SetPercentualDesconto;
    property ObrigaDesconto : Boolean read FObrigaDesconto;
    property QtdMax : Integer read FQtdMax;
    constructor Create(pCod, pGrupo : Integer ; pDescricao : String = ''; pBarras : TBarras = nil; pPreco_Unitario : Currency = 0 ; pNome_Lab : String = '' ; pPercentualDesconto : Currency = 0; pObrigaDesconto : Boolean = True; pQtd_Max : Integer = 0);
  end;

implementation

{ TProduto }
constructor TProduto.Create(pCod, pGrupo : Integer ; pDescricao: String; pBarras : TBarras;
  pPreco_Unitario : Currency ; pNome_Lab: String ; pPercentualDesconto : Currency ; pObrigaDesconto : Boolean; pQtd_Max : Integer);
begin
  FCod := pCod;
  FGrupo := pGrupo;
  FDescricao := pDescricao;
  FBarras := pBarras;
  FPrecoUnitario := pPreco_Unitario;
  FObrigaDesconto := pObrigaDesconto;
  FPercentualDesconto := pPercentualDesconto;
  FNomeLab := pNome_Lab;
  FQtdMax := pQtd_Max;
end;

procedure TProduto.setCod(const Value: Integer);
begin
  FCod := Value;
end;


procedure TProduto.setGrupo(const Value: Integer);
begin
  FGrupo := Value;
end;

procedure TProduto.setDescricao(const Value: String);
begin
  FDescricao := Value;
end;

procedure TProduto.setBarras(const Value: TBarras);
begin
  FBarras := Value;
end;

procedure TProduto.setPrecoUnitario(const Value: Currency);
begin
  FPrecoUnitario := Value;
end;

procedure TProduto.setQtdMax(const Value: Integer);
begin
  FQtdMax := Value;
end;

procedure TProduto.setNomeLab(const Value: String);
begin
  FNomeLab := Value;
end;

procedure TProduto.CopiarProduto(prod: TProduto);
begin
  Self.setCod(prod.Cod);
  Self.setBarras(prod.Barras);
  Self.setDescricao(prod.Descricao);
  Self.setNomeLab(prod.PrecoNomeLab);
  Self.setQtdMax(prod.QtdMax);
  Self.setPrecoUnitario(prod.PrecoUnitario);
end;

procedure TProduto.setObrigaDesconto(const Value: Boolean);
begin
  FObrigaDesconto := Value;
end;

procedure TProduto.SetPercentualDesconto(const Value: Currency);
begin
  FPercentualDesconto := Value;
end;

{ TBarras }

constructor TBarras.Create(Barras, INBS: String);
begin
  FBarras := Barras;
  FINBS := INBS;
end;

procedure TBarras.setBarra(const Value: String);
begin
  FBarras := Value;
end;

procedure TBarras.setINBS(const Value: String);
begin
  FINBS := Value;
end;

end.
