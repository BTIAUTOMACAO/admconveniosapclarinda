unit UCons_Autor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DB, Mask, DBCtrls,
  ComCtrls, Grids, DBGrids, JvToolEdit, JvExMask, JvExDBGrids, JvDBGrid, ADODB;

type
  TFCons_Autor = class(TF1)
    Panel1: TPanel;
    Panel2: TPanel;
    EdAutor: TEdit;
    Button1: TButton;
    Label1: TLabel;
    DataSource1: TDataSource;
    RGTipoCons: TRadioGroup;
    DBNavigator1: TDBNavigator;
    Status: TStatusBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label11: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    JvDBGrid1: TJvDBGrid;
    Bevel6: TBevel;
    DBEdit32: TDBEdit;
    Label33: TLabel;
    Bevel7: TBevel;
    Label20: TLabel;
    DBEdit19: TDBEdit;
    Label34: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    Label35: TLabel;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label37: TLabel;
    DBEdit36: TDBEdit;
    TabProdutos: TTabSheet;
    pnlMovimentacaoAutor: TPanel;
    DBGrid2: TDBGrid;
    Panel21: TPanel;
    LabTotProd: TLabel;
    DSProdutos: TDataSource;
    Label38: TLabel;
    DBEdit37: TDBEdit;
    Label39: TLabel;
    DBEdit38: TDBEdit;
    TabHistorico: TTabSheet;
    JvDBGrid4: TJvDBGrid;
    Panel14: TPanel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Btnhist1: TButton;
    DBNavigator6: TDBNavigator;
    dataini1: TJvDateEdit;
    datafin1: TJvDateEdit;
    DBCampoCC: TComboBox;
    DSHistorico: TDataSource;
    Label40: TLabel;
    DBEdit39: TDBEdit;
    QHistorico: TADOQuery;
    QProdutos: TADOQuery;
    QAutor: TADOQuery;
    QAutorAUTORIZACAO_ID: TIntegerField;
    QAutorCARTAO_ID: TIntegerField;
    QAutorCONV_ID: TIntegerField;
    QAutorCRED_ID: TIntegerField;
    QAutorDIGITO: TWordField;
    QAutorDATA: TDateTimeField;
    QAutorHORA: TStringField;
    QAutorDATAVENDA: TDateTimeField;
    QAutorDEBITO: TBCDField;
    QAutorCREDITO: TBCDField;
    QAutorVALOR_CANCELADO: TBCDField;
    QAutorBAIXA_CONVENIADO: TStringField;
    QAutorBAIXA_CREDENCIADO: TStringField;
    QAutorENTREG_NF: TStringField;
    QAutorRECEITA: TStringField;
    QAutorCESTA: TStringField;
    QAutorCANCELADA: TStringField;
    QAutorDIGI_MANUAL: TStringField;
    QAutorTRANS_ID: TIntegerField;
    QAutorFORMAPAGTO_ID: TIntegerField;
    QAutorFATURA_ID: TIntegerField;
    QAutorPAGAMENTO_CRED_ID: TIntegerField;
    QAutorAUTORIZACAO_ID_CANC: TIntegerField;
    QAutorOPERADOR: TStringField;
    QAutorDATA_VENC_EMP: TDateTimeField;
    QAutorDATA_FECHA_EMP: TDateTimeField;
    QAutorDATA_VENC_FOR: TDateTimeField;
    QAutorDATA_FECHA_FOR: TDateTimeField;
    QAutorHISTORICO: TStringField;
    QAutorNF: TIntegerField;
    QAutorDATA_ALTERACAO: TDateTimeField;
    QAutorDATA_BAIXA_CONV: TDateTimeField;
    QAutorDATA_BAIXA_CRED: TDateTimeField;
    QAutorOPER_BAIXA_CONV: TStringField;
    QAutorOPER_BAIXA_CRED: TStringField;
    QAutorDATA_CONFIRMACAO: TDateTimeField;
    QAutorOPER_CONFIRMACAO: TStringField;
    QAutorCONFERIDO: TStringField;
    QAutorNSU: TIntegerField;
    QAutorPREVIAMENTE_CANCELADA: TStringField;
    QAutorEMPRES_ID: TIntegerField;
    QAutortitular: TStringField;
    QAutorchapa: TFloatField;
    QAutornomecartao: TStringField;
    QAutorcatnum: TIntegerField;
    QAutorcartdig: TWordField;
    QAutorcodcartimp: TStringField;
    QAutorfornecedor: TStringField;
    QAutoremp_razao: TStringField;
    QAutoremp_fantasia: TStringField;
    QAutorforma: TStringField;
    QProdutosMOV_ID: TIntegerField;
    QProdutosAUTORIZACAO_ID: TIntegerField;
    QProdutosQTDE: TIntegerField;
    QProdutosPRECO_UNI: TFloatField;
    QProdutosPRECO_TAB: TFloatField;
    QProdutosCANCELADO: TStringField;
    QProdutosCOMREC: TStringField;
    QProdutosPROD_ID: TIntegerField;
    QProdutosCRM: TStringField;
    QProdutosDATA_CADASTRO: TDateTimeField;
    QProdutosdescricao: TStringField;
    QProdutoscodinbs: TStringField;
    procedure EdAutorKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RGTipoConsClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabProdutosHide(Sender: TObject);
    procedure QProdutosAfterOpen(DataSet: TDataSet);
    procedure TabProdutosShow(Sender: TObject);
    procedure TabHistoricoShow(Sender: TObject);
    procedure Btnhist1Click(Sender: TObject);
    procedure datafin1Exit(Sender: TObject);
  private
    procedure SomarProdutos;
    procedure PesqLogCC;
    { Private declarations }
  public
    { Public declarations }

  end;


implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFCons_Autor.EdAutorKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#27,#8]) then Key := #0;
  if ((Key = #13) and (Trim(EdAutor.Text) <> '') ) then Button1Click(nil);
end;

procedure TFCons_Autor.Button1Click(Sender: TObject);
begin
  inherited;
  if Trim(EdAutor.Text) = '' then Exit;
  QAutor.Close;
  case RGTipoCons.ItemIndex of
    0: QAutor.SQl[11] := '  where contacorrente.autorizacao_id = '+EdAutor.Text;
    1: QAutor.SQl[11] := '  where contacorrente.NF = '+EdAutor.Text;
    2: QAutor.SQl[11] := '  where contacorrente.trans_id = '+EdAutor.Text;
  end;
  QAutor.Open;
  if QAutor.IsEmpty then begin
     ShowMessage('Autoriza��o n�o encontrada.');
     EdAutor.SetFocus; 
  end;
end;

procedure TFCons_Autor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  QAutor.Close;
end;

procedure TFCons_Autor.RGTipoConsClick(Sender: TObject);
begin
  inherited;
  if RGTipoCons.ItemIndex = 0 then Label1.Caption := 'Autoriza��o (Sem Dig)'
                              else Label1.Caption := 'N� da Nota Fiscal';
end;

procedure TFCons_Autor.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  Status.SimpleText := ' Registro '+IntToStr(QAutor.RecNo)+' de '+IntToStr(QAutor.RecordCount);
end;

procedure TFCons_Autor.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if not QAutor.IsEmpty then TabSheet1.Show;
end;

procedure TFCons_Autor.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_return then TabSheet1.Show
end;

procedure TFCons_Autor.SomarProdutos;
var Atual : TBookmark; Soma : Currency;
begin
   Soma := 0;
   if QProdutos.Active and ( not QProdutos.IsEmpty ) then begin
      Atual := QProdutos.GetBookmark;
      QProdutos.DisableControls;
      QProdutos.First;

      while not QProdutos.Eof do begin
         Soma := Soma + ArredondaDin( QProdutosQTDE.AsInteger * QProdutosPRECO_UNI.AsCurrency);
         QProdutos.Next;
      end;
      QProdutos.GotoBookmark(Atual);
      QProdutos.FreeBookmark(Atual);
      QProdutos.EnableControls;
   end;
   LabTotProd.Caption := FormatDinBR(Soma);
   Application.ProcessMessages;
end;

procedure TFCons_Autor.TabProdutosHide(Sender: TObject);
begin
  inherited;
  QProdutos.Close;
end;

procedure TFCons_Autor.QProdutosAfterOpen(DataSet: TDataSet);
begin
  inherited;
  SomarProdutos;
end;

procedure TFCons_Autor.TabProdutosShow(Sender: TObject);
begin
  inherited;
  if not QAutor.IsEmpty then
  begin
    QProdutos.Close;
    QProdutos.Parameters.ParamByName('AUTORIZACAO_ID').Value.AsInteger:= QAutorAUTORIZACAO_ID.AsInteger;
    QProdutos.Open;
  end;
end;

procedure TFCons_Autor.PesqLogCC;
begin
  DMConexao.AdoQry.Close;
  QHistorico.Close;
  DBCampoCC.Clear;
  if not QAutor.IsEmpty then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Clear;
    DMConexao.AdoQry.Sql.Add(' Select distinct CADASTRO+''.''+CAMPO from logs ');
    DMConexao.AdoQry.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('dd/mm/yyyy 00:00:00',dataini1.Date))+' and '+QuotedStr(FormatDateTime('mm/dd/yyyy 23:59:59',datafin1.Date)));
    DMConexao.AdoQry.Sql.Add(' and ID = '+QAutorAUTORIZACAO_ID.AsString);
    DMConexao.AdoQry.Sql.Add(' and JANELA = "CONTACORRENTE" ');
    DMConexao.AdoQry.Sql.Add('  order by 1');
    DMConexao.AdoQry.Sql.Text;//mostra debug.
    DMConexao.AdoQry.Open;
    DMConexao.AdoQry.First;
    DBCampoCC.Items.Add('Todos os Campos');
    while not DMConexao.AdoQry.Eof do
    begin
      DBCampoCC.Items.Add(DMConexao.AdoQry.Fields[0].AsString);
      DMConexao.AdoQry.Next;
    end;
    DMConexao.AdoQry.Close;
    DBCampoCC.ItemIndex := 0;
  end;
end;

procedure TFCons_Autor.TabHistoricoShow(Sender: TObject);
begin
  inherited;
  dataini1.date := date;
  datafin1.date := date;
  dataini1.SetFocus;
end;

procedure TFCons_Autor.Btnhist1Click(Sender: TObject);
var cadastro, field: string;
begin
  inherited;
  if QAutor.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('mm/dd/yyyy 00:00:00',dataini1.Date))+' and '+QuotedStr(FormatDateTime('mm/dd/yyyy 23:59:59',datafin1.Date)));
  QHistorico.Sql.Add(' and ID = '+QAutorAUTORIZACAO_ID.AsString);
  QHistorico.Sql.Add(' and JANELA = "CONTACORRENTE" ');
  if DBCampoCC.ItemIndex > 0 then
  begin
    cadastro := Copy(DBCampoCC.Text,1,Pos('.',DBCampoCC.Text)-1);
    field    := Copy(DBCampoCC.Text,Pos('.',DBCampoCC.Text)+1,Length(DBCampoCC.Text));
    QHistorico.Sql.Add(' and CAMPO = '+QuotedStr(field));
  end;
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFCons_Autor.datafin1Exit(Sender: TObject);
begin
  inherited;
  PesqLogCC;
end;

end.
