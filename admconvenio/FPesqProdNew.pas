unit FPesqProdNew;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, ExtCtrls, Grids, DBGrids, DB, uProdutos, Buttons, ADODB;

type
  TF_PesqProdNew = class(TForm)
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    Label3: TLabel;
    edtBusca: TEdit;
    DSProdutos: TDataSource;
    DSBarras: TDataSource;
    DSProdutosProgProd: TDataSource;
    QProdutos: TADOQuery;
    QProdutosProgProd: TADOQuery;
    QBarras: TADOQuery;
    QProdutosProgProdprod_id: TIntegerField;
    QProdutosProgProdgrupo_prod_id: TIntegerField;
    QProdutosProgProdperc_desc: TIntegerField;
    QProdutosProgProddescricao: TStringField;
    QProdutosProgProdqtd_max: TIntegerField;
    QProdutosProgProdnomelab: TStringField;
    QProdutosProgProdprc_unit: TBCDField;
    QProdutosProgProdCODINBS: TStringField;
    QProdutosProgProdobriga_desconto: TStringField;
    QBarrasbarras: TStringField;
    QBarrascodinbs: TStringField;
    QProdutosprod_id: TIntegerField;
    QProdutosgrupo_prod_id: TIntegerField;
    QProdutosperc_desc: TBCDField;
    QProdutosdescricao: TStringField;
    QProdutosqtd_max: TIntegerField;
    QProdutosnomelab: TStringField;
    QProdutosprc_unit: TBCDField;
    QProdutosCODINBS: TStringField;
    QProdutosobriga_desconto: TStringField;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure edtBuscaEnter(Sender: TObject);
    procedure edtBuscaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtBuscaKeyPress(Sender: TObject; var Key: Char);
  private
    function getBarras(prod_id : Integer) : TBarras;
    procedure addFiltro(valor : string);
    { Private declarations }
  public
    produto : TProduto;
    letra, prog_prod, uf, cred_id : string;
    { Public declarations }
  end;

var
  F_PesqProdNew: TF_PesqProdNew;

implementation

uses URotinasGrids, UCadProduto, DM;

{$R *.dfm}

function TF_PesqProdNew.getBarras(prod_id: Integer): TBarras;
begin
  QBarras.Close;
  QBarras.Parameters.ParamByName('PROD_ID').Value := prod_id;
  QBarras.Open;
  if not qBarras.IsEmpty then
    Result := TBarras.Create(qBarrasBARRAS.Value,qBarrasCODINBS.Value)
  else
    Result := nil;
end;

procedure TF_PesqProdNew.addFiltro(valor : string);
begin
  if Trim(valor) <> '' then begin
    qProdutos.Filtered := False;
    qProdutos.Filter := 'DESCRICAO LIKE ' + QuotedStr('*' + valor + '*');
    qProdutos.Filtered := True;
  end else begin
    qProdutos.Filtered := False;
  end;
end;

procedure TF_PesqProdNew.DBGrid1DblClick(Sender: TObject);
begin
  if not qProdutos.IsEmpty then
    ModalResult := mrOk;
end;

procedure TF_PesqProdNew.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  pintaGrid(Sender,Rect,DataCol, Column,State);
end;

procedure TF_PesqProdNew.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
    if not qProdutos.IsEmpty then
      ModalResult := mrOk;
  if key = vk_escape then begin
    ModalResult := mrCancel;
    Close;
  end;
end;

procedure TF_PesqProdNew.FormClose(Sender: TObject;
  var Action: TCloseAction);
var Barras : TBarras;
begin
  produto := nil;
  if (ModalResult = mrOk) then begin
    Barras := getBarras(qProdutosPROD_ID.Value);
    if barras <> nil then begin
      produto := TProduto.Create(qProdutosPROD_ID.Value, qProdutosGRUPO_PROD_ID.AsInteger, qProdutosDESCRICAO.Value, Barras, qProdutosPRC_UNIT.AsCurrency, qProdutosNOMELAB.AsString,  qProdutosPERC_DESC.AsCurrency, UpperCase(qProdutosOBRIGA_DESCONTO.AsString) = 'S',  qProdutosQTD_MAX.AsInteger);
    end;
  end;
end;

procedure TF_PesqProdNew.FormShow(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QProdutos.Close;
  if Trim(prog_prod) <> '' then begin
    QProdutos.Parameters.ParamByName('prog_id').Value := StrToInt(prog_prod);
    QProdutos.Parameters.ParamByName('progId').Value := StrToInt(prog_prod);
  end;

  QProdutos.Parameters.ParamByName('uf').Value := uf;
  QProdutos.Parameters.ParamByName('UF').Value := uf;
  if Trim(cred_id) <> '' then
    QProdutos.Parameters.ParamByName('cred_id').Value := StrToInt(cred_id);
  QProdutos.Open;
  addFiltro(edtBusca.Text);
  edtBusca.SetFocus;
  letra := '';
  Screen.Cursor := crDefault;
end;

procedure TF_PesqProdNew.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    DBGrid1.SetFocus
  else if Key = VK_UP then
    DBGrid1.DataSource.DataSet.Prior
  else if Key = VK_DOWN then
    DBGrid1.DataSource.DataSet.Next;
end;

procedure TF_PesqProdNew.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in ['A'..'Z']) or (Key in ['a'..'z']) or (Key in ['0'..'9']) or (Key in [' ',#8]) then begin
    letra := key;
    edtBusca.SetFocus;
  end;
end;

procedure TF_PesqProdNew.edtBuscaEnter(Sender: TObject);
var aux : string;
begin
  if (letra <> '') and (letra <> #8) then begin
    TEdit(Sender).Text := TEdit(Sender).Text + letra;
    TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
  end else if (letra = #8) and (TEdit(Sender).Text <> '') then begin
    aux := TEdit(Sender).Text;
    Delete(aux,Length(TEdit(Sender).Text),1);
    TEdit(Sender).Text := aux;
    TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
  end;
end;

procedure TF_PesqProdNew.edtBuscaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if TEdit(Sender).Text = '' then
    addFiltro('');
end;

procedure TF_PesqProdNew.edtBuscaKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in ['A'..'Z']) or (Key in ['a'..'z']) or (Key in ['0'..'9']) or (Key in [' ']) then
    addFiltro(TEdit(Sender).Text + Key)
  else if(Key = #8) then
    addFiltro(Copy(TEdit(Sender).Text,1,Length(TEdit(Sender).Text)-1));
end;

end.
