inherited FrmAtendimentoVendas: TFrmAtendimentoVendas
  Left = 366
  Top = 159
  Caption = 'Atendimento Vendas'
  ClientHeight = 668
  ClientWidth = 827
  OldCreateOrder = True
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 827
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 24
    Width = 569
    Height = 305
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Dados do Associado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    DesignSize = (
      569
      305)
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 140
      Height = 19
      Caption = 'N'#250'mero do Cart'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 155
      Width = 195
      Height = 19
      Caption = 'Raz'#227'o Social da Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 203
      Width = 166
      Height = 19
      Caption = 'Nome do Conveniado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 247
      Width = 124
      Height = 19
      Caption = 'Nome do Cart'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 8
      Top = 88
      Width = 33
      Height = 19
      Caption = 'CPF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtCodCartImp: TMaskEdit
      Left = 8
      Top = 36
      Width = 221
      Height = 32
      EditMask = '9999 9999 9999 9999;0; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 19
      ParentFont = False
      TabOrder = 3
    end
    object dbLkpEmp: TDBLookupComboBox
      Left = 8
      Top = 176
      Width = 554
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      KeyField = 'EMPRES_ID'
      ListField = 'NOME'
      ListSource = dsEmpresas
      ParentFont = False
      TabOrder = 5
    end
    object dbLkpConv: TDBLookupComboBox
      Left = 8
      Top = 224
      Width = 556
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      KeyField = 'CONV_ID'
      ListField = 'TITULAR'
      ListSource = dsConveniados
      ParentFont = False
      TabOrder = 6
    end
    object btnPesquisar: TBitBtn
      Left = 235
      Top = 35
      Width = 89
      Height = 33
      Caption = '&Pesquisar'
      TabOrder = 0
      OnClick = btnPesquisarClick
    end
    object dbLkpCart: TDBLookupComboBox
      Left = 8
      Top = 268
      Width = 556
      Height = 27
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      KeyField = 'CODCARTIMP'
      ListField = 'NOME'
      ListSource = dsCartoes
      ParentFont = False
      TabOrder = 7
      OnKeyDown = dbLkpCartKeyDown
    end
    object btnMov: TBitBtn
      Left = 325
      Top = 35
      Width = 89
      Height = 33
      Caption = '&Movimentos'
      TabOrder = 1
      OnClick = btnMovClick
    end
    object btnNovo: TBitBtn
      Left = 419
      Top = 35
      Width = 89
      Height = 33
      Caption = '&Nova Consulta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btnNovoClick
    end
    object txtCPF: TMaskEdit
      Left = 8
      Top = 108
      Width = 219
      Height = 32
      EditMask = '999.999.999-99;0; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 14
      ParentFont = False
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 571
    Top = 24
    Width = 254
    Height = 305
    Anchors = [akTop, akRight]
    Caption = 'Status para Movimenta'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label4: TLabel
      Left = 9
      Top = 16
      Width = 110
      Height = 19
      Caption = 'Empresa.........:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblEmpLib: TDBText
      Left = 120
      Top = 16
      Width = 129
      Height = 19
      DataField = 'LIBERADA'
      DataSource = dsEmpresas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 9
      Top = 82
      Width = 109
      Height = 19
      Caption = 'Cart'#227'o.............:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCartLib: TDBText
      Left = 120
      Top = 82
      Width = 129
      Height = 19
      DataField = 'LIBERADO'
      DataSource = dsCartoes
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 9
      Top = 48
      Width = 111
      Height = 19
      Caption = 'Conveniado...:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblConvLib: TDBText
      Left = 120
      Top = 48
      Width = 129
      Height = 19
      DataField = 'LIBERADO'
      DataSource = dsConveniados
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblStatus: TLabel
      Left = 2
      Top = 208
      Width = 250
      Height = 95
      Align = alBottom
      Alignment = taCenter
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
    object Label11: TLabel
      Left = 9
      Top = 114
      Width = 109
      Height = 19
      Caption = 'Aceita Parcel.:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblParcel: TDBText
      Left = 119
      Top = 114
      Width = 130
      Height = 19
      DataField = 'aceita_parc'
      DataSource = dsEmpresas
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnlLimites: TPanel [3]
    Left = 0
    Top = 336
    Width = 827
    Height = 185
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 3
    object gbLimite1: TGroupBox
      Left = 0
      Top = 0
      Width = 124
      Height = 185
      Align = alLeft
      Caption = 'Limite/M'#234's 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        124
        185)
      object Label8: TLabel
        Left = 8
        Top = 16
        Width = 40
        Height = 16
        Caption = 'Limite'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 8
        Top = 136
        Width = 96
        Height = 16
        Caption = 'Saldo Restante'
        FocusControl = DBEdit2
      end
      object Label10: TLabel
        Left = 8
        Top = 75
        Width = 59
        Height = 16
        Caption = 'Consumo'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 32
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'limite_1'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 152
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_restante'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 91
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'consumo_mes_1'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
    object gbLimite2: TGroupBox
      Left = 124
      Top = 0
      Width = 124
      Height = 185
      Align = alLeft
      Caption = 'Limite/M'#234's 2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        124
        185)
      object Label12: TLabel
        Left = 8
        Top = 16
        Width = 40
        Height = 16
        Caption = 'Limite'
        FocusControl = DBEdit5
      end
      object Label13: TLabel
        Left = 8
        Top = 136
        Width = 96
        Height = 16
        Caption = 'Saldo Restante'
        FocusControl = DBEdit6
      end
      object Label14: TLabel
        Left = 8
        Top = 75
        Width = 59
        Height = 16
        Caption = 'Consumo'
        FocusControl = DBEdit7
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 32
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'limite_2'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 8
        Top = 152
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_restante2'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 91
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'consumo_mes_2'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
    object gbLimite3: TGroupBox
      Left = 248
      Top = 0
      Width = 124
      Height = 185
      Align = alLeft
      Caption = 'Limite/M'#234's 3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      DesignSize = (
        124
        185)
      object Label16: TLabel
        Left = 8
        Top = 16
        Width = 40
        Height = 16
        Caption = 'Limite'
        FocusControl = DBEdit9
      end
      object Label17: TLabel
        Left = 8
        Top = 136
        Width = 96
        Height = 16
        Caption = 'Saldo Restante'
        FocusControl = DBEdit10
      end
      object Label18: TLabel
        Left = 8
        Top = 75
        Width = 59
        Height = 16
        Caption = 'Consumo'
        FocusControl = DBEdit11
      end
      object DBEdit9: TDBEdit
        Left = 8
        Top = 32
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'limite_3'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit10: TDBEdit
        Left = 8
        Top = 152
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_restante3'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit11: TDBEdit
        Left = 8
        Top = 91
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'consumo_mes_3'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
    object gbLimite4: TGroupBox
      Left = 372
      Top = 0
      Width = 124
      Height = 185
      Align = alLeft
      Caption = 'Alimenta'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      DesignSize = (
        124
        185)
      object Label20: TLabel
        Left = 8
        Top = 16
        Width = 40
        Height = 16
        Caption = 'Limite'
        FocusControl = DBEdit13
      end
      object Label21: TLabel
        Left = 8
        Top = 136
        Width = 96
        Height = 16
        Caption = 'Saldo Restante'
        FocusControl = DBEdit14
      end
      object Label22: TLabel
        Left = 8
        Top = 56
        Width = 72
        Height = 16
        Caption = 'Acumulado'
        FocusControl = DBEdit15
      end
      object Label23: TLabel
        Left = 8
        Top = 96
        Width = 59
        Height = 16
        Caption = 'Consumo'
        FocusControl = DBEdit16
      end
      object DBEdit13: TDBEdit
        Left = 8
        Top = 32
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'limite_alimentacao'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit14: TDBEdit
        Left = 8
        Top = 152
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_rest_alimentacao'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
      end
      object DBEdit15: TDBEdit
        Left = 8
        Top = 72
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_acumulado'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit16: TDBEdit
        Left = 7
        Top = 112
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_usado_alimentacao'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
    end
    object gbLimite5: TGroupBox
      Left = 496
      Top = 0
      Width = 124
      Height = 185
      Align = alLeft
      Caption = 'Total Geral'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      DesignSize = (
        124
        185)
      object Label24: TLabel
        Left = 8
        Top = 16
        Width = 40
        Height = 16
        Caption = 'Limite'
        FocusControl = DBEdit17
      end
      object Label25: TLabel
        Left = 8
        Top = 136
        Width = 96
        Height = 16
        Caption = 'Saldo Restante'
        FocusControl = DBEdit18
      end
      object Label26: TLabel
        Left = 8
        Top = 75
        Width = 80
        Height = 16
        Caption = 'Saldo Usado'
        FocusControl = DBEdit19
      end
      object DBEdit17: TDBEdit
        Left = 8
        Top = 32
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'limite_total'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit18: TDBEdit
        Left = 8
        Top = 152
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_rest_total'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit19: TDBEdit
        Left = 8
        Top = 91
        Width = 109
        Height = 25
        Anchors = [akLeft, akTop, akRight]
        Ctl3D = False
        DataField = 'saldo_usado_total'
        DataSource = dsSituacaoCartoes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object pg: TPageControl [4]
    Left = 0
    Top = 528
    Width = 827
    Height = 140
    ActivePage = tsDescLimites
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 4
    object tsDescLimites: TTabSheet
      Caption = 'Descri'#231#227'o Limites'
      object gbSaldosAssociados: TGroupBox
        Left = 0
        Top = 0
        Width = 819
        Height = 108
        Align = alClient
        Caption = 'Saldo Associado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object pnlDescLimites: TPanel
          Left = 2
          Top = 16
          Width = 815
          Height = 90
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object dbLblDescLim2: TDBText
            Left = 0
            Top = 25
            Width = 815
            Height = 25
            Align = alTop
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'desc_lim_2'
            DataSource = dsSituacaoCartoes
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object dbLblDescLim3: TDBText
            Left = 0
            Top = 50
            Width = 815
            Height = 29
            Align = alTop
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'desc_lim_3'
            DataSource = dsSituacaoCartoes
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object dbLblDescLim4: TDBText
            Left = 0
            Top = 79
            Width = 815
            Height = 31
            Align = alTop
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'desc_lim_4'
            DataSource = dsSituacaoCartoes
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
            Visible = False
          end
          object dbLblRenovSaldos: TDBText
            Left = 0
            Top = 67
            Width = 815
            Height = 23
            Align = alBottom
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'Fechamento'
            DataSource = dsSituacaoCartoes
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object dbLblDescLim1: TDBText
            Left = 0
            Top = 0
            Width = 815
            Height = 25
            Align = alTop
            Alignment = taCenter
            Color = clBtnFace
            DataField = 'desc_lim_1'
            DataSource = dsSituacaoCartoes
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
        end
      end
    end
    object tsMovimentacao: TTabSheet
      Caption = 'Movimenta'#231#227'o'
      ImageIndex = 1
      OnShow = tsMovimentacaoShow
      object pnlHeadMov: TPanel
        Left = 0
        Top = 0
        Width = 819
        Height = 36
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          819
          36)
        object lblTotalDesc: TLabel
          Left = 296
          Top = 0
          Width = 43
          Height = 36
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          Caption = 'Total: '
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lblMesAberto: TLabel
          Left = 0
          Top = 0
          Width = 154
          Height = 36
          Align = alLeft
          Alignment = taCenter
          Caption = 'Movimentos em Aberto de: '
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lblNomeConv: TLabel
          Left = 154
          Top = 0
          Width = 142
          Height = 36
          Align = alLeft
          Anchors = [akLeft, akTop, akRight]
          Caption = 'Nome Conveniado'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object lblTotal: TLabel
          Left = 339
          Top = 0
          Width = 96
          Height = 36
          Align = alLeft
          AutoSize = False
          Caption = 'Total: '
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
        end
        object btnAnt: TBitBtn
          Left = 588
          Top = 0
          Width = 113
          Height = 36
          Anchors = [akTop, akRight]
          Caption = 'Movimentos'#13#10'Anteriores'
          TabOrder = 1
          OnClick = btnAntClick
        end
        object btnMovAtu: TBitBtn
          Left = 693
          Top = 0
          Width = 113
          Height = 36
          Anchors = [akTop, akRight]
          Caption = 'Movimentos'#13#10'Atuais'
          TabOrder = 2
          OnClick = btnMovAtuClick
        end
        object btnFecfharAbaMovimentos: TBitBtn
          Left = 501
          Top = 0
          Width = 89
          Height = 36
          Anchors = [akTop, akRight]
          Caption = '&Fechar'
          TabOrder = 0
          OnClick = btnFecfharAbaMovimentosClick
        end
      end
      object dbGridMov: TJvDBGrid
        Left = 0
        Top = 36
        Width = 819
        Height = 72
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsMovimentacao
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        OnDrawColumnCell = dbGridMovDrawColumnCell
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 18
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'autorizacao_id'
            Width = 79
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'digito'
            Title.Caption = 'Dig'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'trans_id'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'hora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'debito'
            Title.Caption = 'Debito'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'credito'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'credenciado'
            Width = 284
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'data_fecha_emp'
            Title.Caption = 'Data Fechamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nf'
            Title.Caption = 'Num. NF'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'receita'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'entreg_nf'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'historico'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'operador'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'valor_cancelado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'formapagto_id'
            Title.Caption = 'Forma de Pagamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'fatura_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'datavenda'
            Width = 130
            Visible = True
          end>
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 108
    Top = 208
  end
  object dsSituacaoCartoes: TDataSource
    DataSet = qSituacaoCartao
    OnDataChange = dsSituacaoCartoesDataChange
    Left = 148
    Top = 216
  end
  object dsEmpresas: TDataSource
    DataSet = qEmpresas
    OnDataChange = dsEmpresasDataChange
    Left = 204
    Top = 216
  end
  object dsConveniados: TDataSource
    DataSet = qConveniados
    OnDataChange = dsConveniadosDataChange
    Left = 260
    Top = 216
  end
  object dsCartoes: TDataSource
    DataSet = qCartoes
    OnDataChange = dsCartoesDataChange
    Left = 316
    Top = 216
  end
  object dsDiaFecha: TDataSource
    DataSet = qDiaFecha
    Left = 372
    Top = 216
  end
  object dsMovimentacao: TDataSource
    DataSet = qMovimentacao
    Left = 436
    Top = 216
  end
  object qEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select'
      
        '  e.empres_id,e.aceita_parc, e.nome, iif(liberada = '#39'S'#39','#39'LIBERAD' +
        'O'#39','#39'BLOQUEADO'#39') liberada,band_id'
      'from empresas e,CONVENIADOS c'
      'where e.EMPRES_ID = c.EMPRES_ID'
      'and c.CONV_ID = :conv_id')
    Left = 200
    Top = 184
    object qEmpresasempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEmpresasliberada: TStringField
      FieldName = 'liberada'
      ReadOnly = True
      Size = 9
    end
    object qEmpresasband_id: TIntegerField
      FieldName = 'band_id'
    end
    object qEmpresasaceita_parc: TStringField
      FieldName = 'aceita_parc'
      FixedChar = True
      Size = 1
    end
  end
  object qConveniados: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeOpen = qConveniadosBeforeOpen
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      
        '  conv_id, titular, iif(liberado = '#39'S'#39','#39'LIBERADO'#39','#39'BLOQUEADO'#39') l' +
        'iberado,cpf CPF'
      'from conveniados'
      'where apagado <> '#39'S'#39
      'and empres_id = :empres_id'
      'order by titular')
    Left = 256
    Top = 184
    object qConveniadosconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qConveniadostitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object qConveniadosliberado: TStringField
      FieldName = 'liberado'
      ReadOnly = True
      Size = 9
    end
    object qConveniadosCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
  end
  object qCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeOpen = qCartoesBeforeOpen
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select'
      
        '  codcartimp, nome, iif(liberado = '#39'S'#39','#39'LIBERADO'#39','#39'BLOQUEADO'#39') l' +
        'iberado'
      'from cartoes'
      'where apagado <> '#39'S'#39
      'and conv_id = :conv_id'
      'order by nome')
    Left = 312
    Top = 184
    object qCartoescodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object qCartoesnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object qCartoesliberado: TStringField
      FieldName = 'liberado'
      ReadOnly = True
      Size = 9
    end
  end
  object qDiaFecha: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select'
      '  month (data_fecha) mes,'
      '  year (data_fecha) ano,'
      '  data_fecha, dia_fecha.data_venc'
      'from '
      '  dia_fecha'
      'where'
      '  empres_id = :empres_id')
    Left = 368
    Top = 184
    object qDiaFechames: TIntegerField
      FieldName = 'mes'
      ReadOnly = True
    end
    object qDiaFechaano: TIntegerField
      FieldName = 'ano'
      ReadOnly = True
    end
    object qDiaFechadata_fecha: TDateTimeField
      FieldName = 'data_fecha'
    end
    object qDiaFechadata_venc: TDateTimeField
      FieldName = 'data_venc'
    end
  end
  object qMovimentacao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'data_fecha'
        DataType = ftDateTime
        Size = 10
        Value = 0d
      end>
    SQL.Strings = (
      'select '
      '  cc.data,'
      '  cc.autorizacao_id,'
      '  cc.digito,'
      '  cc.trans_id,'
      '  cc.hora,'
      '  cc.debito,'
      '  cc.credito,'
      '  cred.nome as credenciado,'
      '  cc.data_fecha_emp,'
      '  cc.nf,'
      '  cc.receita,'
      '  cc.entreg_nf,'
      '  cc.historico,'
      '  cc.operador,'
      '  cc.valor_cancelado,'
      '  cc.formapagto_id,'
      '  cc.fatura_id,'
      '  cc.datavenda,'
      '  cc.cancelada,'
      
        '  (SELECT sum(contaC.debito - contaC.credito) FROM contacorrente' +
        ' contaC'
      
        '     where contaC.conv_id = cc.conv_id and contaC.baixa_convenia' +
        'do <> '#39'S'#39
      '     and contaC.data_fecha_emp = cc.data_fecha_emp) AS TOTAL'
      'from contacorrente  cc'
      'join credenciados cred on cred.cred_id = cc.cred_id'
      'where cc.conv_id = :conv_id'
      'and cc.baixa_conveniado <> '#39'S'#39
      'and cc.data_fecha_emp = :data_fecha'
      'order by cc.data desc, cc.hora desc')
    Left = 432
    Top = 184
    object qMovimentacaodata: TDateTimeField
      FieldName = 'data'
    end
    object qMovimentacaoautorizacao_id: TIntegerField
      FieldName = 'autorizacao_id'
    end
    object qMovimentacaodigito: TWordField
      FieldName = 'digito'
    end
    object qMovimentacaotrans_id: TIntegerField
      FieldName = 'trans_id'
    end
    object qMovimentacaohora: TStringField
      FieldName = 'hora'
      FixedChar = True
      Size = 8
    end
    object qMovimentacaodebito: TBCDField
      FieldName = 'debito'
      Precision = 15
      Size = 2
    end
    object qMovimentacaocredito: TBCDField
      FieldName = 'credito'
      Precision = 15
      Size = 2
    end
    object qMovimentacaocredenciado: TStringField
      FieldName = 'credenciado'
      Size = 60
    end
    object qMovimentacaodata_fecha_emp: TDateTimeField
      FieldName = 'data_fecha_emp'
    end
    object qMovimentacaonf: TIntegerField
      FieldName = 'nf'
    end
    object qMovimentacaoreceita: TStringField
      FieldName = 'receita'
      FixedChar = True
      Size = 1
    end
    object qMovimentacaoentreg_nf: TStringField
      FieldName = 'entreg_nf'
      FixedChar = True
      Size = 1
    end
    object qMovimentacaohistorico: TStringField
      FieldName = 'historico'
      Size = 80
    end
    object qMovimentacaooperador: TStringField
      FieldName = 'operador'
      Size = 25
    end
    object qMovimentacaovalor_cancelado: TBCDField
      FieldName = 'valor_cancelado'
      Precision = 15
      Size = 2
    end
    object qMovimentacaoformapagto_id: TIntegerField
      FieldName = 'formapagto_id'
    end
    object qMovimentacaofatura_id: TIntegerField
      FieldName = 'fatura_id'
    end
    object qMovimentacaodatavenda: TDateTimeField
      FieldName = 'datavenda'
    end
    object qMovimentacaocancelada: TStringField
      FieldName = 'cancelada'
      FixedChar = True
      Size = 1
    end
    object qMovimentacaoTOTAL: TBCDField
      FieldName = 'TOTAL'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object Q1: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 488
    Top = 184
  end
  object qSituacaoCartao: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeOpen = qSituacaoCartaoBeforeOpen
    AfterPost = qSituacaoCartaoAfterPost
    OnCalcFields = qSituacaoCartaoCalcFields
    Parameters = <
      item
        Name = 'empres_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'conv_id_1'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'conv_id_3'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT      c.conv_id,'
      #9'   c.titular,'
      '                   c.cpf,'
      '                 0.00 as saldo_usado_alimentacao,'
      '                 0.00 as limite_alimentacao,'
      '                 0.00 as saldo_rest_alimentacao,  '
      
        #9'   case when coalesce(c.liberado, '#39'S'#39') = '#39'S'#39' then '#39'SIM'#39' else '#39'N' +
        #195'O'#39' end as liberado,'
      #9'   c.limite_mes AS limite_1,'
      #9'   c.consumo_mes_1 as consumo_mes_1,'
      #9'   coalesce(saldo_acumulado,0) as saldo_acumulado,'
      #9'   coalesce(abono_mes,0) as abono_mes,'
      '                 cd.data_demissao as demissao,'
      '                 ca.codcartimp as CODCARTIMP,'
      '                 ca.nome AS NOME_CARTAO,'
      '                 ca.codigo as COD_CARTAO,'
      
        '                 case when coalesce(ca.liberado, '#39'S'#39') = '#39'S'#39' then' +
        ' '#39'SIM'#39' else '#39'N'#195'O'#39' end AS CART_LIB,'
      '                 e.empres_id,'
      '                 e.nome as nome_empres, '
      
        '                 case when coalesce(e.liberada, '#39'S'#39')= '#39'S'#39' then '#39 +
        'SIM'#39' else '#39'N'#195'O'#39' END as emp_lib,'
      
        '                 case when coalesce(e.todos_segmentos, '#39'S'#39') = '#39'S' +
        #39' then '#39'SIM'#39' else '#39'N'#195'O'#39' end as todos_segmentos,'
      
        '                 case when coalesce(e.aceita_parc, '#39'S'#39') = '#39'S'#39' th' +
        'en '#39'SIM'#39' else '#39'N'#195'O'#39' end as aceita_parc,'
      
        '                 case when coalesce(e.venda_nome, '#39'S'#39') = '#39'S'#39' the' +
        'n '#39'SIM'#39' else '#39'N'#195'O'#39' end as venda_nome,'
      
        '                (CONCAT(iif((select b.band_id from bandeiras b w' +
        'here band_id = 999) <> 999 ,'#39'RENOVA'#199#195'O DOS LIMITES EM: '#39','#39'RENOVA' +
        #199#195'O DO LIMITE EM: '#39'),'
      
        '                (SELECT top 1 FORMAT(data_fecha,'#39'dd-MM-yyyy'#39') fr' +
        'om dia_fecha WHERE data_fecha > CURRENT_TIMESTAMP and'
      
        '                EMPRES_ID = (SELECT conveniados.EMPRES_ID from c' +
        'onveniados where conv_id = 119199)))) as FECHAMENTO,'
      '                (CONCAT('#39'SALDO DISPON'#205'VEL PARA '#39', '
      
        #9#9#9' iif((select b.band_id from bandeiras b where band_id = 0) <>' +
        ' 999, b.desc_limite_1, b.descricao),'
      #9#9#9' '#39' '#201' DE '#39',  (select * from getSaldoRest(0,0)) '
      #9#9#9' '
      #9#9'   )'
      #9'   ) AS desc_lim_1,'
      '                 (CONCAT('#39'SALDO DISPON'#205'VEL PARA '#39', '
      
        #9#9#9' iif((select b.band_id from bandeiras b where band_id = 0) <>' +
        ' 999, b.desc_limite_2, b.descricao),'
      #9#9#9' '#39' '#201' DE '#39',  (select * from getSaldoRest(0,0)) '
      #9#9#9' '
      #9#9'   )'
      #9'   ) AS desc_lim_2,'
      '                 (CONCAT('#39'SALDO DISPON'#205'VEL PARA '#39', '
      
        #9#9#9' iif((select b.band_id from bandeiras b where band_id = 0) <>' +
        ' 999, b.desc_limite_3, b.descricao),'
      #9#9#9' '#39' '#201' DE '#39',  (select * from getSaldoRest(0,0)) '
      #9#9#9' '
      #9#9'   )'
      #9'   ) AS desc_lim_3,'
      '                 (CONCAT('#39'SALDO DISPON'#205'VEL PARA '#39', '
      
        #9#9#9' iif((select b.band_id from bandeiras b where band_id = 0) <>' +
        ' 999, b.desc_limite_4, b.descricao),'
      #9#9#9' '#39' '#201' DE '#39',  (select * from getSaldoRest(0,0)) '
      #9#9#9' '
      #9#9'   )'
      #9'   ) AS desc_lim_4,   '
      
        #9'   (SELECT conv.limite_mes + CASE WHEN e.tipo_credito in(2,3) T' +
        'HEN'
      #9#9#9'coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) '
      
        #9#9#9'else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = '#39'N'#39' TH' +
        'EN 0'
      #9#9#9'else 0 END END - CONSUMO_MES'
      
        #9#9#9'from conveniados conv join empresas e on e.EMPRES_ID = :empre' +
        's_id AND CONV.CONV_ID = :conv_id_1) AS'
      #9#9#9'saldo_restante,'
      
        '               0.0 AS limite_2, 0.0 as limite_3 , 0.0 as consumo' +
        '_mes_2,0.0 as consumo_mes_3,0.0 as saldo_restante2, 0.0 as saldo' +
        '_restante3'
      
        '               FROM Conveniados c, CARTOES ca, empresas e, conv_' +
        'detail cd, bandeiras b'
      'WHERE c.conv_id = :conv_id_3')
    Left = 152
    Top = 184
    object qSituacaoCartaoconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qSituacaoCartaotitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object qSituacaoCartaosaldo_usado_alimentacao: TBCDField
      FieldName = 'saldo_usado_alimentacao'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qSituacaoCartaolimite_alimentacao: TBCDField
      FieldName = 'limite_alimentacao'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qSituacaoCartaosaldo_rest_alimentacao: TBCDField
      FieldName = 'saldo_rest_alimentacao'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qSituacaoCartaoliberado: TStringField
      FieldName = 'liberado'
      ReadOnly = True
      Size = 3
    end
    object qSituacaoCartaolimite_1: TBCDField
      FieldName = 'limite_1'
      DisplayFormat = '#,##0.00'
      Precision = 15
      Size = 2
    end
    object qSituacaoCartaoconsumo_mes_1: TBCDField
      FieldName = 'consumo_mes_1'
      DisplayFormat = '#,##0.00'
      Precision = 15
      Size = 2
    end
    object qSituacaoCartaosaldo_acumulado: TBCDField
      FieldName = 'saldo_acumulado'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 15
      Size = 2
    end
    object qSituacaoCartaoabono_mes: TBCDField
      FieldName = 'abono_mes'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 15
      Size = 2
    end
    object qSituacaoCartaoCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object qSituacaoCartaoNOME_CARTAO: TStringField
      FieldName = 'NOME_CARTAO'
      Size = 58
    end
    object qSituacaoCartaoCOD_CARTAO: TIntegerField
      FieldName = 'COD_CARTAO'
    end
    object qSituacaoCartaoCART_LIB: TStringField
      FieldName = 'CART_LIB'
      ReadOnly = True
      Size = 3
    end
    object qSituacaoCartaoempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qSituacaoCartaonome_empres: TStringField
      FieldName = 'nome_empres'
      Size = 60
    end
    object qSituacaoCartaoemp_lib: TStringField
      FieldName = 'emp_lib'
      ReadOnly = True
      Size = 3
    end
    object qSituacaoCartaoaceita_parc: TStringField
      FieldName = 'aceita_parc'
      ReadOnly = True
      Size = 3
    end
    object qSituacaoCartaovenda_nome: TStringField
      FieldName = 'venda_nome'
      ReadOnly = True
      Size = 3
    end
    object qSituacaoCartaosaldo_restante: TBCDField
      FieldName = 'saldo_restante'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qSituacaoCartaolimite_2: TBCDField
      FieldName = 'limite_2'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaolimite_3: TBCDField
      FieldName = 'limite_3'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaoconsumo_mes_2: TBCDField
      FieldName = 'consumo_mes_2'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaosaldo_restante2: TBCDField
      FieldName = 'saldo_restante2'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaoconsumo_mes_3: TBCDField
      FieldName = 'consumo_mes_3'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaosaldo_restante3: TBCDField
      FieldName = 'saldo_restante3'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 1
      Size = 1
    end
    object qSituacaoCartaoFECHAMENTO: TWideStringField
      FieldName = 'FECHAMENTO'
      ReadOnly = True
      Size = 4000
    end
    object qSituacaoCartaodesc_lim_1: TStringField
      FieldName = 'desc_lim_1'
      ReadOnly = True
      Size = 129
    end
    object qSituacaoCartaodesc_lim_2: TStringField
      FieldName = 'desc_lim_2'
      ReadOnly = True
      Size = 129
    end
    object qSituacaoCartaodesc_lim_3: TStringField
      FieldName = 'desc_lim_3'
      ReadOnly = True
      Size = 129
    end
    object qSituacaoCartaodesc_lim_4: TStringField
      FieldName = 'desc_lim_4'
      ReadOnly = True
      Size = 129
    end
    object qSituacaoCartaolimite_total: TFloatField
      FieldKind = fkCalculated
      FieldName = 'limite_total'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qSituacaoCartaosaldo_usado_total: TFloatField
      FieldKind = fkCalculated
      FieldName = 'saldo_usado_total'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qSituacaoCartaosaldo_rest_total: TFloatField
      FieldKind = fkCalculated
      FieldName = 'saldo_rest_total'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
  end
end
