unit ULancamentosFatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, JvExStdCtrls, JvEdit,
  JvValidateEdit, ExtCtrls, DB, ADODB, JvExControls, JvDBLookup, JvExMask,
  JvToolEdit, JvMaskEdit, JvDBControls;

type
  TFLancamentosFatura = class(TForm)
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    Panel1: TPanel;
    Label4: TLabel;
    txtEmp: TLabel;
    Label6: TLabel;
    txtData: TLabel;
    Label7: TLabel;
    txtValTot: TLabel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    btnCancela: TBitBtn;
    LabAguarde: TLabel;
    cbBanco: TJvDBLookupCombo;
    cbFormaPgto: TJvDBLookupCombo;
    QFormaPgto: TADOQuery;
    DSBancos: TDataSource;
    DSFormaPgto: TDataSource;
    QBanco: TADOQuery;
    QBancoCODIGO: TIntegerField;
    QBancoBANCO: TStringField;
    QBancoAPAGADO: TStringField;
    QBancoLAYOUT: TMemoField;
    QBancoDTAPAGADO: TDateTimeField;
    QBancoDTALTERACAO: TDateTimeField;
    QBancoOPERADOR: TStringField;
    QBancoDTCADASTRO: TDateTimeField;
    QBancoOPERCADASTRO: TStringField;
    QBancoDESC_TAXA: TStringField;
    QFormaPgtoFORMA_ID: TIntegerField;
    QFormaPgtoLIBERADO: TStringField;
    QFormaPgtoDESCRICAO: TStringField;
    Label3: TLabel;
    txtObs: TMemo;
    Label5: TLabel;
    txtDataBaixaFatura: TMaskEdit;
    Label8: TLabel;
    txtValorPago: TEdit;
    procedure FormShow(Sender: TObject);
    procedure txtValorPagoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLancamentosFatura: TFLancamentosFatura;

implementation

{$R *.dfm}

procedure TFLancamentosFatura.FormShow(Sender: TObject);
begin
  QBanco.Open;
  QFormaPgto.Open;
end;

procedure TFLancamentosFatura.txtValorPagoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = '.' then Key := ',';
end;

end.
