unit UCadEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,}
  StdCtrls, Buttons, DBCtrls, ExtCtrls, ComCtrls, Mask, {ZAbstractRODataset,
  ZAbstractDataset, ZDataset,} {JvMemDS,} JvToolEdit, Menus, dateutils, UClassLog,
  ToolEdit, {JvDBComb,} {JvLookup,} XMLDoc, IdHTTP, JvMemoryDataset,
  JvExControls, JvDBLookup, JvExStdCtrls, JvCombobox, JvDBCombobox,
  JvExMask, JvExDBGrids, JvDBGrid, DBClient, RXDBCtrl, ADODB, JvSpeedButton,
  JvExButtons, JvBitBtn, JvEdit, JvValidateEdit, JvMaskEdit, JvDBControls,
  JvBaseDlg, JvFindFiles, RpDefine, RpRender, RpRenderPDF, JvDialogs,
  OleCtnrs, JvgLabel,MaskUtils, JvADOQuery, JvDataSource, ComObj;

type
  TSaldoRenovacao = (srValor, srPerSalario, srPerLimite, srNone);

  TFCadEmp = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    dbEdtNm: TDBEdit;
    Label4: TLabel;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    GroupBox3: TGroupBox;
    GroupBox6: TGroupBox;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    Label24: TLabel;
    dbEdtEmail: TDBEdit;
    Label25: TLabel;
    DBEdit23: TDBEdit;
    Label26: TLabel;
    DBEdit24: TDBEdit;
    Label27: TLabel;
    DBEdit25: TDBEdit;
    Label28: TLabel;
    DBEdit26: TDBEdit;
    ButLimpaSenha: TBitBtn;
    TabSeg: TTabSheet;
    DSSegLib: TDataSource;
    SegLib: TJvMemoryData;
    SegLibSeg_ID: TIntegerField;
    SegLibLiberado: TStringField;
    SegLibDescricao: TStringField;
    SegLibempres_id: TIntegerField;
    Tabdatas: TTabSheet;
    DSDatasFecha: TDataSource;
    Label32: TLabel;
    EdCidade: TEdit;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    Label23: TLabel;
    Label33: TLabel;
    DBEdit20: TDBEdit;
    TabSaldo: TTabSheet;
    DSSaldos: TDataSource;
    TabCartEmp: TTabSheet;
    GroupBox4: TGroupBox;
    Label37: TLabel;
    DBCheckBox7: TDBCheckBox;
    DBCart_Ini: TDBEdit;
    Label36: TLabel;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel7: TPanel;
    Label34: TLabel;
    CBAno: TComboBox;
    Griddatas: TJvDBGrid;
    Panel5: TPanel;
    Label43: TLabel;
    Label41: TLabel;
    ProgressDatas: TProgressBar;
    TabGrupos: TTabSheet;
    DSQGrupo_conv_emp: TDataSource;
    DBCheckBox10: TDBCheckBox;
    PopupEmpres_ID: TPopupMenu;
    AlterarIddaempresa1: TMenuItem;
    TabGrupoProd: TTabSheet;
    DSGrupo_Prod: TDataSource;
    Label49: TLabel;
    TabVendaNome: TTabSheet;
    GridVendaNome: TJvDBGrid;
    Panel14: TPanel;
    DSVendaNome: TDataSource;
    Panel15: TPanel;
    btnGravaVendaNome: TButton;
    btnCancelVendaNome: TButton;
    DBNavigator5: TDBNavigator;
    TabCredLib: TTabSheet;
    DSCredLib: TDataSource;
    grpProgDesc: TGroupBox;
    DBCheckBox12: TDBCheckBox;
    dsProdBloq: TDataSource;
    Panel19: TPanel;
    PageControl4: TPageControl;
    TabConfigGrupo: TTabSheet;
    TabConfigPrograma: TTabSheet;
    Panel20: TPanel;
    Panel21: TPanel;
    dsPrograma: TDataSource;
    cbbCredInd: TJvDBLookupCombo;
    Label52: TLabel;
    DBEdit34: TDBEdit;
    Label53: TLabel;
    EdCredInd: TEdit;
    dsListaCred: TDataSource;
    Label5: TLabel;
    Label6: TLabel;
    Label19: TLabel;
    DBEdit4: TDBEdit;
    DBEdit17: TDBEdit;
    Label21: TLabel;
    Label22: TLabel;
    lblModalidade: TLabel;
    dbEdtDiaFechamento: TDBEdit;
    dbEdtDiaVenc: TDBEdit;
    cbbModalidade: TJvDBComboBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    Label17: TLabel;
    Label18: TLabel;
    Label35: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit21: TDBEdit;
    Label50: TLabel;
    DBEdit33: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    txtENDERECO: TDBEdit;
    txtCEP: TDBEdit;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label54: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Panel23: TPanel;
    Label40: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    ChVencnomes: TCheckBox;
    ChkUsa2Fecha: TCheckBox;
    DBEdFecha2: TDBEdit;
    DBEdVenc2: TDBEdit;
    EdMesesVenc: TEdit;
    ChVencnomes2: TCheckBox;
    EdMesesVenc2: TEdit;
    Label38: TLabel;
    Label39: TLabel;
    btnGravaDataFecha: TBitBtn;
    btnCancelDataFecha: TBitBtn;
    Panel24: TPanel;
    Panel18: TPanel;
    grdProdBloq: TJvDBGrid;
    Panel12: TPanel;
    btnExclProdBloq: TBitBtn;
    btnInclProdBloq: TBitBtn;
    Panel27: TPanel;
    Panel11: TPanel;
    GridGrupo_Prod: TJvDBGrid;
    Panel13: TPanel;
    btnGravaGrupProd: TBitBtn;
    btnCancelGrupProd: TBitBtn;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel22: TPanel;
    JvDBGrid1: TJvDBGrid;
    Panel25: TPanel;
    GridPbm: TJvDBGrid;
    Panel26: TPanel;
    btnGravaPbm: TBitBtn;
    btnCancelPbm: TBitBtn;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel30: TPanel;
    Panel6: TPanel;
    LabInfo: TLabel;
    GridSaldos: TDBGrid;
    Panel8: TPanel;
    btnVisualizaSaldo: TBitBtn;
    Panel31: TPanel;
    GridSeg: TJvDBGrid;
    PanelSeg: TPanel;
    btnGravaSeg: TBitBtn;
    btnCancelSeg: TBitBtn;
    Panel4: TPanel;
    Panel32: TPanel;
    Panel9: TPanel;
    Grid_Grupo: TJvDBGrid;
    Panel10: TPanel;
    Panel33: TPanel;
    Panel16: TPanel;
    GridCredLib: TJvDBGrid;
    Panel17: TPanel;
    btnGravaCredLib: TBitBtn;
    btnCancelCredLib: TBitBtn;
    btnAltDataFecha: TBitBtn;
    btnIncGrupEmp: TBitBtn;
    btnExclGrupEmp: TBitBtn;
    btnGravaGrupEmp: TBitBtn;
    btnCancGrupEmp: TBitBtn;
    Label29: TLabel;
    txtNUMERO: TDBEdit;
    DBCheckBox11: TDBCheckBox;
    PopupAltLinearGrupoProd: TPopupMenu;
    AlteraoLinearDescontoemGrupodeProdutos1: TMenuItem;
    AlteraoLineardeGrupodeProdutoLiberado1: TMenuItem;
    GroupBox2: TGroupBox;
    Label30: TLabel;
    JvDBLookupCombo1: TJvDBLookupCombo;
    daAgenciador: TDataSource;
    DBCheckBox14: TDBCheckBox;
    DBCheckBox13: TDBCheckBox;
    DBCheckBox15: TDBCheckBox;
    tabFormasPgto: TTabSheet;
    Panel34: TPanel;
    grdFormasPgto: TJvDBGrid;
    panFormasLib: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    btnGravaFormas: TBitBtn;
    btnCancelFormas: TBitBtn;
    dsFormasPgto: TDataSource;
    DBCheckBox16: TDBCheckBox;
    tsSenhaConv: TTabSheet;
    DSCred_Obriga_Senha: TDataSource;
    Panel39: TPanel;
    Panel37: TPanel;
    GridCredObrigarSenha: TJvDBGrid;
    Panel38: TPanel;
    btnGravaCredObrigarSenha: TBitBtn;
    btnCancelCredObrigarSenha: TBitBtn;
    Label31: TLabel;
    dbLkpCbBandeiras: TDBLookupComboBox;
    dsBandeiras: TDataSource;
    DBCheckBox17: TDBCheckBox;
    Label51: TLabel;
    cbbTipoCredito: TJvDBComboBox;
    dbEdtDiaRepasse: TDBEdit;
    Label55: TLabel;
    DBCheckBox18: TDBCheckBox;
    DBMemo1: TDBMemo;
    tsUsuariosWeb: TTabSheet;
    Panel40: TPanel;
    Panel41: TPanel;
    dbGridUsuWeb: TJvDBGrid;
    Panel42: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dsUsu_Web: TDataSource;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    tsAlimentacao: TTabSheet;
    btnGravarConvAlim: TBitBtn;
    btnCancelarConvAlim: TBitBtn;
    dbGridAlim: TJvDBGrid;
    Panel44: TPanel;
    dsCredAlim: TDataSource;
    Button1: TButton;
    edtSaldoRenovacao: TEdit;
    edtAbonoMes: TEdit;
    Label56: TLabel;
    Label57: TLabel;
    DBCheckBox19: TDBCheckBox;
    lkpCIDADE: TDBLookupComboBox;
    Label12: TLabel;
    dsCidades: TDataSource;
    dsEstados: TDataSource;
    lkpESTADO: TDBLookupComboBox;
    Label58: TLabel;
    dsOperadores: TDataSource;
    dbLkpRespFechamento: TJvDBLookupCombo;
    JvDBLookupCombo2: TJvDBLookupCombo;
    Label59: TLabel;
    dsModelosCartoes: TDataSource;
    Label60: TLabel;
    QPrograma: TADOQuery;
    QProgramaprod_id: TIntegerField;
    QProgramadescricao: TStringField;
    QProgramaprog_id: TIntegerField;
    QProgramacodbarras: TStringField;
    QProgramaprc_unit: TBCDField;
    QProgramaperc_desc: TBCDField;
    QProgramaqtd_max: TIntegerField;
    QProgramafabricante: TStringField;
    QProgramaobrig_receita: TStringField;
    qPbm: TADOQuery;
    QGrupo_conv_emp: TADOQuery;
    QGrupo_conv_empGRUPO_CONV_EMP_ID: TIntegerField;
    QGrupo_conv_empDESCRICAO: TStringField;
    QGrupo_conv_empEMPRES_ID: TIntegerField;
    QAtualizaSaldo: TADOQuery;
    qPbmprog_id: TIntegerField;
    qPbmnome: TStringField;
    qPbmparticipa: TStringField;
    QCredLib: TADOQuery;
    QCredLibcred_id: TIntegerField;
    QCredLibnome: TStringField;
    QCredLibliberado: TStringField;
    QSaldoEmp: TADOQuery;
    QDatasFecha: TADOQuery;
    qAgenciador: TADOQuery;
    qAgenciadoragenciador_id: TIntegerField;
    qAgenciadornome: TStringField;
    QGrupo_Prod: TADOQuery;
    //QCadastroEMPRES_ID: TIntegerField;
    qModelosCartoes: TADOQuery;
    qModelosCartoesMOD_CART_ID: TIntegerField;
    qModelosCartoesDESCRICAO: TStringField;
    qModelosCartoesOBSERVACAO: TStringField;
    QCredAlim: TADOQuery;
    QCred_Obriga_Senha: TADOQuery;
    QCred_Obriga_Senhacred_id: TIntegerField;
    QCred_Obriga_Senhanome: TStringField;
    QCred_Obriga_SenhaobrigaSenha: TStringField;
    qProdBloq: TADOQuery;
    qProdBloqprod_id: TIntegerField;
    qProdBloqdescricao: TStringField;
    qProdBloqcodbarras: TStringField;
    QVendaNome: TADOQuery;
    QVendaNomecred_id: TIntegerField;
    QVendaNomenome: TStringField;
    QVendaNomefantasia: TStringField;
    QVendaNomeliberado: TStringField;
    qFormasPgto: TADOQuery;
    qFormasPgtoforma_id: TIntegerField;
    qFormasPgtodescricao: TStringField;
    qFormasPgtoliberado: TStringField;
    qCidades: TADOQuery;
    qCidadesCID_ID: TIntegerField;
    qCidadesESTADO_ID: TIntegerField;
    qCidadesNOME: TStringField;
    QSegLib: TADOQuery;
    QSegLibliberado: TStringField;
    qUsu_Web: TADOQuery;
    qUsu_Webusu_id: TIntegerField;
    qUsu_Webusu_nome: TStringField;
    qUsu_Webusu_email: TStringField;
    qUsu_Webusu_senha: TStringField;
    qUsu_Webusu_liberado: TStringField;
    qUsu_Webusu_apagado: TStringField;
    qOperadores: TADOQuery;
    qOperadoresnome: TStringField;
    qEstados: TADOQuery;
    qEstadosESTADO_ID: TIntegerField;
    qEstadosUF: TStringField;
    QSegmentos: TADOQuery;
    QSegmentosseg_id: TIntegerField;
    QSegmentosdescricao2: TStringField;
    ADOQuery2: TADOQuery;
    qListaCred: TADOQuery;
    qListaCredcred_id: TIntegerField;
    qListaCrednome: TStringField;
    qListaCredfantasia: TStringField;
    QGrupo_ProdGRUPO_PROD_ID: TIntegerField;
    QGrupo_ProdDESCRICAO: TStringField;
    QGrupo_ProdLIBERADO: TStringField;
    QGrupo_ProdDESCONTO: TFloatField;
    QGrupo_ProdPRECO_FABRICA: TStringField;
    QGrupo_ProdREMP_GRUPO_PROD_ID: TIntegerField;
    QGrupo_ProdFIDELIDADE: TStringField;
    QGrupo_ProdFIDE_ID: TIntegerField;
    qUpdate: TADOQuery;
    qBandeiras: TADOQuery;
    qBandeirasBAND_ID: TIntegerField;
    qBandeirasDESCRICAO: TStringField;
    QDatasFechaEMPRES_ID: TIntegerField;
    QDatasFechaDATA_FECHA: TDateTimeField;
    QDatasFechaDATA_VENC: TDateTimeField;
    QDatasFechaDESC_FECHAMENTO: TStringField;
    QDatasFechaDESC_VENCIMENTO: TStringField;
    QSaldoEmpsaldo_conf: TBCDField;
    QSaldoEmpsaldo_nconf: TBCDField;
    QSaldoEmpsaldo_mes: TBCDField;
    QSaldoEmpdata_fecha_emp: TDateTimeField;
    QSaldoEmpdata_venc_emp: TDateTimeField;
    QSaldoEmpfatura_id: TIntegerField;
    QSaldoEmpdata_fatura: TDateTimeField;
    QSaldoEmptipo: TStringField;
    btnInserirPos: TBitBtn;
    qUsu_Webusu_cpf: TStringField;
    qUsu_Webemp_for_id: TIntegerField;
    TabSheet5: TTabSheet;
    Panel45: TPanel;
    Panel46: TPanel;
    DBGridEmpDptos: TJvDBGrid;
    Panel47: TPanel;
    BtnGravar: TBitBtn;
    BtnCancelar: TBitBtn;
    BtnApagar: TBitBtn;
    BtnInserir: TBitBtn;
    QEMP_DPTOS: TADOQuery;
    DSEmpDptos: TDataSource;
    QEMP_DPTOSDEPT_ID: TIntegerField;
    QEMP_DPTOSDESCRICAO: TStringField;
    QEMP_DPTOSEMPRES_ID: TIntegerField;
    qUsu_Webusu_tipo: TStringField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroFORMA_LIMITE_ID: TIntegerField;
    QCadastroTIPO_CARTAO_ID: TIntegerField;
    QCadastroCRED_ID: TIntegerField;
    QCadastroCONTRATO: TIntegerField;
    QCadastroFECHAMENTO1: TWordField;
    QCadastroFECHAMENTO2: TWordField;
    QCadastroVENCIMENTO1: TWordField;
    QCadastroVENCIMENTO2: TWordField;
    QCadastroINC_CART_PBM: TStringField;
    QCadastroPROG_DESC: TStringField;
    QCadastroNOME: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroLIBERADA: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroNOMECARTAO: TStringField;
    QCadastroCOMISSAO_CRED: TFloatField;
    QCadastroFATOR_RISCO: TFloatField;
    QCadastroSENHA: TStringField;
    QCadastroCGC: TStringField;
    QCadastroINSCRICAOEST: TStringField;
    QCadastroTELEFONE1: TStringField;
    QCadastroTELEFONE2: TStringField;
    QCadastroFAX: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroCEP: TStringField;
    QCadastroREPRESENTANTE: TStringField;
    QCadastroEMAIL: TStringField;
    QCadastroHOMEPAGE: TStringField;
    QCadastroOBS1: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroTODOS_SEGMENTOS: TStringField;
    QCadastroDT_DEBITO: TDateTimeField;
    QCadastroTAXA_BANCO: TStringField;
    QCadastroVALOR_TAXA: TBCDField;
    QCadastroTAXA_JUROS: TBCDField;
    QCadastroMULTA: TBCDField;
    QCadastroDESCONTO_FUNC: TBCDField;
    QCadastroREPASSE_EMPRESA: TBCDField;
    QCadastroBLOQ_ATE_PGTO: TStringField;
    QCadastroOBS3: TStringField;
    QCadastroPEDE_NF: TStringField;
    QCadastroPEDE_REC: TStringField;
    QCadastroACEITA_PARC: TStringField;
    QCadastroDESCONTO_EMP: TBCDField;
    QCadastroUSA_CARTAO_PROPRIO: TStringField;
    QCadastroCARTAO_INI: TIntegerField;
    QCadastroFIDELIDADE: TStringField;
    QCadastroENCONTRO_CONTAS: TStringField;
    QCadastroSOLICITA_PRODUTO: TStringField;
    QCadastroVENDA_NOME: TStringField;
    QCadastroOBS_FECHAMENTO: TStringField;
    QCadastroLIMITE_PADRAO: TBCDField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroVALE_DESCONTO: TStringField;
    QCadastroAGENCIADOR_ID: TIntegerField;
    QCadastroSOM_PROD_PROG: TStringField;
    QCadastroEMITE_NF: TStringField;
    QCadastroRECEITA_SEM_LIMITE: TStringField;
    QCadastroCOMPLEMENTO: TStringField;
    QCadastroUSA_COD_IMPORTACAO: TStringField;
    QCadastroBAND_ID: TIntegerField;
    QCadastroNAO_GERA_CARTAO_MENOR: TStringField;
    QCadastroTIPO_CREDITO: TIntegerField;
    QCadastroDIA_REPASSE: TIntegerField;
    QCadastroOBRIGA_SENHA: TStringField;
    QCadastroQTD_DIG_SENHA: TIntegerField;
    QCadastroUTILIZA_RECARGA: TStringField;
    QCadastroRESPONSAVEL_FECHAMENTO: TStringField;
    QCadastroMOD_CART_ID: TIntegerField;
    QCadastroGERA_ACUMULADO: TStringField;
    DBCheckBox20: TDBCheckBox;
    Label61: TLabel;
    Label62: TLabel;
    EdDiaRepasse: TEdit;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    EdFantasia: TEdit;
    cbModCart: TJvDBLookupCombo;
    cbRespFecha: TJvDBLookupCombo;
    rgTipo: TRadioGroup;
    dbDataRenovacao: TJvDateEdit;
    Panel43: TPanel;
    Bevel1: TBevel;
    Bevel3: TBevel;
    Button2: TButton;
    lblTitulo: TLabel;
    btnImportar: TBitBtn;
    tExcel: TADOTable;
    QCadastroEXPORTA_TXT: TStringField;
    DBCheckBox21: TDBCheckBox;
    cbData: TComboBox;
    Label66: TLabel;
    QCadastroREALIZA_LANC_CREDITO: TStringField;
    DBCheckBox22: TDBCheckBox;
    ts1: TTabSheet;
    pnl2: TPanel;
    pgc1: TPageControl;
    ts2: TTabSheet;
    ts3: TTabSheet;
    grp1: TGroupBox;
    txtNomeEmpres: TDBEdit;
    lbl1: TLabel;
    txtCod: TDBEdit;
    lbl2: TLabel;
    grp2: TGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    DSOcorrencias: TDataSource;
    pnl1: TPanel;
    lbl9: TLabel;
    grp3: TGroupBox;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    txtNome: TDBEdit;
    txtTelefone: TDBEdit;
    txtMotivo: TDBEdit;
    pnl3: TPanel;
    btn4: TSpeedButton;
    btn5: TSpeedButton;
    btn6: TSpeedButton;
    btn7: TSpeedButton;
    grp4: TGroupBox;
    MemoDesc_atendimento: TDBMemo;
    pnl4: TPanel;
    pnl5: TPanel;
    grdOcorrencias2: TJvDBGrid;
    txtProtocolo: TEdit;
    lbl11: TLabel;
    grp5: TGroupBox;
    lbl12: TLabel;
    btnBuscar: TBitBtn;
    QOcorrencias: TADOQuery;
    QOcorrenciasatendimento_id: TIntegerField;
    QOcorrenciasnome_solicitante: TStringField;
    QOcorrenciastel_solictante: TStringField;
    QOcorrenciasmotivo: TStringField;
    QOcorrenciasdesc_atendimento: TStringField;
    QOcorrenciasdata_atendimento: TDateTimeField;
    btn3: TBitBtn;
    btn2: TBitBtn;
    btn1: TBitBtn;
    dtInicial: TJvDateEdit;
    dtFinal: TJvDateEdit;
    QOcorrenciasempres_id: TIntegerField;
    QOcorrenciasoperador: TStringField;
    btGravar: TJvBitBtn;
    btCancelar: TJvBitBtn;
    GroupBox9: TGroupBox;
    Label67: TLabel;
    TabAtendHistorico: TTabSheet;
    Panel48: TPanel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    dtIniHistorico: TJvDateEdit;
    dtFimHistorico: TJvDateEdit;
    ComboBox1: TComboBox;
    JvDBGrid2: TJvDBGrid;
    JvBitBtn1: TJvBitBtn;
    QLogOcorrencias: TADOQuery;
    DSLogOcorrencias: TDataSource;
    QLogOcorrenciaslog_id: TIntegerField;
    QLogOcorrenciasjanela: TStringField;
    QLogOcorrenciascampo: TStringField;
    QLogOcorrenciasvalor_ant: TStringField;
    QLogOcorrenciasvalor_pos: TStringField;
    QLogOcorrenciasoperador: TStringField;
    QLogOcorrenciasoperacao: TStringField;
    QLogOcorrenciasdata_hora: TDateTimeField;
    QLogOcorrenciascadastro: TStringField;
    QLogOcorrenciasid: TIntegerField;
    QLogOcorrenciasdetalhe: TStringField;
    QLogOcorrenciasmotivo: TStringField;
    QLogOcorrenciassolicitante: TStringField;
    QLogOcorrenciasprotocolo: TIntegerField;
    QOcorrenciasSTATUS_ID: TIntegerField;
    JvDBLookupCombo3: TJvDBLookupCombo;
    Label72: TLabel;
    QStatusAtend: TADOQuery;
    DSStatusAtend: TDataSource;
    QStatusAtendstatus_id: TIntegerField;
    QStatusAtenddescricao: TStringField;
    txtDataAlteracao: TEdit;
    txtOperadorAlteracao: TEdit;
    GroupBox10: TGroupBox;
    Label68: TLabel;
    Label73: TLabel;
    PanelPendente: TPanel;
    PanelFinalizado: TPanel;
    GroupBox11: TGroupBox;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    txtLogradouroCartao: TDBEdit;
    txtNumeroCartao: TDBEdit;
    QCadastroLOGRADOURO_CARTAO: TStringField;
    QCadastroNUMERO_CARTAO: TIntegerField;
    btnAddEndereco: TBitBtn;
    Label79: TLabel;
    lkpCIDADE_CARTAO: TDBLookupComboBox;
    lkpUF_CARTAO: TDBLookupComboBox;
    txtCepCartao: TDBEdit;
    Label80: TLabel;
    QCadastroCEP_CARTAO: TStringField;
    DBSegmento: TJvDBLookupCombo;
    Label81: TLabel;
    btnAlterarTodos: TBitBtn;
    DSSegmento: TDataSource;
    QEstabSeg: TADOQuery;
    QEstabSegseg_id: TIntegerField;
    QEstabSegdescricao: TStringField;
    btnBuscaSeg: TBitBtn;
    cbbLiberado: TComboBox;
    lbl8: TLabel;
    dbchkUSA_PARCELAMENTO_ESPECIFICO: TDBCheckBox;
    ParcelamentoEspecifico: TTabSheet;
    GridEmpCredParcelamento: TJvDBGrid;
    Panel49: TPanel;
    btnGravaEmpLib: TBitBtn;
    btnCancelEmpLib: TBitBtn;
    Panel50: TPanel;
    Label82: TLabel;
    QParcelaEspecifica: TADOQuery;
    DSParcelaEspecifica: TDataSource;
    QParcelaEspecificacred_id: TIntegerField;
    QParcelaEspecificaliberado: TStringField;
    QParcelaEspecificaNOME_ESTABELECIMENTO: TStringField;
    QParcelaEspecificaempres_id: TIntegerField;
    DBCheckBox24: TDBCheckBox;
    QCadastroUSA_LIMITE_MAX: TStringField;
    edtLimiteMax: TDBEdit;
    Label83: TLabel;
    QCadastroLIMITE_MAX_POR_CONV: TBCDField;
    DBCheckBox25: TDBCheckBox;
    QCadastroUSA_PARCELAMENTO_ESPECIFICO: TStringField;
    QCadastroTEM_VALOR_FINANCEIRO: TStringField;
    btnInserirNovaParcela: TBitBtn;
    DBCheckBox26: TDBCheckBox;
    QCadastroALTERA_LIMITE_SITE: TStringField;
    DBCheckBox27: TDBCheckBox;
    QCadastroOBRIGA_RECEITA_MEDICA: TStringField;
    LimiteConv: TTabSheet;
    Panel51: TPanel;
    Bevel2: TBevel;
    Label84: TLabel;
    Bevel4: TBevel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    edtLimiteMes: TEdit;
    btnAlterarTodosLimites: TButton;
    btnImportarLimiteConv: TBitBtn;
    Panel52: TPanel;
    Label87: TLabel;
    QManutencaoLimiteConv: TADOQuery;
    DSManutencaoLimiteConv: TDataSource;
    QManutencaoLimiteConvconv_id: TIntegerField;
    QManutencaoLimiteConvtitular: TStringField;
    QManutencaoLimiteConvlimite_mes: TBCDField;
    TabSheet6: TTabSheet;
    Panel53: TPanel;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    Panel54: TPanel;
    Label86: TLabel;
    JvDBGrid3: TJvDBGrid;
    btnSoliciarNovaVia: TBitBtn;
    btnReimprimir: TBitBtn;
    QManutencaoCartaoConveniados: TADOQuery;
    DSManutencaoCartaoConveniados: TDataSource;
    QManutencaoCartaoConveniadosconv_id: TIntegerField;
    QManutencaoCartaoConveniadostitular: TStringField;
    QCartoes: TADOQuery;
    QCartoesCARTAO_ID: TIntegerField;
    QCartoesCONV_ID: TIntegerField;
    QCartoesNOME: TStringField;
    QCartoesLIBERADO: TStringField;
    QCartoesCODIGO: TIntegerField;
    QCartoesDIGITO: TWordField;
    QCartoesTITULAR: TStringField;
    QCartoesJAEMITIDO: TStringField;
    QCartoesAPAGADO: TStringField;
    QCartoesLIMITE_MES: TBCDField;
    QCartoesCODCARTIMP: TStringField;
    QCartoesPARENTESCO: TStringField;
    QCartoesDATA_NASC: TDateTimeField;
    QCartoesNUM_DEP: TIntegerField;
    QCartoesFLAG: TStringField;
    QCartoesDTEMISSAO: TDateTimeField;
    QCartoesCPF: TStringField;
    QCartoesRG: TStringField;
    QCartoesVIA: TIntegerField;
    QCartoesDTAPAGADO: TDateTimeField;
    QCartoesDTALTERACAO: TDateTimeField;
    QCartoesOPERADOR: TStringField;
    QCartoesDTCADASTRO: TDateTimeField;
    QCartoesOPERCADASTRO: TStringField;
    QCartoesCRED_ID: TIntegerField;
    QCartoesATIVO: TStringField;
    QCartoesEMPRES_ID: TIntegerField;
    QCartoesSENHA: TStringField;
    DSCartoes: TDataSource;
    QManutencaoCartaoConveniadoschapa: TFloatField;
    QManutencaoCartaoConveniadosempres_id: TIntegerField;
    QManutencaoLimiteConvempres_id: TIntegerField;
    dbGrigManutLimitesConveniados: TJvDBGrid;
    Label85: TLabel;
    Label88: TLabel;
    lblTotalRenovacao: TJvValidateEdit;
    lblTotalAbono: TJvValidateEdit;
    btnExcluir: TButton;
    Label89: TLabel;
    edtPorcentLimiteMaximo: TDBEdit;
    QCadastroPORCENT_LIMITEMAXIMO: TIntegerField;

    dbLkpBairros: TDBLookupComboBox;
    btnAdicionaBairro: TBitBtn;
    lblAddBairro: TLabel;
    QBairros: TADOQuery;
    DSBairros: TDataSource;
    dblkbBAIRRO_CARTAO: TDBLookupComboBox;
    dsBairro_Cartao: TDataSource;
    dsCidade_Cartao: TDataSource;
    dsEstado_Cartao: TDataSource;
    QBairro_Cartao: TADOQuery;
    QCidade_Cartao: TADOQuery;
    QEstado_Cartao: TADOQuery;
    QBairro_CartaoBAIRRO_ID: TAutoIncField;
    QBairro_CartaoCID_ID: TIntegerField;
    QBairro_CartaoDESCRICAO: TStringField;
    QEstado_CartaoESTADO_ID: TIntegerField;
    QEstado_CartaoUF: TStringField;
    QCidade_CartaoCID_ID: TIntegerField;
    QCidade_CartaoESTADO_ID: TIntegerField;
    QCidade_CartaoNOME: TStringField;
    QCadastroBAIRRO: TIntegerField;
    QCadastroCIDADE: TIntegerField;
    QCadastroESTADO: TIntegerField;
    QCadastroBAIRRO_CARTAO: TIntegerField;
    QCadastroCIDADE_CARTAO: TIntegerField;
    QCadastroUF_CARTAO: TIntegerField;
    QCartoesTemp: TADOQuery;
    QCartoesTempCARTAO_ID: TIntegerField;
    QCartoesTempCONV_ID: TIntegerField;
    QCartoesTempNOME: TStringField;
    QCartoesTempLIBERADO: TStringField;
    QCartoesTempCODIGO: TIntegerField;
    QCartoesTempDIGITO: TWordField;
    QCartoesTempTITULAR: TStringField;
    QCartoesTempJAEMITIDO: TStringField;
    QCartoesTempAPAGADO: TStringField;
    QCartoesTempLIMITE_MES: TBCDField;
    QCartoesTempCODCARTIMP: TStringField;
    QCartoesTempPARENTESCO: TStringField;
    QCartoesTempDATA_NASC: TDateTimeField;
    QCartoesTempNUM_DEP: TIntegerField;
    QCartoesTempFLAG: TStringField;
    QCartoesTempDTEMISSAO: TDateTimeField;
    QCartoesTempCPF: TStringField;
    QCartoesTempRG: TStringField;
    QCartoesTempVIA: TIntegerField;
    QCartoesTempDTAPAGADO: TDateTimeField;
    QCartoesTempDTALTERACAO: TDateTimeField;
    QCartoesTempOPERADOR: TStringField;
    QCartoesTempDTCADASTRO: TDateTimeField;
    QCartoesTempOPERCADASTRO: TStringField;
    QCartoesTempCRED_ID: TIntegerField;
    QCartoesTempATIVO: TStringField;
    QCartoesTempEMPRES_ID: TIntegerField;
    QCartoesTempSENHA: TStringField;
    QCartoesTempLIMITE_DIARIO: TBCDField;
    QCartoesTempCONSUMO_ATUAL: TBCDField;
    QCartoesTempCVV: TStringField;
    DSCartoesTemp: TDataSource;
    QCadastroNOMEBAIRRO: TStringField;
    QCadastroNOMECIDADE: TStringField;
    QCadastroNOMEESTADO: TStringField;
    QBairrosBAIRRO_ID: TAutoIncField;
    QBairrosCID_ID: TIntegerField;
    QBairrosDESCRICAO: TStringField;
    QCadastroUSA_NOVO_CARTAO: TStringField;
    DBCheckBox28: TDBCheckBox;
    QCartoesCVV: TStringField;
    txtDESTINATARIO_CARTAO: TDBEdit;
    lbl10: TLabel;
    QCadastroDESTINATARIO_CARTAO: TStringField;
    DBCheckBox29: TDBCheckBox;
    QCadastroACESSA_RELATORIO_ALIMENTACAO: TStringField;
    EdCNPJ: TJvMaskEdit;
    dbEdtCNPJ: TJvDBMaskEdit;
    QCadastroUSA_DESCONTO_ESPECIAL: TStringField;
    DBCheckBox30: TDBCheckBox;
    tsDescontoEspecial: TTabSheet;
    PanelHead: TPanel;
    PanelFooter: TPanel;
    btnGravaCredDescontoEspecial: TBitBtn;
    btnCancelDescontoEspecial: TBitBtn;
    QCredEmpDescontoEspecial: TADOQuery;
    DSCredEmpDescontoEspecial: TDataSource;
    QCredEmpDescontoEspecialcred_id: TIntegerField;
    QCredEmpDescontoEspecialnome: TStringField;
    GridUsaDesconto: TJvDBGrid;
    QCredEmpDescontoEspecialLIBERADA: TStringField;
    DBCheckBox31: TDBCheckBox;
    DBCheckBox32: TDBCheckBox;
    DBCheckBox33: TDBCheckBox;
    DBCheckBox34: TDBCheckBox;
    QCadastroTRANS_SIMULTANEA: TStringField;
    QCadastroEMPRESA_PRINC: TStringField;
    QCadastroTRANS_UNICA: TStringField;
    QCadastroATIVA_SAP: TStringField;
    dbchkACESSA_CADASTRO_SITE: TDBCheckBox;
    QCadastroALTERA_CADASTRO_SITE: TStringField;
    QCadastroACESSA_CADASTRO_SITE: TStringField;
    chkApagado: TCheckBox;
    DBCheckBox35: TDBCheckBox;
    QCadastroFILIAL: TIntegerField;
    Label90: TLabel;
    lkpFilial: TJvDBLookupCombo;
    QFilial: TADOQuery;
    DSFilial: TDataSource;
    QFilialFILIAL_ID: TIntegerField;
    QFilialDESCRICAO: TStringField;
    dbAceitaPerfumaria: TDBCheckBox;
    QCadastroACEITA_PERFUMARIA: TStringField;
    rgDetalheSAP: TRadioGroup;
    Label91: TLabel;
    edtTaxaAdmEmp: TDBEdit;
    QCadastroLANCA_TAXA_EMPRESA: TStringField;
    QCadastroVALOR_TAXA_EMPRESA: TBCDField;
    DBCheckBox36: TDBCheckBox;
    rbTipoComplemento: TRadioGroup;
    QTipoEndereco: TADOQuery;
    QTipoEnderecoTIPO_ENDERECO: TStringField;
    DSTipoEndereco: TDataSource;
    lbl13: TLabel;
    dbLkpTipoEndereco: TDBLookupComboBox;
    QCadastroTIPO_ENDERECO: TStringField;
    tsAnexarContrato: TTabSheet;
    FilenameEdit1: TJvFilenameEdit;
    Label92: TLabel;
    btnAnexarContrato: TButton;
    QContratosEmpresa: TADOQuery;
    QContratosEmpresaCONTRATO_ID: TIntegerField;
    QContratosEmpresaEMPRES_ID: TIntegerField;
    QContratosEmpresaCAMINHO_ARQUIVO: TStringField;
    QContratosEmpresaOPERADOR: TStringField;
    JvOpenDialog1: TJvOpenDialog;
    btnAbrirContrato: TBitBtn;
    OleContainer1: TOleContainer;
    btnEnviarEmail: TBitBtn;
    edtEmailOrigem: TJvEdit;
    edtNomeOrigem: TJvEdit;
    edtEmailDestino: TJvEdit;
    JvgLabel1: TJvgLabel;
    lblNome: TJvgLabel;
    JvgLabel3: TJvgLabel;
    JvgLabel4: TJvgLabel;
    edtAssunto: TJvEdit;
    edtCorpo: TEdit;
    dbchkACEITA_PERFUMARIA: TDBCheckBox;
    QCadastroVALIDA_CPF: TStringField;
    DBCheckBox37: TDBCheckBox;
    QCadastroVALIDA_CVV: TStringField;
    QCadastroTRANSFERE_LIMITE: TStringField;
    QCadastrotaxa_conta_digital: TFloatField;
    chkTRANSFERE_LIMITE: TDBCheckBox;
    DBEdit2: TDBEdit;
    Label93: TLabel;
    LimpaSenha: TTabSheet;
    JvDBGrid4: TJvDBGrid;
    Panel55: TPanel;
    BitBtn10: TBitBtn;
    ImporLimpaSenha: TBitBtn;
    btnButAjudaLimpaSenha: TSpeedButton;
    lbl14: TLabel;
    RichEditLimpaSenha: TRichEdit;
    BitBtn7: TBitBtn;
    RichEdit1: TRichEdit;
    SpeedButton1: TSpeedButton;
    Label94: TLabel;
    QSaldoAlim: TADOQuery;
    DataSource1: TDataSource;
    rbRecarga: TCheckBox;
    QSaldoAlimDATA_RENOVACAO: TDateTimeField;
    QSaldoAlimRENOVACAO_VALOR: TBCDField;
    QSaldoAlimABONO_VALOR: TBCDField;
    QSaldoAlimVALOR_TOTAL: TBCDField;
    QSaldoAlimVENCIMENTO: TDateTimeField;
    QCredAlimconv_id: TIntegerField;
    QCredAlimempres_id: TIntegerField;
    QCredAlimtitular: TStringField;
    QCredAlimlimite_mes: TBCDField;
    QCredAlimabono_mes: TBCDField;
    QCredAlimsaldo_renovacao: TBCDField;
    QCredAlimRENOVACAO_ID: TIntegerField;
    QCredAlimDATA_RENOVACAO: TWideStringField;
    QCredAlimTIPO_CREDITO: TStringField;
    QCredAlimDETALHE_EVENTO: TStringField;
    QCredAlimCOMPLEMENTO: TStringField;
    DBCheckBox38: TDBCheckBox;
    QCadastroLANCA_DEBITO_S_CONSULTA: TStringField;
    JvBitBtn2: TJvBitBtn;
    QManutDataFechaEmp: TJvADOQuery;
    QDatasFechaTemp: TADOQuery;
    QDatasFechaTempEMPRES_ID: TIntegerField;
    QDatasFechaTempDATA_FECHA: TDateTimeField;
    QDatasFechaTempDATA_VENC: TDateTimeField;
    chkLimpaSenha: TCheckBox;
    chkBloqueiaConv: TCheckBox;
    chkLiberaConv: TCheckBox;
    dbchkLiberaBloqueio: TDBCheckBox;
    QCadastrolibera_bloqueio: TStringField;
    UsuariosWebDiversos: TTabSheet;
    EdtNomeFantasia: TEdit;
    Label95: TLabel;
    btnBuscaEmp: TButton;
    DBGrid2: TDBGrid;
    qEmpresas: TJvADOQuery;
    dsEmpresas: TDataSource;
    btnMarcarTodos: TButton;
    btnAdicionar: TButton;
    DBGrid3: TDBGrid;
    ClientDataSet1: TClientDataSet;
    DataSource2: TDataSource;
    ClientDataSet1EMPRES_ID: TIntegerField;
    ClientDataSet1FANTASIA: TStringField;
    btnCriarEmails: TButton;
    ADOQuery1: TADOQuery;
    ADOQuery3: TADOQuery;
    txtUsuario: TEdit;
    Label96: TLabel;
    ADOQuery4: TADOQuery;
    btnRemover: TButton;
    ADOQuery5: TADOQuery;
    Button3: TButton;
    btnExcluit: TButton;
    CbLiberado: TCheckBox;
    CbPesqLiberado: TCheckBox;
    txtEmpresId: TEdit;
    Label97: TLabel;

    procedure ButBuscaClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButLimpaSenhaClick(Sender: TObject);
    procedure TabSegShow(Sender: TObject);
    procedure SegLibAfterPost(DataSet: TDataSet);
    procedure SegLibBeforePost(DataSet: TDataSet);
    procedure TabSegHide(Sender: TObject);
    procedure TabdatasShow(Sender: TObject);
    procedure QDatasFechaBeforePost(DataSet: TDataSet);
    procedure QDatasFechaCalcFields(DataSet: TDataSet);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure GridSegTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure TabFichaExit(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure ButApagaClick(Sender: TObject);
    procedure TabSaldoHide(Sender: TObject);
    procedure CBAnoChange(Sender: TObject);
    procedure DBCart_IniExit(Sender: TObject);
    procedure DBCheckBox7Click(Sender: TObject);
    procedure DBCart_IniEnter(Sender: TObject);
    procedure EdMesesVencKeyPress(Sender: TObject; var Key: Char);
    procedure ChVencnomesClick(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure QDatasFechaBeforeDelete(DataSet: TDataSet);
    procedure QDatasFechaAfterPost(DataSet: TDataSet);
    procedure DSCadastroStateChange(Sender: TObject);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    //procedure Grid_GrupoTitleClick(Column: TColumn);
    procedure TabGruposShow(Sender: TObject);
    //procedure QGrupo_conv_empBeforeDelete(DataSet: TDataSet);
    procedure TabGruposHide(Sender: TObject);
    procedure QGrupo_conv_empBeforePost(DataSet: TDataSet);
    procedure AlterarIddaempresa1Click(Sender: TObject);
    procedure PopupEmpres_IDPopup(Sender: TObject);
    procedure QGrupo_ProdAfterPost(DataSet: TDataSet);
    procedure TabSheet3Show(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChkUsa2FechaClick(Sender: TObject);
    procedure EdMesesVenc2Exit(Sender: TObject);
    procedure EdMesesVencExit(Sender: TObject);
    procedure ChVencnomes2Click(Sender: TObject);
    procedure QDatasFechaBeforeEdit(DataSet: TDataSet);
    procedure btnCancelGrupProdClick(Sender: TObject);
    procedure DSGrupo_ProdStateChange(Sender: TObject);
    procedure GridGrupo_ProdDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure TabVendaNomeShow(Sender: TObject);
    procedure DSVendaNomeStateChange(Sender: TObject);
    procedure QVendaNomeAfterInsert(DataSet: TDataSet);
    procedure QVendaNomeAfterPost(DataSet: TDataSet);
    procedure GridVendaNomeDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure TabVendaNomeHide(Sender: TObject);
    procedure QCredLibAfterInsert(DataSet: TDataSet);
    procedure QCredLibAfterPost(DataSet: TDataSet);
    procedure TabCredLibShow(Sender: TObject);
    procedure TabCredLibHide(Sender: TObject);
    procedure DSCredLibStateChange(Sender: TObject);
    //procedure GridCredLibTitleBtnClick(Sender: TObject; ACol: Integer;
    //  Field: TField);
    procedure qPbmAfterPost(DataSet: TDataSet);
    procedure qPbmBeforePost(DataSet: TDataSet);
    procedure dsPbmStateChange(Sender: TObject);
    procedure GridPbmColExit(Sender: TObject);
    procedure GridGrupo_ProdColExit(Sender: TObject);
    procedure GridCredLibColExit(Sender: TObject);
    procedure GriddatasColExit(Sender: TObject);
    procedure dsProdBloqStateChange(Sender: TObject);
    procedure DSDatasFechaStateChange(Sender: TObject);
    procedure qPbmBeforeEdit(DataSet: TDataSet);
    procedure QGrupo_ProdBeforeEdit(DataSet: TDataSet);
    procedure QCredLibBeforeEdit(DataSet: TDataSet);
    procedure QGrupo_conv_empBeforeEdit(DataSet: TDataSet);
    procedure QCadastroBeforeEdit(DataSet: TDataSet);
    procedure qPbmBeforeInsert(DataSet: TDataSet);
    procedure QGrupo_ProdBeforeInsert(DataSet: TDataSet);
    procedure QCredLibBeforeInsert(DataSet: TDataSet);
    procedure QDatasFechaBeforeInsert(DataSet: TDataSet);
    procedure QGrupo_conv_empBeforeInsert(DataSet: TDataSet);
    procedure QCadastroBeforeInsert(DataSet: TDataSet);
    procedure TabConfigGrupoHide(Sender: TObject);
    procedure TabConfigGrupoShow(Sender: TObject);
    procedure TabConfigProgramaHide(Sender: TObject);
    procedure TabConfigProgramaShow(Sender: TObject);
    procedure EdCredIndKeyPress(Sender: TObject; var Key: Char);
    procedure EdCredIndChange(Sender: TObject);
    procedure cbbCredIndChange(Sender: TObject);
    procedure PageControl4Enter(Sender: TObject);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
    procedure DBEdit34KeyPress(Sender: TObject; var Key: Char);
    procedure TabGrupoProdShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnIncGrupEmpClick(Sender: TObject);
    procedure btnCancelDataFechaClick(Sender: TObject);
    procedure btnAltDataFechaClick(Sender: TObject);
    procedure DSSegLibStateChange(Sender: TObject);
    procedure btnAddEnderecoClick(Sender: TObject);
    procedure btnGravaSegClick(Sender: TObject);
    procedure btnCancelSegClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure btnGravaGrupEmpClick(Sender: TObject);
    procedure btnVisualizaSaldoClick(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure btnExclGrupEmpClick(Sender: TObject);
    procedure btnGravaDataFechaClick(Sender: TObject);
    procedure btnGravaPbmClick(Sender: TObject);
    procedure btnCancelPbmClick(Sender: TObject);
    procedure btnCancGrupEmpClick(Sender: TObject);
    procedure DSQGrupo_conv_empStateChange(Sender: TObject);
    procedure QGrupo_conv_empAfterInsert(DataSet: TDataSet);
    procedure btnExclProdBloqClick(Sender: TObject);
    procedure btnInclProdBloqClick(Sender: TObject);
    procedure AlteraoLinearDescontoemGrupodeProdutos1Click(
      Sender: TObject);
    procedure AlteraoLineardeGrupodeProdutoLiberado1Click(Sender: TObject);
    procedure TabCartEmpShow(Sender: TObject);
    procedure TabCartEmpHide(Sender: TObject);
    procedure qFormasPgtoAfterPost(DataSet: TDataSet);
    procedure qFormasPgtoBeforeEdit(DataSet: TDataSet);
    procedure qFormasPgtoBeforeInsert(DataSet: TDataSet);
    procedure qFormasPgtoBeforePost(DataSet: TDataSet);
    procedure grdFormasPgtoColExit(Sender: TObject);
    procedure tabFormasPgtoShow(Sender: TObject);
    procedure tabFormasPgtoHide(Sender: TObject);
    procedure QGrupo_ProdBeforePost(DataSet: TDataSet);
    procedure dbEdtCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelCredObrigarSenhaClick(Sender: TObject);
    procedure btnGravaCredObrigarSenhaClick(Sender: TObject);
    procedure QCred_Obriga_SenhaAfterInsert(DataSet: TDataSet);
    procedure QCred_Obriga_SenhaBeforeEdit(DataSet: TDataSet);
    procedure QCred_Obriga_SenhaBeforeInsert(DataSet: TDataSet);
    procedure DSCred_Obriga_SenhaStateChange(Sender: TObject);
    procedure QCred_Obriga_SenhaAfterPost(DataSet: TDataSet);
    procedure tsSenhaConvShow(Sender: TObject);
    procedure tsSenhaConvHide(Sender: TObject);
    procedure GridCredObrigarSenhaColExit(Sender: TObject);
    //procedure GridCredObrigarSenhaTitleBtnClick(Sender: TObject;
    //ACol: Integer; Field: TField);
    procedure qUsu_WebBeforePost(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure tsUsuariosWebShow(Sender: TObject);
    procedure tsUsuariosWebHide(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btnGravarConvAlimClick(Sender: TObject);
    procedure tsAlimentacaoShow(Sender: TObject);
    procedure edtAbonoMesKeyPress(Sender: TObject; var Key: Char);
    procedure dsCredAlimStateChange(Sender: TObject);
    procedure btnGravaFormasClick(Sender: TObject);
    procedure btnCancelFormasClick(Sender: TObject);
    procedure dsEstadosDataChange(Sender: TObject; Field: TField);
    procedure GridCredLibKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdFormasPgtoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridCredObrigarSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qUsu_WebAfterPost(DataSet: TDataSet);
    procedure GridPbmKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnInserirPosClick(Sender: TObject);
    procedure dbGridUsuWebKeyPress(Sender: TObject; var Key: Char);
    procedure dbGridUsuWebKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtnInserirClick(Sender: TObject);
    procedure BtnGravarClick(Sender: TObject);
    procedure TabSheet5Show(Sender: TObject);
    procedure BtnApagarClick(Sender: TObject);
    procedure TabSheet5Hide(Sender: TObject);
    procedure DBGridEmpDptosKeyPress(Sender: TObject; var Key: Char);
    procedure QCadastroAfterOpen(DataSet: TDataSet);
    procedure TabFichaShow(Sender: TObject);
    procedure GridCredLibTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure dbGridAlimTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnCancelarConvAlimClick(Sender: TObject);
    procedure QCredAlimBeforePost(DataSet: TDataSet);
    procedure btnImportarClick(Sender: TObject);
    procedure GridCredObrigarSenhaTitleBtnClick(Sender: TObject;
      ACol: Integer; Field: TField);
    procedure TabSaldoShow(Sender: TObject);
    procedure QOcorrenciasAfterInsert(DataSet: TDataSet);
    procedure btnBuscarClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btnAlterarTodosClick(Sender: TObject);
    procedure btnBuscaSegClick(Sender: TObject);
    procedure btnFirstBClick(Sender: TObject);
    procedure btnFirstCClick(Sender: TObject);
    procedure btnLastBClick(Sender: TObject);
    procedure btnLastCClick(Sender: TObject);
    procedure btnNextBClick(Sender: TObject);
    procedure btnNextCClick(Sender: TObject);
    procedure btnPriorBClick(Sender: TObject);
    procedure btnPriorCClick(Sender: TObject);
    procedure ButCancelaClick(Sender: TObject);
    procedure DSOcorrenciasStateChange(Sender: TObject);
    procedure grdOcorrencias2DblClick(Sender: TObject);
    procedure grdOcorrencias2DrawColumnCell(Sender: TObject; const Rect: TRect;
        DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvBitBtn1Click(Sender: TObject);
    procedure QOcorrenciasBeforePost(DataSet: TDataSet);
    procedure ts1Show(Sender: TObject);
    procedure ts2Show(Sender: TObject);
    procedure ts3Show(Sender: TObject);
    procedure EdFantasiaKeyPress(Sender: TObject; var Key: Char);
    procedure EdFantasiaExit(Sender: TObject);
    procedure GridEmpCredParcelamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridEmpCredParcelamentoColExit(Sender: TObject);
    procedure ParcelamentoEspecificoShow(Sender: TObject);
    procedure ParcelamentoEspecificoHide(Sender: TObject);
    procedure btnGravaEmpLibClick(Sender: TObject);
    procedure btnInserirNovaParcelaClick(Sender: TObject);
    procedure QManutencaoLimiteConvBeforeOpen(DataSet: TDataSet);
    procedure LimiteConvShow(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure TabSheet6Show(Sender: TObject);
    procedure btnSoliciarNovaViaClick(Sender: TObject);
    procedure TabSheet6Exit(Sender: TObject);
    procedure btnReimprimirClick(Sender: TObject);
    procedure btnImportarLimiteConvClick(Sender: TObject);
    procedure btnImportLimpaSenha(Sender: TObject);
    procedure dbGrigManutLimitesConveniadosColExit(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnAlterarTodosLimitesClick(Sender: TObject);
    procedure btnAdicionaBairroClick(Sender: TObject);
    procedure dsEstado_CartaoDataChange(Sender: TObject; Field: TField);
    procedure dsCidade_CartaoDataChange(Sender: TObject; Field: TField);
    procedure dsCidadesDataChange(Sender: TObject; Field: TField);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1Enter(Sender: TObject);
    procedure txtCEPExit(Sender: TObject);
    procedure tsDescontoEspecialShow(Sender: TObject);
    procedure GridUsaDescontoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure GridUsaDescontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridUsaDescontoColExit(Sender: TObject);
    procedure DSCredEmpDescontoEspecialStateChange(Sender: TObject);
    procedure btnCancelDescontoEspecialClick(Sender: TObject);
    procedure QCredEmpDescontoEspecialAfterPost(DataSet: TDataSet);
//    procedure rgTipoClick(Sender: TObject);
    procedure DBGrid1ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure btnAnexarContratoClick(Sender: TObject);
    procedure tsAnexarContratoEnter(Sender: TObject);
    procedure tsAnexarContratoShow(Sender: TObject);
    procedure btnAbrirContratoClick(Sender: TObject);
    procedure ButAjudaLimpaSenha(Sender: TObject);
    //procedure DBEdit5Change(Sender: TObject);
    procedure ButAjudaClick(Sender: TObject);
    procedure JvBitBtn2Click(Sender: TObject);
    procedure btnBuscaEmpClick(Sender: TObject);
    procedure btnMarcarTodosClick(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnCriarEmailsClick(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
    function VerificaEmail (email: String): boolean;
    procedure Button3Click(Sender: TObject);
    procedure btnExcluitClick(Sender: TObject);
    procedure txtUsuarioKeyPress(Sender: TObject; var Key: Char);


  private
    { Private declarations }
    AlterandoTodasDatas:Boolean;
    Fechamento_Old : TDateTime;
    Vencimento_Old : TDateTime;
    Fechamento1_old : Integer;
    vencimento1_old : Integer;
    LogSegLIb, LogDataFecha: TLog;
    procedure AbreDatas;
    procedure TrocarEmpres_ID(new_EmpresId: integer);
    procedure BuscarOcorrencia();
    procedure CriarDatas(MesInicial,AnoInicial:Integer;MostrarMSG:Boolean);
    //procedure AtualizarDatasCC(MostrarMSG:Boolean);
    function CriarData(ano, mes, dia: integer): TDateTime;
    procedure AtualizarVencimentos;
    function Valida : Boolean;
    function ExisteCredBloq(empres_id, cred_id: integer): boolean;
    procedure ConfiguraTela;
    procedure GravaDescGrupoProd(EmprID, GrupoID: Integer;
      Valor: Currency);
    procedure SolicitaNovaViaTodosConveniados;
    procedure GravaDescGrupoProdLib(EmprID, GrupoID: Integer;
    Valor: String);
    procedure buscarUltimasAlteracoes;
    procedure LimpaCombos;
    procedure AlterarTodosCred(SN: Char);
  public
    { Public declarations }
    camposeg : string;
    descseg : boolean;
    tSaldoRenovacao, tLimiteMes, tAbonoMes : currency;
    SavePlace : TBookmark;
    SavePlace1 : TBookmark;
    procedure AtualizaDataEmp(PedirConfirmacao: Boolean);
    procedure AtualizaSaldoRenovacao(tipo: TSaldoRenovacao);
    procedure AtualizaAbonoMes(tipo: TSaldoRenovacao);
    procedure AtualizaAbonoMesESaldoRenovacao(tipoAbono,tipoSaldo: Integer);
    function geraID: Integer;
    procedure habilitarBotoes;
    procedure InsereAlimentacaoRenovacao;
    procedure InsereAlimRenovacaoCreditos;
    procedure limparCampos;
    procedure CarregaGrid();
  end;

var
  FCadEmp: TFCadEmp;
  cLiberado : string;
implementation

uses DM, {util2,} {ZStoredProcedure,} Types, StrUtils, UMenu, cartao_util,IdSMTP, IdMessage, IdSSLOpenSSL,
  UBuscaProdutos2, UAltLinearGrupoProd, CurrEdit, {UAltLinearGrupoProdLib, }
  UValidacao, URotinasTexto, UAltLinearGrupoProdLib, UAltContatoEmpres,USelTipoImp,
  FOcorrencia, FCadBairro, UChangeLog;

{$R *.dfm}

procedure TFCadEmp.ButBuscaClick(Sender: TObject);
VAR cgc : String;
begin
  inherited;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select emp.*, UPPER(ba.descricao) as NOMEBAIRRO, UPPER(ci.nome) as NOMECIDADE, es.UF as NOMEESTADO from empresas emp ');
  //QCadastro.Sql.Add(' left join bairros ba on (ba.bairro_id = emp.bairro) left join cidades ci on (ci.cid_id = emp.cidade) left join estados es on (es.estado_id = emp.estado) where coalesce(apagado,''N'') <> ''S'' ');
  QCadastro.Sql.Add(' left join bairros ba on (ba.bairro_id = emp.bairro) left join cidades ci on (ci.cid_id = emp.cidade) left join estados es on (es.estado_id = emp.estado) where ');
  if chkApagado.Checked = True then
    QCadastro.SQL.Add(' coalesce(apagado,''N'') = ''S''')
        else
          QCadastro.SQL.Add(' coalesce(apagado,''N'') = ''N''');

  if cbbLiberado.ItemIndex > 0 then
    if cbbLiberado.ItemIndex = 1 then
      QCadastro.SQL.Add(' and emp.liberada = ''S''')
        else
          QCadastro.SQL.Add(' and emp.liberada = ''N''')
  else
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and emp.empres_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and emp.nome like '+QuotedStr('%'+EdNome.Text+'%'));
  if Trim(EdCidade.Text) <> '' then
     QCadastro.Sql.Add(' and ci.nome like '+QuotedStr('%'+EdCidade.Text+'%') + ' COLLATE Latin1_General_CI_AI');
  if Trim(EdFantasia.Text) <> '' then
     QCadastro.SQL.Add(' and emp.fantasia like '+QuotedStr('%'+EdFantasia.Text+'%'));
     cgc :=   Trim(StringReplace(StringReplace((StringReplace(Trim(EdCNPJ.Text),'.','',[rfReplaceAll, rfIgnoreCase])),'/','',[rfReplaceAll, rfIgnoreCase]),'-','',[rfReplaceAll, rfIgnoreCase]));
  if cgc <> '' then
    QCadastro.SQL.Add(' and emp.cgc like''%'+EdCNPJ.Text+'%''');
  if Trim(EdDiaRepasse.Text) <> '' then
     Qcadastro.sql.add(' and emp.dia_repasse = '+QuotedStr(EdDiaRepasse.Text));
  if Trim(cbRespFecha.DisplayValue)<> '' then
     Qcadastro.sql.add(' and emp.responsavel_fechamento = '+QuotedStr(cbRespFecha.DisplayValue));
  if Trim(cbModCart.DisplayValue)<> '' then
     QCadastro.SQL.Add(' and emp.mod_cart_id = '+QuotedStr(cbModCart.KeyValue));
  QCadastro.Sql.Add(' order by emp.nome ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then
  begin
    Self.TextStatus := 'Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroNOME.AsString;
    DBGrid1.SetFocus;
  end
  else
    EdCod.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
  EdCidade.Clear;
  EdFantasia.Clear;
  EdCNPJ.Clear;
  cbbLiberado.ItemIndex := 0;

  // reseta
  if QCadastroTIPO_CREDITO.AsInteger = 2 then
  begin
    rbRecarga.Visible := True;
    rbRecarga.Checked := True;
  end
  else begin
    rbRecarga.Visible := False;
    rbRecarga.Checked := False;
  end;
   btnVisualizaSaldo.Click;

end;

procedure TFCadEmp.TabSheet2Show(Sender: TObject);
begin
  inherited;
  DBEdit5.SetFocus;
end;

procedure TFCadEmp.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtNm.SetFocus;
end;

procedure TFCadEmp.FormCreate(Sender: TObject);
begin
  chavepri := 'empres_id';
  detalhe  := 'Empres ID: ';
  inherited;

  LogSegLIb := TLog.Create;
  LogSegLIb.LogarQuery(SegLib,janela,'Seg ID: ','Segmentos Liberados',Operador.Nome,'empres_id');
  LogDataFecha := TLog.Create;
  LogDataFecha.LogarQuery(QDatasFecha,janela,'Empresa: ','Datas de Fechamento',Operador.Nome,'empres_id');

  CBAno.ItemIndex := CBAno.Items.IndexOf(FormatDateTime('yyyy',Today));
  cbData.ItemIndex := cbData.Items.IndexOf(FormatDateTime('yyyy',Today));
  AlterandoTodasDatas := False;
  ConfiguraTela;
  qListaCred.Open;
  QBandeiras.Open;
  QCadastro.Open;
  QOcorrencias.Open;
  FMenu.vCadEmp := True;
  tsAlimentacao.TabVisible := false;
  qEstados.Open;
  qCidades.Parameters.ParamByName('ESTADO_ID').Value := 0;
  qCidades.Open;
  QBairros.Parameters.ParamByName('CID_ID').Value := 0;
  QBairros.Open;

  QEstado_Cartao.Open;
  QCidade_Cartao.Parameters.ParamByName('ESTADO_ID').Value := 0;
  QCidade_Cartao.Open;
  QBairro_Cartao.Parameters.ParamByName('CID_ID').Value := 0;
  QBairro_Cartao.Open;

  QTipoEndereco.Close;
  QTipoEndereco.SQL.Clear;
  QTipoEndereco.SQL.Add('SELECT DISTINCT TIPO_ENDERECO FROM CREDENCIADOS WHERE TIPO_ENDERECO IS NOT NULL AND TIPO_ENDERECO <> '''' ORDER BY TIPO_ENDERECO') ;
  QTipoEndereco.Open;


  // Para adicionar liberado = true
  if (Operador.ID = 1) or (Operador.ID = 3)or (Operador.ID = 36) or (Operador.ID = 5) or (Operador.ID = 69) or (Operador.ID = 86) or (Operador.ID = 72) or (Operador.ID = 9) or (Operador.ID = 11) or (Operador.ID = 63) or (Operador.IsAdmin) or (Operador.ID = 91) or (Operador.ID = 113) or (Operador.ID = 80) then
  begin
    DBCheckBox1.Visible := True;
  end;

  qOperadores.Open;
  qModelosCartoes.Open;
  QFilial.Open;
  EdCod.SetFocus;
end;

procedure TFCadEmp.ConfiguraTela;
begin
  DMConexao.Config.Open;
  TabConfigPrograma.TabVisible:= (DMConexao.ConfigINTEGRA_SISTEMABIG.AsString = 'S');
  TabConfigGrupo.TabVisible:= (DMConexao.ConfigINTEGRA_SISTEMABIG.AsString = 'S');
  if DMConexao.ConfigUSA_PROG_DESC.AsString = 'S' then
  begin
    TabConfigPrograma.TabVisible:= True;
    lblModalidade.Visible:= True;
    cbbModalidade.Enabled:= True;
    grpProgDesc.Visible:= True;
  end
  else
  begin
    TabConfigPrograma.TabVisible:= False;
    lblModalidade.Visible:= False;
    cbbModalidade.Enabled:= False;
    grpProgDesc.Visible:= False;
  end;
  DMConexao.Config.Close;
end;

procedure TFCadEmp.ButLimpaSenhaClick(Sender: TObject);
begin
  inherited;
  if (TBitBtn(Sender).Enabled = False) then Abort;
  if QCadastro.RecordCount <= 0 then begin
    MsgErro('N�o existe registro para limpar senha, ou n�o existe registro selecinoado!');
    Abort;
  end;
  QCadastro.Edit;
  QCadastro.FieldByName('SENHA').AsString :=  Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.Post;
end;

procedure TFCadEmp.TabSegShow(Sender: TObject);
begin
  inherited;
  panel4.Visible := False;
  SegLib.Close;
  if not QCadastro.IsEmpty then begin
     if QCadastroTODOS_SEGMENTOS.AsString = 'S' then begin
        panel4.Visible := True;
     end
     else begin
        SegLib.Open;
        SegLib.Tag := 1;
        QSegmentos.Open;
        QSegmentos.First;
        while not QSegmentos.Eof do begin
           QSegLib.Close;
           QSegLib.Parameters.ParamByName('empres_id').Value := QCadastroEMPRES_ID.AsInteger;
           QSegLib.Parameters.ParamByName('seg_id').Value := QSegmentosseg_id.AsInteger;
           QSegLib.Open;
           SegLib.Append;
           SegLibSeg_ID.AsInteger := QSegmentosseg_id.AsInteger;
           SegLibDescricao.AsString := QSegmentosdescricao2.AsString;
           SegLibLiberado.AsString := QSegLibliberado.AsString;
           if SegLibLiberado.AsString <> 'S' then SegLibLiberado.AsString := 'N';
           SegLibempres_id.AsInteger := QCadastroEMPRES_ID.AsInteger;
           SegLib.Post;
           QSegLib.Close;
           QSegmentos.Next;
        end;
        QSegmentos.Close;
        SegLib.Tag := 0;
        SegLib.First;
     end;
  end;
end;

procedure TFCadEmp.SegLibAfterPost(DataSet: TDataSet);
var Query : TADOQuery;
begin
  inherited;
  if SegLib.Tag = 0 then
  begin
    Query := TADOQuery.Create(self);
    Query.Connection := DMConexao.AdoCon;
    //Query.RequestLive := True;
    Query.SQL.Text := ' select * from empres_seg_lib where empres_id = '+QCadastroEMPRES_ID.AsString+' and seg_id = '+SegLibSeg_ID.AsString;
    Query.Open;
    if not Query.IsEmpty then
    begin
      Query.Edit;
      Query.FieldByName('liberado').AsString  := SegLibLiberado.AsString;
    end
    else
    begin
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SEMPRES_SEG_LIB_ID AS EMPRES_SEG_LIB_ID');
      DMConexao.AdoQry.Open;
      Query.Append;
      Query.FieldByName('EMPRES_SEG_LIB_ID').AsInteger := DMConexao.AdoQry.FieldByName('EMPRES_SEG_LIB_ID').AsInteger;
      Query.FieldByName('empres_id').AsString := QCadastroEMPRES_ID.AsString;
      Query.FieldByName('seg_id').AsString    := SegLibSeg_ID.AsString;
      Query.FieldByName('liberado').AsString  := SegLibLiberado.AsString;
    end;
    Query.Post;
    Query.Free;
  end;
end;

procedure TFCadEmp.SegLibBeforePost(DataSet: TDataSet);
begin
  inherited;
  if SegLib.Tag = 0 then
  begin
    SegLibLiberado.AsString := UpperCase(SegLibLiberado.AsString);
    if not (SegLibLiberado.AsString[1] in ['N','S']) then SegLibLiberado.AsString := 'N';
  end;
end;

procedure TFCadEmp.TabSegHide(Sender: TObject);
begin
  if SegLib.State = dsEdit then SegLib.Post;
end;

procedure TFCadEmp.TabdatasShow(Sender: TObject);
begin
  AbreDatas;
  PageControl3.ActivePageIndex := 0;
end;

procedure TFCadEmp.QDatasFechaBeforePost(DataSet: TDataSet);
var UltimoFecha : TDateTime;
    dataAtual   : String;
begin
  inherited;
  if QDatasFechaDATA_FECHA.IsNull then
  begin
    ShowMessage('Data de fechamento obrigat�ria.');
    Sysutils.Abort;
  end;
  if QDatasFechaDATA_VENC.IsNull then
  begin
    ShowMessage('Data de vencimento obrigat�ria.');
    Sysutils.Abort;
  end;
  if QDatasFechaDATA_VENC.AsDateTime < QDatasFechaDATA_FECHA.AsDateTime then
  begin
    ShowMessage('Data de vencimento tem que ser maior ou igual que a data de fechamento.');
    Sysutils.Abort;
  end;
  if not AlterandoTodasDatas then
  begin
    if DMConexao.ObterUltimoFechamentoEmp(QCadastroEMPRES_ID.AsInteger,UltimoFecha) then
    begin
      if UltimoFecha >= Fechamento_Old then
      begin
        QDatasFecha.Cancel;
        raise Exception.Create('N�o � poss�vel alterar o fechamento '+FormatDataBR(Fechamento_Old)+sLineBreak+'Existe(m) fatura(s) para esta empresa com data de fechamento maior ou igual a esta');
      end;
      dataAtual := DateToStr(Date);
      if QDatasFechaDATA_FECHA.AsDateTime <= Date then
      begin
        QDatasFecha.Cancel;
        raise Exception.Create('N�o � poss�vel alterar o fechamento '+FormatDataBR(Fechamento_Old)+sLineBreak+'a data de fechamento deve ser maior que a data atual '+FormatDataBR(Now)+sLineBreak);
      end;
    end;

  end;
end;

procedure TFCadEmp.QDatasFechaCalcFields(DataSet: TDataSet);
begin
  inherited;
  QDatasFechaDESC_FECHAMENTO.AsString := FormatDateTime('dddd"," dd "de" mmmm "de" yyyy',QDatasFechaDATA_FECHA.AsDateTime);
  QDatasFechaDESC_VENCIMENTO.AsString := FormatDateTime('dddd"," dd "de" mmmm "de" yyyy',QDatasFechaDATA_VENC.AsDateTime);
end;

procedure TFCadEmp.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroNOME.AsString;
  DBCart_Ini.Enabled := QCadastroUSA_CARTAO_PROPRIO.AsString = 'S';
  tsAlimentacao.TabVisible := QCadastroTIPO_CREDITO.AsInteger = 2;
end;

procedure TFCadEmp.GridSegTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
  Try
    if camposeg = Field.FieldName then
    begin
      descseg := not descseg;
      SegLib.SortOnFields(Field.FieldName,True,descseg);
    end
    else
    begin
      SegLib.SortOnFields(Field.FieldName);
      descseg := False;
    end;
    camposeg := Field.FieldName;
  except
  end;
end;

procedure TFCadEmp.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SEMPRES_ID AS EMPRES_ID');
  DMConexao.AdoQry.Open;
  QCadastroEMPRES_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  QCadastroCONTRATO.AsInteger          := QCadastroEMPRES_ID.AsInteger;
  QCadastroLIBERADA.AsString           := 'S';
  QCadastroUSA_COD_IMPORTACAO.AsString := 'S';
  QCadastroAPAGADO.AsString            := 'N';
  QCadastroTODOS_SEGMENTOS.AsString    := 'N';
  QCadastroBLOQ_ATE_PGTO.AsString      := 'N';
  QCadastroACEITA_PARC.AsString        := 'N';
  QCadastroPEDE_NF.AsString            := 'N';
  QCadastroPEDE_REC.AsString           := 'N';
  QCadastroVENDA_NOME.AsString         := 'S';
  QCadastroSOLICITA_PRODUTO.AsString   := 'N';
  QCadastroUSA_CARTAO_PROPRIO.AsString := 'N';
  QCadastroFIDELIDADE.AsString         := 'N';
  QCadastroINC_CART_PBM.AsString       := 'N';
  QCadastroPROG_DESC.AsString          := 'N';
  QCadastroLIMITE_PADRAO.AsCurrency    := 0.00;
  QCadastroCOMISSAO_CRED.AsCurrency    := 0.00;
  QCadastroREPASSE_EMPRESA.AsCurrency  := 0.00;
  QCadastroDESCONTO_FUNC.AsCurrency    := 0.00;
  QCadastroDESCONTO_EMP.AsCurrency     := 0.00;
  QCadastroVALE_DESCONTO.AsString      := 'N';
  QCadastroSOM_PROD_PROG.AsString      := 'N';
  QCadastroEMITE_NF.AsString           := 'N';
  QCadastroRECEITA_SEM_LIMITE.AsString := 'N';
  QCadastroOBRIGA_SENHA.AsString       := 'N';
  QCadastroUTILIZA_RECARGA.AsString    := 'N';
  QCadastroUSA_NOVO_CARTAO.AsString    := 'S';
  QCadastroSENHA.AsString              := Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.Tag := 1; //Para informar que est� inserindo....
  QCadastroOPERCADASTRO.Value := Operador.Nome;
  QCadastroDTCADASTRO.Value := now;
  QCadastroGERA_ACUMULADO.AsString        := 'S';
  QCadastroEXPORTA_TXT.AsString           := 'N';
  QCadastroREALIZA_LANC_CREDITO.AsString  := 'N';
end;

procedure TFCadEmp.TabFichaExit(Sender: TObject);
begin
  inherited;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFCadEmp.QCadastroBeforePost(DataSet: TDataSet);
begin
  if not Valida then
    Abort;

  if (QCadastro.State in [dsInsert]) then
  begin
    ChVencnomes.Checked := True;
    ChVencnomes2.Checked := True;
  end;
  {if QCadastro.State = dsInsert then
    begin
      QCadastro.FieldByName('DTCADASTRO').AsDateTime := Now;
      QCadastro.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
    end;
    QCadastro.FieldByName('DTALTERACAO').AsDateTime := Now;
    QCadastro.FieldByName('OPERADOR').AsString := Operador.Nome;
    if QCadastro.FieldByName('APAGADO').AsString = 'S' then
       QCadastro.FieldByName('DTAPAGADO').AsDateTime := Now;  }
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;  
  // Criada uma trigger para cadastrar a fidelidade nos conveniados
  {
  if QCadastro.State <> dsInsert then
  begin
    if ((QCadastroFIDELIDADE.OldValue <> QCadastroFIDELIDADE.Value) and (QCadastroFIDELIDADE.AsString = 'S')) then
    begin
      MsgInf('Todos os funcion�rios da empresa estar�o participando do cart�o fidelidade');
      Screen.Cursor := crHourGlass;
      DMConexao.Connection1.StartTransaction;
      DMConexao.ExecuteSql(' execute procedure proc_insert_fidel_emp (' + QCadastroEMPRES_ID.AsString + ') ');
      DMConexao.Connection1.Commit;
      Screen.Cursor := crDefault;
    end;
  end;
  }
end;

procedure TFCadEmp.ButApagaClick(Sender: TObject);
begin
  if TBitBtn(Sender).Enabled = False then Abort;
  if QCadastro.RecordCount <= 0 then
    begin
    MsgErro('N�o existe registro para ser apagado, ou n�o existe registro selecinoado!!');
    Abort;
    end;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.Sql.Text := 'Select count(conv_id) as num from conveniados where coalesce(apagado,''N'') <> ''S'' and empres_id = '+QCadastroEMPRES_ID.asstring;
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.Fields[0].AsInteger > 0 then
  begin
    Application.MessageBox(PChar('N�o foi poss�vel executar esta opera��o!'+#13+'Exitem conveniados cadastrados a esta empresa.'),'Aten��o',MB_ICONINFORMATION+MB_OK);
  end
  else inherited;
  DMConexao.AdoQry.Close;
end;

function TFCadEmp.Valida : Boolean;
var cnpj : string;
begin
  Result := False;
  //Nome
  if fnVerfCompVazioEmTabSheet('Digite o Nome da Empresa!', dbEdtNm)               then Abort;
  //Tipo Endere�o
  if Trim(QCadastroTIPO_ENDERECO.AsString) = '' then
  begin
    MsgInf('Escolha o Tipo de Endere�o!');
    dbLkpTipoEndereco.SetFocus;
    Abort;
  end;
  //Endere�o
  if fnVerfCompVazioEmTabSheet('Digite o Endere�o!', txtENDERECO)  then Abort;
    //Tratar quando � colocado tipo endere�o no endere�o.
  //Ex.: Rua Marab�, Avenida Jos� Cobra
  if (Pos('RUA ',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('R.',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('AVENIDA ',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('AV.',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('ESTRADA ',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('PRA�A ',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('PRACA ',Trim(QCadastroENDERECO.AsString)) > 0) or
   (Pos('RODOVIA ',Trim(QCadastroENDERECO.AsString)) > 0)
  then
  begin
    MsgInf('N�o � permitido colocar o tipo do endere�o no campo endere�o!');
    PageControl2.ActivePageIndex := 0;
    QCadastroENDERECO.AsString := '';
    txtENDERECO.SetFocus;
    Abort;
  end;
  if Trim(lkpFilial.Text) = '' then begin
    MsgInf('Selecione uma filial para a empresa!');
    lkpFilial.SetFocus;
    Abort;
  end;
  //Estado
  if Trim(QCadastroESTADO.AsString) = '' then
  begin
    MsgInf('Escolha o Estado!');
    lkpESTADO.SetFocus;
    Abort;
  end;
  //Cidade
  if Trim(QCadastroCIDADE.AsString) = '' then
  begin
    MsgInf('Escolha a Cidade!');
    lkpCIDADE.SetFocus;
    Abort;
  end;
  //Bairro
  if Trim(QCadastroBAIRRO.AsString) = '' then
  begin
    MsgInf('Escolha o Bairro!');
    dbLkpBairros.SetFocus;
    Abort;
  end;
  //Nome do cart�o
  if Trim(QCadastroNOMECARTAO.AsString) = '' then
    QCadastroNOMECARTAO.AsString:= copy(QCadastroNOME.AsString,1,45);
  //Data de Fechamento
  if fnVerfCompVazioEmTabSheet('Digite a data de fechamento!', dbEdtDiaFechamento) then Abort;
  //Data de Vencimento
  if fnVerfCompVazioEmTabSheet('Digite a data de vencimento!', dbEdtDiaVenc)       then Abort;
  if QCadastroCONTRATO.AsString = '' then
    QCadastroCONTRATO.AsInteger:= QCadastroEMPRES_ID.AsInteger;
  //CNPJ
  cnpj := Trim(StringReplace(StringReplace((StringReplace(Trim(dbEdtCNPJ.Text),'.','',[rfReplaceAll, rfIgnoreCase])),'/','',[rfReplaceAll, rfIgnoreCase]),'-','',[rfReplaceAll, rfIgnoreCase]));
  if Trim(cnpj) <> '' then
  begin
    if not ValidaCNPJ(cnpj) then
      begin
        MsgErro('CNPJ inv�lido!');
        dbEdtCNPJ.SetFocus;
        Abort;
      end;
  end
  else begin
    MsgErro('CNPJ n�o pode ser vazio');
    dbEdtCNPJ.SetFocus;
    Abort;
  end;

  if DBCheckBox36.Checked then
  begin
    if edtTaxaAdmEmp.Text = '' then
    begin
      MsgErro('� obrigat�rio definir o valor da taxa administrativa!');
      edtTaxaAdmEmp.SetFocus;
      Exit;
    end;
  end;
  //Email
  if Trim(dbEdtEmail.Text) <> '' then
    if not fnIsEmail(dbEdtEmail.Text) then
      begin
      MsgErro('E-mail inv�lido!');
      Abort;
      end;

  if DBCheckBox24.Checked then
  begin
    if Trim(edtLimiteMax.Text) = '' then begin
       MsgErro('Especifique o Limite de Cr�dito M�ximo por Conveniado!');
      edtLimiteMax.SetFocus;
      Abort;
    end;
  end;

  if Trim(txtENDERECO.Text) = '' then begin
    MsgErro('d');
    dbLkpRespFechamento.SetFocus;
    Abort;
  end;

  if lkpFilial.LookupDisplayIndex = -1 then begin
    MsgErro('Selecione uma filial para a empresa!');
    lkpFilial.SetFocus;
    Abort;
  end;

  //FILIAL
  if Trim(lkpFilial.Text) = '' THEN begin
    MsgErro('Selecione uma filial para a empresa!');
    lkpFilial.SetFocus;
    Abort;
  end;



  //Bandeira
  if Trim(dbLkpCbBandeiras.Text) = '' then begin
    MsgErro('Selecione uma bandeira para a empresa!');
    dbLkpCbBandeiras.SetFocus;
    Abort;
  end;
  if Trim(dbLkpCbBandeiras.Text) = '' then begin
    MsgErro('Selecione uma bandeira para a empresa!');
    dbLkpCbBandeiras.SetFocus;
    Abort;
  end;
  if (cbbTipoCredito.ItemIndex < 0) then begin
    MsgErro('Selecione uma forma de cr�dito para a empresa!');
    PageControl2.ActivePageIndex := 0;
    cbbTipoCredito.SetFocus;
    Abort;
  end;
  if (QCadastroDIA_REPASSE.AsInteger < 1) then begin
    MsgErro('Selecione uma data v�lida!');
    PageControl2.ActivePageIndex := 0;
    dbEdtDiaRepasse.SetFocus;
    Abort;
  end;

  if Trim(dbLkpRespFechamento.Text) = '' then begin
    MsgErro('Selecione o respons�vel pelo fechamento!');
    dbLkpRespFechamento.SetFocus;
    Abort;
  end;

  if Trim(JvDBLookupCombo2.Text) = '' then begin
    MsgErro('Selecione um modelo de cart�o!');
    JvDBLookupCombo2.SetFocus;
    Abort;
  end;

  if Trim(QCadastroCGC.AsString) = '' then
  begin
     MsgErro('O campo CNPJ � obrigat�rio!');
     DBEdit15.SetFocus;
     Exit;
  end;
  Result := True;
end;

procedure TFCadEmp.TabSaldoHide(Sender: TObject);
begin
  inherited;
  labinfo.Caption := '';
  QSaldoEmp.Close;
end;

procedure TFCadEmp.AbreDatas;
begin
  QDatasFecha.Close;
  if not QCadastro.IsEmpty then
  begin
    QDatasFecha.SQL.Text := ' Select * from dia_fecha where empres_id = '+QCadastroEMPRES_ID.AsString+' and (year(data_fecha)) = '+CBAno.Text;
    QDatasFecha.Open;
  end;
end;

procedure TFCadEmp.CBAnoChange(Sender: TObject);
begin
  inherited;
  AbreDatas;
end;

procedure TFCadEmp.CriarDatas(MesInicial,AnoInicial:Integer;MostrarMSG:Boolean) ;
var ano, mes : integer; data : TDateTime;
begin
  //Exclui as datas para que sejam recriadas as novas
  data := EncodeDate(AnoInicial,MesInicial,1);
  DMConexao.ExecuteSql('delete from dia_fecha where empres_id = '+QCadastroEMPRES_ID.AsString+' and DATA_FECHA >= '+FormatDateIB(data));

  QDatasFecha.Close;
  QDatasFecha.SQL.Clear;
  QDatasFecha.SQL.Add(' select * from dia_fecha where empres_id = '+QCadastroEMPRES_ID.AsString);
  QDatasFecha.Open;

  Screen.Cursor := crHourGlass;
  QDatasFecha.Close;
  QDatasFecha.SQL.Text := 'select * from dia_fecha where empres_id = '+QCadastroEMPRES_ID.AsString;
  QDatasFecha.Open;
  AlterandoTodasDatas := True;
  DMConexao.AdoCon.BeginTrans;
  try
    for ano := AnoInicial to 2024 do
    begin
      if ano = AnoInicial then
      begin
        for mes := MesInicial to 12 do
        begin
          QDatasFecha.Append;
          QDatasFechaEMPRES_ID.AsInteger   := QCadastroEMPRES_ID.AsInteger;
          QDatasFechaDATA_FECHA.AsDateTime := CriarData(ano,mes,QCadastroFECHAMENTO1.AsInteger);
          QDatasFechaDATA_VENC.AsDateTime  := CriarData(ano,mes,QCadastroVENCIMENTO1.AsInteger);
          if not ChVencnomes.Checked then
             QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,StrToInt(EdMesesVenc.Text));
          if QDatasFechaDATA_VENC.AsDateTime <= QDatasFechaDATA_FECHA.AsDateTime then
             QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,1);
          QDatasFecha.Post;
          if ChkUsa2Fecha.Checked then
          begin
            QDatasFecha.Append;
            QDatasFechaEMPRES_ID.AsInteger := QCadastroEMPRES_ID.AsInteger;
            QDatasFechaDATA_FECHA.AsDateTime := CriarData(ano,mes,QCadastroFECHAMENTO2.AsInteger);
            QDatasFechaDATA_VENC.AsDateTime  := CriarData(ano,mes,QCadastroVENCIMENTO2.AsInteger);
            if not ChVencnomes2.Checked then
              QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,StrToInt(EdMesesVenc2.Text));
            if QDatasFechaDATA_VENC.AsDateTime <= QDatasFechaDATA_FECHA.AsDateTime then
              QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,1);
            QDatasFecha.Post;
          end;
        end;
      end
      else
      begin
        for mes := 01 to 12 do
        begin
          QDatasFecha.Append;
          QDatasFechaEMPRES_ID.AsInteger   := QCadastroEMPRES_ID.AsInteger;
          QDatasFechaDATA_FECHA.AsDateTime := CriarData(ano,mes,QCadastroFECHAMENTO1.AsInteger);
          QDatasFechaDATA_VENC.AsDateTime  := CriarData(ano,mes,QCadastroVENCIMENTO1.AsInteger);
          if not ChVencnomes.Checked then
             QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,StrToInt(EdMesesVenc.Text));
          if QDatasFechaDATA_VENC.AsDateTime <= QDatasFechaDATA_FECHA.AsDateTime then
             QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,1);
          QDatasFecha.Post;
          if ChkUsa2Fecha.Checked then
          begin
            QDatasFecha.Append;
            QDatasFechaEMPRES_ID.AsInteger := QCadastroEMPRES_ID.AsInteger;
            QDatasFechaDATA_FECHA.AsDateTime := CriarData(ano,mes,QCadastroFECHAMENTO2.AsInteger);
            QDatasFechaDATA_VENC.AsDateTime  := CriarData(ano,mes,QCadastroVENCIMENTO2.AsInteger);
            if not ChVencnomes2.Checked then
              QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,StrToInt(EdMesesVenc2.Text));
            if QDatasFechaDATA_VENC.AsDateTime <= QDatasFechaDATA_FECHA.AsDateTime then
              QDatasFechaDATA_VENC.AsDateTime := IncMonth(QDatasFechaDATA_VENC.AsDateTime,1);
            QDatasFecha.Post;
          end;
        end;
      end;
    end;
    {
    if QCadastro.FieldByName('FECHAMENTO1').AsInteger <> StrToInt(DBEdit30.Text) then
    begin
      QCadastro.Edit;
      QCadastro.FieldByName('FECHAMENTO1').AsInteger:= StrToInt(DBEdit30.Text);
      QCadastro.Post;
    end;
    if QCadastro.FieldByName('VENCIMENTO1').AsInteger <> StrToInt(DBEdit31.Text) then
    begin
      QCadastro.Edit;
      QCadastro.FieldByName('VENCIMENTO1').AsInteger:= StrToInt(DBEdit31.Text);
      QCadastro.Post;
    end;
    }
    DMConexao.AdoCon.CommitTrans;
  except
    on E:Exception do
    begin
      AlterandoTodasDatas := False;
      DMConexao.AdoCon.RollbackTrans;
      MsgErro('Erro ao atualizar datas, erro: '+E.Message+sLineBreak+'Opera��o Cancelada!');
    end;
  end;
  AlterandoTodasDatas := False;
  QDatasFecha.Close;
  //AtualizarDatasCC(MostrarMSG);
  Screen.Cursor := crDefault;
end;

function TFCadEmp.CriarData(ano,mes,dia:integer):TDateTime;
begin
  if dia <= DaysInAMonth(ano,mes) then
    Result := EncodeDate(ano,mes,dia)
  else
    Result := EncodeDate(ano,mes,DaysInAMonth(ano,mes));
end;

procedure TFCadEmp.AtualizaDataEmp(PedirConfirmacao:Boolean);
var ano, mes,dia : word;
FechamentoCorrenteEmAberto:TDateTime;
dataCorrente : TDateTime;
begin
  if PedirConfirmacao and (not MsgSimNao('Confirma a altera��o das datas de fechamento e vencimento da empresa?')) then
    exit;
  Screen.Cursor := crHourGlass;

  if DMConexao.ObterUltimoFechamentoEmp(QCadastroEMPRES_ID.AsInteger,FechamentoCorrenteEmAberto) then begin
    if PedirConfirmacao then begin
      if MsgSimNao('Essa empresa possu� fatura para o fechamento de '+FormatDataBR(FechamentoCorrenteEmAberto)+sLineBreak+'N�o ser� possivel alterar datas de fechamento menores ou igual a essa data.'+sLineBreak+'Deseja Alterar as datas a partir desta?') then begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select top(1) data_fecha from dia_fecha where data_fecha > getdate() and empres_id = '+QCadastroEMPRES_ID.AsString);
        DMConexao.AdoQry.Open;
        FechamentoCorrenteEmAberto := DMConexao.AdoQry.Fields[0].AsDateTime;
        dataCorrente := Now;
        if FechamentoCorrenteEmAberto > dataCorrente then
        begin
          //  Dia que estou mudando � <= Dia do fechamento corrente aberto
          if (QCadastroFECHAMENTO1.Value <= Integer(DayOf(FechamentoCorrenteEmAberto))) then
          begin
            if(MonthOf(FechamentoCorrenteEmAberto) <= MonthOf(dataCorrente)) then
              FechamentoCorrenteEmAberto := IncMonth(FechamentoCorrenteEmAberto);
          end;
        end;
        dia := DayOf(FechamentoCorrenteEmAberto);
        ano := YearOf(FechamentoCorrenteEmAberto);
        mes := MonthOf(FechamentoCorrenteEmAberto);
        CriarDatas(mes,ano,PedirConfirmacao);
      end else begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end else begin
      FechamentoCorrenteEmAberto := IncMonth(FechamentoCorrenteEmAberto,1);
      ano := YearOf(FechamentoCorrenteEmAberto);
      mes := MonthOf(FechamentoCorrenteEmAberto);
      CriarDatas(mes,ano,PedirConfirmacao);
    end;
  end
  //Verifica na tabela data_Fecha
  else
    begin
      if ((DMConexao.ExecuteQuery('SELECT top(1) data_fecha FROM dia_fecha where data_fecha > getdate() and empres_id = '+QCadastroEMPRES_ID.AsString)) <> Null) then
      begin
        FechamentoCorrenteEmAberto := DMConexao.ExecuteQuery('SELECT top(1) data_fecha FROM dia_fecha where data_fecha > getdate() and empres_id = '+QCadastroEMPRES_ID.AsString);
        dataCorrente := Now;
        if FechamentoCorrenteEmAberto > dataCorrente then
        begin
          //  Dia que estou mudando � <= Dia do fechamento corrente aberto
          if (QCadastroFECHAMENTO1.Value <= Integer(DayOf(FechamentoCorrenteEmAberto))) then
          begin
            if(MonthOf(FechamentoCorrenteEmAberto) <= MonthOf(dataCorrente)) then
              FechamentoCorrenteEmAberto := IncMonth(FechamentoCorrenteEmAberto);
          end;
        end;

        mes := MonthOf(FechamentoCorrenteEmAberto);
        ano := YearOf(Date);
      end
      else begin
        mes := 01;
        ano := 2004;
      end;
      CriarDatas(mes,ano,PedirConfirmacao);

    end;
    Screen.Cursor := crDefault;
  //if PedirConfirmacao then //Quando pedi confirma��o foi a altera��o foi solicitada pelo usu�rio.
    //ShowMessage('Datas atualizadas com sucesso.');

end;
{procedure TFCadEmp.AtualizarDatasCC(MostrarMSG:Boolean);
begin
  if QDatasFecha.State = dsEdit then
    QDatasFecha.Post;
  try
    if MostrarMSG then
    begin
      if DMConexao.ExecuteScalar('select count(*) from contacorrente where conv_id in (select conv_id from conveniados where empres_id = '+QCadastroEMPRES_ID.AsString+')',0)>0 then
      begin
        MsgInf('Aten��o, as datas da contacorrente dos conveniados dessa empresa ser�o alteradas.');
      end;
    end;
    Screen.Cursor := crHourglass;
    DMConexao.ExecuteSql(' Update contacorrente set data_fecha_emp = '+
                   ' (Select datafecha from GET_PROX_FECHA_ABERTO(contacorrente.data,contacorrente.conv_id,'+QCadastroEMPRES_ID.AsString+' ) ) '+
                   '  where coalesce(fatura_id,0) = 0 and conv_id in (select conv_id from conveniados where empres_id = '+QCadastroEMPRES_ID.AsString+')');
    AtualizarVencimentos;
  except on E:Exception do
    ShowMessage('Erro ao atualizar as datas. erro: '+E.Message);
  end;
end; }

procedure TFCadEmp.AtualizarVencimentos;
begin
    DMConexao.ExecuteSql(' Update contacorrente set data_venc_emp = '+
                   ' (select dia_fecha.data_venc from dia_fecha where dia_fecha.empres_id = '+QCadastroEMPRES_ID.AsString+
                   '   and dia_fecha.data_fecha = contacorrente.data_fecha_emp) '+
                   '  where coalesce(fatura_id,0) = 0 and conv_id in (select conv_id from conveniados where empres_id = '+QCadastroEMPRES_ID.AsString+')');
end;

procedure TFCadEmp.DBCart_IniExit(Sender: TObject);
begin
  if Trim(QCadastroCARTAO_INI.AsString) = '' then
  begin
    if not (QCadastro.State in [dsEdit,dsInsert]) then QCadastro.Edit;
      QCadastroCARTAO_INI.AsString := PadR(QCadastroEMPRES_ID.AsString,9,'0');
  end;
  if QCadastroCARTAO_INI.AsString[1] = '0' then
  begin
    ShowMessage('O n�mero inicial dos cart�es n�o pode ser 0(zero)!');
    DBCart_Ini.SetFocus;
  end;
end;

procedure TFCadEmp.DBCheckBox7Click(Sender: TObject);
begin
  inherited;
  DBCart_Ini.Enabled := DBCheckBox7.Checked;
end;

procedure TFCadEmp.DBCart_IniEnter(Sender: TObject);
begin
  inherited;
  DBCart_Ini.Text := StringReplace(DBCart_Ini.Text,'.','',[rfReplaceAll]);
end;

procedure TFCadEmp.EdMesesVencKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFCadEmp.ChVencnomesClick(Sender: TObject);
begin
  EdMesesVenc.Enabled := not ChVencnomes.Checked;
end;

procedure TFCadEmp.TabSheet4Show(Sender: TObject);
begin
  if QCadastro.IsEmpty then
    TabSheet4.Enabled := False
  else
    TabSheet4.Enabled := True;
  if QCadastro.FieldByName('FECHAMENTO1').AsInteger >= QCadastro.FieldByName('VENCIMENTO1').AsInteger then
    ChVencnomes.Checked:= False;
end;

procedure TFCadEmp.QDatasFechaBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  ShowMessage('N�o � poss�vel excluir.');
  SysUtils.Abort;
end;

procedure TFCadEmp.QDatasFechaAfterPost(DataSet: TDataSet);
var fechamento_anterior : TDateTime;
begin
  inherited;
  if not AlterandoTodasDatas then
  begin
    if Fechamento_Old <> QDatasFechaDATA_FECHA.AsDateTime then
    begin
      MsgInf('O sistema ir� atualizar as autoriza��es correspondentes ao fechamento alterado.');
      Screen.Cursor := crHourGlass;
      DMConexao.ExecuteSql('update contacorrente set data_fecha_emp = '+
                     ' '+FormatDataIB(QDatasFechaDATA_FECHA.Value)+
                     ' where data_fecha_emp = '+FormatDataIB(Fechamento_Old)+
                     ' and empres_id = '+QCadastroEMPRES_ID.AsString);
      AtualizarVencimentos;
      MsgInf('Altera��o Conclu�da com sucesso!');
      Screen.Cursor := crDefault;
    end
    else if Vencimento_Old <> QDatasFechaDATA_VENC.AsDateTime then
    begin
      MsgInf('O sistema ir� atualizar as autoriza��es correspondentes ao vencimento alterado.');
      AtualizarVencimentos;
      MsgInf('Altera��o Conclu�da com sucesso!');
    end;
  end;
  {else begin
    fechamento_anterior := DMConexao.ExecuteQuery('SELECT TOP(1) DATA_FECHA FROM DIA_FECHA WHERE DATA_FECHA > GETDATE() AND EMPRES_ID = '+QCadastroEMPRES_ID.AsString);
    if fechamento_anterior <> QDatasFechaDATA_FECHA.AsDateTime then
    begin
      MsgInf('O sistema ir� atualizar as autoriza��es correspondentes ao fechamento alterado.');
      Screen.Cursor := crHourGlass;
      DMConexao.ExecuteSql('update contacorrente set data_fecha_emp = '+
                     ' '+FormatDataIB(QDatasFechaDATA_FECHA.Value)+
                     ' where data_fecha_emp >= '+FormatDataIB(fechamento_anterior)+
                     ' and empres_id = '+QCadastroEMPRES_ID.AsString);
      AtualizarVencimentos;
      Screen.Cursor := crDefault;
    end
    else if Vencimento_Old <> QDatasFechaDATA_VENC.AsDateTime then
    begin
      MsgInf('O sistema ir� atualizar as autoriza��es correspondentes ao vencimento alterado.');
      AtualizarVencimentos;
    end;
  end; }
  end;


procedure TFCadEmp.DSCadastroStateChange(Sender: TObject);
begin
  inherited;
  dbEdtDiaFechamento.ReadOnly := not(QCadastro.State = dsInsert);
  dbEdtDiaVenc.ReadOnly := not(QCadastro.State = dsInsert);
end;

procedure TFCadEmp.QCadastroAfterPost(DataSet: TDataSet);
begin
  inherited;
  {if QCadastro.Tag = 0 then
  begin
    Try
      if(Griddatas.DataSource.DataSet.IsEmpty)then
      begin
        CriarDatas(1,2004,True);
        QCadastro.Tag := 0;
      end
    except
      on E:Exception do
        raise Exception.Create('Erro ao criar datas da empresa, erro: '+E.Message);
    end;
  end;

  {DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('UPDATE empresas SET DTALTERACAO = current_timestamp, OPERADOR = '''+Operador.Nome+''' where empres_id = '+QCadastroEMPRES_ID.AsString);
  DMConexao.AdoQry.ExecSQL; }




end;

{procedure TFCadEmp.Grid_GrupoTitleClick(Column: TColumn);
begin
  inherited;
  DMConexao.SortZQuery(QGrupo_conv_emp,Column.FieldName);
end;}

procedure TFCadEmp.TabGruposShow(Sender: TObject);
begin
  inherited;
  QGrupo_conv_emp.Close;
  if not QCadastro.IsEmpty then
  begin
    QGrupo_conv_emp.Parameters.ParamByName('empres_id').Value:= QCadastroEMPRES_ID.AsInteger;
    QGrupo_conv_emp.Open;
  end;
end;

{procedure TFCadEmp.QGrupo_conv_empBeforeDelete(DataSet: TDataSet);
var existe : boolean;
begin
  inherited;
  DMConexao.Query1.Close;
  DMConexao.Query1.Sql.Text := 'Select first 1 conv_id from conveniados where empres_id = '+QCadastroEMPRES_ID.AsString+' and grupo_conv_emp = '+QGrupo_conv_empGRUPO_CONV_EMP_ID.AsString;
  DMConexao.Query1.Open;
  existe := not DMConexao.Query1.IsEmpty;
  DMConexao.Query1.Close;
  if existe then
  begin
    Application.MessageBox('Existem conveniados vinculados a esse grupo, a exclus�o n�o ser� permitida.','Aten��o',MB_OK+MB_ICONERROR);
    Sysutils.Abort;
  end
  else
    if Application.MessageBox(PChar('Confirma a exclus�o deste grupo ?'+#13+QGrupo_conv_empDESCRICAO.AsString),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDNo then
      Sysutils.Abort;
end; }

procedure TFCadEmp.TabGruposHide(Sender: TObject);
begin
  inherited;
  QGrupo_conv_emp.Close;
end;

procedure TFCadEmp.QGrupo_conv_empBeforePost(DataSet: TDataSet);
begin
  inherited;
  if Trim(QGrupo_conv_empDESCRICAO.AsString) = '' then
  begin
    ShowMessage('Informe a descri��o.');
    Grid_Grupo.SetFocus;
    Grid_Grupo.Col := 1;
    SysUtils.Abort;
  end;
  QGrupo_conv_empDESCRICAO.AsString  := AnsiUpperCase(QGrupo_conv_empDESCRICAO.AsString);
  QGrupo_conv_empEMPRES_ID.AsInteger := QCadastroEMPRES_ID.AsInteger;
end;

procedure TFCadEmp.AlterarIddaempresa1Click(Sender: TObject);
var empnew : string;
begin
  inherited;
  if MsgSimNao('Deseja alterar o c�digo da empresa?') then
    empnew := SoNumero( InputBox('Digite o novo Id para a empresa','Novo ID','') );
    if Trim(empnew) <> '' then
    begin
      TrocarEmpres_ID(strtoint(empnew));
    end;
end;

procedure TFCadEmp.PopupEmpres_IDPopup(Sender: TObject);
begin
  inherited;
  PopupEmpres_ID.Items[0].Enabled := not QCadastro.IsEmpty;
end;

procedure TFCadEmp.TrocarEmpres_ID(new_EmpresId:integer);
var existe : boolean; nome:string;
  old_empresID : string;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.Sql.Text := 'Select nome from empresas where empres_id = '+IntToStr(new_EmpresId);
  DMConexao.AdoQry.Open;
  existe := not DMConexao.AdoQry.IsEmpty;
  nome := DMConexao.AdoQry.Fields[0].AsString;
  DMConexao.AdoQry.Close;
  if existe then begin
     MsgErro('Ja existe uma empresa cadastrada com este Id, Nome: '+nome);
  end
  else begin
    Screen.Cursor := crHourGlass;
    DMConexao.AdoCon.BeginTrans;
    try
      old_empresID := QCadastroEMPRES_ID.AsString;
      QCadastro.Edit;
      QCadastroEMPRES_ID.AsInteger := new_EmpresId;
      QCadastro.Post;
      with DMConexao do
      begin
        //Atualiza todas tabelas que usam empres_id.
        AdoQry.SQL.Text := 'update back_alt_linear set chave_prim = '+IntToStr(new_EmpresId)+' where chave_prim = '+old_empresID+' and tabela = "EMPRESAS" ';
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'update cad_conferencia set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'update conferencia set empres_id = '+inttostr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'ALTER TRIGGER CONVENIADOS_ATUALIZA_CC INACTIVE';
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'update conveniados set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'ALTER TRIGGER CONVENIADOS_ATUALIZA_CC ACTIVE';
        AdoQry.ExecSQL;
        AdoQry.Sql.Text := 'update dia_fecha set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.Sql.Text := 'update empres_seg_lib set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.Sql.Text := 'update grupo_conv_emp set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.Sql.Text := 'update relac_bol_emp_conv set empres_id = '+IntToStr(new_EmpresId)+' where empres_id = '+old_empresID;
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'update logs set id = '+IntToStr(new_EmpresId)+' where id = '+old_empresID+' and cadastro = "Cadastro de Empresas" ';
        AdoQry.ExecSQL;
        AdoQry.SQL.Text := 'update fatura set id = '+IntToStr(new_EmpresId)+' where id = '+old_empresID+' and coalesce(tipo,"E") = "E" ';
        AdoQry.ExecSQL;
      end;
      DMConexao.AdoCon.CommitTrans;
      Screen.Cursor := crDefault;
      MsgInf('Altera��o Conclu�da com sucesso!');
    except
      on e:Exception do
      begin
        QCadastro.Edit;
        QCadastroEMPRES_ID.AsString := old_empresID;
        QCadastro.Post;
        DMConexao.AdoCon.RollbackTrans;
        Screen.Cursor := crDefault;
        MsgErro('Problemas na altera��o do ID, erro: '+e.Message);
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFCadEmp.QGrupo_ProdAfterPost(DataSet: TDataSet);
var Sql, sqlQuery, campo, vvelho, vnovo: String;
  RegAtual : TBookmark;
begin
  inherited;
  QGrupo_Prod.DisableControls;
  RegAtual := QGrupo_Prod.GetBookmark;
  if QGrupo_ProdFIDELIDADE.NewValue <> QGrupo_ProdFIDELIDADE.OldValue then
  begin
    if QGrupo_ProdFIDE_ID.IsNull then
      sql :=  'insert into fidel_grupo (EMPRES_ID,GRUPO_PROD_ID) values('+QCadastroEMPRES_ID.AsString+','+QGrupo_ProdGRUPO_PROD_ID.AsString+')'
    else
      sql :=  'delete from fidel_grupo where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and GRUPO_PROD_ID = '+QGrupo_ProdGRUPO_PROD_ID.AsString;

    DMConexao.ExecuteSql(sql);
    campo:= 'Fidelidade';
    vvelho:= UpperCase(QGrupo_ProdFIDELIDADE.OldValue);
    vnovo:= UpperCase(QGrupo_ProdFIDELIDADE.NewValue);
    sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
               ' OPERACAO, DATA_HORA, ID) values ('+
               ' gen_id(gen_log_id,1), "FCadEmp", "'+campo+'", "'+vvelho+'", "'+vnovo+'", "'+Operador.Nome+'"'+
               ', "Altera��o", current_timestamp, "'+QCadastroEMPRES_ID.AsString+'")';

    DMConexao.ExecuteSql(sqlQuery);
  end;

  if QGrupo_ProdREMP_GRUPO_PROD_ID.IsNull then
  begin
    sql :=  'insert into rel_emp_grupo_prod(EMPRES_ID,GRUPO_PROD_ID,DESCONTO,PRECO_FABRICA,LIBERADO) ';
    sql := sql + ' values('+QCadastroEMPRES_ID.AsString+','+QGrupo_ProdGRUPO_PROD_ID.AsString+','+FormatDimIB(QGrupo_ProdDESCONTO.AsCurrency)+',"'+UpperCase(QGrupo_ProdPRECO_FABRICA.AsString) +'","'+UpperCase(QGrupo_ProdLIBERADO.AsString) +'")';
  end
  else
  begin
    sql :=  'update rel_emp_grupo_prod set DESCONTO = '+FormatDimIB(QGrupo_ProdDESCONTO.AsCurrency);
    sql :=  sql + ', PRECO_FABRICA = "'+UpperCase(QGrupo_ProdPRECO_FABRICA.AsString) +'"';
    sql :=  sql + ', LIBERADO = "'+UpperCase(QGrupo_ProdLIBERADO.AsString) +'"';
    sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and GRUPO_PROD_ID = '+QGrupo_ProdGRUPO_PROD_ID.AsString;

    if QGrupo_ProdDESCONTO.NewValue <> QGrupo_ProdDESCONTO.OldValue then
    begin
      campo:= 'Desconto';
      vvelho:= QGrupo_ProdDESCONTO.OldValue;
      vnovo:= QGrupo_ProdDESCONTO.NewValue;
      sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                 ' OPERACAO, DATA_HORA, ID) values ('+
                 ' gen_id(gen_log_id,1), "FCadEmp", "'+campo+'", "'+vvelho+'", "'+vnovo+'", "'+Operador.Nome+'"'+
                 ', "Altera��o", current_timestamp, "'+QCadastroEMPRES_ID.AsString+'")';

      DMConexao.ExecuteSql(sqlQuery);
    end;
    if QGrupo_ProdLIBERADO.NewValue <> QGrupo_ProdLIBERADO.OldValue then
    begin
      campo:= 'Liberado';
      vvelho:= UpperCase(QGrupo_ProdLIBERADO.OldValue);
      vnovo:= UpperCase(QGrupo_ProdLIBERADO.NewValue);
      sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                 ' OPERACAO, DATA_HORA, CADASTRO, ID,  DETALHE) values ('+
                 ' gen_id(gen_log_id,1), "FCadEmp", "'+campo+'", "'+vvelho+'", "'+vnovo+'", "'+Operador.Nome+'"'+
                 ', "Altera��o", current_timestamp, "Desconto em Grupo de Produto", "'+QCadastroEMPRES_ID.AsString+'")';

      DMConexao.ExecuteSql(sqlQuery);
    end;
    if QGrupo_ProdPRECO_FABRICA.NewValue <> QGrupo_ProdPRECO_FABRICA.OldValue then
    begin
      campo:= 'Pre�o de F�brica';
      vvelho:= UpperCase(QGrupo_ProdPRECO_FABRICA.OldValue);
      vnovo:= UpperCase(QGrupo_ProdPRECO_FABRICA.NewValue);
      sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                 ' OPERACAO, DATA_HORA, ID) values ('+
                 ' gen_id(gen_log_id,1), "FCadEmp", "'+campo+'", "'+vvelho+'", "'+vnovo+'", "'+Operador.Nome+'"'+
                 ', "Altera��o", current_timestamp, "'+QCadastroEMPRES_ID.AsString+'")';

      DMConexao.ExecuteSql(sqlQuery);
    end;
  end;
  if Sql <> '' then
    DMConexao.ExecuteSql(sql);
  QGrupo_Prod.Refresh;
  QGrupo_Prod.GotoBookmark(RegAtual);
  QGrupo_Prod.freebookmark(RegAtual);
  QGrupo_Prod.EnableControls;
end;

procedure TFCadEmp.TabSheet3Show(Sender: TObject);
begin
  inherited;
  CBAno.SetFocus;
end;

procedure TFCadEmp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  LogSegLIb.Free;
  LogDataFecha.Free;
  qListaCred.Close;
  FMenu.vCadEmp := False;
  inherited;
end;

procedure TFCadEmp.ChkUsa2FechaClick(Sender: TObject);
begin
  inherited;
  DBEdFecha2.Enabled := ChkUsa2Fecha.Checked;
  DBEdVenc2.Enabled  := ChkUsa2Fecha.Checked;
end;

procedure TFCadEmp.EdMesesVenc2Exit(Sender: TObject);
begin
  inherited;
  if Trim(TEdit(Sender).Text) = '' then
    TEdit(Sender).Text := '1';
end;

procedure TFCadEmp.EdMesesVencExit(Sender: TObject);
begin
  inherited;
  if (StrToInt(Trim(TEdit(Sender).Text)) > 1) Then
      TEdit(Sender).Text := '1';
end;

procedure TFCadEmp.ChVencnomes2Click(Sender: TObject);
begin
  inherited;
  EdMesesVenc2.Enabled := not ChVencnomes2.Checked;
end;

procedure TFCadEmp.QDatasFechaBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
  Fechamento_Old := QDatasFechaDATA_FECHA.AsDateTime;
  Vencimento_Old := QDatasFechaDATA_VENC.AsDateTime;
end;

procedure TFCadEmp.btnCancelGrupProdClick(Sender: TObject);
begin
  inherited;
  if QGrupo_Prod.State in [dsInsert,dsEdit] then
  begin
    QGrupo_Prod.Cancel;
  end;
end;

procedure TFCadEmp.DSGrupo_ProdStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelGrupProd = ActiveControl) or (btnGravaGrupProd = ActiveControl) then
    GridGrupo_Prod.SetFocus;
  btnCancelGrupProd.Enabled  := QGrupo_Prod.State in [dsEdit,dsInsert];
  btnGravaGrupProd.Enabled := QGrupo_Prod.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.GridGrupo_ProdDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
{  if QGrupo_ProdLIBERADO.AsString = 'N' then
  begin
    GridGrupo_Prod.Canvas.Font.Color := clRed; //Se estiver bloqueado fonte vermelha.
    if gdSelected in State then
    begin
      GridGrupo_Prod.Canvas.Brush.Color:= $00BFFFFF; //Se estiver selecionado fundo amarelo.
    end;
    GridGrupo_Prod.DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;}
end;

procedure TFCadEmp.TabVendaNomeShow(Sender: TObject);
begin
  inherited;
  panel14.Visible := False;
  SegLib.Close;
  if not QCadastro.IsEmpty then
  begin
    if QCadastroVENDA_NOME.AsString = 'S' then
    begin
      panel14.Visible := True;
    end
    else
    begin
      QVendaNome.Close;
      QVendaNome.Parameters.Items[0].Value:= QCadastroEMPRES_ID.AsInteger;
      QVendaNome.Open;
    end;
  end;
end;

procedure TFCadEmp.DSVendaNomeStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelVendaNome = ActiveControl) or (btnGravaVendaNome = ActiveControl) then
    GridVendaNome.SetFocus;
  btnCancelVendaNome.Enabled  := QVendaNome.State in [dsEdit,dsInsert];
  btnGravaVendaNome.Enabled := QVendaNome.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.QVendaNomeAfterInsert(DataSet: TDataSet);
begin
  inherited;
  if (ActiveControl = GridVendaNome) then
    QVendaNome.Cancel;
end;

procedure TFCadEmp.QVendaNomeAfterPost(DataSet: TDataSet);
var sql, sqlLog: String;
  reg_atual : TBookmark;
begin
  inherited;
  sql:= '';
  sqlLog:= '';
  if (QVendaNomeLIBERADO.AsString = 'S') or (QVendaNomeLIBERADO.AsString = 's') then
  begin
    if QVendaNomeLIBERADO.OldValue = 'N' then
    begin
      sql :=  'insert into EMPRES_VENDA_NOME (EMPRES_ID, CRED_ID, LIBERADO) '+
              ' values('+QCadastroEMPRES_ID.AsString+','+QVendaNomeCRED_ID.AsString+',"'+
              UpperCase(QVendaNomeLIBERADO.AsString)+'")';
      DMConexao.GravaLog('FCadEmp','Venda por Nome','Fornec. ('+QVendaNomeCRED_ID.AsString+') bloqueado','Fornec. ('+QVendaNomeCRED_ID.AsString+') liberado',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
    end;
  end
  else
  begin
    if QVendaNomeLIBERADO.OldValue = 'S' then
    begin
      sql :=  'delete from EMPRES_VENDA_NOME ';
      sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and CRED_ID = '+QVendaNomeCRED_ID.AsString;
      DMConexao.GravaLog('FCadEmp','Venda por Nome','Fornec. ('+QVendaNomeCRED_ID.AsString+') liberado','Fornec. ('+QVendaNomeCRED_ID.AsString+') bloqueado',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
    end;
  end;
  if sql <> '' then
  begin
    DMConexao.ExecuteSql(sql);
  end;
  QVendaNome.DisableControls;
  reg_atual := QVendaNome.GetBookmark;
  QVendaNome.Refresh;
  QVendaNome.GotoBookmark(reg_atual);
  QVendaNome.freebookmark(reg_atual);
  QVendaNome.EnableControls;
end;

procedure TFCadEmp.GridVendaNomeDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if QVendaNomeLIBERADO.AsString = 'N' then
  begin
    GridVendaNome.Canvas.Font.Color := clRed; //Se estiver bloqueado fonte vermelha.
    if gdSelected in State then
    begin
      GridVendaNome.Canvas.Brush.Color:= $00BFFFFF; //Se estiver selecionado fundo amarelo.
    end;
    GridVendaNome.DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TFCadEmp.TabVendaNomeHide(Sender: TObject);
begin
  inherited;
  QVendaNome.Close;
end;

procedure TFCadEmp.QCredLibAfterInsert(DataSet: TDataSet);
begin
  inherited;
  if (ActiveControl = GridCredLib) then
     QCredLib.Cancel;
end;

function TFCadEmp.ExisteCredBloq(empres_id: integer; cred_id: integer):boolean;
var teste : Variant;
begin
  {if}teste := DMConexao.ExecuteScalar(' select cred_id from emp_cred_Lib where cred_id = '+
    IntToStr(cred_id)+' and empres_id = '+IntToStr(empres_id));// = 0 then
    if(teste = null) then
    begin
      Result:= False
    end
    else
      Result:= True;
end;

procedure TFCadEmp.QCredLibAfterPost(DataSet: TDataSet);
var sql, sqlLog: String;
  cadastro, detalhe: string;
  reg_atual : TBookmark;
  newValue : String;
  oldValue : String;

begin
  inherited;
  oldValue := QCredLibliberado.AsString;
  newValue := cLiberado;
  sql:= '';
  sqlLog:= '';
  cadastro:= 'Cred. '+QCredLibCRED_ID.AsString+' lib. para esta empresa';
  detalhe := 'Empr ID';

  if ((newValue = 'N') or (newValue = 'n')) then
  begin
    if QCredLibLIBERADO.OldValue = 'S' then
    begin
      if ExisteCredBloq(QCadastroEMPRES_ID.AsInteger,QCredLibCRED_ID.AsInteger) then
      begin

        sql :=  'update EMP_CRED_LIB set liberado = '+QuotedStr(UpperCase(newValue))+
                ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+' and CRED_ID = '+QCredLibCRED_ID.AsString;
      end
      else
      begin

        sql :=  {'SET IDENTITY_INSERT EMP_CRED_LIB ON ' +}
                'insert into EMP_CRED_LIB(EMPRES_ID, CRED_ID, LIBERADO) '+
                ' values('+QCadastroEMPRES_ID.AsString+','+QCredLibCRED_ID.AsString+','+QuotedStr(newValue)+')';
      end;
      if UpperCase(QCredLibLIBERADO.OldValue) <> UpperCase(newValue) then
      begin
        DMConexao.GravaLog('FCadEmp','Cred. Bloq.','S','N',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
      end;
    end;
  end
  else if ((newValue = 'S') or (newValue = 's')) then
  begin
    if QCredLibLIBERADO.OldValue = 'N' then
    begin
      sql :=  'delete from EMP_CRED_LIB ';
      sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and CRED_ID = '+QCredLibCRED_ID.AsString;
    end;
    if UpperCase(QCredLibLIBERADO.OldValue) <> UpperCase(newValue) then
    begin
      DMConexao.GravaLog('FCadEmp','Cred. Bloq.','N','S',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
    end;
  end;
  if sql <> '' then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sql;
    //DMConexao.AdoQry.Open;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;
    //DMConexao.ExecuteSql(sql);
  end;
  QCredLib.DisableControls;
  reg_atual := QCredLib.GetBookmark;
  QCredLib.Requery();
  QCredLib.GotoBookmark(reg_atual);
  QCredLib.freebookmark(reg_atual);
  QCredLib.EnableControls;
  cLiberado := '';
end;

procedure TFCadEmp.TabCredLibShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    DBSegmento.KeyValue := 0;

    QCredLib.Close;
    QCredLib.SQL.Clear;
    QCredLib.SQL.Add(' Select cred.cred_id, cred.nome, coalesce(credlib.liberado,''S'') as liberado');
    QCredLib.SQL.Add(' from credenciados cred');
    QCredLib.SQL.Add(' left join emp_cred_lib credlib on cred.cred_id = credlib.cred_id');
    QCredLib.SQL.Add(' and ((credlib.empres_id = ' + QCadastroEMPRES_ID.AsString + ') or (credlib.empres_id is null)) ');
    if DBSegmento.KeyValue > 0 then
    begin
      QCredLib.SQL.Add(' inner join segmentos seg on seg.seg_id = cred.seg_id and seg.seg_id =' + DBSegmento.KeyValue);
    end;
    QCredLib.SQL.Add(' order by cred.nome');
    QCredLib.Open;
    //QCredLib.Close;
    //QCredLib.Parameters.Items[0].Value:= QCadastroEMPRES_ID.AsInteger;
    //QCredLib.Open;

    QEstabSeg.Close;
    QEstabSeg.Open;
  end;
end;

procedure TFCadEmp.TabCredLibHide(Sender: TObject);
begin
  inherited;
  QCredLib.Close;
end;

procedure TFCadEmp.DSCredLibStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelCredLib = ActiveControl) or (btnGravaCredLib = ActiveControl) then
     GridCredLib.SetFocus;
  btnCancelCredLib.Enabled  := QCredLib.State in [dsEdit,dsInsert];
  btnGravaCredLib.Enabled := QCredLib.State in [dsEdit,dsInsert];
end;

{procedure TFCadEmp.GridCredLibTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
  DMConexao.SortZQuery(Field.DataSet,Field.FieldName);
end;}

procedure TFCadEmp.qPbmAfterPost(DataSet: TDataSet);
var newValue : String;
begin
  inherited;
  newValue := cLiberado;
  if qPbm.FieldByName('PARTICIPA').OldValue <> UpperCase(NewValue) then
  begin
    if UpperCase(newValue) = 'S' then
    begin
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('select empres_id from prog_empr where prog_id = '+qPbm.FieldByName('PROG_ID').AsString+' and empres_id = '+QCadastro.FieldByName('EMPRES_ID').AsString);
      DMConexao.AdoQry.Open;
      if(DMConexao.AdoQry.Fields[0].Value = null)then
      begin
        DMConexao.ExecuteSql(' insert into prog_empr (prog_id, empres_id) values ('+qPbm.FieldByName('PROG_ID').AsString+', '+QCadastro.FieldByName('EMPRES_ID').AsString+') ');
        DMConexao.GravaLog('FCadEmp','Pbm','',qPbm.FieldByName('PROG_ID').AsString+' - '+qPbm.FieldByName('NOME').AsString,Operador.Nome,'Inclus�o',QCadastro.FieldByName('EMPRES_ID').AsString,Self.Name);
        
      end;
    end
    else
    begin
      DMConexao.ExecuteSql(' delete from prog_empr where prog_id = '+qPbm.FieldByName('PROG_ID').AsString+' and empres_id = '+QCadastro.FieldByName('EMPRES_ID').AsString);
      DMConexao.GravaLog('FCadEmp','Pbm',qPbm.FieldByName('PROG_ID').AsString+' - '+qPbm.FieldByName('NOME').AsString, '',Operador.Nome,'Exclus�o','FCadEmp',QCadastro.FieldByName('EMPRES_ID').AsString,Self.Name);
    end;
  end;
  qPbm.Requery();
  cLiberado := '';
end;

procedure TFCadEmp.qPbmBeforePost(DataSet: TDataSet);
begin
  inherited;
  qPbm.FieldByName('PARTICIPA').AsString:= UpperCase(qPbm.FieldByName('PARTICIPA').AsString);
  if qPbm.FieldByName('PARTICIPA').AsString <> 'S' then
  begin
    qPbm.FieldByName('PARTICIPA').AsString:= 'N';
  end;
end;

procedure TFCadEmp.dsPbmStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelPbm = ActiveControl) or (btnGravaPbm = ActiveControl) then
    GridPbm.SetFocus;
  btnCancelPbm.Enabled  := qPbm.State in [dsEdit,dsInsert];
  btnGravaPbm.Enabled := qPbm.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.GridPbmColExit(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsEdit] then qPbm.Post;
end;

procedure TFCadEmp.GridGrupo_ProdColExit(Sender: TObject);
begin
  inherited;
  if QGrupo_Prod.State in [dsEdit] then QGrupo_Prod.Post;
end;

procedure TFCadEmp.GridCredLibColExit(Sender: TObject);
begin
  inherited;
  if QCredLib.State in [dsEdit] then QCredLib.Post;
end;

procedure TFCadEmp.GriddatasColExit(Sender: TObject);
begin
  inherited;
  if QDatasFecha.State in [dsEdit] then QDatasFecha.Post;
end;

procedure TFCadEmp.dsProdBloqStateChange(Sender: TObject);
begin
  inherited;
  btnInclProdBloq.Enabled := (qProdBloq.State = dsBrowse) and Incluir;
  btnExclProdBloq.Enabled := (qProdBloq.State = dsBrowse) and Excluir and (not qProdBloq.IsEmpty);
end;

procedure TFCadEmp.DSDatasFechaStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelDataFecha = ActiveControl) or (btnGravaDataFecha = ActiveControl) then
    Griddatas.SetFocus;
  btnCancelDataFecha.Enabled  := QDatasFecha.State in [dsEdit,dsInsert];
  btnGravaDataFecha.Enabled := QDatasFecha.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.qPbmBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QGrupo_ProdBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QCredLibBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QGrupo_conv_empBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QCadastroBeforeEdit(DataSet: TDataSet);
begin
  inherited;

  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.qPbmBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QGrupo_ProdBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QCredLibBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QDatasFechaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QGrupo_conv_empBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QCadastroBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.TabConfigGrupoHide(Sender: TObject);
begin
  inherited;
  QGrupo_Prod.Close;
  qProdBloq.Close;
end;

procedure TFCadEmp.TabConfigGrupoShow(Sender: TObject);
var aux : integer;
begin

  inherited;
  QGrupo_Prod.Close;
  QGrupo_Prod.Parameters.ParamByName('empre').Value := QCadastroEMPRES_ID.Value;
  QGrupo_Prod.Parameters.ParamByName('emp').Value := QCadastroEMPRES_ID.Value;
  QGrupo_Prod.Open;
  qProdBloq.Close;
  qProdBloq.Parameters.ParamByName('empresa').Value := QCadastroEMPRES_ID.Value;
  qProdBloq.Open;
  GridGrupo_Prod.Enabled := (QCadastro.RecordCount > 0) and (QGrupo_Prod.RecordCount > 0);
  grdProdBloq.Enabled    := (QCadastro.RecordCount > 0);
end;

procedure TFCadEmp.TabConfigProgramaHide(Sender: TObject);
begin
  inherited;
  qPrograma.Close;
  qPbm.Close;
end;

procedure TFCadEmp.TabConfigProgramaShow(Sender: TObject);
begin
  inherited;
  qPbm.Close;
  qPbm.Parameters[0].Value := QCadastroEMPRES_ID.Value;
  qPbm.Open;
  qPrograma.Open;
end;

procedure TFCadEmp.EdCredIndKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TFCadEmp.EdCredIndChange(Sender: TObject);
begin
  inherited;
  if Trim(EdCredInd.Text) <> '' then begin
     if qListaCred.Locate('cred_id',EdCredInd.Text,[]) then
       cbbCredInd.KeyValue := EdCredInd.Text
     else
       cbbCredInd.ClearValue;
  end;
end;

procedure TFCadEmp.cbbCredIndChange(Sender: TObject);
begin
  inherited;
  if EdCredInd.Text <> cbbCredInd.KeyValue then
    EdCredInd.Text := string(cbbCredInd.KeyValue);
end;

procedure TFCadEmp.PageControl4Enter(Sender: TObject);
begin
  inherited;
  TabConfigGrupoShow(Self);
end;

procedure TFCadEmp.DSCadastroDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if not QCadastro.FieldByName('cred_id').IsNull then
    EdCredInd.Text := string(cbbCredInd.KeyValue)
  else
    EdCredInd.Text := '';
end;

procedure TFCadEmp.DBEdit34KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if key = #13 then PageControl2.ActivePageIndex := 1;
end;

procedure TFCadEmp.TabGrupoProdShow(Sender: TObject);
begin
  inherited;
  PageControl4.TabIndex:= 0;
end;

procedure TFCadEmp.SpeedButton1Click(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: TXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  if (txtCEP.Text = '') or (Length(SoNumero(txtCEP.Text)) <> 8) then
  begin
    Application.MessageBox('CEP nulo ou inv�lido.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
    exit;
  end;
  Resposta   := TStringStream.Create('');
  TSConsulta := TStringList.Create;
  IdHTTP1:= TIdHTTP.Create(Self);
  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
  TSConsulta.Values['&cep']  := SoNumero(txtCEP.Text);
  TSConsulta.Values['&formato']  := 'xml';
  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
  TSConsulta.Free;
  IdHTTP1.Free;
  XMLDocCEP:= TXMLDocument.Create(self);
  XMLDocCEP.Active := True;
  XMLDocCEP.Encoding := 'iso-8859-1';
  XMLDocCEP.LoadFromStream(Resposta);
  try
    try
      QCadastro.Edit;
      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
                                                    ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
      //QCadastro.FieldByName('BAIRRO').AsString      := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
      //QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
      //QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
    except
      ShowMessage('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
    end;
  finally
    Resposta.Free;
    XMLDocCEP.Active := False;
    XMLDocCEP.Free;
  end;
  //DBEdit27.SetFocus;
end;

procedure TFCadEmp.btnIncGrupEmpClick(Sender: TObject);
begin
  inherited;
  if fnVerfCampoVazio('Voc� precisa selecionar uma Empresa para incluir um Grupo!' , QCadastroEMPRES_ID ) then Abort;
  prVerfEAbreCon(QGrupo_conv_emp);
  QGrupo_conv_emp.Append;
end;

procedure TFCadEmp.btnCancelDataFechaClick(Sender: TObject);
begin
  inherited;
  if QDatasFecha.State in [dsInsert,dsEdit] then
  begin
    QDatasFecha.Cancel;
  end;
end;

procedure TFCadEmp.btnAltDataFechaClick(Sender: TObject);
var mes, ano, i: Integer  ;
var CurrAno, CurrMes, CurrDia : Word;
begin
  inherited;
   if (((ChVencnomes.Checked) and (QCadastroFECHAMENTO1.AsInteger > QCadastroVENCIMENTO1.AsInteger))
   or  ((ChVencnomes2.Checked) and (ChkUsa2Fecha.Checked) and (QCadastroFECHAMENTO2.AsInteger > QCadastroVENCIMENTO2.AsInteger))) then
   begin
     ShowMessage('A data de vencimento n�o pode ser menor que a data de fechamento.');
     Exit;
   end;

   DecodeDate(Now,CurrAno,CurrMes,CurrDia);
   if((ChVencnomes.Checked and (QCadastroFECHAMENTO1.AsInteger = CurrDia))
    or ((ChVencnomes2.Checked) and (QCadastroFECHAMENTO1.AsInteger = CurrDia)))then
   begin
     ShowMessage('A dia de fechamento n�o pode ser igaul ao dia corrente.');
     Exit;
   end;

   AtualizaDataEmp(True);
   AbreDatas;

   //ESTE TRECHO FAZ A ATUALIZA��O DA DATA DE FECHAMENTO NA CONTA CORRENTE
   if(QCadastroFECHAMENTO1.OldValue <> QCadastroFECHAMENTO1.Value) then
   begin
     Screen.Cursor := crHourGlass;
     DMConexao.Q.Close;
     DMConexao.Q.SQL.Clear;
     DMConexao.Q.SQL.Add('SELECT data_fecha FROM dia_fecha WHERE DATA_FECHA between '+
                        '(SELECT TOP(1) data_fecha FROM DIA_FECHA WHERE DATA_FECHA > getdate()) and ' +
                        '(select top 1 DATA_FECHA_EMP from contacorrente '+
                        'where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+' order by data_fecha_emp desc) '+
                        'and empres_id = '+QCadastroEMPRES_ID.AsString);
     DMConexao.Q.Open;

     while not DMConexao.Q.Eof do
     begin
       mes := MonthOf(DMConexao.Q.Fields[0].Value);
       ano := YearOf(DMConexao.Q.Fields[0].Value);


         i := DMConexao.ExecuteSql('UPDATE contacorrente SET '+
         'DATA_FECHA_EMP = '+QuotedStr(DMConexao.Q.Fields[0].AsString)+' WHERE datepart(month,data_fecha_emp) in ('+IntToStr(mes)+') and '+
         'datepart(year,data_fecha_emp) in ('+IntToStr(ano)+') and empres_id = '+QCadastroEMPRES_ID.AsString);
         DMConexao.Q.Next;

         DateToStr(DayOf(QDatasFechaDATA_FECHA.Value))

     end;

   end
   else begin
     Screen.Cursor := crHourGlass;
     DMConexao.Q.Close;
     DMConexao.Q.SQL.Clear;
     DMConexao.Q.SQL.Add('SELECT data_venc FROM dia_fecha WHERE DATA_VENC > getdate() and empres_id = '+ QCadastroEMPRES_ID.AsString);
     DMConexao.Q.Open;
     while not DMConexao.Q.Eof do

     begin
       mes := MonthOf(DMConexao.Q.Fields[0].Value);
       ano := YearOf(DMConexao.Q.Fields[0].Value);

         i := DMConexao.ExecuteSql('UPDATE contacorrente SET '+
         'DATA_VENC_EMP = '+QuotedStr(DMConexao.Q.Fields[0].AsString)+' WHERE datepart(month,data_venc_emp) in ('+IntToStr(mes)+') and '+
         'datepart(year,data_venc_emp) in ('+IntToStr(ano)+') and empres_id = '+QCadastroEMPRES_ID.AsString);
         DMConexao.Q.Next;
     end;

   end;
   ShowMessage('A(s) data(s) foram atualizadas na conta corrente com sucesso');
   EdMesesVencExit(EdMesesVenc);
   EdMesesVencExit(EdMesesVenc2);
   ChVencnomes.Checked := True;
   ChVencnomes2.Checked := True;
   Screen.Cursor := crDefault;
   if QCadastro.State = dsEdit then
   begin
     QCadastro.Post;
   end;

end;

procedure TFCadEmp.DSSegLibStateChange(Sender: TObject);
begin
  if QCadastro.IsEmpty then
    //if fnVerfCampoVazio('Voc� precisa selecionar uma Empresa para incluir uma Configura��o de Grupo!' , QCadastroEMPRES_ID ) then Abort;
  inherited;
  if (btnCancelSeg = ActiveControl) or (btnGravaSeg = ActiveControl) then
    GridSeg.SetFocus;
  btnCancelSeg.Enabled  := QSegLib.State in [dsEdit,dsInsert];
  btnGravaSeg.Enabled := QSegLib.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.btnAddEnderecoClick(Sender: TObject);
begin
  inherited;
  if QCadastro.State in [dsInsert] then
  begin
    txtCepCartao.Text                     := txtCEP.Text;
    txtLogradouroCartao.Text              := txtENDERECO.Text;
    txtNumeroCartao.Text                  := txtNUMERO.Text;
    lkpUF_CARTAO.KeyValue                 := lkpESTADO.KeyValue;
    lkpCIDADE_CARTAO.KeyValue             := lkpCIDADE.KeyValue;
    dblkbBAIRRO_CARTAO.KeyValue           := dbLkpBairros.KeyValue;
    QCadastroUF_CARTAO.AsInteger          := QCadastroESTADO.AsInteger;
    QCadastroCIDADE_CARTAO.AsInteger      := QCadastroCIDADE.AsInteger;
    QCadastroBAIRRO_CARTAO.AsInteger      := QCadastroBAIRRO.AsInteger;
    QCadastroDESTINATARIO_CARTAO.Text     := QCadastroREPRESENTANTE.Text;
  end
  else begin
    QCadastro.Edit;
    txtCepCartao.Text                    := txtCEP.Text;    
    QCadastroLOGRADOURO_CARTAO.AsString  := QCadastroENDERECO.AsString;
    QCadastroNUMERO_CARTAO.AsString      := QCadastroNUMERO.AsString;
    QCadastroUF_CARTAO.AsInteger         := lkpESTADO.KeyValue;
    QCadastroCIDADE_CARTAO.AsInteger     := QCadastroCIDADE.AsInteger;
    QCadastroBAIRRO_CARTAO.AsInteger     := QCadastroBAIRRO.AsInteger;
    QCadastroDESTINATARIO_CARTAO.Text    := QCadastroREPRESENTANTE.Text;

  end;
end;

procedure TFCadEmp.btnGravaSegClick(Sender: TObject);
begin
  inherited;
  if QSegLib.State in [dsInsert,dsEdit] then
    QSegLib.Post;
end;

procedure TFCadEmp.btnCancelSegClick(Sender: TObject);
begin
  inherited;
  if QSegLib.State in [dsInsert,dsEdit] then
    QSegLib.Cancel;
end;

procedure TFCadEmp.BitBtn6Click(Sender: TObject);
begin
  inherited;
  if QGrupo_Prod.State in [dsInsert,dsEdit] then
    QGrupo_Prod.Cancel;
end;


procedure TFCadEmp.btnGravaGrupEmpClick(Sender: TObject);
begin
  inherited;
  if Trim(QCadastroEMPRES_ID.AsString) = '' then Abort;
  prVerfEAbreCon(QGrupo_conv_emp);
  if QGrupo_conv_emp.State in [dsInsert, dsEdit] then
  QGrupo_conv_emp.Post;
end;


procedure TFCadEmp.btnVisualizaSaldoClick(Sender: TObject);
var cont: integer;
var dt_venc : string;


begin
  inherited;

  if not QCadastro.IsEmpty then begin

     if rbRecarga.Checked then begin

      QSaldoAlim.SQL.Clear;
      QSaldoAlim.SQL.Add(' SELECT');
      QSaldoAlim.SQL.Add(' DATA_RENOVACAO,');
      //QSaldoAlim.SQL.Add(' ALI.EMPRES_ID, ');
      QSaldoAlim.SQL.Add(' (select top 1 DATA_VENC from DIA_FECHA where EMPRES_ID=ALI.EMPRES_ID ');
      QSaldoAlim.SQL.Add(' and DATA_VENC >= ALI.DATA_RENOVACAO)vencimento,');
      QSaldoAlim.SQL.Add(' SUM(ALI.RENOVACAO_VALOR) RENOVACAO_VALOR,');
      QSaldoAlim.SQL.Add(' SUM(ALI.ABONO_VALOR) ABONO_VALOR,');
      QSaldoAlim.SQL.Add(' SUM(ALI.RENOVACAO_VALOR+ABONO_VALOR) VALOR_TOTAL');
      QSaldoAlim.SQL.Add(' FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP ALI');
      QSaldoAlim.SQL.Add(' WHERE ALI.EMPRES_ID= '+QCadastroEMPRES_ID.AsString);
      if cbData.Text = '' then begin
        QSaldoAlim.SQL.Add(' and year(ALI.DATA_FECHA_EMP) >= 2014');
      end
      else begin
        QSaldoAlim.SQL.Add(' and year(ALI.DATA_FECHA_EMP) = '+QuotedStr(cbData.Text));
      end;
      QSaldoAlim.SQL.Add(' AND ALI.RENOVACAO_VALOR IS NOT NULL');
      QSaldoAlim.SQL.Add(' GROUP BY ALI.DATA_RENOVACAO,');
      QSaldoAlim.SQL.Add(' ALI.DATA_VENC_EMP,');
      QSaldoAlim.SQL.Add(' ALI.TIPO_EVENTO,');
      QSaldoAlim.SQL.Add(' ALI.DETALHE_EVENTO,');
      QSaldoAlim.SQL.Add(' ALI.EMPRES_ID');
      QSaldoAlim.SQL.Add(' ORDER BY ALI.DATA_RENOVACAO ASC');
//      QSaldoAlim.SQL.Add(' CASE TIPO_EVENTO WHEN');
//      QSaldoAlim.SQL.Add( QuotedStr('CR')+' THEN DATEADD (MM, -1, DATA_VENC_EMP)');
//      QSaldoAlim.SQL.Add(' ELSE');
//      QSaldoAlim.SQL.Add(' DATA_VENC_EMP');
//      QSaldoAlim.SQL.Add(' END');
//      QSaldoAlim.SQL.Add(' AS DATA_VENC_EMP,');
//      QSaldoAlim.SQL.Add(' TIPO_EVENTO,');
//      QSaldoAlim.SQL.Add(' DETALHE_EVENTO,');
//      QSaldoAlim.SQL.Add(' SUM(RENOVACAO_VALOR) RENOVACAO_VALOR,');
//      QSaldoAlim.SQL.Add(' SUM(ABONO_VALOR) ABONO_VALOR, ');
//      QSaldoAlim.SQL.Add(' SUM(RENOVACAO_VALOR+ABONO_VALOR) VALOR_TOTAL');
//      QSaldoAlim.SQL.Add(' FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP ');
//      QSaldoAlim.SQL.Add(' WHERE EMPRES_ID= '+QCadastroEMPRES_ID.AsString);
//      QSaldoAlim.SQL.Add(' AND YEAR(DATA_FECHA_EMP) = 2019');
//      if cbData.Text = '' then begin
//        QSaldoAlim.SQL.Add(' and year(DATA_FECHA_EMP) >= 2014');
//      end
//      else begin
//        QSaldoAlim.SQL.Add(' and year(DATA_FECHA_EMP) = '+QuotedStr(cbData.Text));
//      end;
//      QSaldoAlim.SQL.Add(' AND RENOVACAO_VALOR is not null');
//      QSaldoAlim.SQL.Add(' GROUP BY DATA_RENOVACAO,DATA_VENC_EMP, TIPO_EVENTO,DETALHE_EVENTO');
//      QSaldoAlim.SQL.Add(' ORDER BY DATA_RENOVACAO asc ');

      QSaldoAlim.Open;

//      if not QSaldoAlim.IsEmpty then begin

        GridSaldos.DataSource := DataSource1;
        GridSaldos.Columns.Clear;

        for Cont := 0 to QSaldoAlim.Fields.Count - 1 do begin
          GridSaldos.Columns.Add;
          GridSaldos.Columns[Cont].FieldName := QSaldoAlim.Fields[Cont].FieldName;

          if Cont = 0 then begin
              GridSaldos.Columns[0].Title.Caption := 'DATA DA RECARGA';
          end else if Cont = 1 then begin
              GridSaldos.Columns[1].Title.Caption := 'DATA DE VENCIMENTO';
          end  else
              GridSaldos.Columns[Cont].Title.Caption := QSaldoAlim.Fields[Cont].FieldName;

        end;
//      end else
//        ShowMessage('A consulta n�o localizou registros - Verifique se a empresa � do tipo cr�dito alimenta��o.');

     end else begin
      GridSaldos.DataSource := DSSaldos;
      GridSaldos.Columns.Clear;
      
      QSaldoEmp.Close;
      QSaldoEmp.SQL.Clear;
      QSaldoEmp.SQL.Add('select ');
      QSaldoEmp.SQL.Add('coalesce(sum(case when cc.entreg_nf  = ''S'' then cc.debito-cc.credito else 0 end),0) as saldo_conf, ');
      QSaldoEmp.SQL.Add('coalesce(sum(case when cc.entreg_nf <> ''S'' then cc.debito-cc.credito else 0 end),0) as saldo_nconf, ');
      QSaldoEmp.SQL.Add('sum(case when cc.entreg_nf <> ''S'' then cc.debito-cc.credito else 0 end)+');
      QSaldoEmp.SQL.Add('sum(case when cc.entreg_nf  = ''S'' then cc.debito-cc.credito else 0 end)as saldo_mes, ');
      QSaldoEmp.SQL.Add('cc.data_fecha_emp, cc.data_venc_emp, cc.fatura_id, fat.data_fatura, fat.tipo ');
      QSaldoEmp.SQL.Add('from contacorrente cc ');
      QSaldoEmp.SQL.Add('join conveniados conv on conv.conv_id = cc.conv_id ');
      QSaldoEmp.SQL.Add('left join fatura fat on fat.fatura_id = cc.fatura_id ');
      if cbData.Text = '' then begin
        QSaldoEmp.SQL.Add('where cc.baixa_conveniado <> ''S''and year(cc.DATA_FECHA_EMP) >= 2014');
      end else begin
        QSaldoEmp.SQL.Add('where cc.baixa_conveniado <> ''S''and year(cc.DATA_FECHA_EMP) = '+QuotedStr(cbData.Text));
      end;
      QSaldoEmp.SQL.Add(' and conv.empres_id = :EMPRES_ID ');
      QSaldoEmp.SQL.Add('group by cc.data_fecha_emp, cc.data_venc_emp, cc.fatura_id, fat.data_fatura, fat.tipo ');
      QSaldoEmp.SQL.Add('order by cc.data_fecha_emp, cc.fatura_id');

      QSaldoEmp.Parameters[0].Value := QCadastroEMPRES_ID.Value;

      QSaldoEmp.Open;

      for Cont := 0 to QSaldoEmp.Fields.Count - 1 do begin
          GridSaldos.Columns.Add;
          GridSaldos.Columns[Cont].FieldName := QSaldoEmp.Fields[Cont].FieldName;

          if Cont = 0 then begin
              GridSaldos.Columns[0].Title.Caption := 'Data de fechamento';

          end else if Cont = 1 then begin
              GridSaldos.Columns[1].Title.Caption := 'Data de vencimento';

          end else if Cont = 2 then begin
              GridSaldos.Columns[2].Title.Caption := 'Saldo n�o confirmado';

          end else if Cont = 3 then begin
              GridSaldos.Columns[3].Title.Caption := 'Saldo confirmado';

          end else if Cont = 4 then begin
              GridSaldos.Columns[4].Title.Caption := 'Saldo m�s';

          end else if Cont = 5 then begin
              GridSaldos.Columns[5].Title.Caption := 'Fatura n�';

          end else if Cont = 6 then begin
              GridSaldos.Columns[6].Title.Caption := 'Data fatura';

          end else
              GridSaldos.Columns[7].Title.Caption := 'Tipo';

         // end  else
         //     GridSaldos.Columns[Cont].Title.Caption := QSaldoAlim.Fields[Cont].FieldName;

        end;
      //DMConexao.AdoCon.Close;
      //DMConexao.AdoQry.Open;
     end;
  end;
end;

procedure TFCadEmp.BitBtn10Click(Sender: TObject);
begin
  inherited;
  cLiberado := GridCredLib.Fields[2].Text;
  if QCredLib.State in [dsInsert,dsEdit] then
     QCredLib.Post;
end;

procedure TFCadEmp.BitBtn11Click(Sender: TObject);
begin
  inherited;
  if QCredLib.State in [dsInsert,dsEdit] then QCredLib.Cancel;
end;

procedure TFCadEmp.btnExclGrupEmpClick(Sender: TObject);
begin
  inherited;
  prVerfEAbreCon(QGrupo_conv_emp);
  if QGrupo_conv_emp.RecordCount > 0 then
    QGrupo_conv_emp.Delete
  else
    ShowMessage('N�o existe nem um grupo para ser excluido!')            
end;

procedure TFCadEmp.btnGravaDataFechaClick(Sender: TObject);
begin
  inherited;
  if QDatasFecha.State in [dsInsert,dsEdit] then
  begin
    QDatasFecha.Post;
  end;
end;

procedure TFCadEmp.btnGravaPbmClick(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsInsert,dsEdit] then qPbm.Post;
end;

procedure TFCadEmp.btnCancelPbmClick(Sender: TObject);
begin
  inherited;
  if qPbm.State in [dsInsert,dsEdit] then qPbm.Cancel;
end;

procedure TFCadEmp.btnCancGrupEmpClick(Sender: TObject);
begin
  inherited;
  if Trim(QCadastroEMPRES_ID.AsString) = '' then Abort;
  prVerfEAbreCon(QGrupo_conv_emp);
  if QGrupo_conv_emp.State in [dsInsert, dsEdit] then
    QGrupo_conv_emp.Cancel;
end;

procedure TFCadEmp.DSQGrupo_conv_empStateChange(Sender: TObject);
begin
  inherited;
  btnCancGrupEmp.Enabled  := QGrupo_conv_emp.State in [dsEdit,dsInsert];
  btnGravaGrupEmp.Enabled := QGrupo_conv_emp.State in [dsEdit,dsInsert];
  btnIncGrupEmp.Enabled   := (QGrupo_conv_emp.State = dsBrowse) and Incluir and (not (QCadastro.State in [dsInsert, dsEdit]));
  btnExclGrupEmp.Enabled  := (QGrupo_conv_emp.State = dsBrowse) and Excluir and (not QGrupo_conv_emp.IsEmpty);
end;

procedure TFCadEmp.QGrupo_conv_empAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Text := 'select gen_id(gen_grupo_conv_emp_id,1) from RDB$database';
  DMConexao.AdoQry.Open;
  QGrupo_conv_empGRUPO_CONV_EMP_ID.AsInteger := DMConexao.AdoQry.Fields[0].AsInteger;
  DMConexao.AdoQry.Close;
end;

procedure TFCadEmp.btnExclProdBloqClick(Sender: TObject);
begin
  inherited;
  if MsgSimNao('Deseja liberar esse produto para compra?') then
  begin
    DMConexao.ExecuteSql(' delete from prod_bloq where prod_id = '+qProdBloqPROD_ID.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString);
    qProdBloq.Refresh;
    dsProdBloqStateChange(Self);
  end;
end;

procedure TFCadEmp.btnInclProdBloqClick(Sender: TObject);
begin
  inherited;
  FBuscaProdutos2:= TFBuscaProdutos2.Create(self);
  FBuscaProdutos2.SetTela(janela);
  FBuscaProdutos2.SetEmpresa(QCadastroEMPRES_ID.AsInteger);
  FBuscaProdutos2.ShowModal;
  FBuscaProdutos2.Free;
  qProdBloq.Refresh;
  dsProdBloqStateChange(Self);
end;

procedure TFCadEmp.AlteraoLinearDescontoemGrupodeProdutos1Click(
  Sender: TObject);
var
  GrupoId: Integer;
  Valor: Currency;
  Historico: String;
begin
  inherited;
  FAltLinearGrupoProd:= TFAltLinearGrupoProd.Create(self);
  FAltLinearGrupoProd.ShowModal;
  if FAltLinearGrupoProd.ModalResult = mrOk then
  begin
    QCadastro.First;
    GrupoId := FAltLinearGrupoProd.cbbGrupo.KeyValue;
    Valor := FAltLinearGrupoProd.edtValor.Value;
    Historico:= FAltLinearGrupoProd.edtHistorico.Text;
    while not QCadastro.Eof do
    begin
      GravaDescGrupoProd(QCadastroEMPRES_ID.AsInteger,GrupoId,Valor);
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  FAltLinearGrupoProd.Free;
  QCadastro.First;
end;

procedure TFCadEmp.GravaDescGrupoProd(EmprID: Integer; GrupoID: Integer; Valor: Currency);
var
  oldGrupo: Integer;
  oldValue: Currency;
begin
  oldGrupo:= DMConexao.ExecuteScalar(' select grupo_prod_id from rel_emp_grupo_prod where empres_id = '+IntToStr(EmprID)+
                       ' and grupo_prod_id = '+IntToStr(GrupoID),0);
  oldValue:= DMConexao.ExecuteScalar(' select desconto from rel_emp_grupo_prod where empres_id = '+IntToStr(EmprID)+
                       ' and grupo_prod_id = '+IntToStr(GrupoID),0);
  if oldGrupo = 0 then
  begin
    DMConexao.ExecuteSql('insert into rel_emp_grupo_prod(EMPRES_ID,GRUPO_PROD_ID,DESCONTO,PRECO_FABRICA,LIBERADO) '+
                   ' values('+IntToStr(EmprID)+','+IntToStr(GrupoID)+','+FormatDimIB(Valor)+',"N","S")');
    DMConexao.GravaLog('FCadEmp','Desconto','0.00',FormatDinBR(Valor),Operador.Nome,'Altera��o','Desconto em Grupo de Produto',
                  IntToStr(EmprID),Self.Name);
  end
  else
  begin
    if oldValue <> Valor then
    begin
      DMConexao.ExecuteSql('update rel_emp_grupo_prod set DESCONTO = '+FormatDimIB(Valor)+
                     ' where EMPRES_ID = '+IntToStr(EmprID)+' and GRUPO_PROD_ID = '+IntToStr(GrupoID));
      DMConexao.GravaLog('FCadEmp','Desconto',FormatDinBR(oldValue),FormatDinBR(Valor),Operador.Nome,'Altera��o',
                   IntToStr(EmprID),Self.Name);
    end;
  end;
end;

procedure TFCadEmp.AlteraoLineardeGrupodeProdutoLiberado1Click(
  Sender: TObject);
var
  GrupoId: Integer;
  Valor: String;
  Historico: String;
begin
//  inherited;
//  FAltLinearGrupoProdLib:= TFAltLinearGrupoProdLib.Create(self);
//  FAltLinearGrupoProdLib.ShowModal;
//  if FAltLinearGrupoProdLib.ModalResult = mrOk then
//  begin
//    QCadastro.First;
//    GrupoId := FAltLinearGrupoProdLib.cbbGrupo.KeyValue;
//    if FAltLinearGrupoProdLib.rdgLib.ItemIndex = 0 then
//      Valor := 'S'
//    else
//      Valor := 'N';
//    Historico:= FAltLinearGrupoProdLib.edtHistorico.Text;
//    while not QCadastro.Eof do
//    begin
//      GravaDescGrupoProdLib(QCadastroEMPRES_ID.AsInteger,GrupoId,Valor);
//      QCadastro.Next;
//    end;
//    MsgInf('Altera��o linear efetuada com sucesso!');
//  end;
//  FAltLinearGrupoProdLib.Free;
//  QCadastro.First;
end;

procedure TFCadEmp.GravaDescGrupoProdLib(EmprID: Integer; GrupoID: Integer; Valor: String);
var
  oldGrupo: Integer;
  oldValue: String;
begin
  oldGrupo:= DMConexao.ExecuteScalar(' select grupo_prod_id from rel_emp_grupo_prod where empres_id = '+IntToStr(EmprID)+
                       ' and grupo_prod_id = '+IntToStr(GrupoID),0);
  oldValue:= DMConexao.ExecuteScalar(' select liberado from rel_emp_grupo_prod where empres_id = '+IntToStr(EmprID)+
                       ' and grupo_prod_id = '+IntToStr(GrupoID),0);
  if oldGrupo = 0 then
  begin
    DMConexao.ExecuteSql('insert into rel_emp_grupo_prod(EMPRES_ID,GRUPO_PROD_ID,DESCONTO,PRECO_FABRICA,LIBERADO) '+
                   ' values('+IntToStr(EmprID)+','+IntToStr(GrupoID)+',0.00,"N","'+Valor+'")');
    DMConexao.GravaLog('FCadEmp','Liberado','S','N',Operador.Nome,'Altera��o',
                  IntToStr(EmprID),Self.Name);
  end
  else
  begin
    if oldValue <> Valor then
    begin
      DMConexao.ExecuteSql('update rel_emp_grupo_prod set LIBERADO = "'+Valor+'" '+
                     ' where EMPRES_ID = '+IntToStr(EmprID)+' and GRUPO_PROD_ID = '+IntToStr(GrupoID));
      DMConexao.GravaLog('FCadEmp','Liberado',oldValue,Valor,Operador.Nome,'Altera��o',
                   IntToStr(EmprID),Self.Name);
    end;
  end;
end;

procedure TFCadEmp.TabCartEmpShow(Sender: TObject);
begin
  inherited;
  qAgenciador.Open;
end;

procedure TFCadEmp.TabCartEmpHide(Sender: TObject);
begin
  inherited;
  qAgenciador.Close;
end;

procedure TFCadEmp.qFormasPgtoAfterPost(DataSet: TDataSet);
var oldValue,newValue,sql : string;
begin
  inherited;
  oldValue := qFormasPgtoliberado.AsString;
  newValue := cLiberado;
  if qFormasPgto.FieldByName('LIBERADO').OldValue <> UpperCase(newValue) then
  begin
    if UpperCase(newValue{qFormasPgto.FieldByName('LIBERADO').AsString}) = 'S' then begin
      if DMConexao.GravaLog('FCadEmp','FormasPgto','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = N','FormaPgto['+qFormasPgto.FieldByName('FORMA_ID').AsString+'] = S',Operador.Nome,'Altera��o',QCadastro.FieldByName('EMPRES_ID').AsString,Self.Name) then
        sql := 'insert into formas_emp_lib (emp_id, forma_id) values ('+QCadastro.FieldByName('EMPRES_ID').AsString+', '+qFormasPgto.FieldByName('FORMA_ID').AsString+')';
    end else begin
      if DMConexao.GravaLog('FCadEmp','FormasPgto','FormaPgto['+newValue+'] = S','FormaPgto['+newValue+'] = N',Operador.Nome,'Altera��o',QCadastro.FieldByName('EMPRES_ID').AsString,Self.Name) then
        sql :='delete from formas_emp_lib where emp_id = '+QCadastro.FieldByName('EMPRES_ID').AsString+' and forma_id = '+qFormasPgto.FieldByName('FORMA_ID').AsString;
    end;
  end;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Text := sql;
    //DMConexao.AdoQry.Open;
  DMConexao.AdoQry.ExecSQL;
  DMConexao.AdoQry.Close;
    //DMConexao.ExecuteSql(sql);
  qFormasPgto.Requery();
  cLiberado := '';
end;

procedure TFCadEmp.qFormasPgtoBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.qFormasPgtoBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.qFormasPgtoBeforePost(DataSet: TDataSet);
begin
  inherited;
  if ((qFormasPgto.FieldByName('DESCRICAO').AsString = ' A VISTA') and (UpperCase(qFormasPgto.FieldByName('LIBERADO').AsString) = 'N')) then
  begin
    MsgInf('A forma de pagamento [A Vista] n�o pode ser bloqueada pelo sistema');
    qFormasPgto.Cancel;
    Exit;
  end;
  qFormasPgto.FieldByName('LIBERADO').AsString:= UpperCase(qFormasPgto.FieldByName('LIBERADO').AsString);
end;

procedure TFCadEmp.grdFormasPgtoColExit(Sender: TObject);
begin
  inherited;
  if qFormasPgto.State in [dsEdit] then qFormasPgto.Post;
end;

procedure TFCadEmp.tabFormasPgtoShow(Sender: TObject);
begin
  inherited;
  if (QCadastro.IsEmpty) or (QCadastro.FieldByName('ACEITA_PARC').AsString = 'N') then
  begin
    panFormasLib.Visible:= False;
    qFormasPgto.Close;
    qFormasPgto.Parameters.Items[0].Value:= QCadastro.FieldByName('EMPRES_ID').AsInteger;
    qFormasPgto.Open;
  end
  else
  begin
    qFormasPgto.Close;
    panFormasLib.Visible:= True;
  end;
end;

procedure TFCadEmp.tabFormasPgtoHide(Sender: TObject);
begin
  inherited;
  if qFormasPgto.Active then qFormasPgto.Close;
end;

procedure TFCadEmp.QGrupo_ProdBeforePost(DataSet: TDataSet);
begin
  inherited;
//  if QGrupo_ProdGRUPO_PROD_ID.IsNull then begin MsgInf('teste obrigat�rio!'); DataSet.Cancel; end;
//  if not QCadastro.Active then begin MsgInf('� necess�rio ter uma empresa selecionada!'); DataSet.Cancel; end;
end;

procedure TFCadEmp.dbEdtCNPJKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
  inherited;
end;

procedure TFCadEmp.btnCancelCredObrigarSenhaClick(Sender: TObject);
begin
  inherited;
  if QCred_Obriga_Senha.State in [dsInsert,dsEdit] then QCred_Obriga_Senha.Cancel;
end;

procedure TFCadEmp.btnGravaCredObrigarSenhaClick(Sender: TObject);
begin
  inherited;
  if QCred_Obriga_Senha.State in [dsInsert,dsEdit] then
     QCred_Obriga_Senha.Post;
end;

procedure TFCadEmp.QCred_Obriga_SenhaAfterInsert(DataSet: TDataSet);
begin
  inherited;
  if (ActiveControl = GridCredObrigarSenha) then
     QCred_Obriga_Senha.Cancel;
end;

procedure TFCadEmp.QCred_Obriga_SenhaBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if ((not Alterar) and (not excluindo)) then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.QCred_Obriga_SenhaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadEmp.DSCred_Obriga_SenhaStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelCredObrigarSenha = ActiveControl) or (btnGravaCredObrigarSenha = ActiveControl) then
     GridCredObrigarSenha.SetFocus;
  btnCancelCredObrigarSenha.Enabled  := QCred_Obriga_Senha.State in [dsEdit,dsInsert];
  btnGravaCredObrigarSenha.Enabled := QCred_Obriga_Senha.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.QCred_Obriga_SenhaAfterPost(DataSet: TDataSet);
var sql, sqlLog: String;
  cadastro,oldValue,newValue, detalhe: string;
  reg_atual : TBookmark;
begin
  inherited;
  sql:= '';
  sqlLog:= '';
  cadastro:= 'Cred. '+QCred_Obriga_SenhaCRED_ID.AsString+' lib. para esta empresa';
  detalhe := 'Empr ID';
  newValue := cLiberado;

  if (UpperCase(newValue) = 'S') then begin
    if QCred_Obriga_SenhaOBRIGASENHA.OldValue = 'N' then begin
      QCredLib.Close;
      QCredLib.SQL.Clear;
      QCredLib.SQL.Add(' Select cred.cred_id, cred.nome, coalesce(credlib.liberado,''S'') as liberado');
      QCredLib.SQL.Add(' from credenciados cred');
      QCredLib.SQL.Add(' left join emp_cred_lib credlib on cred.cred_id = credlib.cred_id');
      QCredLib.SQL.Add(' and ((credlib.empres_id = ' + QCadastroEMPRES_ID.AsString + ') or (credlib.empres_id is null)) ');
      QCredLib.SQL.Add(' order by cred.nome');
      QCredLib.Open;

      if ExisteCredBloq(QCadastroEMPRES_ID.AsInteger,QCredLibCRED_ID.AsInteger) then begin
        sql :=  'update EMP_CRED_OBRIGA_SENHA set liberado = '+QuotedStr(UpperCase(newValue))+''+
                ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+' and CRED_ID = '+QCredLibCRED_ID.AsString;
      end else begin
        sql :=  'insert into EMP_CRED_OBRIGA_SENHA(EMPRES_ID, CRED_ID, OBRIGAR_SENHA) '+
                ' values('+QCadastroEMPRES_ID.AsString+','+QCred_Obriga_SenhaCRED_ID.AsString+','+QuotedStr(UpperCase(newValue))+')';
      end;
      if UpperCase(QCred_Obriga_SenhaOBRIGASENHA.OldValue) <> UpperCase(newValue) then begin
        DMConexao.GravaLog('FCadEmp','Cred. Bloq.','S','N',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
      end;
    end;
  end else if (UpperCase(newValue) = 'N') then begin
    if QCred_Obriga_SenhaOBRIGASENHA.OldValue = 'S' then begin
      sql :=  'delete from EMP_CRED_OBRIGA_SENHA ';
      sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and CRED_ID = '+QCred_Obriga_SenhaCRED_ID.AsString;
    end;
    if UpperCase(QCred_Obriga_SenhaOBRIGASENHA.OldValue) <> UpperCase(newValue) then begin
      DMConexao.GravaLog('FCadEmp','Cred. Bloq.','N','S',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
    end;
  end;
  if sql <> '' then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sql;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;

  end;
  QCred_Obriga_Senha.DisableControls;
  reg_atual := QCred_Obriga_Senha.GetBookmark;
  QCred_Obriga_Senha.Requery();
  QCred_Obriga_Senha.GotoBookmark(reg_atual);
  QCred_Obriga_Senha.freebookmark(reg_atual);
  QCred_Obriga_Senha.EnableControls;
  cLiberado := '';
end;

procedure TFCadEmp.tsSenhaConvShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QCred_Obriga_Senha.Close;
    QCred_Obriga_Senha.Parameters.Items[0].Value:= QCadastroEMPRES_ID.AsInteger;
    QCred_Obriga_Senha.Open;

  end;
end;

procedure TFCadEmp.tsSenhaConvHide(Sender: TObject);
begin
  inherited;
  QCred_Obriga_Senha.Close;
end;

procedure TFCadEmp.GridCredObrigarSenhaColExit(Sender: TObject);
begin
  inherited;
  if QCred_Obriga_Senha.State in [dsEdit] then QCred_Obriga_Senha.Post;
end;

{procedure TFCadEmp.GridCredObrigarSenhaTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  DMConexao.SortZQuery(Field.DataSet,Field.FieldName);
end; }


procedure TFCadEmp.qUsu_WebBeforePost(DataSet: TDataSet);
var newValue : string;
    flag : Integer;
    b : TBookmark;
begin
  inherited;

  if qUsu_Web.State in [dsInsert] then
  begin

    if dbGridUsuWeb.Fields[1].Value = NULL then
    begin
      MsgInf('O nome do usu�rio n�o pode estar vazio!');
      dbGridUsuWeb.Fields[1].FocusControl;
      Abort;
      //dbGridUsuWeb.Fields[1].FocusControl;
    end
    else if Trim(dbGridUsuWeb.Fields[2].Value) = NULL then
    begin
      MsgInf('O email do usu�rio n�o pode estar vazio!');
      dbGridUsuWeb.Fields[2].FocusControl;
       Abort;
    end
    else if not fnIsEmail(dbGridUsuWeb.Fields[2].Value) then
    begin
      MsgErro('Email inv�lido!');
      dbGridUsuWeb.Fields[2].FocusControl;
      Abort;
    end
    else
    begin
      dbGridUsuWeb.Fields[0].ReadOnly := false;
      qUsu_Webusu_id.Value        := dbGridUsuWeb.Fields[0].Value;
      qUsu_Webusu_nome.Value      := UpperCase(dbGridUsuWeb.Fields[1].Value);
      qUsu_Webusu_email.Value     := dbGridUsuWeb.Fields[2].Value;
      qUsu_Webusu_senha.Value     := Crypt('E','1111','BIGCOMPRAS');
      qUsu_Webemp_for_id.Value    := QCadastroEMPRES_ID.Value;
      qUsu_Webusu_tipo.Value      := '0';
      if(dbGridUsuWeb.Fields[3].Value <> null) then
      begin
        qUsu_Webusu_liberado.Value  := UpperCase(dbGridUsuWeb.Fields[3].Value);
      end
      else
        dbGridUsuWeb.Fields[3].Value := 'N';
    end;
  end;
  {else
  begin

    newValue := cLiberado;
    if ( UpperCase(newValue) <> 'S' ) and( UpperCase(newValue) <> 'N' ) then begin
      qUsu_WebUSU_LIBERADO.AsString := 'N';
    end;

    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := 'UPDATE USUARIOS_WEB SET USU_LIBERADO = '+UpperCase(QuotedStr(newValue))+' WHERE USU_ID = '+ QuotedStr(qUsu_WebUSU_ID.AsString);
    qUsu_Web.Parameters[0].Value := QCadastroEMPRES_ID.AsInteger;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;

  end;}



end;

procedure TFCadEmp.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if (qUsu_Web.State in [dsEdit,dsInsert]) then
  begin
    qUsu_Webusu_id.ReadOnly := false;
    qUsu_Web.Post;
  end
  else
  MsgInf('Para adicionar um usu�rio pressione o bot�o Inserir!');
  btnInserirPos.SetFocus;
end;

procedure TFCadEmp.BitBtn2Click(Sender: TObject);
begin
  inherited;
  if (qUsu_Web.State in [dsEdit,dsInsert]) then
    qUsu_Web.Cancel;
end;

procedure TFCadEmp.tsUsuariosWebShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    qUsu_Web.Close;
    qUsu_Web.Parameters.Items[0].Value := QCadastroEMPRES_ID.AsInteger;
    qUsu_Web.Open;
  end;
end;

procedure TFCadEmp.tsUsuariosWebHide(Sender: TObject);
begin
  inherited;
  qUsu_Web.Close;
end;

procedure TFCadEmp.BitBtn3Click(Sender: TObject);
begin
  inherited;
  if (TBitBtn(Sender).Enabled = False) then Abort;
  if qUsu_Web.RecordCount <= 0 then begin
    MsgInf('N�o existe registro para limpar senha, ou n�o existe registro selecinoado!');
    Abort;
  end;
  if DMConexao.ExecuteSql('UPDATE USUARIOS_WEB SET USU_SENHA = '+ quotedStr(Crypt('E', '1111', 'BIGCOMPRAS')) + 'WHERE USU_ID = ' + qUsu_WebUSU_ID.AsString) > 0 then begin
    MsgInf('Senha zerada com sucesso!');
  end else begin
    MsgErro('N�o foi poss�vel alterar a senha');
  end;
end;

procedure TFCadEmp.BitBtn4Click(Sender: TObject);
var bm : TBookmark;
begin
  inherited;
  if (TBitBtn(Sender).Enabled = False) then Abort;
  if qUsu_Web.RecordCount <= 0 then begin
    MsgInf('N�o existe registro para apagar, ou n�o existe registro selecinoado!');
    Abort;
  end;
  if DMConexao.ExecuteSql('UPDATE USUARIOS_WEB SET USU_APAGADO = ''S'' WHERE USU_ID = ' + qUsu_WebUSU_ID.AsString) > 0 then begin
    bm := qUsu_Web.GetBookmark;
    qUsu_Web.Close;
    MsgInf('');
    qUsu_Web.Open;
    MsgInf('');
    if qUsu_Web.BookmarkValid(bm) then begin
      qUsu_Web.GotoBookmark(bm);
    end;
    qUsu_Web.FreeBookmark(bm);
  end else begin
    MsgErro('Erro ao excluir usu�rio');
  end;
end;

procedure TFCadEmp.btnGravarConvAlimClick(Sender: TObject);
var saldoRenoOld, abonoMesOld, limiteMesOld : currency;
incluir : boolean;
valorAbono, valorRenovacao: Currency;
sqlSAP,detalheEvento,empresID,tipoComplementoSAP: String;
begin
  if Application.MessageBox('Confirma a altera��o das informa��es?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    valorAbono := 0;
    valorRenovacao := 0;

    tLimiteMes := StrtoCurr(dbGridAlim.Fields[2].Text);
    tAbonoMes := StrtoCurr(dbGridAlim.Fields[3].Text);
    tSaldoRenovacao := StrtoCurr(dbGridAlim.Fields[4].Text);

    SavePlace := QCredAlim.GetBookmark;
    try
      DMConexao.AdoCon.BeginTrans;
      // Altera��o de Limite M�s
      if VarIsNull(QCredAlimLIMITE_MES.OldValue) then
        limiteMesOld := 0
      else
        limiteMesOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if (limiteMesOld <> tLimiteMes) then
      begin
        DMConexao.GravaLog('FCadConv','LIMITE_MES',FormatDinBR(limiteMesOld),FormatDinBR(tLimiteMes),Operador.Nome,'Altera��o',
                            QCredAlimCONV_ID.AsString,'');
        qUpdate.SQL.Text := ' update conveniados set limite_mes = ' + fnsubstituiString(',','.',CurrToStr(tLimiteMes)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString;
        DMConexao.ExecuteSql(qUpdate.SQL.Text);
      end;

      Screen.Cursor := crHourGlass;

      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE CONV_ID = '+ QCredAlimCONV_ID.AsString);
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then
        incluir := false
      else
        incluir := true;

      // Altera��o de Abono M�s
      if VarIsNull(QCredAlimABONO_MES.OldValue) then
        abonoMesOld := 0
      else
        abonoMesOld := QCredAlimABONO_MES.OldValue;

      // Altera��o de Saldo Renova��o
      if VarIsNull(QCredAlimSALDO_RENOVACAO.OldValue) then
        saldoRenoOld := 0
      else
        saldoRenoOld := QCredAlimSALDO_RENOVACAO.OldValue;

      if ((abonoMesOld <> tAbonoMes) or (saldoRenoOld <> tSaldoRenovacao))  then
      begin
        if incluir = true then begin
            qUpdate.SQL.Text := 'insert into alimentacao_renovacao_creditos(renovacao_id, conv_id, renovacao_valor, abono_valor) values('+ qCredAlimRENOVACAO_ID.AsString + ',' + QCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ',' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) +')';
            DMConexao.ExecuteSql(qUpdate.SQL.Text);

            sqlSAP := '';

            case rbTipoComplemento.ItemIndex of
              0:begin
                  tipoComplementoSAP := '''PR''';
                end;
              else
                tipoComplementoSAP := '''FA''';
            end;

            case rgDetalheSAP.ItemIndex of
              0: begin
                    detalheEvento := 'RFI'
                 end;
              else
                detalheEvento := 'RFP';
            end;

            sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
             ',' + QCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao))+ ',' + fnsubstituiString(',','.',CurrToStr(tAbonoMes))+','+ QuotedStr(DateTimeToStr(Now))+',';

            case rgTipo.ItemIndex of
            0: begin
                sqlSAP := sqlSAP + '''RE'','+QuotedStr(detalheEvento) + ',NULL';
              end;

            else
              sqlSAP := sqlSAP + '''CR'',';
              sqlSAP := sqlSAP + QuotedStr(detalheEvento) + ','+tipoComplementoSAP;
            end;
            empresId := QCadastroEMPRES_ID.AsString;
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empresId + ' and DATA_FECHA >= getdate()');
            DMConexao.AdoQry.Open;

            sqlSAP := sqlSAP + ', ' + empresId + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

            sqlSAP := sqlSAP + ')';
            DMConexao.ExecuteSql(sqlSAP);

          end
        else
          begin
            qUpdate.SQL.Text := ' update alimentacao_renovacao_creditos set abono_valor = ' + fnsubstituiString(',','.',CurrToStr(tAbonoMes)) + ', renovacao_valor = ' + fnsubstituiString(',','.',CurrToStr(tSaldoRenovacao)) + ' where conv_id = ' + QCredAlimCONV_ID.AsString + ' and renovacao_id = ' + qCredAlimRENOVACAO_ID.AsString;
            DMConexao.ExecuteSql(qUpdate.SQL.Text);
          end;
      end;

      DMConexao.AdoCon.CommitTrans;

    except
      on e:Exception do
      begin
         DMConexao.AdoCon.RollbackTrans;
         Screen.Cursor := crDefault;
         MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
         abort;
       end;
    end;
  end;

  QCredAlim.Requery;
  QCredAlim.GotoBookmark(SavePlace);
  QCredAlim.FreeBookmark(SavePlace);
  qCredAlim.First;
  while not qCredAlim.Eof do
    begin
        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
        qCredAlim.Next;
    end;
  qCredAlim.First;
  lblTotalAbono.Value := valorAbono;
  lblTotalRenovacao.Value := valorRenovacao;
  
  Screen.Cursor := crDefault;
  tSaldoRenovacao := 0;
  tAbonoMes := 0;
  tLimiteMes := 0;
end;

procedure TFCadEmp.AtualizaSaldoRenovacao(tipo: TSaldoRenovacao);
var s,msg : String;
    val : Double;
begin
  if tipo = srValor then begin
    msg := 'Digite um valor';
  end else begin
    msg := 'Digite uma porcentagem';
  end;
  s := '';
  repeat
    s := InputBox('Bella ADM Convenio',msg + sLineBreak+ ' somente n�meros e separador decimal','0,00');
    s := fnSubstituiString('.',DecimalSeparator,fnSubstituiString(',',DecimalSeparator,s));
  until (IsFloat(s));
  val := StrToFloat(s);
  qCredAlim.First;
  case tipo of
    srPerLimite: begin
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        qCredAlimSALDO_RENOVACAO.AsFloat := (qCredAlimLIMITE_MES.Value * val) / 100;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
    end;
    srPerSalario: begin
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        //qCredAlimSALDO_RENOVACAO.AsFloat := (qCredAlimSALARIO.Value * val) / 100;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
    end else
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        qCredAlimSALDO_RENOVACAO.Value := val;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
  end;
end;

procedure TFCadEmp.AtualizaAbonoMes(tipo: TSaldoRenovacao);
var s,msg : String;
    val : Double;
begin
  if tipo = srValor then begin
    msg := 'Digite um valor';
  end else begin
    msg := 'Digite uma porcentagem';
  end;
  s := '';
  repeat
    s := InputBox('Bella ADM Convenio',msg + sLineBreak+ ' somente n�meros e separador decimal','0,00');
    s := fnSubstituiString('.',DecimalSeparator,fnSubstituiString(',',DecimalSeparator,s));
  until (IsFloat(s));
  val := StrToFloat(s);
  qCredAlim.First;
  case tipo of
    srPerLimite: begin
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        qCredAlimABONO_MES.AsFloat := (qCredAlimLIMITE_MES.Value * val) / 100;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
    end;
    srPerSalario: begin
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        //qCredAlimABONO_MES.AsFloat := (qCredAlimSALARIO.Value * val) / 100;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
    end else
      while not qCredAlim.Eof do begin
        qCredAlim.Edit;
        qCredAlimABONO_MES.Value := val;
        qCredAlim.Post;
        qCredAlim.Next;
      end;
  end;
end;

procedure TFCadEmp.AtualizaAbonoMesESaldoRenovacao(tipoAbono,tipoSaldo: Integer);
begin
  qCredAlim.First;
  while not qCredAlim.Eof do begin
    qCredAlim.Edit;
    case tipoAbono of
      0: qCredAlimABONO_MES.AsFloat := (qCredAlimLIMITE_MES.Value * StrToCurr(edtAbonoMes.Text)) / 100; //srPerLimite
      //1: qCredAlimABONO_MES.AsFloat := (qCredAlimSALARIO.Value * StrToCurr(edtAbonoMes.Text)) / 100; //srPerSalario
    else
      qCredAlimABONO_MES.AsFloat := StrToCurr(edtAbonoMes.Text);
    end;
    case tipoSaldo of
      0: qCredAlimSALDO_RENOVACAO.AsFloat := (qCredAlimLIMITE_MES.Value * StrToCurr(edtSaldoRenovacao.Text)) / 100; //srPerLimite
      //1: qCredAlimSALDO_RENOVACAO.AsFloat := (qCredAlimSALARIO.Value * StrToCurr(edtSaldoRenovacao.Text)) / 100; //srPerSalario
    else
      qCredAlimSALDO_RENOVACAO.Value := StrToCurr(edtSaldoRenovacao.Text);
    end;
    qCredAlim.Post;
    qCredAlim.Next;
  end;
end;

procedure TFCadEmp.btCancelarClick(Sender: TObject);
begin
  if QOcorrencias.State in [dsInsert,dsEdit] then
    QOcorrencias.Cancel;
end;

procedure TFCadEmp.btGravarClick(Sender: TObject);
begin
  if QOcorrencias.State in [dsInsert,dsEdit] then begin
    try
      QOcorrenciasdata_atendimento.AsDateTime := Now;
      QOcorrenciasatendimento_id.AsInteger    := StrToInt(lbl4.Caption);
      QOcorrenciasempres_id.AsInteger         := QCadastroEMPRES_ID.AsInteger;
      QOcorrenciasoperador.AsString           := Operador.Nome;
      if QOcorrencias.State in [dsInsert] then begin
          if JvDBLookupCombo3.KeyValue = Null then
          begin
                MsgInf('� necess�rio selecionar um Status para o atendimento.');
                JvDBLookupCombo3.SetFocus;
                Exit;
          end;
          colocouMensagem := DMConexao.GravaLogOcorrencia(ts1.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),QOcorrencias.FieldByName('atendimento_id').AsString,Operador.Nome,'Inclus�o',Self.Caption,'NULL','','');
          MsgInf('Ocor�ncia registrada com sucesso '+#13+'O n�mero do protocolo gerado �: '+QOcorrenciasatendimento_id.AsString+'')
      end
      else begin
            colocouMensagem := DMConexao.GravaLogOcorrencia(ts1.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),QOcorrencias.FieldByName('atendimento_id').AsString,Operador.Nome,'Altera��o',Self.Caption,'NULL','','');
            MsgInf('Os dados foram alterados com sucesso');
      end;
      QOcorrencias.Post;
    finally
    end;
  end;
end;

procedure TFCadEmp.btn1Click(Sender: TObject);
begin
  inherited;
  QOcorrencias.Edit;
end;

procedure TFCadEmp.tsAlimentacaoShow(Sender: TObject);
var valorAbono, valorRenovacao: Currency;
begin
  inherited;

    valorAbono := 0;
    valorRenovacao := 0;

    qCredAlim.Close;
    qCredAlim.Parameters.Items[0].Value := qCadastroEMPRES_ID.Value;
    QCredAlim.Parameters.Items[1].Value := QCadastroEMPRES_ID.Value;
    qCredAlim.Open;
    rgTipo.ItemIndex := 0;
    if not qCredAlimDATA_RENOVACAO.IsNull then
    begin

      if QCredAlimTIPO_CREDITO.AsString = 'R' then begin
        rgTipo.ItemIndex := 0;
      end else begin
        rgTipo.ItemIndex := 1;
      end;

      dbGridAlim.Visible := true;
      //Panel43.Visible := true;

      while not qCredAlim.Eof do
      begin
        valorAbono := valorAbono + QCredAlimabono_mes.AsCurrency;
        valorRenovacao := valorRenovacao + QCredAlimsaldo_renovacao.AsCurrency;
        qCredAlim.Next;
      end;

      qCredAlim.First;
    end
    else
    begin
      dbDataRenovacao.Text := qCredAlimDATA_RENOVACAO.AsString;

//    dbDataRenovacao.Text := '';
      dbGridAlim.Visible := false;
      Panel43.Visible := false;
    end;
  //end;

  lblTotalAbono.Value := valorAbono;
  lblTotalRenovacao.Value := valorRenovacao;
  edtSaldoRenovacao.Text := '0,00';
  edtAbonoMes.Text := '0,00';

  lblTitulo.Caption := 'Conveniados da empresa: '+ QCadastroNOME.AsString;
  dbDataRenovacao.Text := FormataDataSql(qCredAlimDATA_RENOVACAO.AsString);

  if QCredAlimTIPO_CREDITO.AsString = 'R' then begin
     rgTipo.ItemIndex := 0;
  end else begin
     rgTipo.ItemIndex := 1;
  end;

  if QCredAlimDETALHE_EVENTO.AsString = 'RFI' then begin
     rgDetalheSAP.ItemIndex := 0;
  end else begin
     rgDetalheSAP.ItemIndex := 1;
  end;


  if QCredAlimCOMPLEMENTO.AsString = 'PR' then begin
     rbTipoComplemento.ItemIndex := 0;
  end else if QCredAlimCOMPLEMENTO.AsString = 'FA'then begin
     rbTipoComplemento.ItemIndex := 1;
  end else begin;
      rbTipoComplemento.ItemIndex := -1;
  end;

end;

procedure TFCadEmp.edtAbonoMesKeyPress(Sender: TObject; var Key: Char);
begin
 if Key in ['.',','] then
    Key := DecimalSeparator;
  if (not (key in['0'..'9', DecimalSeparator, chr(VK_BACK), chr(VK_LEFT), chr(VK_RIGHT),chr(VK_DELETE)]))
  or (((TEdit(Sender).SelStart = 0) or (Pos(DecimalSeparator, TEdit(Sender).Text) > 0)) and (Key = DecimalSeparator)) then begin
    Key := #0;
  end;
end;

procedure TFCadEmp.dsCredAlimStateChange(Sender: TObject);
begin
  inherited;
  btnCancelarConvAlim.Enabled  := qCredAlim.State in [dsEdit,dsInsert];
  btnGravarConvAlim.Enabled := qCredAlim.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.InsereAlimentacaoRenovacao;
var tmp, sql,sqlSAP,detalheEvento,complemento : String;
    dc : char;
    dia_renovacao, mes_renovacao, ano_renovacao, dia_atual, mes_atual, ano_atual : Word;
    renovacao_id : integer;
begin
  if dbDataRenovacao.Date <> 0 then
  begin
    //verifica se data de renovacao e maior que a data atual//
    if dbDataRenovacao.Date <= Date then begin
      msginf('Data de Renova��o deve ser maior que a data atual!');
      Abort;
    end
    else
    //verifica se existe registro na registro_alimentacao se tiver verifica se data de renovacao foi alterada se sim faz update na tabela alimentacao_renovacao//
    if (not qCredAlimDATA_RENOVACAO.IsNull) AND (rgTipo.ItemIndex = 0) then begin
      renovacao_id := QCredAlimRENOVACAO_ID.AsInteger;
      DecodeDate(StrToDate(FormataDataSql(QCredAlimDATA_RENOVACAO.AsString)),ano_renovacao, mes_renovacao, dia_renovacao);
      DecodeDate(dbDataRenovacao.Date,ano_atual, mes_atual, dia_atual);
      if ((dbDataRenovacao.Date <> StrToDate(FormataDataSql(QCredAlimDATA_RENOVACAO.AsString))) and (mes_renovacao = mes_atual)) then begin
        if Application.MessageBox('J� existe Data de Renova��o para este m�s. Deseja alterar a Data?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
        begin
          try
            Screen.Cursor := crHourGlass;
            sql := 'UPDATE ALIMENTACAO_RENOVACAO SET DATA_RENOVACAO =  '+ QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ', TIPO_CREDITO = ';
            case rgTipo.ItemIndex of
              0: sql := sql + '''R''';
            else
              sql := sql + '''C''';
            end;
            sql := sql + ' WHERE RENOVACAO_ID = '+ QCredAlimRENOVACAO_ID.AsString;

            DMConexao.AdoCon.BeginTrans;
            DMConexao.ExecuteSql(sql);
            DMConexao.AdoCon.CommitTrans;
          except
            on e:Exception do
            begin
             DMConexao.AdoCon.RollbackTrans;
             Screen.Cursor := crDefault;
             MsgErro('Um erro ocorreu durante a atualiza��o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
             abort;
            end;
           end;
        end
        else
        begin
          dbDataRenovacao.Text := QCredAlimDATA_RENOVACAO.AsVariant;
          Abort;
        end;
      end;
    end
    else
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SRENOVACAO_ID AS RENOVACAO_ID');
          DMConexao.AdoQry.Open;
          renovacao_id := DMConexao.AdoQry.Fields[0].Value;

          sql := 'INSERT INTO ALIMENTACAO_RENOVACAO(RENOVACAO_ID, EMPRES_ID, DATA_RENOVACAO, TIPO_CREDITO, DATA_LANCAMENTO) VALUES('+ inttostr(renovacao_id);
          //ROTINA PARA TRATAR INSER��O NA TABELA DO SAP
          sqlSAP := 'INSERT INTO ALIMENTACAO_RENOVACAO_SAP(RENOVACAO_ID, EMPRES_ID, DATA_RENOVACAO,TIPO_CREDITO,TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,DATA_LANCAMENTO) VALUES('+ inttostr(renovacao_id);
          sql := sql + ',' + qCredAlimEMPRES_ID.AsString + ',' + QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ',';
          sqlSAP := sqlSAP + ',' + qCredAlimEMPRES_ID.AsString + ',' + QuotedStr(FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date)) + ',';
          //DEFINE QUAL O DETALHE DO EVENTO RFI - RECARGA FINANCEIRO IMEDIATO OU RFP - RECARGA FINANCEIRO PROGRAMADO
//          IF cbFinanceiroImediato.Checked then
//            detalheEvento := 'RFI'
//          ELSE
//            detalheEvento := 'RFP';

           //DEFINE QUAL O DETALHE DO EVENTO RFI - RECARGA FINANCEIRO IMEDIATO OU RFP - RECARGA FINANCEIRO PROGRAMADO
          case rgDetalheSAP.ItemIndex of
          0: begin
                detalheEvento := 'RFI'
             end;
          else
            detalheEvento := 'RFP';
          end;

          case rbTipoComplemento.ItemIndex of
          0: begin
                complemento := 'PR'
             end;
          else
            complemento := 'FA';
          end;


          // DEFINE SE � RECARGA OU COMPLEMENTO
          case rgTipo.ItemIndex of
          0: begin
              sql := sql + '''R'',' ;
              sqlSAP := sqlSAP + '''R'',''RE'','+''''+detalheEvento+''',NULL,';
             end;
          else
            sql := sql + '''C'',';
            sqlSAP := sqlSAP + '''C'',';
            sqlSAP := sqlSAP + '''CR'','+''''+detalheEvento+''','+QuotedStr(complemento)+', ';
          end;

          sql := sql + QuotedStr(FormatDateTime('dd/mm/yyyy',Date) +  ' ' + FormatDateTime('hh:mm:ss',Time))  + ')';
          sqlSAP := sqlSAP + QuotedStr(FormatDateTime('dd/mm/yyyy',Date) +  ' ' + FormatDateTime('hh:mm:ss',Time))  + ')';

          DMConexao.ExecuteSql(sql);
          DMConexao.ExecuteSql(sqlSAP);
          DMConexao.AdoCon.CommitTrans;

        except
          on e:Exception do
          begin
             DMConexao.AdoCon.RollbackTrans;
             Screen.Cursor := crDefault;
             MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
             abort;
          end;
      end;
    end;

    qCredAlim.Close;
    qCredAlim.Parameters.Items[0].Value := qCadastroEMPRES_ID.Value;
    QCredAlim.Parameters.Items[1].Value := QCadastroEMPRES_ID.Value;
    qCredAlim.Open;

    DMConexao.GravaLog('FCadEmp','DATA_RENOVACAO',FormataDataSql(QCredAlimDATA_RENOVACAO.AsString),FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Inclus�o',
        QCadastroEMPRES_ID.AsString,'');

    msginf('Atualiza��o realizada com sucesso');

    dbGridAlim.Visible := true;
    //Panel43.Visible := true;
    dbGridAlim.SetFocus;
    Screen.Cursor := crDefault;

  end
  else
    begin
      MsgInf('Necess�rio escolher a Data de Renova��o');
      dbDataRenovacao.SetFocus;
    end;
end;

procedure TFCadEmp.InsereAlimRenovacaoCreditos;
var  tmp, sql, sqlSAP,detalheEvento,complemento,empresId : String;
  dc : char;
  valorAbono, valorRenovacao: Currency;
begin
    valorAbono := 0;
    valorRenovacao := 0;

    if Application.MessageBox('Confirma a inclus�o/altera��o do Abono/Saldo Renova��o?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      Screen.Cursor := crHourGlass;
      dc := DecimalSeparator;
      Application.ProcessMessages;

      try
        DMConexao.AdoCon.BeginTrans;

        //DELETA OS REGISTROS DA TABELA ALIMENTACAO_RENOVACAO_CREDITOS//
        DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+qCredAlimRENOVACAO_ID.AsString);
        DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP WHERE RENOVACAO_ID =  '+ qCredAlimRENOVACAO_ID.AsString);
        QCredAlim.First;
        while not QCredAlim.Eof do begin
          qUpdate.SQL.Clear;
          qUpdate.SQL.Text := 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + qCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',edtSaldoRenovacao.Text) + ',' + fnsubstituiString(',','.',edtAbonoMes.Text) + ')';
          qUpdate.ExecSQL;

          case rgDetalheSAP.ItemIndex of
            0: begin
                  complemento := 'PR'
               end;
          else
            complemento := 'FA';
          end;

          case rgDetalheSAP.ItemIndex of
          0: begin
                detalheEvento := 'RFI'
             end;
          else
            detalheEvento := 'RFP';
          end;
          sqlSAP := '';
          sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO, DATA_FECHA_EMP, DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
            ',' + qCredAlimCONV_ID.AsString + ',' + fnsubstituiString(',','.',edtSaldoRenovacao.Text) + ',' + fnsubstituiString(',','.',edtAbonoMes.Text)+',';

          case rgTipo.ItemIndex of
          0: begin
              sqlSAP := sqlSAP + '''RE'','''+detalheEvento+''''+',NULL';
            end;

          else
            sqlSAP := sqlSAP + '''CR'',';
            sqlSAP := sqlSAP + QuotedStr(detalheEvento)+','+ QuotedStr(complemento);
          end;




          empresId := QCadastroEMPRES_ID.AsString;
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empresId + ' and DATA_FECHA >= getdate()');
          DMConexao.AdoQry.Open;

          sqlSAP := sqlSAP + ', ' + empresId + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

          sqlSAP := sqlSAP + ')';
          DMConexao.ExecuteSql(sqlSAP);

          valorAbono := valorAbono + STRTOCURR(edtAbonoMes.Text);
          valorRenovacao := valorRenovacao + STRTOCURR(edtSaldoRenovacao.Text);

          QCredAlim.Next;
        end;

        DMConexao.AdoCon.CommitTrans;

        msginf('Atualiza��o realizada com sucesso!');

      except
        on e:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
          Screen.Cursor := crDefault;
          abort;
        end;
      end;

      Screen.Cursor := crDefault;
      qCredAlim.Requery;
      DecimalSeparator := dc;

      lblTotalAbono.Value := valorAbono;
      lblTotalRenovacao.Value := valorRenovacao;
      edtSaldoRenovacao.Text := '0,00';
      edtAbonoMes.Text := '0,00';
      dbDataRenovacao.SetFocus;

    end;

end;

procedure TFCadEmp.btnGravaFormasClick(Sender: TObject);
begin
  inherited;
  cLiberado := grdFormasPgto.Fields[2].Text;
  if (qFormasPgto.State in [dsEdit,dsInsert]) then
    qFormasPgto.Post;
end;

procedure TFCadEmp.btnCancelFormasClick(Sender: TObject);
begin
  inherited;
  if (qFormasPgto.State in [dsEdit,dsInsert]) then
    qFormasPgto.Cancel;
end;

procedure TFCadEmp.dsEstadosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not QCidades.Locate('NOME',lkpCIDADE.KeyValue,[])) then
  begin
    QCidades.Close;
    QCidades.Parameters.ParamByName('ESTADO_ID').Value := lkpESTADO.KeyValue;
    QCidades.Open;
  end;
end;

//TODO -oSidnei Sanches -cImplementar Teste Unit�rio

procedure TFCadEmp.GridCredLibKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
    cLiberado := GridCredLib.Fields[2].Text; //Taxas.Fields[3].Text;
end;

procedure TFCadEmp.grdFormasPgtoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
    cLiberado := grdFormasPgto.Fields[2].Text;
end;

procedure TFCadEmp.GridCredObrigarSenhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
  cLiberado := GridCredObrigarSenha.Fields[2].Text;
end;

procedure TFCadEmp.qUsu_WebAfterPost(DataSet: TDataSet);
var newValue : string;
begin
  inherited;
  cLiberado := '';

end;

procedure TFCadEmp.GridPbmKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
  cLiberado := GridPbm.Fields[2].Text;

end;

procedure TFCadEmp.btnInserirPosClick(Sender: TObject);
begin
  inherited;
  qUsu_Web.Append;
  dbGridUsuWeb.Fields[1].FocusControl;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SWEB_USU_ID');
  DMConexao.AdoQry.Open;
  dbGridUsuWeb.Fields[0].Value := IntToStr(DMConexao.AdoQry.Fields[0].Value);

end;

procedure TFCadEmp.dbGridUsuWebKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if dbGridUsuWeb.SelectedIndex = 3 then
  begin
    Key := UpCase(Key);
  end;
  if Key = #13 then
    dbGridUsuWeb.Perform(WM_KEYDOWN, VK_TAB, 0);



end;

procedure TFCadEmp.dbGridUsuWebKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
    cLiberado := dbGridUsuWeb.Fields[3].Text;
  
end;

procedure TFCadEmp.BtnInserirClick(Sender: TObject);
begin
  inherited;
  QEMP_DPTOS.Append;
  DBGridEmpDptos.Fields[1].FocusControl;
  try
    try
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SEMP_DPTOS');
      DMConexao.AdoQry.Open;
      DBGridEmpDptos.Fields[0].Value := IntToStr(DMConexao.AdoQry.Fields[0].Value);

    except
      on E : Exception do
      MsgInf('A conex�o com o Banco de dados Falhou');
    end;
  finally
    //DMConexao.AdoQry.Close;
    //DMConexao.AdoCon.Close;
  end;
end;

procedure TFCadEmp.BtnGravarClick(Sender: TObject);
begin
  inherited;
  if (QEMP_DPTOS.State in [dsInsert, dsEdit]) then
  begin
    //qUsu_Webusu_id.ReadOnly := false;
    QEMP_DPTOSEMPRES_ID.Value := QCadastroEMPRES_ID.Value;
    QEMP_DPTOS.Post;
  end
  else
  MsgInf('Para adicionar um usu�rio pressione o bot�o Inserir!');
  BtnInserir.SetFocus;
end;

procedure TFCadEmp.TabSheet5Show(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    //QEMP_DPTOS.Close;
    //qUsu_Web.Parameters.Items[0].Value := QCadastroEMPRES_ID.AsInteger;
    QEMP_DPTOS.Parameters[0].Value := QCadastroEMPRES_ID.AsInteger;
    QEMP_DPTOS.Open;
  end;
end;

procedure TFCadEmp.BtnApagarClick(Sender: TObject);
var bm : TBookmark;
begin
  inherited;
  if (TBitBtn(Sender).Enabled = False) then Abort;
  if  QEMP_DPTOS.RecordCount <= 0 then begin
    MsgInf('N�o existe registro para apagar, ou n�o existe registro selecinoado!');
    Abort;
  end;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('select top (1) setor_id from conveniados where empres_id = '+QEMP_DPTOSEMPRES_ID.AsString+' and setor_id = '+QEMP_DPTOSDEPT_ID.AsString);
  DMConexao.AdoQry.Open;

  if Application.MessageBox('Confirma a exclus�o deste registro?','Aten��o',MB_YESNO+MB_DEFBUTTON2+MB_ICONQUESTION) = IDYes then
  BEGIN
    TRY
      if (DMConexao.AdoQry.Fields[0].Value = null) then
      begin
        if DMConexao.ExecuteSql('UPDATE EMP_DPTOS SET DPTO_APAGADO = ''S'' WHERE DEPT_ID = ' + QEMP_DPTOSDEPT_ID.AsString) > 0 then begin
          bm := QEMP_DPTOS.GetBookmark;
          QEMP_DPTOS.Close;
          QEMP_DPTOS.Open;
          if QEMP_DPTOS.BookmarkValid(bm) then begin
            QEMP_DPTOS.GotoBookmark(bm);
          end;
          QEMP_DPTOS.FreeBookmark(bm);
        end else begin
          MsgErro('Erro ao excluir departamento');
        end;
      end
      except
      else
      begin
        MsgInf('N�o foi poss�vel excluir o departamento espec�ficado, Existe conveniados ativos vinculados a este departamento!');
        Abort;
      end;
    end;
  END;
end;

procedure TFCadEmp.TabSheet5Hide(Sender: TObject);
begin
  inherited;
  QEMP_DPTOS.Close;
end;

procedure TFCadEmp.DBGridEmpDptosKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  Key := UpCase(Key);
  if Key = #13 then
    dbGridUsuWeb.Perform(WM_KEYDOWN, VK_TAB, 0);

end;

procedure TFCadEmp.LimpaCombos;
begin
  if(QCadastroLIBERADA.IsNull) then
     DBCheckBox1.Checked  := False;
  if(QCadastroOBRIGA_SENHA.IsNull) then
     DBCheckBox18.Checked := False;
  if(QCadastroTODOS_SEGMENTOS.IsNull) then
     DBCheckBox2.Checked  := False;
  if(QCadastroBLOQ_ATE_PGTO.IsNull) then
     DBCheckBox3.Checked  := False;
  if(QCadastroFIDELIDADE.IsNull) then
     DBCheckBox8.Checked  := False;
  if(QCadastroACEITA_PARC.IsNull) then
     DBCheckBox6.Checked  := False;
  if(QCadastroEMITE_NF.IsNull) then
     DBCheckBox14.Checked := False;
  if(QCadastroVALE_DESCONTO.IsNull) then
     DBCheckBox11.Checked := False;
  if(QCadastroVENDA_NOME.IsNull) then
     DBCheckBox9.Checked  := False;
  if(QCadastroUTILIZA_RECARGA.IsNull) then
     DBCheckBox19.Checked := False;
end;

procedure TFCadEmp.QCadastroAfterOpen(DataSet: TDataSet);
begin
  inherited;

  LimpaCombos;


end;

procedure TFCadEmp.TabFichaShow(Sender: TObject);
begin
  inherited;
  ButInclui.Enabled := True;
  LimpaCombos;
end;

procedure TFCadEmp.GridCredLibTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QCredLib.Sort) > 0 then begin
     if Pos(' DESC',QCredLib.Sort) > 0 then QCredLib.Sort := Field.FieldName
                                                  else QCredLib.Sort := Field.FieldName+' DESC';
  end
  else QCredLib.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadEmp.dbGridAlimTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
try
  if Pos(Field.FieldName,QCredAlim.Sort) > 0 then begin
     if Pos(' DESC',QCredAlim.Sort) > 0 then QCredAlim.Sort := Field.FieldName
     else QCredAlim.Sort := Field.FieldName+' DESC';
  end
  else QCredAlim.Sort := Field.FieldName;
  except
 end;
end;

procedure TFCadEmp.Button2Click(Sender: TObject);
begin
  inherited;
  case rgTipo.ItemIndex of 0:begin

    end;

  else
    if rbTipoComplemento.ItemIndex < 0 then
    begin
      MsgInf('Voc� precisa escolher o tipo do complemento');
      Exit;
    end;
  end;
  InsereAlimentacaoRenovacao;
end;

procedure TFCadEmp.Button1Click(Sender: TObject);
begin
  inherited;
  InsereAlimRenovacaoCreditos;
end;



procedure TFCadEmp.btnCancelarConvAlimClick(Sender: TObject);
begin
  inherited;
//  QCredAlim.Requery;
//  QCredAlim.GotoBookmark(SavePlace);
//  QCredAlim.FreeBookmark(SavePlace);
//  Screen.Cursor := crDefault;
//
//  tSaldoRenovacao := 0;
//  tAbonoMes := 0;
//  tLimiteMes := 0;
end;

procedure TFCadEmp.QCredAlimBeforePost(DataSet: TDataSet);
begin
  inherited;
  SavePlace := QCredAlim.GetBookmark;
end;

procedure TFCadEmp.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFCadEmp.btnImportarClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId,empresId : String;
path,sqlSAP,detalheEvento,tipoComplementoSAP,valorRenovacaoString, valorAbonoString : String;
valorAbono, valorRenovacao, cesta: Currency;
erro: Boolean;
begin
  inherited;
  valorRenovacaoString :='0';
  valorAbonoString := '0';

  valorAbono := 0;
  valorRenovacao := 0;
  try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
    OD.Free;
    tExcel.Active := False;
    //tExcel.Close;
    tExcel.ConnectionString := path;
    tExcel.TableName:= 'credito$';
    tExcel.Active := True;

    try
      tExcel.Open;

    //except
    except on e : exception do
     //MsgErro('Ocorreu o seguinte erro: '+ E.Message);
     MsgErro('UCadEmp_4886 -  N�o foi poss�vel abrir o arquivo Excel.'+#13+'Feche o Excel e/ou certifique - se que o nome da planilha est� como "Planilha1" (Sem aspas)');
     //erro := True;
    end;
  except on E:Exception do
    MsgErro('UCadEmp_4890 - Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.Message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    btnImportar.Caption := 'Importar';
    Abort;
  end;

  if qCredAlimEMPRES_ID.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
  begin
    if not DMConexao.AdoCon.InTransaction then
      QCredAlim.Requery();
      renovacaoId := QCredAlimRENOVACAO_ID.AsString;
      DMConexao.AdoCon.BeginTrans;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add(' SELECT RENOVACAO_ID FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+ QuotedStr(renovacaoId));
      DMConexao.AdoQry.Open;

      if DMConexao.AdoQry.Fields[0].AsString <> ''  then begin
        msginf('Valores anteriores j� lan�ados ser�o sobrepostos pelos valores da planilha');
      end;

      DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = '+renovacaoId);
      DMConexao.ExecuteSql('DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP WHERE RENOVACAO_ID =  '+ renovacaoId);
      Screen.Cursor := crHourGlass;
      while not tExcel.Eof do begin
        if tExcel.fieldByName('CONV_ID').AsString <> '' then begin
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add(' SELECT CONV_ID FROM CONVENIADOS WHERE CONV_ID  = '+tExcel.fieldByName('CONV_ID').AsString+' AND LIBERADO = ''S'' AND APAGADO = ''N''');
          DMConexao.AdoQry.Open;
          if DMConexao.AdoQry.Fields[0].AsString <> ''  then begin
          try
            if tExcel.fieldByName('CONV_ID').AsString <> '' then begin
              qUpdate.SQL.Clear;
               if tExcel.fieldByName('VALOR RENOVA�AO').AsString <> '' then begin
                  qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
                  ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString) + ', ');
               end else begin
                    //valorRenovacaoString := fnsubstituiString(',','.',CurrToStr(valorRenovacao));
                    qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
                     ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + valorRenovacaoString + ', ');
               end;

              //qUpdate.SQL.Add('INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR,DATA_ALTERACAO) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
              // ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString) + ', ');
              if(QCredAlimempres_id.AsInteger = 1550) or (QCredAlimempres_id.AsInteger = 1552) or (QCredAlimempres_id.AsInteger = 1553) or (QCredAlimempres_id.AsInteger = 1696) then begin

                if tExcel.fieldByName('CESTA').AsString <> '' then begin
                    qUpdate.SQL.Add(fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) +  ',');
                end else begin
                    //valorAbonoString := fnsubstituiString(',','.',CurrToStr(valorAbono));
                    qUpdate.SQL.Add(valorAbonoString + ',');
                end;
              end
              else
              begin
                qUpdate.SQL.Add('0.00, ');
              end;
              qUpdate.SQL.Add(QuotedStr(DateTimeToStr(Now))+')');

              DMConexao.ExecuteSql(qUpdate.SQL.Text);
              case rgDetalheSAP.ItemIndex of
              0: begin
                    detalheEvento := 'RFI'
                 end;
              else
                detalheEvento := 'RFP';
              end;
              case rbTipoComplemento.ItemIndex of
              0:begin
                  tipoComplementoSAP := 'PR';
                end;
              else
                tipoComplementoSAP := 'FA';
              end;
              sqlSAP := '';
              if tExcel.fieldByName('VALOR RENOVA�AO').AsString <> '' then begin
                sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
               ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString )+',';
               end else begin
                //valorRenovacaoString := fnsubstituiString(',','.',CurrToStr(valorRenovacao));
                sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
               ',' + tExcel.fieldByName('CONV_ID').AsString + ',' + valorRenovacaoString +',';
              end;

                //sqlSAP := sqlSAP + 'INSERT INTO ALIMENTACAO_RENOVACAO_CREDITOS_SAP (RENOVACAO_ID, CONV_ID, RENOVACAO_VALOR, ABONO_VALOR, DATA_ALTERACAO, TIPO_EVENTO,DETALHE_EVENTO,COMPLEMENTO,EMPRES_ID,DATA_RENOVACAO,DATA_FECHA_EMP,DATA_VENC_EMP) VALUES(' + qCredAlimRENOVACAO_ID.AsString +
                //',' + tExcel.fieldByName('CONV_ID').AsString + ',' + fnsubstituiString(',','.',tExcel.fieldByName('VALOR RENOVA�AO').AsString )+',';

              if(QCredAlimempres_id.AsInteger = 1550) or (QCredAlimempres_id.AsInteger = 1552) or (QCredAlimempres_id.AsInteger = 1553) or (QCredAlimempres_id.AsInteger = 1696) then
              begin
                if tExcel.fieldByName('CESTA').AsString <> '' then begin
                    sqlSAP := sqlSAP + fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) + ',' + QuotedStr(DateTimeToStr(Now))+',';
                    //qUpdate.SQL.Add(fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) +  ',');
                end else begin
                    //valorAbonoString := fnsubstituiString(',','.',CurrToStr(valorAbono));
                    sqlSAP := sqlSAP + (valorAbonoString + ',') + QuotedStr(DateTimeToStr(Now))+',';
                end;
                //sqlSAP := sqlSAP + fnsubstituiString(',','.',tExcel.fieldByName('CESTA').AsString) + ',' + QuotedStr(DateTimeToStr(Now))+',';
              end
              else begin
                sqlSAP := sqlSAP + '0.00, '+QuotedStr(DateTimeToStr(Now))+',';
              end;

              case rgTipo.ItemIndex of
              0: begin
                sqlSAP := sqlSAP + QuotedStr('RE')+','+QuotedStr(detalheEvento)+','+'null';
              end;
              else
                sqlSAP := sqlSAP + QuotedStr('CR')+',';
                sqlSAP := sqlSAP + QuotedStr(detalheEvento) + ','+QuotedStr(tipoComplementoSAP);
              end;
              empresId := QCadastroEMPRES_ID.AsString;
              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;
              DMConexao.AdoQry.SQL.Add('select top(1)DATA_FECHA,DATA_VENC from DIA_FECHA where empres_id = ' + empresId + ' and DATA_FECHA >= getdate()');
              DMConexao.AdoQry.Open;

              sqlSAP := sqlSAP + ', ' + empresId + ',' + QuotedStr(dbDataRenovacao.Text) + ',' + QuotedStr(DMConexao.AdoQry.Fields[0].AsString) + ',' + QuotedStr(DMConexao.AdoQry.Fields[1].AsString) ;

              sqlSAP := sqlSAP + ')';
              DMConexao.ExecuteSql(sqlSAP);

              valorRenovacao := valorRenovacao + tExcel.fieldByName('VALOR RENOVA�AO').AsCurrency;
            end;

          except on E:Exception do begin
            MsgErro('UCadEmp_5018 - Erro ao incluir creditos');
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            tExcel.Close;
            abort;
          end;
          end;
          END
          ELSE begin
            msginf('UCadEmp_5027 - Por favor, verificar Status do Conveniado de Conv. ID - ' + tExcel.fieldByName('CONV_ID').AsString);
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            tExcel.Close;
            abort;

          end;
        end;
      tExcel.Next;
      Application.ProcessMessages;
    end;

    lblTotalAbono.Value := 0;
    lblTotalRenovacao.Value := valorRenovacao;
    DMConexao.AdoCon.CommitTrans;
    QCredAlim.Requery;
    Screen.Cursor := crDefault;
    if tExcel.State in [dsEdit, dsInsert] then
      tExcel.Close;

    msginf('Importa��o realizada com sucesso!');

  end
  else
     msginf('UCadEmp_5051 - Planilha n�o pertence a empresa '+qCredAlimEMPRES_ID.AsString + '. Verifique o arquivo.');


end;

procedure TFCadEmp.GridCredObrigarSenhaTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QCred_Obriga_Senha.Sort) > 0 then begin
     if Pos(' DESC',QCred_Obriga_Senha.Sort) > 0 then QCred_Obriga_Senha.Sort := Field.FieldName
                                                  else QCred_Obriga_Senha.Sort := Field.FieldName+' DESC';
  end
  else QCred_Obriga_Senha.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadEmp.TabSaldoShow(Sender: TObject);
begin
  inherited;
//  if(QCadastroTIPO_CREDITO.AsInteger = 2) then
//    rbRecarga.Visible := True;
  cbData.Text := IntToStr(YearOf(Date));
  if QCadastroTIPO_CREDITO.AsInteger = 2 then
  begin
    rbRecarga.Visible := True;
    rbRecarga.Checked := True;
  end
  else begin
    rbRecarga.Visible := False;
    rbRecarga.Checked := False;
  end;
   btnVisualizaSaldo.Click;

end;

procedure TFCadEmp.QOcorrenciasAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_EMPRES');
  DMConexao.AdoQry.Open;
  lbl4.Caption := IntToStr(DMConexao.AdoQry.Fields[0].AsInteger);
  QOcorrenciasatendimento_id.AsInteger := DMConexao.AdoQry.Fields[0].AsInteger;

  //Abre a query do comboBox de Status do atendimento.
  QStatusAtend.Open;

  //ABRE O SHOW MODAL PARA CONFIRMAR INFORMA��ES DO CONTATO.
  FAltContatoEmpres                    := TFAltContatoEmpres.Create(self);
  FAltContatoEmpres.txtNome1.Text      := QCadastroREPRESENTANTE.AsString;
  FAltContatoEmpres.txtTelefone1.Text  := QCadastroTELEFONE1.AsString;
  FAltContatoEmpres.txtTelefone2.Text  := QCadastroTELEFONE2.AsString;

  //SE O TELEFONE 2 FOR VAZIO OCULTA O MESMO PARA QUE SE
  //TORNE VIS�VEL APENAS SE O OPERADOR QUISER ADICION�-LO
  if QCadastroTELEFONE2.AsString = '' then begin
    FAltContatoEmpres.txtTelefone2.Visible := False;
  end;

  FAltContatoEmpres.ShowModal;

  if FAltContatoEmpres.ModalResult = mrOk then
  begin
    if(((FAltContatoEmpres.txtTelefone1.Text <> '') and (FAltContatoEmpres.txtTelefone1.Text <> QCadastroTELEFONE1.AsString)) or (FAltContatoEmpres.txtTelefone2.Text <> QCadastroTELEFONE2.AsString)) then
    begin
      QCadastro.Edit;
      if ((FAltContatoEmpres.txtTelefone2.Text <> '') and (FAltContatoEmpres.txtTelefone2.Text <> QCadastroTELEFONE2.AsString)) then begin
        QCadastroTELEFONE2.AsString := FAltContatoEmpres.txtTelefone2.Text;
      end;
        QCadastroTELEFONE1.AsString := FAltContatoEmpres.txtTelefone1.Text;
        QCadastro.Post;
    end;
    QOcorrenciasnome_solicitante.AsString := FAltContatoEmpres.txtNome1.Text;
    QOcorrenciastel_solictante.AsString   := FAltContatoEmpres.txtTelefone1.Text;
    txtMotivo.SetFocus;
  end
     else
      begin
        txtNome.SetFocus;
      end;
  //FIM DO MODAL


end;

procedure TFCadEmp.btnBuscarClick(Sender: TObject);
begin
  inherited;

  if QCadastro.IsEmpty then begin
      MsgInf('� necess�rio selecionar uma empresa para efetuar a busca.');
      PageControl1.TabIndex := 0;
      EdCod.SetFocus;
      Exit;
  end;

  if (dtInicial.Date = 0) and (txtProtocolo.Text = '') then begin
      MsgInf('Voc� deve informar a data ou n�mero do protocolo para efetuar a busca.');
      dtInicial.SetFocus;
      Exit;
  end
  else if ((dtInicial.Date <> 0) and (dtFinal.Date = 0)) then begin
      MsgInf('Digite a data final para realizar a busca.');
      dtFinal.SetFocus;
      Exit;
  end;

  BuscarOcorrencia();

  if QOcorrencias.IsEmpty then
  begin
      MsgInf('N�o h� ocorr�ncias para os crit�rios de busca aplicados.');
      dtInicial.SetFocus;
  end
  else begin
    QStatusAtend.Open;
  end;
end;

procedure TFCadEmp.BuscarOcorrencia();
var filtraPorData, filtraPorProtocolo : Boolean;

begin
  //inicializando as variaveis locais
  filtraPorData      := False;
  filtraPorProtocolo := False;
  QOcorrencias.Close;
  QOcorrencias.SQL.Clear;
  QOcorrencias.SQL.Add('select * from empresas_atendimento where');
  if (dtInicial.Date > 0  ) then begin
     QOcorrencias.SQL.Add(' convert(varchar(11),data_atendimento,103) between '+QuotedStr(dtInicial.Text)+' and '+QuotedStr(dtFinal.Text)+'');
     filtraPorData := True;  // FLAG PARA SETAR BUSCA POR DATA
  end;
  if ((txtProtocolo.Text <> '') and (filtraPorData = True)) then begin
     filtraPorProtocolo := True; // FLAG PARA SETAR BUSCA POR PROTOCOLO
     QOcorrencias.SQL.Add(' and atendimento_id = '+txtProtocolo.Text);
  end
  else if (filtraPorData = False) then
     QOcorrencias.SQL.Add(' atendimento_id = '+txtProtocolo.Text);

  //Filtrando a empresa prviamente pesquisada
  QOcorrencias.SQL.Add(' and empres_id = '+QCadastroEMPRES_ID.AsString);

  QOcorrencias.Open;
end;


procedure TFCadEmp.btn2Click(Sender: TObject);
begin
  inherited;
  limparCampos;
  txtNome.SetFocus;
  QOcorrencias.Append;
  lbl4.Caption                        := IntToStr(geraID);
  FAltContatoEmpres                   := TFAltContatoEmpres.Create(self);
  FAltContatoEmpres.txtNome1.Text     := QCadastroREPRESENTANTE.AsString;
  FAltContatoEmpres.txtTelefone1.Text := QCadastroTELEFONE1.AsString;
  FAltContatoEmpres.txtTelefone2.Text := QCadastroTELEFONE2.AsString;
end;

procedure TFCadEmp.btnAlterarTodosClick(Sender: TObject);
begin
  inherited;
  case TFSelTipoImp.AbrirJanela(['Alterar todos para S','Alterar todos para N'],0,'Selecione a Op��o','Selecione o tipo de altera��o') of
    0 : AlterarTodosCred('S');
    1 : AlterarTodosCred('N');
  end;
end;

procedure TFCadEmp.AlterarTodosCred(SN:Char);
var sql, cadastro, detalhe: string;
begin
  detalhe := 'Empr ID';
  if SN = 'N' then
  begin
    if MsgSimNao('Deseja Bloquear todas os estabelecimentos para essa empresa?') then
    begin
      FrmOcorrencia := TFrmOcorrencia.Create(Self);
      if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
        Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
        Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
        FreeAndNil(FrmOcorrencia);
      end;

      Screen.Cursor := crHourGlass;
      QCredLib.First;

      while not QCredLib.Eof do
      begin
        cadastro:= 'Cred. '+QCredLibCRED_ID.AsString+' lib. para esta empresa';
        if ExisteCredBloq(QCadastroEMPRES_ID.AsInteger,QCredLibCRED_ID.AsInteger) then
        begin
          sql :=  'update EMP_CRED_LIB set liberado = ''N'''+
                ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+' and CRED_ID = '+QCredLibCRED_ID.AsString;
        end
        else
        begin
          sql :=  {'SET IDENTITY_INSERT EMP_CRED_LIB ON ' +}
                'insert into EMP_CRED_LIB(EMPRES_ID, CRED_ID, LIBERADO) '+
                ' values('+QCadastroEMPRES_ID.AsString+','+QCredLibCRED_ID.AsString+',''N'')';
        end;

        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := sql;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.GravaLog('FCadEmp','Cred. Bloq.','S','N',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo);

        QCredLib.Next;
      end;
      Screen.Cursor := crDefault;
      MsgInf('Altera��es efetuadas com sucesso!');
    end
  end
  else
  begin
    if MsgSimNao('Deseja Liberar todas os estabelecimentos para esta empresa?') then
    begin
      FrmOcorrencia := TFrmOcorrencia.Create(Self);
      if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
        Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
        Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
        FreeAndNil(FrmOcorrencia);
      end;

      Screen.Cursor := crHourGlass;
      QCredLib.First;

      while not QCredLib.Eof do
      begin
        cadastro:= 'Cred. '+QCredLibCRED_ID.AsString+' lib. para esta empresa';
        sql :=  'delete from EMP_CRED_LIB ';
        sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and CRED_ID = '+QCredLibCRED_ID.AsString;

        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Text := sql;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.GravaLog('FCadEmp','Cred. Bloq.','N','S',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo);


        QCredLib.Next;
      end;
      Screen.Cursor := crDefault;
      MsgInf('Altera��es efetuadas com sucesso!');
    end;
  end;

  QCredLib.Requery();
  QCredLib.EnableControls;

end;

procedure TFCadEmp.btnBuscaSegClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  QCredLib.Close;
  QCredLib.SQL.Clear;
  QCredLib.SQL.Add(' Select cred.cred_id, cred.nome, coalesce(credlib.liberado,''S'') as liberado');
  QCredLib.SQL.Add(' from credenciados cred');
  QCredLib.SQL.Add(' left join emp_cred_lib credlib on cred.cred_id = credlib.cred_id');
  QCredLib.SQL.Add(' and ((credlib.empres_id = ' + QCadastroEMPRES_ID.AsString + ') or (credlib.empres_id is null)) ');
  if DBSegmento.KeyValue > 0 then
  begin
    QCredLib.SQL.Add(' inner join segmentos seg on seg.seg_id = cred.seg_id and seg.seg_id =' + DBSegmento.KeyValue);
  end;
  QCredLib.SQL.Add(' order by cred.nome');
  QCredLib.Open;
  Screen.Cursor := crDefault;

end;

procedure TFCadEmp.btnFirstBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.First;
  lbl4.Caption:= IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadEmp.btnFirstCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.First;
end;

procedure TFCadEmp.btnLastBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Last;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadEmp.btnLastCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Last;
end;

procedure TFCadEmp.btnNextBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Next;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadEmp.btnNextCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Next;
end;

procedure TFCadEmp.btnPriorBClick(Sender: TObject);
begin
  prVerfEAbreCon(QOcorrencias);
  QOcorrencias.Prior;
  lbl4.Caption := IntToStr(QOcorrenciasatendimento_id.AsInteger);
end;

procedure TFCadEmp.btnPriorCClick(Sender: TObject);
begin
  prVerfEAbreCon(QStatusAtend);
  QStatusAtend.Prior;
end;

procedure TFCadEmp.ButCancelaClick(Sender: TObject);
begin
  inherited;
  QOcorrencias.Close;
end;

procedure TFCadEmp.DSOcorrenciasStateChange(Sender: TObject);
begin
  inherited;
  habilitarBotoes;
end;

function TFCadEmp.geraID: Integer;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_EMPRES');
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].AsInteger;
end;


procedure TFCadEmp.grdOcorrencias2DblClick(Sender: TObject);
begin
  inherited;
  if not QOcorrencias.IsEmpty then begin
    pgc1.ActivePage := ts3;
    lbl4.Caption := QOcorrenciasatendimento_id.AsString;
  end;
end;

procedure TFCadEmp.limparCampos;
Var
i : Integer;
begin
  for i := 0 to ComponentCount -1 do
  begin
    if Components[i] is TEdit then
    begin
      TEdit(Components[i]).Text := '';
    end;

    if Components[i] is TMemo then
    begin
      TMemo(Components[i]).Clear;
    end;
  end;
end;

procedure TFCadEmp.QOcorrenciasBeforePost(DataSet: TDataSet);
begin
  if QCadastroEMPRES_ID.AsInteger = 0 then begin
      MsgInf('� necess�rio selecionar uma empresa para efetuar a busca.');
      PageControl1.TabIndex := 0;
      EdCod.SetFocus;
      Exit;
  end;
  
end;

procedure TFCadEmp.ts1Show(Sender: TObject);
begin
  inherited;
  //QOcorrencias.Open;
end;

procedure TFCadEmp.ts3Show(Sender: TObject);
begin
  inherited;
  txtNome.SetFocus;
  habilitarBotoes;
  //Busca ultimas autoriza��es na ocorr�ncias da empresa (Operador e Data)
  if not QCadastro.IsEmpty then
  begin
     buscarUltimasAlteracoes;
     if not DMConexao.Query1.IsEmpty then begin
        txtDataAlteracao.Text     := DMConexao.Query1.Fields[0].AsString; // Fields[0] = DataAltera��o
        txtOperadorAlteracao.Text := DMConexao.Query1.Fields[1].AsString; // Fields[1] = Operdaor
     end;
  end;


end;


procedure TFCadEmp.buscarUltimasAlteracoes;
begin
  DMConexao.Query1.Close;
  DMConexao.Query1.SQL.Clear;
  DMConexao.Query1.SQL.Add('SELECT top 1 coalesce (data_hora,0) as dt_alteracao, coalesce (operador, '''')');
  DMConexao.Query1.SQL.Add( 'FROM LOGS_OCORRENCIAS ');
  DMConexao.Query1.SQL.Add(' WHERE id = '+Qcadastroempres_id.asString+' and janela = ''ts1''');
  DMConexao.Query1.SQL.Add(' order by data_hora desc');
  DMConexao.Query1.Open;
end;

procedure TFCadEmp.grdOcorrencias2DrawColumnCell(Sender: TObject; const Rect:
    TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  with(grdOcorrencias2) do
  begin
    if DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 3 then begin
      //FORMATA��O PARA LINHAS COM STATUS 3 - FINALIZADO E/OU RESOLVIDO
      // PARA ESTA CONDI��O A LINHA DEVE SER AZUL
      grdOcorrencias2.Canvas.Brush.Color := RGB(240, 255, 255);
      grdOcorrencias2.Canvas.Font.Color:= clBlack;
    end
    else if((DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 1) or (DataSource.DataSet.FieldByName('STATUS_ID').AsInteger = 2)) then
    begin
      //FORMATA��O PARA LINHAS COM STATUS 1 E 2 OU SEJA PENDENTES
      // PARA ESTA CONDI��O A LINHA DEVE SER VERMELHA
      grdOcorrencias2.Canvas.Brush.Color := RGB(255,106,106);
      grdOcorrencias2.Canvas.Font.Color:= clWhite;
    end;

      grdOcorrencias2.Canvas.FillRect(Rect);
      Canvas.FillRect(Rect);

      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TFCadEmp.habilitarBotoes;
begin
  btCancelar.Enabled  := QOcorrencias.State in [dsEdit,dsInsert];
  btGravar.Enabled    := QOcorrencias.State in [dsEdit,dsInsert];
  btn2.Enabled        := (QOcorrencias.State = dsBrowse);
  btn1.Enabled        := (QOcorrencias.State = dsBrowse);
  btn3.Enabled        := (QOcorrencias.State = dsBrowse); 
end;

procedure TFCadEmp.JvBitBtn1Click(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then begin
    QLogOcorrencias.Close;
    QLogOcorrencias.SQL.Clear;
    QLogOcorrencias.SQL.Add('SELECT * FROM logs_ocorrencias WHERE id = '+QCadastroEMPRES_ID.AsString+'');
    if((dtIniHistorico.Date > 0) and (dtFimHistorico.Date > 0)) then begin
        QLogOcorrencias.SQL.Add(' and convert(varchar(10), DATA_HORA, 103) between '+QuotedStr(dtIniHistorico.Text)+' and '+QuotedStr(dtFimHistorico.Text));
        QLogOcorrencias.SQL.Add(' and janela  = ''ts1''');
    end;
    QLogOcorrencias.Open;
  end;
end;

procedure TFCadEmp.ts2Show(Sender: TObject);
begin
  PanelFinalizado.Color := RGB(240,255,255);
  PanelPendente.Color   := RGB(255,106,106);
end;

procedure TFCadEmp.EdFantasiaKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then ButBusca.Click;  
end;

procedure TFCadEmp.EdFantasiaExit(Sender: TObject);
begin
//  send := (sender as TEdit).Text; 
//  if Trim(send) <> '' then
//  begin
//    if send[1] = ',' then Delete(send,1,1);
//    if send[Length(send)] = ',' then delete(send,1,Length(send));
//   (sender as TEdit).Text := send;
//  end;
end;

procedure TFCadEmp.GridEmpCredParcelamentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
    cLiberado := GridEmpCredParcelamento.Fields[2].Text;
end;

procedure TFCadEmp.GridEmpCredParcelamentoColExit(Sender: TObject);
var coluna : Integer;
    nomeFantasia : string;
begin
  inherited;
  if QParcelaEspecifica.State in [dsEdit] then QParcelaEspecifica.Post;
  coluna := GridEmpCredParcelamento.SelectedIndex;
  if GridEmpCredParcelamento.Columns[1].Field.AsString = '' then
  begin
    if coluna = 0 then begin
      nomeFantasia := DMConexao.ExecuteQuery('SELECT fantasia FROM credenciados WHERE cred_id = '+QuotedStr(GridEmpCredParcelamento.Columns[0].Field.AsString)+'');
      GridEmpCredParcelamento.Columns[1].Field.AsString := nomeFantasia;
    end;
  end;
end;

procedure TFCadEmp.ParcelamentoEspecificoShow(Sender: TObject);
begin
  inherited;
  QParcelaEspecifica.Close;
  if not QCadastro.IsEmpty then
  begin
    CarregaGrid;
  end;
end;

procedure TFCadEmp.ParcelamentoEspecificoHide(Sender: TObject);
begin
  inherited;
  QParcelaEspecifica.Close;
end;

procedure TFCadEmp.btnGravaEmpLibClick(Sender: TObject);
var linhasAfetadas : Integer;
var cred_id, qtde_parcelas, empres_id : Integer;
var nomeEstabelecimeto, liberado : string;

begin
  inherited;
  if (QParcelaEspecifica.State in [dsEdit,dsInsert]) then
  begin
    cred_id         := StrToInt(QParcelaEspecificacred_id.AsString);
    empres_id       := QCadastroEMPRES_ID.AsInteger;
    liberado        := QParcelaEspecificaliberado.AsString;

    QParcelaEspecificaNOME_ESTABELECIMENTO.AsString;
      QParcelaEspecificaliberado.AsString;
    QParcelaEspecifica.SQL.Text := '';
    QParcelaEspecifica.SQL.Text := 'INSERT INTO dbo.EMP_CRED_DESCONTO_ESPECIFICO '+
           '(EMPRES_ID' +
           ',CRED_ID'   +
           ',LIBERADO)'  +
       'VALUES '  +
           '(' + IntToStr(empres_id)     +
           ',' + IntToStr(cred_id)       +
           ',' + QuotedStr((liberado))   +
           ')';

    QParcelaEspecifica.ExecSQL;

    CarregaGrid;

  end
  else
  MsgInf('Para adicionar um tipo de desconto espec�fico pressione o bot�o Inserir!');
  btnInserirNovaParcela.SetFocus;
end;

procedure TFCadEmp.CarregaGrid();
var empres_id : Integer;
var strQuery : String;
begin
 strQuery := 'select '+
                'CDE.empres_id, '+
                'CDE.cred_id, '+
                'CDE.liberado, '+
                'C.NOME AS NOME_ESTABELECIMENTO '+
              'FROM EMP_CRED_DESCONTO_ESPECIFICO CDE '+
              'INNER JOIN CREDENCIADOS C ' +
              'ON cde.cred_id = c.cred_id '+
              'AND CDE.EMPRES_ID = '+QCadastroEMPRES_ID.AsString;

 QParcelaEspecifica.SQL.Clear;
 QParcelaEspecifica.SQL.Add(strQuery);
 QParcelaEspecifica.Open;

end;

procedure TFCadEmp.btnInserirNovaParcelaClick(Sender: TObject);
begin
  inherited;
  QParcelaEspecifica.Append;
  GridEmpCredParcelamento.Fields[1].FocusControl;
end;

procedure TFCadEmp.QManutencaoLimiteConvBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.IsEmpty then
    QManutencaoLimiteConv.Parameters[0].Value := QCadastroEMPRES_ID.AsString;
end;

procedure TFCadEmp.LimiteConvShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
    QManutencaoLimiteConv.Open;
end;

procedure TFCadEmp.BitBtn5Click(Sender: TObject);
var limiteMesOld : currency;
begin
// if Application.MessageBox('Confirma a altera��o das informa��es?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
//  begin
//    tLimiteMes := StrtoCurr(dbGrigManutLimitesConveniados.Fields[2].Text);
//
//    SavePlace := QCredAlim.GetBookmark;
//    try
//      DMConexao.AdoCon.BeginTrans;
//      // Altera��o de Limite M�s
//      if VarIsNull(QManutencaoLimiteConvlimite_mes.OldValue) then
//        limiteMesOld := 0
//      else
//        limiteMesOld := QManutencaoLimiteConvlimite_mes.OldValue;
//
//      if (limiteMesOld <> tLimiteMes) then
//      begin
//        DMConexao.GravaLog('FCadConv','LIMITE_MES',FormatDinBR(limiteMesOld),FormatDinBR(tLimiteMes),Operador.Nome,'Altera��o',
//                            QManutencaoLimiteConvconv_id.AsString,'');
//        qUpdate.SQL.Text := ' update conveniados set limite_mes = ' + fnsubstituiString(',','.',CurrToStr(tLimiteMes)) + ' where conv_id = ' + QManutencaoLimiteConvconv_id.AsString;
//        DMConexao.ExecuteSql(qUpdate.SQL.Text);
//      end;
//
//      DMConexao.AdoCon.CommitTrans;
//
//    except
//      on e:Exception do
//      begin
//         DMConexao.AdoCon.RollbackTrans;
//         Screen.Cursor := crDefault;
//         MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
//         abort;
//       end;
//    end;
//  end;
//  QManutencaoLimiteConv.GotoBookmark(SavePlace);
//  QManutencaoLimiteConv.FreeBookmark(SavePlace);
//  Screen.Cursor := crDefault;
//  tLimiteMes := 0;
//  //QManutencaoLimiteConv.Requery;

//    if VarIsNull(QManutencaoLimiteConvlimite_mes.OldValue) then
//        limiteMesOld := 0
//      else
//        limiteMesOld := QManutencaoLimiteConvlimite_mes.OldValue;
////
      //tLimiteMes := StrtoCurr(dbGrigManutLimitesConveniados.Fields[2].Text);
      //limiteMesOld := QManutencaoLimiteConvlimite_mes.OldValue;

        DMConexao.GravaLog('FCadConv','LIMPA_SENHA','','',Operador.Nome, 'Limpeza de senha','CONV_ID','NOME', 'Limpeza de senha para novo cart�o');
      
//      DMConexao.AdoQry.Close;
//      DMConexao.AdoQry.SQL.Clear;
//      DMConexao.AdoQry.SQL.Text := ' update conveniados set limite_mes = ' + fnsubstituiString(',','.',CurrToStr(tLimiteMes)) + ' where conv_id = ' + QManutencaoLimiteConvconv_id.AsString;
//      DMConexao.AdoQry.ExecSQL;

      QManutencaoLimiteConv.Requery();
      //end;
end;

procedure TFCadEmp.TabSheet6Show(Sender: TObject);
begin
  inherited;
  if ((not QCadastro.IsEmpty) or (not QManutencaoCartaoConveniados.Active)) then
  begin
    QManutencaoCartaoConveniados.Parameters[0].Value := QCadastroEMPRES_ID.Value;
    QManutencaoCartaoConveniados.Open;
  end;
end;

procedure TFCadEmp.SolicitaNovaViaTodosConveniados;
var codimp: string;
begin
//  DMConexao.Config.Open;
//  prVerfEAbreCon(QCartoes);
//  if QCartoes.RecordCount > 0 then
//  begin
//    while not QCartoes.Eof do
//    begin
//      QCartoes.Edit;
//      QCartoesJAEMITIDO.AsString := 'N';
//      if QCadastroUSA_COD_IMPORTACAO.AsString = 'S' then begin
//        repeat
//          codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
//        until (verificaCartaoExistente(codimp));
//        QCartoesCODCARTIMP.AsString := codimp;
//      end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
//        QCartoesCODCARTIMP.AsString := QCartoesCODIGO.AsString
//      else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
//      begin
//        if QCartoesTITULAR.AsString = 'S' then
//          QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp
//        else
//          QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImp(False);
//      end
//      else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
//        QCartoesCODCARTIMP.AsString := DMConexao.ObterCodCartImpMod1(QCartoesCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QCartoes.AsString)
//      else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
//        QCartoesCODCARTIMP.AsString :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
//      if QCartoes.State in [dsEdit] then begin
//        if (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '')) then begin
//          QCartoes.Post;
//        end;
//      end;
//      QCartoes.Next;
//    end;
//  end else
//    QCartoes.Close;
//  DMConexao.Config.Close;
//  MsgInf('Nova via gerada com sucesso!');
end;

procedure TFCadEmp.btnSoliciarNovaViaClick(Sender: TObject);
var codimp, cvv, cardImpNovo: string;

begin
  inherited;
  if QManutencaoCartaoConveniados.RecordCount > 0 then
  begin
    if Application.MessageBox('Confirma a gera��o de nova via do cart�o para todos os conveniados da empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
        DMConexao.Config.Open;
        try
          DMConexao.AdoCon.BeginTrans;
          While not QManutencaoCartaoConveniados.Eof do
          begin
            QCartoes.Parameters[0].Value := QCadastroEMPRES_ID.Value;
            QCartoes.Parameters[1].Value := QManutencaoCartaoConveniadosconv_id.Value;
            QCartoes.Open;
            while not QCartoes.Eof do
            begin

    //        DMConexao.AdoQry.Close;
    //        DMConexao.AdoQry.SQL.Clear;
    //        DMConexao.AdoQry.SQL.Add('insert into cartoes_temp select * from cartoes where CARTAO_ID = '+QCartoesCARTAO_ID.AsString+'');
//              DMConexao.ExecuteSql('insert into cartoes_temp select * from cartoes where CARTAO_ID = '+QCartoesCARTAO_ID.AsString+'');
//
//              QCartoesTemp.Parameters[0].Value := QCartoesCARTAO_ID.Value;
//              QCartoesTemp.Open;
              QCartoes.Edit;
              QCartoesJAEMITIDO.AsString := 'N';

              //gera novo carImpNovo
              if QCadastroUSA_COD_IMPORTACAO.AsString = 'S' then begin
                repeat
                  codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
                until (verificaCartaoExistente(codimp) and verificaCartaoExistenteHistorico(codimp));

              end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
                codimp := QCartoesCODIGO.AsString
              else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
              begin
                if QCartoesTITULAR.AsString = 'S' then
                  codimp := DMConexao.ObterCodCartImp
                else
                  codimp := DMConexao.ObterCodCartImp(False);
              end
              else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
                codimp := DMConexao.ObterCodCartImpMod1(QCartoesCONV_ID.AsInteger, QCadastroEMPRES_ID.AsInteger,QManutencaoCartaoConveniadoschapa.AsString)
              else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
                codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));

              //fim gera��o de cart�o
              cardImpNovo := codimp;
              QCartoesCODCARTIMP.AsString := cardImpNovo;

              //Gera o CVV
              cvv := GerarCVV(cardImpNovo);
              QCartoesCVV.AsString := cvv;


              if QCartoes.State in [dsEdit] then begin
                  DMConexao.GravaLogSemTelaOcorrencia(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '');
                  QCartoes.Post;
                  DMConexao.ExecuteSql('INSERT INTO CARTOES_HISTORICO SELECT CARTAO_ID, CONV_ID, EMPRES_ID, CODCARTIMP, CVV, SENHA, GETDATE() FROM CARTOES WHERE CARTAO_ID = '  +  QCartoesCARTAO_ID.AsString);
              end;
              //QCartoesTemp.Post;
             // QCartoes.Close;
              QCartoes.Next;
            end;
            QCartoes.Close;

            QManutencaoCartaoConveniados.Next;
          end;
          DMConexao.AdoCon.CommitTrans;
          msginf('Atualiza��o realizada com sucesso!');
        except
          on e:Exception do
          begin
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
            Screen.Cursor := crDefault;
            abort;
          end;
        end;
        DMConexao.Config.Close;
        MsgInf('Nova(s) via(s) gerada(s) com sucesso!');
    end;
  end;
end;

procedure TFCadEmp.TabSheet6Exit(Sender: TObject);
begin
  inherited;
  if QManutencaoCartaoConveniados.Active then
    QManutencaoCartaoConveniados.Close;
end;

procedure TFCadEmp.btnReimprimirClick(Sender: TObject);
var CodCartao,oldEmpresID, oldNome, oldTitular, oldParent, oldCpf, oldRg: string;
  oldDataNasc: TDate;
begin
  inherited;
  if Application.MessageBox('Confirma a reimpress�o de todos os cart�es da empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    While not QManutencaoCartaoConveniados.Eof do
    begin

      QCartoes.Parameters[0].Value := QCadastroEMPRES_ID.Value;
      QCartoes.Parameters[1].Value := QManutencaoCartaoConveniadosconv_id.Value;
      QCartoes.Open;
      while not QCartoes.Eof do
      begin
        DMConexao.Config.Open;
        if (DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S') or
           (DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S') or
           (DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S') or
           (DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S') then
          CodCartao:= QCartoes.FieldByName('CODCARTIMP').AsString
        else
          CodCartao:= QCartoes.FieldByName('CODIGO').AsString + PadL(QCartoes.FieldByName('DIGITO').AsString,2,'0');
        //if not (DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Inclus�o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '')) then
          //Abort;
        //if MsgSimNao('Confirma a emiss�o de segunda via do cart�o n� '+CodCartao+sLineBreak+'no nome de '+QCartoes.FieldByName('NOME').AsString) then
        //begin
        if DMConexao.ConfigEMITE_NOVO_CART_2VIA.AsString = 'S' then
        begin
          oldNome     := QCartoes.FieldByName('NOME').AsString;
          oldTitular  := QCartoes.FieldByName('TITULAR').AsString;
          oldParent   := QCartoes.FieldByName('PARENTESCO').AsString;
          oldCpf      := QCartoes.FieldByName('CPF').AsString;
          oldRg       := QCartoes.FieldByName('RG').AsString;
          oldEmpresID := QCartoes.FieldByName('EMPRES_ID').AsString;
          if QCartoes.FieldByName('DATA_NASC').Value > 0 then
            oldDataNasc:= QCartoes.FieldByName('DATA_NASC').AsDateTime;
          QCartoes.Edit;
          QCartoes.FieldByName('LIBERADO').AsString:= 'I';
          if oldTitular = 'S' then
            QCartoes.FieldByName('TITULAR').AsString:= 'N';
          QCartoes.Post;
          QCartoes.Append;
          QCartoes.FieldByName('NOME').AsString:= oldNome;
          QCartoes.FieldByName('LIBERADO').AsString:= 'S';
          QCartoes.FieldByName('TITULAR').AsString:= oldTitular;
          QCartoes.FieldByName('PARENTESCO').AsString:= oldParent;
          QCartoes.FieldByName('DTCADASTRO').AsDateTime:= Now;
          QCartoes.FieldByName('CPF').AsString:= oldCpf;
          QCartoes.FieldByName('RG').AsString:= oldRg;
          QCartoes.FieldByName('VIA').AsInteger:= 2;
          QCartoes.FieldByName('EMPRES_ID').AsInteger := StrToInt(oldEmpresID);

          if oldDataNasc > 0 then
            QCartoes.FieldByName('DATA_NASC').AsDateTime:= oldDataNasc;
          QCartoes.Post;
        end
        else
        begin
          QCartoes.Edit;
          QCartoes.FieldByName('JAEMITIDO').AsString:= 'N';
          QCartoes.FieldByName('VIA').AsInteger:= 2;
          QCartoes.Post;
          //atualizaCodImp;
        end;
        QCartoes.Next;
      end;
      //end;
      QCartoes.Close;
      QManutencaoCartaoConveniados.Next;

    end;
    MsgInf('Os cart�es podem ser reimpressos com sucesso');
    DMConexao.Config.Close;
  end;

end;

procedure TFCadEmp.btnImportLimpaSenha(Sender: TObject);
var OD : TOpenDialog;
renovacaoId : String;
path,strSql : String;
erro: Boolean;
Conv_id : String;
nome : String;
senha,codcartimp, chapa, id : String;


begin
  inherited;
 try

    if not ((chkLiberaConv.Checked) or (chkBloqueiaConv.Checked) or (chkLimpaSenha.Checked)) then
    begin
      MsgInf('Selecione a a��o que deseja executar ao importar a planilha!');
      chkBloqueiaConv.SetFocus;
      Exit;
    end;
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
     OD.Free;
    tExcel.Active := False;
    tExcel.ConnectionString := path;
    tExcel.TableName := 'conv$';
    tExcel.Active := True;
    try
      tExcel.Open;
    except
      MsgErro('UCadEmp_5929 - Erro! N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "conv" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('UCadEmp_5933 - Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
          if erro then begin
    Screen.Cursor := crDefault;
    ImporLimpaSenha.Caption := 'Importar';
    Abort;
  end;
  Screen.Cursor := crHourGlass;
  if(chkLimpaSenha.Checked) then
  begin
    while not tExcel.Eof do begin
      try
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        nome := tExcel.FieldByName('nome').AsString;
        chapa :=  tExcel.FieldByName('chapa').AsString;
        id := QCadastroEMPRES_ID.AsString;

        DMConexao.AdoQry.SQL.Text :=' SELECT C.CODCARTIMP, C.CONV_ID, C.NOME, CONV.CHAPA, C.CONV_ID ' +
                                    ' FROM CARTOES C INNER JOIN CONVENIADOS CONV ON (CONV.CONV_ID = C.CONV_ID) ' +
                                    ' WHERE CONV.CHAPA = '+chapa+' AND CONV.EMPRES_ID='+id+' AND C.TITULAR = ''S'' AND CONV.LIBERADO = ''S'' ';

        //select CODCARTIMP from cartoes where CONV_ID = '+ conv_id +' AND TITULAR = ''S'' ';
        //DMConexao.AdoQry.ExecSQL;
        DMConexao.AdoQry.Open;

        codcartimp := DMConexao.AdoQry.FieldByName('CODCARTIMP').AsString;
        conv_id := DMConexao.AdoQry.FieldByName('CONV_ID').AsString;

        if ((nome = DMConexao.AdoQry.FieldByName('NOME').AsString) AND (chapa = DMConexao.AdoQry.FieldByName('CHAPA').AsString)) then begin
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;

          DMConexao.AdoQry.SQL.Text:= 'update cartoes set senha = '''+(Crypt('E',Copy(codcartimp, 13, 4),'BIGCOMPRAS')) +
                   ''' where conv_id = '+ conv_id + ' AND NOME LIKE ''%' + nome + '%''';

          DMConexao.AdoQry.ExecSQL;
          DMConexao.GravaLogSemTelaOcorrencia('FCadConv','Senha', '', '', Operador.Nome, 'Limpeza de Senha',conv_id, '', 'Altera��o de senha do para novos cart�es');
        end else begin
         MsgErro('UcadEmp_5970 - Erro ao incluir '+nome);
        end;
      except on E:Exception do begin
          MsgErro('UCadEmp_5973 - Erro ao selecionar empresa!');
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          tExcel.Close;
          abort;
        end;
      end;

      tExcel.Next;
      Application.ProcessMessages;
    end;
  end
  else if(chkBloqueiaConv.Checked) then
  begin
      while not tExcel.Eof do begin
        try
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          nome := tExcel.FieldByName('nome').AsString;
          chapa :=  tExcel.FieldByName('chapa').AsString;
          id := QCadastroEMPRES_ID.AsString;

          DMConexao.AdoQry.SQL.Text :=' SELECT CONV.CONV_ID, CONV.LIBERADO, CONV.CHAPA  ' +
                                      ' FROM CONVENIADOS CONV ' +
                                      ' WHERE CONV.CHAPA = '+chapa+' AND CONV.EMPRES_ID='+id;


          DMConexao.AdoQry.Open;

          if not DMConexao.AdoQry.Eof then
          begin
            conv_id := DMConexao.AdoQry.FieldByName('CONV_ID').AsString;

            //if (chapa = DMConexao.AdoQry.FieldByName('CHAPA').AsString) then begin
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;

            DMConexao.AdoQry.SQL.Text:= 'update conveniados set liberado = '+ QuotedStr('N') +
                     ' where conv_id = '+ conv_id;

            DMConexao.AdoQry.ExecSQL;
            DMConexao.GravaLogSemTelaOcorrencia('FCadConv','Liberado', 'S', 'N', Operador.Nome, 'Bloqueio Conv',conv_id, '', 'Bloqueio via importa��o de planilha');
          end;

          //end else begin
           //MsgErro('A Chapa '+chapa +' n�o foi encontrada na empresa '+ id);
          //end;
        except on E:Exception do begin
            MsgErro(E.ClassName+ 'Erro gerado, com mensagem: '+ e.Message);
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            tExcel.Close;
            abort;
        end;
        end;

        tExcel.Next;
        Application.ProcessMessages;
      end;
  end
  else
  begin
    while not tExcel.Eof do begin
        try
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          nome := tExcel.FieldByName('nome').AsString;
          chapa :=  tExcel.FieldByName('chapa').AsString;
          id := QCadastroEMPRES_ID.AsString;

          DMConexao.AdoQry.SQL.Text :=' SELECT CONV.CONV_ID, CONV.LIBERADO, CONV.CHAPA  ' +
                                      ' FROM CONVENIADOS CONV ' +
                                      ' WHERE CONV.CHAPA = '+chapa+' AND CONV.EMPRES_ID='+id;


          DMConexao.AdoQry.Open;

          if not DMConexao.AdoQry.Eof then
          begin
            conv_id := DMConexao.AdoQry.FieldByName('CONV_ID').AsString;

            //if (chapa = DMConexao.AdoQry.FieldByName('CHAPA').AsString) then begin
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;

            DMConexao.AdoQry.SQL.Text:= 'update conveniados set liberado = '+ QuotedStr('S') +
                     ' where conv_id = '+ conv_id;

            DMConexao.AdoQry.ExecSQL;
            DMConexao.GravaLogSemTelaOcorrencia('FCadConv','Liberado', 'N', 'S', Operador.Nome, 'Libera Conv',conv_id, '', 'Libera conv via importa��o de planilha');
          end;

          //end else begin
           //MsgErro('A Chapa '+chapa +' n�o foi encontrada na empresa '+ id);
          //end;
        except on E:Exception do begin
            MsgErro(E.ClassName+ 'Erro gerado, com mensagem: '+ e.Message);
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            tExcel.Close;
            abort;
        end;
        end;

        tExcel.Next;
        Application.ProcessMessages;
      end;
  end;
  //DMConexao.AdoCon.CommitTrans;
  //QManutencaoLimiteConv.Requery;
  Screen.Cursor := crDefault;
  if tExcel.State in [dsEdit, dsInsert] then
    tExcel.Close;
  msginf('Opera��o Finalizada');

 end;


procedure TFCadEmp.btnImportarLimiteConvClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId : String;
path,strSql : String;
erro: Boolean;
begin
  inherited;
 try
    OD := TOpenDialog.Create(Self);
    if not OD.Execute then begin
      Screen.Cursor := crDefault;
      tExcel.Close;
      Exit;
    end;
    path := '';
    if versaoOffice < 12 then begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end else begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+OD.FileName+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
     OD.Free;
    tExcel.Active := False;
    tExcel.ConnectionString := path;
    tExcel.TableName := 'conv$';
    tExcel.Active := True;
    try
      tExcel.Open;
    except
      MsgErro('N�o foi poss�vel abrir o arquivo Excel. Certifique - se que o nome da planilha est� como "conv" (Sem aspas)');
      erro := True;
    end;
  except on E:Exception do
    MsgErro('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  if erro then begin
    Screen.Cursor := crDefault;
    btnImportar.Caption := 'Importar';
    Abort;
  end;
  //while not QManutencaoLimiteConv.Eof do
  //begin
      if QManutencaoLimiteConvempres_id.AsString = tExcel.fieldByName('EMPRES_ID').AsString then
      begin

        Screen.Cursor := crHourGlass;
        while not tExcel.Eof do begin
          try
            if tExcel.Fields[1].AsString <> '' then begin
              strSql:= 'update conveniados set limite_mes = '+FormatDimIB(ArredondaDin(tExcel.Fields[2].AsCurrency)) +
                       ' where chapa ='+ tExcel.Fields[0].AsString +' and empres_id = '+tExcel.Fields[1].AsString;

              DMConexao.ExecuteSql(strSql);
              DMConexao.GravaLogSemTelaOcorrencia('FCadConv','LIMITE MES','',
              FormatDimIB(ArredondaDin(tExcel.Fields[2].AsCurrency)) ,Operador.Nome,'Altera��o',tExcel.Fields[0].AsString,Operador.Nome,'');
            end;
          except on E:Exception do begin
              MsgErro('Erro ao incluir creditos');
              DMConexao.AdoCon.RollbackTrans;
              Screen.Cursor := crDefault;
              tExcel.Close;
              abort;
            end;
          end;
          QManutencaoLimiteConv.Next;
          tExcel.Next;
          Application.ProcessMessages;
        end;

        //DMConexao.AdoCon.CommitTrans;
        QManutencaoLimiteConv.Requery;
        Screen.Cursor := crDefault;
        if tExcel.State in [dsEdit, dsInsert] then
          tExcel.Close;

        msginf('Importa��o realizada com sucesso!');

      end

      else
         msginf('Planilha n�o pertence a empresa '+QManutencaoLimiteConvEMPRES_ID.AsString + '. Verifique o arquivo.');

  //end;
end;

procedure TFCadEmp.dbGrigManutLimitesConveniadosColExit(Sender: TObject);
begin
  inherited;
  if QManutencaoLimiteConv.State in [dsEdit] then QManutencaoLimiteConv.Post;
end;

procedure TFCadEmp.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if not qCredAlimDATA_RENOVACAO.IsNull then
  begin
    if Application.MessageBox('Confirma a exclus�o do lan�amento de cr�dito empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      DMConexao.AdoCon.BeginTrans;
      try
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO_CREDITOS WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;

        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Text := ' DELETE FROM ALIMENTACAO_RENOVACAO WHERE RENOVACAO_ID = ' + qCredAlimRENOVACAO_ID.AsString;
        DMConexao.AdoQry.ExecSQL;
        DMConexao.AdoCon.CommitTrans;

        DMConexao.GravaLog('FCadEmp','RENOVACAO_ID',qCredAlimRENOVACAO_ID.AsString,FormatDateTime('dd/mm/yyyy',dbDataRenovacao.Date),Operador.Nome,'Exclusao',
        QCadastroEMPRES_ID.AsString,'');

        qCredAlim.Close;
        qCredAlim.Parameters.Items[0].Value := qCadastroEMPRES_ID.Value;
        QCredAlim.Parameters.Items[1].Value := QCadastroEMPRES_ID.Value;
        qCredAlim.Open;

        dbDataRenovacao.Text := '';
        dbGridAlim.Visible := false;
        Panel43.Visible := false;
        rgTipo.ItemIndex := 0;
        dbDataRenovacao.SetFocus;
      except
        on E:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          MsgErro('Erro ao excluir Lan�amento. Erro: '+E.Message+sLineBreak+'Opera��o Cancelada!');
        end;
      end;
    end;
    end
    else
      begin
         msginf('N�o existe lan�amentos a serem exclu�dos!');
         dbDataRenovacao.SetFocus;
      end;
end;

procedure TFCadEmp.btnAlterarTodosLimitesClick(Sender: TObject);
var  tmp, sql : String;
  dc : char;
  valorAbono, valorRenovacao: Currency;
begin


    if Application.MessageBox('Confirma a altera��o do limite m�s de todos os conveniados desta empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      Screen.Cursor := crHourGlass;
      dc := DecimalSeparator;
      Application.ProcessMessages;

      try
        DMConexao.AdoCon.BeginTrans;

        QManutencaoLimiteConv.First;
        while not QManutencaoLimiteConv.Eof do begin
          qUpdate.SQL.Clear;
          qUpdate.SQL.Text := 'UPDATE conveniados SET limite_mes = '+ fnsubstituiString(',','.',edtLimiteMes.Text) +' where conv_id = '+ QManutencaoLimiteConvconv_id.AsString;
          qUpdate.ExecSQL;

          QManutencaoLimiteConv.Next;
        end;

        DMConexao.AdoCon.CommitTrans;

        msginf('Atualiza��o realizada com sucesso!');

      except
        on e:Exception do
        begin
          DMConexao.AdoCon.RollbackTrans;
          Screen.Cursor := crDefault;
          MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
          Screen.Cursor := crDefault;
          abort;
        end;
      end;

      Screen.Cursor := crDefault;
      QManutencaoLimiteConv.Requery;
      DecimalSeparator := dc;


      edtLimiteMes.Text := '0,00';
      edtLimiteMes.SetFocus;

    end;

end;
procedure TFCadEmp.btnAdicionaBairroClick(Sender: TObject);
var bairro : string; cidade : string; sql : string;
begin
  inherited;
  frmBairro := TfrmBairro.Create(Self);
  frmBairro.ShowModal;
  if frmBairro.ModalResult = mrOk then
  begin
    cidade := QBairros.Parameters.ParamByName('CID_ID').Value;
    bairro := frmBairro.txtBairro.Text;
    sql := 'insert into bairros(cid_id, descricao) values(' + cidade + ', ''' + bairro + ''')';
    DMConexao.ExecuteSql(sql);
    QBairros.Requery();
    ShowMessage('Bairro inserido com sucesso!');
  end;
end;

procedure TFCadEmp.dsEstado_CartaoDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not QCidade_Cartao.Locate('NOME',lkpCIDADE.KeyValue,[])) then
  begin
    QCidade_Cartao.Close;
    QCidade_Cartao.Parameters.ParamByName('ESTADO_ID').Value := lkpUF_CARTAO.KeyValue;
    QCidade_Cartao.Open;
  end;
end;

procedure TFCadEmp.dsCidade_CartaoDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not QBairro_Cartao.Locate('DESCRICAO',dbLkpBairros.KeyValue,[])) then
  begin
    QBairro_Cartao.Close;
    QBairro_Cartao.Parameters.ParamByName('CID_ID').Value := lkpCIDADE_CARTAO.KeyValue;
    QBairro_Cartao.Open;
  end;
end;

procedure TFCadEmp.dsCidadesDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not QBairros.Locate('DESCRICAO',dbLkpBairros.KeyValue,[])) then
  begin
    QBairros.Close;
    QBairros.Parameters.ParamByName('CID_ID').Value := lkpCIDADE.KeyValue;
    QBairros.Open;
  end;
  btnAdicionaBairro.Enabled := true;
end;

procedure TFCadEmp.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  QCidades.close;
  QBairros.close;  
  QCidades.Parameters.ParamByName('ESTADO_ID').Value := QCadastroESTADO.Value;
  QBairros.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE.Value;
  QCidades.open;
  QBairros.open;

  QCidade_Cartao.close;
  QBairro_Cartao.close;
  QCidade_Cartao.Parameters.ParamByName('ESTADO_ID').Value := QCadastroUF_CARTAO.Value;
  QBairro_Cartao.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE_CARTAO.Value;
  QCidade_Cartao.open;
  QBairro_Cartao.open;
end;

procedure TFCadEmp.DBGrid1Enter(Sender: TObject);
begin
  inherited;
  QCidades.close;
  QBairros.close;  
  QCidades.Parameters.ParamByName('ESTADO_ID').Value := QCadastroESTADO.Value;
  QBairros.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE.Value;
  QCidades.open;
  QBairros.open;

  QCidade_Cartao.close;
  QBairro_Cartao.close;
  QCidade_Cartao.Parameters.ParamByName('ESTADO_ID').Value := QCadastroUF_CARTAO.Value;
  QBairro_Cartao.Parameters.ParamByName('CID_ID').Value := QCadastroCIDADE_CARTAO.Value;
  QCidade_Cartao.open;
  QBairro_Cartao.open;
end;

procedure TFCadEmp.txtCEPExit(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: TXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  
//  if (txtCEP.Text = '') or (Length(SoNumero(txtCEP.Text)) <> 8) then
//  begin
//    Application.MessageBox('CEP nulo ou inv�lido. Informe o CEP sem pontos ou tra�os.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
//    exit;
//  end;
//  Resposta   := TStringStream.Create('');
//  TSConsulta := TStringList.Create;
//  IdHTTP1:= TIdHTTP.Create(Self);
//  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
//  TSConsulta.Values['&cep']  := SoNumero(txtCEP.Text);
//  TSConsulta.Values['&formato']  := 'xml';
//  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
//  TSConsulta.Free;
//  IdHTTP1.Free;
//  XMLDocCEP:= TXMLDocument.Create(self);
//  XMLDocCEP.Active := True;
//  XMLDocCEP.Encoding := 'iso-8859-1';
//  XMLDocCEP.LoadFromStream(Resposta);
//  try
//    try
//      QCadastro.Edit;
//      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
//                                                    ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
//      //QCadastro.FieldByName('BAIRRO').AsString      := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
//      //QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
//      //QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
//    except
//      ShowMessage('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
//    end;
//  finally
//    Resposta.Free;
//    XMLDocCEP.Active := False;
//    XMLDocCEP.Free;
//  end;
  //DBEdit27.SetFocus;
end;

procedure TFCadEmp.tsDescontoEspecialShow(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QCredEmpDescontoEspecial.Close;
    QCredEmpDescontoEspecial.Parameters.Items[0].Value:= QCadastroEMPRES_ID.AsInteger;
    QCredEmpDescontoEspecial.Open;

  end;
end;

procedure TFCadEmp.GridUsaDescontoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QCredEmpDescontoEspecial.Sort) > 0 then begin
     if Pos(' DESC',QCredEmpDescontoEspecial.Sort) > 0 then QCredEmpDescontoEspecial.Sort := Field.FieldName
                                                  else QCredEmpDescontoEspecial.Sort := Field.FieldName+' DESC';
  end
  else QCredEmpDescontoEspecial.Sort := Field.FieldName;
  except
  end;
end;

procedure TFCadEmp.GridUsaDescontoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( key = 13 ) or ( key = vk_down ) or (key = vk_up) then
  cLiberado := GridUsaDesconto.Fields[2].Text;
end;

procedure TFCadEmp.GridUsaDescontoColExit(Sender: TObject);
begin
  inherited;
  if QCredEmpDescontoEspecial.State in [dsEdit] then QCredEmpDescontoEspecial.Post;
end;

procedure TFCadEmp.DSCredEmpDescontoEspecialStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancelDescontoEspecial = ActiveControl) or (btnGravaCredDescontoEspecial = ActiveControl) then
     GridCredObrigarSenha.SetFocus;
  btnCancelDescontoEspecial.Enabled  := QCredEmpDescontoEspecial.State in [dsEdit,dsInsert];
  btnGravaCredDescontoEspecial.Enabled := QCredEmpDescontoEspecial.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.btnCancelDescontoEspecialClick(Sender: TObject);
begin
  inherited;
  if (btnCancelDescontoEspecial = ActiveControl) or (btnGravaCredDescontoEspecial = ActiveControl) then
     GridUsaDesconto.SetFocus;
  btnCancelDescontoEspecial.Enabled  := QCredEmpDescontoEspecial.State in [dsEdit,dsInsert];
  btnGravaCredDescontoEspecial.Enabled := QCredEmpDescontoEspecial.State in [dsEdit,dsInsert];
end;

procedure TFCadEmp.QCredEmpDescontoEspecialAfterPost(DataSet: TDataSet);
var sql, sqlLog: String;
  cadastro,oldValue,newValue, detalhe: string;
  reg_atual : TBookmark;
begin
  inherited;
  sql:= '';
  sqlLog:= '';
  cadastro:= 'Cred. '+QCredEmpDescontoEspecialcred_id.AsString+' lib. para esta empresa';
  detalhe := 'Empr ID';
  newValue := cLiberado;

  if (UpperCase(newValue) = 'S') then begin
    if QCredEmpDescontoEspecialLIBERADA.OldValue = 'N' then begin
      QCredLib.Close;
      QCredLib.SQL.Clear;
      QCredLib.SQL.Add(' Select cred.cred_id, cred.nome, coalesce(credlib.liberado,''S'') as liberado');
      QCredLib.SQL.Add(' from credenciados cred');
      QCredLib.SQL.Add(' left join emp_cred_lib credlib on cred.cred_id = credlib.cred_id');
      QCredLib.SQL.Add(' and ((credlib.empres_id = ' + QCadastroEMPRES_ID.AsString + ') or (credlib.empres_id is null)) ');
      QCredLib.SQL.Add(' order by cred.nome');
      QCredLib.Open;

      if ExisteCredBloq(QCadastroEMPRES_ID.AsInteger,QCredLibCRED_ID.AsInteger) then begin
        sql :=  'update EMP_CRED_DESCONTO_ESPECIAL set LIBERADA = '+QuotedStr(UpperCase(newValue))+''+
                ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+' and CRED_ID = '+QCredLibCRED_ID.AsString;
      end else begin
        sql :=  'insert into EMP_CRED_DESCONTO_ESPECIAL(EMPRES_ID, CRED_ID, LIBERADA) '+
                ' values('+QCadastroEMPRES_ID.AsString+','+QCredEmpDescontoEspecialcred_id.AsString+','+QuotedStr(UpperCase(newValue))+')';
      end;
      if UpperCase(QCredEmpDescontoEspecialLIBERADA.OldValue) <> UpperCase(newValue) then begin
        DMConexao.GravaLog('FCadEmp','DESC_ESPECIAL','S','N',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
      end;
    end;
  end else if (UpperCase(newValue) = 'N') then begin
    if QCredEmpDescontoEspecialLIBERADA.OldValue = 'S' then begin
      sql :=  'delete from EMP_CRED_DESCONTO_ESPECIAL ';
      sql :=  sql + ' where EMPRES_ID = '+QCadastroEMPRES_ID.AsString+ ' and CRED_ID = '+QCredEmpDescontoEspecialcred_id.AsString;
    end;
    if UpperCase(QCredEmpDescontoEspecialLIBERADA.OldValue) <> UpperCase(newValue) then begin
      DMConexao.GravaLog('FCadEmp','DESC_ESPECIAL','N','S',Operador.Nome,'Altera��o',QCadastroEMPRES_ID.AsString,Self.Name);
    end;
  end;
  if sql <> '' then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sql;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;

  end;
  QCredEmpDescontoEspecial.DisableControls;
  reg_atual := QCredEmpDescontoEspecial.GetBookmark;
  QCredEmpDescontoEspecial.Requery();
  QCredEmpDescontoEspecial.GotoBookmark(reg_atual);
  QCredEmpDescontoEspecial.freebookmark(reg_atual);
  QCredEmpDescontoEspecial.EnableControls;
  cLiberado := '';
end;

//procedure TFCadEmp.rgTipoClick(Sender: TObject);
//begin
//  inherited;
//
//  //rbTipoComplemento := QCredAlimCOMPLEMENTO;
//
//end;

procedure TFCadEmp.DBGrid1ContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  inherited;
  if Operador.ID = 80 then
    Handled := True;
end;

procedure TFCadEmp.btnAnexarContratoClick(Sender: TObject);
var arquivoPDF : string;
    caminhoAno, caminhoMes, caminhoCompleto, caminhoRelativo, mesReferencia, mes, ano,Id : string;
begin
  screen.Cursor  := crHourGlass;

  if ExtractFileExt(FilenameEdit1.FileName) <> '.pdf' then
  begin
    ShowMessage('Selecione o arquivo a ser carregado.');
    screen.Cursor := crDefault;
    Exit;
  end;

  if
  MsgSimNao('Deseja realmente anexar o contrato selecionado' + sLineBreak + '� empresa ' + QCadastroEMPRES_ID.AsString + '?', False) then
  begin

    QContratosEmpresa.Close;
    QContratosEmpresa.Parameters.ParamByName('EMPRES_ID').Value := QCadastroEMPRES_ID.Value;
    QContratosEmpresa.Open;

    QContratosEmpresa.Insert;

    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONTRATO_ID AS CONTRATO_ID');
    DMConexao.AdoQry.Open;

    QContratosEmpresaCONTRATO_ID.Value := DMConexao.AdoQry.Fields[0].AsVariant;

    arquivoPDF :=  FilenameEdit1.FileName;
    //DecodeDate(Now, ano, mes, dia);

    //mes := LeftStr(mesReferencia,2);
    //ano := RightStr(mesReferencia,4);

    caminhoAno := 'C:\Dados\Web\webEmpresas\Contratos';

    if not DirectoryExists(caminhoAno) then
      CreateDir(caminhoAno);
                       
    //aqui tbm
    caminhoCompleto := caminhoAno + '\' + IntToStr(QCadastroEMPRES_ID.AsInteger) + '.pdf';
    caminhoRelativo := 'CONTRATOS\' + IntToStr(QCadastroEMPRES_ID.AsInteger) + '.pdf';

    if CopyFile(PChar(arquivoPDF), PChar(caminhoCompleto), False) then begin
      QContratosEmpresaEMPRES_ID.AsInteger := QCadastroEMPRES_ID.AsInteger;
      QContratosEmpresaCAMINHO_ARQUIVO.Text := caminhoRelativo;
      QContratosEmpresaOPERADOR.AsString := Operador.Nome;
      QContratosEmpresa.Post;

      ShowMessage('Contrato carregado com sucesso!');
    end else
      ShowMessage('O Contrato n�o foi carregado. Tente novamente!');
  end
  else
    ShowMessage('O Contrato n�o foi carregado!');
    //LimparTela();
end;

procedure TFCadEmp.tsAnexarContratoEnter(Sender: TObject);
begin
  //inherited;
  if (not QCadastro.IsEmpty) then
  begin
    FilenameEdit1.Enabled := True;
    btnAnexarContrato.Enabled := True;
  end;
end;

procedure TFCadEmp.tsAnexarContratoShow(Sender: TObject);
begin
 if (not QCadastro.IsEmpty) then
  begin
    FilenameEdit1.Enabled := True;
    btnAnexarContrato.Enabled := True;
  end;
end;

procedure TFCadEmp.btnAbrirContratoClick(Sender: TObject);
var OD : TOpenDialog;
renovacaoId : String;
path,strSql : String;
erro: Boolean;
begin
  inherited;

    OleContainer1.AutoActivate := aaGetFocus;
    OleContainer1.CreateLinkToFile('C:\Dados\Web\WebEmpresas\CONTRATOS\' + QCadastroEMPRES_ID.AsString + '.pdf',True);
    OleContainer1.SetFocus;
end;

procedure TFCadEmp.ButAjudaLimpaSenha(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o para limpar a senha';
  FChangeLog.RichEdit1.Lines := RichEditLimpaSenha.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFCadEmp.JvBitBtn2Click(Sender: TObject);
var ano, mes : integer; data : TDateTime;
begin
  inherited;
  QManutDataFechaEmp.Close;
  QManutDataFechaEmp.SQL.Clear;
  QManutDataFechaEmp.SQL.Add('SELECT EMPRES_ID,FECHAMENTO1,VENCIMENTO1 FROM EMPRESAS WHERE LIBERADA = ''S'' AND APAGADO = ''N'' AND EMPRES_ID NOT IN(SELECT DISTINCT(EMPRES_ID) FROM DIA_FECHA WHERE DATA_FECHA > ''01/01/2021'')');
  QManutDataFechaEmp.Open;

  while not QManutDataFechaEmp.Eof do
  begin
    Screen.Cursor := crHourGlass;
    DMConexao.ExecuteSql('delete from dia_fecha where EMPRES_ID = '+QManutDataFechaEmp.Fields[0].AsString+' and DATA_FECHA >= ''01/01/2021''');
    QDatasFechaTemp.DisableControls;
    QDatasFechaTemp.Close;
    QDatasFechaTemp.SQL.Text := 'select * from dia_fecha where EMPRES_ID = '+QManutDataFechaEmp.Fields[0].AsString;
    QDatasFechaTemp.Open;
    for ano := 2021 to 2031 do
    begin
      for mes := 1 to 12 do
      begin
        QDatasFechaTemp.Append;
        QDatasFechaTempEMPRES_ID.AsInteger := QManutDataFechaEmp.Fields[0].AsInteger;
        //Se o dia for maior que o ultimo dia do mes assume o �ltimo dia.
        if QManutDataFechaEmp.Fields[1].AsInteger <= DaysInAMonth(ano,mes) then
          data := EncodeDate(ano,mes,QManutDataFechaEmp.Fields[1].AsInteger)
        else
          data := EncodeDate(ano,mes,DaysInAMonth(ano,mes));
        QDatasFechaTempDATA_FECHA.AsDateTime := Data;
        if QManutDataFechaEmp.Fields[2].AsInteger <= DaysInAMonth(ano,mes) then
          data := EncodeDate(ano,mes,QManutDataFechaEmp.Fields[2].AsInteger)
        else
          data := EncodeDate(ano,mes,DaysInAMonth(ano,mes));
        QDatasFechaTempDATA_VENC.AsDateTime := Data;
        if QDatasFechaTempDATA_VENC.AsDateTime < QDatasFechaTempDATA_FECHA.AsDateTime then
          QDatasFechaTempDATA_VENC.AsDateTime := IncMonth(QDatasFechaTempDATA_VENC.AsDateTime);
        QDatasFechaTemp.Post;
      end;
    end;
    QDatasFechaTemp.Close;
    QDatasFechaTemp.EnableControls;
    Screen.Cursor := crDefault;
    //datas_alteradas := False;
    QManutDataFechaEmp.Next;
  end;


  MsgInf('Datas Atualizadas com sucesso!');
end;

procedure TFCadEmp.btnBuscaEmpClick(Sender: TObject);
begin
  qEmpresas.Close;
  qEmpresas.SQL.Clear;
  qEmpresas.SQL.Add('select empres_id, fantasia from empresas where fantasia like ' + QuotedStr('%' + EdtNomeFantasia.Text + '%'));
  qEmpresas.SQL.Add(' AND USU_EMAIL_PADRAO IS NOT NULL');
  if txtEmpresId.Text <> '' then
  begin
    qEmpresas.SQL.Add(' and EMPRES_ID  = ' + txtEmpresId.Text)
  end;

  if CbPesqLiberado.Checked then
  begin
    qEmpresas.SQL.Add(' and LIBERADA  = ''S''')
  end;
  qEmpresas.Open;

end;

procedure TFCadEmp.btnMarcarTodosClick(Sender: TObject);
var
  vlLinha: Integer;
begin
  with DBGrid2.DataSource.DataSet do
  begin
    First;
    for vlLinha := 0 to RecordCount - 1 do
    begin
      DBGrid2.SelectedRows.CurrentRowSelected := True;
      Next;
    end;
  end;
  DBGrid2.SelectedRows.Refresh;

end;


procedure TFCadEmp.btnAdicionarClick(Sender: TObject);
var contador,i,j : integer;
DataSet : TDataSet;
begin
  ClientDataSet1.Open;
  DataSet := DBGrid2.DataSource.DataSet;
  for i:=0 to DBGrid2.SelectedRows.Count-1 do
      begin
        ClientDataSet1.Append;
        DataSet.GotoBookmark(Pointer(DBGrid2.SelectedRows.Items[i]));
        for j := 0 to DataSet.FieldCount - 1 do
        begin
          if (j=0) then
          begin
            ClientDataSet1EMPRES_ID.Value := DataSet.Fields[j].AsInteger;
          end
          else
          begin
            ClientDataSet1FANTASIA.Value := DataSet.Fields[j].AsString;
          end;

        end;
      end;


end;

procedure TFCadEmp.btnCriarEmailsClick(Sender: TObject);
var id, linhaEmail, linhaErro : integer;
email : String;
Excel : Variant;
begin

  if txtUsuario.Text = '' then
  begin
    ShowMessage('Digite um nome para o usu�rio!');
    abort;
  end;

  if ClientDataSet1.IsEmpty then
  begin
    ShowMessage('Escolha ao menos uma empresa para a cria��o dos emails!');
    abort;
  end;

  linhaEmail := 2;
  linhaErro := 2;

  ClientDataSet1.First;

  Excel := CreateoleObject('Excel.Application');

  Excel.WorkBooks.Add;

  Excel.WorkBooks[1].Sheets[1].Cells[1,1]:= 'Emails Criados';
  Excel.WorkBooks[1].Sheets[1].Cells[1,2]:= 'Emails Com Duplicidade';
  Excel.WorkBooks[1].Sheets[1].Cells[1,1].font.bold := true;
  Excel.WorkBooks[1].Sheets[1].Cells[1,2].font.bold := true;

  while not ClientDataSet1.Eof do
  begin

    ADOQuery3.Close;
    ADOQuery3.Open;

    ADOQuery4.Close;
    ADOQuery4.SQL.Clear;
    ADOQuery4.SQL.Text := 'select USU_EMAIL_PADRAO from EMPRESAS where EMPRES_ID = ' + IntToStr(ClientDataSet1EMPRES_ID.Value);
    ADOQuery4.Open;



    email := StringReplace(LowerCase(txtUsuario.Text),' ',EmptyStr, [rfReplaceAll] ) + ADOQuery4.Fields[0].Value;

    if (VerificaEmail(email)) then
    begin
      ADOQuery1.Close;
      ADOQuery1.SQL.Clear;
      ADOQuery1.SQL.Add('insert into USUARIOS_WEB(USU_ID, USU_NOME, usu_email, USU_SENHA, EMP_FOR_ID, USU_TIPO, USU_LIBERADO) ');
      ADOQuery1.SQL.Add('values (' + IntToStr(ADOQuery3.Fields[0].Value));
      ADOQuery1.SQL.Add(', ' + QuotedStr(txtUsuario.Text));
      ADOQuery1.SQL.Add(', ' + QuotedStr(email));
      ADOQuery1.SQL.Add(', ' + QuotedStr(Crypt('E','1111','BIGCOMPRAS'))); 
      ADOQuery1.SQL.Add(', ' + IntToStr(ClientDataSet1EMPRES_ID.Value));
      ADOQuery1.SQL.Add(', ''0''');
      if CbLiberado.Checked then
      begin
        ADOQuery1.SQL.Add(', ''S''');
      end
      else begin
        ADOQuery1.SQL.Add(', ''N''');
      end;
      ADOQuery1.SQL.Add(' ) ');
      ADOQuery1.ExecSQL;


      Excel.WorkBooks[1].Sheets[1].Cells[linhaEmail,1]:= email;
      linhaEmail := linhaEmail +1;
    end
    else begin
      Excel.WorkBooks[1].Sheets[1].Cells[linhaErro,2]:=email;
      linhaErro := linhaErro +1;
    end;

    ClientDataSet1.Next;
  end;
  Excel.WorkBooks[1].SaveAs('C:\Users\Public\EmailSpani\Emails.xls');
  Excel.Visible :=True;
  ClientDataSet1.EmptyDataSet;
end;

procedure TFCadEmp.btnRemoverClick(Sender: TObject);
var
 i: Integer;
begin
  if(DBGrid3.SelectedRows.Count > 0) then
  begin
    ClientDataSet1.Open;
    for I := 0 to DBGrid3.SelectedRows.Count -1 do
    begin
      ClientDataSet1.GotoBookmark(Pointer(DBGrid3.SelectedRows.Items[i]));
      ClientDataSet1.Delete;
    end;
  end else
  begin
    ShowMessage('Selecione a empresa para excluir da lista!');
    abort;
  end;
end;

function TFCadEmp.VerificaEmail(email:String):Boolean;
begin
  ADOQuery5.Close;
  ADOQuery5.SQL.Clear;
  ADOQuery5.SQL.Text := 'select USU_EMAIL from USUARIOS_WEB where USU_EMAIL = ' + QuotedStr(email);
  ADOQuery5.Open;

  if ADOQuery5.RecordCount = 0 then
  begin
    Result := true;
  end
  else begin
    Result := false;
  end;
end;

procedure TFCadEmp.Button3Click(Sender: TObject);
var vlLinha: Integer;
begin
  with DBGrid2.DataSource.DataSet do
  begin
    First;
    for vlLinha := 0 to RecordCount - 1 do
    begin
      DBGrid2.SelectedRows.CurrentRowSelected := False;
      Next;
    end;
  end;
  DBGrid2.SelectedRows.Refresh;

end;

procedure TFCadEmp.btnExcluitClick(Sender: TObject);
begin
  ClientDataSet1.EmptyDataSet;  

end;

procedure TFCadEmp.txtUsuarioKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['A'..'Z','a'..'z','0'..'9',' ',#8]) then abort;

end;

end.
