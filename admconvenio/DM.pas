unit DM;

interface

uses
  SysUtils, Classes, DB, ADODB, Windows, IniFiles, Forms, Controls, Dialogs, uClassLog, ClipBrd,
  cartao_util, StrUtils, DBTables, utipos, dateutils;

type
    TDMConexao = class(TDataModule)
    AdoCon: TADOConnection;
    AdoQry: TADOQuery;
    Config: TADOQuery;                
    ConfigCOD_ADM_BIG: TWordField;
    ConfigULTIMA_EXE: TStringField;
    ConfigRODAR_ATE: TStringField;
    ConfigPATH_WEBSERVICE: TStringField;
    ConfigCODIGOCARTAO: TIntegerField;
    ConfigUSAINICIALCODCARTIMP: TStringField;
    ConfigMOVER_CODCART_TO_CODIMP: TStringField;
    ConfigINCREMENTCODCARTIMP: TStringField;
    ConfigINCREMENTCODCARTIMPMOD1: TStringField;
    ConfigEDIT_GRID: TStringField;
    ConfigLANC_DIFE_PROX_PERIODO: TStringField;
    ConfigFLANCAMENTOS_FOCO_FORNEC: TStringField;
    ConfigCODIGOCARTAOIMP: TStringField;
    ConfigCODIGOCARTAOIMPDEP: TStringField;
    ConfigINICIALCODCARTIMP: TStringField;
    ConfigCOD_CRED_BAIXA: TIntegerField;
    ConfigCOD_CRED_ADM: TIntegerField;
    ConfigCOD_CRED_CPMF: TIntegerField;
    ConfigPERCENT_CPMF: TFloatField;
    ConfigVERSAO: TIntegerField;
    ConfigFLANCAMENTOS_AUTOR_WEBSERVICE: TStringField;
    ConfigPERMITE_AUTOR_FORCADA: TStringField;
    ConfigINFORMA_EMP_LANC_IND: TStringField;
    ConfigVERIFICA_CPF_CONV: TStringField;
    ConfigCOD_EMP_LANC: TIntegerField;
    ConfigCODINBSGENERICO: TStringField;
    ConfigACUMULA_AUTOR_PROX_FECHA: TStringField;
    ConfigPERM_CANC_PARC: TStringField;
    ConfigFILTRO_ENTREG_NF_EXT: TStringField;
    ConfigEMP_FANTASIA: TStringField;
    ConfigCRE_FANTASIA: TStringField;
    ConfigEXIBIR_DT_VENC_REL_PAG_FOR: TStringField;
    ConfigFILTRO_ENTREG_NF_CONFERE: TStringField;
    ConfigEMP_FANTASIA_CADCONV: TStringField;
    ConfigEXIBIR_PAGTO_SITE: TStringField;
    ConfigALTERAR_TITULAR_NOME_CARTAO: TStringField;
    ConfigALTERAR_TITULAR_LIBERADO_CARTAO: TStringField;
    ConfigESTILO_ENTREGUE: TStringField;
    ConfigSENHA_CONV_ID: TStringField;
    ConfigEXIBE_ALERTAS: TStringField;
    ConfigUSA_NOVO_FECHAMENTO: TStringField;
    ConfigEXPIRA_PBM: TIntegerField;
    ConfigINTEGRA_SISTEMABIG: TStringField;
    ConfigCUPOM_CONV: TStringField;
    ConfigCUPOM_ESTAB: TStringField;
    ConfigUSA_PROG_DESC: TStringField;
    ConfigUSA_FIDELIDADE: TStringField;
    ConfigDIAS_BLOQ_EMPR_FAT: TIntegerField;
    ConfigEMITE_NOVO_CART_2VIA: TStringField;
    ConfigDEMISSAO_MOVE_AUTS: TStringField;
    ConfigFIDELIDADE_VALOR: TStringField;
    ConfigLINK_EMP: TStringField;
    ConfigLINK_EST: TStringField;
    ConfigLINK_USU: TStringField;
    ConfigLINK_OND: TStringField;
    ConfigTIPO_LIMITE: TStringField;
    ConfigUSA_GRUPO_PROD: TStringField;
    ConfigUSA_VALE_DESCONTO: TStringField;
    ConfigCUPOM: TStringField;
    ConfigIMPRIME_CUPOM_FIDELIZE: TStringField;
    ConfigBLOQUEIA_VENDA_VALOR: TStringField;
    ConfigUSA_VALE_DINHEIRO: TStringField;
    ConfigENVIA_DADOS: TStringField;
    ConfigCOD_CARD_BIN: TStringField;
    ConfigMOSTRAR_TELA_OCORRENCIA: TStringField;
    ConfigPATH_SALE_SERVICE: TStringField;
    AdoConfGrade: TADOQuery;
    StoredProc1: TADOStoredProc;
    Q: TADOQuery;
    UpdateSQL1: TUpdateSQL;
    Query2: TADOQuery;
    Adm: TADOTable;
    AdmADM_ID: TIntegerField;
    AdmRAZAO: TStringField;
    AdmAPAGADO: TStringField;
    AdmCONTA_ID: TIntegerField;
    AdmFANTASIA: TStringField;
    AdmCNPJ: TStringField;
    AdmINSC_EST: TStringField;
    AdmENDERECO: TStringField;
    AdmNUMERO: TIntegerField;
    AdmBAIRRO: TStringField;
    AdmCIDADE: TStringField;
    AdmCEP: TStringField;
    AdmUF: TStringField;
    AdmRESPONSAVEL: TStringField;
    AdmFONE: TStringField;
    AdmDTAPAGADO: TDateTimeField;
    AdmOPERADOR: TStringField;
    AdmDTALTERACAO: TDateTimeField;
    AdmDTCADASTRO: TDateTimeField;
    AdmOPERCADASTRO: TStringField;
    AdmVENCEDIA: TIntegerField;
    AdmCOMPLEMENTO: TStringField;
    Query1: TADOQuery;
    ConfigSEG_ID_CANTINA: TIntegerField;
    ConfigBIN_CANTINA: TStringField;
    ConfigBIN_BEM_ESTAR: TStringField;
    ConfigCRED_ID_BEM_ESTAR: TIntegerField;
    ConfigCOD_CARD_BIN_NOVO: TStringField;
    procedure AbrirCongis;
    procedure DataModuleCreate(Sender: TObject);
    procedure FecharConfigs;

  private
    LogConfig : TLog;
    { Private declarations }
    FExplicitTransactionCounter: Integer;
  public
    { Public declarations }

    //FUNCOES//
    function ExisteCampo(tabela, campo: string): boolean;
    function ExecuteScalar(sql: string): Variant;overload;
    function ExecuteScalar(sql: string;ResultIfNull:Variant): Variant;overload;
    function GravaLog(janela:string;campo:string;vr_ant:string;vr_novo:string;operador:string;operacao:string;id:string;solicitante:string; motivo:string=''): Boolean;
    function GravaLogSAP(tabela,tabela_admcon,campo,campo_admcon,comando,vr_ant,vr_novo,operador,campo_id: String): Boolean;
    function GravaLogFinanceiro(vr_ant:string;vr_novo:string;operador:string;operacao:string): Boolean;
    function GravaLogMovimentosDuvidosos(vr_ant:string;vr_novo:string;autorTransacao:string;transacao:string;operador:string;operacao:string): Boolean;
    function GravaLogCad(janela:string;campo:string;vr_ant:string;vr_novo:string;operador:string;operacao:string;id:string;motivo:string; solicitante:string=''): Boolean;
    function GravaLogOcorrencia(janela:string; campo: string; vr_ant: string; vr_novo: string; operador: string; operacao: string; id: string; motivo: string; solicitante: string): Boolean;
    function GravaLogSemTelaOcorrencia(janela, campo, vr_ant, vr_novo, operador, operacao,
    id, solicitante, motivo: string): Boolean;
    function getGeneratorValue(generator: String): integer;
    function RetornaIDs(dataset: TDataSet; Nome_Chave: string): string;
    function ContaMarcados(dataset:TDataSet):integer;
    function ObterCodCartImpMod1(conv_id:integer; empres_id: integer; chapa:string): string;
    function ObterCodCartImp(titular:boolean=true): string;
    function GeraCartao(Conv_ID : Integer):Integer;
    function getPathDirPadrao: String;
    function ValidarFechamentoEmp(Data_Fecha: TDateTime;Empres_id: Integer): Variant;
    function ValidarFechamentoAbertoConv(Data_Fecha: TDateTime;Conv_id: Integer;Empres_id:Integer): Boolean;
    function ExecuteSql(sql: string): integer;
    function ExecuteQuery(sql : string):Variant;
    function ObterFechaEmpresa(Empres_id:Integer):Variant;
    function ObterUltimoFechamentoEmp(Empres_ID: Integer;var DataFecha: TDateTime): Boolean;
    function fnData : TDateTime;
    function getEstado(cred_id : Integer) : string;
    function VerificaMovEmpPeriodo(empres_id:integer;dataini,datafin:TDateTime;Baixa_Conv:String='N'):Boolean;
    function VerificaMovEmpFechamento(empres_id:integer;data_fecha:tdatetime;Baixa_Conv:String='N'):Boolean;
    function ObterPeriodoEmpresa(Empres_id: Integer;DataFecha: TDateTime): TPeriodo;
       function fnDivideComandoINSQL(Valores, Campo : String; NotIn : Boolean = False) : String;
    //PROCEDURES//
    procedure MarcaDesm(dataset:TDataSet);
    procedure MarcaDesmTodos(dataset: TDataSet;Marcar:Boolean=True);
    procedure SortZQuery(Query: TDataset; FieldName: string);
    procedure AtualizaComReceitaMovProd(const iAutorizacao_ID: Integer; const sComRec: string);
    procedure StartTransaction; virtual;
    procedure ExecuteNonQuery(sql : string);

    //procedure Commit; virtual;
    //procedure Rollback; virtual;
    //procedure CheckConnected;
    //procedure CheckAutoCommitMode;
    //procedure CheckNonAutoCommitMode;


  end;

var
  DMConexao: TDMConexao;
  Ocorrencia : TOcorrencia;

implementation

uses USelecionaBD, FOcorrencia, Variants;

{$R *.dfm}

procedure TDMConexao.StartTransaction;
begin
  //CheckAutoCommitMode;

  if FExplicitTransactionCounter = 0 then
  //AutoCommit := False; COMENTADO - SIDNEI SANCHES
  //DoStartTransaction; COMENTADO - SIDNEI SANCHES
  Inc(FExplicitTransactionCounter);
end;

{procedure TDMConexao.CheckAutoCommitMode;
begin
  if FAutoCommit then
   raise DatabaseError.cCreate(SInvalidOpInAutoCommit);
end;}

procedure TDMConexao.DataModuleCreate(Sender: TObject);
var ini : TIniFile;
seleciona : Boolean;
servidor, banco, usuario, senha: string;
begin                             
 try
   AdoCon.Close;
   ini := TIniFile.Create(ExtractFilePath(Application.ExeName)+'admcartao.ini');
   seleciona := ini.ReadBool('config','seleciona',False);
   if seleciona = True then begin
      FSelecionaBD := TFSelecionaBD.create(self);
      FSelecionaBD.ShowModal;
      if FSelecionaBD.ModalResult = mrOk then begin

      end;
      FSelecionaBD.Free;
   end
   else begin
      servidor :=  ini.ReadString('conexao','host','');
      banco := ini.ReadString('conexao','database','');
      AdoCon.ConnectionString := 'Provider=SQLNCLI.1;User ID=sa;Password=1@allebagord;Persist Security Info=False;Initial Catalog='+banco+';Data Source='+servidor+';';
      AdoCon.Open;
      ini.Free;
   end;
 except on E:Exception do
   ShowMessage('Erro ao conectar a base de dados.'+#13+'Erro: '+E.message);
 end;
 LogConfig := TLog.Create;
 //LogConfig.LogarQuery(Config,'FConfig','Cod Adm Big:','FConfig',Operador.Nome,'cod_adm_big');
end;

procedure TDMConexao.FecharConfigs;
begin
  Config.Close;
end;

function TDMConexao.ExisteCampo(tabela,campo:string):boolean;
var Q: TAdoQuery;
begin
  try
    Result := True;
    Q := TAdoQuery.Create(nil);
    Q.Connection := AdoCon;
    Q.Close;
    Q.SQL.Clear;
    Q.SQL.Add('select RDB$RELATION_NAME,RDB$FIELD_NAME from RDB$RELATION_FIELDS');
    Q.SQL.Add('  where');
    Q.SQL.Add('  RDB$FIELD_NAME    = '+QuotedStr(campo)+' AND');
    Q.SQL.Add('  RDB$RELATION_NAME = '+QuotedStr(tabela));
    Q.Open;
    if Q.RecordCount > 0 then
      Result := True
    else
      Result := False;
  finally
    Q.Close;
    Q.Free;
  end;
end;

function TDMConexao.getPathDirPadrao: String;
var ini : TIniFile;
  caminho : String;
begin
   caminho := '';
   ini := TIniFile.Create(ExtractFilePath(Application.ExeName)+'admcartao.ini');
   caminho := ini.ReadString('local','path_dir_padrao','C:\');
   ini.Free;
   Result:= caminho;
end;

function TDMConexao.ValidarFechamentoAbertoConv(Data_Fecha:TDateTime;Conv_id:Integer;Empres_id:Integer):Boolean;
var sql : string;
    faturaId : Integer;
    valida : Variant;
begin
   Result := False;
   if Empres_id <= 0 then
      Empres_id := ExecuteScalar('Select empres_id from conveniados where conv_id = '+IntToStr(Conv_id));
   valida := ValidarFechamentoEmp(Data_Fecha,Empres_id);
   if (valida <> Null) then begin
      sql := ' select fatura_id from fatura '+
             ' where coalesce(fatura.apagado,''N'') <> ''S'' and fatura.fechamento = '+FormatDataIB(Data_Fecha)+
             '   and (   (id = '+IntToStr(Empres_id)+' and tipo = ''E'') '+
             '        or (id = '+IntToStr(Conv_id)+' and tipo = ''C'') ) ';

      Result := ExecuteScalar(sql) = Null ;
   end;
end;

function TDMConexao.ValidarFechamentoEmp(Data_Fecha:TDateTime;Empres_id:Integer):Variant;
begin
  Result := ExecuteScalar('Select data_fecha from dia_fecha where empres_id = '+IntToStr(Empres_id)+' and convert (nvarchar(10), data_fecha,103) = '+ QuotedStr(DateToStr(Data_Fecha)));
end;

function TDMConexao.ExecuteSql(sql : string):integer;
  var Q : TAdoQuery;
begin
  Q := TAdoQuery.Create(nil);
  Q.Connection := AdoCon;
  Q.Close;
  Q.SQL.Text := sql;
  Q.CommandTimeout := 5000;
  Q.ExecSQL;
  Result := Q.RowsAffected;
  Q.Close;
end;

function TDMConexao.ExecuteQuery(sql : string):Variant;
begin
  Q.SQL.Clear;
  Q.SQL.Text := sql;
  Q.Open;
  Result := Q.Fields[0].Value;
  Q.Close;
end;

procedure TDMConexao.ExecuteNonQuery(sql : string);
begin
  Q.SQL.Clear;
  Q.SQL.Text := sql;
  Q.ExecSQL;
  Q.Close;
end;

{function TDMConexao.GetInTransaction: Boolean;
begin
  CheckConnected;
  Result := not FAutoCommit or (FExplicitTransactionCounter > 0);
end;}

function TDMConexao.ObterCodCartImp(titular:boolean): string;
var proximo:String;
begin
  Config.Open;
  if titular then
     proximo := IntToStr(StrToInt64Def(ConfigCODIGOCARTAOIMP.AsString,0) + 1)
  else begin
     if ConfigCODIGOCARTAOIMPDEP.AsString <> '' then
        proximo := IntToStr(StrToInt64Def(ConfigCODIGOCARTAOIMPDEP.AsString,0) + 1)
     else
        proximo := IntToStr(StrToInt64Def(ConfigCODIGOCARTAOIMP.AsString,0) + 1);
  end;

  Config.Edit;
  if titular then
     ConfigCODIGOCARTAOIMP.AsString:= proximo
  else begin
     if ConfigCODIGOCARTAOIMPDEP.AsString <> '' then
        ConfigCODIGOCARTAOIMPDEP.AsString:= proximo
     else
        ConfigCODIGOCARTAOIMP.AsString:= proximo;
  end;

  Config.Post;
  Result:= proximo;
end;

function TDMConexao.ObterCodCartImpMod1(conv_id: integer; empres_id: integer; chapa:string): string;
var seq: integer;
begin
  seq    := ExecuteScalar('select coalesce(count(*),1) as total from cartoes where conv_id = '+IntToStr(conv_id));
  Result := FormatFloat('0000',empres_id)+FormatFloat('000000',StrToFloat(SoNumero(chapa)))+FormatFloat('00',seq+1);
end;

function TDMConexao.GeraCartao(Conv_ID : Integer):Integer;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(' Select empresas.usa_cartao_proprio from empresas where ');
  DMConexao.AdoQry.SQL.Add(' empresas.empres_id = (Select empres_id from conveniados where conveniados.conv_id = '+IntToStr(Conv_ID)+') ');
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.Fields[0].AsString = 'S' then begin
     DMConexao.AdoQry.Close;
     DMConexao.AdoQry.SQL.Text := 'update empresas set cartao_ini = cartao_ini + 1 where empres_id = (Select empres_id from conveniados where conveniados.conv_id = '+IntToStr(Conv_ID)+')';
     DMConexao.AdoQry.ExecSQL;
     DMConexao.AdoQry.SQL.Clear;
     DMConexao.AdoQry.SQL.Add(' Select empresas.cartao_ini from empresas where ');
     DMConexao.AdoQry.SQL.Add(' empresas.empres_id = (Select empres_id from conveniados where conveniados.conv_id = '+IntToStr(Conv_ID)+') ');
     DMConexao.AdoQry.Open;
     Result := DMConexao.AdoQry.Fields[0].AsInteger;
  end
  else begin
     DMConexao.AdoQry.SQL.Clear;
     DMConexao.AdoQry.SQL.Add('select next value for scartao_num as gen_codcart');
     DMConexao.AdoQry.Open;
     Result := DMConexao.AdoQry.FieldByName('gen_codcart').AsInteger;
     {StoredProc1.StoredProcName := 'PEGA_CODCART';
     StoredProc1.ExecProc;
     Result := StoredProc1.Params[0].AsInteger; }
  end;
  DMConexao.AdoQry.Close;
end;

function TDMConexao.ObterUltimoFechamentoEmp(Empres_ID:Integer;var DataFecha:TDateTime):Boolean;
begin
  Q.Close;
  Q.SQL.Text := ' Select max(fechamento) from fatura where coalesce(apagado,''N'') <> ''S'' and coalesce(tipo,''E'') = ''E'' and id = '+IntToStr(Empres_ID);
  Q.Open;
  Result := (not Q.IsEmpty) and (not Q.Fields[0].isNull);
  if Result then
     DataFecha := Q.Fields[0].AsDateTime;
  Q.Close;
end;


{function TDMConexao.ExecuteScalar(sql : string):Variant;
var Q : TAdoQuery;
begin
  Q := TAdoQuery.Create(nil);
  Q.Connection := AdoCon;
  Q.SQL.Text := sql;
  Clipboard.AsText := SQL;
  Q.Open;
  Result := Q.Fields[0].AsVariant;
  Q.Close;
  Q.Free;
end;                                                    }

function TDMConexao.ExecuteScalar(sql : string):Variant;
begin
//  Q := TZQuery.Create(nil);
//  Q.Connection := Connection1;
  Q.Close;
  Q.SQL.Text := sql;
  //Clipboard.AsText := SQL;
  Q.Open;
  Result := Q.Fields[0].AsVariant;
  Q.Close;
//  Q.Free;
end;

function TDMConexao.ExecuteScalar(sql : string;ResultIfNull:Variant):Variant;
begin
   Result := ExecuteScalar(sql);
   if Result = '' then
      Result := ResultIfNull;
end;

function TDMConexao.GravaLog(janela, campo, vr_ant, vr_novo, operador, operacao,
  id, solicitante, motivo: string): Boolean;
var teste: String;
begin
  if (DMConexao.config.IsEmpty) then
    DMConexao.Config.Open;
  if (ConfigMOSTRAR_TELA_OCORRENCIA.AsString = 'S') and ((Trim(solicitante) = '') or (Trim(motivo) = '')) then begin
    FrmOcorrencia := TFrmOcorrencia.Create(Self);
    if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
      solicitante := FrmOcorrencia.edtSolicitante.Text;
      motivo := FrmOcorrencia.mmoMotivo.Text;
      Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
      Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
      FreeAndNil(FrmOcorrencia);
      Result := True;
    end else begin
      Result := False;
      FreeAndNil(FrmOcorrencia);
      MsgInf('Informações da Ocorrência devem ser preenchidas');
      Application.ProcessMessages;
      Exit;
    end;
  end else begin
    Result := True;
  end;
  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                         ' OPERACAO, DATA_HORA, ID, SOLICITANTE, MOTIVO) ');
  AdoQry.SQL.Add(' values (NEXT VALUE FOR SLOG_ID,'''+janela+''','''+
  campo+''','''+vr_ant+''','''+vr_novo+''','''+operador+''','''+operacao+''',current_timestamp,'''+
  id+''','''+solicitante+''','''+motivo+''')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
end;

function TDMConexao.GravaLogSemTelaOcorrencia(janela, campo, vr_ant, vr_novo, operador, operacao,
  id, solicitante, motivo: string): Boolean;
var teste: String;
begin
  if (DMConexao.config.IsEmpty) then
    DMConexao.Config.Open;

  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                         ' OPERACAO, DATA_HORA, ID, SOLICITANTE, MOTIVO) ');
  AdoQry.SQL.Add(' values (NEXT VALUE FOR SLOG_ID,'''+janela+''','''+
  campo+''','''+vr_ant+''','''+vr_novo+''','''+operador+''','''+operacao+''',current_timestamp,'''+
  id+''','''+solicitante+''','''+motivo+''')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
end;


function TDMConexao.GravaLogFinanceiro(vr_ant, vr_novo, operador, operacao: String): Boolean;
var teste: String;
begin
  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERADOR, '+
                         ' OPERACAO, VALOR_ANT, VALOR_POS) ');
  AdoQry.SQL.Add(' values (NEXT VALUE FOR SLOG_ID,current_timestamp,'+
  QuotedStr(operador)+','+QuotedStr(operacao)+','+QuotedStr(vr_ant)+','+QuotedStr(vr_novo)+')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
  Result := True;
  // modificado sidnei
end;

function TDMConexao.GravaLogMovimentosDuvidosos(vr_ant,vr_novo,autorTransacao,transacao,operador,operacao: String): Boolean;
 //function GravaLogMovimentosDuvidosos(vr_ant:string;vr_novo:string;autorTransacao:string;transacao:string;operador:string;operacao:string): Boolean;

var teste: String;
begin
  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOG_FINANCEIRO(LOG_ID, DATAHORA, OPERADOR, '+
                         ' OPERACAO, AUTOR_TRANSACAO, TRANSACAO, VALOR_ANT, VALOR_POS) ');
  AdoQry.SQL.Add(' values (NEXT VALUE FOR SLOG_ID,current_timestamp,'+
  QuotedStr(operador)+','+QuotedStr(operacao)+','+QuotedStr(vr_ant)+','+QuotedStr(vr_novo)+')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
  Result := True;
  // modificado sidnei
end;

function TDMConexao.GravaLogSAP(tabela,tabela_admcon,campo,campo_admcon,comando,vr_ant,vr_novo,operador,campo_id: String): Boolean;
var teste: String;
begin
  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOG_SAP_ALTERACAO(TABELA_SAP,TABELA_ADMCON,CAMPO_SAP,CAMPO_ADMCON,DATA_HORA, '+
                         'COMANDO,VALOR_ANT,VALOR_POS,OPERADOR,CAMPO_ID) ');
  AdoQry.SQL.Add('values ('+QuotedStr(tabela)+','+QuotedStr(tabela_admcon)+','+QuotedStr(campo)+','+QuotedStr(campo_admcon)+','+'current_timestamp'+','+QuotedStr(comando)+','+
  QuotedStr(vr_ant)+','+QuotedStr(vr_novo)+','+QuotedStr(operador)+','+QuotedStr(campo_id)+')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
  Result := True;
end;



function TDMConexao.GravaLogCad(janela, campo, vr_ant, vr_novo, operador, operacao,
   id, motivo, solicitante: string): Boolean;
var teste: String;
begin
  
  if (DMConexao.config.IsEmpty) then
    DMConexao.Config.Open;
  if (ConfigMOSTRAR_TELA_OCORRENCIA.AsString = 'S') and ((Trim(solicitante) = '') or (Trim(motivo) = '')) then begin
    FrmOcorrencia := TFrmOcorrencia.Create(Self);
    if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
      solicitante := FrmOcorrencia.edtSolicitante.Text;
      motivo := FrmOcorrencia.mmoMotivo.Text;
      Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
      Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
      FreeAndNil(FrmOcorrencia);
      Result := True;
    end else begin
      Result := False;
      FreeAndNil(FrmOcorrencia);
      MsgInf('Informações da Ocorrência devem ser preenchidas');
      Application.ProcessMessages;
      Exit;
    end;
  end else begin
    Result := True;
  end;
end;


function TDMConexao.GravaLogOcorrencia(janela, campo, vr_ant, vr_novo, operador, operacao,
   id, motivo, solicitante: string): Boolean;
var teste: String;
begin
  if (DMConexao.config.IsEmpty) then
    DMConexao.Config.Open;
  if (ConfigMOSTRAR_TELA_OCORRENCIA.AsString = 'S') and ((Trim(solicitante) = '') or (Trim(motivo) = '')) then begin
    FrmOcorrencia := TFrmOcorrencia.Create(Self);
    if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
      solicitante := FrmOcorrencia.edtSolicitante.Text;
      motivo := FrmOcorrencia.mmoMotivo.Text;
      Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
      Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
      FreeAndNil(FrmOcorrencia);
      Result := True;
    end else begin
      Result := False;
      FreeAndNil(FrmOcorrencia);
      MsgInf('Informações da Ocorrência devem ser preenchidas');
      Application.ProcessMessages;
      Exit;
    end;
  end else begin
    Result := True;
  end;
  AdoQry.SQL.Clear;
  AdoQry.SQL.Add(' Insert into LOGS_OCORRENCIAS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
                         ' OPERACAO, DATA_HORA, ID, SOLICITANTE, MOTIVO) ');
  AdoQry.SQL.Add(' values (NEXT VALUE FOR SLOG_OCORRENCIA_ID,'''+janela+''','''+
  campo+''','''+vr_ant+''','''+vr_novo+''','''+operador+''','''+operacao+''',current_timestamp,'''+
  id+','''+solicitante+''','''+motivo+''')');
  AdoQry.SQL.Text;
  AdoQry.ExecSQL;
end;


function TDMConexao.getGeneratorValue(generator: String): integer;
begin
  AdoQry.Close;
  AdoQry.SQL.Clear;
  AdoQry.SQL.Text := 'SELECT NEXT VALUE FOR ' + generator;
  //AdoQry.SQL.Text := 'select gen_id(' + generator + ',1) from rdb$database';
  AdoQry.open;
  Result := AdoQry.Fields[0].AsInteger;
  AdoQry.Close;
end;

function TDMConexao.RetornaIDs(dataset:TDataSet;Nome_Chave:string):string;
var reg_atual : TBookmark;
begin
  Result := '';
  if dataset.IsEmpty then exit;
  dataset.DisableControls;
  reg_atual := dataset.GetBookmark;
  dataset.First;
  while not dataset.Eof do begin
     Result := Result + ',' + dataset.FieldByName(Nome_chave).AsString;
     dataset.next;
  end;
  dataset.GotoBookmark(reg_atual);
  dataset.freebookmark(reg_atual);
  dataset.EnableControls;
  if Trim(Result) <> '' then Delete(Result,1,1); // tirar a primeira virgula da string.
end;

function TDMConexao.ContaMarcados(dataset: TDataSet): integer;
var reg_atual : TBookmark;
begin
  Result := 0;
  if dataset.IsEmpty then exit;
  dataset.DisableControls;
  reg_atual := dataset.GetBookmark;
  dataset.First;
  while not dataset.Eof do begin
    if dataset.FieldByName('MARCADO').DataType in [ftString, ftWideString] then begin
      if dataset.fieldbyname('marcado').AsString = 'S' then Result := Result + 1
    end else if dataset.FieldByName('MARCADO').DataType in [ftBoolean] then
      if dataset.fieldbyname('marcado').AsBoolean then Result := Result + 1;
    dataset.next;
  end;
  dataset.GotoBookmark(reg_atual);
  dataset.freebookmark(reg_atual);
  dataset.EnableControls;
end;

function TDMConexao.fnData: TDateTime;
begin
  Result := Date;//(FormatDateTime('dd/mm/yyyy'));
end;

function TDMConexao.getEstado(cred_id: Integer): string;
begin
  Result := DMConexao.ExecuteScalar('SELECT ESTADO FROM CREDENCIADOS WHERE APAGADO <> ''S'' AND CRED_ID = ' + IntToStr(cred_id),-1);
end;

function TDMConexao.VerificaMovEmpPeriodo(empres_id:integer;dataini,datafin:TDateTime;Baixa_Conv:String='N'):Boolean;
begin
   Result := False;
   Q.Close;
   Q.SQL.Clear;
   Q.SQL.Add(' Select top 1 contacorrente.autorizacao_id ');
   Q.SQL.Add(' from contacorrente where contacorrente.conv_id in ');
   Q.SQL.Add(' (Select conv_id from conveniados where empres_id = '+IntToStr(empres_id)+')');
   Q.SQL.Add(' and contacorrente.data between '''+FormatDateTime('dd/mm/yyyy',dataini)+''' and '''+FormatDateTime('dd/mm/yyyy',datafin)+'''');
   if Baixa_Conv = 'N' then
      Q.SQL.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
   Q.SQL.Text;
   Q.Open;
   if Q.Fields[0].AsFloat > 0 then
      Result := True;
   Q.Close;
end;

function TDMConexao.VerificaMovEmpFechamento(empres_id:integer;data_fecha:tdatetime;Baixa_Conv:String='N'):Boolean;
begin
   Result := False;
   Q.Close;
   Q.SQL.Clear;
   Q.SQL.Add(' Select top 1 contacorrente.autorizacao_id ');
   Q.SQL.Add(' from contacorrente where contacorrente.conv_id in ');
   Q.SQL.Add(' (Select conv_id from conveniados where empres_id = '+IntToStr(empres_id)+')');
   Q.SQL.Add(' and contacorrente.data_fecha_emp = '+formatdateIB(data_fecha));
   if Baixa_Conv = 'N' then
      Q.SQL.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') <> ''S'' ');
   Q.Open;
   if Q.Fields[0].AsFloat > 0 then
       Result := True;
   Q.Close;
end;

function TDMConexao.ObterPeriodoEmpresa(Empres_id:Integer;DataFecha:TDateTime):TPeriodo;
begin
   if ValidarFechamentoEmp(DataFecha,Empres_id) then begin
      Q.Close;
      Q.SQL.Text := ' Select max(data_fecha) periodo from dia_fecha where empres_id = '+inttostr(Empres_id)+' and data_fecha < '+FormatDataIB(DataFecha);
      Q.Open;
      Result.DataIni := Q.Fields[0].AsDateTime;
      Result.DataFin := IncDay(DataFecha,-1);
      Q.Close;
   end
   else
      raise Exception.Create('Fechamento '+FormatDataBR(DataFecha)+' inválido para a empresa: '+inttostr(Empres_id));
end;

function TDMConexao.ObterFechaEmpresa(Empres_id:Integer):Variant;
begin
      Q.Close;
      DMConexao.Q.SQL.Clear;
      Q.SQL.Text := ' Select top(1) data_fecha from dia_fecha where DATA_FECHA > CURRENT_TIMESTAMP and empres_id = '+inttostr(Empres_id);
      Q.Open;
      Result := Q.Fields[0].AsDateTime;
      Q.Close;
end;

function TDMConexao.fnDivideComandoINSQL(Valores, Campo : String; NotIn : Boolean = False) : String;
var
  S: String;
  Condicao, Clausula  : String;
  SL : TStringList;
begin
SL := TStringList.Create;
if NotIn then
  begin
  Condicao := ' AND ';
  Clausula := ' NOT IN ';
  end
else
  begin
  Condicao := ' OR ';
  Clausula := ' IN ';
  end;
while Valores <> '' do
  begin
  S := S + Valores[1];
  Delete(Valores,1,1);
  if (Valores = '') and (Pos(Campo + Clausula, SL.Text) > 0) then
    SL.Add(Condicao + Campo + Clausula +'('+ S +')')
  else if (Valores = '') then
    SL.Add(Campo + Clausula +'('+ S +')')
  else if (Length(S) > 2000) and (S[Length(S)] = ',') then
    begin
    if Pos(Campo + Clausula, SL.Text) = 0 THEN
      SL.Add(Campo + Clausula +'('+ Copy(S,1,Length(S)-1) +')')
    else
      SL.Add(Condicao + Campo + Clausula +'('+ Copy(S,1,Length(S)-1) +')');
    S := '';
    end;
  end;
Result := SL.Text;
SL.Free;
end;


//PROCEDURE//
procedure TDMConexao.MarcaDesm(dataset: TDataSet);
begin
  if dataset.IsEmpty then exit;
  dataset.Edit;
  if dataset.FieldByName('MARCADO').DataType in [ftString, ftWideString] then
    dataset.FieldByName('MARCADO').AsString := ifThen(dataset.FieldByName('MARCADO').AsString = 'S','N','S')
  else if dataset.FieldByName('MARCADO').DataType in [ftBoolean] then
    dataset.FieldByName('MARCADO').AsBoolean := not dataset.FieldByName('MARCADO').AsBoolean;
  dataset.Post;

end;

procedure TDMConexao.AbrirCongis;
begin
  Config.Close;
  Config.Open;
end;

procedure TDMConexao.MarcaDesmTodos(dataset: TDataSet;Marcar:Boolean=True);
  var reg_atual : TBookmark;
  marcado : string[1];
begin
  if dataset.IsEmpty then exit;
  if Marcar then marcado := 'S'
            else marcado := 'N';
  dataset.DisableControls;
  reg_atual := dataset.GetBookmark;
  dataset.First;
  while not dataset.Eof do begin
    dataset.edit;
    dataset.fieldbyname('marcado').asstring := marcado;
    dataset.post;
    dataset.next;
  end;
  dataset.GotoBookmark(reg_atual);
  dataset.freebookmark(reg_atual);
  dataset.EnableControls;
end;

procedure TDMConexao.SortZQuery(Query:TDataset;FieldName:string);
begin
   
end;

procedure TDMConexao.AtualizaComReceitaMovProd(const iAutorizacao_ID: Integer;
  const sComRec: string);
begin
  Q.Close;
  Q.SQL.Text := ' update mov_prod2 set COMREC = ''' + sComRec + ''' where AUTORIZACAO_ID = ' + IntToStr(iAutorizacao_ID);
  Q.ExecSQL;
end;



end.
