object FAlteraDataVenc: TFAlteraDataVenc
  Left = 305
  Top = 201
  Width = 292
  Height = 260
  Caption = 'Altera'#231#227'o de Dados da Fatura'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 276
    Height = 222
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 96
      Width = 104
      Height = 13
      Caption = 'Vencimento da Fatura'
    end
    object Bevel1: TBevel
      Left = 8
      Top = 8
      Width = 264
      Height = 81
    end
    object Label2: TLabel
      Left = 16
      Top = 16
      Width = 59
      Height = 13
      Caption = 'N'#186' Fatura:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 32
      Width = 37
      Height = 13
      Caption = 'Nome:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 48
      Width = 90
      Height = 13
      Caption = 'Data da Fatura:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 64
      Width = 34
      Height = 13
      Caption = 'Valor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabFatura: TLabel
      Left = 112
      Top = 16
      Width = 48
      Height = 13
      Caption = 'LabFatura'
    end
    object LabNome: TLabel
      Left = 112
      Top = 32
      Width = 153
      Height = 13
      AutoSize = False
      Caption = 'LabNome'
    end
    object LabData: TLabel
      Left = 112
      Top = 48
      Width = 41
      Height = 13
      Caption = 'LabData'
    end
    object LabValor: TLabel
      Left = 112
      Top = 64
      Width = 42
      Height = 13
      Caption = 'LabValor'
    end
    object Label6: TLabel
      Left = 16
      Top = 144
      Width = 58
      Height = 13
      Caption = 'Observa'#231#227'o'
    end
    object btnGravar: TBitBtn
      Left = 64
      Top = 192
      Width = 75
      Height = 25
      Caption = 'Gravar'
      ModalResult = 1
      TabOrder = 2
      OnClick = btnGravarClick
    end
    object btnCancelar: TBitBtn
      Left = 152
      Top = 192
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btnCancelarClick
    end
    object edObs: TEdit
      Left = 16
      Top = 160
      Width = 249
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 0
    end
    object data: TJvDateEdit
      Left = 16
      Top = 112
      Width = 251
      Height = 21
      ShowNullDate = False
      TabOrder = 3
    end
  end
end
