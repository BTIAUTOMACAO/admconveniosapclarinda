unit UCadGrupoEstab;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, Menus, DB, ZDataset, {JvMemDS,} ZAbstractRODataset,
  ZAbstractDataset, Buttons, StdCtrls, Mask, JvToolEdit, ComCtrls, Grids,
  DBGrids, {JvDBCtrl,} DBCtrls, ExtCtrls, JvExMask, JvExDBGrids, JvDBGrid,
  ADODB;

type
  TFCadGrupoEstab = class(TFCad)
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    dbEdtDesc: TDBEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    QCadastroGRUPO_ESTAB_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    procedure ButIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButEditClick(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadGrupoEstab: TFCadGrupoEstab;

implementation

uses DM, UMenu, UTipos, UValidacao;

{$R *.dfm}

procedure TFCadGrupoEstab.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtDesc.SetFocus;
end;

procedure TFCadGrupoEstab.FormCreate(Sender: TObject);
begin
  chavepri := 'grupo_estab_id';
  detalhe  := 'Grupo ID';
  QCadastro.Open;  
  inherited;
end;

procedure TFCadGrupoEstab.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add('Select * from grupo_estab where coalesce(apagado,''N'') <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and grupo_estab_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and descricao like ''%'+EdNome.Text+'%'' ');
  QCadastro.Sql.Add(' order by descricao ');
  QCadastro.Open;
  If not QCadastro.IsEmpty then DBGrid1.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
end;

procedure TFCadGrupoEstab.ButEditClick(Sender: TObject);
begin
  inherited;
  dbEdtDesc.SetFocus;
end;

procedure TFCadGrupoEstab.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Grupo de Produto: '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadGrupoEstab.QCadastroAfterInsert(DataSet: TDataSet);
var grupo_estab_id  : Integer;
begin
  inherited;
  grupo_estab_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SGRUPO_ESTAB');
  QCadastroDTCADASTRO.AsDateTime:= date;
  QCadastroAPAGADO.AsString := 'N';
  QCadastroGRUPO_ESTAB_ID.AsInteger := grupo_estab_id;
  QCadastro.Open;
end;

procedure TFCadGrupoEstab.ButGravaClick(Sender: TObject);
begin
if fnVerfCompVazioEmTabSheet('Descrição obrigatória', dbEdtDesc) then Abort;
  inherited;
end;

procedure TFCadGrupoEstab.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  QCadastro.Close;
end;

end.
