inherited FAtualizarConsumoMes: TFAtualizarConsumoMes
  Left = 213
  Top = 132
  Caption = 'Atualizar Consumo M'#234's'
  ClientHeight = 524
  ClientWidth = 1008
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1008
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 1008
    Height = 234
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      1008
      234)
    object GroupBox1: TGroupBox
      Left = 6
      Top = 10
      Width = 999
      Height = 135
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Filtro de Dados'
      TabOrder = 0
      DesignSize = (
        999
        135)
      object Label2: TLabel
        Left = 9
        Top = 28
        Width = 41
        Height = 13
        Caption = 'Empresa'
      end
      object lblRegistros: TLabel
        Left = 720
        Top = 41
        Width = 5
        Height = 16
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblStatus: TLabel
        Left = 608
        Top = 25
        Width = 5
        Height = 16
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTotal: TLabel
        Left = 720
        Top = 41
        Width = 5
        Height = 16
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnAtualizar: TBitBtn
        Left = 374
        Top = 40
        Width = 136
        Height = 25
        Caption = 'Atualizar (F5)'
        TabOrder = 0
        OnClick = btnAtualizarClick
        NumGlyphs = 2
      end
      object edtEmp: TEdit
        Left = 9
        Top = 44
        Width = 48
        Height = 21
        Hint = 'Consulta pelo c'#243'digo do fornecedor'
        TabOrder = 1
        Text = '0'
        OnChange = edtEmpChange
      end
      object cbbEmp: TJvDBLookupCombo
        Left = 65
        Top = 44
        Width = 273
        Height = 21
        DisplayEmpty = 'Selecione uma empresa'
        EmptyValue = '0'
        LookupField = 'EMPRES_ID'
        LookupDisplay = 'NOME'
        LookupSource = dsListEmp
        TabOrder = 2
        OnChange = cbbEmpChange
      end
      object pb: TProgressBar
        Left = 8
        Top = 86
        Width = 985
        Height = 35
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 3
        Visible = False
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 740
    Top = 40
  end
  object dsListEmp: TDataSource
    DataSet = qListEmp
    Left = 704
    Top = 40
  end
  object qListEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome from empresas where apagado <> '#39'S'#39)
    Left = 664
    Top = 40
    object qListEmpempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qListEmpnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object QBusca: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT E.EMPRES_ID, E.BAND_ID, B.QTD_LIMITES, D.DATA_FECHA  FROM' +
        ' EMPRESAS E'
      ' LEFT JOIN BANDEIRAS B ON B.BAND_ID = E.BAND_ID '
      
        ' INNER JOIN DIA_FECHA D ON  D.EMPRES_ID = E.EMPRES_ID AND D.DATA' +
        '_FECHA = CONVERT(DATE,CURRENT_TIMESTAMP)')
    Left = 816
    Top = 39
    object QBuscaEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QBuscaBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object QBuscaQTD_LIMITES: TIntegerField
      FieldName = 'QTD_LIMITES'
    end
    object QBuscaDATA_FECHA: TDateTimeField
      FieldName = 'DATA_FECHA'
    end
  end
end
