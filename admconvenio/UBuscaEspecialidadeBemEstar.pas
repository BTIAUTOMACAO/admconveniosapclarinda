unit UBuscaEspecialidadeBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, Grids, DBGrids, JvExDBGrids, JvDBGrid, StdCtrls,
  Buttons, ComCtrls, Menus, ExtCtrls;

type
  TFBuscaEspecialiade = class(TF1)
    PageControl1: TPageControl;
    Panel1: TPanel;
    btnFiltroDados: TSpeedButton;
    btnAlterLinear: TSpeedButton;
    btnFirstA: TSpeedButton;
    btnPriorA: TSpeedButton;
    btnNextA: TSpeedButton;
    btnLastA: TSpeedButton;
    ButAtualiza: TBitBtn;
    ButFiltro: TBitBtn;
    ButAltLin: TButton;
    ButBusca: TBitBtn;
    EdNome: TEdit;
    EdCod: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    JvDBGrid1: TJvDBGrid;
    QCadastro: TADOQuery;
    DSCadastro: TDataSource;
    QCadastroESPECIALIDADE_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroLIBERADO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBuscaEspecialiade: TFBuscaEspecialiade;

implementation

{$R *.dfm}

end.
