inherited FrmExportPlanCred: TFrmExportPlanCred
  Left = 199
  Top = 207
  Caption = 'Exportar Planilha Credenciados'
  ClientHeight = 384
  ClientWidth = 795
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 795
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 23
    Width = 795
    Height = 88
    Align = alTop
    Caption = 'Filtro de Dados'
    TabOrder = 1
    object Label2: TLabel
      Left = 167
      Top = 18
      Width = 93
      Height = 13
      Caption = 'Dia de Fechamento'
    end
    object Bevel1: TBevel
      Left = 160
      Top = 17
      Width = 2
      Height = 57
    end
    object btnListarEmp: TButton
      Left = 648
      Top = 35
      Width = 137
      Height = 23
      Caption = 'Listar Empresas (F5)'
      TabOrder = 4
      OnClick = btnListarEmpClick
    end
    object DataIni: TJvDateEdit
      Left = 167
      Top = 34
      Width = 103
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 2
    end
    object por_dia_fecha: TRadioButton
      Left = 8
      Top = 24
      Width = 137
      Height = 17
      Caption = 'Por dia de Fechamento'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = por_dia_fechaClick
    end
    object por_periodo: TRadioButton
      Left = 8
      Top = 48
      Width = 129
      Height = 17
      Caption = 'Por Per'#237'odo'
      TabOrder = 1
      OnClick = por_dia_fechaClick
    end
    object dataFin: TJvDateEdit
      Left = 282
      Top = 34
      Width = 107
      Height = 21
      NumGlyphs = 2
      ShowNullDate = False
      TabOrder = 3
      Visible = False
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 111
    Width = 795
    Height = 273
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = '&Empresas'
      object lblTotal: TLabel
        Left = 0
        Top = 178
        Width = 787
        Height = 13
        Align = alBottom
        Caption = 'Total: 0'
      end
      object Panel3: TPanel
        Left = 0
        Top = 191
        Width = 787
        Height = 54
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 0
        DesignSize = (
          783
          50)
        object Label1: TLabel
          Left = 425
          Top = 5
          Width = 176
          Height = 13
          Caption = 'Salvar as planilhas no diret'#243'rio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ButMarcDesm_Emp: TButton
          Left = 7
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F11)'
          TabOrder = 0
          OnClick = ButMarcDesm_EmpClick
        end
        object ButMarcaTodos_Emp: TButton
          Left = 115
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F8)'
          TabOrder = 1
          OnClick = ButMarcaTodos_EmpClick
        end
        object ButDesmTodos_Emp: TButton
          Left = 224
          Top = 14
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F9)'
          TabOrder = 2
          OnClick = ButDesmTodos_EmpClick
        end
        object btnGerarXls: TBitBtn
          Left = 656
          Top = 8
          Width = 118
          Height = 32
          Anchors = [akTop, akRight]
          Caption = '&Gerar XLS (F7)'
          Enabled = False
          TabOrder = 3
          OnClick = btnGerarXlsClick
          Glyph.Data = {
            96010000424D9601000000000000760000002800000018000000180000000100
            0400000000002001000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DD888888888888888DDDDDDDDD8FFFFFFFFFFF888DDDDDDDDD8FFFFFFFFFFFF8
            8DDDDDDDDD8F8888888888FF8DDDDDDDDD8F8F8FFF888FFF8DDDDDDDDD8F8F8F
            FF888FFF8DDDDDDDDD8F8F8FFF888FFF8DDDDDDDDD8F8F8FF8F8FFFF8DDDDD38
            77777728888888FF8DDDDD3FFFFFFF2FF8F8FFFF8DDDDD7F0008202FFFF8FFFF
            8DDDDD7F8287282F888888FF8DDDDD7FF877FF2FFFF8FFFF8DDDDD7F8777382F
            FF8888FF8DDDDD7F88F8882FFFFFFFFF8DDDDD7FF888883FFFFFF8FF7DDDDDF8
            8888888FFFFFF8F7DDDDDDDDDDFFFFFFFFFFF87DDDDDDDDDDD888888888887DD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        end
        object edtDir: TJvDirectoryEdit
          Left = 424
          Top = 19
          Width = 217
          Height = 21
          DialogKind = dkWin32
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 4
        end
      end
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 0
        Width = 787
        Height = 178
        Align = alClient
        DataSource = dsEmp
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = JvDBGrid1DblClick
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'Empresa ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Title.Caption = 'Nome'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'data_fecha'
            Title.Caption = 'Data de Fechamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'data_venc'
            Title.Caption = 'Data de Vencimento'
            Visible = True
          end>
      end
    end
  end
  object dsEmp: TDataSource
    DataSet = MDEmp
    OnDataChange = dsEmpDataChange
    Left = 78
    Top = 209
  end
  object qEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'dataIni'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'dataFin'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  emp.empres_id,'
      '  emp.nome,'
      '  df.data_fecha,'
      '  df.data_venc,'
      ' '#39'N'#39' as marcado'
      'from empresas emp'
      'join dia_fecha df on df.empres_id = emp.empres_id'
      'where df.data_fecha between :dataIni and :dataFin'
      'and emp.tipo_credito in (2,3)'
      'and emp.apagado <> '#39'S'#39)
    Left = 80
    Top = 176
    object qEmpempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qEmpnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEmpdata_fecha: TDateTimeField
      FieldName = 'data_fecha'
    end
    object qEmpdata_venc: TDateTimeField
      FieldName = 'data_venc'
    end
  end
  object qConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select '
      ' c.empres_id, '
      ' c.conv_id, '
      ' card.codcartimp, '
      ' c.titular, '
      ' c.saldo_renovacao as valor_renovacao'
      'from '
      ' conveniados c '
      ' join cartoes card on card.conv_id = c.conv_id '
      'where c.apagado <> '#39'S'#39
      'and card.apagado <> '#39'S'#39
      'and card.titular = '#39'S'#39
      'and c.empres_id = :empres_id'
      'order by c.titular')
    Left = 120
    Top = 176
    object qConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object qConvcodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object qConvtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object qConvvalor_renovacao: TBCDField
      FieldName = 'valor_renovacao'
      Precision = 15
      Size = 2
    end
  end
  object MDEmp: TJvMemoryData
    FieldDefs = <
      item
        Name = 'empres_id'
        DataType = ftInteger
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'data_fecha'
        DataType = ftDateTime
      end
      item
        Name = 'data_venc'
        DataType = ftDateTime
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end>
    Left = 80
    Top = 240
    object MDEmpempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object MDEmpnome: TStringField
      FieldName = 'nome'
    end
    object MDEmpdata_fecha: TDateTimeField
      FieldName = 'data_fecha'
    end
    object MDEmpdata_venc: TDateTimeField
      FieldName = 'data_venc'
    end
    object MDEmpmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
end
