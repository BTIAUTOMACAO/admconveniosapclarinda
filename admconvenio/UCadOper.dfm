inherited FCadOper: TFCadOper
  Left = 57
  Top = -8
  Caption = 'Cadastro de Operadores'
  ClientHeight = 746
  ClientWidth = 1301
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 1301
    Height = 705
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 894
        object Label6: TLabel [2]
          Left = 364
          Top = 5
          Width = 43
          Height = 13
          Caption = '&Grupo ID'
          FocusControl = EdGrupoID
        end
        object Label7: TLabel [3]
          Left = 450
          Top = 5
          Width = 60
          Height = 13
          Caption = 'Grupo Nome'
          FocusControl = EdGrupoNome
        end
        inherited EdNome: TEdit
          TabOrder = 1
        end
        inherited ButBusca: TBitBtn
          Left = 664
          TabOrder = 4
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 791
          TabOrder = 7
        end
        inherited EdCod: TEdit
          TabOrder = 0
        end
        inherited ButFiltro: TBitBtn
          TabOrder = 5
        end
        inherited ButAltLin: TButton
          TabOrder = 6
        end
        object EdGrupoID: TEdit
          Left = 364
          Top = 20
          Width = 73
          Height = 21
          TabOrder = 2
          OnExit = EdCodExit
          OnKeyPress = EdCodKeyPress
        end
        object EdGrupoNome: TEdit
          Left = 448
          Top = 21
          Width = 201
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 3
          OnKeyPress = EdNomeKeyPress
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 894
        Columns = <
          item
            Expanded = False
            FieldName = 'USUARIO_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO_USU_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO'
            Width = 202
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Width = 894
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 645
        Width = 1293
        inherited ButGrava: TBitBtn
          Left = 622
          TabOrder = 4
        end
        inherited ButCancela: TBitBtn
          Left = 707
          TabOrder = 5
        end
        object ButLimpaSenha: TBitBtn
          Left = 261
          Top = 4
          Width = 85
          Height = 25
          Caption = 'Limpar Senha'
          TabOrder = 3
          OnClick = ButLimpaSenhaClick
        end
      end
      inherited Panel3: TPanel
        Width = 1293
        Height = 645
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 1289
          Height = 146
          Align = alTop
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 16
            Top = 21
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 67
            Top = 21
            Width = 28
            Height = 13
            Caption = 'Nome'
            FocusControl = dbEdtNm
          end
          object Label5: TLabel
            Left = 17
            Top = 67
            Width = 29
            Height = 13
            Caption = 'Grupo'
            FocusControl = dbEdtNm
          end
          object DBEdit1: TDBEdit
            Left = 16
            Top = 37
            Width = 45
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'USUARIO_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object dbEdtNm: TDBEdit
            Left = 67
            Top = 37
            Width = 224
            Height = 21
            CharCase = ecUpperCase
            DataField = 'NOME'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object DBCheckBox1: TDBCheckBox
            Left = 16
            Top = 116
            Width = 97
            Height = 17
            Caption = 'Liberado'
            DataField = 'LIBERADO'
            DataSource = DSCadastro
            TabOrder = 3
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object DBGrupo: TJvDBLookupCombo
            Left = 16
            Top = 83
            Width = 275
            Height = 21
            DataField = 'GRUPO_USU_ID'
            DataSource = DSCadastro
            LookupField = 'grupo_usu_id'
            LookupDisplay = 'descricao'
            LookupSource = DSGrupos
            TabOrder = 2
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 576
          Width = 1289
          Height = 67
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 1
          object Label14: TLabel
            Left = 16
            Top = 18
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
            FocusControl = DBEdit12
          end
          object Label13: TLabel
            Left = 144
            Top = 18
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit11
          end
          object DBEdit12: TDBEdit
            Left = 16
            Top = 33
            Width = 113
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 144
            Top = 33
            Width = 161
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 1293
      end
      inherited GridHistorico: TJvDBGrid
        Width = 1293
        Height = 630
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 1301
  end
  inherited panStatus2: TPanel
    Width = 1301
  end
  inherited DSCadastro: TDataSource
    Left = 484
    Top = 200
  end
  inherited DSHistorico: TDataSource
    Top = 200
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from usuarios where usuario_id = 0')
    Left = 484
    object QCadastroGRUPO_USU_ID: TIntegerField
      DisplayLabel = 'Grupo ID'
      FieldName = 'GRUPO_USU_ID'
    end
    object QCadastroLIBERADO: TStringField
      DisplayLabel = 'Lib.'
      FieldName = 'LIBERADO'
      Size = 1
    end
    object QCadastroNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 45
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroUSUARIO_ID: TIntegerField
      DisplayLabel = 'Oper ID'
      FieldName = 'USUARIO_ID'
      Required = True
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroGRUPO: TStringField
      FieldKind = fkLookup
      FieldName = 'GRUPO'
      LookupDataSet = QGrupos
      LookupKeyFields = 'grupo_usu_id'
      LookupResultField = 'descricao'
      KeyFields = 'GRUPO_USU_ID'
      Lookup = True
    end
  end
  object DSGrupos: TDataSource
    DataSet = QGrupos
    OnStateChange = DSCadastroStateChange
    Left = 576
    Top = 200
  end
  object QGrupos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'Select grupo_usu_id,  descricao from grupo_usuarios where libera' +
        'do = '#39'S'#39
      'and apagado <> '#39'S'#39
      'order by descricao')
    Left = 574
    Top = 163
    object QGruposgrupo_usu_id: TIntegerField
      FieldName = 'grupo_usu_id'
    end
    object QGruposdescricao: TStringField
      FieldName = 'descricao'
      Size = 25
    end
  end
end
