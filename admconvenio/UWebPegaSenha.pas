// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://200.204.38.187/pedesenha/PedeSenha.dll/wsdl/IWebSenha
// Encoding : utf-8
// Version  : 1.0
// (24/1/2005 16:17:42 - 1.33.2.5)
// ************************************************************************ //

unit UWebPegaSenha;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"


  // ************************************************************************ //
  // Namespace : urn:UWebSenhaIntf-IWebSenha
  // soapAction: urn:UWebSenhaIntf-IWebSenha#PegaSenha
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IWebSenhabinding
  // service   : IWebSenhaservice
  // port      : IWebSenhaPort
  // URL       : http://200.204.38.187/pedesenha/PedeSenha.dll/soap/IWebSenha
  // ************************************************************************ //
  IWebSenha = interface(IInvokable)
  ['{06F6DD66-EFB1-C4DD-FEB1-259838E62404}']
    function  PegaSenha(const CNPJ, Razao, ip, versao: WideString): WideString; stdcall;
  end;

function GetIWebSenha(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IWebSenha;


implementation

function GetIWebSenha(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IWebSenha;
const
  defWSDL = 'http://189.56.94.82/pedesenha/PedeSenha.dll/wsdl/IWebSenha';
  defURL  = 'http://189.56.94.82/pedesenha/PedeSenha.dll/soap/IWebSenha';
//  webService  = 'http://189.56.94.82/pedesenha/PedeSenha.dll/soap/IWebSenha';
//  Local       = 'http://192.168.0.2/pedesenha/PedeSenha.dll/soap/IWebSenha';
  defSvc  = 'IWebSenhaservice';
  defPrt  = 'IWebSenhaPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IWebSenha);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IWebSenha), 'urn:UWebSenhaIntf-IWebSenha', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IWebSenha), 'urn:UWebSenhaIntf-IWebSenha#PegaSenha');
  InvRegistry.RegisterInvokeOptions(TypeInfo(IWebSenha),ioDocument);
end.