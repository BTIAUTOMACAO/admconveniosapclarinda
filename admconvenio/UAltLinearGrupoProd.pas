unit UAltLinearGrupoProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, {JvLookup,} DB, Mask, ToolEdit,
  CurrEdit, JvExControls, JvDBLookup, JvExStdCtrls, JvEdit, JvValidateEdit;

type
  TFAltLinearGrupoProd = class(TForm)
    Panel1: TPanel;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    edtHistorico: TEdit;
    cbbGrupo: TJvDBLookupCombo;
    Label3: TLabel;
    DataSource1: TDataSource;
    edtValor: TJvValidateEdit;
    procedure btnGravarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAltLinearGrupoProd: TFAltLinearGrupoProd;

implementation

uses cartao_util, DM;

{$R *.dfm}

procedure TFAltLinearGrupoProd.btnGravarClick(Sender: TObject);
begin
  if cbbGrupo.KeyValue = 0 then
  begin
    MsgInf('Selecione um grupo de produto para a altera��o!');
    cbbGrupo.SetFocus;
  end
  else if edtValor.Text = '' then
  begin
    MsgInf('Insira um valor para a altera��o!');
    edtValor.SetFocus;
  end
  else if edtHistorico.Text = '' then
  begin
    MsgInf('Insira um historico para a altera��o!');
    edtHistorico.SetFocus;
  end
  else
    FAltLinearGrupoProd.ModalResult:= mrOk;
end;

procedure TFAltLinearGrupoProd.FormCreate(Sender: TObject);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('select grupo_prod_id, descricao from grupo_prod where apagado = ''N'' order by grupo_prod_id ');
  DMConexao.AdoQry.Open;
end;

procedure TFAltLinearGrupoProd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DMConexao.AdoQry.Close;
end;

procedure TFAltLinearGrupoProd.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

end.
