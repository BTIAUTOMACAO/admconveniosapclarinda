unit URelExtratoFor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, Buttons, ExtCtrls, Mask, JvToolEdit,
  DB, RxMemDS, dateutils, IBCustomDataSet, IBQuery, ZAbstractRODataset, UTipos,
  ZDataset, U1, ToolEdit, ComCtrls, {JvDBCtrl,} ZAbstractDataset, ClassImpressao,
  JvExDBGrids, JvDBGrid, frxClass, frxGradient, frxDBSet, frxExportPDF, ShellApi,
  frxChart, JvExMask, ADODB, JvMemoryDataset;

type
  TFRelExtratoFor = class(TF1)
    DSEmpresa: TDataSource;
    DSCred: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    DBGrid1: TJvDBGrid;
    Panel4: TPanel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    Panel3: TPanel;
    Label2: TLabel;
    Button2: TButton;
    DBGrid2: TJvDBGrid;
    Panel5: TPanel;
    ButMarcaDesmEmp: TSpeedButton;
    ButMarcaTodosEmp: TSpeedButton;
    ButDesmTodosEmp: TSpeedButton;
    Panel1: TPanel;
    Button1: TButton;
    TabSheet3: TTabSheet;
    Panel60: TPanel;
    Label5: TLabel;
    ButListaConv: TButton;
    Button5: TButton;
    DBGrid3: TJvDBGrid;
    Panel7: TPanel;
    ButMarcaDesmConv: TSpeedButton;
    ButMarcaTodosConv: TSpeedButton;
    ButDesmTodosConv: TSpeedButton;
    DSConveniados: TDataSource;
    Bevel4: TBevel;
    PanFechaEmp: TPanel;
    //DataFechaIni: TDateEdit;
    //DataFechaFim: TDateEdit;
    Label6: TLabel;
    Label7: TLabel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabConfExtConferen: TTabSheet;
    CkUmPorPag: TCheckBox;
    CkLiquido: TCheckBox;
    CkMostraNF: TCheckBox;
    RGReceita: TRadioGroup;
    Cktotemp: TCheckBox;
    rgpNFEntregue: TRadioGroup;
    RGModelo: TRadioGroup;
    RGFaturadas: TRadioGroup;
    RGPagas: TRadioGroup;
    PanButVis: TPanel;
    Label4: TLabel;
    ComboOrdem: TComboBox;
    ordemdec: TCheckBox;
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    por_fecha_emp: TRadioButton;
    por_periodo: TRadioButton;
    por_dia_fecha: TRadioButton;
    Label3: TLabel;
    //dataini: TDateEdit;
    //datafin: TDateEdit;
    Bevel1: TBevel;
    RGBaixaFor: TRadioGroup;
    DescFornec: TCheckBox;
    //DataDescinicial: TDateEdit;
    Label1: TLabel;
    //DataDescfinal: TDateEdit;
    RGBaixaEmp: TRadioGroup;
    ButAbrir: TButton;
    BitBtn2: TBitBtn;
    dbCred: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    StringDS: TfrxUserDataSet;
    dbEmpresas: TfrxDBDataset;
    dbQuery1: TfrxDBDataset;
    frxListagem: TfrxReport;
    frxAgrupadoEmpresa: TfrxReport;
    cbMostrarCanceladas: TCheckBox;
    frxPDFExport1: TfrxPDFExport;
    bntGerarPDF: TBitBtn;
    Button3: TButton;
    frxReport1: TfrxReport;
    frxChartObject1: TfrxChartObject;
    frxDBDataset1: TfrxDBDataset;
    dataini: TJvDateEdit;
    datafin: TJvDateEdit;
    DataFechaIni: TJvDateEdit;
    DataFechaFim: TJvDateEdit;
    DataDescfinal: TJvDateEdit;
    DataDescinicial: TJvDateEdit;
    QCred: TADOQuery;
    QCredCred_id: TIntegerField;
    QCrednome: TStringField;
    QCredfantasia: TStringField;
    QCredcomissao: TBCDField;
    QEmpresa: TADOQuery;
    QEmpresaempres_id: TAutoIncField;
    QEmpresanome: TStringField;
    QEmpresamarcado: TStringField;
    QDesconto: TADOQuery;
    QDescontodata: TDateTimeField;
    QDescontohistorico: TStringField;
    QDescontovalor: TBCDField;
    Query1: TADOQuery;
    Query1perc_comiss: TBCDField;
    Query1debito: TBCDField;
    Query1credito: TBCDField;
    Query1receita: TStringField;
    Query1autorizacao_id: TIntegerField;
    Query1digito: TWordField;
    Query1nf: TIntegerField;
    Query1entreg_nf: TStringField;
    Query1titular: TStringField;
    Query1chapa: TFloatField;
    Query1conv_id: TIntegerField;
    Query1data: TDateTimeField;
    Query1empres_id: TAutoIncField;
    Query1empresa: TStringField;
    Query1baixa_conveniado: TStringField;
    Query1baixa_credenciado: TStringField;
    Query1pago: TStringField;
    Query1fatura_id: TIntegerField;
    QConveniados: TADOQuery;
    QConveniadosconv_id: TIntegerField;
    QConveniadoschapa: TFloatField;
    QConveniadostitular: TStringField;
    QConveniadosempres_id: TAutoIncField;
    QConveniadosempresa: TStringField;
    roq: TADOQuery;
    roqperc_comiss: TBCDField;
    roqdebito: TBCDField;
    roqcredito: TBCDField;
    roqreceita: TStringField;
    roqautorizacao_id: TIntegerField;
    roqdigito: TWordField;
    roqnf: TIntegerField;
    roqentreg_nf: TStringField;
    roqtitular: TStringField;
    roqchapa: TFloatField;
    roqconv_id: TIntegerField;
    roqdata: TDateTimeField;
    roqempres_id: TAutoIncField;
    roqempresa: TStringField;
    roqbaixa_conveniado: TStringField;
    roqbaixa_credenciado: TStringField;
    roqdata_pgto: TDateTimeField;
    roqpago: TStringField;
    roqfatura_id: TIntegerField;
    MCred: TJvMemoryData;
    MCredcred_id: TIntegerField;
    MCrednome: TStringField;
    MCredfantasia: TStringField;
    MCredcomissao: TFloatField;
    MCredmarcado: TBooleanField;
    MEmpresa: TJvMemoryData;
    MEmpresaempres_id: TIntegerField;
    MEmpresanome: TStringField;
    MEmpresadata_fecha: TDateTimeField;
    MEmpresamarcado: TBooleanField;
    QEmpresadata_fecha: TDateTimeField;
    MConveniado: TJvMemoryData;
    MConveniadoconv_id: TIntegerField;
    MConveniadochapa: TIntegerField;
    MConveniadotitular: TStringField;
    MConveniadoempres_id: TIntegerField;
    MConveniadoempresa: TStringField;
    MConveniadomarcado: TBooleanField;
    sd: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButMarcaTodosEmpClick(Sender: TObject);
    procedure ButDesmTodosEmpClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    // function ExisteMov:boolean;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure ButAbrirClick(Sender: TObject);
    procedure DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure por_dia_fechaClick(Sender: TObject);
    procedure por_periodoClick(Sender: TObject);
    procedure datainiChange(Sender: TObject);
    procedure DescFornecClick(Sender: TObject);
    procedure ButListaConvClick(Sender: TObject);
    procedure ButMarcaDesmConvClick(Sender: TObject);
    procedure ButMarcaTodosConvClick(Sender: TObject);
    procedure ButDesmTodosConvClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure DBGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid3TitleClick(Column: TColumn);
    procedure CktotempClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure por_fecha_empClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure DBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure RGModeloClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure frxListagemPreview(Sender: TObject);
    procedure frxListagemClosePreview(Sender: TObject);
    procedure QConveniadosAfterScroll(DataSet: TDataSet);
    procedure QCredAfterScroll(DataSet: TDataSet);
    procedure QEmpresaAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure MCredAfterScroll(DataSet: TDataSet);
  private
    empresas, conveniados : string;
    fornec_sel : String;
    empres_sel : String;
    conv_sel : String;
    MostrarRelatorio, filtrarEntrNF, acumularAutor : Boolean;
    Marcados : TStringList;
    procedure ConveniadosSel;
    procedure EmpresasSel;
    function GetEmpresFechamento: String;
    function GetFechamentosSel: String;
    function AbrirQuery: Boolean;
    procedure ImprimirModeloConferencia;
    procedure ImprimirModeloConferencia2;
    procedure CriarTituloModConf(Imp: TImpres);
    function CriarCorpoModConf(Imp:TImpres):TAutorizacao;
    procedure CriarTitulosColunas(Imp: TImpres);
    function CriarLinhaDetalhe(Imp:TImpres):TAutorizacao;
    procedure TotalizarEmp(Imp: Timpres; Debito, Credido: Currency);
    procedure TotalizarForn(Imp: Timpres; Debito, Credido, ValorComiss: Currency);
    function AbrirDescontosFornec: Boolean;
    function CriarDescontosFor(Imp:Timpres):currency;
    procedure ImprimirModeloListagem;
    procedure ImprimirModeloListagem2;
    procedure CriarTituloModList(Imp: TImpres);
    function CriarCorpoModList(Imp:TImpres):TValores;
    procedure CriarTitulosColunasModList(Imp: TImpres);
    function CriarLinhaDetalheModList(Imp:TImpres):TValores;
    procedure TotalizarEmpModList(Imp: TImpres; BrutConf, BrutNConf, LiqConf, LiqNConf: Currency);
    procedure TotalizarForModList(Imp: TImpres; BrutConf, BrutNConf, LiqConf, LiqNConf: Currency);
    procedure Fornecedores_Sel;
  public
  end;

var
  FRelExtratoFor: TFRelExtratoFor;

implementation

uses DM, impressao,  Math, cartao_util;

{$R *.dfm}

procedure TFRelExtratoFor.FormCreate(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := False;
  dataini.Date := date;
  datainiChange(nil);
  DataDescinicial.Date  := StartOfTheMonth(Date);
  DataDescfinal.Date    := EndOfTheMonth(Date);
  DataFechaIni.Date     := StartOfTheMonth(Date);
  DataFechaFim.Date     := EndOfTheMonth(Date);
  DMConexao.Config.Open;
  filtrarEntrNF := (DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'S');
  acumularAutor := (DMConexao.ConfigACUMULA_AUTOR_PROX_FECHA.AsString = 'S');
  DMConexao.Config.Close;
  if not filtrarEntrNF then begin
    rgpNFEntregue.TabStop := False;
    rgpNFEntregue.Visible := False;
  end;
end;

procedure TFRelExtratoFor.Button2Click(Sender: TObject);
var Field : TDateField;
begin
   DBGrid2.Columns[2].Visible   := cartao_util.iif(por_fecha_emp.Checked,True,False);
   empresas := EmptyStr;
   screen.Cursor := crHourGlass;
   QEmpresa.Close;
   QEmpresa.SQL.Clear;
   QEmpresa.SQL.Add(' Select empres_id, nome, ''N'' as marcado ');
   QEmpresa.SQL.Add(' ,  '+cartao_util.iif(por_fecha_emp.Checked,'dia_fecha.data_fecha',' current_timestamp')+' as data_fecha ');
   QEmpresa.SQL.Add(' from empresas ');
   if por_fecha_emp.Checked then begin
      Field := TDateField.Create(QEmpresa);
      Field.FieldName := 'data_fecha';
      Field.DisplayFormat := 'dd/mm/yyyy';
      QEmpresa.Fields.Add(Field);
      QEmpresa.SQL.Add(' join dia_fecha on dia_fecha.empres_id = empresas.empres_id ');
   end;
   QEmpresa.SQL.Add(' where apagado <> ''S'' ');
   if por_fecha_emp.Checked then
      QEmpresa.SQL.Add(' and dia_fecha.data_fecha between '+formatdataIB(DataFechaIni.Date)+' and '+formatdataIB(DataFechaFim.Date));
   QEmpresa.SQL.Add(' order by nome ');
   QEmpresa.Open;
   Screen.Cursor := crDefault;
   if QEmpresa.IsEmpty then
      MsgInf('Nenhuma empresa encontrada!')
   else begin
      MsgInf('Aten��o, ser�o mostrados no relat�rio apenas os valores de conveniados das empresas selecionadas.');
      QEmpresa.First;
      MEmpresa.Open;
      MEmpresa.EmptyTable;
      MEmpresa.DisableControls;
      while not QEmpresa.Eof do begin
        MEmpresa.Append;
        MEmpresaempres_id.AsInteger := QEmpresaempres_id.AsInteger;
        MEmpresanome.AsString := QEmpresanome.AsString;
        MEmpresadata_fecha.AsDateTime := QEmpresadata_fecha.AsDateTime;
        MEmpresaMarcado.AsBoolean := False;
        MEmpresa.Post;
        QEmpresa.Next;
      end;
      MEmpresa.First;
      MEmpresa.EnableControls;
      empres_sel := EmptyStr;
      DBGrid2.SetFocus;
   end;
end;

procedure TFRelExtratoFor.SpeedButton5Click(Sender: TObject);
begin
//DMConexao.MarcaDesm(QCred);
if MCred.IsEmpty then Exit;
  MCred.Edit;
  MCredMarcado.AsBoolean := not MCredMarcado.AsBoolean;
  MCred.Post;
  Fornecedores_Sel;
  DBGrid1.SetFocus;
end;

procedure TFRelExtratoFor.Fornecedores_Sel;
var marca : TBookmark;
begin
  fornec_sel := EmptyStr;
  marca := MCred.GetBookmark;
  MCred.DisableControls;
  MCred.First;
  while not MCred.eof do begin
    if MCredMarcado.AsBoolean then fornec_sel := fornec_sel + ','+MCredcred_id.AsString;
    MCred.Next;
  end;
  MCred.GotoBookmark(marca);
  MCred.FreeBookmark(marca);
  if fornec_sel <> '' then fornec_sel := Copy(fornec_sel,2,Length(fornec_sel));
  MCred.EnableControls;
end;


procedure TFRelExtratoFor.SpeedButton6Click(Sender: TObject);
var marca : TBookmark;
begin
  if MCred.IsEmpty then Exit;
  MCred.DisableControls;
  marca := MCred.GetBookmark;
  MCred.First;
  while not MCred.eof do begin
    MCred.Edit;
    MCredMarcado.AsBoolean := true;
    MCred.Post;
    MCred.Next;
  end;
  MCred.GotoBookmark(marca);
  MCred.FreeBookmark(marca);
  MCred.EnableControls;
  Fornecedores_Sel;
//DMConexao.MarcaDesmTodos(QCred,True);
end;

procedure TFRelExtratoFor.SpeedButton7Click(Sender: TObject);
var marca : TBookmark;
begin
  if MCred.IsEmpty then Exit;
  MCred.DisableControls;
  marca := MCred.GetBookmark;
  MCred.First;
  while not MCred.eof do begin
    MCred.Edit;
    MCredMarcado.AsBoolean := false;
    MCred.Post;
    MCred.Next;
  end;
  MCred.GotoBookmark(marca);
  MCred.FreeBookmark(marca);
  MCred.EnableControls;
  Fornecedores_Sel;
//DMConexao.MarcaDesmTodos(QCred,False);
end;

procedure TFRelExtratoFor.ButMarcaDesmEmpClick(Sender: TObject);
begin
if MEmpresa.IsEmpty then Exit;
  MEmpresa.Edit;
  MEmpresaMarcado.AsBoolean := not MEmpresaMarcado.AsBoolean;
  MEmpresa.Post;
  EmpresasSel;
  DBGrid2.SetFocus;
end;

procedure TFRelExtratoFor.ButMarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresa.IsEmpty then Exit;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  MEmpresa.First;
  while not MEmpresa.eof do begin
    MEmpresa.Edit;
    MEmpresaMarcado.AsBoolean := true;
    MEmpresa.Post;
    MEmpresa.Next;
  end;
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
  EmpresasSel;
//DMConexao.MarcaDesmTodos(QEmpresa,True);
end;

procedure TFRelExtratoFor.ButDesmTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MEmpresa.IsEmpty then Exit;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  MEmpresa.First;
  while not MEmpresa.eof do begin
    MEmpresa.Edit;
    MEmpresaMarcado.AsBoolean := false;
    MEmpresa.Post;
    MEmpresa.Next;
  end;
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
  EmpresasSel;
//DMConexao.MarcaDesmTodos(QEmpresa,False);

end;


procedure TFRelExtratoFor.DBGrid1DblClick(Sender: TObject);
begin
SpeedButton5.Click;
end;

procedure TFRelExtratoFor.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then SpeedButton5.Click;
end;

procedure TFRelExtratoFor.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
If key = vk_return then ButMarcaDesmEmp.Click;
end;

procedure TFRelExtratoFor.BitBtn1Click(Sender: TObject);
begin
  if ((not QCred.Active) or (DMConexao.ContaMarcados(QCred) = 0)) then begin
     MsgInf('Nenhum fornecedor selecionado.');
     Exit;
  end;

  if RGModelo.ItemIndex = 1 then
  begin
    ImprimirModeloConferencia;
    Exit;
  end
  else
  begin
    ImprimirModeloListagem;
    Exit;
  end;
end;

procedure TFRelExtratoFor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_F2  then ButAbrirClick(nil);
  if key = VK_F11 then SpeedButton5.Click;
  if key = VK_F8  then SpeedButton6.Click;
  if key = vk_F9  then SpeedButton7.Click;
  if key = vk_F5  then begin
     if PageControl1.ActivePageIndex = 1 then
        ButMarcaDesmEmp.Click
     else if PageControl1.ActivePageIndex = 2 then
        ButMarcaDesmConv.Click;
  end;
  if key = vk_F6  then begin
     if PageControl1.ActivePageIndex = 1 then
        ButMarcaTodosEmp.Click
     else if PageControl1.ActivePageIndex = 2 then
        ButMarcaTodosConv.Click;
  end;
  if key = vk_F7  then begin
     if PageControl1.ActivePageIndex = 1 then
        ButDesmTodosEmp.Click
     else if PageControl1.ActivePageIndex = 2 then
        ButDesmTodosConv.Click;
  end;

end;

procedure TFRelExtratoFor.EmpresasSel;
var marca : TBookmark;
begin
  empresas := EmptyStr;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  while not MEmpresa.Eof do begin
    if MEmpresaMARCADO.AsBoolean = true then empresas := empresas+','+MEmpresaEMPRES_ID.AsString;
    MEmpresa.Next;
  end;
  empresas := Copy(empresas,2,Length(empresas));
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
end;

procedure TFRelExtratoFor.ConveniadosSel;
var marca : TBookmark;
begin
if not MConveniado.Active then
  begin
  MsgInf('Lista de Conveniados est� fechada, por favor Liste os conveniados!');
  Exit;
  end;
conveniados := EmptyStr;
MConveniado.DisableControls;
marca := MConveniado.GetBookmark;
MConveniado.First;
while not MConveniado.Eof do begin
  if MConveniadoMARCADO.AsBoolean  = true then conveniados := conveniados+','+MConveniadoCONV_ID.AsString;
  MConveniado.Next;
end;
conveniados := Copy(conveniados,2,Length(conveniados));
MConveniado.GotoBookmark(marca);
MConveniado.FreeBookmark(marca);
MConveniado.EnableControls;
end;

procedure TFRelExtratoFor.Button1Click(Sender: TObject);
begin
  QEmpresa.Close;
  MEmpresa.Close;
  empresas := EmptyStr;
  MsgInf('Aten��o, ser�o mostrados no relat�rio apenas os valores de conveniados de todas empresas.');
end;

procedure TFRelExtratoFor.ButAbrirClick(Sender: TObject);
begin
inherited;
  MEmpresa.Close;
  MConveniado.Close;
  Screen.Cursor := crHourGlass;
  QCred.Close;
  QCred.Sql.Clear;
  QCred.Sql.Add(' Select distinct credenciados.Cred_id, nome, fantasia, comissao,  ''N'' as marcado ');
  QCred.Sql.Add(' from credenciados join contacorrente on (contacorrente.cred_id = credenciados.cred_id) ');
  if por_dia_fecha.Checked then
     QCred.SQL.Add(' join dia_fecha_cred on dia_fecha_cred.cred_id = credenciados.cred_id ');
  QCred.Sql.Add(' where apagado <> ''S'' ');
  if por_dia_fecha.Checked then // Por dia fecha
     QCred.SQL.Add(' and dia_fecha_cred.data_fecha = '+formatdataIB(dataini.Date))
  else if por_periodo.Checked then
     QCred.Sql.Add(' and contacorrente.data between '''+FormatDateTime('dd/mm/yyyy',dataini.Date)+''' and '''+FormatDateTime('dd/mm/yyyy',datafin.Date)+'''')
  else if por_fecha_emp.Checked then begin
     if empresas = '' then begin
        MsgErro('Nenhuma empresa foi selecionada.');
        Screen.Cursor := crDefault;
        Exit;
     end;
     QCred.Sql.Add(' and contacorrente.data_fecha_emp in ('+GetFechamentosSel+')');
  end;
  case RGBaixaEmp.ItemIndex of
     1 : QCred.Sql.Add(' and contacorrente.baixa_conveniado = ''S'' ');
     2 : QCred.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') = ''N'' ');
  end;
  case RGBaixaFor.ItemIndex of
     1 : QCred.Sql.Add(' and contacorrente.baixa_credenciado = ''S'' ');
     2 : QCred.Sql.Add(' and coalesce(contacorrente.baixa_credenciado,''N'') = ''N'' ');
  end;
  QCred.Sql.Add(' order by fantasia ');
  QCred.Open;
  if QCred.IsEmpty then
    begin
      MsgInf('Nenhum fornecedor com lan�amentos neste per�odo.');
      MCred.EmptyTable;
    end
  else begin
     QCred.First;
     MCred.Open;
     MCred.EmptyTable;
     MCred.DisableControls;
     while not QCred.Eof do begin
        MCred.Append;
        MCredcred_id.AsInteger := QCredcred_id.AsInteger;
        MCrednome.AsString := QCrednome.AsString;
        MCredfantasia.AsString := QCredfantasia.AsString;
        MCredcomissao.AsFloat := QCredcomissao.AsFloat;
        MCredMarcado.AsBoolean := False;
        MCred.Post;
        QCred.Next;
     end;
     MCred.First;
     MCred.EnableControls;
     fornec_sel := EmptyStr;

     DBGrid1.SetFocus;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFRelExtratoFor.DBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
   inherited;
    try
    if Pos(Field.FieldName,QCred.Sort) > 0 then begin
       if Pos(' DESC',QCred.Sort) > 0 then QCred.Sort := Field.FieldName
                                                  else QCred.Sort := Field.FieldName+' DESC';
    end
    else QCred.Sort := Field.FieldName;
    except
    end;

    QCred.First;
    MCred.Open;
    MCred.EmptyTable;
    MCred.DisableControls;
    while not QCred.Eof do begin
        MCred.Append;
        MCredcred_id.AsInteger := QCredcred_id.AsInteger;
        MCrednome.AsString := QCrednome.AsString;
        MCredfantasia.AsString := QCredfantasia.AsString;
        MCredcomissao.AsFloat := QCredcomissao.AsFloat;
        MCredMarcado.AsBoolean := False;
        MCred.Post;
        QCred.Next;
    end;
    MCred.First;
    MCred.EnableControls;
    fornec_sel := EmptyStr;

    DBGrid1.SetFocus;

end;

procedure TFRelExtratoFor.por_dia_fechaClick(Sender: TObject);
begin
  dataini.Visible       := True;
  dataini.SetFocus;
  datafin.Visible       := False;
  label3.Visible        := True;
  label3.Caption        := 'Dia de Fechamento';
end;

procedure TFRelExtratoFor.por_periodoClick(Sender: TObject);
begin
  inherited;
  datafin.Visible       := True;
  dataini.Visible       := True;
  dataini.SetFocus;
  label3.Visible        := True;
  label3.Caption        := 'Per�odo de Compras';
  dataini.SetFocus;
end;

procedure TFRelExtratoFor.datainiChange(Sender: TObject);
begin
 datafin.Date := IncMonth(dataini.Date)-1;
end;

procedure TFRelExtratoFor.DescFornecClick(Sender: TObject);
begin
  inherited;
if DescFornec.Checked then begin
   Label1.Visible           := True;
   DataDescinicial.Visible  := True;
   DataDescfinal.Visible    := True;
end
else begin
   Label1.Visible           := False;
   DataDescinicial.Visible  := False;
   DataDescfinal.Visible    := False;
end;
end;

procedure TFRelExtratoFor.ButListaConvClick(Sender: TObject);
begin
  inherited;
  QConveniados.Close;
  if (empresas = EmptyStr) or MEmpresa.IsEmpty then
     MsgInf('Nenhuma empresa selecionada')
  else begin
     Screen.Cursor := crHourGlass;
     conveniados := EmptyStr;
     QConveniados.SQL.Clear;
     QConveniados.SQL.Add(' Select conveniados.conv_id, conveniados.chapa, conveniados.titular, ');
     QConveniados.SQL.Add(' ''N'' as marcado, empresas.empres_id, empresas.nome empresa ');
     QConveniados.SQL.Add(' from conveniados join empresas on empresas.empres_id = conveniados.empres_id ');
     QConveniados.SQL.Add(' where conveniados.apagado <> ''S'' ');
     QConveniados.SQL.Add(' and conveniados.empres_id in ('+empresas+')');
     QConveniados.Open;
     Screen.Cursor := crDefault;
     MsgInf('Aten��o, ser�o mostrados no relat�rio apenas os valores dos conveniados selecionados.');

     MConveniado.Open;
     MConveniado.EmptyTable;
     MConveniado.DisableControls;
     while not QConveniados.Eof do begin
       MConveniado.Append;
       MConveniadoconv_id.AsInteger := QConveniadosconv_id.AsInteger;
       MConveniadochapa.AsInteger := QConveniadoschapa.AsInteger;
       MConveniadotitular.AsString := QConveniadostitular.AsString;
       MConveniadoempres_id.AsInteger := QConveniadosempres_id.AsInteger;
       MConveniadoempresa.AsString := QConveniadosempresa.AsString;
       MConveniadomarcado.AsBoolean := False;
       MConveniado.Post;
       QConveniados.Next;
     end;
     MConveniado.First;
     MConveniado.EnableControls;
     conv_sel := EmptyStr;


     DBGrid3.SetFocus;
  end;
end;

procedure TFRelExtratoFor.ButMarcaDesmConvClick(Sender: TObject);
begin
if MConveniado.IsEmpty then Exit;
   MConveniado.Edit;
   MConveniadoMarcado.AsBoolean := not MConveniadoMarcado.AsBoolean;
   MConveniado.Post;
   ConveniadosSel;
   DBGrid3.SetFocus;
end;

procedure TFRelExtratoFor.ButMarcaTodosConvClick(Sender: TObject);
var marca : TBookmark;
begin
  if MConveniado.IsEmpty then Exit;
  MConveniado.DisableControls;
  marca := MConveniado.GetBookmark;
  MConveniado.First;
  while not MConveniado.eof do begin
    MConveniado.Edit;
    MConveniadoMarcado.AsBoolean := true;
    MConveniado.Post;
    MConveniado.Next;
  end;
  MConveniado.GotoBookmark(marca);
  MConveniado.FreeBookmark(marca);
  MConveniado.EnableControls;
  ConveniadosSel;
  //inherited;
  //DMConexao.MarcaDesmTodos(QConveniados,True);

end;

procedure TFRelExtratoFor.ButDesmTodosConvClick(Sender: TObject);
var marca : TBookmark;
begin
  if MConveniado.IsEmpty then Exit;
  MConveniado.DisableControls;
  marca := MConveniado.GetBookmark;
  MConveniado.First;
  while not MConveniado.eof do begin
    MConveniado.Edit;
    MConveniadoMarcado.AsBoolean := false;
    MConveniado.Post;
    MConveniado.Next;
  end;
  MConveniado.GotoBookmark(marca);
  MConveniado.FreeBookmark(marca);
  MConveniado.EnableControls;
  ConveniadosSel;
  //inherited;
  //DMConexao.MarcaDesmTodos(QConveniados,False);

end;

procedure TFRelExtratoFor.DBGrid2DblClick(Sender: TObject);
begin
  inherited;
  ButMarcaDesmEmp.Click;
end;

procedure TFRelExtratoFor.DBGrid3DblClick(Sender: TObject);
begin
  inherited;
  ButMarcaDesmConv.Click;
end;

procedure TFRelExtratoFor.DBGrid3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_return then
     ButMarcaDesmConv.Click;
end;

procedure TFRelExtratoFor.DBGrid3TitleClick(Column: TColumn);
begin
  inherited;
  QConveniados.Sort := Column.FieldName;
end;

procedure TFRelExtratoFor.CktotempClick(Sender: TObject);
var
  iIndEmpresa: Integer;

begin
  inherited;
  with (sender as TCheckBox) do
  begin
    iIndEmpresa := ComboOrdem.Items.IndexOf('EMPRESA');
    if Checked then
    begin
      if iIndEmpresa <> -1 then
      begin
        if iIndEmpresa = ComboOrdem.ItemIndex then
          ComboOrdem.ItemIndex := ComboOrdem.Items.IndexOf('DATA');
        ComboOrdem.Items.Delete(iIndEmpresa);
      end;  
    end
    else
      if iIndEmpresa = -1 then
        ComboOrdem.Items.Add('EMPRESA');
  end;
end;

procedure TFRelExtratoFor.Button5Click(Sender: TObject);
begin
  QConveniados.Close;
  MConveniado.Close;
  conveniados := EmptyStr;
  MsgInf('Aten��o, ser�o mostrados no relat�rio os valores de todos os conveniados das empresas selecionadas.');
end;

procedure TFRelExtratoFor.por_fecha_empClick(Sender: TObject);
begin
  inherited;
  Label3.Visible  := False;
  dataini.Visible := False;
  datafin.Visible := False;
  TabSheet2.Show;
end;

procedure TFRelExtratoFor.TabSheet2Show(Sender: TObject);
begin
  inherited;
  PanFechaEmp.Visible := por_fecha_emp.Checked;
  if PanFechaEmp.Visible then
     DataFechaIni.SetFocus
  else
     Button2.SetFocus;
end;

procedure TFRelExtratoFor.DBGrid2TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
    inherited;
    try
    if Pos(Field.FieldName,QEmpresa.Sort) > 0 then begin
       if Pos(' DESC',QEmpresa.Sort) > 0 then QEmpresa.Sort := Field.FieldName
                                                  else QEmpresa.Sort := Field.FieldName+' DESC';
    end
    else QEmpresa.Sort := Field.FieldName;
    except
    end;

    QEmpresa.First;
    MEmpresa.Open;
    MEmpresa.EmptyTable;
    MEmpresa.DisableControls;
    while not QEmpresa.Eof do begin
        MEmpresa.Append;
        MEmpresaempres_id.AsInteger := QEmpresaempres_id.AsInteger;
        MEmpresanome.AsString := QEmpresanome.AsString;
        MEmpresadata_fecha.AsDateTime := QEmpresadata_fecha.AsDateTime;
        MEmpresaMarcado.AsBoolean := False;
        MEmpresa.Post;
        QEmpresa.Next;
    end;
    MEmpresa.First;
    MEmpresa.EnableControls;
    empres_sel := EmptyStr;
    DBGrid2.SetFocus;
end;

function TFRelExtratoFor.GetEmpresFechamento: String;
var S : String;
begin
   QEmpresa.First;
   while not QEmpresa.Eof do begin
      if QEmpresaMARCADO.AsString = 'S' then
         S := S+cartao_util.iif(S <> '',' or ','')+' (conveniados.empres_id = '+ QEmpresaEMPRES_ID.AsString+' and contacorrente.data_fecha_emp = '+formatdataIB(QEmpresa.FieldByName('data_fecha').AsDateTime)+')';
      QEmpresa.Next;
   end;
   Result := S;
end;


function TFRelExtratoFor.GetFechamentosSel: String;
var S, Data : String;
begin
   QEmpresa.First;
   while not QEmpresa.Eof do begin
      if QEmpresaMARCADO.AsString = 'S' then begin
         Data := formatdataIB(QEmpresa.FieldByName('data_fecha').AsDateTime);
         if Pos(Data,S) = 0 then
         S := S+','+Data;
      end;
      QEmpresa.Next;
   end;
   Delete(S,1,1);
   Result := S;
end;

procedure TFRelExtratoFor.RGModeloClick(Sender: TObject);
begin
  inherited;
  Cktotemp.Enabled   := RGModelo.ItemIndex = 0;
  CkMostraNF.Enabled := RGModelo.ItemIndex = 0;
end;

procedure TFRelExtratoFor.ImprimirModeloConferencia;
var imp : TImpres;  Autor : TAutorizacao;//usada s� para acumudo de valores.
    TotDeb, TotCred, Taxa, TotTaxa : Currency; NumFornecs : Integer;
begin
   if DMConexao.ContaMarcados(MCred) = 0 then begin
      MsgInf('Nenhum Fornecedor Selecionado');
      DBGrid1.SetFocus;
   end
   else begin
      Autor.Debito := 0;
      Autor.Credito := 0;
      TotDeb := 0; TotCred := 0; TotTaxa := 0; NumFornecs := 0;
      imp := TImpres.Create;
      imp.quebrapagina := not CkUmPorPag.Checked;
      imp.cabecaemtodas := True;
      imp.MostrarNumPaginas := True;
      CriarTituloModConf(Imp);
      QCred.First;
      while not QCred.Eof do begin
         if (MCredMARCADO.AsBoolean = true) and AbrirQuery then begin
            if (CkUmPorPag.Checked and (NumFornecs > 0)) then
               Imp.novapagina(True);
            Inc(NumFornecs);
            Imp.AddLinhaSeparadora('=');
            Imp.AddLinha('Fornec: '+PadL(MCredCRED_ID.AsString,6,'0')+' Razao: '+MCredNOME.AsString+' Fant.: '+QCredFANTASIA.AsString);
            Autor := CriarCorpoModConf(Imp);
            if CkLiquido.Checked then Begin
               Taxa  := ArredondaDin(((Autor.Debito-Autor.Credito)/100)*MCredCOMISSAO.AsFloat);
               TotTaxa := TotTaxa + Taxa;
            end;
            TotalizarForn(Imp,Autor.Debito,Autor.Credito,Taxa);
            imp.AddLinha(' ');
            TotDeb  := TotDeb + Autor.Debito;
            TotCred := TotCred + Autor.Credito;
         end;
         QCred.Next;
      end;
      if CkUmPorPag.Checked then
         Imp.novapagina(True);
      Imp.AddLinhaSeparadora('=');
      Imp.AddLinha('Totalizacao do Relatorio - Debito: '+FormatDinBR(TotDeb)+' - Credito: '+FormatDinBR(TotCred)+ ' - Total: '+FormatDinBR(totdeb-TotCred));
      if CkLiquido.Checked then begin
         Imp.AddLinha('Totalizacao Liquida - Valor Total Taxa: '+FormatDinBR(TotTaxa)+' - Total Liquido: '+FormatDinBR(totdeb-TotCred-TotTaxa));
      end;
      Imp.AddLinha(IntToStr(NumFornecs)+ ' Fornecedor(es) Listado(s) ');
      Imp.AddLinhaSeparadora();
      imp.Imprimir();
      imp.Free;
   end;
end;

procedure TFRelExtratoFor.ImprimirModeloConferencia2;
var imp : TImpres;  Autor : TAutorizacao;//usada s� para acumudo de valores.
    TotDeb, TotCred, Taxa, TotTaxa : Currency; NumFornecs : Integer;
    bm : TBookmark;
begin
  if DMConexao.ContaMarcados(QCred) = 0 then begin
    MsgInf('Nenhum Fornecedor Selecionado');
    DBGrid1.SetFocus;
  end else begin
    AbrirQuery;
    QCred.DisableControls;
    bm := QCred.GetBookmark;
    frxAgrupadoEmpresa.ShowReport;
    QCred.Open;
    QCred.First;
    while not QCred.Eof do begin
      if Marcados[QCred.RecNo-1] = 'S' then
        DMConexao.MarcaDesm(QCred);
      QCred.Next;
    end;
    QCred.GotoBookmark(bm);
    QCred.EnableControls;
  end;
end;

function TFRelExtratoFor.CriarCorpoModConf(Imp:TImpres):TAutorizacao;
var empres_id : integer; totdeb, totcred  : Currency;
    Autor : TAutorizacao; //usada s� para acumudo de valores.
begin
   totdeb := 0; totcred := 0;
   Result.Debito := 0;
   Result.Credito := 0;
   empres_id := 0;
   Query1.First;
   while not Query1.eof do begin
      if empres_id <> Query1EMPRES_ID.AsInteger then begin
         if (totdeb > 0) or (totcred > 0) then begin
            TotalizarEmp(Imp,totdeb,totcred);
            Result.Debito  := Result.Debito + totdeb;
            Result.Credito := Result.Credito + totcred;
            totdeb := 0; totcred := 0;
         end;
         empres_id := Query1EMPRES_ID.AsInteger;
         Imp.AddLinhaSeparadora();
         Imp.AddLinha('Empresa: '+Query1EMPRES_ID.AsString+' - '+Query1EMPRESA.AsString);
         CriarTitulosColunas(Imp);
      end;
      Autor := CriarLinhaDetalhe(Imp);
      totdeb := totdeb + Autor.Debito;
      totcred := totcred + Autor.Credito;
      Query1.Next;
   end;
   if (totdeb > 0) or (totcred > 0) then begin
      TotalizarEmp(Imp,totdeb,totcred);
      Result.Debito  := Result.Debito + totdeb;
      Result.Credito := Result.Credito + totcred;
      totdeb := 0; totcred := 0;
   end;
end;

procedure TFRelExtratoFor.TotalizarEmp(Imp:Timpres;Debito,Credido:Currency);
begin
   Imp.AddLinha('Totalizacao da Empresa - Debito: '+FormatDinBR(Debito)+' - Credito: '+FormatDinBR(Credido)+' - Total: '+FormatDinBR(Debito-Credido) );
end;

procedure TFRelExtratoFor.TotalizarForn(Imp:Timpres;Debito,Credido,ValorComiss:Currency);
var totliqui, totdesc : Currency;
begin
   Imp.AddLinhaSeparadora();
   totliqui := Debito-Credido;
   Imp.AddLinha('Totalizacao da Fornecedor - Debito: '+FormatDinBR(Debito)+' - Credito: '+FormatDinBR(Credido)+' - Total: '+FormatDinBR(totliqui) );
   if CkLiquido.Checked then begin
      totliqui  := (Debito-Credido)-ValorComiss;
      Imp.AddLinha('Totalizacao Liquida - Taxa Adm: '+QCredCOMISSAO.DisplayText+' Valor Taxa: '+FormatDinBR(ValorComiss)+' - Total Liquido: '+FormatDinBR(totliqui));
   end;
   if DescFornec.Checked then begin
      totdesc := CriarDescontosFor(Imp);
      Imp.AddLinha('Total do Fornecedor menos Descontos : '+FormatDinBR(totliqui-totdesc));
   end;
end;


procedure TFRelExtratoFor.CriarTitulosColunas(Imp:TImpres);
var l : integer;
begin
   l := Imp.AddLinha('Data');
   //Imp.AddLinha('Autorizacao',14,l);
   Imp.AddLinha('Autorizacao',19,l);
   Imp.AddLinha('N.  F.',26,l);
   Imp.AddLinha('Titular',33,l);
   Imp.AddLinha('Chapa',64,l);
   Imp.AddLinha('Debito',80,l);
   Imp.AddLinha('Credito',89,l);
   Imp.AddLinha('Con',98,l);
   Imp.AddLinha('Pag',102,l);
   Imp.AddLinha('Rec',106,l);
   Imp.AddLinha('Fat',110,l);
   Imp.AddLinha('Fat ID',114,l);
   Imp.AddLinha('B.C ',121,l);
   Imp.AddLinha('B.F ',125,l);
end;

function TFRelExtratoFor.CriarLinhaDetalhe(Imp:TImpres):TAutorizacao;
var l : integer;
begin
   l := Imp.AddLinha(Query1DATA.DisplayText );
   Imp.AddLinha(Imp.Direita(Query1AUTORIZACAO_ID.AsString+Query1DIGITO.DisplayText ,11),14,l);
   Imp.AddLinha(Imp.Direita(Copy(Query1NF.AsString,1,6),6),26,l);
   Imp.AddLinha(Copy(Query1TITULAR.AsString,1,30),33,l);
   Imp.AddLinha(Query1CHAPA.AsString,64,l);
   Imp.AddLinha(Imp.Direita(Query1DEBITO.DisplayText,6),80,l);
   Imp.AddLinha(Imp.Direita(Query1CREDITO.DisplayText,7),89,l);
   Imp.AddLinha(ChangeSNtoSimNao(Query1ENTREG_NF.AsString),98,l);
   Imp.AddLinha(ChangeSNtoSimNao(Query1PAGO.AsString),102,l);
   Imp.AddLinha(ChangeSNtoSimNao(Query1RECEITA.AsString),106,l);
   Imp.AddLinha(cartao_util.iif(Query1FATURA_ID.AsInteger > 0,'Sim','Nao'), 110,l);
   Imp.AddLinha(Imp.Direita(Query1FATURA_ID.AsString,6),114,l);
   Imp.AddLinha(ChangeSNtoSimNao(Query1BAIXA_CONVENIADO.AsString), 121,l);
   Imp.AddLinha(ChangeSNtoSimNao(Query1BAIXA_CREDENCIADO.AsString), 125,l);
   Result.Credito := Query1CREDITO.AsCurrency;
   Result.Debito  := Query1DEBITO.AsCurrency;
end;


procedure TFRelExtratoFor.CriarTituloModConf(Imp:TImpres);
var Texto : String;
begin
   Imp.AddCabecalho(DMConexao.AdmFANTASIA.AsString);
   if por_dia_fecha.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes com fechamento do Fornecedor em ' + FormatDataBR(dataini.Date)))
   else if por_periodo.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes no periodo de '+FormatDataBR(dataini.Date)+' a '+FormatDataBR(datafin.Date)))
   else if por_fecha_emp.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes da empresas selecionadas com fechamento entre '+FormatDataBR(dataini.Date)+' e '+FormatDataBR(datafin.Date)));
   case RGReceita.ItemIndex of
      1 : Texto := ' com receita';
      2 : Texto := ' sem receita';
   end;
   case rgpNFEntregue.ItemIndex of
      1 : Texto := Texto + ' e confirmadas';
      2 : Texto := Texto + ' e nao confirmadas';
   end;
   case RGFaturadas.ItemIndex of
      1 : Texto := Texto + ' e faturadas';
      2 : Texto := Texto + ' e nao faturadas';
   end;
   case RGPagas.ItemIndex of
      1 : Texto := Texto + ' e pagas';
      2 : Texto := Texto + ' e nao pagas';
   end;
   case RGBaixaEmp.ItemIndex of
      1 : Texto := Texto + ' e baixadas empresa';
      2 : Texto := Texto + ' e nao baixadas empresa';
   end;
   case RGBaixaFor.ItemIndex of
      1 : Texto := Texto + ' e baixadas fornecedor';
      2 : Texto := Texto + ' e nao baixadas fornecedor';
   end;
   Texto := Trim(Texto);
   if (Texto <> EmptyStr) then begin
      if (Texto[1] = 'e') then
         Delete(Texto,1,1);
      Texto := 'Somente autorizacoes '+Texto;
      Imp.AddCabecalho(Imp.Centraliza(Texto))
   end;
   Imp.AddCabecalho(' ');
end;

function TFRelExtratoFor.AbrirQuery:Boolean;
var    bOrdemDefinida : Boolean;
begin
   bOrdemDefinida := False;
   Query1.Close;
   Query1.Sql.Clear;
   Query1.Sql.Add(' select coalesce(cred_emp_lib.comissao,credenciados.comissao) as perc_comiss, ');
   Query1.Sql.Add(' contacorrente.debito, contacorrente.credito, contacorrente.autorizacao_id, ');
   Query1.Sql.Add(' contacorrente.digito, contacorrente.nf, coalesce(contacorrente.entreg_nf,''N'') as entreg_nf, coalesce(contacorrente.receita,''N'') as receita, ');
   Query1.Sql.Add(' conveniados.titular, conveniados.chapa, conveniados.conv_id, contacorrente.data, ');
   Query1.Sql.Add(' coalesce(contacorrente.baixa_conveniado,''N'') as baixa_conveniado, coalesce(contacorrente.baixa_credenciado,''N'') as baixa_credenciado, contacorrente.fatura_id, ');
   Query1.Sql.Add(' empresas.empres_id, empresas.nome as empresa, ');
   Query1.Sql.Add('(case when coalesce(contacorrente.PAGAMENTO_CRED_ID,0) = 0  then ''N'' else ''S'' end) as pago ');
   Query1.Sql.Add(' from contacorrente ');
   Query1.Sql.Add(' join conveniados on conveniados.conv_id = contacorrente.conv_id ');
   Query1.Sql.Add(' join empresas on empresas.empres_id = conveniados.empres_id ');
   Query1.Sql.Add(' join credenciados on credenciados.cred_id = contacorrente.cred_id ');
   Query1.Sql.Add(' left join cred_emp_lib on cred_emp_lib.cred_id = contacorrente.cred_id and cred_emp_lib.empres_id = conveniados.empres_id ');
   //Query1.Sql.Add(' where contacorrente.cred_id = '+QCredCRED_ID.AsString);
   Query1.Sql.Add(' where contacorrente.cred_id = :CRED_ID');
   if (RGModelo.ItemIndex = 1) then
     Query1.Sql.Add(' and conveniados.empres_id = :EMPRES_ID');
   if not (cbMostrarCanceladas.Checked) then
     Query1.SQL.Add(' and coalesce(contacorrente.cancelada,''N'')=''N'' and coalesce(contacorrente.credito,0) = 0 ');  
   if por_dia_fecha.Checked then
      Query1.SQL.Add(' and (contacorrente.data_fecha_for = ' + FormatDateIB(dataini.Date)+')' )
   else if por_periodo.Checked then
      Query1.Sql.Add(' and (contacorrente.data between '+FormatDateIB(DataIni.Date)+' and '+FormatDateIB(datafin.Date)+')')
   else if por_fecha_emp.Checked then
      Query1.Sql.Add(' and ( '+GetEmpresFechamento+' )');

   if conveniados <> EmptyStr then
      Query1.Sql.Add(' and (conveniados.conv_id in ('+conveniados+'))')
   else if ((empresas <> EmptyStr) and (not por_fecha_emp.Checked)) then
      Query1.Sql.Add(' and (conveniados.empres_id in ('+empresas+'))');

   case RGBaixaEmp.ItemIndex of
      1 : Query1.Sql.Add(' and contacorrente.baixa_conveniado = ''S'' ');
      2 : Query1.Sql.Add(' and coalesce(contacorrente.baixa_conveniado,''N'') = ''N'' ');
   end;
   case RGBaixaFor.ItemIndex of
      1 : Query1.Sql.Add(' and contacorrente.baixa_credenciado = ''S'' ');
      2 : Query1.Sql.Add(' and coalesce(contacorrente.baixa_credenciado,''N'') = ''N'' ');
   end;
   case RGReceita.ItemIndex of
      1 : Query1.Sql.Add(' and contacorrente.receita = ''S'' ');
      2 : Query1.Sql.Add(' and coalesce(contacorrente.receita,''N'') = ''N'' ');
   end;
   case RGFaturadas.ItemIndex of
      1 : Query1.Sql.Add(' and coalesce(contacorrente.fatura_id,0) > 0 ');
      2 : Query1.Sql.Add(' and coalesce(contacorrente.fatura_id,0) = 0 ');
   end;
   case RGPagas.ItemIndex of
      1 : Query1.Sql.Add(' and coalesce(contacorrente.PAGAMENTO_CRED_ID,0) > 0 ');
      2 : Query1.Sql.Add(' and coalesce(contacorrente.PAGAMENTO_CRED_ID,0) = 0 ');
   end;
   case rgpNFEntregue.ItemIndex of
      1 : Query1.SQL.Add(' and contacorrente.entreg_nf = ''S'' ');
      2 : Query1.SQL.Add(' and coalesce(contacorrente.entreg_nf,''N'') = ''N'' ');
   end;

   if Cktotemp.Checked or (RGModelo.ItemIndex = 1) then
      Query1.SQL.Add(' order by empresas.nome, ' + ComboOrdem.Text)
   else begin
      if ComboOrdem.Text = 'EMPRESA' then
         Query1.Sql.Add(' order by empresas.nome, contacorrente.data')
      else begin
         if ComboOrdem.Text = 'DATA' then
            Query1.SQL.Add(' order by contacorrente.data')
         else begin
            bOrdemDefinida := True;
            if ordemdec.Checked then
               Query1.Sql.Add(' order by ' + ComboOrdem.Text + ' desc, contacorrente.data asc')
            else
               Query1.Sql.Add(' order by ' + ComboOrdem.Text + ', contacorrente.data');
         end;
      end;
   end;
   if ordemdec.Checked and (not bOrdemDefinida) then
      Query1.Sql.Add(' desc ');
   Query1.Parameters.Items[0].Value := QCredCRED_ID.Value;
   Query1.SQL.Text;
   Query1.Open;
   Result := not Query1.isEmpty;
end;

function TFRelExtratoFor.AbrirDescontosFornec:Boolean;
begin
   QDesconto.Close;
   QDesconto.SQL.Clear;
   QDesconto.SQL.Add(' Select desconto.data, desconto.historico, desconto.valor from desconto ');
   QDesconto.SQL.Add(' where apagado <> ''S'' and cred_id = '+QCredCRED_ID.AsString);
   QDesconto.SQL.Add(' and (data between '+QuotedStr(FormatDateTime('mm/dd/yyyy',DataDescinicial.Date))+' and '+QuotedStr(FormatDateTime('mm/dd/yyyy',DataDescfinal.Date))+ ')');
   QDesconto.SQL.Add(' order by data');
   QDesconto.Open;
   Result := not QDesconto.isEmpty;
end;

function TFRelExtratoFor.CriarDescontosFor(Imp:Timpres):currency;
var totdesc : Currency; l : integer;
begin
   totdesc := 0;
   if AbrirDescontosFornec then begin
      Imp.AddLinha('Descontos do Fornecedor');
      l := Imp.AddLinha('Data',3);
      Imp.AddLinha('Desconto',11,l);
      Imp.AddLinha('Historico',21,l);
      while not QDesconto.Eof do begin
         l := Imp.AddLinha(FormatDataBR(QDescontoDATA.AsDateTime));
         Imp.AddLinha(Imp.Direita(QDescontoVALOR.DisplayText,8),11,l);
         Imp.AddLinha(QDescontoHISTORICO.AsString,21,l);
         totdesc := totdesc + QDescontoVALOR.AsCurrency;
         QDesconto.Next;
      end;
   end;
   Result := Totdesc;
   Imp.AddLinha('Total dos Descontos: '+FormatDinBR(totdesc));
end;

function TFRelExtratoFor.CriarCorpoModList(Imp: TImpres): TValores;
var empres_id: Integer;
  totdeb, totcred: Currency;  Autor: TAutorizacao; Totais: TValores;
  TotForConf, TotForNConf, LiqForConf, LiqForNConf: Currency;
begin
  TotForConf := 0; TotForNConf := 0;
  LiqForConf := 0; LiqForNConf := 0;
  Result.VlrBrutConf := 0;
  Result.VlrBrutNConf := 0;
  Result.VlrLiquConf := 0;
  Result.VlrLiquNConf := 0;
  empres_id := 0;
  Query1.First;
  while not Query1.eof do
  begin
    if empres_id <> Query1EMPRES_ID.AsInteger then
    begin
      if (TotForConf > 0) or (TotForNConf > 0) or (LiqForConf > 0) or (LiqForNConf > 0) then
      begin
        if Cktotemp.Checked then
        begin
          TotalizarEmpModList(Imp, TotForConf, TotForNConf, LiqForConf, LiqForNConf);
        end;
        Result.VlrBrutConf:= Result.VlrBrutConf + TotForConf;
        Result.VlrBrutNConf:= Result.VlrBrutNConf + TotForNConf;
        Result.VlrLiquConf:= Result.VlrLiquConf + LiqForConf;
        Result.VlrLiquNConf:= Result.VlrLiquNConf + LiqForNConf;
        TotForConf:= 0; TotForNConf:= 0; LiqForConf:= 0; LiqForNConf:= 0;
      end;
      if ((Cktotemp.Checked) or (empres_id = 0)) then
      begin
        Imp.AddLinhaSeparadora();
        CriarTitulosColunasModList(Imp);
      end;
    end;
    Totais := CriarLinhaDetalheModList(Imp);
    TotForConf:= TotForConf + Totais.VlrBrutConf;
    TotForNConf:= TotForNConf + Totais.VlrBrutNConf;
    LiqForConf:= LiqForConf + Totais.VlrLiquConf;
    LiqForNConf:= LiqForNConf + Totais.VlrLiquNConf;
    empres_id:= Query1EMPRES_ID.AsInteger;
    Query1.Next;
  end;
  if (TotForConf > 0) or (TotForNConf > 0) or (LiqForConf > 0) or (LiqForNConf > 0) then
  begin
    if Cktotemp.Checked then
    begin
      TotalizarEmpModList(Imp, TotForConf, TotForNConf, LiqForConf, LiqForNConf);
    end;
    Result.VlrBrutConf:= Result.VlrBrutConf + TotForConf;
    Result.VlrBrutNConf:= Result.VlrBrutNConf + TotForNConf;
    Result.VlrLiquConf:= Result.VlrLiquConf + LiqForConf;
    Result.VlrLiquNConf:= Result.VlrLiquNConf + LiqForNConf;
    TotForConf:= 0; TotForNConf:= 0; LiqForConf:= 0; LiqForNConf:= 0;
  end;
end;

function TFRelExtratoFor.CriarLinhaDetalheModList(Imp: TImpres): TValores;
var
  l: Integer; vlr_comiss: Currency;
  //Variaveis para as colunas de impress�o dos campos.
  col_data, col_autor, col_nf, col_titu, col_chapa, col_emp, col_bruto, col_liqui, col_entreg_nf, col_taxa : Integer;
  tam_titu, tam_emp : Integer;
begin
  col_data  := 3;
  col_autor := 14;
  col_bruto := 106;
  if not CkMostraNF.Checked then begin
    col_nf    := 0;
    col_titu  := 25;
    col_chapa := 70;
    col_emp   := 85;
  end else begin
    col_nf    := 25;
    col_titu  := 32;
    col_chapa := 67;
    col_emp   := 82;
  end;
  if ((filtrarEntrNF) and (rgpNFEntregue.ItemIndex = 0)) then begin
    if CkLiquido.Checked then begin
      col_liqui     := 114;
      col_taxa      := 122;
      col_entreg_nf := 127;
    end else begin
      col_liqui     := 0;
      col_taxa      := 0;
      col_entreg_nf := 114;
    end;
  end else begin
    if CkLiquido.Checked then begin
      col_liqui     := 114;
      col_taxa      := 122;
      col_entreg_nf := 0;
    end else begin
      col_liqui     := 0;
      col_taxa      := 0;
      col_entreg_nf := 0;
    end;
  end;
  tam_titu   := (col_chapa - col_titu) - 1;
  tam_emp    := (col_bruto - col_emp)  - 2;

  l:= Imp.AddLinha(FormatDataBR(Query1DATA.AsDateTime),col_data);
  Imp.AddLinha(Imp.Direita(Query1AUTORIZACAO_ID.AsString+FormatFloat('00',Query1DIGITO.AsFloat),10),col_autor,l);
  if CkMostraNF.Checked then begin
    Imp.AddLinha(FormatFloat('000000',Query1NF.AsFloat),col_nf,l);
  end;
  Imp.AddLinha(Copy(Query1TITULAR.AsString,0,tam_titu),col_titu,l);
  Imp.AddLinha(Query1CHAPA.AsString,col_chapa,l);
  Imp.AddLinha(Copy(Query1EMPRESA.AsString,0,tam_emp),col_emp,l);
  Imp.AddLinha(Imp.Direita(FormatDinBR(Query1DEBITO.AsCurrency-Query1CREDITO.AsCurrency),8),col_bruto - 3,l);
  vlr_comiss := ((Query1DEBITO.AsCurrency-Query1CREDITO.AsCurrency)/100)*Query1PERC_COMISS.AsFloat;
  if CkLiquido.Checked then begin
    Imp.AddLinha(Imp.Direita(FormatDinBR((Query1DEBITO.AsCurrency-Query1CREDITO.AsCurrency) - vlr_comiss),8),col_liqui - 1,l);
    Imp.AddLinha(Imp.Direita(FormatDinBR(Query1PERC_COMISS.AsCurrency),4),col_taxa,l);
  end;
  if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then begin
    Imp.AddLinha(Query1ENTREG_NF.AsString,col_entreg_nf + 1,l);
  end;
  if Query1ENTREG_NF.AsString = 'S' then begin
    Result.VlrBrutConf := Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency;
    Result.VlrLiquConf := ((Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency) - (Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency) / 100 * Query1PERC_COMISS.AsCurrency);
    Result.VlrBrutNConf := 0;
    Result.VlrLiquNConf := 0;
  end else begin
    Result.VlrBrutNConf := Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency;
    Result.VlrLiquNConf := ((Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency) - (Query1DEBITO.AsCurrency - Query1CREDITO.AsCurrency) / 100 * Query1PERC_COMISS.AsCurrency);
    Result.VlrBrutConf := 0;
    Result.VlrLiquConf := 0;
  end;
end;

procedure TFRelExtratoFor.CriarTituloModList(Imp: TImpres);
var Texto : String;
begin
   Imp.AddCabecalho(DMConexao.AdmFANTASIA.AsString);
   if por_dia_fecha.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes com fechamento do Fornecedor em ' + FormatDataBR(dataini.Date)))
   else if por_periodo.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes no periodo de '+FormatDataBR(dataini.Date)+' a '+FormatDataBR(datafin.Date)))
   else if por_fecha_emp.Checked then
      Imp.AddCabecalho(Imp.Centraliza('Autorizacoes da empresas selecionadas com fechamento entre '+FormatDataBR(dataini.Date)+' e '+FormatDataBR(datafin.Date)));
   case RGReceita.ItemIndex of
      1 : Texto := ' com receita';
      2 : Texto := ' sem receita';
   end;
   case rgpNFEntregue.ItemIndex of
      1 : Texto := Texto + ' e confirmadas';
      2 : Texto := Texto + ' e nao confirmadas';
   end;
   case RGFaturadas.ItemIndex of
      1 : Texto := Texto + ' e faturadas';
      2 : Texto := Texto + ' e nao faturadas';
   end;
   case RGPagas.ItemIndex of
      1 : Texto := Texto + ' e pagas';
      2 : Texto := Texto + ' e nao pagas';
   end;
   case RGBaixaEmp.ItemIndex of
      1 : Texto := Texto + ' e baixadas empresa';
      2 : Texto := Texto + ' e nao baixadas empresa';
   end;
   case RGBaixaFor.ItemIndex of
      1 : Texto := Texto + ' e baixadas fornecedor';
      2 : Texto := Texto + ' e nao baixadas fornecedor';
   end;
   Texto := Trim(Texto);
   if (Texto <> EmptyStr) then begin
      if (Texto[1] = 'e') then
         Delete(Texto,1,2);
      Texto := 'Somente autorizacoes '+Texto;
      Imp.AddCabecalho(Imp.Centraliza(Texto))
   end;
   Imp.AddCabecalho(' ');
end;

procedure TFRelExtratoFor.CriarTitulosColunasModList(Imp: TImpres);
var
  l: Integer;
  //Variaveis para as colunas de impress�o dos campos.
  col_data, col_autor, col_nf, col_titu, col_chapa, col_emp, col_bruto, col_liqui, col_entreg_nf, col_taxa : Integer;
  tam_titu, tam_emp : Integer;
begin
  col_data  := 3;
  col_autor := 14;
  col_bruto := 106;
  if not CkMostraNF.Checked then
  begin
    col_nf    := 0;
    col_titu  := 25;
    col_chapa := 70;
    col_emp   := 85;
  end
  else
  begin
    col_nf    := 25;
    col_titu  := 32;
    col_chapa := 67;
    col_emp   := 82;
  end;
  if ((filtrarEntrNF) and (rgpNFEntregue.ItemIndex = 0)) then
  begin
    if CkLiquido.Checked then
    begin
      col_liqui     := 114;
      col_taxa      := 122;
      col_entreg_nf := 127;
    end
    else
    begin
      col_liqui     := 0;
      col_taxa      := 0;
      col_entreg_nf := 114;
    end;
  end
  else
  begin
    if CkLiquido.Checked then
    begin
      col_liqui     := 114;
      col_taxa      := 122;
      col_entreg_nf := 0;
    end
    else
    begin
      col_liqui     := 0;
      col_taxa      := 0;
      col_entreg_nf := 0;
    end;
  end;
  tam_titu   := (col_chapa - col_titu) - 1;
  tam_emp    := (col_bruto - col_emp)  - 2;
  l:= Imp.AddLinha('Data',col_data);
  Imp.AddLinha('Autoriz.',col_autor,l);
  if CkMostraNF.Checked then
    Imp.AddLinha('Not.F.',col_nf,l);
  Imp.AddLinha('Titular',col_titu,l);
  Imp.AddLinha('Chapa',col_chapa,l);
  Imp.AddLinha('Empresa',col_emp,l);
  Imp.AddLinha('Bruto',col_bruto,l);
  if CkLiquido.Checked then
  begin
    Imp.AddLinha('Liquido',col_liqui,l);
    Imp.AddLinha('Taxa',col_taxa,l);
  end;
  if (filtrarEntrNF and (rgpNFEntregue.ItemIndex = 0)) then
  begin
    Imp.AddLinha('Ent',col_entreg_nf,l);
  end;
end;

procedure TFRelExtratoFor.ImprimirModeloListagem;
var Imp : TImpres;  Totais : TValores;
  Desconto, Taxa, TotTaxa, TotDeb, TotCred : Currency;
  NumFornecs, Linha : Integer;
begin
  if DMConexao.ContaMarcados(QCred) = 0 then
  begin
    MsgInf('Nenhum Fornecedor Selecionado');
    DBGrid1.SetFocus;
  end
  else
  begin
    Totais.VlrBrutConf := 0;
    Totais.VlrBrutNConf := 0;
    Totais.VlrLiquConf := 0;
    Totais.VlrLiquNConf := 0;
    TotDeb := 0; TotCred := 0; TotTaxa := 0; NumFornecs := 0;
    Imp := TImpres.Create;
    Imp.quebrapagina := CkUmPorPag.Checked;
    Imp.cabecaemtodas := True;
    Imp.MostrarNumPaginas := True;
    CriarTituloModList(Imp);
    QCred.First;
    while not QCred.Eof do
    begin
      if (MCredMARCADO.AsString = 'S') and AbrirQuery then
      begin
        if (CkUmPorPag.Checked and (NumFornecs > 0)) then
        begin
          Imp.novapagina(True);
        end
        else if NumFornecs > 0 then
        begin
          Imp.SaltarLinhas(2);
        end;
        Inc(NumFornecs);
        Imp.AddLinhaSeparadora('=');
        linha:= Imp.AddLinha('Fornec: '+PadL(QCredCRED_ID.AsString,6,'0')+' Razao: '+QCredNOME.AsString+' Fant.: '+QCredFANTASIA.AsString);
        if CkLiquido.Checked then
        begin
          Imp.AddLinha('Taxa: '+FormatFloat('##0.00 %',QCredCOMISSAO.AsFloat),118,Linha);
        end;
        Totais := CriarCorpoModList(Imp);
        TotalizarForModList(Imp,Totais.VlrBrutConf, Totais.VlrBrutNConf, Totais.VlrLiquConf, Totais.VlrLiquNConf);
        if DescFornec.Checked then
        begin
          Desconto:= CriarDescontosFor(Imp);
          Imp.AddLinha('Total do Fornecedor menos Descontos : '+FormatDinBR((Totais.VlrLiquConf+Totais.VlrLiquNConf)-Desconto));
        end;
      end;
      QCred.Next;
    end;
    Imp.Imprimir();
    Imp.Free;
  end;
end;

procedure TFRelExtratoFor.ImprimirModeloListagem2;
var bm : TBookmark;
begin
  if DMConexao.ContaMarcados(MCred) = 0 then begin
    MsgInf('Nenhum Fornecedor Selecionado');
    DBGrid1.SetFocus;
  end
  else begin
    AbrirQuery;
    MCred.DisableControls;
    bm := MCred.GetBookmark;
    frxListagem.ShowReport;
    QCred.SQL.Text;
    QCred.Open;
    QCred.First;

    MCred.Open;
    MCred.EmptyTable;
    MCred.DisableControls;
    while not QCred.Eof do begin
       MCred.Append;
       MCredcred_id.AsInteger := QCredcred_id.AsInteger;
       MCrednome.AsString := QCrednome.AsString;
       MCredfantasia.AsString := QCredfantasia.AsString;
       MCredcomissao.AsFloat := QCredcomissao.AsFloat;
       MCredMarcado.AsBoolean := False;
       MCred.Post;
       QCred.Next;
     end;
     MCred.First;
     MCred.EnableControls;
     fornec_sel := EmptyStr;

     while not QCred.Eof do begin
      if Marcados[QCred.RecNo-1] = 'S' then
        DMConexao.MarcaDesm(QCred);
      QCred.Next;
     end;
     MCred.GotoBookmark(bm);
     MCred.EnableControls;
  end;
end;

procedure TFRelExtratoFor.TotalizarEmpModList(Imp: TImpres; BrutConf, BrutNConf, LiqConf, LiqNConf: Currency);
var l: Integer;
begin
  Imp.AddLinhaSeparadora();
  l:= Imp.AddLinha('Totalizacao confirmadas:          ' + Imp.Direita(FormatDinBR(BrutConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqConf),10),111,l);
  l:= Imp.AddLinha('Totalizacao nao confirmadas:      ' + Imp.Direita(FormatDinBR(BrutNConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqNConf),10),111,l);
  l:= Imp.AddLinha('Totalizacao do Fornecedor:        ' + Imp.Direita(FormatDinBR(BrutConf + BrutNConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqConf + LiqNConf),10),111,l);
  Imp.SaltarLinhas(1);
end;

procedure TFRelExtratoFor.TotalizarForModList(Imp: TImpres; BrutConf, BrutNConf, LiqConf, LiqNConf: Currency);
var l: Integer;
begin
  Imp.AddLinhaSeparadora('=');
  l:= Imp.AddLinha('Totalizacao confirmadas:          ' + Imp.Direita(FormatDinBR(BrutConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqConf),10),111,l);
  l:= Imp.AddLinha('Totalizacao nao confirmadas:      ' + Imp.Direita(FormatDinBR(BrutNConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqNConf),10),111,l);
  l:= Imp.AddLinha('Totalizacao do Fornecedor:        ' + Imp.Direita(FormatDinBR(BrutConf + BrutNConf),10),67);
  if CkLiquido.Checked then Imp.AddLinha(Imp.Direita(FormatDinBR(LiqConf + LiqNConf),10),111,l);
  Imp.SaltarLinhas(1);
  l:= Imp.AddLinha('Atendimentos: '+IntToStr(Query1.RecordCount));
  Imp.AddLinha('Valor M�dio: ' + Imp.Direita(FormatDinBR((BrutConf+BrutNConf) / Query1.RecordCount),10),32,l);
  if CkLiquido.Checked then
  begin
    Imp.AddLinha('Taxa de Administra��o:            '+
      Imp.Direita(FormatDinBR((BrutConf+BrutNConf)-(LiqConf+LiqNConf)),10),67,l);
  end;
  Imp.AddLinhaSeparadora();
end;

procedure TFRelExtratoFor.BitBtn2Click(Sender: TObject);
begin
  inherited;
  if ((not QCred.Active) or (DMConexao.ContaMarcados(MCred) = 0)) then begin
     MsgInf('Nenhum fornecedor selecionado.');
     Exit;
  end;
  if Marcados = nil then
    Marcados := TStringList.Create
  else
    Marcados.Clear;
  if RGModelo.ItemIndex = 1 then begin
    //ImprimirModeloConferencia;
    ImprimirModeloConferencia2;
    Exit;
  end else begin
    ImprimirModeloListagem2;
    Exit;
  end;
end;

procedure TFRelExtratoFor.frxListagemPreview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := True;
end;

procedure TFRelExtratoFor.frxListagemClosePreview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := False;
end;

procedure TFRelExtratoFor.QConveniadosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if MostrarRelatorio then begin
    
  end;
end;

procedure TFRelExtratoFor.QCredAfterScroll(DataSet: TDataSet);
var Totais : TValores;
    Imp : TImpres;
    v : integer;
    frx : TfrxReport;
begin
  inherited;
  if (MostrarRelatorio){ and (not Query1.Eof) }then begin
    if not DataSet.Eof then
      Marcados.Add(DataSet.FieldByName('MARCADO').AsString);
    if DataSet.FieldByName('MARCADO').AsString = 'S' then
      if (MCredMARCADO.Value = true) then begin
        Query1.Close;
        Query1.Parameters[0].Value := QCredCRED_ID.Value;
        Query1.SQL.Text;
        Query1.Open;
      end;
    if {(RGModelo.ItemIndex = 0) and }(Query1.Active) then begin
      Imp := TImpres.Create;
      Totais.VlrBrutConf  := 0;
      Totais.VlrBrutNConf := 0;
      Totais.VlrLiquConf  := 0;
      Totais.VlrLiquNConf := 0;
      Totais := CriarCorpoModList(Imp);
      Imp.Free;
      if Query1.RecordCount <= 0 then
        v := 1
      else
        v := Query1.RecordCount;
      if RGModelo.ItemIndex = 0 then begin
        frx := frxListagem;
        frx.Variables['valorMedio']     := (Totais.VlrBrutConf + Totais.VlrBrutNConf) / v;
        frx.Variables['taxaAdm']        := (Totais.VlrBrutConf + Totais.VlrBrutNConf)-(Totais.VlrLiquConf + Totais.VlrLiquNConf);
        frx.Variables['totConfBruto']   := Totais.VlrBrutConf;
        frx.Variables['totNConfBruto']  := Totais.VlrBrutNConf;
        frx.Variables['totFornBruto']   := Totais.VlrBrutConf + Totais.VlrBrutNConf;
        frx.Variables['umPorPagina']    := CkUmPorPag.Checked;
        if CkLiquido.Checked then begin
          frx.Variables['totConfLiq']   := Totais.VlrLiquConf;
          frx.Variables['totNConfLiq']  := Totais.VlrLiquNConf;
          frx.Variables['totFornLiq']   := Totais.VlrLiquConf + Totais.VlrLiquNConf;
        end;
      end else begin
        frx := frxAgrupadoEmpresa;
        frx.Variables['valorMedio']     := (Totais.VlrBrutConf + Totais.VlrBrutNConf) / v;
        frx.Variables['taxaAdm']        := (Totais.VlrBrutConf + Totais.VlrBrutNConf)-(Totais.VlrLiquConf + Totais.VlrLiquNConf);
        frx.Variables['totFornBruto']   := Totais.VlrBrutConf + Totais.VlrBrutNConf;
        frx.Variables['umPorPagina']    := CkUmPorPag.Checked;
        if CkLiquido.Checked then begin
          frx.Variables['totFornLiq']   := Totais.VlrLiquConf + Totais.VlrLiquNConf;
        end;
      end;
    end;
  end;
end;

procedure TFRelExtratoFor.QEmpresaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if (MostrarRelatorio){ and (not Query1.Eof) }then begin
    if (Query1.Active) and (RGModelo.ItemIndex = 1) then begin
      Query1.Close;
      Query1.Parameters.Items[0].Value := QEmpresaEMPRES_ID.Value;
      Query1.Open;
    end;
  end;
end;

procedure TFRelExtratoFor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if Marcados <> nil then
    Marcados.Free;
end;

procedure TFRelExtratoFor.bntGerarPDFClick(Sender: TObject);
var  nome : String;
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
  //nome := ExtractFilePath(Application.ExeName)+'ExtratoEstabelecimentos'+FormatDateTime('ddmmyyyy',now)+'.pdf';
  //frxPDFExport1.FileName := nome;
    BitBtn2.Click;
    if RGModelo.ItemIndex = 1 then
      frxAgrupadoEmpresa.Export(frxPDFExport1)
    else
      frxListagem.Export(frxPDFExport1);
  end;
  //ShellExecute(Handle, 'open', PAnsiChar(nome), nil, nil, SW_SHOWNORMAL);
end;

procedure TFRelExtratoFor.Button3Click(Sender: TObject);
begin
  inherited;
  roq.Close;
  roq.Parameters.Items[0].Value := dataini.Date;
  roq.Parameters.Items[1].Value := datafin.Date;
  roq.Open;
  frxReport1.ShowReport;
end;

procedure TFRelExtratoFor.MCredAfterScroll(DataSet: TDataSet);
var Totais : TValores;
    Imp : TImpres;
    v : integer;
    frx : TfrxReport;
begin
  inherited;
  if (MostrarRelatorio){ and (not Query1.Eof) }then begin
    if not DataSet.Eof then
      Marcados.Add(DataSet.FieldByName('MARCADO').AsString);
    if DataSet.FieldByName('MARCADO').AsBoolean = true then
      if (MCredMARCADO.Value = true) then begin
        Query1.Close;
        Query1.Parameters[0].Value := QCredCRED_ID.Value;
        Query1.SQL.Text;
        Query1.Open;
      end;
    if {(RGModelo.ItemIndex = 0) and }(Query1.Active) then begin
      Imp := TImpres.Create;
      Totais.VlrBrutConf  := 0;
      Totais.VlrBrutNConf := 0;
      Totais.VlrLiquConf  := 0;
      Totais.VlrLiquNConf := 0;
      Totais := CriarCorpoModList(Imp);
      Imp.Free;
      if Query1.RecordCount <= 0 then
        v := 1
      else
        v := Query1.RecordCount;
      if RGModelo.ItemIndex = 0 then begin
        frx := frxListagem;
        frx.Variables['valorMedio']     := (Totais.VlrBrutConf + Totais.VlrBrutNConf) / v;
        frx.Variables['taxaAdm']        := (Totais.VlrBrutConf + Totais.VlrBrutNConf)-(Totais.VlrLiquConf + Totais.VlrLiquNConf);
        frx.Variables['totConfBruto']   := Totais.VlrBrutConf;
        frx.Variables['totNConfBruto']  := Totais.VlrBrutNConf;
        frx.Variables['totFornBruto']   := Totais.VlrBrutConf + Totais.VlrBrutNConf;
        frx.Variables['umPorPagina']    := CkUmPorPag.Checked;
        if CkLiquido.Checked then begin
          frx.Variables['totConfLiq']   := Totais.VlrLiquConf;
          frx.Variables['totNConfLiq']  := Totais.VlrLiquNConf;
          frx.Variables['totFornLiq']   := Totais.VlrLiquConf + Totais.VlrLiquNConf;
        end;
      end else begin
        frx := frxAgrupadoEmpresa;
        frx.Variables['valorMedio']     := (Totais.VlrBrutConf + Totais.VlrBrutNConf) / v;
        frx.Variables['taxaAdm']        := (Totais.VlrBrutConf + Totais.VlrBrutNConf)-(Totais.VlrLiquConf + Totais.VlrLiquNConf);
        frx.Variables['totFornBruto']   := Totais.VlrBrutConf + Totais.VlrBrutNConf;
        frx.Variables['umPorPagina']    := CkUmPorPag.Checked;
        if CkLiquido.Checked then begin
          frx.Variables['totFornLiq']   := Totais.VlrLiquConf + Totais.VlrLiquNConf;
        end;
      end;
    end;
  end;
end;

end.
