object F_PesqProd: TF_PesqProd
  Left = 154
  Top = 147
  ActiveControl = DBGrid1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Pesquisa de Produtos'
  ClientHeight = 419
  ClientWidth = 773
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 773
    Height = 351
    Align = alClient
    DataSource = F_EntregaNF.DSProdutos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid1DrawColumnCell
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATA'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Width = 68
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SN'
        Title.Alignment = taCenter
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 363
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRECO_FINAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pre'#231'o Final'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRECO_SEM_DESCONTO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pre'#231'o S/Desc.'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODINBS'
        Title.Caption = 'C'#243'd. Produto (INBS)'
        Width = 106
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 773
    Height = 41
    Align = alTop
    Caption = 'Para cadastrar um produto novo, pressione F9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 392
    Width = 773
    Height = 27
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 8
      Width = 140
      Height = 13
      Caption = 'Procurando um produto?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 152
      Top = 8
      Width = 79
      Height = 13
      Caption = 'Em qual coluna?'
    end
    object Label3: TLabel
      Left = 384
      Top = 8
      Width = 74
      Height = 13
      Caption = 'O que procura?'
    end
    object edtBusca: TEdit
      Left = 464
      Top = 3
      Width = 305
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnKeyPress = edtBuscaKeyPress
    end
    object cbCampos: TComboBox
      Left = 240
      Top = 3
      Width = 129
      Height = 21
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 128
    object Incluir1: TMenuItem
      Caption = 'Incluir'
      ShortCut = 120
      OnClick = Incluir1Click
    end
  end
end
