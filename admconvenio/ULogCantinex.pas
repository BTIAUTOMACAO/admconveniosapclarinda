unit ULogCantinex;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, Buttons, Grids, DBGrids, JvExDBGrids, JvDBGrid,
  StdCtrls, Mask, JvExMask, JvToolEdit, Menus, ExtCtrls, JvMemoryDataset;

type
  TFLogRecargaCantinex = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    GroupBox2: TGroupBox;
    datafin: TJvDateEdit;
    DataIni: TJvDateEdit;
    GridLogRecargaCantinex: TJvDBGrid;
    Panel4: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    btnGravar: TBitBtn;
    bntGerarPDF: TBitBtn;
    QLogCantinex: TADOQuery;
    DSLogCantinex: TDataSource;
    QLogCantinexID: TAutoIncField;
    QLogCantinexvalor: TBCDField;
    QLogCantinexvalor_taxa: TBCDField;
    QLogCantinexvalor_recarga: TBCDField;
    QLogCantinexDATA_RECARGA: TStringField;
    QLogCantinexORIGEM: TStringField;
    QLogCantinexCONV_ID: TIntegerField;
    QLogCantinexTITULAR: TStringField;
    QLogCantinexNOME: TStringField;
    MLogRecargaCantinex: TJvMemoryData;
    MLogRecargaCantinexID: TIntegerField;
    MLogRecargaCantinexvalor: TCurrencyField;
    MLogRecargaCantinexvalor_taxa: TCurrencyField;
    MLogRecargaCantinexvalor_recarga: TCurrencyField;
    MLogRecargaCantinexdata_recarga: TDateField;
    MLogRecargaCantinexorigem: TStringField;
    MLogRecargaCantinexconv_id: TIntegerField;
    MLogRecargaCantinextitular: TStringField;
    MLogRecargaCantinexnome: TStringField;
    MLogRecargaCantinexmarcado: TBooleanField;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    txtConvID: TEdit;
    txtCodCartImp: TEdit;
    Label2: TLabel;
    txtNome: TEdit;
    Label3: TLabel;
    cbbStatus: TComboBox;
    Label4: TLabel;
    btnCancelar: TBitBtn;
    QLogCantinexCODCARTIMP: TStringField;
    MLogRecargaCantinexcodcartimp: TStringField;
    procedure ButListaEmpClick(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure GridLogRecargaCantinexDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
    aluno_sel : String;
  public
    { Public declarations }
    procedure BuscaLotes;
    procedure Alunos_Sel;
  end;

var
  FLogRecargaCantinex: TFLogRecargaCantinex;

implementation

uses cartao_util, URotinasTexto, impressao, Math, DM;
{$R *.dfm}

procedure TFLogRecargaCantinex.ButListaEmpClick(Sender: TObject);
begin
  inherited;
  BuscaLotes;
  txtConvID.Text := '';
  txtCodCartImp.Text := '';
  txtNome.Text := '';
  DataIni.Text := '';
  datafin.Text := '';
  cbbStatus.ItemIndex := 0;
  btnGravar.SetFocus;
end;

procedure TFLogRecargaCantinex.BuscaLotes;
begin
  QLogCantinex.Close;
  QLogCantinex.SQL.Clear;
  QLogCantinex.SQL.Add('SELECT ');
  QLogCantinex.SQL.Add('LOG_ID ID, ');
  QLogCantinex.SQL.Add('VALOR, ');
  QLogCantinex.SQL.Add('log_recarga_cantinex.valor_taxa,');
  QLogCantinex.SQL.Add('valor_recarga,');
  QLogCantinex.SQL.Add('CONVERT(varchar,dt_cadastro,103) DATA_RECARGA, ');
  QLogCantinex.SQL.Add('ORIGEM, ');
  QLogCantinex.SQL.Add('log_recarga_cantinex.CONV_ID, ');
  QLogCantinex.SQL.Add('CONV.TITULAR, ');
  QLogCantinex.SQL.Add('CART.CODCARTIMP, ');
  QLogCantinex.SQL.Add('EMPRESAS.NOME ');
  QLogCantinex.SQL.Add('from log_recarga_cantinex ');
  QLogCantinex.SQL.Add('INNER JOIN CONVENIADOS CONV ON log_recarga_cantinex.CONV_ID = CONV.CONV_ID ');
  if txtCodCartImp.Text  <> '' then
    QLogCantinex.SQL.Add(' INNER JOIN CARTOES ON CARTOES.CONV_ID = log_recarga_cantinex.CONV_ID ');
  QLogCantinex.SQL.Add('INNER JOIN EMPRESAS ON EMPRESAS.EMPRES_ID = CONV.EMPRES_ID ');
  QLogCantinex.SQL.Add('INNER JOIN CARTOES CART ON CART.CONV_ID = CONV.CONV_ID ');
  QLogCantinex.SQL.Add('WHERE ORIGEM = ''PagSeguro''' );

  if txtConvID.Text <> '' then
    QLogCantinex.SQL.Add(' AND log_recarga_cantinex.CONV_ID = ' + fnRemoverCaracterSQLInjection(txtConvID.Text));

  if txtNome.Text <> '' then
    QLogCantinex.SQL.Add(' AND CONV.TITULAR like ''%'+txtNome.Text+'%''');

  if cbbStatus.ItemIndex = 0 then
    QLogCantinex.SQL.Add(' and log_recarga_cantinex.status = ''P''')
    else if cbbStatus.ItemIndex = 1 then
      QLogCantinex.SQL.Add(' and log_recarga_cantinex.status = ''E''')
      else
        QLogCantinex.SQL.Add(' and log_recarga_cantinex.status = ''P''');

  if txtCodCartImp.Text  <> '' then
    QLogCantinex.SQL.Add('AND CODCARTIMP = '+txtCodCartImp.Text);

  if((DataIni.Date <> 0) or (datafin.Date <> 0)) then
  begin

     if datafin.Date <> 0 then
     begin
        QLogCantinex.SQL.Add(' AND log_recarga_cantinex.DT_CADASTRO  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataFin.Date)));
     end
     else
     QLogCantinex.SQL.Add(' AND log_recarga_cantinex.DT_CADASTRO  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',Date)));

  end;

  QLogCantinex.Open;
  if QLogCantinex.IsEmpty then
  begin
    MsgInf('N�o foi encontrado nenhum registro de recebimento CANTINEX.');
    DataIni.SetFocus;
  end;
  QLogCantinex.First;
  MLogRecargaCantinex.Open;
  MLogRecargaCantinex.EmptyTable;
  MLogRecargaCantinex.DisableControls;
  while not QLogCantinex.Eof do begin
      MLogRecargaCantinex.Append;
      MLogRecargaCantinexID.AsInteger  := QLogCantinexID.AsInteger;
      MLogRecargaCantinexvalor.AsCurrency := QLogCantinexvalor.AsCurrency;
      MLogRecargaCantinexvalor_taxa.AsCurrency := QLogCantinexvalor_taxa.AsCurrency;
      MLogRecargaCantinexvalor_recarga.AsCurrency := QLogCantinexvalor_recarga.AsCurrency;
      MLogRecargaCantinexdata_recarga.AsDateTime := QLogCantinexDATA_RECARGA.AsDateTime;
      MLogRecargaCantinexorigem.AsString := QLogCantinexORIGEM.AsString;
      MLogRecargaCantinexconv_id.AsInteger := QLogCantinexCONV_ID.AsInteger;
      MLogRecargaCantinextitular.AsString := QLogCantinexTITULAR.AsString;
      MLogRecargaCantinexnome.AsString := QLogCantinexNOME.AsString;
      MLogRecargaCantinexcodcartimp.AsString := QLogCantinexCODCARTIMP.AsString;
      MLogRecargaCantinexmarcado.AsBoolean := False;
      MLogRecargaCantinex.Post;
      QLogCantinex.Next;
  end;
  MLogRecargaCantinex.First;
  MLogRecargaCantinex.EnableControls;
  aluno_sel := EmptyStr;
end;

procedure TFLogRecargaCantinex.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  if MLogRecargaCantinex.IsEmpty then Exit;
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexMarcado.AsBoolean := not MLogRecargaCantinexMarcado.AsBoolean;
    MLogRecargaCantinex.Post;
    Alunos_Sel;
end;

procedure TFLogRecargaCantinex.Alunos_Sel;
var marca : TBookmark;
begin
  aluno_sel := EmptyStr;
  marca := MLogRecargaCantinex.GetBookmark;
  MLogRecargaCantinex.DisableControls;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.eof do begin
    if MLogRecargaCantinexMarcado.AsBoolean then
      aluno_sel := aluno_sel + ','+MLogRecargaCantinexconv_id.AsString;
    MLogRecargaCantinex.Next;
  end;
  MLogRecargaCantinex.GotoBookmark(marca);
  MLogRecargaCantinex.FreeBookmark(marca);
  if aluno_sel <> '' then aluno_sel := Copy(aluno_sel,2,Length(aluno_sel));
  MLogRecargaCantinex.EnableControls;
end;

procedure TFLogRecargaCantinex.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  if MLogRecargaCantinex.IsEmpty then Exit;
  MLogRecargaCantinex.DisableControls;
  marca := MLogRecargaCantinex.GetBookmark;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.eof do begin
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexmarcado.AsBoolean := true;
    MLogRecargaCantinex.Post;
    MLogRecargaCantinex.Next;
  end;
  MLogRecargaCantinex.GotoBookmark(marca);
  MLogRecargaCantinex.FreeBookmark(marca);
  MLogRecargaCantinex.EnableControls;
  Alunos_Sel;
end;

procedure TFLogRecargaCantinex.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
   if MLogRecargaCantinex.IsEmpty then Exit;
  MLogRecargaCantinex.DisableControls;
  marca := MLogRecargaCantinex.GetBookmark;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.eof do begin
    MLogRecargaCantinex.Edit;
    MLogRecargaCantinexMarcado.AsBoolean := false;
    MLogRecargaCantinex.Post;
    MLogRecargaCantinex.Next;
  end;
  MLogRecargaCantinex.GotoBookmark(marca);
  MLogRecargaCantinex.FreeBookmark(marca);
  MLogRecargaCantinex.EnableControls;
  Alunos_Sel;
end;

procedure TFLogRecargaCantinex.btnGravarClick(Sender: TObject);
var conv_id : Integer;
    strComand,valorDaRecarga : String;
begin
  inherited;
  //PEGA OS DADOS DOS ALUNOS MARCADO
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.Eof  do
  begin
    if (MLogRecargaCantinexmarcado.AsBoolean = True) then
    begin
      //EXECUTA ALTERA��O NO LIMITE_MES
      conv_id := MLogRecargaCantinexconv_id.AsInteger;
      valorDaRecarga := StringReplace(MLogRecargaCantinexvalor_recarga.asString,',','.',[rfReplaceAll, rfIgnoreCase]);
      strComand := 'update LOG_RECARGA_CANTINEX SET status = '+ QuotedStr('E') + ' where LOG_ID = ' + IntToStr(MLogRecargaCantinexID.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);

      strComand := 'update conveniados SET limite_mes = limite_mes + '+ valorDaRecarga + ' where conv_id = ' + IntToStr(conv_id);
      DMConexao.ExecuteNonQuery(strComand);
    end;
    MLogRecargaCantinex.Next;
  end;
  MsgInf('Recarga(s) realizadas com sucesso!');


end;

procedure TFLogRecargaCantinex.GridLogRecargaCantinexDblClick(
  Sender: TObject);
begin
  inherited;
  ButMarcaDesmEmpClick(nil);
end;

procedure TFLogRecargaCantinex.btnCancelarClick(Sender: TObject);
var conv_id : Integer;
    strComand : string;
begin
  inherited;
  MLogRecargaCantinex.First;
  while not MLogRecargaCantinex.Eof  do
  begin
    if (MLogRecargaCantinexmarcado.AsBoolean = True) then
    begin
      //EXECUTA ALTERA��O NO LIMITE_MES
      conv_id := MLogRecargaCantinexconv_id.AsInteger;

      //valorDaRecarga := StringReplace(MLogRecargaCantinexvalor_recarga.asString,',','.',[rfReplaceAll, rfIgnoreCase]);
      strComand := 'update LOG_RECARGA_CANTINEX SET status = '+ QuotedStr('C') + ' where LOG_ID = ' + IntToStr(MLogRecargaCantinexID.AsInteger);
      DMConexao.ExecuteNonQuery(strComand);


    end;
    MLogRecargaCantinex.Next;
  end;
  MsgInf('Cancelamento(s) realizado(s) com sucesso!');
end;

end.
