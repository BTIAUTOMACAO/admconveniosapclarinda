inherited FCadTaxas: TFCadTaxas
  Left = 316
  Top = 116
  Caption = 'Cadastro de Taxas Fixas para Fornecedores'
  ClientWidth = 796
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 796
    ActivePage = TabGrade
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 788
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 721
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 788
        Columns = <
          item
            Expanded = False
            FieldName = 'TAXA_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTCADASTRO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Width = 788
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Width = 788
      end
      inherited Panel3: TPanel
        Width = 788
        object Label4: TLabel
          Left = 112
          Top = 20
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = DBEdit2
        end
        object Label3: TLabel
          Left = 16
          Top = 20
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
        end
        object Label5: TLabel
          Left = 216
          Top = 69
          Width = 24
          Height = 13
          Caption = 'Valor'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 56
          Top = 16
          Width = 49
          Height = 21
          Color = clBtnFace
          DataField = 'TAXA_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 168
          Top = 16
          Width = 425
          Height = 21
          CharCase = ecUpperCase
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 248
          Top = 64
          Width = 112
          Height = 21
          DataField = 'VALOR'
          DataSource = DSCadastro
          TabOrder = 3
          OnKeyPress = DBEdit3KeyPress
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 393
          Width = 784
          Height = 64
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 6
          object Label14: TLabel
            Left = 16
            Top = 16
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
          end
          object Label13: TLabel
            Left = 144
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Operador'
          end
          object DBEdit4: TDBEdit
            Left = 144
            Top = 32
            Width = 177
            Height = 21
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
          object DBEdit5: TDBEdit
            Left = 16
            Top = 32
            Width = 121
            Height = 21
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
        end
        object UsaCondicao: TDBCheckBox
          Left = 17
          Top = 100
          Width = 360
          Height = 17
          Caption = 
            'Cobrar taxa somente quando o valor bruto do fornecedor for maior' +
            ' que:'
          DataField = 'USACONDICAO'
          DataSource = DSCadastro
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = UsaCondicaoClick
        end
        object DBEdit6: TDBEdit
          Left = 376
          Top = 96
          Width = 112
          Height = 21
          DataField = 'MAIORQUE'
          DataSource = DSCadastro
          Enabled = False
          TabOrder = 4
        end
        object rgTipoValor: TDBRadioGroup
          Left = 16
          Top = 48
          Width = 193
          Height = 41
          Caption = 'Tipo de Valor'
          Columns = 2
          DataField = 'VALORREAL'
          DataSource = DSCadastro
          Items.Strings = (
            'Valor Real'
            'Porcentagem')
          TabOrder = 2
          Values.Strings = (
            'S'
            'N')
        end
      end
    end
    object TabSheet1: TTabSheet [2]
      Caption = '&Estabelecimentos'
      ImageIndex = 3
      OnHide = TabSheet1Hide
      OnShow = TabSheet1Show
      object Panel4: TPanel
        Left = 0
        Top = 25
        Width = 788
        Height = 432
        Align = alClient
        BorderStyle = bsSingle
        TabOrder = 1
        object GridCred: TJvDBGrid
          Left = 1
          Top = 1
          Width = 782
          Height = 426
          Align = alClient
          DataSource = DSCred
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          OnTitleBtnClick = GridCredTitleBtnClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'cred_id'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nome'
              Title.Caption = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'descontar'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Descontar'
              Width = 62
              Visible = True
            end>
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 457
        Width = 788
        Height = 34
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 2
        object btnGrava: TButton
          Left = 2
          Top = 3
          Width = 100
          Height = 25
          Caption = '&Gravar'
          TabOrder = 0
          OnClick = btnGravaClick
        end
        object btnCancel: TButton
          Left = 104
          Top = 3
          Width = 100
          Height = 25
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnCancelClick
        end
        object Button1: TButton
          Left = 206
          Top = 3
          Width = 100
          Height = 25
          Caption = 'Alterar Todos'
          TabOrder = 2
          OnClick = Button1Click
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 25
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 4
          Width = 292
          Height = 13
          Caption = 'Configura'#231#227'o dos Estabelecimentos para esta Taxa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 788
      end
      inherited GridHistorico: TJvDBGrid
        Width = 788
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 796
  end
  inherited panStatus2: TPanel
    Width = 796
  end
  inherited PopupBut: TPopupMenu
    Left = 572
    Top = 80
  end
  inherited PopupGrid1: TPopupMenu
    Left = 500
    Top = 72
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from taxas where taxa_id = 0')
    object QCadastroTAXA_ID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'TAXA_ID'
      Required = True
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Required = True
      Size = 60
    end
    object QCadastroVALOR: TFloatField
      DisplayLabel = 'Valor'
      FieldName = 'VALOR'
      DisplayFormat = '#,##0.00'
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroAPAGADO: TStringField
      DisplayLabel = 'Apagado'
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERCADASTRO: TStringField
      DisplayLabel = 'Operador Cadastro'
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroVALORREAL: TStringField
      FieldName = 'VALORREAL'
      Size = 1
    end
    object QCadastroUSACONDICAO: TStringField
      FieldName = 'USACONDICAO'
      Size = 1
    end
    object QCadastroMAIORQUE: TFloatField
      FieldName = 'MAIORQUE'
      DisplayFormat = '#,##0.00'
    end
    object QCadastroLIQUIDO: TStringField
      FieldName = 'LIQUIDO'
      Size = 1
    end
    object QCadastroDTAPAGADO: TDateTimeField
      DisplayLabel = 'Data Apagado'
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      DisplayLabel = 'Data de Altera'#231#227'o'
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      DisplayLabel = 'Data de Cadastro'
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object DSCred: TDataSource
    DataSet = QCred
    OnStateChange = DSCredStateChange
    Left = 580
    Top = 249
  end
  object QCred: TADOQuery
    Connection = DMConexao.AdoCon
    AfterInsert = QCredAfterInsert
    BeforePost = QCredBeforePost
    AfterPost = QCredAfterPost
    Parameters = <
      item
        Name = 'taxas'
        DataType = ftInteger
        Size = 18
        Value = Null
      end>
    SQL.Strings = (
      
        'Select cred.cred_id, cred.nome, (case when rel.taxa_id is null t' +
        'hen '#39'N'#39' else '#39'S'#39' end) as descontar'
      
        'from credenciados cred left join rel_taxa_cred rel on cred.cred_' +
        'id = rel.cred_id'
      
        'and (rel.taxa_id = :taxa) or (rel.taxa_id is null) order by cred' +
        '.nome')
    Left = 444
    Top = 265
    object QCredcred_id: TIntegerField
      FieldName = 'cred_id'
      ReadOnly = True
    end
    object QCrednome: TStringField
      FieldName = 'nome'
      ReadOnly = True
      Size = 60
    end
    object QCreddescontar: TStringField
      FieldName = 'descontar'
      Size = 1
    end
  end
end
