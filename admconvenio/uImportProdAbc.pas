unit uImportProdAbc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, Mask, ToolEdit, ComCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, JvExControls, JvDBLookup,
  JvExMask, JvToolEdit, URotinasTexto;

type
  TfrmImportProdAbc = class(TForm)
    Table1: TADOTable;
    //FilenameEdit1: TFilenameEdit;
    ProgressBar1: TProgressBar;
    DataSource1: TDataSource;
    Label1: TLabel;
    BitBtn1: TSpeedButton;
    RichEdit1: TRichEdit;
    lblC: TLabel;
    FilenameEdit1: TJvFilenameEdit;
    AdoQry1: TADOQuery;
    AdoQry1prog_id: TIntegerField;
    AdoQry1nome: TStringField;
    QProgramas: TADOQuery;
    QProgramasprog_id: TIntegerField;
    QProgramasnome: TStringField;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportProdAbc: TfrmImportProdAbc;

implementation

uses DM, cartao_util, UMenu, UChangeLog;

{$R *.dfm}

procedure TfrmImportProdAbc.FormCreate(Sender: TObject);
begin
  AdoQry1.Open;
  FilenameEdit1.Text:= '"'+FMenu.GetPersonalFolder+'\"';
end;

procedure TfrmImportProdAbc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Table1.Close;
  AdoQry1.Close;
end;

procedure TfrmImportProdAbc.BitBtn1Click(Sender: TObject);
var
  path, strSql, aux: String;
  prod_id, prod_aut_id, prog_id: Integer;
  preco_prod, preco_prod18, preco_prod19: Currency;
begin
  if not FileExists(FilenameEdit1.FileName) then begin
    MsgErro('Arquivo n�o encontrado.');
    FilenameEdit1.SetFocus;
    screen.Cursor := crDefault;
    Exit;
  end;


  screen.Cursor  := crHourGlass;
  try
    path := '';
    path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+FilenameEdit1.Text+';';
    path := path + 'Extended Properties="Excel 12.0;HDR=YES;"';
    Table1.Active := False;

    Table1.ConnectionString := path;
    Table1.TableName:= 'TABELA$';
    Table1.Active := True;
  except on E:Exception do begin
      MsgInf('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
      screen.Cursor := crDefault;
      Exit;
    end;
  end;
  ProgressBar1.Position := 0;
  BitBtn1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
 // if MsgSimNao('Deseja limpar os produtos do programa?') then
   // DMConexao.ExecuteSql('UPDATE prog_prod SET ativo = ''N'' WHERE prog_id = '+cbbPrograma.KeyValue);
  DMConexao.AdoQry.CommandTimeout := 10;
  while not Table1.Eof do begin
  lblC.Caption := inttostr(strtoint(lblC.Caption)+ 1);
  Application.ProcessMessages;
    if Length(Table1.FieldByName('MED_BARRA').AsString) > 13 then begin
      MsgInf('O produto: '+Table1.FieldByName('MED_PRINCI').AsString+sLineBreak+
             'est� com C�digo de Barras maior que 13 caracteres e por isso'+sLineBreak+'n�o ser� verificado!');
    end else begin
      if Trim(Table1.FieldByName('MED_BARRA').AsString) <> '' then begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.CommandTimeout := 3000;
        //aux := QuotedStr(SoNumero(Table1.FieldByName('MED_BARRA').AsString));
        DMConexao.AdoQry.SQL.Add('select prod_id from barras where barras = '+ QuotedStr(SoNumero(Table1.FieldByName('MED_BARRA').AsString)) +'');
        DMConexao.AdoQry.Open;
        DMConexao.AdoQry.RecordCount;
        QProgramas.Open;
        if DMConexao.AdoQry.IsEmpty then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_ID');
          DMConexao.AdoQry.Open;
          prod_id := DMConexao.AdoQry.Fields[0].AsInteger;
          // cadastrando produto
          //ff///
          if(Table1.FieldByName('MED_TIPMED').Text = 'GENERICO') then
          begin
            if Table1.FieldByName('MED_FRA12').AsCurrency > 0 then begin
              strSql:= 'insert into produtos (prod_id, apagado, descricao, preco_vnd, preco_custo) '+
                     'values ('+IntToStr(prod_id)+', ''N'', '''+iif(copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90)='',
                     'DESCRICAO NAO DEFINIDA',copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90))+
                     ''', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+
                     ', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA12').AsCurrency))+')';
              DMConexao.ExecuteSql(strSql);
              DMConexao.ExecuteSql('insert into barras (barras_id, barras, prod_id) values (NEXT VALUE FOR SBARRAS_ID,'''+SoNumero(Table1.FieldByName('MED_BARRA').AsString)+
                           ''', '+IntToStr(prod_id)+')');

            end
            else begin
              strSql:= 'insert into produtos (prod_id, apagado, descricao, preco_vnd, preco_custo) '+
                       'values ('+IntToStr(prod_id)+', ''N'', '''+iif(copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90)='',
                       'DESCRICAO NAO DEFINIDA',copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90))+
                       ''', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+
                       ', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency))+')';
              DMConexao.ExecuteSql(strSql);
              DMConexao.ExecuteSql('insert into barras (barras_id, barras, prod_id) values (NEXT VALUE FOR SBARRAS_ID,'''+SoNumero(Table1.FieldByName('MED_BARRA').AsString)+
                             ''', '+IntToStr(prod_id)+')');

            end;
          end else if Table1.FieldByName('MED_FRA18').AsCurrency > 0 then begin
            strSql:= 'insert into produtos (prod_id, apagado, descricao, preco_vnd, preco_custo) '+
                   'values ('+IntToStr(prod_id)+', ''N'', '''+iif(copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90)='',
                   'DESCRICAO NAO DEFINIDA',copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90))+
                   ''', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+
                   ', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA18').AsCurrency))+')';
            DMConexao.ExecuteSql(strSql);
            DMConexao.ExecuteSql('insert into barras (barras_id, barras, prod_id) values (NEXT VALUE FOR SBARRAS_ID,'''+SoNumero(Table1.FieldByName('MED_BARRA').AsString)+
                         ''', '+IntToStr(prod_id)+')');

          end else begin
          strSql:= 'insert into produtos (prod_id, apagado, descricao, preco_vnd, preco_custo) '+
                   'values ('+IntToStr(prod_id)+', ''N'', '''+iif(copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90)='',
                   'DESCRICAO NAO DEFINIDA',copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90))+
                   ''', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+
                   ', '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency))+')';
          DMConexao.ExecuteSql(strSql);
          DMConexao.ExecuteSql('insert into barras (barras_id, barras, prod_id) values (NEXT VALUE FOR SBARRAS_ID,'''+SoNumero(Table1.FieldByName('MED_BARRA').AsString)+
                         ''', '+IntToStr(prod_id)+')');

          end;
          // GYBSOM - REGRA DE GEN�RICOS INSERIDA

        end else begin
           prod_id := DMConexao.AdoQry.Fields[0].AsInteger;

          if(Table1.FieldByName('MED_TIPMED').Text = 'GENERICO') then
          begin
            if Table1.FieldByName('MED_FRA12').AsCurrency > 0 then begin
              strSql:= 'update  produtos set preco_vnd = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+
                   ', preco_custo = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA12').AsCurrency))+
                   ' where prod_id ='+ IntToStr(prod_id);
              DMConexao.ExecuteSql(strSql);

            end
            else begin
              strSql:= 'update  produtos set preco_vnd = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+
                   ', preco_custo = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency))+
                   ' where prod_id ='+ IntToStr(prod_id);
              DMConexao.ExecuteSql(strSql);

            end;
          end else if Table1.FieldByName('MED_FRA18').AsCurrency > 0 then begin
            strSql:= 'update  produtos set preco_vnd = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+
                   ', preco_custo = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA18').AsCurrency))+
                   ' where prod_id ='+ IntToStr(prod_id);
            DMConexao.ExecuteSql(strSql);

          end else begin
          strSql:= 'update  produtos set preco_vnd = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+
                   ', preco_custo = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency))+
                   ' where prod_id ='+ IntToStr(prod_id);
          DMConexao.ExecuteSql(strSql);

          end;
          // GYBSOM - REGRA DE GEN�RICOS INCLUSA

         end;

        //PRODUTOS_AUTORIZADOR
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select prod_aut_id from produtos_autorizador where prod_codigo = '+QuotedStr(SoNumero(Table1.FieldByName('MED_BARRA').AsString))+'');
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then begin
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_AUT_ID');
          DMConexao.AdoQry.Open;
          prod_aut_id := DMConexao.AdoQry.Fields[0].AsInteger;
          // cadastrando produto
          //ff///

          strSql:= 'insert into produtos_autorizador (prod_aut_id, prod_codigo, prod_descr, alt_liberada, pre_valor, pre_valor_19) '+
                   'values ('+IntToStr(prod_aut_id)+','+SoNumero(Table1.FieldByName('MED_BARRA').AsString)+',';

          strSql := strSql + ''''+iif(copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90)='',
                    'DESCRICAO NAO DEFINIDA',copy(fnRemoverCaracterSQLInjection(Table1.FieldByName('MED_DES').AsString),1,90))+ ''',';

          if Table1.FieldByName('MED_TIPMED').Text = 'GENERICO' then begin
            if Table1.FieldByName('MED_PCO12').AsCurrency = 0 then
              strSql := strSql + '''S'','
            else
              strSql := strSql + '''N'',';

            if Table1.FieldByName('MED_FRA12').AsCurrency > 0 then
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA12').AsCurrency))+ ','
            else
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency))+ ',';
          end else begin
            if Table1.FieldByName('MED_PCO18').AsCurrency = 0 then
              strSql := strSql + '''S'','
            else
              strSql := strSql + '''N'',';

            if Table1.FieldByName('MED_FRA18').AsCurrency > 0 then
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA18').AsCurrency))+ ','
            else
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency))+ ',';
          end;

          if Table1.FieldByName('MED_FRA20').AsCurrency > 0 then
            strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+ ')'
          else
            strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+ ')';
          // GYBSOM - REGRA DE GEN�RICOS INCLUSA

            DMConexao.ExecuteSql(strSql);

        end
        else
        begin
          prod_aut_id := DMConexao.AdoQry.Fields[0].AsInteger;
          ///
         strSql:= 'update produtos_autorizador set alt_liberada = ';

          if Table1.FieldByName('MED_TIPMED').Text = 'GENERICO' then begin
            if Table1.FieldByName('MED_PCO12').AsCurrency = 0 then
              strSql := strSql + '''S'','
            else
              strSql := strSql + '''N'',';

           strSql:= strSql + ' pre_valor = ';
            if Table1.FieldByName('MED_FRA12').AsCurrency > 0 then
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA12').AsCurrency))+ ','
            else
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency))+ ',';
          end else begin
            if Table1.FieldByName('MED_PCO18').AsCurrency = 0 then
              strSql := strSql + '''S'','
            else
              strSql := strSql + '''N'',';

           strSql:= strSql + ' pre_valor = ';
            if Table1.FieldByName('MED_FRA18').AsCurrency > 0 then
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA18').AsCurrency))+ ','
            else
              strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency))+ ',';
          end;

         strSql:= strSql + ' pre_valor_19 = ';
          if Table1.FieldByName('MED_FRA20').AsCurrency > 0 then
            strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_FRA20').AsCurrency))+ ''
          else
            strSql := strSql +FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))+ '';
          // GYBSOM - REGRA DE GEN�RICOS INCLUSA

          strSql := strSql + ' where prod_aut_id ='+ IntToStr(prod_aut_id);
          DMConexao.ExecuteSql(strSql);
          ///

        end;
        ///// Fim Produtos Autorizador

        // Cadastrando na tabela PROG_PROD

          while not QProgramas.Eof do
          begin
           // DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add ('select prod_id from prog_prod where prog_id = '+QProgramasprog_id.AsString+'and'+
            ' prod_id in (select prod_id from barras where barras = '+QuotedStr(SoNumero(Table1.FieldByName('MED_BARRA').AsString))+')');
            QProgramas.Open;
            DMConexao.AdoQry.Open;

            //verifica se os campos MED_PCO12, MED_PCO18 e MED_PCO20 est�o zerados na planilha excel,
            //se estiverem zerados ele n�o altera os pre�os da tabela prog_prod.
            while not DMConexao.AdoQry.Eof do begin
              prod_id := DMConexao.AdoQry.Fields[0].AsInteger;
              if ((Table1.FieldByName('MED_PCO12').AsCurrency <> 0) or
                 (Table1.FieldByName('MED_PCO18').AsCurrency <> 0)) and
                 (Table1.FieldByName('MED_PCO20').AsCurrency <> 0) then
              begin
                prog_id := DMConexao.AdoQry.Fields[0].AsInteger;
                strSql:= 'update prog_prod set ';

                if(Table1.FieldByName('MED_TIPMED').Text = 'GENERICO') then
                begin
                  if (Table1.FieldByName('MED_PCO12').AsCurrency <> 0) then
                  begin
                    strSql := strSql + ' prc_unit = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency));//+','+
                    strSql := strSql + ', prc_unit_18 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO12').AsCurrency))
                   end;

                  if (Table1.FieldByName('MED_PCO20').AsCurrency <> 0) then
                  begin
                    if(Table1.FieldByName('MED_PCO12').AsCurrency <> 0) then
                      strSql := strSql + ', prc_unit_19 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))
                    else
                      strSql := strSql + ' prc_unit_19 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency));
                  end;
                end else begin
                  if (Table1.FieldByName('MED_PCO18').AsCurrency <> 0) then
                  begin
                    strSql := strSql + ' prc_unit = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency));//+','+
                    strSql := strSql + ', prc_unit_18 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO18').AsCurrency))
                   end;

                  if (Table1.FieldByName('MED_PCO20').AsCurrency <> 0) then
                  begin
                    if(Table1.FieldByName('MED_PCO18').AsCurrency <> 0) then
                      strSql := strSql + ', prc_unit_19 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency))
                    else
                      strSql := strSql + ' prc_unit_19 = '+FormatDimIB(ArredondaDin(Table1.FieldByName('MED_PCO20').AsCurrency));
                  end;
                end;
                strSql := strSql + ' where prod_id ='+IntToStr(prod_id)+' and prog_id = '+QProgramasprog_id.AsString;
                DMConexao.ExecuteSql(strSql);

              end;
            DMConexao.AdoQry.Next;
            end;
          //QProgramas.Edit;
          //QProgramas.Post;
          QProgramas.Next;
          end;


      end;
    end;
    QProgramas.Close;
    Table1.Next;
    ProgressBar1.Position:= ProgressBar1.Position + 1;
  end;
  Table1.Close;
  BitBtn1.Enabled := True;
  screen.Cursor := crDefault;
  MsgInf('Importa��o realizada com sucesso!');
end;

end.
