unit USelecionaBD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, inifiles, StdCtrls;

type
  TFSelecionaBD = class(TForm)
    CBBanco: TComboBox;
    Button1: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CBBancoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSelecionaBD: TFSelecionaBD;

implementation

{$R *.dfm}

procedure TFSelecionaBD.FormCreate(Sender: TObject);
var ini : TIniFile;
banco : string;
cont : integer;
begin
   ini := TIniFile.Create(ExtractFilePath(Application.ExeName)+'admcartao.ini');
   CBBanco.Items.Clear;
   banco := ini.ReadString('database','database','');
   cont := 0;
   while banco <> ini.ReadString('database','database'+IntToStr(cont),'') do begin
      if cont = 0 then CBBanco.Items.Add(banco)
      else begin
         banco := ini.ReadString('database','database'+IntToStr(cont),'');
         CBBanco.Items.Add(banco);
      end;
      cont := cont+1;
   end;
   ini.Free;
   CBBanco.Items.Delete(cont-1);
   CBBanco.ItemIndex := 0;
end;

procedure TFSelecionaBD.CBBancoKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then Button1.SetFocus;
end;

end.
