unit FOcorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls;

type
  TFrmOcorrencia = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtSolicitante: TEdit;
    mmoMotivo: TMemo;
    BitBtn1: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmOcorrencia: TFrmOcorrencia;

implementation

uses cartao_util;

{$R *.dfm}

procedure TFrmOcorrencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  edtSolicitante.Text := UpperCase(edtSolicitante.Text);
  mmoMotivo.Text := UpperCase(mmoMotivo.Text);
  if (Trim(edtSolicitante.Text) = '') or (Trim(mmoMotivo.Text) = '') then begin
    MsgInf('Digite o "Nome do solicitante" e o "Motivo/Descri��o da Ocorr�ncia" para sair da tela.');
    Abort;
  end else begin
    bitBtn1.Click;
  end;
end;

procedure TFrmOcorrencia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
    SelectNext(ActiveControl,true,true);
  if key = VK_F4 then
    key := 0;
end;



end.
