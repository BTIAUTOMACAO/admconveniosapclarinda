object FBuscaProdutos2: TFBuscaProdutos2
  Left = 58
  Top = 82
  Width = 1317
  Height = 512
  Caption = 'Busca de Produtos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 1309
    Height = 450
    Align = alClient
    Caption = 'Consulta de Produtos'
    TabOrder = 0
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 1305
      Height = 139
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label12: TLabel
        Left = 6
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Prod. ID'
      end
      object Label13: TLabel
        Left = 76
        Top = 4
        Width = 81
        Height = 13
        Caption = 'C'#243'digo de Barras'
      end
      object Label14: TLabel
        Left = 294
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Descri'#231#227'o do produto'
      end
      object Label15: TLabel
        Left = 294
        Top = 39
        Width = 67
        Height = 13
        Caption = 'Principio Ativo'
      end
      object Label1: TLabel
        Left = 190
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Tipo de Lista'
      end
      object Label2: TLabel
        Left = 4
        Top = 39
        Width = 67
        Height = 13
        Caption = 'Tipo de Pre'#231'o'
      end
      object SpeedButton1: TSpeedButton
        Left = 0
        Top = 123
        Width = 836
        Height = 15
        Caption = 'Filtros adicionais'
        Flat = True
      end
      object Label3: TLabel
        Left = 600
        Top = 40
        Width = 54
        Height = 13
        Caption = 'Valor Inicial'
        Visible = False
      end
      object edProdID: TEdit
        Left = 5
        Top = 16
        Width = 66
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        OnKeyPress = edProdIDKeyPress
      end
      object edDescricao: TEdit
        Left = 293
        Top = 16
        Width = 292
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        OnKeyPress = edDescricaoKeyPress
      end
      object edtPAtivo: TJvDBLookupCombo
        Left = 293
        Top = 51
        Width = 293
        Height = 21
        EmptyValue = '0'
        LookupField = 'PAT_ID'
        LookupDisplay = 'PATIVO'
        LookupSource = dsPAtivo
        TabOrder = 6
        OnKeyPress = edtPAtivoKeyPress
      end
      object btnBuscar: TBitBtn
        Left = 592
        Top = 85
        Width = 97
        Height = 32
        Caption = '&Buscar'
        TabOrder = 12
        OnClick = btnBuscarClick
        Glyph.Data = {
          7E090000424D7E0900000000000036000000280000001D0000001B0000000100
          1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
          A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
          EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
          AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
          596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
          A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
          D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
          C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
          89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
          D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
          C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
          EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
          F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
          BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
          FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
          EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
          99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
          FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
          D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
          BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
          FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
          C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
          ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
          F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
          FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
          C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
          FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
          D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
          BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
          CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D400}
      end
      object edBarras: TEdit
        Left = 75
        Top = 16
        Width = 111
        Height = 21
        TabOrder = 1
        OnKeyPress = edBarrasKeyPress
      end
      object btnClasse: TBitBtn
        Left = 5
        Top = 78
        Width = 95
        Height = 40
        Caption = 'Classes'
        TabOrder = 8
        OnClick = btnClasseClick
      end
      object btnFamilia: TBitBtn
        Left = 199
        Top = 78
        Width = 95
        Height = 40
        Caption = 'Familia'
        TabOrder = 10
        OnClick = btnFamiliaClick
      end
      object btnLab: TBitBtn
        Left = 296
        Top = 78
        Width = 95
        Height = 40
        Caption = 'Laborat'#243'rio'
        TabOrder = 11
        OnClick = btnLabClick
      end
      object btnSubClasse: TBitBtn
        Left = 102
        Top = 78
        Width = 95
        Height = 40
        Caption = 'SubClasse'
        TabOrder = 9
        OnClick = btnSubClasseClick
      end
      object edtTpLista: TJvDBComboBox
        Left = 190
        Top = 16
        Width = 100
        Height = 21
        Items.Strings = (
          ''
          'POSITIVO'
          'NEGATIVO'
          'NEUTRO')
        TabOrder = 2
        Values.Strings = (
          ''
          'P'
          'N'
          'U')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
        ListSettings.OutfilteredValueFont.Style = []
      end
      object edtTpPreco: TJvDBComboBox
        Left = 4
        Top = 51
        Width = 286
        Height = 21
        Items.Strings = (
          ''
          'MEDICAMENTO CONTROLADO (COM PRECO MAXIMO AO CONSUMIDOR)'
          'MEDICAMENTO LIBERADO (SEM PRECO MAXIMO AO CONSUMIDOR)'
          
            'MEDICAMENTO COM AUMENTO DE PRECOS LIBERADO PARA OS LABORAOTIRIOS' +
            ' PELO GOVERNO (COM PRECO MAXIMO AO CONSUMIDOR)'
          'PRODUTO CORRELATO (SEM PRECO MAXIMO AO CONSUMIDOR)'
          'MEDICAMENTO HOSPITALAR (SEM PRECO MAXIMO AO CONSUMIDOR)')
        TabOrder = 5
        Values.Strings = (
          ''
          'M'
          'L'
          'X'
          'O'
          'H')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
        ListSettings.OutfilteredValueFont.Style = []
      end
      object GroupBox1: TGroupBox
        Left = 393
        Top = 73
        Width = 193
        Height = 45
        Caption = 'Intervalo de pre'#231'os'
        TabOrder = 7
        object Entre: TLabel
          Left = 8
          Top = 24
          Width = 106
          Height = 13
          Caption = 'Entre                         e'
        end
      end
      object vlrIni: TJvValidateEdit
        Left = 672
        Top = 16
        Width = 65
        Height = 21
        CriticalPoints.MaxValueIncluded = False
        CriticalPoints.MinValueIncluded = False
        DisplayFormat = dfCurrency
        DecimalPlaces = 2
        TabOrder = 4
        Visible = False
      end
    end
    object grdLista: TJvDBGrid
      Left = 2
      Top = 154
      Width = 1305
      Height = 294
      Align = alClient
      DataSource = dsListProd
      DefaultDrawing = False
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = grdListaDblClick
      OnKeyPress = grdListaKeyPress
      AutoAppend = False
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 16
      TitleRowHeight = 17
      Columns = <
        item
          Expanded = False
          FieldName = 'PROD_ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BARRAS'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LIBCIP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECO_VND'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCONTO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = [fsBold]
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LISTA'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LAB_ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LABOR'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAT_ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PATIVO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CLAS_ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CLASSE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GRUPO_PROD_ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GRUPO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REGMS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GENERICO'
          Visible = True
        end>
    end
    object vlrFin: TJvValidateEdit
      Left = 600
      Top = 32
      Width = 65
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      TabOrder = 1
      Visible = False
    end
    object edtDesconto: TJvValidateEdit
      Left = 602
      Top = 72
      Width = 65
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfCurrency
      DecimalPlaces = 2
      TabOrder = 2
      Visible = False
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 450
    Width = 1309
    Height = 31
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object btnMarcDesm: TBitBtn
      Left = 3
      Top = 3
      Width = 96
      Height = 25
      Caption = 'Marca/Desm.'
      TabOrder = 0
      OnClick = btnMarcDesmClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnMarcaTodos: TBitBtn
      Left = 101
      Top = 3
      Width = 96
      Height = 25
      Caption = 'Marca Todos'
      TabOrder = 1
      OnClick = btnMarcaTodosClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E70022CC0022CCFFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF588AAFF88AAFFFFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnDesmarc: TBitBtn
      Left = 198
      Top = 3
      Width = 96
      Height = 25
      Caption = 'Desm. Todos'
      TabOrder = 2
      OnClick = btnDesmarcClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5B4D0BFAECCBBAAFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9966
        FFEEDDFFFFFFFFFFFFFFEEDDCC9966FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCC9966D9AE84E6C4A2E6C4A2D9AE84CC9966FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4A16E
        FFEEDDFFEEDDFFEEDDFFEEDDD4A16EFFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFDFAC79FFF3E7FFF3E7FFF3E7FFF3E7DFAC79FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECB986
        FFFAF5FFFAF5FFFAF5FFFAF5ECB986FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFF7C491FFFFFFFFFFFFFFFFFFFFFFFFF7C491FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC99
        FFD9B3FFE6CCFFE6CCFFD9B3FFCC99FFFFFFEEDDCCEAD9C8E4D3C2DDCCBBD6C5
        B4D0BFAECCBBAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btnRepassar: TBitBtn
      Left = 693
      Top = 3
      Width = 131
      Height = 25
      Caption = '&Incluir Marcados'
      ModalResult = 1
      TabOrder = 3
      OnClick = btnRepassarClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FF078DBE
        078DBE078DBE078DBE078DBE078DBE078DBE078DBE078DBE0C8518078DBE078D
        BE078DBEFF00FFFF00FF078DBE25A1D172C7E785D7FA66CDF965CDF965CDF965
        CDF965CDF865CDF90C85180C851839ADD8078DBEFF00FFFF00FF078DBE4CBCE7
        39A8D1A0E2FB6FD4FA6FD4F96ED4FA6FD4F96FD4FA6FD4FA0C851827B53F0C85
        1884D7EB078DBEFF00FF078DBE72D6FA078DBEAEEAFC79DCFB0C85180C85180C
        85180C85180C85180C85182EBC4A27B53F0C8518078DBEFF00FF078DBE79DDFB
        218CB6B0C1CBB9B4B60C851861EB955CE68D54DF824BD67541CD6638C5582EBC
        4B26B53F0C8518FF00FF078DBE82E3FC5E8BA481C9E1EAFEFE0C851861EB9561
        EB955CE68D54DF824BD77441CE6637C5582EBC4A26B43F0C8518078DBE8AEAFC
        9E8A91279DC6FFFFFF0C851861EB9561EB9561EB955CE68E54DF824BD77541CE
        6637C4570C8518078DBE078DBE93F0FEB889892198C4078DBE0C85180C85180C
        85180C85180C85180C851854DF814BD6740C8518078DBE078DBE078DBE9BF5FE
        B88989FEF6EDFEF2E7FFF5ECFFF6EFFEF8F2FEFAF5FEF6ED0C85185CE68D0C85
        18FF00FFFF00FFFF00FF078DBEFEFEFEB88989FFF0E3FFF0E3FFF1E5FFF2E6FF
        F3E9FFF5EBFFF0E30C85180C85180989BAFF00FFFF00FFFF00FFFF00FF078DBE
        B88989FFEDDDFFEDDDFFEDDDFFEDDDE9D5C9E7D6C9D7C5BA0C8518078DBEFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFB88989FFEBD8FFEAD7FFEBD8FFEBD8C4
        AAA7C5ABA8CDB5B0CD9999FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        B88989FFE8D2FFE8D2FFE8D2FBE4CFC6ACA9FEFEFECD9999FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFB88989FFE6CFFFE6CFFFE6CFE9CFBFD2
        BAB4CD9999FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        B88989B88989B88989B88989B88989CD9999FF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object dsListProd: TDataSource
    DataSet = MListProd
    Left = 873
    Top = 56
  end
  object dsPAtivo: TDataSource
    DataSet = qPAtivo
    Left = 908
    Top = 57
  end
  object qListProd: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '  p.prod_id,'
      '  p.descricao,'
      
        '  (SELECT TOP 1 barras FROM barras WHERE prod_id = p.prod_id ) b' +
        'arras,'
      '  p.libcip,'
      '  coalesce(p.preco_vnd,0.00) preco_vnd,'
      '  0.00 as desconto,'
      '  p.lista,'
      '  p.lab_id,'
      '  coalesce(laboratorios.nomelab,'#39'NAO RELACIONADO'#39') labor,'
      '  p.pat_id,'
      '  coalesce(pativo.pativo,'#39'NAO RELACIONADO'#39') pativo,'
      '  p.clas_id,'
      '  coalesce(classe.classe,'#39'NAO RELACIONADO'#39') classe,'
      '  p.grupo_prod_id,'
      '  coalesce(grupo_prod.descricao,'#39'NAO RELACIONADO'#39') grupo,'
      '  p.regms,'
      '  p.generico'
      '  FROM produtos p'
      '  LEFT JOIN classe ON p.clas_id = classe.clas_id'
      '  LEFT JOIN pativo ON p.pat_id = pativo.pat_id'
      
        '  LEFT JOIN grupo_prod ON p.grupo_prod_id = grupo_prod.grupo_pro' +
        'd_id'
      '  LEFT JOIN laboratorios ON p.lab_id = laboratorios.lab_id'
      'WHERE p.apagado <> '#39'S'#39)
    Left = 872
    Top = 24
    object qListProdPROD_ID: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'PROD_ID'
      Required = True
    end
    object qListProdDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 90
    end
    object qListProdBARRAS: TStringField
      DisplayLabel = 'C'#243'd. de Barras'
      FieldName = 'BARRAS'
      ReadOnly = True
      Size = 13
    end
    object qListProdLIBCIP: TStringField
      DisplayLabel = 'Tipo Pre'#231'o'
      FieldName = 'LIBCIP'
      Size = 1
    end
    object qListProdPRECO_VND: TFloatField
      DisplayLabel = 'Pre'#231'o Venda'
      FieldName = 'PRECO_VND'
      DisplayFormat = '###,###,##0.00'
    end
    object qListProdLISTA: TStringField
      DisplayLabel = 'Tipo Lista'
      FieldName = 'LISTA'
      Size = 1
    end
    object qListProdLAB_ID: TIntegerField
      DisplayLabel = 'Lab. ID'
      FieldName = 'LAB_ID'
    end
    object qListProdLABOR: TStringField
      DisplayLabel = 'Laborat'#243'rio'
      FieldName = 'LABOR'
      ReadOnly = True
      Size = 50
    end
    object qListProdPAT_ID: TIntegerField
      DisplayLabel = 'Pr. Ativo ID'
      FieldName = 'PAT_ID'
    end
    object qListProdPATIVO: TStringField
      DisplayLabel = 'Principio Ativo'
      FieldName = 'PATIVO'
      ReadOnly = True
      Size = 80
    end
    object qListProdCLAS_ID: TIntegerField
      DisplayLabel = 'Clas. ID'
      FieldName = 'CLAS_ID'
    end
    object qListProdCLASSE: TStringField
      DisplayLabel = 'Classe'
      FieldName = 'CLASSE'
      ReadOnly = True
      Size = 50
    end
    object qListProdGRUPO_PROD_ID: TIntegerField
      DisplayLabel = 'Grupo ID'
      FieldName = 'GRUPO_PROD_ID'
    end
    object qListProdGRUPO: TStringField
      DisplayLabel = 'Grupo'
      FieldName = 'GRUPO'
      ReadOnly = True
      Size = 60
    end
    object qListProdREGMS: TStringField
      DisplayLabel = 'Reg. MS'
      FieldName = 'REGMS'
      Size = 15
    end
    object qListProdGENERICO: TStringField
      DisplayLabel = 'Gen'#233'rico'
      FieldName = 'GENERICO'
      Size = 1
    end
    object qListProdDESCONTO: TFloatField
      DisplayLabel = 'Desconto'
      FieldName = 'DESCONTO'
      Required = True
      DisplayFormat = '###,##0.00'
    end
  end
  object qPAtivo: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from pativo ')
    Left = 904
    Top = 24
    object qPAtivoPAT_ID: TIntegerField
      FieldName = 'PAT_ID'
    end
    object qPAtivoPATIVO: TStringField
      FieldName = 'PATIVO'
      Size = 80
    end
  end
  object MListProd: TJvMemoryData
    FieldDefs = <
      item
        Name = 'PROD_ID'
        DataType = ftInteger
      end
      item
        Name = 'DESCRICAO'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'BARRAS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LIBCIP'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRECO_VND'
        DataType = ftFloat
        Precision = 2
      end
      item
        Name = 'LISTA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LAB_ID'
        DataType = ftInteger
      end
      item
        Name = 'LABOR'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'PAT_ID'
        DataType = ftInteger
      end
      item
        Name = 'PATIVO'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CLAS_ID'
        DataType = ftInteger
      end
      item
        Name = 'CLASSE'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'GRUPO_PROD_ID'
        DataType = ftInteger
      end
      item
        Name = 'GRUPO'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'REGMS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'GENERICO'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DESCONTO'
        DataType = ftFloat
      end
      item
        Name = 'MARCADO'
        DataType = ftBoolean
      end>
    Left = 746
    Top = 95
    object MListProdPROD_ID: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'PROD_ID'
    end
    object MListProdDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 60
    end
    object MListProdBARRAS: TStringField
      DisplayLabel = 'C'#243'd. de Barras'
      FieldName = 'BARRAS'
    end
    object MListProdLIBCIP: TStringField
      DisplayLabel = 'Tipo Pre'#231'o'
      FieldName = 'LIBCIP'
    end
    object MListProdPRECO_VND: TFloatField
      DisplayLabel = 'Pre'#231'o Venda'
      FieldName = 'PRECO_VND'
      DisplayFormat = '##0.00'
    end
    object MListProdLISTA: TStringField
      DisplayLabel = 'Tipo Lista'
      FieldName = 'LISTA'
    end
    object MListProdLAB_ID: TIntegerField
      DisplayLabel = 'Lab. ID'
      FieldName = 'LAB_ID'
    end
    object MListProdLABOR: TStringField
      DisplayLabel = 'Laborat'#243'rio'
      FieldName = 'LABOR'
      Size = 60
    end
    object MListProdPAT_ID: TIntegerField
      DisplayLabel = 'Pr. Ativo ID'
      FieldName = 'PAT_ID'
    end
    object MListProdPATIVO: TStringField
      FieldName = 'PATIVO'
      Size = 60
    end
    object MListProdCLAS_ID: TIntegerField
      DisplayLabel = 'Clas. ID'
      FieldName = 'CLAS_ID'
    end
    object MListProdCLASSE: TStringField
      DisplayLabel = 'Classe'
      FieldName = 'CLASSE'
      Size = 60
    end
    object MListProdGRUPO_PROD_ID: TIntegerField
      DisplayLabel = 'Grupo ID'
      FieldName = 'GRUPO_PROD_ID'
    end
    object MListProdGRUPO: TStringField
      DisplayLabel = 'Grupo'
      FieldName = 'GRUPO'
      Size = 60
    end
    object MListProdREGMS: TStringField
      DisplayLabel = 'Reg. MS'
      FieldName = 'REGMS'
    end
    object MListProdGENERICO: TStringField
      DisplayLabel = 'Gen'#233'rico'
      FieldName = 'GENERICO'
      Size = 60
    end
    object MListProdDESCONTO: TFloatField
      DisplayLabel = 'Desconto'
      FieldName = 'DESCONTO'
      DisplayFormat = '##0.00'
    end
    object MListProdMARCADO: TBooleanField
      FieldName = 'MARCADO'
    end
  end
end
