object FMenuArv: TFMenuArv
  Left = 334
  Top = 140
  Width = 280
  Height = 431
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Menu do sistema'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TreeView1: TTreeView
    Left = 0
    Top = 0
    Width = 264
    Height = 393
    Align = alClient
    Images = FMenu.ImageList1
    Indent = 19
    ReadOnly = True
    TabOrder = 0
    OnClick = TreeView1Click
    OnKeyDown = TreeView1KeyDown
  end
end
