object FExportacaoConfabFarmacias: TFExportacaoConfabFarmacias
  Left = 353
  Top = 323
  Width = 339
  Height = 290
  Caption = 'Export. Fechamentos (CONFAB - Farm'#225'cias)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 323
    Height = 236
    Align = alClient
    TabOrder = 0
    DesignSize = (
      323
      236)
    object lblStatus: TLabel
      Left = 1
      Top = 222
      Width = 321
      Height = 13
      Align = alBottom
    end
    object Label1: TLabel
      Left = 17
      Top = 8
      Width = 136
      Height = 13
      Caption = 'Selecione o local de destino:'
    end
    object Bevel1: TBevel
      Left = 0
      Top = 188
      Width = 329
      Height = 2
    end
    object Label2: TLabel
      Left = 19
      Top = 51
      Width = 194
      Height = 13
      Caption = 'Per'#237'odo De                                            '#192
    end
    object Label3: TLabel
      Left = 19
      Top = 90
      Width = 41
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Empresa'
    end
    object Label4: TLabel
      Left = 20
      Top = 130
      Width = 28
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Notas'
    end
    object btnExportar: TButton
      Left = 120
      Top = 192
      Width = 75
      Height = 25
      Caption = '&Exportar'
      TabOrder = 0
      OnClick = btnExportarClick
    end
    object edtCaminho: TEdit
      Left = 18
      Top = 24
      Width = 274
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 289
      Top = 25
      Width = 21
      Height = 21
      Hint = 'Selecione o Diret'#243'rio'
      Anchors = [akTop, akRight]
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object lkpEmp: TDBLookupComboBox
      Left = 20
      Top = 104
      Width = 287
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      KeyField = 'EMPRES_ID'
      ListField = 'NOME'
      ListSource = dsEmp
      TabOrder = 3
    end
    object cbNotas: TComboBox
      Left = 19
      Top = 144
      Width = 286
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 4
      Text = 'TODAS'
      Items.Strings = (
        'TODAS'
        'ENTREGUES'
        'N'#195'O ENTREGUES')
    end
    object dataini: TJvDateEdit
      Left = 20
      Top = 64
      Width = 86
      Height = 21
      ShowNullDate = False
      TabOrder = 5
    end
    object datafin: TJvDateEdit
      Left = 204
      Top = 64
      Width = 89
      Height = 21
      ShowNullDate = False
      TabOrder = 6
    end
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 236
    Width = 323
    Height = 16
    Align = alBottom
    TabOrder = 1
  end
  object sd: TSaveDialog
    Filter = 'txt|*.txt'
    Left = 232
    Top = 192
  end
  object dsEmp: TDataSource
    DataSet = qEmps
    Left = 272
    Top = 112
  end
  object qCc: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'pEmpresID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select'
      'conv.chapa as matricula,'
      'conv.titular as nome,'
      'cc.nf,'
      
        'case when coalesce(cc.credito,0) <> 0 and coalesce(cc.debito,0) ' +
        '<> 0 then coalesce(cc.credito,0) - coalesce(cc.debito,0) else (c' +
        'c.debito - coalesce((select coalesce(credito,0) from contacorren' +
        'te where autorizacao_id_canc = cc.autorizacao_id),0)) end valor,'
      
        'case when coalesce(cc.receita,'#39'S'#39') = '#39'S'#39' then '#39'0001'#39' else '#39'0002'#39 +
        ' end codigo_servico,'
      
        '(select TOP 1 DATARECEITA from pres_prod_trans ppt where ppt.tra' +
        'ns_id = cc.trans_id) as DATARECEITA,'
      'cc.datavenda'
      'from contacorrente cc'
      'join conveniados conv on (conv.conv_id = cc.conv_id)'
      'join empresas emp on (emp.empres_id = conv.empres_id)'
      'join bandeiras band on (band.band_id = emp.band_id)'
      'where emp.empres_id = :pEmpresID'
      'and coalesce(cc.credito,0) <= 0'
      'and coalesce(cc.cancelada,'#39'N'#39') = '#39'N'#39
      'and cc.cred_id in (select cred.cred_id from credenciados cred'
      
        '                   where cred.seg_id in (select bs.seg_id from b' +
        'andeiras_segmentos bs'
      
        '                                         where bs.band_id = band' +
        '.band_id and bs.cod_limite in (1)))'
      '')
    Left = 88
    Top = 192
    object qCcmatricula: TFloatField
      FieldName = 'matricula'
    end
    object qCcnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object qCcnf: TIntegerField
      FieldName = 'nf'
    end
    object qCcvalor: TBCDField
      FieldName = 'valor'
      ReadOnly = True
      Precision = 16
      Size = 2
    end
    object qCccodigo_servico: TStringField
      FieldName = 'codigo_servico'
      ReadOnly = True
      Size = 4
    end
    object qCcDATARECEITA: TDateTimeField
      FieldName = 'DATARECEITA'
      ReadOnly = True
    end
    object qCcdatavenda: TDateTimeField
      FieldName = 'datavenda'
    end
  end
  object qCcCount: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'pEmpresID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select'
      'count(*) as total'
      'from contacorrente cc'
      'join conveniados conv on (conv.conv_id = cc.conv_id)'
      'join empresas emp on (emp.empres_id = conv.empres_id)'
      'join bandeiras band on (band.band_id = emp.band_id)'
      'where emp.empres_id = :pEmpresID'
      'and coalesce(cc.credito,0) <= 0'
      'and coalesce(cc.cancelada,'#39'N'#39') = '#39'N'#39
      'and cc.cred_id in (select cred.cred_id from credenciados cred'
      
        '                   where cred.seg_id in (select bs.seg_id from b' +
        'andeiras_segmentos bs'
      
        '                                         where bs.band_id = band' +
        '.band_id and bs.cod_limite in (1)))'
      '')
    Left = 48
    Top = 192
    object qCcCounttotal: TIntegerField
      FieldName = 'total'
      ReadOnly = True
    end
  end
  object qEmps: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      '*'
      'from'
      'empresas'
      'where empres_id in (455,499)')
    Left = 8
    Top = 192
    object qEmpsEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object qEmpsFORMA_LIMITE_ID: TIntegerField
      FieldName = 'FORMA_LIMITE_ID'
    end
    object qEmpsTIPO_CARTAO_ID: TIntegerField
      FieldName = 'TIPO_CARTAO_ID'
    end
    object qEmpsCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qEmpsCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object qEmpsFECHAMENTO1: TWordField
      FieldName = 'FECHAMENTO1'
    end
    object qEmpsFECHAMENTO2: TWordField
      FieldName = 'FECHAMENTO2'
    end
    object qEmpsVENCIMENTO1: TWordField
      FieldName = 'VENCIMENTO1'
    end
    object qEmpsVENCIMENTO2: TWordField
      FieldName = 'VENCIMENTO2'
    end
    object qEmpsINC_CART_PBM: TStringField
      FieldName = 'INC_CART_PBM'
      FixedChar = True
      Size = 1
    end
    object qEmpsPROG_DESC: TStringField
      FieldName = 'PROG_DESC'
      FixedChar = True
      Size = 1
    end
    object qEmpsNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object qEmpsAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object qEmpsLIBERADA: TStringField
      FieldName = 'LIBERADA'
      FixedChar = True
      Size = 1
    end
    object qEmpsFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object qEmpsNOMECARTAO: TStringField
      FieldName = 'NOMECARTAO'
      Size = 45
    end
    object qEmpsCOMISSAO_CRED: TFloatField
      FieldName = 'COMISSAO_CRED'
    end
    object qEmpsFATOR_RISCO: TFloatField
      FieldName = 'FATOR_RISCO'
    end
    object qEmpsSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object qEmpsCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object qEmpsINSCRICAOEST: TStringField
      FieldName = 'INSCRICAOEST'
      Size = 15
    end
    object qEmpsTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object qEmpsTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object qEmpsFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object qEmpsENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object qEmpsNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object qEmpsBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object qEmpsCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 30
    end
    object qEmpsCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object qEmpsESTADO: TStringField
      FieldName = 'ESTADO'
      FixedChar = True
      Size = 2
    end
    object qEmpsREPRESENTANTE: TStringField
      FieldName = 'REPRESENTANTE'
      Size = 35
    end
    object qEmpsEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 45
    end
    object qEmpsHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 45
    end
    object qEmpsOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object qEmpsOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object qEmpsTODOS_SEGMENTOS: TStringField
      FieldName = 'TODOS_SEGMENTOS'
      FixedChar = True
      Size = 1
    end
    object qEmpsDT_DEBITO: TDateTimeField
      FieldName = 'DT_DEBITO'
    end
    object qEmpsTAXA_BANCO: TStringField
      FieldName = 'TAXA_BANCO'
      FixedChar = True
      Size = 1
    end
    object qEmpsVALOR_TAXA: TBCDField
      FieldName = 'VALOR_TAXA'
      Precision = 15
      Size = 2
    end
    object qEmpsTAXA_JUROS: TBCDField
      FieldName = 'TAXA_JUROS'
      Precision = 15
      Size = 2
    end
    object qEmpsMULTA: TBCDField
      FieldName = 'MULTA'
      Precision = 15
      Size = 2
    end
    object qEmpsDESCONTO_FUNC: TBCDField
      FieldName = 'DESCONTO_FUNC'
      Precision = 6
      Size = 2
    end
    object qEmpsREPASSE_EMPRESA: TBCDField
      FieldName = 'REPASSE_EMPRESA'
      Precision = 6
      Size = 2
    end
    object qEmpsBLOQ_ATE_PGTO: TStringField
      FieldName = 'BLOQ_ATE_PGTO'
      FixedChar = True
      Size = 1
    end
    object qEmpsOBS3: TStringField
      FieldName = 'OBS3'
      Size = 255
    end
    object qEmpsPEDE_NF: TStringField
      FieldName = 'PEDE_NF'
      FixedChar = True
      Size = 1
    end
    object qEmpsPEDE_REC: TStringField
      FieldName = 'PEDE_REC'
      FixedChar = True
      Size = 1
    end
    object qEmpsACEITA_PARC: TStringField
      FieldName = 'ACEITA_PARC'
      FixedChar = True
      Size = 1
    end
    object qEmpsDESCONTO_EMP: TBCDField
      FieldName = 'DESCONTO_EMP'
      Precision = 6
      Size = 2
    end
    object qEmpsUSA_CARTAO_PROPRIO: TStringField
      FieldName = 'USA_CARTAO_PROPRIO'
      FixedChar = True
      Size = 1
    end
    object qEmpsCARTAO_INI: TIntegerField
      FieldName = 'CARTAO_INI'
    end
    object qEmpsFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object qEmpsENCONTRO_CONTAS: TStringField
      FieldName = 'ENCONTRO_CONTAS'
      FixedChar = True
      Size = 1
    end
    object qEmpsSOLICITA_PRODUTO: TStringField
      FieldName = 'SOLICITA_PRODUTO'
      FixedChar = True
      Size = 1
    end
    object qEmpsVENDA_NOME: TStringField
      FieldName = 'VENDA_NOME'
      FixedChar = True
      Size = 1
    end
    object qEmpsOBS_FECHAMENTO: TStringField
      FieldName = 'OBS_FECHAMENTO'
      Size = 1000
    end
    object qEmpsLIMITE_PADRAO: TBCDField
      FieldName = 'LIMITE_PADRAO'
      Precision = 15
      Size = 2
    end
    object qEmpsDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object qEmpsDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object qEmpsOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qEmpsDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object qEmpsOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object qEmpsVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object qEmpsAGENCIADOR_ID: TIntegerField
      FieldName = 'AGENCIADOR_ID'
    end
    object qEmpsSOM_PROD_PROG: TStringField
      FieldName = 'SOM_PROD_PROG'
      FixedChar = True
      Size = 1
    end
    object qEmpsEMITE_NF: TStringField
      FieldName = 'EMITE_NF'
      FixedChar = True
      Size = 1
    end
    object qEmpsRECEITA_SEM_LIMITE: TStringField
      FieldName = 'RECEITA_SEM_LIMITE'
      FixedChar = True
      Size = 1
    end
    object qEmpsCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object qEmpsUSA_COD_IMPORTACAO: TStringField
      FieldName = 'USA_COD_IMPORTACAO'
      FixedChar = True
      Size = 1
    end
    object qEmpsBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object qEmpsNAO_GERA_CARTAO_MENOR: TStringField
      FieldName = 'NAO_GERA_CARTAO_MENOR'
      FixedChar = True
      Size = 1
    end
    object qEmpsTIPO_CREDITO: TIntegerField
      FieldName = 'TIPO_CREDITO'
    end
    object qEmpsDIA_REPASSE: TIntegerField
      FieldName = 'DIA_REPASSE'
    end
    object qEmpsOBRIGA_SENHA: TStringField
      FieldName = 'OBRIGA_SENHA'
      FixedChar = True
      Size = 1
    end
    object qEmpsQTD_DIG_SENHA: TIntegerField
      FieldName = 'QTD_DIG_SENHA'
    end
    object qEmpsUTILIZA_RECARGA: TStringField
      FieldName = 'UTILIZA_RECARGA'
      FixedChar = True
      Size = 1
    end
    object qEmpsRESPONSAVEL_FECHAMENTO: TStringField
      FieldName = 'RESPONSAVEL_FECHAMENTO'
      Size = 15
    end
    object qEmpsMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
  end
end
