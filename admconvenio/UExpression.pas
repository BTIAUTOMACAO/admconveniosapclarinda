unit UExpression;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, math, ComCtrls;

type
  TFExpression = class(TForm)
    Campos: TListBox;
    Panel1: TPanel;
    Label1: TLabel;
    Memo1: TMemo;
    Panel2: TPanel;
    Label2: TLabel;
    ButParent1: TSpeedButton;
    ButParent2: TSpeedButton;
    ButDividir: TSpeedButton;
    ButMultip: TSpeedButton;
    ButSoma: TSpeedButton;
    ButParentdup: TSpeedButton;
    ButSub: TSpeedButton;
    ButValidate: TSpeedButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    ButDiv: TSpeedButton;
    ButMod: TSpeedButton;
    ButOk: TButton;
    ButCancel: TButton;
    But0: TSpeedButton;
    But1: TSpeedButton;
    But2: TSpeedButton;
    But3: TSpeedButton;
    But4: TSpeedButton;
    But5: TSpeedButton;
    But6: TSpeedButton;
    But7: TSpeedButton;
    But8: TSpeedButton;
    But9: TSpeedButton;
    ButPonto: TSpeedButton;
    ButClear: TSpeedButton;
    Label3: TLabel;
    Label10: TLabel;
    RichEdit1: TRichEdit;
    procedure CamposDblClick(Sender: TObject);
    procedure ButParent1Click(Sender: TObject);
    procedure Memo1KeyPress(Sender: TObject; var Key: Char);
    procedure ButClearClick(Sender: TObject);
    procedure ButParentdupClick(Sender: TObject);
    procedure ButValidateClick(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FExpression: TFExpression;

implementation

uses UTelaAltLinear, ConvUtils, calculadora, DB, UChangeLog,
  ULancTaxa, UGeraTaxa;

{$R *.dfm}

procedure TFExpression.CamposDblClick(Sender: TObject);
begin
Memo1.Text := Memo1.Text + '"'+Campos.Items[Campos.ItemIndex]+'"';
Memo1.SelStart := Length(Memo1.Lines.Text);
end;

procedure TFExpression.ButParent1Click(Sender: TObject);
begin
Memo1.Text := Memo1.Text + TSpeedButton(Sender).Caption;
Memo1.SelStart := Length(Memo1.Text);
end;

procedure TFExpression.Memo1KeyPress(Sender: TObject; var Key: Char);
begin
if Key = ',' then begin
   key := '.';
   ShowMessage('Use ponto para separar casas decimais.');
end;
if not (Key in ['0'..'9','(',')','/','*','+','-','.',#8]) then Key := #0;
end;

procedure TFExpression.ButClearClick(Sender: TObject);
begin
Memo1.Clear;
end;

procedure TFExpression.ButParentdupClick(Sender: TObject);
begin
Memo1.Text := Memo1.Text + '()';
Memo1.SelStart := Length(Memo1.Text);
end;

procedure TFExpression.ButValidateClick(Sender: TObject);
var Calc : TCalculadora; marka : TBookmark;
    i, j : integer; valor : string;
    express : String;
begin
 { express := Memo1.Text;
  if Owner.ClassNameIs('TFTelaAltLinear') then begin
    marka := FTelaAltLinear.TempCampos.GetBookmark;
    FTelaAltLinear.TempCampos.First;
    while not FTelaAltLinear.TempCampos.Eof do begin
      if Pos(FTelaAltLinear.TempCamposCampo.AsString,express) > 0 then begin
        valor := FloatToStr(ArredondaDin(FTelaAltLinear.TempCamposValor.AsCurrency));
        valor := StringReplace(Valor,',','.',[rfReplaceAll]);
        express := StringReplace(express,'"'+FTelaAltLinear.TempCamposCampo.AsString+'"',valor,[rfReplaceAll]);
      end;
    FTelaAltLinear.TempCampos.Next;
    end;
    FTelaAltLinear.TempCampos.GotoBookmark(marka);
    FTelaAltLinear.TempCampos.FreeBookmark(marka);
  end;
  if Owner.ClassNameIs('TFLancTaxa') then begin
    FLancTaxa.TempCampos.First;
    while not FLancTaxa.TempCampos.Eof do begin
      if Pos(FLancTaxa.TempCamposCampo.AsString,express) > 0 then begin
        valor := FloatToStr(ArredondaDin(FLancTaxa.TempCamposValor.AsCurrency));
        valor := StringReplace(Valor,',','.',[rfReplaceAll]);
        express := StringReplace(express,'"'+FLancTaxa.TempCamposCampo.AsString+'"',valor,[rfReplaceAll]);
      end;
      FLancTaxa.TempCampos.Next;
    end;
  end;
  Calc := TCalculadora.Create;
  for i := 1 to length(express) do if not (express[i] in ['0'..'9','(',')','/','*','+','-','.']) then delete(express,i,1);
  try
    Calc.Calcular(express);
  except on e:Exception do
    if (Pos('division by zero',LowerCase(E.message)) > 0) then begin
      msgErro('Diviz�o por zero encontrada ao tentar gerar o resultado da expre��o.');
      ButOk.Enabled := False;
      Exit;
    end;
  end;

  try
    StrToFloat(Calc.ResultStr);
    ShowMessage('Express�o validada com sucesso!'+#13+'Express�o: '+Memo1.Text+#13+
                'Substitu�da: '+express+#13+'Resultado: '+Calc.ResultStr);
    ButOk.Enabled := True;
  except
    ButOk.Enabled := False;
    ShowMessage('Erro ao tentar efetuar opera��o!'+#13+'Express�o invalida!'+#13+'Express�o: '+express);
  end;
  Calc.Free;    }  //comentado ariane
end;

procedure TFExpression.Memo1Change(Sender: TObject);
begin
  ButOk.Enabled := False;
end;

procedure TFExpression.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = vk_escape then ButCancel.Click;
if Key = vk_F1 then begin
   FChangeLog := TFChangeLog.Create(Self);
   FChangeLog.Caption := 'Ajuda para cria��o de express�es.';
   FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
   FChangeLog.ShowModal;
   FChangeLog.Free;
end;
end;

procedure TFExpression.CamposKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then CamposDblClick(nil);
end;

end.
