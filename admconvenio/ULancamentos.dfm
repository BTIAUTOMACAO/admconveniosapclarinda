inherited FLancamentos: TFLancamentos
  Left = 297
  Top = 115
  ActiveControl = EdCred_id
  Caption = 'Lan'#231'amentos de Conta Corrente'
  ClientHeight = 575
  ClientWidth = 529
  OldCreateOrder = True
  WindowState = wsNormal
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel [0]
    Left = 0
    Top = 562
    Width = 529
    Height = 13
    Align = alBottom
    Caption = '  Mensagem de retorno:'
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 72
    Width = 529
    Height = 57
    Align = alTop
    Caption = 'Estabelecimento'
    TabOrder = 1
    object Label6: TLabel
      Left = 8
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Estab. ID'
    end
    object LabCre: TLabel
      Left = 98
      Top = 16
      Width = 64
      Height = 13
      Caption = 'Raz'#227'o/Nome'
    end
    object DBCredenciado: TJvDBLookupCombo
      Left = 98
      Top = 30
      Width = 423
      Height = 21
      DropDownWidth = 350
      LookupField = 'cred_id'
      LookupDisplay = 'nome'
      LookupSource = DSQCredenciados
      TabOrder = 1
      OnChange = DBCredenciadoChange
    end
    object EdCred_id: TEdit
      Left = 8
      Top = 30
      Width = 81
      Height = 21
      TabOrder = 0
      OnChange = EdCred_idChange
      OnKeyPress = EdCred_idKeyPress
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 0
    Top = 129
    Width = 529
    Height = 55
    Align = alTop
    Caption = 'Empresa'
    TabOrder = 2
    object Label4: TLabel
      Left = 8
      Top = 14
      Width = 49
      Height = 13
      Caption = 'Empres ID'
    end
    object LabEmp: TLabel
      Left = 98
      Top = 14
      Width = 64
      Height = 13
      Caption = 'Raz'#227'o/Nome'
    end
    object DBEmpresa: TJvDBLookupCombo
      Left = 98
      Top = 29
      Width = 423
      Height = 21
      DropDownWidth = 350
      LookupField = 'empres_id'
      LookupDisplay = 'nome'
      LookupSource = DSQEmpresas
      TabOrder = 1
      OnChange = DBEmpresaChange
    end
    object EdEmpres_id: TEdit
      Left = 8
      Top = 29
      Width = 81
      Height = 21
      TabOrder = 0
      OnChange = EdEmpres_idChange
      OnKeyPress = EdCred_idKeyPress
    end
  end
  object GroupBox3: TGroupBox [3]
    Left = 0
    Top = 184
    Width = 529
    Height = 63
    Align = alTop
    Caption = 'Conv'#234'niado'
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 31
      Height = 13
      Caption = 'Cart'#227'o'
    end
    object Label2: TLabel
      Left = 125
      Top = 14
      Width = 31
      Height = 13
      Caption = 'Chapa'
    end
    object Label3: TLabel
      Left = 259
      Top = 14
      Width = 28
      Height = 13
      Caption = 'Nome'
    end
    object ButPesqConv: TSpeedButton
      Left = 497
      Top = 27
      Width = 23
      Height = 22
      Hint = 'Pesquisa de Clientes'
      Caption = '...'
      Layout = blGlyphRight
      ParentShowHint = False
      ShowHint = True
      OnClick = ButPesqConvClick
    end
    object EdCartao: TEdit
      Left = 8
      Top = 28
      Width = 110
      Height = 21
      TabOrder = 0
      OnExit = EdCartaoExit
      OnKeyPress = EdCred_idKeyPress
    end
    object EdChapa: TEdit
      Left = 126
      Top = 28
      Width = 126
      Height = 21
      TabOrder = 1
      OnExit = EdChapaExit
      OnKeyPress = EdCred_idKeyPress
    end
    object EdNome: TEdit
      Left = 259
      Top = 28
      Width = 231
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 2
      OnKeyDown = EdNomeKeyDown
    end
  end
  object f: TGroupBox [4]
    Left = 0
    Top = 247
    Width = 529
    Height = 157
    Align = alTop
    Caption = 'Dados da Autoriza'#231#227'o'
    TabOrder = 4
    DesignSize = (
      529
      157)
    object Label8: TLabel
      Left = 8
      Top = 13
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object Label9: TLabel
      Left = 132
      Top = 13
      Width = 14
      Height = 13
      Caption = 'NF'
    end
    object Label10: TLabel
      Left = 293
      Top = 13
      Width = 37
      Height = 13
      Caption = 'Receita'
    end
    object Label11: TLabel
      Left = 356
      Top = 13
      Width = 31
      Height = 13
      Caption = 'D'#233'bito'
    end
    object Label12: TLabel
      Left = 441
      Top = 13
      Width = 33
      Height = 13
      Caption = 'Cr'#233'dito'
    end
    object Label14: TLabel
      Left = 250
      Top = 65
      Width = 114
      Height = 13
      Caption = 'Hist'#243'rico do lan'#231'amento'
    end
    object Label15: TLabel
      Left = 9
      Top = 64
      Width = 201
      Height = 13
      Caption = 'Forma de Pagamento(somente se liberado)'
    end
    object LabAguarde: TLabel
      Left = 324
      Top = 129
      Width = 199
      Height = 13
      Caption = 'Aguarde! Conectando ao servidor..'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel
      Left = 229
      Top = 13
      Width = 43
      Height = 13
      Caption = 'Entregue'
    end
    object EdNf: TEdit
      Left = 132
      Top = 27
      Width = 89
      Height = 21
      TabOrder = 1
      OnKeyPress = EdCred_idKeyPress
    end
    object ComRec: TComboBox
      Left = 293
      Top = 27
      Width = 57
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 4
      Text = 'N'#227'o'
      OnKeyPress = ComRecKeyPress
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object Button1: TButton
      Left = 206
      Top = 113
      Width = 114
      Height = 31
      Caption = '&Pegar Autoriza'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = Button1Click
    end
    object EdHist: TEdit
      Left = 250
      Top = 80
      Width = 271
      Height = 21
      MaxLength = 50
      TabOrder = 7
      Text = 'Digitacao Manual'
    end
    object DBFormapgto: TDBLookupComboBox
      Left = 8
      Top = 80
      Width = 233
      Height = 21
      Hint = 
        'Somente as formas de pagamentos liberadas no sistema ser'#227'o mostr' +
        'as.'
      DropDownWidth = 250
      KeyField = 'FORMA_ID'
      ListField = 'DESCRICAO'
      ListSource = DSForma
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
    end
    object ComEntregue: TComboBox
      Left = 229
      Top = 27
      Width = 57
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = 'N'#227'o'
      OnKeyPress = ComRecKeyPress
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object Debito: TJvValidateEdit
      Left = 356
      Top = 27
      Width = 80
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      TabOrder = 5
    end
    object Credito: TJvValidateEdit
      Left = 441
      Top = 27
      Width = 80
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      TabOrder = 6
    end
    object EDData: TJvDateEdit
      Left = 8
      Top = 27
      Width = 118
      Height = 21
      Anchors = [akTop, akRight]
      ShowNullDate = False
      TabOrder = 0
    end
  end
  inherited Panel6: TPanel
    Width = 529
  end
  object Panel1: TPanel [6]
    Left = 0
    Top = 23
    Width = 529
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object Label5: TLabel
      Left = 168
      Top = 8
      Width = 289
      Height = 41
      AutoSize = False
      Caption = 
        'Com a regra de valida'#231#227'o do Web Service, '#233' verificado '#13#10'todos os' +
        ' bloqueios existentes na Administradora e habilitado '#13#10'a op'#231#227'o d' +
        'e Formas de Pagamento.'
    end
    object rdgUsaWebService: TRadioGroup
      Left = 0
      Top = 0
      Width = 161
      Height = 49
      Align = alCustom
      Caption = 'Usar Regras do Web Service'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 0
      OnClick = rdgUsaWebServiceClick
    end
  end
  object Memo1: TMemo [7]
    Left = 0
    Top = 404
    Width = 529
    Height = 158
    TabStop = False
    Align = alClient
    Lines.Strings = (
      '')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 6
  end
  inherited PopupBut: TPopupMenu
    Left = 20
    Top = 488
  end
  object DSQCredenciados: TDataSource
    DataSet = QCredenciados
    Left = 32
    Top = 445
  end
  object DSQEmpresas: TDataSource
    DataSet = QEmpresas
    Left = 72
    Top = 445
  end
  object DataSource3: TDataSource
    Left = 128
    Top = 485
  end
  object DSForma: TDataSource
    DataSet = QFormaspgto
    Left = 176
    Top = 449
  end
  object QCredenciados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select cred_id, nome, fantasia, codacesso, liberado, vencimento1'
      'from credenciados where apagado <> '#39'S'#39)
    Left = 32
    Top = 416
    object QCredenciadoscred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCredenciadosnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QCredenciadosfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object QCredenciadoscodacesso: TIntegerField
      FieldName = 'codacesso'
    end
    object QCredenciadosliberado: TStringField
      FieldName = 'liberado'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosvencimento1: TWordField
      FieldName = 'vencimento1'
    end
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select empres_id, nome, fantasia, liberada from empresas '
      'where apagado <> '#39'S'#39)
    Left = 72
    Top = 416
    object QEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
    object QEmpresasliberada: TStringField
      FieldName = 'liberada'
      FixedChar = True
      Size = 1
    end
  end
  object QConveniado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select cartoes.conv_id, cartoes.cartao_id, cartoes.nome,'
      'cartoes.codigo, cartoes.digito, cartoes.liberado as cartlib,'
      'conveniados.liberado as convlib, conveniados.chapa, '
      'conveniados.empres_id'
      'from cartoes'
      'join conveniados on conveniados.conv_id = cartoes.conv_id'
      'where conveniados.apagado <> '#39'S'#39' and cartoes.apagado <> '#39'S'#39)
    Left = 112
    Top = 416
    object QConveniadoconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QConveniadocartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object QConveniadonome: TStringField
      FieldName = 'nome'
      Size = 58
    end
    object QConveniadocodigo: TIntegerField
      FieldName = 'codigo'
    end
    object QConveniadodigito: TWordField
      FieldName = 'digito'
    end
    object QConveniadocartlib: TStringField
      FieldName = 'cartlib'
      FixedChar = True
      Size = 1
    end
    object QConveniadoconvlib: TStringField
      FieldName = 'convlib'
      FixedChar = True
      Size = 1
    end
    object QConveniadochapa: TFloatField
      FieldName = 'chapa'
    end
    object QConveniadoempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object QContaCorrente: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from contacorrente where autorizacao_id = 0')
    Left = 144
    Top = 416
    object QContaCorrenteAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QContaCorrenteCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QContaCorrenteCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QContaCorrenteCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QContaCorrenteDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QContaCorrenteDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QContaCorrenteHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object QContaCorrenteDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object QContaCorrenteDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object QContaCorrenteBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object QContaCorrenteFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object QContaCorrenteFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QContaCorrentePAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object QContaCorrenteOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QContaCorrenteDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QContaCorrenteDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QContaCorrenteDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object QContaCorrenteDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object QContaCorrenteHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object QContaCorrenteNF: TIntegerField
      FieldName = 'NF'
    end
    object QContaCorrenteDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object QContaCorrenteDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object QContaCorrenteDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object QContaCorrenteOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object QContaCorrenteOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object QContaCorrenteDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object QContaCorrenteOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object QContaCorrenteCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteNSU: TIntegerField
      FieldName = 'NSU'
    end
    object QContaCorrentePREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrenteEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object QFormaspgto: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select forma_id, descricao from formaspagto where '
      'apagado <> '#39'S'#39' and liberado = '#39'S'#39
      'order by descricao')
    Left = 176
    Top = 416
    object QFormaspgtoforma_id: TIntegerField
      FieldName = 'forma_id'
    end
    object QFormaspgtodescricao: TStringField
      FieldName = 'descricao'
      Size = 60
    end
  end
end
