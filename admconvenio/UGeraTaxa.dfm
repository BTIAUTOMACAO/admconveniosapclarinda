inherited FGeraTaxa: TFGeraTaxa
  Left = 313
  Top = 139
  Caption = 'Lan'#231'amentos lineares / Taxas'
  ClientHeight = 411
  ClientWidth = 771
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl [0]
    Left = 0
    Top = 23
    Width = 771
    Height = 388
    ActivePage = TabConv
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '&Empresas'
      object Panel1: TPanel
        Left = 0
        Top = 319
        Width = 763
        Height = 41
        Align = alBottom
        TabOrder = 2
        object ButMarcaDesmEmp: TButton
          Left = 24
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F2)'
          TabOrder = 0
          OnClick = ButMarcaDesmEmpClick
        end
        object ButMarcaTodosEmp: TButton
          Left = 128
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F3)'
          TabOrder = 1
          OnClick = ButMarcaTodosEmpClick
        end
        object ButDesmTodosEmp: TButton
          Left = 232
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F4)'
          TabOrder = 2
          OnClick = ButDesmTodosEmpClick
        end
      end
      object DBGridEmp: TJvDBGrid
        Left = 0
        Top = 68
        Width = 763
        Height = 251
        Align = alClient
        DataSource = DSEmpresas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGridEmpDrawColumnCell
        OnDblClick = DBGridEmpDblClick
        OnKeyDown = DBGridEmpKeyDown
        TitleButtons = True
        OnTitleBtnClick = DBGridEmpTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'Empres ID'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Title.Caption = 'Nome/Raz'#227'o da empresa'
            Width = 365
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dataini'
            Title.Caption = 'Data Inicial'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'datafin'
            Title.Caption = 'Data Final'
            Visible = True
          end>
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 763
        Height = 68
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 296
          Top = 25
          Width = 93
          Height = 13
          Caption = 'Dia de Fechamento'
        end
        object Label2: TLabel
          Left = 11
          Top = 9
          Width = 332
          Height = 13
          Caption = 
            'Selecione o dia de fechamento ou per'#237'odo para consulta de empres' +
            'as'
        end
        object ButListaEmp: TButton
          Left = 416
          Top = 36
          Width = 105
          Height = 24
          Caption = 'Listar Empresas (F5)'
          TabOrder = 1
          OnClick = ButListaEmpClick
        end
        object DataIni: TJvDateEdit
          Left = 297
          Top = 39
          Width = 111
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          YearDigits = dyFour
          TabOrder = 2
          OnChange = DataIniChange
        end
        object Datafin: TJvDateEdit
          Left = 417
          Top = 39
          Width = 111
          Height = 21
          NumGlyphs = 2
          ShowNullDate = False
          YearDigits = dyFour
          TabOrder = 3
          Visible = False
        end
        object Baixapor: TRadioGroup
          Left = 10
          Top = 22
          Width = 272
          Height = 38
          BiDiMode = bdLeftToRight
          Columns = 2
          Ctl3D = True
          ItemIndex = 0
          Items.Strings = (
            'Dia de Fechamento'
            'Per'#237'odo de Movimento')
          ParentBiDiMode = False
          ParentCtl3D = False
          TabOrder = 0
          OnClick = BaixaporClick
        end
      end
    end
    object TabConv: TTabSheet
      Caption = '&Titulares'
      ImageIndex = 1
      OnHide = TabConvHide
      object Panel2: TPanel
        Left = 0
        Top = 319
        Width = 763
        Height = 41
        Align = alBottom
        TabOrder = 2
        object Bevel2: TBevel
          Left = 412
          Top = 8
          Width = 2
          Height = 23
        end
        object ButMarcDesmConv: TButton
          Left = 24
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F2)'
          TabOrder = 0
          OnClick = ButMarcDesmConvClick
        end
        object ButMarcaTodosConv: TButton
          Left = 128
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F3)'
          TabOrder = 1
          OnClick = ButMarcaTodosConvClick
        end
        object ButDesmTodosConv: TButton
          Left = 232
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F4)'
          TabOrder = 2
          OnClick = ButDesmTodosConvClick
        end
        object ButProcessa: TButton
          Left = 512
          Top = 8
          Width = 154
          Height = 25
          Caption = 'Efetuar Lan'#231'amentos (F7)'
          TabOrder = 3
          OnClick = ButProcessaClick
        end
      end
      object DBGridConv: TJvDBGrid
        Left = 0
        Top = 59
        Width = 763
        Height = 260
        Align = alClient
        DataSource = DSConveniados
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGridConvDrawColumnCell
        OnDblClick = DBGridConvDblClick
        OnKeyDown = DBGridConvKeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = DBGridConvTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'Conv_id'
            ReadOnly = True
            Title.Caption = 'Conv ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Titular'
            ReadOnly = True
            Width = 265
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'empresa'
            Width = 319
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'codigo'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'digito'
            Width = 24
            Visible = True
          end>
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 763
        Height = 59
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Bevel1: TBevel
          Left = 307
          Top = 8
          Width = 2
          Height = 36
        end
        object Bevel3: TBevel
          Left = 500
          Top = 8
          Width = 2
          Height = 36
        end
        object SoComMov: TCheckBox
          Left = 9
          Top = 8
          Width = 288
          Height = 36
          Hint = 
            'Marque esta op'#231#227'o para listar apenas os conveniados com moviment' +
            'a'#231#227'o no per'#237'odo informado.'
          Caption = 
            'Somente conveniados com movimento, se a sele'#231#227'o de fornecedores ' +
            'for utilizada o sistema s'#243' procurar'#225' movimenta'#231#227'o nesses estabel' +
            'ecimentos.'
          Checked = True
          ParentShowHint = False
          ShowHint = True
          State = cbChecked
          TabOrder = 1
          WordWrap = True
          OnClick = SoComMovClick
        end
        object ButAbreConv: TButton
          Left = 520
          Top = 14
          Width = 113
          Height = 25
          Caption = '&Abrir conveniados'
          TabOrder = 2
          OnClick = ButAbreConvClick
        end
        object SoCartaoLib: TCheckBox
          Left = 320
          Top = 18
          Width = 161
          Height = 17
          Caption = 'Somente cart'#245'es liberados'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = SoCartaoLibClick
        end
        object SoConvLib: TCheckBox
          Left = 320
          Top = 3
          Width = 173
          Height = 17
          Caption = 'Somente conveniados liberados'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = SoConvLibClick
        end
        object SoCartaoTitular: TCheckBox
          Left = 320
          Top = 33
          Width = 173
          Height = 17
          Caption = 'Somente cart'#245'es de titulares'
          Checked = True
          State = cbChecked
          TabOrder = 4
          OnClick = SoConvLibClick
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'E&stabelecimentos'
      ImageIndex = 2
      object Panel4: TPanel
        Left = 0
        Top = 319
        Width = 763
        Height = 41
        Align = alBottom
        TabOrder = 2
        object ButMarcaDesmFornec: TButton
          Left = 24
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca/Desm.(F2)'
          TabOrder = 0
          OnClick = ButMarcaDesmFornecClick
        end
        object ButMarcaTodosFornec: TButton
          Left = 128
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Marca Todos (F3)'
          TabOrder = 1
          OnClick = ButMarcaTodosFornecClick
        end
        object ButDesmTodosFornec: TButton
          Left = 232
          Top = 8
          Width = 105
          Height = 25
          Caption = 'Desm. Todos (F4)'
          TabOrder = 2
          OnClick = ButDesmTodosFornecClick
        end
      end
      object DBGridFornec: TJvDBGrid
        Left = 0
        Top = 23
        Width = 763
        Height = 296
        Align = alClient
        DataSource = DSFornecedores
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGridFornecDrawColumnCell
        OnDblClick = DBGridFornecDblClick
        OnKeyDown = DBGridFornecKeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = DBGridFornecTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'cred_id'
            Title.Caption = 'Estab. ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            Title.Caption = 'Nome/Raz'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'fantasia'
            Title.Caption = 'Fantasia'
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 763
        Height = 23
        Align = alTop
        Alignment = taLeftJustify
        BorderStyle = bsSingle
        Caption = 
          ' Selecione os estabelecimentos para que o sistema verifique a mo' +
          'vimenta'#231#227'o do conveniado.'
        TabOrder = 0
      end
    end
  end
  inherited Panel6: TPanel
    Width = 771
  end
  inherited PopupBut: TPopupMenu
    Left = 324
    Top = 280
  end
  object DSEmpresas: TDataSource
    DataSet = QEmpresas
    Left = 180
    Top = 184
  end
  object DSConveniados: TDataSource
    DataSet = QConveniados
    Left = 180
    Top = 281
  end
  object DSFornecedores: TDataSource
    DataSet = QFornecedores
    Left = 180
    Top = 232
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'Select empresas.empres_id, empresas.nome, dia_fecha.data_fecha a' +
        's datafin,'
      
        '  (select top 1 dia_fecha.data_fecha from dia_fecha where dia_fe' +
        'cha.data_fecha <  '#39'13/01/2005'#39
      
        '     and dia_fecha.empres_id = empresas.empres_id order by  dia_' +
        'fecha.data_fecha desc)'
      '  as dataini, '#39'N'#39' as marcado'
      'from empresas'
      'join dia_fecha on dia_fecha.empres_id = empresas.empres_id'
      'where dia_fecha.data_fecha = '#39'13/01/2005'#39)
    Left = 140
    Top = 183
    object QEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasdatafin: TDateTimeField
      FieldName = 'datafin'
    end
    object QEmpresasdataini: TDateTimeField
      FieldName = 'dataini'
      ReadOnly = True
    end
    object QEmpresasmarcado: TStringField
      FieldName = 'marcado'
      ReadOnly = True
      Size = 1
    end
  end
  object QFornecedores: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select cred_id, nome, fantasia, '#39'N'#39' as marcado'
      'from credenciados where apagado <> '#39'S'#39'  order by nome')
    Left = 140
    Top = 231
    object QFornecedorescred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QFornecedoresnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QFornecedoresfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object QFornecedoresmarcado: TStringField
      FieldName = 'marcado'
      ReadOnly = True
      Size = 1
    end
  end
  object QConveniados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Value = Null
      end>
    SQL.Strings = (
      
        'select conveniados.Titular, conveniados.Conv_id, conveniados.Emp' +
        'res_id,'
      'conveniados.limite_mes, conveniados.salario,'
      
        'empresas.nome as empresa, cartoes.cartao_id, cartoes.codigo, car' +
        'toes.digito,'
      #39'N'#39' as marcado'
      'from conveniados '
      'join empresas on empresas.empres_id = conveniados.empres_id'
      'join cartoes on cartoes.conv_id = conveniados.conv_id'
      'where conveniados.apagado <> '#39'S'#39
      
        'and cartoes.titular = '#39'S'#39' and cartoes.liberado = '#39'S'#39' and cartoes' +
        '.apagado = '#39'N'#39
      'and conveniados.empres_id in (:empres_id) order by Titular')
    Left = 140
    Top = 279
    object QConveniadosTitular: TStringField
      FieldName = 'Titular'
      Size = 58
    end
    object QConveniadosConv_id: TIntegerField
      FieldName = 'Conv_id'
    end
    object QConveniadosEmpres_id: TIntegerField
      FieldName = 'Empres_id'
    end
    object QConveniadoslimite_mes: TBCDField
      FieldName = 'limite_mes'
      Precision = 15
      Size = 2
    end
    object QConveniadossalario: TBCDField
      FieldName = 'salario'
      Precision = 15
      Size = 2
    end
    object QConveniadosempresa: TStringField
      FieldName = 'empresa'
      Size = 60
    end
    object QConveniadoscartao_id: TIntegerField
      FieldName = 'cartao_id'
    end
    object QConveniadoscodigo: TIntegerField
      FieldName = 'codigo'
    end
    object QConveniadosdigito: TWordField
      FieldName = 'digito'
    end
    object QConveniadosmarcado: TStringField
      FieldName = 'marcado'
      ReadOnly = True
      Size = 1
    end
  end
  object QContaCorr: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from contacorrente where autorizacao_id = 0')
    Left = 284
    Top = 279
    object QContaCorrAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QContaCorrCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QContaCorrCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QContaCorrCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QContaCorrDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QContaCorrDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QContaCorrHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object QContaCorrDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object QContaCorrDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object QContaCorrCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object QContaCorrVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object QContaCorrBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object QContaCorrRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object QContaCorrTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object QContaCorrFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object QContaCorrFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QContaCorrPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QContaCorrAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object QContaCorrOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QContaCorrDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QContaCorrDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QContaCorrDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object QContaCorrDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object QContaCorrHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object QContaCorrNF: TIntegerField
      FieldName = 'NF'
    end
    object QContaCorrDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object QContaCorrDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object QContaCorrDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object QContaCorrOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object QContaCorrOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object QContaCorrDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object QContaCorrOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object QContaCorrCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object QContaCorrNSU: TIntegerField
      FieldName = 'NSU'
    end
    object QContaCorrPREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QContaCorrEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
end
