unit FRelFatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, frxClass, frxExportPDF, ADODB, frxDBSet, JvDialogs, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, StdCtrls, Buttons, ComCtrls, ExtCtrls,
  Menus, DateUtils, JvComponentBase, JvDBGridExport, JvgExportComponents;

type
  TFRelFaturas = class(TForm)
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    PanAbe: TPanel;
    Bevel2: TBevel;
    gpbDatas: TGroupBox;
    dtpMesReferencia: TDateTimePicker;
    btnConsultar: TBitBtn;
    Panel9: TPanel;
    Bevel1: TBevel;
    btnImprimir: TBitBtn;
    btnGerarPDF: TBitBtn;
    DSInformacoes: TDataSource;
    frxInformacoes: TfrxDBDataset;
    QFaturas: TADOQuery;
    DSFaturas: TDataSource;
    ReportData1: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    QInformacoes: TADOQuery;
    QFaturasDATA_BAIXA: TDateField;
    QFaturasDATA_VENCIMENTO: TDateField;
    DBGrid1: TDBGrid;
    frxDBDataset1: TfrxDBDataset;
    QFaturasFATURA_ID: TIntegerField;
    QFaturasVALOR_PAGO: TCurrencyField;
    QFaturasFATURA_PAGA: TStringField;
    QFaturasNOME: TStringField;
    QFaturasVALOR: TCurrencyField;
    QInformacoesDATAFIM: TDateField;
    QInformacoesDATAINI: TDateField;
    QInformacoesRECEBER: TCurrencyField;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    JvgExportExcel1: TJvgExportExcel;
    BitBtn3: TBitBtn;
    sv: TSaveDialog;
    sd: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure ButCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelFaturas: TFRelFaturas;
  year,month,Day : Word;
  dataInicio, dataFim : string;
  mydate : TDateTime;

implementation

uses DM, URotinasTexto, cartao_util, UValidacao, ComObj, StrUtils ,
  uDatas;

{$R *.dfm}

procedure TFRelFaturas.FormCreate(Sender: TObject);

begin

  dtpMesReferencia.Date := Now;
  if((month = 1) and (Day > 28)) then
    dtpMesReferencia.Date := StrToDate('28/02/'+IntToStr(year));

  dtpMesReferencia.Format := 'MM/yyyy';
  

end;

procedure TFRelFaturas.btnConsultarClick(Sender: TObject);

begin
  DecodeDate(dtpMesReferencia.Date,Year, Month, Day);
  if(month < 10) then begin
    dataInicio := '01/0' + IntToStr(month) + '/' + IntToStr(year);
  end else begin
    dataInicio := '01/' + IntToStr(month) + '/' + IntToStr(year);
  end;

  mydate := EndOfAMonth(year,month);
  dataFim := DateToStr(mydate);
  QFaturas.SQL.Clear;
  QFaturas.SQL.Add('SELECT  EM.NOME, FA.DATA_VENCIMENTO, FA.DATA_BAIXA, FA.FATURA_ID, FA.VALOR, FA.VALOR_PAGO,');
  QFaturas.SQL.Add(' CASE WHEN DATA_BAIXA IS NULL THEN ''N'' ELSE ''S'' END AS FATURA_PAGA');
  QFaturas.SQL.Add(' FROM FATURA FA INNER JOIN EMPRESAS EM ON FA.ID = EM.EMPRES_ID');
  QFaturas.SQL.Add(' WHERE DATA_VENCIMENTO BETWEEN '''+dataInicio +''' AND '''+ dataFim +''' ');
  QFaturas.SQL.Add(' ORDER BY FA.DATA_VENCIMENTO');
  QFaturas.Open;
  QInformacoes.SQL.Clear;
  QInformacoes.SQL.Add('SELECT SUM(CASE WHEN VALOR_PAGO IS NULL THEN VALOR ELSE 0 END) AS RECEBER,');
  QInformacoes.SQL.Add(' MAX(DATA_VENCIMENTO) AS DATAFIM,MIN(DATA_VENCIMENTO) AS DATAINI FROM FATURA ');
  QInformacoes.SQL.Add(' WHERE DATA_VENCIMENTO BETWEEN '''+dataInicio +''' AND '''+ dataFim +''' ');
end;

procedure TFRelFaturas.btnImprimirClick(Sender: TObject);
begin
  if(not QFaturas.IsEmpty) then begin
      ReportData1.ShowReport;
  end else begin
      MsgInf('Selecione um m�s para a pesquisa e clique em Abrir');
  end;

end;



procedure TFRelFaturas.ButCloseClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFRelFaturas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;

end;

procedure TFRelFaturas.btnGerarPDFClick(Sender: TObject);
begin
  if QFaturas.IsEmpty then begin
    MsgInf('Selecione um m�s para a pesquisa e clique em Abrir');
  end else begin
    try
      sd.Filter := '.pdf|.pdf';
      if sd.Execute then begin
        if ExtractFileExt(sd.FileName) = '' then
          sd.FileName := sd.FileName + '.pdf';
        frxPDFExport.FileName := sd.FileName;
        btnImprimir.Click;
        ReportData1.PrepareReport();
        ReportData1.Export(frxPDFExport);
      end;
    except
      MsgErro('Houve algum erro ao gerar o PDF.');
    end;

  end;

end;


procedure TFRelFaturas.BitBtn3Click(Sender: TObject);
begin
  inherited;
  if QFaturas.IsEmpty then begin
    MsgInf('Selecione um m�s para a pesquisa e clique em Abrir');
  end else begin
    try
      sd.Filter := '.xlsx|.xlsx';
      if sv.Execute then begin
        if ExtractFileExt(sv.FileName) = '' then begin
          JvgExportExcel1.SaveToFileName := sv.FileName+'.xlsx';
          JvgExportExcel1.CloseExcel := True;
          JvgExportExcel1.Header.Clear;
          JvgExportExcel1.SubHeader.Clear;
          JvgExportExcel1.Header.Add('Relat�rio de Faturas Pagas') ;
          //JvgExportExcel1.SubHeader.Add('Empresa: '+QEmpresasnome.AsString+' - Setor: '+qry1SETOR.AsString);
          JvgExportExcel1.Execute;
        end;
      end;
    except
      MsgErro('Houve algum erro ao tentar exportar a planilha.');
    end;
  end;

end;


end.
