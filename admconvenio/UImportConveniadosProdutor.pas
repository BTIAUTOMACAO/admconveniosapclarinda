unit UImportConveniadosProdutor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, Mask, ToolEdit, ExtCtrls, Buttons,
  DB, ZAbstractRODataset, ZDataset, {JvLookup,} ADODB, ZStoredProcedure,
  ZSqlUpdate, ZAbstractDataset, JvToolEdit, ShellApi, JvExControls,
  JvDBLookup, JvExMask;

type
  TFImportConveniadosProdutor = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    ButAjuda: TSpeedButton;
    Bevel1: TBevel;
    //FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    RichEdit1: TRichEdit;
    ProgressBar1: TProgressBar;
    Empresas: TJvDBLookupCombo;
    DSEmpresas: TDataSource;
    Table1: TADOTable;
    FilenameEdit1: TJvFilenameEdit;
    QConv: TADOQuery;
    QConvCONV_ID: TIntegerField;
    QConvEMPRES_ID: TIntegerField;
    QConvCHAPA: TFloatField;
    QConvSENHA: TStringField;
    QConvTITULAR: TStringField;
    QConvCONTRATO: TIntegerField;
    QConvLIMITE_MES: TBCDField;
    QConvLIMITE_TOTAL: TBCDField;
    QConvLIMITE_PROX_FECHAMENTO: TBCDField;
    QConvLIBERADO: TStringField;
    QConvBANCO: TIntegerField;
    QConvTIPOPAGAMENTO: TStringField;
    QConvDTCADASTRO: TDateTimeField;
    QConvDTALTERACAO: TDateTimeField;
    QConvOPERADOR: TStringField;
    QConvSALARIO: TBCDField;
    QConvAPAGADO: TStringField;
    QCartao: TADOQuery;
    QCartaoCARTAO_ID: TIntegerField;
    QCartaoCONV_ID: TIntegerField;
    QCartaoNOME: TStringField;
    QCartaoLIBERADO: TStringField;
    QCartaoCODIGO: TIntegerField;
    QCartaoDIGITO: TWordField;
    QCartaoTITULAR: TStringField;
    QCartaoJAEMITIDO: TStringField;
    QCartaoAPAGADO: TStringField;
    QCartaoLIMITE_MES: TBCDField;
    QCartaoCODCARTIMP: TStringField;
    QCartaoPARENTESCO: TStringField;
    QCartaoDATA_NASC: TDateTimeField;
    QCartaoNUM_DEP: TIntegerField;
    QCartaoFLAG: TStringField;
    QCartaoDTEMISSAO: TDateTimeField;
    QCartaoCPF: TStringField;
    QCartaoRG: TStringField;
    QCartaoVIA: TIntegerField;
    QCartaoDTAPAGADO: TDateTimeField;
    QCartaoDTALTERACAO: TDateTimeField;
    QCartaoOPERADOR: TStringField;
    QCartaoDTCADASTRO: TDateTimeField;
    QCartaoOPERCADASTRO: TStringField;
    QCartaoCRED_ID: TIntegerField;
    QCartaoATIVO: TStringField;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasUSA_COD_IMPORTACAO: TStringField;
    QConvGRUPO_CONV_EMP: TIntegerField;
    cbPlanHasDependente: TCheckBox;
    QCartaoSENHA: TStringField;
    QCartaoCVV: TStringField;
    QCartaoEMPRES_ID: TIntegerField;
    QEmpresasMOD_CART_ID: TIntegerField;
    QCartaoSERIE: TStringField;
    QCartaoMODELO_CARTAO_CANTINEX: TIntegerField;
    QConvSERIE: TStringField;
    QConvMODELO_CARTAO_CANTINEX: TIntegerField;
    cbImportaApenasDependente: TCheckBox;
    QConvCPF: TStringField;
    QConvRG: TStringField;
    QConvDT_NASCIMENTO: TDateTimeField;
    QCartaodt_nascimento_cartao: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButAjudaClick(Sender: TObject);
    procedure EmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure GeraCartao(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
    procedure GeraCartaoDependente (Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
    function VerificaSeChapaJaExiste(Chapa : String; Empres_id : String) : Boolean;
    procedure GeraCartaoNovo(Nome: String; cpf: String; rg: String; dataNasc: String; Conv_id: integer; Empres_id: integer; Chapa:string);
  public
    { Public declarations }
  end;

var
  FImportConveniadosProdutor: TFImportConveniadosProdutor;

implementation

uses DM, UChangeLog, UMenu, cartao_util, UValidacao, Math;

{$R *.dfm}

procedure TFImportConveniadosProdutor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end;
end;

procedure TFImportConveniadosProdutor.FormCreate(Sender: TObject);
begin
  QEmpresas.Open;
  FilenameEdit1.Text:= '"'+FMenu.GetPersonalFolder+'\"';
end;

procedure TFImportConveniadosProdutor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresas.Close;
end;

procedure TFImportConveniadosProdutor.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFImportConveniadosProdutor.EmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
    Perform(WM_NEXTDLGCTL,0,0);
  end;
end;


function Split(const Str: string; Delimiter: Char): TStringList;
begin
  Result := TStringList.Create;
  //Result. := Delimiter;
  Result.Text := Str;
end;
procedure TFImportConveniadosProdutor.Button1Click(Sender: TObject);
var path, nome, chapa,serieDoAluno, cpf, rg, dataNasc, numdep: String;
  conv_id, qtdImp, qtdErr,modeloCartaoCantinex ,grupo_conv_emp,cont,empres_id,I,quantidadeDePalavras : Integer;
  limite : Double;
  hasDependente : Boolean;
  lista,listaNomesParaInserir : TStringlist;
  erro: Boolean;
  item : TListItem;
begin
  hasDependente := False;
  qtdImp:= 0; qtdErr:= 0;
  erro:= False;
  screen.Cursor  := crHourGlass;
  if Empresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  try
    path := '';
    if versaoOffice < 12 then
    begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end
    else
    begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties="Excel 12.0 Xml;HDR=YES;"';
    end;
    Table1.Active := False;
    Table1.ConnectionString := path;
    Table1.TableName:= 'conv$';
    Table1.Active := True;
  except on E:Exception do
    ShowMessage('UImportConveniadosProdutor_199 - Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  ProgressBar1.Position := 0;
  Button1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
  QConv.Open;
  QCartao.Open;
  lista := TStringList.Create;
  //listaNomesParaInserir := TStringList.Create;
  while not Table1.Eof do
  begin
   empres_id := Empresas.KeyValue;

   if cbImportaApenasDependente.Checked then begin
     numdep := Table1.FieldByName('NUMDEP').AsString;
     hasDependente := True;
     cont := 1;
          if (numdep <> '') then begin
              if hasDependente then begin
                while(cont <= StrToInt(numdep)) do begin
                  chapa := SoNumero(Table1.FieldByName('chapa').AsString);

                  DMConexao.AdoQry.Close;
                  DMConexao.AdoQry.SQL.Clear;
                  DMConexao.AdoQry.SQL.Add('select c.conv_id from CARTOES c inner join conveniados conv on (conv.conv_id = c.conv_id)  where c.empres_id = '+ IntToStr(empres_id) +' and conv.chapa = ' + chapa);
                  DMConexao.AdoQry.Open;

                  conv_id := DMConexao.AdoQry.Fields[0].Value;
                  GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('DEP' + IntToStr(cont)).AsString))),conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
                  //GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString))),conv_id, empres_id,chapa);

                  cont := cont + 1;
                end;
                  hasDependente := False; //Zerando o valor da variavel has Dependente.
              end
          end;

      //conv_id := ('select c.conv_id from CARTOES c inner join conveniados conv on (conv.conv_id = c.conv_id)  where c.empres_id ="'+ QuotedStr(empres_id) +'" and conv.chapa = "' + QuotedStr(chapa)+'"');
      //GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('DEP' + IntToStr(cont)).AsString))),conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
      //GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString))),conv_id, empres_id,chapa);
      //cont := cont + 1;

   end
   else begin
     //Importa conveniado apenas se a chapa ainda n�o estiver cadastrada.
     if((not VerificaSeChapaJaExiste(SoNumero(Table1.FieldByName('CHAPA').AsString),IntToStr(empres_id))) or (QEmpresasMOD_CART_ID.AsInteger = 8))then
     begin
      if cbPlanHasDependente.Checked = True then begin
          numdep := Table1.FieldByName('NUMDEP').AsString;
          if (numdep <> '') then begin
              hasDependente := True;
          end;
      end;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONV_ID');
      DMConexao.AdoQry.Open;

      conv_id := DMConexao.AdoQry.Fields[0].Value;
      nome    := UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString)));
      cpf     := sonumero(Table1.FieldByName('CPF').AsString);
      rg      := sonumero(Table1.FieldByName('RG').AsString);
      dataNasc:= Table1.FieldByName('DT_NASCIMENTO').AsString;

      if QEmpresasMOD_CART_ID.AsInteger = 8 then
      begin
        chapa   := '0' ;
        serieDoAluno := Table1.FieldByName('CHAPA').AsString ;
        modeloCartaoCantinex := Table1.FieldByName('MODELO').AsInteger;
      end
        else
          chapa   := SoNumero(Table1.FieldByName('CHAPA').AsString);

      limite  := Table1.FieldByName('LIMITE').AsFloat;

      QConv.Insert;
      try
        QConv.FieldByName('CONV_ID').AsInteger                := conv_id;
        QConv.FieldByName('EMPRES_ID').AsInteger              := QEmpresasEMPRES_ID.AsInteger;

        QConv.FieldByName('CHAPA').AsString                   := chapa;
        DMConexao.Config.Open;
        if DMConexao.ConfigSENHA_CONV_ID.AsString = 'S' then
        begin
          QConv.FieldByName('SENHA').AsString                := Crypt('E', IntToStr(conv_id), 'BIGCOMPRAS');
        end
        else
        begin
          QConv.FieldByName('SENHA').AsString                := Crypt('E', '1111', 'BIGCOMPRAS');
        end;
        DMConexao.Config.Close;
        QConv.FieldByName('TITULAR').AsString                 := nome;
        QConv.FieldByName('CPF').AsString                     := cpf;
        QConv.FieldByName('RG').AsString                      := rg;
        QConv.FieldByName('DT_NASCIMENTO').AsString           := dataNasc;
        QConv.FieldByName('CONTRATO').AsInteger               := conv_id;
        QConv.FieldByName('LIMITE_MES').AsFloat               := limite;
        QConv.FieldByName('LIMITE_TOTAL').AsFloat             := 0;
        QConv.FieldByName('LIMITE_PROX_FECHAMENTO').AsFloat   := 0;
        QConv.FieldByName('LIBERADO').AsString                := 'S';
        QConv.FieldByName('BANCO').AsInteger                  := 0;
        QConv.FieldByName('TIPOPAGAMENTO').AsString           := 'N';
        QConv.FieldByName('DTCADASTRO').AsDateTime            := Date;
        QConv.FieldByName('DTALTERACAO').AsDateTime           := Date;
        QConv.FieldByName('OPERADOR').AsString                := 'CONVERSOR';
        QConv.FieldByName('SALARIO').AsFloat                  := 0;
        QConv.FieldByName('APAGADO').AsString                 := 'N';
        if QEmpresasMOD_CART_ID.AsInteger = 8 then
        begin
          QConv.FieldByName('SERIE').AsString         := Table1.FieldByName('SERIE').AsString;
          QConv.FieldByName('MODELO_CARTAO_CANTINEX').AsInteger     := Table1.FieldByName('MODELO').AsInteger;
        end;
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('SELECT GRUPO_CONV_EMP_ID from GRUPO_CONV_EMP WHERE EMPRES_ID = '+QEmpresasempres_id.AsString);
        DMConexao.AdoQry.Open;
        QConv.FieldByName('GRUPO_CONV_EMP').AsInteger         := DMConexao.AdoQry.Fields[0].AsInteger;
        QConv.Post;
        qtdImp:= qtdImp + 1;
        cont := 0;
        if hasDependente then begin
          while(cont <= StrToInt(numdep)) do
          begin
            if cont = 0 then
              GeraCartaoNovo(nome,cpf, rg, dataNasc, conv_id, QEmpresasEMPRES_ID.AsInteger,chapa)
              else
                GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('DEP' + IntToStr(cont)).AsString))),conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
                //GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString))),conv_id, empres_id,chapa);
            cont := cont + 1;
          end;
          hasDependente := False; //Zerando o valor da variavel has Dependente.
        end
        else begin
          GeraCartaoNovo(nome,cpf, rg, dataNasc, conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
        end;
      except
        on E:Exception do
        begin
          erro:= True;
          qtdErr:= qtdErr + 1;
          lista.Append('UImportConveniadosProdutor_309 Erro: '+E.Message+' Cliente: '+Table1.FieldByName('NOME').AsString);
          QConv.Cancel;
        end;
      end;
     end;
     //FINALIZA AQUIIIIIIIIIIIIII
   end;

    Table1.Next;
    Application.ProcessMessages;
    ProgressBar1.Position := ProgressBar1.Position +1;
  end;
  Button1.Enabled := True;
  screen.Cursor := crDefault;
  Table1.Close;
  QConv.Close;
  QCartao.Close;
  lista.SaveToFile(FMenu.GetPersonalFolder+'\erros.txt');
  lista.Free;
  ProgressBar1.Position := 0;
  if erro then
  begin
    if MsgSimNao('UImportConveniadosProdutor_329 A Importa��o foi efetuada, por�m, ocorreram '+IntToStr(qtdErr)+' erro(s) na importa��o. '+sLineBreak+
    'foi gerado um relat�rio e salvo no diret�rio: '+FMenu.GetPersonalFolder+
    '\Erros.txt'+sLineBreak+'Deseja visualizar o relat�rio de erros?') then
      ShellExecute(Handle,'open','c:\windows\notepad.exe',PChar(FMenu.GetPersonalFolder+'\Erros.txt'),nil, SW_SHOWNORMAL);
  end
  else
  begin
    MsgInf('Importa��o Realizada com Sucesso.'+sLineBreak+IntToStr(qtdImp)+' Conveniados Importados');
  end;
end;

function TFImportConveniadosProdutor.VerificaSeChapaJaExiste(Chapa : String; Empres_id : String) : Boolean;
var chapaBD : String;
begin
  chapaBD := DMConexao.ExecuteScalar('SELECT COALESCE((select TOP 1 coalesce(chapa,0) from conveniados where empres_id = '+Empres_id+' AND CHAPA = '+Chapa+'),0)');
  if(chapaBD <> '0') then
    Result := True
  else
    Result := False;
end;

procedure TFImportConveniadosProdutor.GeraCartao(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
var
  cartao_id, codigo_cartao : Integer;
  codimp: string;
begin
  cartao_id                                     := DMConexao.getGeneratorValue('SCARTAO_ID');
  codigo_cartao                                 := DMConexao.getGeneratorValue('SCARTAO_NUM');
  QCartao.Append;
  QCartao.FieldByName('CARTAO_ID').AsInteger    := cartao_id;
  QCartao.FieldByName('CONV_ID').AsInteger      := conv_id;
  QCartao.FieldByName('NOME').AsString          := UpperCase(nome);
  //QCartao.FieldByName('CPF').AsString           := cpf;
  //QCartao.FieldByName('RG').AsString            := rg;
  QCartao.FieldByName('LIBERADO').AsString      := 'N';
  QCartao.FieldByName('CODIGO').AsInteger       := codigo_cartao;
  QCartao.FieldByName('DIGITO').AsInteger       := DigitoCartao(codigo_cartao);
  QCartao.FieldByName('LIMITE_MES').AsFloat     := 0;
  QCartao.FieldByName('JAEMITIDO').AsString     := 'N';
  QCartao.FieldByName('TITULAR').AsString       := 'S';
  QCartao.FieldByName('DTALTERACAO').AsDateTime := Date;
  QCartao.FieldByName('DTCADASTRO').AsDateTime  := Date;
  QCartao.FieldByName('OPERADOR').AsString      := 'CONVERSOR';
  QCartao.FieldByName('APAGADO').AsString       := 'N';
  QCartao.FieldByName('ATIVO').AsString         := 'S';
  DMConexao.Config.Open;
  if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
    until (verificaCartaoExistente(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;

  QCartao.FieldByName('OPERADOR').AsString      :=  Operador.Nome;
  QCartao.Post;
end;



procedure TFImportConveniadosProdutor.GeraCartaoNovo(Nome: String; cpf: String; rg: String; dataNasc: String;  Conv_id: integer; Empres_id: integer; Chapa:string);
var
  cartao_id, codigo_cartao : Integer;
  codimp,cvv: string;
begin
  //cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');

  cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');
  codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');
  //codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');

  QCartao.Append;
  QCartao.FieldByName('CARTAO_ID').AsInteger            := cartao_id;
  QCartao.FieldByName('CONV_ID').AsInteger              := conv_id;
  QCartao.FieldByName('NOME').AsString                  := UpperCase(nome);
  QCartao.FieldByName('LIBERADO').AsString              := 'N';
  QCartao.FieldByName('CPF').AsString                   := cpf;
  QCartao.FieldByName('RG').AsString                    := rg;
  QCartao.FieldByName('DATA_NASC').AsString             := dataNasc;
  QCartao.FieldByName('CODIGO').AsInteger               := codigo_cartao;
  QCartao.FieldByName('DIGITO').AsInteger               := DigitoCartao(codigo_cartao);
  QCartao.FieldByName('LIMITE_MES').AsFloat             := 0;
  QCartao.FieldByName('JAEMITIDO').AsString             := 'N';
  QCartao.FieldByName('TITULAR').AsString               := 'S';
  QCartao.FieldByName('DTALTERACAO').AsDateTime         := Date;
  QCartao.FieldByName('DTCADASTRO').AsDateTime          := Date;
  QCartao.FieldByName('OPERADOR').AsString              := 'CONVERSOR';
  QCartao.FieldByName('APAGADO').AsString               := 'N';
  QCartao.FieldByName('EMPRES_ID').AsInteger            := Empres_id;
  QCartao.FieldByName('ATIVO').AsString                 := 'S';
  if QEmpresasMOD_CART_ID.AsInteger = 8 then
  begin
    QCartao.FieldByName('SERIE').AsString         := Table1.FieldByName('SERIE').AsString;
    QCartao.FieldByName('MODELO_CARTAO_CANTINEX').AsInteger     := Table1.FieldByName('MODELO').AsInteger;
  end;
  DMConexao.Config.Open;
  if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasMOD_CART_ID.AsInteger = 6 then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigBIN_BEM_ESTAR.AsInteger));
    until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp) and verificaCartaoExistenteHistorico(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if QEmpresasMOD_CART_ID.AsInteger = 8 then begin
    repeat
      codimp := RemoveCaracter(gerarCartaoCantinex(DMConexao.ConfigBIN_CANTINA.AsString));
    until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp) and verificaCartaoExistenteHistorico(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
    until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp) and verificaCartaoExistenteHistorico(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;
  QCartao.FieldByName('SENHA').AsString   :=  Crypt('E',Copy(QCartao.FieldByName('CODCARTIMP').AsString, 13, 4),'BIGCOMPRAS');

  //Gera o CVV
  cvv := GerarCVV(QCartao.FieldByName('CODCARTIMP').AsString);
  QCartaoCVV.AsString := cvv;
  QCartao.Post;

  DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE() where CARTAO_ID = '+IntToStr(cartao_id)+'');
end;

procedure TFImportConveniadosProdutor.GeraCartaoDependente(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
var
  cartao_id, codigo_cartao : Integer;
  codimp: string;
begin
  cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');
  codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');
  QCartao.Append;
  QCartao.FieldByName('CARTAO_ID').AsInteger    := cartao_id;
  QCartao.FieldByName('CONV_ID').AsInteger      := conv_id;
  QCartao.FieldByName('NOME').AsString          := UpperCase(nome);
  QCartao.FieldByName('LIBERADO').AsString      := 'N';
  QCartao.FieldByName('CODIGO').AsInteger       := codigo_cartao;
  QCartao.FieldByName('DIGITO').AsInteger       := DigitoCartao(codigo_cartao);
  QCartao.FieldByName('LIMITE_MES').AsFloat     := 0;
  QCartao.FieldByName('JAEMITIDO').AsString     := 'N';
  QCartao.FieldByName('TITULAR').AsString       := 'N';
  QCartao.FieldByName('DTALTERACAO').AsDateTime := Date;
  QCartao.FieldByName('DTCADASTRO').AsDateTime  := Date;
  QCartao.FieldByName('OPERADOR').AsString      := 'CONVERSOR';
  QCartao.FieldByName('APAGADO').AsString       := 'N';
  QCartao.FieldByName('EMPRES_ID').AsInteger    := Empres_id;
  QCartao.FieldByName('ATIVO').AsString         := 'S';
  DMConexao.Config.Open;
  if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
    until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp) and verificaCartaoExistenteHistorico(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;

  QCartao.FieldByName('SENHA').AsString   :=  Crypt('E',Copy(QCartao.FieldByName('CODCARTIMP').AsString, 13, 4),'BIGCOMPRAS');

  //Gera o CVV
  QCartaoCVV.AsString := GerarCVV(QCartao.FieldByName('CODCARTIMP').AsString);
  QCartao.Post;

  DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE() where CARTAO_ID = '+IntToStr(cartao_id)+'');
end;

end.
