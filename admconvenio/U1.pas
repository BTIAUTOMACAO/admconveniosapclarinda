unit U1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, Buttons, ExtCtrls, DBGrids, JvDBLookup, midasLib, db;

type
  TF1 = class(TForm)
    PopupBut: TPopupMenu;
    Restaurar1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    Minimizar1: TMenuItem;
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure Restaurar1Click(Sender: TObject);
    procedure PopupButPopup(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BFClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Minimizar1Click(Sender: TObject);
    procedure Panel6Resize(Sender: TObject);
    procedure ButCloseClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
  private
    { Private declarations }
    function GetTextTitulo:String;
    procedure SetTextTitulo(Text:String);
  public
    { Public declarations }
    but, sep : TToolButton;
    procedure ShowForm(Sender:TObject);
    property TextTitulo : String read GetTextTitulo write SetTextTitulo;
  end;

var
  F1: TF1;

implementation

uses UMenu, UMenuArv, DM, StdCtrls, UDBGridHelper, UStringGridHelper;

{$R *.dfm}

procedure TF1.SetTextTitulo(Text:String);
begin
panTitulo.Caption := Text;
end;

function TF1.GetTextTitulo:String;
begin
Result := panTitulo.Caption;
end;

procedure TF1.FormCreate(Sender: TObject);
begin
//Rotina para a cria��o do bot�o na barra de tarefas do programa.
but         := TToolButton.Create(self);
sep         := TToolButton.Create(self);
but.Name    := Name+'1';
sep.Name    := Name+'2';
but.Caption := Caption;
but.Hint    := Caption;
but.ShowHint := True;
but.OnClick := ShowForm;
but.PopupMenu := PopupBut;
if FMenu.Barra.ButtonCount > 0 then begin
   FMenu.Barra.AutoSize := False;
   sep.Left := FMenu.Barra.Buttons[FMenu.Barra.ButtonCount-1].Left+1;
   but.Left := sep.Left+1;
end;
sep.Style   := tbsSeparator;
FMenu.Barra.InsertControl(sep);
FMenu.Barra.InsertControl(but);
but.Click;

TDBGridHelper.DBGridHelperTratrForm(Self,DMConexao.AdoQry,Operador.ID);
TStringGridHelper.StringGridHelperTratrForm(Self);
end;

procedure TF1.ShowForm(Sender:TObject);
var i : integer;
begin
  for i := 0 to FMenu.Barra.ButtonCount-1 do FMenu.Barra.Buttons[i].Down := False;
  if Self.WindowState <> wsMaximized then Self.WindowState := wsMaximized;
  Self.Show;
  TToolButton(Sender).Down := True;
end;


procedure TF1.Restaurar1Click(Sender: TObject);
begin
Self.Show;
end;

procedure TF1.PopupButPopup(Sender: TObject);
begin
  Restaurar1.Enabled := WindowState <> wsMaximized;
  Minimizar1.Enabled := WindowState = wsMaximized;
end;

procedure TF1.FormDeactivate(Sender: TObject);
var i : integer;
begin
for i := 0 to FMenu.Barra.ButtonCount-1 do FMenu.Barra.Buttons[i].Down := False;
end;

procedure TF1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TF1.BFClick(Sender: TObject);
begin
  Close;
end;

procedure TF1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
If key = vk_escape then if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then close;
if ((Key = VK_RETURN) and (not (ActiveControl is TDBGrid))) then begin
   if (ActiveControl is TJvDBLookupCombo) then begin
      if TJvDBLookupCombo(ActiveControl).ListVisible then begin
         TJvDBLookupCombo(ActiveControl).CloseUp(True);
         exit;
      end;
   end
   else if (ActiveControl is TCustomComboBox) then begin                          // TCustomComboBox � progenitora de TComboBox, TJvComboBox, etc...
      if TCustomComboBox(ActiveControl).DroppedDown then begin
         TCustomComboBox(ActiveControl).DroppedDown := False;
         exit;
      end;
   end;
  SelectNext(TForm(Sender).ActiveControl,true,true);
end;
end;

procedure TF1.Minimizar1Click(Sender: TObject);
begin
Self.WindowState := wsMinimized;
end;

procedure TF1.Panel6Resize(Sender: TObject);
begin
  panTitulo.Top:= 1;
  panTitulo.Left:= 1;
  panTitulo.Height:= 20;
  panTitulo.Width:= Panel6.Width - 3;
  ButClose.Left := Panel6.Width - (ButClose.Width+4);
end;

procedure TF1.ButCloseClick(Sender: TObject);
begin
Self.Close;
end;

procedure TF1.FormActivate(Sender: TObject);
begin
Self.WindowState := wsMaximized;
but.Down         := True;
end;

procedure TF1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var i : integer;
begin
for i := 0 to ComponentCount - 1 do
  if Components[i] is TDataSet then
    if TDataSet(Components[i]).Active Then
      TDataSet(Components[i]).Close;
end;


procedure TF1.FormShow(Sender: TObject);
begin
  Self.WindowState:= wsMaximized;
  Self.SetTextTitulo('  '+self.Caption);
end;

procedure TF1.Fechar1Click(Sender: TObject);
begin
  Self.Close;
end;

end.
