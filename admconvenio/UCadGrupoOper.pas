unit UCadGrupoOper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,}
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, {JvMemDS,} Mask, JvToolEdit,
  ZSqlUpdate, Menus, JvExMask, JvExDBGrids, JvDBGrid, ADODB;

type
  TFCadGrupoOper = class(TFCad)
    QCadastroGRUPO_USU_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroADMINISTRADOR: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroAPAGADO: TStringField;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    dbEdtDesc: TDBEdit;
    CheckLib: TDBCheckBox;
    heckADM: TDBCheckBox;
    DSModulos: TDataSource;
    TabPermis: TTabSheet;
    GroupBox2: TGroupBox;
    JvDBGrid1: TJvDBGrid;
    JvDBGrid2: TJvDBGrid;
    Label5: TLabel;
    Label6: TLabel;
    
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    DSPermissoes: TDataSource;
    QCadastroOPERADOR: TStringField;
    SpeedButton1: TSpeedButton;
    GroupBox3: TGroupBox;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    QCadastroNIVEL_ALFABETICO: TStringField;
    QCadastroNIVEL_NUMERICO: TSmallintField;
    Label7: TLabel;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QPermissoes: TADOQuery;
    QModulos: TADOQuery;
    QModulosMODULO_ID: TIntegerField;
    QModulosDESCRICAO: TStringField;
    QModulosNOME: TStringField;
    QPermissoesPERM_ID: TIntegerField;
    QPermissoesGRUPO_ID: TIntegerField;
    QPermissoesMODULO_ID: TIntegerField;
    QPermissoesINCLUI: TStringField;
    QPermissoesALTERA: TStringField;
    QPermissoesEXCLUI: TStringField;
    QPermissoesACESSA: TStringField;
    QPermissoesmodulo: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure TabPermisShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure QPermissoesBeforePost(DataSet: TDataSet);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure JvDBGrid2TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure QPermissoesAfterScroll(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    
  end;
  TCustomDBGridCracker = class(TCustomDBGrid);
var
  FCadGrupoOper: TFCadGrupoOper;

implementation

uses DM, UMenu, UValidacao;

{$R *.dfm}

procedure TFCadGrupoOper.FormCreate(Sender: TObject);
begin
  chavepri := 'grupo_usu_id';
  detalhe  := 'Grupo ID: ';
  inherited;
end;

procedure TFCadGrupoOper.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Grupo de Operador: '+ QCadastroDESCRICAO.AsString;
end;

procedure TFCadGrupoOper.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select * from grupo_usuarios where apagado <> ''S'' ');
  if Trim(EdCod.Text) <> '' then QCadastro.SQL.Add(' and grupo_usu_id in ('+EdCod.Text+')')
  else if Trim(EdNome.Text) <> '' then QCadastro.SQL.Add(' and descricao like '+QuotedStr('%'+EdNome.Text+'%'));
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus;                                     
end;

procedure TFCadGrupoOper.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtDesc.SetFocus;
  QCadastroAPAGADO.AsString           := 'N';
  QCadastroADMINISTRADOR.AsString     := 'N';
  QCadastroLIBERADO.AsString          := 'S';
  QCadastroNIVEL_ALFABETICO.AsString  := 'Z';
  QCadastroNIVEL_NUMERICO.AsInteger   := 9;
end;

procedure TFCadGrupoOper.TabPermisShow(Sender: TObject);
begin
  inherited;
  If not QCadastro.IsEmpty then begin
     if QCadastroADMINISTRADOR.AsString = 'S' then begin
        ShowMessage('Grupo de administradores, todos m�dulos permitidos.');
        PageControl1.ActivePage := TabFicha;
     end
     else begin
        QPermissoes.Close;
        QPermissoes.Sql.Text := ' Select * from permissoes where grupo_id = '+QCadastroGRUPO_USU_ID.AsString;
        QPermissoes.Open;
        //QPermissoes.SortedFields := 'modulo';
        QPermissoes.First;
        QModulos.First;
        Screen.Cursor := crDefault;
     end;   
  end;
end;

procedure TFCadGrupoOper.SpeedButton1Click(Sender: TObject);
var marka : TBookmark;
begin
  if QModulos.IsEmpty then exit;
//  marka := QPermissoes.GetBookmark;
  QPermissoes.DisableControls;
  if not QPermissoes.Locate('modulo_id',QModulosmodulo_id.AsInteger,[]) then begin
     QPermissoes.Append;
     QPermissoesgrupo_id.AsInteger := QCadastroGRUPO_USU_ID.AsInteger;
     QPermissoesmodulo_id.AsInteger := QModulosmodulo_id.AsInteger;

     QPermissoes.Post;
     //Usar um refresh para buscar os valores gerados no bd, e usar o locate para parar no modulo rec�m inserido.
     QPermissoes.Refresh;
     QPermissoes.RecNo;
     QPermissoes.Locate('modulo_id',QModulosmodulo_id.AsInteger,[]);
     QPermissoes.RecNo;

  end;
//  QPermissoes.GotoBookmark(marka);
//  QPermissoes.FreeBookmark(marka);
  QPermissoes.EnableControls;
end;

procedure TFCadGrupoOper.SpeedButton3Click(Sender: TObject);
var marka, markaMod : TBookmark;
begin
  if QModulos.IsEmpty then exit;
  marka := QPermissoes.GetBookmark;
  markaMod := QModulos.GetBookmark;
  QPermissoes.DisableControls;
  QModulos.DisableControls;
  QModulos.First;
  while not QModulos.Eof do begin
     if not QPermissoes.Locate('modulo_id',QModulosmodulo_id.AsInteger,[]) then begin
        QPermissoes.Append;
        QPermissoesgrupo_id.AsInteger := QCadastroGRUPO_USU_ID.AsInteger;
        QPermissoesmodulo_id.AsInteger := QModulosmodulo_id.AsInteger;
        QPermissoes.Post;
     end;
     QModulos.Next;
  end;
  QPermissoes.GotoBookmark(marka);
  QModulos.GotoBookmark(markaMod);
  QModulos.FreeBookmark(markaMod);
  QPermissoes.FreeBookmark(marka);
  QPermissoes.EnableControls;
  QModulos.EnableControls;
end;


procedure TFCadGrupoOper.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if not QPermissoes.IsEmpty then QPermissoes.Delete;
end;

procedure TFCadGrupoOper.SpeedButton4Click(Sender: TObject);
begin
if not QPermissoes.IsEmpty then
   while not QPermissoes.IsEmpty do QPermissoes.Delete;
end;

procedure TFCadGrupoOper.QPermissoesBeforePost(DataSet: TDataSet);
var permissao_id : Integer;
begin
  inherited;    // no inserir os cadastros aparecem como N
  if QPermissoes.State = dsEdit then begin
     QPermissoesinclui.AsString := UpperCase(QPermissoesinclui.AsString);
     QPermissoesexclui.AsString := UpperCase(QPermissoesexclui.AsString);
     QPermissoesaltera.AsString := UpperCase(QPermissoesaltera.AsString);
     QPermissoesacessa.AsString := UpperCase(QPermissoesacessa.AsString);
     If not (QPermissoesinclui.AsString[1] in ['S','N']) then QPermissoesinclui.AsString := 'N';
     If not (QPermissoesexclui.AsString[1] in ['S','N']) then QPermissoesexclui.AsString := 'N';
     If not (QPermissoesaltera.AsString[1] in ['S','N']) then QPermissoesaltera.AsString := 'N';
     If not (QPermissoesacessa.AsString[1] in ['S','N']) then QPermissoesacessa.AsString := 'N';
  end;
  if not ((Pos('Cadastros', JvDBGrid2.Columns[0].Field.AsString)) > 0) then begin
     QPermissoesinclui.AsString := '-';
     QPermissoesexclui.AsString := '-';
     QPermissoesaltera.AsString := '-';
  end;
  if(QPermissoes.State = dsInsert)then
  begin
    permissao_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SPERMISSAO');
    QPermissoesPERM_ID.AsInteger := permissao_id;
  end;
end;

procedure TFCadGrupoOper.QCadastroAfterInsert(DataSet: TDataSet);
var grupo_usu_id,permissao_id : Integer;
begin
  inherited;

  grupo_usu_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SGRUPO_USU_ID');
  QCadastroGRUPO_USU_ID.AsInteger := grupo_usu_id;
  QCadastroGRUPO_USU_ID.AsInteger := grupo_usu_id
end;

procedure TFCadGrupoOper.JvDBGrid2TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  if ACol = 0 then exit;
  if Application.MessageBox(PChar('Aten��o todos os modulos ter�o permiss�o nesta coluna.'+#13+'Confirma a altera��o?'),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDYes then begin
     QPermissoes.First;
     while not QPermissoes.Eof do begin
        QPermissoes.Edit;
        QPermissoes.FieldByName(Field.FieldName).AsString := 'S';
        QPermissoes.Post;
        QPermissoes.Next;
     end;
     QPermissoes.First;
  end
  else if Application.MessageBox(PChar('Aten��o todos os modulos ter�o permiss�o negada nesta coluna.'+#13+'Confirma a altera��o?'),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDYes then begin
     QPermissoes.First;
     while not QPermissoes.Eof do begin
        QPermissoes.Edit;
        QPermissoes.FieldByName(Field.FieldName).AsString := 'N';
        QPermissoes.Post;
        QPermissoes.Next;
     end;
     QPermissoes.First;
  end;
end;

procedure TFCadGrupoOper.JvDBGrid1DblClick(Sender: TObject);
begin
SpeedButton1Click(nil);
end;

procedure TFCadGrupoOper.QPermissoesAfterScroll(DataSet: TDataSet);
begin
  inherited;
 { if not ((Pos('Cadastros', QPermissoesmodulo.AsString)) > 0) then begin
     QPermissoesinclui.ReadOnly := True;
     QPermissoesaltera.ReadOnly := True;
     QPermissoesexclui.ReadOnly := True;
  end
  else begin
     QPermissoesinclui.ReadOnly := false;
     QPermissoesaltera.ReadOnly := false;
     QPermissoesexclui.ReadOnly := false;
  end;
  JvDBGrid2.Refresh;
  }
end;

procedure TFCadGrupoOper.ButGravaClick(Sender: TObject);
begin
  if fnVerfCompVazioEmTabSheet('Descri��o obrigat�ria',dbEdtDesc) then Abort;
  inherited;
end;

end.
