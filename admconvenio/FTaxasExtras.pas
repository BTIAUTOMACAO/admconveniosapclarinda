unit FTaxasExtras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, ToolEdit, CurrEdit,
  U1;

type
  TFrmTaxasExtras = class(TF1)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtCredId: TEdit;
    BitBtn1: TBitBtn;
    edtValor: TCurrencyEdit;
    btnCancelar: TBitBtn;
    edtCodLote: TEdit;
    edtCredNome: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    Label3: TLabel;
    edtDescricao: TEdit;
    procedure InserirTaxa();
    procedure BitBtn1Click(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var

  FrmTaxasExtras: TFrmTaxasExtras;

  


implementation

uses DM,cartao_util,UMenu;

{$R *.dfm}

procedure TFrmTaxasExtras.InserirTaxa();
var pagamentoId, dataDesconto, valorTaxa: string;
begin
  //ENCONTRANDO OS VALORES QUE N�O EST�O NO FORM PARA FAZER INSERT NA TABELA TAXAS_REPASSE
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT PAGAMENTO_CRED_ID FROM PAGAMENTO_CRED WHERE LOTE_PAGAMENTO = '+ edtCodLote.Text +' AND CRED_ID = '+ edtCredId.Text +'');
  DMConexao.AdoQry.Open;
  pagamentoId := VarToStr(DMConexao.AdoQry.Fields[0].Value);
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT TOP 1CONVERT(VARCHAR, DT_DESCONTO,103)FROM TAXAS_REPASSE ');
  DMConexao.AdoQry.SQL.Add('WHERE CRED_ID = '+ edtCredId.Text +' AND COD_LOTE_PAGAMENTO = '+ edtCodLote.Text +' ORDER BY DT_DESCONTO DESC');
  DMConexao.AdoQry.Open;

  //VALOR DA TAXA SENDO COLOCADO COM PONTOS, PARA N�O DAR ERRO NA INSTRU��O SQL
  valorTaxa := StringReplace(edtValor.Text, ',', '.', []);

  //INSERT NA TABELA TAXAS_REPASSE
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('INSERT INTO TAXAS_REPASSE (CRED_ID, TAXA_ID, DT_DESCONTO,NEGOCIADA,  ');
  DMConexao.AdoQry.SQL.Add('TAXAS_PROX_PAG_ID_FK, HISTORICO, VALOR, TIPO_DESCONTO, PAGAMENTO_CRED_ID,COD_LOTE_PAGAMENTO) ');
  DMConexao.AdoQry.SQL.Add('VALUES('+ edtCredId.Text +', 0, '''+ dataDesconto +''', ''N'', 0, ');
  DMConexao.AdoQry.SQL.Add(''''+ edtDescricao.Text +''', '+ valorTaxa +', ''D'', '+ pagamentoId +',' );
  DMConexao.AdoQry.SQL.Add(' '+ edtCodLote.Text +' )');
  DMConexao.ExecuteSql(DMConexao.AdoQry.SQL.Text);
end;

procedure TFrmTaxasExtras.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if Application.MessageBox('Confirma a inser��o de taxa?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
  begin
    //                   janela           ,campo     , vr_ant                                   , vr_novo                             , operador    , operacao  , cadastro                      , id                                                    , detalhe                                                ,)
    DMConexao.GravaLog('FTaxasExtras','INSER��O TAXA', edtValor.Text, '',Operador.Nome,'Inser��o','0',Self.Name);
    try
      InserirTaxa();
      MsgInf('Taxa Inserida com sucesso');
    except
      MsgErro('A taxa n�o foi inserida, devido algum problema com os valores.');
    end;
    Self.Close
  end;
end;

procedure TFrmTaxasExtras.btnCancelarClick(Sender: TObject);
begin
  inherited;
  Self.Close;
end;

end.
