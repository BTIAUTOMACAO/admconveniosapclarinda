unit SispagItau;

interface

uses Classes, SysUtils, //ACBrBoleto,
        dateutils; //ACBrD5, DB, ADODB,SispagEntidades ;
type TSispagItau = class

private
//  fpDigito                      : Integer;
//  fpNumero                      : Integer;
//  fpTamanhoMaximoNossoNum       : Integer;
//  fpTamanhoAgencia              : Integer;
//  fpTamanhoConta                : Integer;
//  fpTamanhoCarteira             : Integer;
//  fpNome                        : String;
//  fNomeSacado                   : String;
//  fEnderecoEmpresaSacado        : String;
//  fComplemetoEnderecoSacado     : String;
//  fCidadeSacado                 : String;
//  fCEPSacado                    : String;
//  fEstadoSacado                 : String;
//  fAgenciaSacado                : String;
//  fContaSacado                  : String;
//  fCPFCNPJSacado                : String;
//  fNumeroEnderecoSacado         : Integer;
//  fSacado                       : TSacado;
//  fCedente                      : TCedente;
//
//  procedure SetCEPSacado(const Value: String);
//  procedure SetCidadeSacado(const Value: String);
//  procedure SetComplemetoEnderecoSacado(const Value: String);
//  procedure SetEnderecoEmpresaSacado(const Value: String);
//  procedure SetEstadoSacado(const Value: String);
//  procedure SetNomeSacado(const Value: String);
//  procedure SetNumeroSacado(const Value: Integer);
//  procedure SetContaSacado(const Value: String);
//  procedure SetAgenciaSacado(const Value: String);
//  procedure SetCPFCNPJSacado(const Value: String);

//public
  //CodigoLoteServico : String; //Declarado como String devido ao formato obrigat�rio "0000"
  //CodigoSequencialRegistroLote : Integer;



//  property NomeSacado                   : String read fNomeSacado write SetNomeSacado;   //NomeSacado do Credenciado Favorecido
//  property EnderecoEmpresaSacado        : String read fEnderecoEmpresaSacado write SetEnderecoEmpresaSacado; //Endere�o do Credenciado Favorecido
//  property ComplemetoEnderecoSacado     : String read fComplemetoEnderecoSacado write SetComplemetoEnderecoSacado; //Complemento de Endere�o do Credenciado Favorecido
//  property CidadeSacado                 : String read fCidadeSacado write SetCidadeSacado;
//  property CEPSacado                    : String read fCEPSacado write SetCEPSacado;
//  property EstadoSacado                 : String read fEstadoSacado write SetEstadoSacado;
//  property NumeroEnderecoSacado         : Integer read fNumeroEnderecoSacado write SetNumeroSacado;
//  property AgenciaSacado                : String read fAgenciaSacado write SetAgenciaSacado;
//  property ContaSacado                  : String read fContaSacado   write SetContaSacado;
//  property CPFCNPJSacado                : String read fCPFCNPJSacado write SetCPFCNPJSacado;
//  property Sacado                       : TSacado  read fSacado      write fSacado;
//  property Cedente                      : TCedente read fCedente     write fCedente;

//function GerarRegistroHeader240(tipoInscDaEmpresa : Byte; ContaDigito : Byte; TipoDePgto: Byte; FinalidadeDoLote : String; HistoricoDeContaCorrente : String ) : String ;
//function GerarRegistroHeader240(Sacado : TSacado ) : String ;
//function getLoteDeServico() : String;
//function GerarRegistroDetalheSegmentoA(Favorecido : TCedente; ValorPgtos : Currency) : String;
//function GerarRegistroTraillerDeLote(ValorPgtos : Currency) : String;
//function getAgenciaConta(DAC : String; Favorecido : TCedente) : String;
//function GerarTraillerDoArquivo240() : String;
//function GeraHeaderDeLote(Sacado : TSacado; Cedente : TCedente) : String ;
//Function PreencherComZeros(Digitos : Integer) : String;
//Constructor Create;    // declara��o do metodo construtor
//Destructor  Destroy; Override; // declara��o do metodo destrutor
//function GetCodigoDoBanco(codBanco: String): String;

//end;

//var
//ContadorDeLinhasDoArquivo    : Integer = 0;
//CodigoLoteServico            : Integer = 0;
//ContadordeLotesDoArquivo     : Integer = 0;
//ContadorDeRegistrosDeLote    : Integer = 0; // Quantidades de registros com c�digo 1,3,5
//implementation

//function TSispagItau.GetCodigoDoBanco(codBanco: String): String;
//begin
//  Result := DMConexao.ExecuteQuery('SELECT COD_BANCO FROM BANCOS WHERE CODIGO = '+codBanco);
//end;
//
//constructor TSispagItau.Create;
//begin
//  //fpDigito := 7;
//  //fpNome   := 'Banco Itau';
//  fpNumero:= 0;
//  //fpTamanhoMaximoNossoNum := 8;
//  //fpTamanhoAgencia := 4;
//  //fpTamanhoConta   := 5;
//  //fpTamanhoCarteira:= 3;
//  fNomeSacado                    := '';
//  fEnderecoEmpresaSacado         := '';
//  fComplemetoEnderecoSacado      := '';
//  fCidadeSacado                  := '';
//  fCEPSacado                     := '';
//  fEstadoSacado                  := '';
//  fAgenciaSacado                 := '';
//  //Sacado.Nome                    := 'teste';
//  fContaSacado                   := '';
//  fNumeroEnderecoSacado          := 0;
//  fCedente      := TCedente.Create();
//  fSacado       := TSacado.Create();
//
//  {$IFDEF COMPILER6_UP}
//    fCedente.SetSubComponent(True);   // Ajustando como SubComponente para aparecer no ObjectInspector
//  {$ENDIF}
//end;
//
//destructor TSispagItau.Destroy;
//begin
//
//  inherited;
//end;
//
//procedure TSispagItau.SetCEPSacado(const Value: String);
//begin
//  fCEPSacado := Value;
//end;
//
//procedure TSispagItau.SetCidadeSacado(const Value: String);
//begin
//  fCidadeSacado := Value;
//end;
//
//procedure TSispagItau.SetComplemetoEnderecoSacado(const Value: String);
//begin
//  fComplemetoEnderecoSacado := Value;
//end;
//
//procedure TSispagItau.SetEnderecoEmpresaSacado(const Value: String);
//begin
//  fEnderecoEmpresaSacado := Value;
//end;
//
//procedure TSispagItau.SetEstadoSacado(const Value: String);
//begin
//  fEstadoSacado := Value;
//end;
//
//procedure TSispagItau.SetNomeSacado(const Value: String);
//begin
//  fNomeSacado := Value;
//end;
//
//procedure TSispagItau.SetNumeroSacado(const Value: Integer);
//begin
//  fNumeroEnderecoSacado := Value;
//end;
//
//procedure TSispagItau.SetContaSacado(const Value: String);
//begin
//  fContaSacado := Value;
//end;
//
//procedure TSispagItau.SetAgenciaSacado(const Value: String);
//begin
//  fAgenciaSacado := Value;
//end;
//
//procedure TSispagItau.SetCPFCNPJSacado(const Value: String);
//begin
//  fCPFCNPJSacado := Value;
//end;
//
//
//function TSispagItau.getLoteDeServico() : String;
//begin
////   CodigoLoteServico := DMConexao.ExecuteQuery('SELECT REPLACE(STR((NEXT VALUE FOR SCOD_LOTE_SISPAGITAU),4),'' '',''0'')');
////   Result := CodigoLoteServico;
//  CodigoLoteServico := CodigoLoteServico + 1;
//  Result := IntToStrZero(CodigoLoteServico,4);
//end;
//
//
//
//function TSispagItau.getAgenciaConta(DAC : String; Favorecido : TCedente) : String ;
//begin
//  if (Favorecido.CodBanco = '2') or (Favorecido.CodBanco = '409') or (Favorecido.CodBanco = '341') then begin
//    Result := '0' +
//            PadRight(OnlyNumber(Favorecido.Agencia), 4, '0') +
//            ' '+
//            '000000'+
//            PadRight(OnlyNumber(Favorecido.Conta), 6, '0') +
//            ' '+
//            DAC;
//  end
//  else
//  begin
//    Result := PadRight(OnlyNumber(Favorecido.Agencia), 4, '0') +
//            ' '+
//            PadRight(OnlyNumber(Favorecido.Conta), 12, '0') +
//            ' '+
//            DAC;
//  end;
//
//end;
//
//Function TSispagItau.PreencherComZeros(Digitos : Integer) : String;
//var
//i : integer;
//begin
//  For i := 0 to Digitos -1 do
//  Result := '0' + Result;
//end;
//
//function TSispagItau.GerarRegistroHeader240(Sacado : TSacado) : String ;
//var pEnderecoEmpresa, pComplemento, pCidade, pCEP, pEstado,pNome,pAgencia, pConta,DAC,fConta : String;
//var pNumero : Integer;
//begin
////  pEnderecoEmpresa := EnderecoEmpresaSacado;
////  pNumero          := NumeroEnderecoSacado;
////  pComplemento     := ComplemetoEnderecoSacado;
////  pCidade          := CidadeSacado;
////  pCEP             := CEPSacado;
////  pEstado          := EstadoSacado;
////  pNome            := NomeSacado;
////  pAgencia         := AgenciaSacado;
////  pConta           := ContaSacado;
//  ContadorDeLinhasDoArquivo := ContadorDeLinhasDoArquivo + 1;
//  //DAC := copy(Sacado.Conta,Length(Sacado.Conta)-1,1);
//  DAC := '6';
//  fConta := copy(Sacado.Conta,1,5);
//  fpNumero :=  StrToInt(PadLeft(GetCodigoDoBanco(Sacado.CodBanco), 3));
//  Result:=     IntToStrZero(fpNumero,3)                    +       //1 a 3 - C�digo do banco
//               '0000'                                      + //4 a 7 - Lote de servi�o
//               '0'                                         + //8 - Tipo de registro - Registro header de arquivo
//               space(6)                                    + //9 a 14 Uso exclusivo FEBRABAN/CNAB
//               '081'                                       + //15 a 17 - N�mero da vers�o do layout do arquivo
//               IntToStr(2)                                 + //18 - Tipo de inscri��o do cedente 1 = CPF 2 = CNPJ
//               PadRight(OnlyNumber(Sacado.CPFCNPJ), 14, '0')   + //19 a 32 -N�mero de inscri��o do cedente
//               space(20)                                   + // 33 a 52 - Brancos
//               PadRight(OnlyNumber(Sacado.Agencia), 5, '0')    + //53 a 57 - C�digo da ag�ncia do cedente
//               ' '                                         + // 58 - Brancos
//               PadRight(OnlyNumber(fConta), 12, '0')     + // 59 a 70 - N�mero da ContaSacado do cedente
//               ' '                                         + // 71 - Branco  - Complemento do Registro
//               DAC                                         + // 72 - D�gito da ContaSacado do cedente
//               PadLeft(Sacado.Nome, 30, ' ')                  + // 73 a 102 - NomeSacado do cedente
//               PadLeft('BANCO ITAU S/A', 30, ' ')             + // 103 a 132 - NomeSacado do banco
//               space(10)                                   + // 133 A 142 - Brancos
//               '1'                                         + // 143 - C�digo de Remessa (1) / Retorno (2)
//               FormatDateTime('ddmmyyyy', Now)             + // 144 a 151 - Data do de gera��o do arquivo
//               FormatDateTime('hhmmss', Now)               + // 152 a 157 - Hora de gera��o do arquivo
//               '000000000'                                 + // 158 a 166 - N�mero sequencial do arquivo retorno
//               '00000'                                     + // 167 a 171 - Zeros
//               space(69)                                   ; // 172 a 225 - 54 Brancos
//
//     { GERAR REGISTRO HEADER DO LOTE }
//
//
//end;
//
//function TSispagItau.GeraHeaderDeLote(Sacado : TSacado; Cedente : TCedente) : String ;
//var pEnderecoEmpresa, pComplemento, pCidade, pCEP, pEstado,pNome,pAgencia, pConta,DAC,fConta,FormaDePagamento : String;
//var pNumero,fpNumero : Integer;
//begin
//  ContadordeLotesDoArquivo := ContadordeLotesDoArquivo + 1;
//  ContadorDeRegistrosDeLote  := ContadorDeRegistrosDeLote + 1;
//  ContadorDeLinhasDoArquivo := ContadorDeLinhasDoArquivo + 1;
//  fpNumero :=  StrToInt(PadLeft(GetCodigoDoBanco(Cedente.CodBanco), 3));
//
//  if (fpNumero = 341) then
//    FormaDePagamento := '01'
//    else
//    FormaDePagamento := '41';
//  DAC := '6';
//  fConta := copy(Sacado.Conta,1,5);
//  Result:=     IntToStrZero(fpNumero,3)                    + //1 a 3 - C�digo do banco
//               getLoteDeServico                            + //4 a 7 - Lote de servi�o
//               '1'                                         + //8 - Tipo de registro - Registro header de arquivo
//               'C'                                         + //9 - Tipo de opera��o: R (Remessa) ou T (Retorno)
//               intToStr(20)                                + //10 a 11 - Tipo de servi�o: 01 (Cobran�a)
//               '01'                                        + //12 a 13 - Forma de lan�amento: preencher com ZEROS no caso de cobran�a
//               '040'                                       + //14 a 16 - N�mero da vers�o do layout do lote
//               FormaDePagamento                            + //17 - Uso exclusivo FEBRABAN/CNAB
//               IntToStr(2)                                 + //18 - Tipo de inscri��o do cedente
//               PadRight(OnlyNumber(Sacado.CPFCNPJ), 14, '0')   + //19 a 32 -N�mero de inscri��o do cedente
//               Space(4)                                    +
//               space(16)                                   + //37 a 52 - Brancos
//               PadRight(OnlyNumber(Sacado.Agencia), 5, '0')    + //53 a 57 - C�digo da ag�ncia do cedente
//               ' '                                         + // 58
//               PadRight(OnlyNumber(fConta), 12, '0')           + //59 a 70 - N�mero da ContaSacado do cedente
//               ' '                                         + // 71
//               DAC                                         + // 72 - D�gito verificador da ag�ncia / ContaSacado
//               PadLeft(Sacado.Nome, 30, ' ')                  + //73 a 102 - NomeSacado do cedente
//               Space(30)                                   + // 103 a 132 - Brancos
//               Space(10)                                   + //133 a 142
//               PadLeft(Sacado.EnderecoEmpresa,30,' ')         + //143 a 172 x(30)
//               IntToStrZero(Sacado.Numero,5)               + //173 a 177 9(5)
//               PadLeft(Sacado.Complemeto,15,' ')              + //178 a 192 x(15)
//               PadLeft(Sacado.Cidade,20,' ')                  + //193 a 212 x(20)
//               PadLeft(Sacado.CEP,08,' ')                     + //213 a 220 9(08)
//               PadLeft(Sacado.Estado,02,' ')                  + //221 a 222 x(02)
//               space(8)                                    + //223 a 230 Complemento do registro - Em branco
//               space(10)                                   ;//231 a 240 x(10)
//end;
//
//function TSispagItau.GerarRegistroDetalheSegmentoA(Favorecido : TCedente; ValorPgtos : Currency) : String;
//var valor,dec,DAC,fContaFavorecido : String;
//begin
//   ContadorDeLinhasDoArquivo := ContadorDeLinhasDoArquivo + 1;
//   ContadorDeRegistrosDeLote  := ContadorDeRegistrosDeLote + 1;
//   fContaFavorecido := copy(fnSubstituiString('-','',Favorecido.Conta),1,5);
//   valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',ValorPgtos));
//          dec := Copy(valor,Length(valor)-1,2);
//          valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
//
//   //DAC := copy(fnSubstituiString('-','',Favorecido.Conta),6,6);
//   DAC := copy(Favorecido.Conta,Length(Favorecido.Conta),1);
//
//   Result:=    IntToStrZero(fpNumero,3)                                   +  //1 a 3 - C�digo do banco
//               IntToStrZero(CodigoLoteServico,4)                          + //4 a 7 - Lote de servi�o
//               '3'                                                        + //8 - Tipo do registro: Registro detalhe do lote
//               //IntToStrZero(ACBrBoleto.ListadeBoletos.IndexOf(ACBrTitulo)+ 1 ,5) + //9 a 13 - N�mero seq�encial do registro no lote - Cada registro possui dois segmentos
//               IntToStrZero((CodigoSequencialRegistroLote + 1),5)         +
//               'A'                                                        + //14 - C�digo do segmento do registro detalhe
//               '000'                                                      + //15 a 17 9(3) - Tipo de Movimento
//               '000'                                                      + //18 a 20 - C�digo da camara centralizada
//               PadLeft(GetCodigoDoBanco(Favorecido.CodBanco), 3)             + //21 a 23 9(3) - C�digo o banco favorecido
//               PadRight(getAgenciaConta(DAC,Favorecido),20,' ')               + //24 a 43 x(20)- Ag�ncia e ContaSacado do favorecido - NOTA 11
//               PadLeft(Favorecido.NomeCedente,30,' ')                        + //44 a 73 x(30)- NomeSacado do favorecido
//               Space(20)                                                  + //74 a 93 x(20)- N� documento atr�buido pela empresa(seu n�mero)
//               FormatDateTime('ddmmyyyy', Now)                            + //94 a 101 - data prevista pagamento (DDMMAAAA)
//               'REA'                                                      + //102 A 104 - Moeda Tipo - REA ou 009
//               PreencherComZeros(8)                                       + //105 a 112 9(08) - C�digo ISPB
//               PreencherComZeros(7)                                       + //113 a 119 - PREENCHER COM ZEROS
//               PadRight(valor,15,'0')                                         + //120 A 134 9(13)V9(02) - Valor do Pagto(Valor previsto do pagamento)
//               Space(15)                                                  + //135 a 149 x(15) Nosso N�mero(N� Docto atribu�do pelo banco)
//               Space(5)                                                   + //150 a 154 PREENCHER COM ESPA�O VAZIO
//               PreencherComZeros(8)                                       + //155 A 162 9(08)Data Efetiva do Pgto DDMMAAAA
//               PreencherComZeros(15)                                      + //163 a 177 9(13)v9(02)- Valor real efetivo do pgto
//               PadLeft('SISPAGPGT_CRED_RPC',18,' ')                          + //178 a 195 x(18) Informa��o Complementar p/ Hist. de ContaSacado Corrente
//               space(2)                                                   + //196 a 197 x(02) Complemento de Registro
//               PreencherComZeros(6)                                       + //198 a 203 9(6) - N� de DOC/TED/OP/CHEQUE no retorno
//               PadRight(OnlyNumber(Favorecido.CNPJCPF), 14, '0')              + //204 a 217 9(14) - N� de Inscri��o Estadual do Favorecido(CPF/CNPJ)
//               '01'                                                       + //218 a 219 x(02) - Finalidade do DOC e Status do Funcio�rio na empresa
//               '00001'                                                    + //220 a 224 x(05) - Finalidade da TED
//               Space(5)                                                   + //225 a 229 x(05) - PREENCHER COM ESPA�O EM BRANCO
//               ' '                                                        + //230 a 230 x(01) - Aviso ao Favorecido
//               Space(10)                                      ; //231 a 240 x(10) - C�digo Ocorr�ncias no Retorno
//end;
//
//function TSispagItau.GerarRegistroTraillerDeLote(ValorPgtos : Currency) : String;
//var valor,dec : String;
//begin
//  ContadorDeLinhasDoArquivo := ContadorDeLinhasDoArquivo + 1;
//  ContadorDeRegistrosDeLote  := ContadorDeRegistrosDeLote + 1;
//  valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',ValorPgtos));
//          dec := Copy(valor,Length(valor)-1,2);
//          valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
//
//  Result:=    IntToStrZero(fpNumero,3)                    +  //1 a 3 - C�digo do banco
//              IntToStrZero(CodigoLoteServico,4)           + //4 a 7 9(4) C�digo do lote
//              '5'                                         + //8 a 8
//              Space(9)                                    + //9 a 17 x(9)
//              IntToStrZero(ContadorDeRegistrosDeLote,6)   + //Quantidade de Registros de Lote
//              PadRight(valor,18,'0')                          +
//              PreencherComZeros(18)                       +
//              Space(171)                                  +
//              Space(10)                               ;
//end;
//
//function TSispagItau.GerarTraillerDoArquivo240() : String;
//begin
//  ContadorDeLinhasDoArquivo := ContadorDeLinhasDoArquivo + 1;
//  Result:=    IntToStrZero(fpNumero,3)                  + //1 a 3 - C�digo do banco
//              '9999'                                    + //4 a 7 9(4) C�digo do lote
//              '9'                                       +
//              Space(9)                                  +
//              IntToStrZero(ContadordeLotesDoArquivo,6)  +
//              IntToStrZero(ContadorDeLinhasDoArquivo,6) +
//              Space(211)                                ;
//end;

//end.









end;

end.
