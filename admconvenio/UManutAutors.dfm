inherited FManutAutors: TFManutAutors
  Left = 216
  Top = 40
  Caption = 'Manuten'#231#227'o de Autoriza'#231#245'es'
  ClientHeight = 573
  ClientWidth = 792
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 792
    TabOrder = 3
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 792
    Height = 135
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 0
    DesignSize = (
      788
      131)
    object Label1: TLabel
      Left = 11
      Top = 8
      Width = 107
      Height = 13
      Caption = 'Selecionar Fornecedor'
    end
    object Bevel1: TBevel
      Left = 367
      Top = 5
      Width = 2
      Height = 39
    end
    object Label2: TLabel
      Left = 379
      Top = 8
      Width = 94
      Height = 13
      Caption = 'Selecionar Empresa'
    end
    object Label6: TLabel
      Left = 624
      Top = 100
      Width = 156
      Height = 28
      AutoSize = False
      Caption = 'Aten'#231#227'o, autoriza'#231#227'o faturadas e ou baixadas n'#227'o aparecer'#227'o.'
      WordWrap = True
    end
    object DBFornecedor: TJvDBLookupCombo
      Left = 64
      Top = 25
      Width = 295
      Height = 21
      DropDownWidth = 350
      DisplayEmpty = 'Selecione o estabelecimento'
      EmptyValue = '0'
      Enabled = False
      FieldsDelimiter = #0
      LookupField = 'CRED_ID'
      LookupDisplay = 'NOME'
      LookupSource = dscrCredenciado
      TabOrder = 2
    end
    object EdCred: TEdit
      Left = 10
      Top = 25
      Width = 47
      Height = 21
      Enabled = False
      TabOrder = 1
      OnChange = EdCredChange
      OnKeyPress = EdCredKeyPress
    end
    object ChkFornec: TCheckBox
      Left = 128
      Top = 7
      Width = 129
      Height = 15
      Caption = 'Todos Estabelecimentos'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = ChkFornecClick
    end
    object RGAutors: TRadioGroup
      Left = 424
      Top = 49
      Width = 197
      Height = 78
      Caption = 'Autoriza'#231#245'es...'
      ItemIndex = 0
      Items.Strings = (
        'Todas'
        'Somente Confirmadas'
        'Somente n'#227'o Confirmadas')
      TabOrder = 8
    end
    object RGdatas: TRadioGroup
      Left = 8
      Top = 49
      Width = 151
      Height = 78
      Caption = 'Filtrar por data...'
      ItemIndex = 0
      Items.Strings = (
        'Data de Fecham. Emp'
        'Periodo')
      TabOrder = 6
      OnClick = RGdatasClick
    end
    object GroupBox1: TGroupBox
      Left = 161
      Top = 49
      Width = 261
      Height = 78
      Caption = 'Datas...'
      TabOrder = 7
      object LabDatas: TLabel
        Left = 7
        Top = 30
        Width = 85
        Height = 13
        Caption = 'Data Fechamento'
      end
      object datafin: TJvDateEdit
        Left = 133
        Top = 46
        Width = 121
        Height = 21
        ShowNullDate = False
        TabOrder = 0
        Visible = False
      end
      object dataini: TJvDateEdit
        Left = 7
        Top = 46
        Width = 121
        Height = 21
        ShowNullDate = False
        TabOrder = 1
      end
    end
    object DBEmpresa: TJvDBLookupCombo
      Left = 427
      Top = 25
      Width = 295
      Height = 21
      DropDownWidth = 350
      DisplayEmpty = 'Selecione a empresa'
      EmptyValue = '0'
      Enabled = False
      FieldsDelimiter = #0
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'NOME'
      LookupSource = DSEmpresa
      TabOrder = 5
      OnChange = DBEmpresaChange
    end
    object ChkEmp: TCheckBox
      Left = 479
      Top = 7
      Width = 129
      Height = 15
      Caption = 'Todas Empresas'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = ChkEmpClick
    end
    object EdEmp: TEdit
      Left = 377
      Top = 25
      Width = 47
      Height = 21
      Enabled = False
      TabOrder = 4
      OnChange = EdEmpChange
      OnKeyPress = EdEmpKeyPress
    end
    object BitBtn1: TBitBtn
      Left = 644
      Top = 65
      Width = 76
      Height = 25
      Caption = '&Buscar'
      TabOrder = 9
      OnClick = BitBtn1Click
      Glyph.Data = {
        42050000424D4205000000000000360000002800000019000000110000000100
        1800000000000C050000120B0000120B00000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C7CFD3C0C5C7BBC0C0BBC0C0BBC0
        C0BBC0C0BBC0C0BBC0C0BBC0C0BBC0C0BBC0C0BBC0C0BBC0C0BCC0C1C2C8CAC8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C7CED2A19A
        937B605374584874584874584874584874584874584874584874584874584874
        584874574874584A7E7068A8A7A5C5CCCFC8D0D4C8D0D4C8D0D4C8D0D400C8D0
        D4BABEC0BABFC2ABA7A4A87157BC9C8ABA9B8ABA9B89BA9B89BA9B89BA9B89BA
        9B89BA9B89BA9B89BA9B89BA9B89BB9C8ABB99868E563964483AA8A7A4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4AEA198938E8C987B6DD3C0AFE0FFFFDDF6FDDD
        F5FDDDF5FDDDF5FDDDF5FDDDF5FDDDF5FDDDF5FDDDF5FDDDF5FDDDF5FDDEFAFF
        DDE5E692593C7B6F66C1C7C9C8D0D4C8D0D4C8D0D400C8D0D4B8B4ACABABACAE
        A5A1C1997EDCF5FDD8EAEFDBEDF2DBECF1DAECF0DAECF1DAECF1DAECF1DAECF1
        DAECF1DAECF1DAECF1DAEBF0DDF7FFC2A99A694837B2B4B3C8D0D4C8D0D4C8D0
        D400C8D0D4C0C6C8B6BABDB4B6B6AA7253DCECEED8EAF0DBE5E7E4EEF1E5EFF2
        E0EAEDE0EAEDE0EAEDE0EAEDE0EAEDE0EAEDDFEAEDE5EEF0E1F1F7D8DCDB7745
        2A999490C8D0D4C8D0D4C8D0D400C8D0D4B2A69A998B82928E8C987868D7CDC1
        DAF0F7A1AFB0B5BBBCCFD3D4EEF2F4EDF1F3EDF1F3EDF1F3EDF1F3EDF1F3EDF1
        F2DCE4E6E4F0F3DCF0F69D6F567B6C62C1C7C9C8D0D4C8D0D400C8D0D4BAB4AC
        AAA6A2A9ABACB2ADABC0977DE2FBFFB0C0C3A1A5A4C1BFBFFAF9F9FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFEEF3F4E0EAECE0F7FFC0A697674737B2B4B3C8
        D0D4C8D0D400C8D0D4C2C8CCB8BDBFB6BABDB5B8B9A97657DEEEF0CFE2E7929B
        9BB6B6B5E9E9E9FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDF8F8F9DEE6E9E3
        F3F8D7DBDA724127999490C8D0D4C8D0D400C8D0D4B6ABA09F8F85988881938F
        8D987868D7CDC1DCF2FA9DAAAC939999A0A5A5A0A5A5A0A5A5A0A5A5A0A5A5A0
        A5A5A0A5A59DA2A2AFB8B9E5F0F3DDF1F69E70577A6C62C1C7C9C8D0D400C8D0
        D4BCB6ADACA7A1A8A5A2AAABACB1ADABC0967DDEF7FFCFE1E6C0D1D6BFD1D5BF
        D1D5BFD1D5BFD1D5BFD1D5BFD1D5BFD1D5BFD0D4C2D3D7D3E4E8DDF6FEC0A798
        674837B1B3B2C8D0D400C8D0D4C1C7CAB8BDBFB8BCBEB6BABDB5B8B9A67050DB
        ECF2DAF3FFDBF3FEDBF3FEDBF3FEDBF3FEDBF3FEDBF3FEDBF3FEDBF3FEDAF2FC
        DAF2FDD7EFFADBF3FDD8E0E372432B989490C8D0D400C8D0D4B8AC9EA393889D
        8C83978882938E8E9A7B68E5AC5CEBB969E8B566E8B466E8B466E8B466E8B466
        E8B466E8B466E8B566EABB72E9B86DEBBB70DDB577E5B874A2571C877D78C8D0
        D400C8D0D4B7B3A9A9A29DA59F9BA39E9CA5A7A6BABCBEB66127DC6A01DC6E01
        DE7001DD7001DD7001DD7001DD7001DD7001DD7001EF983DE98A26F79C38A172
        5AA76F4DA84F11A29F9CC8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4BBBFBFA7886EC49970CDA87CCCA67ACCA67ACCA67ACCA67ACCA67ACCA6
        7ACDA67ACDA77BCDA87CCFA87AB78E6AA89E99C5CCCFC8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D400}
    end
    object cbNListCanc: TCheckBox
      Left = 618
      Top = 7
      Width = 123
      Height = 17
      Anchors = [akTop, akRight]
      Caption = 'N'#227'o listar canceladas'
      TabOrder = 10
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 516
    Width = 792
    Height = 57
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 2
    object Bevel3: TBevel
      Left = 305
      Top = 8
      Width = 2
      Height = 33
    end
    object Label4: TLabel
      Left = 419
      Top = 7
      Width = 63
      Height = 13
      Caption = 'Totaliza'#231#245'es:'
    end
    object Bevel4: TBevel
      Left = 408
      Top = 8
      Width = 2
      Height = 33
    end
    object ButMarcaTodos: TButton
      Left = 107
      Top = 13
      Width = 92
      Height = 25
      Caption = 'Marca Todos (F8)'
      TabOrder = 1
      OnClick = ButMarcaTodosClick
    end
    object ButDesmTodos: TButton
      Left = 203
      Top = 13
      Width = 95
      Height = 25
      Caption = 'Desm. Todos (F9)'
      TabOrder = 2
      OnClick = ButDesmTodosClick
    end
    object ButMarcDesm: TButton
      Left = 6
      Top = 13
      Width = 97
      Height = 25
      Caption = 'Marca/Desm.(F11)'
      TabOrder = 0
      OnClick = ButMarcDesmClick
    end
    object ButAlterar: TButton
      Left = 314
      Top = 13
      Width = 88
      Height = 25
      Caption = 'Alterar Marcados'
      TabOrder = 3
      OnClick = ButAlterarClick
    end
    object Panel3: TPanel
      Left = 417
      Top = 20
      Width = 369
      Height = 19
      BorderStyle = bsSingle
      Color = clWindow
      TabOrder = 4
      object Label3: TLabel
        Left = 5
        Top = 0
        Width = 48
        Height = 13
        Caption = 'Debitos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabTotCred: TLabel
        Left = 188
        Top = 0
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label5: TLabel
        Left = 135
        Top = 0
        Width = 51
        Height = 13
        Caption = 'Creditos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabTotal: TLabel
        Left = 302
        Top = 0
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label7: TLabel
        Left = 267
        Top = 0
        Width = 34
        Height = 13
        Caption = 'Total:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabTotDeb: TLabel
        Left = 54
        Top = 0
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DBGrid1: TJvDBGrid [3]
    Left = 0
    Top = 158
    Width = 792
    Height = 358
    Hint = 'Duplo click ou enter para marcar a autoriza'#231#227'o.'
    Align = alClient
    DataSource = DataSource1
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    AutoAppend = False
    TitleButtons = True
    OnTitleBtnClick = DBGrid1TitleBtnClick
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'autorizacao_id'
        Title.Alignment = taRightJustify
        Title.Caption = 'Autoriza'#231#227'o ID'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'digito'
        Title.Alignment = taRightJustify
        Title.Caption = 'Digito'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_id'
        Title.Caption = 'Trans. ID'
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'data'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'data_fecha_emp'
        Title.Alignment = taCenter
        Title.Caption = 'Data Fechamento Emp.'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'data_venc_emp'
        Title.Caption = 'Data Venc. Emp.'
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'hora'
        Title.Alignment = taCenter
        Title.Caption = 'Hora'
        Width = 50
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'receita'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Caption = 'Receita'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nf'
        Title.Alignment = taCenter
        Title.Caption = 'NF'
        Width = 40
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'entreg_nf'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Caption = 'Entrega NF'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'conv_id'
        Title.Caption = 'Conv. ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'chapa'
        Title.Caption = 'Chapa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Title.Caption = 'Titular'
        Width = 228
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'liberado'
        Title.Alignment = taCenter
        Title.Caption = 'Liberado'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'debito'
        Title.Alignment = taRightJustify
        Title.Caption = 'D'#233'bito'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = [fsBold]
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'credito'
        Title.Alignment = taRightJustify
        Title.Caption = 'Cr'#233'dito'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = [fsBold]
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'
        Title.Caption = 'Nome'
        Width = 217
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fornecedor'
        Title.Caption = 'Estabelecimento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'data_alteracao'
        Title.Caption = 'Data Altera'#231#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'operador'
        Title.Caption = 'Operador'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'historico'
        Title.Caption = 'Hist'#243'rico'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'obs1'
        Title.Caption = 'Obs. '
        Visible = True
      end>
  end
  inherited PopupBut: TPopupMenu
    Left = 276
    Top = 320
  end
  object DSQuery1: TDataSource
    DataSet = Query1
    Left = 376
    Top = 240
  end
  object DSEmpresa: TDataSource
    DataSet = Empresa
    Left = 525
    Top = 376
  end
  object dscrCredenciado: TDataSource
    DataSet = Credenciado
    Left = 528
    Top = 320
  end
  object Query1: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select contacorrente.autorizacao_id, contacorrente.trans_id, con' +
        'tacorrente.debito, contacorrente.credito,'
      
        'contacorrente.data, contacorrente.digito, contacorrente.data_fec' +
        'ha_emp,'
      
        'contacorrente.historico, contacorrente.operador, contacorrente.h' +
        'ora,'
      'conveniados.obs1, conveniados.liberado, conveniados.chapa,'
      
        'contacorrente.entreg_nf, conveniados.titular, conveniados.conv_i' +
        'd,'
      
        'contacorrente.nf, contacorrente.receita, contacorrente.data_alte' +
        'racao,'
      'empresas.empres_id, empresas.nome, '#39'N'#39' as Marcado,'
      
        'contacorrente.cred_id, (Select nome from credenciados where cred' +
        '_id = contacorrente.cred_id) as fornecedor,'
      
        'empresas.TIPO_CREDITO, EMPRESAS.BAND_ID, CONTACORRENTE.DATA_VENC' +
        '_EMP '
      'from contacorrente'
      
        'join conveniados on (conveniados.conv_id = contacorrente.conv_id' +
        ')'
      'join empresas on (empresas.empres_id = conveniados.empres_id)'
      'join cartoes on (cartoes.conv_id = contacorrente.conv_id)')
    Left = 416
    Top = 240
    object Query1autorizacao_id: TIntegerField
      DisplayLabel = 'Autoriza'#231#227'o ID'
      FieldName = 'autorizacao_id'
    end
    object Query1trans_id: TIntegerField
      DisplayLabel = 'Trans. ID'
      FieldName = 'trans_id'
    end
    object Query1debito: TBCDField
      DisplayLabel = 'D'#233'bito'
      FieldName = 'debito'
      Precision = 15
      Size = 2
    end
    object Query1credito: TBCDField
      DisplayLabel = 'Cr'#233'dito'
      FieldName = 'credito'
      Precision = 15
      Size = 2
    end
    object Query1data: TDateTimeField
      DisplayLabel = 'Data'
      FieldName = 'data'
    end
    object Query1digito: TWordField
      DisplayLabel = 'D'#237'gito'
      FieldName = 'digito'
    end
    object Query1data_fecha_emp: TDateTimeField
      Alignment = taCenter
      DisplayLabel = 'Dt. Fechamento Emp.'
      FieldName = 'data_fecha_emp'
    end
    object Query1historico: TStringField
      DisplayLabel = 'Hist'#243'rico'
      FieldName = 'historico'
      Size = 80
    end
    object Query1operador: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'operador'
      Size = 25
    end
    object Query1hora: TStringField
      DisplayLabel = 'Hora'
      FieldName = 'hora'
      FixedChar = True
      Size = 8
    end
    object Query1obs1: TStringField
      DisplayLabel = 'Obs.'
      FieldName = 'obs1'
      Size = 80
    end
    object Query1liberado: TStringField
      Alignment = taCenter
      DisplayLabel = 'Liberado'
      FieldName = 'liberado'
      FixedChar = True
      Size = 1
    end
    object Query1chapa: TFloatField
      DisplayLabel = 'Chapa'
      FieldName = 'chapa'
    end
    object Query1entreg_nf: TStringField
      DisplayLabel = 'Entrega NF'
      FieldName = 'entreg_nf'
      FixedChar = True
      Size = 1
    end
    object Query1titular: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'titular'
      Size = 58
    end
    object Query1conv_id: TIntegerField
      DisplayLabel = 'Conv. ID'
      FieldName = 'conv_id'
    end
    object Query1nf: TIntegerField
      DisplayLabel = 'NF'
      FieldName = 'nf'
    end
    object Query1receita: TStringField
      DisplayLabel = 'Receita'
      FieldName = 'receita'
      FixedChar = True
      Size = 1
    end
    object Query1data_alteracao: TDateTimeField
      DisplayLabel = 'Data Altera'#231#227'o'
      FieldName = 'data_alteracao'
    end
    object Query1empres_id: TAutoIncField
      DisplayLabel = 'Empresa ID'
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object Query1nome: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'nome'
      Size = 60
    end
    object Query1Marcado: TStringField
      FieldName = 'Marcado'
      ReadOnly = True
      Size = 1
    end
    object Query1cred_id: TIntegerField
      DisplayLabel = 'Cred. ID'
      FieldName = 'cred_id'
    end
    object Query1fornecedor: TStringField
      DisplayLabel = 'Fornecedor'
      FieldName = 'fornecedor'
      ReadOnly = True
      Size = 60
    end
    object Query1TIPO_CREDITO: TIntegerField
      FieldName = 'TIPO_CREDITO'
    end
    object Query1BAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object Query1DATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
  end
  object qConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'conv_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'Select consumo_mes_1, consumo_mes_2, consumo_mes_3, consumo_mes_' +
        '4,'
      'limite_mes, saldo_acumulado from conveniados'
      'where  conv_id = :conv_id'
      'and apagado <> '#39'S'#39)
    Left = 600
    Top = 280
    object qConvconsumo_mes_1: TBCDField
      FieldName = 'consumo_mes_1'
      Precision = 15
      Size = 2
    end
    object qConvconsumo_mes_2: TBCDField
      FieldName = 'consumo_mes_2'
      Precision = 15
      Size = 2
    end
    object qConvconsumo_mes_3: TBCDField
      FieldName = 'consumo_mes_3'
      Precision = 15
      Size = 2
    end
    object qConvconsumo_mes_4: TBCDField
      FieldName = 'consumo_mes_4'
      Precision = 15
      Size = 2
    end
    object qConvlimite_mes: TBCDField
      FieldName = 'limite_mes'
      Precision = 15
      Size = 2
    end
    object qConvsaldo_acumulado: TBCDField
      FieldName = 'saldo_acumulado'
      Precision = 15
      Size = 2
    end
  end
  object Credenciado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select Cred_id, nome, comissao, fantasia from credenciados '
      'where apagado <> '#39'S'#39
      'order by nome')
    Left = 600
    Top = 328
    object CredenciadoCred_id: TIntegerField
      FieldName = 'Cred_id'
    end
    object Credenciadonome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object Credenciadocomissao: TBCDField
      FieldName = 'comissao'
      Precision = 6
      Size = 2
    end
    object Credenciadofantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
  end
  object Empresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select empres_id, nome from empresas  '
      'where apagado <> '#39'S'#39
      'order by nome')
    Left = 600
    Top = 376
    object Empresaempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object Empresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object MDQuery1: TJvMemoryData
    FieldDefs = <
      item
        Name = 'autorizacao_id'
        DataType = ftInteger
      end
      item
        Name = 'trans_id'
        DataType = ftInteger
      end
      item
        Name = 'debito'
        DataType = ftCurrency
      end
      item
        Name = 'credito'
        DataType = ftCurrency
      end
      item
        Name = 'data'
        DataType = ftDate
      end
      item
        Name = 'digito'
        DataType = ftInteger
      end
      item
        Name = 'data_fecha_emp'
        DataType = ftDate
      end
      item
        Name = 'historico'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'operador'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'hora'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'obs1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'liberado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'chapa'
        DataType = ftFloat
      end
      item
        Name = 'entrega_nf'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'titular'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'conv_id'
        DataType = ftInteger
      end
      item
        Name = 'nf'
        DataType = ftInteger
      end
      item
        Name = 'receita'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'data_alteracao'
        DataType = ftDate
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
      end
      item
        Name = 'nome'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end
      item
        Name = 'cred_id'
        DataType = ftInteger
      end
      item
        Name = 'fornecedor'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'data_venc_emp'
        DataType = ftDate
      end>
    Left = 376
    Top = 288
    object MDQuery1autorizacao_id: TIntegerField
      FieldName = 'autorizacao_id'
    end
    object MDQuery1trans_id: TIntegerField
      FieldName = 'trans_id'
    end
    object MDQuery1debito: TFloatField
      FieldName = 'debito'
      DisplayFormat = '#0.00'
    end
    object MDQuery1credito: TFloatField
      FieldName = 'credito'
      DisplayFormat = '#0.00'
    end
    object MDQuery1data: TDateField
      FieldName = 'data'
    end
    object MDQuery1digito: TIntegerField
      FieldName = 'digito'
    end
    object MDQuery1data_fecha_emp: TDateField
      FieldName = 'data_fecha_emp'
    end
    object MDQuery1historico: TStringField
      FieldName = 'historico'
    end
    object MDQuery1operador: TStringField
      FieldName = 'operador'
    end
    object MDQuery1obs1: TStringField
      FieldName = 'obs1'
    end
    object MDQuery1liberado: TStringField
      FieldName = 'liberado'
    end
    object MDQuery1entrega_nf: TStringField
      FieldName = 'entrega_nf'
    end
    object MDQuery1titular: TStringField
      FieldName = 'titular'
      Size = 50
    end
    object MDQuery1conv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object MDQuery1nf: TIntegerField
      FieldName = 'nf'
    end
    object MDQuery1receita: TStringField
      FieldName = 'receita'
    end
    object MDQuery1data_alteracao: TDateField
      FieldName = 'data_alteracao'
    end
    object MDQuery1empres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object MDQuery1nome: TStringField
      FieldName = 'nome'
      Size = 50
    end
    object MDQuery1marcado: TBooleanField
      FieldName = 'marcado'
    end
    object MDQuery1cred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object MDQuery1fornecedor: TStringField
      FieldName = 'fornecedor'
    end
    object MDQuery1hora: TStringField
      FieldName = 'hora'
    end
    object MDQuery1chapa: TFloatField
      FieldName = 'chapa'
    end
    object MDQuery1data_venc_emp: TDateField
      FieldName = 'data_venc_emp'
    end
  end
  object DataSource1: TDataSource
    DataSet = MDQuery1
    Left = 416
    Top = 288
  end
  object QBuscaSeg_id: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select seg_id from CREDENCIADOS,contacorrente'
      'where contacorrente.CRED_ID = CREDENCIADOS.CRED_ID'
      'and credenciados.CRED_ID LIKE '#39'%teste%'#39)
    Left = 392
    Top = 344
    object QBuscaSeg_idseg_id: TIntegerField
      FieldName = 'seg_id'
    end
  end
end
