object FrmAlterarValorEQtde: TFrmAlterarValorEQtde
  Left = 194
  Top = 119
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Atualiza'#231#227'o de Dados AESP Odonto'
  ClientHeight = 371
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 313
    Height = 369
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 66
      Height = 13
      Caption = 'Cod. Empresa'
    end
    object Label2: TLabel
      Left = 152
      Top = 24
      Width = 42
      Height = 13
      Caption = 'Conv. ID'
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 24
      Height = 13
      Caption = 'Valor'
    end
    object Label4: TLabel
      Left = 152
      Top = 80
      Width = 55
      Height = 13
      Caption = 'Quantidade'
    end
    object Label5: TLabel
      Left = 8
      Top = 176
      Width = 94
      Height = 13
      Caption = 'Motivo da altera'#231#227'o'
    end
    object lblOperador: TLabel
      Left = 240
      Top = 176
      Width = 44
      Height = 13
      Caption = 'Operador'
    end
    object Label6: TLabel
      Left = 184
      Top = 176
      Width = 47
      Height = 13
      Caption = 'Operador:'
    end
    object btnCancelar: TButton
      Left = 176
      Top = 312
      Width = 59
      Height = 49
      Caption = 'Cancelar'
      TabOrder = 7
      OnClick = btnCancelarClick
    end
    object btnConfirma: TButton
      Left = 248
      Top = 312
      Width = 59
      Height = 49
      Caption = 'Confirmar'
      TabOrder = 6
      OnClick = btnConfirmaClick
    end
    object codEmpresInput: TEdit
      Left = 8
      Top = 40
      Width = 97
      Height = 21
      TabOrder = 0
      OnKeyPress = codEmpresInputKeyPress
    end
    object convIdInput: TEdit
      Left = 152
      Top = 40
      Width = 97
      Height = 21
      TabOrder = 1
      OnKeyPress = convIdInputKeyPress
    end
    object valorInput: TEdit
      Left = 8
      Top = 96
      Width = 97
      Height = 21
      TabOrder = 2
    end
    object QtdeInput: TEdit
      Left = 152
      Top = 96
      Width = 97
      Height = 21
      Color = clYellow
      Enabled = False
      TabOrder = 3
      OnKeyPress = QtdeInputKeyPress
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 136
      Width = 113
      Height = 17
      Caption = 'Alterar Quantidade'
      TabOrder = 4
      OnClick = CheckBox1Click
    end
    object Motivo: TMemo
      Left = 5
      Top = 200
      Width = 300
      Height = 81
      TabOrder = 5
    end
  end
  object QCadastro: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT CONV_ID, EMPRES_ID, LIBERADO, APAGADO FROM CONVENIADOS')
    Left = 156
    Top = 137
    object QCadastroCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
  end
  object QAESP: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT EMPRES_ID, CONV_ID, LIBERADO FROM CONV_AESP_ODONTO')
    Left = 196
    Top = 137
    object QAESPEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QAESPCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QAESPLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
  end
end
