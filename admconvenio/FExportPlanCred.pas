unit FExportPlanCred;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, Buttons, ComCtrls, StdCtrls, Mask,
  JvExMask, JvToolEdit, ExtCtrls, Menus, ComObj, ADODB, JvMemoryDataset;

type
  TFrmExportPlanCred = class(TF1)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Bevel1: TBevel;
    btnListarEmp: TButton;
    DataIni: TJvDateEdit;
    por_dia_fecha: TRadioButton;
    por_periodo: TRadioButton;
    dataFin: TJvDateEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    ButMarcDesm_Emp: TButton;
    ButMarcaTodos_Emp: TButton;
    ButDesmTodos_Emp: TButton;
    btnGerarXls: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    dsEmp: TDataSource;
    lblTotal: TLabel;
    edtDir: TJvDirectoryEdit;
    Label1: TLabel;
    qEmp: TADOQuery;
    qEmpempres_id: TIntegerField;
    qEmpnome: TStringField;
    qEmpdata_fecha: TDateTimeField;
    qEmpdata_venc: TDateTimeField;
    qConv: TADOQuery;
    qConvempres_id: TIntegerField;
    qConvconv_id: TIntegerField;
    qConvcodcartimp: TStringField;
    qConvtitular: TStringField;
    qConvvalor_renovacao: TBCDField;
    MDEmp: TJvMemoryData;
    MDEmpempres_id: TIntegerField;
    MDEmpnome: TStringField;
    MDEmpdata_fecha: TDateTimeField;
    MDEmpdata_venc: TDateTimeField;
    MDEmpmarcado: TBooleanField;
    procedure por_dia_fechaClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure btnListarEmpClick(Sender: TObject);
    procedure btnGerarXlsClick(Sender: TObject);
    procedure dsEmpDataChange(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    procedure selecionaTipoData(rb: TRadioButton);
    procedure montarSqlConv;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmExportPlanCred: TFrmExportPlanCred;

implementation

uses DM, cartao_util, URotinasTexto;

{$R *.dfm}

procedure TFrmExportPlanCred.selecionaTipoData(rb: TRadioButton);
begin
  por_dia_fecha.Checked := rb = por_dia_fecha;
  datafin.Visible   := not (rb = por_dia_fecha);
  if rb = por_dia_fecha then begin
    label2.Caption    := 'Dia de Fechamento';
  end else begin
    label2.Caption    := 'Per�odo do Fechamento';
  end;
  por_periodo.Checked := not (rb = por_dia_fecha);
  DataIni.SetFocus;
end;

procedure TFrmExportPlanCred.montarSqlConv;
var s : string;
begin
  s := '';
  qConv.Close;
  qConv.SQL.Clear;
  while not MDEmp.Eof do begin
    if (MDEmpmarcado.AsBoolean = true) then begin
      if s = '' then
        s := ' and ( empres_id = ' + MDEmpempres_id.AsString + sLineBreak
      else
        s := s + 'or empres_id = ' + MDEmpempres_id.AsString + sLineBreak;
    end;
    MDEmp.Next;
  end;
  if s <> '' then
    s := s + ')';
  qConv.SQL.Text := 'select ' + sLineBreak +
                    ' c.empres_id, ' + sLineBreak +
                    ' c.conv_id, ' + sLineBreak +
                    ' card.codcartimp, ' + sLineBreak +
                    ' c.titular, ' + sLineBreak +
                    ' c.saldo_renovacao as valor_renovacao' + sLineBreak +
                    'from ' + sLineBreak +
                    ' conveniados c ' + sLineBreak +
                    ' join cartoes card on card.conv_id = c.conv_id ' + sLineBreak +
                    'where c.apagado <> ''S'' ' + sLineBreak + s +
                    'and card.apagado <> ''S'' ' + sLineBreak +
                    'and card.titular = ''S'' ' + sLineBreak +
                    'order by c.titular';
  qConv.Open;
end;

procedure TFrmExportPlanCred.por_dia_fechaClick(Sender: TObject);
begin
  inherited;
  selecionaTipoData(TRadioButton(Sender));
end;

procedure TFrmExportPlanCred.ButMarcDesm_EmpClick(Sender: TObject);
begin
  inherited;
  //DMConexao.MarcaDesm(QEmp);
  if MDEmp.IsEmpty then Exit;
    MDEmp.Edit;
    MDEmpmarcado.AsBoolean := not MDEmpmarcado.AsBoolean;
    MDEmp.Post;
end;

procedure TFrmExportPlanCred.ButMarcaTodos_EmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  //DMConexao.MarcaDesmTodos(QEmp);
  if MDEmp.IsEmpty then Exit;
  MDEmp.DisableControls;
  marca := MDEmp.GetBookmark;
  //POSICIONA O CURSOR DO GRID PRINCIPAL NO PRIMEIRO REGISTRO
  MDEmp.First;
  while not MDEmp.Eof do
  begin
    MDEmp.Edit;
    MDEmpmarcado.AsBoolean := True;
    MDEmp.Post;

    MDEmp.Next;
  end;
  MDEmp.GotoBookmark(marca);
  MDEmp.FreeBookmark(marca);
  MDEmp.EnableControls;
end;

procedure TFrmExportPlanCred.ButDesmTodos_EmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  //DMConexao.MarcaDesmTodos(QEmp,False);
  if MDEmp.IsEmpty then Exit;
  MDEmp.DisableControls;
  marca := MDEmp.GetBookmark;
  //POSICIONA O CURSOR DO GRID PRINCIPAL NO PRIMEIRO REGISTRO
  MDEmp.First;
  while not MDEmp.Eof do
  begin
    MDEmp.Edit;
    MDEmpmarcado.AsBoolean := False;
    MDEmp.Post;

    MDEmp.Next;
  end;
  MDEmp.GotoBookmark(marca);
  MDEmp.FreeBookmark(marca);
  MDEmp.EnableControls;
end;

procedure TFrmExportPlanCred.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm_EmpClick(nil);
end;

procedure TFrmExportPlanCred.btnListarEmpClick(Sender: TObject);
begin
  inherited;
  if(Pos(' ',DataIni.Text) > 0) then begin
    MsgInf('Selecione a data de Fechamento');
    DataIni.SetFocus;
  end else if(por_periodo.Checked) and (Pos(' ',dataFin.Text) > 0) then begin
    MsgInf('Selecione a data final de Fechamento');
    datafin.SetFocus;
  end else begin
    qEmp.Close;
    qEmp.Parameters.ParamByName('dataIni').Value := StrToDateTime(FormatDateTime('dd/mm/yyyy 00:00:00',StrToDateTime(DataIni.Text)));
    if por_periodo.Checked then begin
      qEmp.Parameters.ParamByName('dataFin').Value := StrToDateTime(FormatDateTime('dd/mm/yyyy 23:59:00',StrToDateTime(DataFin.Text)));
    end else begin
      qEmp.Parameters.ParamByName('dataFin').Value := StrToDateTime(FormatDateTime('dd/mm/yyyy 23:59:00',StrToDateTime(DataIni.Text)));
    end;
    qEmp.Open;
    qEmp.First;
    MDEmp.Open;
    MDEmp.EmptyTable;
    MDEmp.DisableControls;

    while not qEmp.Eof do
    begin
      MDEmp.Append;
      MDEmpempres_id.AsInteger   := qEmpempres_id.AsInteger;
      MDEmpnome.AsString         := qEmpnome.AsString;
      MDEmpdata_fecha.AsDateTime := qEmpdata_fecha.AsDateTime;
      MDEmpdata_venc.AsDateTime  := qEmpdata_venc.AsDateTime;
      MDEmpmarcado.AsBoolean     := false;
      qEmp.Next;
    end;
    MDEmp.Next;
    MDEmp.First;
    MDEmp.EnableControls;
    lblTotal.Caption := 'Total: ' + IntToStr(qEmp.RecordCount);
  end;
end;

procedure TFrmExportPlanCred.btnGerarXlsClick(Sender: TObject);
var
  Arquivo : string;
  i , coluna, linha: integer;
  excel: variant;
  valor: variant;
begin
  MDEmp.First;
  if not DirectoryExists(edtDir.Text) then begin
    MsgInf('Informe um diret�rio para salvar as planilhas');
    edtDir.SetFocus;
    Exit;
  end;
  while not MDEmp.Eof do begin
    if (MDEmpmarcado.AsBoolean = true) then begin
      qConv.Close;
      qConv.Parameters[0].Value := MDEmpempres_id.AsInteger;
      qConv.Open;
      //montando excel
      try
        excel := CreateOleObject('Excel.Application');
        excel.Workbooks.add(1);
      except
        ShowMessage('N�o foi possivel abrir o excel!');
        SysUtils.Abort
      end;

      try
        Screen.Cursor := crHourGlass;
        //Arquivo := ExtractFilePath(Application.ExeName) + qEmpEMPRES_ID.AsString + '_' + fnRemoverCaracterParaSalvarEmArq(qEmpNOME.AsString) + '_credito_alimentacao.xlsx';
        Arquivo := edtDir.Text + '\' + MDEmpempres_id.AsString + '_' + fnRemoverCaracterParaSalvarEmArq(MDEmpnome.AsString) + '_credito_alimentacao.xlsx';
        coluna := 0;
        linha  := 1;
        for i:=0 to qConv.FieldCount - 1  do begin
          inc(coluna);
          valor := qConv.Fields[i].DisplayLabel;
          excel.cells[1,coluna] := valor;
          excel.cells[linha,coluna].Font.Color := $001717FF; //pintandoa fonte de vermelho
          excel.cells[linha,coluna].Font.bold := True;
          if coluna = 5 then begin
            excel.cells[linha,coluna] := 'VALOR RENOVA�AO';
          end;
        end;
        inc(linha);
        qConv.First;
        While not qConv.Eof do begin
          Coluna := 0;
          for i:= 0 to qConv.FieldCount-1 do begin
            inc(coluna);
            if (qConv.Fields[i] is TIntegerField) or (qConv.Fields[i] is TFloatField) then begin
              valor := qConv.Fields[i].AsString;
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFC8;
            end else begin
              valor := qConv.Fields[i].AsString;
              excel.cells[linha,coluna].NumberFormat := '@';
              excel.cells[linha,coluna].HorizontalAlignment := $FFFFEFDD;
            end;
            if coluna = 5 then begin
              excel.cells[linha,coluna].Interior.Color := $00F1D9C5;
            end;
            excel.cells[linha,coluna].Value := valor;
          end;
          inc(linha);
          qConv.Next;
        end;
        excel.columns.AutoFit;
        excel.ActiveWorkBook.SaveAs(FileName:=Arquivo,Password := '');
        excel.Visible := true;
      except on e:Exception do
        msgErro('Ocorreu um problema durande a gera��o do arquivo.' + sLineBreak +
        e.message);
      end;
    end;
    MDEmp.Next;
  end;
  Screen.Cursor := crDefault;
  MsgInf('Arquivos gerados com sucesso!');
end;

procedure TFrmExportPlanCred.dsEmpDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnGerarXls.Enabled := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
end;

procedure TFrmExportPlanCred.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key = VK_F5)  then
    btnListarEmp.Click;
  if (key = VK_F7) and (btnGerarXls.Enabled) then
    btnGerarXls.Click;
  if (key = VK_F8) then
    DMConexao.MarcaDesmTodos(QEmp)
  else if (key = VK_F9) then
    DMConexao.MarcaDesmTodos(QEmp,False)
  else if (key = VK_F11) then
    DMConexao.MarcaDesm(QEmp);
end;

procedure TFrmExportPlanCred.FormCreate(Sender: TObject);
begin
  inherited;
  DataIni.Date := now;
end;

end.
