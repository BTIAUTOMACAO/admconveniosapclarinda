unit UCadAluno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ExtCtrls, DBCtrls, StdCtrls, JvExControls, JvDBLookup,
  ADODB, Menus, Mask, JvExMask, JvToolEdit, ComCtrls, Grids, DBGrids, UClassLog,
  JvExDBGrids, JvDBGrid, Buttons;

type
  TFCadAluno = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label30: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    Label93: TLabel;
    Label39: TLabel;
    lblAddBairro: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    dbEdit2: TDBEdit;
    dbEdtChapa: TDBEdit;
    DBEmpresa: TJvDBLookupCombo;
    DBEdit7: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit10: TDBEdit;
    dbLkpCidades: TDBLookupComboBox;
    dbLkpEstados: TDBLookupComboBox;
    edtEmpr: TEdit;
    dbLkpBairros: TDBLookupComboBox;
    btnAdicionaBairro: TBitBtn;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit60: TDBEdit;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    GroupBox8: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label73: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBBanco: TJvDBLookupCombo;
    GroupBox9: TGroupBox;
    Label22: TLabel;
    Bevel4: TBevel;
    DBEdit16: TDBEdit;
    dbEdtEmail: TDBEdit;
    TabSheet4: TTabSheet;
    GroupBox12: TGroupBox;
    Label8: TLabel;
    DBComboBox2: TDBComboBox;
    Label17: TLabel;
    DBComboBox3: TDBComboBox;
    Label29: TLabel;
    txtTaxaRecarga: TDBEdit;
    QCadastroCONV_ID: TIntegerField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroCHAPA: TFloatField;
    QCadastroSENHA: TStringField;
    QCadastroTITULAR: TStringField;
    QCadastroLIMITE_MES: TBCDField;
    QCadastroCONSUMO_DIARIO: TBCDField;
    QCadastroCONSUMO_MES: TBCDField;
    QCadastroLIBERADO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroRESPONSAVEL: TStringField;
    QCadastroRG_RESPONSAVEL: TStringField;
    QCadastroCPF_RESPONSAVEL: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroBAIRRO: TIntegerField;
    QCadastroCIDADE: TIntegerField;
    QCadastroESTADO: TIntegerField;
    QCadastroCEP: TStringField;
    QCadastroTELEFONE: TStringField;
    QCadastroEMAIL: TStringField;
    QCadastroSERIE: TStringField;
    QCadastroMODELO_CARTAO: TIntegerField;
    QCadastroCONSULTA_LIMITE: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOBS1: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroOPERADOR: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroBANCO: TIntegerField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTACORRENTE: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroTAXA_RECARGA: TFloatField;
    QCadastroEMPRESA: TStringField;
    QEmpresa: TADOQuery;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresausa_cod_importacao: TStringField;
    QEmpresafantasia: TStringField;
    QEmpresafidelidade: TStringField;
    QEmpresaband_id: TIntegerField;
    QEmpresausa_novo_cartao: TStringField;
    DSEmpresa: TDataSource;
    QConvDetail: TADOQuery;
    QConvDetailCONV_ID: TIntegerField;
    QConvDetailPIS: TFloatField;
    QConvDetailNOME_PAI: TStringField;
    QConvDetailNOME_MAE: TStringField;
    QConvDetailCART_TRAB_NUM: TIntegerField;
    QConvDetailCART_TRAB_SERIE: TStringField;
    QConvDetailREGIME_TRAB: TStringField;
    QConvDetailVENC_TOTAL: TBCDField;
    QConvDetailESTADO_CIVIL: TStringField;
    QConvDetailNUM_DEPENDENTES: TIntegerField;
    QConvDetailDATA_ADMISSAO: TDateTimeField;
    QConvDetailDATA_DEMISSAO: TDateTimeField;
    QConvDetailFIM_CONTRATO: TDateTimeField;
    QConvDetailDISTRITO: TStringField;
    QConvDetailSALDO_DEVEDOR: TBCDField;
    QConvDetailSALDO_DEVEDOR_FAT: TBCDField;
    QContaCorrente: TADOQuery;
    QContaCorrenteAUTORIZACAO_ID: TIntegerField;
    QContaCorrenteCARTAO_ID: TIntegerField;
    QContaCorrenteCONV_ID: TIntegerField;
    QContaCorrenteCRED_ID: TIntegerField;
    QContaCorrenteDIGITO: TWordField;
    QContaCorrenteDATA: TDateTimeField;
    QContaCorrenteHORA: TStringField;
    QContaCorrenteDATAVENDA: TDateTimeField;
    QContaCorrenteDEBITO: TBCDField;
    QContaCorrenteCREDITO: TBCDField;
    QContaCorrenteVALOR_CANCELADO: TBCDField;
    QContaCorrenteBAIXA_CONVENIADO: TStringField;
    QContaCorrenteBAIXA_CREDENCIADO: TStringField;
    QContaCorrenteENTREG_NF: TStringField;
    QContaCorrenteRECEITA: TStringField;
    QContaCorrenteCESTA: TStringField;
    QContaCorrenteCANCELADA: TStringField;
    QContaCorrenteDIGI_MANUAL: TStringField;
    QContaCorrenteTRANS_ID: TIntegerField;
    QContaCorrenteFORMAPAGTO_ID: TIntegerField;
    QContaCorrenteFATURA_ID: TIntegerField;
    QContaCorrentePAGAMENTO_CRED_ID: TIntegerField;
    QContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    QContaCorrenteOPERADOR: TStringField;
    QContaCorrenteDATA_VENC_EMP: TDateTimeField;
    QContaCorrenteDATA_FECHA_EMP: TDateTimeField;
    QContaCorrenteDATA_VENC_FOR: TDateTimeField;
    QContaCorrenteDATA_FECHA_FOR: TDateTimeField;
    QContaCorrenteHISTORICO: TStringField;
    QContaCorrenteNF: TIntegerField;
    QContaCorrenteDATA_ALTERACAO: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CONV: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CRED: TDateTimeField;
    QContaCorrenteOPER_BAIXA_CONV: TStringField;
    QContaCorrenteOPER_BAIXA_CRED: TStringField;
    QContaCorrenteDATA_CONFIRMACAO: TDateTimeField;
    QContaCorrenteOPER_CONFIRMACAO: TStringField;
    QContaCorrenteCONFERIDO: TStringField;
    QContaCorrenteNSU: TIntegerField;
    QContaCorrentePREVIAMENTE_CANCELADA: TStringField;
    QContaCorrenteEMPRES_ID: TIntegerField;
    QContaCorrenteCREDENCIADO: TStringField;
    DSContaCorrente: TDataSource;
    procedure ButBuscaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroAfterRefresh(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure edtEmprChange(Sender: TObject);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure DSCadastroDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    incluindo : Boolean;
    LogCartoes, LogConvDetail, LogContaC : TLog;
  public
    { Public declarations }
    procedure buscaConveniado;
    procedure LimpaEdits;
    procedure Valida;
  end;

var
  FCadAluno: TFCadAluno;

implementation

uses DM, UMenu, DateUtils, UAltContaCor, USaldoMes, UTipos, Types, StrUtils, cartao_util, UDigitaSenha,
  USelTipoImp, ULimiteSeg, UAltLinSenha, UCancelaAutor, TypInfo,
  USQLMount, UValidacao, UComandosQuery, URotinasTexto, FOcorrencia,
  UCadConv;

{$R *.dfm}

procedure TFCadAluno.ButBuscaClick(Sender: TObject);
begin
  inherited;
  buscaConveniado;
end;

procedure TFCadAluno.buscaConveniado;
begin
  QCadastro.Close;
//  if ((Trim(EdCod.Text) = '') and (Trim(EdNome.Text) = '') and (Trim(EdChapa.Text) = '') and (Trim(EdCodEmp.Text) = '')
//    and (Trim(EdNomeEmp.text) ='') and (Trim(EdCartao.Text) = '')) then
  if ((Trim(EdCod.Text) = '') and (Trim(EdNome.Text) = '')) then
  begin
     MsgInf('� necess�rio especificar um crit�rio de busca.');
     EdCod.SetFocus;
     Exit;
  end;
  Screen.Cursor := crHourGlass;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('SELECT ');
  QCadastro.SQL.Add(' distinct CONVENIADOS_CANTINEX.CONV_ID, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.EMPRES_ID, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CHAPA, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.SENHA, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.TITULAR, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.LIMITE_MES, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CONSUMO_DIARIO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CONSUMO_MES, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.LIBERADO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.APAGADO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.BANCO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.AGENCIA, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CONTACORRENTE, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.RESPONSAVEL, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.RG_RESPONSAVEL, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CPF_RESPONSAVEL,  ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.ENDERECO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.NUMERO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.BAIRRO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CIDADE, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.ESTADO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CEP, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.TELEFONE, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.EMAIL, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.SERIE, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.MODELO_CARTAO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.CONSULTA_LIMITE, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.DTCADASTRO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.DTALTERACAO, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.OBS1, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.OBS2, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.OPERADOR, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.TAXA_RECARGA, ');
  QCadastro.SQL.Add('CONVENIADOS_CANTINEX.OPERCADASTRO, ');
  QCadastro.SQL.Add('EMPRESAS_CANTINEX.FANTASIA, ');
  QCadastro.SQL.Add('EMPRESAS_CANTINEX.NOME EMPRESA');
  QCadastro.SQL.Add('from CONVENIADOS_CANTINEX');

  QCadastro.SQL.Add('join EMPRESAS_CANTINEX on EMPRESAS_CANTINEX.empres_id = CONVENIADOS_CANTINEX.empres_id and EMPRESAS_CANTINEX.apagado <> ''S''');
//  if ((Trim(EdCartao.Text) <> '') or (Trim(EdNome.Text) <> '')) then
//  begin
//    QCadastro.SQL.Add('join cartoes on cartoes.conv_id = alunos.conv_id and cartoes.apagado = ''N''');
//    QCadastro.Sql.Add(' and cartoes.nome like ''%' + fnRemoverCaracterSQLInjection(EdNome.Text) + '%''');
//    QCadastro.SQL.Add('where alunos.apagado <> ''S''');
//  end;

  QCadastro.SQL.Add('where CONVENIADOS_CANTINEX.apagado <> ''S''');
  if Trim(EdCod.Text) <> '' then
    QCadastro.Sql.Add(' and CONVENIADOS_CANTINEX.conv_id in (' + fnRemoverCaracterSQLInjection(EdCod.Text) + ')');
//  if Trim(EdBuscaCartao.Text) <> '' then
//    QCadastro.SQL.Add(' and alunos.cartao_id in (' + fnRemoverCaracterSQLInjection(EdBuscaCartao.Text) + ')');
//  if Trim(EdBuscaCartao.Text) <> '' then
//    QCadastro.SQL.Add(' and alunos.cartao_id in (' + fnRemoverCaracterSQLInjection(EdBuscaCartao.Text) + ')');

//  if Trim(EdChapa.Text) <> '' then
//    QCadastro.Sql.Add(' and alunos.chapa in (' + fnRemoverCaracterSQLInjection(EdChapa.Text) + ')');
//  if Trim(EdCodEmp.Text) <> '' then
//    QCadastro.Sql.Add(' and alunos.empres_id in (' + fnRemoverCaracterSQLInjection(EdCodEmp.Text) + ')');


//  if Trim(EdNomeEmp.text) <>''then
//  begin
//    DMConexao.Config.Open;
//    if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
//      QCadastro.Sql.Add(' and escolas.fantasia like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''')
//    else
//      QCadastro.Sql.Add(' and escolas.nome like ''%' + fnRemoverCaracterSQLInjection(EdNomeEmp.Text) + '%''');
//    DMConexao.Config.Close;
//  end;

//  if Trim(EdCartao.Text) <> '' then
//  begin
//    if (( Length(Trim(EdCartao.Text)) = 11) and ( DigitoCartao(StrToFloat(Copy(EdCartao.Text,1,9))) = StrToInt(Copy(EdCartao.Text,10,2)))) then
//      QCadastro.Sql.Add(' and (cartoes.codigo in (' + Copy( fnRemoverCaracterSQLInjection(EdCartao.Text),1,9) + ')' + ' or cartoes.codcartimp in (''' + fnRemoverCaracterSQLInjection(EdCartao.Text) + ''')))')
//    else begin
//      EdCartao.Text := fnRemoverCaracterSQLInjection(StringReplace(EdCartao.Text,',','","',[]));
//      QCadastro.Sql.Add(' and cartoes.codcartimp in ('''+fnRemoverCaracterSQLInjection(EdCartao.Text)+''')');
//    end;
//  end;
  QCadastro.Sql.Add('order by CONVENIADOS_CANTINEX.titular');
  //clipBoard.AsText := QCadastro.SQL.Text;
  Barra.Panels[0].Text := 'buscando conveniados';
  QCadastro.Open;
  Barra.Panels[0].Text := 'busca conclu�da conveniados';
  //QCadastro.EnableControls;
  if not QCadastro.IsEmpty then
  begin
    DBGrid1.SetFocus;
    Self.TextStatus := '  Aluno: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString;//+
    //'                    Escola: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroempresa.AsString;
  end
  else
    EdCod.SetFocus;
  LimpaEdits;
  Screen.Cursor := crDefault;
end;

procedure TFCadAluno.LimpaEdits;
begin
  EdCod.Clear;
  //EdBuscaCartao.Clear;
  EdNome.Clear;
  //EdCodEmp.Clear;
  //EdNomeEmp.Clear;
  //EdCartao.Clear;
  //EdChapa.Clear;
  //DEdataini.Clear;
  //DEdatafim.Clear;
end;

procedure TFCadAluno.FormCreate(Sender: TObject);
begin
  chavepri := 'conv_id';
  detalhe := 'Conv ID: ';
  DMConexao.Config.Open;
  //TabProgDesc.TabVisible:= (DMConexao.ConfigINTEGRA_SISTEMABIG.AsString = 'S');
  if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
    //ButAtualizaCodImp.Visible:= True
  else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
    //ButAtualizaCodImp.Visible:= True;
  if DMConexao.ConfigEMP_FANTASIA_CADCONV.AsString = 'S' then
  begin
    //DBEmpresa.LookupDisplay := 'empres_id;fantasia';
    DBEmpresa.LookupDisplay := 'fantasia';
    //Label32.Caption:= '&Nome Fantasia da Empresa';
    QCadastroempresa.LookupResultField:= 'fantasia';
  end
  else
  begin
    DBEmpresa.LookupDisplay := 'nome';
    //Label32.Caption:= '&Raz�o/Nome Empresa';
    QCadastroempresa.LookupResultField:= 'nome';
    //QCadastroempresa.LookupResultField:= 'empres_id;nome';
  end;

  DMConexao.Config.Close;
  QCadastro.Open;
  //inherited;

  //LogCartoes := TLog.Create;
  //LogCartoes.LogarQuery(QCartoes,'FCadCartoes','Cart�o ID:','FCadCartoes',Operador.Nome,'cartao_id');

  LogConvDetail:= TLog.Create;
  LogConvDetail.LogarQuery(QConvDetail,janela,'Conv ID: ','FCadConv',Operador.Nome,chavepri);

  LogContaC := TLog.Create;
  LogContaC.LogarQuery(Qcontacorrente,'CONTACORRENTE','Autoriza��o: ','Conta Corrente',Operador.Nome,'autorizacao_id');

  //QCadastro.Open;
//  try
//    QEmpresa.Open;
//  except
//  end;
//  try
//    QEmpresa.Open;
//  except
//  end;
//  QBanco.Open;
//  qEstados.Open;
//  qCidades.Open;
//  data1.date := date;
//  data2.date := date;
//  DatainiCartao.Date := Date;
//  DatafimCartao.Date := Date;
  FMenu.vCadAluno := True;

end;

procedure TFCadAluno.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  {Recuperando o �ltimo ID do conveniado}
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONV_ID_CANTINEX AS CONV_ID');
  DMConexao.AdoQry.Open;
  QCadastroCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  {Fim do bloco para recupera��o do ID}
  QCadastroLIBERADO.AsString          := 'S';
  QCadastroLIMITE_MES.AsCurrency      := 0;
  QCadastroAPAGADO.AsString          := 'N';
  QCadastroCONSUMO_MES.AsCurrency     := 0;
  QCadastroOPERADOR.AsString  := Operador.Nome;
  QCadastroOPERCADASTRO.Value := Operador.Nome;
  QCadastroDTCADASTRO.Value := now;
end;

procedure TFCadAluno.QCadastroAfterRefresh(DataSet: TDataSet);
begin
  inherited;
  Self.TextStatus := '  Titular: ['+QCadastroCONV_ID.AsString+'] - '+QCadastroTITULAR.AsString+
   '                    Empresa: ['+QCadastroEMPRES_ID.AsString+'] - '+QCadastroEMPRESA.AsString;
end;

procedure TFCadAluno.QCadastroBeforePost(DataSet: TDataSet);
var mens, verifica_cpf : string;
    colocouMensagem : Boolean;
  SQL : TSqlMount;
begin
  valida;
  //QCadastroCONV_ID.ProviderFlags:=[pfInUpdate,pfInWhere];
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;

  DMConexao.Config.Open;
  if (QCadastro.State = dsEdit) and (QCadastroTITULAR.OldValue <> QCadastroTITULAR.AsString) and (DMConexao.ConfigALTERAR_TITULAR_NOME_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set nome = '+ QuotedStr(QCadastroTITULAR.AsString)+'" where conv_id = '+QuotedStr(QCadastroCONV_ID.AsString)+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  if (QCadastro.State = dsEdit) and (QCadastroLIBERADO.OldValue <> QCadastroLIBERADO.AsString) and (DMConexao.ConfigALTERAR_TITULAR_LIBERADO_CARTAO.AsString = 'S') then begin
    DMConexao.Query2.Close;
    DMConexao.Query2.SQL.Text:= ' update cartoes set liberado = '+QuotedStr(QCadastroLIBERADO.AsString)+' where conv_id = '+QCadastroCONV_ID.AsString+' and titular = ''S'' ';
    DMConexao.Query2.ExecSQL;
  end;
  verifica_cpf := DMConexao.ConfigVERIFICA_CPF_CONV.AsString;
  if (verifica_cpf[1] in ['C','P']) and (Trim(QCadastroCPF_RESPONSAVEL.AsString) <> '') then
  begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := 'Select conv_id from PESQUISA_CPF("'+QCadastroCPF_RESPONSAVEL.AsString+'") where conv_id <> '+QCadastroCONV_ID.AsString;
    DMConexao.AdoQry.Open;
    if not DMConexao.AdoQry.IsEmpty then
      while not DMConexao.AdoQry.Eof do
      begin
        mens := mens + DMConexao.AdoQry.Fields[0].AsString+sLineBreak;
        DMConexao.AdoQry.Next;
      end;
      DMConexao.AdoQry.Close;
      if Trim(mens) <> '' then
      begin
      if verifica_cpf = 'P' then
      begin
        if Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens+sLineBreak+'Deseja continuar o cadastro?'),'Confirma��o',MB_YESNO+MB_ICONQUESTION) = IDNO then
          Sysutils.Abort;
      end
      else
      begin
        Application.MessageBox(PChar('Foi(ram) encontrado(s) conveniado(s) com o mesmo cpf. '+sLineBreak+'Conv ID(s):'+sLineBreak+mens),'Opera��o cancelada.',MB_OK+MB_ICONERROR);
        Sysutils.Abort;
      end;
    end;
  end;
  if QCadastro.State = dsInsert then
    incluindo := True;

  //if(DBEmp_Dpto.Value <> '')then
 // begin
    //DMConexao.ExecuteSql('UPDATE conveniados SET setor_id = ' + QEMP_DPTODEPT_ID.AsString + ' WHERE conv_id = '+QCadastroCONV_ID.AsString);
    //QCadastroSETOR.Value := QEMP_DPTODEPT_ID.AsString;
  //end;
  DMConexao.Config.Close;

end;

procedure TFCadAluno.Valida;
var mens, chapa : string; focar : TWinControl;
    qtdLimites : Integer;
    limite : string;
begin
  mens := '';
  if not (QCadastro.State in [dsInsert, dsEdit]) then
    Exit;
  if fnVerfCompVazioEmTabSheet('Informe o nome do conveniado.',dbEdit2     )                 then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o n� de chapa/matricula do conveniado.',dbEdtChapa)  then Abort;
  //if fnVerfCompVazioEmTabSheet('Informe o limite do conveniado.',dbEdtLimite)                then Abort;
  if fnVerfCompVazioEmTabSheet('Informe a empresa do conveniado.',DBEmpresa)                 then Abort;
  //if fnVerfCompVazioEmTabSheet('Informe o grupo do conveniado na empresa.',DBGrupo_conv_emp) then Abort;
  if fnVerfCompVazioEmTabSheet('Chapa Obrigat�ria'  ,dbEdtChapa)                             then Abort;
  if fnVerfCompVazioEmTabSheet('Empresa Obrigat�ria', DBEmpresa)                             then Abort;


  if Trim(dbEdtEmail.Text) <> '' then
    if not fnIsEmail(dbEdtEmail.Text) then
      begin
      MsgErro('Email inv�lido!');
      dbEdtEmail.SetFocus;
      Abort;
      end;
  if length(QCadastro.FieldByName('CPF').AsString) > 0 then
  begin
    QCadastro.FieldByName('CPF').AsString:= SoNumero(Trim(QCadastro.FieldByName('CPF').AsString));
    if not ValidaCPF(DBEdit12.Text) then
    begin
      MsgErro('CPF Inv�lido!');
      DBEdit12.SetFocus;
      Exit;
    end;
  end;
  if QCadastro.State = dsEdit then
  begin

    {
    if QCadastroEMPRES_ID.NewValue <> QCadastroEMPRES_ID.AsInteger then
    begin
      if DMConexao.ExecuteScalar(' select count(autorizacao_id) from contacorrente where conv_id = '+QCadastroCONV_ID.AsString+' and ((fatura_id > 0) or (pagamento_cred_id > 0)) ',0) = 0 then
      begin
        if DMConexao.ExecuteScalar(' select count(autorizacao_id) from contacorrente where conv_id = '+QCadastroCONV_ID.AsString,0) = 0 then
        begin
          if DMConexao.ExecuteScalar('select grupo_conv_emp_id from grupo_conv_emp where grupo_conv_emp_id = '+QCadastroGRUPO_CONV_EMP.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString,0) = 0 then
          begin
            mens := 'Selecione um grupo v�lido para essa empresa!';
            focar:= DBGrupo_conv_emp;
          end;
        end
        else
        begin
          if not MsgSimNao('Existem movimenta��es em aberto deste conveniado'+sLineBreak+'caso a nova empresa tenha datas de fechamento diferentes'+sLineBreak+'ser� mudada junto com a empresa as datas de fechamentos das autoriza��es'+sLineBreak+'Confirma a altera��o?',False) then
          begin
            mens := 'Opera��o Cancelada.';
            focar := DBGrupo_conv_emp;
          end;
        end;
      end
      else
      begin
        mens := 'Existem autoriza��es faturadas/baixadas desse conv�niado.'+sLineBreak+'Opera��o n�o permitida!';
        focar := DBGrupo_conv_emp;
      end;
    end;
    }
  end;
  if QCadastro.State in [dsInsert] then
  begin
    //chapa:= DMConexao.ExecuteScalar('select titular from conveniados where chapa = '+QCadastroCHAPA.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString,'');
    DMConexao.AdoQry.SQL.Text := 'select titular from conveniados where chapa = '+QCadastroCHAPA.AsString+' and empres_id = '+QCadastroEMPRES_ID.AsString;
    DMConexao.AdoQry.Open;
    chapa:=  DMConexao.AdoQry.Fields[0].AsString;

    if (chapa <> '') then
    begin
      mens := 'Chapa ja em uso nessa empresa para o conveniado: '+sLineBreak+chapa;
      focar := dbEdtChapa;
    end;


  end;
  if QCadastroBANCO.IsNull then QCadastroBANCO.AsInteger := 0;
  
end;

procedure TFCadAluno.edtEmprChange(Sender: TObject);
begin
 inherited;
  if Trim(edtEmpr.Text) <> '' then begin
     if QEmpresa.Locate('empres_id',edtEmpr.Text,[]) then
        DBEmpresa.KeyValue := edtEmpr.Text
     else begin
        DBEmpresa.ClearValue;
        {AposEmpEstabNull;
        cds.First;
        while not cds.Eof do
          cds.Delete;}
     end;
  end
  else begin
    DBEmpresa.ClearValue;
    {AposEmpEstabNull;
    cds.First;
    while not cds.Eof do
      cds.Delete; }
  end;

end;

procedure TFCadAluno.edtEmprKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFCadAluno.DSCadastroDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  edtEmpr.Text := QEmpresaempres_id.AsString;
end;

end.
