inherited FCadCredTef: TFCadCredTef
  Left = 236
  Top = 214
  Caption = 'Cadastro de Estabelecimentos POS/TEF'
  ClientHeight = 557
  ClientWidth = 1008
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 1008
    Height = 516
    ActivePage = tbCadPos
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 397
        Width = 1000
        inherited Label2: TLabel
          Left = 259
          Width = 31
          Caption = '&Raz'#227'o'
        end
        object Label37: TLabel [2]
          Left = 506
          Top = 3
          Width = 33
          Height = 13
          Caption = '&Cidade'
          FocusControl = EdCidade
        end
        object Label38: TLabel [3]
          Left = 70
          Top = 3
          Width = 40
          Height = 13
          Caption = '&Fantasia'
          FocusControl = EdNome
        end
        object Label49: TLabel [10]
          Left = 402
          Top = 3
          Width = 27
          Height = 13
          Caption = 'CNP&J'
          FocusControl = EdCidade
        end
        object lbl8: TLabel [11]
          Left = 655
          Top = 3
          Width = 41
          Height = 13
          Caption = 'Liberado'
        end
        inherited EdNome: TEdit
          Left = 254
          Width = 142
          TabOrder = 3
        end
        inherited ButBusca: TBitBtn
          Left = 747
          Height = 24
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 926
          TabOrder = 9
        end
        inherited EdCod: TEdit
          Width = 64
        end
        inherited ButFiltro: TBitBtn
          TabOrder = 7
        end
        inherited ButAltLin: TButton
          TabOrder = 8
        end
        object EdCidade: TEdit
          Left = 504
          Top = 18
          Width = 145
          Height = 21
          Hint = 'Busca pela cidade'
          CharCase = ecUpperCase
          TabOrder = 5
          OnKeyPress = EdNomeKeyPress
        end
        object EdFanta: TEdit
          Left = 70
          Top = 18
          Width = 179
          Height = 21
          Hint = 'Busca pelo nome fantasia'
          CharCase = ecUpperCase
          TabOrder = 2
          OnKeyPress = EdNomeKeyPress
        end
        object pb: TProgressBar
          Left = 608
          Top = 48
          Width = 313
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 10
          Visible = False
        end
        object EdCNPJ: TEdit
          Left = 401
          Top = 18
          Width = 97
          Height = 21
          Hint = 'Busca pela cidade'
          CharCase = ecUpperCase
          TabOrder = 4
          OnKeyPress = EdNomeKeyPress
        end
        object cbbLiberado: TComboBox
          Left = 656
          Top = 18
          Width = 81
          Height = 21
          ItemHeight = 13
          TabOrder = 6
          Text = 'TODOS'
          Items.Strings = (
            'TODOS'
            'S'
            'N'
            '')
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 1000
        Height = 397
        PopupMenu = PopupMenu1
        Columns = <
          item
            Expanded = False
            FieldName = 'CRED_ID_TEF'
            Title.Caption = 'C'#243'digo'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Title.Caption = 'Nome'
            Width = 205
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Lib.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANTASIA'
            Title.Caption = 'Nome Fantasia'
            Width = 210
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CGC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INSCRICAOEST'
            Title.Caption = 'Inscri'#231#227'o Estadual'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE1'
            Title.Caption = 'Telefone 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE2'
            Title.Caption = 'Telefone 2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FAX'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTATO'
            Title.Caption = 'Contato'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENVIAR_EMAIL'
            Title.Caption = 'Envia e-mail'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMAIL'
            Title.Caption = 'e-mail'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HOMEPAGE'
            Title.Caption = 'Home Page'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENDERECO'
            Title.Caption = 'Endere'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BAIRRO'
            Title.Caption = 'Bairro'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CIDADE'
            Title.Caption = 'Cidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ESTADO'
            Title.Caption = 'UF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTRATO'
            Title.Caption = 'Contrato'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPOFECHAMENTO'
            Title.Caption = 'Tipo Fechamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODACESSO'
            Title.Caption = 'C'#243'digo de Acesso'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SENHA'
            Title.Caption = 'Senha'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COMISSAO'
            Title.Caption = 'Comissao'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BANCO'
            Title.Caption = 'Banco'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AGENCIA'
            Title.Caption = 'Agencia'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTACORRENTE'
            Title.Caption = 'Conta Corrente'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS1'
            Title.Caption = 'Observa'#231#227'o1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS2'
            Title.Caption = 'Observa'#231#227'o2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTCADASTRO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOVOCODACESSO'
            Title.Caption = 'Novo C'#243'digo de Acesso'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APAGADO'
            Title.Caption = 'Apagado'
            Visible = False
          end>
      end
      inherited Barra: TStatusBar
        Top = 469
        Width = 1000
      end
    end
    inherited TabFicha: TTabSheet
      OnExit = TabFichaExit
      inherited Panel2: TPanel
        Top = 456
        Width = 1000
        inherited btnFirstB: TSpeedButton
          Left = 488
        end
        inherited btnPriorB: TSpeedButton
          Left = 521
        end
        inherited btnNextB: TSpeedButton
          Left = 554
        end
        inherited btnLastB: TSpeedButton
          Left = 587
        end
        object ButLimpaSenha: TBitBtn [4]
          Left = 261
          Top = 4
          Width = 100
          Height = 25
          Hint = 'Limpa a senha do estabelecimento'
          Caption = 'Limpar Senha'
          TabOrder = 3
          OnClick = ButLimpaSenhaClick
          Glyph.Data = {
            96050000424D9605000000000000960300002800000020000000100000000100
            08000000000000020000232E0000232E0000D800000000000000FFFFFF00CCFF
            FF0099FFFF0066FFFF0033FFFF0000FFFF00FFCCFF00CCCCFF0099CCFF0066CC
            FF0033CCFF0000CCFF00FF99FF00CC99FF009999FF006699FF003399FF000099
            FF00FF66FF00CC66FF009966FF006666FF003366FF000066FF00FF33FF00CC33
            FF009933FF006633FF003333FF000033FF00FF00FF00CC00FF009900FF006600
            FF003300FF000000FF00FFFFCC00CCFFCC0099FFCC0066FFCC0033FFCC0000FF
            CC00FFCCCC00CCCCCC0099CCCC0066CCCC0033CCCC0000CCCC00FF99CC00CC99
            CC009999CC006699CC003399CC000099CC00FF66CC00CC66CC009966CC006666
            CC003366CC000066CC00FF33CC00CC33CC009933CC006633CC003333CC000033
            CC00FF00CC00CC00CC009900CC006600CC003300CC000000CC00FFFF9900CCFF
            990099FF990066FF990033FF990000FF9900FFCC9900CCCC990099CC990066CC
            990033CC990000CC9900FF999900CC9999009999990066999900339999000099
            9900FF669900CC66990099669900666699003366990000669900FF339900CC33
            990099339900663399003333990000339900FF009900CC009900990099006600
            99003300990000009900FFFF6600CCFF660099FF660066FF660033FF660000FF
            6600FFCC6600CCCC660099CC660066CC660033CC660000CC6600FF996600CC99
            660099996600669966003399660000996600FF666600CC666600996666006666
            66003366660000666600FF336600CC3366009933660066336600333366000033
            6600FF006600CC00660099006600660066003300660000006600FFFF3300CCFF
            330099FF330066FF330033FF330000FF3300FFCC3300CCCC330099CC330066CC
            330033CC330000CC3300FF993300CC9933009999330066993300339933000099
            3300FF663300CC66330099663300666633003366330000663300FF333300CC33
            330099333300663333003333330000333300FF003300CC003300990033006600
            33003300330000003300FFFF0000CCFF000099FF000066FF000033FF000000FF
            0000FFCC0000CCCC000099CC000066CC000033CC000000CC0000FF990000CC99
            000099990000669900003399000000990000FF660000CC660000996600006666
            00003366000000660000FF330000CC3300009933000066330000333300000033
            0000FF000000CC00000099000000660000003300000000000000002D092D0000
            00000000000000000000012B072B0000000000000000000000002D35350B5F00
            000000000000000000002B2B2B2B2B000000000000000000000009033B350B5F
            0000000000000000000007002B2B072B000000000000000000002D010B5F350B
            5F0000000000000000000700002B2B2B2B0000000000000000000035010A3B35
            04350000000000000000002B00002B2B072B0000000000000000000035010A5F
            350A352D00000000000000002B00002B2B012B010000000000000000340A0909
            3B350A353B5F5F3400000000070000002B2B012B2B2B2B2B0000000035013501
            09350909090909345F0000002B002B00002B00000000002B2B00000009350935
            01030203020302030A340000012B012B0000010000000100072B000000000009
            340102020101010102350000000000012B00000000000000002B000000000000
            350102020335350902350000000000002B000000012B2B00002B000000000000
            350101013500013501350000000000002B0000002B00002B002B000000000000
            3501010135071101092E0000000000002B0000002B002B00012B000000000000
            2E0901012D5F010235000000000000002B0000002B2B00002B00000000000000
            00350901010109350000000000000000002B01000000012B0000000000000000
            00000A3535350A00000000000000000000002B2B2B2B2B000000}
          NumGlyphs = 2
        end
        inherited ButGrava: TBitBtn
          Left = 639
          TabOrder = 4
        end
        inherited ButCancela: TBitBtn
          Left = 724
          TabOrder = 5
        end
      end
      inherited Panel3: TPanel
        Width = 1000
        Height = 456
        object PageControl2: TPageControl
          Left = 2
          Top = 2
          Width = 996
          Height = 452
          ActivePage = TabSheet1
          Align = alClient
          MultiLine = True
          Style = tsFlatButtons
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = '&Dados'
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 988
              Height = 140
              Align = alTop
              Caption = 'Principal'
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 15
                Width = 33
                Height = 13
                Caption = 'C'#243'digo'
                FocusControl = DBEdit1
              end
              object Label4: TLabel
                Left = 94
                Top = 15
                Width = 76
                Height = 13
                Caption = 'Raz'#227'o Social'
                FocusControl = DBEdit2
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label5: TLabel
                Left = 456
                Top = 15
                Width = 71
                Height = 13
                Caption = 'Nome Fantasia'
                FocusControl = DBEdit3
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label7: TLabel
                Left = 269
                Top = 55
                Width = 37
                Height = 13
                Caption = 'Contato'
                FocusControl = DBEdit5
              end
              object Label9: TLabel
                Left = 539
                Top = 95
                Width = 86
                Height = 13
                Caption = 'C'#243'digo de Acesso'
                FocusControl = DBEdit6
              end
              object Label8: TLabel
                Left = 397
                Top = 55
                Width = 46
                Height = 13
                Caption = 'Endere'#231'o'
                FocusControl = DBEdit4
              end
              object Label10: TLabel
                Left = 8
                Top = 95
                Width = 27
                Height = 13
                Caption = 'Bairro'
                FocusControl = DBEdit7
              end
              object Label11: TLabel
                Left = 207
                Top = 95
                Width = 33
                Height = 13
                Caption = 'Cidade'
              end
              object Label12: TLabel
                Left = 419
                Top = 95
                Width = 21
                Height = 13
                Caption = 'CEP'
                FocusControl = DBEdit9
              end
              object Label13: TLabel
                Left = 155
                Top = 95
                Width = 17
                Height = 13
                Caption = 'UF'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object SpeedButton1: TSpeedButton
                Left = 509
                Top = 110
                Width = 23
                Height = 21
                Flat = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFD9B28CCF9D6CD4A577D9AD83DEB68FE2BD9932
                  65984C7FB24C7FB24C7FB24C7FB2326598CF9D6CD9B28CFFFFFFFFFFFFD6A26F
                  FFEEDDFFFFFFFFEEDDFFFFFFFFEEDD376A9D6598CC6598CC6598CC6598CC376A
                  9DFFEEDDD6A26FFFFFFFFFFFFFE5B17FFFEFDF988776FFEFDF988776FFEFDF3E
                  71A474A4D474A4D474A4D474A4D43E71A4FFEFDFE5B17FFFFFFFFFFFFFF5C18E
                  FFF0E2CCBAA9FFF0E2CCBAA9FFF0E2477AAD6598CC0032658BB5DF8BB5DF477A
                  ADFFF0E2F5C18EFFFFFFFFFFFFFFCC98FFF2E5FFFFFFFFF2E5FFFFFFFFF2E550
                  83B6A5C8ECA5C8ECA5C8ECA5C8EC5083B6FFF2E5FFCC98FFFFFF7FB2E5CC9865
                  FFF4E8988776FFF4E8988776FFF4E8749DC7A3C5E9BBD9F7BBD9F7A3C5E9749D
                  C7FFF4E8CC98657FB2E50065CC0065CCCCBAA9CCBAA9FFF7F0CCBAA9FFF7F0AF
                  C4DB84AEDACCE5FFCCE5FF84AEDAAFC4DBCCBAA90065CC0065CC0069D00098FF
                  0065CCCCBAA9FFF9F4FFF9F4FFF9F4EFEFF0B1C9E06598CC6598CCB1C9E0C1B7
                  AC0065CC0098FF0069D07FB7EA006FD60C9EFF0065CCCCBAA9FFFBF7FFFBF7FF
                  FBF7FFFBF7FFFBF7FFFBF7CCBAA90065CC0C9EFF006FD67FB7EAFFFFFF7FBBEE
                  0077DE1DA7FF0065CCCCBAA9FFFDFAFFFDFAFFFDFAFFFDFACCBAA90065CC1DA7
                  FF0077DE7FBBEEFFFFFFFFFFFFFFFFFF326598007FE532B1FF0065CCCCBAA9FF
                  FEFDFFFEFDCCBAA90065CC32B1FF007FE57FBFF2FFFFFFFFFFFFFFFFFFFFFFFF
                  3C6FA26598CC0086ED47BCFF0065CCCCBAA9CCBAA90065CC47BCFF0086ED7FC2
                  F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4C7FB198BEE64C7FB1008EF559C5FF00
                  65CC0065CC59C5FF008EF57FC6FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  5B8EC1CCE5FF5B8EC17FC9FD0094FB65CCFF65CCFF0094FB7FC9FDFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6598CC6598CC6598CCFFFFFF7FCBFF00
                  98FF0098FF7FCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                OnClick = SpeedButton1Click
              end
              object Label21: TLabel
                Left = 680
                Top = 55
                Width = 12
                Height = 13
                Caption = 'N'#186
                FocusControl = DBEdit15
              end
              object Label14: TLabel
                Left = 8
                Top = 55
                Width = 27
                Height = 13
                Caption = 'CNPJ'
                FocusControl = DBEdit11
              end
              object Label15: TLabel
                Left = 143
                Top = 55
                Width = 87
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual'
                FocusControl = DBEdit12
              end
              object Label16: TLabel
                Left = 635
                Top = 95
                Width = 40
                Height = 13
                Caption = 'Contrato'
                FocusControl = DBEdit13
              end
              object Label34: TLabel
                Left = 372
                Top = 95
                Width = 31
                Height = 13
                Caption = 'ICMS'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBText1: TDBText
                Left = 376
                Top = 112
                Width = 37
                Height = 17
                Alignment = taCenter
                DataField = 'ICMS_ESTADUAL'
                DataSource = DSEstados
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object DBEdit1: TDBEdit
                Left = 8
                Top = 30
                Width = 81
                Height = 21
                Hint = 'C'#243'digo do fornecedor'
                TabStop = False
                CharCase = ecUpperCase
                Color = clBtnFace
                DataField = 'CRED_ID_TEF'
                DataSource = DSCadastro
                PopupMenu = PopupAlteraID
                ReadOnly = True
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 94
                Top = 30
                Width = 358
                Height = 21
                Hint = 'Raz'#227'o social do estabelecimento'
                CharCase = ecUpperCase
                DataField = 'NOME'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 456
                Top = 30
                Width = 272
                Height = 21
                Hint = 'Nome fantasia do estabelecimento'
                CharCase = ecUpperCase
                DataField = 'FANTASIA'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBEdit5: TDBEdit
                Left = 269
                Top = 70
                Width = 124
                Height = 21
                Hint = 'Nome de contato no estabelecimento'
                CharCase = ecUpperCase
                DataField = 'CONTATO'
                DataSource = DSCadastro
                TabOrder = 5
              end
              object DBEdit6: TDBEdit
                Left = 539
                Top = 110
                Width = 92
                Height = 21
                Hint = 
                  'C'#243'digo de acesso do estabelecimento, usado para acessar '#13#10'o M'#243'du' +
                  'lo Web e configura'#231#227'o do BELLA Conv.'
                CharCase = ecUpperCase
                DataField = 'CODACESSO'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                PopupMenu = PopCodAcesso
                ReadOnly = True
                TabOrder = 12
              end
              object DBEdit4: TDBEdit
                Left = 397
                Top = 70
                Width = 278
                Height = 21
                Hint = 'Endere'#231'o'
                CharCase = ecUpperCase
                DataField = 'ENDERECO'
                DataSource = DSCadastro
                TabOrder = 6
              end
              object DBEdit7: TDBEdit
                Left = 8
                Top = 110
                Width = 137
                Height = 21
                Hint = 'Bairro'
                CharCase = ecUpperCase
                DataField = 'BAIRRO'
                DataSource = DSCadastro
                TabOrder = 8
              end
              object DBEdit9: TDBEdit
                Left = 419
                Top = 110
                Width = 89
                Height = 21
                Hint = 'CEP'
                CharCase = ecUpperCase
                DataField = 'CEP'
                DataSource = DSCadastro
                TabOrder = 11
              end
              object DBEdit15: TDBEdit
                Left = 680
                Top = 70
                Width = 48
                Height = 21
                DataField = 'NUMERO'
                DataSource = DSCadastro
                TabOrder = 7
              end
              object DBEdit11: TDBEdit
                Left = 8
                Top = 70
                Width = 129
                Height = 21
                Hint = 'Cadastro nacional de pessoa juridica'
                CharCase = ecUpperCase
                DataField = 'CGC'
                DataSource = DSCadastro
                TabOrder = 3
              end
              object DBEdit12: TDBEdit
                Left = 143
                Top = 70
                Width = 122
                Height = 21
                Hint = 'Inscri'#231#227'o Estadual'
                CharCase = ecUpperCase
                DataField = 'INSCRICAOEST'
                DataSource = DSCadastro
                TabOrder = 4
              end
              object DBEdit13: TDBEdit
                Left = 635
                Top = 110
                Width = 93
                Height = 21
                Hint = 'N'#250'mero de contrato do estabelecimento'
                CharCase = ecUpperCase
                DataField = 'CONTRATO'
                DataSource = DSCadastro
                TabOrder = 13
              end
              object dbLkpEstados: TDBLookupComboBox
                Left = 153
                Top = 110
                Width = 47
                Height = 21
                DataField = 'ESTADO'
                DataSource = DSCadastro
                KeyField = 'UF'
                ListField = 'UF'
                ListSource = DSEstados
                TabOrder = 9
              end
              object dbLkpCidades: TDBLookupComboBox
                Left = 208
                Top = 110
                Width = 157
                Height = 21
                DataField = 'CIDADE'
                DataSource = DSCadastro
                KeyField = 'NOME'
                ListField = 'NOME'
                ListSource = DSCidades
                NullValueKey = 46
                TabOrder = 10
              end
            end
            object GroupBox3: TGroupBox
              Left = 0
              Top = 140
              Width = 988
              Height = 221
              Align = alTop
              Caption = 'Outras Informa'#231#245'es'
              TabOrder = 1
              object DBCheckBox1: TDBCheckBox
                Left = 7
                Top = 16
                Width = 130
                Height = 17
                Hint = 'Estabelecimento liberado para venda'
                Caption = 'Liberado'
                DataField = 'LIBERADO'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 359
              Width = 988
              Height = 62
              Align = alBottom
              Caption = 'Informa'#231#245'es sobre altera'#231#227'o do cadastro'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object Label46: TLabel
                Left = 8
                Top = 15
                Width = 141
                Height = 13
                Caption = 'Data da Altera'#231#227'o / Operador'
                FocusControl = DBEdit10
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label50: TLabel
                Left = 232
                Top = 15
                Width = 138
                Height = 13
                Caption = 'Data do Cadastro / Operador'
                FocusControl = DBEdit16
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object DBEdit10: TDBEdit
                Left = 8
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTALTERACAO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
              end
              object DBEdit16: TDBEdit
                Left = 232
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
              end
              object DBEdit17: TDBEdit
                Left = 342
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
              end
              object DBEdit18: TDBEdit
                Left = 118
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERADOR'
                DataSource = DSCadastro
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGray
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Dados &Adicionais'
            ImageIndex = 1
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 988
              Height = 66
              Align = alTop
              Caption = 'Telefones / Internet'
              TabOrder = 0
              object Label22: TLabel
                Left = 8
                Top = 15
                Width = 51
                Height = 13
                Caption = 'Telefone 1'
                FocusControl = DBEdit19
              end
              object Label23: TLabel
                Left = 103
                Top = 15
                Width = 51
                Height = 13
                Caption = 'Telefone 2'
                FocusControl = DBEdit20
              end
              object Label24: TLabel
                Left = 198
                Top = 15
                Width = 20
                Height = 13
                Caption = 'FAX'
                FocusControl = DBEdit21
              end
              object Label25: TLabel
                Left = 292
                Top = 15
                Width = 29
                Height = 13
                Caption = 'E-Mail'
                FocusControl = dbEdtEmail
              end
              object Label26: TLabel
                Left = 522
                Top = 15
                Width = 56
                Height = 13
                Caption = 'Home Page'
                FocusControl = DBEdit23
              end
              object DBEdit19: TDBEdit
                Left = 8
                Top = 30
                Width = 89
                Height = 21
                Hint = 'Telefone'
                CharCase = ecUpperCase
                DataField = 'TELEFONE1'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object DBEdit20: TDBEdit
                Left = 103
                Top = 30
                Width = 89
                Height = 21
                Hint = 'Telefone'
                CharCase = ecUpperCase
                DataField = 'TELEFONE2'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit21: TDBEdit
                Left = 198
                Top = 30
                Width = 89
                Height = 21
                Hint = 'Fax'
                CharCase = ecUpperCase
                DataField = 'FAX'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object dbEdtEmail: TDBEdit
                Left = 292
                Top = 30
                Width = 225
                Height = 21
                Hint = 'Email'
                CharCase = ecLowerCase
                DataField = 'EMAIL'
                DataSource = DSCadastro
                TabOrder = 3
              end
              object DBEdit23: TDBEdit
                Left = 522
                Top = 30
                Width = 219
                Height = 21
                Hint = 'Site'
                CharCase = ecLowerCase
                DataField = 'HOMEPAGE'
                DataSource = DSCadastro
                TabOrder = 4
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 66
              Width = 988
              Height = 66
              Align = alTop
              Caption = 'Observa'#231#245'es'
              TabOrder = 1
              object Label27: TLabel
                Left = 8
                Top = 15
                Width = 35
                Height = 13
                Caption = 'Linha 1'
                FocusControl = DBEdit24
              end
              object Label28: TLabel
                Left = 386
                Top = 15
                Width = 35
                Height = 13
                Caption = 'Linha 2'
                FocusControl = DBEdit25
              end
              object DBEdit24: TDBEdit
                Left = 8
                Top = 30
                Width = 369
                Height = 21
                Hint = 'Observa'#231#227'o 1'
                CharCase = ecUpperCase
                DataField = 'OBS1'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object DBEdit25: TDBEdit
                Left = 382
                Top = 30
                Width = 358
                Height = 21
                Hint = 'Observa'#231#227'o 2'
                CharCase = ecUpperCase
                DataField = 'OBS2'
                DataSource = DSCadastro
                TabOrder = 1
              end
            end
            object GroupBox7: TGroupBox
              Left = 0
              Top = 132
              Width = 988
              Height = 59
              Align = alTop
              Caption = 'Informa'#231#245'es para Cr'#233'dito'
              TabOrder = 2
              object Label29: TLabel
                Left = 8
                Top = 15
                Width = 31
                Height = 13
                Caption = 'Banco'
              end
              object Label30: TLabel
                Left = 191
                Top = 15
                Width = 39
                Height = 13
                Caption = 'Ag'#234'ncia'
                FocusControl = DBEdit26
              end
              object Label31: TLabel
                Left = 266
                Top = 15
                Width = 71
                Height = 13
                Caption = 'Conta Corrente'
                FocusControl = DBEdit27
              end
              object Label32: TLabel
                Left = 406
                Top = 15
                Width = 50
                Height = 13
                Caption = 'Correntista'
                FocusControl = DBEdit28
              end
              object DBEdit26: TDBEdit
                Left = 191
                Top = 30
                Width = 69
                Height = 21
                Hint = 'Ag'#234'ncia'
                CharCase = ecUpperCase
                DataField = 'AGENCIA'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit27: TDBEdit
                Left = 266
                Top = 30
                Width = 134
                Height = 21
                Hint = 'Conta corrente'
                CharCase = ecUpperCase
                DataField = 'CONTACORRENTE'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBEdit28: TDBEdit
                Left = 406
                Top = 30
                Width = 333
                Height = 21
                Hint = 'Correntista'
                CharCase = ecUpperCase
                DataField = 'CORRENTISTA'
                DataSource = DSCadastro
                TabOrder = 3
              end
              object JvDBLookupCombo2: TJvDBLookupCombo
                Left = 8
                Top = 30
                Width = 179
                Height = 21
                Hint = 'Banco do estabelecimento'
                DataField = 'BANCO'
                DataSource = DSCadastro
                LookupField = 'CODIGO'
                LookupDisplay = 'BANCO'
                LookupSource = DSBanco
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    object tbCadPos: TTabSheet [2]
      Caption = 'Cadastro de POS'
      ImageIndex = 9
      OnHide = tbCadPosHide
      OnShow = tbCadPosShow
      object Panel24: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 25
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Label35: TLabel
          Left = 8
          Top = 4
          Width = 252
          Height = 13
          Caption = 'POS cadastrados para este estabelecimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object dbGridPos: TJvDBGrid
        Left = 0
        Top = 25
        Width = 1000
        Height = 432
        Align = alClient
        DataSource = DSPos
        DefaultDrawing = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = dbGridPosColExit
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = dbGridPosTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'POS_SERIAL_NUMBER'
            Title.Caption = 'Serial Number'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CRED_ID'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'MODELO_POS'
            PickList.Strings = (
              'GETNET'
              'VERIFONE')
            Title.Caption = 'Modelo do POS'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAPTURA'
            PickList.Strings = (
              'NAVS'
              'SOFTNEX'
              'TEF')
            Title.Caption = 'Meio de Captura'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ATU_SERVER_IP'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Atualiza POS'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VERSAO_NAVS'
            Title.Caption = 'Vers'#227'o NAVS'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VERSAO_OS'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Vers'#227'o OS Atualizada'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VERSAO_EOS'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Vers'#227'o EOS Atualizada'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ATUALIZOU_LUA'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Arquivo LUA Atualizado'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IP_TEF'
            Title.Caption = 'IP TEF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IP_TEF_GERENCIAL'
            Title.Caption = 'IP TEF Gerencial'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PORTA_TEF'
            Title.Caption = 'N'#176' Porta TEF'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CODLOJA'
            Title.Caption = 'Cod. Loja'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_TEMINAL'
            Title.Caption = 'ID Terminal'
            Width = 70
            Visible = True
          end>
      end
      object Panel25: TPanel
        Left = 0
        Top = 457
        Width = 1000
        Height = 31
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        object btnGravarPos: TBitBtn
          Left = 65
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Gravar'
          TabOrder = 0
          OnClick = btnGravarPosClick
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object btnCancelarPos: TBitBtn
          Left = 145
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnCancelarPosClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object btnExcluirPos: TBitBtn
          Left = 225
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Excluir'
          TabOrder = 2
          OnClick = btnExcluirPosClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000CED5D8CCD3D6
            CCD3D6CBD2D6D8DFE4CDD4D693988F93988F93988F93988F9A9F98DAE1E5D2D9
            DDCBD2D6CCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CBD2D6D2D8DDCDD4D7ADB2B5AE
            B3B6AEB3B6ADB2B5B1B6B9D3DADECFD6DACBD2D6CCD3D6CDD4D7CCD3D6CAD1D5
            CBD2D5D9E1DF868C84434747000044000041000041000045000044565955A2A8
            A6D9E0E4CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5D0D7DCA9ADB08A8C8D7B7B7B7C
            7B7B7C7B7B7C7C7B7E7E7E8E9092B5BABDD1D8DDCAD1D5CBD2D5CCD3D7CBD2D5
            DBE4E180828D0500710000B30600D70700D60400D30000CE0000A70000700F0F
            43A8AEA6D9E0E4C9D0D4CAD1D5CAD1D5D1D8DCAAAFB2898A899C9C9BAAAAAAA9
            A9A9A9A9A9A7A7A79C9C9C888787808080B6BBBED1D8DDCAD1D5CCD3D7D2D9D7
            7D7EB90003B31B0EFA3100FF2E00FF3100FF2800FB1200E20000D40000C70000
            90121242A2A8A6D0D8DCCAD1D5CCD3D7B6BABD9F9F9FB6B6B5B9B9B9B6B6B6B7
            B7B7B5B5B5ADADADA8A8A8A5A5A5949494808080B4BBBDCED5D9CCD3D7D1D6D4
            1B20A40049F61F28FF3B0BFF3103FF2900FF2B00FF3000FF2804F31C0AE60000
            C800006D4D504CD9E1E5CAD1D5CBD3D7A0A1A1BAB9B9BCBCBCB8B8B8B7B7B7B4
            B4B4B5B5B5B6B6B6B4B4B4B1B1B1A5A5A58989878C8F90D2DADEDAE1DC6666AF
            3146E0194EFF2D03FFBDABFFF4F2FF7F63FF1D03FFA692FFF5F0FF8777F40300
            D40000A8080944969B94D9E1E5AFB3B5B5B5B4C2C2C2B4B4B4E6E6E6FBFBFBD4
            D4D4AFAFAFDFDFDFFCFCFCD7D7D7A5A5A59C9C9C7F7E7EAFB4B6DCE2DC5657AA
            3A4EE73560FF2A00FF3917FFD2C8FFFFFFFFB8A7FFFFFFFFAC9AFF2C00FF1100
            E30000CF0000428D9289DBE3E7ABAEB0B8B8B7C9C9C9B2B2B2B9B9B9EEEEEEFF
            FFFFE6E6E6FFFFFFE4E4E4B5B5B5ADADADA7A7A77D7D7BAAAFB1DCE2DC595AAB
            394DE53263FF2F03FF2200FF3411FFD8CFFFFFFFFFB4A4FF1D00FF2B00FF2900
            F80500D40000418F948BDBE3E7ACAFB1B8B8B7C9C9C9B5B5B5B2B2B2B6B6B6F0
            F0F0FFFFFFE6E6E6B0B0B0B4B4B4B5B5B5A9A9A97D7C7CABB0B2DCE2DC595AAB
            3C48E81E88FF1814FF2900FFA592FFFEFDFFDCD2FFFDFCFF7E63FF1F00FF3200
            FF0600D70000418F948BDBE3E7ACAFB1B8B7B7CDCDCDB7B7B7B2B2B2DDDDDDFF
            FFFFF3F3F3FEFEFED3D3D3B0B0B0B8B8B8AAAAAA7D7C7CABB0B2DDE3DC5555AA
            3B46D42A93FF0F45FFAE9AFFFFFFFFA895FF331AFFCFC3FFFFFFFF8164FF2600
            FF0600D90000408C9188DCE4E8ABAEB0B3B2B1D0D0D0BEBEBEE1E1E1FFFFFFE2
            E2E2B6B6B6EDEDEDFFFFFFD4D4D4B4B4B4ABABAB7C7B7BA9AEB0CFD6D8B9BFCD
            2C28A8497AFC0E83FF3579FF5632FF3400FF2300FF3C0FFF5731FF532CFF2F00
            FF0000AB3C4047CCD4D5CDD4D8C5CCD0A2A3A4C8C8C8CACACACDCDCDC4C4C4B6
            B6B6B1B1B1BABABAC4C4C4C3C3C3B8B8B89C9B9B888A8BCBD3D6CCD2D7D5DCD8
            5D5AAB3544CF3896FF0073FF0034FF1B1BFF3200FF2C00FF2500FF2B00FF2300
            FC000256858B83D7DFE3C9D0D4CDD5D9ACAFB1B1B0B0D3D3D3C6C6C6BABABAB7
            B7B7B6B6B6B4B4B4B3B3B3B4B4B4B6B6B6848383A6AAACD1D9DDCCD3D7CCD3D6
            D1D9D55A58B53545CC4474FC2089FF1E87FF144BFF2521FF3400FF1D00FB0000
            7A7E847ED9E1E3C9D0D4CAD1D5CAD2D6CCD3D7AEB0B2B2B1B1C8C7C7CDCDCDCD
            CDCDC1C1C1BBBBBBB7B7B7B4B4B38E8D8DA3A7AAD1D8DCCAD1D5CCD3D7CAD1D5
            CFD6D7D1D9D55C5AAC2D2AA83F4AD33C46E53146E5373EE40A03C82727A47C82
            A8DCE3DCCBD2D6CAD1D5CAD1D5CAD1D5CBD2D6CCD3D8ACAFB1A2A2A3B4B3B3B9
            B9B8B7B7B6B6B6B5A6A5A59F9FA0AFB4B7CFD6DBCBD2D6CBD2D6CCD2D6CAD1D5
            CAD1D5CCD3D6D5DCD8B9BFCD595AAA5A5AAB5B5BAB5A5DAA656AB0D0D8D4D1D8
            D9CBD2D6CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CBD2D6CDD4D9C5CCD0AAAEAFAB
            AEB0ABAEB0ABAEB0AEB2B4CAD2D6CDD4D8CBD2D6CAD1D5CAD1D5CFD5D9CCD3D7
            CCD3D7CCD3D7CCD2D7CFD6D8DCE2DDDCE2DCDCE2DCDCE2DCDAE1DCCCD3D7CCD3
            D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CDD4D8D0D8DCD0
            D8DCD0D8DCD0D8DCD0D7DCCCD3D7CCD3D7CCD3D7CCD3D7CED4D8}
          NumGlyphs = 2
        end
        object btnInserirPos: TBitBtn
          Left = 2
          Top = 3
          Width = 63
          Height = 25
          Caption = '&Inserir'
          TabOrder = 3
          OnClick = btnInserirPosClick
          Glyph.Data = {
            5A030000424D5A030000000000005A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004900000000000000117611001379
            1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
            37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
            4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
            6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
            7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
            840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
            C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
            D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
            DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
            4848484848484848484848484848484848484848484848484848484848484848
            484848480C00000C484848484848484848484848302323304848484848484848
            4848484801161601484848484848484848484848243A3A244848484848484848
            4848484802181802484848484848484848484848253C3C254848484848484848
            48484848031A1A03484848484848484848484848263D3D264848484848484848
            48484848041B1B04484848484848484848484848273F3F274848484848484813
            0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
            1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
            2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
            0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
            484848480D21210D484848484848484848484848324444324848484848484848
            4848484810282810484848484848484848484848354545354848484848484848
            4848484812292912484848484848484848484848374646374848484848484848
            48484848152A2A15484848484848484848484848394747394848484848484848
            484848481E16161E484848484848484848484848423A3A424848484848484848
            484848484848484848484848484848484848484848484848484848484848}
          NumGlyphs = 2
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 1000
      end
      inherited GridHistorico: TJvDBGrid
        Width = 1000
        Height = 441
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 1008
  end
  inherited panStatus2: TPanel
    Width = 1008
  end
  inherited DSCadastro: TDataSource
    Left = 12
    Top = 520
  end
  inherited PopupBut: TPopupMenu
    Left = 584
    Top = 417
  end
  inherited PopupGrid1: TPopupMenu
    Left = 524
    Top = 417
  end
  inherited QHistorico: TADOQuery
    SQL.Strings = (
      'Select * from logs')
    Left = 52
    Top = 489
  end
  object DSBanco: TDataSource [7]
    DataSet = QBanco
    OnStateChange = DSCadastroStateChange
    Left = 328
    Top = 521
  end
  object PopupCC: TPopupMenu [8]
    Left = 453
    Top = 464
    object MenuItem1: TMenuItem
      Caption = 'Configura'#231#227'o da Grade'
    end
    object Exportarparaoexcel2: TMenuItem
      Caption = 'Exportar para o excel'
    end
  end
  object PopupDesconto: TPopupMenu [9]
    Left = 380
    Top = 464
    object MenuItem2: TMenuItem
      Caption = 'Configura'#231#227'o da Grade'
    end
    object MenuItem3: TMenuItem
      Caption = 'Exportar para o excel'
    end
  end
  object PopCodAcesso: TPopupMenu [10]
    OnPopup = PopCodAcessoPopup
    Left = 524
    Top = 465
    object ColocarCodAcessoManual1: TMenuItem
      Caption = 'Alterar C'#243'd Acesso'
      OnClick = ColocarCodAcessoManual1Click
    end
  end
  object PopupAlteraID: TPopupMenu [11]
    OnPopup = PopupAlteraIDPopup
    Left = 384
    Top = 417
    object AlterarIddoFornecedor1: TMenuItem
      Caption = 'Alterar Id do Estabelecimento'
      OnClick = AlterarIddoFornecedor1Click
    end
    object DuplicarEstabelecimentoalterandoID1: TMenuItem
      Caption = 'Duplicar Estabelecimento alterando ID'
      Visible = False
    end
  end
  object PopupComissaoEmpr: TPopupMenu [12]
    Left = 452
    Top = 417
    object AltLineardeComissoporEmpresa1: TMenuItem
      Caption = 'Alt. Linear de Comiss'#227'o por Empresa'
      Visible = False
      OnClick = AltLineardeComissoporEmpresa1Click
    end
  end
  object PopupMenu1: TPopupMenu [13]
    Left = 588
    Top = 465
    object este1: TMenuItem
      Caption = 'Alt. Linear de Comiss'#227'o por Empresa'
    end
  end
  object tExcel: TADOTable [14]
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 384
    Top = 384
  end
  object DSEstados: TDataSource [15]
    DataSet = QEstados
    OnDataChange = DSEstadosDataChange
    Left = 425
    Top = 525
  end
  object DSPos: TDataSource [16]
    DataSet = QPos
    OnStateChange = DSPosStateChange
    Left = 588
    Top = 528
  end
  object DSCidades: TDataSource [17]
    DataSet = QCidades
    Left = 458
    Top = 526
  end
  object QBanco: TADOQuery [18]
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from bancos where apagado <>'#39'S'#39' order by banco')
    Left = 327
    Top = 491
    object QBancoCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QBancoBANCO: TStringField
      FieldName = 'BANCO'
      Size = 45
    end
    object QBancoAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QBancoLAYOUT: TMemoField
      FieldName = 'LAYOUT'
      BlobType = ftMemo
    end
    object QBancoDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QBancoDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QBancoOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QBancoDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QBancoOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
  end
  object QCredenciados: TADOQuery [19]
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from credenciados where cred_id = 0')
    Left = 396
    Top = 491
    object QCredenciadosCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCredenciadosNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QCredenciadosSEG_ID: TIntegerField
      FieldName = 'SEG_ID'
    end
    object QCredenciadosLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCODACESSO: TIntegerField
      FieldName = 'CODACESSO'
    end
    object QCredenciadosCOMISSAO: TBCDField
      FieldName = 'COMISSAO'
      Precision = 6
      Size = 2
    end
    object QCredenciadosBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QCredenciadosDIAFECHAMENTO1: TWordField
      FieldName = 'DIAFECHAMENTO1'
    end
    object QCredenciadosDIAFECHAMENTO2: TWordField
      FieldName = 'DIAFECHAMENTO2'
    end
    object QCredenciadosVENCIMENTO1: TWordField
      FieldName = 'VENCIMENTO1'
    end
    object QCredenciadosVENCIMENTO2: TWordField
      FieldName = 'VENCIMENTO2'
    end
    object QCredenciadosAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosPAGA_CRED_POR_ID: TIntegerField
      FieldName = 'PAGA_CRED_POR_ID'
    end
    object QCredenciadosCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QCredenciadosCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object QCredenciadosINSCRICAOEST: TStringField
      FieldName = 'INSCRICAOEST'
      Size = 18
    end
    object QCredenciadosFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 58
    end
    object QCredenciadosTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QCredenciadosTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QCredenciadosFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QCredenciadosCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 58
    end
    object QCredenciadosENVIAR_EMAIL: TStringField
      FieldName = 'ENVIAR_EMAIL'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 40
    end
    object QCredenciadosHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 40
    end
    object QCredenciadosPOSSUICOMPUTADOR: TStringField
      FieldName = 'POSSUICOMPUTADOR'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCredenciadosNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCredenciadosBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QCredenciadosCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 30
    end
    object QCredenciadosCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCredenciadosESTADO: TStringField
      FieldName = 'ESTADO'
      FixedChar = True
      Size = 2
    end
    object QCredenciadosMAQUINETA: TStringField
      FieldName = 'MAQUINETA'
      Size = 14
    end
    object QCredenciadosTIPOFECHAMENTO: TStringField
      FieldName = 'TIPOFECHAMENTO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosNOVOCODACESSO: TIntegerField
      FieldName = 'NOVOCODACESSO'
    end
    object QCredenciadosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCredenciadosAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QCredenciadosCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 15
    end
    object QCredenciadosOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QCredenciadosOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QCredenciadosCORRENTISTA: TStringField
      FieldName = 'CORRENTISTA'
      Size = 40
    end
    object QCredenciadosCARTIMPRESSO: TStringField
      FieldName = 'CARTIMPRESSO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosFORNCESTA: TStringField
      FieldName = 'FORNCESTA'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCredenciadosACEITA_PARC: TStringField
      FieldName = 'ACEITA_PARC'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosGERA_LANC_CPMF: TStringField
      FieldName = 'GERA_LANC_CPMF'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosENCONTRO_CONTAS: TStringField
      FieldName = 'ENCONTRO_CONTAS'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCredenciadosDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCredenciadosOPERADOR: TStringField
      FieldName = 'OPERADOR'
    end
    object QCredenciadosDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCredenciadosOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCredenciadosVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object QCredenciadosDIA_PAGTO: TIntegerField
      FieldName = 'DIA_PAGTO'
    end
    object QCredenciadosTX_DVV: TBCDField
      FieldName = 'TX_DVV'
      Precision = 15
      Size = 2
    end
    object QCredenciadosUTILIZA_AUTORIZADOR: TStringField
      FieldName = 'UTILIZA_AUTORIZADOR'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosUTILIZA_COMANDA: TStringField
      FieldName = 'UTILIZA_COMANDA'
      FixedChar = True
      Size = 1
    end
    object QCredenciadosUTILIZA_RECARGA: TStringField
      FieldName = 'UTILIZA_RECARGA'
      FixedChar = True
      Size = 1
    end
  end
  object QEstados: TADOQuery [20]
    Connection = DMConexao.AdoCon
    AfterScroll = QEstadosAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT ESTADO_ID, UF, ICMS_ESTADUAL FROM ESTADOS'
      'ORDER BY UF')
    Left = 428
    Top = 491
    object QEstadosESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object QEstadosUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object QEstadosICMS_ESTADUAL: TFloatField
      FieldName = 'ICMS_ESTADUAL'
    end
  end
  object QCidades: TADOQuery [21]
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CID_ID, ESTADO_ID, UPPER(NOME collate sql_latin1_general_' +
        'cp1251_cs_as) as NOME FROM CIDADES'
      'ORDER BY NOME'
      '')
    Left = 460
    Top = 491
    object QCidadesCID_ID: TIntegerField
      FieldName = 'CID_ID'
    end
    object QCidadesESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object QCidadesNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 60
    end
  end
  object QPos: TADOQuery [22]
    Connection = DMConexao.AdoCon
    BeforePost = QPosBeforePost
    Parameters = <
      item
        Name = 'cred_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select * from pos where cred_id =:cred_id and pos_plantao_card =' +
        ' '#39'N'#39)
    Left = 588
    Top = 491
    object QPosPOS_SERIAL_NUMBER: TStringField
      FieldName = 'POS_SERIAL_NUMBER'
    end
    object QPosCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QPosUSUARIO_RECARGA: TStringField
      FieldName = 'USUARIO_RECARGA'
    end
    object QPosTIPO_RECARGA: TStringField
      FieldName = 'TIPO_RECARGA'
      FixedChar = True
      Size = 1
    end
    object QPosATU_SERVER_IP: TStringField
      FieldName = 'ATU_SERVER_IP'
      FixedChar = True
      Size = 1
    end
    object QPosMODELO_POS: TStringField
      FieldName = 'MODELO_POS'
      Size = 10
    end
    object QPosCAPTURA: TStringField
      FieldName = 'CAPTURA'
      Size = 10
    end
    object QPosVERSAO_NAVS: TStringField
      FieldName = 'VERSAO_NAVS'
      Size = 10
    end
    object QPosVERSAO_OS: TStringField
      FieldName = 'VERSAO_OS'
      FixedChar = True
      Size = 1
    end
    object QPosVERSAO_EOS: TStringField
      FieldName = 'VERSAO_EOS'
      FixedChar = True
      Size = 1
    end
    object QPosATUALIZOU_LUA: TStringField
      FieldName = 'ATUALIZOU_LUA'
      FixedChar = True
      Size = 1
    end
    object QPosIP_TEF: TStringField
      FieldName = 'IP_TEF'
      Size = 15
    end
    object QPosIP_TEF_GERENCIAL: TStringField
      FieldName = 'IP_TEF_GERENCIAL'
      Size = 15
    end
    object QPosPORTA_TEF: TStringField
      FieldName = 'PORTA_TEF'
      Size = 4
    end
    object QPosCODLOJA: TStringField
      FieldName = 'CODLOJA'
      Size = 8
    end
    object QPosID_TEMINAL: TStringField
      FieldName = 'ID_TEMINAL'
      Size = 8
    end
    object QPosPOS_PLANTAO_CARD: TStringField
      FieldName = 'POS_PLANTAO_CARD'
      Size = 1
    end
  end
  inherited DSHistorico: TDataSource
    Left = 49
    Top = 520
  end
  inherited QCadastro: TADOQuery
    AfterClose = QCadastroAfterClose
    AfterInsert = QCadastroAfterInsert
    AfterPost = QCadastroAfterPost
    SQL.Strings = (
      'select * from credenciados_tef WHERE cred_id_tef = 0')
    Left = 20
    Top = 489
    object QCadastroCRED_ID_TEF: TIntegerField
      FieldName = 'CRED_ID_TEF'
    end
    object QCadastroNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroCODACESSO: TIntegerField
      FieldName = 'CODACESSO'
    end
    object QCadastroBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QCadastroCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object QCadastroINSCRICAOEST: TStringField
      FieldName = 'INSCRICAOEST'
      Size = 18
    end
    object QCadastroFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 58
    end
    object QCadastroTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QCadastroTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QCadastroFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QCadastroCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 58
    end
    object QCadastroENVIAR_EMAIL: TStringField
      FieldName = 'ENVIAR_EMAIL'
      FixedChar = True
      Size = 1
    end
    object QCadastroEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 40
    end
    object QCadastroHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 40
    end
    object QCadastroENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCadastroNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCadastroBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QCadastroCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 30
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCadastroESTADO: TStringField
      FieldName = 'ESTADO'
      FixedChar = True
      Size = 2
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object QCadastroCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 15
    end
    object QCadastroOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QCadastroOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QCadastroCORRENTISTA: TStringField
      FieldName = 'CORRENTISTA'
      Size = 40
    end
    object QCadastroCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
  end
end
