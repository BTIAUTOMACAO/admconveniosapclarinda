unit UFechamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, Mask, JvToolEdit, ComCtrls, Grids, DBGrids, {JvDBCtrl,} DB, dateutils, uClassLog, {ClipBrd,} JvExDBGrids, JvDBGrid, JvExMask, ADODB, JvMemoryDataset, JvDialogs;

type
  THackDBgrid = class(TDBGrid);

  TFFechamento = class(TF1)
    Panel1: TPanel;
    Label2: TLabel;
    ButAbrir: TButton;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    grdFech: TJvDBGrid;
    DSEmpresa: TDataSource;
    ButFechar: TButton;
    Panel17: TPanel;
    Label63: TLabel;
    DataIni: TJvDateEdit;
    RGAutor: TRadioGroup;
    ChkMostrarAbertos: TCheckBox;
    DataFin: TJvDateEdit;
    Label1: TLabel;
    btnMarDes: TButton;
    btnMarTod: TButton;
    btnDesTod: TButton;
    qContaC: TADOQuery;
    qContaCAUTORIZACAO_ID: TIntegerField;
    qContaCCARTAO_ID: TIntegerField;
    qContaCCONV_ID: TIntegerField;
    qContaCCRED_ID: TIntegerField;
    qContaCDIGITO: TWordField;
    qContaCDATA: TDateTimeField;
    qContaCHORA: TStringField;
    qContaCDATAVENDA: TDateTimeField;
    qContaCDEBITO: TBCDField;
    qContaCCREDITO: TBCDField;
    qContaCVALOR_CANCELADO: TBCDField;
    qContaCBAIXA_CONVENIADO: TStringField;
    qContaCBAIXA_CREDENCIADO: TStringField;
    qContaCENTREG_NF: TStringField;
    qContaCRECEITA: TStringField;
    qContaCCESTA: TStringField;
    qContaCCANCELADA: TStringField;
    qContaCDIGI_MANUAL: TStringField;
    qContaCTRANS_ID: TIntegerField;
    qContaCFORMAPAGTO_ID: TIntegerField;
    qContaCFATURA_ID: TIntegerField;
    qContaCPAGAMENTO_CRED_ID: TIntegerField;
    qContaCAUTORIZACAO_ID_CANC: TIntegerField;
    qContaCOPERADOR: TStringField;
    qContaCDATA_VENC_EMP: TDateTimeField;
    qContaCDATA_FECHA_EMP: TDateTimeField;
    qContaCDATA_VENC_FOR: TDateTimeField;
    qContaCDATA_FECHA_FOR: TDateTimeField;
    qContaCHISTORICO: TStringField;
    qContaCNF: TIntegerField;
    qContaCDATA_ALTERACAO: TDateTimeField;
    qContaCDATA_BAIXA_CONV: TDateTimeField;
    qContaCDATA_BAIXA_CRED: TDateTimeField;
    qContaCOPER_BAIXA_CONV: TStringField;
    qContaCOPER_BAIXA_CRED: TStringField;
    qContaCDATA_CONFIRMACAO: TDateTimeField;
    qContaCOPER_CONFIRMACAO: TStringField;
    qContaCCONFERIDO: TStringField;
    qContaCNSU: TIntegerField;
    qContaCPREVIAMENTE_CANCELADA: TStringField;
    qContaCEMPRES_ID: TIntegerField;
    QEmpresa: TADOQuery;
    QEmpresaempres_id: TIntegerField;
    QEmpresanome: TStringField;
    QEmpresadata_fecha: TDateTimeField;
    QEmpresadata_venc: TDateTimeField;
    QEmpresasaldo: TFloatField;
    QEmpresafatura_id: TIntegerField;
    QDescEmp: TADOQuery;
    QDescEmpcred_id: TIntegerField;
    QDescEmpdata_fecha_emp: TDateTimeField;
    QDescEmpvalor: TBCDField;
    QDescEmpdesconto_emp: TBCDField;
    QDescEmpdesc_valor: TBCDField;
    MEmpresa: TJvMemoryData;
    MEmpresaNOME: TStringField;
    MEmpresaSALDO: TFloatField;
    MEmpresaFATURA_ID: TIntegerField;
    MEmpresaOBS: TStringField;
    MEmpresaMARCADO: TBooleanField;
    MEmpresaDATA_FECHA: TDateTimeField;
    MEmpresaDATA_VENC: TDateTimeField;
    MEmpresaEMPRES_ID: TIntegerField;
    MEmpresaFATURADA: TStringField;
    dlgAbrePDF: TOpenDialog;
    qBoletos: TADOQuery;
    qBoletosBOLETO_ID: TAutoIncField;
    qBoletosCAMINHO_ARQUIVO: TStringField;
    qBoletosDATA_VENCIMENTO: TDateTimeField;
    QBoletosFATURA_ID1: TIntegerField;
    qBoletosDATA_ALTERACAO: TDateTimeField;
    qBoletosOPERADOR: TStringField;
    procedure ButAbrirClick(Sender: TObject);
    procedure ButFecharClick(Sender: TObject);
    procedure QEmpresaAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure RGAutorClick(Sender: TObject);
    procedure btnMarDesClick(Sender: TObject);
    procedure btnMarTodClick(Sender: TObject);
    procedure btnDesTodClick(Sender: TObject);
    procedure grdFechDblClick(Sender: TObject);
    procedure grdFechKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure grdFechDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    AutConf: Boolean;
    LogCC: TLog;
    emp_sel: string;
    procedure GeraBonus(fatura_id: integer);
    procedure GeraDescontoEmpresa(Emp: Integer; DataFecha: TDateTime; Fat: integer; Nome: string);
    function ValorAlterado(empres_id: Integer; Fecha: TDateTime; Valor: Currency): Boolean;
    procedure MarcaDesm(Dataset: TDataSet);
    procedure MarcaTodos(DataSet: TDataSet);
    procedure DesmTodos(DataSet: TDataSet);
    procedure EmpSel;
  public
    { Public declarations }
  end;

var
  FFechamento: TFFechamento;

implementation

uses
  DM, UTelaAltLinear, UMenu, cartao_util;

{$R *.dfm}

procedure TFFechamento.ButAbrirClick(Sender: TObject);
begin
  inherited;
  QEmpresa.Close;
  if ((DataIni.Date > Today) or (DataFin.Date > Today)) then
  begin
    MsgInf('N�o � possivel faturar uma empresa com fechamento maior que a data de hoje!');
    DataIni.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  QEmpresa.CommandTimeout := 5000;
  QEmpresa.SQL.Clear;
  QEmpresa.SQL.Add(' Select DISTINCT(conv.empres_id), emp.nome as nome, cc.data_fecha_emp data_fecha, ');
  QEmpresa.SQL.Add(' (select top(1) data_venc from DIA_FECHA where empres_id = conv.EMPRES_ID and DATA_VENC >= ' + FormatDataIB(DataIni.Date) + ') data_venc, ');
  QEmpresa.SQL.Add(' sum(cc.debito-cc.credito) saldo, cc.fatura_id ');
  QEmpresa.SQL.Add(' from contacorrente cc join conveniados conv on conv.conv_id = cc.conv_id ');
  QEmpresa.SQL.Add(' join empresas emp on conv.empres_id = emp.empres_id ');
  QEmpresa.SQL.Add(' INNER JOIN dia_fecha on dia_fecha.empres_id = emp.empres_id and dia_fecha.DATA_VENC >= ' + FormatDataIB(DataIni.Date));
  QEmpresa.SQL.Add(' where coalesce(cc.baixa_conveniado,''N'') <> ''S'' ');
  QEmpresa.SQL.Add(' and coalesce(cc.fatura_id,0) = 0 ');
  if RGAutor.ItemIndex = 0 then
    QEmpresa.SQL.Add(' and coalesce(cc.entreg_nf,''N'') = ''S'' ');
  if ChkMostrarAbertos.Checked then
    QEmpresa.SQL.Add(' and cc.data_fecha_emp <= ' + FormatDataIB(DataFin.Date))
  else
    QEmpresa.SQL.Add(' and cc.data_fecha_emp between ' + FormatDataIB(DataIni.Date) + ' and ' + FormatDataIB(DataFin.Date));
  QEmpresa.SQL.Add(' GROUP BY cc.data_fecha_emp,conv.empres_id,cc.fatura_id,emp.nome,data_venc, emp.obs_fechamento ');
  QEmpresa.SQL.Add(' union all select fatura.id as empres_id, empresas.nome, fatura.fechamento data_fecha, ');
  QEmpresa.SQL.Add(' (select top(1) data_venc from DIA_FECHA where empres_id = empresas.EMPRES_ID and DATA_VENC >= ' + FormatDataIB(DataIni.Date) + ') data_venc, ');
  QEmpresa.SQL.Add(' valor as saldo, fatura.fatura_id ');
  QEmpresa.SQL.Add(' from fatura join empresas on empresas.empres_id = fatura.id ');
  QEmpresa.SQL.Add(' and coalesce(fatura.tipo,''E'')=''E'' where fatura.apagado <> ''S'' ');
  QEmpresa.SQL.Add(' and coalesce(fatura.so_confirmadas,''N'') = ''S'' ');
  QEmpresa.SQL.Add(' and fatura.fechamento between ' + FormatDataIB(DataIni.Date) + ' and ' + FormatDataIB(DataFin.Date));
  QEmpresa.SQL.Add(' order by 3,2 ');
  QEmpresa.SQL.Text;
  QEmpresa.Open;
  Screen.Cursor := crDefault;
  if QEmpresa.IsEmpty then
  begin
    MsgInf('Nenhuma empresa encontrado com fechamento para estas datas.');
    MEmpresa.EmptyTable;
    Exit;
  end
  else
  begin
    QEmpresa.First;

    MEmpresa.Open;
    MEmpresa.EmptyTable;
    MEmpresa.DisableControls;
    while not QEmpresa.Eof do
    begin
      MEmpresa.Append;
      MEmpresaEMPRES_ID.AsInteger := QEmpresaEMPRES_ID.AsInteger;
      MEmpresaNOME.AsString := QEmpresaNOME.AsString;
      MEmpresaDATA_FECHA.AsDateTime := QEmpresaDATA_FECHA.AsDateTime;
      MEmpresaDATA_VENC.AsDateTime := QEmpresaDATA_VENC.AsDateTime;
      MEmpresaSALDO.AsFloat := QEmpresaSALDO.AsFloat;
      MEmpresaFATURA_ID.AsInteger := QEmpresaFATURA_ID.AsInteger;
      MEmpresaMARCADO.AsBoolean := False;
      MEmpresaFATURADA.AsString := 'N';
      MEmpresa.Post;
      QEmpresa.Next;
    end;
    MEmpresa.First;
    MEmpresa.EnableControls;
    emp_sel := EmptyStr;

    grdFech.SetFocus;
    ButFechar.Enabled := true;
  end;

end;

function TFFechamento.ValorAlterado(empres_id: Integer; Fecha: TDateTime; Valor: Currency): Boolean;
var
  strSql: string;
  newValue: Currency;
begin
  strSql := 'Select sum(cc.debito-cc.credito) saldo from contacorrente cc join conveniados conv on conv.conv_id = cc.conv_id ' + ' where coalesce(cc.baixa_conveniado,''N'') <> ''S'' and coalesce(cc.fatura_id,0) = 0 ';
  if RGAutor.ItemIndex = 0 then
  begin
    strSql := strSql + ' and coalesce(cc.entreg_nf,''N'') = ''S'' ';
  end;
  strSql := strSql + ' and cc.data_fecha_emp = ' + FormatDataIB(Fecha);
  strSql := strSql + ' and conv.empres_id = ' + IntToStr(empres_id);
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(strSql);
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.IsEmpty then
    newValue := 0
  else
    newValue := DMConexao.AdoQry.Fields[0].AsCurrency;

  if newValue = Valor then
    Result := False
  else
    Result := True;
end;

procedure TFFechamento.ButFecharClick(Sender: TObject);
var
  Fat_id, c: Integer;
  desc_emp: Currency;
  nomeEmp: string;
  transp_autor: Boolean;
  arquivoPDF: string;
  caminhoAno, caminhoMes, caminhoCompleto, caminhoRelativo : string;
  dia, mes, ano : Word;
begin
  inherited;
  if DMConexao.ContaMarcados(MEmpresa) > 0 then
  begin
    if MsgSimNao('Aten��o!' + sLineBreak + 'Efetuando o fechamento, as autoriza��es deste per�odo N�O poder�o' + sLineBreak + 'ser alteradas e ser� gerada uma fatura para cada empresa marcada.' + sLineBreak + 'Confirma a Opera��o?') then
    begin
      MEmpresa.First;
      transp_autor := False;
      if (RGAutor.ItemIndex = 0) and MsgSimNao('Deseja que o sistema mude o fechamento das autoriza��es nao confirmadas' + sLineBreak + 'para o pr�ximo fechamento n�o faturado da empresa? ') then
      begin
        transp_autor := True;
      end;

      while not MEmpresa.Eof do
      begin
        if MEmpresaMARCADO.AsBoolean = True then
        begin
          if ValorAlterado(MEmpresaEMPRES_ID.AsInteger, MEmpresaDATA_FECHA.AsVariant, MEmpresaSALDO.AsCurrency) then
          begin
            MsgErro('O valor do fechamento foi alterado' + sLineBreak + 'Provavelmente foi alterada a situa��o de alguma autoriza��o e essa consulta n�o est� atualizada' + sLineBreak + 'Por favor, clique no bot�o "Abrir" novamente.');
            ButAbrir.SetFocus;
            Exit;
          end;
          DMConexao.AdoCon.BeginTrans; //Inicia transa��o explicitamente.
          try
            Screen.Cursor := crHourGlass;
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SFATURA_ID');
            DMConexao.AdoQry.Open;
            Fat_id := DMConexao.AdoQry.Fields[0].AsInteger;
            DMConexao.ExecuteSql('insert into fatura(FATURA_ID,DATA_FATURA,HORA_FATURA,DATA_VENCIMENTO,ID,VALOR,FECHAMENTO,BAIXADA,OPERADOR,SO_CONFIRMADAS,TIPO) ' + ' values(' + IntToStr(Fat_id) + ',' + FormatDataIB(Date) + ',''' + FormatDateTime('hh:mm:ss', Now) + ''',' + FormatDataIB(MEmpresaDATA_VENC.AsVariant) + ',' + IntToStr(MEmpresaEMPRES_ID.AsInteger) + ',' + FormatDimIB(MEmpresaSALDO.AsCurrency) + ',' + FormatDataIB(MEmpresaDATA_FECHA.AsVariant) + ',''N'',''' + Operador.Nome + ''',''' + iif(RGAutor.ItemIndex = 0, 'S', 'N') + ''',''E'')');

            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add(' update contacorrente set fatura_id = ' + inttostr(Fat_id));
            DMConexao.AdoQry.SQL.Add(' where coalesce(fatura_id,0) = 0 ');
            DMConexao.AdoQry.SQL.Add(' and coalesce(baixa_conveniado,''N'') = ''N'' ');
            DMConexao.AdoQry.SQL.Add(' and data_fecha_emp = ' + FormatDataIB(MEmpresaDATA_FECHA.AsVariant));
            DMConexao.AdoQry.SQL.Add(' and conv_id in (select conv_id from conveniados where empres_id = ' + MEmpresaEMPRES_ID.AsString + ') ');
            case RGAutor.ItemIndex of
              0:
                DMConexao.AdoQry.SQL.Add(' and coalesce(entreg_nf,''N'') = ''S'' ');
            end;
            DMConexao.AdoQry.ExecSQL;

            //verificar se tem desconto na empresa e criar o desconto
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('select desconto_emp from empresas where empres_id = ' + MEmpresaEMPRES_ID.AsString + '');
            DMConexao.AdoQry.Open;
            if DMConexao.AdoQry.IsEmpty then
              desc_emp := 0
            else
              desc_emp := DMConexao.AdoQry.Fields[0].AsCurrency;

            if desc_emp > 0 then
            begin
              if MessageBox(Application.Handle, PChar('Foi definido ' + FormatDinBR(desc_emp) + '% de desconto para a empresa' + sLineBreak + MEmpresaNOME.AsString + sLineBreak + 'Deseja utilizar esse desconto?'), 'Aviso', MB_ICONWARNING + MB_YESNO + MB_DEFBUTTON2) = ID_YES then
              begin
                DMConexao.ExecuteSql('update fatura set desc_empresa = ' + FormatDimIB(MEmpresaSALDO.AsCurrency * desc_emp / 100) + ' where fatura_id = ' + IntToStr(Fat_id));
                // essa rotina � feita na hora de dar a baixa na fatura
              end;
            end;

            // ***  Verifica se existe conveniado com bonus
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add(' select count(b.conv_id) from bonus b join conveniados c on c.conv_id = b.conv_id where c.empres_id = ' + IntToStr(MEmpresaEMPRES_ID.AsInteger) + '');
            DMConexao.AdoQry.Open;
            if not DMConexao.AdoQry.IsEmpty then
            begin
              // ***  Atualiza Bonus do conveniado
              GeraBonus(Fat_id);
            end;
            // ***  Temporariamente para acertar a data de fechamento das q ficaram aberta, o trigger acertar�.
            if transp_autor then
            begin
              qContaC.Close;
              qContaC.SQL.Clear;
              qContaC.SQL.Add('select * from contacorrente');
              qContaC.SQL.Add('where coalesce(fatura_id,0) = 0');
              qContaC.SQL.Add('and coalesce(baixa_conveniado,''N'') = ''N''');
              qContaC.SQL.Add('and data_fecha_emp = ' + FormatDataIB(MEmpresaDATA_FECHA.AsVariant));
              qContaC.SQL.Add('and conv_id in (select conv_id from conveniados where empres_id = ' + MEmpresaEMPRES_ID.AsString + ') ');
              qContaC.SQL.Add('and coalesce(entreg_nf,''N'') = ''N''');
              qContaC.SQL.Text;
              qContaC.Open;
              qContaC.First;
              c := 0;
              while not qContaC.Eof do
              begin
                qContaC.Edit;
                DMConexao.AdoQry.Close;
                DMConexao.AdoQry.SQL.Clear;
                DMConexao.AdoQry.SQL.Add(' select min(data_fecha) from dia_fecha where data_fecha > ''' + FormatDataIB(qContaC.FieldByName('DATA').AsVariant) + '''');
                DMConexao.AdoQry.SQL.Add(' and empres_id = ' + MEmpresaEMPRES_ID.AsString + '');
                DMConexao.AdoQry.SQL.Add(' and data_fecha not in (Select fechamento from fatura where apagado <> ''S'' and tipo = ''E'' and id = ' + MEmpresaEMPRES_ID.AsString + ''')');
                DMConexao.AdoQry.SQL.Add(' and data_fecha not in (Select fechamento from fatura where apagado <> ''S'' and tipo = ''C'' and id = ' + qContaC.FieldByName('CONV_ID').AsString + ''')');
                DMConexao.AdoQry.SQL.Text;
                DMConexao.AdoQry.Open;
                qContaC.FieldByName('DATA_FECHA_EMP').AsDateTime := DMConexao.AdoQry.Fields[0].AsDateTime;
                qContaC.Post;
                c := c + 1;
                qContaC.Next;
              end;
              qContaC.Close;
              MsgInf(IntToStr(c) + ' autoriza��es tiveram a data de fechamento alteradas!' + sLineBreak + 'Aten��o o saldo do conveniado n�o ser� verificado!');
            end;
            // ***
            MEmpresa.Edit;
            MEmpresaFATURADA.AsString := 'S';
            MEmpresaMARCADO.AsBoolean := False;
            MEmpresaFATURA_ID.AsInteger := Fat_id;
            MEmpresa.Post;
            DMConexao.AdoCon.CommitTrans;
            Screen.Cursor := crDefault;
          except
            on E: Exception do
            begin
              DMConexao.AdoCon.RollbackTrans;
              MsgErro('Opera��o cancelada! ' + sLineBreak + ' Erro: ' + e.Message);
            end;
          end;

          //Trecho que adiciona o boleto na linha processada
          if MsgSimNao('Deseja anexar o boleto de cobran�a da Empresa ' + MEmpresaEMPRES_ID.AsString + '?', False) then
          begin
            dlgAbrePDF.Title := 'Carregamento de Boleto';
            dlgAbrePDF.InitialDir := '"\\tsclient\Z\"';
            qBoletos.Open;
            qBoletos.Insert;

            if dlgAbrePDF.Execute then
            begin
              if ExtractFileExt(dlgAbrePDF.FileName) <> '.pdf' then
                ShowMessage('Arquivo inv�lido.')
              else begin
                arquivoPDF := dlgAbrePDF.FileName;
                DecodeDate(Now, ano, mes, dia);
                caminhoAno := 'C:\Dados\Web\webEmpresas\Faturas\' + IntToStr(ano);
                if Length(IntToStr(mes)) < 2 then
                  caminhoMes := '0' + IntToStr(mes)
                else
                  caminhoMes := IntToStr(mes);

                if not DirectoryExists(caminhoAno) then
                  CreateDir(caminhoAno);

                if not DirectoryExists(caminhoAno + '\' + caminhoMes) then
                  CreateDir(caminhoAno + '\' + caminhoMes);

                caminhoCompleto := caminhoAno + '\' + caminhoMes + '\' + IntToStr(Fat_id) + '.pdf';
                caminhoRelativo := 'FATURAS\' + IntToStr(ano) + '\' + caminhoMes + '\' + IntToStr(Fat_id) + '.pdf';

                CopyFile(PChar(arquivoPDF), PChar(caminhoCompleto), False);
                qBoletosCAMINHO_ARQUIVO.Text := caminhoRelativo;
                qBoletosDATA_VENCIMENTO.AsDateTime := MEmpresaDATA_VENC.AsDateTime;
                QBoletosFATURA_ID1.AsInteger := Fat_id;
                qBoletosDATA_ALTERACAO.AsDateTime := Now;
                qBoletosOPERADOR.AsString := Operador.Nome;
                qBoletos.Post;
                ShowMessage('Boleto carregado com sucesso!');
              end
            end
            else
              ShowMessage('O boleto n�o p�de ser carregado');
            qBoletos.Close;
          end;
        end;
        MEmpresa.Next;
      end;
      MEmpresa.First;
      MsgInf('Opera��o efetuada com sucesso!'+sLineBreak+'Os fechamentos marcados foram efetuados.'+sLineBreak+'Ser� poss�vel visualiz�-los nas Faturas.');
    end;
  end
  else
    MsgInf('Nenhuma empresa marcada!');
end;

procedure TFFechamento.GeraDescontoEmpresa(Emp: Integer; DataFecha: TDateTime; Fat: integer; Nome: string);
var
  strSql: string;
begin
  QDescEmp.Close;
  QDescEmp.SQL.Clear;
  QDescEmp.SQL.Add(' Select cc.cred_id, cc.data_fecha_emp, sum(cc.debito-cc.credito) valor, emp.desconto_emp, ');
  QDescEmp.SQL.Add(' sum(cc.debito-cc.credito)*emp.desconto_emp/100 as desc_valor ');
  QDescEmp.SQL.Add(' from contacorrente cc ');
  QDescEmp.SQL.Add(' join conveniados conv on conv.conv_id = cc.conv_id ');
  QDescEmp.SQL.Add(' join empresas emp on conv.empres_id = emp.empres_id ');
  QDescEmp.SQL.Add(' where cc.fatura_id = ' + IntToStr(Fat));
  QDescEmp.SQL.Add(' and cc.data_fecha_emp = ' + FormatDataIB(DataFecha));
  QDescEmp.SQL.Add(' and emp.empres_id = ' + IntToStr(Emp));
  QDescEmp.SQL.Add(' group by cc.cred_id, cc.data_fecha_emp, emp.desconto_emp ');
  QDescEmp.Open;
  if not QDescEmp.IsEmpty then
  begin
    QDescEmp.First;
    while not QDescEmp.Eof do
    begin
      if QDescEmp.FieldByName('VALOR').AsCurrency > 0 then
      begin
        strSql := 'insert into taxas_prox_pag (TAXAS_PROX_PAG_ID,CRED_ID,DESCRICAO,VALOR,FATURA_ID,APAGADO,BRUTO) ' + 'values (gen_id(GEN_TAXAS_PROX_PAG_ID,1),' + QDescEmp.FieldByName('CRED_ID').AsString + ',"DESCONTO EMPRESA ' + FormatDinBR(QDescEmp.FieldByName('DESCONTO_EMP').AsCurrency) + '% - FAT: ' + IntToStr(Fat) + ' - ' + copy(Nome, 1, 20) + '",' + FormatDimIB(QDescEmp.FieldByName('DESC_VALOR').AsCurrency) + ',' + IntToStr(Fat) + ',"N","N")';
        DMConexao.ExecuteSql(strSql);
      end;
      QDescEmp.Next;
    end;
  end;
  QDescEmp.Close;
end;

procedure TFFechamento.QEmpresaAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  ButFechar.Enabled := (QEmpresaFATURADA.AsString = 'N');
end;

procedure TFFechamento.FormCreate(Sender: TObject);
begin
  inherited;
  LogCC := TLog.Create;
  LogCC.LogarQuery(qContaC, 'CONTACORRENTE', 'Autoriza��o: ', 'Fechamento Empr.', Operador.Nome, 'autorizacao_id');
  DataIni.Date := Date;
  DataFin.Date := Date;
  DMConexao.Config.Open;
  if DMConexao.ConfigESTILO_ENTREGUE.AsString = 'N' then
  begin
    RGAutor.ItemIndex := 1;
    AutConf := false;
  end
  else
  begin
    AutConf := true;
  end;
  DMConexao.Config.Close;
  FMenu.vFechtoEmp := True;
end;

procedure TFFechamento.GeraBonus(fatura_id: integer);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(' insert into bonus (conv_id, valor, fatura_id) ');
  DMConexao.AdoQry.SQL.Add(' select cc.conv_id, sum(cc.debito-cc.credito)*-1 as valor, cc.fatura_id ');
  DMConexao.AdoQry.SQL.Add(' from contacorrente cc where cc.fatura_id = ' + IntToStr(fatura_id));
  DMConexao.AdoQry.SQL.Add(' group by cc.conv_id, cc.fatura_id ');
  DMConexao.AdoQry.ExecSQL;
end;

procedure TFFechamento.RGAutorClick(Sender: TObject);
begin
  inherited;
  if ((RGAutor.ItemIndex = 1) and (AutConf)) then
  begin
    AutConf := false;
    ButFechar.Enabled := false;
  end
  else if ((RGAutor.ItemIndex = 0) and (not AutConf)) then
  begin
    AutConf := true;
    ButFechar.Enabled := false;
  end;
end;

procedure TFFechamento.btnMarDesClick(Sender: TObject);
begin
  inherited;
  MarcaDesm(MEmpresa);
end;

procedure TFFechamento.btnMarTodClick(Sender: TObject);
begin
  inherited;
  MarcaTodos(MEmpresa);
end;

procedure TFFechamento.btnDesTodClick(Sender: TObject);
begin
  inherited;
  DesmTodos(MEmpresa);
end;

procedure TFFechamento.grdFechDblClick(Sender: TObject);
begin
  inherited;
  btnMarDes.Click;
end;

procedure TFFechamento.grdFechKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    grdFechDblClick(nil)
  else
    GridScroll(Sender, Key, Shift);
end;

procedure TFFechamento.MarcaTodos(DataSet: TDataSet);
var
  marca: TBookmark;
begin
  if MEmpresa.IsEmpty then
    Exit;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  MEmpresa.First;
  while not MEmpresa.eof do
  begin
    if MEmpresaFATURADA.AsString = 'N' then
    begin
      MEmpresa.Edit;
      MEmpresaMarcado.AsBoolean := true;
      MEmpresa.Post;
    end;
    MEmpresa.Next;
  end;
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
  EmpSel;

end;

procedure TFFechamento.DesmTodos(DataSet: TDataSet);
var
  marca: TBookmark;
begin
  if MEmpresa.IsEmpty then
    Exit;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  MEmpresa.First;
  while not MEmpresa.eof do
  begin
    if MEmpresaFATURADA.AsString = 'N' then
    begin
      MEmpresa.Edit;
      MEmpresaMarcado.AsBoolean := false;
      MEmpresa.Post;
    end;
    MEmpresa.Next;
  end;
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
  EmpSel;
end;

procedure TFFechamento.MarcaDesm(Dataset: TDataSet);
begin
  if DataSet.IsEmpty then
    exit;
  DataSet.Edit;
  if DataSet.FieldByName('FATURADA').AsString = 'N' then
  begin
    if MEmpresa.IsEmpty then
      Exit;
    MEmpresa.Edit;
    MEmpresaMARCADO.AsBoolean := not MEmpresaMARCADO.AsBoolean;
    MEmpresa.Post;
    EmpSel;
  end
  else
    MsgInf('Marca��o dispon�vel apenas nos fechamentos n�o faturados!');
end;

procedure TFFechamento.EmpSel;
var
  marca: TBookmark;
begin
  emp_sel := EmptyStr;
  MEmpresa.DisableControls;
  marca := MEmpresa.GetBookmark;
  MEmpresa.First;
  while not MEmpresa.Eof do
  begin
    if MEmpresaMARCADO.AsBoolean = true then
      emp_sel := emp_sel + ',' + MEmpresaEMPRES_ID.AsString;
    MEmpresa.Next;
  end;
  emp_sel := Copy(emp_sel, 2, Length(emp_sel));
  MEmpresa.GotoBookmark(marca);
  MEmpresa.FreeBookmark(marca);
  MEmpresa.EnableControls;
end;

procedure TFFechamento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  LogCC.Free;
  FMenu.vFechtoEmp := False;
end;

procedure TFFechamento.grdFechDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  L, R: Integer;
  marc: Boolean;
begin
  inherited;
  marc := False;
  Grid := TDBGrid(Sender);
  if ((gdFocused in State) and (dgEditing in Grid.Options)) then
  begin
    Grid.Canvas.Font.Style := [fsBold];
    Grid.Canvas.Font.Color := clTeal;
  end
  else if (Grid.DataSource.DataSet.FindField('FATURADA') <> nil) and (Grid.DataSource.DataSet.FindField('FATURADA').AsString = 'S') then
  begin
    Grid.Canvas.Font.Color := clRed;
    marc := True;
  end
  else
    Grid.Canvas.Font.Color := clBlack;
  R := Rect.Right;
  L := Rect.Left;
  if Column.Index = 0 then
    L := L + 1;
  if Column.Index = Grid.Columns.Count - 1 then
    R := R - 1;
  Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top + 1, R, Rect.Bottom - 1));
  Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top + 1, R, Rect.Bottom - 1), DataCol, Column, State);

end;

procedure TFFechamento.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (key = VK_F8) then
    btnMarDes.Click
  else if (key = VK_F9) then
    btnMarTod.Click
  else if (key = VK_F10) then
    btnDesTod.Click;
end;

end.


