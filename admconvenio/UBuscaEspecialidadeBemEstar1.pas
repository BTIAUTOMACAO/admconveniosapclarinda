unit UBuscaEspecialidadeBemEstar1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, JvExDBGrids, JvDBGrid, ComCtrls,
  StdCtrls, Buttons, ExtCtrls;

type
  TFBuscaEspecialidade1 = class(TForm)
    Panel6: TPanel;
    panTitulo: TPanel;
    ButClose: TSpeedButton;
    Panel1: TPanel;
    btnFiltroDados: TSpeedButton;
    btnAlterLinear: TSpeedButton;
    btnFirstA: TSpeedButton;
    btnPriorA: TSpeedButton;
    btnNextA: TSpeedButton;
    btnLastA: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    ButAtualiza: TBitBtn;
    ButFiltro: TBitBtn;
    ButAltLin: TButton;
    ButBusca: TBitBtn;
    EdNome: TEdit;
    EdCod: TEdit;
    PageControl1: TPageControl;
    JvDBGrid1: TJvDBGrid;
    QCadastro: TADOQuery;
    QCadastroESPECIALIDADE_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroLIBERADO: TStringField;
    DSCadastro: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBuscaEspecialidade1: TFBuscaEspecialidade1;

implementation

{$R *.dfm}

end.
