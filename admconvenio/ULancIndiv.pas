unit ULancIndiv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, DBCtrls, CurrEdit, Mask,
  ToolEdit, {JvLookup,} DB, ZAbstractDataset, ZDataset, ZAbstractRODataset,
  UTipos, XMLDoc, DBClient, RxMemDS, Dateutils, JvExControls, JvDBLookup,
  Grids, DBGrids, SimpleDS, clipbrd, InvokeRegistry, Rio, SOAPHTTPClient,
  ComCtrls, JvExComCtrls, JvDateTimePicker, JvDBDateTimePicker, JvExMask,
  JvToolEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit, JvExDBGrids, JvDBGrid, CheckLst, uProdutos,
  JvExStdCtrls, JvEdit, JvValidateEdit, ADODB;

const DEBUG: boolean = false;

type
  TPalavras = Record
    DS_PALAVRA: String;
    VR_COR : TColor;
    ESTILO : TFontStyles;
  end;

  TFLancIndiv = class(TF1)
    dsEstab: TDataSource;
    dsEmpr: TDataSource;
    dsConv: TDataSource;
    dsCCorrente: TDataSource;
    qForPgto: TRxMemoryData;
    qForPgtoFormaId: TIntegerField;
    qForPgtoDescricao: TStringField;
    dsForPgto: TDataSource;
    dsProgramas: TDataSource;
    cds: TSimpleDataSet;
    cdsCodBarras: TStringField;
    cdsDescricao: TStringField;
    cdsID: TIntegerField;
    cdsValorUnitario: TCurrencyField;
    dsCds: TDataSource;
    cdsQuantidade: TIntegerField;
    cdsValorTotal: TCurrencyField;
    HTTPRIO1: THTTPRIO;
    cdsGrupo: TIntegerField;
    cdsNumPrescritor: TStringField;
    cdsufPrescritor: TStringField;
    cdsNumReceita: TIntegerField;
    cdsdtReceita: TDateField;
    cdstipoPrescritor: TStringField;
    pc: TPageControl;
    tsPrincipal: TTabSheet;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    LabEmp: TLabel;
    cbbEmpr: TJvDBLookupCombo;
    edtEmpr: TEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    LabCre: TLabel;
    cbbEstab: TJvDBLookupCombo;
    edtEstab: TEdit;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ButPesqConv: TSpeedButton;
    edtCartao: TEdit;
    edtChapa: TEdit;
    edtTitular: TEdit;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label17: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label10: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    lblCodProd: TDBText;
    lblPreco: TDBText;
    lblValorTotal: TDBText;
    Label20: TLabel;
    BtnInclui: TBitBtn;
    btnApaga: TBitBtn;
    cbbReceita: TComboBox;
    btnPesqProd: TBitBtn;
    edtCodBarras: TDBEdit;
    edtDescricao: TDBEdit;
    edtQtd: TDBEdit;
    edtNumPresc: TDBEdit;
    btnGravar: TBitBtn;
    dtpDtReceita: TDBEdit;
    cbUF: TDBComboBox;
    edtNumReceit: TDBEdit;
    z: TJvDBGrid;
    cbTipoPresc: TComboBox;
    FLancIndiv: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    LabAguarde: TLabel;
    //edtData: TDateEdit;
    edtNota: TEdit;
    //edtDebito: TCurrencyEdit;
    //edtCredito: TCurrencyEdit;
    edtHistorico: TEdit;
    cbbFormaPgto: TDBLookupComboBox;
    btnPegaAut: TButton;
    Label13: TLabel;
    reAut: TRichEdit;
    lblAut: TLabel;
    btnNovoLanc: TButton;
    Label26: TLabel;
    edtCnpj: TEdit;
    edtDebito: TJvValidateEdit;
    edtData: TJvDateEdit;
    qEstab: TADOQuery;
    qEstabcred_id: TIntegerField;
    qEstabnome: TStringField;
    qEstabfantasia: TStringField;
    qEstabcodacesso: TIntegerField;
    qEstabliberado: TStringField;
    qEstabsenha: TStringField;
    qEstabcgc: TStringField;
    qEstabvencimento1: TWordField;
    qEmpr: TADOQuery;
    qEmprempres_id: TAutoIncField;
    qEmprnome: TStringField;
    qEmprfantasia: TStringField;
    qEmprliberada: TStringField;
    qEmprprog_desc: TStringField;
    qConv: TADOQuery;
    qConvconv_id: TIntegerField;
    qConvcartao_id: TIntegerField;
    qConvnome: TStringField;
    qConvcpf: TStringField;
    qConvcodigo: TIntegerField;
    qConvdigito: TWordField;
    qConvcodcartimp: TStringField;
    qConvcartlib: TStringField;
    qConvconvlib: TStringField;
    qConvchapa: TFloatField;
    qConvsenha: TStringField;
    qConvempres_id: TIntegerField;
    qCCorrente: TADOQuery;
    qCCorrenteAUTORIZACAO_ID: TIntegerField;
    qCCorrenteCARTAO_ID: TIntegerField;
    qCCorrenteCONV_ID: TIntegerField;
    qCCorrenteCRED_ID: TIntegerField;
    qCCorrenteDIGITO: TWordField;
    qCCorrenteDATA: TDateTimeField;
    qCCorrenteHORA: TStringField;
    qCCorrenteDATAVENDA: TDateTimeField;
    qCCorrenteDEBITO: TBCDField;
    qCCorrenteCREDITO: TBCDField;
    qCCorrenteVALOR_CANCELADO: TBCDField;
    qCCorrenteBAIXA_CONVENIADO: TStringField;
    qCCorrenteBAIXA_CREDENCIADO: TStringField;
    qCCorrenteENTREG_NF: TStringField;
    qCCorrenteRECEITA: TStringField;
    qCCorrenteCESTA: TStringField;
    qCCorrenteCANCELADA: TStringField;
    qCCorrenteDIGI_MANUAL: TStringField;
    qCCorrenteTRANS_ID: TIntegerField;
    qCCorrenteFORMAPAGTO_ID: TIntegerField;
    qCCorrenteFATURA_ID: TIntegerField;
    qCCorrentePAGAMENTO_CRED_ID: TIntegerField;
    qCCorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    qCCorrenteOPERADOR: TStringField;
    qCCorrenteDATA_VENC_EMP: TDateTimeField;
    qCCorrenteDATA_FECHA_EMP: TDateTimeField;
    qCCorrenteDATA_VENC_FOR: TDateTimeField;
    qCCorrenteDATA_FECHA_FOR: TDateTimeField;
    qCCorrenteHISTORICO: TStringField;
    qCCorrenteNF: TIntegerField;
    qCCorrenteDATA_ALTERACAO: TDateTimeField;
    qCCorrenteDATA_BAIXA_CONV: TDateTimeField;
    qCCorrenteDATA_BAIXA_CRED: TDateTimeField;
    qCCorrenteOPER_BAIXA_CONV: TStringField;
    qCCorrenteOPER_BAIXA_CRED: TStringField;
    qCCorrenteDATA_CONFIRMACAO: TDateTimeField;
    qCCorrenteOPER_CONFIRMACAO: TStringField;
    qCCorrenteCONFERIDO: TStringField;
    qCCorrenteNSU: TIntegerField;
    qCCorrentePREVIAMENTE_CANCELADA: TStringField;
    qCCorrenteEMPRES_ID: TIntegerField;
    qProgramas: TADOQuery;
    qProgramasprog_id: TIntegerField;
    qProgramasnome: TStringField;
    qLogLancIndv: TADOQuery;
    qLogLancIndvLOG_LANC_IND_ID: TIntegerField;
    qLogLancIndvDATA_INI: TDateTimeField;
    qLogLancIndvDATA_FIN: TDateTimeField;
    qLogLancIndvABRIR_TRANS_XML: TMemoField;
    qLogLancIndvABRIR_TRANS_XML_RET: TMemoField;
    qLogLancIndvVALIDAR_PRODS_XML: TMemoField;
    qLogLancIndvVALIDAR_PRODS_XML_RET: TMemoField;
    qLogLancIndvFECHAR_TRANS_XML: TMemoField;
    qLogLancIndvFECHAR_TRANS_XML_RET: TMemoField;
    qLogLancIndvCONFIRMAR_TRANS_XML: TMemoField;
    qLogLancIndvCONFIRMAR_TRANS_XML_RET: TMemoField;
    qLogLancIndvCANCELA_TRANS_XML: TMemoField;
    qLogLancIndvCANCELA_TRANS_XML_RET: TMemoField;
    qLogLancIndvMSG_ERRO: TMemoField;
    QFormaPagto: TADOQuery;
    QFormaPagtoFORMA_ID: TIntegerField;
    QFormaPagtoDESCRICAO: TStringField;
    DSFormaPagto: TDataSource;
    Label12: TLabel;
    edtSaldo: TJvValidateEdit;
    procedure FormCreate(Sender: TObject);
    procedure edtEstabKeyPress(Sender: TObject; var Key: Char);
    procedure edtEmprKeyPress(Sender: TObject; var Key: Char);
    procedure edtChapaKeyPress(Sender: TObject; var Key: Char);
    procedure edtEstabChange(Sender: TObject);
    procedure edtEmprChange(Sender: TObject);
    procedure cbbEstabChange(Sender: TObject);
    procedure cbbEmprChange(Sender: TObject);
    procedure edtChapaExit(Sender: TObject);
    procedure edtTitularKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButPesqConvClick(Sender: TObject);
    procedure edtNotaKeyPress(Sender: TObject; var Key: Char);
    procedure FormResize(Sender: TObject);
    procedure btnApagaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cdsAfterPost(DataSet: TDataSet);
    procedure cdsAfterDelete(DataSet: TDataSet);
    procedure btnPegaAutClick(Sender: TObject);
    procedure btnPesqProdClick(Sender: TObject);
    procedure cbbReceitaChange(Sender: TObject);
    procedure edtCartaoExit(Sender: TObject);
    procedure edtQtdChange(Sender: TObject);
    procedure cdsBeforePost(DataSet: TDataSet);
    procedure BtnIncluiClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure edtDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dtpDtReceitaExit(Sender: TObject);
    procedure edtQtdKeyPress(Sender: TObject; var Key: Char);
    procedure zKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbTipoPrescChange(Sender: TObject);
    procedure edtCodBarrasExit(Sender: TObject);
    procedure cbbReceitaExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tsPrincipalShow(Sender: TObject);
    procedure btnNovoLancClick(Sender: TObject);
    procedure edtEstabEnter(Sender: TObject);
    procedure edtEstabExit(Sender: TObject);
    procedure edtCnpjExit(Sender: TObject);
    procedure edtDebitoKeyPress(Sender: TObject; var Key: Char);
    procedure edtDebitoEnter(Sender: TObject);
    procedure edtDebitoExit(Sender: TObject);
  private
    { Private declarations }
    palavra : TPalavras;
    posPalavras : Array of Integer;
    Autors : Array [1..24] of TAutorizacao;
    FocoCodFor : Boolean;
    retornoTestes : TStrings;
    procedure AposEmpEstabNull;
    function ErroConectarWebService(mensagem : String) : Boolean;
    function somaProdutos : Currency;
    procedure LimparCamposProdutos;
    procedure Valida;
    function existeProdsRec : Boolean;
    Procedure CMDialogKey(Var Msg: TWMKey); message CM_DIALOGKEY;
    function venc_fornec:TDateTime;
    function  achaConveniado(Tipo: Integer) : Boolean;
    procedure ExibeForPgto;
    procedure ExibeForPgto2;
    procedure pesquisarProduto(Descricao : string  = ''; CodBarras : String = ''; prog_ids : string = '' ; UF : String = '');
    function pegarAutorizacao : String;
    procedure preencherCds(prod : uProdutos.TProduto);
    procedure mudarCorComp(Obj :TObject; cor : TColor);
    procedure acertarDecimal;
  public
    { Public declarations }
    Lanc_Webservice, Path_WebService: string;
    informar_emp : Boolean;
  end;

var
  FLancIndiv: TFLancIndiv;

implementation

uses DM, cartao_util, UPesqConv, UMenu, wsconvenio,
 MValidarFormasPagto, XMLIntf, MEfetuarTransacao , FPesqProdNew,
  dmProgramasDesconto, URotinasTexto, UValidacao,  StrUtils;

{$R *.dfm}

procedure TFLancIndiv.AposEmpEstabNull;
begin
  qForPgto.EmptyTable;
  edtCartao.Clear;
  edtChapa.Clear;
  edtTitular.Clear;
end;

function TFLancIndiv.ErroConectarWebService(mensagem: String): Boolean;
begin
  Result := False;
  if Pos('A connection with the server could not be established',mensagem) > 0 then begin
    MsgErro('N�o foi poss�vel estabelecer uma conex�o com o servidor,' + sLineBreak +
      'verifique se a op��o "Caminho do Web Service" est� com um caminho v�lido ' + sLineBreak +
      'caso o problema persista entre em contato com o suporte t�cnico');
    Result := True;
  end;
end;



function TFLancIndiv.existeProdsRec: Boolean;
var bm : tbookmark;
begin
  Result := False;
  bm := cds.GetBookmark;
  cds.DisableControls;
  cds.First;
  while not cds.eof do begin
    if cdsufPrescritor.AsString <> '' then begin
      Result := True;
      Break;
    end else
      cds.Next;
  end;
  cds.GotoBookmark(bm);
  cds.FreeBookmark(bm);
  cds.EnableControls;
end;

function TFLancIndiv.somaProdutos : Currency;
var BM : TBookmark;
    total : currency;
begin
  total := 0;
  if not cds.IsEmpty then begin
    BM := cds.GetBookmark;
    cds.DisableControls;
    cds.First;
    while not cds.Eof do begin
      total := total + cdsValorTotal.AsCurrency;
      cds.Next;
    end;
    cds.GotoBookmark(BM);
    cds.FreeBookmark(BM);
  end;
  if cds.ControlsDisabled then
    cds.EnableControls;
  Result := total;
end;

procedure TFLancIndiv.acertarDecimal;
begin
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
end;

procedure TFLancIndiv.LimparCamposProdutos;
begin
    edtQtd.Clear;
    edtDescricao.Clear;
    edtCodBarras.Clear;
    lblPreco.Caption   := '';
    lblPreco.Caption   := '';
    lblCodProd.Caption := '';
end;

function TFLancIndiv.achaConveniado(Tipo: Integer) : Boolean;
var conSaldo : String;
  Retorno : String;
  saldo : Real;
  i : currency;
  ConsultarCartaoRet : TConsultarCartaoRet;
   numero:Real;
begin
  Result := False;
  try
  qConv.Close;
  qConv.SQL.Clear;
  qConv.SQL.Add(' Select cartoes.conv_id, conveniados.cpf, cartoes.cartao_id, cartoes.nome, ');
  qConv.SQL.Add(' cartoes.codigo, cartoes.digito, cartoes.codcartimp, cartoes.liberado as cartlib, ');
  qConv.SQL.Add(' conveniados.liberado as convlib, conveniados.chapa, conveniados.senha, conveniados.empres_id ');
  qConv.SQL.Add(' from cartoes ');
  qConv.SQL.Add(' join conveniados on conveniados.conv_id = cartoes.conv_id ');
  qConv.SQL.Add(' where conveniados.apagado <> ''S'' and cartoes.apagado <> ''S'' ');
  if Trim(edtEmpr.Text) <> '' then
    qConv.SQL.Add(' and conveniados.empres_id = '+edtEmpr.Text);
  case Tipo of
    1: qConv.SQL.Add(' and cartoes.cartao_id = '+FPesqConv.QConveniadoscartao_id.AsString);
    2: begin
         if (Trim(edtEmpr.Text) = '') then begin
           edtEmpr.SetFocus;
           raise Exception.Create('Selecione a empresa.'+#13+'Empresa obrigat�ria para pesquisa por chapa.');
         end;
         qConv.SQL.Add(' and cartoes.titular = ''S'' ');
         qConv.SQL.Add(' and conveniados.chapa = '+edtChapa.Text);
       end;
    3: begin
         if Length(edtCartao.Text) = 11 then begin
           qConv.SQL.Add(' and ( ( cartoes.codigo = '+Copy(edtCartao.Text,1,9));
           qConv.SQL.Add(' and cartoes.digito = '+Copy(edtCartao.Text,10,2)+')');
           qConv.SQL.Add(' or ');
         end
         else
           qConv.SQL.Add(' and (');
         qConv.SQL.Add(' cartoes.codcartimp = '''+edtCartao.Text+''' )');
       end;
   end;
   qConv.SQL.Add(' order by cartoes.titular desc ');
   qConv.Sql.Text;
   qConv.Open;
   if ((qConv.RecordCount > 1) and (Tipo in [1,3])) then begin// teste para verificar se existe mais de um cartao com o mesmo codcartimp.
     edtTitular.Text  := edtCartao.Text;
     ButPesqConv.Click;
     Exit;
   end;
   if not qConv.IsEmpty then begin
     Screen.Cursor := crHourGlass;
     conSaldo := '<?xml version="1.0" encoding="iso-8859-1"?>';
     conSaldo := conSaldo + '<CONSULTAR_CARTOES_PARAM><CREDENCIADO><CODACESSO>'+qEstab.FieldByName('CODACESSO').AsString;
     conSaldo := conSaldo + '</CODACESSO><SENHA>'+Crypt('D',qEstab.FieldByName('SENHA').AsString,'BIGCOMPRAS')+'</SENHA>';
     conSaldo := conSaldo + '</CREDENCIADO><NOME_CARTAO>'+QConvcodcartimp.AsString+'</NOME_CARTAO></CONSULTAR_CARTOES_PARAM>';
     edtEmpr.Text := QConv.FieldByName('EMPRES_ID').AsString;
     edtTitular.Text := QConv.FieldByName('NOME').AsString;
     edtChapa.Text   := IntToStr(Trunc(QConv.FieldByName('CHAPA').AsFloat));
     edtCartao.Text  := QConv.FieldByName('CODIGO').AsString+FormatFloat('00',QConv.FieldByName('DIGITO').AsFloat);
     cbbEmpr.KeyValue := QConv.FieldByName('EMPRES_ID').AsString;
     Retorno := (HTTPRIO1 as wsconvenioSoap).MConsultarCartoes(conSaldo);
     if Retorno <> '' then
        ConsultarCartaoRet := TConsultarCartaoRet.Create(Retorno);
        DecimalSeparator := '.';
        edtSaldo.Text := FormatFloat('0.00',ArredondaDin(StrToInt(ConsultarCartaoRet.SaldoRestante)/100));
        edtNota.SetFocus;
        Result := True;
   end else begin
     QConv.Close;
     msgInf('Nenhum cart�o encontrado.');
     Case Tipo Of
       1 : edtTitular.SetFocus;
       2 : edtChapa.SetFocus;
       3 : edtCartao.SetFocus;
     end;
     Exit;
   end;
   Screen.Cursor := crDefault;
   ExibeForPgto2;
   except
    on e : exception do begin
      if not ErroConectarWebService(e.message) then
        msgErro('N�o foi poss�vel capturar as formas de pagamento. Devido: '
        + sLineBreak + e.Message + 'Entre em contato com o suporte T�cnico');
      end;
   end;
   edtEmpr.ReadOnly := qConv.IsEmpty;
   cbbEmpr.ReadOnly := qConv.IsEmpty;
end;

procedure TFLancIndiv.ExibeForPgto2;
var
  i : Integer;
  Retorno : String;
  cred : TCredenciado;
  ValidarFormasPgto : TValidarFormaPgto;
  ValidarFormasPgtoRet : TValidarFormaPgtoRet;
begin
  lblAut.Caption := 'Aguarde! Conectando ao servidor..';
  lblAut.Font.Size := 30;
  Application.ProcessMessages;
  Screen.Cursor := crHourGlass;
  cred := TCredenciado.Create(qEstab.FieldByName('CODACESSO').AsInteger, Crypt('D',qEstab.FieldByName('SENHA').AsString,'BIGCOMPRAS'));
  ValidarFormasPgto := TValidarFormaPgto.Create(cred,qEmprEMPRES_ID.AsInteger);
  try
    if (Trim(ValidarFormasPgto.getXML) <> '') then begin
      Retorno := (HTTPRIO1 as wsconvenioSoap).MValidarFormasPagto(ValidarFormasPgto.getXML);
    end else begin
      msgErro('Erro ao capturar formas de pagamento');
      Screen.Cursor := crDefault;
    end;
  except
    on e:Exception do
    begin
      if not ErroConectarWebService(e.Message) then begin
        MsgErro('Erro ao conectar a administradora para validar as formas de pagamento.'+sLineBreak+
                'Erro: '+e.Message+sLineBreak+
                'Favor entrar em contato com a administradora para efetuar a(s) autoriza��o(�es)');
                Screen.Cursor := crDefault;
        Exit;
      end else
        Screen.Cursor := crDefault;
        Exit;
    end;
  end;
  ValidarFormasPgtoRet := TValidarFormaPgtoRet.Create(Retorno);
  if ValidarFormasPgtoRet.Status = 0 then
  begin
    qForPgto.Open;
    qForPgto.EmptyTable;
    for i := 0 to ValidarFormasPgtoRet.FormasPagtos.Count -1 do
    begin
      qForPgto.Append;
      qForPgto.FieldByName('FORMAID').AsInteger  := ValidarFormasPgtoRet.FormasPagtos.FormasPgto[I].Codigo;
      qForPgto.FieldByName('DESCRICAO').AsString := ValidarFormasPgtoRet.FormasPagtos.FormasPgto[I].Descricao;
      qForPgto.Post;
    end;
    qForPgto.First;
    cbbFormaPgto.KeyValue:= qForPgto.FieldByName('FORMAID').AsInteger;
    cbbFormaPgto.Enabled:= True;
  end
  else
  begin
    cbbFormaPgto.Enabled:= False;
  end;
  lblAut.Caption := '';
  ValidarFormasPgto.Free;
  ValidarFormasPgtoRet.Free;
  Screen.Cursor := crDefault;
end;

procedure TFLancIndiv.ExibeForPgto;
var
  Retorno : string;
  Param : IXMLTValidarFormasPagtoParam;
  Ret   : IXMLTValidarFormasPagtoRet;
  i: Integer;
begin
  lblAut.Caption := 'Aguarde! Conectando ao servidor..';
  lblAut.Font.Size := 30;
  Param := NewXMLDocument.GetDocBinding('VALIDAR_FORMASPAGTO_PARAM', TXMLTValidarFormasPagtoParam, '') as IXMLTValidarFormasPagtoParam;
  Param.CREDENCIADO.CODACESSO := qEstab.FieldByName('CODACESSO').AsInteger;
  Param.CREDENCIADO.SENHA := Crypt('D',qEstab.FieldByName('SENHA').AsString,'BIGCOMPRAS');
  Param.EMPRES_ID := qEmpr.FieldByName('EMPRES_ID').AsInteger;
  try
    Retorno := GetwsconvenioSoap(False,Path_WebService).MValidarFormasPagto(Param.OwnerDocument.XML.Text);
    Ret := LoadXMLData(retorno).GetDocBinding('VALIDAR_FORMASPAGTO_RET',TXMLTValidarFormasPagtoRet,'') as IXMLTValidarFormasPagtoRet;
  except
    on e:Exception do
    begin
      MsgErro('Erro ao conectar a administradora para validar as formas de pagamento.'+sLineBreak+
              'Erro: '+e.Message+sLineBreak+
              'Favor entrar em contato com a administradora para efetuar a(s) autoriza��o(�es)');
      Exit;
    end;
  end;
  if Ret.STATUS = 0 then
  begin
    qForPgto.Open;
    qForPgto.EmptyTable;
    for i := 0 to Ret.FORMAPAGTO.Count -1 do
    begin
      qForPgto.Append;
      qForPgto.FieldByName('FORMAID').AsInteger  := Ret.FORMAPAGTO.Items[i].CODIGO;
      qForPgto.FieldByName('DESCRICAO').AsString := Ret.FORMAPAGTO.Items[i].DESCRICAO;
      qForPgto.Post;
    end;
    qForPgto.First;
    cbbFormaPgto.KeyValue:= qForPgto.FieldByName('FORMAID').AsInteger;
    cbbFormaPgto.Enabled:= True;
  end
  else
  begin
    cbbFormaPgto.Enabled:= False;
  end;
  lblAut.Caption := '';
end;

procedure TFLancIndiv.FormCreate(Sender: TObject);
var ip, pcName : String;
begin
  inherited;
  DecimalSeparator  := ',';
  ThousandSeparator := '.';
  Screen.Cursor := crHourGlass;
  DMConexao.Config.Open;
  if DMConexao.ConfigCRE_FANTASIA.AsString = 'S' then
  begin
    LabCre.Caption := 'Nome Fantasia';
    cbbEstab.LookupDisplay := 'FANTASIA';
    qEstab.SQL.Text := qEstab.SQL.Text + ' order by fantasia ';
  end
  else
    qEstab.SQL.Text := qEstab.SQL.Text + ' order by nome ';
  if DMConexao.ConfigEMP_FANTASIA.AsString = 'S' then
  begin
    LabEmp.Caption := 'Nome Fantasia';
    cbbEmpr.LookupDisplay := 'FANTASIA';
    qEmpr.SQL.Text := qEmpr.SQL.Text + ' order by fantasia ';
  end
  else
    qEmpr.SQL.Text := qEmpr.SQL.Text + ' order by nome ';
  DMConexao.Config.Close;
  qEmpr.Open;
  qEstab.Open;
  qProgramas.Open;
  cds.CreateDataSet;
  edtData.Date := Date;
  DMConexao.Config.Open;  //Define se o foco ap�s o lan�amentos vai para o fornecedor.
  FocoCodFor := (DMConexao.ConfigFLANCAMENTOS_FOCO_FORNEC.AsString = 'S');
  if DMConexao.ConfigFLANCAMENTOS_AUTOR_WEBSERVICE.AsString = 'S' then
  begin
    Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString;
    if Path_WebService <> '' then begin
      Path_WebService := Path_WebService + 'wsconvenio.asmx?wsdl';
      HTTPRIO1.URL := Path_WebService;
      HTTPRIO1.WSDLLocation := Path_WebService;
      HTTPRIO1.Service := 'wsconvenio';
      HTTPRIO1.Port :=  'wsconvenioSoap';
    end else
      msgInf('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');
  end;
  informar_emp := (DMConexao.ConfigINFORMA_EMP_LANC_IND.AsString = 'S');
  DMConexao.Config.Close;
  ip := GetIP;
  pcName := UpperCase(GetNomeComputador);
  pc.Pages[0].TabVisible := false;

  pc.ActivePageIndex := 0;
  Screen.Cursor := crDefault;
  SetLength(posPalavras,Length(posPalavras)+1);
  palavra.DS_PALAVRA := '';
  palavra.VR_COR := clBlue;
  palavra.ESTILO := [fsBold];
  //qLogLancIndv.Open;
  HTTPRIO1.HTTPWebNode.ConnectTimeout := 120000; //em milesegundos
  HTTPRIO1.HTTPWebNode.ReceiveTimeout := 120000;
  HTTPRIO1.HTTPWebNode.SendTimeout    := 120000;

end;

procedure TFLancIndiv.Valida;
begin
  if not (IsValidDateTime(edtData.Text)) then
  begin
    msgInf('Informe uma data v�lida.');
    edtData.SetFocus;
    Sysutils.Abort;
  end;
  if (edtDebito.Value <= 0) then
  begin
    msgInf('Informe o valor a ser lan�ado.');
    edtDebito.Enabled := true;
    edtDebito.SetFocus;
    Sysutils.Abort;
  end;
  if ((QConv.FieldByName('cartao_id').AsInteger <= 0) or (not qConv.Active)) then
  begin
    msgInf('Cart�o n�o encontrado.');
    edtCartao.SetFocus;
    Sysutils.Abort;
  end;
  if Trim(edtEmpr.Text) = '' then
  begin
    msgInf('Informe a empresa.');
    edtEmpr.SetFocus;
    Sysutils.Abort;
  end;
  if qEmpr.FieldByName('PROG_DESC').AsString = 'S' then
  begin
    msgInf('Empresa do tipo PROGRAMA DE DESCONTO'+sLineBreak+'N�o � poss�vel pegar autoriza��es.');
    edtEmpr.SetFocus;
    Sysutils.Abort;
  end;
  if Trim(edtEstab.Text) = '' then
  begin
    msgInf('Informe a estabelecimento.');
    edtEstab.SetFocus;
    Sysutils.Abort;
  end;
  if Trim(edtHistorico.Text) = '' then
  begin
    msgInf('Informe o hist�rico do lan�amento.');
    edtHistorico.SetFocus;
    Sysutils.Abort;
  end;
  if Trim(edtNota.Text) = '' then
  begin
    msgInf('Informe o n�mero da Nota Fiscal (NF)');
    edtNota.SetFocus;
    sysutils.Abort;
  end;
  if QEmpr.FieldByName('LIBERADA').AsString <> 'S' then
    if Application.MessageBox(PChar('A empresa '+QEmpr.FieldByName('NOME').AsString+' n�o est� liberada.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then
      Sysutils.Abort;
  if QConv.FieldByName('CONVLIB').AsString <> 'S' then
    if Application.MessageBox(PChar('O titular deste cart�o n�o est� liberado.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then
      Sysutils.Abort;
  if QConv.FieldByName('CARTLIB').AsString <> 'S' then
    if Application.MessageBox(PChar('Este cart�o n�o est� liberado.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then
      Sysutils.Abort;
end;

function TFLancIndiv.venc_fornec: TDateTime;
begin
  Result := EncodeDate(YearOf(edtData.Date),MonthOf(edtData.Date),word(qEstab.FieldByName('VENCIMENTO1').AsInteger));
  if Result <= edtData.Date then
    Result := IncMonth(Result);
end;

procedure TFLancIndiv.edtEstabKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFLancIndiv.edtEmprKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFLancIndiv.edtChapaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFLancIndiv.edtEstabChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEstab.Text) <> '' then begin
     if qEstab.Locate('cred_id',edtEstab.Text,[]) then begin
        cbbEstab.KeyValue := edtEstab.Text;
        edtCnpj.Text := qEstabCGC.AsString;
     end else begin
        cbbEstab.ClearValue;
        AposEmpEstabNull;
     end;
  end
  else begin cbbEstab.ClearValue; AposEmpEstabNull; end;
end;

procedure TFLancIndiv.edtEmprChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEmpr.Text) <> '' then begin
     if qEmpr.Locate('empres_id',edtEmpr.Text,[]) then
        cbbEmpr.KeyValue := edtEmpr.Text
     else begin
        cbbEmpr.ClearValue;
        AposEmpEstabNull;
        cds.First;
        while not cds.Eof do
          cds.Delete;
     end;
  end
  else begin
    cbbEmpr.ClearValue;
    AposEmpEstabNull;
    cds.First;
    while not cds.Eof do
      cds.Delete;
  end;
end;

procedure TFLancIndiv.cbbEstabChange(Sender: TObject);
begin
  inherited;
  edtEstab.Text := cbbEstab.KeyValue;
  edtCartao.OnExit(edtCartao);
end;

procedure TFLancIndiv.cbbEmprChange(Sender: TObject);
begin
  inherited;
  edtEmpr.Text := cbbEmpr.KeyValue;
  edtCartao.OnExit(edtCartao);
end;

procedure TFLancIndiv.edtChapaExit(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clWindow);
  if Trim(edtChapa.Text) <> '' then
  begin
    if Trim(edtEmpr.Text) = '' then
    begin
      msgInf('Informe a empresa.');
      edtEmpr.SetFocus;
      Exit;
    end;
    if not achaConveniado(2) then begin
      edtCartao.Clear;
      edtTitular.Clear;
    end;
  end;
end;

procedure TFLancIndiv.edtTitularKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_return then ButPesqConv.Click;
end;

procedure TFLancIndiv.ButPesqConvClick(Sender: TObject);
begin
  inherited;
  qConv.Close;
  FPesqConv := TFPesqConv.Create(self);
  FPesqConv.FLancIndiv := Self;
  FPesqConv.Tag := 5;
  FPesqConv.Edit1.Text := edtTitular.Text;
  FPesqConv.ShowModal;
  if FPesqConv.ModalResult = mrok then
  begin
    achaConveniado(1);
  end
  else
  begin
    edtChapa.Clear;
    edtTitular.Clear;
    edtCartao.Clear;
    edtTitular.SetFocus;
  end;
  FPesqConv.QConveniados.Close;
  FPesqConv.Free;
  DecimalSeparator := ',';
end;

procedure TFLancIndiv.edtNotaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

Procedure TFLancIndiv.CMDialogKey(Var Msg: TWMKEY);
Begin
  if (ActiveControl = edtTitular) Then
    if Msg.Charcode = VK_TAB Then
    begin
      ButPesqConv.Click;
    end;
  inherited;
End;

procedure TFLancIndiv.pesquisarProduto(Descricao, CodBarras, prog_ids, UF : String);
begin

  F_PesqProdNew := TF_PesqProdNew.Create(Self);
  if prog_ids <> '' then
    F_PesqProdNew.prog_prod := prog_ids;
  F_PesqProdNew.uf := uf;
  if edtEstab.Text <> '' then
    F_PesqProdNew.cred_id := edtEstab.Text
  else
    F_PesqProdNew.cred_id := '-1';
  if Trim(edtDescricao.Text) <> '' then
    F_PesqProdNew.edtBusca.Text := Trim(edtDescricao.Text);
  if (F_PesqProdNew.ShowModal = mrOk) then begin
    preencherCds(F_PesqProdNew.produto);
  end;
end;

procedure TFLancIndiv.FormResize(Sender: TObject);
begin
  inherited;
  btnInclui.Width := GroupBox4.Width div 3;

  btnGravar.Width := GroupBox4.Width div 3;
  btnGravar.Left  := btnInclui.Width;

  btnApaga.Width := GroupBox4.Width div 3;
  btnApaga.Left := (GroupBox4.Width div 3) + btnGravar.Left;
end;

procedure TFLancIndiv.btnApagaClick(Sender: TObject);
begin
  inherited;
  if cds.State in [dsInsert, dsEdit] then begin cds.Cancel; edtCodBarras.SetFocus; end
  else if not cds.IsEmpty then begin cds.Delete; edtCodBarras.SetFocus; end
  else MsgInf('N�o existe registro para ser excluido!');
end;

procedure TFLancIndiv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(dmProgramasDesconto1);
  FreeAndNil(cds);
  if retornoTestes <> nil then
    FreeAndNil(retornoTestes);
end;

procedure TFLancIndiv.cdsAfterPost(DataSet: TDataSet);
begin
  inherited;
  edtDebito.Text := CurrToStr(somaProdutos);
end;

procedure TFLancIndiv.cdsAfterDelete(DataSet: TDataSet);
begin
  inherited;
  edtDebito.Text := CurrToStr(somaProdutos);
end;

function TFLancIndiv.pegarAutorizacao: String;
var aut, Retorno, valor, codigo, digito, get, senha, numAut, digAut, autForm : string;
    ptsFidel, transId, I,J, pIni, pFin : integer;
    Saldo : Currency;
    Fidelidade : Boolean;
    cred : TCredenciado;
    produto : TProduto;
    produtos : TProdutos;
    prescritor : TPrescritor;
    prescredProd : TPrescrProd;
    prescredsProd : TPrescrsProd;
    abrirTransacao : TAbrirTransacao; abrirTrasnacaoRet : TAbrirTransacaoRet;
    validProd : TValidarProdutos; valdProdRet : TValidarProdutosRet;
    FecharTransacao : TFecharTransacao;    FecharTransacaoRet : TFecharTransacaoRet;
    ConfirmarTransacao : TConfirmarTransacao; ConfirmarTransacaoRet : TConfirmarTransacaoRet;
    cancelarTrans : TCancelarTrasnacao;  cancelarTransRet : TCancelarTransacaoRet;
    bm : TBookmark;
    sl : TStrings;
begin
  inherited;
	if (edtDebito.Value > 0) then
	begin
		if Trim(Path_WebService) = '' then begin
  		MsgErro('Caminho do webservice n�o encontrado, favor atualizar suas configura��es.');
	  	Exit;
		end;
    try
      if (DEBUG) then
        sl := TStringList.Create;
		  try
  		ptsFidel:= 0;
      if (edtDebito.Value > 0) then
  	  	valor := IntToStr(Trunc(edtDebito.Value*100)); //passar como inteiro.
		  lblAut.Caption := 'Aguarde! Conectando ao servidor..';
      lblAut.Font.Size := 30;
	  	Application.ProcessMessages;
		  ///MONTANDO XML DE ENVIO (ABRIR_TRANSACAO);
  		cred := TCredenciado.Create(qEstab.FieldByName('CODACESSO').AsInteger, Crypt('D',qEstab.FieldByName('SENHA').AsString,'BIGCOMPRAS'));
      acertarDecimal;
	  	abrirTransacao := TAbrirTransacao.Create(cred,edtCartao.Text, '', 'ADM:'+Operador.Nome,Copy('N',1,1));
      acertarDecimal;
      reAut.Lines.Add('----------------------------------');
      reAut.Lines.Add('INICIO - ' + DateTimeToStr(Now));
		  try
        reAut.Lines.Add('SOLICITANDO TRANSA��O');
        if (DEBUG) then begin
          sl.add('ABRIR TRANSACAO');
          sl.Add(abrirTransacao.getXML);
        end;
		  	Retorno := (HTTPRIO1 as wsconvenioSoap).MAbrirTransacao(abrirTransacao.getXML);
        if (DEBUG) then begin
          sl.Add('RETORNO ABRIR TRANSACAO');
          sl.Add(Retorno);
        end;
  		except
	  		on e:Exception do begin
		  	if not ErroConectarWebService(e.Message) then begin
          reAut.Lines.Add('ERRO AO SOLICITAR TRANSA��O');
				  MsgErro('Erro ao abrir transa��o.'+sLineBreak+
  						'Erro: '+e.Message+sLineBreak+
	  					'Favor entrar em contato com o suporte t�cnico');
				  lblAut.Caption := '';
          edtDebito.Enabled := true;
          if (DEBUG) then begin
            sl.SaveToFile('logXmlWsConvenio.txt');
            sl.Free;
          end;
  				Exit;
	  		end else
		  		Exit;
			  end;
  		end;
	  	abrirTrasnacaoRet := TAbrirTransacaoRet.Create(Retorno);

		  if (abrirTrasnacaoRet.Status = 0) then begin
			  Fidelidade:= abrirTrasnacaoRet.Fidelidade;
  			if Fidelidade then
	  		ptsFidel := abrirTrasnacaoRet.SaldoPontos;
		  	transId := abrirTrasnacaoRet.TransId;
  		end else begin
        reAut.Lines.Add('ERRO AO SOLICITAR TRANSA��O');
        MsgErro('N�o foi poss�vel abrir a transa��o.'+sLineBreak+
			  		'Motivo: '+abrirTrasnacaoRet.Mensagem+sLineBreak);
		  	lblAut.Caption := '';
        edtDebito.Enabled := true;
			  exit;
  		end;

      if (edtDebito.Value > 0) then
    		Retorno := fnRemoveCasasDecimais(edtDebito.Text);

		  bm := cds.GetBookmark;
	    cds.First;
      if (edtDebito.Value > 0) then
        produtos := TProdutos.Create(StrToInt(fnRemoveCasasDecimais(edtDebito.Text)));

    	if existeProdsRec then
 	    prescredsProd := TPrescrsProd.Create;
  	  while not cds.Eof do begin
  	  	produto := TProduto.Create(cdsCodBarras.asString,cdsDescricao.AsString,
	  	  StrToInt(cdsQuantidade.AsString),
 		  	StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',cdsValorUnitario.AsCurrency))),
  		  StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',cdsValorUnitario.AsCurrency))),
  	  	StrToInt(fnRemoveCasasDecimais(Formatfloat('###,##0.00',cdsValorUnitario.AsCurrency))),
	  	  cdsGrupo.AsInteger);
     		produtos.Add(produto);
	    	if not cdsufPrescritor.IsNull then begin
  	    	prescritor := TPrescritor.Create(cdstipoPrescritor.AsInteger,cdsufPrescritor.AsString,cdsNumPrescritor.AsString, cdsNumReceita.AsInteger, cdsdtReceita.AsString);
  	  	  prescredProd := TPrescrProd.Create(cdsCodBarras.asString,prescritor);
 	  	  	prescredsProd.Add(prescredProd);
  		  end;
		    cds.Next;
   		end;
    	cds.GotoBookmark(bm);
	    cds.FreeBookmark(bm);
      acertarDecimal;
    	validProd := TValidarProdutos.Create(cred,edtCartao.Text,'',transId,produtos);
      acertarDecimal;
	    try
		    get := validProd.getXML;
        if (DEBUG) then begin
          sl.Add('VALIDAR PRODUTOS');
          sl.Add(get);
        end;
        reAut.Lines.Add('VALIDANDO TRANSA��O');
		    Retorno := (HTTPRIO1 as wsconvenioSoap).MValidarProdutos(get);
        if (DEBUG) then begin
          sl.Add('RETORNO VALIDAR PRODUTOS');
          sl.Add(Retorno);
        end;
    	except
	    	on e:Exception do
		    begin
   			if not ErroConectarWebService(e.Message) then begin
          reAut.Lines.Add('ERRO AO VALIDAR TRANSA��O');
  	  		MsgErro('Erro ao validar produtos.'+sLineBreak+
	  	  			'Erro: '+e.Message+sLineBreak+
		  	  		'Favor entrar em contato com o suporte t�cnico');
 		  		lblAut.Caption := '';
          edtDebito.Enabled := true;
          if (DEBUG) then begin
            sl.SaveToFile('logXmlWsConvenio.txt');
            sl.Free;
          end;
  		  	Exit;
  	  	end else
	  	  	Exit;
 		  	end;
    	end;
	    valdProdRet := TValidarProdutosRet.Create(Retorno);
 		  if valdProdRet.STATUS = 0 then begin
	  	  if Fidelidade then
  		    ptsFidel := ptsFidel + valdProdRet.PontosTrans;
   			transId := valdProdRet.TransId;
    	end else begin
        reAut.Lines.Add('ERRO AO VALIDAR TRANSA��O');
  		  MsgErro('N�o foi poss�vel validar produtos.'+sLineBreak+
  	  			'Motivo: '+valdProdRet.Mensagem+sLineBreak);
	  	  cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
 		  	try
        if (DEBUG) then begin
          sl.Add('CANCELAR TRANSACAO');
          sl.Add(cancelarTrans.getXML);
        end;
    		Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
        if (DEBUG) then begin
          sl.Add('RETORNO CANCELAR TRANSACAO');
          sl.Add(Retorno);
        end;
        edtDebito.Enabled := true;
        acertarDecimal;
        acertarDecimal;
	    	except
  		    on e:Exception do	begin
  		  	  MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
	  		  	  	'Erro: '+e.Message+sLineBreak);
  	  	  	lblAut.Caption := '';
            edtDebito.Enabled := true;
            if (DEBUG) then begin
              sl.SaveToFile('logXmlWsConvenio.txt');
              sl.Free;
            end;
	  	  	  Exit;
 		  	  end;
    		end;
	    	cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
 		  	if cancelarTransRet.Status <> 0 then
  		  begin
  	  	MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	  	  		'Motivo: '+cancelarTransRet.Mensagem+sLineBreak);
   			lblAut.Caption := '';
        edtDebito.Enabled := true;
    		exit;
	    	end;
  		  lblAut.Caption := '';
 	  		exit;
  	  end;

	  	senha := '1234';

    	if cbbFormaPgto.Enabled then
	    	get := cbbFormaPgto.KeyValue
      else
  	    get := '0';
      reAut.Lines.Add('FECHANDO TRANSA��O');
  		if existeProdsRec then
	  		FecharTransacao := TFecharTransacao.Create(cred,edtCartao.Text,'',transId,senha,
			  StrToInt(get), produtos.ValorTotal,'1234',prescredsProd)
  		else
	  		FecharTransacao := TFecharTransacao.Create(cred,edtCartao.Text,'',transId,senha,
			   StrToInt(get), produtos.ValorTotal,'1234');
  		try
	  		get := FecharTransacao.getXML;
        if (DEBUG) then begin
          sl.Add('FECHAR TRANSACAO');
          sl.Add(get);
        end;
        acertarDecimal;
			  Retorno := (HTTPRIO1 as wsconvenioSoap).MFecharTransacao(get);
        acertarDecimal;
        if (DEBUG) then begin
          sl.Add('RETORNO FECHAR TRANSACAO');
          sl.Add(Retorno);
        end;
		  except
			  on e:Exception do
  			begin
	  		if not ErroConectarWebService(e.Message) then begin
			  	MsgErro('Erro ao validar produtos.'+sLineBreak+
				  		'Erro: '+e.Message+sLineBreak+
					  	'Favor entrar em contato com o suporte t�cnico');
		  		lblAut.Caption := '';
          edtDebito.Enabled := true;
          if (DEBUG) then begin
            sl.SaveToFile('logXmlWsConvenio.txt');
            sl.Free;
          end;
			  	Exit;
  			end else
	  			Exit;
		  	end;
  		end;
	  	FecharTransacaoRet := TFecharTransacaoRet.Create(Retorno);
		  if FecharTransacaoRet.Status = 0 then
  		begin
	  		if FecharTransacaoRet.CupomAut.Count = 0 then
	  	end
		  else
  		begin
		  	cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
        reAut.Lines.Add('ERRO AO FECHAR TRANSA��O');
        MsgErro('Erro ao fechar transa��o.'+sLineBreak+
			     			'Erro: '+ 'Problema na autoriza��o: '+StringReplace(FecharTransacaoRet.Mensagem,''#$A' ',sLineBreak,[rfReplaceAll])+sLineBreak+FormatDateTime('dd/mm/yyyy hh:nn:ss',Now));
  			try
        acertarDecimal;
        acertarDecimal;
        if (DEBUG) then begin
          sl.Add('CANCELAR TRANSACAO');
          sl.Add(cancelarTrans.getXML);
        end;
	  		Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
        if (DEBUG) then begin
          sl.Add('RETORNO CANCELAR TRANSACAO');
          sl.Add(Retorno);
        end;
		  	except
  		  	on e:Exception do	begin
	  		  	if not ErroConectarWebService(e.Message) then begin
		    	  	MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
			    	  		'Erro: '+e.Message+sLineBreak);
		  		    lblAut.Caption := '';
              edtDebito.Enabled := true;
    	  			Exit;
	  		  	end else
	  	  	  	Exit;
  		  	end;
	  		end;
        acertarDecimal;
		  	cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
        acertarDecimal;
			  if cancelarTransRet.Status <> 0 then begin
          MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	    				'Motivo: '+cancelarTransRet.Mensagem+sLineBreak); //se n funcionar eh pq eh RET 1
		  	  lblAut.Caption := '';
          edtDebito.Enabled := true;
  			  exit;
	  		end;
      	lblAut.Caption := '';
        edtDebito.Enabled := true;
	      exit;
  		end;

	  	if edtNota.Text = '' then
		    get := '0'
  		else
	  	  get := edtNota.Text;
      reAut.Lines.Add('CONFIRMANDO TRANSA��O');
		  ConfirmarTransacao:= TConfirmarTransacao.Create(cred,edtCartao.Text,'',transId,StrToInt(get));
  		try
      if (DEBUG) then begin
        sl.Add('CONFIRMAR TRANSACAO');
        sl.Add(ConfirmarTransacao.getXML);
      end;
      acertarDecimal;
		  Retorno := (HTTPRIO1 as wsconvenioSoap).MConfirmarTransacao(ConfirmarTransacao.getXML);
      acertarDecimal;
      if (DEBUG) then begin
        sl.Add('RETORNO CONFIRMAR TRANSACAO');
        sl.Add(Retorno);
      end;

  		except
    		on e:Exception do	begin
		    	if not ErroConectarWebService(e.Message) then begin
  			    MsgErro('Erro ao confirmar transa��o.'+sLineBreak+
	  			    	'Erro: '+e.Message+sLineBreak+
		  			    'Favor entrar em contato com o suporte t�cnico');
  	  		  lblAut.Caption := '';
            edtDebito.Enabled := true;
    	  		Exit;
  	  	  end else
	      	  Exit;
    		end;
	  	end;
  		ConfirmarTransacaoRet := TConfirmarTransacaoRet.Create(Retorno);
	  	if ConfirmarTransacaoRet.Status <> 0 then	begin
       reAut.Lines.Add('ERRO AO CONFIRMAR TRANSA��O');
  		  MsgErro('N�o foi poss�vel confirmar a transa��o.'+sLineBreak+
	  		  	'Motivo: '+ConfirmarTransacaoRet.Mensagem+sLineBreak);
  		  cancelarTrans := TCancelarTrasnacao.Create(cred,transId,Operador.Nome);
    		try
	  	  	Retorno := (HTTPRIO1 as wsconvenioSoap).MCancelarTransacao(cancelarTrans.getXML);
  		  except
    			on e:Exception do	begin
      			if not ErroConectarWebService(e.Message) then begin
		  	    	MsgErro('Erro ao cancelar a transa��o.'+sLineBreak+
			  	    		'Erro: '+e.Message+sLineBreak);
      				lblAut.Caption := '';
              edtDebito.Enabled := true;
		      		Exit;
    	  		end else
		      		Exit;
    			end;
	    	end;
		    cancelarTransRet := TCancelarTransacaoRet.Create(Retorno);
  		  if cancelarTransRet.Status <> 0 then begin
  			  MsgErro('N�o foi poss�vel cancelar a transa��o.'+sLineBreak+
	  			  	'Motivo: '+cancelarTransRet.Mensagem+sLineBreak);
  		  	lblAut.Caption := '';
          edtDebito.Enabled := true;
	  		  exit;
  	  	end;
		    lblAut.Caption := '';
    		exit;
	  	end;
      numAut := DMConexao.ExecuteScalar('SELECT AUTORIZACAO_ID FROM CONTACORRENTE WHERE TRANS_ID = ' + (IntToStr(transId)));
      digAut := DMConexao.ExecuteScalar('SELECT DIGITO FROM CONTACORRENTE WHERE TRANS_ID = ' + (IntToStr(transId)));

      autForm := '';

      J := 0;
      if ((Length(numAut) mod 2) = 0) then
        begin
            while J < Length(numAut) do begin
              begin
              if (J + 2) > Length(numAut) then
                  autForm := autForm + Copy(numAut,J, 1) + ' '
              else
                  autForm := autForm + Copy(numAut,J, 2) + ' ';
              end;
            J := J + 2;
          end
         end
      else
          begin
            autForm := autForm + '0' + Copy(numAut,J, 1) + ' ';
            J := J + 1;
            while J < Length(numAut) do begin
              begin
              if (J + 2) > Length(numAut) then
                  autForm := autForm + Copy(numAut,J, 1) + ' '
              else
                  autForm := autForm + Copy(numAut,J, 2) + ' ';
              end;
            J := J + 2;
          end
      end;

      if  Length(digAut) = 1 then
        autForm := autForm + '0' + digAut + ' '
      else
        autForm := autForm + digAut + ' ';


        //autor = autorizacao.Substring(i, (i + 2 > autorizacao.Length ? 1 : 2));
           // ret += autor.Length == 1 ? "0" + autor : autor + " ";

      //AnsiMidStr
      //if Length(IntToStr(digAut)) = 1 then
      //   aut := DMConexao.ExecuteScalar('select top 1 (concat(autorizacao_id,  right(''00'' + rtrim(digito),2))) as autorid_digito from contacorrente where trans_id = ' + IntToStr(transId))
      //else
      //  aut := DMConexao.ExecuteScalar('select top 1 (CONCAT(autorizacao_id, case when len(digito) = 0 then ''00'' else case when len(digito) = 1 then convert(varchar(1),''0'') + convert(varchar(1),digito) else digito end end)) autorid_digito from contacorrente where trans_id = ' + IntToStr(transId),'');
      //J := 1;
      lblAut.Font.Size := 40;
      lblAut.Caption := 'Autoriza��o: ';
      //while J < Length(aut) do begin
        lblAut.Caption := lblAut.Caption + autForm;
      //  J := J + 2;
      //end;
      reAut.Lines.Add('FIM - ' + DateTimeToStr(Now));
      reAut.Lines.Add('----------------------------------');
		  cds.First;

      btnNovoLanc.SetFocus;
      
  		while not cds.Eof do
	  	cds.Delete;
	  	finally
  		if abrirTransacao <> nil then
	  		FreeAndNil(abrirTransacao);
		  if abrirTrasnacaoRet <> nil then
  			FreeAndNil(abrirTrasnacaoRet);

		  if validProd <> nil then
  			FreeAndNil(validProd);
	  	if valdProdRet <> nil then
  			FreeAndNil(valdProdRet);

	  	if FecharTransacao <> nil then
		  	FreeAndNil(FecharTransacao);
  		if FecharTransacaoRet <> nil then
	  		FreeAndNil(FecharTransacao);

  		if ConfirmarTransacao <> nil then
	  		ConfirmarTransacao.Free;
		  if ConfirmarTransacaoRet <> nil then
			  ConfirmarTransacaoRet.Free;

	  	if cancelarTrans <> nil then
		  	FreeAndNil(cancelarTrans);
  		if cancelarTransRet <> nil then
	  		FreeAndNil(cancelarTransRet);
		  end;
    finally
      if (DEBUG) then begin
        sl.SaveToFile('logXmlWsConvenio.txt');
        sl.Free;
      end;
    end;
	end;
end;


procedure TFLancIndiv.btnPegaAutClick(Sender: TObject);
begin
  inherited;
  Valida;
  if Application.MessageBox('Confirma o lan�amento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    pegarAutorizacao;
  acertarDecimal;
end;

procedure TFLancIndiv.btnPesqProdClick(Sender: TObject);
var prog_desc_ids,uf : string;
begin
  if (cbbEstab.Text = '') then begin
    MsgInf('Selecione um estabelecimento');
    cbbEstab.SetFocus;
  end else if (cbbEmpr.Text = '') then begin
    MsgInf('Selecione uma empresa');
    cbbEmpr.SetFocus;
  end else begin
    if (qEmprPROG_DESC.AsString = 'S') then begin
      prog_desc_ids := ArrayIntegerToString(dmProgramasDesconto1.getProgramasDescontoId(qEmprEMPRES_ID.AsInteger, qEstabCRED_ID.AsInteger));
    end;
    uf := DMConexao.getEstado(cbbEstab.KeyValue);
    pesquisarProduto(edtDescricao.Text, edtCodBarras.Text, prog_desc_ids,uf);
    edtQtd.SetFocus;
  end
end;

procedure TFLancIndiv.cbbReceitaChange(Sender: TObject);
begin
  inherited;
  if TComboBox(Sender).ItemIndex <= 0 then begin
    edtNumPresc.Color := clBtnFace;
    cbTipoPresc.Color := clBtnFace;
    edtNumReceit.Color := clBtnFace;
    cbUF.Color := clBtnFace;
    dtpDtReceita.Color := clBtnFace;
  end else begin
    edtNumPresc.Color := clWindow;
    cbTipoPresc.Color := clWindow;
    edtNumReceit.Color := clWindow;
    cbUF.Color := clWindow;
    dtpDtReceita.Color := clWindow;
  end;
  edtNumPresc.Enabled := TComboBox(Sender).ItemIndex > 0;
  cbTipoPresc.Enabled := TComboBox(Sender).ItemIndex > 0;
  edtNumReceit.Enabled := TComboBox(Sender).ItemIndex > 0;
  cbUF.Enabled := TComboBox(Sender).ItemIndex > 0;
  dtpDtReceita.Enabled := TComboBox(Sender).ItemIndex > 0;
end;

procedure TFLancIndiv.edtCartaoExit(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clWindow);
  if Trim(TEdit(Sender).Text) <> '' then
    if not achaConveniado(3) then begin
      TEdit(Sender).Clear;
      edtTitular.Clear;
    end;
end;

procedure TFLancIndiv.edtQtdChange(Sender: TObject);
begin
  inherited;
  if edtQtd.Text <> '' then begin
    try
      lblValorTotal.Caption := FloatToStr(StrToInt(edtQtd.Text) * cdsValorUnitario.asFloat);
    except
      lblValorTotal.Caption := cdsValorUnitario.AsString;
    end;
  end;
end;

procedure TFLancIndiv.cdsBeforePost(DataSet: TDataSet);
begin
  inherited;
  if cdsID.IsNull or fnVerfCampoVazio('"C�d. barras" obrigat�rio',cdsCodBarras)
  or fnVerfCampoVazio('"Descric�o" Obrigat�rio',cdsDescricao) then Abort;
  if (cbbReceita.ItemIndex > 0) then begin
    if (Trim(cdstipoPrescritor.AsString) = '') then begin
      MsgInf('Tipo Prescritor obrigat�rio');
      edtNumPresc.SetFocus;
      Abort;
    end;
    if (Trim(cdsNumPrescritor.AsString) = '') then begin
      MsgInf('N�m Prescritor obrigat�rio');
      cbTipoPresc.SetFocus;
      Abort;
    end;
    if (Trim(cdsNumReceita.AsString) = '') then begin
      MsgInf('N�m. Receita obrigat�rio');
      edtNumReceit.SetFocus;
      Abort;
    end;
    if (Trim(cdsufPrescritor.AsString) = '') then begin
      MsgInf('UF Prescritor obrigat�rio');
      cbUF.SetFocus;
      Abort;
    end;
    if (Trim(cdsdtReceita.AsString) = '') then begin
      MsgInf('Data da Receita obrigat�rio');
      dtpDtReceita.SetFocus;
      Abort;
    end;
  end;
  cdsValorTotal.AsCurrency := cdsValorUnitario.AsCurrency * cdsQuantidade.AsInteger;
end;

procedure TFLancIndiv.BtnIncluiClick(Sender: TObject);
begin
  cds.Append;
  edtCodBarras.SetFocus;
end;

procedure TFLancIndiv.btnGravarClick(Sender: TObject);
begin
  if (cds.State in [dsEdit, dsInsert]) then begin
    cds.Post;
    edtCodBarras.SetFocus;
  end;
end;

procedure TFLancIndiv.edtDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    btnPesqProd.Click;
end;

procedure TFLancIndiv.dtpDtReceitaExit(Sender: TObject);
begin
  inherited;
  if not IsDate(TDBEdit(Sender).Text) then
    TDBEdit(Sender).Clear;
end;

procedure TFLancIndiv.edtQtdKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if key = '-' then key := #0;
  if key = '.' then key := ',';
end;

procedure TFLancIndiv.zKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_TAB then begin
    if edtData.Enabled then
      edtData.SetFocus
    else
      edtNota.SetFocus;
  end;
end;

procedure TFLancIndiv.cbTipoPrescChange(Sender: TObject);
begin
  inherited;
  cdsNumPrescritor.AsInteger := cbTipoPresc.ItemIndex + 1;
end;

procedure TFLancIndiv.edtCodBarrasExit(Sender: TObject);
var prod : uProdutos.TProduto;
begin
  inherited;
  if Trim(TEdit(Sender).Text) <> '' then
    if Trim(edtDescricao.Text) = '' then begin
      Screen.Cursor := crHourGlass;
      //comentado arianeprod := dmProdutos.getProdutoPorCodBarras(TEdit(Sender).Text);
      if prod <> nil then begin
        preencherCds(prod);
        edtQtd.SetFocus;
      end;
    end;
  Screen.Cursor := crDefault;
end;

procedure TFLancIndiv.preencherCds(prod: uProdutos.TProduto);
var ad : Currency;
begin
  if (cds.IsEmpty) and not (cds.State  in [dsInsert,dsEdit]) then
    cds.Append
  else if not (cds.State in [dsInsert,dsEdit]) then
    cds.edit;
  cdsCodBarras.AsString  := prod.Barras.Barras;
  cdsDescricao.AsString  := prod.Descricao;
  cdsID.AsInteger := prod.Cod;
  cdsGrupo.AsInteger     := prod.Grupo;
  if prod.ObrigaDesconto then begin
    ad := prod.PrecoUnitario;
    cdsValorUnitario.AsFloat  := ArredondaDin(ArredondaDin(ad) - ((ArredondaDin(ad) *  prod.PercentualDesconto)  / 100));
  end else
    cdsValorUnitario.AsFloat  := ArredondaDin(prod.PrecoUnitario);
  if cdsQuantidade.IsNull then
     cdsQuantidade.AsInteger := 1;
  cdsValorTotal.AsFloat := ArredondaDin(cdsValorUnitario.AsFloat) * cdsQuantidade.AsInteger;
end;

procedure TFLancIndiv.cbbReceitaExit(Sender: TObject);
begin
  inherited;
  if (TComboBox(Sender).ItemIndex = 0) then begin
    if (cds.State in [dsEdit,dsInsert]) then
      btnGravar.Click
    else
      edtDebito.SetFocus;
  end else begin
  end;
end;

procedure TFLancIndiv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    edtCodBarras.SetFocus
  else if Key = VK_F5 then
    btnInclui.Click
  else if Key = VK_F7 then
    btnGravar.Click
  else if Key = VK_F6 then
    btnApaga.Click
  else if Key = VK_F12 then
    btnPegaAut.Click
  else if Key = VK_F9 then
    btnNovoLanc.Click
  else
    inherited;
end;

procedure TFLancIndiv.tsPrincipalShow(Sender: TObject);
begin
  inherited;
  edtEstab.SetFocus;
end;

procedure TFLancIndiv.btnNovoLancClick(Sender: TObject);
var I : Integer;
begin
  inherited;
  edtEstab.Clear;
  //cbbEstab.ClearValue;

  edtEmpr.Clear;
  //cbbEmpr.ClearValue;

  edtCartao.Clear;
  edtChapa.Clear;
  edtTitular.Clear;
  edtSaldo.Clear;

  lblCodProd.Caption := '';
  edtCodBarras.Clear;
  edtDescricao.Clear;
  edtQtd.Clear;
  lblPreco.Caption := '';
  lblValorTotal.Caption := '';
  cbbReceita.ItemIndex := 0;
  edtNumPresc.Clear;
  cbTipoPresc.ItemIndex := -1;
  edtNumReceit.Clear;
  cbUf.ItemIndex := -1;
  dtpDtReceita.Clear;
  edtDebito.Enabled := true;
  for I := 0 to cds.RecordCount -1 do begin
    cds.Delete;
  end;

  edtData.Date := now;
  edtNota.Clear;
  //cbEntrega.ItemIndex := 0;
  edtDebito.Clear;
  edtHistorico.Clear;
  edtDebito.Text := '0,00';
  reAut.Clear;
  edtCnpj.Text := '';
  lblAut.Caption := '';
  edtEmpr.ReadOnly := False;
  cbbEmpr.ReadOnly := False;
  edtEstab.SetFocus;
end;

procedure TFLancIndiv.mudarCorComp(Obj: TObject; cor: TColor);
begin
       if (Obj is TEdit) then begin TEdit(Obj).Color := cor; end
  else if (Obj is TComboBox) then begin TComboBox(Obj).Color := cor; end
  else if (Obj is TCurrencyEdit) then begin TCurrencyEdit(Obj).Color := cor; end
  else if (Obj is TDBLookupComboBox) then begin TDBLookupComboBox(Obj).Color := cor; end;
end;

procedure TFLancIndiv.edtEstabEnter(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clMoneyGreen);
end;

procedure TFLancIndiv.edtEstabExit(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clWindow);
end;

procedure TFLancIndiv.edtCnpjExit(Sender: TObject);
var cred_id : Integer;
begin
  inherited;
  mudarCorComp(Sender,clWindow);
  if (Trim(TEdit(Sender).Text) <> '') then begin
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add(' SELECT TOP 1 CRED_ID FROM CREDENCIADOS WHERE APAGADO <> ''S'' AND CGC = ' + quotedStr(edtCnpj.Text));
    DMConexao.AdoQry.Open;
    if DMConexao.AdoQry.Fields[0].AsString <> '' then
      cbbEstab.KeyValue := DMConexao.AdoQry.Fields[0].AsString;
  end;
end;



procedure TFLancIndiv.edtDebitoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = '.' then Key := ',';
end;

procedure TFLancIndiv.edtDebitoEnter(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clMoneyGreen);
end;

procedure TFLancIndiv.edtDebitoExit(Sender: TObject);
begin
  inherited;
  mudarCorComp(Sender,clWindow);
  if (edtDebito.Value <= 0) then
    edtDebito.Enabled := true
  else
    edtDebito.Enabled := false;
end;



end.
