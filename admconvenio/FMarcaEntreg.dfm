object F_MarcaEntreg: TF_MarcaEntreg
  Left = 340
  Top = 218
  ActiveControl = EDNF
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Informa'#231#245'es adicionais nos lan'#231'amentos'
  ClientHeight = 312
  ClientWidth = 420
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 206
    Top = 8
    Width = 43
    Height = 13
    Caption = 'Entregue'
  end
  object Label13: TLabel
    Left = 77
    Top = 37
    Width = 248
    Height = 13
    AutoSize = False
    Caption = 'LabEmpresa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 13
    Top = 37
    Width = 57
    Height = 13
    Caption = 'Fornecedor:'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 420
    Height = 312
    Align = alClient
    TabOrder = 0
    object Bevel1: TBevel
      Left = 1
      Top = 262
      Width = 368
      Height = 2
    end
    object Label2: TLabel
      Left = 9
      Top = 85
      Width = 53
      Height = 13
      Caption = 'Nota Fiscal'
    end
    object Label3: TLabel
      Left = 229
      Top = 85
      Width = 60
      Height = 13
      Caption = 'NF Entregue'
    end
    object Label5: TLabel
      Left = 120
      Top = 85
      Width = 52
      Height = 13
      Caption = 'C/ Receita'
    end
    object Label1: TLabel
      Left = 229
      Top = 129
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object Label6: TLabel
      Left = 9
      Top = 126
      Width = 31
      Height = 13
      Caption = 'D'#233'bito'
    end
    object Label7: TLabel
      Left = 120
      Top = 129
      Width = 33
      Height = 13
      Caption = 'Cr'#233'dito'
    end
    object Label8: TLabel
      Left = 9
      Top = 217
      Width = 41
      Height = 13
      Caption = 'Hist'#243'rico'
    end
    object Bevel2: TBevel
      Left = 4
      Top = 6
      Width = 330
      Height = 73
    end
    object Label9: TLabel
      Left = 12
      Top = 13
      Width = 32
      Height = 13
      Caption = 'Titular:'
    end
    object Label10: TLabel
      Left = 12
      Top = 45
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LabTitular: TLabel
      Left = 82
      Top = 13
      Width = 248
      Height = 13
      AutoSize = False
      Caption = 'LabTitular'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabEmpresa: TLabel
      Left = 82
      Top = 45
      Width = 248
      Height = 13
      AutoSize = False
      Caption = 'LabEmpresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LabNomeCartao: TLabel
      Left = 82
      Top = 29
      Width = 248
      Height = 13
      AutoSize = False
      Caption = 'LabNomeCartao'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 12
      Top = 29
      Width = 65
      Height = 13
      Caption = 'Nome Cart'#227'o:'
    end
    object Label15: TLabel
      Left = 12
      Top = 62
      Width = 53
      Height = 13
      Caption = 'Estabelec.:'
    end
    object LabFornec: TLabel
      Left = 82
      Top = 62
      Width = 248
      Height = 13
      AutoSize = False
      Caption = 'LabFornec'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 9
      Top = 173
      Width = 69
      Height = 13
      Caption = 'N'#250'm Prescritor'
    end
    object Label16: TLabel
      Left = 121
      Top = 173
      Width = 63
      Height = 13
      Caption = 'Data Receita'
    end
    object Label17: TLabel
      Left = 81
      Top = 173
      Width = 37
      Height = 13
      Caption = 'Receita'
    end
    object Label18: TLabel
      Left = 266
      Top = 173
      Width = 63
      Height = 13
      Caption = 'Tp. Prescritor'
    end
    object lblUF: TLabel
      Left = 220
      Top = 173
      Width = 14
      Height = 13
      Caption = 'UF'
    end
    object BitBtn1: TBitBtn
      Left = 157
      Top = 274
      Width = 77
      Height = 29
      Caption = '&OK'
      TabOrder = 13
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 241
      Top = 274
      Width = 77
      Height = 29
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 14
      NumGlyphs = 2
    end
    object CBReceita: TComboBox
      Left = 120
      Top = 101
      Width = 100
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = 'Sim'
      OnChange = CBReceitaChange
      OnKeyDown = EDNFKeyDown
      Items.Strings = (
        'Sim'
        'N'#227'o')
    end
    object EDNF: TEdit
      Left = 9
      Top = 101
      Width = 100
      Height = 21
      TabOrder = 0
      OnKeyDown = EDNFKeyDown
      OnKeyPress = EDNFKeyPress
    end
    object EdHist: TEdit
      Left = 9
      Top = 233
      Width = 320
      Height = 21
      TabStop = False
      CharCase = ecUpperCase
      Color = clBtnFace
      MaxLength = 50
      ReadOnly = True
      TabOrder = 12
    end
    object CBNFEntregue: TJvComboBox
      Left = 229
      Top = 101
      Width = 100
      Height = 21
      Style = csDropDownList
      TabOrder = 2
      Text = 'Sim'
      OnKeyDown = EDNFKeyDown
      Items.Strings = (
        'Sim'
        'N'#227'o')
      ItemIndex = 0
    end
    object edtNumPrescritor: TEdit
      Left = 9
      Top = 189
      Width = 66
      Height = 21
      TabOrder = 7
      OnKeyDown = EDNFKeyDown
      OnKeyPress = EDNFKeyPress
    end
    object cbTpPres: TJvComboBox
      Left = 270
      Top = 189
      Width = 60
      Height = 21
      Style = csDropDownList
      TabOrder = 10
      OnKeyDown = EDNFKeyDown
      Items.Strings = (
        'CRM'
        'CRO')
    end
    object edtNumReceita: TEdit
      Left = 80
      Top = 189
      Width = 37
      Height = 21
      TabOrder = 8
      OnKeyDown = EDNFKeyDown
      OnKeyPress = EDNFKeyPress
    end
    object cbUF_OLD: TJvComboBox
      Left = 355
      Top = 221
      Width = 46
      Height = 21
      Style = csDropDownList
      TabOrder = 11
      Visible = False
      OnKeyDown = EDNFKeyDown
      Items.Strings = (
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PR'
        'PB'
        'PA'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SE'
        'SP'
        'TO')
    end
    object debito: TJvValidateEdit
      Left = 8
      Top = 144
      Width = 100
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      EditText = '000'
      TabOrder = 4
      OnKeyPress = debitoKeyPress
    end
    object credito: TJvValidateEdit
      Left = 120
      Top = 144
      Width = 100
      Height = 21
      CriticalPoints.MaxValueIncluded = False
      CriticalPoints.MinValueIncluded = False
      DisplayFormat = dfFloat
      DecimalPlaces = 2
      TabOrder = 5
      OnKeyPress = creditoKeyPress
    end
    object data: TJvDateEdit
      Left = 229
      Top = 142
      Width = 99
      Height = 21
      ReadOnly = True
      ShowNullDate = False
      TabOrder = 3
      OnKeyPress = dataKeyPress
    end
    object edtDtReceita: TJvDateEdit
      Left = 121
      Top = 188
      Width = 91
      Height = 21
      ShowNullDate = False
      TabOrder = 6
      OnEnter = edtDtReceitaEnter
      OnExit = edtDtReceitaExit
      OnKeyPress = edtDtReceitaKeyPress
    end
    object cbUF: TDBLookupComboBox
      Left = 220
      Top = 189
      Width = 47
      Height = 21
      KeyField = 'ESTADO_ID'
      ListField = 'UF'
      ListSource = DSEstado
      TabOrder = 9
    end
  end
  object QEstado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT ESTADO_ID, UF FROM ESTADOS'
      'ORDER BY UF')
    Left = 336
    Top = 144
  end
  object DataSource1: TDataSource
    Left = 65512
    Top = 16
  end
  object DSEstado: TDataSource
    DataSet = QEstado
    Left = 336
    Top = 184
  end
end
