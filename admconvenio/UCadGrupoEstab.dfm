inherited FCadGrupoEstab: TFCadGrupoEstab
  Left = 90
  Top = 95
  Caption = 'Cadastro de Grupo de Estabelecimentos'
  ClientWidth = 800
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 800
    ActivePage = TabFicha
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 792
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 709
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 792
        Columns = <
          item
            Expanded = False
            FieldName = 'GRUPO_ESTAB_ID'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Width = 792
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Width = 792
      end
      inherited Panel3: TPanel
        Width = 792
        object Label3: TLabel
          Left = 24
          Top = 24
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 80
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = dbEdtDesc
        end
        object DBEdit1: TDBEdit
          Left = 24
          Top = 40
          Width = 49
          Height = 21
          TabStop = False
          Color = clBtnFace
          DataField = 'GRUPO_ESTAB_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 0
        end
        object dbEdtDesc: TDBEdit
          Left = 80
          Top = 40
          Width = 618
          Height = 21
          CharCase = ecUpperCase
          DataField = 'DESCRICAO'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object GroupBox1: TGroupBox
          Left = 2
          Top = 403
          Width = 788
          Height = 72
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 2
          object Label5: TLabel
            Left = 24
            Top = 20
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
            FocusControl = DBEdit3
          end
          object Label6: TLabel
            Left = 157
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit4
          end
          object DBEdit3: TDBEdit
            Left = 24
            Top = 36
            Width = 113
            Height = 21
            DataField = 'DTCADASTRO'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 160
            Top = 36
            Width = 161
            Height = 21
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            TabOrder = 1
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 792
      end
      inherited GridHistorico: TJvDBGrid
        Width = 792
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 800
  end
  inherited panStatus2: TPanel
    Width = 800
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from grupo_estab where grupo_estab_id = 0')
    object QCadastroGRUPO_ESTAB_ID: TIntegerField
      FieldName = 'GRUPO_ESTAB_ID'
    end
    object QCadastroDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 45
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 45
    end
  end
end
