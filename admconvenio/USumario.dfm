object FSumario: TFSumario
  Left = 264
  Top = 211
  Width = 699
  Height = 303
  ActiveControl = Button1
  BorderIcons = []
  Caption = 'Visualiza'#231#227'o do Filtro'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 666
    Height = 225
    Align = alTop
    DataSource = FFiltro.DSDetalhes
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CONJUNCAO'
        Title.Caption = 'Conjun'#231#227'o'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FIELD_NAME'
        Title.Caption = 'Campo do cadastro'
        Width = 191
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUMARIO'
        Title.Caption = 'Compara'#231#227'o'
        Width = 399
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 225
    Width = 666
    Height = 51
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 331
      Top = 14
      Width = 79
      Height = 27
      Caption = '&Ok'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
end
