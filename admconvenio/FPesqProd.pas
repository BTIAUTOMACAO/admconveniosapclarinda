unit FPesqProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, FEntregaNF, Menus, DB;

type
  TF_PesqProd = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    PopupMenu1: TPopupMenu;
    Incluir1: TMenuItem;
    Panel2: TPanel;
    Label1: TLabel;
    edtBusca: TEdit;
    cbCampos: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Incluir1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
    VALOR_ANTES_DA_BUSCA : String;
    F_EntregaNF: TF_EntregaNF;
  end;

var
  F_PesqProd: TF_PesqProd;

implementation

uses UCadProduto, URotinasGrids;

{$R *.dfm}

procedure TF_PesqProd.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
    if not F_EntregaNF.QProdutos.IsEmpty then
      ModalResult := mrOk;
  if key = vk_escape then Close;
end;

procedure TF_PesqProd.DBGrid1DblClick(Sender: TObject);
begin
  if not F_EntregaNF.QProdutos.IsEmpty then
    ModalResult := mrOk;
end;

procedure TF_PesqProd.Incluir1Click(Sender: TObject);
begin
F_EntregaNF.CriaFrmCadProduto;
{  FCadProduto := TFCadProduto.create(self);
  FCadProduto.ShowModal;
  if FCadProduto.ModalResult = mrOk then begin
    begin
    if not F_EntregaNF.Produtos.Active then
      F_EntregaNF.Produtos.Open;
    F_EntregaNF.Produtos.Refresh;
    end;
  end;
  FCadProduto.Free;}
end;

procedure TF_PesqProd.FormCreate(Sender: TObject);
var I : Integer;
begin
for I := 0 to DBGrid1.Columns.Count - 1 do
  cbCampos.Items.Add(dbGrid1.columns[I].Title.Caption);
cbCampos.ItemIndex := 0;
end;

procedure TF_PesqProd.edtBuscaKeyPress(Sender: TObject; var Key: Char);
begin
if TEdit(Sender).Text = '' then TEdit(Sender).Color := clWindow
else
  begin
  if F_EntregaNF.QProdutos.Locate(BuscaNomeFieldpeloTitulo(cbcampos.Text,dbGrid1),TEdit(Sender).Text,[loPartialKey]) then
    begin
    TEdit(Sender).Color := clMoneyGreen;
    end
  else
    begin
    TEdit(Sender).Color := clRed;
    Beep;
    end;
  end;
end;

procedure TF_PesqProd.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
TDbGrid(Sender).Canvas.font.Color:= clBlack; //aqui � definida a cor da fonte
if gdSelected in State then
  with (Sender as TDBGrid).Canvas do
    begin
    Brush.Color := clMoneyGreen; //aqui � definida a cor do fundo
    FillRect(Rect);
    end;
TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);
end;

end.
