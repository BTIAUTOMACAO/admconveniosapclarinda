unit dmProgramasDesconto;

interface

uses
  SysUtils, Classes, DB, uProgramaDesconto,
  UTipos, uProdutos, ADODB;


type
  TdmProgramasDesconto1 = class(TDataModule)
    DSProgramaDesconto: TDataSource;
    QProgramaDesconto: TADOQuery;
    QProgramaDescontoprog_id: TIntegerField;
    QProgramaDescontodt_inicio: TDateTimeField;
    QProgramaDescontodt_fim: TDateTimeField;
    QProgramaDescontoverifica_valor: TStringField;
    QProgramaDescontoempres_id: TIntegerField;
    QProgramaDescontocred_id: TIntegerField;
    QProgramaDescontoobriga_desconto: TStringField;
  private
    { Private declarations }
  public
    function getProgramasDescontoId(Empresa, Estabelecimento : Integer) : TIntegerArray;
    function getProgramasDesconto(Empresa, Estabelecimento : Integer) : TProgramasDesconto;
    { Public declarations }
  end;

var
  dmProgramasDesconto1: TdmProgramasDesconto1;

implementation

uses DM;

{$R *.dfm}

{ TdmProgramasDesconto1 }

function TdmProgramasDesconto1.getProgramasDesconto(Empresa,
  Estabelecimento: Integer): TProgramasDesconto;
var rn : Integer;
begin
  QProgramaDesconto.Close;
  QProgramaDesconto.SQL.Strings[1] := ' p.prog_id, p.dt_inicio, p.dt_fim, p.verifica_valor, pe.empres_id, pc.cred_id, pc.obriga_desconto ';
  QProgramaDesconto.Parameters.ParamByName('empres_id').Value := Empresa;
  QProgramaDesconto.Parameters.ParamByName('cred_id').Value   := estabelecimento;
  QProgramaDesconto.Open;
  SetLength(Result,QProgramaDesconto.RecordCount);
  while not QProgramaDesconto.Eof do begin
     rn := QProgramaDesconto.RecNo;
     Result[rn].Prog_Id         := QProgramaDesconto.FieldByName('PROG_ID').AsInteger;
     Result[rn].Nome            := QProgramaDesconto.FieldByName('NOME').AsString;
     Result[rn].Dt_Inicio       := QProgramaDesconto.FieldByName('DT_INICIO').AsDateTime;
     Result[rn].Dt_Fim          := QProgramaDesconto.FieldByName('DT_FIM').AsDateTime;
     Result[rn].Verfica_Valor   := QProgramaDesconto.FieldByName('VERIFICA_VALOR').AsString;
     Result[rn].Empres_Id       := QProgramaDesconto.FieldByName('EMPRES_ID').AsInteger;
     Result[rn].Cred_Id         := QProgramaDesconto.FieldByName('CRED_ID').AsInteger;
     Result[rn].Obriga_Desconto := UpperCase(QProgramaDesconto.FieldByName('OBRIGA_DESCONTO').AsString) = 'S';
     QProgramaDesconto.Next;
  end;
  QProgramaDesconto.Close;
end;

function TdmProgramasDesconto1.getProgramasDescontoId(Empresa,
  Estabelecimento: Integer): TIntegerArray;
var rn : Integer;
begin
  QProgramaDesconto.Close;
  QProgramaDesconto.SQL.Strings[1] := ' p.prog_id ';
  QProgramaDesconto.Parameters.ParamByName('empres_id').Value := Empresa;
  QProgramaDesconto.Parameters.ParamByName('cred_id').Value   := estabelecimento;
  QProgramaDesconto.Open;
  SetLength(Result,QProgramaDesconto.RecordCount);
  while not QProgramaDesconto.Eof do begin
     rn := qProgramaDesconto.RecNo;
     Result[rn-1] := QProgramaDesconto.FieldByName('PROG_ID').AsInteger;
     QProgramaDesconto.Next;
  end;
  QProgramaDesconto.Close;
end;

end.
