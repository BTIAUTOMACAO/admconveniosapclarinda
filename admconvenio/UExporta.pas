unit UExporta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFExporta = class(TForm)
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    EdChar: TEdit;
    Label1: TLabel;
    RGDet: TRadioGroup;
    CKAspas: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    ckprimlinha: TCheckBox;
    Bevel1: TBevel;
    edmaskdata: TEdit;
    lb: TLabel;
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FExporta: TFExporta;

implementation

{$R *.dfm}

procedure TFExporta.CheckBox1Click(Sender: TObject);
begin
EdChar.Visible := CheckBox1.Checked;
label1.Visible := EdChar.Visible;
end;

end.
