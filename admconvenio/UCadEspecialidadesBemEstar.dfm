inherited FCadEspecialidadesBemEstar: TFCadEspecialidadesBemEstar
  Left = 321
  Top = 75
  Caption = 'Cadastro de Especialidades'
  ClientHeight = 601
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Height = 560
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 441
        inherited Label2: TLabel
          Width = 66
          Caption = '&Especialidade'
        end
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
      end
      inherited DBGrid1: TJvDBGrid
        Height = 441
        Columns = <
          item
            Expanded = False
            FieldName = 'ESPECIALIDADE_ID'
            Title.Caption = 'Cod Especialidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Title.Caption = 'Liberado'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 513
        Panels = <
          item
            Width = 50
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 500
      end
      inherited Panel3: TPanel
        Height = 500
        object GroupBox2: TGroupBox
          Left = 2
          Top = 2
          Width = 818
          Height = 87
          Align = alTop
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 15
            Width = 80
            Height = 13
            Caption = 'Especialidade ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 105
            Top = 15
            Width = 58
            Height = 13
            Caption = 'Descri'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblServico: TLabel
            Left = 505
            Top = 15
            Width = 44
            Height = 13
            Caption = 'Servi'#231'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBEdit1: TDBEdit
            Left = 10
            Top = 30
            Width = 79
            Height = 21
            Hint = 'C'#243'digo do conveniado'
            TabStop = False
            Color = clBtnFace
            DataField = 'ESPECIALIDADE_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBCheckBox1: TDBCheckBox
            Left = 9
            Top = 65
            Width = 136
            Height = 17
            Hint = 'Titular liberado para compra'
            Caption = 'Liberado'
            DataField = 'LIBERADO'
            DataSource = DSCadastro
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object DBEdit2: TDBEdit
            Left = 104
            Top = 32
            Width = 377
            Height = 21
            DataField = 'DESCRICAO'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object jvdbSERV_ID: TJvDBLookupCombo
            Left = 504
            Top = 32
            Width = 281
            Height = 21
            Hint = 'Escolha o servi'#231'o da especialidade'
            DeleteKeyClear = False
            DataField = 'SERV_ID'
            DataSource = DSCadastro
            LookupField = 'SERV_ID'
            LookupDisplay = 'DESCRICAO'
            LookupSource = dsServico
            TabOrder = 3
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited GridHistorico: TJvDBGrid
        Height = 485
      end
    end
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterRefresh = QCadastroAfterRefresh
    SQL.Strings = (
      'SELECT * FROM ESPECIALIDADES WHERE ESPECIALIDADE_ID = 0')
    object QCadastroESPECIALIDADE_ID: TIntegerField
      FieldName = 'ESPECIALIDADE_ID'
    end
    object QCadastroDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroSERV_ID: TIntegerField
      FieldName = 'SERV_ID'
    end
  end
  object dsServico: TDataSource
    DataSet = QServico
    Left = 532
    Top = 193
  end
  object QServico: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT SERV_ID, DESCRICAO FROM SERVICO_BEM_ESTAR')
    Left = 532
    Top = 155
    object QServicoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 30
    end
    object QServicoSERV_ID: TIntegerField
      FieldName = 'SERV_ID'
    end
  end
end
