unit URelAutorizacoesDesconto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, Grids, DBGrids, {JvDBCtrl, ClipBrd,}
  StdCtrls, ComCtrls, Mask, JvToolEdit, DB, {JvMemDS,} ZAbstractRODataset,
  ZDataset, ToolEdit, JvCombobox, ClassImpressao, JvMemoryDataset,
  JvExDBGrids, JvDBGrid, JvExMask, frxClass, frxGradient, frxDBSet,
  ZAbstractDataset, Printers, DBClient, frxExportPDF, ShellApi, ADODB,
  JvExControls, JvDBLookup, RxMemDS, DBCtrls, ActiveX, frxRich;

type
    TFRelAutorizacoesDesconto = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    DataIni: TJvDateEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    JvDBGrid1: TJvDBGrid;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    but_MarcDem_For: TButton;
    But_MarcaT_For: TButton;
    But_DesmT_For: TButton;
    Panel3: TPanel;
    but_Abre_For: TButton;
    But_Fecha_For: TButton;
    DBGrid1: TJvDBGrid;
    TabConv: TTabSheet;
    Panel1: TPanel;
    ButMarcDesmConv: TButton;
    ButMarcaTodosConv: TButton;
    ButDesmTodosConv: TButton;
    BitBtn1: TBitBtn;
    datafin: TJvDateEdit;
    Panel4: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    ChKMostraChapa: TCheckBox;
    Bevel3: TBevel;
    lblEntregaNF: TLabel;
    pnlOutrasOpcoes: TPanel;
    ckNaoAgruparPorEmpresa: TCheckBox;
    ckQuebraPagina: TCheckBox;
    CKHistAuts: TCheckBox;
    Panel5: TPanel;
    Label14: TLabel;
    lblTotal: TLabel;
    DSLotesCartao: TDataSource;
    bntGerarPDF: TBitBtn;
    DBEmpresa: TJvDBLookupCombo;
    GroupBox2: TGroupBox;
    QLotesCartao: TADOQuery;
    frxTermoRecebimento: TfrxReport;
    dbLote: TfrxDBDataset;
    BitBtn2: TBitBtn;
    dbEmpresas: TfrxDBDataset;
    QEmpresas: TADOQuery;
    dsEmpresas: TDataSource;
    QEmpresasnome: TStringField;
    QEmpresasfantasia: TStringField;
    QEmpCombo: TADOQuery;
    dsEmpCombo: TDataSource;
    QEmpresasendereco: TStringField;
    QCartao: TADOQuery;
    dsCartao: TDataSource;
    dbCartao: TfrxDBDataset;
    frxAutorizacao: TfrxReport;
    QEmpresastipo_credito: TIntegerField;
    QEmpresasempres_id: TIntegerField;
    QEmpresasDESCRICAO: TStringField;
    QEmpresasmod_cart_id: TIntegerField;
    QEmpresasempres_fornece_cartao: TStringField;
    QEmpresasCOLUMN1: TStringField;
    GroupBox3: TGroupBox;
    rbTermo: TRadioButton;
    rbAutorizacao: TRadioButton;
    QEmpComboempres_id: TIntegerField;
    QEmpCombonome: TStringField;
    QEmpCombomod_cart_id: TIntegerField;
    Label1: TLabel;
    EdEmp_ID: TEdit;
    BitBtn3: TBitBtn;
    sd: TSaveDialog;
    frxPermitMsgCelular: TfrxReport;
    rbEnviaSMS: TRadioButton;
    QCartaochapa: TFloatField;
    QCartaocodcartimp: TStringField;
    QCartaoseq_lote: TIntegerField;
    QCartaotitular: TStringField;
    QCartaoCELULAR: TStringField;
    QCartaodependente: TStringField;
    QCartaoempres_id: TIntegerField;
    QCartaonome: TStringField;
    QCartaotipo_credito: TIntegerField;
    QCartaodescricao: TStringField;
    QCartaoFANTASIA: TStringField;
    QCartaoendereco: TStringField;
    QCartaoempres_fornece_cartao: TStringField;
    QCartaoCOLUMN1: TStringField;
    QCartaoassinatura: TStringField;
    QCartaosetor: TStringField;
    QCartaosetor_id: TIntegerField;
    QLotesCartaoseq_lote: TIntegerField;
    QLotesCartaoDATA: TWideStringField;
    QLotesCartaonome_arq: TStringField;
    QLotesCartaofantasia: TStringField;
    QLotesCartaoEMPRES_ID: TIntegerField;
    QLotesCartaoDESCRICAO: TStringField;
    MLotes: TJvMemoryData;
    MLotesseq_lote: TIntegerField;
    MLotesempres_id: TIntegerField;
    MLotesmarcado: TBooleanField;
    MLotesdata: TDateTimeField;
    MLotesfantasia: TStringField;
    MLotessetor: TStringField;
    MLotesnome_arq: TStringField;
    chkOrdenaSetor: TCheckBox;
    grpOrdena: TGroupBox;
    GroupBox4: TGroupBox;
    lkpModeloCartao: TJvDBLookupCombo;
    QModelos: TADOQuery;
    DSModelo: TDataSource;
    QModelosMOD_CART_ID: TIntegerField;
    QModelosDESCRICAO: TStringField;
    QModelosOBSERVACAO: TStringField;
    QModelosAPAGADO: TStringField;
    QModelosDTAPAGADO: TDateTimeField;
    QModelosDTALTERACAO: TDateTimeField;
    QModelosDTCADASTRO: TDateTimeField;
    QModelosOPERADOR: TStringField;
    QModelosOPERCADASTRO: TStringField;
    frxEtiqueta: TfrxReport;
    rbEtiqueta: TRadioButton;
    ckbImprimeIndividual: TCheckBox;
    frxAutorizacaoModeloMilClean: TfrxReport;
    frxTermoModeloMilClean: TfrxReport;
    frxBemEstar: TfrxReport;
    chkBemEstar: TCheckBox;
    frxrchbjct1: TfrxRichObject;
    QCartaocgc: TStringField;
    frxBemEstar2: TfrxReport;
    frxBemEstar3: TfrxReport;
    frxBemEstarISINDSERV: TfrxReport;
    chkItaqua: TCheckBox;
    FrxAutorizacao2430: TfrxReport;
    frxBemEstarItaquaECunha: TfrxReport;
    frxTermoRecebimentoBemEstar: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    frxBemEstarRPC: TfrxReport;
    frxBemEstarRPC1: TfrxReport;
    frxDrogaBellaBemEstar: TfrxReport;
    procedure ButListaEmpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButMarcaDesmEmpClick(Sender: TObject);
    procedure ButDesmarcaTodosEmpClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure carregaDataSet;
    procedure ButMarcaTodasEmpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure chempresaKeyPress(Sender: TObject; var Key: Char);
    procedure EdEmp_IDKeyPress(Sender: TObject; var Key: Char);
    procedure EdEmp_IDChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure frxAutorizacaoClosePreview(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure frxAutorizacaoBeforePrint(Sender: TfrxReportComponent);
    procedure frxPermitMsgCelularBeforePrint(Sender: TfrxReportComponent);
    procedure frxTermoRecebimentoBeforePrint(Sender: TfrxReportComponent);
  private
    { Private declarations }
    campo_ord : string;
    desc : Boolean;
    campo_ord2 : string;
    desc2 : Boolean;
    linha : integer;
    lpage : integer;
    empres_sel : String;
    bVisualizaTit, filtrarEntrNF, acumularAutor : Boolean;
    procedure BuscaLotes;


  public
    { Public declarations }
    sl : TStrings;
    fornec_sel, conv_sel, conveniados : String;
    MostrarRelatorio : Boolean;
    FiltraPorSetor :  Boolean;
    Marcados : TStringList;
    myList   : TList;
    lastRec  : boolean;
    recConv  : Integer;
    qtdConv  : Integer;
    procedure Empresas_Sel;
    //procedure carregarDataSet;

  end;

var
  FRelAutorizacoesDesconto: TFRelAutorizacoesDesconto;
  flagIsPDF : Boolean = False;

implementation

uses DM, impressao, Math, cartao_util;

{$R *.dfm}


procedure TFRelAutorizacoesDesconto.ButListaEmpClick(Sender: TObject);
var aux : TDateTime;
begin
  inherited;
  BuscaLotes;
end;

procedure TFRelAutorizacoesDesconto.FormCreate(Sender: TObject);
begin
  inherited;
  QEmpCombo.Open;

end;

procedure TFRelAutorizacoesDesconto.ButMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  if MLotes.IsEmpty then Exit;
    MLotes.Edit;
    MLotesMarcado.AsBoolean := not MLotesMarcado.AsBoolean;
    MLotes.Post;
    Empresas_Sel;
end;

procedure TFRelAutorizacoesDesconto.Empresas_Sel;
var marca : TBookmark;
begin
  empres_sel := EmptyStr;
  MLotes.DisableControls;
  marca := MLotes.GetBookmark;
  MLotes.First;
  while not MLotes.eof do begin
    if MLotesMarcado.AsBoolean then empres_sel := empres_sel + ','+MLotesempres_id.AsString;
    MLotes.Next;
  end;
  //if empres_sel <> '' then empres_sel := Copy(empres_sel,2,Length(empres_sel));
  empres_sel := Copy(empres_sel,2,Length(empres_sel));
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  MLotes.EnableControls;
end;

procedure TFRelAutorizacoesDesconto.ButDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
   if MLotes.IsEmpty then Exit;
  MLotes.DisableControls;
  marca := MLotes.GetBookmark;
  MLotes.First;
  while not MLotes.eof do begin
    MLotes.Edit;
    MLotesMarcado.AsBoolean := false;
    MLotes.Post;
    MLotes.Next;
  end;
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  MLotes.EnableControls;
  Empresas_Sel;
end;

procedure TFRelAutorizacoesDesconto.BitBtn2Click(Sender: TObject);
var x : Boolean;
    size,aux : Integer;
    sqlConcat : String;
    hasDepartamento : Boolean;
var b : TBookmark;
var posMarcado : TBookmark;
var array_lotes,array_empres : array of Integer;
var memo : TfrxMemoView;
begin
  b := MLotes.GetBookmark;
    posMarcado := MLotes.GetBookmark;
  MLotes.Open;
  MLotes.First;
  size := 0;
  //flagIsPDF := False;
  //FiltraPorSetor := False;
  while not MLotes.Eof do begin

    if(MLotesmarcado.AsBoolean = True)then
    begin
      //i := i+1 ;
      SetLength(array_lotes,size+1);
      SetLength(array_empres,size+1);
      //array_lotes armazena todos os ids de lotes retornados pela busca
      array_lotes[size]  := MLotesseq_lote.AsInteger;
      //array_lotes armazena todos os ids de empresas retornados pela busca
      array_empres[size] := MLotesempres_id.AsInteger;
      //size � o contador da busca
      size := size+1;
    end;
    MLotes.Next;
  end;
  if((QLotesCartao.RecordCount>=1) and (size = 0))then
  begin
    MsgInf('Voc� precisa selecionar pelo menos uma linha para exibir o relat�rio.');
    exit;
  end;
  QCartao.Close;
  QCartao.SQL.Clear;
  QCartao.SQL.Add('SELECT conv.chapa, conv.celular,'''' as assinatura, cart.codcartimp,cl.seq_lote,coalesce(EMP_DPTOS.DEPT_ID,-1) setor_id,coalesce(EMP_DPTOS.DESCRICAO,'''') setor,  ');
  QCartao.SQL.Add('conv.TITULAR as titular,case coalesce(cart.TITULAR,''N'') when ''N'' then cart.nome else '''' end as dependente,e.nome,e.cgc,e.tipo_credito, ');
  QCartao.SQL.Add('e.empres_id,b.descricao,e.FANTASIA,case when E.LOGRADOURO_CARTAO IS NOT NULL and E.CIDADE_CARTAO IS NOT NULL and E.UF_CARTAO IS NOT NULL ');
  QCartao.SQL.Add('then CONCAT(UPPER(e.logradouro_cartao),'', '',e.numero_cartao,'', '',UPPER(ba.descricao),'','',CHAR(13)+CHAR(10),UPPER(ci.nome),'' - '',es.uf, '' / CEP: '', e.cep_cartao) ELSE ');
  QCartao.SQL.Add('CONCAT(e.endereco,'', '',e.numero,'', '',UPPER(ba.descricao),'','',CHAR(13)+CHAR(10),UPPER(ci.nome),'' - '',es.uf, '' / CEP: '', e.cep) END as endereco, ');
  QCartao.SQL.Add('CASE e.mod_cart_id when 1 then ''DROGABELLA'' else ''PLANT�O CARD'' end as empres_fornece_cartao, ');
  QCartao.SQL.Add('CASE mc.DESCRICAO WHEN ''DB - Drogabella'' then ''DROGABELLA'' WHEN ''RPC - Plantao Card'' then ''PLANT�O CARD'' WHEN ''AL - ALIMENTA��O'' then ''PLANT�O CARD ALIMENTA��O'' ');
  QCartao.SQL.Add('when ''RF - Refei��o'' then ''PLANT�O CARD REFEI��O'' END ');
  QCartao.SQL.Add('from CONVENIADOS conv ');
  QCartao.SQL.Add('inner join CARTOES cart ON conv.CONV_ID = cart.CONV_ID ');
  QCartao.SQL.Add('inner join bandeiras b ON b.band_id = (SELECT band_id from empresas WHERE empres_id = conv.empres_id)');
  QCartao.SQL.Add('inner join CARTOES_LOTES_DETALHE cld ON CART.CARTAO_ID in(cld.CARTAO_ID) ');
  QCartao.SQL.Add('inner join CARTOES_LOTES cl ON cl.SEQ_LOTE = cld.SEQ_LOTE and cl.SEQ_LOTE IN(');
  //Size � a vari�vel que verifica se o vetor possui registro ou n�o
  if(size = 0)THEN
      QCartao.SQL.Add(QuotedStr(IntToStr(array_lotes[0]))+')')
  else
  begin
      aux := 0;
      sqlConcat := '';
      while aux < size do
      begin
        if(aux = 0)then
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + IntToStr(array_lotes[aux]);
              Inc(aux);
          end
        else
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + ','+IntToStr(array_lotes[aux]);
              Inc(aux);
          end;
      end;
      QCartao.SQL.Text := QCartao.SQL.Text + ')';

  end;
  QCartao.SQL.Text;
  QCartao.SQL.Add('inner join EMPRESAS e ON e.EMPRES_ID = conv.EMPRES_ID AND e.empres_id in(');
  if(size = 0)THEN
      QCartao.SQL.Add(QuotedStr(IntToStr(array_empres[0]))+')')
  else
  begin
      aux := 0;
      sqlConcat := '';
      while aux < size do
      begin
        if(aux = 0)then
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + IntToStr(array_empres[aux]);
              Inc(aux);
          end
        else
          begin
              QCartao.SQL.Text := QCartao.SQL.Text + ','+IntToStr(array_empres[aux]);
              Inc(aux);
          end;

       end;
       QCartao.SQL.Text := QCartao.SQL.Text + ')';

  end;
  QCartao.SQL.Add('left join BAIRROS BA on (ba.bairro_id = e.bairro) ');
  QCartao.SQL.Add('left join CIDADES CI on (ci.cid_id = e.cidade) ');
  QCartao.SQL.Add('left join ESTADOS ES on (es.estado_id = e.estado) ');  
  QCartao.SQL.Add('inner join MODELOS_CARTOES mc ON mc.mod_cart_id = e.mod_cart_id');
  QCartao.SQL.Add('left join EMP_DPTOS ON EMP_DPTOS.DEPT_ID = cl.setor_id');
  QCartao.SQL.Add('WHERE conv.liberado = ''S'' and conv.apagado = ''N''');
  QCartao.SQL.Add('group by conv.TITULAR,b.descricao,conv.CHAPA,cart.CODCARTIMP,cl.SEQ_LOTE,cart.NOME,cart.TITULAR,e.nome,e.cgc,e.FANTASIA,');
  QCartao.SQL.Add('e.empres_id,e.endereco,e.numero, e.cep, e.logradouro_cartao, e.numero_cartao, e.cep_cartao, e.bairro_cartao, e.cidade_cartao, e.uf_cartao, ');
  QCartao.SQL.Add('ba.descricao, ci.nome, es.uf,e.MOD_CART_ID,mc.DESCRICAO,e.tipo_credito, conv.celular,EMP_DPTOS.DEPT_ID,EMP_DPTOS.DESCRICAO');
  MLotes.First;
  hasDepartamento := False;
  while not MLotes.Eof do
  begin
    if MLotesmarcado.AsBoolean = True then begin
      if MLotessetor.AsString <> '' then begin
        hasDepartamento := True;
        FiltraPorSetor  := True;
        MLotes.Last;
      end;
    end;
    MLotes.Next;
  end;
  if hasDepartamento = False or chkOrdenaSetor.Checked = False then begin
     QCartao.SQL.Add('order by empres_id,conv.TITULAR');
     hasDepartamento := False;
     FiltraPorSetor  := False;
  end
     else
        QCartao.SQL.Add('order by dept_id,conv.TITULAR');

  QCartao.SQL.Text;
  QCartao.Open;

  if rbEtiqueta.Checked then
  begin
    frxEtiqueta.ShowReport();
  end
  else begin
   //CONTROLE DO TIPO DE RELAT�RIO A EXIBIR. SE tipo_credito <= 1 ent�o IMPRIME AUTORIZA��O
  //SE N�O SE MAIOR Q 1 IMPRIME TERMORECEBIMENTO

  //REGION que trata apenas "AUTORIZA��ES DE DESCONTO" e "ENVIA MENSAGEM PARA CELULAR"
  if  QCartaotipo_credito.AsInteger <= 1 then
    begin
      if rbEnviaSMS.Checked then begin
        //<< flagIsPDF � uma vari�vel global que muda ao passo que o usu�rio clica no BitBtn3(Bot�o GerarPDF)>>
        //<<Caso o o usu�rio clique em Gerar PDF o Valor True � atribuido a vari�vel. E a visualiza��o do relat�rio>>
        //<<n�o � feita. Caso o contr�rio significa que o usu�rio quer visualizar.>>

        if flagIsPDF = False then begin
           if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

              FiltraPorSetor := True;
              frxPermitMsgCelular.ScriptText.Clear;
              frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');

           end
           else begin
              FiltraPorSetor := False;
              frxPermitMsgCelular.ScriptText.Clear;
              frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                         'end.');

           end;
           frxPermitMsgCelular.ShowReport()
        end;

      end
      //Regi�o que trata a impre��o de "AUTORIZA��O DE DESCONTO".
      else
         if flagIsPDF = False then//flagIsPDF � uma vari�vel para verivicar se o usu�rio est� gerando PDF
         begin

            if ckbImprimeIndividual.Checked then begin
              if MLotesempres_id.AsInteger = 2430 then begin
                if chkOrdenaSetor.Checked = True then
                begin
                    FiltraPorSetor := False;
                    FrxAutorizacao2430.ScriptText.Clear;
                      FrxAutorizacao2430.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                               'end.');
                    FrxAutorizacao2430.ShowReport();
                end
                else if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      FrxAutorizacao2430.ScriptText.Clear;
                        FrxAutorizacao2430.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');

                       FrxAutorizacao2430.ShowReport();
                  end;
              end
              else begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                    FiltraPorSetor := True;
                    frxAutorizacaoModeloMilClean.ScriptText.Clear;
                    frxAutorizacaoModeloMilClean.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                               'end.');


                end
                else begin
                  FiltraPorSetor := False;
                  frxAutorizacaoModeloMilClean.ScriptText.Clear;
                  frxAutorizacaoModeloMilClean.ScriptText.Add('begin' + #13 +
                                                    'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                    'end.' );


                end;
                frxAutorizacaoModeloMilClean.ShowReport();

              end;

            end
            else if chkBemEstar.Checked = True then
            begin
              if (MLotesempres_id.AsInteger = 2379) or (MLotesempres_id.AsInteger = 2380) or (MLotesempres_id.AsInteger = 2568) then
              begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      frxBemEstarISINDSERV.ScriptText.Clear;
                      frxBemEstarISINDSERV.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');
                end
                else begin

                    FiltraPorSetor := False;
                    frxBemEstarISINDSERV.ScriptText.Clear;
                    frxBemEstarISINDSERV.ScriptText.Add('begin' + #13 +
                                                      'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                      'end.' );
                end;
                frxBemEstarISINDSERV.ShowReport();
              end
              else if (MLotesempres_id.AsInteger = 1650) or (MLotesempres_id.AsInteger = 1813) then
              begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      frxBemEstarRPC.ScriptText.Clear;
                      frxBemEstarRPC.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');
                end
                else begin

                    FiltraPorSetor := False;
                    frxBemEstarRPC.ScriptText.Clear;
                    frxBemEstarRPC.ScriptText.Add('begin' + #13 +
                                                      'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                      'end.' );
                end;
                frxBemEstarRPC.ShowReport();
              end
              else if QCartaoempres_fornece_cartao.Text = 'DROGABELLA' then
              begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      frxDrogaBellaBemEstar.ScriptText.Clear;
                      frxDrogaBellaBemEstar.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');
                end
                else begin

                    FiltraPorSetor := False;
                    frxDrogaBellaBemEstar.ScriptText.Clear;
                    frxDrogaBellaBemEstar.ScriptText.Add('begin' + #13 +
                                                      'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                      'end.' );
                end;
                frxDrogaBellaBemEstar.ShowReport();
              end
              else begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin
                  FiltraPorSetor := True;
                  frxBemEstar.ScriptText.Clear;
                  frxBemEstar.ScriptText.Add('begin' + #13 +
                                             'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                             'end.');
                end
                else begin
                  FiltraPorSetor := False;
                  frxBemEstar.ScriptText.Clear;
                  frxBemEstar.ScriptText.Add('begin' + #13 +
                                                    'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                    'end.' );
                end;
                frxBemEstar.ShowReport();
              end;

            end
            else if chkItaqua.Checked = True then
            begin
              if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                    FiltraPorSetor := True;
                    frxBemEstarItaquaECunha.ScriptText.Clear;
                    frxBemEstarItaquaECunha.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                               'end.');
              end
              else begin

                  FiltraPorSetor := False;
                  frxBemEstarItaquaECunha.ScriptText.Clear;
                  frxBemEstarItaquaECunha.ScriptText.Add('begin' + #13 +
                                                    'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                    'end.' );
              end;
                frxBemEstarItaquaECunha.ShowReport();
            end


            else
            begin
              if MLotesempres_id.AsInteger = 2430 then begin
                if chkOrdenaSetor.Checked = True then
                begin
                    FiltraPorSetor := False;
                    FrxAutorizacao2430.ScriptText.Clear;
                      FrxAutorizacao2430.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                               'end.');
                    FrxAutorizacao2430.ShowReport();
                end
                  else if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      FrxAutorizacao2430.ScriptText.Clear;
                        FrxAutorizacao2430.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');

                       FrxAutorizacao2430.ShowReport();
                  end;
              end
              else begin
                if chkOrdenaSetor.Checked = True then
                begin
                    FiltraPorSetor := False;
                    frxAutorizacao.ScriptText.Clear;
                      frxAutorizacao.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                               'end.');
                    frxAutorizacao.ShowReport();
                end
                  else if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                      FiltraPorSetor := True;
                      frxAutorizacao.ScriptText.Clear;
                        frxAutorizacao.ScriptText.Add('begin' + #13 +
                                                 'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                                 'end.');

                       frxAutorizacao.ShowReport();
                  end;
              end;
            end;


         end;
         // end region AUTORIZA��O DE DESCONTO
    end
  else
    //REGION que trata a visualiza��o da "MENSAGEM ENVIADA PARA CELULAR" e "TERMO DE RECEBIMENTO".
    begin
      if rbEnviaSMS.Checked then begin
        if flagIsPDF = False then begin
           if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin
              //FiltraPorSetor - vari�vel booleana utilizada no evento
              FiltraPorSetor  := True;
              FiltraPorSetor := True;
              frxPermitMsgCelular.ScriptText.Clear;
              frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');
           end
           else begin
              FiltraPorSetor  := False;
              FiltraPorSetor := True;
              frxPermitMsgCelular.ScriptText.Clear;
              frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                         'end.');

           end;
           frxPermitMsgCelular.ShowReport();
        end;
      end
      else begin
        if ckbImprimeIndividual.Checked then begin
                if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                    FiltraPorSetor := True;
                    frxTermoModeloMilClean.ScriptText.Clear;
                    frxTermoModeloMilClean.ScriptText.Add('begin' + #13 +
                                               'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                               'end.');

                end
                else begin
                  FiltraPorSetor := False;
                  frxTermoModeloMilClean.ScriptText.Clear;
                  frxTermoModeloMilClean.ScriptText.Add('begin' + #13 +
                                                    'GroupHeader1.Condition := '+ QuotedStr('Cartao."titular"') +#13+
                                                    'end.' );
                end;
                frxTermoModeloMilClean.ShowReport();


        end
        else
        if flagIsPDF = False then begin

           if chkBemEstar.Checked = True then begin
              if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                  FiltraPorSetor := True;
                  frxTermoRecebimentoBemEstar.ScriptText.Clear;
                  frxTermoRecebimentoBemEstar.ScriptText.Add('begin' + #13 +
                                             'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                             'end.');

               end
              else begin
                  FiltraPorSetor := False;
                  frxTermoRecebimentoBemEstar.ScriptText.Add('begin' + #13 +
                                             'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                             'end.');

              end;
               frxTermoRecebimentoBemEstar.ShowReport();


           end
           else
           begin
             if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin

                FiltraPorSetor := True;
                frxTermoRecebimento.ScriptText.Clear;
                frxTermoRecebimento.ScriptText.Add('begin' + #13 +
                                           'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                           'end.');

             end
             else begin
                FiltraPorSetor := False;
                frxTermoRecebimento.ScriptText.Add('begin' + #13 +
                                           'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
                                           'end.');

             end;
             frxTermoRecebimento.ShowReport();
           end;
        end;
//         if flagIsPDF = False then begin
//
//           if hasDepartamento = True or chkOrdenaSetor.Checked = True then begin
//
//              FiltraPorSetor := True;
//              frxTermoRecebimento.ScriptText.Clear;
//              frxTermoRecebimento.ScriptText.Add('begin' + #13 +
//                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
//                                         'end.');
//
//           end
//           else begin
//              FiltraPorSetor := False;
//              frxTermoRecebimento.ScriptText.Add('begin' + #13 +
//                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id"') + #13 +
//                                         'end.');
//
//           end;
//           frxTermoRecebimento.ShowReport();
//         end;
      end;
    end;
  end;
    //flagIsPDF := False;
    //FiltraPorSetor := False;
    MLotes.GotoBookmark(posMarcado);
end;

procedure TFRelAutorizacoesDesconto.BitBtn3Click(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    flagIsPDF := True;
    BitBtn2.Click;
    if  QCartaotipo_credito.AsInteger <= 1 then
        begin
          if rbEnviaSMS.Checked then
          begin
            frxPermitMsgCelular.PrepareReport();
            frxPermitMsgCelular.Export(frxPDFExport1);
            flagIsPDF := False;
            FiltraPorSetor := False;
          end
          else begin
            if ckbImprimeIndividual.Checked then begin
              frxAutorizacaoModeloMilClean.PrepareReport();
              frxAutorizacaoModeloMilClean.Export(frxPDFExport1);
              flagIsPDF := False;
              FiltraPorSetor := False;
            end
            else begin
              frxAutorizacao.PrepareReport();
              frxAutorizacao.Export(frxPDFExport1);
              flagIsPDF := False;
              FiltraPorSetor := False;
            end;
          end;
        end
    else
    begin
        if rbEnviaSMS.Checked then
          begin
            frxPermitMsgCelular.PrepareReport();
            frxPermitMsgCelular.Export(frxPDFExport1);
            frxPDFExport1.OpenAfterExport := False;
            flagIsPDF := False;
            FiltraPorSetor := False;
          end
        else begin
            frxTermoRecebimento.PrepareReport();
            frxTermoRecebimento.Export(frxPDFExport1);
            frxPDFExport1.OpenAfterExport := False;
            flagIsPDF := False;
            FiltraPorSetor := False;
        end;

    end;
    //ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

procedure TFRelAutorizacoesDesconto.BuscaLotes;
begin
  QLotesCartao.Close;
  QLotesCartao.SQL.Clear;
  QLotesCartao.SQL.Add('SELECT cld.seq_lote, FORMAT(data,''dd/MM/yyyy'') as DATA, nome_arq,hora,');
  QLotesCartao.SQL.Add('e.fantasia,e.EMPRES_ID, EMP_DPTOS.descricao');
  QLotesCartao.SQL.Add('FROM cartoes_lotes cl');
  QLotesCartao.SQL.Add(' left join EMP_DPTOS ON EMP_DPTOS.DEPT_ID = cl.setor_id');
  QLotesCartao.SQL.Add(' INNER JOIN cartoes_lotes_detalhe cld ON cld.SEQ_LOTE = cl.SEQ_LOTE');
  QLotesCartao.SQL.Add(' INNER JOIN EMPRESAS e ON cld.empres_id =  e.empres_id');
  QLotesCartao.SQL.Add('WHERE ');

  if((DataIni.Date = 0) and (DBEmpresa.KeyValue = Null)) then
  begin
     MsgInf('A data inicial n�o pode estar vazia'); DataIni.SetFocus; Exit;
  end;

  if((DataIni.Date > datafin.Date) and (datafin.Date <> 0))then
  begin
     MsgInf('A data inicial n�o pode ser maior do que a data final');
     DataIni.SetFocus;
     Exit;
  end;

  if((DataIni.Date <> 0) or (datafin.Date <> 0)) then
  begin
    if(rbTermo.Checked or rbAutorizacao.Checked or rbEnviaSMS.Checked or rbEtiqueta.Checked) then
    begin
       if datafin.Date <> 0 then
       begin
          QLotesCartao.SQL.Add('DATA  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataFin.Date)));
       end
       else
       QLotesCartao.SQL.Add('DATA  BETWEEN '+QuotedStr(FormatDateTime('dd/mm/yyyy',DataIni.Date))+'AND '+QuotedStr(FormatDateTime('dd/mm/yyyy',Date)));
    end
    else
    begin
       MsgInf('Voc� precisa selecionar uma das op��es de impress�o.');
       Exit;
    end;

    if(rbTermo.Checked)then
       QLotesCartao.SQL.Add('and e.tipo_credito >1')
       else
          QLotesCartao.SQL.Add('and e.tipo_credito <= 1');
    if(DBEmpresa.KeyValue <> Null)then
    begin
       QLotesCartao.SQL.Add('and e.empres_id = '+DBEmpresa.KeyValue);
    end;

    if(lkpModeloCartao.KeyValue <> null) then
    begin
      QLotesCartao.SQL.Add('and e.mod_cart_id = ' + lkpModeloCartao.KeyValue);
    end;
  end
  else
  begin
    QLotesCartao.SQL.Add('e.empres_id = '+DBEmpresa.KeyValue);
  end;
  QLotesCartao.SQL.Add('group by e.empres_id,cld.SEQ_LOTE,cl.data,cl.hora,cl.NOME_ARQ,e.FANTASIA,EMP_DPTOS.DESCRICAO');
  QLotesCartao.SQL.Add('ORDER BY DATA ASC');
  QLotesCartao.Open;
  if QLotesCartao.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum lote com os filtros aplicados.');
    DataIni.SetFocus;
  end;
  QLotesCartao.First;
  MLotes.Open;
  MLotes.EmptyTable;
  MLotes.DisableControls;
  while not QLotesCartao.Eof do begin
      MLotes.Append;
      //ShowMessage(QLotesCartaofantasia.AsString);
      MLotesSeq_Lote.AsInteger  := QLotesCartaoSEQ_LOTE.AsInteger;
      MLotesfantasia.AsString   := QLotesCartaofantasia.Value;
      MLotesempres_id.AsInteger := QLotesCartaoEMPRES_ID.AsInteger;
      MLotesdata.AsDateTime     := QLotesCartaodata.AsDateTime;
      MLotesnome_arq.Value      := QLotesCartaoNOME_ARQ.Value;
      MLotessetor.AsString      := QLotesCartaoDESCRICAO.AsString;
      MLotesMarcado.AsBoolean := False;
      MLotes.Post;
      QLotesCartao.Next;
  end;
  MLotes.First;
  MLotes.EnableControls;
  empres_sel := EmptyStr;
end;

procedure TFRelAutorizacoesDesconto.carregaDataSet;
var b : TBookmark;
begin
  b := MLotes.GetBookmark;
  MLotes.DisableControls;
  MLotes.Open;
  MLotes.First;
  while not QEmpresas.Eof do begin
    if (MLotesmarcado.asBoolean = True) then begin

    end;
    MLotes.Next;
  end;
  //Carregando os cart�es
  //QCartao.Parameters.ParamByName('num_lote').Value := 19;
  QCartao.Open;

  //ShowMessage(QCartaotitular.AsString);
//  cdsCartao.Edit;
  While not QCartao.Eof do begin
//    cdsCartaoChapa.AsFloat       := QCartaochapa.AsFloat;
//    cdsCartaoCodCartao.AsString  := QCartaocodcartimp.AsString;
//    cdsCartaoSeq_Lote.AsInteger  := QCartaoseq_lote.AsInteger;
//    cdsCartaoTitular.AsString    := QCartaotitular.AsString;
//    cdsCartaoDependente.AsString := QCartaodependente.AsString;
//    cdsCartao.Post;
//    QCartao.Next
  end;
  //carregando Empresas
  QEmpresas.Parameters.ParamByName('empres_id').Value := 45;
  QEmpresas.Open;
end;

procedure TFRelAutorizacoesDesconto.ButMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  if MLotes.IsEmpty then Exit;
  MLotes.DisableControls;
  marca := MLotes.GetBookmark;
  MLotes.First;
  while not MLotes.eof do begin
    MLotes.Edit;
    MLotesmarcado.AsBoolean := true;
    MLotes.Post;
    MLotes.Next;
  end;
  MLotes.GotoBookmark(marca);
  MLotes.FreeBookmark(marca);
  MLotes.EnableControls;
  Empresas_Sel;
  //DMConexao.MarcaDesmTodos(QConv,True);
end;

procedure TFRelAutorizacoesDesconto.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = vk_F12 then if PageControl1.ActivePageIndex = 0 then ButMarcaDesmEmp.Click;
  if key = vk_F6  then if PageControl1.ActivePageIndex = 0 then ButMarcaTodasEmp.Click;
  if key = vk_f7  then if PageControl1.ActivePageIndex = 0 then ButDesmarcaTodosEmp.Click;
end;

procedure TFRelAutorizacoesDesconto.chempresaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  //DBEmpresa.Enabled := chempresa.Checked;
  //QEmpCombo.Open;
end;

procedure TFRelAutorizacoesDesconto.EdEmp_IDKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFRelAutorizacoesDesconto.EdEmp_IDChange(Sender: TObject);
begin
  inherited;
  if Sender = EdEmp_ID then begin
     if EdEmp_ID.Text = '' then
        DBEmpresa.ClearValue
     else if QEmpCombo.Locate('empres_id',EdEmp_ID.Text,[]) then
       DBEmpresa.KeyValue := EdEmp_ID.Text
     else
       DBEmpresa.ClearValue;
  end;
end;

procedure TFRelAutorizacoesDesconto.FormShow(Sender: TObject);
begin
  inherited;
  QModelos.Open;
end;

procedure TFRelAutorizacoesDesconto.frxAutorizacaoClosePreview(Sender: TObject);
begin
  inherited;
  MostrarRelatorio := False;
  lastRec := False;
  recConv := 0;
end;

procedure TFRelAutorizacoesDesconto.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcaDesmEmpClick(nil);
end;

procedure TFRelAutorizacoesDesconto.JvDBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = vk_return then ButMarcaDesmEmpClick(nil);
end;

procedure TFRelAutorizacoesDesconto.frxAutorizacaoBeforePrint(
  Sender: TfrxReportComponent);
begin
  inherited;
  if FiltraPorSetor then
  begin

    frxAutorizacao.ScriptText.Clear;
                frxAutorizacao.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');

  end

  else
  begin
    frxAutorizacao.ScriptText.Clear;
                frxAutorizacao.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id";') + #13 +
                                         ' <Cartao."setor">.visible := FALSE;' + #13 +
                                         'end.');
                frxAutorizacao.ScriptText.Add('procedure CartaosetorOnBeforePrint(Sender: TfrxComponent);' + #13 +
                                          'begin'  + #13 +
                                          ' <Cartao."setor">.visible := FALSE;' + #13 +
                                          'end;');

    TfrxMemoView(frxAutorizacao.FindObject('Cartaosetor')).Visible := False;
  end;
end;

procedure TFRelAutorizacoesDesconto.frxPermitMsgCelularBeforePrint(Sender:
    TfrxReportComponent);
begin
  inherited;
  if FiltraPorSetor then
  begin

    frxPermitMsgCelular.ScriptText.Clear;
                frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');

  end

  else
  begin
   frxPermitMsgCelular.ScriptText.Clear;
                frxPermitMsgCelular.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id";') + #13 +
                                         ' <Cartao."setor">.visible := FALSE;' + #13 +
                                         'end.');
                frxPermitMsgCelular.ScriptText.Add('procedure CartaosetorOnBeforePrint(Sender: TfrxComponent);' + #13 +
                                          'begin'  + #13 +
                                          ' <Cartao."setor">.visible := FALSE;' + #13 +
                                          'end;');

    TfrxMemoView(frxPermitMsgCelular.FindObject('Cartaosetor')).Visible := False;
  end;
end;

procedure TFRelAutorizacoesDesconto.frxTermoRecebimentoBeforePrint(Sender:
    TfrxReportComponent);
begin
  inherited;

  if FiltraPorSetor then
  begin

    frxTermoRecebimento.ScriptText.Clear;
                frxTermoRecebimento.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."setor_id"') + #13 +
                                         'end.');

  end

  else
  begin
    frxTermoRecebimento.ScriptText.Clear;
                frxTermoRecebimento.ScriptText.Add('begin' + #13 +
                                         'GroupHeader1.Condition := ' + QuotedStr('Cartao."empres_id";') + #13 +
                                         ' <Cartao."setor">.visible := FALSE;' + #13 +
                                         'end.');
                frxTermoRecebimento.ScriptText.Add('procedure CartaosetorOnBeforePrint(Sender: TfrxComponent);' + #13 +
                                          'begin'  + #13 +
                                          ' <Cartao."setor">.visible := FALSE;' + #13 +
                                          'end;');

    TfrxMemoView(frxTermoRecebimento.FindObject('Cartaosetor')).Visible := False;
  end;
  
end;

end.

