object FRelNFMunic: TFRelNFMunic
  Left = 609
  Top = 254
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Relat'#243'rio NF Servi'#231'os Municipais'
  ClientHeight = 191
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 305
    Height = 193
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Data Inicio'
    end
    object Label2: TLabel
      Left = 144
      Top = 8
      Width = 42
      Height = 13
      Caption = 'Data Fim'
    end
    object Label3: TLabel
      Left = 144
      Top = 64
      Width = 20
      Height = 13
      Caption = 'Filial'
    end
    object Button1: TButton
      Left = 200
      Top = 112
      Width = 91
      Height = 65
      Caption = 'Efetuar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 8
      Top = 152
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 1
      OnClick = Button2Click
    end
    object ComboBox1: TComboBox
      Left = 144
      Top = 80
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'RPC REDE PLANT'#195'O DE CONVENIOS EIRELI'
        'CDC ADMINISTRA'#199#195'O DE CART'#213'ES EIRELI ME'
        'PRATICARD ADMINISTRA'#199#195'O DE CONVENIOS EIRELI')
    end
    object DataInicio: TJvDateEdit
      Left = 8
      Top = 27
      Width = 100
      Height = 21
      ShowNullDate = False
      TabOrder = 3
    end
    object DataFinal: TJvDateEdit
      Left = 144
      Top = 27
      Width = 100
      Height = 21
      ShowNullDate = False
      TabOrder = 4
    end
    object CheckSpani: TCheckBox
      Left = 8
      Top = 64
      Width = 97
      Height = 17
      Caption = 'Spani'
      TabOrder = 5
    end
    object CheckEstrela: TCheckBox
      Left = 8
      Top = 88
      Width = 97
      Height = 17
      Caption = 'Sete Estrelas'
      TabOrder = 6
    end
  end
  object QBusca: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 104
    Top = 112
  end
end
