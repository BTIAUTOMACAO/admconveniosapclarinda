inherited FCadEmp: TFCadEmp
  Left = 79
  Top = 36
  Caption = 'Cadastro de Empresas'
  ClientHeight = 680
  ClientWidth = 1237
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label23: TLabel [0]
    Left = 488
    Top = 169
    Width = 70
    Height = 13
    Caption = 'Representante'
  end
  inherited PageControl1: TPageControl
    Width = 1237
    Height = 639
    ActivePage = UsuariosWebDiversos
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 502
        Width = 1229
        inherited Label1: TLabel
          Left = 0
        end
        inherited Label2: TLabel
          Left = 81
          Width = 96
          Caption = '&Nome/Raz'#227'o Social'
        end
        object Label32: TLabel [2]
          Left = 545
          Top = 3
          Width = 33
          Height = 13
          Caption = '&Cidade'
          FocusControl = EdCidade
        end
        inherited btnFiltroDados: TSpeedButton
          Left = 225
        end
        inherited btnAlterLinear: TSpeedButton
          Left = 319
        end
        object Label61: TLabel [9]
          Left = 688
          Top = 3
          Width = 76
          Height = 13
          Caption = '&Dia de Repasse'
        end
        object Label62: TLabel [10]
          Left = 416
          Top = 3
          Width = 27
          Height = 13
          Caption = 'CNP&J'
        end
        object Label63: TLabel [11]
          Left = 776
          Top = 3
          Width = 84
          Height = 13
          Caption = '&Modelo do Cart'#227'o'
        end
        object Label64: TLabel [12]
          Left = 904
          Top = 3
          Width = 90
          Height = 13
          Caption = '&Resp. Fechamento'
        end
        object Label65: TLabel [13]
          Left = 272
          Top = 3
          Width = 40
          Height = 13
          Caption = '&Fantasia'
        end
        object lbl8: TLabel [14]
          Left = 1047
          Top = 3
          Width = 41
          Height = 13
          Caption = 'Liberado'
        end
        inherited EdNome: TEdit
          Left = 81
          Width = 184
        end
        inherited ButBusca: TBitBtn
          Left = 935
          Top = 45
          TabOrder = 12
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 1401
          TabOrder = 13
        end
        inherited EdCod: TEdit
          Left = 0
        end
        inherited ButFiltro: TBitBtn
          Left = 415
          TabOrder = 10
        end
        inherited ButAltLin: TButton
          Left = 509
          TabOrder = 11
        end
        object EdCidade: TEdit
          Left = 545
          Top = 18
          Width = 136
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 4
          OnKeyPress = EdNomeKeyPress
        end
        object EdDiaRepasse: TEdit
          Left = 688
          Top = 18
          Width = 81
          Height = 21
          TabOrder = 5
        end
        object EdFantasia: TEdit
          Left = 272
          Top = 18
          Width = 137
          Height = 21
          TabOrder = 3
          OnExit = EdFantasiaExit
          OnKeyPress = EdFantasiaKeyPress
        end
        object cbModCart: TJvDBLookupCombo
          Left = 776
          Top = 19
          Width = 121
          Height = 21
          LookupField = 'MOD_CART_ID'
          LookupDisplay = 'DESCRICAO'
          LookupSource = dsModelosCartoes
          TabOrder = 6
        end
        object cbRespFecha: TJvDBLookupCombo
          Left = 904
          Top = 19
          Width = 137
          Height = 21
          LookupField = 'nome'
          LookupDisplay = 'nome'
          LookupSource = dsOperadores
          TabOrder = 7
        end
        object cbbLiberado: TComboBox
          Left = 1048
          Top = 19
          Width = 73
          Height = 21
          ItemHeight = 13
          TabOrder = 8
          Text = 'TODOS'
          Items.Strings = (
            'TODOS'
            'S'
            'N')
        end
        object EdCNPJ: TJvMaskEdit
          Left = 416
          Top = 16
          Width = 121
          Height = 21
          EditMask = '00.000.000/0000-00;1;_'
          MaxLength = 18
          TabOrder = 0
          Text = '  .   .   /    -  '
        end
        object chkApagado: TCheckBox
          Left = 1128
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Busca Apagado'
          TabOrder = 9
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 1229
        Height = 502
        PopupMenu = PopupAltLinearGrupoProd
        OnEnter = DBGrid1Enter
        OnContextPopup = DBGrid1ContextPopup
        Columns = <
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 256
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANTASIA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECARTAO'
            Width = 202
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CGC'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INSCRICAOEST'
            Width = 138
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENDERECO'
            Width = 273
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUMERO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEBAIRRO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECIDADE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEESTADO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CEP'
            Title.Caption = 'C.E.P'
            Width = 98
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE1'
            Width = 89
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TELEFONE2'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FAX'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'REPRESENTANTE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADA'
            PickList.Strings = (
              'S'
              'N')
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FIDELIDADE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ACEITA_PARC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TODOS_SEGMENTOS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENDA_NOME'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BLOQ_ATE_PGTO'
            Title.Caption = 'Bloq. at'#233' Pgto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMISSAO_CRED'
            Title.Caption = 'Comiss'#227'o Cred'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FECHAMENTO1'
            Title.Caption = 'Dia Fechamento 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VENCIMENTO1'
            Title.Caption = 'Dia Vencimento 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS_FECHAMENTO'
            Title.Caption = 'Obs. Fechamento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMAIL'
            Width = 142
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HOMEPAGE'
            Width = 159
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTRATO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTCADASTRO'
            Width = 101
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERCADASTRO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAXA_BANCO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DT_DEBITO'
            Title.Caption = 'Data D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCONTO_EMP'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCONTO_FUNC'
            Title.Caption = 'Desconto Func.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMISSAO_CRED'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CRED_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIMITE_PADRAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INC_CART_PBM'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PROG_DESC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FATOR_RISCO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR_TAXA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAXA_JUROS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MULTA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALE_DESCONTO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USA_LIMITE_MAX'
            Title.Caption = 'USA LIMITE MAX'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BAIRRO'
            Width = 156
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CIDADE'
            Width = 191
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ESTADO'
            Title.Caption = 'Estado'
            Width = 40
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 574
        Width = 1229
      end
    end
    inherited TabFicha: TTabSheet
      OnExit = TabFichaExit
      inherited Panel2: TPanel
        Top = 561
        Width = 1229
        inherited btnFirstB: TSpeedButton
          Left = 480
        end
        inherited btnPriorB: TSpeedButton
          Left = 513
        end
        inherited btnNextB: TSpeedButton
          Left = 546
        end
        inherited btnLastB: TSpeedButton
          Left = 579
        end
        inherited ButInclui: TBitBtn
          Left = 97
        end
        inherited ButApaga: TBitBtn
          Left = 187
        end
        inherited ButCancela: TBitBtn [7]
          Left = 717
          TabOrder = 5
        end
        object ButLimpaSenha: TBitBtn [8]
          Left = 277
          Top = 4
          Width = 100
          Height = 25
          Hint = 'Limpar senha da empresa'
          Caption = 'Limpar Senha'
          TabOrder = 3
          OnClick = ButLimpaSenhaClick
          Glyph.Data = {
            96050000424D9605000000000000960300002800000020000000100000000100
            08000000000000020000232E0000232E0000D800000000000000FFFFFF00CCFF
            FF0099FFFF0066FFFF0033FFFF0000FFFF00FFCCFF00CCCCFF0099CCFF0066CC
            FF0033CCFF0000CCFF00FF99FF00CC99FF009999FF006699FF003399FF000099
            FF00FF66FF00CC66FF009966FF006666FF003366FF000066FF00FF33FF00CC33
            FF009933FF006633FF003333FF000033FF00FF00FF00CC00FF009900FF006600
            FF003300FF000000FF00FFFFCC00CCFFCC0099FFCC0066FFCC0033FFCC0000FF
            CC00FFCCCC00CCCCCC0099CCCC0066CCCC0033CCCC0000CCCC00FF99CC00CC99
            CC009999CC006699CC003399CC000099CC00FF66CC00CC66CC009966CC006666
            CC003366CC000066CC00FF33CC00CC33CC009933CC006633CC003333CC000033
            CC00FF00CC00CC00CC009900CC006600CC003300CC000000CC00FFFF9900CCFF
            990099FF990066FF990033FF990000FF9900FFCC9900CCCC990099CC990066CC
            990033CC990000CC9900FF999900CC9999009999990066999900339999000099
            9900FF669900CC66990099669900666699003366990000669900FF339900CC33
            990099339900663399003333990000339900FF009900CC009900990099006600
            99003300990000009900FFFF6600CCFF660099FF660066FF660033FF660000FF
            6600FFCC6600CCCC660099CC660066CC660033CC660000CC6600FF996600CC99
            660099996600669966003399660000996600FF666600CC666600996666006666
            66003366660000666600FF336600CC3366009933660066336600333366000033
            6600FF006600CC00660099006600660066003300660000006600FFFF3300CCFF
            330099FF330066FF330033FF330000FF3300FFCC3300CCCC330099CC330066CC
            330033CC330000CC3300FF993300CC9933009999330066993300339933000099
            3300FF663300CC66330099663300666633003366330000663300FF333300CC33
            330099333300663333003333330000333300FF003300CC003300990033006600
            33003300330000003300FFFF0000CCFF000099FF000066FF000033FF000000FF
            0000FFCC0000CCCC000099CC000066CC000033CC000000CC0000FF990000CC99
            000099990000669900003399000000990000FF660000CC660000996600006666
            00003366000000660000FF330000CC3300009933000066330000333300000033
            0000FF000000CC00000099000000660000003300000000000000002D092D0000
            00000000000000000000012B072B0000000000000000000000002D35350B5F00
            000000000000000000002B2B2B2B2B000000000000000000000009033B350B5F
            0000000000000000000007002B2B072B000000000000000000002D010B5F350B
            5F0000000000000000000700002B2B2B2B0000000000000000000035010A3B35
            04350000000000000000002B00002B2B072B0000000000000000000035010A5F
            350A352D00000000000000002B00002B2B012B010000000000000000340A0909
            3B350A353B5F5F3400000000070000002B2B012B2B2B2B2B0000000035013501
            09350909090909345F0000002B002B00002B00000000002B2B00000009350935
            01030203020302030A340000012B012B0000010000000100072B000000000009
            340102020101010102350000000000012B00000000000000002B000000000000
            350102020335350902350000000000002B000000012B2B00002B000000000000
            350101013500013501350000000000002B0000002B00002B002B000000000000
            3501010135071101092E0000000000002B0000002B002B00012B000000000000
            2E0901012D5F010235000000000000002B0000002B2B00002B00000000000000
            00350901010109350000000000000000002B01000000012B0000000000000000
            00000A3535350A00000000000000000000002B2B2B2B2B000000}
          NumGlyphs = 2
        end
        inherited ButGrava: TBitBtn [9]
          Left = 624
          TabOrder = 4
        end
        object JvBitBtn2: TJvBitBtn
          Left = 816
          Top = 8
          Width = 75
          Height = 25
          Caption = 'JvBitBtn2'
          TabOrder = 6
          OnClick = JvBitBtn2Click
        end
      end
      inherited Panel3: TPanel
        Width = 1229
        Height = 561
        object PageControl2: TPageControl
          Left = 2
          Top = 2
          Width = 1225
          Height = 557
          ActivePage = TabSheet1
          Align = alClient
          Style = tsFlatButtons
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = '&Dados da Empresa'
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 1217
              Height = 153
              Hint = 'CNPJ'
              Align = alTop
              Caption = 'Dados cadastrais'
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 15
                Width = 49
                Height = 13
                Caption = 'Empres ID'
                FocusControl = DBEdit1
              end
              object Label4: TLabel
                Left = 78
                Top = 15
                Width = 75
                Height = 13
                Caption = 'Raz'#227'o/Nome'
                FocusControl = dbEdtNm
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label16: TLabel
                Left = 454
                Top = 15
                Width = 62
                Height = 13
                Caption = 'Nome Cart'#227'o'
                FocusControl = DBEdit14
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label15: TLabel
                Left = 540
                Top = 55
                Width = 70
                Height = 13
                Caption = 'Representante'
                FocusControl = DBEdit13
              end
              object Label33: TLabel
                Left = 8
                Top = 55
                Width = 71
                Height = 13
                Caption = 'Nome Fantasia'
                FocusControl = DBEdit20
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label5: TLabel
                Left = 287
                Top = 55
                Width = 21
                Height = 13
                Caption = 'Cnpj'
              end
              object Label6: TLabel
                Left = 421
                Top = 55
                Width = 87
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual'
                FocusControl = DBEdit4
              end
              object Label19: TLabel
                Left = 693
                Top = 55
                Width = 40
                Height = 13
                Caption = 'Contrato'
                FocusControl = DBEdit17
              end
              object Label10: TLabel
                Left = 213
                Top = 95
                Width = 54
                Height = 13
                Caption = 'Logradouro'
                FocusControl = txtENDERECO
              end
              object Label11: TLabel
                Left = 700
                Top = 95
                Width = 27
                Height = 13
                Caption = 'Bairro'
              end
              object Label13: TLabel
                Left = 8
                Top = 95
                Width = 30
                Height = 13
                Caption = 'C.E.P.'
                FocusControl = txtCEP
              end
              object Label14: TLabel
                Left = 484
                Top = 95
                Width = 14
                Height = 13
                Caption = 'UF'
                FocusControl = lkpESTADO
              end
              object Label29: TLabel
                Left = 439
                Top = 95
                Width = 12
                Height = 13
                Caption = 'N'#186
                FocusControl = txtNUMERO
              end
              object Label12: TLabel
                Left = 530
                Top = 95
                Width = 33
                Height = 13
                Caption = 'Cidade'
              end
              object Label79: TLabel
                Left = 984
                Top = 112
                Width = 200
                Height = 13
                Caption = 'Add o endere'#231'o para entrega dos cart'#245'es.'
              end
              object lblAddBairro: TLabel
                Left = 920
                Top = 85
                Width = 27
                Height = 26
                Caption = 'Add'#13#10'Bairro'
              end
              object lbl13: TLabel
                Left = 88
                Top = 95
                Width = 70
                Height = 13
                Caption = 'Tipo Endere'#231'o'
                FocusControl = DBEdit4
              end
              object DBEdit1: TDBEdit
                Left = 8
                Top = 30
                Width = 65
                Height = 21
                Hint = 'C'#243'digo da empresa'
                TabStop = False
                CharCase = ecUpperCase
                Color = clBtnFace
                DataField = 'EMPRES_ID'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clMaroon
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                PopupMenu = PopupEmpres_ID
                TabOrder = 1
              end
              object dbEdtNm: TDBEdit
                Left = 78
                Top = 30
                Width = 371
                Height = 21
                Hint = 'Raz'#227'o social da empresa'
                CharCase = ecUpperCase
                DataField = 'NOME'
                DataSource = DSCadastro
                MaxLength = 59
                TabOrder = 2
              end
              object DBEdit14: TDBEdit
                Left = 454
                Top = 30
                Width = 299
                Height = 21
                Hint = 'Nome da empresa que ser'#225' exibido no cart'#227'o'
                CharCase = ecUpperCase
                DataField = 'NOMECARTAO'
                DataSource = DSCadastro
                MaxLength = 30
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
              end
              object DBEdit13: TDBEdit
                Left = 540
                Top = 70
                Width = 149
                Height = 21
                Hint = 'Nome do representante da empresa'
                CharCase = ecUpperCase
                DataField = 'REPRESENTANTE'
                DataSource = DSCadastro
                TabOrder = 9
              end
              object DBCheckBox4: TDBCheckBox
                Left = 762
                Top = 59
                Width = 92
                Height = 17
                Hint = 
                  'Ao pegar autoriza'#231#227'o pelo URA perguntar '#13#10'se '#233' com receita ou se' +
                  'm receita.'
                Caption = 'Pedir Receita'
                DataField = 'PEDE_REC'
                DataSource = DSCadastro
                TabOrder = 5
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBCheckBox5: TDBCheckBox
                Left = 762
                Top = 43
                Width = 136
                Height = 17
                Hint = 'Pedir n'#250'mero de nota fiscal no URA'#13#10'ao pegar uma autoriza'#231#227'o'
                Caption = 'Pedir N'#186' de Nota Fiscal'
                DataField = 'PEDE_NF'
                DataSource = DSCadastro
                TabOrder = 4
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBEdit20: TDBEdit
                Left = 8
                Top = 70
                Width = 274
                Height = 21
                Hint = 'Nome fantasia da empresa'
                CharCase = ecUpperCase
                DataField = 'FANTASIA'
                DataSource = DSCadastro
                TabOrder = 6
              end
              object DBCheckBox10: TDBCheckBox
                Left = 761
                Top = 26
                Width = 137
                Height = 17
                Hint = 'Marque caso a empresa solicite os produtos vendidos.'
                Caption = 'Solicitar Produtos'
                DataField = 'SOLICITA_PRODUTO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 0
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBEdit4: TDBEdit
                Left = 421
                Top = 70
                Width = 114
                Height = 21
                Hint = 'Inscri'#231#227'o Estadual'
                CharCase = ecUpperCase
                DataField = 'INSCRICAOEST'
                DataSource = DSCadastro
                TabOrder = 8
              end
              object DBEdit17: TDBEdit
                Left = 693
                Top = 70
                Width = 60
                Height = 21
                Hint = 'N'#250'mero de contrato do fornecedor'
                CharCase = ecUpperCase
                DataField = 'CONTRATO'
                DataSource = DSCadastro
                TabOrder = 10
              end
              object txtENDERECO: TDBEdit
                Left = 216
                Top = 110
                Width = 217
                Height = 21
                Hint = 'Endere'#231'o da empresa'
                CharCase = ecUpperCase
                DataField = 'ENDERECO'
                DataSource = DSCadastro
                TabOrder = 14
              end
              object txtCEP: TDBEdit
                Left = 8
                Top = 110
                Width = 81
                Height = 21
                Hint = 'Cep'
                CharCase = ecUpperCase
                DataField = 'CEP'
                DataSource = DSCadastro
                TabOrder = 12
                OnExit = txtCEPExit
              end
              object txtNUMERO: TDBEdit
                Left = 437
                Top = 110
                Width = 36
                Height = 21
                DataField = 'NUMERO'
                DataSource = DSCadastro
                TabOrder = 15
              end
              object lkpCIDADE: TDBLookupComboBox
                Left = 529
                Top = 110
                Width = 160
                Height = 21
                DataField = 'CIDADE'
                DataSource = DSCadastro
                KeyField = 'CID_ID'
                ListField = 'NOME'
                ListSource = dsCidades
                TabOrder = 17
              end
              object lkpESTADO: TDBLookupComboBox
                Left = 479
                Top = 110
                Width = 47
                Height = 21
                DataField = 'ESTADO'
                DataSource = DSCadastro
                KeyField = 'ESTADO_ID'
                ListField = 'UF'
                ListSource = dsEstados
                TabOrder = 16
              end
              object btnAddEndereco: TBitBtn
                Left = 952
                Top = 104
                Width = 25
                Height = 25
                Hint = 'Adicionar o mesmo endere'#231'o para a entrega dos cart'#245'es.'
                TabOrder = 11
                OnClick = btnAddEnderecoClick
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D984D11
                  76111176114D984DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF13791354BA5454BA54137913FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF177D1757
                  BD5757BD57177D17FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF1C821C5BC15B5BC15B1C821CFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22882260
                  C76060C760228822FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5EAA5E
                  2C922C319731379D373EA43E43A94365CC6565CC6543A9433EA43E379D373197
                  312C922C5EAA5EFFFFFFFFFFFF2F952F6BD26B6BD26B6BD26B6BD26B6BD26B6B
                  D26B6BD26B6BD26B6BD26B6BD26B6BD26B6BD26B2F952FFFFFFFFFFFFF359B35
                  87EE8787EE8787EE8787EE877CE37C70D77070D7707CE37C87EE8787EE8787EE
                  8787EE87359B35FFFFFFFFFFFF6DB96D3FA53F45AB454BB14B50B65055BB5576
                  DD7676DD7655BB5550B6504BB14B45AB453FA53F6DB96DFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF42A8427BE27B7BE27B42A842FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48AE4880
                  E78080E78048AE48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF4DB34D84EB8484EB844DB34DFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF51B75187
                  EE8787EE8751B751FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFF7FCB7F54BA5454BA547FCB7FFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              object dbLkpBairros: TDBLookupComboBox
                Left = 696
                Top = 110
                Width = 225
                Height = 21
                DataField = 'BAIRRO'
                DataSource = DSCadastro
                KeyField = 'BAIRRO_ID'
                ListField = 'DESCRICAO'
                ListSource = DSBairros
                NullValueKey = 46
                TabOrder = 18
              end
              object btnAdicionaBairro: TBitBtn
                Left = 923
                Top = 112
                Width = 17
                Height = 17
                Hint = 'Adiciona Bairro|Bot'#227'o que adiciona um novo bairro '#224' listagem.'
                Enabled = False
                TabOrder = 19
                OnClick = btnAdicionaBairroClick
                Glyph.Data = {
                  5A030000424D5A030000000000005A0100002800000020000000100000000100
                  08000000000000020000232E0000232E00004900000000000000117611001379
                  1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
                  37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
                  4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
                  6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
                  7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
                  840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
                  C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
                  D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
                  DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
                  4848484848484848484848484848484848484848484848484848484848484848
                  484848480C00000C484848484848484848484848302323304848484848484848
                  4848484801161601484848484848484848484848243A3A244848484848484848
                  4848484802181802484848484848484848484848253C3C254848484848484848
                  48484848031A1A03484848484848484848484848263D3D264848484848484848
                  48484848041B1B04484848484848484848484848273F3F274848484848484813
                  0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
                  1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
                  2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
                  0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
                  484848480D21210D484848484848484848484848324444324848484848484848
                  4848484810282810484848484848484848484848354545354848484848484848
                  4848484812292912484848484848484848484848374646374848484848484848
                  48484848152A2A15484848484848484848484848394747394848484848484848
                  484848481E16161E484848484848484848484848423A3A424848484848484848
                  484848484848484848484848484848484848484848484848484848484848}
                NumGlyphs = 2
              end
              object dbEdtCNPJ: TJvDBMaskEdit
                Left = 288
                Top = 70
                Width = 119
                Height = 21
                DataField = 'CGC'
                DataSource = DSCadastro
                MaxLength = 18
                TabOrder = 7
                EditMask = '00.000.000/0000-00'
              end
              object dbLkpTipoEndereco: TDBLookupComboBox
                Left = 89
                Top = 110
                Width = 120
                Height = 21
                DataField = 'TIPO_ENDERECO'
                DataSource = DSCadastro
                KeyField = 'TIPO_ENDERECO'
                ListField = 'TIPO_ENDERECO'
                ListSource = DSTipoEndereco
                TabOrder = 13
              end
            end
            object GroupBox3: TGroupBox
              Left = 0
              Top = 153
              Width = 1217
              Height = 231
              Align = alTop
              Caption = 'Outras informa'#231#245'es'
              TabOrder = 1
              object Label52: TLabel
                Left = 456
                Top = 105
                Width = 192
                Height = 13
                Caption = 'Empresa Indicada pelo Estabelecimento:'
              end
              object Label53: TLabel
                Left = 661
                Top = 105
                Width = 90
                Height = 13
                Caption = 'Repasse Comiss'#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label21: TLabel
                Left = 8
                Top = 105
                Width = 74
                Height = 13
                Caption = 'Dia de Fech.'
                FocusControl = dbEdtDiaFechamento
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label22: TLabel
                Left = 95
                Top = 105
                Width = 75
                Height = 13
                Caption = 'Dia de Venc.'
                FocusControl = dbEdtDiaVenc
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object lblModalidade: TLabel
                Left = 178
                Top = 105
                Width = 124
                Height = 13
                Caption = 'Modalidade de Venda'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label17: TLabel
                Left = 883
                Top = 142
                Width = 58
                Height = 13
                Caption = 'Desc. Func.'
                FocusControl = DBEdit15
                Visible = False
              end
              object Label18: TLabel
                Left = 947
                Top = 142
                Width = 69
                Height = 13
                Caption = 'Repasse Emp.'
                FocusControl = DBEdit16
                Visible = False
              end
              object Label35: TLabel
                Left = 817
                Top = 142
                Width = 55
                Height = 13
                Caption = 'Desc. Emp.'
                FocusControl = DBEdit21
                Visible = False
              end
              object Label50: TLabel
                Left = 1023
                Top = 142
                Width = 64
                Height = 13
                Caption = 'Limite Padr'#227'o'
                FocusControl = DBEdit33
                Visible = False
              end
              object Label31: TLabel
                Left = 8
                Top = 146
                Width = 51
                Height = 13
                Caption = 'Bandeira'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label51: TLabel
                Left = 168
                Top = 146
                Width = 88
                Height = 13
                Caption = 'Tipo de Cr'#233'dito'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label55: TLabel
                Left = 312
                Top = 145
                Width = 73
                Height = 13
                Caption = 'Dia Repasse'
                FocusControl = dbEdtDiaRepasse
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label58: TLabel
                Left = 400
                Top = 145
                Width = 145
                Height = 13
                Caption = 'Respons'#225'vel pelo Fecha.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label59: TLabel
                Left = 560
                Top = 145
                Width = 83
                Height = 13
                Caption = 'Modelo Cart'#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label83: TLabel
                Left = 8
                Top = 185
                Width = 97
                Height = 13
                Caption = 'Taxa Adm/ Empresa'
                FocusControl = dbEdtDiaFechamento
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label89: TLabel
                Left = 408
                Top = 185
                Width = 87
                Height = 13
                Caption = 'Taxa Conta Digital'
                FocusControl = dbEdtDiaFechamento
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label90: TLabel
                Left = 328
                Top = 185
                Width = 27
                Height = 13
                Caption = 'Filial'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label91: TLabel
                Left = 120
                Top = 185
                Width = 83
                Height = 13
                Caption = 'Limite Max./Conv'
                FocusControl = dbEdtDiaFechamento
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object Label93: TLabel
                Left = 224
                Top = 185
                Width = 95
                Height = 26
                Caption = 'Porcent Max./Limite'#13#10
                FocusControl = dbEdtDiaFechamento
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object cbbCredInd: TJvDBLookupCombo
                Left = 498
                Top = 120
                Width = 157
                Height = 21
                DataField = 'CRED_ID'
                DataSource = DSCadastro
                DisplayEmpty = 'SEM INDICA'#199#195'O'
                LookupField = 'CRED_ID'
                LookupDisplay = 'NOME'
                LookupSource = dsListaCred
                TabOrder = 36
                OnChange = cbbCredIndChange
              end
              object DBEdit34: TDBEdit
                Left = 660
                Top = 120
                Width = 92
                Height = 21
                DataField = 'COMISSAO_CRED'
                DataSource = DSCadastro
                TabOrder = 37
                OnKeyPress = DBEdit34KeyPress
              end
              object EdCredInd: TEdit
                Left = 457
                Top = 120
                Width = 36
                Height = 21
                TabOrder = 35
                OnChange = EdCredIndChange
                OnKeyPress = EdCredIndKeyPress
              end
              object dbEdtDiaFechamento: TDBEdit
                Left = 8
                Top = 120
                Width = 81
                Height = 21
                CharCase = ecUpperCase
                DataField = 'FECHAMENTO1'
                DataSource = DSCadastro
                TabOrder = 32
              end
              object dbEdtDiaVenc: TDBEdit
                Left = 94
                Top = 120
                Width = 79
                Height = 21
                CharCase = ecUpperCase
                DataField = 'VENCIMENTO1'
                DataSource = DSCadastro
                TabOrder = 33
              end
              object cbbModalidade: TJvDBComboBox
                Left = 178
                Top = 120
                Width = 274
                Height = 21
                Hint = 
                  'Modalidade a qual a empresa pertence se CONVENIO ela '#13#10'poder'#225' pa' +
                  'rticipar de programas de desconto e ao mesmo '#13#10'tempo pegar autor' +
                  'iza'#231#245'es se PROGRAMA DE DESCONTO '#13#10'a empresa participa de program' +
                  'as de desconto mas n'#227'o '#13#10'poder'#225' pegar autoriza'#231#245'es'
                DataField = 'PROG_DESC'
                DataSource = DSCadastro
                Items.Strings = (
                  'CONVENIO'
                  'PROGRAMA DE DESCONTO')
                TabOrder = 34
                Values.Strings = (
                  'N'
                  'S')
                ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                ListSettings.OutfilteredValueFont.Color = clRed
                ListSettings.OutfilteredValueFont.Height = -11
                ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
                ListSettings.OutfilteredValueFont.Style = []
              end
              object DBCheckBox8: TDBCheckBox
                Left = 8
                Top = 33
                Width = 89
                Height = 17
                Hint = 'Marcar caso a empresa participe do cart'#227'o fidelidade'
                Caption = 'Fidelidade'
                DataField = 'FIDELIDADE'
                DataSource = DSCadastro
                TabOrder = 7
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object DBCheckBox1: TDBCheckBox
                Left = 8
                Top = 16
                Width = 89
                Height = 17
                Hint = 'Empresa liberada para compra'
                Caption = 'Liberada'
                DataField = 'LIBERADA'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBCheckBox2: TDBCheckBox
                Left = 104
                Top = 16
                Width = 185
                Height = 17
                Hint = 
                  'Empresa aceita todos os segmentos'#13#10'(Se essa op'#231#227'o estiver habili' +
                  'tada, n'#227'o '#13#10#233' necess'#225'rio marcar quais segmentos '#13#10'na aba "Segmen' +
                  'to")'
                Caption = 'Aceita todos os segmentos'
                DataField = 'TODOS_SEGMENTOS'
                DataSource = DSCadastro
                TabOrder = 1
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox6: TDBCheckBox
                Left = 104
                Top = 33
                Width = 185
                Height = 17
                Caption = 'Aceita todas as formas de pagto'
                DataField = 'ACEITA_PARC'
                DataSource = DSCadastro
                TabOrder = 8
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox3: TDBCheckBox
                Left = 297
                Top = 16
                Width = 142
                Height = 17
                Hint = 'Bloquear empresa at'#233' que o pagamento seja efetuado'
                Caption = 'Bloqueia at'#233' pagamento'
                DataField = 'BLOQ_ATE_PGTO'
                DataSource = DSCadastro
                TabOrder = 2
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox9: TDBCheckBox
                Left = 104
                Top = 50
                Width = 144
                Height = 17
                Hint = 
                  'Permite que as farm'#225'cias fa'#231'am vendas '#13#10'para esta empresa pelo n' +
                  'ome do funcion'#225'rio.'#13#10'(Se essa op'#231#227'o estiver habilitada, n'#227'o '#233#13#10'n' +
                  'ecess'#225'rio marcar a empresa na aba "Venda Nome")'
                Caption = 'Habilita venda por nome'
                DataField = 'VENDA_NOME'
                DataSource = DSCadastro
                TabOrder = 16
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBEdit15: TDBEdit
                Left = 882
                Top = 157
                Width = 60
                Height = 21
                Hint = 'Valor a descontar dos conveniados'
                CharCase = ecUpperCase
                DataField = 'DESCONTO_FUNC'
                DataSource = DSCadastro
                TabOrder = 39
                Visible = False
              end
              object DBEdit16: TDBEdit
                Left = 948
                Top = 157
                Width = 69
                Height = 21
                Hint = 'Vaor de repasse para empresa'
                CharCase = ecUpperCase
                DataField = 'REPASSE_EMPRESA'
                DataSource = DSCadastro
                TabOrder = 40
                Visible = False
              end
              object DBEdit21: TDBEdit
                Left = 816
                Top = 157
                Width = 60
                Height = 21
                Hint = 'Valor a descontar da empresa'
                CharCase = ecUpperCase
                DataField = 'DESCONTO_EMP'
                DataSource = DSCadastro
                TabOrder = 38
                Visible = False
              end
              object DBEdit33: TDBEdit
                Left = 1023
                Top = 157
                Width = 89
                Height = 21
                Hint = 
                  'Valor padr'#227'o que '#233' usado no cadastro do conveniado.'#13#10'Esse valor ' +
                  'aparece como limite ao selecionar a empresa'#13#10'no cadastro do conv' +
                  'eniado.'
                DataField = 'LIMITE_PADRAO'
                DataSource = DSCadastro
                TabOrder = 41
                Visible = False
              end
              object DBCheckBox11: TDBCheckBox
                Left = 8
                Top = 50
                Width = 93
                Height = 15
                Hint = 'Marcar caso a empresa participe do cart'#227'o fidelidade'
                Caption = 'Vale Desconto'
                DataField = 'VALE_DESCONTO'
                DataSource = DSCadastro
                TabOrder = 15
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object DBCheckBox14: TDBCheckBox
                Left = 297
                Top = 33
                Width = 142
                Height = 17
                Hint = 'Bloquear empresa at'#233' que o pagamento seja efetuado'
                Caption = 'Empresa solicita NF'
                DataField = 'EMITE_NF'
                DataSource = DSCadastro
                TabOrder = 9
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object dbLkpCbBandeiras: TDBLookupComboBox
                Left = 8
                Top = 162
                Width = 145
                Height = 21
                DataField = 'BAND_ID'
                DataSource = DSCadastro
                KeyField = 'BAND_ID'
                ListField = 'DESCRICAO'
                ListSource = dsBandeiras
                TabOrder = 45
              end
              object cbbTipoCredito: TJvDBComboBox
                Left = 168
                Top = 162
                Width = 137
                Height = 21
                DataField = 'TIPO_CREDITO'
                DataSource = DSCadastro
                Items.Strings = (
                  'Rotativo'
                  'Credi'#225'rio'
                  'Alimenta'#231#227'o'
                  'Refei'#231#227'o'
                  'Cantinex'
                  'Bem Estar')
                TabOrder = 46
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5')
                ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                ListSettings.OutfilteredValueFont.Color = clRed
                ListSettings.OutfilteredValueFont.Height = -11
                ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
                ListSettings.OutfilteredValueFont.Style = []
              end
              object dbEdtDiaRepasse: TDBEdit
                Left = 312
                Top = 160
                Width = 81
                Height = 21
                CharCase = ecUpperCase
                DataField = 'DIA_REPASSE'
                DataSource = DSCadastro
                TabOrder = 42
              end
              object DBCheckBox18: TDBCheckBox
                Left = 297
                Top = 50
                Width = 152
                Height = 17
                Hint = 'Bloquear empresa at'#233' que o pagamento seja efetuado'
                Caption = 'Obriga senha conveniado'
                DataField = 'OBRIGA_SENHA'
                DataSource = DSCadastro
                TabOrder = 17
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox19: TDBCheckBox
                Left = 8
                Top = 66
                Width = 93
                Height = 15
                Hint = 'Marcar caso a empresa participe do cart'#227'o fidelidade'
                Caption = 'Utiliza recarga'
                DataField = 'UTILIZA_RECARGA'
                DataSource = DSCadastro
                TabOrder = 22
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object dbLkpRespFechamento: TJvDBLookupCombo
                Left = 400
                Top = 160
                Width = 157
                Height = 21
                DataField = 'RESPONSAVEL_FECHAMENTO'
                DataSource = DSCadastro
                LookupField = 'NOME'
                LookupDisplay = 'NOME'
                LookupSource = dsOperadores
                TabOrder = 43
              end
              object JvDBLookupCombo2: TJvDBLookupCombo
                Left = 560
                Top = 160
                Width = 192
                Height = 21
                DataField = 'MOD_CART_ID'
                DataSource = DSCadastro
                LookupField = 'MOD_CART_ID'
                LookupDisplay = 'DESCRICAO'
                LookupSource = dsModelosCartoes
                TabOrder = 44
              end
              object DBCheckBox20: TDBCheckBox
                Left = 464
                Top = 50
                Width = 337
                Height = 15
                Hint = 'Marcar caso empresa n'#227'o considerar saldo acumulado'
                Caption = 'N'#227'o gerar saldo acumulado na renova'#231#227'o de cr'#233'dito (alimenta'#231#227'o)'
                DataField = 'GERA_ACUMULADO'
                DataSource = DSCadastro
                TabOrder = 18
                ValueChecked = 'N'
                ValueUnchecked = 'S'
                OnClick = DBCheckBox7Click
              end
              object DBCheckBox21: TDBCheckBox
                Left = 296
                Top = 68
                Width = 93
                Height = 15
                Hint = 'Marcar caso a empresa gerar arquivo TXT'
                Caption = 'Exporta TXT'
                DataField = 'EXPORTA_TXT'
                DataSource = DSCadastro
                TabOrder = 27
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox22: TDBCheckBox
                Left = 464
                Top = 36
                Width = 273
                Height = 15
                Hint = 'Marcar caso a empresa realizar lan'#231'amento de credito alimenta'#231#227'o'
                Caption = 'Realiza lan'#231'amento de cr'#233'dito via site (alimenta'#231#227'o)'
                DataField = 'REALIZA_LANC_CREDITO'
                DataSource = DSCadastro
                TabOrder = 12
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object dbchkUSA_PARCELAMENTO_ESPECIFICO: TDBCheckBox
                Left = 464
                Top = 20
                Width = 177
                Height = 15
                Hint = 'Marcar caso a empresa use parcelamento especifico'
                Caption = 'Permite parcelamento espec'#237'fico'
                DataField = 'USA_PARCELAMENTO_ESPECIFICO'
                DataSource = DSCadastro
                TabOrder = 5
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox24: TDBCheckBox
                Left = 104
                Top = 66
                Width = 144
                Height = 17
                Hint = 
                  'Permite que a empresa aplique um valor m'#225'ximo '#13#10'padr'#227'o no limite' +
                  ' de todos os conveniados da empresa.'
                Caption = 'Utiliza Valor Max. de Limite'
                DataField = 'USA_LIMITE_MAX'
                DataSource = DSCadastro
                TabOrder = 23
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object edtLimiteMax: TDBEdit
                Left = 120
                Top = 200
                Width = 97
                Height = 21
                CharCase = ecUpperCase
                DataField = 'LIMITE_MAX_POR_CONV'
                DataSource = DSCadastro
                TabOrder = 48
              end
              object DBCheckBox25: TDBCheckBox
                Left = 464
                Top = 66
                Width = 153
                Height = 15
                Hint = 
                  'Marcar caso empresa n'#227'o possua efeito financeiro(empresas de tes' +
                  'te).'
                Caption = 'N'#227'o gerar valor financeiro'
                DataField = 'TEM_VALOR_FINANCEIRO'
                DataSource = DSCadastro
                TabOrder = 24
                ValueChecked = 'N'
                ValueUnchecked = 'S'
                OnClick = DBCheckBox7Click
              end
              object DBCheckBox26: TDBCheckBox
                Left = 808
                Top = 20
                Width = 121
                Height = 17
                Hint = 
                  'Permite que a empresa alterar o limite dos conveniados via site ' +
                  'ou n'#227'o caso o checkbox '#13#10'esteja desmarcado.'
                Caption = 'Altera Limite Via Site'
                DataField = 'ALTERA_LIMITE_SITE'
                DataSource = DSCadastro
                TabOrder = 6
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox27: TDBCheckBox
                Left = 808
                Top = 36
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa alterar o limite dos conveniados via site ' +
                  'ou n'#227'o caso o checkbox '#13#10'esteja desmarcado.'
                Caption = 'Obriga receita m'#233'dica'
                DataField = 'OBRIGA_RECEITA_MEDICA'
                DataSource = DSCadastro
                TabOrder = 13
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object edtPorcentLimiteMaximo: TDBEdit
                Left = 224
                Top = 200
                Width = 97
                Height = 21
                CharCase = ecUpperCase
                DataField = 'PORCENT_LIMITEMAXIMO'
                DataSource = DSCadastro
                TabOrder = 49
              end
              object DBCheckBox28: TDBCheckBox
                Left = 808
                Top = 98
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa alterar o limite dos conveniados via site ' +
                  'ou n'#227'o caso o checkbox '#13#10'esteja desmarcado.'
                Caption = 'Utiliza cart'#227'o novo'
                DataField = 'USA_NOVO_CARTAO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 31
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                Visible = False
              end
              object DBCheckBox29: TDBCheckBox
                Left = 808
                Top = 66
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa vizualize o relat'#243'rio do alimenta'#231#227'o no si' +
                  'te.'
                Caption = 'Visualiza Relat'#243'rio Alimenta'#231#227'o Site'
                DataField = 'ACESSA_RELATORIO_ALIMENTACAO'
                DataSource = DSCadastro
                TabOrder = 25
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox30: TDBCheckBox
                Left = 1008
                Top = 66
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa compre com um desconto diferenciado, por'#233'm' +
                  ' o estabelecimento tem que estar liberado na aba Emp/Cred Descon' +
                  'to Especial.'
                Caption = 'Utiliza Desconto Especial'
                DataField = 'USA_DESCONTO_ESPECIAL'
                DataSource = DSCadastro
                TabOrder = 26
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox31: TDBCheckBox
                Left = 1008
                Top = 18
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa compre com um desconto diferenciado, por'#233'm' +
                  ' o estabelecimento tem que estar liberado na aba Emp/Cred Descon' +
                  'to Especial.'
                Caption = 'SAP- Trans Simultanea'
                DataField = 'TRANS_SIMULTANEA'
                DataSource = DSCadastro
                TabOrder = 3
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox32: TDBCheckBox
                Left = 1008
                Top = 34
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa compre com um desconto diferenciado, por'#233'm' +
                  ' o estabelecimento tem que estar liberado na aba Emp/Cred Descon' +
                  'to Especial.'
                Caption = 'SAP- Empresa Principal'
                DataField = 'EMPRESA_PRINC'
                DataSource = DSCadastro
                TabOrder = 10
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox33: TDBCheckBox
                Left = 1008
                Top = 50
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa compre com um desconto diferenciado, por'#233'm' +
                  ' o estabelecimento tem que estar liberado na aba Emp/Cred Descon' +
                  'to Especial.'
                Caption = 'SAP- Trans '#218'nica'
                DataField = 'TRANS_UNICA'
                DataSource = DSCadastro
                TabOrder = 20
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox34: TDBCheckBox
                Left = 1160
                Top = 18
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa compre com um desconto diferenciado, por'#233'm' +
                  ' o estabelecimento tem que estar liberado na aba Emp/Cred Descon' +
                  'to Especial.'
                Caption = 'SAP- Ativa Sap'
                DataField = 'ATIVA_SAP'
                DataSource = DSCadastro
                TabOrder = 4
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object dbchkACESSA_CADASTRO_SITE: TDBCheckBox
                Left = 1160
                Top = 34
                Width = 145
                Height = 17
                Hint = 
                  'Permite que a empresa visualize a tela de cadastro de conveniado' +
                  's no site.'
                Caption = 'Libera o Cadastro no Site'
                DataField = 'ACESSA_CADASTRO_SITE'
                DataSource = DSCadastro
                TabOrder = 11
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox35: TDBCheckBox
                Left = 1160
                Top = 48
                Width = 89
                Height = 17
                Hint = 'Empresa liberada para compra'
                Caption = 'Apagado'
                DataField = 'APAGADO'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 14
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object lkpFilial: TJvDBLookupCombo
                Left = 328
                Top = 200
                Width = 73
                Height = 21
                DataField = 'FILIAL'
                DataSource = DSCadastro
                LookupField = 'FILIAL_ID'
                LookupDisplay = 'DESCRICAO'
                LookupSource = DSFilial
                TabOrder = 50
              end
              object dbAceitaPerfumaria: TDBCheckBox
                Left = 1160
                Top = 64
                Width = 177
                Height = 17
                Hint = 'Verifica se libera todos medicamentos regras de conv'#234'nio'
                Caption = 'Aceita Perfumaria - Regra Convenio'
                DataField = 'ACEITA_PERFUMARIA'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 21
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object edtTaxaAdmEmp: TDBEdit
                Left = 8
                Top = 200
                Width = 105
                Height = 21
                CharCase = ecUpperCase
                DataField = 'VALOR_TAXA_EMPRESA'
                DataSource = DSCadastro
                TabOrder = 47
              end
              object DBCheckBox36: TDBCheckBox
                Left = 808
                Top = 50
                Width = 177
                Height = 17
                Hint = 
                  'Aplica uma taxa a taxa administrativa para os conveniados da emp' +
                  'resa que tiveram movimenta'#231#227'o. O valor da taxa '#13#10'deve ser setado' +
                  ' no campo Taxa Adm / Empresa'
                Caption = 'Aplicar Taxa Adm/ Empresa'
                DataField = 'LANCA_TAXA_EMPRESA'
                DataSource = DSCadastro
                TabOrder = 19
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object dbchkACEITA_PERFUMARIA: TDBCheckBox
                Left = 8
                Top = 84
                Width = 98
                Height = 17
                Hint = 
                  'Verifica se a empresa vai validar ou n'#227'o o CPF nas altera'#231#245'es re' +
                  'alizadas via site.'
                Caption = 'Valida CPF'
                DataField = 'VALIDA_CPF'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 29
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object DBCheckBox37: TDBCheckBox
                Left = 104
                Top = 84
                Width = 98
                Height = 17
                Hint = 
                  'Verifica se a empresa vai validar ou n'#227'o o CVV na hora de trocar' +
                  ' senha pela URA'
                Caption = 'Valida CVV'
                DataField = 'VALIDA_CVV'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 30
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
              object chkTRANSFERE_LIMITE: TDBCheckBox
                Left = 296
                Top = 82
                Width = 225
                Height = 15
                Hint = 
                  'Marcar quando quiser lan'#231'ar d'#233'bitos sem verificar limite dos con' +
                  'veniados dessa empresa.'
                Caption = 'Permite Lanc. D'#233'bito s/ Cons Saldo'
                DataField = 'LANCA_DEBITO_S_CONSULTA'
                DataSource = DSCadastro
                TabOrder = 28
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object DBEdit2: TDBEdit
                Left = 408
                Top = 200
                Width = 97
                Height = 21
                CharCase = ecUpperCase
                DataField = 'taxa_conta_digital'
                DataSource = DSCadastro
                TabOrder = 51
              end
              object DBCheckBox38: TDBCheckBox
                Left = 808
                Top = 82
                Width = 153
                Height = 15
                Hint = 'Marcar caso empresa permita transferencia para a conta digital'
                Caption = 'Transfere Limite'
                DataField = 'TRANSFERE_LIMITE'
                DataSource = DSCadastro
                TabOrder = 52
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object dbchkLiberaBloqueio: TDBCheckBox
                Left = 808
                Top = 114
                Width = 177
                Height = 17
                Hint = 
                  'Permite que a empresa bloqueie seus funcion'#225'rios caso o CHKBox e' +
                  'steja selecionado'
                Caption = 'Libera bloqueio pelo site'
                DataField = 'libera_bloqueio'
                DataSource = DSCadastro
                TabOrder = 53
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 471
              Width = 1217
              Height = 55
              Align = alBottom
              Caption = 'Informa'#231#245'es Adicionais/Dados da '#250'ltima altera'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object Label20: TLabel
                Left = 8
                Top = 15
                Width = 141
                Height = 13
                Caption = 'Data da Altera'#231#227'o / Operador'
                FocusControl = DBEdit35
              end
              object Label54: TLabel
                Left = 288
                Top = 15
                Width = 138
                Height = 13
                Caption = 'Data do Cadastro / Operador'
                FocusControl = DBEdit36
              end
              object DBEdit35: TDBEdit
                Left = 8
                Top = 30
                Width = 121
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTALTERACAO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 0
              end
              object DBEdit36: TDBEdit
                Left = 288
                Top = 30
                Width = 105
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'DTCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 2
              end
              object DBEdit37: TDBEdit
                Left = 397
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERCADASTRO'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 3
              end
              object DBEdit38: TDBEdit
                Left = 137
                Top = 30
                Width = 100
                Height = 21
                TabStop = False
                Color = clBtnFace
                DataField = 'OPERADOR'
                DataSource = DSCadastro
                Enabled = False
                TabOrder = 1
              end
            end
            object GroupBox11: TGroupBox
              Left = 0
              Top = 384
              Width = 1217
              Height = 65
              Align = alTop
              Caption = 'Endere'#231'o de Entrega dos Cart'#245'es '
              TabOrder = 3
              object Label74: TLabel
                Left = 232
                Top = 16
                Width = 54
                Height = 13
                Caption = 'Logradouro'
              end
              object Label75: TLabel
                Left = 517
                Top = 16
                Width = 12
                Height = 13
                Caption = 'N'#186
              end
              object Label76: TLabel
                Left = 800
                Top = 16
                Width = 27
                Height = 13
                Caption = 'Bairro'
              end
              object Label77: TLabel
                Left = 564
                Top = 16
                Width = 14
                Height = 13
                Caption = 'UF'
              end
              object Label78: TLabel
                Left = 620
                Top = 16
                Width = 33
                Height = 13
                Caption = 'Cidade'
              end
              object Label80: TLabel
                Left = 1072
                Top = 16
                Width = 30
                Height = 13
                Caption = 'C.E.P.'
                FocusControl = txtCEP
              end
              object lbl10: TLabel
                Left = 8
                Top = 16
                Width = 56
                Height = 13
                Caption = 'Destinat'#225'rio'
                FocusControl = txtCEP
              end
              object txtLogradouroCartao: TDBEdit
                Left = 232
                Top = 32
                Width = 273
                Height = 21
                DataField = 'LOGRADOURO_CARTAO'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object txtNumeroCartao: TDBEdit
                Left = 515
                Top = 32
                Width = 41
                Height = 21
                DataField = 'NUMERO_CARTAO'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object lkpCIDADE_CARTAO: TDBLookupComboBox
                Left = 618
                Top = 32
                Width = 175
                Height = 21
                DataField = 'CIDADE_CARTAO'
                DataSource = DSCadastro
                KeyField = 'CID_ID'
                ListField = 'NOME'
                ListSource = dsCidade_Cartao
                TabOrder = 4
              end
              object lkpUF_CARTAO: TDBLookupComboBox
                Left = 563
                Top = 32
                Width = 47
                Height = 21
                DataField = 'UF_CARTAO'
                DataSource = DSCadastro
                KeyField = 'ESTADO_ID'
                ListField = 'UF'
                ListSource = dsEstado_Cartao
                TabOrder = 3
              end
              object txtCepCartao: TDBEdit
                Left = 1072
                Top = 32
                Width = 97
                Height = 21
                DataField = 'CEP_CARTAO'
                DataSource = DSCadastro
                TabOrder = 6
              end
              object dblkbBAIRRO_CARTAO: TDBLookupComboBox
                Left = 796
                Top = 32
                Width = 269
                Height = 21
                DataField = 'BAIRRO_CARTAO'
                DataSource = DSCadastro
                KeyField = 'BAIRRO_ID'
                ListField = 'DESCRICAO'
                ListSource = dsBairro_Cartao
                NullValueKey = 46
                TabOrder = 5
              end
              object txtDESTINATARIO_CARTAO: TDBEdit
                Left = 8
                Top = 32
                Width = 217
                Height = 21
                DataField = 'DESTINATARIO_CARTAO'
                DataSource = DSCadastro
                TabOrder = 0
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Dados &Adicionais'
            ImageIndex = 1
            OnShow = TabSheet2Show
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 982
              Height = 61
              Align = alTop
              Caption = 'Telefones/Fax'
              TabOrder = 0
              object Label7: TLabel
                Left = 8
                Top = 15
                Width = 51
                Height = 13
                Caption = 'Telefone 1'
                FocusControl = DBEdit5
              end
              object Label8: TLabel
                Left = 104
                Top = 15
                Width = 51
                Height = 13
                Caption = 'Telefone 2'
                FocusControl = DBEdit6
              end
              object Label9: TLabel
                Left = 200
                Top = 15
                Width = 17
                Height = 13
                Caption = 'Fax'
                FocusControl = DBEdit7
              end
              object DBEdit5: TDBEdit
                Left = 8
                Top = 30
                Width = 90
                Height = 21
                Hint = 'Telefone para contato'
                CharCase = ecUpperCase
                DataField = 'TELEFONE1'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object DBEdit6: TDBEdit
                Left = 104
                Top = 30
                Width = 90
                Height = 21
                Hint = 'Telefone para contato'
                CharCase = ecUpperCase
                DataField = 'TELEFONE2'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit7: TDBEdit
                Left = 200
                Top = 30
                Width = 90
                Height = 21
                Hint = 'Fax da empresa'
                CharCase = ecUpperCase
                DataField = 'FAX'
                DataSource = DSCadastro
                TabOrder = 2
              end
            end
            object GroupBox7: TGroupBox
              Left = 0
              Top = 121
              Width = 982
              Height = 216
              Align = alTop
              Caption = 'Observa'#231#245'es'
              TabOrder = 2
              object Label26: TLabel
                Left = 8
                Top = 15
                Width = 357
                Height = 13
                Caption = 
                  'OBS1 ( Texto Escrito - Implica em bloqueio de venda no Sist. BEL' +
                  'LA Conv.)'
                FocusControl = DBEdit24
              end
              object Label27: TLabel
                Left = 378
                Top = 15
                Width = 357
                Height = 13
                Caption = 
                  'OBS2 ( Texto Escrito - Implica em bloqueio de venda no Sist. BEL' +
                  'LA Conv.)'
                FocusControl = DBEdit25
              end
              object Label28: TLabel
                Left = 8
                Top = 55
                Width = 28
                Height = 13
                Caption = 'OBS3'
                FocusControl = DBEdit26
              end
              object Label49: TLabel
                Left = 8
                Top = 95
                Width = 105
                Height = 13
                Caption = 'Obs. para fechamento'
              end
              object DBEdit24: TDBEdit
                Left = 8
                Top = 30
                Width = 361
                Height = 21
                Hint = 
                  'Observa'#231#227'o 1'#13#10'Implica em bloqueio de venda no Sistema BELLA Conv' +
                  '.'
                CharCase = ecUpperCase
                DataField = 'OBS1'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object DBEdit25: TDBEdit
                Left = 376
                Top = 30
                Width = 353
                Height = 21
                Hint = 
                  'Observa'#231#227'o 2'#13#10'Implica em bloqueio de venda no Sistema BELLA Conv' +
                  '.'
                CharCase = ecUpperCase
                DataField = 'OBS2'
                DataSource = DSCadastro
                TabOrder = 1
              end
              object DBEdit26: TDBEdit
                Left = 8
                Top = 70
                Width = 721
                Height = 21
                Hint = 'Observa'#231#227'o 3'
                CharCase = ecUpperCase
                DataField = 'OBS3'
                DataSource = DSCadastro
                TabOrder = 2
              end
              object DBMemo1: TDBMemo
                Left = 8
                Top = 112
                Width = 721
                Height = 89
                DataField = 'OBS_FECHAMENTO'
                DataSource = DSCadastro
                TabOrder = 3
              end
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 61
              Width = 982
              Height = 60
              Align = alTop
              Caption = 'Internet'
              TabOrder = 1
              object Label24: TLabel
                Left = 8
                Top = 15
                Width = 29
                Height = 13
                Caption = 'E-Mail'
                FocusControl = dbEdtEmail
              end
              object Label25: TLabel
                Left = 378
                Top = 15
                Width = 56
                Height = 13
                Caption = 'Home Page'
                FocusControl = DBEdit23
              end
              object dbEdtEmail: TDBEdit
                Left = 8
                Top = 30
                Width = 361
                Height = 21
                Hint = 'Email para contato com a empresa'
                CharCase = ecLowerCase
                DataField = 'EMAIL'
                DataSource = DSCadastro
                TabOrder = 0
              end
              object DBEdit23: TDBEdit
                Left = 376
                Top = 30
                Width = 353
                Height = 21
                CharCase = ecLowerCase
                DataField = 'HOMEPAGE'
                DataSource = DSCadastro
                TabOrder = 1
              end
            end
          end
          object TabCartEmp: TTabSheet
            Caption = 'Cart'#245'es da Empresa'
            ImageIndex = 2
            OnHide = TabCartEmpHide
            OnShow = TabCartEmpShow
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 1217
              Height = 60
              Align = alTop
              Caption = 'Cart'#245'es da Empresa '
              TabOrder = 0
              object Label37: TLabel
                Left = 159
                Top = 15
                Width = 114
                Height = 13
                Caption = 'Numera'#231#227'o dos Cart'#245'es'
              end
              object Label36: TLabel
                Left = 294
                Top = 24
                Width = 219
                Height = 26
                Caption = 
                  '(Obrigat'#243'rio n'#250'mero com 9 d'#237'gitos)'#13#10'(O primeiro d'#237'gito tem que s' +
                  'er maior que zero.) '
              end
              object DBCheckBox7: TDBCheckBox
                Left = 8
                Top = 37
                Width = 145
                Height = 17
                Hint = 'Marcar caso a empresa use numera'#231#227'o pr'#243'pria'
                Caption = 'Usa numera'#231#227'o pr'#243'pria'
                DataField = 'USA_CARTAO_PROPRIO'
                DataSource = DSCadastro
                TabOrder = 1
                ValueChecked = 'S'
                ValueUnchecked = 'N'
                OnClick = DBCheckBox7Click
              end
              object DBCart_Ini: TDBEdit
                Left = 159
                Top = 30
                Width = 129
                Height = 21
                Hint = 'Numera'#231#227'o de cart'#227'o'
                DataField = 'CARTAO_INI'
                DataSource = DSCadastro
                MaxLength = 9
                TabOrder = 0
                OnEnter = DBCart_IniEnter
                OnExit = DBCart_IniExit
              end
            end
            object grpProgDesc: TGroupBox
              Left = 0
              Top = 60
              Width = 1217
              Height = 39
              Align = alTop
              Caption = 'Programa de Desconto'
              TabOrder = 1
              object DBCheckBox12: TDBCheckBox
                Left = 8
                Top = 15
                Width = 529
                Height = 17
                Hint = 'Empresa liberada para compra'
                Caption = 'Autoriza inclus'#227'o de cart'#245'es pelo estabelecimento'
                DataField = 'INC_CART_PBM'
                DataSource = DSCadastro
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                ValueChecked = 'S'
                ValueUnchecked = 'N'
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 99
              Width = 1217
              Height = 61
              Align = alTop
              Caption = 'Empresa agenciada por:'
              TabOrder = 2
              object Label30: TLabel
                Left = 8
                Top = 15
                Width = 72
                Height = 13
                Caption = 'Agenciado por:'
              end
              object JvDBLookupCombo1: TJvDBLookupCombo
                Left = 8
                Top = 32
                Width = 289
                Height = 21
                DataField = 'AGENCIADOR_ID'
                DataSource = DSCadastro
                LookupField = 'AGENCIADOR_ID'
                LookupDisplay = 'NOME'
                LookupSource = daAgenciador
                TabOrder = 0
              end
            end
            object DBCheckBox13: TDBCheckBox
              Left = 0
              Top = 168
              Width = 345
              Height = 17
              Hint = 
                'Se esta op'#231#227'o estiver marcada, os conveniados desta empresa s'#243' '#13 +
                #10'poder'#227'o comprar produtos que est'#227'o cadastrados no programa de'#13#10 +
                'desconto, programas estes relacionados a essa empresa.'
              Caption = 'Aceita apenas produtos cadastrados em programas de desconto'
              DataField = 'SOM_PROD_PROG'
              DataSource = DSCadastro
              TabOrder = 3
              ValueChecked = 'S'
              ValueUnchecked = 'N'
            end
            object DBCheckBox15: TDBCheckBox
              Left = 0
              Top = 187
              Width = 345
              Height = 17
              Hint = 
                'Se esta op'#231#227'o estiver marcada, as vendas efetuadas com receita n' +
                #227'o ir'#227'o '#13#10'chegar o limite do conveniado.'#13#10'O sistema ir'#225' separar ' +
                'os produtos que possuem receita e pegar'#225' uma autoriza'#231#227'o'#13#10'separa' +
                'da para esses produtos, onde essa autoriza'#231#227'o n'#227'o passar'#225' pela v' +
                'alida'#231#227'o'#13#10'de limite do conveniado.'#13#10'Para os produtos sem receita' +
                ', a valida'#231#227'o ser'#225' feita normalmente;'
              Caption = 'Para as vendas com receita n'#227'o calcular limite.'
              DataField = 'RECEITA_SEM_LIMITE'
              DataSource = DSCadastro
              TabOrder = 4
              ValueChecked = 'S'
              ValueUnchecked = 'N'
            end
            object DBCheckBox16: TDBCheckBox
              Left = 0
              Top = 205
              Width = 345
              Height = 17
              Caption = 'Utilizar c'#243'd. Importa'#231#227'o'
              DataField = 'USA_COD_IMPORTACAO'
              DataSource = DSCadastro
              TabOrder = 5
              ValueChecked = 'S'
              ValueUnchecked = 'N'
            end
            object DBCheckBox17: TDBCheckBox
              Left = 0
              Top = 223
              Width = 345
              Height = 17
              Caption = 'N'#227'o Gerar cart'#227'o para menores de 18 anos'
              DataField = 'NAO_GERA_CARTAO_MENOR'
              DataSource = DSCadastro
              TabOrder = 6
              ValueChecked = 'S'
              ValueUnchecked = 'N'
            end
          end
          object tsAnexarContrato: TTabSheet
            Caption = 'Contrato'
            ImageIndex = 3
            OnEnter = tsAnexarContratoEnter
            OnShow = tsAnexarContratoShow
            object Label92: TLabel
              Left = 0
              Top = 24
              Width = 255
              Height = 16
              Caption = 'Selecione o arquivo a ser carregado'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object JvgLabel1: TJvgLabel
              Left = 8
              Top = 176
              Width = 61
              Height = 16
              Caption = 'Email origem'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              FontWeight = fwDONTCARE
              Options = [floActiveWhileControlFocused]
              Gradient.Active = False
              Gradient.Orientation = fgdHorizontal
              Alignment = taLeftJustify
            end
            object lblNome: TJvgLabel
              Left = 360
              Top = 176
              Width = 29
              Height = 16
              Caption = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              FontWeight = fwDONTCARE
              Options = [floActiveWhileControlFocused]
              Gradient.Active = False
              Gradient.Orientation = fgdHorizontal
              Alignment = taLeftJustify
            end
            object JvgLabel3: TJvgLabel
              Left = 8
              Top = 224
              Width = 65
              Height = 16
              Caption = 'Email Destino'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              FontWeight = fwDONTCARE
              Options = [floActiveWhileControlFocused]
              Gradient.Active = False
              Gradient.Orientation = fgdHorizontal
              Alignment = taLeftJustify
            end
            object JvgLabel4: TJvgLabel
              Left = 8
              Top = 272
              Width = 88
              Height = 16
              Caption = 'Assunto do email:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              FontWeight = fwDONTCARE
              Options = [floActiveWhileControlFocused]
              Gradient.Active = False
              Gradient.Orientation = fgdHorizontal
              Alignment = taLeftJustify
            end
            object FilenameEdit1: TJvFilenameEdit
              Left = 2
              Top = 44
              Width = 479
              Height = 21
              Filter = 'Arquivo PDF(*.pdf)|*.pdf'
              InitialDir = 'C:\'
              Enabled = False
              TabOrder = 0
              Text = 'C:\'
            end
            object btnAnexarContrato: TButton
              Left = 8
              Top = 72
              Width = 113
              Height = 41
              Caption = 'Anexar Contrato'
              Enabled = False
              TabOrder = 1
              OnClick = btnAnexarContratoClick
              OnEnter = AlteraoLineardeGrupodeProdutoLiberado1Click
            end
            object btnAbrirContrato: TBitBtn
              Left = 8
              Top = 120
              Width = 113
              Height = 41
              Caption = 'Abrir Contrato'
              TabOrder = 3
              OnClick = btnAbrirContratoClick
              Glyph.Data = {
                7E090000424D7E0900000000000036000000280000001D0000001B0000000100
                1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
                A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
                EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
                AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
                596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
                A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
                D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
                C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
                89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
                D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
                C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
                EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
                F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
                BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
                FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
                EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
                99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
                FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
                D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
                BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
                FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
                C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
                ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
                F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
                FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
                C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
                FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
                BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
                CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400}
            end
            object OleContainer1: TOleContainer
              Left = 144
              Top = 80
              Width = 81
              Height = 81
              Caption = 'OleContainer1'
              TabOrder = 2
            end
            object btnEnviarEmail: TBitBtn
              Left = 8
              Top = 392
              Width = 113
              Height = 41
              Caption = 'Enviar Email'
              TabOrder = 9
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFEBEAEBEEF0EFDFF0E8D9F3E6E4F0EAECE6E6E7E7E5FF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFE7E7E5E7E7E5EEF0EFA7E4C750CE9C34C68929C28440C98F74D7AAD1ED
                DEEBEAEBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE7E7E5E7
                E7E5E7E7E5E7E4DFE6E2DDE6E2DDEEF0EF6AD4A71AC0801AC08023C28343C685
                18BD7A18BD7A32C382B3E5C7E7E7E5FF00FFE4F0EAE0E6E4DFE2DFE0DCD6E0DC
                D6E0DCD6DFDEDBE6E2DDE6E2DDECE6E6ECE6E6F6F1F07BD4A318BD7A23C28323
                C283A8E8D0F4F7EA62C37B18BD7A20C0812ABF79C8E6D1E7E7E5DFDEDBD4CDC5
                D1CAC0D4CDC5DCD7D0E0DCD6E6E2DDE7E7E5EEECE8EEECE8F6F1F0CFEAD52ABF
                791AC0801AC0801AC08096E1C3FFFFFFF4F7EA62C37B18BD7A18BD7A65CC8FE0
                E6E4E7E7E5E6E2DDE6E2DDE0DCD6E0DCD6E0DCD6E7E4DFEEECE8EEECE8F6F1F0
                FDF5F88DD5A534C68950CE9C5CD2A36AD4A77AD7ADD9F3E6FFFFFFF5F7EB62C3
                7B23C28348C682D5DED4DFE2DFDCD7D0E6E2DDECE6E6EEECE8EEECE8EEECE8EE
                ECE8F6F1F0F8F7F6F8F7F669C68342CB95EAF7EEFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFF7EF69C68343C685C8E0CBE7E7E5E0DCD6E0DCD6E6E2DDE7E7
                E5EEECE8F8F7F6F8F7F6F8F7F6F8F7F6F8F7F675C78443C685B1E1BCCDEBD3CD
                EBD3C0E6C8E0F3E4FFFFFFFFFFFFBFDDB05AC07246C27BCADEC8FF00FFDFDEDB
                E6E2DDE6E2DDECE6E6EEECE8EEF0EFF8F7F6F8F7F6F8F7F6FFFAFEADD8A947AF
                4559B96159BE6D5AC07282CF95EAF7EEFFFFFFB6DBAA55B65855B65857B449D5
                DED4FF00FFE7E4DFE7E4DFECE6E6EEECE8F6F1F0F6F1F0F8F7F6F8F7F6FFFAFE
                FFFAFEE4ECDF54A5275CAC3A5CB6555BBA61DDF1E0FFFFFFB5D9A654B0495CB6
                554BA22098C67CE0E6E4FF00FFE7E7E5E7E7E5EEECE8EEECE8F6F1F0F8F7F6F8
                F7F6FFFFFFFFFFFFFFFFFFFFFFFFBFDDB0489B145AA72E5CAC3A80C067AED497
                59AA345CAC3A4E9C1D6AAD42D6DEDEFF00FFFF00FFE7E7E5E7E7E5EEECE8F6F1
                F0F6F1F0F8F7F6FFFFFFFFFFFFFFFFFFF6F1F0ABA6A0BFB8BAB5D9A65AA72E4E
                9A1C4E9A1C4E9A1C519D214C981978B157D5DED4E7E7E5FF00FFFF00FFFF00FF
                ECE6E6EEF0EFF6F1F0F8F7F6FFFAFEFFFAFEFFFAFEECE6E6948E85776F637B73
                68A39A99C6CCBFABCA9A7FB46473AD5490BD78C7DBBCEBEAEBECE6E6E7E7E5FF
                00FFFF00FFFF00FFEEECE8EEECE8F6F1F0F8F7F6FFFFFFFFFFFFDFDEDB8C857A
                7F786D8780758780758780758C857AA39A99B4ACABB4ACABB4ACABD0C8C7E0DC
                D6E7E7E5E7E7E5FF00FFFF00FFFF00FFEEECE8EEF0EFF6F1F0F8F7F6FFFFFFD8
                D5CF8A8377857E738C857A8C857A8C857A938B7F938B7F938B7F948E859D958A
                A69E93AAA398AFA99EC3BDB5D8D7D3FF00FFFF00FFFF00FFEBEAEBEEF0EFF8F7
                F6FFFFFFCCC7C08A83778A8377938B7F938B7F948E859D958AA69E93AAA398AF
                A99EB5AEA5B5AEA5B5AEA5B5AEA5B5AEA5C3BDB5D8D7D3FF00FFFF00FFFF00FF
                EBEAEBEEF0EFF8F7F6BEBAB38C857A948E859D958AA69E93AAA398AFA99EB5AE
                A5B5AEA5B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8C3BDB5CFCCC5E0E6E4FF
                00FFFF00FFFF00FFFF00FFEEECE8B8B2A89D958AAAA398AFA99EB5AEA5B5AEA5
                B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8C7C2B9CFCC
                C5E0E6E4FF00FFFF00FFFF00FFFF00FFFF00FFE7E7E5C3BDB5B8B2A8B5AEA5B5
                AEA5B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8
                C7C2B9CFCCC5E0E6E4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFEBEA
                EBE6E2DDD4CDC5C3BDB5B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8B2A8B8
                B2A8B8B2A8C7C2BBCFCCC5E0E6E4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFE7E7E5DFE2DFD8D5CFCCC7C0BEBAB3B8B2A8B8B2
                A8B8B2A8B8B2A8BCB6AEC7C2BBCFCCC5E0E6E4FF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE0E6E4DFDEDB
                D8D5CFCFCCC5C7C2B9BCB6AEBCB6AEC7C2BBCFCCC5E0E6E4FF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFE0E6E4D8D7D3D4CDC5CCC7C0CCC7C0CFCCC5E0E6E4FF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE4F0EADFDEDBD8D5CFE0
                E6E4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
            end
            object edtEmailOrigem: TJvEdit
              Left = 8
              Top = 192
              Width = 337
              Height = 21
              TabOrder = 4
            end
            object edtNomeOrigem: TJvEdit
              Left = 360
              Top = 192
              Width = 185
              Height = 21
              TabOrder = 5
            end
            object edtEmailDestino: TJvEdit
              Left = 8
              Top = 240
              Width = 337
              Height = 21
              TabOrder = 6
            end
            object edtAssunto: TJvEdit
              Left = 8
              Top = 288
              Width = 337
              Height = 21
              TabOrder = 7
            end
            object edtCorpo: TEdit
              Left = 8
              Top = 320
              Width = 553
              Height = 21
              TabOrder = 8
            end
          end
        end
      end
    end
    object Tabdatas: TTabSheet [2]
      Caption = '&Datas de Fech./Venc.'
      ImageIndex = 4
      OnShow = TabdatasShow
      object PageControl3: TPageControl
        Left = 0
        Top = 0
        Width = 1229
        Height = 593
        ActivePage = TabSheet3
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Datas'
          OnShow = TabSheet3Show
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 1221
            Height = 54
            Align = alTop
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object Label34: TLabel
              Left = 7
              Top = 10
              Width = 81
              Height = 13
              Caption = 'Selecionar o Ano'
            end
            object CBAno: TComboBox
              Left = 7
              Top = 25
              Width = 89
              Height = 21
              Hint = 'Selecione o ano para consulta'
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = CBAnoChange
              Items.Strings = (
                '2007'
                '2008'
                '2009'
                '2010'
                '2011'
                '2012'
                '2013'
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023')
            end
          end
          object Griddatas: TJvDBGrid
            Left = 0
            Top = 54
            Width = 1221
            Height = 440
            Hint = 
              'Clique em cima da data caso queire alterar um fechamento especif' +
              'ico'
            Align = alClient
            Ctl3D = True
            DataSource = DSDatasFecha
            DefaultDrawing = False
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnColExit = GriddatasColExit
            AutoAppend = False
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 17
            Columns = <
              item
                Expanded = False
                FieldName = 'DATA_FECHA'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 116
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESC_FECHAMENTO'
                Title.Caption = 'Extenso do Fechamento'
                Width = 253
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATA_VENC'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 111
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESC_VENCIMENTO'
                Title.Caption = 'Extenso do Vencimento'
                Width = 241
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 0
            Top = 525
            Width = 1221
            Height = 37
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            object Label43: TLabel
              Left = 2
              Top = 18
              Width = 607
              Height = 16
              Align = alTop
              Alignment = taCenter
              Caption = 
                'Obs:  A data de vencimento corresponde ao vencimento do fechamen' +
                'to da mesma linha'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label41: TLabel
              Left = 2
              Top = 2
              Width = 621
              Height = 16
              Align = alTop
              Alignment = taCenter
              Caption = 
                'Aten'#231#227'o! Alterando algum dia de fechamento a  atualiza'#231#227'o de dat' +
                'as dever'#225' ser efetuada'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
          object Panel19: TPanel
            Left = 0
            Top = 494
            Width = 1221
            Height = 31
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 2
            object btnGravaDataFecha: TBitBtn
              Left = 3
              Top = 3
              Width = 80
              Height = 25
              Caption = '&Gravar'
              TabOrder = 0
              OnClick = btnGravaDataFechaClick
              Glyph.Data = {
                A6030000424DA603000000000000A60100002800000020000000100000000100
                08000000000000020000232E0000232E00005C00000000000000343434003535
                3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
                49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
                63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
                800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
                A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
                B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
                BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
                C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
                D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
                CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
                E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
                F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
                3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
                3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
                2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
                284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
                234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
                54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
                3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
                323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
                5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
                57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
                58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
                5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
                5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
                53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                5B5B5B5B5B5B5B5B5B5B}
              NumGlyphs = 2
            end
            object btnCancelDataFecha: TBitBtn
              Left = 84
              Top = 3
              Width = 80
              Height = 25
              Caption = '&Cancelar'
              TabOrder = 1
              OnClick = btnCancelDataFechaClick
              Glyph.Data = {
                0E040000424D0E040000000000000E0200002800000020000000100000000100
                08000000000000020000232E0000232E000076000000000000000021CC001031
                DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
                DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
                FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
                F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
                F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
                FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
                E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
                ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
                FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
                C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
                CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
                D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
                E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
                E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
                F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
                75757575757575757575757575757575757575757575622F080000082F627575
                757575757575705E493434495E70757575757575753802030E11110E03023875
                7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
                7575757567354D5354555554534D35677575756307102A00337575442C151007
                63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
                367575604B545568345E7575745B544B607575171912301C3700317575401219
                1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
                057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
                0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
                217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
                3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
                65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
                757575756C566058483434485860566C75757575754324283237373228244375
                75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
                757575757575736A5D55555D6A73757575757575757575757575757575757575
                757575757575757575757575757575757575}
              NumGlyphs = 2
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Alterar datas'
          ImageIndex = 1
          OnShow = TabSheet4Show
          object ProgressDatas: TProgressBar
            Left = 0
            Top = 429
            Width = 1257
            Height = 16
            Align = alBottom
            TabOrder = 1
          end
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 1257
            Height = 429
            Align = alClient
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object Label40: TLabel
              Left = 353
              Top = 28
              Width = 27
              Height = 13
              Caption = 'Ap'#243's '
            end
            object Label44: TLabel
              Left = 8
              Top = 80
              Width = 62
              Height = 13
              Caption = '2'#186' Dia Fecha'
              FocusControl = DBEdFecha2
            end
            object Label45: TLabel
              Left = 89
              Top = 80
              Width = 57
              Height = 13
              Caption = '2'#186' Dia Venc'
              FocusControl = DBEdVenc2
            end
            object Label46: TLabel
              Left = 435
              Top = 28
              Width = 36
              Height = 13
              Caption = 'm'#234's(es)'
            end
            object Label47: TLabel
              Left = 352
              Top = 98
              Width = 27
              Height = 13
              Caption = 'Ap'#243's '
            end
            object Label48: TLabel
              Left = 434
              Top = 98
              Width = 36
              Height = 13
              Caption = 'm'#234's(es)'
            end
            object Label38: TLabel
              Left = 8
              Top = 10
              Width = 49
              Height = 13
              Caption = 'Dia Fecha'
            end
            object Label39: TLabel
              Left = 89
              Top = 10
              Width = 44
              Height = 13
              Caption = 'Dia Venc'
            end
            object DBEdit30: TDBEdit
              Left = 8
              Top = 25
              Width = 64
              Height = 21
              Hint = 'Insira o dia de fechamento'
              CharCase = ecUpperCase
              DataField = 'FECHAMENTO1'
              DataSource = DSCadastro
              TabOrder = 0
            end
            object DBEdit31: TDBEdit
              Left = 89
              Top = 25
              Width = 64
              Height = 21
              Hint = 'Insira o dia de vencimento'
              CharCase = ecUpperCase
              DataField = 'VENCIMENTO1'
              DataSource = DSCadastro
              TabOrder = 1
            end
            object ChVencnomes: TCheckBox
              Left = 158
              Top = 27
              Width = 193
              Height = 17
              Hint = 'Marcar caso o vencimento for no mesmo m'#234's de fechamento'
              Caption = 'Vencimento no m'#234's do fechamento'
              Checked = True
              State = cbChecked
              TabOrder = 3
              OnClick = ChVencnomesClick
            end
            object ChkUsa2Fecha: TCheckBox
              Left = 8
              Top = 55
              Width = 177
              Height = 17
              Hint = 'Marcar caso queira dois fechamentos no mesmo m'#234's'
              Caption = 'Usar dois fechamentos por m'#234's'
              TabOrder = 4
              OnClick = ChkUsa2FechaClick
            end
            object DBEdFecha2: TDBEdit
              Left = 8
              Top = 95
              Width = 64
              Height = 21
              Hint = 'Insira o dia de fechamento'
              CharCase = ecUpperCase
              DataField = 'FECHAMENTO2'
              DataSource = DSCadastro
              Enabled = False
              TabOrder = 5
            end
            object DBEdVenc2: TDBEdit
              Left = 89
              Top = 95
              Width = 64
              Height = 21
              Hint = 'Insira o dia de vencimento'
              CharCase = ecUpperCase
              DataField = 'VENCIMENTO2'
              DataSource = DSCadastro
              Enabled = False
              TabOrder = 6
            end
            object EdMesesVenc: TEdit
              Left = 383
              Top = 25
              Width = 48
              Height = 21
              Hint = 
                'Numero de meses que devem ser incrementados a mais no vencimento' +
                '.'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Text = '1'
              OnExit = EdMesesVenc2Exit
              OnKeyPress = EdMesesVencKeyPress
            end
            object ChVencnomes2: TCheckBox
              Left = 157
              Top = 98
              Width = 193
              Height = 17
              Hint = 'Marcar caso o vencimento for no mesmo m'#234's de fechamento'
              Caption = 'Vencimento no m'#234's do fechamento'
              Checked = True
              State = cbChecked
              TabOrder = 8
              OnClick = ChVencnomes2Click
            end
            object EdMesesVenc2: TEdit
              Left = 382
              Top = 95
              Width = 48
              Height = 21
              Hint = 
                'Numero de meses que devem ser incrementados a mais no vencimento' +
                '.'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 7
              Text = '1'
              OnExit = EdMesesVenc2Exit
              OnKeyPress = EdMesesVencKeyPress
            end
            object btnAltDataFecha: TBitBtn
              Left = 8
              Top = 125
              Width = 89
              Height = 25
              Caption = 'Alterar'
              TabOrder = 9
              OnClick = btnAltDataFechaClick
              Glyph.Data = {
                E6020000424DE602000000000000E60000002800000020000000100000000100
                08000000000000020000232E0000232E00002C00000000000000323232000054
                0000116E110065656500323298003232CC003265CC004C7FE5006565DD006565
                FF0021872100329832004CB24C0065CC65006598FF008787870098989800A9A9
                A900A8AAAA00B2B3B400B5B6B700BABABA00BBBCBD00BCBDBE0098BEFF00BFBF
                C000C3C4C400C9CACA00CCCCCC00CDCECE00CECFCF00D2D3D300D4D5D500D8D9
                D900DCDCDC00DDDDDD00DFDFE000CCE5FF00E2E3E300EAEAEB00EBEBEB00F2F2
                F200F7F7F700FFFFFF002B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
                2B2B2B2B2B2B2B2B2B2B2B000004062B2B2B2B2B2B2B2B2B2B2B2B1212141B2B
                2B2B2B2B2B2B2B2B2B2B2B00180E0E012B2B2B2B2B2B2B2B2B2B2B1228242412
                2B2B2B2B2B2B2B2B2B2B2B072518010A012B2B2B2B2B2B2B2B2B2B202A281216
                122B2B2B2B2B2B2B2B2B2B0E25020B0A0A012B2B2B2B2B2B2B2B2B242A131A16
                16122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E1A
                1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E
                1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B1621
                1E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16
                211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B
                16211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A032B2B2B2B2B2B2B2B
                2B16211E1A1616192B2B2B2B2B2B2B2B2B2B0A0D0C0B0F15032B2B2B2B2B2B2B
                2B2B16211E1A1D26192B2B2B2B2B2B2B2B2B2B0A0D111C0F052B2B2B2B2B2B2B
                2B2B2B162122271D172B2B2B2B2B2B2B2B2B2B2B10231108052B2B2B2B2B2B2B
                2B2B2B2B2029221E172B2B2B2B2B2B2B2B2B2B2B2B1009092B2B2B2B2B2B2B2B
                2B2B2B2B2B201F1F2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
                2B2B2B2B2B2B2B2B2B2B}
              NumGlyphs = 2
            end
          end
        end
      end
    end
    object TabSeg: TTabSheet [3]
      Caption = '&Segmentos'
      ImageIndex = 3
      OnHide = TabSegHide
      OnShow = TabSegShow
      object Panel31: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 507
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object GridSeg: TJvDBGrid
          Left = 2
          Top = 2
          Width = 792
          Height = 472
          Hint = 'Clique em cima da do campo liberado para alterar'
          Align = alClient
          BorderStyle = bsNone
          DataSource = DSSegLib
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          OnTitleBtnClick = GridSegTitleBtnClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'Seg_ID'
              ReadOnly = True
              Title.Caption = 'Seg ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              ReadOnly = True
              Width = 284
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Liberado'
              PickList.Strings = (
                'S'
                'N')
              Visible = True
            end>
        end
        object PanelSeg: TPanel
          Left = 2
          Top = 474
          Width = 792
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object btnGravaSeg: TBitBtn
            Left = 3
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Gravar'
            TabOrder = 0
            OnClick = btnGravaSegClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancelSeg: TBitBtn
            Left = 84
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = btnCancelSegClick
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
        end
        object Panel4: TPanel
          Left = 56
          Top = 64
          Width = 617
          Height = 41
          BevelOuter = bvNone
          Caption = 'Todos os Segmentos est'#227'o liberados para esta empresa'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
    end
    object TabVendaNome: TTabSheet [4]
      Caption = 'Venda Nome'
      ImageIndex = 8
      TabVisible = False
      OnHide = TabVendaNomeHide
      OnShow = TabVendaNomeShow
      object GridVendaNome: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1108
        Height = 409
        Hint = 'Clique no campo [Liberado] para alterar'
        Align = alClient
        DataSource = DSVendaNome
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GridVendaNomeDrawColumnCell
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = GridSegTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'CRED_ID'
            Title.Caption = 'Seg ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 284
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FANTASIA'
            PickList.Strings = (
              'S'
              'N')
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            PickList.Strings = (
              'S'
              'N')
            Visible = True
          end>
      end
      object Panel14: TPanel
        Left = 32
        Top = 64
        Width = 617
        Height = 41
        BevelOuter = bvNone
        Caption = 
          'Todos os fornecedores est'#227'o habilitados a vender pelo nome nessa' +
          ' empresa.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object Panel15: TPanel
        Left = 0
        Top = 409
        Width = 1108
        Height = 34
        Align = alBottom
        BorderStyle = bsSingle
        TabOrder = 2
        object btnGravaVendaNome: TButton
          Left = 2
          Top = 3
          Width = 100
          Height = 25
          Hint = 'Gravar altera'#231#245'es feitas'
          Caption = '&Gravar'
          TabOrder = 0
        end
        object btnCancelVendaNome: TButton
          Left = 104
          Top = 3
          Width = 100
          Height = 25
          Hint = 'Cancelar as altera'#231#245'es feitas'
          Caption = '&Cancelar'
          TabOrder = 1
        end
        object DBNavigator5: TDBNavigator
          Left = 241
          Top = 3
          Width = 408
          Height = 25
          DataSource = DSVendaNome
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          Align = alCustom
          Flat = True
          TabOrder = 2
        end
      end
    end
    object TabGrupoProd: TTabSheet [5]
      Caption = 'Grupos Prod. / Regras de Convs.'
      ImageIndex = 7
      OnShow = TabGrupoProdShow
      object PageControl4: TPageControl
        Left = 0
        Top = 0
        Width = 1229
        Height = 593
        ActivePage = TabConfigPrograma
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 0
        OnEnter = PageControl4Enter
        object TabConfigGrupo: TTabSheet
          Caption = 'Configura'#231#245'es de Grupo'
          OnHide = TabConfigGrupoHide
          OnShow = TabConfigGrupoShow
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 1100
            Height = 412
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter2: TSplitter
              Left = 0
              Top = 184
              Width = 1100
              Height = 3
              Cursor = crVSplit
              Align = alBottom
            end
            object Panel24: TPanel
              Left = 0
              Top = 187
              Width = 1100
              Height = 225
              Align = alBottom
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 1
              object Panel18: TPanel
                Left = 2
                Top = 2
                Width = 1096
                Height = 31
                Align = alTop
                Alignment = taLeftJustify
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Caption = '   Produtos Bloqueados para Venda por C'#243'digo de Barras'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object grdProdBloq: TJvDBGrid
                Left = 2
                Top = 33
                Width = 1096
                Height = 159
                Align = alClient
                BorderStyle = bsNone
                DataSource = dsProdBloq
                DefaultDrawing = False
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                AutoAppend = False
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 16
                TitleRowHeight = 17
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'codbarras'
                    Width = 120
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'descricao'
                    Visible = True
                  end>
              end
              object Panel12: TPanel
                Left = 2
                Top = 192
                Width = 1096
                Height = 31
                Align = alBottom
                BevelInner = bvRaised
                BevelOuter = bvLowered
                TabOrder = 2
                object btnExclProdBloq: TBitBtn
                  Left = 84
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Excluir'
                  TabOrder = 1
                  OnClick = btnExclProdBloqClick
                  Glyph.Data = {
                    BE020000424DBE02000000000000BE0000002800000020000000100000000100
                    08000000000000020000232E0000232E000022000000000000000526CF000D2E
                    D4001335DB001436D9001D3FDE002446E2002A4CE6002F51F0004059D9004769
                    FF004D6FFF005476FF005B7DFF006183FF006587FF00728CFF00B8B9B900BABB
                    BC00BEBFBF0087A9FF00C1C2C300C4C5C500C5C6C700C8C9C900C9CACA00D1D2
                    D200D3D4D400D6D6D700D7D8D800D9DADA00DBDBDC00DCDDDD00E5E5E600FFFF
                    FF00212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121210800010304050606050403010008212117101112141516161514121110
                    172121020B0B0B0B0B0B0B0B0B0B0B0B022121121B1B1B1B1B1B1B1B1B1B1B1B
                    1221210713131313131313131313131307212118202020202020202020202020
                    1821210F090A0B0C0D0E0E0D0C0B0A090F21211F191A1B1C1D1E1E1D1C1B1A19
                    1F21212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121212121212121212121212121212121212121212121212121212121212121
                    2121}
                  NumGlyphs = 2
                end
                object btnInclProdBloq: TBitBtn
                  Left = 3
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Incluir'
                  TabOrder = 0
                  OnClick = btnInclProdBloqClick
                  Glyph.Data = {
                    5A030000424D5A030000000000005A0100002800000020000000100000000100
                    08000000000000020000232E0000232E00004900000000000000117611001379
                    1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
                    37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
                    4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
                    6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
                    7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
                    840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
                    C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
                    D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
                    DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
                    4848484848484848484848484848484848484848484848484848484848484848
                    484848480C00000C484848484848484848484848302323304848484848484848
                    4848484801161601484848484848484848484848243A3A244848484848484848
                    4848484802181802484848484848484848484848253C3C254848484848484848
                    48484848031A1A03484848484848484848484848263D3D264848484848484848
                    48484848041B1B04484848484848484848484848273F3F274848484848484813
                    0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
                    1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
                    2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
                    0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
                    484848480D21210D484848484848484848484848324444324848484848484848
                    4848484810282810484848484848484848484848354545354848484848484848
                    4848484812292912484848484848484848484848374646374848484848484848
                    48484848152A2A15484848484848484848484848394747394848484848484848
                    484848481E16161E484848484848484848484848423A3A424848484848484848
                    484848484848484848484848484848484848484848484848484848484848}
                  NumGlyphs = 2
                end
              end
            end
            object Panel27: TPanel
              Left = 0
              Top = 0
              Width = 1100
              Height = 184
              Align = alClient
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 0
              object Panel11: TPanel
                Left = 2
                Top = 2
                Width = 1096
                Height = 33
                Align = alTop
                Alignment = taLeftJustify
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Caption = '   Configura'#231#227'o de Grupos de Produtos para esta Empresa'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object GridGrupo_Prod: TJvDBGrid
                Left = 2
                Top = 35
                Width = 1096
                Height = 116
                Hint = 
                  'Clique nas campos [Liberado] e [Pre'#231'o F'#225'brica]'#13#10'para poder alter' +
                  'ar os campos'
                Align = alClient
                BorderStyle = bsNone
                DataSource = DSGrupo_Prod
                DefaultDrawing = False
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnColExit = GridGrupo_ProdColExit
                OnDrawColumnCell = GridGrupo_ProdDrawColumnCell
                AutoAppend = False
                TitleButtons = True
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 16
                TitleRowHeight = 17
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'GRUPO_PROD_ID'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DESCRICAO'
                    ReadOnly = True
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'LIBERADO'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    PickList.Strings = (
                      'N'
                      'S')
                    Title.Font.Charset = DEFAULT_CHARSET
                    Title.Font.Color = clWindowText
                    Title.Font.Height = -11
                    Title.Font.Name = 'MS Sans Serif'
                    Title.Font.Style = [fsBold]
                    Visible = True
                  end
                  item
                    Alignment = taRightJustify
                    Expanded = False
                    FieldName = 'DESCONTO'
                    Title.Caption = 'Desconto (%)'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PRECO_FABRICA'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    PickList.Strings = (
                      'N'
                      'S')
                    Title.Font.Charset = DEFAULT_CHARSET
                    Title.Font.Color = clWindowText
                    Title.Font.Height = -11
                    Title.Font.Name = 'MS Sans Serif'
                    Title.Font.Style = [fsBold]
                    Width = 92
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FIDELIDADE'
                    Visible = True
                  end>
              end
              object Panel13: TPanel
                Left = 2
                Top = 151
                Width = 1096
                Height = 31
                Align = alBottom
                BevelInner = bvRaised
                BevelOuter = bvLowered
                TabOrder = 2
                object btnGravaGrupProd: TBitBtn
                  Left = 3
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Gravar'
                  TabOrder = 0
                  OnClick = btnAddEnderecoClick
                  Glyph.Data = {
                    A6030000424DA603000000000000A60100002800000020000000100000000100
                    08000000000000020000232E0000232E00005C00000000000000343434003535
                    3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
                    49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
                    63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
                    800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
                    A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
                    B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
                    BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
                    C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
                    D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
                    CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
                    E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
                    F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                    5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
                    3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
                    3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
                    2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
                    284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
                    234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
                    54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
                    3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
                    323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
                    5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
                    57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
                    58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
                    5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
                    5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
                    53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                    5B5B5B5B5B5B5B5B5B5B}
                  NumGlyphs = 2
                end
                object btnCancelGrupProd: TBitBtn
                  Left = 84
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Cancelar'
                  TabOrder = 1
                  OnClick = BitBtn6Click
                  Glyph.Data = {
                    0E040000424D0E040000000000000E0200002800000020000000100000000100
                    08000000000000020000232E0000232E000076000000000000000021CC001031
                    DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
                    DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
                    FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
                    F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
                    F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
                    FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
                    E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
                    ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
                    FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
                    C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
                    CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
                    D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
                    E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
                    E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
                    F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
                    75757575757575757575757575757575757575757575622F080000082F627575
                    757575757575705E493434495E70757575757575753802030E11110E03023875
                    7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
                    7575757567354D5354555554534D35677575756307102A00337575442C151007
                    63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
                    367575604B545568345E7575745B544B607575171912301C3700317575401219
                    1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
                    057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
                    0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
                    217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
                    3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
                    65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
                    757575756C566058483434485860566C75757575754324283237373228244375
                    75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
                    757575757575736A5D55555D6A73757575757575757575757575757575757575
                    757575757575757575757575757575757575}
                  NumGlyphs = 2
                end
              end
            end
          end
        end
        object TabConfigPrograma: TTabSheet
          Caption = 'Regras de Conv'#234'nios'
          ImageIndex = 1
          OnHide = TabConfigProgramaHide
          OnShow = TabConfigProgramaShow
          object Panel21: TPanel
            Left = 0
            Top = 0
            Width = 1350
            Height = 587
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 0
              Top = 305
              Width = 1350
              Height = 3
              Cursor = crVSplit
              Align = alTop
            end
            object Panel28: TPanel
              Left = 0
              Top = 0
              Width = 1350
              Height = 305
              Align = alTop
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 0
              object Panel25: TPanel
                Left = 2
                Top = 2
                Width = 1346
                Height = 31
                Align = alTop
                Alignment = taLeftJustify
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Caption = '   Configura'#231#227'o de Regras de Conv'#234'nios em Medicamentos'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object GridPbm: TJvDBGrid
                Left = 2
                Top = 33
                Width = 1346
                Height = 239
                Hint = 
                  'Clique nas campos [Liberado] e [Pre'#231'o F'#225'brica]'#13#10'para poder alter' +
                  'ar os campos'
                Align = alClient
                BorderStyle = bsNone
                DataSource = dsPbm
                DefaultDrawing = False
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnColExit = GridPbmColExit
                OnKeyDown = GridPbmKeyDown
                AutoAppend = False
                TitleButtons = True
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 16
                TitleRowHeight = 17
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'prog_id'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'nome'
                    Visible = True
                  end
                  item
                    ButtonStyle = cbsEllipsis
                    Expanded = False
                    FieldName = 'participa'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    PickList.Strings = (
                      'S'
                      'N')
                    Title.Font.Charset = DEFAULT_CHARSET
                    Title.Font.Color = clWindowText
                    Title.Font.Height = -11
                    Title.Font.Name = 'MS Sans Serif'
                    Title.Font.Style = [fsBold]
                    Width = 70
                    Visible = True
                  end>
              end
              object Panel26: TPanel
                Left = 2
                Top = 272
                Width = 1346
                Height = 31
                Align = alBottom
                BevelInner = bvRaised
                BevelOuter = bvLowered
                TabOrder = 2
                object btnGravaPbm: TBitBtn
                  Left = 3
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Gravar'
                  TabOrder = 0
                  OnClick = btnGravaPbmClick
                  Glyph.Data = {
                    A6030000424DA603000000000000A60100002800000020000000100000000100
                    08000000000000020000232E0000232E00005C00000000000000343434003535
                    3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
                    49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
                    63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
                    800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
                    A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
                    B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
                    BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
                    C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
                    D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
                    CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
                    E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
                    F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                    5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
                    3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
                    3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
                    2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
                    284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
                    234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
                    54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
                    3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
                    323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
                    5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
                    57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
                    58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
                    5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
                    5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
                    53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                    5B5B5B5B5B5B5B5B5B5B}
                  NumGlyphs = 2
                end
                object btnCancelPbm: TBitBtn
                  Left = 84
                  Top = 3
                  Width = 80
                  Height = 25
                  Caption = '&Cancelar'
                  TabOrder = 1
                  OnClick = btnCancelPbmClick
                  Glyph.Data = {
                    0E040000424D0E040000000000000E0200002800000020000000100000000100
                    08000000000000020000232E0000232E000076000000000000000021CC001031
                    DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
                    DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
                    FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
                    F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
                    F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
                    FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
                    E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
                    ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
                    FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
                    C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
                    CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
                    D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
                    E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
                    E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
                    F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
                    75757575757575757575757575757575757575757575622F080000082F627575
                    757575757575705E493434495E70757575757575753802030E11110E03023875
                    7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
                    7575757567354D5354555554534D35677575756307102A00337575442C151007
                    63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
                    367575604B545568345E7575745B544B607575171912301C3700317575401219
                    1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
                    057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
                    0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
                    217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
                    3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
                    65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
                    757575756C566058483434485860566C75757575754324283237373228244375
                    75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
                    757575757575736A5D55555D6A73757575757575757575757575757575757575
                    757575757575757575757575757575757575}
                  NumGlyphs = 2
                end
              end
            end
            object Panel29: TPanel
              Left = 0
              Top = 308
              Width = 1350
              Height = 279
              Align = alClient
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 1
              object Panel22: TPanel
                Left = 2
                Top = 2
                Width = 1346
                Height = 31
                Align = alTop
                Alignment = taLeftJustify
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Caption = '   Exibi'#231#227'o do Regras de Conv'#234'nios'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
              object JvDBGrid1: TJvDBGrid
                Left = 2
                Top = 33
                Width = 1346
                Height = 244
                Align = alClient
                BorderStyle = bsNone
                DataSource = dsPrograma
                DefaultDrawing = False
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                AutoAppend = False
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 16
                TitleRowHeight = 17
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'descricao'
                    Width = 300
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'codbarras'
                    Width = 120
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'prc_unit'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'perc_desc'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'qtd_max'
                    Width = 70
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
    object TabSaldo: TTabSheet [6]
      Caption = '&Saldo da Empresa'
      ImageIndex = 5
      OnHide = TabSaldoHide
      OnShow = TabSaldoShow
      object Panel30: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 593
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 2
          Width = 1225
          Height = 47
          Align = alTop
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object LabInfo: TLabel
            Left = 276
            Top = 24
            Width = 3
            Height = 13
          end
          object Label66: TLabel
            Left = 10
            Top = 3
            Width = 81
            Height = 13
            Hint = 'Selecione o ano para consulta'
            Caption = 'Selecionar o Ano'
          end
          object btnVisualizaSaldo: TBitBtn
            Left = 204
            Top = 8
            Width = 118
            Height = 30
            Caption = '&Visualizar Saldo'
            TabOrder = 0
            OnClick = btnVisualizaSaldoClick
            Glyph.Data = {
              AE030000424DAE03000000000000AE0100002800000020000000100000000100
              08000000000000020000232E0000232E00005E00000000000000512600005157
              61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
              7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
              9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
              BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
              BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
              CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
              D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
              DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
              E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
              ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
              F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
              FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
              09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
              0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
              23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
              065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
              5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
              5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
              5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
              5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
              5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
              5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
              5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
              5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
              5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
              5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
              5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
              5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
            NumGlyphs = 2
          end
          object cbData: TComboBox
            Left = 10
            Top = 19
            Width = 87
            Height = 21
            ItemHeight = 13
            TabOrder = 1
            Items.Strings = (
              ''
              '2004'
              '2005'
              '2006'
              '2007'
              '2008'
              '2009'
              '2010'
              '2011'
              '2012'
              '2013'
              '2014'
              '2015'
              '2016'
              '2017'
              '2018'
              '2019'
              '2020')
          end
          object rbRecarga: TCheckBox
            Left = 120
            Top = 16
            Width = 81
            Height = 17
            Hint = 
              'selecione esta op'#231#227'o para visualizar informa'#231#245'es sobre as recarg' +
              'as'
            Caption = 'Recarga'
            TabOrder = 2
            Visible = False
          end
        end
        object GridSaldos: TDBGrid
          Left = 2
          Top = 49
          Width = 1225
          Height = 514
          Align = alClient
          BorderStyle = bsNone
          Ctl3D = True
          DataSource = DSSaldos
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'data_fecha_emp'
              Title.Caption = 'Data de Fechamento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'data_venc_emp'
              Title.Caption = 'Data de vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'saldo_nconf'
              Title.Caption = 'Saldo n'#227'o confirmado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'saldo_conf'
              Title.Caption = 'Saldo confirmado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'saldo_mes'
              Title.Caption = 'Saldo M'#234's'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'fatura_id'
              Title.Caption = 'Fatura n'#186
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'data_fatura'
              Title.Caption = 'Data fatura'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tipo'
              Title.Caption = 'Tipo'
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 2
          Top = 563
          Width = 1225
          Height = 28
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = 
            'Saldo = Soma dos valores em aberto dos conveniados da empresa po' +
            'r data de fechamento'
          TabOrder = 2
        end
      end
    end
    object TabGrupos: TTabSheet [7]
      Caption = '&Grupos da Empr.'
      ImageIndex = 6
      OnHide = TabGruposHide
      OnShow = TabGruposShow
      object Panel32: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 507
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel9: TPanel
          Left = 2
          Top = 2
          Width = 792
          Height = 31
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = '   Grupos de conveniados da empresa.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object Grid_Grupo: TJvDBGrid
          Left = 2
          Top = 33
          Width = 792
          Height = 441
          Align = alClient
          BorderStyle = bsNone
          DataSource = DSQGrupo_conv_emp
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'GRUPO_CONV_EMP_ID'
              ReadOnly = True
              Width = 71
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRICAO'
              ReadOnly = True
              Width = 415
              Visible = True
            end>
        end
        object Panel10: TPanel
          Left = 2
          Top = 474
          Width = 792
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object btnIncGrupEmp: TBitBtn
            Left = 2
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Incluir'
            TabOrder = 0
            OnClick = btnIncGrupEmpClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
          object btnExclGrupEmp: TBitBtn
            Left = 78
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Excluir'
            TabOrder = 1
            OnClick = btnExclGrupEmpClick
            Glyph.Data = {
              BE020000424DBE02000000000000BE0000002800000020000000100000000100
              08000000000000020000232E0000232E000022000000000000000526CF000D2E
              D4001335DB001436D9001D3FDE002446E2002A4CE6002F51F0004059D9004769
              FF004D6FFF005476FF005B7DFF006183FF006587FF00728CFF00B8B9B900BABB
              BC00BEBFBF0087A9FF00C1C2C300C4C5C500C5C6C700C8C9C900C9CACA00D1D2
              D200D3D4D400D6D6D700D7D8D800D9DADA00DBDBDC00DCDDDD00E5E5E600FFFF
              FF00212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121210800010304050606050403010008212117101112141516161514121110
              172121020B0B0B0B0B0B0B0B0B0B0B0B022121121B1B1B1B1B1B1B1B1B1B1B1B
              1221210713131313131313131313131307212118202020202020202020202020
              1821210F090A0B0C0D0E0E0D0C0B0A090F21211F191A1B1C1D1E1E1D1C1B1A19
              1F21212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121212121212121212121212121212121212121212121212121212121212121
              2121}
            NumGlyphs = 2
          end
          object btnGravaGrupEmp: TBitBtn
            Left = 194
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Gravar'
            TabOrder = 2
            OnClick = btnGravaGrupEmpClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancGrupEmp: TBitBtn
            Left = 270
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Cancelar'
            TabOrder = 3
            OnClick = btnCancGrupEmpClick
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
        end
      end
    end
    object tabFormasPgto: TTabSheet [8]
      Caption = 'Formas de Pagto'
      ImageIndex = 10
      OnHide = tabFormasPgtoHide
      OnShow = tabFormasPgtoShow
      object Panel34: TPanel
        Left = 0
        Top = 33
        Width = 796
        Height = 443
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object grdFormasPgto: TJvDBGrid
          Left = 2
          Top = 2
          Width = 792
          Height = 439
          Align = alClient
          DataSource = dsFormasPgto
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColExit = grdFormasPgtoColExit
          OnKeyDown = grdFormasPgtoKeyDown
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'forma_id'
              ReadOnly = True
              Title.Caption = 'Forma ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'descricao'
              ReadOnly = True
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Expanded = False
              FieldName = 'liberado'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Liberado'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 60
              Visible = True
            end>
        end
        object panFormasLib: TPanel
          Left = 40
          Top = 56
          Width = 641
          Height = 41
          BevelOuter = bvNone
          Caption = 
            'Todas as Formas de Pagamentos existentes est'#227'o liberadas para es' +
            'se Estabelecimento'
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object Panel35: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 33
        Align = alTop
        Alignment = taLeftJustify
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = '   Formas de pagamentos liberadas para essa empresa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object Panel36: TPanel
        Left = 0
        Top = 476
        Width = 796
        Height = 31
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        object btnGravaFormas: TBitBtn
          Left = 3
          Top = 3
          Width = 89
          Height = 25
          Caption = 'Gravar'
          TabOrder = 0
          OnClick = btnGravaFormasClick
        end
        object btnCancelFormas: TBitBtn
          Left = 99
          Top = 3
          Width = 89
          Height = 25
          Caption = 'Cancelar'
          TabOrder = 1
          OnClick = btnCancelFormasClick
        end
      end
    end
    object TabCredLib: TTabSheet [9]
      Caption = 'Estab. Lib.'
      ImageIndex = 9
      OnHide = TabCredLibHide
      OnShow = TabCredLibShow
      object Panel33: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 593
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel16: TPanel
          Left = 2
          Top = 2
          Width = 1225
          Height = 31
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = '   Estabelecimentos liberados para vender nessa empresa.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object GridCredLib: TJvDBGrid
          Left = 2
          Top = 33
          Width = 1225
          Height = 510
          Align = alClient
          BorderStyle = bsNone
          DataSource = DSCredLib
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColExit = GridCredLibColExit
          OnKeyDown = GridCredLibKeyDown
          AutoAppend = False
          TitleButtons = True
          OnTitleBtnClick = GridCredLibTitleBtnClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'cred_id'
              ReadOnly = True
              Title.Caption = 'Estab. ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nome'
              ReadOnly = True
              Title.Caption = 'Nome do Estabelecimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'liberado'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Liberado'
              Width = 60
              Visible = True
            end>
        end
        object Panel17: TPanel
          Left = 2
          Top = 543
          Width = 1225
          Height = 48
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object Label81: TLabel
            Left = 279
            Top = 5
            Width = 53
            Height = 13
            Caption = 'Segmentos'
          end
          object btnGravaCredLib: TBitBtn
            Left = 3
            Top = 3
            Width = 80
            Height = 40
            Caption = '&Gravar'
            TabOrder = 0
            OnClick = BitBtn10Click
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancelCredLib: TBitBtn
            Left = 84
            Top = 3
            Width = 80
            Height = 40
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = BitBtn11Click
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
          object DBSegmento: TJvDBLookupCombo
            Left = 277
            Top = 20
            Width = 201
            Height = 22
            DropDownWidth = 350
            DisplayEmpty = 'Todos Segmentos'
            EmptyValue = '0'
            FieldsDelimiter = #0
            LookupField = 'SEG_ID'
            LookupDisplay = 'DESCRICAO'
            LookupSource = DSSegmento
            TabOrder = 4
          end
          object btnAlterarTodos: TBitBtn
            Left = 168
            Top = 3
            Width = 97
            Height = 40
            Caption = 'Alterar todos'
            TabOrder = 2
            OnClick = btnAlterarTodosClick
            Glyph.Data = {
              E6020000424DE602000000000000E60000002800000020000000100000000100
              08000000000000020000232E0000232E00002C00000000000000323232000054
              0000116E110065656500323298003232CC003265CC004C7FE5006565DD006565
              FF0021872100329832004CB24C0065CC65006598FF008787870098989800A9A9
              A900A8AAAA00B2B3B400B5B6B700BABABA00BBBCBD00BCBDBE0098BEFF00BFBF
              C000C3C4C400C9CACA00CCCCCC00CDCECE00CECFCF00D2D3D300D4D5D500D8D9
              D900DCDCDC00DDDDDD00DFDFE000CCE5FF00E2E3E300EAEAEB00EBEBEB00F2F2
              F200F7F7F700FFFFFF002B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B2B000004062B2B2B2B2B2B2B2B2B2B2B2B1212141B2B
              2B2B2B2B2B2B2B2B2B2B2B00180E0E012B2B2B2B2B2B2B2B2B2B2B1228242412
              2B2B2B2B2B2B2B2B2B2B2B072518010A012B2B2B2B2B2B2B2B2B2B202A281216
              122B2B2B2B2B2B2B2B2B2B0E25020B0A0A012B2B2B2B2B2B2B2B2B242A131A16
              16122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E1A
              1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E
              1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B1621
              1E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16
              211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B
              16211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A032B2B2B2B2B2B2B2B
              2B16211E1A1616192B2B2B2B2B2B2B2B2B2B0A0D0C0B0F15032B2B2B2B2B2B2B
              2B2B16211E1A1D26192B2B2B2B2B2B2B2B2B2B0A0D111C0F052B2B2B2B2B2B2B
              2B2B2B162122271D172B2B2B2B2B2B2B2B2B2B2B10231108052B2B2B2B2B2B2B
              2B2B2B2B2029221E172B2B2B2B2B2B2B2B2B2B2B2B1009092B2B2B2B2B2B2B2B
              2B2B2B2B2B201F1F2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B}
            NumGlyphs = 2
          end
          object btnBuscaSeg: TBitBtn
            Left = 488
            Top = 3
            Width = 89
            Height = 40
            Caption = 'Buscar'
            TabOrder = 3
            OnClick = btnBuscaSegClick
            Glyph.Data = {
              7E090000424D7E0900000000000036000000280000001D0000001B0000000100
              1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
              A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
              EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
              AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
              596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
              A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
              D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
              C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
              89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
              D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
              C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
              EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
              F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
              BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
              FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
              EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
              99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
              FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
              D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
              BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
              FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
              C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
              ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
              F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
              FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
              C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
              FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
              D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
              BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
              CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
              D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
              C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
              D400}
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 1229
      end
      inherited GridHistorico: TJvDBGrid
        Width = 1229
        Height = 546
      end
    end
    object tsSenhaConv: TTabSheet
      Caption = 'Usar Senha Conveniado no Estab.'
      ImageIndex = 11
      OnHide = tsSenhaConvHide
      OnShow = tsSenhaConvShow
      object Panel39: TPanel
        Left = 0
        Top = 0
        Width = 1209
        Height = 613
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel37: TPanel
          Left = 2
          Top = 2
          Width = 1205
          Height = 31
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = 
            '   Estabelecimentos liberados para vender nessa empresa pedindo ' +
            'a senha do conveniado.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object GridCredObrigarSenha: TJvDBGrid
          Left = 2
          Top = 33
          Width = 1205
          Height = 547
          Align = alClient
          BorderStyle = bsNone
          DataSource = DSCred_Obriga_Senha
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColExit = GridCredObrigarSenhaColExit
          OnKeyDown = GridCredObrigarSenhaKeyDown
          AutoAppend = False
          TitleButtons = True
          OnTitleBtnClick = GridCredObrigarSenhaTitleBtnClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'cred_id'
              ReadOnly = True
              Title.Caption = 'Estab. ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nome'
              ReadOnly = True
              Title.Caption = 'Nome do Estabelecimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'obrigaSenha'
              Title.Caption = 'Obriga Senha'
              Width = 103
              Visible = True
            end>
        end
        object Panel38: TPanel
          Left = 2
          Top = 580
          Width = 1205
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object btnGravaCredObrigarSenha: TBitBtn
            Left = 3
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Gravar'
            TabOrder = 0
            OnClick = btnGravaCredObrigarSenhaClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancelCredObrigarSenha: TBitBtn
            Left = 84
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Cancelar'
            TabOrder = 1
            OnClick = btnCancelCredObrigarSenhaClick
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
        end
      end
    end
    object tsUsuariosWeb: TTabSheet
      Caption = 'Usu'#225'rios Web'
      ImageIndex = 12
      OnHide = tsUsuariosWebHide
      OnShow = tsUsuariosWebShow
      object Panel40: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 607
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel41: TPanel
          Left = 2
          Top = 2
          Width = 1225
          Height = 31
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = '   Usu'#225'rios cadastrados no sistema WebEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object dbGridUsuWeb: TJvDBGrid
          Left = 2
          Top = 33
          Width = 1225
          Height = 541
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsUsu_Web
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = dbGridUsuWebKeyDown
          OnKeyPress = dbGridUsuWebKeyPress
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'usu_id'
              ReadOnly = True
              Title.Caption = 'ID '
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'usu_nome'
              Title.Caption = 'Nome do Usu'#225'rio'
              Width = 247
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'usu_email'
              Title.Caption = 'Email'#13#10
              Width = 351
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'usu_liberado'
              Title.Caption = 'Liberado'
              Visible = True
            end>
        end
        object Panel42: TPanel
          Left = 2
          Top = 574
          Width = 1225
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object BitBtn1: TBitBtn
            Left = 67
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = BitBtn1Click
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object BitBtn2: TBitBtn
            Left = 148
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Cancelar'
            TabOrder = 2
            OnClick = BitBtn2Click
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
          object BitBtn3: TBitBtn
            Left = 328
            Top = 4
            Width = 100
            Height = 25
            Hint = 'Limpar senha da empresa'
            Caption = 'Limpar Senha'
            TabOrder = 4
            OnClick = BitBtn3Click
            Glyph.Data = {
              96050000424D9605000000000000960300002800000020000000100000000100
              08000000000000020000232E0000232E0000D800000000000000FFFFFF00CCFF
              FF0099FFFF0066FFFF0033FFFF0000FFFF00FFCCFF00CCCCFF0099CCFF0066CC
              FF0033CCFF0000CCFF00FF99FF00CC99FF009999FF006699FF003399FF000099
              FF00FF66FF00CC66FF009966FF006666FF003366FF000066FF00FF33FF00CC33
              FF009933FF006633FF003333FF000033FF00FF00FF00CC00FF009900FF006600
              FF003300FF000000FF00FFFFCC00CCFFCC0099FFCC0066FFCC0033FFCC0000FF
              CC00FFCCCC00CCCCCC0099CCCC0066CCCC0033CCCC0000CCCC00FF99CC00CC99
              CC009999CC006699CC003399CC000099CC00FF66CC00CC66CC009966CC006666
              CC003366CC000066CC00FF33CC00CC33CC009933CC006633CC003333CC000033
              CC00FF00CC00CC00CC009900CC006600CC003300CC000000CC00FFFF9900CCFF
              990099FF990066FF990033FF990000FF9900FFCC9900CCCC990099CC990066CC
              990033CC990000CC9900FF999900CC9999009999990066999900339999000099
              9900FF669900CC66990099669900666699003366990000669900FF339900CC33
              990099339900663399003333990000339900FF009900CC009900990099006600
              99003300990000009900FFFF6600CCFF660099FF660066FF660033FF660000FF
              6600FFCC6600CCCC660099CC660066CC660033CC660000CC6600FF996600CC99
              660099996600669966003399660000996600FF666600CC666600996666006666
              66003366660000666600FF336600CC3366009933660066336600333366000033
              6600FF006600CC00660099006600660066003300660000006600FFFF3300CCFF
              330099FF330066FF330033FF330000FF3300FFCC3300CCCC330099CC330066CC
              330033CC330000CC3300FF993300CC9933009999330066993300339933000099
              3300FF663300CC66330099663300666633003366330000663300FF333300CC33
              330099333300663333003333330000333300FF003300CC003300990033006600
              33003300330000003300FFFF0000CCFF000099FF000066FF000033FF000000FF
              0000FFCC0000CCCC000099CC000066CC000033CC000000CC0000FF990000CC99
              000099990000669900003399000000990000FF660000CC660000996600006666
              00003366000000660000FF330000CC3300009933000066330000333300000033
              0000FF000000CC00000099000000660000003300000000000000002D092D0000
              00000000000000000000012B072B0000000000000000000000002D35350B5F00
              000000000000000000002B2B2B2B2B000000000000000000000009033B350B5F
              0000000000000000000007002B2B072B000000000000000000002D010B5F350B
              5F0000000000000000000700002B2B2B2B0000000000000000000035010A3B35
              04350000000000000000002B00002B2B072B0000000000000000000035010A5F
              350A352D00000000000000002B00002B2B012B010000000000000000340A0909
              3B350A353B5F5F3400000000070000002B2B012B2B2B2B2B0000000035013501
              09350909090909345F0000002B002B00002B00000000002B2B00000009350935
              01030203020302030A340000012B012B0000010000000100072B000000000009
              340102020101010102350000000000012B00000000000000002B000000000000
              350102020335350902350000000000002B000000012B2B00002B000000000000
              350101013500013501350000000000002B0000002B00002B002B000000000000
              3501010135071101092E0000000000002B0000002B002B00012B000000000000
              2E0901012D5F010235000000000000002B0000002B2B00002B00000000000000
              00350901010109350000000000000000002B01000000012B0000000000000000
              00000A3535350A00000000000000000000002B2B2B2B2B000000}
            NumGlyphs = 2
          end
          object BitBtn4: TBitBtn
            Left = 243
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Apagar registro'
            Caption = 'Apagar (F6)'
            TabOrder = 3
            OnClick = BitBtn4Click
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
            Spacing = 2
          end
          object btnInserirPos: TBitBtn
            Left = 0
            Top = 2
            Width = 63
            Height = 25
            Caption = '&Inserir'
            TabOrder = 0
            OnClick = btnInserirPosClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
        end
      end
    end
    object tsAlimentacao: TTabSheet
      Caption = 'Alimenta'#231#227'o'
      ImageIndex = 13
      OnShow = tsAlimentacaoShow
      object dbGridAlim: TJvDBGrid
        Left = 0
        Top = 129
        Width = 1229
        Height = 415
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsCredAlim
        DefaultDrawing = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = dbGridAlimTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'conv_id'
            Title.Caption = 'Conv. ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titular'
            Title.Caption = 'Titular'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'limite_mes'
            Title.Caption = 'Limite M'#234's'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'abono_mes'
            Title.Caption = 'Abono M'#234's'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'saldo_renovacao'
            Title.Caption = 'Saldo Renova'#231#227'o'
            Width = 100
            Visible = True
          end>
      end
      object Panel44: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 129
        Align = alTop
        Alignment = taLeftJustify
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label60: TLabel
          Left = 5
          Top = 28
          Width = 82
          Height = 13
          Caption = 'Data Renova'#231#227'o'
        end
        object lblTitulo: TLabel
          Left = 5
          Top = 5
          Width = 74
          Height = 13
          Caption = 'Conveniados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbDataRenovacao: TJvDateEdit
          Left = 5
          Top = 52
          Width = 107
          Height = 21
          Enabled = False
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 2
        end
        object rgTipo: TRadioGroup
          Left = 123
          Top = 25
          Width = 187
          Height = 49
          Caption = 'Tipo'
          Columns = 2
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Renova'#231#227'o'
            'Complemento')
          TabOrder = 1
        end
        object Button2: TButton
          Left = 597
          Top = 80
          Width = 94
          Height = 43
          Caption = 'Confirmar'
          TabOrder = 5
          Visible = False
          OnClick = Button2Click
        end
        object btnExcluir: TButton
          Left = 701
          Top = 79
          Width = 108
          Height = 43
          Caption = 'Excluir Lan'#231'amento'
          TabOrder = 4
          Visible = False
          OnClick = btnExcluirClick
        end
        object rgDetalheSAP: TRadioGroup
          Left = 321
          Top = 25
          Width = 567
          Height = 49
          Caption = 'Detalhe do Evento'
          Columns = 2
          Enabled = False
          ItemIndex = 1
          Items.Strings = (
            'Recarga Financeiro Antecipado(Boleto Manual SAP)'
            'Recarga Financeiro Programado no SAP')
          TabOrder = 0
        end
        object rbTipoComplemento: TRadioGroup
          Left = 899
          Top = 25
          Width = 255
          Height = 49
          Caption = 'Tipo do Complemento'
          Columns = 2
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Pr'#243'xima Recarga'
            'Fatura Atual')
          TabOrder = 3
        end
      end
      object Panel43: TPanel
        Left = 0
        Top = 544
        Width = 1229
        Height = 49
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        DesignSize = (
          1229
          49)
        object Bevel1: TBevel
          Left = 186
          Top = 6
          Width = 2
          Height = 37
          Visible = False
        end
        object Label57: TLabel
          Left = 292
          Top = 6
          Width = 86
          Height = 13
          Caption = 'Saldo Renova'#231#227'o'
          Visible = False
        end
        object Label56: TLabel
          Left = 204
          Top = 6
          Width = 54
          Height = 13
          Caption = 'Abono M'#234's'
          Visible = False
        end
        object Bevel3: TBevel
          Left = 512
          Top = 6
          Width = 2
          Height = 37
          Visible = False
        end
        object Label85: TLabel
          Left = 464
          Top = 16
          Width = 64
          Height = 13
          Caption = 'Total Abono: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label88: TLabel
          Left = 608
          Top = 16
          Width = 119
          Height = 13
          Caption = 'Total Saldo Renova'#231#227'o: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton1: TSpeedButton
          Left = 1318
          Top = 6
          Width = 32
          Height = 22
          Hint = 'Ajuda'
          Flat = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
            5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
            DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
            CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
            E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
            A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
            2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
            D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
            3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
            F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
            3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
            FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
            3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
            FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
            3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
            FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
            5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
            FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
            FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
            FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
            FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
            E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
            BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
            C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
            7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
            D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
            E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
            FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
            FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
            D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          OnClick = ButAjudaClick
        end
        object Label94: TLabel
          Left = 1318
          Top = 29
          Width = 33
          Height = 13
          Caption = 'Ajuda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btnGravarConvAlim: TBitBtn
          Left = 6
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Gravar'
          TabOrder = 0
          Visible = False
          OnClick = btnGravarConvAlimClick
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object btnCancelarConvAlim: TBitBtn
          Left = 92
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Cancelar'
          TabOrder = 1
          Visible = False
          OnClick = btnCancelarConvAlimClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object edtSaldoRenovacao: TEdit
          Left = 292
          Top = 21
          Width = 93
          Height = 21
          TabOrder = 5
          Text = '0,00'
          Visible = False
          OnKeyPress = edtAbonoMesKeyPress
        end
        object edtAbonoMes: TEdit
          Left = 204
          Top = 21
          Width = 64
          Height = 21
          TabOrder = 4
          Text = '0,00'
          Visible = False
          OnKeyPress = edtAbonoMesKeyPress
        end
        object Button1: TButton
          Left = 398
          Top = 8
          Width = 94
          Height = 34
          Caption = 'Alterar p/ Todos'
          TabOrder = 3
          Visible = False
          OnClick = Button1Click
        end
        object btnImportar: TBitBtn
          Left = 1083
          Top = 7
          Width = 158
          Height = 34
          Hint = 'Importar'
          Anchors = [akLeft]
          Caption = 'Importa'#231#227'o de Planilha'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = btnImportarClick
          NumGlyphs = 2
        end
        object lblTotalRenovacao: TJvValidateEdit
          Left = 729
          Top = 16
          Width = 93
          Height = 18
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfCurrency
          DecimalPlaces = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = True
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
        end
        object lblTotalAbono: TJvValidateEdit
          Left = 514
          Top = 16
          Width = 89
          Height = 18
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          CriticalPoints.MaxValueIncluded = False
          CriticalPoints.MinValueIncluded = False
          DisplayFormat = dfCurrency
          DecimalPlaces = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = True
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
        end
        object RichEdit1: TRichEdit
          Left = 1272
          Top = 16
          Width = 34
          Height = 25
          Lines.Strings = (
            'Importa'#231#227'o de Conveniados para cr'#233'dito alimenta'#231#227'o'
            ''
            '   Para realizar a importa'#231#227'o de um arquivo dos conveniados, '
            
              #233' necess'#225'rio que o arquivo seja do tipo (.xls) Excel 97-2003 ou ' +
              '(.xlxs) e'
            'esteja no formato esperado pelo sistema.'
            ''
            
              'Favor usar a Planilha "Importa'#231#227'o_Conveniados_Credito_Alimentaca' +
              'o.xlsx" na pasta ADMConv'#234'nio_Manuais para importar os conveniado' +
              's.'
            ''
            
              '   Obs.: Caso tenha algum erro na importa'#231#227'o ser'#225' informado na t' +
              'ela'
            ''
            '   Segue abaixo o formato para o arquivo.'
            ''
            
              '   Campo CESTA '#233' obrigat'#243'rio para as empresas 1550, 1552, 1553, ' +
              ' 1696.'
            ''
            '   Aten'#231#227'o: O nome da planilha deve ser "credito" (sem aspas)'
            ''
            
              '   Exemplo do formato da planilha, onde todos campos para atuali' +
              'za'#231#227'o'
            'foram selecionados:'
            ''
            
              'EMPRES_ID |       CONV_ID  |'#9'CODCARTIMP             |    TITULAR' +
              #9'                                  |    CESTA      |VALOR RENOVA' +
              #199'AO'
            
              '-------------|---------------|------------------------|---------' +
              '---------------------------------|-------------|----------------' +
              '------|'
            
              '1115           |      220824     |'#9'6064211018792122 |   ADRIANA ' +
              'PEREIRA SANTO'#9'                  |      50,00     |      94,18'
            
              '-------------|---------------|------------------------|---------' +
              '---------------------------------|-------------|----------------' +
              '------|'
            
              '1115           |     198828      |'#9'6064211040058172 |    ALINE D' +
              'E SOUZA BARBOSA'#9'  |      50,00     |      94,18'
            
              '-------------|---------------|------------------------|---------' +
              '---------------------------------|-------------|----------------' +
              '------|'
            
              '1115'#9'   |     232788      |'#9'6064215020951877 |   ALINE ELVILICH ' +
              'DE ANDRADE ARAUJO|       50,00    |      94,18'
            
              '-------------|---------------|------------------------|---------' +
              '---------------------------------|-------------|----------------' +
              '------|'
            ''
            ''
            ''
            ''
            '_______\credito/_________'
            ''
            ''
            ''
            
              'Para maiores informa'#231#245'es acesse a pasta "ADMConv'#234'nio - Manuais" ' +
              'dentro do "pool" '
            
              'atrav'#233's do caminho \\servidor\pool\ADMConv'#234'nio_Manuais e abra o ' +
              'arquivo Importa'#231#227'o_Conveniados_Credito_Alimentacao.pdf ')
          TabOrder = 8
          Visible = False
          WordWrap = False
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Departamentos'
      ImageIndex = 14
      OnHide = TabSheet5Hide
      OnShow = TabSheet5Show
      object Panel45: TPanel
        Left = 0
        Top = 0
        Width = 1132
        Height = 577
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel46: TPanel
          Left = 2
          Top = 2
          Width = 1128
          Height = 31
          Align = alTop
          Alignment = taLeftJustify
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = '   Usu'#225'rios cadastrados no sistema WebEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBGridEmpDptos: TJvDBGrid
          Left = 2
          Top = 33
          Width = 1128
          Height = 511
          Align = alClient
          BorderStyle = bsNone
          DataSource = DSEmpDptos
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyPress = DBGridEmpDptosKeyPress
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DEPT_ID'
              ReadOnly = True
              Title.Caption = 'Departamento ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRICAO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 833
              Visible = True
            end>
        end
        object Panel47: TPanel
          Left = 2
          Top = 544
          Width = 1128
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 2
          object BtnGravar: TBitBtn
            Left = 67
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Gravar'
            TabOrder = 1
            OnClick = BtnGravarClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object BtnCancelar: TBitBtn
            Left = 148
            Top = 3
            Width = 80
            Height = 25
            Caption = '&Cancelar'
            TabOrder = 2
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
          object BtnApagar: TBitBtn
            Left = 243
            Top = 4
            Width = 85
            Height = 25
            Hint = 'Apagar registro'
            Caption = 'Apagar (F6)'
            TabOrder = 3
            OnClick = BtnApagarClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
            Spacing = 2
          end
          object BtnInserir: TBitBtn
            Left = 0
            Top = 2
            Width = 63
            Height = 25
            Caption = '&Inserir'
            TabOrder = 0
            OnClick = BtnInserirClick
            Glyph.Data = {
              5A030000424D5A030000000000005A0100002800000020000000100000000100
              08000000000000020000232E0000232E00004900000000000000117611001379
              1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
              37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
              4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
              6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
              7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
              840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
              C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
              D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
              DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
              4848484848484848484848484848484848484848484848484848484848484848
              484848480C00000C484848484848484848484848302323304848484848484848
              4848484801161601484848484848484848484848243A3A244848484848484848
              4848484802181802484848484848484848484848253C3C254848484848484848
              48484848031A1A03484848484848484848484848263D3D264848484848484848
              48484848041B1B04484848484848484848484848273F3F274848484848484813
              0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
              1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
              2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
              0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
              484848480D21210D484848484848484848484848324444324848484848484848
              4848484810282810484848484848484848484848354545354848484848484848
              4848484812292912484848484848484848484848374646374848484848484848
              48484848152A2A15484848484848484848484848394747394848484848484848
              484848481E16161E484848484848484848484848423A3A424848484848484848
              484848484848484848484848484848484848484848484848484848484848}
            NumGlyphs = 2
          end
        end
      end
    end
    object ts1: TTabSheet
      Caption = 'Atendimento'
      ImageIndex = 15
      OnShow = ts1Show
      object pnl2: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 593
        Align = alClient
        Caption = 'pnl2'
        TabOrder = 0
        object pgc1: TPageControl
          Left = 1
          Top = 1
          Width = 1227
          Height = 591
          ActivePage = TabAtendHistorico
          Align = alClient
          Style = tsFlatButtons
          TabOrder = 0
          object ts2: TTabSheet
            Caption = 'Em Grade - Atendimento'
            OnShow = ts2Show
            object pnl4: TPanel
              Left = 0
              Top = 0
              Width = 1098
              Height = 73
              Align = alTop
              TabOrder = 0
              object lbl11: TLabel
                Left = 312
                Top = 16
                Width = 130
                Height = 13
                Caption = 'N'#186' Protocolo / Atendimento'
              end
              object txtProtocolo: TEdit
                Left = 312
                Top = 32
                Width = 129
                Height = 21
                TabOrder = 3
              end
              object grp5: TGroupBox
                Left = 8
                Top = 8
                Width = 297
                Height = 49
                Hint = 
                  'Aplicar filtro de per'#237'odo de cadastro para localizar usu'#225'rios qu' +
                  'e efetuaram a troca da senha.'
                Caption = 'Busca por Per'#237'odo de Cadastro'
                TabOrder = 0
                object lbl12: TLabel
                  Left = 6
                  Top = 24
                  Width = 152
                  Height = 13
                  Caption = 'De:                                           a'
                end
                object dtInicial: TJvDateEdit
                  Left = 28
                  Top = 20
                  Width = 107
                  Height = 21
                  NumGlyphs = 2
                  ShowNullDate = False
                  TabOrder = 0
                end
                object dtFinal: TJvDateEdit
                  Left = 180
                  Top = 21
                  Width = 107
                  Height = 21
                  NumGlyphs = 2
                  ShowNullDate = False
                  TabOrder = 1
                end
              end
              object btnBuscar: TBitBtn
                Left = 456
                Top = 16
                Width = 89
                Height = 41
                Caption = 'Buscar'
                TabOrder = 2
                OnClick = btnBuscarClick
                Glyph.Data = {
                  7E090000424D7E0900000000000036000000280000001D0000001B0000000100
                  1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
                  A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
                  EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
                  AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
                  596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
                  A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
                  D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
                  C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
                  89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
                  D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
                  C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
                  EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
                  F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
                  BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
                  FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
                  EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
                  99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
                  FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
                  D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
                  BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
                  FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
                  C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
                  ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
                  F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
                  FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
                  C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
                  FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
                  BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
                  CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400}
              end
              object GroupBox10: TGroupBox
                Left = 584
                Top = 8
                Width = 353
                Height = 57
                Caption = 'Legenda'
                TabOrder = 1
                object Label68: TLabel
                  Left = 37
                  Top = 32
                  Width = 129
                  Height = 13
                  Caption = 'Atendimento Pendente'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label73: TLabel
                  Left = 212
                  Top = 32
                  Width = 132
                  Height = 13
                  Caption = 'Atendimento Finalizado'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object PanelPendente: TPanel
                  Left = 8
                  Top = 24
                  Width = 25
                  Height = 25
                  Hint = 
                    'Linhas do Grid com a cor vermelha, inferem que o atendimento est' +
                    #225' pendente.'
                  TabOrder = 0
                end
                object PanelFinalizado: TPanel
                  Left = 184
                  Top = 24
                  Width = 25
                  Height = 25
                  Hint = 
                    'Linhas do Grid com a cor azul, inferem que o atendimento est'#225' fi' +
                    'nalizado.'
                  TabOrder = 1
                end
              end
            end
            object pnl5: TPanel
              Left = 0
              Top = 73
              Width = 1098
              Height = 337
              Align = alClient
              TabOrder = 1
              object grdOcorrencias2: TJvDBGrid
                Left = 1
                Top = 1
                Width = 1096
                Height = 335
                Align = alClient
                DataSource = DSOcorrencias
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                ReadOnly = True
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDrawColumnCell = grdOcorrencias2DrawColumnCell
                OnDblClick = grdOcorrencias2DblClick
                SelectColumnsDialogStrings.Caption = 'Select columns'
                SelectColumnsDialogStrings.OK = '&OK'
                SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
                EditControls = <>
                RowsHeight = 17
                TitleRowHeight = 17
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'atendimento_id'
                    Title.Caption = 'N'#186' Protocolo'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'nome_solicitante'
                    Title.Caption = 'Nome do Solicitante'
                    Width = 245
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'tel_solictante'
                    Title.Caption = 'Telefone do Solicitante'
                    Width = 135
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'motivo'
                    Title.Caption = 'Motivo'
                    Width = 338
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'operador'
                    Title.Caption = 'Atendente'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'STATUS_ID'
                    Title.Caption = 'Status'
                    Visible = True
                  end>
              end
            end
          end
          object ts3: TTabSheet
            Caption = 'Em Ficha - Atendimento'
            ImageIndex = 1
            OnShow = ts3Show
            object lbl9: TLabel
              Left = 472
              Top = 208
              Width = 16
              Height = 13
              Caption = 'lbl9'
            end
            object pnl1: TPanel
              Left = 0
              Top = 0
              Width = 1219
              Height = 560
              Align = alClient
              Caption = 'pnl1'
              TabOrder = 0
              object grp1: TGroupBox
                Left = 1
                Top = 50
                Width = 1217
                Height = 65
                Align = alTop
                Caption = 'Dados Cadastrais'
                TabOrder = 1
                object lbl1: TLabel
                  Left = 80
                  Top = 16
                  Width = 102
                  Height = 13
                  Caption = 'Nome / Raz'#227'o Social'
                end
                object lbl2: TLabel
                  Left = 8
                  Top = 15
                  Width = 49
                  Height = 13
                  Caption = 'Empres ID'
                  FocusControl = DBEdit1
                end
                object txtNomeEmpres: TDBEdit
                  Left = 80
                  Top = 32
                  Width = 369
                  Height = 21
                  Color = clBtnFace
                  DataField = 'NOME'
                  DataSource = DSCadastro
                  ReadOnly = True
                  TabOrder = 1
                end
                object txtCod: TDBEdit
                  Left = 8
                  Top = 32
                  Width = 65
                  Height = 21
                  Color = clBtnFace
                  DataField = 'EMPRES_ID'
                  DataSource = DSCadastro
                  ReadOnly = True
                  TabOrder = 0
                end
              end
              object grp2: TGroupBox
                Left = 1
                Top = 1
                Width = 1217
                Height = 49
                Align = alTop
                Caption = 'Ocorr'#234'ncia'
                TabOrder = 0
                object lbl3: TLabel
                  Left = 8
                  Top = 18
                  Width = 155
                  Height = 24
                  Caption = 'N'#186' do Protocolo:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -19
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object lbl4: TLabel
                  Left = 184
                  Top = 18
                  Width = 7
                  Height = 24
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -19
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
              end
              object grp3: TGroupBox
                Left = 1
                Top = 115
                Width = 1217
                Height = 142
                Align = alTop
                Caption = 'Informa'#231#245'es do Contato'
                TabOrder = 2
                object lbl5: TLabel
                  Left = 8
                  Top = 15
                  Width = 83
                  Height = 13
                  Caption = 'Nome do Contato'
                  FocusControl = DBEdit1
                end
                object lbl6: TLabel
                  Left = 328
                  Top = 15
                  Width = 42
                  Height = 13
                  Caption = 'Telefone'
                  FocusControl = DBEdit1
                end
                object lbl7: TLabel
                  Left = 8
                  Top = 57
                  Width = 87
                  Height = 13
                  Caption = 'Motivo do Contato'
                  FocusControl = DBEdit1
                end
                object Label72: TLabel
                  Left = 8
                  Top = 97
                  Width = 30
                  Height = 13
                  Caption = 'Status'
                  FocusControl = DBEdit1
                end
                object txtNome: TDBEdit
                  Left = 8
                  Top = 32
                  Width = 313
                  Height = 21
                  DataField = 'nome_solicitante'
                  DataSource = DSOcorrencias
                  TabOrder = 0
                end
                object txtTelefone: TDBEdit
                  Left = 328
                  Top = 32
                  Width = 121
                  Height = 21
                  DataField = 'tel_solictante'
                  DataSource = DSOcorrencias
                  TabOrder = 1
                end
                object txtMotivo: TDBEdit
                  Left = 8
                  Top = 72
                  Width = 441
                  Height = 21
                  DataField = 'motivo'
                  DataSource = DSOcorrencias
                  TabOrder = 2
                end
                object JvDBLookupCombo3: TJvDBLookupCombo
                  Left = 8
                  Top = 112
                  Width = 313
                  Height = 21
                  DataField = 'STATUS_ID'
                  DataSource = DSOcorrencias
                  LookupField = 'status_id'
                  LookupDisplay = 'descricao'
                  LookupSource = DSStatusAtend
                  TabOrder = 3
                end
              end
              object pnl3: TPanel
                Left = 1
                Top = 518
                Width = 1217
                Height = 41
                Align = alBottom
                TabOrder = 4
                object btn4: TSpeedButton
                  Left = 480
                  Top = 12
                  Width = 33
                  Height = 25
                  Flat = True
                  Glyph.Data = {
                    1E030000424D1E030000000000001E0100002800000020000000100000000100
                    08000000000000020000232E0000232E00003A00000000000000986532009B68
                    35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
                    6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABB00BBBC
                    BD00BDBEBE00BFBFC000D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
                    9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C1C2C300C4C5C500C5C6
                    C700C8C9C900CBCBCC00CBCCCD00CCCDCD00CECFCF00D0D0D100D3D4D400D5D5
                    D600DBDBDC00DCDCDC00DCDDDD00DFDFE000FFE3C700FFE5CC00E1E1E100E6E6
                    E600E7E7E700E8E9E900EBEBEB00EEEEEE00EFEFF000F2F2F200F3F3F300FFFF
                    FF00393939393939393939393939393939393939393939393939393939393939
                    3939393939393939393939393939393939393939393939393939393939393939
                    3939393939393939000939393939390009393939393939391024393939393910
                    24393939393939010F0139393939010F01393939393939112A1139393939112A
                    11393939393902151902393939021519023939393939122A3112393939122A31
                    123939393903161A1A03393903161A1A0339393939132C3232133939132C3232
                    1339393904171B1B1B043904171B1B1B043939391F2D3333331F391F2D333333
                    1F393905181C1C1C1C050D181C1C1C1C05393920303434343420283034343434
                    203939062F1D1D1D1D060E2F1D1D1D1D06393921383535353521293835353535
                    21393939072F1E1E1E0739072F1E1E1E07393939223836363622392238363636
                    2239393939082F2E2E083939082F2E2E08393939392338373723393923383737
                    2339393939390A2F2F0A3939390A2F2F0A393939393925383825393939253838
                    253939393939390B2F0B393939390B2F0B393939393939263826393939392638
                    26393939393939390C1439393939390C1439393939393939272B393939393927
                    2B39393939393939393939393939393939393939393939393939393939393939
                    3939393939393939393939393939393939393939393939393939393939393939
                    3939}
                  NumGlyphs = 2
                  OnClick = btnFirstBClick
                end
                object btn5: TSpeedButton
                  Left = 513
                  Top = 12
                  Width = 33
                  Height = 25
                  Flat = True
                  Glyph.Data = {
                    32030000424D3203000000000000320100002800000020000000100000000100
                    08000000000000020000232E0000232E00003F00000000000000986532009A67
                    34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
                    5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
                    BE00BEBFBF00BFC0C100D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
                    9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C1C2
                    C300C3C4C400C5C5C600C7C8C800C9CACA00CBCBCC00CDCECE00CECFCF00D0D0
                    D100D1D1D200DCDCDC00DCDDDD00DDDEDE00DEDFDF00FFE1C200FFE3C800FFE5
                    CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00ECECEC00EEEE
                    EE00EFEFF000F1F1F100F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                    000A3E3E3E3E3E3E3E3E3E3E3E3E3E3E10273E3E3E3E3E3E3E3E3E3E3E3E3E01
                    0F013E3E3E3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E0215
                    1A023E3E3E3E3E3E3E3E3E3E3E3E122B34123E3E3E3E3E3E3E3E3E3E3E03161B
                    1B033E3E3E3E3E3E3E3E3E3E3E132D3535133E3E3E3E3E3E3E3E3E3E04171C1C
                    1C043E3E3E3E3E3E3E3E3E3E212E363636213E3E3E3E3E3E3E3E3E05181D1D1D
                    1D053E3E3E3E3E3E3E3E3E223237373737223E3E3E3E3E3E3E3E06191E1E1E1E
                    1E063E3E3E3E3E3E3E3E23333838383838233E3E3E3E3E3E3E3E07311F1F1F1F
                    1F073E3E3E3E3E3E3E3E243D3939393939243E3E3E3E3E3E3E3E3E0831202020
                    20083E3E3E3E3E3E3E3E3E253D3A3A3A3A253E3E3E3E3E3E3E3E3E3E09312F2F
                    2F093E3E3E3E3E3E3E3E3E3E263D3B3B3B263E3E3E3E3E3E3E3E3E3E3E0B3130
                    300B3E3E3E3E3E3E3E3E3E3E3E273D3C3C273E3E3E3E3E3E3E3E3E3E3E3E0C31
                    310C3E3E3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E3E0D
                    310D3E3E3E3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E3E
                    0E143E3E3E3E3E3E3E3E3E3E3E3E3E3E2A2C3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
                  NumGlyphs = 2
                  OnClick = btnPriorBClick
                end
                object btn6: TSpeedButton
                  Left = 546
                  Top = 12
                  Width = 33
                  Height = 25
                  Flat = True
                  Glyph.Data = {
                    32030000424D3203000000000000320100002800000020000000100000000100
                    08000000000000020000232E0000232E00003F00000000000000986532009A67
                    34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
                    5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
                    BE00BEBFBF00BFBFC000D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
                    9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C0C1
                    C200C2C3C300C5C5C600C6C7C800C9CACA00CBCBCC00CCCDCD00CECFCF00D0D0
                    D100D1D1D200DBDBDC00DCDCDC00DCDDDD00DEDFDF00FFE1C200FFE3C800FFE5
                    CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00EBEBEB00EDED
                    ED00EFEFF000F0F0F000F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E0A003E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E27103E3E3E3E3E3E3E3E3E3E3E3E3E3E010F013E3E3E
                    3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E3E021A15023E3E
                    3E3E3E3E3E3E3E3E3E3E12342C123E3E3E3E3E3E3E3E3E3E3E3E031B1B16033E
                    3E3E3E3E3E3E3E3E3E3E1335352D133E3E3E3E3E3E3E3E3E3E3E041C1C1C1704
                    3E3E3E3E3E3E3E3E3E3E213636362E213E3E3E3E3E3E3E3E3E3E051D1D1D1D18
                    053E3E3E3E3E3E3E3E3E223737373732223E3E3E3E3E3E3E3E3E061E1E1E1E1E
                    19063E3E3E3E3E3E3E3E23383838383833233E3E3E3E3E3E3E3E071F1F1F1F1F
                    31073E3E3E3E3E3E3E3E2439393939393D243E3E3E3E3E3E3E3E082020202031
                    083E3E3E3E3E3E3E3E3E253A3A3A3A3D253E3E3E3E3E3E3E3E3E092F2F2F3109
                    3E3E3E3E3E3E3E3E3E3E263B3B3B3D263E3E3E3E3E3E3E3E3E3E0B3030310B3E
                    3E3E3E3E3E3E3E3E3E3E273C3C3D273E3E3E3E3E3E3E3E3E3E3E0C31310C3E3E
                    3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E0D310D3E3E3E
                    3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E140E3E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E2D2A3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                    3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
                  NumGlyphs = 2
                  OnClick = btnNextBClick
                end
                object btn7: TSpeedButton
                  Left = 579
                  Top = 12
                  Width = 33
                  Height = 25
                  Flat = True
                  Glyph.Data = {
                    1E030000424D1E030000000000001E0100002800000020000000100000000100
                    08000000000000020000232E0000232E00003A00000000000000986532009B68
                    35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
                    6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABA00BABB
                    BC00BCBDBE00BEBFBF00D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
                    9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C0C1C200C3C4C400C5C5
                    C600C7C8C800CACACB00CBCBCC00CBCCCD00CDCECE00CFCFD000D2D3D300D4D5
                    D500DADADB00DBDBDC00DCDDDD00DEDFDF00FFE3C700FFE5CC00E1E1E100E5E5
                    E600E7E7E700E8E9E900EAEAEB00EDEDED00EFEFF000F1F1F100F3F3F300FFFF
                    FF00393939393939393939393939393939393939393939393939393939393939
                    3939393939393939393939393939393939393939393939393939393939393939
                    3939390900393939393909003939393939393924103939393939241039393939
                    393939010F0139393939010F01393939393939112A1139393939112A11393939
                    3939390219150239393902191502393939393912312A1239393912312A123939
                    393939031A1A16033939031A1A1603393939391332322C1339391332322C1339
                    393939041B1B1B170439041B1B1B17043939391F3333332D1F391F3333332D1F
                    393939051C1C1C1C180D051C1C1C1C1805393920343434343028203434343430
                    203939061D1D1D1D2F0E061D1D1D1D2F06393921353535353829213535353538
                    213939071E1E1E2F0739071E1E1E2F0739393922363636382239223636363822
                    393939082E2E2F083939082E2E2F083939393923373738233939233737382339
                    3939390A2F2F0A3939390A2F2F0A393939393925383825393939253838253939
                    3939390B2F0B393939390B2F0B39393939393926382639393939263826393939
                    393939140C3939393939140C393939393939392B2739393939392B2739393939
                    3939393939393939393939393939393939393939393939393939393939393939
                    3939393939393939393939393939393939393939393939393939393939393939
                    3939}
                  NumGlyphs = 2
                  OnClick = btnLastBClick
                end
                object btn3: TBitBtn
                  Left = 168
                  Top = 12
                  Width = 75
                  Height = 25
                  Caption = 'Apagar'
                  TabOrder = 4
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F90E50021CCBFC7F2FFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFBFC7F20021CC7F90E5FFFFFFFFFFFFFFFFFF8191E6
                    0324CE3254FF0324CEC0C8F3FFFFFFFFFFFFFFFFFFFFFFFFC0C8F30324CE3254
                    FF0324CE8191E6FFFFFFFFFFFF0728D287A9FF3759FF3759FF0728D2C1C9F4FF
                    FFFFFFFFFFC1C9F40728D23759FF3759FF87A9FF0728D2FFFFFFFFFFFFC2CAF4
                    0C2DD587A9FF3D5FFF3D5FFF0C2DD5C2CAF4C2CAF40C2DD53D5FFF3D5FFF87A9
                    FF0C2DD5C2CAF4FFFFFFFFFFFFFFFFFFC4CCF61233DA87A9FF4567FF4567FF12
                    33DA1233DA4567FF4567FF87A9FF1233DAC4CCF6FFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFC5CDF71739DE87A9FF4E70FF4E70FF4E70FF4E70FF87A9FF1739DEC5CD
                    F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7CFF81E40E37092FF58
                    7AFF587AFF7092FF1E40E3C7CFF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFC8D1F92446E86183FF6183FF6183FF6183FF2446E8C8D1F9FFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAD2FA2B4DED6B8DFF6B8DFF87
                    A9FF87A9FF6B8DFF6B8DFF2B4DEDCAD2FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    CBD4FB3153F17496FF7496FF87A9FF3153F13153F187A9FF7496FF7496FF3153
                    F1CBD4FBFFFFFFFFFFFFFFFFFFCDD5FD3759F67C9EFF7C9EFF87A9FF3759F6CD
                    D5FDCDD5FD3759F687A9FF7C9EFF7C9EFF3759F6CDD5FDFFFFFFFFFFFF3C5EF9
                    87A9FF82A4FF87A9FF3C5EF9CED7FDFFFFFFFFFFFFCED7FD3C5EF987A9FF82A4
                    FF87A9FF3C5EF9FFFFFFFFFFFF9FB0FE4062FD87A9FF4062FDCFD8FEFFFFFFFF
                    FFFFFFFFFFFFFFFFCFD8FE4062FD87A9FF4062FD9FB0FEFFFFFFFFFFFFFFFFFF
                    A1B2FF4365FFD0D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D8FF4365
                    FFA1B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                end
                object btn1: TBitBtn
                  Left = 8
                  Top = 12
                  Width = 75
                  Height = 25
                  Caption = '&Editar'
                  TabOrder = 2
                  OnClick = btn1Click
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000000000000000000000000
                    8000008000000080800080000000800080008080000080808000C0C0C0000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFF0013FFFFFFFFFFFF8888FFFFFFFFFFFF08882FFFFFF
                    FFFFF8F888FFFFFFFFFFF7F8222FFFFFFFFFF8FF888FFFFFFFFFF8F22222FFFF
                    FFFFF8F88888FFFFFFFFFF2772222FFFFFFFFF8888888FFFFFFFFFF2772222FF
                    FFFFFFF8888888FFFFFFFFFF2772222FFFFFFFFF8888888FFFFFFFFFF2772222
                    FFFFFFFFF8888888FFFFFFFFFF2772222FFFFFFFFF8888888FFFFFFFFFF27722
                    27FFFFFFFFF8888888FFFFFFFFFF2772787FFFFFFFFF88888F8FFFFFFFFFF278
                    879FFFFFFFFFF888F88FFFFFFFFFFF78879FFFFFFFFFFF8F888FFFFFFFFFFFF7
                    77FFFFFFFFFFFFF888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                  NumGlyphs = 2
                end
                object btn2: TBitBtn
                  Left = 88
                  Top = 12
                  Width = 75
                  Height = 25
                  Caption = '&Incluir'
                  TabOrder = 3
                  OnClick = btn2Click
                  Glyph.Data = {
                    36030000424D3603000000000000360000002800000010000000100000000100
                    18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                    D9B28CD09E6ED6A87BDCB38AE2BC97E6C3A1E6C3A1E2BC97DCB38AD6A87BD09E
                    6ED9B28CFFFFFFFFFFFFFFFFFFFFFFFFCE9A67FFEEDDFFEEDDFFEEDDFFEEDDFF
                    EEDDFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDCE9A67FFFFFFFFFFFFFFFFFFFFFFFF
                    D19D6AFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEF
                    DFD19D6AFFFFFFFFFFFFFFFFFFFFFFFFD4A06DFFF0E1FFF0E1FFF0E1FFF0E1FF
                    F0E1FFF0E1FFF0E1FFF0E1FFF0E1FFF0E1D4A06DFFFFFFFFFFFFFFFFFFFFFFFF
                    D7A370FFF1E3FFF1E3FFF1E3FFF1E3FFF1E3FFF1E3FFF1E3FFF1E3FFF1E3FFF1
                    E3D7A370FFFFFFFFFFFFFFFFFFFFFFFFDBA774FFF3E6FFF3E6FFF3E6FFF3E6FF
                    F3E6FFF3E6FFF3E6FFF3E6FFF3E6FFF3E6DBA774FFFFFFFFFFFFFFFFFFFFFFFF
                    DFAB78FFF4E9FFF4E9FFF4E9FFF4E9FFF4E9FFF4E9FFF4E9FFF4E9FFF4E9FFF4
                    E9DFAB78FFFFFFFFFFFFFFFFFFFFFFFFE3AF7CFFF6ECFFF6ECFFF6ECFFF6ECFF
                    F6ECFFF6ECFFF6ECFFF6ECFFF6ECFFF6ECE3AF7CFFFFFFFFFFFFFFFFFFFFFFFF
                    E8B481FFF7F0FFF7F0FFF7F0FFF7F0FFF7F0FFF7F0FFF7F0FFF7F0FFF7F0FFF7
                    F0E8B481FFFFFFFFFFFFFFFFFFFFFFFFECB885FFF9F3FFF9F3FFF9F3FFF9F3FF
                    F9F3FFF9F3FFF9F3FFF9F3FFF9F3FFF9F3ECB885FFFFFFFFFFFFFFFFFFFFFFFF
                    F0BC89FFFAF6FFFAF6FFFAF6FFFAF6FFFAF6FFFAF6FFFAF6FFFAF6FFFAF6FFFA
                    F6F0BC89FFFFFFFFFFFFFFFFFFFFFFFFF4C08DFFFCF9FFFCF9FFFCF9FFFCF9FF
                    FCF9FFFCF9FFFCF9FFFCF9FFFCF9FFFCF9F4C08DFFFFFFFFFFFFFFFFFFFFFFFF
                    F7C390FFFDFBFFFDFBFFFDFBFFFDFBFFFDFBFFFDFBFFFDFBE6B27FCC9865CC98
                    65CC9865FFFFFFFFFFFFFFFFFFFFFFFFFAC793FFFEFDFFFEFDFFFEFDFFFEFDFF
                    FEFDFFFEFDFFFEFDFFCC98FFFFFFCC9865FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FDCA96FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC98CC9865FFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9B2FFD0A0FFD6ABFFDCB8FFE2C3FF
                    E6CCFFE6CCFFE2C3FFCC98FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                end
                object btGravar: TJvBitBtn
                  Left = 744
                  Top = 8
                  Width = 75
                  Height = 25
                  Caption = 'Gravar'
                  TabOrder = 0
                  OnClick = btGravarClick
                end
                object btCancelar: TJvBitBtn
                  Left = 832
                  Top = 8
                  Width = 75
                  Height = 25
                  Caption = 'Cancelar'
                  TabOrder = 1
                  OnClick = btCancelarClick
                end
              end
              object grp4: TGroupBox
                Left = 1
                Top = 257
                Width = 1217
                Height = 261
                Align = alClient
                Caption = 'Detalhes da Ocorr'#234'ncia'
                TabOrder = 3
                object MemoDesc_atendimento: TDBMemo
                  Left = 2
                  Top = 15
                  Width = 1092
                  Height = 34
                  Align = alClient
                  DataField = 'desc_atendimento'
                  DataSource = DSOcorrencias
                  MaxLength = 2000
                  TabOrder = 0
                end
                object GroupBox9: TGroupBox
                  Left = 2
                  Top = 49
                  Width = 1092
                  Height = 60
                  Align = alBottom
                  Caption = 'Informa'#231#245'es Adicionais/Dados da '#250'ltima altera'#231#227'o'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGray
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  object Label67: TLabel
                    Left = 8
                    Top = 15
                    Width = 141
                    Height = 13
                    Caption = 'Data da Altera'#231#227'o / Operador'
                  end
                  object txtDataAlteracao: TEdit
                    Left = 8
                    Top = 32
                    Width = 121
                    Height = 21
                    Color = clBtnFace
                    Enabled = False
                    TabOrder = 0
                  end
                  object txtOperadorAlteracao: TEdit
                    Left = 136
                    Top = 32
                    Width = 121
                    Height = 21
                    Color = clBtnFace
                    Enabled = False
                    TabOrder = 1
                  end
                end
              end
            end
          end
          object TabAtendHistorico: TTabSheet
            Caption = 'Hist'#243'rico'
            ImageIndex = 2
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 1219
              Height = 47
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label69: TLabel
                Left = 4
                Top = 4
                Width = 53
                Height = 13
                Caption = 'Data Inicial'
              end
              object Label70: TLabel
                Left = 111
                Top = 4
                Width = 48
                Height = 13
                Caption = 'Data Final'
              end
              object Label71: TLabel
                Left = 216
                Top = 4
                Width = 86
                Height = 13
                Caption = 'Selecionar Campo'
              end
              object SpeedButton2: TSpeedButton
                Left = 656
                Top = 14
                Width = 33
                Height = 25
                Flat = True
                Glyph.Data = {
                  1E030000424D1E030000000000001E0100002800000020000000100000000100
                  08000000000000020000232E0000232E00003A00000000000000986532009B68
                  35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
                  6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABB00BBBC
                  BD00BDBEBE00BFBFC000D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
                  9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C1C2C300C4C5C500C5C6
                  C700C8C9C900CBCBCC00CBCCCD00CCCDCD00CECFCF00D0D0D100D3D4D400D5D5
                  D600DBDBDC00DCDCDC00DCDDDD00DFDFE000FFE3C700FFE5CC00E1E1E100E6E6
                  E600E7E7E700E8E9E900EBEBEB00EEEEEE00EFEFF000F2F2F200F3F3F300FFFF
                  FF00393939393939393939393939393939393939393939393939393939393939
                  3939393939393939393939393939393939393939393939393939393939393939
                  3939393939393939000939393939390009393939393939391024393939393910
                  24393939393939010F0139393939010F01393939393939112A1139393939112A
                  11393939393902151902393939021519023939393939122A3112393939122A31
                  123939393903161A1A03393903161A1A0339393939132C3232133939132C3232
                  1339393904171B1B1B043904171B1B1B043939391F2D3333331F391F2D333333
                  1F393905181C1C1C1C050D181C1C1C1C05393920303434343420283034343434
                  203939062F1D1D1D1D060E2F1D1D1D1D06393921383535353521293835353535
                  21393939072F1E1E1E0739072F1E1E1E07393939223836363622392238363636
                  2239393939082F2E2E083939082F2E2E08393939392338373723393923383737
                  2339393939390A2F2F0A3939390A2F2F0A393939393925383825393939253838
                  253939393939390B2F0B393939390B2F0B393939393939263826393939392638
                  26393939393939390C1439393939390C1439393939393939272B393939393927
                  2B39393939393939393939393939393939393939393939393939393939393939
                  3939393939393939393939393939393939393939393939393939393939393939
                  3939}
                NumGlyphs = 2
                OnClick = btnFirstCClick
              end
              object SpeedButton3: TSpeedButton
                Left = 690
                Top = 14
                Width = 33
                Height = 25
                Flat = True
                Glyph.Data = {
                  32030000424D3203000000000000320100002800000020000000100000000100
                  08000000000000020000232E0000232E00003F00000000000000986532009A67
                  34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
                  5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
                  BE00BEBFBF00BFC0C100D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
                  9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C1C2
                  C300C3C4C400C5C5C600C7C8C800C9CACA00CBCBCC00CDCECE00CECFCF00D0D0
                  D100D1D1D200DCDCDC00DCDDDD00DDDEDE00DEDFDF00FFE1C200FFE3C800FFE5
                  CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00ECECEC00EEEE
                  EE00EFEFF000F1F1F100F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                  000A3E3E3E3E3E3E3E3E3E3E3E3E3E3E10273E3E3E3E3E3E3E3E3E3E3E3E3E01
                  0F013E3E3E3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E0215
                  1A023E3E3E3E3E3E3E3E3E3E3E3E122B34123E3E3E3E3E3E3E3E3E3E3E03161B
                  1B033E3E3E3E3E3E3E3E3E3E3E132D3535133E3E3E3E3E3E3E3E3E3E04171C1C
                  1C043E3E3E3E3E3E3E3E3E3E212E363636213E3E3E3E3E3E3E3E3E05181D1D1D
                  1D053E3E3E3E3E3E3E3E3E223237373737223E3E3E3E3E3E3E3E06191E1E1E1E
                  1E063E3E3E3E3E3E3E3E23333838383838233E3E3E3E3E3E3E3E07311F1F1F1F
                  1F073E3E3E3E3E3E3E3E243D3939393939243E3E3E3E3E3E3E3E3E0831202020
                  20083E3E3E3E3E3E3E3E3E253D3A3A3A3A253E3E3E3E3E3E3E3E3E3E09312F2F
                  2F093E3E3E3E3E3E3E3E3E3E263D3B3B3B263E3E3E3E3E3E3E3E3E3E3E0B3130
                  300B3E3E3E3E3E3E3E3E3E3E3E273D3C3C273E3E3E3E3E3E3E3E3E3E3E3E0C31
                  310C3E3E3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E3E0D
                  310D3E3E3E3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E3E
                  0E143E3E3E3E3E3E3E3E3E3E3E3E3E3E2A2C3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
                NumGlyphs = 2
                OnClick = btnPriorCClick
              end
              object SpeedButton4: TSpeedButton
                Left = 724
                Top = 14
                Width = 33
                Height = 25
                Flat = True
                Glyph.Data = {
                  32030000424D3203000000000000320100002800000020000000100000000100
                  08000000000000020000232E0000232E00003F00000000000000986532009A67
                  34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
                  5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
                  BE00BEBFBF00BFBFC000D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
                  9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C0C1
                  C200C2C3C300C5C5C600C6C7C800C9CACA00CBCBCC00CCCDCD00CECFCF00D0D0
                  D100D1D1D200DBDBDC00DCDCDC00DCDDDD00DEDFDF00FFE1C200FFE3C800FFE5
                  CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00EBEBEB00EDED
                  ED00EFEFF000F0F0F000F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E0A003E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E27103E3E3E3E3E3E3E3E3E3E3E3E3E3E010F013E3E3E
                  3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E3E021A15023E3E
                  3E3E3E3E3E3E3E3E3E3E12342C123E3E3E3E3E3E3E3E3E3E3E3E031B1B16033E
                  3E3E3E3E3E3E3E3E3E3E1335352D133E3E3E3E3E3E3E3E3E3E3E041C1C1C1704
                  3E3E3E3E3E3E3E3E3E3E213636362E213E3E3E3E3E3E3E3E3E3E051D1D1D1D18
                  053E3E3E3E3E3E3E3E3E223737373732223E3E3E3E3E3E3E3E3E061E1E1E1E1E
                  19063E3E3E3E3E3E3E3E23383838383833233E3E3E3E3E3E3E3E071F1F1F1F1F
                  31073E3E3E3E3E3E3E3E2439393939393D243E3E3E3E3E3E3E3E082020202031
                  083E3E3E3E3E3E3E3E3E253A3A3A3A3D253E3E3E3E3E3E3E3E3E092F2F2F3109
                  3E3E3E3E3E3E3E3E3E3E263B3B3B3D263E3E3E3E3E3E3E3E3E3E0B3030310B3E
                  3E3E3E3E3E3E3E3E3E3E273C3C3D273E3E3E3E3E3E3E3E3E3E3E0C31310C3E3E
                  3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E0D310D3E3E3E
                  3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E140E3E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E2D2A3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
                  3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
                NumGlyphs = 2
                OnClick = btnNextCClick
              end
              object SpeedButton5: TSpeedButton
                Left = 758
                Top = 14
                Width = 33
                Height = 25
                Flat = True
                Glyph.Data = {
                  1E030000424D1E030000000000001E0100002800000020000000100000000100
                  08000000000000020000232E0000232E00003A00000000000000986532009B68
                  35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
                  6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABA00BABB
                  BC00BCBDBE00BEBFBF00D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
                  9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C0C1C200C3C4C400C5C5
                  C600C7C8C800CACACB00CBCBCC00CBCCCD00CDCECE00CFCFD000D2D3D300D4D5
                  D500DADADB00DBDBDC00DCDDDD00DEDFDF00FFE3C700FFE5CC00E1E1E100E5E5
                  E600E7E7E700E8E9E900EAEAEB00EDEDED00EFEFF000F1F1F100F3F3F300FFFF
                  FF00393939393939393939393939393939393939393939393939393939393939
                  3939393939393939393939393939393939393939393939393939393939393939
                  3939390900393939393909003939393939393924103939393939241039393939
                  393939010F0139393939010F01393939393939112A1139393939112A11393939
                  3939390219150239393902191502393939393912312A1239393912312A123939
                  393939031A1A16033939031A1A1603393939391332322C1339391332322C1339
                  393939041B1B1B170439041B1B1B17043939391F3333332D1F391F3333332D1F
                  393939051C1C1C1C180D051C1C1C1C1805393920343434343028203434343430
                  203939061D1D1D1D2F0E061D1D1D1D2F06393921353535353829213535353538
                  213939071E1E1E2F0739071E1E1E2F0739393922363636382239223636363822
                  393939082E2E2F083939082E2E2F083939393923373738233939233737382339
                  3939390A2F2F0A3939390A2F2F0A393939393925383825393939253838253939
                  3939390B2F0B393939390B2F0B39393939393926382639393939263826393939
                  393939140C3939393939140C393939393939392B2739393939392B2739393939
                  3939393939393939393939393939393939393939393939393939393939393939
                  3939393939393939393939393939393939393939393939393939393939393939
                  3939}
                NumGlyphs = 2
                OnClick = btnLastCClick
              end
              object dtIniHistorico: TJvDateEdit
                Left = 4
                Top = 18
                Width = 102
                Height = 21
                Hint = 'Data inicial para busca'
                NumGlyphs = 2
                ShowNullDate = False
                TabOrder = 1
                OnExit = datainiExit
              end
              object dtFimHistorico: TJvDateEdit
                Left = 110
                Top = 18
                Width = 102
                Height = 21
                Hint = 'Data final para busca'
                NumGlyphs = 2
                ShowNullDate = False
                TabOrder = 2
                OnExit = datainiExit
              end
              object ComboBox1: TComboBox
                Left = 215
                Top = 18
                Width = 305
                Height = 21
                Hint = 'Selecionar campos para pesquisa'
                Style = csDropDownList
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 3
                Text = 'Todos os Campos'
                Items.Strings = (
                  'Todos os Campos')
              end
              object JvBitBtn1: TJvBitBtn
                Left = 544
                Top = 8
                Width = 89
                Height = 31
                Caption = '&Visualizar'
                TabOrder = 0
                OnClick = JvBitBtn1Click
                Glyph.Data = {
                  7E090000424D7E0900000000000036000000280000001D0000001B0000000100
                  1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
                  A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
                  EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
                  AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
                  596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
                  A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
                  D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
                  C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
                  89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
                  D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
                  C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
                  EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
                  F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
                  BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
                  FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
                  EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
                  99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
                  FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
                  D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
                  BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
                  FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
                  C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
                  ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
                  F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
                  FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
                  C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
                  FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
                  D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
                  BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
                  CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                  D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                  C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                  D400}
              end
            end
            object JvDBGrid2: TJvDBGrid
              Left = 0
              Top = 47
              Width = 1219
              Height = 513
              Align = alClient
              DataSource = DSLogOcorrencias
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              SelectColumnsDialogStrings.Caption = 'Select columns'
              SelectColumnsDialogStrings.OK = '&OK'
              SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
              EditControls = <>
              RowsHeight = 17
              TitleRowHeight = 17
              Columns = <
                item
                  Expanded = False
                  FieldName = 'detalhe'
                  Title.Caption = 'Detalhes'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'data_hora'
                  Title.Caption = 'Data / Hora'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'cadastro'
                  Title.Caption = 'Cadastro'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'campo'
                  Title.Caption = 'Campo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'valor_ant'
                  Title.Caption = 'Valor anterior'
                  Width = 283
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'valor_pos'
                  Title.Caption = 'Valor Atual'
                  Width = 281
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'operador'
                  Title.Caption = 'Operador'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'operacao'
                  Title.Caption = 'Opera'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'motivo'
                  Title.Caption = 'Motivo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'solicitante'
                  Title.Caption = 'Solicitante'
                  Width = 37
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object ParcelamentoEspecifico: TTabSheet
      Caption = 'Parcelamento Especifico'
      ImageIndex = 16
      OnHide = ParcelamentoEspecificoHide
      OnShow = ParcelamentoEspecificoShow
      object GridEmpCredParcelamento: TJvDBGrid
        Left = 0
        Top = 25
        Width = 1358
        Height = 562
        Align = alClient
        DataSource = DSParcelaEspecifica
        DefaultDrawing = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = GridEmpCredParcelamentoColExit
        OnKeyDown = GridEmpCredParcelamentoKeyDown
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'cred_id'
            Title.Caption = 'Cod. Credenciado'
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_ESTABELECIMENTO'
            Title.Caption = 'Nome'
            Width = 186
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'liberado'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Liberado'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 60
            Visible = True
          end>
      end
      object Panel49: TPanel
        Left = 0
        Top = 587
        Width = 1358
        Height = 31
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        object btnGravaEmpLib: TBitBtn
          Left = 75
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Gravar'
          TabOrder = 1
          OnClick = btnGravaEmpLibClick
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object btnCancelEmpLib: TBitBtn
          Left = 157
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Cancelar'
          TabOrder = 2
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object btnInserirNovaParcela: TBitBtn
          Left = 8
          Top = 3
          Width = 65
          Height = 25
          Caption = '&Inserir'
          TabOrder = 0
          OnClick = btnInserirNovaParcelaClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D984D11
            76111176114D984DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF13791354BA5454BA54137913FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF177D1757
            BD5757BD57177D17FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF1C821C5BC15B5BC15B1C821CFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF22882260
            C76060C760228822FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5EAA5E
            2C922C319731379D373EA43E43A94365CC6565CC6543A9433EA43E379D373197
            312C922C5EAA5EFFFFFFFFFFFF2F952F6BD26B6BD26B6BD26B6BD26B6BD26B6B
            D26B6BD26B6BD26B6BD26B6BD26B6BD26B6BD26B2F952FFFFFFFFFFFFF359B35
            87EE8787EE8787EE8787EE877CE37C70D77070D7707CE37C87EE8787EE8787EE
            8787EE87359B35FFFFFFFFFFFF6DB96D3FA53F45AB454BB14B50B65055BB5576
            DD7676DD7655BB5550B6504BB14B45AB453FA53F6DB96DFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF42A8427BE27B7BE27B42A842FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF48AE4880
            E78080E78048AE48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF4DB34D84EB8484EB844DB34DFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF51B75187
            EE8787EE8751B751FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF7FCB7F54BA5454BA547FCB7FFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        end
      end
      object Panel50: TPanel
        Left = 0
        Top = 0
        Width = 1358
        Height = 25
        Align = alTop
        BorderStyle = bsSingle
        TabOrder = 0
        object Label82: TLabel
          Left = 8
          Top = 4
          Width = 347
          Height = 13
          Caption = 'Estabelecimentos liberados para venda parcelada espec'#237'fica'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object LimiteConv: TTabSheet
      Caption = 'Manut. Limites Conveniado'
      ImageIndex = 17
      OnShow = LimiteConvShow
      object Panel51: TPanel
        Left = 0
        Top = 544
        Width = 1229
        Height = 49
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        DesignSize = (
          1229
          49)
        object Bevel2: TBevel
          Left = 186
          Top = 6
          Width = 2
          Height = 37
        end
        object Label84: TLabel
          Left = 12
          Top = 6
          Width = 73
          Height = 13
          Caption = 'Novo Limite R$'
        end
        object Bevel4: TBevel
          Left = 400
          Top = 6
          Width = 2
          Height = 37
        end
        object BitBtn5: TBitBtn
          Left = 502
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Gravar'
          TabOrder = 1
          OnClick = BitBtn5Click
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object BitBtn6: TBitBtn
          Left = 412
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Cancelar'
          TabOrder = 0
          OnClick = btnCancelarConvAlimClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object edtLimiteMes: TEdit
          Left = 12
          Top = 21
          Width = 93
          Height = 21
          TabOrder = 4
          Text = '0,00'
          OnKeyPress = edtAbonoMesKeyPress
        end
        object btnAlterarTodosLimites: TButton
          Left = 110
          Top = 8
          Width = 94
          Height = 34
          Caption = 'Alterar p/ Todos'
          TabOrder = 3
          OnClick = Button1Click
        end
        object btnImportarLimiteConv: TBitBtn
          Left = 211
          Top = 7
          Width = 158
          Height = 34
          Hint = 'Importar'
          Anchors = [akLeft]
          Caption = 'Importa'#231#227'o de Planilha'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnImportarLimiteConvClick
          NumGlyphs = 2
        end
      end
      object Panel52: TPanel
        Left = 0
        Top = 0
        Width = 1229
        Height = 33
        Align = alTop
        Alignment = taLeftJustify
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label87: TLabel
          Left = 8
          Top = 8
          Width = 74
          Height = 13
          Caption = 'Conveniados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object dbGrigManutLimitesConveniados: TJvDBGrid
        Left = 0
        Top = 33
        Width = 1229
        Height = 511
        Align = alClient
        DataSource = DSManutencaoLimiteConv
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = dbGrigManutLimitesConveniadosColExit
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'conv_id'
            ReadOnly = True
            Title.Caption = 'Cod Conv'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titular'
            ReadOnly = True
            Title.Caption = 'Nome do Titular'
            Width = 405
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'limite_mes'
            Title.Caption = 'Limite R$'
            Width = 181
            Visible = True
          end>
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Manut. Cart'#245'es/Empresa'
      ImageIndex = 18
      OnExit = TabSheet6Exit
      OnShow = TabSheet6Show
      object Panel53: TPanel
        Left = 0
        Top = 569
        Width = 1358
        Height = 49
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        object BitBtn8: TBitBtn
          Left = 398
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Gravar'
          TabOrder = 1
          Visible = False
          OnClick = BitBtn5Click
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object BitBtn9: TBitBtn
          Left = 308
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Cancelar'
          TabOrder = 0
          Visible = False
          OnClick = btnCancelarConvAlimClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object btnSoliciarNovaVia: TBitBtn
          Left = 3
          Top = 8
          Width = 118
          Height = 34
          Hint = 'Atualizar c'#243'digo de importa'#231#227'o.'
          Caption = 'Solicitar Nova Via'
          TabOrder = 2
          OnClick = btnSoliciarNovaViaClick
          Glyph.Data = {
            B2030000424DB203000000000000B20100002800000020000000100000000100
            08000000000000020000232E0000232E00005F000000000000000D7D0D001785
            1700278F27003B9D3B00469F46004A9D4A004FAB4F0052AC52005BAC5B0064B9
            640069BC69006DB96D0076C576007FC47F007AC87A0086BD860086BE8600B7B8
            B900BABBBC0086C0860080CD80008DCF8D008AD48A008CD58C0092C6920090C8
            900097DC97009DE09D00B1E3B100B3E0B300B4EEB400BBE3BB00B7F0B700BCF3
            BC00BFC0C100C6C7C800C9CACA00CDCECE00CECFCF00C2DFC200D0D0D100D5D5
            D600D6D6D700DADADB00DBDBDC00DCDCDC00DEDFDF00DFDFE000C4E0C400C6E7
            C600C5EAC500C9E3C900CCE4CC00CBEECB00C7FAC700C9FBC900D4ECD400D6ED
            D600D3FFD300DEF1DE00E1E1E100E2E2E200E6E6E600E8E8E800EBEBEB00ECEC
            EC00EEEEEE00EEEFEF00EFEFF000E1F4E100E4F6E400E5F6E500E2FFE200ECF5
            EC00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F4F5F500F5F5F500F6F6
            F600F7F7F700F0F8F000F2FBF200F1FFF100F4FFF400F8F8F800F9F9F900FAFA
            FA00FBFBFB00F8FCF800FDFDFD00FEFEFE00FFFFFF005E5E5E5E5E5E5E5E1010
            5E5E5E5E5E5E5E5E5E5E5E5E5E5E2D2D5E5E5E5E5E5E5E5E5E5E5E5E5E5E010C
            015E5E5E5E5E5E5E5E5E5E5E5E5E122B125E5E5E5E5E5E5E491304020202021A
            0E025E5E5E5E5E5E592D24222222223E2D225E5E5E5E5E34080E1A2020202020
            201A035E5E5E5E4A282D3E4A4A4A4A4A4A3E235E5E5E5B0B21565E5E5E5E5E48
            485E065E5E5E5D2A4C5D5E5E5E5E5E5A5A5E255E5E5E381E5E3617090909095E
            5E095E5E5E5E4E435E503D292929295E5E295E5E5E5E31550E1F3B5E5E5E0E5E
            0E5E5E5E270F4A5D2D42525E5E5E2D5E2D5E5E5E422C3216455E5E5E5E5E1616
            5E5E5E5E05004B3D575E5E5E5E5E3D3D5E5E5E5E2411351C5E5E5E5E01015E5E
            5E5E5E3001014D415E5E5E5E12125E5E5E5E5E42121247465E5E5E020E025E5E
            5E3318020E0458585E5E5E222D225E5E5E442F222D245E5E5E5E03141A030303
            03070A1A20195E5E5E5E232E3E23232323262A3E4A2F5E5E5E061B2020202020
            2020375E0B535E5E5E253F4A4A4A4A4A4A4A515E2A5A5E5E5E095E48485E5E5E
            5E5E3A0D395E5E5E5E295E5A5A5E5E5E5E5E582D4F5E5E5E5E5E0E5E5E0E0E0E
            0E151D545E5E5E5E5E5E2D5E5E2D2D2D2D3C405C5E5E5E5E5E5E5E165E165E5E
            5E5E5E5E5E5E5E5E5E5E5E3D5E3D5E5E5E5E5E5E5E5E5E5E5E5E5E5E1A1A5E5E
            5E5E5E5E5E5E5E5E5E5E5E5E3E3E5E5E5E5E5E5E5E5E}
          NumGlyphs = 2
        end
        object btnReimprimir: TBitBtn
          Left = 123
          Top = 8
          Width = 118
          Height = 34
          Caption = 'Reimprimir Cart'#227'o'
          TabOrder = 3
          OnClick = btnReimprimirClick
          Glyph.Data = {
            B2030000424DB203000000000000B20100002800000020000000100000000100
            08000000000000020000232E0000232E00005F000000000000000D7D0D001785
            1700278F27003B9D3B00469F46004A9D4A004FAB4F0052AC52005BAC5B0064B9
            640069BC69006DB96D0076C576007FC47F007AC87A0086BD860086BE8600B7B8
            B900BABBBC0086C0860080CD80008DCF8D008AD48A008CD58C0092C6920090C8
            900097DC97009DE09D00B1E3B100B3E0B300B4EEB400BBE3BB00B7F0B700BCF3
            BC00BFC0C100C6C7C800C9CACA00CDCECE00CECFCF00C2DFC200D0D0D100D5D5
            D600D6D6D700DADADB00DBDBDC00DCDCDC00DEDFDF00DFDFE000C4E0C400C6E7
            C600C5EAC500C9E3C900CCE4CC00CBEECB00C7FAC700C9FBC900D4ECD400D6ED
            D600D3FFD300DEF1DE00E1E1E100E2E2E200E6E6E600E8E8E800EBEBEB00ECEC
            EC00EEEEEE00EEEFEF00EFEFF000E1F4E100E4F6E400E5F6E500E2FFE200ECF5
            EC00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F4F5F500F5F5F500F6F6
            F600F7F7F700F0F8F000F2FBF200F1FFF100F4FFF400F8F8F800F9F9F900FAFA
            FA00FBFBFB00F8FCF800FDFDFD00FEFEFE00FFFFFF005E5E5E5E5E5E5E5E1010
            5E5E5E5E5E5E5E5E5E5E5E5E5E5E2D2D5E5E5E5E5E5E5E5E5E5E5E5E5E5E010C
            015E5E5E5E5E5E5E5E5E5E5E5E5E122B125E5E5E5E5E5E5E491304020202021A
            0E025E5E5E5E5E5E592D24222222223E2D225E5E5E5E5E34080E1A2020202020
            201A035E5E5E5E4A282D3E4A4A4A4A4A4A3E235E5E5E5B0B21565E5E5E5E5E48
            485E065E5E5E5D2A4C5D5E5E5E5E5E5A5A5E255E5E5E381E5E3617090909095E
            5E095E5E5E5E4E435E503D292929295E5E295E5E5E5E31550E1F3B5E5E5E0E5E
            0E5E5E5E270F4A5D2D42525E5E5E2D5E2D5E5E5E422C3216455E5E5E5E5E1616
            5E5E5E5E05004B3D575E5E5E5E5E3D3D5E5E5E5E2411351C5E5E5E5E01015E5E
            5E5E5E3001014D415E5E5E5E12125E5E5E5E5E42121247465E5E5E020E025E5E
            5E3318020E0458585E5E5E222D225E5E5E442F222D245E5E5E5E03141A030303
            03070A1A20195E5E5E5E232E3E23232323262A3E4A2F5E5E5E061B2020202020
            2020375E0B535E5E5E253F4A4A4A4A4A4A4A515E2A5A5E5E5E095E48485E5E5E
            5E5E3A0D395E5E5E5E295E5A5A5E5E5E5E5E582D4F5E5E5E5E5E0E5E5E0E0E0E
            0E151D545E5E5E5E5E5E2D5E5E2D2D2D2D3C405C5E5E5E5E5E5E5E165E165E5E
            5E5E5E5E5E5E5E5E5E5E5E3D5E3D5E5E5E5E5E5E5E5E5E5E5E5E5E5E1A1A5E5E
            5E5E5E5E5E5E5E5E5E5E5E5E3E3E5E5E5E5E5E5E5E5E}
          NumGlyphs = 2
        end
      end
      object Panel54: TPanel
        Left = 0
        Top = 0
        Width = 1358
        Height = 33
        Align = alTop
        Alignment = taLeftJustify
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label86: TLabel
          Left = 8
          Top = 8
          Width = 429
          Height = 13
          Caption = 
            '2'#170' Via / Reimpress'#227'o de Cart'#245'es de Todos Conveniados Ativos da E' +
            'mpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object JvDBGrid3: TJvDBGrid
        Left = 0
        Top = 33
        Width = 1358
        Height = 536
        Align = alClient
        BorderStyle = bsNone
        DataSource = DSManutencaoCartaoConveniados
        DefaultDrawing = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = dbGridAlimTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'conv_id'
            Title.Caption = 'Conv. ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titular'
            Title.Caption = 'Titular'
            Visible = True
          end>
      end
    end
    object tsDescontoEspecial: TTabSheet
      Caption = 'Usar Desconto Especial no Estab.'
      ImageIndex = 19
      OnShow = tsDescontoEspecialShow
      object PanelHead: TPanel
        Left = 0
        Top = 0
        Width = 1358
        Height = 31
        Align = alTop
        Alignment = taLeftJustify
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = 
          '   Estabelecimentos liberados para vender com desconto especial ' +
          'para a empresa.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object PanelFooter: TPanel
        Left = 0
        Top = 587
        Width = 1358
        Height = 31
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 2
        object btnGravaCredDescontoEspecial: TBitBtn
          Left = 3
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Gravar'
          TabOrder = 0
          OnClick = btnGravaCredObrigarSenhaClick
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object btnCancelDescontoEspecial: TBitBtn
          Left = 84
          Top = 3
          Width = 80
          Height = 25
          Caption = '&Cancelar'
          TabOrder = 1
          OnClick = btnCancelDescontoEspecialClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
      end
      object GridUsaDesconto: TJvDBGrid
        Left = 0
        Top = 31
        Width = 1358
        Height = 556
        Align = alClient
        BorderStyle = bsNone
        DataSource = DSCredEmpDescontoEspecial
        DefaultDrawing = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = GridUsaDescontoColExit
        OnKeyDown = GridUsaDescontoKeyDown
        AutoAppend = False
        TitleButtons = True
        OnTitleBtnClick = GridUsaDescontoTitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'cred_id'
            ReadOnly = True
            Title.Caption = 'Estab. ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nome'
            ReadOnly = True
            Title.Caption = 'Nome do Estabelecimento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADA'
            PickList.Strings = (
              'S'
              'N')
            Title.Caption = 'Liberado'
            Visible = True
          end>
      end
    end
    object LimpaSenha: TTabSheet
      Caption = 'Utilit'#225'rios'
      ImageIndex = 20
      object JvDBGrid4: TJvDBGrid
        Left = 0
        Top = 0
        Width = 1229
        Height = 544
        Align = alClient
        DataSource = DSManutencaoLimiteConv
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'conv_id'
            ReadOnly = True
            Title.Caption = 'Cod Conv'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'titular'
            ReadOnly = True
            Title.Caption = 'Nome do Titular'
            Width = 405
            Visible = True
          end>
      end
      object Panel55: TPanel
        Left = 0
        Top = 544
        Width = 1229
        Height = 49
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        DesignSize = (
          1229
          49)
        object btnButAjudaLimpaSenha: TSpeedButton
          Left = 1318
          Top = 6
          Width = 32
          Height = 22
          Hint = 'Ajuda'
          Flat = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
            5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
            DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
            CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
            E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
            A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
            2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
            D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
            3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
            F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
            3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
            FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
            3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
            FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
            3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
            FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
            5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
            FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
            FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
            FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
            FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
            E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
            BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
            C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
            7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
            D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
            E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
            FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
            FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
            D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          OnClick = ButAjudaLimpaSenha
        end
        object lbl14: TLabel
          Left = 1318
          Top = 29
          Width = 33
          Height = 13
          Caption = 'Ajuda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BitBtn10: TBitBtn
          Left = 212
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Cancelar'
          TabOrder = 0
          OnClick = btnCancelarConvAlimClick
          Glyph.Data = {
            0E040000424D0E040000000000000E0200002800000020000000100000000100
            08000000000000020000232E0000232E000076000000000000000021CC001031
            DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
            DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
            FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
            F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
            F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
            FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
            E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
            ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
            FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
            C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
            CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
            D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
            E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
            E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
            F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
            75757575757575757575757575757575757575757575622F080000082F627575
            757575757575705E493434495E70757575757575753802030E11110E03023875
            7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
            7575757567354D5354555554534D35677575756307102A00337575442C151007
            63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
            367575604B545568345E7575745B544B607575171912301C3700317575401219
            1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
            057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
            0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
            217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
            3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
            65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
            757575756C566058483434485860566C75757575754324283237373228244375
            75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
            757575757575736A5D55555D6A73757575757575757575757575757575757575
            757575757575757575757575757575757575}
          NumGlyphs = 2
        end
        object ImporLimpaSenha: TBitBtn
          Left = 19
          Top = 7
          Width = 158
          Height = 34
          Hint = 'Importar'
          Anchors = [akLeft]
          Caption = 'Importa'#231#227'o de Planilha'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnImportLimpaSenha
          NumGlyphs = 2
        end
        object RichEditLimpaSenha: TRichEdit
          Left = 1196
          Top = 16
          Width = 53
          Height = 25
          Lines.Strings = (
            'Importa'#231#227'o de Conveniados para limpar senha do cart'#227'o'
            ''
            '   Para realizar a importa'#231#227'o de um arquivo dos conveniados, '
            #233' necess'#225'rio que o arquivo seja do tipo (.xls) Excel 97-2003 e'
            'esteja no formato esperado pelo sistema.'
            ''
            
              'Favor usar a Planilha "Importa'#231#227'o de Conveniados para limpar sen' +
              'ha do cart'#227'o.xlsx" na pasta ADMConv'#234'nio_Manuais para importar os' +
              ' conveniados.'
            ''
            
              '   Obs.: Caso tenha algum erro na importa'#231#227'o ser'#225' informado na t' +
              'ela'
            ''
            '   Segue abaixo o formato para o arquivo.'
            ''
            
              '   Campo Chapa obrigat'#243'rio contendo a identifica'#231#227'o do conveniad' +
              'o.'
            '   (Obs: Campo do tipo num'#233'rico sem decimais).'
            ''
            '   Campo NOME contendo o nome completo do conveniado.'
            ''
            '   Aten'#231#227'o: O nome da planilha deve ser CONV.'
            '   Para o nome do arquivo '#233' sugerido que n'#227'o contenha espa'#231'os.'
            ''
            
              '   Exemplo do formato da planilha, onde todos campos para atuali' +
              'za'#231#227'o'
            'foram selecionados:'
            ''
            'CHAPA |'#9'NOME'
            '--------|------------------------------------|'
            '37737  | '#9'ANDREA M S OLIMPIO'
            '--------|------------------------------------|'
            '37745  |'#9'ARACI M TAKAI YAMANAKA'
            '--------|------------------------------------|'
            '37736  |'#9'JOSE AP RODRIGUES OLIMPIO'
            '--------|------------------------------------|'
            '37738  |'#9'JOSE FERNANDO DA SILVA'
            '--------|------------------------------------|'
            '37743  |'#9'PAULO CESAR DA SILVA'
            '--------|------------------------------------|'
            '37744  |'#9'REGINA MARTA DA SILVA'
            '--------|------------------------------------|'
            '37746  |'#9'VLADIMIR RODRIGUES PALMA'
            '--------|------------------------------------|'
            ''
            ''
            ''
            ''
            '_______\conv/_________'
            ''
            ''
            ''
            
              'Para maiores informa'#231#245'es acesse a pasta "ADMConv'#234'nio - Manuais" ' +
              'dentro do "pool" '
            
              'atrav'#233's do caminho \\servidor\pool\ADMConv'#234'nio_Manuais e abra o ' +
              'arquivo Importa'#231#227'o_Conveniados _limpar_Senha_Cart'#227'o.pdf ')
          TabOrder = 2
          Visible = False
          WordWrap = False
        end
        object BitBtn7: TBitBtn
          Left = 302
          Top = 6
          Width = 80
          Height = 34
          Caption = '&Gravar'
          TabOrder = 3
          OnClick = BitBtn5Click
          Glyph.Data = {
            A6030000424DA603000000000000A60100002800000020000000100000000100
            08000000000000020000232E0000232E00005C00000000000000343434003535
            3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
            49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
            63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
            800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
            A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
            B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
            BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
            C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
            D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
            CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
            E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
            F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
            3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
            3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
            2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
            284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
            234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
            54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
            3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
            323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
            5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
            57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
            58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
            5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
            5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
            53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B}
          NumGlyphs = 2
        end
        object chkLimpaSenha: TCheckBox
          Left = 656
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Limpar Senha Conveniados'
          TabOrder = 4
        end
        object chkBloqueiaConv: TCheckBox
          Left = 400
          Top = 16
          Width = 113
          Height = 17
          Caption = 'Bloq. Conveniados'
          TabOrder = 5
        end
        object chkLiberaConv: TCheckBox
          Left = 528
          Top = 16
          Width = 129
          Height = 17
          Caption = 'Liberar Conveniados'
          TabOrder = 6
        end
      end
    end
    object UsuariosWebDiversos: TTabSheet
      Caption = 'Usu'#225'rios Web Diversos'
      ImageIndex = 21
      object Label95: TLabel
        Left = 56
        Top = 16
        Width = 71
        Height = 13
        Caption = 'Nome Fantasia'
      end
      object Label96: TLabel
        Left = 888
        Top = 120
        Width = 67
        Height = 13
        Caption = 'Nome Usuario'
      end
      object Label97: TLabel
        Left = 288
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Empres_Id'
      end
      object EdtNomeFantasia: TEdit
        Left = 56
        Top = 32
        Width = 201
        Height = 21
        TabOrder = 0
      end
      object btnBuscaEmp: TButton
        Left = 288
        Top = 64
        Width = 97
        Height = 25
        Caption = 'Buscar'
        TabOrder = 1
        OnClick = btnBuscaEmpClick
      end
      object DBGrid2: TDBGrid
        Left = 48
        Top = 144
        Width = 337
        Height = 393
        DataSource = dsEmpresas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'empres_id'
            Title.Caption = 'EMPRES_ID'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'fantasia'
            Title.Alignment = taCenter
            Title.Caption = 'FANTASIA'
            Width = 242
            Visible = True
          end>
      end
      object btnMarcarTodos: TButton
        Left = 104
        Top = 112
        Width = 89
        Height = 25
        Caption = 'Marcar Todos'
        TabOrder = 3
        OnClick = btnMarcarTodosClick
      end
      object btnAdicionar: TButton
        Left = 424
        Top = 240
        Width = 89
        Height = 25
        Caption = '>>'
        TabOrder = 4
        OnClick = btnAdicionarClick
      end
      object DBGrid3: TDBGrid
        Left = 552
        Top = 104
        Width = 313
        Height = 433
        DataSource = DataSource2
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 5
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object btnCriarEmails: TButton
        Left = 888
        Top = 192
        Width = 89
        Height = 25
        Caption = 'Criar Emails'
        TabOrder = 6
        OnClick = btnCriarEmailsClick
      end
      object txtUsuario: TEdit
        Left = 888
        Top = 136
        Width = 185
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 7
        OnKeyPress = txtUsuarioKeyPress
      end
      object btnRemover: TButton
        Left = 424
        Top = 280
        Width = 89
        Height = 25
        Caption = '<<'
        TabOrder = 8
        OnClick = btnRemoverClick
      end
      object Button3: TButton
        Left = 240
        Top = 112
        Width = 89
        Height = 25
        Caption = 'Desmarcar Todos'
        TabOrder = 9
        OnClick = Button3Click
      end
      object btnExcluit: TButton
        Left = 424
        Top = 320
        Width = 89
        Height = 25
        Caption = 'Excluir Todas'
        TabOrder = 10
        OnClick = btnExcluitClick
      end
      object CbLiberado: TCheckBox
        Left = 888
        Top = 168
        Width = 97
        Height = 17
        Caption = 'Liberado'
        TabOrder = 11
      end
      object CbPesqLiberado: TCheckBox
        Left = 56
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Liberado'
        TabOrder = 12
      end
      object txtEmpresId: TEdit
        Left = 288
        Top = 32
        Width = 97
        Height = 21
        TabOrder = 13
      end
    end
  end
  inherited PanStatus: TPanel
    Top = 18
    Width = 1237
    TabOrder = 1
    inherited panTitulo: TPanel
      Left = -72
      Width = 985
    end
  end
  inherited panStatus2: TPanel
    Top = 0
    Width = 1237
    TabOrder = 0
  end
  inherited DSCadastro: TDataSource
    OnDataChange = dsEstado_CartaoDataChange
    Left = 577
    Top = 439
  end
  inherited PopupBut: TPopupMenu
    Left = 47
    Top = 400
  end
  inherited PopupGrid1: TPopupMenu
    Left = 16
    Top = 400
  end
  inherited QHistorico: TADOQuery
    Left = 1060
    Top = 337
  end
  inherited DSHistorico: TDataSource
    Left = 543
    Top = 442
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    AfterPost = QCadastroAfterPost
    SQL.Strings = (
      
        'SELECT emp.*, UPPER(ba.descricao) as NOMEBAIRRO, UPPER(ci.nome) ' +
        'as NOMECIDADE, '
      'es.UF as NOMEESTADO FROM empresas emp inner join '
      'bairros ba on (ba.bairro_id = emp.bairro) inner join cidades ci '
      
        'on (ci.cid_id = emp.cidade) inner join estados es on (es.estado_' +
        'id = emp.estado) '
      'WHERE empres_id = 0')
    Left = 972
    Top = 337
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object QCadastroFORMA_LIMITE_ID: TIntegerField
      FieldName = 'FORMA_LIMITE_ID'
    end
    object QCadastroTIPO_CARTAO_ID: TIntegerField
      FieldName = 'TIPO_CARTAO_ID'
    end
    object QCadastroCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCadastroCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QCadastroFECHAMENTO1: TWordField
      FieldName = 'FECHAMENTO1'
    end
    object QCadastroFECHAMENTO2: TWordField
      FieldName = 'FECHAMENTO2'
    end
    object QCadastroVENCIMENTO1: TWordField
      FieldName = 'VENCIMENTO1'
    end
    object QCadastroVENCIMENTO2: TWordField
      FieldName = 'VENCIMENTO2'
    end
    object QCadastroINC_CART_PBM: TStringField
      FieldName = 'INC_CART_PBM'
      FixedChar = True
      Size = 1
    end
    object QCadastroPROG_DESC: TStringField
      FieldName = 'PROG_DESC'
      FixedChar = True
      Size = 1
    end
    object QCadastroNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroLIBERADA: TStringField
      FieldName = 'LIBERADA'
      FixedChar = True
      Size = 1
    end
    object QCadastroFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object QCadastroNOMECARTAO: TStringField
      FieldName = 'NOMECARTAO'
      Size = 45
    end
    object QCadastroCOMISSAO_CRED: TFloatField
      FieldName = 'COMISSAO_CRED'
    end
    object QCadastroFATOR_RISCO: TFloatField
      FieldName = 'FATOR_RISCO'
    end
    object QCadastroSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QCadastroCGC: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object QCadastroINSCRICAOEST: TStringField
      FieldName = 'INSCRICAOEST'
      Size = 15
    end
    object QCadastroTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      Size = 14
    end
    object QCadastroTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      Size = 14
    end
    object QCadastroFAX: TStringField
      FieldName = 'FAX'
      Size = 14
    end
    object QCadastroENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object QCadastroNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QCadastroCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object QCadastroREPRESENTANTE: TStringField
      FieldName = 'REPRESENTANTE'
      Size = 35
    end
    object QCadastroEMAIL: TStringField
      DisplayWidth = 80
      FieldName = 'EMAIL'
      Size = 80
    end
    object QCadastroHOMEPAGE: TStringField
      FieldName = 'HOMEPAGE'
      Size = 45
    end
    object QCadastroOBS1: TStringField
      FieldName = 'OBS1'
      Size = 80
    end
    object QCadastroOBS2: TStringField
      FieldName = 'OBS2'
      Size = 80
    end
    object QCadastroTODOS_SEGMENTOS: TStringField
      FieldName = 'TODOS_SEGMENTOS'
      FixedChar = True
      Size = 1
    end
    object QCadastroDT_DEBITO: TDateTimeField
      FieldName = 'DT_DEBITO'
    end
    object QCadastroTAXA_BANCO: TStringField
      FieldName = 'TAXA_BANCO'
      FixedChar = True
      Size = 1
    end
    object QCadastroVALOR_TAXA: TBCDField
      FieldName = 'VALOR_TAXA'
      Precision = 15
      Size = 2
    end
    object QCadastroTAXA_JUROS: TBCDField
      FieldName = 'TAXA_JUROS'
      Precision = 15
      Size = 2
    end
    object QCadastroMULTA: TBCDField
      FieldName = 'MULTA'
      Precision = 15
      Size = 2
    end
    object QCadastroDESCONTO_FUNC: TBCDField
      FieldName = 'DESCONTO_FUNC'
      Precision = 6
      Size = 2
    end
    object QCadastroREPASSE_EMPRESA: TBCDField
      FieldName = 'REPASSE_EMPRESA'
      Precision = 6
      Size = 2
    end
    object QCadastroBLOQ_ATE_PGTO: TStringField
      FieldName = 'BLOQ_ATE_PGTO'
      FixedChar = True
      Size = 1
    end
    object QCadastroOBS3: TStringField
      FieldName = 'OBS3'
      Size = 255
    end
    object QCadastroPEDE_NF: TStringField
      FieldName = 'PEDE_NF'
      FixedChar = True
      Size = 1
    end
    object QCadastroPEDE_REC: TStringField
      FieldName = 'PEDE_REC'
      FixedChar = True
      Size = 1
    end
    object QCadastroACEITA_PARC: TStringField
      FieldName = 'ACEITA_PARC'
      FixedChar = True
      Size = 1
    end
    object QCadastroDESCONTO_EMP: TBCDField
      FieldName = 'DESCONTO_EMP'
      Precision = 6
      Size = 2
    end
    object QCadastroUSA_CARTAO_PROPRIO: TStringField
      FieldName = 'USA_CARTAO_PROPRIO'
      FixedChar = True
      Size = 1
    end
    object QCadastroCARTAO_INI: TIntegerField
      FieldName = 'CARTAO_INI'
    end
    object QCadastroFIDELIDADE: TStringField
      FieldName = 'FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object QCadastroENCONTRO_CONTAS: TStringField
      FieldName = 'ENCONTRO_CONTAS'
      FixedChar = True
      Size = 1
    end
    object QCadastroSOLICITA_PRODUTO: TStringField
      FieldName = 'SOLICITA_PRODUTO'
      FixedChar = True
      Size = 1
    end
    object QCadastroVENDA_NOME: TStringField
      FieldName = 'VENDA_NOME'
      FixedChar = True
      Size = 1
    end
    object QCadastroOBS_FECHAMENTO: TStringField
      FieldName = 'OBS_FECHAMENTO'
      Size = 1000
    end
    object QCadastroLIMITE_PADRAO: TBCDField
      FieldName = 'LIMITE_PADRAO'
      Precision = 15
      Size = 2
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroVALE_DESCONTO: TStringField
      FieldName = 'VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object QCadastroAGENCIADOR_ID: TIntegerField
      FieldName = 'AGENCIADOR_ID'
    end
    object QCadastroSOM_PROD_PROG: TStringField
      FieldName = 'SOM_PROD_PROG'
      FixedChar = True
      Size = 1
    end
    object QCadastroEMITE_NF: TStringField
      FieldName = 'EMITE_NF'
      FixedChar = True
      Size = 1
    end
    object QCadastroRECEITA_SEM_LIMITE: TStringField
      FieldName = 'RECEITA_SEM_LIMITE'
      FixedChar = True
      Size = 1
    end
    object QCadastroCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
    object QCadastroUSA_COD_IMPORTACAO: TStringField
      FieldName = 'USA_COD_IMPORTACAO'
      FixedChar = True
      Size = 1
    end
    object QCadastroBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object QCadastroNAO_GERA_CARTAO_MENOR: TStringField
      FieldName = 'NAO_GERA_CARTAO_MENOR'
      FixedChar = True
      Size = 1
    end
    object QCadastroTIPO_CREDITO: TIntegerField
      FieldName = 'TIPO_CREDITO'
    end
    object QCadastroDIA_REPASSE: TIntegerField
      FieldName = 'DIA_REPASSE'
    end
    object QCadastroOBRIGA_SENHA: TStringField
      FieldName = 'OBRIGA_SENHA'
      FixedChar = True
      Size = 1
    end
    object QCadastroQTD_DIG_SENHA: TIntegerField
      FieldName = 'QTD_DIG_SENHA'
    end
    object QCadastroUTILIZA_RECARGA: TStringField
      FieldName = 'UTILIZA_RECARGA'
      FixedChar = True
      Size = 1
    end
    object QCadastroRESPONSAVEL_FECHAMENTO: TStringField
      FieldName = 'RESPONSAVEL_FECHAMENTO'
      Size = 15
    end
    object QCadastroMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
    object QCadastroGERA_ACUMULADO: TStringField
      FieldName = 'GERA_ACUMULADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroEXPORTA_TXT: TStringField
      FieldName = 'EXPORTA_TXT'
      FixedChar = True
      Size = 1
    end
    object QCadastroREALIZA_LANC_CREDITO: TStringField
      FieldName = 'REALIZA_LANC_CREDITO'
      FixedChar = True
      Size = 1
    end
    object QCadastroLOGRADOURO_CARTAO: TStringField
      FieldName = 'LOGRADOURO_CARTAO'
      Size = 80
    end
    object QCadastroNUMERO_CARTAO: TIntegerField
      FieldName = 'NUMERO_CARTAO'
    end
    object QCadastroCEP_CARTAO: TStringField
      FieldName = 'CEP_CARTAO'
      Size = 15
    end
    object QCadastroUSA_LIMITE_MAX: TStringField
      FieldName = 'USA_LIMITE_MAX'
      FixedChar = True
      Size = 1
    end
    object QCadastroLIMITE_MAX_POR_CONV: TBCDField
      FieldName = 'LIMITE_MAX_POR_CONV'
      DisplayFormat = '#,##0.00'
      Precision = 6
      Size = 2
    end
    object QCadastroUSA_PARCELAMENTO_ESPECIFICO: TStringField
      FieldName = 'USA_PARCELAMENTO_ESPECIFICO'
      FixedChar = True
      Size = 1
    end
    object QCadastroTEM_VALOR_FINANCEIRO: TStringField
      FieldName = 'TEM_VALOR_FINANCEIRO'
      FixedChar = True
      Size = 1
    end
    object QCadastroALTERA_LIMITE_SITE: TStringField
      FieldName = 'ALTERA_LIMITE_SITE'
      FixedChar = True
      Size = 1
    end
    object QCadastroOBRIGA_RECEITA_MEDICA: TStringField
      FieldName = 'OBRIGA_RECEITA_MEDICA'
      FixedChar = True
      Size = 1
    end
    object QCadastroPORCENT_LIMITEMAXIMO: TIntegerField
      FieldName = 'PORCENT_LIMITEMAXIMO'
    end
    object QCadastroBAIRRO: TIntegerField
      FieldName = 'BAIRRO'
    end
    object QCadastroCIDADE: TIntegerField
      FieldName = 'CIDADE'
    end
    object QCadastroESTADO: TIntegerField
      FieldName = 'ESTADO'
    end
    object QCadastroBAIRRO_CARTAO: TIntegerField
      FieldName = 'BAIRRO_CARTAO'
    end
    object QCadastroCIDADE_CARTAO: TIntegerField
      FieldName = 'CIDADE_CARTAO'
    end
    object QCadastroUF_CARTAO: TIntegerField
      FieldName = 'UF_CARTAO'
    end
    object QCadastroNOMEBAIRRO: TStringField
      FieldName = 'NOMEBAIRRO'
      ReadOnly = True
      Size = 50
    end
    object QCadastroNOMECIDADE: TStringField
      FieldName = 'NOMECIDADE'
      ReadOnly = True
      Size = 60
    end
    object QCadastroNOMEESTADO: TStringField
      FieldName = 'NOMEESTADO'
      FixedChar = True
      Size = 2
    end
    object QCadastroUSA_NOVO_CARTAO: TStringField
      FieldName = 'USA_NOVO_CARTAO'
      FixedChar = True
      Size = 1
    end
    object QCadastroDESTINATARIO_CARTAO: TStringField
      FieldName = 'DESTINATARIO_CARTAO'
      Size = 100
    end
    object QCadastroACESSA_RELATORIO_ALIMENTACAO: TStringField
      FieldName = 'ACESSA_RELATORIO_ALIMENTACAO'
      FixedChar = True
      Size = 1
    end
    object QCadastroUSA_DESCONTO_ESPECIAL: TStringField
      FieldName = 'USA_DESCONTO_ESPECIAL'
      FixedChar = True
      Size = 1
    end
    object QCadastroTRANS_SIMULTANEA: TStringField
      FieldName = 'TRANS_SIMULTANEA'
      FixedChar = True
      Size = 1
    end
    object QCadastroEMPRESA_PRINC: TStringField
      FieldName = 'EMPRESA_PRINC'
      FixedChar = True
      Size = 1
    end
    object QCadastroTRANS_UNICA: TStringField
      FieldName = 'TRANS_UNICA'
      FixedChar = True
      Size = 1
    end
    object QCadastroATIVA_SAP: TStringField
      FieldName = 'ATIVA_SAP'
      FixedChar = True
      Size = 1
    end
    object QCadastroALTERA_CADASTRO_SITE: TStringField
      FieldName = 'ALTERA_CADASTRO_SITE'
      FixedChar = True
      Size = 1
    end
    object QCadastroACESSA_CADASTRO_SITE: TStringField
      FieldName = 'ACESSA_CADASTRO_SITE'
      FixedChar = True
      Size = 1
    end
    object QCadastroFILIAL: TIntegerField
      FieldName = 'FILIAL'
    end
    object QCadastroACEITA_PERFUMARIA: TStringField
      FieldName = 'ACEITA_PERFUMARIA'
      FixedChar = True
      Size = 1
    end
    object QCadastroLANCA_TAXA_EMPRESA: TStringField
      FieldName = 'LANCA_TAXA_EMPRESA'
      FixedChar = True
      Size = 1
    end
    object QCadastroVALOR_TAXA_EMPRESA: TBCDField
      FieldName = 'VALOR_TAXA_EMPRESA'
      Precision = 6
      Size = 2
    end
    object QCadastroTIPO_ENDERECO: TStringField
      FieldName = 'TIPO_ENDERECO'
      Size = 25
    end
    object QCadastroVALIDA_CPF: TStringField
      FieldName = 'VALIDA_CPF'
      FixedChar = True
      Size = 1
    end
    object QCadastroVALIDA_CVV: TStringField
      FieldName = 'VALIDA_CVV'
      FixedChar = True
      Size = 1
    end
    object QCadastroTRANSFERE_LIMITE: TStringField
      FieldName = 'TRANSFERE_LIMITE'
      Size = 1
    end
    object QCadastrotaxa_conta_digital: TFloatField
      FieldName = 'taxa_conta_digital'
    end
    object QCadastroLANCA_DEBITO_S_CONSULTA: TStringField
      FieldName = 'LANCA_DEBITO_S_CONSULTA'
      FixedChar = True
      Size = 1
    end
    object QCadastrolibera_bloqueio: TStringField
      FieldName = 'libera_bloqueio'
      Size = 1
    end
  end
  object DSSegLib: TDataSource
    DataSet = SegLib
    OnStateChange = DSSegLibStateChange
    Left = 273
    Top = 440
  end
  object SegLib: TJvMemoryData
    FieldDefs = <
      item
        Name = 'Seg_ID'
        DataType = ftInteger
      end
      item
        Name = 'Descricao'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Liberado'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
      end>
    BeforeInsert = QCadastroBeforeInsert
    BeforeEdit = QCadastroBeforeEdit
    BeforePost = SegLibBeforePost
    AfterPost = SegLibAfterPost
    Left = 1099
    Top = 454
    object SegLibSeg_ID: TIntegerField
      FieldName = 'Seg_ID'
    end
    object SegLibDescricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'Descricao'
      Size = 45
    end
    object SegLibLiberado: TStringField
      FieldName = 'Liberado'
      Size = 1
    end
    object SegLibempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object DSDatasFecha: TDataSource
    DataSet = QDatasFecha
    OnStateChange = DSDatasFechaStateChange
    Left = 304
    Top = 440
  end
  object DSSaldos: TDataSource
    DataSet = QSaldoEmp
    Left = 716
    Top = 439
  end
  object DSQGrupo_conv_emp: TDataSource
    DataSet = QGrupo_conv_emp
    OnStateChange = DSQGrupo_conv_empStateChange
    Left = 436
    Top = 441
  end
  object PopupEmpres_ID: TPopupMenu
    OnPopup = PopupEmpres_IDPopup
    Left = 80
    Top = 400
    object AlterarIddaempresa1: TMenuItem
      Caption = 'Alterar Id da empresa'
      OnClick = AlterarIddaempresa1Click
    end
  end
  object DSGrupo_Prod: TDataSource
    DataSet = QGrupo_Prod
    OnStateChange = DSGrupo_ProdStateChange
    Left = 512
    Top = 441
  end
  object DSVendaNome: TDataSource
    OnStateChange = DSVendaNomeStateChange
    Left = 1100
    Top = 769
  end
  object DSCredLib: TDataSource
    DataSet = QCredLib
    OnStateChange = DSCredLibStateChange
    Left = 472
    Top = 441
  end
  object dsPbm: TDataSource
    DataSet = qPbm
    OnStateChange = dsPbmStateChange
    Left = 611
    Top = 439
  end
  object dsProdBloq: TDataSource
    DataSet = qProdBloq
    OnStateChange = dsProdBloqStateChange
    Left = 646
    Top = 439
  end
  object dsPrograma: TDataSource
    DataSet = QPrograma
    Left = 682
    Top = 439
  end
  object dsListaCred: TDataSource
    DataSet = qListaCred
    Left = 335
    Top = 439
  end
  object PopupAltLinearGrupoProd: TPopupMenu
    Left = 796
    Top = 321
    object AlteraoLinearDescontoemGrupodeProdutos1: TMenuItem
      Caption = 'Altera'#231#227'o Linear de Grupo de Produto por Desconto'
      OnClick = AlteraoLinearDescontoemGrupodeProdutos1Click
    end
    object AlteraoLineardeGrupodeProdutoLiberado1: TMenuItem
      Caption = 'Altera'#231#227'o Linear de Grupo de Produto Liberado'
      OnClick = AlteraoLineardeGrupodeProdutoLiberado1Click
    end
  end
  object daAgenciador: TDataSource
    Left = 369
    Top = 440
  end
  object dsFormasPgto: TDataSource
    DataSet = qFormasPgto
    Left = 20
    Top = 432
  end
  object DSCred_Obriga_Senha: TDataSource
    DataSet = QCred_Obriga_Senha
    OnStateChange = DSCred_Obriga_SenhaStateChange
    Left = 400
    Top = 441
  end
  object dsBandeiras: TDataSource
    DataSet = qBandeiras
    Left = 244
    Top = 391
  end
  object dsUsu_Web: TDataSource
    DataSet = qUsu_Web
    OnStateChange = DSCred_Obriga_SenhaStateChange
    Left = 81
    Top = 433
  end
  object dsCredAlim: TDataSource
    DataSet = QCredAlim
    OnStateChange = dsCredAlimStateChange
    Left = 49
    Top = 433
  end
  object dsCidades: TDataSource
    DataSet = qCidades
    OnDataChange = dsCidadesDataChange
    Left = 146
    Top = 414
  end
  object dsEstados: TDataSource
    DataSet = qEstados
    OnDataChange = dsEstadosDataChange
    Left = 114
    Top = 431
  end
  object dsOperadores: TDataSource
    DataSet = qOperadores
    Left = 178
    Top = 390
  end
  object dsModelosCartoes: TDataSource
    DataSet = qModelosCartoes
    Left = 210
    Top = 390
  end
  object QPrograma: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select pp.prod_id, p.descricao, pp.prog_id, b.barras codbarras,'
      
        'coalesce(pp.prc_unit, 0.00) prc_unit, coalesce(pp.perc_desc, 0.0' +
        '0) perc_desc,'
      
        'coalesce(pp.qtd_max, 0) qtd_max, coalesce(f.nome,'#39'NAO RELACIONAD' +
        'O'#39') fabricante,'
      'coalesce(pp.obrig_receita,'#39'N'#39') obrig_receita'
      'from prog_prod pp'
      'join produtos p on pp.prod_id = p.prod_id'
      'join barras b on p.prod_id = b.prod_id'
      'left join fabricantes f on p.fabr_id = f.fabr_id'
      'where pp.ativo = '#39'S'#39)
    Left = 1032
    Top = 432
    object QProgramaprod_id: TIntegerField
      FieldName = 'prod_id'
    end
    object QProgramadescricao: TStringField
      FieldName = 'descricao'
      Size = 90
    end
    object QProgramaprog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object QProgramacodbarras: TStringField
      FieldName = 'codbarras'
      Size = 13
    end
    object QProgramaprc_unit: TBCDField
      FieldName = 'prc_unit'
      ReadOnly = True
      Precision = 15
      Size = 2
    end
    object QProgramaperc_desc: TBCDField
      FieldName = 'perc_desc'
      ReadOnly = True
      Precision = 5
      Size = 2
    end
    object QProgramaqtd_max: TIntegerField
      FieldName = 'qtd_max'
      ReadOnly = True
    end
    object QProgramafabricante: TStringField
      FieldName = 'fabricante'
      ReadOnly = True
      Size = 60
    end
    object QProgramaobrig_receita: TStringField
      FieldName = 'obrig_receita'
      ReadOnly = True
      Size = 1
    end
  end
  object qPbm: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = qPbmBeforeInsert
    BeforeEdit = qPbmBeforeEdit
    BeforePost = qPbmBeforePost
    AfterPost = qPbmAfterPost
    Parameters = <
      item
        Name = 'empres'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'select p.prog_id, p.nome, case when pe.empres_id is null then '#39'N' +
        #39' else '#39'S'#39' end as participa'
      
        'from programas p left join prog_empr pe on p.prog_id = pe.prog_i' +
        'd and :empres = pe.empres_id'
      'where p.apagado <>'#39'S'#39
      'and p.dt_fim >= current_timestamp')
    Left = 1160
    Top = 328
    object qPbmprog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object qPbmnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qPbmparticipa: TStringField
      FieldName = 'participa'
      Size = 1
    end
  end
  object QGrupo_conv_emp: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = QGrupo_conv_empBeforeInsert
    AfterInsert = QGrupo_conv_empAfterInsert
    BeforeEdit = QGrupo_conv_empBeforeEdit
    BeforePost = QGrupo_conv_empBeforePost
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'Select * from grupo_conv_emp where empres_id = :empres_id')
    Left = 1064
    Top = 432
    object QGrupo_conv_empGRUPO_CONV_EMP_ID: TIntegerField
      FieldName = 'GRUPO_CONV_EMP_ID'
    end
    object QGrupo_conv_empDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 60
    end
    object QGrupo_conv_empEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object QAtualizaSaldo: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 1064
    Top = 400
  end
  object QCredLib: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = QCredLibBeforeInsert
    AfterInsert = QCredLibAfterInsert
    BeforeEdit = QCredLibBeforeEdit
    AfterPost = QCredLibAfterPost
    Parameters = <
      item
        Name = 'emp'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'Select cred.cred_id, cred.nome, coalesce(credlib.liberado,'#39'S'#39') a' +
        's liberado'
      'from credenciados cred'
      'left join emp_cred_lib credlib on cred.cred_id = credlib.cred_id'
      'and ((credlib.empres_id = :emp) or (credlib.empres_id is null))'
      'order by cred.nome')
    Left = 1032
    Top = 400
    object QCredLibcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCredLibnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QCredLibliberado: TStringField
      FieldName = 'liberado'
      Size = 1
    end
  end
  object QSaldoEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'EMPRES_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select'
      
        '      coalesce(sum(case when cc.entreg_nf  = '#39'S'#39' then cc.debito-' +
        'cc.credito else 0 end),0) as saldo_conf,'
      
        '      coalesce(sum(case when cc.entreg_nf <> '#39'S'#39' then cc.debito-' +
        'cc.credito else 0 end),0) as saldo_nconf,'
      
        '      sum(case when cc.entreg_nf <> '#39'S'#39' then cc.debito-cc.credit' +
        'o else 0 end)+'
      
        '      sum(case when cc.entreg_nf  = '#39'S'#39' then cc.debito-cc.credit' +
        'o else 0 end)as saldo_mes,'
      
        '      cc.data_fecha_emp, cc.data_venc_emp, cc.fatura_id, fat.dat' +
        'a_fatura, fat.tipo'
      '      from contacorrente cc'
      '      join conveniados conv on conv.conv_id = cc.conv_id'
      '      left join fatura fat on fat.fatura_id = cc.fatura_id'
      
        '      where cc.baixa_conveniado <> '#39'S'#39'and year(cc.DATA_FECHA_EMP' +
        ') >= year(GETDATE())'
      '      and conv.empres_id = :EMPRES_ID'
      
        '      group by cc.data_fecha_emp, cc.data_venc_emp, cc.fatura_id' +
        ', fat.data_fatura, fat.tipo'
      '      order by cc.data_fecha_emp, cc.fatura_id;')
    Left = 1064
    Top = 368
    object QSaldoEmpdata_fecha_emp: TDateTimeField
      FieldName = 'data_fecha_emp'
    end
    object QSaldoEmpdata_venc_emp: TDateTimeField
      FieldName = 'data_venc_emp'
    end
    object QSaldoEmpsaldo_nconf: TBCDField
      Alignment = taLeftJustify
      FieldName = 'saldo_nconf'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      currency = True
      Precision = 32
      Size = 2
    end
    object QSaldoEmpsaldo_conf: TBCDField
      Alignment = taLeftJustify
      FieldName = 'saldo_conf'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object QSaldoEmpsaldo_mes: TBCDField
      Alignment = taLeftJustify
      FieldName = 'saldo_mes'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      currency = True
      Precision = 32
      Size = 2
    end
    object QSaldoEmpfatura_id: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'fatura_id'
    end
    object QSaldoEmpdata_fatura: TDateTimeField
      FieldName = 'data_fatura'
    end
    object QSaldoEmptipo: TStringField
      FieldName = 'tipo'
      FixedChar = True
      Size = 1
    end
  end
  object QDatasFecha: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = QDatasFechaBeforeInsert
    BeforeEdit = QDatasFechaBeforeEdit
    BeforePost = QDatasFechaBeforePost
    AfterPost = QDatasFechaAfterPost
    BeforeDelete = QDatasFechaBeforeDelete
    OnCalcFields = QDatasFechaCalcFields
    Parameters = <>
    SQL.Strings = (
      'select EMPRES_ID, DATA_FECHA, DATA_VENC '
      'from DIA_FECHA '
      
        'where EMPRES_ID=emp.EMPRES_ID and month(data_fecha) > MONTH(GETD' +
        'ATE ()) and year(DATA_FECHA) = year(GETDATE ())')
    Left = 1128
    Top = 392
    object QDatasFechaEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QDatasFechaDATA_FECHA: TDateTimeField
      FieldName = 'DATA_FECHA'
    end
    object QDatasFechaDESC_FECHAMENTO: TStringField
      DisplayLabel = 'Extenso do Fecham.'
      FieldKind = fkCalculated
      FieldName = 'DESC_FECHAMENTO'
      ReadOnly = True
      Size = 100
      Calculated = True
    end
    object QDatasFechaDATA_VENC: TDateTimeField
      FieldName = 'DATA_VENC'
    end
    object QDatasFechaDESC_VENCIMENTO: TStringField
      DisplayLabel = 'Extenso do Venc.'
      FieldKind = fkCalculated
      FieldName = 'DESC_VENCIMENTO'
      ReadOnly = True
      Size = 100
      Calculated = True
    end
  end
  object qAgenciador: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select agenciador_id, nome from agenciadores '
      'where apagado = '#39'N'#39' order by nome')
    Left = 1160
    Top = 360
    object qAgenciadoragenciador_id: TIntegerField
      FieldName = 'agenciador_id'
    end
    object qAgenciadornome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object QGrupo_Prod: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = QGrupo_ProdBeforeInsert
    BeforeEdit = QGrupo_ProdBeforeEdit
    BeforePost = QGrupo_ProdBeforePost
    AfterPost = QGrupo_ProdAfterPost
    Parameters = <
      item
        Name = 'empre'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'emp'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      ' Select gprod.grupo_prod_id, gprod.descricao,'
      ' coalesce(remp.liberado,'#39'S'#39') liberado, '
      ' coalesce(remp.desconto,0) desconto, '
      ' coalesce(remp.preco_fabrica,'#39'N'#39') preco_fabrica,'
      
        ' case when fg.grupo_prod_id is null then '#39'N'#39' else '#39'S'#39' end as Fid' +
        'elidade,'
      ' fg.grupo_prod_id fide_id,'
      ' remp.grupo_prod_id remp_grupo_prod_id'
      ' from grupo_prod gprod'
      
        ' left join rel_emp_grupo_prod remp on remp.grupo_prod_id = gprod' +
        '.grupo_prod_id and remp.empres_id = :empre'
      
        ' left join fidel_grupo fg on fg.grupo_prod_id = gprod.grupo_prod' +
        '_id and fg.empres_id = :emp'
      ' where gprod.apagado <> '#39'S'#39
      ' order by gprod.grupo_prod_id')
    Left = 1128
    Top = 360
    object QGrupo_ProdGRUPO_PROD_ID: TIntegerField
      DisplayLabel = 'Grupo de Produto ID'
      FieldName = 'GRUPO_PROD_ID'
      Required = True
    end
    object QGrupo_ProdDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Required = True
      Size = 60
    end
    object QGrupo_ProdLIBERADO: TStringField
      DisplayLabel = 'Liberado'
      FieldName = 'LIBERADO'
      Size = 1
    end
    object QGrupo_ProdDESCONTO: TFloatField
      Alignment = taLeftJustify
      DisplayLabel = 'Desconto'
      FieldName = 'DESCONTO'
      DisplayFormat = '##0.00'
    end
    object QGrupo_ProdPRECO_FABRICA: TStringField
      DisplayLabel = 'Pre'#231'o F'#225'brica'
      FieldName = 'PRECO_FABRICA'
      Size = 1
    end
    object QGrupo_ProdREMP_GRUPO_PROD_ID: TIntegerField
      FieldName = 'REMP_GRUPO_PROD_ID'
      Required = True
    end
    object QGrupo_ProdFIDELIDADE: TStringField
      DisplayLabel = 'Acumula Pts.'
      FieldName = 'FIDELIDADE'
      Required = True
      Size = 1
    end
    object QGrupo_ProdFIDE_ID: TIntegerField
      FieldName = 'FIDE_ID'
      Required = True
    end
  end
  object qModelosCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      '  *'
      'from'
      '  modelos_cartoes')
    Left = 1096
    Top = 328
    object qModelosCartoesMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
    object qModelosCartoesDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
    end
    object qModelosCartoesOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 50
    end
  end
  object QCredAlim: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = QCredAlimBeforePost
    Parameters = <
      item
        Name = 'empres_id_1'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'empres_id'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '    c.conv_id,'
      '    c.empres_id, '
      '    c.titular, '
      #9'coalesce(c.limite_mes,0.00) limite_mes, '
      '    coalesce(alc.abono_valor,0.00) abono_mes, '
      '    coalesce(alc.renovacao_valor, 0.00) saldo_renovacao, '
      #9'alr.RENOVACAO_ID,'
      #9'alr.DATA_RENOVACAO,'
      #9'alr.TIPO_CREDITO,'
      #9'alc.DETALHE_EVENTO,'
      #9'alc.COMPLEMENTO'
      'from'
      '    conveniados c'
      
        #9'left join ALIMENTACAO_RENOVACAO alr on alr.EMPRES_ID = c.EMPRES' +
        '_ID and alr.DATA_RENOVACAO = (SELECT TOP 1 DATA_RENOVACAO FROM A' +
        'LIMENTACAO_RENOVACAO where EMPRES_ID = :empres_id_1 ORDER BY DAT' +
        'A_RENOVACAO ASC)'
      
        #9'left join ALIMENTACAO_RENOVACAO_CREDITOS_SAP alc on alc.RENOVAC' +
        'AO_ID = alr.RENOVACAO_ID and alc.CONV_ID = c.CONV_ID'
      'where c.empres_id = :empres_id '
      'AND C.LIBERADO = '#39'S'#39' AND C.APAGADO = '#39'N'#39
      'order by c.titular')
    Left = 1160
    Top = 456
    object QCredAlimconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QCredAlimempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QCredAlimtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QCredAlimlimite_mes: TBCDField
      FieldName = 'limite_mes'
      ReadOnly = True
      Precision = 15
      Size = 2
    end
    object QCredAlimabono_mes: TBCDField
      FieldName = 'abono_mes'
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object QCredAlimsaldo_renovacao: TBCDField
      FieldName = 'saldo_renovacao'
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object QCredAlimRENOVACAO_ID: TIntegerField
      FieldName = 'RENOVACAO_ID'
    end
    object QCredAlimDATA_RENOVACAO: TWideStringField
      FieldName = 'DATA_RENOVACAO'
      Size = 10
    end
    object QCredAlimTIPO_CREDITO: TStringField
      FieldName = 'TIPO_CREDITO'
      FixedChar = True
      Size = 1
    end
    object QCredAlimDETALHE_EVENTO: TStringField
      FieldName = 'DETALHE_EVENTO'
      FixedChar = True
      Size = 3
    end
    object QCredAlimCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      FixedChar = True
      Size = 2
    end
  end
  object QCred_Obriga_Senha: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeInsert = QCred_Obriga_SenhaBeforeInsert
    BeforeEdit = QCred_Obriga_SenhaBeforeEdit
    AfterPost = QCred_Obriga_SenhaAfterPost
    Parameters = <
      item
        Name = 'emp'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'Select cred.cred_id, cred.nome, coalesce(CredObrigaSenha.obrigar' +
        '_senha,'#39'N'#39') as obrigaSenha'
      'from credenciados cred'
      
        'left join emp_cred_obriga_senha CredObrigaSenha on cred.cred_id ' +
        '= CredObrigaSenha.cred_id'
      
        'and ((CredObrigaSenha.empres_id = :emp) or (CredObrigaSenha.empr' +
        'es_id is null))'
      'order by cred.nome')
    Left = 1096
    Top = 424
    object QCred_Obriga_Senhacred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCred_Obriga_Senhanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QCred_Obriga_SenhaobrigaSenha: TStringField
      FieldName = 'obrigaSenha'
      Size = 1
    end
  end
  object qProdBloq: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select pb.prod_id, p.descricao, b.barras codbarras'
      'from prod_bloq pb'
      'join produtos p on pb.prod_id = p.prod_id'
      'join barras b on p.prod_id = b.prod_id'
      'where pb.empres_id = :empresa order by p.descricao')
    Left = 1128
    Top = 424
    object qProdBloqprod_id: TIntegerField
      FieldName = 'prod_id'
    end
    object qProdBloqdescricao: TStringField
      FieldName = 'descricao'
      Size = 90
    end
    object qProdBloqcodbarras: TStringField
      FieldName = 'codbarras'
      Size = 13
    end
  end
  object QVendaNome: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'select cred.cred_id, cred.nome, cred.fantasia, coalesce(evn.libe' +
        'rado,'#39'N'#39') as liberado from credenciados cred'
      
        'left join empres_venda_nome evn on cred.cred_id = evn.cred_id an' +
        'd :empres_id = evn.empres_id order by cred.nome')
    Left = 1160
    Top = 424
    object QVendaNomecred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QVendaNomenome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QVendaNomefantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
    object QVendaNomeliberado: TStringField
      FieldName = 'liberado'
      ReadOnly = True
      Size = 1
    end
  end
  object qFormasPgto: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    BeforeInsert = qFormasPgtoBeforeInsert
    BeforeEdit = qFormasPgtoBeforeEdit
    BeforePost = qFormasPgtoBeforePost
    AfterPost = qFormasPgtoAfterPost
    Parameters = <
      item
        Name = 'cred'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select'
      '  f.forma_id,'
      '  f.descricao,'
      
        '  case when NOT (r.forma_id is null) then '#39'S'#39' else '#39'N'#39' end as li' +
        'berado'
      'from formaspagto f'
      'left join formas_emp_lib r on f.forma_id = r.forma_id'
      'and r.emp_id = :cred'
      'where f.apagado <> '#39'S'#39)
    Left = 1160
    Top = 392
    object qFormasPgtoforma_id: TIntegerField
      FieldName = 'forma_id'
    end
    object qFormasPgtodescricao: TStringField
      FieldName = 'descricao'
      Size = 60
    end
    object qFormasPgtoliberado: TStringField
      FieldName = 'liberado'
      Size = 1
    end
  end
  object qCidades: TADOQuery
    Connection = DMConexao.AdoCon
    DataSource = dsEstados
    Parameters = <
      item
        Name = 'ESTADO_ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 10
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT CID_ID, ESTADO_ID, UPPER(NOME collate sql_latin1_general_' +
        'cp1251_cs_as) as NOME FROM CIDADES'
      'WHERE ESTADO_ID = :ESTADO_ID'
      'ORDER BY NOME')
    Left = 1096
    Top = 392
    object qCidadesCID_ID: TIntegerField
      FieldName = 'CID_ID'
    end
    object qCidadesESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object qCidadesNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
  end
  object QSegLib: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'seg_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select liberado from empres_seg_lib where'
      'empres_id = :empres_id and seg_id = :seg_id')
    Left = 1032
    Top = 368
    object QSegLibliberado: TStringField
      FieldName = 'liberado'
      FixedChar = True
      Size = 1
    end
  end
  object qUsu_Web: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = qUsu_WebBeforePost
    AfterPost = qUsu_WebAfterPost
    Parameters = <
      item
        Name = 'emp_for_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select usu_id, usu_nome, usu_email, usu_senha, usu_liberado, usu' +
        '_apagado,emp_for_id,usu_cpf,usu_tipo from usuarios_web'
      'where coalesce(usu_apagado,'#39'N'#39') <> '#39'S'#39
      'and emp_for_id = :emp_for_id')
    Left = 1000
    Top = 368
    object qUsu_Webusu_id: TIntegerField
      FieldName = 'usu_id'
    end
    object qUsu_Webusu_nome: TStringField
      FieldName = 'usu_nome'
      Size = 58
    end
    object qUsu_Webusu_email: TStringField
      FieldName = 'usu_email'
      Size = 100
    end
    object qUsu_Webusu_senha: TStringField
      FieldName = 'usu_senha'
      Size = 40
    end
    object qUsu_Webusu_liberado: TStringField
      FieldName = 'usu_liberado'
      FixedChar = True
      Size = 1
    end
    object qUsu_Webusu_apagado: TStringField
      FieldName = 'usu_apagado'
      FixedChar = True
      Size = 1
    end
    object qUsu_Webusu_cpf: TStringField
      FieldName = 'usu_cpf'
      Size = 14
    end
    object qUsu_Webemp_for_id: TIntegerField
      FieldName = 'emp_for_id'
    end
    object qUsu_Webusu_tipo: TStringField
      FieldName = 'usu_tipo'
      FixedChar = True
      Size = 1
    end
  end
  object qOperadores: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select nome from usuarios where apagado <> '#39'S'#39' and liberado <> '#39 +
        'N'#39' order by nome')
    Left = 1032
    Top = 336
    object qOperadoresnome: TStringField
      FieldName = 'nome'
      Size = 58
    end
  end
  object qEstados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT ESTADO_ID, UF FROM ESTADOS'
      'ORDER BY UF')
    Left = 1000
    Top = 432
    object qEstadosESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object qEstadosUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
  end
  object QSegmentos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select seg_id, descricao from segmentos '
      'where apagado <> '#39'S'#39' order by descricao')
    Left = 1000
    Top = 336
    object QSegmentosseg_id: TIntegerField
      FieldName = 'seg_id'
    end
    object QSegmentosdescricao2: TStringField
      FieldName = 'descricao'
      Size = 58
    end
  end
  object ADOQuery2: TADOQuery
    Parameters = <>
    Left = 1000
    Top = 400
  end
  object qListaCred: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select cred_id, nome, fantasia from credenciados'
      'where coalesce(apagado,'#39'N'#39') <> '#39'S'#39)
    Left = 1096
    Top = 360
    object qListaCredcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qListaCrednome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qListaCredfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
  end
  object qUpdate: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'abono_mes'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'saldo_renovacao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'update conveniados set abono_mes = :abono_mes, saldo_renovacao =' +
        ' :saldo_renovacao')
    Left = 1128
    Top = 456
  end
  object qBandeiras: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      'BAND_ID,'
      'DESCRICAO'
      'FROM'
      'BANDEIRAS')
    Left = 1128
    Top = 328
    object qBandeirasBAND_ID: TIntegerField
      FieldName = 'BAND_ID'
    end
    object qBandeirasDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 60
    end
  end
  object QEMP_DPTOS: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'EMPRES_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT * FROM EMP_DPTOS WHERE DPTO_APAGADO = '#39'N'#39' AND EMPRES_ID =' +
        ' :EMPRES_ID ')
    Left = 960
    Top = 368
    object QEMP_DPTOSDEPT_ID: TIntegerField
      FieldName = 'DEPT_ID'
    end
    object QEMP_DPTOSDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 150
    end
    object QEMP_DPTOSEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
      LookupDataSet = QCadastro
      LookupKeyFields = 'EMPRES_ID'
      LookupResultField = 'EMPRES_ID'
      KeyFields = 'EMPRES_ID'
    end
  end
  object DSEmpDptos: TDataSource
    DataSet = QEMP_DPTOS
    Left = 752
    Top = 440
  end
  object tExcel: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\User\Meus documentos\Meus arquivos recebidos\Estabelecime' +
      'ntos.xls;Extended Properties=Excel 8.0;Persist Security Info=Fal' +
      'se'
    CursorType = ctStatic
    TableDirect = True
    Left = 792
    Top = 440
  end
  object DSOcorrencias: TDataSource
    DataSet = QOcorrencias
    OnStateChange = DSOcorrenciasStateChange
    Left = 472
    Top = 512
  end
  object QOcorrencias: TADOQuery
    Connection = DMConexao.AdoCon
    AfterInsert = QOcorrenciasAfterInsert
    BeforePost = QOcorrenciasBeforePost
    Parameters = <>
    SQL.Strings = (
      'select * from empresas_atendimento where empres_id = 0')
    Left = 432
    Top = 512
    object QOcorrenciasatendimento_id: TIntegerField
      FieldName = 'atendimento_id'
    end
    object QOcorrenciasnome_solicitante: TStringField
      FieldName = 'nome_solicitante'
      Size = 100
    end
    object QOcorrenciastel_solictante: TStringField
      FieldName = 'tel_solictante'
      Size = 30
    end
    object QOcorrenciasmotivo: TStringField
      FieldName = 'motivo'
      Size = 50
    end
    object QOcorrenciasdesc_atendimento: TStringField
      FieldName = 'desc_atendimento'
      Size = 2000
    end
    object QOcorrenciasdata_atendimento: TDateTimeField
      FieldName = 'data_atendimento'
    end
    object QOcorrenciasempres_id: TIntegerField
      FieldName = 'empres_id'
      LookupDataSet = QCadastro
      LookupKeyFields = 'EMPRES_ID'
    end
    object QOcorrenciasoperador: TStringField
      FieldName = 'operador'
      Size = 50
    end
    object QOcorrenciasSTATUS_ID: TIntegerField
      FieldName = 'STATUS_ID'
    end
  end
  object QLogOcorrencias: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM logs_ocorrencias WHERE log_id = 0')
    Left = 360
    Top = 392
    object QLogOcorrenciaslog_id: TIntegerField
      FieldName = 'log_id'
    end
    object QLogOcorrenciasjanela: TStringField
      FieldName = 'janela'
    end
    object QLogOcorrenciascampo: TStringField
      FieldName = 'campo'
    end
    object QLogOcorrenciasvalor_ant: TStringField
      FieldName = 'valor_ant'
      Size = 200
    end
    object QLogOcorrenciasvalor_pos: TStringField
      FieldName = 'valor_pos'
      Size = 200
    end
    object QLogOcorrenciasoperador: TStringField
      FieldName = 'operador'
      Size = 15
    end
    object QLogOcorrenciasoperacao: TStringField
      FieldName = 'operacao'
      Size = 15
    end
    object QLogOcorrenciasdata_hora: TDateTimeField
      FieldName = 'data_hora'
    end
    object QLogOcorrenciascadastro: TStringField
      FieldName = 'cadastro'
      Size = 30
    end
    object QLogOcorrenciasid: TIntegerField
      FieldName = 'id'
    end
    object QLogOcorrenciasdetalhe: TStringField
      FieldName = 'detalhe'
      Size = 30
    end
    object QLogOcorrenciasmotivo: TStringField
      FieldName = 'motivo'
      Size = 200
    end
    object QLogOcorrenciassolicitante: TStringField
      FieldName = 'solicitante'
      Size = 50
    end
    object QLogOcorrenciasprotocolo: TIntegerField
      FieldName = 'protocolo'
    end
  end
  object DSLogOcorrencias: TDataSource
    DataSet = QLogOcorrencias
    Left = 392
    Top = 392
  end
  object QStatusAtend: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from status_atendimento_emp')
    Left = 416
    Top = 264
    object QStatusAtendstatus_id: TIntegerField
      FieldName = 'status_id'
    end
    object QStatusAtenddescricao: TStringField
      FieldName = 'descricao'
      Size = 45
    end
  end
  object DSStatusAtend: TDataSource
    DataSet = QStatusAtend
    Left = 472
    Top = 264
  end
  object DSSegmento: TDataSource
    DataSet = QEstabSeg
    Left = 20
    Top = 475
  end
  object QEstabSeg: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select seg_id, descricao from segmentos '
      'where apagado <> '#39'S'#39' order by descricao')
    Left = 52
    Top = 475
    object QEstabSegseg_id: TIntegerField
      FieldName = 'seg_id'
    end
    object QEstabSegdescricao: TStringField
      FieldName = 'descricao'
      Size = 58
    end
  end
  object QParcelaEspecifica: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select '
      'CDE.empres_id,'
      'CDE.cred_id, '
      'CDE.liberado, '
      'C.NOME AS NOME_ESTABELECIMENTO'
      'FROM EMP_CRED_DESCONTO_ESPECIFICO CDE'
      'INNER JOIN CREDENCIADOS C'
      'ON cde.cred_id = c.cred_id '
      'AND CDE.EMPRES_ID = :EMPRES_ID')
    Left = 1068
    Top = 115
    object QParcelaEspecificacred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QParcelaEspecificaliberado: TStringField
      FieldName = 'liberado'
      FixedChar = True
      Size = 1
    end
    object QParcelaEspecificaNOME_ESTABELECIMENTO: TStringField
      FieldName = 'NOME_ESTABELECIMENTO'
      Size = 60
    end
    object QParcelaEspecificaempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object DSParcelaEspecifica: TDataSource
    DataSet = QParcelaEspecifica
    Left = 1116
    Top = 115
  end
  object QManutencaoLimiteConv: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeOpen = QManutencaoLimiteConvBeforeOpen
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select '
      'conv_id,'
      'titular, '
      'limite_mes,'
      'empres_id '
      'from conveniados '
      
        'where liberado = '#39'S'#39' and apagado = '#39'N'#39' and empres_id = :empres_i' +
        'd')
    Left = 212
    Top = 475
    object QManutencaoLimiteConvconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QManutencaoLimiteConvtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QManutencaoLimiteConvlimite_mes: TBCDField
      FieldName = 'limite_mes'
      Precision = 15
      Size = 2
    end
    object QManutencaoLimiteConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object DSManutencaoLimiteConv: TDataSource
    DataSet = QManutencaoLimiteConv
    Left = 260
    Top = 475
  end
  object QManutencaoCartaoConveniados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select '
      'conv_id,'
      'titular,'
      'chapa,'
      'empres_id'
      'from conveniados '
      
        'where liberado = '#39'S'#39' and apagado = '#39'N'#39' and empres_id = :empres_i' +
        'd')
    Left = 268
    Top = 323
    object QManutencaoCartaoConveniadosconv_id: TIntegerField
      FieldName = 'conv_id'
    end
    object QManutencaoCartaoConveniadostitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QManutencaoCartaoConveniadoschapa: TFloatField
      FieldName = 'chapa'
    end
    object QManutencaoCartaoConveniadosempres_id: TIntegerField
      FieldName = 'empres_id'
    end
  end
  object DSManutencaoCartaoConveniados: TDataSource
    DataSet = QManutencaoCartaoConveniados
    Left = 300
    Top = 323
  end
  object QCartoes: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'conv_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select * from cartoes where apagado <> '#39'S'#39' and empres_id = :empr' +
        'es_id and conv_id = :conv_id'
      'order by titular desc, nome')
    Left = 268
    Top = 353
    object QCartoesCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QCartoesCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCartoesNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QCartoesLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCartoesCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QCartoesDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QCartoesTITULAR: TStringField
      FieldName = 'TITULAR'
      FixedChar = True
      Size = 1
    end
    object QCartoesJAEMITIDO: TStringField
      FieldName = 'JAEMITIDO'
      FixedChar = True
      Size = 1
    end
    object QCartoesAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCartoesLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      DisplayFormat = '#0.00'
      Precision = 15
      Size = 2
    end
    object QCartoesCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object QCartoesPARENTESCO: TStringField
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object QCartoesDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object QCartoesNUM_DEP: TIntegerField
      FieldName = 'NUM_DEP'
    end
    object QCartoesFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QCartoesDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object QCartoesCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCartoesRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QCartoesVIA: TIntegerField
      FieldName = 'VIA'
    end
    object QCartoesDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCartoesDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCartoesOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCartoesDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCartoesOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCartoesCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCartoesATIVO: TStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object QCartoesEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCartoesSENHA: TStringField
      FieldName = 'SENHA'
      Size = 45
    end
    object QCartoesCVV: TStringField
      FieldName = 'CVV'
      FixedChar = True
      Size = 3
    end
  end
  object DSCartoes: TDataSource
    DataSet = QCartoes
    Left = 299
    Top = 354
  end
  object dsBairro_Cartao: TDataSource
    DataSet = QBairro_Cartao
    Left = 906
    Top = 536
  end
  object dsCidade_Cartao: TDataSource
    DataSet = QCidade_Cartao
    OnDataChange = dsCidade_CartaoDataChange
    Left = 858
    Top = 536
  end
  object dsEstado_Cartao: TDataSource
    DataSet = QEstado_Cartao
    OnDataChange = dsEstado_CartaoDataChange
    Left = 810
    Top = 536
  end
  object DSBairros: TDataSource
    DataSet = QBairros
    Left = 1100
    Top = 297
  end
  object QBairros: TADOQuery
    Connection = DMConexao.AdoCon
    DataSource = dsCidades
    Parameters = <
      item
        Name = 'CID_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT BAIRRO_ID, CID_ID, UPPER(DESCRICAO collate sql_latin1_gen' +
        'eral_cp1251_cs_as) AS DESCRICAO '
      'FROM BAIRROS WHERE CID_ID = :CID_ID ORDER BY DESCRICAO ASC')
    Left = 1146
    Top = 293
    object QBairrosBAIRRO_ID: TAutoIncField
      FieldName = 'BAIRRO_ID'
      ReadOnly = True
    end
    object QBairrosCID_ID: TIntegerField
      FieldName = 'CID_ID'
    end
    object QBairrosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
      Size = 50
    end
  end
  object QBairro_Cartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'CID_ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT * FROM BAIRROS WHERE CID_ID = :CID_ID ORDER BY DESCRICAO ' +
        'ASC')
    Left = 906
    Top = 504
    object QBairro_CartaoBAIRRO_ID: TAutoIncField
      FieldName = 'BAIRRO_ID'
      ReadOnly = True
    end
    object QBairro_CartaoCID_ID: TIntegerField
      FieldName = 'CID_ID'
    end
    object QBairro_CartaoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
  end
  object QCidade_Cartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'ESTADO_ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT CID_ID, ESTADO_ID, UPPER(NOME collate sql_latin1_general_' +
        'cp1251_cs_as) as NOME FROM CIDADES'
      'WHERE ESTADO_ID = :ESTADO_ID'
      'ORDER BY NOME')
    Left = 858
    Top = 504
    object QCidade_CartaoCID_ID: TIntegerField
      FieldName = 'CID_ID'
    end
    object QCidade_CartaoESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object QCidade_CartaoNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 60
    end
  end
  object QEstado_Cartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT ESTADO_ID, UF FROM ESTADOS'
      'ORDER BY UF')
    Left = 810
    Top = 504
    object QEstado_CartaoESTADO_ID: TIntegerField
      FieldName = 'ESTADO_ID'
    end
    object QEstado_CartaoUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
  end
  object QCartoesTemp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'cartao_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select * from cartoes_temp where apagado <> '#39'S'#39'  and cartao_id =' +
        ' :cartao_id'
      'order by titular desc, nome')
    Left = 340
    Top = 353
    object QCartoesTempCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QCartoesTempCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCartoesTempNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QCartoesTempLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QCartoesTempDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QCartoesTempTITULAR: TStringField
      FieldName = 'TITULAR'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempJAEMITIDO: TStringField
      FieldName = 'JAEMITIDO'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QCartoesTempCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object QCartoesTempPARENTESCO: TStringField
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object QCartoesTempDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object QCartoesTempNUM_DEP: TIntegerField
      FieldName = 'NUM_DEP'
    end
    object QCartoesTempFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object QCartoesTempCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCartoesTempRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QCartoesTempVIA: TIntegerField
      FieldName = 'VIA'
    end
    object QCartoesTempDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCartoesTempDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCartoesTempOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCartoesTempDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCartoesTempOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCartoesTempCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCartoesTempATIVO: TStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object QCartoesTempEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCartoesTempSENHA: TStringField
      FieldName = 'SENHA'
      Size = 45
    end
    object QCartoesTempLIMITE_DIARIO: TBCDField
      FieldName = 'LIMITE_DIARIO'
      Precision = 5
      Size = 2
    end
    object QCartoesTempCONSUMO_ATUAL: TBCDField
      FieldName = 'CONSUMO_ATUAL'
      Precision = 7
      Size = 2
    end
    object QCartoesTempCVV: TStringField
      FieldName = 'CVV'
      FixedChar = True
      Size = 3
    end
  end
  object DSCartoesTemp: TDataSource
    DataSet = QCartoesTemp
    Left = 371
    Top = 354
  end
  object QCredEmpDescontoEspecial: TADOQuery
    Connection = DMConexao.AdoCon
    AfterPost = QCredEmpDescontoEspecialAfterPost
    Parameters = <
      item
        Name = 'emp'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'Select cred.cred_id, cred.nome, coalesce(emp_cred_desconto_espec' +
        'ial.LIBERADA,'#39'N'#39') as LIBERADA'
      'from credenciados cred'
      
        'left join EMP_CRED_DESCONTO_ESPECIAL emp_cred_desconto_especial ' +
        'on cred.cred_id = emp_cred_desconto_especial.cred_id'
      
        'and ((emp_cred_desconto_especial.empres_id = :emp) or (emp_cred_' +
        'desconto_especial.empres_id is null))'
      'order by cred.nome')
    Left = 876
    Top = 307
    object QCredEmpDescontoEspecialcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCredEmpDescontoEspecialnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QCredEmpDescontoEspecialLIBERADA: TStringField
      FieldName = 'LIBERADA'
      Size = 1
    end
  end
  object DSCredEmpDescontoEspecial: TDataSource
    DataSet = QCredEmpDescontoEspecial
    OnStateChange = DSCredEmpDescontoEspecialStateChange
    Left = 908
    Top = 307
  end
  object QFilial: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from filial where filial_id > 0')
    Left = 674
    Top = 291
    object QFilialFILIAL_ID: TIntegerField
      FieldName = 'FILIAL_ID'
    end
    object QFilialDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 30
    end
  end
  object DSFilial: TDataSource
    DataSet = QFilial
    Left = 706
    Top = 291
  end
  object QTipoEndereco: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'SELECT cc.*, UPPER(ba.descricao) as NOMEBAIRRO, UPPER(ci.nome) a' +
        's NOMECIDADE, '
      'es.UF as NOMEESTADO FROM credenciados cc inner join '
      'bairros ba on (ba.bairro_id = cc.bairro) inner join cidades ci'
      
        'on (ci.cid_id = cc.cidade) inner join estados es on (es.estado_i' +
        'd = cc.estado) WHERE cred_id = 0')
    Left = 1196
    Top = 425
    object QTipoEnderecoTIPO_ENDERECO: TStringField
      FieldName = 'TIPO_ENDERECO'
      Size = 25
    end
  end
  object DSTipoEndereco: TDataSource
    DataSet = QTipoEndereco
    Left = 1196
    Top = 456
  end
  object QContratosEmpresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'EMPRES_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT TOP 1 * FROM  contratos_empresa WHERE EMPRES_ID = :EMPRES' +
        '_ID ORDER BY contrato_id DESC')
    Left = 320
    Top = 512
    object QContratosEmpresaCONTRATO_ID: TIntegerField
      FieldName = 'CONTRATO_ID'
    end
    object QContratosEmpresaEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QContratosEmpresaCAMINHO_ARQUIVO: TStringField
      FieldName = 'CAMINHO_ARQUIVO'
      Size = 200
    end
    object QContratosEmpresaOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 50
    end
  end
  object JvOpenDialog1: TJvOpenDialog
    Height = 490
    Width = 653
    Left = 562
    Top = 586
  end
  object QSaldoAlim: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT DATA_RENOVACAO,'
      '  (SELECT top 1 DATA_VENC'
      '   FROM DIA_FECHA'
      '   WHERE EMPRES_ID=ALI.EMPRES_ID'
      '     AND DATA_VENC >= ALI.DATA_RENOVACAO)VENCIMENTO,'
      '       SUM(ALI.RENOVACAO_VALOR) RENOVACAO_VALOR,'
      '       SUM(ALI.ABONO_VALOR) ABONO_VALOR,'
      '       SUM(ALI.RENOVACAO_VALOR+ABONO_VALOR) VALOR_TOTAL'
      'FROM ALIMENTACAO_RENOVACAO_CREDITOS_SAP ALI'
      'WHERE ALI.EMPRES_ID= 555'
      '  AND year(ALI.DATA_FECHA_EMP) = '#39'2020'#39
      '  AND ALI.RENOVACAO_VALOR IS NOT NULL'
      'GROUP BY ALI.DATA_RENOVACAO,'
      '         ALI.DATA_VENC_EMP,'
      '         ALI.TIPO_EVENTO,'
      '         ALI.DETALHE_EVENTO,'
      '         ALI.EMPRES_ID'
      'ORDER BY ALI.DATA_RENOVACAO ASC')
    Left = 1008
    Top = 512
    object QSaldoAlimDATA_RENOVACAO: TDateTimeField
      DisplayWidth = 39
      FieldName = 'DATA_RENOVACAO'
    end
    object QSaldoAlimVENCIMENTO: TDateTimeField
      DisplayWidth = 39
      FieldName = 'VENCIMENTO'
      ReadOnly = True
    end
    object QSaldoAlimRENOVACAO_VALOR: TBCDField
      FieldName = 'RENOVACAO_VALOR'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object QSaldoAlimABONO_VALOR: TBCDField
      FieldName = 'ABONO_VALOR'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object QSaldoAlimVALOR_TOTAL: TBCDField
      FieldName = 'VALOR_TOTAL'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object DataSource1: TDataSource
    DataSet = QSaldoAlim
    Left = 1010
    Top = 544
  end
  object QManutDataFechaEmp: TJvADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    DialogOptions.FormStyle = fsNormal
    Left = 650
    Top = 371
  end
  object QDatasFechaTemp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select EMPRES_ID, DATA_FECHA, DATA_VENC '
      'from DIA_FECHA '
      
        'where EMPRES_ID=0 and month(data_fecha) > MONTH(GETDATE ()) and ' +
        'year(DATA_FECHA) = year(GETDATE ())')
    Left = 394
    Top = 274
    object QDatasFechaTempEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QDatasFechaTempDATA_FECHA: TDateTimeField
      FieldName = 'DATA_FECHA'
    end
    object QDatasFechaTempDATA_VENC: TDateTimeField
      FieldName = 'DATA_VENC'
    end
  end
  object qEmpresas: TJvADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      '')
    DialogOptions.FormStyle = fsNormal
    Left = 508
    Top = 195
  end
  object dsEmpresas: TDataSource
    DataSet = qEmpresas
    Left = 472
    Top = 192
  end
  object ClientDataSet1: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 620
    Top = 147
    Data = {
      490000009619E0BD010000001800000002000000000003000000490009454D50
      5245535F494404000100000000000846414E5441534941010049000000010005
      5749445448020002001E000000}
    object ClientDataSet1EMPRES_ID: TIntegerField
      DisplayWidth = 12
      FieldName = 'EMPRES_ID'
    end
    object ClientDataSet1FANTASIA: TStringField
      DisplayWidth = 45
      FieldName = 'FANTASIA'
      Size = 30
    end
  end
  object DataSource2: TDataSource
    DataSet = ClientDataSet1
    Left = 660
    Top = 147
  end
  object ADOQuery1: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 708
    Top = 147
  end
  object ADOQuery3: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT NEXT VALUE FOR SWEB_USU_ID ID')
    Left = 748
    Top = 147
  end
  object ADOQuery4: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT NEXT VALUE FOR SWEB_USU_ID ID')
    Left = 804
    Top = 147
  end
  object ADOQuery5: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT NEXT VALUE FOR SWEB_USU_ID ID')
    Left = 852
    Top = 147
  end
end
