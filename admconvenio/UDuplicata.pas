unit UDuplicata;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, pngimage, frxpngimage;

type
  TrDuplicata = class(TQuickRep)
    QRBand1: TQRBand;
    admRazao: TQRDBText;
    admFantasia: TQRDBText;
    fatNumero: TQRDBText;
    fatNumOrdem: TQRDBText;
    fatVenc: TQRDBText;
    QRLabel10: TQRLabel;
    fatValExtenso: TQRLabel;
    admCnpj: TQRLabel;
    fatEmissao: TQRLabel;
    admFone: TQRLabel;
    admCep: TQRLabel;
    admInscr: TQRLabel;
    sacNome: TQRLabel;
    sacEnd: TQRLabel;
    sacCidade: TQRLabel;
    sacCnpj: TQRLabel;
    sacCep: TQRLabel;
    sacFone: TQRLabel;
    sacInscr: TQRLabel;
    sacBairro: TQRLabel;
    sacUf: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    admFant2: TQRLabel;
    fatValor: TQRLabel;
    sacCidade2: TQRLabel;
    QRImage1: TQRImage;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

  end;

var
  rDuplicata: TrDuplicata;

implementation

uses cartao_util, UFatura, DM, DB;

{$R *.DFM}

procedure TrDuplicata.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  admFone.Caption:= 'Fone: '+DMConexao.Adm.FieldByName('FONE').AsString;
  admCep.Caption:= 'Cep: '+DMConexao.Adm.FieldByName('CEP').AsString;
  admCnpj.Caption:= 'CNPJ: '+DMConexao.Adm.FieldByName('CNPJ').AsString;
  admFant2.Caption:= DMConexao.Adm.FieldByName('RAZAO').AsString+' ou a sua ordem na pra�a e vencimento acima indicados.';
  admInscr.Caption:= 'Inscr.Estadual: '+DMConexao.Adm.FieldByName('INSC_EST').AsString;
  fatValor.Caption:= 'R$ '+FormatFloat('###,###,##0.00',DMConexao.Query1.FieldByName('VALOR').AsCurrency);
  fatValExtenso.Caption:= extenso(DMConexao.Query1.FieldByName('VALOR').AsCurrency);
  fatEmissao.Caption:= 'Data Emiss�o: '+FormatDataBR(DMConexao.Query1.FieldByName('DATA_FATURA').AsDateTime);
  sacNome.Caption:= 'Nome do Sacado: '+DMConexao.Query2.FieldByName('NOME').AsString;
  sacEnd.Caption:= 'Endere�o: '+DMConexao.Query2.FieldByName('ENDERECO').AsString;
  sacCidade.Caption:= 'Municipio: '+DMConexao.Query2.FieldByName('CIDADE').AsString;
  sacCidade2.Caption:= 'Pra�a de Pagto: '+DMConexao.Query2.FieldByName('CIDADE').AsString;
  sacCnpj.Caption:= 'CNPJ: '+DMConexao.Query2.FieldByName('CGC').AsString;
  sacInscr.Caption:= 'Inscr.Estadual: '+DMConexao.Query2.FieldByName('INSCRICAOEST').AsString;
  sacCep.Caption:= 'Cep: '+DMConexao.Query2.FieldByName('CEP').AsString;
  sacFone.Caption:= 'Fone: '+DMConexao.Query2.FieldByName('TELEFONE1').AsString;
  sacBairro.Caption:= 'Bairro: '+DMConexao.Query2.FieldByName('BAIRRO').AsString;
  sacUf.Caption:= 'Estado: '+DMConexao.Query2.FieldByName('ESTADO').AsString;
end;

end.
