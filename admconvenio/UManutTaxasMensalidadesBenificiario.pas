unit UManutTaxasMensalidadesBenificiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ADODB, Menus, StdCtrls, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls,
  DBCtrls, DBClient;

type
  TFManutTaxaBenificiarioBemEstar = class(TFCad)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    edtConvIdBem: TDBEdit;
    Label5: TLabel;
    edtNomeBem: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label8: TLabel;
    edtConvId: TDBEdit;
    Label9: TLabel;
    edtNome: TDBEdit;
    Label10: TLabel;
    edtEmpresa: TDBEdit;
    GroupBox4: TGroupBox;
    btnBuscaConvBemEstar: TBitBtn;
    edtQtdeCartoes: TDBEdit;
    edtValor: TDBEdit;
    edtEmpresaBem: TDBEdit;
    btnBuscaConvPadrao: TBitBtn;
    QAux: TADOQuery;
    QCadastroNM_Emp: TStringField;
    QAuxempres_id: TIntegerField;
    QAuxfantasia: TStringField;
    DSAux: TDataSource;
    QCadastroID: TIntegerField;
    QCadastroCONV_ID: TIntegerField;
    QCadastroCONV_ID_BE: TIntegerField;
    QCadastroEMPRES_ID: TIntegerField;
    QCadastroEMPRES_ID_BE: TIntegerField;
    QCadastroTAXA_VALOR: TBCDField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroTAXA_ID: TStringField;
    DataSource1: TDataSource;
    QAux_Conv: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    QAuxConv_BE: TADOQuery;
    DSAuxConv_BE: TDataSource;
    QCadastroNM_Conv: TStringField;
    QAux_Convtitular: TStringField;
    QAux_Convconv_id: TIntegerField;
    QAuxConv_BEconv_id_be: TIntegerField;
    QAuxConv_BEtitular: TStringField;
    QAuxConv_BEempres_id: TIntegerField;
    QAuxConv_BEfantasia: TStringField;
    QCadastroNM_Conv_Be: TStringField;
    QCadastroNM_Emp_BE: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnBuscaConvBemEstarClick(Sender: TObject);
    procedure ButGravaClick(Sender: TObject);
    procedure btnBuscaConvPadraoClick(Sender: TObject);
    procedure QCadastroAfterOpen(DataSet: TDataSet);
    procedure HabilitarBotoes;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FManutTaxaBenificiarioBemEstar: TFManutTaxaBenificiarioBemEstar;

  implementation

uses UValidacao, DM, UBuscaConvBemEstar, cartao_util,
  UBuscaConvBemEstarPadrao, UMenu;

{$R *.dfm}

procedure TFManutTaxaBenificiarioBemEstar.FormCreate(Sender: TObject);
begin
  chavepri := 'ID';
  detalhe := 'ID: ';
  QCadastro.Open;
//  try
//    QEmpresa.Open;
//  except
//  end;
  FMenu.vFCadBenificiarioBemEstar := True;
  inherited;
end;

procedure TFManutTaxaBenificiarioBemEstar.ButBuscaClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  QCadastro.Close;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('SELECT * FROM CONVENIADOS_BEM_ESTAR_HAS_EMP_TAXAS ');
  QCadastro.Open;
  Screen.Cursor := crDefault;
end;

procedure TFManutTaxaBenificiarioBemEstar.QCadastroAfterInsert(
  DataSet: TDataSet);
begin
  //inherited;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_CONV');
  DMConexao.AdoQry.Open;
  QCadastroID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
end;

procedure TFManutTaxaBenificiarioBemEstar.QCadastroBeforePost(
  DataSet: TDataSet);
begin
//  if QCadastro.State in [dsInsert, dsEdit] then
//    valida;
  if QCadastro.State = dsInsert then begin
    QCadastroDTCADASTRO.Value   := Now;
    QCadastroOPERCADASTRO.Value := Operador.Nome;
  end;
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;

end;

procedure TFManutTaxaBenificiarioBemEstar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFCadBenificiarioBemEstar := False;
end;

procedure TFManutTaxaBenificiarioBemEstar.btnBuscaConvBemEstarClick(Sender: TObject);
var conv_id, empres_id, qtdeLinhas : Integer;
    titular,fantasia : String;
begin
  inherited;
  FBuscaConv:= TFBuscaConv.Create(Self);
  FBuscaConv.ShowModal;
  if FBuscaConv.ModalResult = mrOk then
  begin

    conv_id   := FBuscaConv.grdConveniados.Columns[0].Field.Value;
    titular   := FBuscaConv.grdConveniados.Columns[1].Field.Value;
    empres_id := FBuscaConv.grdConveniados.Columns[2].Field.Value;
    fantasia := FBuscaConv.grdConveniados.Columns[3].Field.Value;

    if QCadastro.IsEmpty then begin
      QCadastro.Insert;
      //QAux.Parameters[0].Value := 45;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_CONV');
      DMConexao.AdoQry.Open;
      QCadastroID.AsInteger := DMConexao.AdoQry.Fields[0].Value;

      QAuxConv_BE.Parameters[0].DataType := ftInteger;
      QAuxConv_BE.Close;
      QAuxConv_BE.Parameters[0].value := conv_id;
      //QAuxConv_BE.Parameters[0].Value := conv_id;
      QAuxConv_BE.Open;

      QCadastroCONV_ID_BE.AsInteger := conv_id;
      QCadastroEMPRES_ID_BE.AsInteger := empres_id;

      //Ap�s carregar o conveniado da tabela bem estar efetua busca na tabela conveniados geral do plant�o.
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT conv.conv_id, conv.empres_id FROM conveniados conv ');
      DMConexao.AdoQry.SQL.Add('WHERE conv.titular = '+QuotedStr(QAuxConv_BEtitular.AsString)+' and conv.empres_id = '+IntToStr(empres_id)+' ');
      //DMConexao.AdoQry.SQL.Text;
      DMConexao.AdoQry.Open;

      QCadastroCONV_ID.AsInteger                := DMConexao.AdoQry.Fields[0].Value;
      QAux_Conv.Parameters[0].DataType := ftInteger;
      QAux_Conv.Close;
      QAux_Conv.Parameters[0].value := DMConexao.AdoQry.Fields[0].Value;
      QAux_Conv.Open;

      QCadastroCONV_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
      QCadastroEMPRES_ID.AsInteger := DMConexao.AdoQry.Fields[1].Value;
      QCadastroLIBERADO.AsString := QuotedStr('S');

     qtdeLinhas := DMConexao.AdoQry.RecordCount;
    end;



  end;

end;

procedure TFManutTaxaBenificiarioBemEstar.ButGravaClick(Sender: TObject);
VAR dtCadastro : TDateTime;
    //operador   : String;
    id,linhasAfetadas,empres_id         : Integer;

begin
     inherited;
//  if QCadastro.IsEmpty then
//  begin
//    MsgInf('Aten��o � obrigat�rio selecionar um conveniado par efetuar a inclus�o');
//  end
//  else
//  begin
//    dtCadastro := Now;
//    id := QCadastroID.AsInteger;
//    linhasAfetadas := DMConexao.ExecuteSql('INSERT INTO CONVENIADOS_BEM_ESTAR_HAS_EMP_TAXAS VALUES (' +
//    ''+QCadastroID.AsString+','+QCadastroCONV_ID.AsString+','+QCadastroCONV_ID_BE.AsString+',' +
//    ''+QCadastroEMPRES_ID.AsString+','+QCadastroEMPRES_ID_BE.AsString+','+FloatToStr(QCadastroTAXA_VALOR.AsCurrency)+',' +
//    ''+QuotedStr(DateToStr(dtCadastro))+','+QuotedStr(Operador.Nome)+','+'''S'''+',0'+',''N'''+
//    ')');
//  end;
//
//  Try
//   flag := False;
//   if QCadastro.State = dsInsert then
//      colocouMensagem := DMConexao.GravaLog(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Inclus�o',Self.Caption,iif(Trim(QCadastro.FieldByName(chavepri).AsString) = '','NULL',QCadastro.FieldByName(chavepri).AsString),StringReplace(detalhe,': ','',[rfReplaceAll]),'', '')
//   else
//   begin
//      colocouMensagem := DMConexao.GravaLogCad(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCadastro.FieldByName(chavepri).AsString,Operador.Nome,'Altera��o',Self.Caption,iif(Trim(QCadastro.FieldByName(chavepri).AsString) = '','NULL',QCadastro.FieldByName(chavepri).AsString),StringReplace(detalhe,': ','',[rfReplaceAll]),'', '');
//      flag := True;
//   end;
//    { QCadastro.FieldByName('DTALTERACAO').AsDateTime := Now;
//    QCadastro.FieldByName('OPERADOR').AsString := Operador.Nome;
//    if QCadastro.FieldByName('APAGADO').AsString = 'S' then
//       QCadastro.FieldByName('DTAPAGADO').AsDateTime := Now;}
//    if QCadastro.State = dsInsert then
//      ShowMessage('Cadastro realizado com sucesso!')
//    else
//      ShowMessage('Altera��o realizada com sucesso!');
//
//  except on E:Exception do
//    if e.Message = INFORMACOES_OCORRENCIA_OBRIGATORIO then
//      msgInf(e.Message);
//  end;
//  QCadastro.Close;


end;

procedure TFManutTaxaBenificiarioBemEstar.btnBuscaConvPadraoClick(
  Sender: TObject);
var conv_id, empres_id, qtdeLinhas : Integer;
  titular,fantasia : String;
begin
  inherited;
  FBuscaConvPadrao:= TFBuscaConvPadrao.Create(Self);
  FBuscaConvPadrao.ShowModal;
  if FBuscaConvPadrao.ModalResult = mrOk then
  begin

    conv_id   := FBuscaConvPadrao.grdConveniados.Columns[0].Field.Value;
    titular   := FBuscaConvPadrao.grdConveniados.Columns[1].Field.Value;
    empres_id := FBuscaConvPadrao.grdConveniados.Columns[2].Field.Value;
    fantasia := FBuscaConvPadrao.grdConveniados.Columns[3].Field.Value;

    if QCadastro.IsEmpty then begin
      QCadastro.Insert;
      //QAux.Parameters[0].Value := 45;
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_CONV');
      DMConexao.AdoQry.Open;
      QCadastroID.AsInteger := DMConexao.AdoQry.Fields[0].Value;

      QAux_Conv.Parameters[0].DataType := ftInteger;
      QAux_Conv.Close;
      QAux_Conv.Parameters[0].value := conv_id;
      QAux_Conv.Open;

      QCadastroCONV_ID.AsInteger := conv_id;
      QCadastroEMPRES_ID.AsInteger := empres_id;

      //Ap�s carregar o conveniado da tabela bem estar efetua busca na tabela conveniados geral do plant�o.
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT conv_be.conv_id_be, conv_be.empres_id FROM conveniados_bem_estar conv_be ');
      DMConexao.AdoQry.SQL.Add('WHERE conv_be.titular = '+QuotedStr(QAux_Convtitular.AsString)+' and conv_be.empres_id = '+IntToStr(empres_id)+' ');
      //DMConexao.AdoQry.SQL.Text;
      DMConexao.AdoQry.Open;

      QCadastroCONV_ID_BE.AsInteger  := DMConexao.AdoQry.Fields[0].Value;
      QAuxConv_BE.Parameters[0].DataType := ftInteger;
      QAuxConv_BE.Close;
      QAuxConv_BE.Parameters[0].value := DMConexao.AdoQry.Fields[0].Value;
      QAuxConv_BE.Open;

      QCadastroCONV_ID_BE.AsInteger := DMConexao.AdoQry.Fields[0].Value;
      QCadastroEMPRES_ID_BE.AsInteger := DMConexao.AdoQry.Fields[1].Value;
      QCadastroLIBERADO.AsString := QuotedStr('S');

     qtdeLinhas := DMConexao.AdoQry.RecordCount;
    end;



  end;
end;

//procedure TFManutTaxaBenificiarioBemEstar.BitBtn1Click(Sender: TObject);
//var conv_id, empres_id, qtdeLinhas : Integer;
//  titular,fantasia : String;
//begin
//  inherited;
//  FBuscaConvPadrao:= TFBuscaConvPadrao.Create(Self);
//  FBuscaConvPadrao.ShowModal;
//  if FBuscaConvPadrao.ModalResult = mrOk then
//  begin
//
//    conv_id   := FBuscaConvPadrao.grdConveniados.Columns[0].Field.Value;
//    titular   := FBuscaConvPadrao.grdConveniados.Columns[1].Field.Value;
//    empres_id := FBuscaConvPadrao.grdConveniados.Columns[2].Field.Value;
//    fantasia := FBuscaConvPadrao.grdConveniados.Columns[3].Field.Value;
//
//    if QCadastro.IsEmpty then begin
//      QCadastro.Insert;
//
//      DMConexao.AdoQry.Close;
//      DMConexao.AdoQry.SQL.Clear;
//      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_CONV');
//      DMConexao.AdoQry.Open;
//      QCadastroID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
//
//      QCadastroCONV_ID.AsInteger := conv_id;
//      QCadastroEMPRES_ID.AsInteger  := empres_id;
//      QCadastroTITULAR.AsString     := titular;
//      QCadastroFANTASIA.AsString    := fantasia;
//
//      //Ap�s carregar o conveniado da tabela bem estar efetua busca na tabela conveniados geral do plant�o.
//      DMConexao.AdoQry.Close;
//      DMConexao.AdoQry.SQL.Clear;
//      DMConexao.AdoQry.SQL.Add('SELECT conv_be.conv_id_be, conv_be.empres_id, conv_be.titular, emp.fantasia FROM conveniados_bem_estar conv_be ');
//      DMConexao.AdoQry.SQL.Add('INNER JOIN empresas emp ON emp.empres_id = conv_be.empres_id ');
//      DMConexao.AdoQry.SQL.Add('WHERE conv_be.titular = '+QuotedStr(QCadastroTITULAR.AsString)+' and conv_be.empres_id = '+QCadastroEMPRES_ID.AsString+' ');
//      //DMConexao.AdoQry.SQL.Text;
//      DMConexao.AdoQry.Open;
//
//      QCadastroCONV_ID_BE.AsInteger             := DMConexao.AdoQry.Fields[0].Value;
//      QCadastroEMPRES_ID_BE.AsInteger           := DMConexao.AdoQry.Fields[1].Value;
//      QCadastroTITULAR_BEM_ESTAR.AsString       := DMConexao.AdoQry.Fields[2].Value;
//
//      qtdeLinhas := DMConexao.AdoQry.RecordCount;
//    end;
//    edtValor.SetFocus;
//  end;
//end;

procedure TFManutTaxaBenificiarioBemEstar.QCadastroAfterOpen(
  DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.IsEmpty then
    edtValor.SetFocus;
end;

procedure TFManutTaxaBenificiarioBemEstar.HabilitarBotoes;
begin
  ButCancela.Enabled  := QCadastro.State in [dsEdit,dsInsert];
  ButGrava.Enabled    := QCadastro.State in [dsEdit,dsInsert];
  ButInclui.Enabled   := (QCadastro.State = dsBrowse) and Incluir;
  ButEdit.Enabled     := (QCadastro.State = dsBrowse) and Alterar and (not QCadastro.IsEmpty);
  ButApaga.Enabled    := (QCadastro.State = dsBrowse) and Excluir and (not QCadastro.IsEmpty);
  if FindComponent('ButNovoCod') <> nil then
    TWinControl(FindComponent('ButNovoCod')).Enabled := Alterar and (not QCadastro.IsEmpty);
  if FindComponent('ButLimpaSenha') <> nil then
    TWinControl(FindComponent('ButLimpaSenha')).Enabled := Alterar and (not QCadastro.IsEmpty);
end;

end.
