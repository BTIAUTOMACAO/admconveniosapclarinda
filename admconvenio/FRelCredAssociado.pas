unit FRelCredAssociado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, frxClass, frxExportPDF, frxGradient, frxDBSet, DBCtrls,
  StdCtrls, Mask, JvExMask, JvToolEdit, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons, ComCtrls, Menus, ZDataset, DB,
  ZAbstractRODataset, ZAbstractDataset, ShellApi, ADODB, JvMemoryDataset;

type
  TFrmRelCredAssociado = class(TF1)
    frxReport1: TfrxReport;
    dbConveniados: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    dsConv: TDataSource;
    dsEmpresas: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dbLkpEmpresas: TDBLookupComboBox;
    qConv: TADOQuery;
    qConvCONV_ID: TIntegerField;
    qConvCODCARTIMP: TStringField;
    qConvTITULAR: TStringField;
    qConvRENOVACAO_VALOR: TBCDField;
    qConvABONO_VALOR: TBCDField;
    qConvDATA_RENOVACAO: TWideStringField;
    qConvNOME: TStringField;
    qEmpresas: TADOQuery;
    qEmpresasempres_id: TIntegerField;
    qEmpresasnome: TStringField;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    qConvempres_id: TIntegerField;
    dbLbpFechamentos: TDBLookupComboBox;
    lblFechamento: TLabel;
    dsFechamentos: TDataSource;
    qFechamentos: TADOQuery;
    qFechamentosdata_fechamento: TStringField;
    qFechamentosempres_id: TIntegerField;
    qConvdata_fechamento: TDateTimeField;
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dsConvDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbLkpEmpresasExit(Sender: TObject);
  private
    conv_sel : String;
  public
    { Public declarations }
  end;

var
  FrmRelCredAssociado: TFrmRelCredAssociado;

implementation

uses DM, cartao_util;

{$R *.dfm}



procedure TFrmRelCredAssociado.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm_EmpClick(nil);
end;

procedure TFrmRelCredAssociado.ButMarcDesm_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesm(QConv);
end;

procedure TFrmRelCredAssociado.ButMarcaTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv);
end;

procedure TFrmRelCredAssociado.ButDesmTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv,False);
end;

procedure TFrmRelCredAssociado.btnGerarPDFClick(Sender: TObject);
begin
  inherited;
  if(dbLkpEmpresas.KeyValue = Null) then begin
    MsgInf('Selecione uma empresa.');
    dbLkpEmpresas.SetFocus;
  end
  else
  begin
    sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      btnVisualizar.Click;
      frxReport1.Export(frxPDFExport1);
    end;
  end;
end;

procedure TFrmRelCredAssociado.btnVisualizarClick(Sender: TObject);
begin
  inherited;
  if(dbLkpEmpresas.KeyValue = Null) then begin
    MsgInf('Selecione uma empresa.');
    dbLkpEmpresas.SetFocus;
  end else begin
    qConv.Close;
    qConv.Parameters.ParamByName('empres_id').Value := qEmpresasEMPRES_ID.AsInteger;
    qConv.Parameters.ParamByName('data').Value := dbLbpFechamentos.KeyValue;
    qConv.Open;

    if qConv.IsEmpty then
    begin
      MsgInf('N�o foi encontrado nenhum lan�amento para essa empresa.');
      dbLkpEmpresas.SetFocus;
      abort;
    end;
    frxReport1.ShowReport;
  end;

end;

procedure TFrmRelCredAssociado.dsConvDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  //btnVisualizar.Enabled := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
  btnGerarPDF.Enabled   := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
end;

procedure TFrmRelCredAssociado.FormCreate(Sender: TObject);
begin
  inherited;
  qEmpresas.Open;
  dbLkpEmpresas.SetFocus;
end;

procedure TFrmRelCredAssociado.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {if (key = VK_F8) then
    Dm1.MarcaDesmTodos(qConv)
  else if (key = VK_F9) then
    Dm1.MarcaDesmTodos(qConv,False)
  else if (key = VK_F11) then
    Dm1.MarcaDesm(qConv);   }
end;

procedure TFrmRelCredAssociado.dbLkpEmpresasExit(Sender: TObject);
begin
  inherited;
   qFechamentos.Close;
   qFechamentos.Parameters.ParamByName('empres_id').Value := dbLkpEmpresas.KeyValue;
   qFechamentos.Open;
   if(qFechamentos.RecordCount = 0) then
   begin
      dbLbpFechamentos.Visible := false;
      lblFechamento.Visible := false;
      MsgInf('N�o � poss�vel exibir o relat�rio. A empresa n�o possui hist�rico de alimenta��o!');
      dbLkpEmpresas.SetFocus;
   end
   else
      begin
        dbLbpFechamentos.Visible := true;
        lblFechamento.Visible := true;
        dbLbpFechamentos.SetFocus;
      end;
end;

end.
