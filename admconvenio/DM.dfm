object DMConexao: TDMConexao
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 440
  Top = 325
  Height = 313
  Width = 409
  object AdoCon: TADOConnection
    CommandTimeout = 1000
    ConnectionString = 
      'Provider=SQLNCLI.1;Password=1@allebagord;Persist Security Info=T' +
      'rue;User ID=sa;Initial Catalog=BELLA_08_08_21;Data Source=192.16' +
      '8.0.184\MSSQLSERVER,1433;Use Procedure for Prepare=1;Auto Transl' +
      'ate=True;Packet Size=4096;Workstation ID=BTI05-PC;Use Encryption' +
      ' for Data=False;Tag with column collation when possible=False;MA' +
      'RS Connection=False;DataTypeCompatibility=0;Trust Server Certifi' +
      'cate=False'
    ConnectionTimeout = 1000
    IsolationLevel = ilReadCommitted
    LoginPrompt = False
    Provider = 'SQLNCLI.1'
    Left = 24
    Top = 32
  end
  object AdoQry: TADOQuery
    Connection = AdoCon
    Parameters = <>
    Left = 80
    Top = 24
  end
  object Config: TADOQuery
    Connection = AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CONFIG')
    Left = 128
    Top = 24
    object ConfigCOD_ADM_BIG: TWordField
      FieldName = 'COD_ADM_BIG'
    end
    object ConfigULTIMA_EXE: TStringField
      FieldName = 'ULTIMA_EXE'
      Size = 100
    end
    object ConfigRODAR_ATE: TStringField
      FieldName = 'RODAR_ATE'
      Size = 100
    end
    object ConfigPATH_WEBSERVICE: TStringField
      FieldName = 'PATH_WEBSERVICE'
      Size = 50
    end
    object ConfigCODIGOCARTAO: TIntegerField
      FieldName = 'CODIGOCARTAO'
    end
    object ConfigUSAINICIALCODCARTIMP: TStringField
      FieldName = 'USAINICIALCODCARTIMP'
      FixedChar = True
      Size = 1
    end
    object ConfigMOVER_CODCART_TO_CODIMP: TStringField
      FieldName = 'MOVER_CODCART_TO_CODIMP'
      FixedChar = True
      Size = 1
    end
    object ConfigINCREMENTCODCARTIMP: TStringField
      FieldName = 'INCREMENTCODCARTIMP'
      FixedChar = True
      Size = 1
    end
    object ConfigINCREMENTCODCARTIMPMOD1: TStringField
      FieldName = 'INCREMENTCODCARTIMPMOD1'
      FixedChar = True
      Size = 1
    end
    object ConfigEDIT_GRID: TStringField
      FieldName = 'EDIT_GRID'
      FixedChar = True
      Size = 1
    end
    object ConfigLANC_DIFE_PROX_PERIODO: TStringField
      FieldName = 'LANC_DIFE_PROX_PERIODO'
      FixedChar = True
      Size = 1
    end
    object ConfigFLANCAMENTOS_FOCO_FORNEC: TStringField
      FieldName = 'FLANCAMENTOS_FOCO_FORNEC'
      FixedChar = True
      Size = 1
    end
    object ConfigCODIGOCARTAOIMP: TStringField
      FieldName = 'CODIGOCARTAOIMP'
    end
    object ConfigCODIGOCARTAOIMPDEP: TStringField
      FieldName = 'CODIGOCARTAOIMPDEP'
    end
    object ConfigINICIALCODCARTIMP: TStringField
      FieldName = 'INICIALCODCARTIMP'
    end
    object ConfigCOD_CRED_BAIXA: TIntegerField
      FieldName = 'COD_CRED_BAIXA'
    end
    object ConfigCOD_CRED_ADM: TIntegerField
      FieldName = 'COD_CRED_ADM'
    end
    object ConfigCOD_CRED_CPMF: TIntegerField
      FieldName = 'COD_CRED_CPMF'
    end
    object ConfigPERCENT_CPMF: TFloatField
      FieldName = 'PERCENT_CPMF'
    end
    object ConfigVERSAO: TIntegerField
      FieldName = 'VERSAO'
    end
    object ConfigFLANCAMENTOS_AUTOR_WEBSERVICE: TStringField
      FieldName = 'FLANCAMENTOS_AUTOR_WEBSERVICE'
      FixedChar = True
      Size = 1
    end
    object ConfigPERMITE_AUTOR_FORCADA: TStringField
      FieldName = 'PERMITE_AUTOR_FORCADA'
      FixedChar = True
      Size = 1
    end
    object ConfigINFORMA_EMP_LANC_IND: TStringField
      FieldName = 'INFORMA_EMP_LANC_IND'
      FixedChar = True
      Size = 1
    end
    object ConfigVERIFICA_CPF_CONV: TStringField
      FieldName = 'VERIFICA_CPF_CONV'
      FixedChar = True
      Size = 1
    end
    object ConfigCOD_EMP_LANC: TIntegerField
      FieldName = 'COD_EMP_LANC'
    end
    object ConfigCODINBSGENERICO: TStringField
      FieldName = 'CODINBSGENERICO'
      Size = 13
    end
    object ConfigACUMULA_AUTOR_PROX_FECHA: TStringField
      FieldName = 'ACUMULA_AUTOR_PROX_FECHA'
      FixedChar = True
      Size = 1
    end
    object ConfigPERM_CANC_PARC: TStringField
      FieldName = 'PERM_CANC_PARC'
      FixedChar = True
      Size = 1
    end
    object ConfigFILTRO_ENTREG_NF_EXT: TStringField
      FieldName = 'FILTRO_ENTREG_NF_EXT'
      FixedChar = True
      Size = 1
    end
    object ConfigEMP_FANTASIA: TStringField
      FieldName = 'EMP_FANTASIA'
      FixedChar = True
      Size = 1
    end
    object ConfigCRE_FANTASIA: TStringField
      FieldName = 'CRE_FANTASIA'
      FixedChar = True
      Size = 1
    end
    object ConfigEXIBIR_DT_VENC_REL_PAG_FOR: TStringField
      FieldName = 'EXIBIR_DT_VENC_REL_PAG_FOR'
      FixedChar = True
      Size = 1
    end
    object ConfigFILTRO_ENTREG_NF_CONFERE: TStringField
      FieldName = 'FILTRO_ENTREG_NF_CONFERE'
      FixedChar = True
      Size = 1
    end
    object ConfigEMP_FANTASIA_CADCONV: TStringField
      FieldName = 'EMP_FANTASIA_CADCONV'
      FixedChar = True
      Size = 1
    end
    object ConfigEXIBIR_PAGTO_SITE: TStringField
      FieldName = 'EXIBIR_PAGTO_SITE'
      FixedChar = True
      Size = 1
    end
    object ConfigALTERAR_TITULAR_NOME_CARTAO: TStringField
      FieldName = 'ALTERAR_TITULAR_NOME_CARTAO'
      FixedChar = True
      Size = 1
    end
    object ConfigALTERAR_TITULAR_LIBERADO_CARTAO: TStringField
      FieldName = 'ALTERAR_TITULAR_LIBERADO_CARTAO'
      FixedChar = True
      Size = 1
    end
    object ConfigESTILO_ENTREGUE: TStringField
      FieldName = 'ESTILO_ENTREGUE'
      FixedChar = True
      Size = 1
    end
    object ConfigSENHA_CONV_ID: TStringField
      FieldName = 'SENHA_CONV_ID'
      FixedChar = True
      Size = 1
    end
    object ConfigEXIBE_ALERTAS: TStringField
      FieldName = 'EXIBE_ALERTAS'
      FixedChar = True
      Size = 1
    end
    object ConfigUSA_NOVO_FECHAMENTO: TStringField
      FieldName = 'USA_NOVO_FECHAMENTO'
      FixedChar = True
      Size = 1
    end
    object ConfigEXPIRA_PBM: TIntegerField
      FieldName = 'EXPIRA_PBM'
    end
    object ConfigINTEGRA_SISTEMABIG: TStringField
      FieldName = 'INTEGRA_SISTEMABIG'
      FixedChar = True
      Size = 1
    end
    object ConfigCUPOM_CONV: TStringField
      FieldName = 'CUPOM_CONV'
      Size = 10
    end
    object ConfigCUPOM_ESTAB: TStringField
      FieldName = 'CUPOM_ESTAB'
      Size = 10
    end
    object ConfigUSA_PROG_DESC: TStringField
      FieldName = 'USA_PROG_DESC'
      FixedChar = True
      Size = 1
    end
    object ConfigUSA_FIDELIDADE: TStringField
      FieldName = 'USA_FIDELIDADE'
      FixedChar = True
      Size = 1
    end
    object ConfigDIAS_BLOQ_EMPR_FAT: TIntegerField
      FieldName = 'DIAS_BLOQ_EMPR_FAT'
    end
    object ConfigEMITE_NOVO_CART_2VIA: TStringField
      FieldName = 'EMITE_NOVO_CART_2VIA'
      FixedChar = True
      Size = 1
    end
    object ConfigDEMISSAO_MOVE_AUTS: TStringField
      FieldName = 'DEMISSAO_MOVE_AUTS'
      FixedChar = True
      Size = 1
    end
    object ConfigFIDELIDADE_VALOR: TStringField
      FieldName = 'FIDELIDADE_VALOR'
      FixedChar = True
      Size = 1
    end
    object ConfigLINK_EMP: TStringField
      FieldName = 'LINK_EMP'
      Size = 100
    end
    object ConfigLINK_EST: TStringField
      FieldName = 'LINK_EST'
      Size = 100
    end
    object ConfigLINK_USU: TStringField
      FieldName = 'LINK_USU'
      Size = 100
    end
    object ConfigLINK_OND: TStringField
      FieldName = 'LINK_OND'
      Size = 100
    end
    object ConfigTIPO_LIMITE: TStringField
      FieldName = 'TIPO_LIMITE'
      FixedChar = True
      Size = 1
    end
    object ConfigUSA_GRUPO_PROD: TStringField
      FieldName = 'USA_GRUPO_PROD'
      FixedChar = True
      Size = 1
    end
    object ConfigUSA_VALE_DESCONTO: TStringField
      FieldName = 'USA_VALE_DESCONTO'
      FixedChar = True
      Size = 1
    end
    object ConfigCUPOM: TStringField
      FieldName = 'CUPOM'
      Size = 10
    end
    object ConfigIMPRIME_CUPOM_FIDELIZE: TStringField
      FieldName = 'IMPRIME_CUPOM_FIDELIZE'
      FixedChar = True
      Size = 1
    end
    object ConfigBLOQUEIA_VENDA_VALOR: TStringField
      FieldName = 'BLOQUEIA_VENDA_VALOR'
      FixedChar = True
      Size = 1
    end
    object ConfigUSA_VALE_DINHEIRO: TStringField
      FieldName = 'USA_VALE_DINHEIRO'
      FixedChar = True
      Size = 1
    end
    object ConfigENVIA_DADOS: TStringField
      FieldName = 'ENVIA_DADOS'
      FixedChar = True
      Size = 1
    end
    object ConfigCOD_CARD_BIN: TStringField
      FieldName = 'COD_CARD_BIN'
      Size = 16
    end
    object ConfigMOSTRAR_TELA_OCORRENCIA: TStringField
      FieldName = 'MOSTRAR_TELA_OCORRENCIA'
      FixedChar = True
      Size = 1
    end
    object ConfigPATH_SALE_SERVICE: TStringField
      FieldName = 'PATH_SALE_SERVICE'
      Size = 50
    end
    object ConfigSEG_ID_CANTINA: TIntegerField
      FieldName = 'SEG_ID_CANTINA'
    end
    object ConfigBIN_CANTINA: TStringField
      DisplayWidth = 12
      FieldName = 'BIN_CANTINA'
      FixedChar = True
      Size = 12
    end
    object ConfigBIN_BEM_ESTAR: TStringField
      FieldName = 'BIN_BEM_ESTAR'
      FixedChar = True
      Size = 8
    end
    object ConfigCRED_ID_BEM_ESTAR: TIntegerField
      FieldName = 'CRED_ID_BEM_ESTAR'
    end
    object ConfigCOD_CARD_BIN_NOVO: TStringField
      FieldName = 'COD_CARD_BIN_NOVO'
      Size = 16
    end
  end
  object AdoConfGrade: TADOQuery
    Connection = AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CONF_GRADES')
    Left = 184
    Top = 24
  end
  object StoredProc1: TADOStoredProc
    Connection = AdoCon
    Parameters = <>
    Left = 248
    Top = 24
  end
  object Q: TADOQuery
    Connection = AdoCon
    Parameters = <>
    Left = 120
    Top = 96
  end
  object UpdateSQL1: TUpdateSQL
    Left = 168
    Top = 96
  end
  object Query2: TADOQuery
    Connection = AdoCon
    Parameters = <>
    Left = 72
    Top = 96
  end
  object Adm: TADOTable
    Connection = AdoCon
    TableName = 'ADMINISTRADORA'
    Left = 72
    Top = 224
    object AdmADM_ID: TIntegerField
      FieldName = 'ADM_ID'
    end
    object AdmRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 45
    end
    object AdmAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object AdmCONTA_ID: TIntegerField
      FieldName = 'CONTA_ID'
    end
    object AdmFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 45
    end
    object AdmCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object AdmINSC_EST: TStringField
      FieldName = 'INSC_EST'
      Size = 15
    end
    object AdmENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 60
    end
    object AdmNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object AdmBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object AdmCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 35
    end
    object AdmCEP: TStringField
      FieldName = 'CEP'
      Size = 10
    end
    object AdmUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object AdmRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Size = 50
    end
    object AdmFONE: TStringField
      FieldName = 'FONE'
    end
    object AdmDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object AdmOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object AdmDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object AdmDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object AdmOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object AdmVENCEDIA: TIntegerField
      FieldName = 'VENCEDIA'
    end
    object AdmCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 30
    end
  end
  object Query1: TADOQuery
    Connection = AdoCon
    Parameters = <>
    Left = 232
    Top = 136
  end
end
