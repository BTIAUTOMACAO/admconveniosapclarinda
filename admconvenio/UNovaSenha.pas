unit UNovaSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, ExtCtrls,
  JvComponent, {JvTransBtn,} JvGIF, RxGIF, JvExControls, JvButton,
  {JvTransparentButton,} jpeg, cartao_util, JvTransparentButton,
  frxpngimage, pngimage;

type
  TFNovaSenha = class(TForm)
    ED1: TEdit;
    ED2: TEdit;
    Image1: TImage;
    ButOk: TJvTransparentButton;
    ButCancel: TJvTransparentButton;
    procedure ButCancelClick(Sender: TObject);
    procedure ButOkClick(Sender: TObject);
    procedure ED1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ED2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FNovaSenha: TFNovaSenha;

implementation

uses DM, USenha;

{$R *.dfm}

procedure TFNovaSenha.ButCancelClick(Sender: TObject);
begin
//Exit;
Application.Terminate;
//Close;
end;

procedure TFNovaSenha.ButOkClick(Sender: TObject);
begin
if ed1.Text = '1111' then begin
   ShowMessage('Senha inv�lida.');
   ed1.SetFocus;
   Exit;
end;
if ed1.Text = ed2.Text then begin
   DMConexao.AdoQry.Close;
   DMConexao.AdoQry.SQL.Text := 'Update usuarios set senha = '+QuotedStr(Crypt('E',ed1.Text,'BIGCOMPRAS'))+' where usuario_id = '+FSenha.QLoginusuario_id.AsString;
   DMConexao.AdoQry.execSQL;
   Close;
end
else begin
   ShowMessage('Confirma��o de senha difere da senha informada.');
   ed1.SetFocus;
end;
end;

procedure TFNovaSenha.ED1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
  begin
    ED2.SetFocus;
  end;
end;

procedure TFNovaSenha.ED2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_return then
  begin
    ButOk.Click;
  end;
end;

procedure TFNovaSenha.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_escape then
  begin
    ButCancel.Click;
  end;
end;

end.
