object FConfig: TFConfig
  Left = 717
  Top = 307
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Configura'#231#245'es do Sistema'
  ClientHeight = 626
  ClientWidth = 444
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 444
    Height = 626
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 444
      Height = 580
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = '&Gerais'
        object Label1: TLabel
          Left = 16
          Top = 82
          Width = 101
          Height = 13
          Caption = 'N'#186' Inicial dos Cart'#245'es'
        end
        object Label3: TLabel
          Left = 15
          Top = 10
          Width = 118
          Height = 13
          Caption = 'Caminho do WebService'
        end
        object Label6: TLabel
          Left = 16
          Top = 122
          Width = 102
          Height = 13
          Caption = 'Prog. expira em: (Min)'
        end
        object Label9: TLabel
          Left = 133
          Top = 82
          Width = 127
          Height = 13
          Caption = 'N'#186' Cod. Cart'#227'o Importa'#231#227'o'
        end
        object Label10: TLabel
          Left = 269
          Top = 82
          Width = 109
          Height = 13
          Caption = 'N'#186' Import. Dependente'
        end
        object SpeedButton1: TSpeedButton
          Left = 241
          Top = 97
          Width = 23
          Height = 22
          Hint = 'Exibir '#250'ltima numera'#231#227'o com 14 caracteres.'
          Caption = '?'
          Flat = True
          OnClick = SpeedButton1Click
        end
        object Label11: TLabel
          Left = 288
          Top = 122
          Width = 123
          Height = 13
          Caption = 'N'#186' Inicial Cod. Importa'#231#227'o'
        end
        object Label17: TLabel
          Left = 140
          Top = 122
          Width = 69
          Height = 13
          Caption = 'Tipo de Limite:'
        end
        object Label8: TLabel
          Left = 16
          Top = 162
          Width = 110
          Height = 13
          Caption = 'N'#186' de Calculo para BIN'
        end
        object Label18: TLabel
          Left = 140
          Top = 162
          Width = 98
          Height = 13
          Caption = 'Percentual de CPMF'
        end
        object Label19: TLabel
          Left = 15
          Top = 45
          Width = 119
          Height = 13
          Caption = 'Caminho do Sale Service'
        end
        object EditCartoes: TMaskEdit
          Left = 16
          Top = 97
          Width = 109
          Height = 21
          EditMask = '000\.000\.000;0; '
          MaxLength = 11
          TabOrder = 2
          OnExit = EditCartoesExit
        end
        object DBEdit1: TDBEdit
          Left = 15
          Top = 25
          Width = 398
          Height = 21
          DataField = 'PATH_WEBSERVICE'
          DataSource = DSConfig
          TabOrder = 0
        end
        object DBEditGrade: TDBCheckBox
          Left = 14
          Top = 283
          Width = 167
          Height = 17
          Caption = 'Editar Grades dos Cadastros .'
          DataField = 'EDIT_GRID'
          DataSource = DSConfig
          TabOrder = 11
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBEdit2: TDBEdit
          Left = 16
          Top = 137
          Width = 116
          Height = 21
          DataField = 'EXPIRA_PBM'
          DataSource = DSConfig
          TabOrder = 5
        end
        object DBCheckBox5: TDBCheckBox
          Left = 14
          Top = 300
          Width = 245
          Height = 17
          Caption = 'Informar empresa nos lan'#231'amentos individuais.'
          DataField = 'INFORMA_EMP_LANC_IND'
          DataSource = DSConfig
          TabOrder = 12
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox6: TDBCheckBox
          Left = 14
          Top = 317
          Width = 285
          Height = 17
          Caption = 'Habilitar op'#231#245'es de filtro de Entrega de NF nos Extratos.'
          DataField = 'FILTRO_ENTREG_NF_EXT'
          DataSource = DSConfig
          TabOrder = 13
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox7: TDBCheckBox
          Left = 14
          Top = 334
          Width = 365
          Height = 16
          Caption = 
            'Acumular as autoriza'#231#245'es n'#227'o confirmadas para o pr'#243'ximo fechamen' +
            'to.'
          DataField = 'ACUMULA_AUTOR_PROX_FECHA'
          DataSource = DSConfig
          TabOrder = 14
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox11: TDBCheckBox
          Left = 14
          Top = 352
          Width = 357
          Height = 29
          Hint = 
            'No relat'#243'rio de pagamento a fornecedores em aberto, o cabe'#231'alho ' +
            'sai com a Data de Vencimento do Fornecedor se essa op'#231#227'o estiver' +
            ' marcada.'
          Caption = 
            'Exibir Data de Vencimento do Estabelecimento no cabe'#231'alho do rel' +
            'at'#243'rio de Pagamento a Estabelecimentos em Aberto.'
          DataField = 'EXIBIR_DT_VENC_REL_PAG_FOR'
          DataSource = DSConfig
          TabOrder = 15
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox12: TDBCheckBox
          Left = 14
          Top = 384
          Width = 365
          Height = 15
          Hint = 
            'Filtro no menu de Lan'#231'amentos Entrega de NF onde '#233' dada a op'#231#227'o ' +
            'para exibi'#231#227'o ou n'#227'o de autoriza'#231#245'es '#13#10'marcadas como Conferida (' +
            'esse campo s'#243' '#233' acess'#237'vel na tela de Lan'#231'amentos Entrega de NF)'
          Caption = 'Habilitar filtro de Conferido no menu Lan'#231'. Entrega de NF'
          DataField = 'FILTRO_ENTREG_NF_CONFERE'
          DataSource = DSConfig
          TabOrder = 16
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBEdit3: TDBEdit
          Left = 132
          Top = 97
          Width = 105
          Height = 21
          DataField = 'CODIGOCARTAOIMP'
          DataSource = DSConfig
          TabOrder = 3
        end
        object DBCheckBox15: TDBCheckBox
          Left = 14
          Top = 400
          Width = 365
          Height = 16
          Hint = 
            'Habilitar op'#231#227'o para marcar os pagamentos em aberto quer ser'#227'o e' +
            'xibidos '#13#10'no site webfornecedores'
          Caption = 'Habilitar op'#231#227'o para marcar exibi'#231#227'o de pagtos em aberto no site'
          DataField = 'EXIBIR_PAGTO_SITE'
          DataSource = DSConfig
          TabOrder = 17
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBEdit4: TDBEdit
          Left = 268
          Top = 97
          Width = 145
          Height = 21
          DataField = 'CODIGOCARTAOIMPDEP'
          DataSource = DSConfig
          TabOrder = 4
        end
        object DBCheckBox18: TDBCheckBox
          Left = 14
          Top = 417
          Width = 365
          Height = 15
          Hint = 
            'Com essa op'#231#227'o desmarcada, a tela de fechamento ficar'#225' com'#13#10'o ma' +
            'rcador para todas as autoriza'#231#245'es, e como ela marcada'#13#10'o cursor ' +
            'manter'#225' em Somente Entregue.'
          Caption = 'Estilo do sistema para utilizar a Entrega de Notas Fiscais'
          DataField = 'ESTILO_ENTREGUE'
          DataSource = DSConfig
          TabOrder = 18
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object RGCodCartImp: TRadioGroup
          Left = 12
          Top = 205
          Width = 400
          Height = 73
          Caption = 'Utiliza C'#243'digo de Importa'#231#227'o ?'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o'
            'Mover c'#243'digo do cart'#227'o (sem d'#237'gito)'
            'Sequ'#234'ncial'
            'Modelo (empresa/chapa/sequ'#234'ncia)'
            'Modelo (14 Dig com numero inicial)')
          TabOrder = 10
          OnClick = RGCodCartImpClick
        end
        object DBEdit5: TDBEdit
          Left = 288
          Top = 137
          Width = 125
          Height = 21
          DataField = 'INICIALCODCARTIMP'
          DataSource = DSConfig
          TabOrder = 7
        end
        object DBCheckBox14: TDBCheckBox
          Left = 14
          Top = 434
          Width = 395
          Height = 15
          Hint = 
            'Com essa op'#231#227'o marcada, ao iniciar o sistema, ser'#225' '#13#10'verificado ' +
            'se existem alertas no sistema.'
          Caption = 'Verificar se existem alertas ao iniciar o sistema.'
          DataField = 'EXIBE_ALERTAS'
          DataSource = DSConfig
          TabOrder = 19
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox19: TDBCheckBox
          Left = 14
          Top = 451
          Width = 395
          Height = 15
          Hint = 
            'Com essa op'#231#227'o desmarcada, a tela de fechamento ficar'#225' com'#13#10'o ma' +
            'rcador para todas as autoriza'#231#245'es, e como ela marcada'#13#10'o cursor ' +
            'manter'#225' em Somente Entregue.'
          Caption = 'Utiliza Novo Modo de Fechamento (Cria'#231#227'o de Faturas)'
          DataField = 'USA_NOVO_FECHAMENTO'
          DataSource = DSConfig
          TabOrder = 20
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox20: TDBCheckBox
          Left = 14
          Top = 467
          Width = 219
          Height = 17
          Hint = 
            'Com essa op'#231#227'o habilitada, s'#227'o exibidas as tela de Configura'#231#227'o ' +
            'de Grupo de Produto e Programas de Desconto'
          Caption = 'Usa Integra'#231#227'o com SistemaBig'
          DataField = 'INTEGRA_SISTEMABIG'
          DataSource = DSConfig
          TabOrder = 21
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox21: TDBCheckBox
          Left = 242
          Top = 483
          Width = 175
          Height = 17
          Hint = 
            'Com essa op'#231#227'o habilitada, s'#227'o exibidas as tela de Configura'#231#227'o ' +
            'de Grupo de Produto e Programas de Desconto'
          Caption = 'Utiliza Programas de Desconto'
          DataField = 'USA_PROG_DESC'
          DataSource = DSConfig
          TabOrder = 24
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox24: TDBCheckBox
          Left = 14
          Top = 515
          Width = 227
          Height = 17
          Caption = 'Gera Pontos em Transa'#231#245'es Sem Produtos.'
          DataField = 'FIDELIDADE_VALOR'
          DataSource = DSConfig
          TabOrder = 27
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox25: TDBCheckBox
          Left = 242
          Top = 499
          Width = 175
          Height = 17
          Caption = 'Acumula Pontos Fidelidade'
          DataField = 'USA_FIDELIDADE'
          DataSource = DSConfig
          TabOrder = 26
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object JvDBComboBox1: TJvDBComboBox
          Left = 140
          Top = 137
          Width = 141
          Height = 21
          DataField = 'TIPO_LIMITE'
          DataSource = DSConfig
          Items.Strings = (
            'Limite M'#234's'
            'Limite Total')
          TabOrder = 6
          Values.Strings = (
            'M'
            'T')
          ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
          ListSettings.OutfilteredValueFont.Color = clRed
          ListSettings.OutfilteredValueFont.Height = -11
          ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
          ListSettings.OutfilteredValueFont.Style = []
        end
        object DBCheckBox26: TDBCheckBox
          Left = 242
          Top = 515
          Width = 175
          Height = 17
          Caption = 'Imprime Cupom Fidelidade'
          DataField = 'IMPRIME_CUPOM_FIDELIZE'
          DataSource = DSConfig
          TabOrder = 28
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox27: TDBCheckBox
          Left = 14
          Top = 483
          Width = 217
          Height = 17
          Caption = 'Utiliza Descontos em Grupo de Produtos'
          DataField = 'USA_GRUPO_PROD'
          DataSource = DSConfig
          TabOrder = 23
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox28: TDBCheckBox
          Left = 14
          Top = 499
          Width = 219
          Height = 17
          Hint = 
            'Com essa op'#231#227'o habilitada, s'#227'o exibidas as tela de Configura'#231#227'o ' +
            'de Grupo de Produto e Programas de Desconto'
          Caption = 'Utiliza Vale Desconto'
          DataField = 'USA_VALE_DESCONTO'
          DataSource = DSConfig
          TabOrder = 25
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox29: TDBCheckBox
          Left = 242
          Top = 467
          Width = 199
          Height = 17
          Caption = 'Bloqueia autoriza'#231#245'es sem produtos'
          DataField = 'BLOQUEIA_VENDA_VALOR'
          DataSource = DSConfig
          TabOrder = 22
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBEdit11: TDBEdit
          Left = 16
          Top = 177
          Width = 116
          Height = 21
          DataField = 'COD_CARD_BIN'
          DataSource = DSConfig
          TabOrder = 8
          OnKeyPress = DBEdit11KeyPress
        end
        object DBCheckBox30: TDBCheckBox
          Left = 14
          Top = 531
          Width = 227
          Height = 17
          Caption = 'Mostrar Tela Ocorr'#234'ncia'
          DataField = 'MOSTRAR_TELA_OCORRENCIA'
          DataSource = DSConfig
          TabOrder = 29
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBEdit12: TDBEdit
          Left = 140
          Top = 177
          Width = 141
          Height = 21
          DataField = 'PERCENT_CPMF'
          DataSource = DSConfig
          TabOrder = 9
          OnKeyPress = DBEdit12KeyPress
        end
        object DBEdit13: TDBEdit
          Left = 15
          Top = 60
          Width = 398
          Height = 21
          DataField = 'PATH_SALE_SERVICE'
          DataSource = DSConfig
          TabOrder = 1
        end
      end
      object TabSheet2: TTabSheet
        Caption = '&Lan'#231'amentos/Baixas'
        ImageIndex = 1
        object Label2: TLabel
          Left = 20
          Top = 9
          Width = 122
          Height = 13
          Caption = 'Estabelecimento de Baixa'
        end
        object Label4: TLabel
          Left = 20
          Top = 49
          Width = 232
          Height = 13
          Caption = 'Estabelecimento de Lan'#231'amentos Administradora'
        end
        object Label5: TLabel
          Left = 20
          Top = 91
          Width = 207
          Height = 13
          Caption = 'Estabelecimento de Lan'#231'amentos de CPMF'
        end
        object Label7: TLabel
          Left = 20
          Top = 137
          Width = 234
          Height = 13
          Caption = 'Empresa de Lan'#231'amentos de descontos de notas'
        end
        object Label12: TLabel
          Left = 22
          Top = 362
          Width = 357
          Height = 26
          Caption = 
            'Ao abrir o sistema, perguntar se sobre o bloqueio da empresa    ' +
            '            dias '#13#10'depois do vencimento da fatura. (zero n'#227'o ser' +
            #225' verificado)'
        end
        object DBCred: TJvDBLookupCombo
          Left = 20
          Top = 25
          Width = 345
          Height = 21
          DataField = 'COD_CRED_BAIXA'
          DataSource = DSConfig
          LookupField = 'cred_id'
          LookupDisplay = 'cred_id;nome'
          LookupDisplayIndex = 1
          LookupFormat = 'cred_id;nome'
          LookupSource = DSCredenciados
          TabOrder = 0
        end
        object JvDBLookupCombo1: TJvDBLookupCombo
          Left = 20
          Top = 65
          Width = 345
          Height = 21
          DataField = 'COD_CRED_ADM'
          DataSource = DSConfig
          LookupField = 'cred_id'
          LookupDisplay = 'cred_id;nome'
          LookupDisplayIndex = 1
          LookupFormat = 'cred_id;nome'
          LookupSource = DSCredenciados
          TabOrder = 1
        end
        object JvDBLookupCombo2: TJvDBLookupCombo
          Left = 20
          Top = 107
          Width = 345
          Height = 21
          DataField = 'COD_CRED_CPMF'
          DataSource = DSConfig
          LookupField = 'cred_id'
          LookupDisplay = 'cred_id;nome'
          LookupDisplayIndex = 1
          LookupFormat = 'cred_id;nome'
          LookupSource = DSCredenciados
          TabOrder = 2
        end
        object DBCheckBox1: TDBCheckBox
          Left = 20
          Top = 203
          Width = 319
          Height = 17
          Hint = 
            'Marque este campo para que quando na baixa de empresas'#13#10'se o val' +
            'or a baixar for menor que o valor devido o lan'#231'amento'#13#10'da difere' +
            'n'#231'a entre os valores seja efetuado para o pr'#243'ximo'#13#10'per'#237'odo de fe' +
            'chamento da empresa.'
          Caption = 'Lan'#231'ar diferen'#231'a na baixa de empresas para pr'#243'ximo per'#237'odo'
          DataField = 'LANC_DIFE_PROX_PERIODO'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 20
          Top = 244
          Width = 300
          Height = 36
          Hint = 
            'Na tela de lan'#231'amentos ap'#243's um lan'#231'amento mover o foco para o '#13#10 +
            'c'#243'digo da empresa. (obs: Se estiver desmarcado o foco ir'#225' para o' +
            ' '#13#10'c'#243'digo do estabelecimento)'
          Caption = 
            'Na tela de lan'#231'amentos foco para o c'#243'digo do estab.. Se desmarca' +
            'do o foco ir'#225' para o c'#243'digo da empresa.'
          DataField = 'FLANCAMENTOS_FOCO_FORNEC'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox3: TDBCheckBox
          Left = 20
          Top = 226
          Width = 389
          Height = 17
          Hint = 
            'Marque este campo para que quando na baixa de empresas'#13#10'se o val' +
            'or a baixar for menor que o valor devido o lan'#231'amento'#13#10'da difere' +
            'n'#231'a entre os valores seja efetuado para o pr'#243'ximo'#13#10'per'#237'odo de fe' +
            'chamento da empresa.'
          Caption = 
            'Na tela de lan'#231'amentos utilizar regra de autoriza'#231#227'o inclusive p' +
            'arcelamento'
          DataField = 'FLANCAMENTOS_AUTOR_WEBSERVICE'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 20
          Top = 278
          Width = 340
          Height = 36
          Hint = 
            'Na tela de lan'#231'amentos ap'#243's um lan'#231'amento mover o foco para o '#13#10 +
            'c'#243'digo da empresa. (obs: Se estiver desmarcado o foco ir'#225' para o' +
            ' '#13#10'c'#243'digo do estabelecimento)'
          Caption = 
            'Habilitar os estabelecimentos a pegarem autoriza'#231#245'es for'#231'adas, i' +
            'sto '#233', sem passar pelas valida'#231#245'es de saldo e bloqueio do sistem' +
            'a.'
          DataField = 'PERMITE_AUTOR_FORCADA'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox8: TDBCheckBox
          Left = 20
          Top = 316
          Width = 340
          Height = 17
          Hint = 
            'Na tela de lan'#231'amentos ap'#243's um lan'#231'amento mover o foco para o '#13#10 +
            'c'#243'digo da empresa. (obs: Se estiver desmarcado o foco ir'#225' para o' +
            ' '#13#10'c'#243'digo do estabelecimento)'
          Caption = 'Na tela de lan'#231'amentos pesquisar empresa pelo Nome Fantasia'
          DataField = 'EMP_FANTASIA'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox9: TDBCheckBox
          Left = 20
          Top = 337
          Width = 340
          Height = 17
          Hint = 
            'Na tela de lan'#231'amentos ap'#243's um lan'#231'amento mover o foco para o '#13#10 +
            'c'#243'digo da empresa. (obs: Se estiver desmarcado o foco ir'#225' para o' +
            ' '#13#10'c'#243'digo do estabelecimento)'
          Caption = 'Na tela de lan'#231'amentos pesquisar estab. pelo Nome Fantasia'
          DataField = 'CRE_FANTASIA'
          DataSource = DSConfig
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object JvDBLookupCombo3: TJvDBLookupCombo
          Left = 20
          Top = 153
          Width = 345
          Height = 21
          DataField = 'COD_EMP_LANC'
          DataSource = DSConfig
          LookupField = 'EMPRES_ID'
          LookupDisplay = 'empres_id;nome'
          LookupDisplayIndex = 1
          LookupFormat = 'empres_id;nome'
          LookupSource = DSEmpresas
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 314
          Top = 355
          Width = 40
          Height = 21
          DataField = 'DIAS_BLOQ_EMPR_FAT'
          DataSource = DSConfig
          TabOrder = 10
          OnKeyPress = DBEdit6KeyPress
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Cadastro de Conveniados'
        ImageIndex = 2
        object DBRadioGroup1: TDBRadioGroup
          Left = 20
          Top = 8
          Width = 373
          Height = 89
          Caption = 'Verifica'#231#227'o de CPF'
          DataField = 'VERIFICA_CPF_CONV'
          DataSource = DSConfig
          Items.Strings = (
            'N'#227'o efetuar.'
            'Se j'#225' existir perguntar se continua ou n'#227'o.'
            'Se j'#225' existir n'#227'o gravar o cadastro')
          TabOrder = 0
          TabStop = True
          Values.Strings = (
            'N'
            'P'
            'C')
        end
        object DBCheckBox13: TDBCheckBox
          Left = 20
          Top = 106
          Width = 373
          Height = 23
          Hint = 
            'Essa op'#231#227'o implica na mudan'#231'a de Nome da Empresa para Nome Fanta' +
            'sia da '#13#10'Empresa, na consulta, grade e sele'#231#227'o da empresa no Cad' +
            'astro de Conveniados.'
          Caption = 
            'Selecionar e consultar a empresa pelo Nome Fantasia no Cadastro ' +
            'de Conveniados'
          DataField = 'EMP_FANTASIA_CADCONV'
          DataSource = DSConfig
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox16: TDBCheckBox
          Left = 20
          Top = 138
          Width = 373
          Height = 23
          Caption = 
            'Ao alterar o nome do titular, alterar automaticamente o nome no ' +
            'cart'#227'o do titular.'
          DataField = 'ALTERAR_TITULAR_NOME_CARTAO'
          DataSource = DSConfig
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox17: TDBCheckBox
          Left = 20
          Top = 170
          Width = 373
          Height = 25
          Caption = 
            'Ao alterar o titular para liberado, alterar automaticamente o ca' +
            'rt'#227'o do titular para liberado. (o mesmo ocorre para bloqueio)'
          DataField = 'ALTERAR_TITULAR_LIBERADO_CARTAO'
          DataSource = DSConfig
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox10: TDBCheckBox
          Left = 20
          Top = 202
          Width = 373
          Height = 25
          Caption = 
            'Ao incluir um novo conveniado, jogar o Conv. ID como senha inici' +
            'al, se essa op'#231#227'o estiver desmarcada, ir'#225' jogar como padr'#227'o a se' +
            'nha 1111.'
          DataField = 'SENHA_CONV_ID'
          DataSource = DSConfig
          TabOrder = 4
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox22: TDBCheckBox
          Left = 20
          Top = 234
          Width = 373
          Height = 39
          Caption = 
            'Ao solicitar uma 2'#170' Via do cart'#227'o, emitir um novo cart'#227'o. Caso a' +
            ' op'#231#227'o esteja desmarcada, ao solicitar uma 2'#170' via, ser'#225' mudado a' +
            'penas o campo ja emitido para N'
          DataField = 'EMITE_NOVO_CART_2VIA'
          DataSource = DSConfig
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
        object DBCheckBox23: TDBCheckBox
          Left = 20
          Top = 282
          Width = 373
          Height = 23
          Caption = 
            'Ao preencher o campo demitido, mover as autoriza'#231#245'es em aberto p' +
            'ara o proximo movimento em aberto n'#227'o faturado.'
          DataField = 'DEMISSAO_MOVE_AUTS'
          DataSource = DSConfig
          TabOrder = 6
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          WordWrap = True
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'M'#243'dulos Web'
        ImageIndex = 3
        object SpeedButton2: TSpeedButton
          Left = 384
          Top = 32
          Width = 23
          Height = 22
          Caption = 'Ir'
          OnClick = SpeedButton2Click
        end
        object Label13: TLabel
          Left = 16
          Top = 16
          Width = 109
          Height = 13
          Caption = 'Web Estabelecimentos'
        end
        object Label14: TLabel
          Left = 16
          Top = 64
          Width = 72
          Height = 13
          Caption = 'Web Empresas'
        end
        object Label15: TLabel
          Left = 16
          Top = 112
          Width = 67
          Height = 13
          Caption = 'Web Usu'#225'rios'
        end
        object Label16: TLabel
          Left = 16
          Top = 160
          Width = 68
          Height = 13
          Caption = 'Onde Comprar'
        end
        object SpeedButton3: TSpeedButton
          Left = 384
          Top = 80
          Width = 23
          Height = 22
          Caption = 'Ir'
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 384
          Top = 128
          Width = 23
          Height = 22
          Caption = 'Ir'
          OnClick = SpeedButton4Click
        end
        object SpeedButton5: TSpeedButton
          Left = 384
          Top = 176
          Width = 23
          Height = 22
          Caption = 'Ir'
          OnClick = SpeedButton5Click
        end
        object DBEdit7: TDBEdit
          Left = 16
          Top = 32
          Width = 361
          Height = 21
          DataField = 'LINK_EST'
          DataSource = DSConfig
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 16
          Top = 80
          Width = 361
          Height = 21
          DataField = 'LINK_EMP'
          DataSource = DSConfig
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 16
          Top = 128
          Width = 361
          Height = 21
          DataField = 'LINK_USU'
          DataSource = DSConfig
          TabOrder = 2
        end
        object DBEdit10: TDBEdit
          Left = 16
          Top = 176
          Width = 361
          Height = 21
          DataField = 'LINK_OND'
          DataSource = DSConfig
          TabOrder = 3
        end
      end
      object tabHistorico: TTabSheet
        Caption = 'Hist'#243'rico'
        ImageIndex = 4
        OnHide = tabHistoricoHide
        OnShow = tabHistoricoShow
        object PanelHistorico: TPanel
          Left = 0
          Top = 0
          Width = 436
          Height = 47
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LabelDataini: TLabel
            Left = 4
            Top = 4
            Width = 53
            Height = 13
            Caption = 'Data Inicial'
          end
          object LabelDataFinal: TLabel
            Left = 111
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Data Final'
          end
          object btnFirstC: TSpeedButton
            Left = 311
            Top = 14
            Width = 33
            Height = 25
            Flat = True
            Glyph.Data = {
              1E030000424D1E030000000000001E0100002800000020000000100000000100
              08000000000000020000232E0000232E00003A00000000000000986532009B68
              35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
              6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABB00BBBC
              BD00BDBEBE00BFBFC000D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
              9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C1C2C300C4C5C500C5C6
              C700C8C9C900CBCBCC00CBCCCD00CCCDCD00CECFCF00D0D0D100D3D4D400D5D5
              D600DBDBDC00DCDCDC00DCDDDD00DFDFE000FFE3C700FFE5CC00E1E1E100E6E6
              E600E7E7E700E8E9E900EBEBEB00EEEEEE00EFEFF000F2F2F200F3F3F300FFFF
              FF00393939393939393939393939393939393939393939393939393939393939
              3939393939393939393939393939393939393939393939393939393939393939
              3939393939393939000939393939390009393939393939391024393939393910
              24393939393939010F0139393939010F01393939393939112A1139393939112A
              11393939393902151902393939021519023939393939122A3112393939122A31
              123939393903161A1A03393903161A1A0339393939132C3232133939132C3232
              1339393904171B1B1B043904171B1B1B043939391F2D3333331F391F2D333333
              1F393905181C1C1C1C050D181C1C1C1C05393920303434343420283034343434
              203939062F1D1D1D1D060E2F1D1D1D1D06393921383535353521293835353535
              21393939072F1E1E1E0739072F1E1E1E07393939223836363622392238363636
              2239393939082F2E2E083939082F2E2E08393939392338373723393923383737
              2339393939390A2F2F0A3939390A2F2F0A393939393925383825393939253838
              253939393939390B2F0B393939390B2F0B393939393939263826393939392638
              26393939393939390C1439393939390C1439393939393939272B393939393927
              2B39393939393939393939393939393939393939393939393939393939393939
              3939393939393939393939393939393939393939393939393939393939393939
              3939}
            NumGlyphs = 2
            OnClick = btnFirstCClick
          end
          object btnPriorC: TSpeedButton
            Left = 345
            Top = 14
            Width = 33
            Height = 25
            Flat = True
            Glyph.Data = {
              32030000424D3203000000000000320100002800000020000000100000000100
              08000000000000020000232E0000232E00003F00000000000000986532009A67
              34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
              5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
              BE00BEBFBF00BFC0C100D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
              9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C1C2
              C300C3C4C400C5C5C600C7C8C800C9CACA00CBCBCC00CDCECE00CECFCF00D0D0
              D100D1D1D200DCDCDC00DCDDDD00DDDEDE00DEDFDF00FFE1C200FFE3C800FFE5
              CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00ECECEC00EEEE
              EE00EFEFF000F1F1F100F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
              3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
              000A3E3E3E3E3E3E3E3E3E3E3E3E3E3E10273E3E3E3E3E3E3E3E3E3E3E3E3E01
              0F013E3E3E3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E0215
              1A023E3E3E3E3E3E3E3E3E3E3E3E122B34123E3E3E3E3E3E3E3E3E3E3E03161B
              1B033E3E3E3E3E3E3E3E3E3E3E132D3535133E3E3E3E3E3E3E3E3E3E04171C1C
              1C043E3E3E3E3E3E3E3E3E3E212E363636213E3E3E3E3E3E3E3E3E05181D1D1D
              1D053E3E3E3E3E3E3E3E3E223237373737223E3E3E3E3E3E3E3E06191E1E1E1E
              1E063E3E3E3E3E3E3E3E23333838383838233E3E3E3E3E3E3E3E07311F1F1F1F
              1F073E3E3E3E3E3E3E3E243D3939393939243E3E3E3E3E3E3E3E3E0831202020
              20083E3E3E3E3E3E3E3E3E253D3A3A3A3A253E3E3E3E3E3E3E3E3E3E09312F2F
              2F093E3E3E3E3E3E3E3E3E3E263D3B3B3B263E3E3E3E3E3E3E3E3E3E3E0B3130
              300B3E3E3E3E3E3E3E3E3E3E3E273D3C3C273E3E3E3E3E3E3E3E3E3E3E3E0C31
              310C3E3E3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E3E0D
              310D3E3E3E3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E3E
              0E143E3E3E3E3E3E3E3E3E3E3E3E3E3E2A2C3E3E3E3E3E3E3E3E3E3E3E3E3E3E
              3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
            NumGlyphs = 2
            OnClick = btnPriorCClick
          end
          object btnNextC: TSpeedButton
            Left = 379
            Top = 14
            Width = 33
            Height = 25
            Flat = True
            Glyph.Data = {
              32030000424D3203000000000000320100002800000020000000100000000100
              08000000000000020000232E0000232E00003F00000000000000986532009A67
              34009E6B3800A16E3B00A6734000AA774400AF7C4900B4814E00B9865300BD8A
              5700B28C6500C28F5C00C5925F00CA966300CC986500E6B27F00BBBCBD00BCBD
              BE00BEBFBF00BFBFC000D9B28C00E7B38000E8B58400E9B88900EABB8E00EBBF
              9500FFCC9800FFCE9C00FFD0A100FFD3A700FFD7AE00FFDAB500FFDEBC00C0C1
              C200C2C3C300C5C5C600C6C7C800C9CACA00CBCBCC00CCCDCD00CECFCF00D0D0
              D100D1D1D200DBDBDC00DCDCDC00DCDDDD00DEDFDF00FFE1C200FFE3C800FFE5
              CC00E0E0E100E2E2E200E6E6E600E7E7E700E8E9E900EAEAEB00EBEBEB00EDED
              ED00EFEFF000F0F0F000F2F2F200F3F3F300FFFFFF003E3E3E3E3E3E3E3E3E3E
              3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E0A003E3E3E3E
              3E3E3E3E3E3E3E3E3E3E27103E3E3E3E3E3E3E3E3E3E3E3E3E3E010F013E3E3E
              3E3E3E3E3E3E3E3E3E3E112B113E3E3E3E3E3E3E3E3E3E3E3E3E021A15023E3E
              3E3E3E3E3E3E3E3E3E3E12342C123E3E3E3E3E3E3E3E3E3E3E3E031B1B16033E
              3E3E3E3E3E3E3E3E3E3E1335352D133E3E3E3E3E3E3E3E3E3E3E041C1C1C1704
              3E3E3E3E3E3E3E3E3E3E213636362E213E3E3E3E3E3E3E3E3E3E051D1D1D1D18
              053E3E3E3E3E3E3E3E3E223737373732223E3E3E3E3E3E3E3E3E061E1E1E1E1E
              19063E3E3E3E3E3E3E3E23383838383833233E3E3E3E3E3E3E3E071F1F1F1F1F
              31073E3E3E3E3E3E3E3E2439393939393D243E3E3E3E3E3E3E3E082020202031
              083E3E3E3E3E3E3E3E3E253A3A3A3A3D253E3E3E3E3E3E3E3E3E092F2F2F3109
              3E3E3E3E3E3E3E3E3E3E263B3B3B3D263E3E3E3E3E3E3E3E3E3E0B3030310B3E
              3E3E3E3E3E3E3E3E3E3E273C3C3D273E3E3E3E3E3E3E3E3E3E3E0C31310C3E3E
              3E3E3E3E3E3E3E3E3E3E283D3D283E3E3E3E3E3E3E3E3E3E3E3E0D310D3E3E3E
              3E3E3E3E3E3E3E3E3E3E293D293E3E3E3E3E3E3E3E3E3E3E3E3E140E3E3E3E3E
              3E3E3E3E3E3E3E3E3E3E2D2A3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E
              3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E3E}
            NumGlyphs = 2
            OnClick = btnNextCClick
          end
          object btnLastC: TSpeedButton
            Left = 413
            Top = 14
            Width = 33
            Height = 25
            Flat = True
            Glyph.Data = {
              1E030000424D1E030000000000001E0100002800000020000000100000000100
              08000000000000020000232E0000232E00003A00000000000000986532009B68
              35009F6C3900A4713E00A9764300AF7C4900B4814E00BA875400BF8C5900B28C
              6500C4915E00C9956200CC986500C39D7700C7A17A00E6B27F00B9BABA00BABB
              BC00BCBDBE00BEBFBF00D9B28C00E7B38000E8B68500EABA8C00EBBF9400FFCC
              9800FFCE9D00FFD2A400FFD6AD00FFDBB600FFDFBF00C0C1C200C3C4C400C5C5
              C600C7C8C800CACACB00CBCBCC00CBCCCD00CDCECE00CFCFD000D2D3D300D4D5
              D500DADADB00DBDBDC00DCDDDD00DEDFDF00FFE3C700FFE5CC00E1E1E100E5E5
              E600E7E7E700E8E9E900EAEAEB00EDEDED00EFEFF000F1F1F100F3F3F300FFFF
              FF00393939393939393939393939393939393939393939393939393939393939
              3939393939393939393939393939393939393939393939393939393939393939
              3939390900393939393909003939393939393924103939393939241039393939
              393939010F0139393939010F01393939393939112A1139393939112A11393939
              3939390219150239393902191502393939393912312A1239393912312A123939
              393939031A1A16033939031A1A1603393939391332322C1339391332322C1339
              393939041B1B1B170439041B1B1B17043939391F3333332D1F391F3333332D1F
              393939051C1C1C1C180D051C1C1C1C1805393920343434343028203434343430
              203939061D1D1D1D2F0E061D1D1D1D2F06393921353535353829213535353538
              213939071E1E1E2F0739071E1E1E2F0739393922363636382239223636363822
              393939082E2E2F083939082E2E2F083939393923373738233939233737382339
              3939390A2F2F0A3939390A2F2F0A393939393925383825393939253838253939
              3939390B2F0B393939390B2F0B39393939393926382639393939263826393939
              393939140C3939393939140C393939393939392B2739393939392B2739393939
              3939393939393939393939393939393939393939393939393939393939393939
              3939393939393939393939393939393939393939393939393939393939393939
              3939}
            NumGlyphs = 2
            OnClick = btnLastCClick
          end
          object dataini: TJvDateEdit
            Left = 4
            Top = 18
            Width = 102
            Height = 21
            Hint = 'Data inicial para busca'
            NumGlyphs = 2
            ShowNullDate = False
            TabOrder = 1
          end
          object datafin: TJvDateEdit
            Left = 110
            Top = 18
            Width = 102
            Height = 21
            Hint = 'Data final para busca'
            NumGlyphs = 2
            ShowNullDate = False
            TabOrder = 2
          end
          object btnHist: TBitBtn
            Left = 216
            Top = 14
            Width = 89
            Height = 25
            Caption = '&Visualizar'
            TabOrder = 0
            OnClick = btnHistClick
            Glyph.Data = {
              AE030000424DAE03000000000000AE0100002800000020000000100000000100
              08000000000000020000232E0000232E00005E00000000000000512600005157
              61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
              7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
              9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
              BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
              BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
              CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
              D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
              DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
              E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
              ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
              F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
              FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
              09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
              0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
              23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
              065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
              5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
              5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
              5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
              5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
              5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
              5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
              5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
              5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
              5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
              5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
              5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
              5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
            NumGlyphs = 2
          end
        end
        object grdHistorico: TJvDBGrid
          Left = 0
          Top = 47
          Width = 436
          Height = 505
          Align = alClient
          DataSource = dsHistorico
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DETALHE'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_HORA'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CADASTRO'
              Width = 170
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CAMPO'
              Width = 99
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_ANT'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_POS'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERADOR'
              Width = 97
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERACAO'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MOTIVO'
              Visible = True
            end>
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 580
      Width = 444
      Height = 46
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object ButOk: TBitBtn
        Left = 178
        Top = 10
        Width = 98
        Height = 27
        Caption = '&Ok'
        TabOrder = 0
        OnClick = ButOkClick
      end
      object ButCancel: TBitBtn
        Left = 276
        Top = 10
        Width = 98
        Height = 27
        Caption = '&Cancelar'
        TabOrder = 1
        OnClick = ButCancelClick
      end
    end
  end
  object DSConfig: TDataSource
    DataSet = DMConexao.Config
    Left = 215
    Top = 208
  end
  object DSCredenciados: TDataSource
    Left = 316
    Top = 208
  end
  object DSEmpresas: TDataSource
    Left = 264
    Top = 208
  end
  object dsHistorico: TDataSource
    OnDataChange = dsHistoricoDataChange
    Left = 364
    Top = 208
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome from empresas order by nome')
    Left = 276
    Top = 168
    object QEmpresasempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
  object QHistorico: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from logs'
      
        'where data_hora between '#39'03/01/2011 00:00:00'#39' and '#39'12/31/2011 59' +
        ':59:59'#39
      'and JANELA = '#39'FConfig'#39
      'order by data_hora desc ')
    Left = 316
    Top = 168
  end
  object QCredenciados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select cred_id, nome from credenciados order by nome')
    Left = 356
    Top = 168
    object QCredenciadoscred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QCredenciadosnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
  end
end
