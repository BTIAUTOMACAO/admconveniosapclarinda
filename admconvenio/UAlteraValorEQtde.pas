unit UAlteraValorEQtde;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, ADODB;

type
  TFrmAlterarValorEQtde = class(TForm)
    Panel1: TPanel;
    btnCancelar: TButton;
    btnConfirma: TButton;
    codEmpresInput: TEdit;
    Label1: TLabel;
    convIdInput: TEdit;
    Label2: TLabel;
    valorInput: TEdit;
    Label3: TLabel;
    QtdeInput: TEdit;
    Label4: TLabel;
    QCadastro: TADOQuery;
    CheckBox1: TCheckBox;
    QCadastroCONV_ID: TIntegerField;
    QCadastroEMPRES_ID: TIntegerField;
    Label5: TLabel;
    lblOperador: TLabel;
    Motivo: TMemo;
    Label6: TLabel;
    QCadastroLIBERADO: TStringField;
    QCadastroAPAGADO: TStringField;
    QAESP: TADOQuery;
    QAESPEMPRES_ID: TIntegerField;
    QAESPCONV_ID: TIntegerField;
    QAESPLIBERADO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmaClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure codEmpresInputKeyPress(Sender: TObject; var Key: Char);
    procedure convIdInputKeyPress(Sender: TObject; var Key: Char);
    procedure QtdeInputKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAlterarValorEQtde: TFrmAlterarValorEQtde;

implementation

uses DM, Umenu, DateUtils;
{$R *.dfm}

procedure TFrmAlterarValorEQtde.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAlterarValorEQtde.btnConfirmaClick(Sender: TObject);
var
  S: Integer;
begin
  if Trim(codEmpresInput.Text) <> '' then begin
    if Trim(convIdInput.Text) <> '' then begin
      QCadastro.Close;
      QCadastro.SQL.Clear;
      QCadastro.SQL.Add('SELECT CONV_ID, EMPRES_ID, LIBERADO, APAGADO FROM CONVENIADOS WHERE EMPRES_ID = '+QuotedStr(codEmpresInput.Text)+' AND CONV_ID = '+QuotedStr(convIdInput.Text)+'');
      QCadastro.Open;
      
      if QCadastroEMPRES_ID.IsNull = true or QCadastroCONV_ID.IsNull = true then begin
        ShowMessage('Empresa ou Conveniado n�o encontrados, por favor repita o processo');
        codEmpresInput.Clear;
        convIdInput.Clear;
        valorInput.Clear;
        QtdeInput.Clear;
        motivo.Clear;
        CheckBox1.Checked := false;
        codEmpresInput.SetFocus;
      end
      else if (QCadastroLIBERADO.AsString = 'N') or (QCadastroAPAGADO.AsString = 'S') then begin
        ShowMessage('Conveniado Apagado ou bloqueado, por favor repita o processo');
        codEmpresInput.Clear;
        convIdInput.Clear;
        valorInput.Clear;
        QtdeInput.Clear;
        motivo.Clear;
        CheckBox1.Checked := false;
        convIdInput.SetFocus;
      end
      else begin
        QCadastro.Close;
        QCadastro.SQL.Clear;

        QAESP.Close;
        QAESP.SQL.Clear;
        QAESP.SQL.Add('SELECT EMPRES_ID, CONV_ID, LIBERADO FROM CONV_AESP_ODONTO WHERE EMPRES_ID ='+QuotedStr(codEmpresInput.Text)+' AND CONV_ID = '+QuotedStr(convIdInput.Text)+'');
        QAESP.Open;

        if QAESPEMPRES_ID.IsNull = true or QAESPCONV_ID.IsNull = true then begin
          ShowMessage('Empresa ou Conveniado n�o fazem parte do AESP ODONTO');
        end
        else if QAESPLIBERADO.AsString = 'N' then begin
          ShowMessage('Conveniado bloqueado no AESP Odonto');
        end
        else begin
          QCadastro.Close;
          QCadastro.SQL.Clear;
          QCadastro.SQL.Add('UPDATE CONV_AESP_ODONTO SET');

          if Trim(valorInput.Text) <> '' then begin
             QCadastro.SQL.Add(' VALOR = '+StringReplace(valorInput.Text, ',', '.', [rfReplaceAll, rfIgnoreCase])+'');
          end;

          if CheckBox1.Checked then begin
            if Trim(QtdeInput.Text) <> '' then begin
              if Trim(valorInput.Text) <> '' then begin
                QCadastro.SQL.Add(', QUANTIDADE = '+QtdeInput.Text+'');
              end
              else begin
                QCadastro.SQL.Add(' QUANTIDADE = '+QtdeInput.Text+'');
              end
            end
            else begin
              ShowMessage('Por favor informe a quantidade');
              QtdeInput.SetFocus;
            end;
          end;

          //Parte de Log
          S := Length(Trim(Motivo.Text));
          if Trim(Motivo.Text) <> '' then begin
            if S > 5 then begin
              QCadastro.SQL.Add(', DATA_ALTERACAO = '+QuotedStr(FormatDateTime('yyyy-mm-dd hh:MM:ss', Now))+', MOTIVO = '+QuotedStr(Motivo.Text)+', OPERADOR = '+QuotedStr(Operador.Nome)+'');

              QCadastro.SQL.Add('WHERE CONV_ID = '+QuotedStr(convIdInput.Text)+' AND EMPRES_ID = '+QuotedStr(codEmpresInput.Text)+'');
              try
                QCadastro.ExecSQL;
                ShowMessage('Altera��o feita com sucesso!');
                codEmpresInput.Clear;
                convIdInput.Clear;
                valorInput.Clear;
                QtdeInput.Clear;
                CheckBox1.Checked := false;
                Motivo.Lines.Clear; 
                codEmpresInput.SetFocus;
                QCadastro.Close;
              except on E:Exception do
                ShowMessage('Aconteceu algo de errado - ERRO: '+E.Message);
              end; //End do Try
            end //End do IF
            else begin
              ShowMessage('O motivo precisa ter mais de cinco caracteres');
            end
          end
          else begin
            ShowMessage('o Motivo precisa ser preenchido');
          end;
        end
      end
    end
    else begin
      ShowMessage('Por favor insira o ID do Conveniado');
      convIdInput.SetFocus;
    end;
  end
  else begin
    ShowMessage('Por favor Insira a empresa');
    codEmpresInput.SetFocus;
  end;
end;

procedure TFrmAlterarValorEQtde.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then begin
    QtdeInput.Enabled := true;
    QtdeInput.Color := clWindow;
  end
  else begin
    QtdeInput.Enabled := false;
    QtdeInput.Color := clYellow;
  end;
  end; 
procedure TFrmAlterarValorEQtde.codEmpresInputKeyPress(Sender: TObject;
  var Key: Char);
begin
  If not( key in['0'..'9',#08] ) then
    key:=#0;
end;

procedure TFrmAlterarValorEQtde.convIdInputKeyPress(Sender: TObject;
  var Key: Char);
begin
  If not( key in['0'..'9',#08] ) then
    key:=#0;
end;

procedure TFrmAlterarValorEQtde.QtdeInputKeyPress(Sender: TObject;
  var Key: Char);
begin
  If not( key in['0'..'9',#08] ) then
    key:=#0;
end;

procedure TFrmAlterarValorEQtde.FormShow(Sender: TObject);
begin
  lblOperador.Caption := Operador.Nome; //Verificar como se faz isso
end;

end.
