unit UFatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, StdCtrls, Mask, JvToolEdit, DateUtils,
  {JvLookup,} DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids, inifiles,
  DBGrids, {JvDBCtrl,} ComCtrls, ToolEdit, CurrEdit, ClassImpressao, UClassLog, umenu,
  DBCtrls, ShellAPI, JvExDBGrids, JvDBGrid, JvExControls, JvDBLookup,
  JvExMask, JvExStdCtrls, JvEdit, JvValidateEdit, ADODB, JvMemoryDataset;

type
  THackDBgrid = class(TDBGrid);
  TFFatura = class(TF1)
    Panel1: TPanel;
    rdgTipo: TRadioGroup;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    DataIni: TJvDateEdit;
    DataFim: TJvDateEdit;
    GroupBox2: TGroupBox;
    EdCod: TEdit;
    DBEmpresa: TJvDBLookupCombo;
    Label1: TLabel;
    Label3: TLabel;
    DSEmpresa: TDataSource;
    Button1: TButton;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridFatur: TJvDBGrid;
    DSFatura: TDataSource;
    ButFechar: TButton;
    Bevel2: TBevel;
    ButImprimir: TBitBtn;
    Panel17: TPanel;
    Label63: TLabel;
    Panel3: TPanel;
    //EdBaixar: TCurrencyEdit;
    //EdBaixado: TCurrencyEdit;
    Label4: TLabel;
    Label5: TLabel;
    Bevel3: TBevel;
    ChkTodasDatas: TCheckBox;
    DSDadosEmp: TDataSource;
    TabSheet2: TTabSheet;
    PanelHistorico: TPanel;
    LabelDataini: TLabel;
    LabelDataFinal: TLabel;
    Buthist: TButton;
    DBNavigatorHistorico: TDBNavigator;
    DataIni2: TJvDateEdit;
    DataFim2: TJvDateEdit;
    GridHistorico: TJvDBGrid;
    DSHistorico: TDataSource;
    PopupMenu1: TPopupMenu;
    InserirObservao1: TMenuItem;
    ReabreFatura: TMenuItem;
    CancelaFatura: TMenuItem;
    ButMarcDesm: TButton;
    ButMarcaTodos: TButton;
    ButDesmTodos: TButton;
    AlteraraDatadeVencimento1: TMenuItem;
    AplicaPreBaixa: TMenuItem;
    CancelaPreBaixa: TMenuItem;
    RGFaturas: TRadioGroup;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    edtFaturaID: TEdit;
    ImprimeBoletoWeb: TMenuItem;
    EdBaixar: TJvValidateEdit;
    EdBaixado: TJvValidateEdit;
    QDescEmp: TADOQuery;
    QDescEmpcred_id: TIntegerField;
    QDescEmpdata_fecha_emp: TDateTimeField;
    QDescEmpvalor: TBCDField;
    QDescEmpdesconto_emp: TBCDField;
    QDescEmpdesc_valor: TBCDField;
    Empresa: TADOQuery;
    Empresaempres_id: TIntegerField;
    Empresanome: TStringField;
    Empresadesconto_emp: TBCDField;
    QHistorico: TADOQuery;
    QHistoricoLOG_ID: TIntegerField;
    QHistoricoJANELA: TStringField;
    QHistoricoCAMPO: TStringField;
    QHistoricoVALOR_ANT: TStringField;
    QHistoricoVALOR_POS: TStringField;
    QHistoricoOPERADOR: TStringField;
    QHistoricoOPERACAO: TStringField;
    QHistoricoDATA_HORA: TDateTimeField;
    QHistoricoCADASTRO: TStringField;
    QHistoricoID: TIntegerField;
    QHistoricoDETALHE: TStringField;
    QHistoricoMOTIVO: TStringField;
    QHistoricoSOLICITANTE: TStringField;
    QDadosConv: TADOQuery;
    QDadosConvCOLUMN1: TBCDField;
    QDadosConvvalor_rec: TBCDField;
    QDadosConvvalor_sem: TBCDField;
    QDadosConvconv_id: TIntegerField;
    QDadosConvchapa: TFloatField;
    QDadosConvtitular: TStringField;
    QDadosConvreceita: TStringField;
    QFatura: TADOQuery;
    QDadosEmp: TADOQuery;
    MFatura: TJvMemoryData;
    MFaturaID: TIntegerField;
    MFaturaDATA_FATURA: TDateTimeField;
    MFaturaHORA_FATURA: TTimeField;
    MFaturaDATA_VENCIMENTO: TDateTimeField;
    MFaturaSO_CONFIRMADAS: TStringField;
    MFaturaOBS: TStringField;
    MFaturaFATURA_ID: TIntegerField;
    MFaturaFECHAMENTO: TDateTimeField;
    MFaturaOPERADOR: TStringField;
    MFaturaVALOR: TFloatField;
    MFaturaDATA_BAIXA: TDateTimeField;
    MFaturaAPAGADO: TStringField;
    MFaturaBAIXADA: TStringField;
    MFaturaPRE_BAIXA: TStringField;
    MFaturaDATA_PRE_BAIXA: TDateTimeField;
    MFaturaTIPO: TStringField;
    MFaturaNOME: TStringField;
    MFaturaDESC_EMPRESA: TFloatField;
    MFaturaMARCADO: TBooleanField;
    btnInsObs: TButton;
    btnAlterarVenc: TButton;
    btnRFatura: TButton;
    btnCanReab: TButton;
    QFaturaid: TIntegerField;
    QFaturadata_fatura: TDateTimeField;
    QFaturahora_fatura: TStringField;
    QFaturadata_vencimento: TDateTimeField;
    QFaturaso_confirmadas: TStringField;
    QFaturaobs: TStringField;
    QFaturadesc_empresa: TFloatField;
    QFaturafatura_id: TIntegerField;
    QFaturafechamento: TDateTimeField;
    QFaturaoperador: TStringField;
    QFaturabanco_id: TIntegerField;
    QFaturaforma_id: TIntegerField;
    QFaturavalor: TFloatField;
    QFaturadata_baixa: TDateTimeField;
    QFaturaapagado: TStringField;
    QFaturabaixada: TStringField;
    QFaturapre_baixa: TStringField;
    QFaturadata_pre_baixa: TDateTimeField;
    QFaturatipo: TStringField;
    QFaturanome: TStringField;
    MFATURABANCO_ID: TIntegerField;
    MFaturaFORMAPGTO_ID: TIntegerField;
    QFaturabanco: TStringField;
    MFaturaBANCO: TStringField;
    MFaturaFORMA_PGTO: TStringField;
    QFaturaFORMA_PGTO: TStringField;
    QFaturavalor_pago: TBCDField;
    MFaturaVALOR_PAGO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure EdCodChange(Sender: TObject);
    procedure EdCodKeyPress(Sender: TObject; var Key: Char);
    procedure GridFaturKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButFecharClick(Sender: TObject);
    procedure GridFaturDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBEmpresaChange(Sender: TObject);
    procedure ButImprimirClick(Sender: TObject);
    procedure DataIniExit(Sender: TObject);
    procedure GridFaturTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure rdgTipoClick(Sender: TObject);
    procedure ChkTodasDatasClick(Sender: TObject);
    procedure QFaturaAfterScroll(DataSet: TDataSet);
    procedure GridFaturDblClick(Sender: TObject);
    procedure ButhistClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure InserirObservao1Click(Sender: TObject);
    procedure ReabreFaturaClick(Sender: TObject);
    procedure CancelaFaturaClick(Sender: TObject);
    procedure ButMarcDesmClick(Sender: TObject);
    procedure ButMarcaTodosClick(Sender: TObject);
    procedure ButDesmTodosClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure AlteraraDatadeVencimento1Click(Sender: TObject);
    procedure AplicaPreBaixaClick(Sender: TObject);
    procedure CancelaPreBaixaClick(Sender: TObject);
    procedure edtFaturaIDKeyPress(Sender: TObject; var Key: Char);
    procedure ImprimeBoletoWebClick(Sender: TObject);
    procedure MFaturaAfterScroll(DataSet: TDataSet);
    procedure btnInsObsClick(Sender: TObject);
    procedure btnAlterarVencClick(Sender: TObject);
    procedure btnRFaturaClick(Sender: TObject);
    procedure btnCanReabClick(Sender: TObject);
  private
    fatura_sel : String;
    function Sql: string;
    procedure SomarFaturas;
    function ContarConveniadosPorFatura: String;
    function ContarAturizacoesPorFatura: String;
    procedure ImprimirValoresFornecedores(Imp: TImpres);
    procedure CriarCorpoFatura(Imp: TImpres);
    procedure ImprimeBoleto;
    procedure CriaRelLista(Imp: TImpres);
    procedure CriarRelTotalConv(Imp: Timpres; SepararComSemRec:Boolean; PorAut: Boolean);
    procedure HabilitarBotoes;
    function AbrirQueryConv(Fatura_ID: Integer;
      SepararComSemRec: Boolean; PorAut: Boolean;PorGrupo:Boolean=False): Boolean;
    function CriarBoleto(LayOUt: TStringList): TStringList;
    function ImprimirBoleto(Boleto: TStringList): Boolean;
    procedure ImprimeBoletoCobreBem;
    procedure AlterandoDataVenc;
    procedure GeraDescontoEmpresa(Emp: Integer; DataFecha: TDateTime;
      Fat: integer; Nome: String);
    procedure CriarRelPorGrupoEmp(Imp: Timpres; SepararComSemRec: Boolean);
    procedure RelConvPorAutorizacao(Imp: Timpres);
    procedure Fatura;
    procedure BaixarFatura;
    { Private declarations }
  public
    { Public declarations }

  end;

var
  FFatura: TFFatura;

implementation

uses DM, USelTipoImp, cartao_util, USelecionaBanco, UImpCobreBem,
  UAlteraDataVenc, UDuplicata, FOcorrencia, UPreFatura, ULancamentosFatura,
  URotinasTexto;

{$R *.dfm}

procedure TFFatura.FormCreate(Sender: TObject);
begin
  inherited;
  DataIni.Date := Date;
  DataIni2.Date := Date;
  DataFim.Date := Date;
  DataFim2.Date := Date;
  Empresa.Open;
  QFatura.Open;
  HabilitarBotoes;
  QFatura.Close;
  FMenu.vMantFat := True;
end;

procedure TFFatura.EdCodChange(Sender: TObject);
begin
  inherited;
  if Trim(EdCod.Text) <> '' then
  begin
    if Empresa.Locate('empres_id',EdCod.Text,[]) then
      DBEmpresa.KeyValue := EdCod.Text
    else
      DBEmpresa.ClearValue;
  end
  else
    DBEmpresa.ClearValue;
end;

procedure TFFatura.EdCodKeyPress(Sender: TObject; var Key: Char);
begin
      inherited;
   if not (key in ['0'..'9',#13,#8]) then Key := #0;
end;

procedure TFFatura.GridFaturKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    GridFaturDblClick(nil)
  else
    GridScroll(Sender,Key,Shift);
end;

function TFFatura.Sql:string;
begin
  Result := ' Select '+
            '   fat.id, '+
            '   fat.data_fatura, '+
            '   coalesce(hora_fatura,''00:00:00'') as hora_fatura, '+
            '   fat.data_vencimento, '+
            '   fat.so_confirmadas, '+
            '   fat.obs, '+
            '   fat.fatura_id, '+
            '   fat.fechamento, '+
            '   fat.operador, '+
            '   fat.valor, '+
            '   fat.valor_pago, '+
            '   coalesce(fat.banco_id,-1) banco_id, '+
            '   coalesce(fat.forma_id,-1) forma_id, '+
            '   fat.data_baixa, '+
            '   fat.apagado, '+
            '   coalesce(fat.baixada,''N'') baixada, '+
            '   coalesce(fat.pre_baixa,''N'') pre_baixa, '+
            '   fat.data_pre_baixa, '+
            '   coalesce(fat.tipo,''E'') as tipo, '+
            '   case when coalesce(fat.tipo,''E'') = ''E'' then emp.nome else conv.titular end as nome, '+
            '   coalesce(desc_empresa,0) as desc_empresa, '+
            '   coalesce(banco.banco,'''') BANCO, '+
            '   coalesce(FORMASPAGTO.DESCRICAO,'''') FORMA_PGTO' +
             ' from fatura fat '+
            ' left join empresas emp on ((coalesce(fat.tipo,''E'') = ''E'') or (coalesce(fat.tipo,''E'') = ''D'')) and emp.empres_id = fat.id '+
            ' left join conveniados conv on fat.tipo = ''C'' and conv.conv_id = fat.id ' +
            'LEFT JOIN BANCOS banco ON fat.BANCO_ID = banco.CODIGO ' +
            'LEFT JOIN FORMAPAGTO_FATURAS FORMASPAGTO ON FORMASPAGTO.FORMA_ID = FAT.FORMA_ID ';
end;


procedure TFFatura.Button1Click(Sender: TObject);
begin
  inherited;
  QFatura.Close;
  QFatura.SQL.Text := Sql;
  QFatura.SQL.Add(' where coalesce(fat.apagado,''N'')<>''S'' ');
  if edtFaturaID.Text <> '' then
  begin
    MsgInf('A busca ser� feita pelo n�mero da fatura,'+sLineBreak+'os outros filtros ser�o descartados!');
    QFatura.SQL.Add(' and fat.fatura_id in ('+edtFaturaID.Text+') ');
  end
  else
  begin
    case rdgTipo.ItemIndex of
      1: QFatura.SQL.Add(' and fat.tipo = ''E'' ');
      2: QFatura.SQL.Add(' and fat.tipo = ''C'' ');
      3: QFatura.SQL.Add(' and fat.tipo = ''D'' ');
    end;
    if RGFaturas.ItemIndex = 0 then
      QFatura.SQL.Add(' and fat.baixada = ''N'' ')
    else if RGFaturas.ItemIndex = 2 then
      QFatura.SQL.Add(' and fat.baixada = ''S'' ');
    if (DBEmpresa.KeyValue <> null) and (DBEmpresa.KeyValue <> 0) then
      QFatura.SQL.Add(' and ((conv.empres_id = '+DBEmpresa.KeyValue+') or (fat.id = '+DBEmpresa.KeyValue+' ))');
    if not ChkTodasDatas.Checked then
    begin
      case RGFaturas.ItemIndex of
        2 : QFatura.SQL.Add(' and fat.data_vencimento between '+FormatDataIB(DataIni.Date,True,Inicial)+' and '+FormatDataIB(DataFim.Date,True,Final));
      else
        QFatura.SQL.Add(' and fat.data_vencimento between '+FormatDataIB(DataIni.Date)+' and '+FormatDataIB(DataFim.Date));
      end;
    end;
  end;
  Screen.Cursor := crHourGlass;
  QFatura.SQL.Text;
  QFatura.Open;
  EdBaixar.Value  := 0;
  EdBaixado.Value := 0;
  if QFatura.IsEmpty then
  begin
    MFatura.EmptyTable;
    MsgInf('Nenhuma fatura encontrada');
  end
  else begin

    QFatura.First;

    MFatura.Open;
    MFatura.EmptyTable;
    MFatura.DisableControls;
    while not QFatura.Eof do begin
      MFatura.Append;
      MFaturaID.AsInteger :=QFaturaID.AsInteger;
      MFaturaDATA_FATURA.AsDateTime := QFaturaDATA_FATURA.AsDateTime;
      MFaturaHORA_FATURA.AsVariant := QFaturaHORA_FATURA.AsVariant;
      MFaturaDATA_VENCIMENTO.AsDateTime := QFaturaDATA_VENCIMENTO.AsDateTime;
      MFaturaSO_CONFIRMADAS.AsString := QFaturaSO_CONFIRMADAS.AsString;
      MFaturaOBS.AsString := QFaturaOBS.AsString;
      MFaturaFORMAPGTO_ID.AsInteger := QFaturaforma_id.AsInteger;
      MFATURABANCO_ID.AsInteger     := QFaturabanco_id.AsInteger;
      MFaturaFATURA_ID.AsInteger := QFaturaFATURA_ID.AsInteger;
      MFaturaVALOR_PAGO.AsString := QFaturavalor_pago.AsString;
      MFaturaOPERADOR.AsString := QFaturaOPERADOR.AsString;
      MFaturaVALOR.AsFloat := QFaturaVALOR.AsFloat;
      MFaturaBANCO.AsString := QFaturabanco.AsString;
      MFaturaFORMA_PGTO.AsString := QFaturaFORMA_PGTO.AsString;
      if QFaturadata_baixa.AsDateTime = 0 then
        MFaturaDATA_BAIXA.Clear
      else
      MFaturaDATA_BAIXA.AsDateTime    := QFaturaDATA_BAIXA.AsDateTime;
      MFaturaAPAGADO.AsString         := QFaturaAPAGADO.AsString;
      MFaturaBAIXADA.AsString         := QFaturaBAIXADA.AsString;
      MFaturaPRE_BAIXA.AsString       := QFaturaPRE_BAIXA.AsString;
      if QFaturadata_pre_baixa.AsDateTime = 0 then
      begin
        MFaturaDATA_PRE_BAIXA.Clear;
      end
      else
      begin
        MFaturaDATA_PRE_BAIXA.AsDateTime := QFaturaDATA_PRE_BAIXA.AsDateTime;
      end;
      MFaturaTIPO.AsString := QFaturaTIPO.AsString;
      MFaturaNOME.AsString := QFaturaNOME.AsString;
      MFaturaDESC_EMPRESA.AsFloat := QFaturaDESC_EMPRESA.AsFloat;
      MFaturaMARCADO.AsBoolean := false;
      MFatura.Post;
      QFatura.Next;
    end;
    MFatura.First;
    MFatura.EnableControls;
    fatura_sel := EmptyStr;


    SomarFaturas;
    GridFatur.SetFocus;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFFatura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Empresa.Close;
  QFatura.Close;
  FMenu.vMantFat := False;
end;

procedure TFFatura.ButFecharClick(Sender: TObject);
var porcent: Currency;
SavePlace  : TBookmark;
begin
  inherited;
  if not MFatura.IsEmpty then
  begin
    if DMConexao.ContaMarcados(MFatura) > 0 then
    begin
      if MsgSimNao('Confirma a baixa das faturas marcadas ?'+sLineBreak+sLineBreak+'Aten��o!'+sLineBreak+'As autoriza��es que compoem estas faturas tamb�m ser�o baixadas.') then
      begin

        FrmOcorrencia := TFrmOcorrencia.Create(Self);
        if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
          Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
          Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
          FreeAndNil(FrmOcorrencia);
        end;

        MFatura.First;
        while not MFatura.Eof do
        begin
          if ((MFaturaMARCADO.AsBoolean = True) and (MFaturaBAIXADA.AsString = 'N')) then
          begin
            try
              SavePlace := MFatura.GetBookmark;
              Screen.Cursor := crHourGlass;
              DMConexao.AdoCon.BeginTrans;
              DMConexao.ExecuteSql(' update fatura set baixada = ''S'' where fatura_id = '+MFaturaFATURA_ID.AsString);
              DMConexao.GravaLog('FFatura','Baixada','N','S',Operador.Nome,'Altera��o',MFaturaFATURA_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo);
              MFatura.edit;
              MFaturaBaixada.asstring := 'S';
              //MFaturaData_Baixa.AsDatetime := Now;
              MFatura.post;
              DMConexao.ExecuteSql(' update contacorrente set baixa_conveniado = ''S'' where fatura_id = '+MFaturaFATURA_ID.AsString);
              if ((MFatura.FieldByName('PRE_BAIXA').AsString <> 'S') and (MFatura.FieldByName('DESC_EMPRESA').AsCurrency > 0)) then
              begin
                porcent:= ArredondaDin((MFatura.FieldByName('DESC_EMPRESA').AsCurrency * 100)/MFatura.FieldByName('VALOR').AsCurrency);
                if MsgSimNao('Existe um desconto de '+FormatDinBR(porcent)+'% no valor de R$ '+FormatDinBR(MFatura.FieldByName('DESC_EMPRESA').AsCurrency)+sLineBreak+
                             'Deseja repassar o desconto para os estabelecimentos?'+sLineBreak+sLineBreak+
                             'Caso escolha Sim, ser� criado taxas de desconto para os estabelecimentos.',False) then
                begin
                  GeraDescontoEmpresa(MFatura.FieldByName('ID').AsInteger,
                                      MFatura.FieldByName('FECHAMENTO').AsDateTime,
                                      MFatura.FieldByName('FATURA_ID').AsInteger,
                                      MFatura.FieldbyName('NOME').AsString);
                end;
              end;
              DMConexao.AdoCon.CommitTrans;
              MFatura.Edit;
              MFaturaMarcado.AsBoolean := not MFaturaMarcado.AsBoolean;
              MFatura.Post;
              SomarFaturas;
              GridFatur.SetFocus;
              Screen.Cursor := crDefault;
            except
              on e:Exception do
              begin
                DMConexao.AdoCon.RollbackTrans;
                Screen.Cursor := crDefault;
                MsgErro('Um erro ocorreu durante a baixa, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
              end;
            end;
          end;
          MFatura.Next;
        end;
        MsgInf('Baixa das faturas e das autoriza��es concluidas com sucesso!');
      end;
      MFatura.GotoBookmark(SavePlace);
      MFatura.Next;
    end
    else
      MsgInf('Nenhuma fatura selecionada!');
  end;
end;

procedure TFFatura.BaixarFatura;
var porcent: Currency;
begin
  if not MFatura.IsEmpty then
  begin
    if DMConexao.ContaMarcados(MFatura) > 0 then
    begin
      if MsgSimNao('Confirma a baixa das faturas marcadas ?'+sLineBreak+sLineBreak+'Aten��o!'+sLineBreak+'As autoriza��es que compoem estas faturas tamb�m ser�o baixadas.') then
      begin

        FrmOcorrencia := TFrmOcorrencia.Create(Self);
        if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
          Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
          Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
          FreeAndNil(FrmOcorrencia);
        end;


        if ((MFaturaMARCADO.AsBoolean = True) and (MFaturaBAIXADA.AsString = 'N')) then
        begin
          try
            //SavePlace := MFatura.GetBookmark;
            Screen.Cursor := crHourGlass;
            DMConexao.AdoCon.BeginTrans;
            DMConexao.ExecuteSql(' update fatura set baixada = ''S'' where fatura_id = '+MFaturaFATURA_ID.AsString);
            DMConexao.GravaLog('FFatura','Baixada','N','S',Operador.Nome,'Altera��o',MFaturaFATURA_ID.AsString,Ocorrencia.Solicitante, Ocorrencia.Motivo);
            MFatura.edit;
            MFaturaBaixada.asstring := 'S';
            //MFaturaData_Baixa.AsDatetime := Now;
            MFatura.post;
            DMConexao.ExecuteSql(' update contacorrente set baixa_conveniado = ''S'' where fatura_id = '+MFaturaFATURA_ID.AsString);
            if ((MFatura.FieldByName('PRE_BAIXA').AsString <> 'S') and (MFatura.FieldByName('DESC_EMPRESA').AsCurrency > 0)) then
            begin
              porcent:= ArredondaDin((MFatura.FieldByName('DESC_EMPRESA').AsCurrency * 100)/MFatura.FieldByName('VALOR').AsCurrency);
              if MsgSimNao('Existe um desconto de '+FormatDinBR(porcent)+'% no valor de R$ '+FormatDinBR(MFatura.FieldByName('DESC_EMPRESA').AsCurrency)+sLineBreak+
                           'Deseja repassar o desconto para os estabelecimentos?'+sLineBreak+sLineBreak+
                           'Caso escolha Sim, ser� criado taxas de desconto para os estabelecimentos.',False) then
              begin
                GeraDescontoEmpresa(MFatura.FieldByName('ID').AsInteger,
                                    MFatura.FieldByName('FECHAMENTO').AsDateTime,
                                    MFatura.FieldByName('FATURA_ID').AsInteger,
                                    MFatura.FieldbyName('NOME').AsString);
              end;
            end;
            DMConexao.AdoCon.CommitTrans;
            MFatura.Edit;
            MFaturaMarcado.AsBoolean := not MFaturaMarcado.AsBoolean;
            MFatura.Post;
            SomarFaturas;
            GridFatur.SetFocus;
            Screen.Cursor := crDefault;
          except
            on e:Exception do
            begin
              DMConexao.AdoCon.RollbackTrans;
              Screen.Cursor := crDefault;
              MsgErro('Um erro ocorreu durante a baixa, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
            end;
          end;
        end;

        //MsgInf('Baixa das faturas e das autoriza��es concluidas com sucesso!');

      end;
      //MFatura.GotoBookmark(SavePlace);
      MFatura.Next;
    end
    else
      MsgInf('Nenhuma fatura selecionada!');
  end;
end;

procedure TFFatura.GridFaturDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
//begin
//  inherited;
var Grid : TDBGrid;
  L, R: Integer;
  marc: Boolean;
begin
  if not (TDBGrid(Sender).Name = 'GridBranco') then
  begin
    marc:= False;
    Grid := TDBGrid(Sender);
    if ((gdFocused in State) and (dgEditing in Grid.Options)) then
    begin
      Grid.Canvas.Font.Style:= [fsBold];
      Grid.Canvas.Font.Color:= clTeal;
    end
    else if ((Grid.DataSource.DataSet.FindField('BAIXADA') <> nil) and (Grid.DataSource.DataSet.FindField('BAIXADA').AsString = 'S')) then
    begin
      Grid.Canvas.Font.Color := clRed;
      marc:= True;
    end
    else if ((Grid.DataSource.DataSet.FindField('PRE_BAIXA') <> nil) and (Grid.DataSource.DataSet.FindField('PRE_BAIXA').AsString = 'S')) then
    begin
      Grid.Canvas.Font.Color := cl3DDkShadow;
    end
    else
      Grid.Canvas.Font.Color := clBlack;
    if ((QFaturaDATA_VENCIMENTO.AsDateTime <= Now-1) and (GridFatur.Columns[DataCol].FieldName = 'DATA_VENCIMENTO')) then
      GridFatur.Canvas.Font.Style := [fsBold];
    with THackDBgrid(Sender) do
    begin
      if DataLink.ActiveRecord = Row -1 then
      begin
        Grid.Canvas.Brush.Color:= clBlack;
        Grid.Canvas.FillRect(Rect);
        if odd(Grid.DataSource.DataSet.RecNo) then
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite
        else
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and(Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $007676EB;
          end
          else
            Canvas.Brush.Color:= $0EEEEEE;
      end
      else
      begin
        if odd(Grid.DataSource.DataSet.RecNo) then
        begin
          if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean) then
          begin
            if marc then Grid.Canvas.Font.Color := clWhite;
            Canvas.Brush.Color:= $008080FF;
          end
          else
            Canvas.Brush.Color:= clWhite;
        end
        else if (Grid.DataSource.DataSet.FindField('MARCADO') <> nil) and (Grid.DataSource.DataSet.FindField('MARCADO').AsBoolean) then
        begin
          if marc then Grid.Canvas.Font.Color := clWhite;
          Canvas.Brush.Color:= $007676EB;
        end
        else
          Canvas.Brush.Color:= $0EEEEEE;
        Grid.Canvas.FillRect(Rect);
      end;
    end;
    R:= Rect.Right; L:= Rect.Left;
    if Column.Index = 0 then L := L + 1;
    if Column.Index = Grid.Columns.Count -1 then R := R - 1;
    Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
    Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
  end;
end;

procedure TFFatura.DBEmpresaChange(Sender: TObject);
begin
  inherited;
  if EdCod.Text <> DBEmpresa.KeyValue then
    EdCod.Text := string(DBEmpresa.KeyValue);
end;

procedure TFFatura.SomarFaturas;
var RegAtual : TBookmark;
begin
  EdBaixar.Value  := 0;
  EdBaixado.Value := 0;
  MFatura.DisableControls;
  RegAtual := MFatura.GetBookmark;
  MFatura.First;
  while not MFatura.Eof do
  begin
    if MFaturaBAIXADA.AsString = 'S' then
      EdBaixado.Value := EdBaixado.Value + MFaturaVALOR.AsCurrency
    else
      EdBaixar.Value  := EdBaixar.Value + MFaturaVALOR.AsCurrency;
    MFatura.Next;
  end;
  MFatura.GotoBookmark(RegAtual);
  MFatura.FreeBookmark(RegAtual);
  MFatura.EnableControls;
end;

procedure TFFatura.ButImprimirClick(Sender: TObject);
var imp : TImpres;  linha, ItemSel : integer;
ComSemRec, jaPerguntou: Boolean;
begin
  jaPerguntou:= False;
  if not MFatura.IsEmpty then
  begin
    ItemSel := TFSelTipoImp.AbrirJanela(['Somente a Fatura','Fatura e os valores por Conveniado','Somente valores por Conveniado','Detalhado por Autoriza��es','Conveniados Por Grupo da Empresa','Duplicata Mercantil','Boleto Bancario','Lista de Faturas']);
    if ItemSel = -1 then
      Exit;
    if ((DMConexao.ContaMarcados(MFatura) > 0) or (ItemSel = 7)) then
    begin
      screen.cursor := crHourGlass;
      if ((ItemSel <> 5) and (ItemSel <> 6)) then
      begin
        imp := TImpres.Create;
        imp.quebrapagina  := True;
        imp.tituloemtodas := True;
        DMConexao.Adm.Open;
        imp.AddTitulo(sLineBreak);
        imp.AddTitulo(imp.Centraliza(DMConexao.AdmFANTASIA.AsString));
        imp.AddTitulo(imp.Centraliza(' Fatura Conv�nio '));
        DMConexao.Adm.Close;
        imp.AddTitulo('');
      end;
      if ItemSel = 7 then
        CriaRelLista(Imp)
      else
      begin
        MFatura.First;
        while not MFatura.Eof do
        begin
          if MFaturaMARCADO.AsBoolean = True then
          begin
            if ItemSel = 5 then
            begin
              DMConexao.Query2.Close;
              DMConexao.Query2.SQL.Text:= 'select * from empresas where empres_id = '+MFatura.FieldByName('ID').AsString;
              DMConexao.Query2.Open;
              DMConexao.Query1.Close;
              DMConexao.Query1.SQL.Text:= 'select * from fatura where fatura_id = '+MFatura.FieldByName('FATURA_ID').AsString;
              DMConexao.Query1.Open;
              DMConexao.Adm.Open;
              rDuplicata:= TrDuplicata.Create(self);
              screen.cursor := crDefault;
              rDuplicata.Preview;
              rDuplicata.Free;
              DMConexao.Adm.Close;
              DMConexao.Query2.Close;
              DMConexao.Query1.Close;
            end
            else if ItemSel = 6 then
            begin
              ImprimeBoleto;
            end
            else
            begin
              if imp.LinhasCount > 0 then
                imp.novapagina(True);
              if ItemSel <> 7 then
              begin
                if RGFaturas.ItemIndex in[1,2] then
                begin
                  if MFaturaDATA_BAIXA.AsString <> '' then
                  begin
                    imp.AddLinha(' Data Fatura: '+FormatDataBR(MFaturaDATA_FATURA.AsDateTime)
                      + '  -  Data da Baixa: '+FormatDataBR(MFaturaDATA_BAIXA.AsDateTime));
                  end
                  else
                    imp.AddLinha(' Data Fatura: '+FormatDataBR(MFaturaDATA_FATURA.AsDateTime));
                end
                else
                  imp.AddLinha(' Data Fatura: '+FormatDataBR(MFaturaDATA_FATURA.AsDateTime));
                imp.AddLinha(' Fatura N�  : '+MFaturaFATURA_ID.DisplayText);
                if MFaturaTIPO.AsString = 'E' then
                  imp.AddLinha(' Empresa Conveniada: '+MFaturaID.AsString+' - '+MFaturaNOME.AsString)
                else
                  imp.AddLinha(' Conveniado: '+MFaturaID.AsString+' - '+MFaturaNOME.AsString);
                imp.AddLinha(' N� Clientes Atendidos: '+ContarConveniadosPorFatura);
              end;
              if ItemSel in [0,1] then
                CriarCorpoFatura(Imp);
              if ItemSel in [1,2] then
              begin
                if not jaPerguntou then
                begin
                  ComSemRec:= MsgSimNao('Deseja Exibir Detalhado Com e Sem Receita?');
                  jaPerguntou:= True;
                end;
                if ItemSel = 1 then
                  Imp.novapagina(True);
                CriarRelTotalConv(Imp,ComSemRec,False);
              end;
              if ItemSel = 3 then
                CriarRelTotalConv(Imp,False,True);
              if ItemSel = 4 then
              begin
                if not jaPerguntou then
                begin
                  ComSemRec:= MsgSimNao('Deseja Exibir Detalhado Com e Sem Receita?');
                  jaPerguntou:= True;
                end;
                CriarRelPorGrupoEmp(Imp, ComSemRec);
              end
            end;
          end;
          MFatura.Next;
        end;
      end;
      if ((ItemSel <> 5) and (ItemSel <> 6)) then
      begin
        imp.Imprimir();
        imp.Free;
      end;
      DMConexao.Adm.Close;
      screen.cursor := crDefault;
    end
    else
      MsgInf('Nenhuma fatura selecionada!');
  end;
end;

procedure TFFatura.RelConvPorAutorizacao(Imp: TImpres);
var
  Total, desc_empr : Currency;
  Linha : Integer;
begin
    Imp.AddLinha('-------------------------------------------------------------------------------------------------------------------------------------');
    //Imp.AddLinha('Trans.  Autorizacao        Chapa  Nome do Titular                       Valor  Estabelecimento                           Data     Rec',3);
    Imp.AddLinha('Trans   N� Autor        Chapa  Nome do Titular                         Valor  Estabelecimento                 Data        Rec  Nota',3);
    Imp.AddLinha('-------------------------------------------------------------------------------------------------------------------------------------');
    QDadosConv.First;
    while not QDadosConv.Eof do
    begin
      Linha := Imp.AddLinha(Imp.Direita(QDadosConv.FieldByName('TRANS_ID').AsString,6));
      Imp.AddLinha(Imp.Direita(QDadosConv.FieldByName('AUTORIZACAO_ID').AsString+'-'+PadL(QDadosConv.FieldByName('DIGITO').AsString,2,'0'),9),11,Linha);
      Imp.AddLinha(Imp.Direita(QDadosConv.FieldByName('CHAPA').AsString,10),22,Linha);
      Imp.AddLinha(copy(QDadosConv.FieldByName('TITULAR').AsString,1,33),34,Linha);
      Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.FieldByName('VALOR').AsCurrency),10),69,Linha);
      Imp.AddLinha(copy(QDadosConv.FieldByName('CRED_ID').AsString+'-'+QDadosConv.FieldByName('NOMECRED').AsString,1,30),81,Linha);
      Imp.AddLinha(FormatDataBR(QDadosConv.FieldByName('DATA').AsDateTime),113,Linha);
      Imp.AddLinha(cartao_util.iif(QDadosConv.FieldByName('RECEITA').AsString='S','SIM','NAO'),125,Linha);
      Imp.AddLinha(Imp.Direita(QDadosConv.FieldByName('NF').AsString,6,''),130,Linha);

      Total := Total + QDadosConv.FieldByName('VALOR').AsCurrency;
      QDadosConv.Next;
    end;
    Imp.AddLinha('=====================================================================================================================================');
    Imp.AddLinha('Totalizacao dos Conveniados: Valor Total: '+FormatDinBR(Total));
    if MFatura.FieldByName('DESC_EMPRESA').AsCurrency > 0 then
    begin
      //if QFaturaTIPO.AsString = 'E' then
      //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = '+QFatura.FieldByName('ID').AsString,0)
      //else
      //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+QFatura.FieldByName('ID').AsString+')',0);

      if MFaturaTIPO.AsString = 'E' then
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = '+MFaturaID.AsString);
        DMConexao.AdoQry.SQL.Text;
        DMConexao.AdoQry.Open;
        desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
      end
      else
        begin
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+MFaturaID.AsString+')');
          DMConexao.AdoQry.SQL.Text;
          DMConexao.AdoQry.Open;
          desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
        end;


      Imp.AddLinha('Desc. Empresa '+Imp.Direita(FormatDinBR(desc_empr),5)+
                   ' %: '+Imp.Direita(FormatDinBR(mFatura.FieldByName('DESC_EMPRESA').AsCurrency),10));
      Imp.AddLinha('Valor Liquido:    '+Imp.Direita(FormatDinBR(mFatura.FieldByName('VALOR').AsCurrency-mFatura.FieldByName('DESC_EMPRESA').AsCurrency),15));
    end;
end;

procedure TFFatura.CriarRelTotalConv(Imp:Timpres; SepararComSemRec:Boolean; PorAut: Boolean);
var Total, tot_cR, tot_sR, desc_empr : Currency;    Linha : Integer;
begin
  Total := 0;
  Imp.SaltarLinhas(1);
  Imp.AddLinha(Imp.Centraliza('Extrato Totalizado por Conveniados ref. a Fatura N� '+MFaturaFATURA_ID.DisplayText));
  AbrirQueryConv(MFaturaFATURA_ID.AsInteger,SepararComSemRec,PorAut);
  if PorAut then
    RelConvPorAutorizacao(Imp)
  else
  begin
    Imp.AddLinhaSeparadora();
    Linha := Imp.AddLinha('Conv ID',3);
    if not PorAut then
      Imp.AddLinha('Chapa',13,Linha)
    else
      Imp.AddLinha('Autorizacao',13,Linha);                                                 Imp.AddLinha('Nome do Titular',28,Linha);
    if SepararComSemRec then
    begin
      Imp.AddLinha('Com Receita',76,Linha);
      Imp.AddLinha('Sem Receita',90,Linha);
      Imp.AddLinha('Valor Total',104,Linha);
    end
    else
      Imp.AddLinha('Valor Total',76,Linha);
    if PorAut then
    begin
      Imp.AddLinha('Data',90,Linha);
      Imp.AddLinha('Rec',102,Linha);
      Imp.AddLinha('Estabelecimento',107,Linha);
    end;
    Imp.AddLinhaSeparadora();
    QDadosConv.First;
    while not QDadosConv.Eof do
    begin
      Linha := Imp.AddLinha(Imp.Direita(QDadosConv.Fields[0].AsString,7));
      Imp.AddLinha(imp.Direita(QDadosConv.Fields[1].AsString,12,'0'),13,Linha);
      Imp.AddLinha(QDadosConv.Fields[2].AsString,28,Linha);
      if SepararComSemRec then
      begin
        Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[4].AsCurrency),11),76,Linha);
        Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[5].AsCurrency),11),90,Linha);
        Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[3].AsCurrency),11),104,Linha);
        tot_cR:= tot_cR + QDadosConv.Fields[4].AsCurrency;
        tot_sR:= tot_sR + QDadosConv.Fields[5].AsCurrency;
      end
      else
        Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[3].AsCurrency),11),76,Linha);
      if PorAut then
      begin
        Imp.AddLinha(FormatDataBR(QDadosConv.Fields[4].AsDateTime),90,Linha);
        if QDadosConv.Fields[5].AsString = 'S' then
          Imp.AddLinha('Sim',102,Linha)
        else
          Imp.AddLinha('Nao',102,Linha);
        Imp.AddLinha(copy(QDadosConv.Fields[6].AsString,1,23),107,Linha);
      end;
      Total := Total + QDadosConv.Fields[3].AsCurrency;
      QDadosConv.Next;
    end;
    Imp.AddLinhaSeparadora('=');
    if SepararComSemRec then
      Imp.AddLinha('Totalizacao dos Conveniados: Valor com Receita: '+ FormatDinBR(tot_cR) + '  Valor sem Receita: ' + FormatDinBR(tot_sR) + '  Valor Total: ' + FormatDinBR(Total))
    else
      Imp.AddLinha('Totalizacao dos Conveniados: Valor Total: '+FormatDinBR(Total));
    if MFaturaDESC_EMPRESA.AsCurrency > 0 then
    begin
      //if MFaturaTIPO.AsString = 'E' then
      //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = '+QFaturaID.AsString,0)
      //else
      //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+QFaturaID.AsString+')',0);

      if MFaturaTIPO.AsString = 'E' then
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = '+MFaturaID.AsString);
        DMConexao.AdoQry.SQL.Text;
        DMConexao.AdoQry.Open;
        desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
      end
      else
        begin
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+MFaturaID.AsString+')');
          DMConexao.AdoQry.SQL.Text;
          DMConexao.AdoQry.Open;
          desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
        end;

      Imp.AddLinha('Desc. Empresa '+Imp.Direita(FormatDinBR(desc_empr),5)+
                   ' %: '+Imp.Direita(FormatDinBR(MFaturaDESC_EMPRESA.AsCurrency),10));
      Imp.AddLinha('Valor Liquido:    '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency-MFaturaDESC_EMPRESA.AsCurrency),15));
    end;
  end;
  QDadosConv.Close;
end;

procedure TFFatura.CriarRelPorGrupoEmp(Imp:Timpres; SepararComSemRec: Boolean);
var gru_tt, gru_cR, gru_sR, total, tot_cR, tot_sR, desc_empr : Currency;
  Linha : Integer;
  i, gr: Integer;
begin
  gr:= 0;
  gru_tt:= 0;  gru_cR:= 0;  gru_sR:= 0;
  total:= 0;  tot_cR:= 0;  tot_sR:= 0;
  Imp.SaltarLinhas(1);
  Imp.AddLinha(Imp.Centraliza('Extrato Totalizado por Conveniados por Grupo ref. a Fatura N� '+QFaturaFATURA_ID.DisplayText));
  AbrirQueryConv(QFaturaFATURA_ID.AsInteger,True,False,True);
  QDadosConv.First;
  while not QDadosConv.Eof do
  begin
    if ((gr = 0) or (gr <> QDadosConv.Fields[6].AsInteger)) then
    begin
      gr:= QDadosConv.Fields[6].AsInteger;
      Imp.SaltarLinhas(1);
      Imp.AddLinhaSeparadora();
      Imp.AddLinha('Grupo: '+QDadosConv.Fields[7].AsString,3);
      Imp.AddLinhaSeparadora();
      if SepararComSemRec then
        Imp.AddLinha('Conv ID   Chapa          Nome do Titular                                 Com Receita   Sem Receita   Valor Total',5)
      else
        Imp.AddLinha('Conv ID   Chapa          Nome do Titular                                 Valor Total',5);
    end;
    Linha := Imp.AddLinha(Imp.Direita(QDadosConv.Fields[0].AsString,7));
    Imp.AddLinha(imp.Direita(QDadosConv.Fields[1].AsString,12,'0'),15,Linha);
    Imp.AddLinha(QDadosConv.Fields[2].AsString,30,Linha);
    if SepararComSemRec then
    begin
      Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[4].AsCurrency),11),78,Linha);
      Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[5].AsCurrency),11),92,Linha);
      Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[3].AsCurrency),11),106,Linha);
    end
    else
    begin
      Imp.AddLinha(Imp.Direita(FormatDinBR(QDadosConv.Fields[3].AsCurrency),11),78,Linha);
    end;
    gru_tt:= gru_tt + QDadosConv.Fields[3].AsCurrency;
    gru_cR:= gru_cR + QDadosConv.Fields[4].AsCurrency;
    gru_sR:= gru_sR + QDadosConv.Fields[5].AsCurrency;
    total:= total + QDadosConv.Fields[3].AsCurrency;
    tot_cR:= tot_cR + QDadosConv.Fields[4].AsCurrency;
    tot_sR:= tot_sR + QDadosConv.Fields[5].AsCurrency;
    QDadosConv.Next;
    if ((QDadosConv.Eof) or (gr <> QDadosConv.Fields[6].AsInteger)) then
    begin
      if SepararComSemRec then
      begin
        Imp.AddLinha('---------------------------------------',78);
        Linha:= Imp.AddLinha(Imp.Direita(FormatDinBR(gru_cR),11),78);
        Imp.AddLinha(Imp.Direita(FormatDinBR(gru_sR),11),92,Linha);
        Imp.AddLinha(Imp.Direita(FormatDinBR(gru_tt),11),106,Linha);
      end
      else
      begin
        Imp.AddLinha('-----------',78);
        Linha:= Imp.AddLinha(Imp.Direita(FormatDinBR(gru_tt),11),78);
      end;
      gru_tt:= 0; gru_cR:= 0; gru_sR:= 0;
    end;
  end;
  Imp.AddLinhaSeparadora('=');
  if SepararComSemRec then
  begin
    Linha:= Imp.AddLinha(Imp.Direita(FormatDinBR(tot_cR),11),78);
    Imp.AddLinha(Imp.Direita(FormatDinBR(tot_sR),11),92,Linha);
    Imp.AddLinha(Imp.Direita(FormatDinBR(total),11),106,Linha);
  end
  else
  begin
    Linha:= Imp.AddLinha(Imp.Direita(FormatDinBR(total),11),78);
  end;
  if MFaturaDESC_EMPRESA.AsCurrency > 0 then
  begin
    //if QFaturaTIPO.AsString = 'E' then
    //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = '+QFaturaID.AsString,0)
    //else
    //  desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+QFaturaID.AsString+')',0);
    if MFaturaTIPO.AsString = 'E' then
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = '+MFaturaID.AsString);
        DMConexao.AdoQry.SQL.Text;
        DMConexao.AdoQry.Open;
        desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
      end
      else
        begin
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+MFaturaID.AsString+')');
          DMConexao.AdoQry.SQL.Text;
          DMConexao.AdoQry.Open;
          desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
        end;

    Imp.AddLinha('Desc. Empresa '+Imp.Direita(FormatDinBR(desc_empr),5)+
                 ' %: '+Imp.Direita(FormatDinBR(MFaturaDESC_EMPRESA.AsCurrency),10));
    Imp.AddLinha('Valor Liquido:    '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency-MFaturaDESC_EMPRESA.AsCurrency),15));
  end;
  QDadosConv.Close;
end;

procedure TFFatura.CriarCorpoFatura(Imp:TImpres);
var Linha : Integer;
  desc_empr: Currency;
begin
  DMConexao.Adm.Open;
  imp.AddLinha(sLineBreak);
  imp.AddLinha(' Caso ocorra algum problema em rela��o a fatura ou disquete fornecido pela: '+DMConexao.AdmFANTASIA.AsString+',');
  imp.AddLinha(' favor entrar em contato com a central de conv�nios e apresentar o n�mero da fatura indicado acima para esclarecimentos. ');
  imp.AddLinha(sLineBreak);
  imp.AddLinha(' Apresentamos a lista de estabelecimentos participantes da fatura e suas respectivas vendas, com um total de '+ContarAturizacoesPorFatura+' cupons. ');
  imp.AddLinha(imp.Preenche());
  imp.AddLinha('                           Estabelecimento                                   N� Cupons                Valor ');
  imp.AddLinha(imp.Preenche());
  ImprimirValoresFornecedores(imp);
  imp.AddLinha(imp.Preenche());
  Imp.AddLinha('');
  //Imp.AddLinha('Total � Pagar: '+Imp.Direita(FormatDinBR(QFaturaVALOR.AsCurrency),15));
  //Imp.AddLinha('Valor por extenso: '+extenso(QFaturaVALOR.AsCurrency));
  if MFaturaDESC_EMPRESA.AsCurrency > 0 then
  begin
    if MFaturaTIPO.AsString = 'E' then
    //desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = '+QFaturaID.AsString,0)
    begin
      DMConexao.AdoQry.Close;
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = '+MFaturaID.AsString);
      DMConexao.AdoQry.SQL.Text;
      DMConexao.AdoQry.Open;
      desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
    end
    else
      begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+MFaturaID.AsString+')');
        DMConexao.AdoQry.SQL.Text;
        DMConexao.AdoQry.Open;
        desc_empr := DMConexao.AdoQry.Fields[0].AsCurrency;
      end;
      //desc_empr:= DMConexao.ExecuteScalar('select DESCONTO_EMP from empresas where empres_id = (select empres_id from conveniados where conv_id = '+QFaturaID.AsString+')',0);



    Imp.AddLinha('Total � Pagar:    '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency),15));
    Imp.AddLinha('Desc. Empresa '+Imp.Direita(FormatDinBR(desc_empr),5)+
                 ' %: '+Imp.Direita(FormatDinBR(MFaturaDESC_EMPRESA.AsCurrency),10));
    Imp.AddLinha('Valor Liquido:    '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency-MFaturaDESC_EMPRESA.AsCurrency),15));
    Imp.AddLinha('Valor por extenso: '+extenso(MFaturaVALOR.AsCurrency-MFaturaDESC_EMPRESA.AsCurrency));
  end
  else
  begin
    Imp.AddLinha('Total � Pagar: '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency),15));
    Imp.AddLinha('Valor por extenso: '+extenso(MFaturaVALOR.AsCurrency));
  end;
  imp.AddLinha(imp.Preenche());
  Imp.AddLinha('');
  if MFaturaOBS.AsString <> '' then
  begin
    Imp.AddLinha('Observacao: '+MFaturaOBS.AsString);
    imp.AddLinha(imp.Preenche());
    Imp.AddLinha('');
  end;
  DMConexao.Query1.Close;
  DMConexao.Query1.SQL.Clear;
  DMConexao.Query1.SQL.Add(' Select endereco, bairro, cidade, cep, estado ');
  if MFaturaTIPO.AsString = 'E' then
    DMConexao.Query1.SQL.Add(' from empresas where empres_id = '+MFaturaID.AsString)
  else
    DMConexao.Query1.SQL.Add(' from conveniados where conv_id = '+MFaturaID.AsString);
  DMConexao.Query1.Open;
  linha := imp.AddLinha(DMConexao.AdmFANTASIA.AsString);
  imp.AddLinha(MFaturaNOME.AsString,70,linha);
  linha := imp.AddLinha(DMConexao.AdmENDERECO.AsString);
  imp.AddLinha(DMConexao.Query1.Fields[0].AsString,70,linha);
  linha := imp.AddLinha(DMConexao.AdmBAIRRO.AsString);
  imp.AddLinha(DMConexao.Query1.Fields[1].AsString,70,linha);
  linha := imp.AddLinha(DMConexao.AdmCEP.AsString+' - '+DMConexao.AdmCIDADE.AsString+' '+DMConexao.AdmUF.AsString);
  imp.AddLinha(DMConexao.Query1.Fields[3].AsString+' - '+DMConexao.Query1.Fields[2].AsString+' '+DMConexao.Query1.Fields[4].AsString,70,linha);
  DMConexao.Adm.Close;
  DMConexao.Query1.Close;
end;

function TFFatura.ContarConveniadosPorFatura:String;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('Select count(distinct conv_id) from contacorrente where fatura_id = '+MFaturaFATURA_ID.AsString);
  DMConexao.AdoQry.SQL.Text;
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].AsString;
  //Result := DMConexao.ExecuteScalar('Select count(distinct conv_id) from contacorrente where fatura_id = '+QFaturaFATURA_ID.AsString);
end;

function TFFatura.ContarAturizacoesPorFatura:String;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('Select count(*) from contacorrente where fatura_id = '+MFaturaFATURA_ID.AsString);
  DMConexao.AdoQry.SQL.Text;
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].AsString;
  //Result := DMConexao.ExecuteScalar('Select count(*) from contacorrente where fatura_id = '+QFaturaFATURA_ID.AsString);
end;

procedure TFFatura.ImprimirValoresFornecedores(Imp:TImpres);
var Q : TADOQuery;  L : Integer;
begin
  Q := TADOQuery.Create(Self);
  Q.Connection := DMConexao.AdoCon;
  Q.SQL.Add(' Select cred.cred_id, cred.fantasia, sum(cc.debito-cc.credito), ');
  Q.SQL.Add(' count(cc.autorizacao_id) from contacorrente cc join credenciados cred on cred.cred_id = cc.cred_id ');
  Q.SQL.Add(' where cc.fatura_id = '+MFaturaFATURA_ID.AsString );
  Q.SQL.Add(' group by cred.cred_id, cred.fantasia order by cred.fantasia ');
  q.Open;
  q.First;
  while not q.Eof do
  begin
    l := Imp.AddLinha(Q.Fields[1].AsString,30);
    Imp.AddLinha(Imp.Direita(Q.Fields[3].AsString,9),80,l);
    Imp.AddLinha(Imp.Direita(FormatDinBR(Q.Fields[2].AsCurrency),10),100,l);
    q.Next;
  end;
  q.Free;
end;

procedure TFFatura.DataIniExit(Sender: TObject);
begin
  if (DataIni.Date > DataFim.Date) then
    DataFim.Date := DataIni.Date;
end;

procedure TFFatura.GridFaturTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
   try
    if Pos(Field.FieldName,QFatura.Sort) > 0 then begin
       if Pos(' DESC',QFatura.Sort) > 0 then QFatura.Sort := Field.FieldName
                                                  else QFatura.Sort := Field.FieldName+' DESC';
    end
    else QFatura.Sort := Field.FieldName;
    except
   end;

   QFatura.First;

   MFatura.Open;
   MFatura.EmptyTable;
   MFatura.DisableControls;
   while not QFatura.Eof do begin
      MFatura.Append;
      MFaturaID.AsInteger :=QFaturaID.AsInteger;
      MFaturaDATA_FATURA.AsDateTime := QFaturaDATA_FATURA.AsDateTime;
      MFaturaHORA_FATURA.AsVariant := QFaturaHORA_FATURA.AsVariant;
      MFaturaDATA_VENCIMENTO.AsDateTime := QFaturaDATA_VENCIMENTO.AsDateTime;
      MFaturaSO_CONFIRMADAS.AsString := QFaturaSO_CONFIRMADAS.AsString;
      MFaturaOBS.AsString := QFaturaOBS.AsString;
      MFaturaFATURA_ID.AsInteger := QFaturaFATURA_ID.AsInteger;
      MFaturaOPERADOR.AsString := QFaturaOPERADOR.AsString;
      MFaturaVALOR.AsFloat := QFaturaVALOR.AsFloat;
      MFaturaDATA_BAIXA.AsDateTime := QFaturaDATA_BAIXA.AsDateTime;
      MFaturaAPAGADO.AsString := QFaturaAPAGADO.AsString;
      MFaturaBAIXADA.AsString := QFaturaBAIXADA.AsString;
      MFaturaPRE_BAIXA.AsString := QFaturaPRE_BAIXA.AsString;
      MFaturaDATA_PRE_BAIXA.AsDateTime := QFaturaDATA_PRE_BAIXA.AsDateTime;
      MFaturaTIPO.AsString := QFaturaTIPO.AsString;
      MFaturaNOME.AsString := QFaturaNOME.AsString;
      MFaturaDESC_EMPRESA.AsFloat := QFaturaDESC_EMPRESA.AsFloat;
      MFaturaMARCADO.AsBoolean := false;
      MFatura.Post;
      QFatura.Next;
   end;
   MFatura.First;
   MFatura.EnableControls;
   fatura_sel := EmptyStr;

end;


procedure TFFatura.rdgTipoClick(Sender: TObject);
begin
  inherited;
  if RGFaturas.ItemIndex = 2 then
  begin
    GroupBox1.Caption := 'Faturas baixadas em';
  end
  else
  begin
    GroupBox1.Caption := 'Data da Fatura';
  end;
  QFatura.Close;
end;

procedure TFFatura.ChkTodasDatasClick(Sender: TObject);
begin
  inherited;
  DataIni.Enabled := not ChkTodasDatas.Checked;
  DataFim.Enabled := not ChkTodasDatas.Checked;
end;


procedure TFFatura.HabilitarBotoes;
begin
  ButFechar.Enabled      := MFaturaBAIXADA.AsString = 'N';
  btnInsObs.Enabled    := not MFatura.IsEmpty;
  btnAlterarVenc.Enabled    := not MFatura.IsEmpty;
  btnRFatura.Enabled   := MFaturaBAIXADA.AsString = 'S';
  btnCanReab.Enabled    := not MFatura.IsEmpty;
  //ButReabrir.Enabled   := QFaturaBAIXADA.AsString = 'S';
  //ReabreFatura.Enabled   := QFaturaBAIXADA.AsString = 'S';
  //ButCancelar.Enabled  := QFaturaBAIXADA.AsString = 'N';
  //CancelaFatura.Enabled  := ((QFaturaBAIXADA.AsString = 'N') and (QFaturaPRE_BAIXA.AsString = 'N'));
  ButImprimir.Enabled    := false;
  //AplicaPreBaixa.Enabled := QFaturaPRE_BAIXA.AsString = 'N';
  //CancelaPreBaixa.Enabled  := QFaturaPRE_BAIXA.AsString = 'S';
  //ImprimeBoletoWeb.Enabled := QFaturaBAIXADA.AsString = 'N';
end;

procedure TFFatura.QFaturaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  HabilitarBotoes;
end;


function TFFatura.AbrirQueryConv(Fatura_ID:Integer;SepararComSemRec:Boolean;PorAut:Boolean; PorGrupo:Boolean=False):Boolean;
begin
  if PorAut then
  begin
    QDadosConv.Close;
    QDadosConv.SQL.Clear;
    QDadosConv.SQL.Add(' select ');
    QDadosConv.SQL.Add('   conv.conv_id, ');
    QDadosConv.SQL.Add('   conv.chapa, ');
    QDadosConv.SQL.Add('   conv.titular, ');
    QDadosConv.SQL.Add('   conv.grupo_conv_emp, ');
    QDadosConv.SQL.Add('   gr.descricao, ');
    if PorAut then
    begin
      QDadosConv.SQL.Add('   (cc.debito-cc.credito), ');
      QDadosConv.SQL.Add(cartao_util.iif(SepararComSemRec,'   case when cc.receita = ''S'' then (cc.debito-cc.credito) else 0 end','   (cc.debito-cc.credito)')+' as valor_rec, ');
      QDadosConv.SQL.Add(cartao_util.iif(SepararComSemRec,'   case when cc.receita = ''N'' then (cc.debito-cc.credito) else 0 end','   (cc.debito-cc.credito)')+' as valor_sem ');
      QDadosConv.SQL.Add('   ,cc.autorizacao_id, ');
      QDadosConv.SQL.Add('   cc.digito, ');
      QDadosConv.SQL.Add('   cc.trans_id, ');
      QDadosConv.SQL.Add('   cc.data, ');
      QDadosConv.SQL.Add('   cc.receita, ');
      QDadosConv.SQL.Add('   cr.nome as nomecred, ');
      QDadosConv.SQL.Add('   cr.cred_id, ');
      QDadosConv.SQL.Add('   cc.nf ');
    end
    else
    begin
      QDadosConv.SQL.Add('   sum(cc.debito-cc.credito), ');
      QDadosConv.SQL.Add(cartao_util.iif(SepararComSemRec,'   sum(case when cc.receita = ''S'' then (cc.debito-cc.credito) else 0 end)','   sum(cc.debito-cc.credito)')+' as valor_rec, ');
      QDadosConv.SQL.Add(cartao_util.iif(SepararComSemRec,'   sum(case when cc.receita = ''N'' then (cc.debito-cc.credito) else 0 end)','   sum(cc.debito-cc.credito)')+' as valor_sem ');
    end;
    QDadosConv.SQL.Add(' from conveniados conv ');
    QDadosConv.SQL.Add(' join contacorrente cc on cc.conv_id = conv.conv_id ');
    QDadosConv.SQL.Add(' join credenciados cr on cr.cred_id = cc.cred_id ');
    QDadosConv.SQL.Add(' left join grupo_conv_emp gr on conv.grupo_conv_emp = gr.grupo_conv_emp_id ');
    QDadosConv.SQL.Add(' where cc.fatura_id = '+IntToStr(Fatura_ID) );
    if not PorAut then
    begin
      if SepararComSemRec then
        QDadosConv.SQL.Add(' group by conv.conv_id, conv.chapa, conv.titular, conv.grupo_conv_emp, gr.descricao, cc.receita ')
      else
        QDadosConv.SQL.Add(' group by conv.conv_id, conv.chapa, conv.titular, conv.grupo_conv_emp, gr.descricao ');
    end;
    if PorGrupo then
      QDadosConv.SQL.Add(' order by gr.descricao, conv.titular, cc.data, cc.autorizacao_id ')
    else
      QDadosConv.SQL.Add(' order by conv.titular, cc.data, cc.autorizacao_id ');
  end
  else
  begin
    if not PorAut then
    begin
      QDadosConv.SQL.Clear;
      QDadosConv.SQL.Add(' Select conv.conv_id, conv.chapa, conv.titular, sum(debito-credito) ');
      if SepararComSemRec then
      begin
        QDadosConv.SQL.Add(' , cc.receita,sum(case when cc.receita = ''S'' then (cc.debito-cc.credito) else 0 end) valor_rec, ');
        QDadosConv.SQL.Add(', cc.receita sum(case when cc.receita = ''N'' then (cc.debito-cc.credito) else 0 end) valor_sem ');
      end
      else
          QDadosConv.SQL.Add(' , cc.receita ,0.0 valor_rec, 0.0 valor_sem');
      //end.
      if PorGrupo then
        QDadosConv.SQL.Add(' ,conv.grupo_conv_emp, gr.descricao, cc.receita ');
      QDadosConv.SQL.Add(' from conveniados conv ');
      QDadosConv.SQL.Add(' join contacorrente cc on cc.conv_id = conv.conv_id ');
      if PorGrupo then
      begin
        QDadosConv.SQL.Add(' join grupo_conv_emp gr on conv.grupo_conv_emp = gr.grupo_conv_emp_id ');
        QDadosConv.SQL.Add(' where cc.fatura_id = '+inttostr(Fatura_ID));
        QDadosConv.SQL.Add(' group by conv.grupo_conv_emp, gr.descricao, conv.conv_id, conv.chapa, conv.titular, cc.receita ');
        QDadosConv.SQL.Add(' order by gr.descricao, conv.titular ');
      end
      else
      begin
        QDadosConv.SQL.Add(' where cc.fatura_id = '+inttostr(Fatura_ID));
        QDadosConv.SQL.Add(' group by conv.conv_id, conv.chapa, conv.titular, cc.receita ');
        QDadosConv.SQL.Add(' order by conv.titular ');
      end;
    end
    else
    begin
      QDadosConv.SQL.Add(' Select conv.conv_id, cc.autorizacao_id, conv.titular ,(debito-credito), ');
      QDadosConv.SQL.Add(' cc.data, cc.receita, cr.nome, cr.cred_id, cc.trans_id, cc.digito ');
      QDadosConv.SQL.Add(' from conveniados conv ');
      QDadosConv.SQL.Add(' join contacorrente cc on cc.conv_id = conv.conv_id ');
      QDadosConv.SQL.Add(' join credenciados cr on cr.cred_id = cc.cred_id ');
      QDadosConv.SQL.Add(' where cc.fatura_id = '+inttostr(Fatura_ID)+' order by conv.titular, ');
      QDadosConv.SQL.Add(' cc.autorizacao_id, cc.digito, cr.cred_id, cc.trans_id ');
    end;
  end;
  QDadosConv.Open;
  Result := not QDadosConv.IsEmpty;
end;

procedure TFFatura.ImprimeBoleto;
var banco: integer;
  LayOut : TStringList;
begin
  FSelecionaBanco := TFSelecionaBanco.create(self);
  FSelecionaBanco.ShowModal;
  if FSelecionaBanco.ModalResult = mrOk then
  begin
    banco:= FSelecionaBanco.CBBanco.KeyValue;
    LayOut := TStringList.Create;
    LayOut.Text :=  FSelecionaBanco.QBanco.FieldValues['LAYOUT'];
    LayOut := CriarBoleto(LayOut);
    ImprimirBoleto(LayOut);
    LayOut.Free;
  end;
  FSelecionaBanco.Free;
end;

function TFFatura.ImprimirBoleto(Boleto:TStringList):Boolean;
var ini : TIniFile; porta : string ;
    Impres : TextFile; i : integer;
begin
    ini := TIniFile.Create(ExtractFilePath(Application.ExeName)+'admcartao.ini');
    porta := ini.ReadString('local','path_imp','Lpt1');
    ini.Free;
    try
       AssignFile(Impres,porta);
       Rewrite(Impres);
    except
       on e:Exception do begin
          MsgErro('Erro ao abrir a impressora na porta '+porta+sLineBreak+'Erro: '+e.Message);
       Exit;
       end;
    end;

    Write(Impres,chr(27)+chr(15)+chr(27)+chr(67)+chr(0)+chr(4)+chr(27)+CHR(48));
    for i := 0 to Boleto.Count-1 do begin
       Writeln(Impres,boleto[i]);
    end;
    Write(Impres,chr(18));
    Write(Impres,chr(12)+chr(27)+chr(67)+chr(0)+chr(11));
    CloseFile(Impres);

end;

function TFFatura.CriarBoleto(LayOUt:TStringList):TStringList;
begin
  QDadosEmp.Close;
  QDadosEmp.SQL.Clear;
  QDadosEmp.SQL.Add('select endereco, bairro||'' ''||cep as bairro, cidade||'' - ''||estado as cidade,');
  if QFaturaTIPO.AsString = 'E' then
  begin
    QDadosEmp.SQL.Add('nome from empresas where empres_id = '+MFaturaID.AsString)
  end
  else
  begin
    QDadosEmp.SQL.Add('titular from conveniados where conv_id = '+MFaturaID.AsString);
  end;
  QDadosEmp.Open;
  Result := LayOUt;
  Result.Text := StringReplace(Result.Text,'data_doc',FormatDataBR(Date),[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'data_venc',FormatDataBR(MFaturaDATA_VENCIMENTO.AsDateTime),[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'valor',FormatDinBR(MFaturaVALOR.AsCurrency),[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'fatura',FormatFloat('000000',MFaturaFATURA_ID.AsCurrency),[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'sac_razao',QDadosEmp.Fields[3].AsString,[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'sac_end',QDadosEmp.Fields[0].AsString,[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'sac_bairro_cep',QDadosEmp.Fields[1].AsString,[rfIgnoreCase,rfReplaceAll]);
  Result.Text := StringReplace(Result.Text,'sac_cidade_uf',QDadosEmp.Fields[2].AsString,[rfIgnoreCase,rfReplaceAll]);
  QDadosEmp.Close;
end;

procedure TFFatura.ImprimeBoletoCobreBem;
begin
  FImpCobreBem := TFImpCobreBem.create(self);
  FImpCobreBem.AbreFatura(MFaturaFATURA_ID.AsInteger);
  FImpCobreBem.ShowModal;
  FImpCobreBem.Free;
end;

procedure TFFatura.GridFaturDblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm.Click;
  if MFaturaMARCADO.AsBoolean = True then
    AplicaPreBaixa.Click;
end;

procedure TFFatura.ButhistClick(Sender: TObject);
var cadastro, field : string;
begin
  inherited;
  if QFatura.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('dd/mm/yyyy 00:00:00',DataIni2.Date))+' and '+QuotedStr(FormatDateTime('dd/mm/yyyy 23:59:59',DataFim2.Date)));
  QHistorico.Sql.Add(' and ID = '+MFatura.FieldByName('FATURA_ID').AsString);
  QHistorico.Sql.Add(' and JANELA = '+QuotedStr('FFatura'));
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFFatura.TabSheet2Show(Sender: TObject);
begin
  inherited;
  DataIni2.SetFocus;
  QHistorico.Close;
end;

procedure TFFatura.AlterandoDataVenc;
var newDate: TDateTime;
  newObs: string;
begin
  FAlteraDataVenc := TFAlteraDataVenc.Create(self);
  FAlteraDataVenc.LabFatura.Caption := MFaturaFATURA_ID.AsString;
  FAlteraDataVenc.LabNome.Caption := MFaturaNOME.AsString;
  FAlteraDataVenc.LabData.Caption  := FormatDataBR(MFaturaDATA_FATURA.AsDateTime);
  FAlteraDataVenc.LabValor.Caption := FormatDinBR(MFaturaVALOR.AsCurrency);
  FAlteraDataVenc.data.Date:= MFaturaDATA_VENCIMENTO.AsDateTime;
  FAlteraDataVenc.dtvenc:= MFaturaDATA_VENCIMENTO.AsDateTime;
  FAlteraDataVenc.obs:= MFaturaOBS.AsString;
  FAlteraDataVenc.edObs.Text:= MFaturaOBS.AsString;
  FAlteraDataVenc.ShowModal;
  if FAlteraDataVenc.ModalResult = mrOk then
  begin
    newDate:= FAlteraDataVenc.data.Date;
    newObs:= FAlteraDataVenc.edObs.Text;
    MFatura.Edit;
    MFaturaDATA_VENCIMENTO.AsDateTime:= newDate;
    MFaturaOBS.AsString:= newObs;
    MFatura.Post;
  end;
  FAlteraDataVenc.Free;
end;

procedure TFFatura.InserirObservao1Click(Sender: TObject);
begin
  inherited;
  if not (QFatura.IsEmpty) and (QFatura.Active) then
    AlterandoDataVenc
end;

procedure TFFatura.CriaRelLista(Imp: TImpres);
var linha: Integer;
valor: Currency;
begin
  if not QFatura.IsEmpty then
  begin
    valor:= 0;
    Imp.AddLinha(Imp.Centraliza('Listagem de Faturas no Periodo'),3);
    Imp.SaltarLinhas(2);
    Imp.AddLinha('================================================================================================================',11);
    Imp.AddLinha('| ID      | Nome                                                         | Fechamento | Fat. ID | Valor        |',11);
    Imp.AddLinha('================================================================================================================',11);
    QFatura.First;
    while not QFatura.Eof do
    begin
      linha:= Imp.AddLinha('| '+FormatFloat('0000000',MFaturaID.AsInteger),11);
      Imp.AddLinha('| '+copy(MFaturaNOME.AsString,1,60),21,linha);
      Imp.AddLinha('| '+FormatDataBR(MFaturaFECHAMENTO.AsDateTime),84,linha);
      Imp.AddLinha('| '+FormatFloat('0000000',MFaturaFATURA_ID.AsInteger),97,linha);
      Imp.AddLinha('| '+Imp.Direita(FormatDinBR(MFaturaVALOR.AsCurrency),12)+' |',107,linha);
      valor:= valor + MFaturaVALOR.AsCurrency;
      QFatura.Next;
      if QFatura.Eof then
      begin
        Imp.AddLinha('--------------------------------------------------------------------------------------------------------------',12);
        Imp.AddLinha(Imp.Direita(FormatDinBR(valor),12),109);
      end
      else
        Imp.AddLinha('|--------------------------------------------------------------------------------------------------------------|',11);
    end;
    QFatura.First;
  end;
end;

procedure TFFatura.ReabreFaturaClick(Sender: TObject);
begin
  inherited;
  {if not QFatura.IsEmpty then
  begin
    if QFaturaBAIXADA.AsString = 'S' then
    begin
      if (Coalesce(DMConexao.ExecuteScalar('Select count(*) from pagamento_cred_det where coalesce(apagado,''N'') <> ''S'' and fatura_id = '+QFaturaFATURA_ID.AsString)) > 0) then
        MsgInf('Existem pagamentos � estabelecimentos correspondentes a esta fatura j� efetuados, reabertura n�o permitida!  ')
      else if MsgSimNao('Confirma a reabertura da fatura?') then
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('update fatura set operador = '''+Operador.Nome+''', baixada = ''N'', data_baixa = null where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('update contacorrente set baixa_conveniado = ''N'' where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.GravaLog('FFatura','Baixada','S','N',Operador.Nome,'Altera��o','Fatura',QFaturaFATURA_ID.AsString,'Fatura',Self.Name);
          DMConexao.ExecuteSql('delete from taxas_prox_pag where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.AdoCon.CommitTrans;
          QFatura.Refresh;
          Screen.Cursor := crDefault;
          MsgInf('Reabertura efetuada com sucesso!');
        except
          on E:Exception do
          begin
            Screen.Cursor := crDefault;
            DMConexao.AdoCon.RollbackTrans;
            MsgErro('Opera��o Cancelada, erro: '+e.Message);
          end;
        end;
      end;
    end
    else
    begin
      MsgInf('Fatura em aberto!');
      ReabreFatura.Enabled:= True;
    end;
  end;   }
end;

procedure TFFatura.CancelaFaturaClick(Sender: TObject);
begin
  {inherited;
  if not QFatura.IsEmpty then
  begin
    if QFaturaBAIXADA.AsString = 'N' then
    begin
      if MsgSimNao('Confirma o Cancelamento da fatura n� '+QFaturaFATURA_ID.DisplayText+' da Empresa: '+QFaturaNOME.AsString+' ?'+sLineBreak+'Aten��o: Cancelando a Fatura, esta ser� apagada e as autoriza��es que a compoem ter�o as baixas canceladas.') then
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('update fatura set apagado = ''S'', dtapagado = current_timestamp, operador = '''+Operador.Nome+''' where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('delete from bonus where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('update contacorrente set fatura_id = 0 where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.AdoCon.CommitTrans;
          QFatura.Refresh;
          Screen.Cursor := crDefault;
          MsgInf('Cancelamento Conclu�do com sucesso!');
        except
          on E:Exception do
          begin
            Screen.Cursor := crDefault;
            DMConexao.AdoCon.RollbackTrans;
            MsgErro('Opera��o cancelada, erro: '+e.Message);
          end;
        end;
      end;
    end
    else
      MsgInf('Fatura baixada, cancelamento n�o permitido!');
  end;         }
end;

procedure TFFatura.ButMarcDesmClick(Sender: TObject);
begin
  inherited;
  if MFatura.IsEmpty then Exit;
    MFatura.Edit;
    MFaturaMarcado.AsBoolean := not MFaturaMarcado.AsBoolean;
    MFatura.Post;
    Fatura;
end;

procedure TFFatura.Fatura;
var marca : TBookmark;
begin
  fatura_sel := EmptyStr;
  marca := MFatura.GetBookmark;
  MFatura.DisableControls;
  MFatura.First;
  while not MFatura.eof do begin
    if MFaturaMarcado.AsBoolean then fatura_sel := fatura_sel + ','+MFaturaFatura_ID.AsString;
    MFatura.Next;
  end;
  MFatura.GotoBookmark(marca);
  MFatura.FreeBookmark(marca);
  if fatura_sel <> '' then fatura_sel := Copy(fatura_sel,2,Length(fatura_sel));
  MFatura.EnableControls;
end;

procedure TFFatura.ButMarcaTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  if MFatura.IsEmpty then Exit;
  MFatura.DisableControls;
  marca := MFatura.GetBookmark;
  MFatura.First;
  while not MFatura.eof do begin
    MFatura.Edit;
    MFaturaMarcado.AsBoolean := true;
    MFatura.Post;
    MFatura.Next;
  end;
  MFatura.GotoBookmark(marca);
  MFatura.FreeBookmark(marca);
  MFatura.EnableControls;
  Fatura;
end;

procedure TFFatura.ButDesmTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  if MFatura.IsEmpty then Exit;
  MFatura.DisableControls;
  marca := MFatura.GetBookmark;
  MFatura.First;
  while not MFatura.eof do begin
    MFatura.Edit;
    MFaturaMarcado.AsBoolean := false;
    MFatura.Post;
    MFatura.Next;
  end;
  MFatura.GotoBookmark(marca);
  MFatura.FreeBookmark(marca);
  MFatura.EnableControls;
  Fatura;
end;

procedure TFFatura.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     vk_f11 : ButMarcDesm.Click;
     vk_f8  : ButMarcaTodos.Click;
     vk_f9  : ButDesmTodos.Click;
  end;
end;

procedure TFFatura.AlteraraDatadeVencimento1Click(Sender: TObject);
begin
  inherited;
  if not (QFatura.IsEmpty) and (QFatura.Active) then
  AlterandoDataVenc
end;

procedure TFFatura.GeraDescontoEmpresa(Emp:Integer;DataFecha:TDateTime;Fat:integer;Nome:String);
var strSql: String;
begin
  QDescEmp.Close;
  QDescEmp.SQL.Clear;
  QDescEmp.SQL.Add(' Select cc.cred_id, cc.data_fecha_emp, sum(cc.debito-cc.credito) valor, emp.desconto_emp, ');
  QDescEmp.SQL.Add(' sum(cc.debito-cc.credito)*emp.desconto_emp/100 as desc_valor ');
  QDescEmp.SQL.Add(' from contacorrente cc ');
  QDescEmp.SQL.Add(' join conveniados conv on conv.conv_id = cc.conv_id ');
  QDescEmp.SQL.Add(' join empresas emp on conv.empres_id = emp.empres_id ');
  QDescEmp.SQL.Add(' where cc.fatura_id = '+IntToStr(Fat));
  QDescEmp.SQL.Add(' and cc.data_fecha_emp = '+FormatDataIB(DataFecha));
  QDescEmp.SQL.Add(' and emp.empres_id = '+IntToStr(Emp));
  QDescEmp.SQL.Add(' group by cc.cred_id, cc.data_fecha_emp, emp.desconto_emp ');
  QDescEmp.Open;
  if not QDescEmp.IsEmpty then
  begin
    QDescEmp.First;
    while not QDescEmp.Eof do
    begin
      if QDescEmp.FieldByName('VALOR').AsCurrency > 0 then
      begin
        strSql:= 'insert into taxas_prox_pag (TAXAS_PROX_PAG_ID,CRED_ID,DESCRICAO,VALOR,FATURA_ID,APAGADO) '+
                 'values (NEXT VALUE FOR STAXAS_PROX_PAG_ID,'+QDescEmp.FieldByName('CRED_ID').AsString+',''DESCONTO EMPRESA '+
                 FormatDinBR(QDescEmp.FieldByName('DESCONTO_EMP').AsCurrency)+'% - FAT: '+IntToStr(Fat)+' - '+copy(Nome,1,20)+
                 ''','+FormatDimIB(QDescEmp.FieldByName('DESC_VALOR').AsCurrency)+','+IntToStr(Fat)+',''N'')';
        DMConexao.ExecuteSql(strSql);
      end;
      QDescEmp.Next;
    end;
  end;
  QDescEmp.Close;
end;

procedure TFFatura.AplicaPreBaixaClick(Sender: TObject);
var porcent: Currency;
    valor_pago : String;
  RegAtual : TBookmark;

begin
  inherited;
  if (QFatura.IsEmpty) or (not QFatura.Active) then
  begin
    MsgInf('� necess�rio efetuar busca e selecionar uma empresa para realizar a pr�-baixa.');
    DataIni.SetFocus;
    Abort;
  end;

  QFatura.DisableControls;
  RegAtual := QFatura.GetBookmark;

  FLancamentosFatura  := TFLancamentosFatura.Create(Self);
  FLancamentosFatura.txtEmp.Caption := MFaturaNOME.AsString;
  FLancamentosFatura.txtValTot.Caption := MFaturaVALOR.AsString;
  FLancamentosFatura.txtData.Caption := MFaturaDATA_FATURA.AsString;
  FLancamentosFatura.ShowModal;

  if FLancamentosFatura.ModalResult = mrOk then
  begin
    if(((FLancamentosFatura.cbBanco.KeyValue <> '') and (FLancamentosFatura.cbBanco.KeyValue <> ''))) then
    begin
       MFatura.Edit;
       MFaturaBANCO.AsString            := FLancamentosFatura.cbBanco.Text;
       MFaturaFORMA_PGTO.AsString       := FLancamentosFatura.cbFormaPgto.Text;
       MFaturaOBS.AsString              := FLancamentosFatura.txtObs.Text;
       valor_pago := StringReplace(FLancamentosFatura.txtValorPago.Text,',','.',[rfReplaceAll, rfIgnoreCase]);
       MFaturaVALOR_PAGO.AsString       := valor_pago;
       MFaturaDATA_BAIXA.AsDateTime     := StrToDateTime(FLancamentosFatura.txtDataBaixaFatura.Text);
       MFaturaPRE_BAIXA.AsString        := 'S';
       MFaturaDATA_PRE_BAIXA.AsDateTime := Now;
       MFatura.Post;
       
       DMConexao.ExecuteSql('UPDATE FATURA SET BANCO_ID = '+IntToStr(FLancamentosFatura.cbBanco.KeyValue)+
       ', FORMA_ID = '+IntToStr(FLancamentosFatura.cbFormaPgto.KeyValue)+
       ', OBS = '+QuotedStr(FLancamentosFatura.txtObs.Text)             +
       ', VALOR_PAGO = '+ MFaturaVALOR_PAGO.AsString                    +
       ', PRE_BAIXA = '+QuotedStr(MFaturaPRE_BAIXA.AsString)            +
       ', DATA_BAIXA = '+QuotedStr(MFaturaDATA_BAIXA.AsString)          +
       ', DATA_PRE_BAIXA = '+QuotedStr(MFaturaDATA_PRE_BAIXA.AsString)  +
       ' WHERE FATURA_ID = '+MFaturaFATURA_ID.AsString+'');
    end;
  end;
  BaixarFatura;
end;

procedure TFFatura.CancelaPreBaixaClick(Sender: TObject);
var RegAtual : TBookmark;
begin
  {inherited;
  if not QFatura.IsEmpty then
  begin
    if QFaturaPRE_BAIXA.AsString = 'S' then
    begin
      QFatura.DisableControls;
      RegAtual := QFatura.GetBookmark;
      if (Coalesce(DMConexao.ExecuteScalar('Select count(*) from pagamento_cred_det where coalesce(apagado,''N'') <> ''S'' and fatura_id = '+QFaturaFATURA_ID.AsString)) > 0) then
        MsgInf('Existem pagamentos � estabelecimentos correspondentes a esta fatura j� efetuados, reabertura n�o permitida!  ')
      else if MsgSimNao('Confirma cancelamento da pr�-baixa?') then
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('update fatura set pre_baixa = ''N'', data_pre_baixa = null where fatura_id = '+QFaturaFATURA_ID.AsString);
          DMConexao.GravaLog('FFatura','Pr�-Baixa','S','N',Operador.Nome,'Altera��o','Fatura',QFaturaFATURA_ID.AsString,'Fatura',Self.Name);
          DMConexao.AdoCon.CommitTrans;
          QFatura.Refresh;
          Screen.Cursor := crDefault;
          MsgInf('Pr�-Baixa cancelada com sucesso!');
        except
          on E:Exception do
          begin
            Screen.Cursor := crDefault;
            DMConexao.AdoCon.RollbackTrans;
            MsgErro('Opera��o Cancelada, erro: '+e.Message);
          end;
        end;
      end;
      QFatura.GotoBookmark(RegAtual);
      QFatura.FreeBookmark(RegAtual);
      QFatura.EnableControls;
    end
    else
    begin
      MsgInf('Fatura em aberto!');
      CancelaPreBaixa.Enabled:= True;
    end;
  end;  }
end;

procedure TFFatura.edtFaturaIDKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
end;

procedure TFFatura.ImprimeBoletoWebClick(Sender: TObject);
var site: String;
begin
  inherited;
{  if not (QFatura.IsEmpty) or (QFatura.Active) then Abort;
  DMConexao.Config.Open;
  if QFaturaTIPO.AsString = 'E' then
  begin
    site := DMConexao.ConfigLINK_EMP.AsString;
    site:= site + 'empboleto.aspx?fat='+QFaturaFATURA_ID.AsString;
    site:= site + '&chave='+Crypt('E', FormatDateTime('dd/MM/yyyy hh',Now)+'WEBEMPRESAS', 'BIGCOMPRAS');
  end
  else
  begin
    site := DMConexao.ConfigLINK_USU.AsString;
    site:= site + 'userboleto.aspx?fat='+QFaturaFATURA_ID.AsString;
    site:= site + '&chave='+Crypt('E', FormatDateTime('dd/MM/yyyy hh',Now)+'WEBUSUARIOS', 'BIGCOMPRAS');
  end;
  ShellExecute(Application.Handle, nil, PChar(site), nil, nil, sw_hide);
  DMConexao.Config.Close; }
end;

procedure TFFatura.MFaturaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  HabilitarBotoes;
end;

procedure TFFatura.btnInsObsClick(Sender: TObject);
begin
  inherited;
   if not (MFatura.IsEmpty) and (MFatura.Active) then
    AlterandoDataVenc
end;

procedure TFFatura.btnAlterarVencClick(Sender: TObject);
begin
  inherited;
  if not (MFatura.IsEmpty) and (MFatura.Active) then
    AlterandoDataVenc
end;

procedure TFFatura.btnRFaturaClick(Sender: TObject);
var retorno : Integer;
begin
  inherited;
  if not MFatura.IsEmpty then
  begin
    if MFaturaBAIXADA.AsString = 'S' then
    begin
      //if (Coalesce(DMConexao.ExecuteScalar('Select count(*) from pagamento_cred_det where coalesce(apagado,''N'') <> ''S'' and fatura_id = '+QFaturaFATURA_ID.AsString)) > 0) then
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('Select count(*) from pagamento_cred_det where coalesce(apagado,''N'') <> ''S'' and fatura_id = '+MFaturaFATURA_ID.AsString);
      DMConexao.AdoQry.SQL.Text;
      DMConexao.AdoQry.Open;
      retorno := DMConexao.AdoQry.Fields[0].AsInteger;
      if retorno > 0 then
        MsgInf('Existem pagamentos � estabelecimentos correspondentes a esta fatura j� efetuados, reabertura n�o permitida!  ')
      else if MsgSimNao('Confirma a reabertura da fatura?') then
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('update fatura set operador = '''+Operador.Nome+''', baixada = ''N'', data_baixa = null where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('update contacorrente set baixa_conveniado = ''N'' where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.GravaLog('FFatura','Baixada','S','N',Operador.Nome,'Altera��o',MFaturaFATURA_ID.AsString,Self.Name);
          DMConexao.ExecuteSql('delete from taxas_prox_pag where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.AdoCon.CommitTrans;
          Screen.Cursor := crDefault;
          MsgInf('Reabertura efetuada com sucesso!');

          QFatura.Requery;
          QFatura.First;

          MFatura.Open;
          MFatura.EmptyTable;
          MFatura.DisableControls;
          while not QFatura.Eof do begin
              MFatura.Append;
              MFaturaID.AsInteger :=QFaturaID.AsInteger;
              MFaturaDATA_FATURA.AsDateTime := QFaturaDATA_FATURA.AsDateTime;
              MFaturaHORA_FATURA.AsVariant := QFaturaHORA_FATURA.AsVariant;
              MFaturaDATA_VENCIMENTO.AsDateTime := QFaturaDATA_VENCIMENTO.AsDateTime;
              MFaturaSO_CONFIRMADAS.AsString := QFaturaSO_CONFIRMADAS.AsString;
              MFaturaOBS.AsString := QFaturaOBS.AsString;
              MFaturaFATURA_ID.AsInteger := QFaturaFATURA_ID.AsInteger;
              MFaturaOPERADOR.AsString := QFaturaOPERADOR.AsString;
              MFaturaVALOR.AsFloat := QFaturaVALOR.AsFloat;
              MFaturaDATA_BAIXA.AsDateTime := QFaturaDATA_BAIXA.AsDateTime;
              MFaturaAPAGADO.AsString := QFaturaAPAGADO.AsString;
              MFaturaBAIXADA.AsString := QFaturaBAIXADA.AsString;
              MFaturaPRE_BAIXA.AsString := QFaturaPRE_BAIXA.AsString;
              MFaturaDATA_PRE_BAIXA.AsDateTime := QFaturaDATA_PRE_BAIXA.AsDateTime;
              MFaturaTIPO.AsString := QFaturaTIPO.AsString;
              MFaturaNOME.AsString := QFaturaNOME.AsString;
              MFaturaDESC_EMPRESA.AsFloat := QFaturaDESC_EMPRESA.AsFloat;
              MFaturaMARCADO.AsBoolean := false;
              MFatura.Post;
              QFatura.Next;
        end;
        MFatura.First;
        MFatura.EnableControls;
        fatura_sel := EmptyStr;


        SomarFaturas;

        except
          on E:Exception do
          begin
            Screen.Cursor := crDefault;
            DMConexao.AdoCon.RollbackTrans;
            MsgErro('Opera��o Cancelada, erro: '+e.Message);
          end;
        end;
      end;
    end
    else
    begin
      MsgInf('Fatura em aberto!');
      ReabreFatura.Enabled:= True;
    end;
  end;
end;

procedure TFFatura.btnCanReabClick(Sender: TObject);
begin
  inherited;
  if not MFatura.IsEmpty then
  begin
    if MFaturaBAIXADA.AsString = 'N' then
    begin
      if MsgSimNao('Confirma o Cancelamento da fatura n� '+MFaturaFATURA_ID.DisplayText+' da Empresa: '+MFaturaNOME.AsString+' ?'+sLineBreak+'Aten��o: Cancelando a Fatura, esta ser� apagada e as autoriza��es que a compoem ter�o as baixas canceladas.') then
      begin
        try
          Screen.Cursor := crHourGlass;
          DMConexao.AdoCon.BeginTrans;
          DMConexao.ExecuteSql('update fatura set apagado = ''S'', dtapagado = current_timestamp, operador = '''+Operador.Nome+''' where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('delete from bonus where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.ExecuteSql('update contacorrente set fatura_id = 0 where fatura_id = '+MFaturaFATURA_ID.AsString);
          DMConexao.AdoCon.CommitTrans;
          Screen.Cursor := crDefault;
          MsgInf('Cancelamento Conclu�do com sucesso!');

          QFatura.Requery;
          QFatura.First;
    
          MFatura.Open;
          MFatura.EmptyTable;
          MFatura.DisableControls;
          while not QFatura.Eof do begin
              MFatura.Append;
              MFaturaID.AsInteger :=QFaturaID.AsInteger;
              MFaturaDATA_FATURA.AsDateTime := QFaturaDATA_FATURA.AsDateTime;
              MFaturaHORA_FATURA.AsVariant := QFaturaHORA_FATURA.AsVariant;
              MFaturaDATA_VENCIMENTO.AsDateTime := QFaturaDATA_VENCIMENTO.AsDateTime;
              MFaturaSO_CONFIRMADAS.AsString := QFaturaSO_CONFIRMADAS.AsString;
              MFaturaOBS.AsString := QFaturaOBS.AsString;
              MFaturaFATURA_ID.AsInteger := QFaturaFATURA_ID.AsInteger;
              MFaturaOPERADOR.AsString := QFaturaOPERADOR.AsString;
              MFaturaVALOR.AsFloat := QFaturaVALOR.AsFloat;
              MFaturaDATA_BAIXA.AsDateTime := QFaturaDATA_BAIXA.AsDateTime;
              MFaturaAPAGADO.AsString := QFaturaAPAGADO.AsString;
              MFaturaBAIXADA.AsString := QFaturaBAIXADA.AsString;
              MFaturaPRE_BAIXA.AsString := QFaturaPRE_BAIXA.AsString;
              MFaturaDATA_PRE_BAIXA.AsDateTime := QFaturaDATA_PRE_BAIXA.AsDateTime;
              MFaturaTIPO.AsString := QFaturaTIPO.AsString;
              MFaturaNOME.AsString := QFaturaNOME.AsString;
              MFaturaDESC_EMPRESA.AsFloat := QFaturaDESC_EMPRESA.AsFloat;
              MFaturaMARCADO.AsBoolean := false;
              MFatura.Post;
              QFatura.Next;
        end;
        MFatura.First;
        MFatura.EnableControls;
        fatura_sel := EmptyStr;


        SomarFaturas;
        except
          on E:Exception do
          begin
            Screen.Cursor := crDefault;
            DMConexao.AdoCon.RollbackTrans;
            MsgErro('Opera��o cancelada, erro: '+e.Message);
          end;
        end;
      end;
    end
    else
      MsgInf('Fatura baixada, cancelamento n�o permitido!');
  end;

end;

end.
