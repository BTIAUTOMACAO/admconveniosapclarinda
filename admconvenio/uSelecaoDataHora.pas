unit uSelecaoDataHora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, Buttons, ExtCtrls,
  ComCtrls, uDatas;


{type
  TDiaSemana = (Domingo, Segunda, Terca, Quarta, Quinta, Sexta, Sabado, Hoje);
  TSelecaoSemana = (ssSeguinte, ssAtual, ssTudo);}
type
  TfrmSelecaoDataHora = class(TForm)
    Label2: TLabel;
    calendario: TMonthCalendar;
    Panel1: TPanel;
    btnCancelar: TBitBtn;
    btnConfirmar: TBitBtn;
  private
    maiorHoje, menorHoje : Boolean;
    diaSemanaPadrao : TDiaSemana;
    { Private declarations }
  public
    //semana seguinte: True para deixar o "diaSemanaPadrao" quando diferente de
    //"diasHoje" 7 dias depois da data marcada no diaSemanaPadrao
    procedure ExibirMsgDataHora(diaSemanaPadrao : TDiaSemana = Hoje; selecaoSemana : TSelecaoSemana = ssTudo);
    { Public declarations }
  end;

var
  frmSelecaoDataHora: TfrmSelecaoDataHora;

implementation

{$R *.dfm}

{ TfrmSelecaoDataHora }

{ TfrmSelecaoDataHora }

procedure TfrmSelecaoDataHora.ExibirMsgDataHora(diaSemanaPadrao : TDiaSemana ; selecaoSemana: TSelecaoSemana);
var incremento : integer;
    intervalo, dia : Word;
    _hoje : TDateTime;
begin
  //DecodeDate(Now, ano,mes,dia);
  if diaSemanaPadrao <> Hoje then begin
    calendario.Date := Date;
    _hoje := now;
    case diaSemanaPadrao of
      Domingo : dia := 1;
      Segunda : dia := 2;
      Terca   : dia := 3;
      Quarta  : dia := 4;
      Quinta  : dia := 5;
      Sexta   : dia := 6;
      Sabado  : dia := 7;
    end;
    if selecaoSemana in [ssTudo, ssSeguinte] then incremento := 1
                                             else incremento := -1;

    if (DayOfWeek(Date) = dia) and (dia > 0) then
      _hoje := _hoje + incremento;
      while DayOfWeek(_hoje) <> dia do
        _hoje := _hoje + incremento;

    calendario.Date := _hoje;
  end;
end;

end.
