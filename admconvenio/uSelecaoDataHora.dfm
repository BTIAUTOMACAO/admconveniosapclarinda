object frmSelecaoDataHora: TfrmSelecaoDataHora
  Left = 380
  Top = 267
  ActiveControl = btnConfirmar
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Sele'#231#227'o de Data'
  ClientHeight = 207
  ClientWidth = 195
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 0
    Top = 0
    Width = 195
    Height = 16
    Align = alTop
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
  end
  object calendario: TMonthCalendar
    Left = 0
    Top = 16
    Width = 195
    Height = 160
    Align = alClient
    CalColors.TitleBackColor = clBlue
    Date = 41159.398028460650000000
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 176
    Width = 195
    Height = 31
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object btnCancelar: TBitBtn
      Left = 16
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 0
    end
    object btnConfirmar: TBitBtn
      Left = 104
      Top = 3
      Width = 75
      Height = 25
      Caption = 'Con&firmar'
      ModalResult = 1
      TabOrder = 1
    end
  end
end
