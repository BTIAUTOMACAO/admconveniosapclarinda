unit ULimiteSeg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, {ZAbstractRODataset, ZAbstractDataset, ZDataset,} Grids,
  DBGrids, {JvDBCtrl,} ExtCtrls, Buttons, JvSpeedButton, StdCtrls, Mask,
  ToolEdit, CurrEdit, {JvLookup,} JvExControls, JvDBLookup, JvExStdCtrls,
  JvEdit, JvValidateEdit, ADODB;

type
  TFLimiteSeg = class(TForm)

    DSSeg: TDataSource;
    btnGravar: TJvSpeedButton;
    btnCancelar: TJvSpeedButton;
    Segmento: TJvDBLookupCombo;
    Label1: TLabel;
    //edvalor: TCurrencyEdit;
    Label2: TLabel;
    edvalor: TJvValidateEdit;
    Qseg: TADOQuery;
 
    QsegSEG_ID: TIntegerField;   Qsegdescricao: TStringField;
    Panel2: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SegmentoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    seg_nome: String;
    list: TStringList;
  end;

var
  FLimiteSeg: TFLimiteSeg;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFLimiteSeg.FormCreate(Sender: TObject);
begin
  Qseg.Open;
end;

procedure TFLimiteSeg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  QSeg.Close;
end;

procedure TFLimiteSeg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_escape then btnCancelar.Click;
end;

procedure TFLimiteSeg.SegmentoExit(Sender: TObject);
begin
  seg_nome:= Segmento.DisplayValue;
end;

end.
