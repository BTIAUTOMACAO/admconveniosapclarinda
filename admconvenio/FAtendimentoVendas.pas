unit FAtendimentoVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, DBCtrls, StdCtrls, Mask, ZAbstractRODataset, ZDataset,
  Menus, Buttons, ExtCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, ComCtrls,
  ADODB;

type
  TFrmAtendimentoVendas = class(TF1)
    GroupBox1: TGroupBox;
    edtCodCartImp: TMaskEdit;
    Label1: TLabel;
    dbLkpEmp: TDBLookupComboBox;
    dbLkpConv: TDBLookupComboBox;
    dsSituacaoCartoes: TDataSource;
    dsEmpresas: TDataSource;
    dsConveniados: TDataSource;
    btnPesquisar: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    lblEmpLib: TDBText;
    Label5: TLabel;
    lblCartLib: TDBText;
    Label6: TLabel;
    lblConvLib: TDBText;
    lblStatus: TLabel;
    dbLkpCart: TDBLookupComboBox;
    Label7: TLabel;
    dsCartoes: TDataSource;
    pnlLimites: TPanel;
    gbLimite1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    gbLimite2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    gbLimite3: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    gbLimite4: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    gbLimite5: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    btnNovo: TBitBtn;
    dsDiaFecha: TDataSource;
    dsMovimentacao: TDataSource;
    pg: TPageControl;
    tsDescLimites: TTabSheet;
    tsMovimentacao: TTabSheet;
    gbSaldosAssociados: TGroupBox;
    pnlDescLimites: TPanel;
    dbLblDescLim2: TDBText;
    dbLblDescLim3: TDBText;
    dbLblDescLim4: TDBText;
    dbLblRenovSaldos: TDBText;
    pnlHeadMov: TPanel;
    lblTotalDesc: TLabel;
    btnAnt: TBitBtn;
    btnMovAtu: TBitBtn;
    dbGridMov: TJvDBGrid;
    btnMov: TBitBtn;
    btnFecfharAbaMovimentos: TBitBtn;
    lblMesAberto: TLabel;
    lblNomeConv: TLabel;
    lblTotal: TLabel;
    qEmpresas: TADOQuery;
    qConveniados: TADOQuery;
    qConveniadosconv_id: TIntegerField;
    qConveniadostitular: TStringField;
    qConveniadosliberado: TStringField;
    qCartoes: TADOQuery;
    qCartoescodcartimp: TStringField;
    qCartoesnome: TStringField;
    qCartoesliberado: TStringField;
    qDiaFecha: TADOQuery;
    qDiaFechames: TIntegerField;
    qDiaFechaano: TIntegerField;
    qDiaFechadata_fecha: TDateTimeField;
    qDiaFechadata_venc: TDateTimeField;
    qMovimentacao: TADOQuery;
    qMovimentacaodata: TDateTimeField;
    qMovimentacaoautorizacao_id: TIntegerField;
    qMovimentacaodigito: TWordField;
    qMovimentacaotrans_id: TIntegerField;
    qMovimentacaohora: TStringField;
    qMovimentacaodebito: TBCDField;
    qMovimentacaocredito: TBCDField;
    qMovimentacaocredenciado: TStringField;
    qMovimentacaodata_fecha_emp: TDateTimeField;
    qMovimentacaonf: TIntegerField;
    qMovimentacaoreceita: TStringField;
    qMovimentacaoentreg_nf: TStringField;
    qMovimentacaohistorico: TStringField;
    qMovimentacaooperador: TStringField;
    qMovimentacaovalor_cancelado: TBCDField;
    qMovimentacaoformapagto_id: TIntegerField;
    qMovimentacaofatura_id: TIntegerField;
    qMovimentacaodatavenda: TDateTimeField;
    qMovimentacaocancelada: TStringField;
    qMovimentacaoTOTAL: TBCDField;
    Q1: TADOQuery;
    qEmpresasempres_id: TAutoIncField;
    qEmpresasnome: TStringField;
    qEmpresasliberada: TStringField;
    qEmpresasband_id: TIntegerField;
    qSituacaoCartao: TADOQuery;
    qSituacaoCartaoconv_id: TIntegerField;
    qSituacaoCartaotitular: TStringField;
    qSituacaoCartaosaldo_usado_alimentacao: TBCDField;
    qSituacaoCartaolimite_alimentacao: TBCDField;
    qSituacaoCartaosaldo_rest_alimentacao: TBCDField;
    qSituacaoCartaoliberado: TStringField;
    qSituacaoCartaolimite_1: TBCDField;
    qSituacaoCartaoconsumo_mes_1: TBCDField;
    qSituacaoCartaosaldo_acumulado: TBCDField;
    qSituacaoCartaoabono_mes: TBCDField;
    qSituacaoCartaoCODCARTIMP: TStringField;
    qSituacaoCartaoNOME_CARTAO: TStringField;
    qSituacaoCartaoCOD_CARTAO: TIntegerField;
    qSituacaoCartaoCART_LIB: TStringField;
    qSituacaoCartaoempres_id: TAutoIncField;
    qSituacaoCartaonome_empres: TStringField;
    qSituacaoCartaoemp_lib: TStringField;
    qSituacaoCartaoaceita_parc: TStringField;
    qSituacaoCartaovenda_nome: TStringField;
    qSituacaoCartaosaldo_restante: TBCDField;
    qSituacaoCartaolimite_2: TBCDField;
    qSituacaoCartaolimite_3: TBCDField;
    qSituacaoCartaoconsumo_mes_2: TBCDField;
    qSituacaoCartaoconsumo_mes_3: TBCDField;
    qSituacaoCartaosaldo_restante2: TBCDField;
    qSituacaoCartaosaldo_restante3: TBCDField;
    qSituacaoCartaoFECHAMENTO: TWideStringField;
    qSituacaoCartaodesc_lim_1: TStringField;
    qSituacaoCartaodesc_lim_2: TStringField;
    qSituacaoCartaodesc_lim_3: TStringField;
    qSituacaoCartaodesc_lim_4: TStringField;
    qSituacaoCartaolimite_total: TFloatField;
    qSituacaoCartaosaldo_usado_total: TFloatField;
    qSituacaoCartaosaldo_rest_total: TFloatField;
    dbLblDescLim1: TDBText;
    Label11: TLabel;
    lblParcel: TDBText;
    qEmpresasaceita_parc: TStringField;
    qConveniadosCPF: TStringField;
    txtCPF: TMaskEdit;
    Label15: TLabel;
    procedure btnPesquisarClick(Sender: TObject);
    procedure dsEmpresasDataChange(Sender: TObject; Field: TField);
    procedure dsConveniadosDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dsCartoesDataChange(Sender: TObject; Field: TField);
    procedure dsSituacaoCartoesDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbLkpCartKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAntClick(Sender: TObject);
    procedure qSituacaoCartaoBeforeOpen(DataSet: TDataSet);
    procedure btnMovClick(Sender: TObject);
    procedure tsMovimentacaoShow(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnMovAtuClick(Sender: TObject);
    procedure btnFecfharAbaMovimentosClick(Sender: TObject);
    procedure dbGridMovDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qConveniadosBeforeOpen(DataSet: TDataSet);
    procedure qCartoesBeforeOpen(DataSet: TDataSet);
    procedure qSituacaoCartaoAfterPost(DataSet: TDataSet);
    procedure qSituacaoCartaoCalcFields(DataSet: TDataSet);
  private
    function getFechamentoAtual : TDateTime;
    function abrirMovimentacao(data : TDateTime) : Boolean;
    function calcularTotalGasto : Double;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAtendimentoVendas: TFrmAtendimentoVendas;

implementation




uses DM, cartao_util, URotinasTexto, MaskUtils, Math, UCadConv;

{$R *.dfm}

function TFrmAtendimentoVendas.getFechamentoAtual : TDateTime;

begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('EXEC GET_PROX_FECHA '+QuotedStr(qEmpresasempres_id.AsString));
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].Value;
end;

function TFrmAtendimentoVendas.abrirMovimentacao(data : TDateTime) : Boolean;
var dia, mes, ano : Word;
begin
  //qDiaFecha.Open;
  DecodeDate(data, ano, mes, dia);
  if data = now then
    lblMesAberto.Caption := 'Movimentos em Aberto do conv.: '
  else
    lblMesAberto.Caption := 'Fechamento de: ' + IntToStr(dia) + '/' + IntToStr(mes) + '/' + IntToStr(ano) + ' do conv.: ';
  if qDiaFecha.Locate('MES;ANO', VarArrayOf([mes,ano]),[]) then begin;
    qMovimentacao.Close;
    qMovimentacao.Parameters.ParamByName('CONV_ID').Value     := qSituacaoCartaoCONV_ID.AsInteger;
    qMovimentacao.Parameters.ParamByName('DATA_FECHA').Value  := qDiaFechaDATA_FECHA.AsDateTime;
    qMovimentacao.Open;
    calcularTotalGasto;
    Result := True;
  end else
    Result := False;
end;

function TFrmAtendimentoVendas.calcularTotalGasto : Double;
begin
  lblTotal.Caption := 'R$' + FormatFloat(qMovimentacaoTOTAL.DisplayFormat, qMovimentacaoTOTAL.AsCurrency);
end;

procedure ExecutaQuery(Query : string ; Parametro : Variant);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(Query);
  DMConexao.AdoQry.Parameters[0].Value := Parametro;
  DMConexao.AdoQry.Open;
end;

procedure TFrmAtendimentoVendas.btnPesquisarClick(Sender: TObject);
var qtd_limites, convId, EmpresId, tipoCredito : Integer;
    usa_lim_dif : String;
    saldo_res : String;
begin
  inherited;
  screen.Cursor := crHourGlass;
  btnPesquisar.Enabled := false;
  if (RemoveCaracter(edtCodCartImp.Text,' ') <> '') then begin
    TButton(Sender).Enabled := False;

    Q1.SQL.Clear;
    Q1.SQL.Add('SELECT c.conv_id, c.empres_id FROM CONVENIADOS c, CARTOES ca WHERE c.CONV_ID = ca.CONV_ID and c.CONV_ID = ');
    Q1.SQL.Add('(SELECT ca.CONV_ID from CARTOES ca where ca.CODCARTIMP = '+QuotedStr(edtCodCartImp.Text)+')');

    Q1.Open;
    IF(Q1.Fields[0].Value = null) THEN
    begin
      msgInf('N�o encontramos nenhum cart�o com o c�digo informado.');
      edtCodCartImp.Text := '';
      edtCodCartImp.SetFocus;
      TButton(Sender).Enabled := True;
      screen.Cursor := crDefault;
    end

    else 
    BEGIN
      convId   := Q1.Fields[0].Value;
      EmpresId := Q1.Fields[1].Value;

      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT usa_saldo_dif from conveniados WHERE conv_id = ');
      DMConexao.AdoQry.SQL.Add('(SELECT ca.conv_id FROM CARTOES ca WHERE codcartimp = '+ QuotedStr(edtCodCartImp.Text)+')');
      DMConexao.AdoQry.SQL.Text;
      DMConexao.AdoQry.Open;
      usa_lim_dif := DMConexao.AdoQry.fields[0].Value;
      DMConexao.AdoQry.Close;

      qEmpresas.Close;
      qEmpresas.Parameters[0].Value := convId;
      qEmpresas.Open;
      qEmpresasempres_id.Value;
      ExecutaQuery('SELECT qtd_limites from BANDEIRAS WHERE BAND_ID = '+
                               '(SELECT band_id FROM EMPRESAS WHERE EMPRES_ID = '+
                               '(SELECT empres_id FROM Conveniados C WHERE C.conv_id  = '+
                               '(SELECT ca.conv_id FROM CARTOES ca WHERE codcartimp = :codcartimp)))',edtCodCartImp.Text);

      qtd_limites := DMConexao.AdoQry.Fields[0].Value;
      qSituacaoCartao.Close;
      qSituacaoCartao.SQL.Clear;

      //Verificar Tipo de Cr�dito
      Q1.Close;
      Q1.SQL.Clear;
      Q1.SQL.Add('SELECT tipo_credito FROM empresas WHERE empres_id = '+QuotedStr(IntToStr(EmpresId)));
      Q1.Open;
      tipoCredito := Q1.Fields[0].Value;

      qSituacaoCartao.SQL.Add('SELECT ');

      //Inicio da Query Din�mica

      qSituacaoCartao.SQL.Add('ca.conv_id,ca.codcartimp,c.empres_id,c.titular, ');
      qSituacaoCartao.SQL.Add('case when coalesce(c.liberado, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as liberado,');
      //-----------------QUERY PARA TIPO CR�DITO 2 OU 3-----------------------------
      if(tipoCredito in [2,3])then
      begin
        qSituacaoCartao.SQL.Add(' coalesce(c.limite_mes,0.0) as limite_alimentacao, coalesce(c.consumo_mes_1,0.0) as saldo_usado_alimentacao, coalesce(saldo_acumulado,0.0),');
        qSituacaoCartao.SQL.Add(' coalesce((SELECT (SELECT conv.limite_mes AS limite_1) + CASE WHEN e.tipo_credito in(2,3) THEN');
        qSituacaoCartao.SQL.Add(' coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = ''N'' THEN 0 else 0 END END - (SELECT CONSUMO_MES_1 as consumo_mes_1)');
        qSituacaoCartao.SQL.Add(' from conveniados conv join empresas e on e.EMPRES_ID = '+IntToStr(EmpresId)+' AND CONV.CONV_ID = '+IntToStr(ConvId)+'),0) AS saldo_rest_alimentacao,');
        qSituacaoCartao.SQL.Add(' 0.00 as limite_1, 0.00 as limite_2, 0.00 as limite_3,');
        qSituacaoCartao.SQL.Add(' 0.00 as saldo_restante, 0.00 as saldo_restante2, 0.00 as saldo_restante3,' );
        qSituacaoCartao.SQL.Add(' 0.00 as consumo_mes_1, 0.00 as consumo_mes_2, 0.00 as consumo_mes_3,');
        qSituacaoCartao.SQL.Add(' coalesce(c.saldo_acumulado,0) as saldo_acumulado,coalesce(c.abono_mes,0) as abono_mes,');
        qSituacaoCartao.SQL.Add(' (CONCAT(iif((select b.band_id from bandeiras b where band_id = 999) <> 999 ,'+QuotedStr('RENOVA��O DOS LIMITES EM: ')+','+QuotedStr('RENOVA��O DO LIMITE EM: ')+'),');
        qSituacaoCartao.SQL.Add('(SELECT top 1 FORMAT(data_fecha,'+QuotedStr('dd-MM-yyyy')+') from dia_fecha WHERE data_fecha > CURRENT_TIMESTAMP and');
        qSituacaoCartao.SQL.Add(' EMPRES_ID = (SELECT conveniados.EMPRES_ID from conveniados where conv_id = '+QuotedStr(IntToStr(convId))+')))) as FECHAMENTO,');

        //Retorno do dbLblDescLim1 caso o cliente tenha tipo cr�dito [2,3]
        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_1, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT (saldo_disponivel_1,''C'',''pt'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_1,');

        //Retorno do Label dbLblDescLim2 caso o cliente tenha tipo cr�dito [2,3]
        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_2, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT (saldo_disponivel_2,''C'',''pt'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_2,');

        //Retorno do Label dbLblDescLim3 caso o cliente tenha tipo cr�dito [2,3]
        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_3, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT (saldo_disponivel_3,''C'',''pt'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_3,');

        //Retorno do Label dbLblDescLim4 caso o cliente tenha tipo cr�dito [2,3]
        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_4, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT (saldo_disponivel_4,''C'',''pt'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_4,');

        qSituacaoCartao.SQL.Text;
        qSituacaoCartao.SQL.Add(' ca.nome AS NOME_CARTAO,ca.codigo as COD_CARTAO,');
        qSituacaoCartao.SQL.Add(' case when coalesce(ca.liberado, ''S'') = ''S'' then ''SIM'' else ''N�O'' end AS CART_LIB,');
        qSituacaoCartao.SQL.Add(' e.empres_id, e.nome as nome_empres,');
        qSituacaoCartao.SQL.Add(' case when coalesce(e.liberada, ''S'')= ''S'' then ''SIM'' else ''N�O'' END as emp_lib,');
        //QSituacao_new.SQL.Add(' case when coalesce(e.todos_segmentos, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as todos_segmentos,');
        qSituacaoCartao.SQL.Add(' case when coalesce(e.aceita_parc, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as aceita_parc,');
        qSituacaoCartao.SQL.Add(' case when coalesce(e.venda_nome, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as venda_nome FROM conveniados c');
        qSituacaoCartao.SQL.Add(' INNER JOIN EMPRESAS AS e ON e.EMPRES_ID = c.empres_id');
        qSituacaoCartao.SQL.Add(' LEFT JOIN CARTOES AS ca ON ca.CODCARTIMP = '+QuotedStr(edtCodCartImp.Text));//(SELECT TOP 1 CA.CODIGO FROM CARTOES CA WHERE ca.conv_id = '+ConvId+')');
        qSituacaoCartao.SQL.Add(' INNER JOIN BANDEIRAS AS b ON b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+IntToStr(EmpresId)+') AND c.conv_id = '+IntToStr(ConvId));
        qSituacaoCartao.SQL.Text;
        qSituacaoCartao.Open;
        dbLblDescLim2.Visible := false;
        dbLblDescLim3.Visible := false;
        dbLblDescLim4.Visible := false;

      end
      //---------------------------------------------------------------------------
      //---------------------------------------------------------------------------
      //------------------FIM DA QUERY PARA TIPO CR�DITO 2 OU 3--------------------
      else
      begin
        if(qEmpresasband_id.Value = 999) then
        begin
          if(usa_lim_dif = 'S')then
            qSituacaoCartao.SQL.Add(' coalesce(bc.limite_1 as limite_1,0.0) as limite_1, coalesce(c.consumo_mes_1,0.0) as consumo_mes_1, ' )
          else begin
            qSituacaoCartao.SQL.Add(' coalesce(c.limite_mes,0.0) as limite_1, coalesce(c.consumo_mes_1,0.0) as consumo_mes_1,');
          end;
        end;
        qSituacaoCartao.SQL.Add(' coalesce(c.saldo_acumulado,0.0) as saldo_acumulado,coalesce(c.abono_mes,0.0) as abono_mes,');

        qSituacaoCartao.SQL.Add(' (CONCAT(iif((select b.band_id from bandeiras b where band_id = '+qEmpresasband_id.AsString+') <> 999 ,'+QuotedStr('RENOVA��O DOS LIMITES EM: ')+','+QuotedStr('RENOVA��O DO LIMITE EM: ')+'),');
        qSituacaoCartao.SQL.Add('(SELECT top 1 FORMAT(data_fecha,'+QuotedStr('dd-MM-yyyy')+') from dia_fecha WHERE data_fecha > CURRENT_TIMESTAMP and');
        qSituacaoCartao.SQL.Add(' EMPRES_ID = (SELECT conveniados.EMPRES_ID from conveniados where conv_id = '+QuotedStr(IntToStr(convId))+')))) as FECHAMENTO,');

        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_1, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT(saldo_disponivel_1,''C'',''PT'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_1,');

        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_2, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT(saldo_disponivel_2,''C'',''PT'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_2,');

        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_3, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' �  DE ')+',(select FORMAT(saldo_disponivel_3,''C'',''PT'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_3,');

        //Retorno do Label dbLblDescLim4 caso o cliente tenha tipo cr�dito [2,3]
        qSituacaoCartao.SQL.Add(' (CONCAT('+QuotedStr('SALDO DISPON�VEL PARA ') + ',');
        qSituacaoCartao.SQL.Add(' iif((select b.band_id from bandeiras b where band_id = (SELECT band_id FROM empresas WHERE empres_id = '+IntToStr(EmpresId)+')) <> 999, b.desc_limite_4, b.descricao),');
        qSituacaoCartao.SQL.Add(' '+QuotedStr(' � DE ')+',(select FORMAT(saldo_disponivel_4,''C'',''PT'') from getLimite('+QuotedStr(IntToStr(convId))+')))) as desc_lim_4,');

          if(qEmpresasband_id.Value = 999)then
          begin
            if(usa_lim_dif = 'S')then
            BEGIN
              qSituacaoCartao.SQL.Add(' coalesce((SELECT (SELECT bc.limite_1 AS limite_1) + CASE WHEN e.tipo_credito in(2,3) THEN');
              qSituacaoCartao.SQL.Add(' coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = ''N'' THEN 0 else 0 END END - (SELECT CONSUMO_MES_1 as consumo_mes_1)');
              qSituacaoCartao.SQL.Add(' from conveniados conv join empresas e on e.EMPRES_ID = '+IntToStr(EmpresId));
              qSituacaoCartao.SQL.Add(' join BANDEIRAS_CONV bc on bc.CONV_ID = conv.CONV_ID AND CONV.CONV_ID = '+IntToStr(ConvId)+'),0.0) AS saldo_restante,')
            END
            else
            begin
              qSituacaoCartao.SQL.Add(' coalesce((SELECT (SELECT conv.limite_mes AS limite_1) + CASE WHEN e.tipo_credito in(2,3) THEN');
              qSituacaoCartao.SQL.Add(' coalesce(conv.saldo_acumulado,0) + (conv.abono_mes) else CASE WHEN e.TIPO_CREDITO in(2,3) and e.liberada = ''N'' THEN 0 else 0 END END - (SELECT CONSUMO_MES_1 as consumo_mes_1)');
              qSituacaoCartao.SQL.Add(' from conveniados conv join empresas e on e.EMPRES_ID = '+IntToStr(EmpresId)+' AND CONV.CONV_ID = '+IntToStr(ConvId)+'),0.0) AS saldo_restante,');
            end;
            dbLblDescLim2.Visible := false;
            dbLblDescLim3.Visible := false;
            dbLblDescLim4.Visible := false;
          end;

          qSituacaoCartao.SQL.Text;
          //Calculando o valor da colunao saldo restante 1 para empresa com Bandeira 1 (ex: Confab)
          if(qEmpresasband_id.Value <> 999)then
          begin
            if(usa_lim_dif = 'S')then
            BEGIN
              qSituacaoCartao.SQL.Add(' coalesce((SELECT bc.limite_1 - coalesce(c.consumo_mes_1,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
              qSituacaoCartao.SQL.Add(' WHERE bc.conv_id = c.conv_id and c.conv_id = ');
              qSituacaoCartao.SQL.Add(' (SELECT CONV_ID FROM CONVENIADOS c WHERE c.conv_id = '+IntToStr(ConvId)+')),0.0) as saldo_restante,');
            END
            else begin
              qSituacaoCartao.SQL.Add(' coalesce((SELECT b.limite_1 - coalesce(c.consumo_mes_1,0) from BANDEIRAS b, CONVENIADOS c');
              qSituacaoCartao.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+IntToStr(EmpresId)+') AND');
              qSituacaoCartao.SQL.Add(' c.conv_id = '+IntToStr(ConvId)+'),0.0) as saldo_restante,');
            end;
            dbLblDescLim2.Visible := true;
            dbLblDescLim3.Visible := true;
            //dbLblDescLim4.Visible := true;
            end;
          //Calculando o valor da colunao saldo restante 2
          if(usa_lim_dif = 'S')then
          BEGIN
            qSituacaoCartao.SQL.Add(' coalesce((SELECT bc.limite_2 - coalesce(c.consumo_mes_2,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
            qSituacaoCartao.SQL.Add(' WHERE bc.conv_id = c.conv_id and c.conv_id = ');
            qSituacaoCartao.SQL.Add(' (SELECT CONV_ID FROM CONVENIADOS c WHERE c.conv_id = '+IntToStr(ConvId)+')),0.0) as saldo_restante2,');
          END
          else begin
            qSituacaoCartao.SQL.Add(' coalesce((SELECT b.limite_2 - coalesce(c.consumo_mes_2,0) from BANDEIRAS b, CONVENIADOS c');
            qSituacaoCartao.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+IntToStr(EmpresId)+') AND');
            qSituacaoCartao.SQL.Add(' c.conv_id = '+IntToStr(ConvId)+'),0.0) as saldo_restante2,');
            //Calculando o valor da colunao saldo restante 3
          end;
          if(usa_lim_dif = 'S')then
          BEGIN
            qSituacaoCartao.SQL.Add(' coalesce((SELECT bc.limite_3 - coalesce(c.consumo_mes_3,0) from BANDEIRAS_CONV bc, CONVENIADOS c');
            qSituacaoCartao.SQL.Add(' WHERE bc.conv_id = c.conv_id and c.conv_id = ');
            qSituacaoCartao.SQL.Add(' (SELECT CONV_ID FROM CONVENIADOS c WHERE c.conv_id = '+IntToStr(ConvId)+')),0.0) as saldo_restante3,');
          END
          else begin
            qSituacaoCartao.SQL.Add(' coalesce((SELECT b.limite_3 - coalesce(c.consumo_mes_3,0) from BANDEIRAS b, CONVENIADOS c');
            qSituacaoCartao.SQL.Add(' WHERE b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+IntToStr(EmpresId)+') AND');
            qSituacaoCartao.SQL.Add(' c.conv_id = '+IntToStr(ConvId)+'),0.0) as saldo_restante3,');
          end;

          qSituacaoCartao.SQL.Add(' ca.nome AS NOME_CARTAO,ca.codigo as COD_CARTAO,');
          qSituacaoCartao.SQL.Add(' case when coalesce(ca.liberado, ''S'') = ''S'' then ''SIM'' else ''N�O'' end AS CART_LIB,');
          qSituacaoCartao.SQL.Add(' e.empres_id, e.nome as nome_empres,');
          qSituacaoCartao.SQL.Add(' case when coalesce(e.liberada, ''S'')= ''S'' then ''SIM'' else ''N�O'' END as emp_lib,');
          //QSituacao_new.SQL.Add(' case when coalesce(e.todos_segmentos, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as todos_segmentos,');
          qSituacaoCartao.SQL.Add(' case when coalesce(e.aceita_parc, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as aceita_parc,');
          qSituacaoCartao.SQL.Add(' case when coalesce(e.venda_nome, ''S'') = ''S'' then ''SIM'' else ''N�O'' end as venda_nome,');
          qSituacaoCartao.SQL.ADD(' 0.00 AS saldo_usado_alimentacao, 0.00 AS limite_alimentacao, 0.00 AS saldo_rest_alimentacao,');
          //Busca consumo M�s 1 e Limite m�s 1 para empresa de bandeira 1(ex: Confab)
          if(qEmpresasband_id.AsInteger <> 999)then
          begin
            if(usa_lim_dif = 'S')then
              qSituacaoCartao.SQL.Add(' coalesce(c.consumo_mes_1,0.0) as consumo_mes_1,coalesce(bc.limite_1,0.0) as limite_1,')
            else
              qSituacaoCartao.SQL.Add(' coalesce(c.consumo_mes_1,0.0) as consumo_mes_1,coalesce(b.limite_1,0.0) as limite_1,');
          end;
          if(usa_lim_dif = 'S')then
            qSituacaoCartao.SQL.Add(' coalesce(bc.limite_2,0.0) as limite_2, coalesce(bc.LIMITE_3,0.0) as limite_3,')
          else
            qSituacaoCartao.SQL.Add(' coalesce(b.limite_2,0.0) as limite_2, coalesce(b.LIMITE_3,0.0) as limite_3,');

          qSituacaoCartao.SQL.Add(' coalesce(c.consumo_mes_2,0.0) as consumo_mes_2,coalesce(c.consumo_mes_3,0.0) as consumo_mes_3');
          qSituacaoCartao.SQL.Add(' from CONVENIADOS as c INNER JOIN EMPRESAS AS e ON e.EMPRES_ID = c.empres_id');
          qSituacaoCartao.SQL.Add(' LEFT JOIN CARTOES AS ca ON ca.CODCARTIMP = '+QuotedStr(edtCodCartImp.Text));//(SELECT TOP 1 CA.CODIGO FROM CARTOES CA WHERE ca.conv_id = '+ConvId+')');
          qSituacaoCartao.SQL.Add(' INNER JOIN BANDEIRAS AS b ON b.BAND_ID = (SELECT e.band_id FROM EMPRESAS AS e WHERE EMPRES_ID = '+IntToStr(EmpresId)+') AND c.conv_id = '+IntToStr(ConvId));
          if(usa_lim_dif = 'S') then
            qSituacaoCartao.SQL.Add(' INNER JOIN BANDEIRAS_CONV AS bc ON bc.CONV_ID = c.conv_id and c.conv_id = (SELECT c.conv_id FROM conveniados c WHERE c.CONV_ID = '+IntToStr(convId)+')');



        qSituacaoCartao.SQL.Text;
        qSituacaoCartao.Open;
        if(qtd_limites = 2 ) then
          dbLblDescLim3.Visible := false;

        screen.Cursor := crDefault;
      end;


      //qEmpresas.Open;
      dbLkpEmp.KeyValue := qSituacaoCartaoEMPRES_ID.AsInteger;
      if dbLkpEmp.Text = '' then Exit;
      dbLkpEmp.Enabled := false;
      qConveniados.Close;
      qConveniados.SQL.Clear;
      qConveniados.SQL.Add('select conv_id,titular,iif(liberado = ''S'',''LIBERADO'',''BLOQUEADO'') liberado,cpf CPF ');
      qConveniados.SQL.Add(' from conveniados  where apagado <> ''S''  and empres_id = '+qSituacaoCartaoEMPRES_ID.AsString+' and conv_id '+qSituacaoCartaoconv_id.AsString+'  order by titular');
      qConveniados.Open;
      txtCPF.Text := qConveniadosCPF.AsString;
      dbLkpConv.KeyValue := qSituacaoCartaoCONV_ID.AsInteger;
      qConveniadostitular.Value;
      dbLkpConv.Enabled := false;
      if dbLkpConv.Text = '' then Exit;
      //qCartoes.Close;
      qCartoes.Open;
      if qSituacaoCartaoCODCARTIMP.AsString <> '' then begin
        dbLkpCart.KeyValue := qSituacaoCartaoCODCARTIMP.AsString
      end else begin
        dbLkpCart.KeyValue := fnRemoveCaracters(' ',edtCodCartImp.Text);
      end;
      dbLkpCart.Enabled := false;
      if dbLkpCart.Text = '' then begin
        msginf(dbLkpCart.Text);
        Exit;
      end;
    END;
  end else begin
    MsgInf('Digite um cart�o ou escolha um nos campos de sele��o.');
    TButton(Sender).Enabled := True;
    screen.Cursor := crDefault;
    edtCodCartImp.SetFocus;
  end;
  //TButton(Sender).Enabled := True;

  //Q1.Close;
end;

procedure TFrmAtendimentoVendas.FormCreate(Sender: TObject);
begin
  inherited;
  //qEmpresas.Open;
  //dbLkpEmp.KeyValue := -1;
end;

procedure TFrmAtendimentoVendas.btnNovoClick(Sender: TObject);
begin
  inherited;
  btnPesquisar.Enabled := true;
  edtCodCartImp.Text := '';
  dbLkpEmp.KeyValue := -1;
  //qEmpresas.Locate('EMPRES_ID',-1,[]);
  qCartoes.Close;
  qConveniados.Close;
  qEmpresas.Close;
  qSituacaoCartao.Close;
  qEmpresas.Open;
  qDiaFecha.Close;
  lblStatus.Caption := '';
  edtCodCartImp.SetFocus;
  pg.TabIndex := 0;
end;

procedure TFrmAtendimentoVendas.FormResize(Sender: TObject);
var tamLimites, tamDescLim : Integer;
begin
  inherited;
  tamLimites := pnlLimites.Width div 5;
  gbLimite1.Width := tamLimites;
  gbLimite2.Width := tamLimites;
  gbLimite3.Width := tamLimites;
  gbLimite4.Width := tamLimites;
  gbLimite5.Width := tamLimites;

  //-15 por que apenas "div 4" fica um alinhamento mto estranho...
  tamDescLim := (gbSaldosAssociados.Height-15) div 4;
  dbLblDescLim1.Height := tamDescLim;
  dbLblDescLim2.Height := tamDescLim;
  dbLblDescLim3.Height := tamDescLim;
  dbLblDescLim4.Height := tamDescLim;

end;

procedure TFrmAtendimentoVendas.dsEmpresasDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  dbLkpConv.Enabled := dbLkpEmp.KeyValue > -1;
  qConveniados.Active := dbLkpEmp.KeyValue > -1;
  lblEmpLib.Visible := dbLkpEmp.KeyValue > -1;
  dbLkpConv.KeyValue := -1;
  lblConvLib.Caption := '';
  lblEmpLib.Font.Color := StringToColor(iif(lblEmpLib.Caption = 'LIBERADO','clGreen','clRed'));
  lblParcel.Font.Color := StringToColor(iif(lblParcel.Caption = 'S','clGreen','clRed'));
end;

procedure TFrmAtendimentoVendas.dsConveniadosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  dbLkpCart.Enabled := dbLkpConv.KeyValue > -1;
  qCartoes.Active := dbLkpConv.KeyValue > -1;
  dbLkpCart.KeyValue := -1;
  lblCartLib.Caption := '';
  lblConvLib.Visible := dbLkpConv.KeyValue > -1;
  lblConvLib.Font.Color := StringToColor(iif(lblConvLib.Caption = 'LIBERADO','clGreen','clRed'));
  lblNomeConv.Caption := qConveniadosTITULAR.AsString;
end;

procedure TFrmAtendimentoVendas.dsCartoesDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  lblCartLib.Visible := dbLkpCart.KeyValue > -1;
  lblCartLib.Font.Color := StringToColor(iif(lblCartLib.Caption = 'LIBERADO','clGreen','clRed'));
  if dbLkpCart.KeyValue = -1 then begin
    lblStatus.Caption := '';
    //qSituacaoCartao.Close;
  end else begin
    if (qEmpresasLIBERADA.AsString = 'LIBERADO') and (qConveniadosLIBERADO.AsString = 'LIBERADO') and (qCartoesLIBERADO.AsString = 'LIBERADO') then begin
      lblStatus.Caption := 'VENDA PERMITIDA';
      lblStatus.Font.Color := clBlue;
    end else begin
      lblStatus.Caption := 'VENDA N�O PERMITIDA';
      lblStatus.Font.Color := clRed;
    end;
  end;
end;

procedure TFrmAtendimentoVendas.dsSituacaoCartoesDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  lblEmpLib.Visible := dbLkpEmp.KeyValue > -1;
  lblEmpLib.Font.Color := StringToColor(iif(lblEmpLib.Caption = 'LIBERADO', 'clGreen', 'clRed'));
  qMovimentacao.Close;

  abrirMovimentacao(getFechamentoAtual);
end;

procedure TFrmAtendimentoVendas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qSituacaoCartao.Close;
  qCartoes.Close;
  qConveniados.Close;
  qEmpresas.Close;
end;

procedure TFrmAtendimentoVendas.dbLkpCartKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = vk_return then begin
    edtCodCartImp.Text := qCartoesCODCARTIMP.AsString;
    btnPesquisar.Click;
  end;
end;

procedure TFrmAtendimentoVendas.btnAntClick(Sender: TObject);
begin
  inherited;
  qDiaFecha.Prior;
  abrirMovimentacao(qDiaFechaDATA_FECHA.AsDateTime);
  btnMovAtu.Enabled := True;
end;

procedure TFrmAtendimentoVendas.qSituacaoCartaoBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;

  qDiaFecha.Parameters[0].Value := qEmpresasempres_id.Value;
  qDiaFecha.Open;
end;

procedure TFrmAtendimentoVendas.btnMovClick(Sender: TObject);
begin
  inherited;
  if (TBitBtn(Sender).Caption = '&Movimentos') then begin
    TBitBtn(Sender).Caption := '&Fechar';
    pg.ActivePage := tsMovimentacao;
  end else begin
    TBitBtn(Sender).Caption := '&Movimentos';
    pg.ActivePage := tsDescLimites;
  end;
end;

procedure TFrmAtendimentoVendas.tsMovimentacaoShow(Sender: TObject);
begin
  inherited;
  btnMovAtu.enabled := (not qMovimentacao.IsEmpty) and (qDiaFecha.Active) and (qDiaFecha.Locate('DATA_FECHA', Date,[])) and (not qDiaFecha.Eof);
  btnAnt.enabled  := (tsMovimentacao.Visible) and (qDiaFecha.Active) and (qDiaFecha.RecNo <> qDiaFecha.RecordCount -1);
  lblMesAberto.Left := 0;
  lblNomeConv.Left  := lblMesAberto.Left  + lblMesAberto.Width + 1;
  lblTotalDesc.Left := lblNomeConv.Left   + lblNomeConv.Width  + 1;
  lblTotal.Left     := lblTotalDesc.Left  + lblTotalDesc.Width + 1;
end;

procedure TFrmAtendimentoVendas.FormShow(Sender: TObject);
var i : Integer;
begin
  inherited;
  for i:=0 to pg.PageCount-1 do begin
    pg.Pages[i].TabVisible := false;
  end;
  pg.Pages[0].Visible := True;
end;

procedure TFrmAtendimentoVendas.btnMovAtuClick(Sender: TObject);
begin
  inherited;
  TBitBtn(Sender).Enabled := False;
  abrirMovimentacao(getFechamentoAtual);
end;

procedure TFrmAtendimentoVendas.btnFecfharAbaMovimentosClick(Sender: TObject);
begin
  inherited;
  btnMov.OnClick(btnMov);  
end;

procedure TFrmAtendimentoVendas.dbGridMovDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var Grid : TDBGrid;
  L, R: Integer;
  marc: Boolean;
begin
  inherited;
{  with TDBGrid(Sender) do begin
    if UpperCase(qMovimentacaoCANCELADA.AsString) = 'S' then
      Grid.Canvas.Font.Color := clRed
  end;
  dbGridMov.Canvas.FillRect(Rect);
  dbGridMov.DefaultDrawDataCell(Rect,Column.Field,State);}
//  R:= Rect.Right; L:= Rect.Left;
//  if Column.Index = 0 then L := L + 1;
//  if Column.Index = Grid.Columns.Count -1 then R := R - 1;
//  Grid.Canvas.FillRect(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1));
//  Grid.DefaultDrawColumnCell(Classes.Rect(L, Rect.Top+1, R, Rect.Bottom-1), DataCol, Column, State);
end;

procedure TFrmAtendimentoVendas.qConveniadosBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //qConveniados.Close;
  //qConveniados.Parameters[0].Value := qEmpresasempres_id.Value;
end;

procedure TFrmAtendimentoVendas.qCartoesBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  //qCartoes.Close;
  qCartoes.Parameters[0].Value := qConveniadosconv_id.Value;
end;

procedure TFrmAtendimentoVendas.qSituacaoCartaoAfterPost(DataSet: TDataSet);
begin
  inherited;
  btnPesquisar.Enabled := false;
end;

procedure TFrmAtendimentoVendas.qSituacaoCartaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  qSituacaoCartaolimite_total.Value      := qSituacaoCartaolimite_alimentacao.Value + qSituacaoCartaolimite_1.Value + qSituacaoCartaolimite_3.Value + qSituacaoCartaosaldo_acumulado.Value;
  qSituacaoCartaosaldo_usado_total.Value := qSituacaoCartaoconsumo_mes_1.Value + qSituacaoCartaoconsumo_mes_2.Value + qSituacaoCartaoconsumo_mes_3.Value + qSituacaoCartaosaldo_usado_alimentacao.Value;
  qSituacaoCartaosaldo_rest_total.Value  := qSituacaoCartaosaldo_rest_alimentacao.Value + qSituacaoCartaosaldo_restante.Value + qSituacaoCartaosaldo_restante2.Value + qSituacaoCartaosaldo_restante3.Value 

end;

end.
