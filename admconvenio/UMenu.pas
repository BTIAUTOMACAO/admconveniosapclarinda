unit UMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ToolWin, ComCtrls, ImgList, DB, ExtCtrls, StdCtrls, RxGIF,
  cartao_util, Menus, JvComponent, JvGIF, Inifiles, UTipos, Winsock, JclShell,
  UCad, DBGrids, Shellapi, JvPanel, pngimage, ActiveX, ShlObj, Buttons, MSNPopUp,
  AppEvnts, jpeg, JvButton, Grids, Mask, JvToolEdit, ADODB, IBCustomDataSet,
  IBQuery, JvExControls, JvTransparentButton, frxpngimage, UCadSegEmpresas;

type
  TFMenu = class(TForm)
    StatusBar1: TStatusBar;
    ImageList1: TImageList;
    Barra: TToolBar;
    MainMenu1: TMainMenu;
    CadAgenciador: TMenuItem;
    Lanamentos1: TMenuItem;
    Operacional1: TMenuItem;
    Relatrios1: TMenuItem;
    Consultas1: TMenuItem;
    AuditoriadoSistema1: TMenuItem;
    Conferncia1: TMenuItem;
    Utilitrios1: TMenuItem;
    Configuraes1: TMenuItem;
    CadAdm: TMenuItem;
    N1: TMenuItem;
    CadEmp: TMenuItem;
    CadCred: TMenuItem;
    CadConv: TMenuItem;
    CadCartoes: TMenuItem;
    CadSeg: TMenuItem;
    N2: TMenuItem;
    CadFormaPagto: TMenuItem;
    CadBanco: TMenuItem;
    N3: TMenuItem;
    Operadores: TMenuItem;
    GeraTaxaConv: TMenuItem;
    N5: TMenuItem;
    Lancamentos: TMenuItem;
    N6: TMenuItem;
    LancaCPMF: TMenuItem;
    BaixaEmp: TMenuItem;
    BaixaFornec: TMenuItem;
    GerarArquivoparaCartes1: TMenuItem;
    N9: TMenuItem;
    OperGeraCartConv: TMenuItem;
    OperGeraCartForn: TMenuItem;
    ImportConveniados: TMenuItem;
    ImportaLimite: TMenuItem;
    N10: TMenuItem;
    GeraDoc: TMenuItem;
    N11: TMenuItem;
    RelExtratoEmp: TMenuItem;
    RelExtratoFor: TMenuItem;
    RelExtratoUser: TMenuItem;
    RelExtratoDemit: TMenuItem;
    ctExtForVencEmp: TMenuItem;
    TotPagFor: TMenuItem;
    Action1: TMenuItem;
    RelMediaPorEmp: TMenuItem;
    Action2: TMenuItem;
    Consulta_emp_fornec: TMenuItem;
    Consulta_for_emp: TMenuItem;
    Consulta_autor: TMenuItem;
    Consulta_fecha: TMenuItem;
    Medicao_uso: TMenuItem;
    ConsContaCorr: TMenuItem;
    Cons_Mov_Fornec: TMenuItem;
    Cons_Mov_Empres: TMenuItem;
    Cons_Mov_Conv: TMenuItem;
    Auditor: TMenuItem;
    ActAuditDatas: TMenuItem;
    Aud_gruposempresas: TMenuItem;
    CadRelConf: TMenuItem;
    ConfVal: TMenuItem;
    ConsConferencia: TMenuItem;
    AtualizaodeDatasdeFechamentoseVencimentos1: TMenuItem;
    Alteracoes_Sis: TMenuItem;
    Rel_Alt_Lin: TMenuItem;
    Logoff: TMenuItem;
    ExportXML: TMenuItem;
    RegistrosApagados: TMenuItem;
    Config: TMenuItem;
    ConfigDebCC: TMenuItem;
    CadGrupProd: TMenuItem;
    CadOper: TMenuItem;
    CadGrupOper: TMenuItem;
    Fechamentos1: TMenuItem;
    Financeiro1: TMenuItem;
    FinancFatura: TMenuItem;
    PagamentodeFornecedores1: TMenuItem;
    ManutenodeAutorizaes1: TMenuItem;
    CriarDatasdeFechamentoEmpresasFornecedores1: TMenuItem;
    AnlisedeAtividadeporEmpresa1: TMenuItem;
    ProdutosporAutorizao1: TMenuItem;
    LanamentosIndividuais1: TMenuItem;
    VisualizarLanamentos1: TMenuItem;
    CadTaxas: TMenuItem;
    CadDespAdm: TMenuItem;
    ConfernciadeDespesasdaAdministradora1: TMenuItem;
    ConfiguraoLocal1: TMenuItem;
    OperFechaEmp: TMenuItem;
    OperFechaConv: TMenuItem;
    GerarArquivopadaDbitoemConta1: TMenuItem;
    RetornodeDbitoemConta1: TMenuItem;
    N7: TMenuItem;
    RelatriodeDiferenaentreAutorizaeseProdutos1: TMenuItem;
    RelatriodeTaxaparaoPrxFechamento1: TMenuItem;
    Relatrio1: TMenuItem;
    Ajuda1: TMenuItem;
    SuporteOnLineChat1: TMenuItem;
    N12: TMenuItem;
    SuporteTcnicoVNC1: TMenuItem;
    VNCWilliamRocha1: TMenuItem;
    N13: TMenuItem;
    QualomeuIP1: TMenuItem;
    Image2: TImage;
    lblAlert: TLabel;
    Mensagens1: TMenuItem;
    Arquivos1: TMenuItem;
    RichEdit1: TRichEdit;
    imgAlert: TImage;
    ImportaoPersonalizadadeConveniados1: TMenuItem;
    VNCAlineSabio1: TMenuItem;
    MSNPopUp1: TMSNPopUp;
    ExibirAlertas1: TMenuItem;
    MSNPopUp2: TMSNPopUp;
    MSNPopUp3: TMSNPopUp;
    MSNPopUp4: TMSNPopUp;
    CadProgDesc: TMenuItem;
    ImportaodeListaABCFarma1: TMenuItem;
    CadProd: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CadFabr: TMenuItem;
    ConfigCupom: TMenuItem;
    EsconderAutorizaes1: TMenuItem;
    UtilOcultAutor: TMenuItem;
    UtilVisualizAutOcult: TMenuItem;
    RelProgDesconto: TMenuItem;
    lblMsg: TLabel;
    ConfiguraodeFidelidade1: TMenuItem;
    CadPremios: TMenuItem;
    este1: TMenuItem;
    VNCAlbertoVeloso1: TMenuItem;
    ResgatedePrmiosFidelidade1: TMenuItem;
    N14: TMenuItem;
    RelExtratoConvEnd: TMenuItem;
    AtualizaPreos1: TMenuItem;
    pec1: TMenuItem;
    VisualizaodeCartesEmitidos1: TMenuItem;
    AtualizaodeProdutos1: TMenuItem;
    ImportaProdutosnoPrograma1: TMenuItem;
    ValeDesconto1: TMenuItem;
    RelValeDescontos: TMenuItem;
    Agenciadores1: TMenuItem;
    VendasAgenciadosporAtivao: TMenuItem;
    ImportaodeConveniadosEmbraer1: TMenuItem;
    N15: TMenuItem;
    ExportaoTransaes1: TMenuItem;
    EmbraerELEB1: TMenuItem;
    axasporEstabelecimento1: TMenuItem;
    EmbraerAviation1: TMenuItem;
    ImportaodeConveniadosComunix1: TMenuItem;
    ImportaodeConveniadosSantoAntonio1: TMenuItem;
    Label3: TLabel;
    ImportaodeEmpresas1: TMenuItem;
    Bandeiras1: TMenuItem;
    CONFABFarmcias1: TMenuItem;
    CONFABticas1: TMenuItem;
    PrefeituraJacarei1: TMenuItem;
    Ultimate1: TMenuItem;
    Trelleborg1: TMenuItem;
    ApoloTubularsSA1: TMenuItem;
    ADPParker1: TMenuItem;
    Adatex1: TMenuItem;
    Univap1: TMenuItem;
    ImportConveniadosProdutor: TMenuItem;
    Volks1: TMenuItem;
    PrefeituraSJC: TMenuItem;
    Radicinylon1: TMenuItem;
    Crylor1: TMenuItem;
    Poliedro1: TMenuItem;
    RelatoriodeExportacaodeFechamento1: TMenuItem;
    ImportacaoMovimentacao: TMenuItem;
    panMenu2: TPanel;
    ImportaodeDependentes1: TMenuItem;
    ProdutosAutorizador1: TMenuItem;
    AtendimentoVendas1: TMenuItem;
    Mexichem1: TMenuItem;
    Eaton2: TMenuItem;
    Cetec1: TMenuItem;
    Hitachi1: TMenuItem;
    Heatcraft1: TMenuItem;
    Bundy1: TMenuItem;
    ProServ1: TMenuItem;
    Vilarreal1: TMenuItem;
    Spani1: TMenuItem;
    Latecoere1: TMenuItem;
    Drogaclin1: TMenuItem;
    IBrasil1: TMenuItem;
    ICaapava1: TMenuItem;
    Facility1: TMenuItem;
    intasTaubate1: TMenuItem;
    Pisani1: TMenuItem;
    Lear1: TMenuItem;
    RelatriodeEmpresasquefecharamnoPerodo1: TMenuItem;
    ModelosCartes1: TMenuItem;
    Alimentao1: TMenuItem;
    Relatriodecreditosdeconveniados1: TMenuItem;
    AnlisedevendasdoPOSTEF1: TMenuItem;
    ExportarPlanilhadecredito1: TMenuItem;
    edtBuscaMenu: TEdit;
    afapem1: TMenuItem;
    SindicatoSindSep1: TMenuItem;
    qPermissoes: TADOQuery;
    Q1: TADOQuery;
    ProtocolodeEntrega1: TMenuItem;
    AutorizaoparaDescontoemFolhaTermodeRecebimento1: TMenuItem;
    Image1: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    AtualizaConsumoMes: TMenuItem;
    ActLctoEntregas: TMenuItem;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Grupos1: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    SobreoSistema1: TMenuItem;
    N27: TMenuItem;
    Image14: TImage;
    MovimentosDuvidosos: TMenuItem;
    ltimosFornecedoresCadastrados1: TMenuItem;
    RelatorioExtratoAlim: TMenuItem;
    ExportaodeArquivosdeFechamento1: TMenuItem;
    RelatrioGerenciais1: TMenuItem;
    UsuriosSenhaPadro1: TMenuItem;
    ManutenodeSenhadeConveniados1: TMenuItem;
    RelatriodeRepassesAnaltico1: TMenuItem;
    BEMESTAR1: TMenuItem;
    CadastrodeEspecialidades1: TMenuItem;
    CadastrodeCredenciados1: TMenuItem;
    CadastrodeBenificirios1: TMenuItem;
    N28: TMenuItem;
    ManutenodeTaxaFixaPadro1: TMenuItem;
    ManutenodeTaxasporEmpresa1: TMenuItem;
    ManutenodeTaxasporBenificirio1: TMenuItem;
    N29: TMenuItem;
    GedorCartesEmpresaExistente1: TMenuItem;
    GerarAtualizarMensalidades1: TMenuItem;
    N30: TMenuItem;
    Relatrios2: TMenuItem;
    Cantinex1: TMenuItem;
    CadastrodeCantinas1: TMenuItem;
    CadastrodeEscolas1: TMenuItem;
    CadastrodeAlunos1: TMenuItem;
    CadastrodeCartes1: TMenuItem;
//    qPermissoesnome: TIBStringField;
//    qPermissoesinclui: TIBStringField;
//    qPermissoesaltera: TIBStringField;
//    qPermissoesacessa: TIBStringField;
//    qPermissoesexclui: TIBStringField;
//    qPermissoesadministrador: TIBStringField;
    Contas1: TMenuItem;
    ImportProdRotinaAbc: TMenuItem;
    tulosaPagar1: TMenuItem;
    qPermissoesnome: TStringField;
    qPermissoesinclui: TStringField;
    qPermissoesaltera: TStringField;
    qPermissoesacessa: TStringField;
    qPermissoesexclui: TStringField;
    qPermissoesadministrador: TStringField;
    CadastroDeParceiros: TMenuItem;
    PagamentodeEstabelecimentosAntecipao1: TMenuItem;
    RelatriodeEstabTaxaBTI1: TMenuItem;
    ReimpressodeRepasses1: TMenuItem;
    PagamentodeEstabelecimentoCanceladoRelatrio1: TMenuItem;
    RelatorioExtratoAlimentacaoMensal: TMenuItem;
    LanamentodeCreditoExtraAlimentao1: TMenuItem;
    Relatrio2: TMenuItem;
    POSTEF1: TMenuItem;
    EstabelecimentosPOS: TMenuItem;
    GeraodeCartes1: TMenuItem;
    AutorizaoparaDescontoemFolhaTermodeRecebimento2: TMenuItem;
    ProtocolodeEntrega2: TMenuItem;
    VisualizaodeCartesEmitidos2: TMenuItem;
    GerarArquivoparacartoestabelecimento1: TMenuItem;
    GerarArquivoparacartoconveniados1: TMenuItem;
    GerarNovosCartes1: TMenuItem;
    ImportaoeGeraodeNovosCartes1: TMenuItem;
    PagamentoBanco: TMenuItem;
    mniNotasFiscais1: TMenuItem;
    tAnexarBoletodaEmpresa1: TMenuItem;
    CadSegEmpresas: TMenuItem;
    mniLanamentoCrditoRenovaoComplementoAlimentao1: TMenuItem;
    ManutenodeTaxas1: TMenuItem;
    Cadastros1: TMenuItem;
    N31: TMenuItem;
    GeraodeCartes2: TMenuItem;
    GerarArquivoparaCartodeBeneficirios1: TMenuItem;
    CarregarBoletoEmpCartoAlimentao1: TMenuItem;
    CarregarLotedeBoletosAlimentao1: TMenuItem;
    PainelRecargaCantinex1: TMenuItem;
    GeraNotadeDbitosEmpresas1: TMenuItem;
    GerarArquivoConciliaoSpani1: TMenuItem;
    ransfernciadeConveniadosOutrasEmpresas1: TMenuItem;
    RelatriodeCrditos1: TMenuItem;
    HabilitarDescontoBemEstarConv1: TMenuItem;
    CancelarDescontoBemEstar1: TMenuItem;
    mniCadastrodeFiliais1: TMenuItem;
    RelatriodeFaturas1: TMenuItem;
    LanamentoDbitosMsAnterior1: TMenuItem;
    PaineldeContrataoBemEstar1: TMenuItem;
    RelatrioBemEstarMensal1: TMenuItem;
    AESPOdonto1: TMenuItem;
    HabDescOdonto: TMenuItem;
    DesabDescOdonto: TMenuItem;
    RelatrioNFServioMunicipal1: TMenuItem;
    AlterarValoreQuantidade1: TMenuItem;
    RelatrioCartadeCircularizaoSpani1: TMenuItem;
    Fechamento1: TMenuItem;
    FechamentoEmpresas1: TMenuItem;
    procedure CadConvClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Image5Click(Sender: TObject);
    procedure IMGEmpClick(Sender: TObject);
    procedure IMGForClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Cons_FechaClick(Sender: TObject);
    procedure CalculaSaldoExecute(Sender: TObject);
    procedure exportadebitoExecute(Sender: TObject);
    procedure importadebitoExecute(Sender: TObject);
    procedure ActContasPagarExecute(Sender: TObject);
    procedure ActRelContasPagarExecute(Sender: TObject);
    procedure ActVencContasExecute(Sender: TObject);
    procedure ActEmissaoReciboExecute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure AtualizaodeDatasdeFechamentoseVencimentos1Click(Sender: TObject);
    procedure CadAdmClick(Sender: TObject);
    procedure CadGrupProdClick(Sender: TObject);
    procedure CadEmpClick(Sender: TObject);
    procedure CadSegClick(Sender: TObject);
    procedure SairClick(Sender: TObject);
    procedure CadCredClick(Sender: TObject);
    procedure CadFormaPagtoClick(Sender: TObject);
    procedure ConfigClick(Sender: TObject);
    procedure BaixaEmpClick(Sender: TObject);
    procedure BaixaFornecClick(Sender: TObject);
    procedure CadOperClick(Sender: TObject);
    procedure CadGrupOperClick(Sender: TObject);
    procedure Medicao_usoClick(Sender: TObject);
    procedure RelExtratoForClick(Sender: TObject);
    procedure Cons_Mov_FornecClick(Sender: TObject);
    procedure CadCartoesClick(Sender: TObject);
    procedure GeraTaxaConvClick(Sender: TObject);
    procedure LancaCPMFClick(Sender: TObject);
    procedure ActLancDescForClick(Sender: TObject);
    procedure OperGeraCartConvClick(Sender: TObject);
    procedure OperGeraCartFornClick(Sender: TObject);
    procedure ImportConveniadosClick(Sender: TObject);
    procedure ImportaLimiteClick(Sender: TObject);
    procedure ExportaCobrancasClick(Sender: TObject);
    procedure GeraDocClick(Sender: TObject);
    procedure ActLctoEntregasClick(Sender: TObject);
    procedure RelExtratoEmpClick(Sender: TObject);
    procedure RelExtratoUserClick(Sender: TObject);
    procedure RelExtratoDemitClick(Sender: TObject);
    procedure ctExtForVencEmpClick(Sender: TObject);
    procedure TotPagForClick(Sender: TObject);
    procedure Action1Click(Sender: TObject);
    procedure RelMediaPorEmpClick(Sender: TObject);
    procedure Action2Click(Sender: TObject);
    procedure Consulta_emp_fornecClick(Sender: TObject);
    procedure Consulta_for_empClick(Sender: TObject);
    procedure Consulta_autorClick(Sender: TObject);
    procedure Consulta_fechaClick(Sender: TObject);
    procedure ConsContaCorrClick(Sender: TObject);
    procedure Cons_Mov_EmpresClick(Sender: TObject);
    procedure Cons_Mov_ConvClick(Sender: TObject);
    procedure AuditorClick(Sender: TObject);
    procedure ActAuditDatasClick(Sender: TObject);
    procedure Aud_gruposempresasClick(Sender: TObject);
    procedure CadRelConfClick(Sender: TObject);
    procedure ConfValClick(Sender: TObject);
    procedure ConsConferenciaClick(Sender: TObject);
    procedure Alteracoes_SisClick(Sender: TObject);
    procedure Rel_Alt_LinClick(Sender: TObject);
    procedure LogoffClick(Sender: TObject);
    procedure ExportXMLClick(Sender: TObject);
    procedure RegistrosApagadosClick(Sender: TObject);
    procedure ConfigDebCCClick(Sender: TObject);
    procedure IMGCONVClick(Sender: TObject);
    procedure IMGFechaClick(Sender: TObject);
    procedure FinancFaturaClick(Sender: TObject);
    procedure ManutenodeAutorizaes1Click(Sender: TObject);
    procedure CriarDatasdeFechamentoEmpresasFornecedores1Click(Sender: TObject);
    procedure AnlisedeAtividadeporEmpresa1Click(Sender: TObject);
    procedure ProdutosporAutorizao1Click(Sender: TObject);
    procedure VisualizarLanamentos1Click(Sender: TObject);
    procedure LancamentosClick(Sender: TObject);
    procedure FinancPgtoFornAbertClick(Sender: TObject);
    procedure FinancPgtoFornRealizClick(Sender: TObject);
    procedure CadTaxasClick(Sender: TObject);
    procedure FinancPgtoFornPrevisClick(Sender: TObject);
    procedure CadDespAdmClick(Sender: TObject);
    procedure ConfernciadeDespesasdaAdministradora1Click(Sender: TObject);
    procedure ConfiguraoLocal1Click(Sender: TObject);
    procedure OperFechaEmpClick(Sender: TObject);
    procedure OperFechaConvClick(Sender: TObject);
    procedure GerarArquivopadaDbitoemConta1Click(Sender: TObject);
    procedure RetornodeDbitoemConta1Click(Sender: TObject);
    procedure RelatriodeDiferenaentreAutorizaeseProdutos1Click(Sender: TObject);
    procedure RelatriodeTaxaparaoPrxFechamento1Click(Sender: TObject);
    procedure Relatrio1Click(Sender: TObject);
    procedure SuporteOnLineChat1Click(Sender: TObject);
    procedure VNCWilliamRocha1Click(Sender: TObject);
    procedure QualomeuIP1Click(Sender: TObject);
    procedure Mensagens1Click(Sender: TObject);
    procedure Arquivos1Click(Sender: TObject);
    procedure lblAlertClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure imgAlertClick(Sender: TObject);
    procedure ImportaoPersonalizadadeConveniados1Click(Sender: TObject);
    procedure VNCAlineSabio1Click(Sender: TObject);
    procedure ExibirAlertas1Click(Sender: TObject);
    procedure CadProgDescClick(Sender: TObject);
    procedure ImportaodeListaABCFarma1Click(Sender: TObject);
    procedure CadProdClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure speedMenu1Click(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure ApplicationEvents1Restore(Sender: TObject);
    procedure CadFabrClick(Sender: TObject);
    procedure ConfigCupomClick(Sender: TObject);
    procedure UtilVisualizAutOcultClick(Sender: TObject);
    procedure UtilOcultAutorClick(Sender: TObject);
    procedure RelProgDescontoClick(Sender: TObject);
    procedure lblMsgClick(Sender: TObject);
    procedure ConfiguraodeFidelidade1Click(Sender: TObject);
    procedure CadPremiosClick(Sender: TObject);
    procedure EmAbertoTeste1Click(Sender: TObject);
    procedure RealizadosTeste1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure VNCAlbertoVeloso1Click(Sender: TObject);
    procedure ResgatedePrmiosFidelidade1Click(Sender: TObject);
    procedure MovimentosDuvidososClick(Sender: TObject);
    procedure RelExtratoConvEndClick(Sender: TObject);
    procedure AtualizaPreos1Click(Sender: TObject);
    procedure pec1Click(Sender: TObject);
    procedure VisualizaodeCartesEmitidos1Click(Sender: TObject);
    procedure ImportaProdutosnoPrograma1Click(Sender: TObject);
    procedure ValeDesconto1Click(Sender: TObject);
    procedure RelValeDescontosClick(Sender: TObject);
    procedure Agenciadores1Click(Sender: TObject);
    procedure VendasAgenciadosporAtivaoClick(Sender: TObject);
    procedure CadBancoClick(Sender: TObject);
    procedure ImportaodeConveniadosEmbraer1Click(Sender: TObject);
    procedure EmbraerELEB1Click(Sender: TObject);
    procedure axasporEstabelecimento1Click(Sender: TObject);
    procedure EmbraerAviation1Click(Sender: TObject);
    procedure ImportaodeConveniadosComunix1Click(Sender: TObject);
    procedure ImportaodeConveniadosSantoAntonio1Click(Sender: TObject);
    procedure ImportaodeEmpresas1Click(Sender: TObject);
    procedure Bandeiras1Click(Sender: TObject);
    procedure CONFABFarmcias1Click(Sender: TObject);
    procedure CONFABticas1Click(Sender: TObject);
    procedure ImportConveniadosProdutorClick(Sender: TObject);
    procedure RelatoriodeExportacaodeFechamento1Click(Sender: TObject);
    procedure ImportacaoMovimentacaoClick(Sender: TObject);
    procedure ImportaodeDependentes1Click(Sender: TObject);
    procedure ProdutosAutorizador1Click(Sender: TObject);
    procedure AtendimentoVendas1Click(Sender: TObject);
    procedure Mexichem1Click(Sender: TObject);
    procedure Lear1Click(Sender: TObject);
    procedure RelatriodeEmpresasquefecharamnoPerodo1Click(Sender: TObject);
    procedure ModelosCartes1Click(Sender: TObject);
    procedure Relatriodecreditosdeconveniados1Click(Sender: TObject);
    procedure AnlisedevendasdoPOSTEF1Click(Sender: TObject);
    procedure ExportarPlanilhadecredito1Click(Sender: TObject);
    procedure edtBuscaMenuKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ProtocolodeEntrega1Click(Sender: TObject);
    procedure AutorizaoparaDescontoemFolhaTermodeRecebimento1Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image6Click(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure Image8Click(Sender: TObject);
    procedure AtualizaConsumoMesClick(Sender: TObject);
    procedure Grupos1Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image10Click(Sender: TObject);
    procedure Image11Click(Sender: TObject);
    procedure Image13Click(Sender: TObject);
    procedure Image14Click(Sender: TObject);
    procedure ltimosFornecedoresCadastrados1Click(Sender: TObject);
    procedure RelatorioExtratoAlimClick(Sender: TObject);
    procedure ExportaodeArquivosdeFechamento1Click(Sender: TObject);
    procedure PagamentodeFornecedores1Click(Sender: TObject);
    procedure RelatrioGerenciais1Click(Sender: TObject);
    procedure UsuriosSenhaPadro1Click(Sender: TObject);
    procedure ManutenodeSenhadeConveniados1Click(Sender: TObject);
    procedure RelatriodeRepassesAnaltico1Click(Sender: TObject);
    procedure CadastrodeCredenciados1Click(Sender: TObject);
    procedure CadastrodeBenificirios1Click(Sender: TObject);
    procedure CadastrodeEspecialidades1Click(Sender: TObject);
    procedure ManutenodeTaxaFixaPadro1Click(Sender: TObject);
    procedure ManutenodeTaxasporEmpresa1Click(Sender: TObject);
    procedure ManutenodeTaxasporBenificirio1Click(Sender: TObject);
    procedure GedorCartesEmpresaExistente1Click(Sender: TObject);
    procedure CadastrodeCantinas1Click(Sender: TObject);
    procedure CadastrodeEscolas1Click(Sender: TObject);
    procedure CadastrodeAlunos1Click(Sender: TObject);
    procedure CadastrodeCartes1Click(Sender: TObject);
    procedure ImportProdRotinaAbcClick(Sender: TObject);
    procedure CadastroDeParceirosClick(Sender: TObject);
    procedure PagamentodeEstabelecimentosAntecipao1Click(Sender: TObject);
    procedure ReimpressodeRepasses1Click(Sender: TObject);
    procedure PagamentodeEstabelecimentoCanceladoRelatrio1Click(Sender: TObject);
    procedure RelatriodeEstabTaxaBTI1Click(Sender: TObject);
    procedure RelatorioExtratoAlimentacaoMensalClick(Sender: TObject);
    procedure LanamentodeCreditoExtraAlimentao1Click(Sender: TObject);
    procedure Relatrio2Click(Sender: TObject);
    procedure EstabelecimentosPOSClick(Sender: TObject);
    procedure GerarArquivoparacartoconveniados1Click(Sender: TObject);
    procedure ProtocolodeEntrega2Click(Sender: TObject);
    procedure AutorizaoparaDescontoemFolhaTermodeRecebimento2Click(Sender: TObject);
    procedure GerarNovosCartes1Click(Sender: TObject);
    procedure ImportaoeGeraodeNovosCartes1Click(Sender: TObject);
    procedure PagamentoBancoClick(Sender: TObject);
    procedure mniNotasFiscais1Click(Sender: TObject);
    procedure tAnexarBoletodaEmpresa1Click(Sender: TObject);
    procedure CadSegEmpresasClick(Sender: TObject);
    procedure CarregarBoletoEmpCartoAlimentao1Click(Sender: TObject);
    procedure CarregarLotedeBoletosAlimentao1Click(Sender: TObject);
    procedure GerarAtualizarMensalidades1Click(Sender: TObject);
    procedure PainelRecargaCantinex1Click(Sender: TObject);
    procedure GeraNotadeDbitosEmpresas1Click(Sender: TObject);
    procedure GerarArquivoConciliaoSpani1Click(Sender: TObject);
    procedure ransfernciadeConveniadosOutrasEmpresas1Click(
      Sender: TObject);
    procedure RelatriodeCrditos1Click(Sender: TObject);
    procedure HabilitarDescontoBemEstarConv1Click(Sender: TObject);
    procedure CancelarDescontoBemEstar1Click(Sender: TObject);
    procedure mniCadastrodeFiliais1Click(Sender: TObject);
    procedure RelatriodeFaturas1Click(Sender: TObject);
    procedure LanamentoDbitosMsAnterior1Click(Sender: TObject);
    procedure PaineldeContrataoBemEstar1Click(Sender: TObject);
    procedure RelatrioBemEstarMensal1Click(Sender: TObject);
    procedure HabDescOdontoClick(Sender: TObject);
    procedure mniLanamentoCrditoRenovaoComplementoAlimentao1Click(
      Sender: TObject);
    procedure RelatrioNFServioMunicipal1Click(Sender: TObject);
    procedure AlterarValoreQuantidade1Click(Sender: TObject);
    procedure DesabDescOdontoClick(Sender: TObject);
    procedure RelatrioCartadeCircularizaoSpani1Click(Sender: TObject);
    procedure FechamentoEmpresas1Click(Sender: TObject);

    
  private
    { Private declarations }
    procedure LinkTo(const aAdress: string);
    procedure ValidacaoDoSistema;
    procedure PedeSenhaLibera(DataVence: TDateTime);
    procedure MostrarForm(Classe: TFormClass; MenuName: string; Modal: Boolean);
    procedure MostrarFormExportacao(Classe: TFormClass; MenuName: string; Modal: Boolean);
    procedure DefinirPermissoes(Form: TFCad; MenuName: string);
    procedure AtualizarPermissaoAcesso;
    function GetMenuName(Item: TMenuITem): string;
    procedure AtualizarModulos;
    function IsMenuJanela(Component: TComponent): Boolean;
    procedure DesabilitarFilhos(Item: TMenuITem);
    procedure HabilarAllMenus;
    procedure AbrirVNC(porta: integer);
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    procedure Alertas;
    procedure BloqEmprPorFat;
    procedure CarregaMenuRel;
    procedure VerificaBanco;
  public
    { Public declarations }
    vCadConv, vRelCred, vRelGerencial, vRelRepasseAnalitico2, vRelRepasseAnalitico, vManutSenhaConv, vCadEmp, vCadEstab, vGeracaoCartao, vFechtoEmp, vMantFat, vPagEstab, vNFAberto, vRelUsuarioSenhaPadrao, vFrmcadCredBemEstar, vFCadBenificiarioBemEstar, vFCadEspecialidadesBemEstar, vFCadManutTaxaFixaPadraoBemEstar, vFCadManutTaxasMensalidadeEmpresa, vCadParceirosBemEstar, vCadCartaoBemEstar, vFManutTaxaBenificiarioBemEstar, vFGeraCartaBemEstar, vCadAluno, vCadCantina, vCadEscolas, vCadImportABC: Boolean;
    vFormRepasseAntecipacao,vfrmCancelarConvBemEstarDebitos,vFAESPOdontoCancelar, vFLogBemEstarCompras,vFLancamentoDebitoMesAnterior,vfrmConvBemEstarDebitos,vFLogRecargaCantinex,vManutencaoEmpConv,vExportConciliacaoSpani,vEmpGeraNotaDebito ,vFormReimpresaoRepasse,vFGeraLancamentoCartaBemEstar ,vFormSegmentoEmpresa, vCadGeracaoCartaoNovo, vCadImportEGeraNovoCartao, vFormRepassesCancelados, vFormRelInadimplentesBti, vGeracaoCartaoNovo, vFormIdenticaoBancaria,vFAESPOdontoDebitos, vFRelBemEstar: Boolean;
    FDownX, FDownY: Integer;
    FDragging: Boolean;
    function GetPersonalFolder: string;
    function focarFilho(frm: TFormClass): Boolean;
    function LiberaMenu(menuItem: TMenuItem; busca: string): Boolean;
  end;

var
  FMenu: TFMenu;
  inc, alt, exc: Boolean; //Variaveis para gravar a permissao do operador por modulo.
  Operador: TOperador; //Operador logado no sistema.

implementation

uses
  USenha, DM, USenhaLibera, UChangeLog, UCadSeg, UCadAdm, UCadEmp, UCadCred,
  FEntregaNF, UCadConv, ULancIndiv, URelExtratoEmp, URelProtocoloEntrega,
  URelExtratoFor, URelExtratoConv, UGeraCartConv, UCadProduto2, UCadCartoes,
  uRelEmpFecharamNoPeriodo, UManutAutors, FAtendimentoVendas, UFechamento,
  UCadProgramas, UMovimentosDuvidosos, uImportProdXls, FCadProdutosAutorizador,
  uCadModelosCartoes, uPgtoEstabAberto, URelAutorizacoesDesconto, UFatura,
  UAtualizarConsumoMes, UCadTaxas, UCadFormaPgto, UCadBanco, UCadGrupoProd,
  UCadGrupoEstab, UCadBandeiras, UCadOper, UCadGrupoOper,
  UImportConveniadosProdutor, FExportPlanCred, URelEmpresaCad, URelCredCad,
  uExportacaoConfabFarmacias, uExportacaoConfabOticas, FRelCredAssociado,
  URelExtratoAlim, uExportEstabPadrao, FRelGerenciais, URelSenhaConv,
  UManutSenhaConv, URelRepasseAnalitico, URelRepasseAnalitico2, UConfig,
  UCad_CredBemEstar, UCadEspecialidadesBemEstar, UCadBenBemEstar,
  UManutTaxaFixaPadraoBemEstar, UManutTaxasMensalidadeEmpresa,
  UManutTaxasMensalidadesBenificiario, UGeraCartaoBemEstar, UCadCantina,
  UCadEscolas, UCadProduto, UCadCartoesBemEstar, uImportProdAbc,
  UCadParceirosBemEstar, uPgtoEstabAbertoAntecipacao, uRepassesReimpressao,
  uRepassesCancelados, URelatorioInadimplentesBTI, URelExtratoAlimentacaoMensal,
  ULancamentoExtraAlimentacao, FRelAlimentacaoCredito, UCadCredTef,
  UGeraCartConvNovo, URelProtocoloEntregaNovo, URelAutorizacoesDescontoNovo,
  UCadGeraCartaoNovo, UImportEGeracaoDeCartao, UIdentificacaoBancariaRepasse,
  URelNotaFiscal, UAnexarBoleto, ULancamentoAlimentacao, UAnexarLoteBoletoAlimentacao,
  UAnexarBoletoAlimentacao, 
  UGeraLancamentoCartaoBemEstar, UCadAluno, ULogCantinex,
  uEmpGeraNotaDebito, uExportConciliacaoSpani, UManutencaoEmpConv,
  UmanutencaoEmpConv2, FRelBemEstarCreditos, UConvBemEstarDebitos,
  UCancelarConvBemEstarDebitos, UcadFil,FRelFatura,
  ULancamentoDebitoMesAnterior,ULogBemEstarCompras, URelNFMunic, URelBemEstar,
  UAESPOdontoDebitos, UAlteraValorEQtde, UCancelarConvAESPDebitos, UFRealCartaCircularizacaoSpani, FrmFechamentoEmpresas;


{$R *.dfm}

function TFMenu.focarFilho(frm: TFormClass): Boolean;
var
  I: Integer;
begin
  result := false;
  for I := 0 to Self.MDIChildCount - 1 do
    if (Self.MDIChildren[I].ClassType = frm) then
    begin
      Self.MDIChildren[I].Show;
      result := true;
      Exit;
    end;
end;

procedure TFMenu.CadConvClick(Sender: TObject);
begin
  if not vCadConv then
    MostrarForm(TFCadConv, CadConv.Name, False)
  else if not focarFilho(TFCadConv) then
    MsgInf('A tela de "Cadastro de conveniados" j� est� aberta.');
end;

procedure TFMenu.FormShow(Sender: TObject);
var
  NumMsg: Integer;
  dataArq: string;
  appNm: string;
  sistema: string;
  SR: TSearchRec;
  ini: TIniFile;
begin

  FSenha := TFSenha.Create(self);
  appNm := Application.ExeName;
  FSenha.LabVers.Caption := 'Vers�o ' + IntToStr(GetMajorVersionExe(appNm)) + '.' + IntToStr(GetMinorVersionExe(appNm)) + ' Revis�o ' + IntToStr(GetBuildVersionExe(appNm));
  FSenha.BringToFront;
  FSenha.ShowModal;
  if FSenha.ModalResult = mrOk then
  begin
    if FSenha.EdOper.Text = 'ADM' then
    begin
      Operador.Nome := FSenha.EdOper.Text;
      Operador.ID := 0;
      Operador.IsAdmin := True;
    end
    else
    begin
      Operador.Nome := FSenha.EdOper.Text;
      Operador.ID := FSenha.QLoginusuario_id.AsInteger;
      Operador.IsAdmin := FSenha.QLoginADMINISTRADOR.AsString = 'S';
    end;
    DMConexao.AdoQry.Close;
    if(Operador.ID = 74) then
      GerarArquivoConciliaoSpani1.Visible := True;
    sistema := 'ADMCONVENIO';
    DMConexao.ExecuteSql('INSERT INTO LOG_ACESSO_CONVENIO VALUES ('+IntToStr(Operador.ID)+', '''+Operador.Nome+''', GETDATE(),'''+sistema+''')');
    DMConexao.AdoQry.Close;
    ini := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'admcartao.ini');
    FMenu.StatusBar1.Panels[0].Text := 'Operador: ' + Operador.Nome + '     ID: ' + IntToStr(Operador.ID) + '     Administrador: ' + cartao_util.iif(Operador.IsAdmin, 'Sim', 'N�o');
    FMenu.StatusBar1.Panels[2].Text := 'Vers�o: ' + IntToStr(GetMajorVersionExe(Application.ExeName)) + '.' + IntToStr(GetMinorVersionExe(Application.ExeName));
    FMenu.StatusBar1.Panels[3].Text := 'Revis�o: ' + IntToStr(GetBuildVersionExe(Application.ExeName));

    if FindFirst(Application.ExeName, faAnyFile, SR) = 0 then
    begin
      FMenu.StatusBar1.Panels[1].Text := 'Data Compila��o: ' + DateTimeToStr(FileTimeToDTime(SR.FindData.ftLastWriteTime));

    end;
    FindClose(SR);
    //Dm1.QConf_Grade.Close;
    //Dm1.QConf_Grade.Sql.Text :=  'Select * from conf_grades where oper_id = '+IntToStr(Operador.ID);
    //Dm1.QConf_Grade.Open;
    FSenha.QLogin.Close;
    FSenha.Free;
    AtualizarPermissaoAcesso;
  end
  else
  begin
    Application.Terminate;
  end;

  DMConexao.Config.Open;
  NumMsg := DMConexao.ExecuteScalar('select count(*) from mensagens where destinatario_tp = ''ADMINISTRADORA'' ' + 'and coalesce(lido,''N'')=''N'' and coalesce(apagado,''N'')=''N'' ');
  if NumMsg > 0 then
  begin
    lblMsg.Caption := (IntToStr(NumMsg)) + ' nova(s) mensagen(s)';
    lblMsg.Visible := True;
  end
  else
    lblMsg.Visible := False;
  if DMConexao.ConfigEXIBE_ALERTAS.AsString = 'S' then
    Alertas;
  DMConexao.Config.Close;

end;

procedure TFMenu.VerificaBanco;
begin
  if not DMConexao.ExisteCampo('CONFIG', 'CUPOM') then
  begin
    DMConexao.ExecuteSql('alter table CONFIG add CUPOM blob ');
    DMConexao.ExecuteSql('update CONFIG set cupom = cast('' '' as blob)');
  end;

  if not DMConexao.ExisteCampo('CONFIG', 'IMPRIME_CUPOM_FIDELIZE') then
  begin
    DMConexao.ExecuteSql('alter table CONFIG add IMPRIME_CUPOM_FIDELIZE char(1) ');
    DMConexao.ExecuteSql('update CONFIG set IMPRIME_CUPOM_FIDELIZE = ''S'' ');
  end;

  if not DMConexao.ExisteCampo('CONFIG', 'USA_GRUPO_PROD') then
  begin
    DMConexao.ExecuteSql('alter table CONFIG add USA_GRUPO_PROD char(1) ');
    DMConexao.ExecuteSql('update CONFIG set USA_GRUPO_PROD = ''N'' ');
  end;

  if not DMConexao.ExisteCampo('CONVENIADOS', 'VALE_DESCONTO') then
  begin
    DMConexao.ExecuteSql('alter table CONVENIADOS add VALE_DESCONTO char(1) ');
    DMConexao.ExecuteSql('update CONVENIADOS set VALE_DESCONTO = ''N'' ');
  end;

  if not DMConexao.ExisteCampo('CONVENIADOS', 'LIBERA_GRUPOSPROD') then
  begin
    DMConexao.ExecuteSql('alter table CONVENIADOS add LIBERA_GRUPOSPROD char(1) ');
    DMConexao.ExecuteSql('update CONVENIADOS set LIBERA_GRUPOSPROD = ''N'' ');
  end;

  if not DMConexao.ExisteCampo('EMPRESAS', 'AGENCIADOR_ID') then
  begin
    DMConexao.ExecuteSql('alter table EMPRESAS add AGENCIADOR_ID integer ');
  end;

  if not DMConexao.ExisteCampo('EMPRESAS', 'SOM_PROD_PROG') then
  begin
    DMConexao.ExecuteSql('alter table EMPRESAS add SOM_PROD_PROG char(1) ');
    DMConexao.ExecuteSql('update EMPRESAS set SOM_PROD_PROG = ''N'' ');
  end;

end;

procedure TFMenu.Alertas;
var
  NumMsg, Tam: Integer;
begin
  Tam := 0;
  // Mensagens n�o lidas
  NumMsg := DMConexao.ExecuteScalar('select count(*) from mensagens where destinatario_tp = ''ADMINISTRADORA'' ' + 'and coalesce(lido,''N'')=''N'' and coalesce(apagado,''N'')=''N'' ');
  if ((NumMsg > 0) and (Mensagens1.Enabled)) then
  begin
    MSNPopUp1.Options := MSNPopUp1.Options - [msnAllowHyperlink, msnAutoOpenURL];
    MSNPopUp1.Text := 'Existem ' + IntToStr(NumMsg) + ' mensage' + cartao_util.iif(NumMsg > 1, 'ns', 'm') + ' n�o lida' + cartao_util.iif(NumMsg > 1, 's!', '!');
    MSNPopUp1.OnClick := Mensagens1Click;
    MSNPopUp2.PopupStartY := Tam;
    MSNPopUp1.ShowPopUp;
    tam := Tam + 125;
    NumMsg := 0;
  end;
  DMConexao.Config.Open;
  if not (DMConexao.ConfigUSA_NOVO_FECHAMENTO.AsString = 'N') then
  begin
    // Fechamentos para hoje
    NumMsg := DMConexao.ExecuteScalar('select count(*) from contacorrente cc where coalesce(cc.fatura_id,0)=0 and coalesce(cc.baixa_conveniado,''N'')=''N'' and cc.data_fecha_emp = ' + FormatDataIB(Now));
    if ((NumMsg > 0) and (OperFechaEmp.Enabled)) then
    begin
      MSNPopUp2.Options := MSNPopUp2.Options - [msnAllowHyperlink, msnAutoOpenURL];
      MSNPopUp2.Text := 'Existem ' + IntToStr(NumMsg) + ' autoriza�' + cartao_util.iif(NumMsg > 1, '�es', '�o') + ' com fechamento para hoje!';
      MSNPopUp2.OnClick := OperFechaEmpClick;
      MSNPopUp2.PopupStartY := Tam;
      MSNPopUp2.ShowPopUp;
      tam := Tam + 125;
      NumMsg := 0;
    end;
    // Fechamentos em atraso
    NumMsg := DMConexao.ExecuteScalar('select count(*) from contacorrente cc where coalesce(cc.fatura_id,0)=0 and coalesce(cc.baixa_conveniado,''N'')=''N'' and cc.data_fecha_emp < ' + FormatDataIB(Now));
    if ((NumMsg > 0) and (OperFechaEmp.Enabled)) then
    begin
      MSNPopUp3.Options := MSNPopUp3.Options - [msnAllowHyperlink, msnAutoOpenURL];
      MSNPopUp3.Text := 'Existem ' + IntToStr(NumMsg) + ' autoriza�' + cartao_util.iif(NumMsg > 1, '�es', '�o') + ' com fechamento em atraso!';
      MSNPopUp3.OnClick := OperFechaEmpClick;
      MSNPopUp3.PopupStartY := Tam;
      MSNPopUp3.ShowPopUp;
      tam := Tam + 125;
      NumMsg := 0;
    end;
    // Faturas Vencidas
    NumMsg := DMConexao.ExecuteScalar('select count(*) from fatura fat where coalesce(fat.apagado,''N'')=''N'' and coalesce(fat.baixada,''N'')=''N'' and fat.data_vencimento <= ' + FormatDataIB(Now));
    if ((NumMsg > 0) and (FinancFatura.Enabled)) then
    begin
      MSNPopUp4.Options := MSNPopUp4.Options - [msnAllowHyperlink, msnAutoOpenURL];
      MSNPopUp4.Text := 'Existem ' + IntToStr(NumMsg) + ' fatura' + cartao_util.iif(NumMsg > 1, 's ', ' ') + 'vencida' + cartao_util.iif(NumMsg > 1, 's !', ' !');
      MSNPopUp4.OnClick := FinancFaturaClick;
      MSNPopUp4.PopupStartY := Tam;
      MSNPopUp4.ShowPopUp;
      tam := Tam + 125;
      NumMsg := 0;
    end;
    BloqEmprPorFat;
  end;
end;

procedure TFMenu.BloqEmprPorFat;
begin
  if ((not DMConexao.ConfigDIAS_BLOQ_EMPR_FAT.IsNull) and (DMConexao.ConfigDIAS_BLOQ_EMPR_FAT.AsInteger > 0)) then
  begin
    Q1.Close;
    Q1.SQL.Clear;
    Q1.SQL.Add(' select e.nome, f.* from fatura f join empresas e on e.empres_id = f.id ');
    Q1.SQL.Add(' where coalesce(f.apagado,''N'') <> ''S'' and coalesce(f.baixada,''N'') <> ''S'' and coalesce(f.tipo,''E'')=''E'' ');
    Q1.SQL.Add(' and (f.data_vencimento + ' + DMConexao.ConfigDIAS_BLOQ_EMPR_FAT.AsString + ') <= current_timestamp ');
    Q1.SQL.Add(' and e.liberada <> ''N'' ');
    Q1.Open;
    if not Q1.IsEmpty then
    begin
      Q1.First;
      while not Q1.Eof do
      begin
        if MsgSimNao('A empresa ' + Q1.FieldByName('ID').AsString + ' - ' + Q1.FieldByName('NOME').AsString + ' possui a fatura n� ' + Q1.FieldByName('FATURA_ID').AsString + sLineBreak + 'no valor de R$ ' + FormatDinBR(Q1.FieldByName('VALOR').AsCurrency) + ' com vencimento em ' + FormatDataBR(Q1.FieldByName('DATA_VENCIMENTO').AsDateTime) + ', vencida a mais de ' + DMConexao.ConfigDIAS_BLOQ_EMPR_FAT.AsString + ' dias.' + sLineBreak + 'Deseja que o sistema bloqueie essa empresa?') then
        begin
          DMConexao.ExecuteSql(' update empresas set liberada = ''N'' where empres_id = ' + Q1.FieldByName('ID').AsString);
          DMConexao.GravaLog('FCadEmp', 'Liberada', 'S', 'N', Operador.Nome, 'Altera��o', Q1.FieldByName('ID').AsString, Self.Name);
        end;
        Q1.Next;
      end;
      Q1.Close;
    end;
  end;
end;

procedure TFMenu.Image4Click(Sender: TObject);
begin
  if not vCadEstab then
    MostrarForm(TFCadCred, TMenuItem(sender).Name, False)
  else if not focarFilho(TFCadCred) then
    MsgInf('A tela de "Cadastro de Estabelecimentos" j� est� aberta.');
end;

procedure TFMenu.Image5Click(Sender: TObject);
begin
  if not vGeracaoCartao then
    MostrarForm(TFGeraCartConv, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Gera��o de Arquivo para cart�o de conveniados" j� est� aberta.');  //comentado ariane

  WinExec('notepad.exe', SW_SHOWNORMAL);
end;

procedure TFMenu.IMGEmpClick(Sender: TObject);
begin
  CadEmpClick(CadEmp);
end;

procedure TFMenu.IMGForClick(Sender: TObject);
begin
  CadCredClick(CadCred);
end;

procedure TFMenu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMConexao.AdoConfGrade.Close;
end;

procedure TFMenu.FormCreate(Sender: TObject);
begin
  Self.WindowState := wsMaximized;
  ShortDateFormat := 'dd/mm/yyyy';
  Self.Caption := 'Administra��o de Conv�nios - Vers�o ' + IntToStr(GetMajorVersionExe(Application.ExeName)) + '.' + IntToStr(GetMinorVersionExe(Application.ExeName)) + ' - Revis�o ' + IntToStr(GetBuildVersionExe(Application.ExeName));

  ValidacaoDoSistema;
  DMConexao.Config.Open;
  if DMConexao.Config.FieldByName('USA_NOVO_FECHAMENTO').AsString = 'S' then
  begin
    BaixaEmp.Visible := False;
    BaixaFornec.Visible := False;
    //N4.Visible:= False;
  end
  else
  begin
    BaixaEmp.Visible := True;
    BaixaFornec.Visible := True;
    //N4.Visible:= True;
  end;
  if DMConexao.ConfigUSA_PROG_DESC.AsString = 'S' then
    CadProgDesc.Visible := True
  else
    CadProgDesc.Visible := False;
  DMConexao.Config.Edit;
  DMConexao.ConfigULTIMA_EXE.AsString := Crypt('E', DateToStr(Date), DMConexao.ConfigCOD_ADM_BIG.AsString);
  if DMConexao.ConfigVERSAO.AsInteger <> GetExeVersion then
  begin
    AtualizarModulos;
    DMConexao.ConfigVERSAO.AsInteger := GetExeVersion; //atualiza a versao que est� no banco de dados para a vers�o do exe.
  end;
  DMConexao.Config.Post;
  DMConexao.Config.Close;
  vCadConv := False;
  vCadEmp := False;
  vManutSenhaConv := False;
  vCadEstab := False;
  vGeracaoCartao := False;
  vFechtoEmp := False;
  vMantFat := False;
  vPagEstab := False;
  vNFAberto := False;
  lblAlert.Caption := 'Administradora: ' + DMConexao.ExecuteScalar('SELECT TOP 1 RAZAO FROM ADMINISTRADORA');

end;

procedure TFMenu.Cons_FechaClick(Sender: TObject);
begin
  Consulta_fecha.Click;
end;

procedure TFMenu.CalculaSaldoExecute(Sender: TObject);
begin
  //COMENTADO ARIANE MostrarForm(TFCalcSaldo,TMenuItem(sender).Name,True);
end;

procedure TFMenu.ValidacaoDoSistema;
var
  data_valid: TDateTime;
  dia, mes, ano: string;
begin
  //VerificaBanco;
  DMConexao.Config.Open;
  if Trim(DMConexao.ConfigRODAR_ATE.AsString) = '' then
  begin
    PedeSenhaLibera(Date - 1);
  end
  else
  begin
    try
      dia := Copy(DMConexao.ConfigRODAR_ATE.AsString, 1, Pos('-', DMConexao.ConfigRODAR_ATE.AsString) - 1);
      dia := Crypt('D', dia, DMConexao.ConfigCOD_ADM_BIG.AsString);
      mes := Copy(DMConexao.ConfigRODAR_ATE.AsString, Pos('-', DMConexao.ConfigRODAR_ATE.AsString) + 1, length(DMConexao.ConfigRODAR_ATE.AsString));
      mes := copy(mes, 1, Pos('-', mes) - 1);
      mes := Crypt('D', mes, DMConexao.ConfigCOD_ADM_BIG.AsString);
      ano := Copy(DMConexao.ConfigRODAR_ATE.AsString, Pos('-', DMConexao.ConfigRODAR_ATE.AsString) + 1, length(DMConexao.ConfigRODAR_ATE.AsString));
      ano := Copy(ano, Pos('-', ano) + 1, length(ano));
      ano := Crypt('D', ano, DMConexao.ConfigCOD_ADM_BIG.AsString);
      ano := '20' + ano;
      data_valid := EncodeDate(StrToInt(ano), StrToInt(mes), StrToInt(dia));
      //if Date < StrToDate(Crypt('D', DMConexao.ConfigULTIMA_EXE.AsString, DMConexao.ConfigCOD_ADM_BIG.AsString)) then
      //begin
      //  DMConexao.Config.edit;
      //  DMConexao.ConfigULTIMA_EXE.AsString := '';
      //  DMConexao.Config.Post;
      //  ShowMessage('Aten��o' + 'A data do sistema foi alterada.');
      //  PedeSenhaLibera(data_valid);
      //end
      //else if data_valid < Date then
      //begin
      //  PedeSenhaLibera(data_valid)
      //end
      //else if ((data_valid = Date) or ((data_valid - 10) <= Date)) then
      //begin
      //  PedeSenhaLibera(data_valid);
      //end;
    except
      PedeSenhaLibera(data_valid);
    end;
  end;
end;

procedure TFMenu.PedeSenhaLibera(DataVence: TDateTime);
begin
  FSenhaLibera := TFSenhaLibera.Create(Self);
  FSenhaLibera.BringToFront;
  if (DataVence < Date) then
  begin
    FSenhaLibera.LabelContinuar.Caption := '  &Cancelar';
    FSenhaLibera.LabelMensagem.Caption := 'A senha do Sistema expirou!';
    FSenhaLibera.LabelMensagem2.Caption := 'Clique no bot�o "Senha online" ou entre em contato com o Suporte ' + #13 + 'T�cnico para obter sua senha de libera��o do Sistema!' + #13 + 'Bella Automa��o - Tel.: (0xx12) 3938-3644';
  end
  else
  begin
    FSenhaLibera.LabelContinuar.Caption := '&Continuar >>';
    FSenhaLibera.LabelMensagem.Caption := 'Faltam ' + IntToStr(Trunc(DataVence - Date)) + ' dia(s) para expira��o da senha do Sistema. ';
    FSenhaLibera.LabelMensagem2.Caption := 'Clique no bot�o "Senha online" ou entre em contato com o Suporte ' + #13 + 'T�cnico para obter sua senha de libera��o do Sistema!' + #13 + 'Bella Automa��o - Tel.: (0xx12) 3938-3644'
  end;
  DMConexao.Config.Close;
  FSenhaLibera.ShowModal;
  if FSenhaLibera.ModalResult = mrCancel then
  begin
    Application.Terminate;
  end;
  FSenha.Free;
end;

procedure TFMenu.exportadebitoExecute(Sender: TObject);
begin
 //comentado ariane MostrarForm(TFExportDebito,TMenuItem(sender).Name,False);
end;

procedure TFMenu.importadebitoExecute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportDebito,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActContasPagarExecute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadContasPagar,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActRelContasPagarExecute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelContasPagar,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActVencContasExecute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFVencContas,TMenuItem(sender).Name,True);
end;

procedure TFMenu.ActEmissaoReciboExecute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFEmissaoRecibo,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Action3Execute(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadGrupoProd,TMenuItem(sender).Name,False);
end;

procedure TFMenu.AtualizaodeDatasdeFechamentoseVencimentos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCalcSaldo,TMenuItem(sender).Name,True);
end;

procedure TFMenu.CadAdmClick(Sender: TObject);
begin
  MostrarForm(TFCadAdm, TMenuItem(sender).Name, False);
end;

procedure TFMenu.CadGrupProdClick(Sender: TObject);
begin
  MostrarForm(TFCadGrupoProd, TMenuItem(Sender).Name, False);
end;

procedure TFMenu.CadEmpClick(Sender: TObject);
begin
  if not vCadEmp then
    MostrarForm(TFCadEmp, TMenuItem(Sender).Name, False)
  else
  begin
    if not focarFilho(TFCadEmp) then
      MsgInf('A tela de "Cadastro de Empresas" j� est� aberta.');
  end;
end;

procedure TFMenu.CadSegClick(Sender: TObject);
begin
  MostrarForm(TFCadSeg, TMenuItem(sender).Name, False);
end;

procedure TFMenu.SairClick(Sender: TObject);
begin
  Close;
end;

procedure TFMenu.CadCredClick(Sender: TObject);
begin

  if not vCadEstab then
    MostrarForm(TFCadCred, TMenuItem(sender).Name, False)
  else if not focarFilho(TFCadCred) then
    MsgInf('A tela de "Cadastro de Estabelecimentos" j� est� aberta.');
end;

procedure TFMenu.CadFormaPagtoClick(Sender: TObject);
begin
  MostrarForm(TFCadFormaPgto, TMenuItem(sender).Name, False);
end;

procedure TFMenu.ConfigClick(Sender: TObject);
begin
  MostrarForm(TFConfig, TMenuItem(sender).Name, True);
end;

procedure TFMenu.BaixaEmpClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFBaixaEmpres,TMenuItem(sender).Name,False);
end;

procedure TFMenu.BaixaFornecClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFBaixaFornec,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadOperClick(Sender: TObject);
begin
  MostrarForm(TFCadOper, TMenuItem(Sender).Name, false);
end;

procedure TFMenu.CadGrupOperClick(Sender: TObject);
begin
  MostrarForm(TFCadGrupoOper, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Medicao_usoClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFMedicao,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelExtratoForClick(Sender: TObject);
begin
  MostrarForm(TFRelExtratoFor, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Cons_Mov_FornecClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_MovFornec,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadCartoesClick(Sender: TObject);
begin
  MostrarForm(TFCadCartoes, TMenuItem(sender).Name, False);
end;

procedure TFMenu.GeraTaxaConvClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFGeraTaxa,TMenuItem(sender).Name,False);
end;

procedure TFMenu.LancaCPMFClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFLancCPMF,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActLancDescForClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFLancDescFor,TMenuItem(sender).Name,False);
end;

procedure TFMenu.OperGeraCartConvClick(Sender: TObject);
begin
  if not vGeracaoCartao then
    MostrarForm(TFGeraCartConv, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Gera��o de Arquivo para cart�o de conveniados" j� est� aberta.');  //comentado ariane
end;

procedure TFMenu.OperGeraCartFornClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFGeraCartCred,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ImportConveniadosClick(Sender: TObject);
begin
  MostrarForm(TFImportConveniadosProdutor, TMenuItem(sender).Name, True);
end;

procedure TFMenu.ImportaLimiteClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportaLimite,TMenuItem(sender).Name,True);
end;

procedure TFMenu.ExportaCobrancasClick(Sender: TObject);
begin
  {DMExport1 := TDMExport1.Create(Self);
  MostrarForm(TFExportaCobranca,TMenuItem(sender).Name,False);}//comentado ariane
end;

procedure TFMenu.GeraDocClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFGeraContrato,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActLctoEntregasClick(Sender: TObject);
begin
  if not vNFAberto then
    MostrarForm(TF_EntregaNF, TMenuItem(sender).Name, False)
  else if not focarFilho(TF_EntregaNF) then
    MsgInf('A tela de "Lan�amento Entrega NFs" j� est� aberta.');
end;

procedure TFMenu.RelExtratoEmpClick(Sender: TObject);
begin
  MostrarForm(TFRelExtratoEmp, TMenuItem(sender).Name, False);
end;

procedure TFMenu.RelatriodeEmpresasquefecharamnoPerodo1Click(Sender: TObject);
begin
  MostrarForm(TfrmRelEmpFecharamNoPeriodo, TMenuItem(sender).Name, False);
end;

procedure TFMenu.RelExtratoUserClick(Sender: TObject);
begin
  MostrarForm(TFRelExtratoConv, TMenuItem(sender).Name, False);
end;

procedure TFMenu.RelExtratoDemitClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelExtratoDemit,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ctExtForVencEmpClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelForVencEmp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.TotPagForClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFTotalPagFor,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Action1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFExtratoDiario,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelMediaPorEmpClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelMediaEmp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Action2Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelConfNFFornec,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Consulta_emp_fornecClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_Emp_For,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Consulta_for_empClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_For_Emp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Consulta_autorClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_Autor,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Consulta_fechaClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_Fecha,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ConsContaCorrClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConsContaCorre,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Cons_Mov_EmpresClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCons_MoveEmp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Cons_Mov_ConvClick(Sender: TObject);
begin
 //comentado ariane  MostrarForm(TFCons_MovConv,TMenuItem(sender).Name,False);
end;

procedure TFMenu.AuditorClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFAuditoria,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ActAuditDatasClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFAuditDatas,TMenuItem(sender).Name,True);
end;

procedure TFMenu.Aud_gruposempresasClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFAuditGruposEmp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadRelConfClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadConferencia,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ConfValClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConfValor,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ConsConferenciaClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConsConferencia,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Alteracoes_SisClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFChangeLog,TMenuItem(sender).Name,True);
end;

procedure TFMenu.Rel_Alt_LinClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFBackAltLinear,TMenuItem(sender).Name,True);
end;

procedure TFMenu.LogoffClick(Sender: TObject);
var
  i: integer;
begin
  FMenu.Hide;
  for i := 0 to FMenu.MDIChildCount - 1 do
  begin
    FMenu.MDIChildren[i].Close;
  end;
  FMenu.Show;
end;

procedure TFMenu.ExportXMLClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFExportaXML,TMenuItem(sender).Name,True);
end;

procedure TFMenu.RegistrosApagadosClick(Sender: TObject);
begin
  {if Operador.IsAdmin then
  begin
    MostrarForm(TFApagados,TMenuItem(sender).Name,False);
  end
  else
  begin
    ShowMessage('Opera��o liberada somente para administradores do sistema.');
  end;       }//comentado ariane
end;

procedure TFMenu.ConfigDebCCClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConfigDebCC,TMenuItem(sender).Name,True);
end;

//Procedure mostrar form, chamada para abrir um form, necess�rios classe do form e nome do menu associado ao form, para buscar as configura��es de permiss�o.
procedure TFMenu.MostrarForm(Classe: TFormClass; MenuName: string; Modal: Boolean);
var
  Form: TForm;
begin
  Screen.Cursor := crHourGlass;
  Form := Classe.Create(Application.MainForm);
  if Form is TFCad then
  begin
    DefinirPermissoes(Form as TFCad, MenuName);
  end;
  Screen.Cursor := crDefault;
  if Modal then
  begin
    Form.ShowModal;
    try
      FreeAndNil(Form);
    except

    end;
  end
  else
  begin
    Form.Show;
  end;
end;

procedure TFMenu.MostrarFormExportacao(Classe: TFormClass; MenuName: string; Modal: Boolean);
var
  Form: TForm;
begin
  Screen.Cursor := crHourGlass;
  Form := Classe.Create(Application.MainForm);
  Screen.Cursor := crDefault;
  Form.Caption := MenuName;
  if Modal then
  begin
    Form.ShowModal;
    try
      FreeAndNil(Form);
    except

    end;
  end
  else
  begin
    Form.Show;
  end;
end;

procedure TFMenu.DefinirPermissoes(Form: TFCad; MenuName: string);
begin
  //S� verificar as permissoes quando nao administrador e encontrar o menu nas permissoes do user.
  if ((not Operador.IsAdmin) and QPErmissoes.Locate('nome', MenuName, [])) then
  begin
    Form.Incluir := qPermissoesinclui.AsString = 'S';
    Form.Alterar := qPermissoesaltera.AsString = 'S';
    Form.Excluir := qPermissoesexclui.AsString = 'S';
  end
  else
  begin
    Form.Incluir := True;
    Form.Alterar := True;
    Form.Excluir := True;
  end;
  Form.HabilitarBotoes;
end;

procedure TFMenu.IMGCONVClick(Sender: TObject);
begin
  CadConvClick(CadConv);
end;

procedure TFMenu.IMGFechaClick(Sender: TObject);
begin
  Close;
end;

procedure TFMenu.AtualizarModulos;
var
  i: integer;
  MenuItem: TMenuItem;
begin

  DMConexao.adoQry.Close;
  DMConexao.adoQry.Sql.Text := 'Select nome from modulos ';
  DMConexao.adoQry.Open;
  for i := 0 to pred(self.ComponentCount) do
  begin
    if (Self.Components[i] is TMenuItem) then
    begin
      MenuItem := FMenu.Components[i] as TMenuItem;
      if IsMenuJanela(MenuItem) then
      begin
        if not DMConexao.adoQry.Locate('nome', MenuItem.Name, []) then
        begin
          DMConexao.ExecuteSql(' insert into modulos(modulo_id,descricao,nome) values(next value for SMODULO,''' + GetMenuName(MenuItem) + ''',''' + MenuItem.Name + ''')  ');
        end;
      end;
    end;
  end;
  DMConexao.adoQry.Close;
end;

procedure TFMenu.AtualizarPermissaoAcesso;
var
  i: integer;
  MenuItem: TMenuItem;
begin
  HabilarAllMenus; //necess�rio para quando executando log off
  qPermissoes.Close;
  qPermissoes.Parameters[0].Value := Operador.ID;
  qPermissoes.Open;
  if not Operador.IsAdmin then
  begin
    for i := 0 to pred(self.ComponentCount) do
    begin
      if IsMenuJanela(FMenu.Components[i]) then
      begin
        MenuItem := FMenu.Components[i] as TMenuItem;
        if QPErmissoes.Locate('nome', MenuItem.Name, []) then
        begin
          MenuItem.Enabled := (QPErmissoesACESSA.AsString = 'S');
          if not MenuItem.Enabled then
          begin
            DesabilitarFilhos(MenuItem);
          end;
        end;
      end;
    end;
  end;
  //Habilita icones.
  Image1.Enabled := CadConv.Enabled;
  Image3.Enabled := CadEmp.Enabled;
  Image4.Enabled := CadCred.Enabled;
  Image9.Enabled := OperGeraCartConv.Enabled;
  Image10.Enabled := OperFechaEmp.Enabled;
  Image11.Enabled := FinancFatura.Enabled;
  //Image12.Enabled  := FinancPgtoFornAbert.Enabled;
  Image13.Enabled := ActLctoEntregas.Enabled;
  if Operacional1.Enabled then
    Image14.Enabled := este1.Enabled
  else
    Image14.Enabled := false;
  //QPErmissoes.Close;
end;

function TFMenu.IsMenuJanela(Component: TComponent): Boolean;
begin
  Result := (Component is TMenuItem) and (TMenuItem(Component).GetParentMenu = MainMenu1) and (TMenuItem(Component).Caption <> '-');
end;

procedure TFmenu.HabilarAllMenus;
var
  i: integer;
begin
  for i := 0 to pred(self.ComponentCount) do
  begin
    if IsMenuJanela(FMenu.Components[i]) then
    begin
      TMenuItem(FMenu.Components[i]).Enabled := True;
    end;
  end;
end;

function TFMenu.GetMenuName(Item: TMenuITem): string;
var
  name: string;
begin
  while (Item.Parent <> nil) and (Item.Caption <> '') do
  begin
    name := StringReplace(Item.Caption, '&', '', []) + '-' + name;
    Item := Item.Parent;
  end;
  name := Trim(name);
  if name[Length(name)] = '-' then
  begin
    delete(name, Length(name), 1);
  end;
  Result := name;
end;

procedure TFMenu.DesabilitarFilhos(Item: TMenuITem);
var
  i: integer;
begin
  for i := 0 to pred(Item.Count) do
  begin
    Item.Items[i].Enabled := false;
    DesabilitarFilhos(Item.Items[i]);
  end;
end;

procedure TFMenu.FinancFaturaClick(Sender: TObject);
begin
  if not vMantFat then
    MostrarForm(TFFatura, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Manuten��o de Fatura" j� est� aberta.');
end;

procedure TFMenu.ManutenodeAutorizaes1Click(Sender: TObject);
begin
  MostrarForm(TFManutAutors, TMenuItem(sender).Name, False);
end;

procedure TFMenu.CriarDatasdeFechamentoEmpresasFornecedores1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCriaDatasEmpFornec,TMenuItem(sender).Name,True);
end;

procedure TFMenu.AnlisedeAtividadeporEmpresa1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFAnlisedeAtividadeporEmpresa,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ProdutosporAutorizao1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelProdAutor,TMenuItem(sender).Name,False);
end;

procedure TFMenu.VisualizarLanamentos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelDigitados,TMenuItem(sender).Name,False);
end;

procedure TFMenu.LancamentosClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFLancamentos,TMenuItem(sender).Name,False);
end;

procedure TFMenu.FinancPgtoFornAbertClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFPgtoFornec,TMenuItem(sender).Name,False);
end;

procedure TFMenu.FinancPgtoFornRealizClick(Sender: TObject);
begin
  //comentado arianeMostrarForm(TFPgtoRealizado,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadTaxasClick(Sender: TObject);
begin
  MostrarForm(TFCadTaxas, TMenuItem(sender).Name, False);
end;

procedure TFMenu.FinancPgtoFornPrevisClick(Sender: TObject);
begin
  //comentado arianeMostrarForm(TFPgtoPrevisao,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadDespAdmClick(Sender: TObject);
begin
  //comentado arianeMostrarForm(TFCadDespAdm,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ConfernciadeDespesasdaAdministradora1Click(Sender: TObject);
begin
  //comentado arianeMostrarForm(TFRelDespAdm,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ConfiguraoLocal1Click(Sender: TObject);
begin
  //comentado arianeMostrarForm(TFConfigLocal,TMenuItem(sender).Name,True);
end;

procedure TFMenu.OperFechaEmpClick(Sender: TObject);
begin
  if not vFechtoEmp then
    MostrarForm(TFFechamento, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Fechamento Empresas" j� est� aberta.');
end;

procedure TFMenu.OperFechaConvClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFFecha_Conv,TMenuItem(sender).Name,False);
end;

procedure TFMenu.GerarArquivopadaDbitoemConta1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFGeraDebConta,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RetornodeDbitoemConta1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRetornoDebConta,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelatriodeDiferenaentreAutorizaeseProdutos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelDifAutProd,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelatriodeTaxaparaoPrxFechamento1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelTaxa,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Relatrio1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelDescGrupEmp,TMenuItem(sender).Name,False);
end;

procedure TFMenu.LinkTo(const aAdress: string);
var
  buffer: string;
begin
  buffer := 'http://' + aAdress;
  ShellExecute(Application.Handle, nil, PChar(buffer), nil, nil, sw_hide); //SW_SHOWNORMAL);
end;

procedure TFMenu.SuporteOnLineChat1Click(Sender: TObject);
begin
  LinkTo('http://www.drogabella.com.br/bti/suporte.htm');
end;

procedure TFMenu.AbrirVNC(porta: integer);
var
  host: string;
begin
  host := ' 189.56.94.82';
  if FileExists('C:\Arquivos de programas\UltraVNC\winvnc.exe') then
  begin
    WinExec(PChar('C:\Arquivos de programas\UltraVNC\winvnc.exe -connect ' + host + ':' + IntToStr(porta)), SW_SHOW);
  end
  else if FileExists('C:\Program Files\UltraVNC\winvnc.exe') then
  begin
    WinExec(PChar('C:\Program Files\UltraVNC\winvnc.exe -connect ' + host + ':' + IntToStr(porta)), SW_SHOW);
  end
  else
  begin
    MsgErro('VNC n�o encontrado!');
  end;
end;

procedure TFMenu.VNCWilliamRocha1Click(Sender: TObject);
begin
  AbrirVNC(5510);
end;

procedure TFMenu.QualomeuIP1Click(Sender: TObject);
begin
  LinkTo('www.meuip.com.br');
end;

function TFMenu.GetPersonalFolder: string;
var
  shellMalloc: IMalloc;
  ppidl: PItemIdList;
begin
  ppidl := nil;
  try
    if SHGetMalloc(shellMalloc) = NOERROR then
    begin
      SHGetSpecialFolderLocation(FMenu.Handle, CSIDL_PERSONAL, ppidl);
      SetLength(Result, MAX_PATH);
      if not SHGetPathFromIDList(ppidl, PChar(Result)) then
      begin
        raise exception.create('SHGetPathFromIDList failed : invalid pidl');
      end;
      SetLength(Result, lStrLen(PChar(Result)));
    end;
  finally
    if ppidl <> nil then
    begin
      shellMalloc.free(ppidl);
    end;
  end;
end;

procedure TFMenu.Mensagens1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFMensagem,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Arquivos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFUtiArquivos,TMenuItem(sender).Name,False);
end;

procedure TFMenu.lblAlertClick(Sender: TObject);
begin
//  imgAlertClick(nil);
end;

procedure TFMenu.Image1Click(Sender: TObject);
begin
  if not vCadConv then
    MostrarForm(TFCadConv, CadConv.Name, False)
  else if not focarFilho(TFCadConv) then
    MsgInf('A tela de "Cadastro de conveniados" j� est� aberta.');
end;

procedure TFMenu.imgAlertClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Altera��es e Atualiza��es do Sistema';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFMenu.ImportaoPersonalizadadeConveniados1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportPersonal,TMenuItem(Sender).Name,True);
end;

procedure TFMenu.VNCAlineSabio1Click(Sender: TObject);
begin
  AbrirVNC(5520);
end;

procedure TFMenu.WMSysCommand(var Msg: TWMSysCommand);
begin
  inherited;
  case Msg.CmdType of
    //SC_MINIMIZE: Esconde(True); //Aqui voc� declara o que ser� feito se o bot�o minimizar foi clicado e assim por diante
    //SC_MAXIMIZE: Esconde(False);
    // SC_MAXIMIZE, 61490: Esconde(False); //Para evitar a maximiza��o com um clique duplo, usamos o par�metro 61490
    SC_RESTORE:
      FMenu.Width := FMenu.Width + 1;
  end;
end;

procedure TFMenu.ExibirAlertas1Click(Sender: TObject);
begin
  Alertas;
end;

procedure TFMenu.ImportConveniadosProdutorClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportConveniadosProdutor,TMenuItem(sender).Name,True);
end;

procedure TFMenu.CadProgDescClick(Sender: TObject);
begin
  MostrarForm(TFCadProgramas, CadProgDesc.Name, False);
end;

procedure TFMenu.ModelosCartes1Click(Sender: TObject);
begin
  MostrarForm(TFCadModelosCartoes, CadProgDesc.Name, False);
end;

procedure TFMenu.ImportaodeListaABCFarma1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImpProdABC,TMenuItem(sender).Name,True);
end;

procedure TFMenu.CadProdClick(Sender: TObject);
begin
  MostrarForm(TFCadProduto2, CadProd.Name, False);
end;

procedure TFMenu.FormResize(Sender: TObject);
begin
  //speedMenu1.Left:= 0;
  //speedMenu1.Top:= 0;
  //speedMenu1.Height:= panMenu1.Height;
  //speedMenu1.Width:= 10;
  //panMenu3.Height:= panMenu1.Height;
  //panClica1.Top:= Trunc(speedMenu1.Height/2)-20;
  //panClica2.Top:= Trunc(speedMenu1.Height/2)-20;
end;

procedure TFMenu.speedMenu1Click(Sender: TObject);
begin
  {if panMenu2.Visible then begin
    panMenu2.Visible := False;
    //imgTela.Left := 24;
    imgAlert.Left := 24;
    lblAlert.Left := imgAlert.Left + 20;
  end else begin
    panMenu2.Visible := True;
    //imgTela.Left := 120;
    imgAlert.Left := 120;
    lblAlert.Left := imgAlert.Left + 20;
  end;}
end;

procedure TFMenu.ApplicationEvents1Exception(Sender: TObject; E: Exception);
var
  mensagem: string;
  Pos1, Pos2: integer;
begin
  if Pos(UpperCase('is not a valid date'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'Data inv�lida, proceda a corre��o.';
  end
  else if Pos(UpperCase('must have a value'), UpperCase(E.Message)) <> 0 then
  begin
    Pos1 := Pos('''', E.Message);
    mensagem := E.Message;
    Delete(mensagem, Pos1, 1);
    Pos2 := Pos('''', mensagem);
    mensagem := copy(E.Message, Pos1 + 1, Pos2 - Pos1);
    mensagem := '� obrigat�rio o preenchimento do campo ' + mensagem + '.';
  end
  else if Pos(UpperCase('key violation'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'Houve viola��o de Chave. Registro j� incluido.';
  end
  else if Pos(UpperCase('is not a valid time'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'Hora inv�lida, proceda a corre��o.';
  end
  else if Pos(UpperCase('is not a valid float'), UpperCase(E.Message)) <> 0 then
  begin
    Pos1 := Pos('''', E.Message);
    mensagem := E.Message;
    Delete(mensagem, Pos1, 1);
    Pos2 := Pos('''', mensagem);
    mensagem := copy(E.Message, Pos1 + 1, Pos2 - Pos1);
    mensagem := 'O valor ' + mensagem + ' n�o � v�lido.';
  end
  else if Pos(UpperCase('field value required'), UpperCase(E.Message)) <> 0 then
  begin
    Pos1 := Pos('column ', E.Message) + 7;
    Pos2 := Pos(',', E.Message);
    mensagem := copy(E.Message, Pos1, Pos2 - Pos1);
    mensagem := 'Campo ' + mensagem + ' deve ser preenchido.';
  end
  else if Pos(UpperCase('COLUMN UNKNOWN'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'O processo est� tentando usar um campo que n�o foi encontrado no banco de dados. ' + sLineBreak + 'Entre em contato com suporte do sistema.';
  end
  else if Pos(UpperCase('ATTEMPT TO STORE DUPLICATE VALUE'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'N�o � permitido valor duplicado. ';
  end
  else if Pos(UpperCase('FOREIGN KEY'), UpperCase(E.Message)) <> 0 then
  begin
    mensagem := 'Opera��o n�o permitida, registro vinculado em outra tabela est� faltando.';
  end
  else if Pos('VIOLATION OF PRIMARY OR UNIQUE KEY CONSTRAINT', UpperCase(E.Message)) <> 0 then
    mensagem := 'Registro Duplicado' + #13#10 + Copy(UpperCase(E.Message), Pos('VIOLATION OF PRIMARY OR UNIQUE KEY CONSTRAINT', UpperCase(E.Message)) + 47, 100)
  else if Pos(UpperCase('MUST APPLY UPDATES BEFORE REFRESHING DATA'), UpperCase(E.Message)) <> 0 then
    mensagem := '� necess�rio aplicara as altera��es antes de atualizar os dados'
  else if (Pos(UpperCase('UNABLE TO COMPLETE NETWORK REQUEST TO HOST'), UpperCase(E.Message)) <> 0) or (Pos(UpperCase('NO ACTIVE CONNECTION'), UpperCase(E.Message)) <> 0) then
  begin
    mensagem := 'Sua conex�o com o servidor falhou. O sistema ser� encerrado!';
    MsgErro(mensagem);
    Application.Terminate;
  end
  else
    mensagem := 'Ocorreu o seguinte erro: ' + #13 + UpperCase(E.Message);
  MessageDlg(mensagem, mtError, [mbOk], 0);
//  ABORT;
end;

procedure TFMenu.ApplicationEvents1Restore(Sender: TObject);
begin
//    if Assigned(ActiveMDIChild) then
//      ActiveMDIChild.Release;
end;

procedure TFMenu.CadFabrClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadFabricante,CadFabr.Name,False);
end;

procedure TFMenu.ConfigCupomClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConfigCupom,TMenuItem(sender).Name,True);
end;

procedure TFMenu.UtilVisualizAutOcultClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFAutorizOcultas,TMenuItem(sender).Name,False);
end;

procedure TFMenu.UtilOcultAutorClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFOcultaAutoriz,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelProgDescontoClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelProgramaDesc,TMenuItem(sender).Name,False);
end;

procedure TFMenu.lblMsgClick(Sender: TObject);
begin
  Mensagens1.Click;
end;

procedure TFMenu.ConfiguraodeFidelidade1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFConfigFidel,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CadPremiosClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadPremios,CadPremios.Name,False);
end;

procedure TFMenu.EmAbertoTeste1Click(Sender: TObject);
begin
  if not vPagEstab then
    MostrarForm(TfrmPgtoEstabAberto, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Pagamento de Estabelecimentos" j� est� aberta.');
end;

procedure TFMenu.RealizadosTeste1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TfrmPgtoEstabRealiz,TMenuItem(sender).Name,False);
end;

procedure TFMenu.este1Click(Sender: TObject);
begin
  if not focarFilho(TFLancIndiv) then
    MostrarForm(TFLancIndiv, TMenuItem(sender).Name, False);
end;

procedure TFMenu.VNCAlbertoVeloso1Click(Sender: TObject);
begin
  AbrirVNC(5571);
end;

procedure TFMenu.ResgatedePrmiosFidelidade1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFResgate,TMenuItem(sender).Name,False);
end;

procedure TFMenu.CarregaMenuRel;
var
  g, s, r: Integer;
  MenuRel, Grupo, SubGrupo, Item: TMenuItem;
begin
  // Ler o itens que fazerao parte do menu
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.sql.clear;
  DMConexao.AdoQry.sql.Add(' SELECT ');
  DMConexao.AdoQry.sql.Add('   re.grupo_relatorio_id AS grupo_id, ');
  DMConexao.AdoQry.sql.Add('   COALESCE(gr.descricao,"XXX") AS grupo, ');
  DMConexao.AdoQry.sql.Add('   re.subgrupo_id, ');
  DMConexao.AdoQry.sql.Add('   COALESCE(sg.descricao,"XXX") AS subgrupo, ');
  DMConexao.AdoQry.sql.Add('   re.relatorio_id, ');
  DMConexao.AdoQry.sql.Add('   COALESCE(re.descricao,"XXX") AS item ');
  DMConexao.AdoQry.sql.Add(' FROM relatorio re ');
  DMConexao.AdoQry.sql.Add(' LEFT JOIN grupo_relatorio gr ON re.grupo_relatorio_id = gr.grupo_relatorio_id AND gr.apagado <> "S" ');
  DMConexao.AdoQry.sql.Add(' LEFT JOIN subgrupos sg ');
  DMConexao.AdoQry.sql.Add('   ON re.subgrupo_id = sg.subgrupo_id ');
  DMConexao.AdoQry.sql.Add('   AND gr.grupo_relatorio_id = sg.grupo_relatorio_id ');
  DMConexao.AdoQry.sql.Add('   AND sg.apagado <> "S" ');
  DMConexao.AdoQry.sql.Add(' WHERE re.apagado <> "S" ');
  DMConexao.AdoQry.sql.Add(' ORDER BY 2,4,6 ');
  DMConexao.AdoQry.Open;
  DMConexao.AdoQry.First;
  MenuRel := TMenuItem.Create(FMenu);
  MenuRel.Caption := 'Gerador de Relat�rios';
  MainMenu1.Items.Add(MenuRel);
  g := 0;
  while not DMConexao.AdoQry.Eof do
  begin
    // Cria o item do Menu e depois inclui o mesmo //
    if ((g <> DMConexao.AdoQry.FieldByName('GRUPO_ID').AsInteger) or (DMConexao.AdoQry.FieldByName('GRUPO').AsString = 'XXX')) then
    begin
      if DMConexao.AdoQry.FieldByName('GRUPO').AsString = 'XXX' then
      begin
        g := DMConexao.AdoQry.FieldByName('GRUPO_ID').AsInteger;
        Grupo := TMenuItem.Create(FMenu);
        Grupo.Caption := DMConexao.AdoQry.FieldByName('GRUPO').AsString;
      end
      else
      begin
        g := DMConexao.AdoQry.FieldByName('GRUPO_ID').AsInteger;
        Grupo := TMenuItem.Create(FMenu);
        Grupo.Caption := DMConexao.AdoQry.FieldByName('GRUPO').AsString;
      end;
      MenuRel.Add(Grupo);
    end;
    DMConexao.AdoQry.Next;
  end;
  DMConexao.AdoQry.close;
end;

procedure TFMenu.MovimentosDuvidososClick(Sender: TObject);
begin
  MostrarForm(TFMovimentosDuvidosos, TMenuItem(sender).Name, False);
end;

procedure TFMenu.RelExtratoConvEndClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFExtratoConvEnd,TMenuItem(sender).Name,False);
end;

procedure TFMenu.AtualizaPreos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TfAtualizaPrecos,TMenuItem(sender).Name,True);
end;

procedure TFMenu.pec1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFPec,TMenuItem(sender).Name,True);
end;

procedure TFMenu.VisualizaodeCartesEmitidos1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFVisualizaCartoes,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ImportaProdutosnoPrograma1Click(Sender: TObject);
begin
  MostrarForm(TfrmImportProdXls, TMenuItem(sender).Name, True);
end;

procedure TFMenu.ValeDesconto1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFGruposValeDesconto,TMenuItem(sender).Name,False);
end;

procedure TFMenu.RelValeDescontosClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelValeDesconto,TMenuItem(sender).Name,False);
end;

procedure TFMenu.Agenciadores1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFCadAgenciador,CadAgenciador.Name,False);
end;

procedure TFMenu.VendasAgenciadosporAtivaoClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelVendasAgenc,TMenuItem(sender).Name,True);
end;

procedure TFMenu.CadBancoClick(Sender: TObject);
begin
  MostrarForm(TFCadBanco, TMenuItem(sender).Name, False);
end;

procedure TFMenu.ImportaodeConveniadosEmbraer1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportaConveniadosEmbraer,TMenuItem(sender).Name,True);
  //MostrarForm(TFImportaConveniadosEmbraer2,TMenuItem(sender).Name,True);
end;

procedure TFMenu.EmbraerELEB1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TfrmExportacaoTransacoes,TMenuItem(sender).Name,True);
end;

procedure TFMenu.axasporEstabelecimento1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelTaxasCred,TMenuItem(sender).Name,False);
end;

procedure TFMenu.EmbraerAviation1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFrmEmbraerAviation, TMenuItem(sender).Name,False);
end;

procedure TFMenu.ImportaodeConveniadosComunix1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportConveniadosComunix,TMenuItem(sender).Name,True);
end;

procedure TFMenu.ImportaodeConveniadosSantoAntonio1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportConveniadosSantoAntonio,TMenuItem(sender).Name,True);
end;

procedure TFMenu.ImportaodeEmpresas1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportEmpresas,TMenuItem(sender).Name,True);
end;

procedure TFMenu.Bandeiras1Click(Sender: TObject);
begin
  MostrarForm(TFCadBandeiras, TMenuItem(sender).Name, False);
end;

procedure TFMenu.CONFABFarmcias1Click(Sender: TObject);
begin
  MostrarForm(TFExportacaoConfabFarmacias, TMenuItem(sender).Name, True);
end;

procedure TFMenu.CONFABticas1Click(Sender: TObject);
begin
  MostrarForm(TFExportacaoConfabOticas, TMenuItem(sender).Name, True);
end;

procedure TFMenu.RelatoriodeExportacaodeFechamento1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFRelExporFechamento,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ImportacaoMovimentacaoClick(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportMovimentacao,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ImportaodeDependentes1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFImportDependentes,TMenuItem(sender).Name,False);
end;

procedure TFMenu.ProdutosAutorizador1Click(Sender: TObject);
begin
  MostrarForm(TFrmCadProdutosAutorizador, TMenuItem(sender).Name, False);
end;

procedure TFMenu.AtendimentoVendas1Click(Sender: TObject);
begin
  MostrarForm(TFrmAtendimentoVendas, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Mexichem1Click(Sender: TObject);
begin
  //comentado ariane MostrarFormExportacao(TFrmExportEstabPadrao,StringReplace(TMenuItem(sender).Caption,'&','',[rfReplaceAll]),True);
end;

procedure TFMenu.Lear1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFExportacaoLear,TMenuItem(sender).Name,True);
end;

procedure TFMenu.Relatriodecreditosdeconveniados1Click(Sender: TObject);
begin
  MostrarForm(TFrmRelCredAssociado, TMenuItem(sender).Name, False);
end;

procedure TFMenu.AnlisedevendasdoPOSTEF1Click(Sender: TObject);
begin
  //comentado ariane MostrarForm(TFrmAnaliseVendas,TMenuItem(Sender).Name,False);
end;

procedure TFMenu.ExportarPlanilhadecredito1Click(Sender: TObject);
begin
  MostrarForm(TFrmExportPlanCred, TMenuItem(Sender).Name, False);
end;

function TFMenu.LiberaMenu(menuItem: TMenuItem; busca: string): Boolean;
var
  I: Integer;
begin
  for I := 0 to menuItem.Count - 1 do
  begin
    if menuItem.Items[I].Count > 0 then
      Result := LiberaMenu(menuItem.Items[I], busca)
    else
    begin
      menuItem[I].Visible := Pos(lowerCase(busca), lowerCase(menuItem.Items[I].Name)) > 0;
    end;
  end;
end;

procedure TFMenu.edtBuscaMenuKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key = VK_RETURN then
    LiberaMenu(MainMenu1.Items, edtBuscaMenu.Text);
end;

procedure TFMenu.ProtocolodeEntrega1Click(Sender: TObject);
begin
  MostrarForm(TFRelProtocoloEntrega, TMenuItem(sender).Name, False);
end;

procedure TFMenu.AutorizaoparaDescontoemFolhaTermodeRecebimento1Click(Sender: TObject);
begin
  MostrarForm(TFRelAutorizacoesDesconto, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Image3Click(Sender: TObject);
begin
  if not vCadEmp then
    MostrarForm(TFCadEmp, TMenuItem(Sender).Name, False)
  else
  begin
    if not focarFilho(TFCadEmp) then
      MsgInf('A tela de "Cadastro de Empresas" j� est� aberta.');
  end;
end;

procedure TFMenu.Image6Click(Sender: TObject);
begin
  if not vFechtoEmp then
    MostrarForm(TFFechamento, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Fechamento Empresas" j� est� aberta.');
end;

procedure TFMenu.Image7Click(Sender: TObject);
begin
{if not vMantFat then
    MostrarForm(TFFatura,TMenuItem(sender).Name,False)
  else
    MsgInf('A tela de "Manuten��o de Fatura" j� est� aberta.');}
end;

procedure TFMenu.Image8Click(Sender: TObject);
begin
  if not vPagEstab then
    MostrarForm(TfrmPgtoEstabAberto, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Pagamento de Estabelecimentos" j� est� aberta.');
end;

procedure TFMenu.AtualizaConsumoMesClick(Sender: TObject);
begin
  MostrarForm(TFAtualizarConsumoMes, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Grupos1Click(Sender: TObject);
begin
  MostrarForm(TFCadGrupoEstab, TMenuItem(Sender).Name, False);
end;

procedure TFMenu.Image9Click(Sender: TObject);
begin
  if not vGeracaoCartao then
    MostrarForm(TFGeraCartConv, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Gera��o de Arquivo para cart�o de conveniados" j� est� aberta.');
end;

procedure TFMenu.Image10Click(Sender: TObject);
begin
  if not vFechtoEmp then
    MostrarForm(TFFechamento, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Fechamento Empresas" j� est� aberta.');
end;

procedure TFMenu.Image11Click(Sender: TObject);
begin
  if not vMantFat then
    MostrarForm(TFFatura, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Manuten��o de Fatura" j� est� aberta.');
end;

procedure TFMenu.Image13Click(Sender: TObject);
begin
  if not vNFAberto then
    MostrarForm(TF_EntregaNF, TMenuItem(sender).Name, False)
  else if not focarFilho(TF_EntregaNF) then
    MsgInf('A tela de "Lan�amento Entrega NFs" j� est� aberta.');
end;

procedure TFMenu.Image14Click(Sender: TObject);
begin
  if not focarFilho(TFLancIndiv) then
    MostrarForm(TFLancIndiv, TMenuItem(sender).Name, False);
end;

procedure TFMenu.ltimosFornecedoresCadastrados1Click(Sender: TObject);
begin
   //MostrarForm(TFRelEmpresaCad,TMenuItem(sender).Name,False);
  if not vRelCred then
    MostrarForm(TFRelCredCad, TMenuItem(sender).Name, False)
  else if not focarFilho(TFRelCredCad) then
    MsgInf('A tela de "Lan�amento Entrega NFs" j� est� aberta.');
end;

procedure TFMenu.RelatorioExtratoAlimClick(Sender: TObject);
begin
  MostrarForm(TFrmRelExtratoAlim, TMenuItem(sender).Name, False);
end;

procedure TFMenu.ExportaodeArquivosdeFechamento1Click(Sender: TObject);
begin
  MostrarForm(TfrmExportEstabPadrao, TMenuItem(sender).Name, True);
end;

procedure TFMenu.PagamentodeFornecedores1Click(Sender: TObject);
begin
  if not vPagEstab then
    MostrarForm(TfrmPgtoEstabAberto, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Pagamento de Estabelecimentos" j� est� aberta.');

end;

procedure TFMenu.RelatrioGerenciais1Click(Sender: TObject);
begin
  if not vRelGerencial then
    MostrarForm(TFGerenciais, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rios Gerenciais" j� est� aberta.');
end;

procedure TFMenu.UsuriosSenhaPadro1Click(Sender: TObject);
begin
  if not vRelUsuarioSenhaPadrao then
    MostrarForm(TFRelSenhaConv, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rios de Usu�rios com Senha Padr�o" j� est� aberta.');
end;

procedure TFMenu.ManutenodeSenhadeConveniados1Click(Sender: TObject);
begin
  if not vManutSenhaConv then
    MostrarForm(TFManutSenhaConv, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Manuten��o de Senha de Conveniados" j� est� aberta.');
end;

procedure TFMenu.RelatriodeRepassesAnaltico1Click(Sender: TObject);
begin
  if not vRelRepasseAnalitico2 then
    MostrarForm(TFrmRelRepasseAnalitico2, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.CadastrodeCredenciados1Click(Sender: TObject);
begin
  if not vFrmcadCredBemEstar then
    MostrarForm(TFCad_CredBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.CadastrodeBenificirios1Click(Sender: TObject);
begin
  if not vFCadBenificiarioBemEstar then
    MostrarForm(TFCadBenBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.CadastrodeEspecialidades1Click(Sender: TObject);
begin
  if not vFCadEspecialidadesBemEstar then
    MostrarForm(TFCadEspecialidadesBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.ManutenodeTaxaFixaPadro1Click(Sender: TObject);
begin
  if not vFCadManutTaxaFixaPadraoBemEstar then
    MostrarForm(TFCadManutTaxaFixaPadraoBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.ManutenodeTaxasporEmpresa1Click(Sender: TObject);
begin
  if not vFCadManutTaxasMensalidadeEmpresa then
    MostrarForm(TFCadManutTaxasMensalidadeEmpresa, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.ManutenodeTaxasporBenificirio1Click(Sender: TObject);
begin
  if not vFManutTaxaBenificiarioBemEstar then
    MostrarForm(TFManutTaxaBenificiarioBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Repasses - Anal�tico" j� est� aberta.');
end;

procedure TFMenu.GedorCartesEmpresaExistente1Click(Sender: TObject);
begin
  if not vFGeraCartaBemEstar then
    MostrarForm(TFGeraCartaBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Gera��o de Cart�es Empresa Existente - Bem Estar" j� est� aberta.');
end;

procedure TFMenu.CadastrodeCantinas1Click(Sender: TObject);
begin
  if not vCadCantina then
    MostrarForm(TFCadCantina, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de cantinas" j� est� aberta.');
end;

procedure TFMenu.CadastrodeEscolas1Click(Sender: TObject);
begin
  if not vCadEscolas then
    MostrarForm(TFCadEscola, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de escolas" j� est� aberta.');
end;

procedure TFMenu.CadastrodeAlunos1Click(Sender: TObject);
begin
  if not vCadAluno then
    MostrarForm(TFCadAluno, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de alunos" j� est� aberta.');
end;

procedure TFMenu.CadastrodeCartes1Click(Sender: TObject);
begin
  if not vCadCartaoBemEstar then
    MostrarForm(TFCadCartoesBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de cart�es bem estar" j� est� aberta.');
end;

procedure TFMenu.ImportProdRotinaAbcClick(Sender: TObject);
begin
  if not vCadImportABC then
    MostrarForm(TfrmImportProdAbc, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Importa��o de Produtos ABC Farma" j� est� aberta.');
end;

procedure TFMenu.CadastroDeParceirosClick(Sender: TObject);
begin
  if not vCadParceirosBemEstar then
    MostrarForm(TFCadParceirosBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de Parceiros" j� est� aberta.');
end;

procedure TFMenu.PagamentodeEstabelecimentosAntecipao1Click(Sender: TObject);
begin
  if not vFormRepasseAntecipacao then
    MostrarForm(TfrmPgtoEstabAbertoAntecipacao, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro Repasses - Antecipa��o" j� est� aberta.');
end;

procedure TFMenu.ReimpressodeRepasses1Click(Sender: TObject);
begin
  if not vFormReimpresaoRepasse then
    MostrarForm(TfrmRepassesReimpressao, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Reimpressao de Repasse" j� est� aberta.');
end;

procedure TFMenu.PagamentodeEstabelecimentoCanceladoRelatrio1Click(Sender: TObject);
begin
  if not vFormRepassesCancelados then
    MostrarForm(TfrmRepassesCancelados, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Repasse Cancelados" j� est� aberta.');
end;

procedure TFMenu.RelatriodeEstabTaxaBTI1Click(Sender: TObject);
begin
  if not vFormRelInadimplentesBti then
    MostrarForm(TFrmRelInadimplentesBti, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Relat�rio de Inadimplentes BTI" j� est� aberta.');
end;

procedure TFMenu.RelatorioExtratoAlimentacaoMensalClick(Sender: TObject);
begin
  MostrarForm(TFrmRelExtratoAlimentacaoMensal, TMenuItem(sender).Name, False);
end;

procedure TFMenu.LanamentodeCreditoExtraAlimentao1Click(Sender: TObject);
begin
  MostrarForm(TFrmLancamentoExtraAlimentacao, TMenuItem(sender).Name, False);
end;

procedure TFMenu.Relatrio2Click(Sender: TObject);
begin
  MostrarForm(TFrmRelAlimentacaoCredito, TMenuItem(sender).Name, False);

end;

procedure TFMenu.EstabelecimentosPOSClick(Sender: TObject);
begin
  MostrarForm(TFCadCredTef, TMenuItem(sender).Name, False);
end;

procedure TFMenu.GerarArquivoparacartoconveniados1Click(Sender: TObject);
begin
  if not vGeracaoCartaoNovo then
    MostrarForm(TFGeraCartConvNovo, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Gera��o de Arquivo para novos cart�es de conveniados" j� est� aberta.');  //comentado ariane
end;

procedure TFMenu.ProtocolodeEntrega2Click(Sender: TObject);
begin
  MostrarForm(TFRelProtocoloEntregaNovo, TMenuItem(sender).Name, False);
end;

procedure TFMenu.AutorizaoparaDescontoemFolhaTermodeRecebimento2Click(Sender: TObject);
begin
  MostrarForm(TFRelAutorizacoesDescontoNovo, TMenuItem(sender).Name, False);
end;

procedure TFMenu.GerarNovosCartes1Click(Sender: TObject);
begin
  if not vCadGeracaoCartaoNovo then
    MostrarForm(TFCadGeraCartaoNovo, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Gera��o novos cart�es" j� est� aberta.');
end;

procedure TFMenu.ImportaoeGeraodeNovosCartes1Click(Sender: TObject);
begin
  if not vCadImportEGeraNovoCartao then
    MostrarForm(TFImportEGeraCartao, TMenuItem(sender).Name, False)
  else
    MsgInf('A tela de "Importa��o e Gera��o novos cart�es" j� est� aberta.');
end;

procedure TFMenu.PagamentoBancoClick(Sender: TObject);
begin
  if not vFormIdenticaoBancaria then
    MostrarForm(TfrmIdentificacaoBancariaRepasse, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Identifica��o Banc�ria" j� est� aberta.');
end;

procedure TFMenu.mniNotasFiscais1Click(Sender: TObject);
begin
  MostrarForm(TfrmRelNotaFiscal, TMenuItem(Sender).Name, False)
end;

procedure TFMenu.tAnexarBoletodaEmpresa1Click(Sender: TObject);
begin
  MostrarForm(TFAnexarBoleto, TMenuItem(sender).Name, True);
end;

//procedure TFMenu.tGerarArquivoparaCartodeBeneficirio1Click(
//  Sender: TObject);
//begin
//  if not vGeracaoCartaoBemEstar then
//    MostrarForm(TFGeraCartConvBemEstar,TMenuItem(sender).Name,False)
//  else
//    MsgInf('A tela de "Gera��o de Arquivo para Cart�es de Benefici�rios Bem Estar" j� est� aberta.');
//end;

procedure TFMenu.CadSegEmpresasClick(Sender: TObject);
begin
  if not vFormSegmentoEmpresa then
    MostrarForm(TFCadSegEmpresas, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Cadastro de Segmentos de Empresas" j� est� aberta.');
end;

procedure TFMenu.mniLanamentoCrditoRenovaoComplementoAlimentao1Click(Sender: TObject);
begin
//  MostrarForm(TfrmLancamentoAlimentacao, TMenuItem(sender).Name, False);
  MostrarForm(TFrmLancamentoExtraAlimentacao, TMenuItem(sender).Name, False);
end;

procedure TFMenu.CarregarBoletoEmpCartoAlimentao1Click(Sender: TObject);
begin
  MostrarForm(TFAnexarBoletoAlimentacao, TMenuItem(sender).Name, True);
end;

procedure TFMenu.CarregarLotedeBoletosAlimentao1Click(Sender: TObject);
begin
  MostrarForm(TFAnexarLoteBoletoAlimentacao, TMenuItem(sender).Name, True);
end;

procedure TFMenu.GerarAtualizarMensalidades1Click(Sender: TObject);
begin
  if not vFGeraLancamentoCartaBemEstar then
    MostrarForm(TFGeraLancamentoCartaBemEstar, TMenuItem(Sender).Name, False)
  else
    MsgInf('A tela de "Lan�amento de D�bitos Bem Estar" j� est� aberta.');
end;

procedure TFMenu.PainelRecargaCantinex1Click(Sender: TObject);
begin
   if not vFLogRecargaCantinex then
    MostrarForm(TFLogRecargaCantinex, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Painel de Controle - Recarga Cantinex" j� est� aberta.');
end;

procedure TFMenu.GeraNotadeDbitosEmpresas1Click(Sender: TObject);
begin
  if not vEmpGeraNotaDebito then
    MostrarForm(TfrmEmpGeraNotaDebito, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Gera��o de Nota" j� est� aberta.');
end;

procedure TFMenu.GerarArquivoConciliaoSpani1Click(Sender: TObject);
begin
  if not vExportConciliacaoSpani then
    MostrarForm(TFrmExportConciliacaoSpani, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Exporta��o de Arquivo de Concilia��o" j� est� aberta.');
end;

procedure TFMenu.ransfernciadeConveniadosOutrasEmpresas1Click(
  Sender: TObject);
begin
   if not vManutencaoEmpConv then

    MostrarForm(TFManutencaoEmpConv2, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Manuten��o e Transfer�ncia de funcion�rios" j� est� aberta.');
end;

procedure TFMenu.RelatriodeCrditos1Click(Sender: TObject);
begin
  MostrarForm( TFrmRelBemEstarCreditos, TMenuItem(Sender).Name, False)
end;

procedure TFMenu.HabilitarDescontoBemEstarConv1Click(Sender: TObject);
begin
  if not vfrmConvBemEstarDebitos then

    MostrarForm(TfrmConvBemEstarDebitos, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Desconto Bem Estar" j� est� aberta.');
end;

procedure TFMenu.CancelarDescontoBemEstar1Click(Sender: TObject);
begin
  if not vfrmCancelarConvBemEstarDebitos then

    MostrarForm(TfrmCancelarConvBemEstarDebitos, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Cancelamento Bem Estar" j� est� aberta.');
end;

procedure TFMenu.mniCadastrodeFiliais1Click(Sender: TObject);
begin
     MostrarForm(TFCadFil, TMenuItem(Sender).Name, False)
end;

procedure TFMenu.RelatriodeFaturas1Click(Sender: TObject);
begin
  MostrarForm(TFRelFaturas, TMenuItem(Sender).Name, False);
end;

procedure TFMenu.LanamentoDbitosMsAnterior1Click(Sender: TObject);
begin
   if not vFLancamentoDebitoMesAnterior then

    MostrarForm(TFLancamentoDebitoMesAnterior, TMenuItem(Sender).Name, False)
   else
    MsgInf('A tela de "Lan�amento de D�bitos do M�s Anterior" j� est� aberta.');
end;

procedure TFMenu.PaineldeContrataoBemEstar1Click(Sender: TObject);
begin
    if not vFLogBemEstarCompras then

      MostrarForm(TFLogBemEstarCompras, TMenuItem(Sender).Name, False)
    else
    MsgInf('A tela de "Painel de Contrata��es Bem Estar" j� est� aberta.');
end;

procedure TFMenu.RelatrioBemEstarMensal1Click(Sender: TObject);
begin
  MostrarForm(TFRelBemEstar, TMenuItem(Sender).Name, False)
end;

procedure TFMenu.HabDescOdontoClick(Sender: TObject);
begin
  if not vFAESPOdontoDebitos then

      MostrarForm(TfrmAESPOdontoDebitos, TMenuItem(Sender).Name, False)
    else
    MsgInf('A tela de "Lan�amento de D�bitos AESP Odonto" j� est� aberta.');

end;
                


procedure TFMenu.RelatrioNFServioMunicipal1Click(Sender: TObject);
begin
  MostrarForm(TFRelNFMunic, TMenuItem(Sender).Name, False)
end;

procedure TFMenu.AlterarValoreQuantidade1Click(Sender: TObject);
begin
  MostrarForm(TFrmAlterarValorEQtde, TMenuItem(Sender).Name, False);
end;

procedure TFMenu.DesabDescOdontoClick(Sender: TObject);
begin
    if not vFAESPOdontoCancelar then

      MostrarForm(TfrmCancelarConvAESPDebitos, TMenuItem(Sender).Name, False)
    else
    MsgInf('A tela de "Cancelamento de D�bitos AESP Odonto" j� est� aberta.');
end;

procedure TFMenu.RelatrioCartadeCircularizaoSpani1Click(Sender: TObject);
begin
  MostrarForm(TFRelCartaCircularizacaoSpani, TMenuItem(Sender).Name, False);
end;


procedure TFMenu.FechamentoEmpresas1Click(Sender: TObject);
begin
  MostrarForm(TFrmFechamentoEmpresa, TMenuItem(Sender).Name, False);
end;

end.

