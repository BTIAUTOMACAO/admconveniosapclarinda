inherited FrmRelInadimplentesBti: TFrmRelInadimplentesBti
  Left = 392
  Top = 91
  Caption = 'Relat'#243'rio de Pagamento BTI'
  ClientHeight = 550
  ClientWidth = 772
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 772
  end
  object PanAbe: TPanel [1]
    Left = 0
    Top = 23
    Width = 772
    Height = 146
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Bevel2: TBevel
      Left = 328
      Top = -7
      Width = 1
      Height = 193
    end
    object gpbDatas: TGroupBox
      Left = 6
      Top = 15
      Width = 259
      Height = 50
      Caption = 'Per'#237'odo do Pagamento'
      TabOrder = 0
      object dtpMesReferencia: TDateTimePicker
        Left = 8
        Top = 21
        Width = 81
        Height = 21
        Date = 43052.000000000000000000
        Time = 43052.000000000000000000
        DateMode = dmUpDown
        TabOrder = 0
        OnChange = dtpMesReferenciaChange
      end
    end
    object btnConsultar: TBitBtn
      Left = 232
      Top = 76
      Width = 81
      Height = 41
      Caption = '&Abrir'
      TabOrder = 1
      OnClick = btnConsultarClick
      Glyph.Data = {
        7E090000424D7E0900000000000036000000280000001D0000001B0000000100
        1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
        A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
        EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
        AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
        596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
        A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
        D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
        C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
        89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
        D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
        C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
        EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
        F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
        BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
        FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
        EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
        99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
        FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
        D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
        BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
        FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
        C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
        ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
        F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
        FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
        C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
        FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
        D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
        BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
        CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D400}
    end
    object grp1: TGroupBox
      Left = 6
      Top = 71
      Width = 211
      Height = 50
      Caption = 'Pagamento Realizado'
      TabOrder = 2
      object ckbPagamentoSim: TCheckBox
        Left = 7
        Top = 20
        Width = 81
        Height = 17
        Hint = 'Consultar todas as datas'
        Caption = 'Sim'
        TabOrder = 0
      end
      object chkPagamentoNao: TCheckBox
        Left = 79
        Top = 20
        Width = 81
        Height = 17
        Caption = 'N'#227'o'
        TabOrder = 1
      end
    end
  end
  object JvDBGrid1: TJvDBGrid [2]
    Left = 0
    Top = 169
    Width = 772
    Height = 351
    Align = alClient
    DataSource = dsPgtoEstab
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'CRED_ID'
        Title.Caption = 'Cod. Cred'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Title.Caption = 'Fantasia'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO_TAXA'
        Title.Caption = 'Descri'#231#227'o da Taxa'
        Width = 189
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR'
        Title.Caption = 'Valor da Taxa R$'
        Width = 98
        Visible = True
      end>
  end
  object Panel9: TPanel [3]
    Left = 0
    Top = 520
    Width = 772
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      772
      30)
    object Bevel1: TBevel
      Left = 946
      Top = 12
      Width = 2
      Height = 20
      Anchors = [akTop, akRight]
    end
    object btnImprimir: TBitBtn
      Left = 614
      Top = 0
      Width = 99
      Height = 28
      Hint = 'Op'#231#245'es de impress'#227'o de pagamentos a fornecedores'
      Anchors = [akTop, akRight]
      Caption = '&Visualizar'
      TabOrder = 0
      OnClick = BtnImprimirClick
      Glyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B0040000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC6CED2A8ADAFB0B1B2A8A6A6868585919394A69698987879A29A9AB4B5B5
        B2B3B4B2B7BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B8BAAAAB
        ACC2C2C2E7E6E6DADADAA0A1A19994945B4A494A4141606060858585AEAEADC2
        C1C1ACADAEB1B5B7FFFFFFFFFFFFFFFFFFBAC1C5A6A7A9B8B8B8E5E5E5F1F1F1
        EAEAEACECECE9999999696965858583939394545454F4F4F6E6E6E969595BAB9
        B9B7B7B7A1A2A4FFFFFFFFFFFFAEB0B1DBDBDAFAFAFAF3F3F3EDEDEDCDCDCDA2
        A2A27D7D7D8F8F8FA6A6A6A2A2A28A8A8A6F6F6F6D6D6D5C5C5C7171719A9B9B
        ACB2B4FFFFFFFFFFFFC3C3C2FFFFFFF2F2F2D0D0D09595959999999E9E9E7878
        787070707171718080809C9C9CAEAEAEA7A7A79090908F8B8DB1B0B2B7BEC1FF
        FFFFFFFFFFAFAFAED5D5D5939393959595C0C0C0C3C3C3C8C8C8BFBFBFA2A2A2
        9191918787877777776F6F6F828282A3A4A36AA27B7FA08BBBBDC5FFFFFFFFFF
        FF7F7F7E939393CDCDCDD7D7D7C7C7C7C1C1C1DADADAC5C5C5CDCDCDC9C9C9C2
        C2C2BDBDBDB5B5B59B9B9B7B7B7B757173848385B6BDC0FFFFFFFFFFFF979695
        F0F0F0D2D2D2C6C6C6C2C2C2DBDBDBBEBEBEC7C7C7C8C8C8B8B8B8B0B0B0BDBD
        BDBDBDBDC1C1C1CFCFCFC7C7C7A3A4A4B4BABEFFFFFFFFFFFFB3B7B8CBCBCBC6
        C6C6C3C3C3CCCCCCB8B8B8DDDDDDF5F5F5F2F2F2E9E9E9DFDFDFD4D4D4BFBFBF
        B1B1B1B1B1B1B1B0B0BBBDBEC1C9CDFFFFFFFFFFFFC4CCCFBDC2C5A5A7A8A8A8
        A8C3C4C4B5B7B8B0B1B1D1D1D1E0E0E0E1E1E1E6E6E6E9EAEAE9E9E9DFDFDFC0
        BFBF9D9E9EBBC1C5CBD3D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFFFE
        CEC7C1A9ADB0A1A5AA9EA1A4A6A8AAB6B7B9C2B6B6C1B7B7B3B4B4A7A9AABBC2
        C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1AFB0DEB094FED5A5F4
        CBA2EECAA7ECD2B7E3D3C2D6CBC1AB8D8DAAA5A7BEC6CAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8A8A8E3AC86FFD2A1FFCE9EFFCF
        9FFFD0A0FFD1A2F0C09BAD8D8DCAD5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB49A95FDD5ADFFD7B0FFD6B0FFD6B0FFD6B0
        FFDDB4C39A8DAF9495FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFB79188FFECD0FFE3C9FFE3C9FFE3C9FFE4CAFDE3C9B0
        8B89BEBCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB2A7AADABBAFFFEFDBFFEBD8FFEBD8FFEBD8FFF2DEDCBFB4AA8383FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA
        A2A3FFFFF8FFFFF8FFFFF8FFFFF8FFFFF9FFFFFEC4A5A1AB9394FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA89799B99797CBB0
        B0CAB0B0CAB0B0CAB0B0CAB1B0C9ADACB59999C2C2C6FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C1C5BDBABDBBB8BBBBB8BB
        BBB8BBBBB8BBBBB8BBBBB7BAC3C5C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
    end
    object btnGerarPDF: TBitBtn
      Left = 489
      Top = -2
      Width = 96
      Height = 30
      Anchors = [akTop, akRight]
      Caption = '&Gerar PDF'
      TabOrder = 1
      OnClick = btnGerarPDFClick
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
        FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
        9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
        FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
        FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
        E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
        B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
        FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
        FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
        F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
        F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
        F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
        F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
        F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
        F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
        EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
        F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
        EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
        EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
        A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
        00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
        3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
        99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
        85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
        AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
        FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
        03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
        3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
        FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
        1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
        BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 436
    Top = 112
  end
  object SD: TJvSaveDialog
    Filter = '.pdf|.pdf'
    Height = 0
    Width = 0
    Left = 192
    Top = 264
  end
  object dsPgtoEstab: TDataSource
    DataSet = qry1
    Left = 256
    Top = 232
  end
  object frxPgtoEstab: TfrxDBDataset
    UserName = 'frxPgtoEstab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FANTASIA=FANTASIA'
      'CRED_ID=cred_id'
      'CGC=cgc'
      'VALOR=VALOR'
      'DESCRICAO_TAXA=DESCRICAO_TAXA'
      'LOTE=LOTE'
      'DATA_COMPENSACAO=DATA_COMPENSACAO'
      'NOME=NOME'
      'TELEFONE=TELEFONE')
    DataSet = qry1
    BCDToCurrency = False
    Left = 392
    Top = 232
  end
  object QAux: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 912
    Top = 200
  end
  object DSAux: TDataSource
    DataSet = QAux
    Left = 912
    Top = 232
  end
  object popCancTaxa: TPopupMenu
    Left = 817
    Top = 246
    object CancelaAutorizao1: TMenuItem
      Caption = 'Cancelar Taxa'
    end
  end
  object frxReport2: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41353.581082893500000000
    ReportOptions.LastChange = 43486.604776307900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      ''
      'begin'
      ''
      'end.')
    Left = 296
    Top = 232
    Datasets = <
      item
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'TotalGeral'
        Value = Null
      end
      item
        Name = 'tipoPagamento'
        Value = Null
      end
      item
        Name = 'dtCompensa'
        Value = Null
      end
      item
        Name = 'dataIni'
        Value = Null
      end
      item
        Name = 'dataFin'
        Value = Null
      end
      item
        Name = 'tipoEstab'
        Value = Null
      end
      item
        Name = 'semana'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object PageHeader1: TfrxPageHeader
        Height = 98.267780000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Top = 30.236240000000000000
          Width = 1050.709340000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Inadimplentes BTI')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 1046.929810000000000000
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
        RowCount = 0
        object frxPgtoEstabFANTASIA: TfrxMemoView
          Left = 389.291590000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FANTASIA'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."FANTASIA"]')
        end
        object frxPgtoEstabcred_id: TfrxMemoView
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'cred_id'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."cred_id"]')
        end
        object frxPgtoEstabcgc: TfrxMemoView
          Left = 665.197280000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'TELEFONE'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxPgtoEstab."TELEFONE"]')
          ParentFont = False
        end
        object frxPgtoEstabVALOR: TfrxMemoView
          Left = 948.662030000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          DisplayFormat.FormatStr = 'R$ #,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."VALOR"]')
          ParentFont = False
        end
        object frxPgtoEstabNOME: TfrxMemoView
          Left = 60.472480000000000000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOME'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."NOME"]')
        end
        object frxPgtoEstabDESCRICAO_TAXA1: TfrxMemoView
          Left = 797.480830000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DESCRICAO_TAXA'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxPgtoEstab."DESCRICAO_TAXA"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 343.937230000000000000
        Width = 1046.929810000000000000
        object Memo3: TfrxMemoView
          Left = 60.472480000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 139.842610000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Time]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data/Hora:')
          ParentFont = False
        end
        object Page2: TfrxMemoView
          Left = 925.984850000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 979.118740000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '/')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          Left = 990.236860000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 1046.929810000000000000
        object Gradient1: TfrxGradientView
          Align = baClient
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          ShowHint = False
          BeginColor = clSilver
          EndColor = clSilver
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = clSilver
        end
        object Memo2: TfrxMemoView
          Left = 925.984850000000000000
          Top = 3.779530000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'R$ #,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[SUM(<frxPgtoEstab."VALOR">,MasterData1)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 820.158010000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Total:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 3.779530000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'N'#195#186'mero de Estabelecimentos:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 215.433210000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 18.897650000000000000
        Top = 177.637910000000000000
        Width = 1046.929810000000000000
        object Gradient2: TfrxGradientView
          Align = baClient
          Width = 1046.929810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          BeginColor = clSilver
          EndColor = clSilver
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = clSilver
        end
        object Memo16: TfrxMemoView
          Left = 389.291590000000000000
          Width = 264.567038980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'FANTASIA')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 665.197280000000000000
          Width = 132.283488980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'TELEFONE')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 948.662030000000000000
          Width = 102.047261180000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'C'#195#8220'DIGO')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 64.252010000000000000
          Width = 309.921460000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'NOME CREDENCIADO')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 797.480830000000000000
          Width = 151.181138980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'DESCRI'#195#8225#195#402'O DA TAXA')
          ParentFont = False
        end
      end
    end
  end
  object ReportData1: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41353.581082893500000000
    ReportOptions.LastChange = 43486.604776307900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      ''
      'begin'
      '                              '
      'end.')
    Left = 328
    Top = 232
    Datasets = <
      item
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'TotalGeral'
        Value = Null
      end
      item
        Name = 'tipoPagamento'
        Value = Null
      end
      item
        Name = 'dtCompensa'
        Value = Null
      end
      item
        Name = 'dataIni'
        Value = Null
      end
      item
        Name = 'dataFin'
        Value = Null
      end
      item
        Name = 'tipoEstab'
        Value = Null
      end
      item
        Name = 'semana'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object PageHeader1: TfrxPageHeader
        Height = 98.267780000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Top = 30.236240000000000000
          Width = 1050.709340000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Pagamento Efetuado BTI')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 1046.929810000000000000
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
        RowCount = 0
        object frxPgtoEstabFANTASIA: TfrxMemoView
          Left = 366.614410000000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FANTASIA'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxPgtoEstab."FANTASIA"]')
          ParentFont = False
        end
        object frxPgtoEstabcred_id: TfrxMemoView
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'cred_id'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."cred_id"]')
        end
        object frxPgtoEstabVALOR: TfrxMemoView
          Left = 971.339210000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VALOR'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          DisplayFormat.FormatStr = 'R$ #,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."VALOR"]')
          ParentFont = False
        end
        object frxPgtoEstabDESCRICAO: TfrxMemoView
          Left = 627.401980000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DESCRICAO_TAXA'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxPgtoEstab."DESCRICAO_TAXA"]')
          ParentFont = False
        end
        object frxPgtoEstabLOTE: TfrxMemoView
          Left = 891.969080000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'LOTE'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxPgtoEstab."LOTE"]')
          ParentFont = False
        end
        object frxPgtoEstabDATA_COMPENSACAO: TfrxMemoView
          Left = 778.583180000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DATA_COMPENSACAO'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxPgtoEstab."DATA_COMPENSACAO"]')
          ParentFont = False
        end
        object frxPgtoEstabNOME: TfrxMemoView
          Left = 64.252010000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOME'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."NOME"]')
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 343.937230000000000000
        Width = 1046.929810000000000000
        object Memo3: TfrxMemoView
          Left = 60.472480000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 139.842610000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Time]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data/Hora:')
          ParentFont = False
        end
        object Page2: TfrxMemoView
          Left = 925.984850000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 979.118740000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '/')
          ParentFont = False
        end
        object TotalPages: TfrxMemoView
          Left = 990.236860000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 1046.929810000000000000
        object Gradient1: TfrxGradientView
          Align = baClient
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          ShowHint = False
          BeginColor = clSilver
          EndColor = clSilver
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = clSilver
        end
        object Memo2: TfrxMemoView
          Left = 925.984850000000000000
          Top = 3.779530000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'R$ #,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[SUM(<frxPgtoEstab."VALOR">,MasterData1)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 820.158010000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Total:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'N'#195#186'mero de Estabelecimentos:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 219.212740000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 18.897650000000000000
        Top = 177.637910000000000000
        Width = 1046.929810000000000000
        object Gradient2: TfrxGradientView
          Align = baClient
          Width = 1046.929810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          BeginColor = clSilver
          EndColor = clSilver
          Style = gsVertical
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Color = clSilver
        end
        object Memo16: TfrxMemoView
          Left = 366.614410000000000000
          Width = 260.787508980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'FANTASIA')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 971.339210000000000000
          Width = 79.370081180000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'C'#195#8220'DIGO')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 64.252010000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'NOME CREDENCIADO')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 627.401980000000000000
          Width = 151.181138980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'DESCRI'#195#8225#195#402'O DA TAXA')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 778.583180000000000000
          Width = 113.385838980000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'COMPENSA'#195#8225#195#402'O')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 891.969080000000000000
          Width = 79.370081180000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'LOTE')
          ParentFont = False
        end
      end
    end
  end
  object frxPDFExport1: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 360
    Top = 264
  end
  object frxPDFExport2: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 360
    Top = 232
  end
  object qry1: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      'C.CRED_ID,'
      'C.FANTASIA,'
      'C.NOME,'
      'C.CGC,'
      'c.CORRENTISTA,'
      'B.BANCO,'
      'B.CODIGO COD_BANCO,'
      'C.AGENCIA,'
      'C.CONTACORRENTE,'
      'c.LIBERADO, '
      'taxas.TAXA_ID,'
      'TAXAS.DESCRICAO,'
      'taxas.VALOR '
      'FROM credenciados c '
      'inner join BANCOS B ON b.CODIGO = c.BANCO'
      'inner join REL_TAXA_CRED on c.CRED_ID = REL_TAXA_CRED.CRED_ID '
      'inner join TAXAS on TAXAS.TAXA_ID = REL_TAXA_CRED.TAXA_ID '
      
        'WHERE REL_TAXA_CRED.TAXA_ID in(8,23,25,27,29,31,33,35,38,42,46,4' +
        '8,50,56,59,61,63,65,67,69,71,74,88,92,102,103,105, 106,107,108) ' +
        ' AND REL_TAXA_CRED.CRED_ID NOT IN (SELECT CRED_ID FROM TAXAS_REP' +
        'ASSE  WHERE TAXA_ID = 23   AND DT_DESCONTO BETWEEN '#39'01/05/2016'#39' ' +
        'AND '#39'10/05/2016'#39') and c.APAGADO = '#39'N'#39)
    Left = 192
    Top = 232
    object q1: TStringField
      FieldName = 'FANTASIA'
      Size = 58
    end
    object ADORegra1: TIntegerField
      FieldName = 'CRED_ID'
    end
    object q2: TStringField
      FieldName = 'CGC'
      Size = 18
    end
    object q3: TFloatField
      FieldName = 'VALOR'
    end
    object q4: TStringField
      FieldName = 'DESCRICAO_TAXA'
    end
    object qry1LOTE: TIntegerField
      FieldName = 'LOTE'
    end
    object qry1DATA_COMPENSACAO: TStringField
      FieldName = 'DATA_COMPENSACAO'
    end
    object qry1NOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
    object qry1TELEFONE: TStringField
      FieldName = 'TELEFONE'
    end
  end
end
