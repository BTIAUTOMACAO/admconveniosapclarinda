unit UMovimentosDuvidosos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, {JvLookup,} StdCtrls, Grids,
  DBGrids, {JvDBCtrl,} DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  XMLDoc, JvExDBGrids, JvDBGrid, JvExControls, JvDBLookup, Mask, JvExMask,
  JvToolEdit, wsconvenio_new, XSBuiltIns, InvokeRegistry, Rio,
  SOAPHTTPClient, ShellApi, DBClient, DBTables, JvBDEMemTable, clipbrd,
  ZSqlUpdate, ADODB, saleService2, JvMemoryDataset;

type
  TFMovimentosDuvidosos = class(TF1)
    Panel1: TPanel;
    Panel2: TPanel;
    dsTrans: TDataSource;
    grdTrans: TJvDBGrid;
    Label1: TLabel;
    cbbEstab: TJvDBLookupCombo;
    dsEstab: TDataSource;
    btnConsultar: TBitBtn;
    edtEstab: TEdit;
    btnCancelarTrans: TBitBtn;
    btnConfirmarTrans: TBitBtn;
    dsListEmp: TDataSource;
    cbbEmp: TJvDBLookupCombo;
    edtEmp: TEdit;
    Label2: TLabel;
    edtCartao: TEdit;
    Label3: TLabel;
    dataIni: TJvDateEdit;
    Label4: TLabel;
    dataFin: TJvDateEdit;
    btnMarcaDesmEmp: TBitBtn;
    btnMarcaTodasEmp: TBitBtn;
    btnDesmarcaTodosEmp: TBitBtn;
    HTTPRIO1: THTTPRIO;
    Status: TLabel;
    lblStatus: TLabel;
    Label5: TLabel;
    cbOperador: TComboBox;
    qListEmp: TADOQuery;
    qListEstab: TADOQuery;
    qListEmpempres_id: TAutoIncField;
    qListEmpnome: TStringField;
    qListEstabcred_id: TIntegerField;
    qListEstabnome: TStringField;
    qListEstabfantasia: TStringField;
    qListEstabcodacesso: TIntegerField;
    qListEstabsenha: TStringField;
    qAutorTrans: TADOQuery;
    qTrans: TADOQuery;
    qAutorTranstrans_id: TIntegerField;
    qAutorTransautor_id: TIntegerField;
    qAutorTransdigito: TWordField;
    qAutorTransvalor: TBCDField;
    qAutorTransdatahora: TDateTimeField;
    qAutorTranshistorico: TStringField;
    qAutorTransformapagto_id: TIntegerField;
    qAutorTransreceita: TStringField;
    qAutorTransdata_venc_emp: TDateTimeField;
    qAutorTransdata_fecha_emp: TDateTimeField;
    qAutorTransdata_venc_for: TDateTimeField;
    qAutorTransdata_fecha_for: TDateTimeField;
    qAutorTransconv_id: TIntegerField;
    qAutorTranscred_id: TIntegerField;
    qAutorTranscartao_id: TIntegerField;
    qContaCorrente: TADOQuery;
    qContaCorrenteAUTORIZACAO_ID: TIntegerField;
    qContaCorrenteCARTAO_ID: TIntegerField;
    qContaCorrenteCONV_ID: TIntegerField;
    qContaCorrenteCRED_ID: TIntegerField;
    qContaCorrenteDIGITO: TWordField;
    qContaCorrenteDATA: TDateTimeField;
    qContaCorrenteHORA: TStringField;
    qContaCorrenteDATAVENDA: TDateTimeField;
    qContaCorrenteDEBITO: TBCDField;
    qContaCorrenteCREDITO: TBCDField;
    qContaCorrenteVALOR_CANCELADO: TBCDField;
    qContaCorrenteBAIXA_CONVENIADO: TStringField;
    qContaCorrenteBAIXA_CREDENCIADO: TStringField;
    qContaCorrenteENTREG_NF: TStringField;
    qContaCorrenteRECEITA: TStringField;
    qContaCorrenteCESTA: TStringField;
    qContaCorrenteCANCELADA: TStringField;
    qContaCorrenteDIGI_MANUAL: TStringField;
    qContaCorrenteTRANS_ID: TIntegerField;
    qContaCorrenteFORMAPAGTO_ID: TIntegerField;
    qContaCorrenteFATURA_ID: TIntegerField;
    qContaCorrentePAGAMENTO_CRED_ID: TIntegerField;
    qContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    qContaCorrenteOPERADOR: TStringField;
    qContaCorrenteDATA_VENC_EMP: TDateTimeField;
    qContaCorrenteDATA_FECHA_EMP: TDateTimeField;
    qContaCorrenteDATA_VENC_FOR: TDateTimeField;
    qContaCorrenteDATA_FECHA_FOR: TDateTimeField;
    qContaCorrenteHISTORICO: TStringField;
    qContaCorrenteNF: TIntegerField;
    qContaCorrenteDATA_ALTERACAO: TDateTimeField;
    qContaCorrenteDATA_BAIXA_CONV: TDateTimeField;
    qContaCorrenteDATA_BAIXA_CRED: TDateTimeField;
    qContaCorrenteOPER_BAIXA_CONV: TStringField;
    qContaCorrenteOPER_BAIXA_CRED: TStringField;
    qContaCorrenteDATA_CONFIRMACAO: TDateTimeField;
    qContaCorrenteOPER_CONFIRMACAO: TStringField;
    qContaCorrenteCONFERIDO: TStringField;
    qContaCorrenteNSU: TIntegerField;
    qContaCorrentePREVIAMENTE_CANCELADA: TStringField;
    qContaCorrenteEMPRES_ID: TIntegerField;
    qTransdatahora: TDateTimeField;
    qTransnsu: TIntegerField;
    qTranstrans_id: TIntegerField;
    qTransoperador: TStringField;
    qTransconv_id: TIntegerField;
    qTranstitular: TStringField;
    qTranscred_id: TIntegerField;
    qTranscred_id_nome: TStringField;
    qTranstelefone1: TStringField;
    qTransempres_id: TIntegerField;
    qTransnome: TStringField;
    qTransvalor: TBCDField;
    qTranshistorico: TStringField;
    qTranscartao: TStringField;
    qEmp: TADOQuery;
    qEmpempres_id: TAutoIncField;
    qEmpnome: TStringField;
    MTrans: TJvMemoryData;
    MTransDATAHORA: TDateTimeField;
    MTransNSU: TIntegerField;
    MTransTRANS_ID: TIntegerField;
    MTransOPERADOR: TStringField;
    MTransCONV_ID: TIntegerField;
    MTransTITULAR: TStringField;
    MTransCRED_ID: TIntegerField;
    MTransCRED_ID_NOME: TStringField;
    MTransTELEFONE1: TStringField;
    MTransEMPRES_ID: TIntegerField;
    MTransNOME: TStringField;
    MTransVALOR: TFloatField;
    MTransHISTORICO: TStringField;
    MTransMARCADO: TBooleanField;
    lblTotal: TLabel;
    lblRegistros: TLabel;
    qTransseg_id: TIntegerField;
    qTransband_id: TIntegerField;
    MTransBAND_ID: TIntegerField;
    MTransSEG_ID: TIntegerField;
    qTranscodacesso: TIntegerField;
    qTranssenha: TStringField;
    MTransCODACESSO: TIntegerField;
    MTransSENHA: TStringField;
    MTransCARTAO: TStringField;
    procedure edtEstabChange(Sender: TObject);
    procedure edtEstabKeyPress(Sender: TObject; var Key: Char);
    procedure cbbEstabChange(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarTransClick(Sender: TObject);
    procedure btnConfirmarTransClick(Sender: TObject);
    procedure cbbEmpChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnMarcaDesmEmpClick(Sender: TObject);
    procedure btnMarcaTodasEmpClick(Sender: TObject);
    procedure btnDesmarcaTodosEmpClick(Sender: TObject);
    procedure grdTransDblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEmpChange(Sender: TObject);
  private
    function getTipoNSU(tipo : String) : Byte;
    function getTotalTransacao(transId : Integer) : Double;
    function getCodAcesso(estabId : Integer) : Integer;
    procedure montarSelectSimples;
    procedure montarSelectComJoins;
    procedure montarSelect;
    function usaSelectSimples : Boolean;
    procedure Trans_Sel;
    { Private declarations }
  public
    { Public declarations }
    Path_WebService: string;
    Path_SaleService: string;
    autor_sel : String;
  end;

var
  FMovimentosDuvidosos: TFMovimentosDuvidosos;
  Sql, sqlQuery : String;
implementation

uses DM, cartao_util, MEfetuarTransacao, UMenu, Math, URotinasTexto,
  FOcorrencia;

{$R *.dfm}

function TFMovimentosDuvidosos.getTipoNSU(tipo : String): Byte;
begin
  if (UpperCase(tipo) = 'POS') then
    Result := 15
  else if (UpperCase(tipo) = 'GETNET') then
    Result := 13
  else if (UpperCase(tipo) = 'POS.DB') then
    Result := 20
  else
    Result := 1;
end;

function TFMovimentosDuvidosos.getTotalTransacao(transId : Integer) : Double;
begin
  Result := DMConexao.ExecuteScalar('select sum(atr.valor) from autor_transacoes atr where atr.trans_id = ' + IntToStr(transId),0.00);
end;

function TFMovimentosDuvidosos.getCodAcesso(estabId : Integer) : Integer;
begin
  Result := DMConexao.ExecuteScalar('select cred.codacesso from credenciados cred where cred.cred_id = ' + IntToStr(estabId),-1);
end;

procedure TFMovimentosDuvidosos.montarSelectComJoins;
var total : double;
contator : integer;
begin
  qTrans.Close;
  qTrans.SQL.Clear;
  qTrans.SQL.Add('select');
  qTrans.SQL.Add('  t.datahora,');
  qTrans.SQL.Add('  t.nsu,');
  qTrans.SQL.Add('  t.trans_id,');
  qTrans.SQL.Add('  t.operador,');
  qTrans.SQL.Add('  atr.conv_id,');
  qTrans.SQL.Add('  c.titular,');
  qTrans.SQL.Add('  cred.cred_id,');
  qTrans.SQL.Add('  cred.fantasia cred_id_nome,');
  qTrans.SQL.Add('  cred.telefone1,');
  qTrans.SQL.Add('  t.empres_id,');
  qTrans.SQL.Add('  emp.nome,');
  qTrans.SQL.Add('  atr.valor,');
  qTrans.SQL.Add('  atr.historico,');
  qTrans.SQL.Add('  t.cartao, emp.band_id, cred.seg_id, cred.codacesso, cred.senha ');
  qTrans.SQL.Add('from transacoes t');
  qTrans.SQL.Add('join autor_transacoes atr on atr.trans_id = t.trans_id');
  qTrans.SQL.Add('join credenciados cred on cred.cred_id = t.cred_id');
  qTrans.SQL.Add('join conveniados c on c.conv_id = atr.conv_id');
  qTrans.SQL.Add('join cartoes cart on cart.cartao_id = t.cartao_id');
  qTrans.SQL.Add('join empresas emp on emp.empres_id = c.empres_id');
  qTrans.SQL.Add('where t.aberta = ''S'' ');
  qTrans.SQL.Add('and t.confirmada = ''N'' ');
  qTrans.SQL.Add('and t.cancelado = ''N'' ');
  qTrans.SQL.Add('and t.datahora > ' + QuotedStr('22.04.2014 12:00:00'));
  qTrans.SQL.Add('and t.datahora between coalesce('''+dataIni.Text+' 00:00:00'',t.datahora) and coalesce('''+dataFin.Text+' 23:59:59'',t.datahora)');
  if cbbEstab.KeyValue <> 0 then
    qTrans.SQL.Add('and t.cred_id = '+cbbEstab.KeyValue+'');
  if cbbEmp.KeyValue <> 0 then
    qTrans.SQL.Add('and emp.empres_id = coalesce('+cbbEmp.KeyValue+', emp.empres_id)');
  if Trim(edtCartao.Text) <> '' then
    qTrans.SQL.Add('and cart.codcartimp = coalesce('''+edtCartao.Text+''', cart.codcartimp)');
  if cbOperador.Text <> 'TODOS' then
    qTrans.SQL.Add('and t.operador = coalesce('''+cbOperador.Text+''', t.operador)');
  qTrans.SQL.Add(' order by t.datahora');
  qTrans.Open;
  if qTrans.IsEmpty then begin
      lblRegistros.Caption := '';
      lblTotal.Caption := '';
      MsgInf('Nenhum registro encontrado');
      MTrans.EmptyTable;
      btnConsultar.SetFocus;
      Exit;
  end;

  qTrans.First;

  contator := 0;
  total :=0;
  MTrans.Open;
  MTrans.EmptyTable;
  MTrans.DisableControls;
  while not qTrans.Eof do begin
      MTrans.Append;
      MTransDatahora.AsDateTime := QTransDataHora.AsDateTime;
      MTransNSU.AsInteger := QTransNSU.AsInteger;
      MTransTrans_id.AsInteger := QTransTrans_id.AsInteger;
      MTransOperador.AsString := QTransOperador.AsString;
      MTransConv_id.AsInteger := QTransConv_id.AsInteger;
      MTransTitular.AsString := QTransTitular.AsString;
      MTransCred_id.AsInteger := QTransCred_id.AsInteger;
      MTransCred_id_nome.AsString := QTransCred_id_nome.AsString;
      MTransTelefone1.AsString := QTransTelefone1.AsString;
      MTransEmpres_id.AsInteger := QTransEmpres_id.AsInteger;
      MTransNome.AsString := QTransNome.AsString;
      MTransValor.AsFloat := QTransValor.AsFloat;
      MTransHistorico.AsString := QTransHistorico.AsString;
      MTransBand_id.AsInteger := QTransBand_id.AsInteger;
      MTransSeg_id.AsInteger := QTransSeg_id.AsInteger;
      MTransCodacesso.AsInteger := QTransCodacesso.AsInteger;
      MTransSenha.AsString := QTransSenha.AsString;
      MTransCartao.AsString := QTransCartao.AsString;
      MTransMarcado.AsBoolean := False;
      MTrans.Post;
      contator := contator + 1;
      total := total + QTransValor.AsFloat;
      qTrans.Next;
  end;
  MTrans.First;
  MTrans.EnableControls;
  autor_sel := EmptyStr;

  lblRegistros.Caption := 'Total de Registros: '+IntToStr(contator);
  lblTotal.Caption := 'Total: R$ ' + FormatFloat('0.00',ArredondaDin(total));


end;

procedure TFMovimentosDuvidosos.montarSelectSimples;
begin
  qTrans.Close;
  qTrans.SQL.Clear;
  qTrans.SQL.Add('select');
  qTrans.SQL.Add('  t.nsu,');
  qTrans.SQL.Add('  t.trans_id,');
  qTrans.SQL.Add('  t.cred_id,');
  qTrans.SQL.Add('  t.cartao_id,');
  qTrans.SQL.Add('  t.datahora,');
  qTrans.SQL.Add('  t.cartao,');
  qTrans.SQL.Add('  t.cpf,');
  qTrans.SQL.Add('  t.operador,');
  qTrans.SQL.Add('  atr.conv_id,');
  qTrans.SQL.Add('  atr.valor,');
  qTrans.SQL.Add('  atr.historico,');
  qTrans.SQL.Add('  ''N'' marcado');
  qTrans.SQL.Add('from transacoes t');
  qTrans.SQL.Add('join autor_transacoes atr on atr.trans_id = t.trans_id');
  qTrans.SQL.Add('where t.aberta = ''S'' ');
  qTrans.SQL.Add('and t.confirmada = ''N'' ');
  qTrans.SQL.Add('and t.cancelado = ''N'' ');
  qTrans.SQL.Add('and t.cred_id = iif(:cred_id = 0 or :cred_id = null, t.cred_id, :cred_id)');
  //Fixada a data '30.04.2014  12:00:00 pois a regra vai ser usada a partir dessa data,
  //pois se for usasda antes ir� dainificar os saldos anteriores.
  qTrans.SQL.Add('and t.datahora > ' + QuotedStr('30.04.2014 12:00:00'));
  qTrans.SQL.Add('and t.datahora > ' + QuotedStr('22.04.2014 00:00:00'));
  qTrans.SQL.Add('and t.datahora between coalesce(:dataIni,t.datahora) and coalesce(:dataFin,t.datahora)');

  qTrans.Parameters.ParamByName('cred_id').Value := cbbEstab.KeyValue;
  qTrans.Parameters.ParamByName('dataIni').Value := dataIni.Date;
  qTrans.Parameters.ParamByName('dataFin').Value := dataFin.Date + 1;

  //clipboard.astext := qTrans.sql.Text;
  qTrans.Open;
end;

function TFMovimentosDuvidosos.usaSelectSimples : Boolean;
begin
  Result := (cbbEmp.KeyValue = 0) and (Trim(edtCartao.Text) = '');
end;

procedure TFMovimentosDuvidosos.montarSelect;
begin
  try
    montarSelectComJoins;
    except on E:Exception do begin
      MsgInf('Erro: ' + E.message);
    end;
  end;
end;

procedure TFMovimentosDuvidosos.edtEstabChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEstab.Text) <> '' then begin
    if qListEstab.Locate('cred_id',edtEstab.Text,[]) then
      cbbEstab.KeyValue := edtEstab.Text
    else
      cbbEstab.ClearValue;
  end else
    cbbEstab.ClearValue;
end;

procedure TFMovimentosDuvidosos.edtEstabKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  IsNumericKey(Sender,Key);
end;

procedure TFMovimentosDuvidosos.edtEmpChange(Sender: TObject);
begin
  inherited;
  if Trim(edtEmp.Text) <> '' then begin
    if qListEmp.Locate('empres_id',edtEmp.Text,[]) then
      cbbEmp.KeyValue := edtEmp.Text
    else
      cbbEmp.ClearValue;
  end else
    cbbEmp.ClearValue;
end;

procedure TFMovimentosDuvidosos.cbbEstabChange(Sender: TObject);
begin
  inherited;
  if edtEstab.Text <> cbbEstab.KeyValue then
    edtEstab.Text := string(cbbEstab.KeyValue);
end;

procedure TFMovimentosDuvidosos.cbbEmpChange(Sender: TObject);
begin
  inherited;
  if edtEmp.Text <> cbbEmp.KeyValue then
    edtEmp.Text := string(cbbEmp.KeyValue);
end;

procedure TFMovimentosDuvidosos.btnConsultarClick(Sender: TObject);
var dtIni, dtFin : String;
begin
  inherited;
  dtIni := StringReplace(dataIni.Text, ' ', '',[rfReplaceAll]);
  dtFin := StringReplace(dataFin.Text, ' ', '',[rfReplaceAll]);
  if (dtIni = '//') and (dtFin = '//') then begin
    msgErro('Digite um per�odo');
    dataIni.SetFocus;
    Exit;
  end;
  if (dtIni <> '//') or (dtFin <> '//') then begin
    if (not isDate(dtIni)) then begin
      msgErro('Digite uma data Inicial V�lida');
      dataIni.SetFocus;
      Exit;
    end;
    if (not isDate(dtFin)) then begin
      msgErro('Digite uma data Final V�lida');
      dataFin.SetFocus;
      Exit;
    end;
  end;
  screen.Cursor := crHourGlass;
  lblStatus.Caption := 'Buscando registros...';
  Application.ProcessMessages;
  qEmp.Open;
  montarSelect;
  grdTrans.SetFocus;
  lblStatus.Caption := '';
  Application.ProcessMessages;
  qTrans.Open;
  screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.FormCreate(Sender: TObject);
begin
  inherited;
  qListEstab.Open;
  qListEmp.Open;
  if not DMConexao.Config.Active then
    DMConexao.Config.Open;

  if DMConexao.ConfigPATH_WEBSERVICE.AsString <> '' then
    Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString + 'wsconvenio.asmx'
  else
    msgErro('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');

  HTTPRIO1.URL := Path_WebService;
  HTTPRIO1.WSDLLocation := Path_WebService;
  HTTPRIO1.Service := 'wsconvenio';
  HTTPRIO1.Port :=  'wsconvenioSoap';
  DMConexao.Config.Close;
end;

procedure TFMovimentosDuvidosos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if qTrans.Active then
    qTrans.Close;
  qListEstab.Close;
  //qListEstab.Close;
  //qListCartao.Close;
  //qListConv.Close;
  qListEmp.Close;
end;

procedure TFMovimentosDuvidosos.btnCancelarTransClick(Sender: TObject);
var
  strings: TStrings;
  Retorno, status: string;
  Param : IXMLTCancelarTransacaoParam;
  Ret : IXMLTCancelarTransacaoRet;
  reverseRequest : ReversalRequest;
  reverseResponse : ReversalResponse;
  id : IdentificadorEstab;
  codLimite : integer;
  contator : integer;
  total : double;
  solicitante,motivo : string;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  if not qTrans.IsEmpty then begin
    lblStatus.Caption := 'Contando registros selecionados';
    Application.ProcessMessages;
    if DMConexao.ContaMarcados(MTrans) = 0 then begin
      MsgInf('Selecione algum(ns) ou todos as notas para serem canceladas');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if MsgSimNao('Deseja cancelar a(s) transa��o(�es)?', False) then begin
      FrmOcorrencia := TFrmOcorrencia.Create(Self);
      if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
        solicitante := FrmOcorrencia.edtSolicitante.Text;
        motivo := FrmOcorrencia.mmoMotivo.Text;
        Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
        Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
        FreeAndNil(FrmOcorrencia);
        //Result := True;
      end else begin
        //Result := False;
        FreeAndNil(FrmOcorrencia);
        MsgInf('Informa��es da Ocorr�ncia devem ser preenchidas');
        Application.ProcessMessages;
        Exit;
      end;
      strings := TStringList.Create;
      MTrans.First;
      while not MTrans.Eof do begin
        if MTransMarcado.AsBoolean = true then begin
            try
              lblStatus.Caption := 'Cancelando transa��o N�' + MTransTRANS_ID.AsString + '...';
              Application.ProcessMessages;
              DMConexao.ExecuteSql('DELETE FROM AUTOR_TRANSACOES WHERE TRANS_ID = ' + MTransTRANS_ID.AsString);
              DMConexao.ExecuteSql('UPDATE TRANSACOES SET ABERTA = ''N'' WHERE TRANS_ID = ' + MTransTRANS_ID.AsString);
              codLimite := 1;
              if MTransBand_id.AsInteger <> 999 then begin
                DMConexao.AdoQry.SQL.Clear;
                DMConexao.AdoQry.SQL.Add('select coalesce(cod_limite,1) as cod_limite from bandeiras_segmentos where band_id ='+MTransBand_id.AsString+'and seg_id ='+MTransSeg_id.AsString+'');
                DMConexao.AdoQry.Open;
                codLimite := DMConexao.AdoQry.Fields[0].AsInteger
              end;
              DecimalSeparator := '.';
              if codLimite = 1 then
                DMConexao.ExecuteSql('UPDATE CONVENIADOS SET CONSUMO_MES_1 = COALESCE(COALESCE(CONSUMO_MES_1,0) - '+ MTransValor.AsString+ ',0) WHERE CONV_ID = '+MTransConv_id.AsString+'')
              else if codLimite = 2 then
                DMConexao.ExecuteSql('UPDATE CONVENIADOS SET CONSUMO_MES_2 = COALESCE(COALESCE(CONSUMO_MES_2,0) - '+ MTransValor.AsString+ ',0) WHERE CONV_ID = '+MTransConv_id.AsString+'')
              else if codLimite = 3 then
                DMConexao.ExecuteSql('UPDATE CONVENIADOS SET CONSUMO_MES_3 = COALESCE(COALESCE(CONSUMO_MES_3,0) - '+ MTransValor.AsString+ ',0) WHERE CONV_ID = '+MTransConv_id.AsString+'')
              else if codLimite = 4 then
                DMConexao.ExecuteSql('UPDATE CONVENIADOS SET CONSUMO_MES_4 = COALESCE(COALESCE(CONSUMO_MES_4,0) - '+ MTransValor.AsString+ ',0) WHERE CONV_ID = '+MTransConv_id.AsString+'');

              status := 'transa��o N� ' + MTransTRANS_ID.AsString + ' cancelada com sucesso!' + sLineBreak;

              DMConexao.AdoQry.Close;
              DMConexao.AdoQry.SQL.Clear;

              DMConexao.AdoQry.SQL.Add('INSERT INTO LOG_MOVIMENTOSDUVIDOSOS(LOG_ID,DATAHORA,AUTOR_TRANSACAO,TRANSACAO,OPERADOR,OPERACAO,VALOR_ANT,VALOR_POS,OPERADOR_AUT,NSU,CONV_ID,CRED_ID,EMPRES_ID,VALOR,DATAHORA_AUTORIZACAO,CANCELADO,MOTIVO,SOLICITANTE)');
              DMConexao.AdoQry.SQL.Add('VALUES');

              DMConexao.AdoQry.SQL.Add(' (next value for SLOGMOVIMENTOS_DUVIDOSOS,current_timestamp,'+QuotedStr('')+','+QuotedStr(MTransTRANS_ID.AsString)+',');
              DMConexao.AdoQry.SQL.Add(QuotedStr(Operador.Nome)+','+QuotedStr('Movimentos Duvidosos')+','+QuotedStr('Movimento em Analise')+','+QuotedStr('Movimento cancelado'));
              DMConexao.AdoQry.SQL.Add(',' + QuotedStr(MTransOPERADOR.AsString)+','+MTransNSU.AsString+','+MTransCONV_ID.AsString+',');
              DMConexao.AdoQry.SQL.Add(MTransCRED_ID.AsString+','+MTransEMPRES_ID.AsString+','+MTransVALOR.AsString+','+QuotedStr(MTransDATAHORA.AsString)+','+'''S'''+','+QuotedStr(motivo)+','+QuotedStr(solicitante)+')');



              DMConexao.AdoQry.SQL.Text;
              DMConexao.AdoQry.ExecSQL;
              DMConexao.AdoQry.Close;

              except on E:Exception do begin
                status := 'Erro ao cancelar a transa��o N� ' + MTransTRANS_ID.AsString + '. O Erro ocorrido foi: ' + e.Message + sLineBreak;
              end;
            end;
					end;
        if (status <> '') then
          MsgInf(status);
        status := '';
        MTrans.Next;
      end;
      MTrans.Refresh;
    end;
  end;
  QTrans.Requery;
  qTrans.First;

  contator := 0;
  total :=0;
  MTrans.Open;
  MTrans.EmptyTable;
  MTrans.DisableControls;
  while not qTrans.Eof do begin
      MTrans.Append;
      MTransDatahora.AsDateTime := QTransDataHora.AsDateTime;
      MTransNSU.AsInteger := QTransNSU.AsInteger;
      MTransTrans_id.AsInteger := QTransTrans_id.AsInteger;
      MTransOperador.AsString := QTransOperador.AsString;
      MTransConv_id.AsInteger := QTransConv_id.AsInteger;
      MTransTitular.AsString := QTransTitular.AsString;
      MTransCred_id.AsInteger := QTransCred_id.AsInteger;
      MTransCred_id_nome.AsString := QTransCred_id_nome.AsString;
      MTransTelefone1.AsString := QTransTelefone1.AsString;
      MTransEmpres_id.AsInteger := QTransEmpres_id.AsInteger;
      MTransNome.AsString := QTransNome.AsString;
      MTransValor.AsFloat := QTransValor.AsFloat;
      MTransHistorico.AsString := QTransHistorico.AsString;
      MTransBand_id.AsInteger := QTransBand_id.AsInteger;
      MTransSeg_id.AsInteger := QTransSeg_id.AsInteger;
      MTransCodacesso.AsInteger := QTransCodacesso.AsInteger;
      MTransSenha.AsString := QTransSenha.AsString;
      MTransCartao.AsString := QTransCartao.AsString;
      MTransMarcado.AsBoolean := False;
      MTrans.Post;
      contator := contator + 1;
      total := total + QTransValor.AsFloat;
      qTrans.Next;
  end;
  MTrans.First;
  MTrans.EnableControls;
  autor_sel := EmptyStr;

  lblRegistros.Caption := 'Total de Registros: '+IntToStr(contator);
  lblTotal.Caption := 'Total: R$ ' + FormatFloat('0.00',ArredondaDin(total));

  lblStatus.Caption := '';
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.btnConfirmarTransClick(Sender: TObject);
var
  strings : TStrings;
  Retorno, status, sql, sql2, confTrans: string;
  Param : IXMLTConfirmarTransacaoParam;
  Ret : IXMLTConfirmarTransacaoRet;
  saleConfirm : SaleConfirmation;
  id : IdentificadorEstab;
  contator : integer;
  total : double;
  sqlLog : String;
  solicitante,motivo : string;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  if not MTrans.IsEmpty then begin
    lblStatus.Caption := 'Contando registros selecionados';
    Application.ProcessMessages;
    if DMConexao.ContaMarcados(MTrans) = 0 then begin
      MsgInf('Selecione algum(ns) ou todos as notas para serem confirmadas');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if MsgSimNao('Deseja confirmar a(s) transa��o(�es)?', False) then begin
      FrmOcorrencia := TFrmOcorrencia.Create(Self);
      if (FrmOcorrencia.ShowModal = mrOk) and ((Trim(FrmOcorrencia.edtSolicitante.Text) <> '') and (Trim(FrmOcorrencia.mmoMotivo.Text) <> '')) then begin
        solicitante := FrmOcorrencia.edtSolicitante.Text;
        motivo := FrmOcorrencia.mmoMotivo.Text;
        Ocorrencia.Solicitante :=  FrmOcorrencia.edtSolicitante.Text;
        Ocorrencia.Motivo :=  FrmOcorrencia.mmoMotivo.Text;
        FreeAndNil(FrmOcorrencia);
        //Result := True;
      end else begin
        //Result := False;
        FreeAndNil(FrmOcorrencia);
        MsgInf('Informa��es da Ocorr�ncia devem ser preenchidas');
        Application.ProcessMessages;
        Exit;
      end;
      strings := TStringList.Create;
      MTrans.First;
      while not MTrans.Eof do begin
        if MTransMarcado.AsBoolean = true then begin
					if (MTransNSU.AsInteger = 0) then begin
					  DMConexao.Config.Open;
					  Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString;
					  DMConexao.Config.Close;
					  if Path_WebService <> '' then
						  Path_WebService := Path_WebService + 'wsconvenio.asmx'
					  else begin
  						MsgErro('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');
	  					Exit;
					  end;
					  Param :=  NewXMLDocument.GetDocBinding('CONFIRMAR_TRANSACAO_PARAM', TXMLTConfirmarTransacaoParam, '') as IXMLTConfirmarTransacaoParam;
					  Param.CREDENCIADO.CODACESSO := MTransCodAcesso.AsInteger;
					  Param.CREDENCIADO.SENHA := Crypt('D',MTransSenha.AsString,'BIGCOMPRAS');
					  Param.TRANSID := MTransTrans_id.AsInteger;
					  Param.CARTAO := MTransCartao.AsString;
					  Param.CPF := '';
					  Param.DOCFISCAL := 0;
					  try
  						Retorno := GetwsconvenioSoap(False,Path_WebService).MConfirmarTransacao(Param.XML);
  					  except on e:Exception do begin
	  					  status := 'Erro ao confirmar a transa��o N� ' + MTransTRANS_ID.AsString + '. O erro ocorrido foi:' + e.Message + sLineBreak;
                MsgErro(status);
                Screen.Cursor := crDefault;
			  			  Exit;
				  		end;
					  end;
					  Ret := LoadXMLData(retorno).GetDocBinding('CONFIRMAR_TRANSACAO_RET',TXMLTConfirmarTransacaoRet,'') as IXMLTConfirmarTransacaoRet;
					  if Ret.STATUS <> 0 then begin
  						status := 'N�o foi poss�vel confirmar a transa��o N� ' + MTransTRANS_ID.AsString + '. O erro ocorrido foi:' + Ret.MSG + sLineBreak;
              MsgErro(status);
              Screen.Cursor := crDefault;
		  				exit;
					  end else begin;
				  		status := 'Confirma��o da transa��o ' + MTransTRANS_ID.AsString + ' efetuada com sucesso!';

					  end;
					  qTrans.Refresh;
					end else begin
            lblStatus.Caption := 'Confirmando transa��o N�' + MTransTRANS_ID.AsString + '...';
            Application.ProcessMessages;
            qAutorTrans.Close;
            qAutorTrans.Parameters.ParamByName('TRANS_ID').Value := MTransTRANS_ID.AsInteger;
            qAutorTrans.SQL.Text;
            qAutorTrans.Open;

            qAutorTrans.First;
            qContaCorrente.Open;
            try
              while not qAutorTrans.Eof do begin
                qContaCorrente.Close;
                qContaCorrente.Open;
                qContaCorrente.Append;
                qContaCorrenteAUTORIZACAO_ID.AsInteger := qAutorTransAUTOR_ID.AsInteger;
                qContaCorrenteDIGITO.AsInteger := qAutorTransDIGITO.AsInteger;
                qContaCorrenteDATAVENDA.AsString := formatDateTime('dd/mm/yyyy hh:nn:ss',qAutorTransDATAHORA.AsVariant);
                qContaCorrenteDATA.AsString := formatDateTime('dd/mm/yyyy 00:00:00',qAutorTransDATAHORA.AsVariant);
                qContaCorrenteHORA.AsString := formatDateTime('hh:nn:ss',qAutorTransDATAHORA.AsVariant);
                qContaCorrenteCONV_ID.AsInteger := qAutorTransCONV_ID.AsInteger;
                qContaCorrenteCARTAO_ID.AsInteger := qAutorTransCARTAO_ID.AsInteger;
                qContaCorrenteCRED_ID.AsInteger := qAutorTransCRED_ID.AsInteger;
                qContaCorrenteDEBITO.AsCurrency := qAutorTransVALOR.AsCurrency;
                qContaCorrenteCREDITO.AsCurrency := 0.00;
                qContaCorrenteOPERADOR.AsString := qTransOPERADOR.AsString;
                qContaCorrenteHISTORICO.AsString := qAutorTransHISTORICO.AsString;
                qContaCorrenteFORMAPAGTO_ID.AsInteger := qAutorTransFORMAPAGTO_ID.AsInteger;
                qContaCorrenteBAIXA_CONVENIADO.AsString := 'N';
                qContaCorrenteBAIXA_CREDENCIADO.AsString := 'N';
                qContaCorrenteRECEITA.AsString := qAutorTransRECEITA.AsString;
                qContaCorrenteDATA_VENC_EMP.AsString := qAutorTransDATA_VENC_EMP.AsString;
                qContaCorrenteDATA_FECHA_EMP.AsString := qAutorTransDATA_FECHA_EMP.AsString;
                qContaCorrenteDATA_VENC_FOR.AsString := qAutorTransDATA_VENC_FOR.AsString;
                qContaCorrenteDATA_FECHA_FOR.AsString := qAutorTransDATA_FECHA_FOR.AsString;
                qContaCorrenteTRANS_ID.AsInteger := qAutorTransTRANS_ID.AsInteger;
                qContaCorrentePREVIAMENTE_CANCELADA.AsString := 'N';
                qContaCorrenteNSU.AsInteger := MTransNSU.AsInteger;
                qContaCorrente.SQL.Text;
                qContaCorrente.Post;  
                qAutorTrans.Next;
              end;

              DMConexao.ExecuteSql('UPDATE TRANSACOES SET ABERTA = ''N'', CONFIRMADA = ''S'' WHERE TRANS_ID = ' + MTransTRANS_ID.AsString);
              //----------------LOG

              sqlLog := 'INSERT INTO LOG_MOVIMENTOSDUVIDOSOS(LOG_ID,DATAHORA,AUTOR_TRANSACAO,TRANSACAO,OPERADOR,OPERACAO,VALOR_ANT,VALOR_POS,OPERADOR_AUT,NSU,CONV_ID,CRED_ID,EMPRES_ID,VALOR,DATAHORA_AUTORIZACAO,CANCELADO,MOTIVO,SOLICITANTE)'
                        +' VALUES '
                        +' (next value for SLOGMOVIMENTOS_DUVIDOSOS,current_timestamp,'+IntToStr(qAutorTransautor_id.AsInteger)+','+QuotedStr(MTransTRANS_ID.AsString)+','+QuotedStr(Operador.Nome)+','+QuotedStr('Movimentos Duvidosos')+','+QuotedStr('Movimento em Analise')+','+QuotedStr('Movimento confirmado')
                        +',' + QuotedStr(MTransOPERADOR.AsString)+','+MTransNSU.AsString+','+MTransCONV_ID.AsString+','
                        +MTransCRED_ID.AsString+','+MTransEMPRES_ID.AsString+','+MTransVALOR.AsString+','+QuotedStr(MTransDATAHORA.AsString)+','+'''N'''+','+QuotedStr(motivo)+','+QuotedStr(solicitante)
                        +')';

              DMConexao.ExecuteSql(sqlLog);

              //----------------LOG
              status := 'Confirma��o da transa��o ' + MTransTRANS_ID.AsString + ' efetuada com sucesso!';
              except on E:Exception do begin
                status := 'N�o foi poss�vel confirmar a transa��o N�: ' + MTransTRANS_ID.AsString;
                Break;
              end;
              end;
            qContaCorrente.Close;
					end;
        end;
        if (status <> '') then
          MsgInf(status);
        status := '';
        MTrans.Next;
      end;
      MTrans.Refresh;
    end;
  end;

  QTrans.Requery;
  qTrans.First;

  contator := 0;
  total :=0;
  MTrans.Open;
  MTrans.EmptyTable;
  MTrans.DisableControls;
  while not qTrans.Eof do begin
      MTrans.Append;
      MTransDatahora.AsDateTime := QTransDataHora.AsDateTime;
      MTransNSU.AsInteger := QTransNSU.AsInteger;
      MTransTrans_id.AsInteger := QTransTrans_id.AsInteger;
      MTransOperador.AsString := QTransOperador.AsString;
      MTransConv_id.AsInteger := QTransConv_id.AsInteger;
      MTransTitular.AsString := QTransTitular.AsString;
      MTransCred_id.AsInteger := QTransCred_id.AsInteger;
      MTransCred_id_nome.AsString := QTransCred_id_nome.AsString;
      MTransTelefone1.AsString := QTransTelefone1.AsString;
      MTransEmpres_id.AsInteger := QTransEmpres_id.AsInteger;
      MTransNome.AsString := QTransNome.AsString;
      MTransValor.AsFloat := QTransValor.AsFloat;
      MTransHistorico.AsString := QTransHistorico.AsString;
      MTransBand_id.AsInteger := QTransBand_id.AsInteger;
      MTransSeg_id.AsInteger := QTransSeg_id.AsInteger;
      MTransCodacesso.AsInteger := QTransCodacesso.AsInteger;
      MTransSenha.AsString := QTransSenha.AsString;
      MTransCartao.AsString := QTransCartao.AsString;
      MTransMarcado.AsBoolean := False;
      MTrans.Post;
      contator := contator + 1;
      total := total + QTransValor.AsFloat;
      qTrans.Next;
  end;
  MTrans.First;
  MTrans.EnableControls;
  autor_sel := EmptyStr;

  lblRegistros.Caption := 'Total de Registros: '+IntToStr(contator);
  lblTotal.Caption := 'Total: R$ ' + FormatFloat('0.00',ArredondaDin(total));

  lblStatus.Caption := '';
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.FormShow(Sender: TObject);
begin
  inherited;
  dataIni.Date := now;
  dataFin.Date := now;
end;

procedure TFMovimentosDuvidosos.btnMarcaDesmEmpClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    if MTrans.IsEmpty then Exit;
    MTrans.Edit;
    MTransMarcado.AsBoolean := not MTransMarcado.AsBoolean;
    MTrans.Post;
    Trans_Sel;
  except
  end;
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.Trans_Sel;
var marca : TBookmark;
begin
  autor_sel := EmptyStr;
  marca := MTrans.GetBookmark;
  MTrans.DisableControls;
  MTrans.First;
  while not MTrans.eof do begin
    if MTransMarcado.AsBoolean then autor_sel := autor_sel + ','+MTransTrans_id.AsString;
    MTrans.Next;
  end;
  MTrans.GotoBookmark(marca);
  MTrans.FreeBookmark(marca);
  if autor_sel <> '' then autor_sel := Copy(autor_sel,2,Length(autor_sel));
  MTrans.EnableControls;
end;

procedure TFMovimentosDuvidosos.btnMarcaTodasEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    if MTrans.IsEmpty then Exit;
    MTrans.DisableControls;
    marca := MTrans.GetBookmark;
    MTrans.First;
    while not MTrans.eof do begin
      MTrans.Edit;
      MTransMarcado.AsBoolean := true;
      MTrans.Post;
      MTrans.Next;
    end;
    MTrans.GotoBookmark(marca);
    MTrans.FreeBookmark(marca);
    MTrans.EnableControls;
    Trans_Sel;
    except
  end;
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.btnDesmarcaTodosEmpClick(Sender: TObject);
var marca : TBookmark;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    if MTrans.IsEmpty then Exit;
    MTrans.DisableControls;
    marca := MTrans.GetBookmark;
    MTrans.First;
    while not MTrans.eof do begin
      MTrans.Edit;
      MTransMarcado.AsBoolean := false;
      MTrans.Post;
      MTrans.Next;
    end;
    MTrans.GotoBookmark(marca);
    MTrans.FreeBookmark(marca);
    MTrans.EnableControls;
    Trans_Sel;
  except
  end;
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.grdTransDblClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    if MTrans.IsEmpty then Exit;
    MTrans.Edit;
    MTransMarcado.AsBoolean := not MTransMarcado.AsBoolean;
    MTrans.Post;
    Trans_Sel;
  except
  end;
  Screen.Cursor := crDefault;
end;

procedure TFMovimentosDuvidosos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F5 : btnConsultar.Click;
    VK_F6 : btnMarcaTodasEmp.Click;
    VK_F7 : btnDesmarcaTodosEmp.Click;
    VK_F12: btnMarcaDesmEmp.Click;
    VK_SPACE: if ActiveControl = grdTrans then btnMarcaDesmEmp.Click;
  end;
end;

end.
