unit uMigracao;

interface

uses SysUtils, DB, Graphics, ComObj, grids, Forms, controls, Classes, Windows, dialogs;
const
  //Caso vc precise procure nos c�digos de VBA que pelo visto � isso q o ComObj faz...
  // SheetType
  xlChart = -4109;
  xlWorksheet = -4167;
  // WBATemplate
  xlWBATWorksheet = -4167;
  xlWBATChart = -4109;
  // Page Setup
  xlPortrait = 1;
  xlLandscape = 2;
  xlPaperA4 = 9;
  // Format Cells
  xlBottom = $FFFFEFF5;
  xlLeft = $FFFFEFDD;
  xlRight = $FFFFEFC8;
  xlTop = $FFFFEFC0;
  // Text Alignment
  xlHAlignCenter = -4108;
  xlVAlignCenter = -4108;
  // Cell Borders
  xlThick = 4;
  xlThin = 2;

  procedure prExportarExcel(DSet : TDataSet); overload;
  procedure prExportarExcel(sg : TStringGrid; identificarTipos : Boolean = True); overload;
  procedure prExportarTexto(sg : TStringGrid);
  function CriarLinha(Grid : TStringGrid ; linha : Integer ; Titulo:Boolean=False):String;

implementation

uses URotinasTexto, impressao, cartao_util;

procedure prExportarExcel(DSet : TDataSet);
var linha, coluna : integer;
    planilha : variant;
    valorcampo : string;
begin
 planilha := CreateoleObject('Excel.Application');
 planilha.WorkBooks.add(1);
 planilha.caption := 'Exportando dados da Grade para o Excel';
 planilha.visible := true;

 DSet.First;
 for linha := 0 to DSet.RecordCount - 1 do
 begin
   for coluna := 1 to DSet.FieldCount do
   begin
     if ((DSet.Fields[coluna - 1] is TDateTimeField) or (DSet.Fields[coluna - 1] is TDateField)) then begin
       if DSet.Fields[coluna - 1].AsString <> '' then begin
         valorcampo := FormatDateTime('dd/mm/yyyy',StrToDateTime(DSet.Fields[coluna - 1].AsString));
         planilha.cells[linha + 2,coluna].HorizontalAlignment := xlLeft;
       end else
         valorcampo := '';
     end else if (DSet.Fields[coluna - 1] is TIntegerField) or (DSet.Fields[coluna - 1] is TFloatField) then begin
       valorcampo := DSet.Fields[coluna - 1].AsString;
       planilha.cells[linha + 2,coluna].HorizontalAlignment := xlRight;
     end else begin
       valorcampo := DSet.Fields[coluna - 1].AsString;
       planilha.cells[linha + 2,coluna].HorizontalAlignment := xlLeft;
     end;
     if (linha + 2) mod 2 = 0 then
       planilha.cells[linha + 2,coluna].Interior.Color := $00FFF4EA
     else
       planilha.cells[linha + 2,coluna].Interior.Color := clWhite;
     planilha.cells[linha + 2,coluna] := valorCampo;
//     planilha.cells[linha + 2,coluna].Borders(7).LineStyle := xlContinuous;
//     planilha.cells[linha + 2,coluna].Borders(8).LineStyle := xlContinuous;
//     planilha.cells[linha + 2,coluna].Borders(xlEdgeBottom).LineStyle := xlContinuous;
//     planilha.cells[linha + 2,coluna].Borders(10).LineStyle := xlContinuous;
   end;
   DSet.Next;
 end;
 for coluna := 1 to DSet.FieldCount do
 begin
   valorcampo := DSet.Fields[coluna - 1].DisplayLabel;
   planilha.cells[1,coluna] := valorcampo;
   planilha.cells[1,coluna].Interior.Color := clBlue;
   planilha.cells[1,coluna].Font.bold := True;
   planilha.cells[1,coluna].Font.Color := clWhite;
   planilha.cells[1,coluna].HorizontalAlignment := xlLeft
 end;
 planilha.columns.Autofit;
end;

procedure prExportarExcel(sg : TStringGrid; identificarTipos : Boolean); overload;
var linha, coluna : integer;
    planilha, sheet : Variant;
    valCel : string;
    valorCampo : variant;
begin
  planilha := CreateoleObject('Excel.Application');
  planilha.WorkBooks.add(1);
  planilha.caption := 'Exportando dados da Grade para o Excel';
  planilha.visible := false;
  Screen.Cursor := crHourGlass;
  Sheet := planilha.ActiveSheet;
  for linha := 1 to sg.RowCount do begin
    //for coluna := 1 to sg.ColCount-1 do begin
    for coluna := 1 to sg.ColCount do begin
      valCel := sg.Cells[coluna-1,linha-1];
      if ((identificarTipos) and IsDate(valCel)) then begin
        valorCampo := FormatDateTime('dd/mm/yyyy',StrToDateTime(valCel));
        Sheet.cells[linha,coluna].HorizontalAlignment := xlLeft;
      end else if (identificarTipos) and ((IsInteger(valCel)) or (isFloat(valCel))) then begin
        if IsInteger(valCel) then
          valorCampo := StrToInt(valCel)
        else begin
          valorCampo := StrToFloat(valCel);
          sheet.cells[linha,coluna].NumberFormat := '#.##0,00';
        end;

        Sheet.cells[linha,coluna].HorizontalAlignment := xlRight;
      end else begin
        valorCampo := valCel;
        sheet.cells[linha,coluna].NumberFormat := '@';
        Sheet.cells[linha,coluna].HorizontalAlignment := xlLeft;
      end;
      Sheet.cells[linha,coluna] := valorCampo;
//      if (linha + 2) mod 2 = 0 then begin
//        Sheet.cells[linha+1,coluna].Interior.Color := $00FFF4EA
//      end else begin
//        Sheet.cells[linha+1,coluna].Interior.Color := clWhite;
//      end;
//      planilha.cells[linha+1,coluna].Borders(7).LineStyle := xlContinuous;
//      planilha.cells[linha+1,coluna].Borders(8).LineStyle := xlContinuous;
//      planilha.cells[linha+1,coluna].Borders(xlEdgeBottom).LineStyle := xlContinuous;
//      planilha.cells[linha+1,coluna].Borders(10).LineStyle := xlContinuous;
    end;
  end;
  Screen.Cursor := crDefault;
  planilha.visible := true;
  planilha.columns.Autofit;
end;

procedure prExportarTexto(sg : TStringGrid);
var Txt : TStringList;
    Tamanho, row, col, maior : Integer;
    SLMaiorLinha : TStringList;
    Arq, Linha, Coluna :String;
begin
  Arq := ExtractFilePath(Application.ExeName)+'Grade_'+FormatDateTime('hh_nn',now)+'.txt';
  Txt := TStringList.Create;
  maior := 0;
  SLMaiorLinha := TStringList.Create;
  //Capturando a maior linha de cada coluna
  for col := 0 to sg.ColCount-1 do begin
    for row := 0 to sg.RowCount-1 do
      if (Length(sg.Cells[col,row]) > maior) then
        maior := Length(sg.Cells[col,row]);
    SLMaiorLinha.Add(IntToStr(maior));
    maior := 0;
  end;

  for row := 0 to sg.RowCount-1 do begin
    for col := 0 to sg.ColCount-1 do begin
      Tamanho := StrToInt(SLMaiorLinha.Strings[col]);

      Coluna := TrimRight(TrimLeft(sg.Cells[col,row]));
      if IsInteger(sg.Cells[col,row]) or IsFloat(sg.Cells[col,row]) then
        Coluna := Direita(Coluna,' ',Tamanho)
      else
        Coluna := Preenche(Coluna,' ',Tamanho);
      Linha := Linha +'  '+ Coluna;
    end;
    Txt.Add(Linha);
    Linha := '';
  end;
  Txt.SaveToFile(Arq);
  WinExec(PChar('notepad.exe '+Arq),SW_SHOWMAXIMIZED);
end;

function CriarLinha(Grid : TStringGrid ; linha : Integer ; Titulo:Boolean=False):String;
//var i,Tamanho : integer; Linha, Coluna :String;
begin
{   Linha := EmptyStr;
   for i := 0 to pred(Grid.ColCount) do begin
       Tamanho := iif(Grid.Columns[i].Field.DisplayWidth > length(Grid.Columns[i].Title.Caption),Grid.Columns[i].Field.DisplayWidth,length(Grid.Columns[i].Title.Caption));
       if Titulo then
          Coluna := Grid.Columns[i].Title.Caption
       else
          Coluna := Grid.Columns[i].Field.DisplayText;

       if IsNumericField(Grid.Columns[i].Field.DataType) then
          Coluna := Direita(Coluna,' ',Tamanho)
       else begin
          case Grid.Columns[i].Title.Alignment of
             taLeftJustify  : Coluna := Preenche(Coluna,' ',Tamanho);
             taRightJustify : Coluna := Direita(Coluna,' ',Tamanho);
             taCenter       : Coluna := Centraliza(Coluna,' ',Tamanho);
          end;
       end;
       Linha := Linha +'  '+ Coluna;
   end;
   Result := Linha;}
end;

end.
