unit URelOcorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, frxClass, frxExportPDF, DB, frxDBSet, ADODB,
  JvMemoryDataset, StdCtrls, ExtCtrls, Buttons, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvExControls,ShellApi, JvDBLookup, Mask,
  JvExMask, JvToolEdit, Menus;

type
  TFRelOcorrencia = class(TF1)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    datafin: TJvDateEdit;
    DataIni: TJvDateEdit;
    MDOcorrenciaEmpresa: TJvMemoryData;
    frxCredenciados: TfrxReport;
    QOcorrenciaEmpresa: TADOQuery;
    dsCredenciados: TDataSource;
    dbCredenciado: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    BitBtn1: TBitBtn;
    bntGerarPDF: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    GroupBox3: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    BitBtn2: TBitBtn;
    QOcorrenciaEmpresaatendimento_id: TIntegerField;
    QOcorrenciaEmpresanome_solicitante: TStringField;
    QOcorrenciaEmpresatel_solictante: TStringField;
    QOcorrenciaEmpresamotivo: TStringField;
    QOcorrenciaEmpresadesc_atendimento: TStringField;
    QOcorrenciaEmpresadata_atendimento: TDateTimeField;
    QOcorrenciaEmpresaempres_id: TIntegerField;
    QOcorrenciaEmpresaoperador: TStringField;
    QOcorrenciaEmpresaSTATUS_ID: TIntegerField;
    procedure BitBtn1Click(Sender: TObject);
    procedure bntGerarPDFClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelOcorrencia: TFRelOcorrencia;

implementation

uses DM, impressao, Math, cartao_util;

{$R *.dfm}

procedure TFRelOcorrencia.BitBtn1Click(Sender: TObject);
begin
  inherited;
  frxCredenciados.Variables['dataIni'] := QuotedStr(DateToStr(DataIni.Date));
  frxCredenciados.Variables['dataFin'] := QuotedStr(DateToStr(datafin.Date));

  frxCredenciados.ShowReport();
end;

{procedure TFRelOcorrencia.ButListaEmpClick(Sender: TObject);
begin
  inherited;

  if((DataIni.Date = 0)) then
  begin
     MsgInf('A data inicial n�o pode estar vazia'); DataIni.SetFocus; Exit;
  end;

  if((DataIni.Date > datafin.Date) and (datafin.Date <> 0))then
  begin
     MsgInf('A data inicial n�o pode ser maior do que a data final');
     DataIni.SetFocus;
     Exit;
  end;
  
  QCredenciados.Parameters[0].Value := DataIni.Date;
  QCredenciados.Parameters[1].Value := datafin.Date;

  QCredenciados.Close;
  QCredenciados.Open;
  if QCredenciados.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum credenciado cadastrado no per�odo selecionado.');
    DataIni.SetFocus;
  end;
  QCredenciados.First;
  MCredenciados.Open;
  MCredenciados.EmptyTable;
  MCredenciados.DisableControls;
  while not QCredenciados.Eof do
  begin
    MCredenciados.Append;
    MCredenciadoscred_id.AsInteger       := QCredenciadoscred_id.AsInteger;
    MCredenciadosnome.AsString           := QCredenciadosnome.AsString;
    MCredenciadosfantasia.AsString       := QCredenciadosfantasia.AsString;
    MCredenciadosendereco.AsString       := QCredenciadosendereco.AsString;
    MCredenciadosnumero.AsInteger        := QCredenciadosnumero.AsInteger;
    MCredenciadosbairro.AsString         := QCredenciadosbairro.AsString;
    MCredenciadoscidade.AsString         := QCredenciadoscidade.AsString;
    MCredenciadostelefone1.AsString      := QCredenciadostelefone1.AsString;
    MCredenciadostelefone2.AsString      := QCredenciadostelefone2.AsString;
    MCredenciadosdtcadastro.AsDateTime   := QCredenciadosdtcadastro.AsDateTime;
    MCredenciadosmarcado.AsBoolean       := False;
    MCredenciadosdescricao.AsString      := QCredenciadosdescricao.AsString;
    QCredenciados.Next;
  end;
  MCredenciados.First;
  MCredenciados.EnableControls;
end; }

procedure TFRelOcorrencia.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BitBtn1.Click;
    if QOcorrenciaEmpresaatendimento_id.AsInteger <= 1 then
    begin
      frxCredenciados.PrepareReport();
      frxCredenciados.Export(frxPDFExport1);
    end
  else
    begin
      frxCredenciados.PrepareReport();
      frxCredenciados.Export(frxPDFExport1);
    end;
    ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

end.
