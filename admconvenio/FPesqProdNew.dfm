object F_PesqProdNew: TF_PesqProdNew
  Left = 208
  Top = 174
  Width = 789
  Height = 481
  Caption = 'Pesquisa de Produtos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 38
    Width = 773
    Height = 405
    Align = alClient
    DataSource = DSProdutos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = DBGrid1DrawColumnCell
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    OnKeyPress = DBGrid1KeyPress
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 773
    Height = 38
    Align = alTop
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 12
      Width = 90
      Height = 13
      Caption = 'Digite a Descri'#231#227'o:'
    end
    object edtBusca: TEdit
      Left = 104
      Top = 9
      Width = 665
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnEnter = edtBuscaEnter
      OnKeyDown = edtBuscaKeyDown
      OnKeyPress = edtBuscaKeyPress
      OnKeyUp = edtBuscaKeyUp
    end
  end
  object DSProdutos: TDataSource
    DataSet = QProdutos
    Left = 400
    Top = 160
  end
  object DSBarras: TDataSource
    DataSet = QBarras
    Left = 520
    Top = 160
  end
  object DSProdutosProgProd: TDataSource
    DataSet = QProdutos
    Left = 456
    Top = 160
  end
  object QProdutos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'prog_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cred_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'progId'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'estado'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'SELECT p.prod_id,p.grupo_prod_id, pp.perc_desc, p.descricao, pp.' +
        'qtd_max, l.nomelab,'
      'coalesce((case when e.icms_estadual = 17 then pp.prc_unit'
      
        ' else case when e.icms_estadual = 18 then pp.prc_unit_18 else pp' +
        '.prc_unit_19 end'
      ' end),p.preco_vnd) as prc_unit,'
      
        '(SELECT TOP 1 BARRAS FROM BARRAS WHERE BARRAS.PROD_ID = p.PROD_I' +
        'D) AS CODINBS,'
      
        'coalesce((SELECT TOP 1 case when pc.obriga_desconto <> '#39'S'#39' then ' +
        #39'N'#39' else '#39'S'#39' end FROM prog_cred pc where pc.prog_id = :prog_id a' +
        'nd pc.cred_id = :cred_id),'#39'N'#39') as obriga_desconto'
      'FROM produtos p'
      
        'left join prog_prod pp on pp.prod_id = p.prod_id AND pp.prog_id ' +
        '= :progId'
      'left join laboratorios l on p.lab_id = l.lab_id'
      
        'join (SELECT TOP 1 e.uf, e.ICMS_ESTADUAL from ESTADOS e where e.' +
        'UF = :UF) e on e.uf = :estado'
      
        'where  (pp.prc_unit > 0 or pp.prc_unit_18 > 0 or pp.prc_unit_19 ' +
        '> 0 or p.preco_vnd > 0)'
      'order by p.descricao')
    Left = 400
    Top = 120
    object QProdutosprod_id: TIntegerField
      FieldName = 'prod_id'
    end
    object QProdutosgrupo_prod_id: TIntegerField
      FieldName = 'grupo_prod_id'
    end
    object QProdutosperc_desc: TBCDField
      FieldName = 'perc_desc'
      Precision = 5
      Size = 2
    end
    object QProdutosdescricao: TStringField
      FieldName = 'descricao'
      Size = 90
    end
    object QProdutosqtd_max: TIntegerField
      FieldName = 'qtd_max'
    end
    object QProdutosnomelab: TStringField
      FieldName = 'nomelab'
      Size = 50
    end
    object QProdutosprc_unit: TBCDField
      FieldName = 'prc_unit'
      ReadOnly = True
      Precision = 15
      Size = 2
    end
    object QProdutosCODINBS: TStringField
      FieldName = 'CODINBS'
      ReadOnly = True
      Size = 13
    end
    object QProdutosobriga_desconto: TStringField
      FieldName = 'obriga_desconto'
      ReadOnly = True
      Size = 1
    end
  end
  object QProdutosProgProd: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'barras'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'codBarras'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT '
      'p.prod_id,'
      'p.grupo_prod_id,'
      'null as perc_desc,'
      'p.descricao,'
      'null as qtd_max,'
      'l.nomelab,'
      'p.preco_vnd as prc_unit,'
      'coalesce(b.barras,p.codinbs) AS CODINBS,'
      #39'N'#39' as obriga_desconto'
      'FROM produtos p'
      'left join laboratorios l on p.lab_id = l.lab_id'
      
        'left join (SELECT b.barras, b.prod_id FROM BARRAS b where b.barr' +
        'as = :barras) b on b.prod_id = p.prod_id'
      'where coalesce(b.barras,p.codinbs) =:codBarras'
      'and p.preco_vnd > 0'
      'order by p.descricao')
    Left = 448
    Top = 120
    object QProdutosProgProdprod_id: TIntegerField
      FieldName = 'prod_id'
    end
    object QProdutosProgProdgrupo_prod_id: TIntegerField
      FieldName = 'grupo_prod_id'
    end
    object QProdutosProgProdperc_desc: TIntegerField
      FieldName = 'perc_desc'
      ReadOnly = True
    end
    object QProdutosProgProddescricao: TStringField
      FieldName = 'descricao'
      Size = 90
    end
    object QProdutosProgProdqtd_max: TIntegerField
      FieldName = 'qtd_max'
      ReadOnly = True
    end
    object QProdutosProgProdnomelab: TStringField
      FieldName = 'nomelab'
      Size = 50
    end
    object QProdutosProgProdprc_unit: TBCDField
      FieldName = 'prc_unit'
      Precision = 15
      Size = 2
    end
    object QProdutosProgProdCODINBS: TStringField
      FieldName = 'CODINBS'
      ReadOnly = True
      Size = 13
    end
    object QProdutosProgProdobriga_desconto: TStringField
      FieldName = 'obriga_desconto'
      ReadOnly = True
      Size = 1
    end
  end
  object QBarras: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'PROD_ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      'barras,'
      'codinbs'
      'FROM BARRAS '
      'WHERE BARRAS.PROD_ID = :PROD_ID'
      'order by barras')
    Left = 496
    Top = 120
    object QBarrasbarras: TStringField
      FieldName = 'barras'
      Size = 13
    end
    object QBarrascodinbs: TStringField
      FieldName = 'codinbs'
      Size = 13
    end
  end
end
