inherited FCons_Autor: TFCons_Autor
  Left = 266
  Top = 113
  Caption = 'Consulta de Autoriza'#231#227'o/Nota Fiscal'
  ClientHeight = 618
  ClientWidth = 791
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 791
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 791
    Height = 595
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 1
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 785
      Height = 80
      Align = alTop
      BorderStyle = bsSingle
      TabOrder = 0
      object Label1: TLabel
        Left = 167
        Top = 19
        Width = 105
        Height = 13
        Caption = 'Autoriza'#231#227'o (Sem Dig)'
      end
      object EdAutor: TEdit
        Left = 167
        Top = 35
        Width = 121
        Height = 21
        Hint = 'Digite o numero da autoriza'#231#227'o e tecle <enter>.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnKeyPress = EdAutorKeyPress
      end
      object Button1: TButton
        Left = 295
        Top = 32
        Width = 99
        Height = 25
        Caption = '&Buscar'
        TabOrder = 2
        OnClick = Button1Click
      end
      object RGTipoCons: TRadioGroup
        Left = 1
        Top = 1
        Width = 160
        Height = 74
        Align = alLeft
        Caption = 'Tipo da Consulta'
        ItemIndex = 0
        Items.Strings = (
          'N'#186' de Autoriza'#231#227'o'
          'N'#186' de Nota Fiscal'
          'N'#186' da Transa'#231#227'o')
        TabOrder = 0
        OnClick = RGTipoConsClick
      end
      object DBNavigator1: TDBNavigator
        Left = 411
        Top = 31
        Width = 336
        Height = 25
        DataSource = DataSource1
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        Flat = True
        Hints.Strings = (
          'Primeiro'
          'Anterior'
          'Pr'#243'ximo'
          #218'ltimo')
        TabOrder = 3
      end
    end
    object Status: TStatusBar
      Left = 1
      Top = 571
      Width = 785
      Height = 19
      Panels = <>
      SimplePanel = True
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 81
      Width = 785
      Height = 490
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Em &Ficha'
        object Label2: TLabel
          Left = 23
          Top = 9
          Width = 56
          Height = 13
          Caption = 'Autoriza'#231#227'o'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 128
          Top = 8
          Width = 27
          Height = 13
          Caption = 'Digito'
          FocusControl = DBEdit2
        end
        object Label4: TLabel
          Left = 192
          Top = 8
          Width = 66
          Height = 13
          Caption = 'Data Autor.'
          FocusControl = DBEdit3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 288
          Top = 8
          Width = 28
          Height = 13
          Caption = 'Hora'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 24
          Top = 163
          Width = 39
          Height = 13
          Caption = 'Conv ID'
          FocusControl = DBEdit5
        end
        object Label7: TLabel
          Left = 23
          Top = 242
          Width = 44
          Height = 13
          Caption = 'Estab. ID'
          FocusControl = DBEdit6
        end
        object Label8: TLabel
          Left = 24
          Top = 48
          Width = 38
          Height = 13
          Caption = 'D'#233'bito'
          FocusControl = DBEdit7
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 24
          Top = 86
          Width = 41
          Height = 13
          Caption = 'Cr'#233'dito'
          FocusControl = DBEdit8
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 172
          Top = 202
          Width = 45
          Height = 13
          Caption = 'Cart'#227'o ID'
          FocusControl = DBEdit9
        end
        object Label12: TLabel
          Left = 408
          Top = 8
          Width = 53
          Height = 13
          Caption = 'Operador'
          FocusControl = DBEdit11
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 351
          Top = 48
          Width = 83
          Height = 13
          Caption = 'Vencimento Emp.'
          FocusControl = DBEdit12
        end
        object Label14: TLabel
          Left = 576
          Top = 48
          Width = 89
          Height = 13
          Caption = 'Vencimento Estab.'
          FocusControl = DBEdit13
        end
        object Label15: TLabel
          Left = 239
          Top = 48
          Width = 86
          Height = 13
          Caption = 'Fechamento Emp.'
          FocusControl = DBEdit14
        end
        object Label18: TLabel
          Left = 24
          Top = 318
          Width = 41
          Height = 13
          Caption = 'Hist'#243'rico'
          FocusControl = DBEdit17
        end
        object Label25: TLabel
          Left = 125
          Top = 86
          Width = 84
          Height = 13
          Caption = 'Cancel. ref. autor.'
          FocusControl = DBEdit24
        end
        object Label26: TLabel
          Left = 126
          Top = 48
          Width = 78
          Height = 13
          Caption = 'Valor Cancelado'
          FocusControl = DBEdit25
        end
        object Label27: TLabel
          Left = 240
          Top = 163
          Width = 37
          Height = 13
          Caption = 'Titular'
          FocusControl = DBEdit26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label28: TLabel
          Left = 432
          Top = 202
          Width = 92
          Height = 13
          Caption = 'Nome no Cart'#227'o'
          FocusControl = DBEdit27
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 95
          Top = 242
          Width = 164
          Height = 13
          Caption = 'Estabelecimento/Fornecedor'
          FocusControl = DBEdit28
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 23
          Top = 202
          Width = 46
          Height = 13
          Caption = 'Cart'#227'o N'#186
          FocusControl = DBEdit10
        end
        object Label30: TLabel
          Left = 138
          Top = 202
          Width = 16
          Height = 13
          Caption = 'Dig'
          FocusControl = DBEdit29
        end
        object Label31: TLabel
          Left = 376
          Top = 242
          Width = 107
          Height = 13
          Caption = 'Raz'#227'o da Empresa'
          FocusControl = DBEdit30
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label32: TLabel
          Left = 24
          Top = 281
          Width = 119
          Height = 13
          Caption = 'Fantasia da Empresa'
          FocusControl = DBEdit31
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Bevel2: TBevel
          Left = 106
          Top = 51
          Width = 2
          Height = 35
        end
        object Bevel3: TBevel
          Left = 221
          Top = 51
          Width = 2
          Height = 35
        end
        object Bevel4: TBevel
          Left = 106
          Top = 89
          Width = 2
          Height = 35
        end
        object Label16: TLabel
          Left = 387
          Top = 125
          Width = 86
          Height = 13
          Caption = 'Baixada do Estab.'
          FocusControl = DBEdit15
        end
        object Label17: TLabel
          Left = 263
          Top = 125
          Width = 113
          Height = 13
          Caption = 'Baixada do Conveniado'
          FocusControl = DBEdit16
        end
        object Label19: TLabel
          Left = 509
          Top = 125
          Width = 27
          Height = 13
          Caption = 'Cesta'
          FocusControl = DBEdit18
        end
        object Label21: TLabel
          Left = 24
          Top = 125
          Width = 83
          Height = 13
          Caption = 'N'#186' da Nota Fiscal'
          FocusControl = DBEdit20
        end
        object Label22: TLabel
          Left = 190
          Top = 125
          Width = 61
          Height = 13
          Caption = 'Com Receita'
          FocusControl = DBEdit21
        end
        object Label23: TLabel
          Left = 126
          Top = 125
          Width = 51
          Height = 13
          Caption = 'NF Entreg.'
          FocusControl = DBEdit22
        end
        object Label24: TLabel
          Left = 549
          Top = 125
          Width = 71
          Height = 13
          Caption = 'Data Altera'#231#227'o'
          FocusControl = DBEdit23
        end
        object Bevel6: TBevel
          Left = 445
          Top = 51
          Width = 2
          Height = 35
        end
        object Label33: TLabel
          Left = 460
          Top = 48
          Width = 92
          Height = 13
          Caption = 'Fechamento Estab.'
          FocusControl = DBEdit32
        end
        object Bevel7: TBevel
          Left = 221
          Top = 89
          Width = 2
          Height = 35
        end
        object Label20: TLabel
          Left = 657
          Top = 126
          Width = 57
          Height = 13
          Caption = 'Data Venda'
          FocusControl = DBEdit19
        end
        object Label34: TLabel
          Left = 375
          Top = 281
          Width = 68
          Height = 13
          Caption = 'Forma Pgto ID'
          FocusControl = DBEdit33
        end
        object Label35: TLabel
          Left = 452
          Top = 280
          Width = 195
          Height = 13
          Caption = 'Descri'#231#227'o da forma de pagamento'
          FocusControl = DBEdit34
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label36: TLabel
          Left = 300
          Top = 202
          Width = 104
          Height = 13
          Caption = 'C'#243'digo de Importa'#231#227'o'
          FocusControl = DBEdit35
        end
        object Label37: TLabel
          Left = 112
          Top = 163
          Width = 31
          Height = 13
          Caption = 'Chapa'
          FocusControl = DBEdit36
        end
        object Label38: TLabel
          Left = 239
          Top = 88
          Width = 32
          Height = 13
          Caption = 'Fat. ID'
          FocusControl = DBEdit37
        end
        object Label39: TLabel
          Left = 383
          Top = 88
          Width = 45
          Height = 13
          Caption = 'Pagto. ID'
          FocusControl = DBEdit38
        end
        object Label40: TLabel
          Left = 608
          Top = 88
          Width = 65
          Height = 13
          Caption = 'Transa'#231#227'o ID'
          FocusControl = DBEdit39
        end
        object DBEdit1: TDBEdit
          Left = 23
          Top = 25
          Width = 98
          Height = 21
          TabStop = False
          DataField = 'AUTORIZACAO_ID'
          DataSource = DataSource1
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 128
          Top = 24
          Width = 49
          Height = 21
          TabStop = False
          DataField = 'DIGITO'
          DataSource = DataSource1
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 192
          Top = 24
          Width = 89
          Height = 21
          TabStop = False
          DataField = 'DATA'
          DataSource = DataSource1
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 288
          Top = 24
          Width = 108
          Height = 21
          TabStop = False
          DataField = 'HORA'
          DataSource = DataSource1
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 24
          Top = 179
          Width = 81
          Height = 21
          TabStop = False
          DataField = 'CONV_ID'
          DataSource = DataSource1
          TabOrder = 21
        end
        object DBEdit6: TDBEdit
          Left = 23
          Top = 258
          Width = 63
          Height = 21
          TabStop = False
          DataField = 'CRED_ID'
          DataSource = DataSource1
          TabOrder = 29
        end
        object DBEdit7: TDBEdit
          Left = 24
          Top = 64
          Width = 65
          Height = 21
          TabStop = False
          DataField = 'DEBITO'
          DataSource = DataSource1
          TabOrder = 5
        end
        object DBEdit8: TDBEdit
          Left = 24
          Top = 101
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'CREDITO'
          DataSource = DataSource1
          TabOrder = 11
        end
        object DBEdit9: TDBEdit
          Left = 172
          Top = 218
          Width = 117
          Height = 21
          TabStop = False
          DataField = 'CARTAO_ID'
          DataSource = DataSource1
          TabOrder = 26
        end
        object DBEdit11: TDBEdit
          Left = 408
          Top = 24
          Width = 332
          Height = 21
          TabStop = False
          DataField = 'OPERADOR'
          DataSource = DataSource1
          TabOrder = 4
        end
        object DBEdit12: TDBEdit
          Left = 351
          Top = 64
          Width = 83
          Height = 21
          TabStop = False
          DataField = 'DATA_VENC_EMP'
          DataSource = DataSource1
          TabOrder = 8
        end
        object DBEdit13: TDBEdit
          Left = 575
          Top = 64
          Width = 105
          Height = 21
          TabStop = False
          DataField = 'DATA_VENC_FOR'
          DataSource = DataSource1
          TabOrder = 10
        end
        object DBEdit14: TDBEdit
          Left = 239
          Top = 64
          Width = 97
          Height = 21
          TabStop = False
          DataField = 'DATA_FECHA_EMP'
          DataSource = DataSource1
          TabOrder = 7
        end
        object DBEdit17: TDBEdit
          Left = 24
          Top = 334
          Width = 713
          Height = 21
          TabStop = False
          DataField = 'HISTORICO'
          DataSource = DataSource1
          TabOrder = 35
        end
        object DBEdit24: TDBEdit
          Left = 125
          Top = 102
          Width = 82
          Height = 21
          TabStop = False
          DataField = 'AUTORIZACAO_ID_CANC'
          DataSource = DataSource1
          TabOrder = 12
        end
        object DBEdit25: TDBEdit
          Left = 126
          Top = 64
          Width = 78
          Height = 21
          TabStop = False
          DataField = 'VALOR_CANCELADO'
          DataSource = DataSource1
          TabOrder = 6
        end
        object DBEdit26: TDBEdit
          Left = 240
          Top = 179
          Width = 497
          Height = 21
          TabStop = False
          DataField = 'TITULAR'
          DataSource = DataSource1
          TabOrder = 23
        end
        object DBEdit27: TDBEdit
          Left = 432
          Top = 218
          Width = 305
          Height = 21
          TabStop = False
          DataField = 'NOMECARTAO'
          DataSource = DataSource1
          TabOrder = 28
        end
        object DBEdit28: TDBEdit
          Left = 95
          Top = 258
          Width = 274
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          DataSource = DataSource1
          TabOrder = 30
        end
        object DBEdit10: TDBEdit
          Left = 23
          Top = 218
          Width = 105
          Height = 21
          TabStop = False
          DataField = 'CATNUM'
          DataSource = DataSource1
          TabOrder = 24
        end
        object DBEdit29: TDBEdit
          Left = 138
          Top = 218
          Width = 25
          Height = 21
          TabStop = False
          DataField = 'CARTDIG'
          DataSource = DataSource1
          TabOrder = 25
        end
        object DBEdit30: TDBEdit
          Left = 376
          Top = 258
          Width = 361
          Height = 21
          TabStop = False
          DataField = 'EMP_RAZAO'
          DataSource = DataSource1
          TabOrder = 31
        end
        object DBEdit31: TDBEdit
          Left = 24
          Top = 295
          Width = 337
          Height = 21
          TabStop = False
          DataField = 'EMP_FANTASIA'
          DataSource = DataSource1
          TabOrder = 32
        end
        object DBEdit15: TDBEdit
          Left = 387
          Top = 141
          Width = 17
          Height = 21
          TabStop = False
          DataField = 'BAIXA_CREDENCIADO'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 17
        end
        object DBEdit16: TDBEdit
          Left = 263
          Top = 141
          Width = 17
          Height = 21
          TabStop = False
          DataField = 'BAIXA_CONVENIADO'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 16
        end
        object DBEdit18: TDBEdit
          Left = 511
          Top = 141
          Width = 17
          Height = 21
          TabStop = False
          DataField = 'CESTA'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 18
        end
        object DBEdit20: TDBEdit
          Left = 24
          Top = 141
          Width = 89
          Height = 21
          TabStop = False
          DataField = 'NF'
          DataSource = DataSource1
          TabOrder = 13
        end
        object DBEdit21: TDBEdit
          Left = 193
          Top = 141
          Width = 17
          Height = 21
          TabStop = False
          DataField = 'RECEITA'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 15
        end
        object DBEdit22: TDBEdit
          Left = 126
          Top = 141
          Width = 17
          Height = 21
          TabStop = False
          DataField = 'ENTREG_NF'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 14
        end
        object DBEdit23: TDBEdit
          Left = 551
          Top = 141
          Width = 97
          Height = 21
          TabStop = False
          DataField = 'DATA_ALTERACAO'
          DataSource = DataSource1
          TabOrder = 19
        end
        object DBEdit32: TDBEdit
          Left = 459
          Top = 64
          Width = 105
          Height = 21
          TabStop = False
          DataField = 'DATA_FECHA_FOR'
          DataSource = DataSource1
          TabOrder = 9
        end
        object DBEdit19: TDBEdit
          Left = 657
          Top = 142
          Width = 81
          Height = 21
          TabStop = False
          DataField = 'DATAVENDA'
          DataSource = DataSource1
          TabOrder = 20
        end
        object DBEdit33: TDBEdit
          Left = 375
          Top = 296
          Width = 68
          Height = 21
          TabStop = False
          DataField = 'FORMAPAGTO_ID'
          DataSource = DataSource1
          TabOrder = 33
        end
        object DBEdit34: TDBEdit
          Left = 452
          Top = 296
          Width = 285
          Height = 21
          TabStop = False
          DataField = 'FORMA'
          DataSource = DataSource1
          TabOrder = 34
        end
        object DBEdit35: TDBEdit
          Left = 300
          Top = 218
          Width = 117
          Height = 21
          TabStop = False
          DataField = 'CODCARTIMP'
          DataSource = DataSource1
          TabOrder = 27
        end
        object DBEdit36: TDBEdit
          Left = 112
          Top = 179
          Width = 113
          Height = 21
          TabStop = False
          DataField = 'CHAPA'
          DataSource = DataSource1
          TabOrder = 22
        end
        object DBEdit37: TDBEdit
          Left = 239
          Top = 102
          Width = 134
          Height = 21
          DataField = 'FATURA_ID'
          DataSource = DataSource1
          TabOrder = 36
        end
        object DBEdit38: TDBEdit
          Left = 383
          Top = 102
          Width = 134
          Height = 21
          DataField = 'PAGAMENTO_CRED_ID'
          DataSource = DataSource1
          TabOrder = 37
        end
        object DBEdit39: TDBEdit
          Left = 608
          Top = 102
          Width = 131
          Height = 21
          DataField = 'TRANS_ID'
          DataSource = DataSource1
          TabOrder = 38
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Em &Grade'
        ImageIndex = 1
        object JvDBGrid1: TJvDBGrid
          Left = 0
          Top = 0
          Width = 777
          Height = 462
          Hint = 'Clique duplo vai para ficha.'
          Align = alClient
          DataSource = DataSource1
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = JvDBGrid1DblClick
          OnKeyDown = JvDBGrid1KeyDown
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'AUTORIZACAO_ID'
              Title.Caption = 'Autoriza'#231#227'o ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIGITO'
              Title.Caption = 'Dig.'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA'
              Title.Caption = 'Data'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DEBITO'
              Title.Caption = 'D'#233'bito'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CREDITO'
              Title.Caption = 'Cr'#233'dito'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'Nota F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'titular'
              Title.Caption = 'Titular'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'fornecedor'
              Title.Caption = 'Estabelecimento'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'HISTORICO'
              Title.Caption = 'Hist'#243'rico'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'emp_razao'
              Title.Caption = 'Raz'#227'o Empresa'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'emp_fantasia'
              Title.Caption = 'Fantasia Empresa'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FORMAPAGTO_ID'
              Title.Caption = 'Forma Pgto ID'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'forma'
              Title.Caption = 'Descri'#231#227'o Forma Pgto'
              Width = 64
              Visible = True
            end>
        end
      end
      object TabProdutos: TTabSheet
        Caption = 'Detalhe Produtos'
        ImageIndex = 2
        OnHide = TabProdutosHide
        OnShow = TabProdutosShow
        object pnlMovimentacaoAutor: TPanel
          Left = 0
          Top = 0
          Width = 777
          Height = 462
          Align = alClient
          TabOrder = 0
          object DBGrid2: TDBGrid
            Left = 1
            Top = 1
            Width = 775
            Height = 436
            TabStop = False
            Align = alClient
            DataSource = DSProdutos
            DefaultDrawing = False
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CODINBS'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESCRICAO'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTDE'
                Title.Alignment = taRightJustify
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO_UNI'
                Title.Alignment = taRightJustify
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO_TAB'
                Title.Alignment = taRightJustify
                Width = 100
                Visible = True
              end>
          end
          object Panel21: TPanel
            Left = 1
            Top = 437
            Width = 775
            Height = 24
            Align = alBottom
            Alignment = taLeftJustify
            BorderStyle = bsSingle
            Caption = 'Total de Produtos:'
            TabOrder = 1
            object LabTotProd: TLabel
              Left = 92
              Top = 4
              Width = 26
              Height = 13
              Caption = '0,00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
      end
      object TabHistorico: TTabSheet
        Caption = 'Hist'#243'rico'
        ImageIndex = 3
        OnShow = TabHistoricoShow
        object JvDBGrid4: TJvDBGrid
          Left = 0
          Top = 65
          Width = 777
          Height = 397
          Align = alClient
          DataSource = DSHistorico
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DETALHE'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_HORA'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CADASTRO'
              Width = 170
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CAMPO'
              Width = 99
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_ANT'
              Width = 103
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR_POS'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERADOR'
              Width = 97
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OPERACAO'
              Width = 90
              Visible = True
            end>
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 777
          Height = 65
          Align = alTop
          BorderStyle = bsSingle
          TabOrder = 1
          object Label67: TLabel
            Left = 8
            Top = 14
            Width = 53
            Height = 13
            Caption = 'Data Inicial'
          end
          object Label68: TLabel
            Left = 115
            Top = 14
            Width = 48
            Height = 13
            Caption = 'Data Final'
          end
          object Label69: TLabel
            Left = 220
            Top = 14
            Width = 86
            Height = 13
            Caption = 'Selecionar Campo'
          end
          object Btnhist1: TButton
            Left = 528
            Top = 25
            Width = 79
            Height = 23
            Hint = 'Visualizar hist'#243'ricos de autoriza'#231#245'es'
            Caption = '&Visualizar'
            TabOrder = 3
            OnClick = Btnhist1Click
          end
          object DBNavigator6: TDBNavigator
            Left = 616
            Top = 24
            Width = 136
            Height = 24
            VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
            Flat = True
            Hints.Strings = (
              'Primeiro Registro'
              'Registro Anterior'
              'Pr'#243'ximo Registro'
              #218'ltimo Registro')
            TabOrder = 4
          end
          object dataini1: TJvDateEdit
            Left = 8
            Top = 28
            Width = 102
            Height = 21
            Hint = 'Data inicial para consulta'
            NumGlyphs = 2
            ShowNullDate = False
            TabOrder = 0
            OnExit = datafin1Exit
          end
          object datafin1: TJvDateEdit
            Left = 114
            Top = 28
            Width = 102
            Height = 21
            Hint = 'Data final para consulta'
            NumGlyphs = 2
            ShowNullDate = False
            TabOrder = 1
            OnExit = datafin1Exit
          end
          object DBCampoCC: TComboBox
            Left = 219
            Top = 28
            Width = 305
            Height = 21
            Hint = 'Selecione o registro para consulta'
            Style = csDropDownList
            ItemHeight = 13
            ItemIndex = 0
            TabOrder = 2
            Text = 'Todos os Campos'
            Items.Strings = (
              'Todos os Campos')
          end
        end
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 636
    Top = 66
  end
  object DataSource1: TDataSource
    DataSet = QAutor
    OnDataChange = DataSource1DataChange
    Left = 536
    Top = 538
  end
  object DSProdutos: TDataSource
    DataSet = QProdutos
    Left = 493
    Top = 538
  end
  object DSHistorico: TDataSource
    DataSet = QHistorico
    Left = 452
    Top = 536
  end
  object QHistorico: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 453
    Top = 504
  end
  object QProdutos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'AUTORIZACAO_ID'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'Select MOV_PROD2.*, produtos.descricao, produtos.codinbs'
      'from MOV_PROD2'
      'join produtos on produtos.prod_id = mov_prod2.prod_id'
      'where cancelado <> '#39'S'#39
      'and autorizacao_id = :AUTORIZACAO_ID'
      'order by MOV_PROD2.MOV_ID')
    Left = 493
    Top = 504
    object QProdutosMOV_ID: TIntegerField
      FieldName = 'MOV_ID'
    end
    object QProdutosAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QProdutosQTDE: TIntegerField
      FieldName = 'QTDE'
    end
    object QProdutosPRECO_UNI: TFloatField
      FieldName = 'PRECO_UNI'
    end
    object QProdutosPRECO_TAB: TFloatField
      FieldName = 'PRECO_TAB'
    end
    object QProdutosCANCELADO: TStringField
      FieldName = 'CANCELADO'
      FixedChar = True
      Size = 1
    end
    object QProdutosCOMREC: TStringField
      FieldName = 'COMREC'
      FixedChar = True
      Size = 1
    end
    object QProdutosPROD_ID: TIntegerField
      FieldName = 'PROD_ID'
    end
    object QProdutosCRM: TStringField
      FieldName = 'CRM'
      Size = 7
    end
    object QProdutosDATA_CADASTRO: TDateTimeField
      FieldName = 'DATA_CADASTRO'
    end
    object QProdutosdescricao: TStringField
      FieldName = 'descricao'
      Size = 90
    end
    object QProdutoscodinbs: TStringField
      FieldName = 'codinbs'
      Size = 13
    end
  end
  object QAutor: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'autor'
        DataType = ftInteger
        Value = 0
      end>
    SQL.Strings = (
      
        'Select contacorrente.*, conveniados.titular, conveniados.chapa, ' +
        'cartoes.nome nomecartao,'
      
        'cartoes.codigo catnum, cartoes.digito cartdig, cartoes.codcartim' +
        'p,'
      'credenciados.nome fornecedor,'
      'empresas.nome emp_razao, empresas.fantasia emp_fantasia,'
      
        '(case coalesce(contacorrente.formapagto_id,0) when 0 then '#39'A Vis' +
        'ta'#39
      
        ' else(select formaspagto.descricao from formaspagto where formas' +
        'pagto.forma_id = contacorrente.formapagto_id ) end) as forma'
      'from contacorrente'
      'join conveniados on conveniados.conv_id = contacorrente.conv_id'
      'join cartoes on cartoes.cartao_id = contacorrente.cartao_id'
      
        'join credenciados on credenciados.cred_id = contacorrente.cred_i' +
        'd'
      'join empresas on empresas.empres_id = conveniados.empres_id'
      'where contacorrente.autorizacao_id = :autor')
    Left = 533
    Top = 504
    object QAutorAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object QAutorCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QAutorCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QAutorCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QAutorDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QAutorDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object QAutorHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object QAutorDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object QAutorDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object QAutorCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object QAutorVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object QAutorBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object QAutorBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object QAutorENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object QAutorRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object QAutorCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object QAutorCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QAutorDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object QAutorTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object QAutorFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object QAutorFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QAutorPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QAutorAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object QAutorOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QAutorDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object QAutorDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object QAutorDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object QAutorDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object QAutorHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object QAutorNF: TIntegerField
      FieldName = 'NF'
    end
    object QAutorDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object QAutorDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object QAutorDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object QAutorOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object QAutorOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object QAutorDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object QAutorOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object QAutorCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object QAutorNSU: TIntegerField
      FieldName = 'NSU'
    end
    object QAutorPREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object QAutorEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QAutortitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QAutorchapa: TFloatField
      FieldName = 'chapa'
    end
    object QAutornomecartao: TStringField
      FieldName = 'nomecartao'
      Size = 58
    end
    object QAutorcatnum: TIntegerField
      FieldName = 'catnum'
    end
    object QAutorcartdig: TWordField
      FieldName = 'cartdig'
    end
    object QAutorcodcartimp: TStringField
      FieldName = 'codcartimp'
    end
    object QAutorfornecedor: TStringField
      FieldName = 'fornecedor'
      Size = 60
    end
    object QAutoremp_razao: TStringField
      FieldName = 'emp_razao'
      Size = 60
    end
    object QAutoremp_fantasia: TStringField
      FieldName = 'emp_fantasia'
      Size = 60
    end
    object QAutorforma: TStringField
      FieldName = 'forma'
      ReadOnly = True
      Size = 60
    end
  end
end
