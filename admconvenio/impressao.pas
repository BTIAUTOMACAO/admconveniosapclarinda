unit impressao;

interface

uses SysUtils, Dialogs, classes, Printers, Windows, Messages, UVisualizaImpressao;

function Centraliza(Texto, Preenchedor: String; Tamanho: Integer): String;
function Preenche(Texto, Preenchedor: String; Tamanho: Integer): String;
function Direita(Texto, Preenchedor: String; Tamanho: Integer): String;
function PegaPortaImpressora : String;
procedure IniciaImpressao;
procedure Imprime(Texto: String; Linha, Coluna: Integer);
procedure Imprimir(Visualizar: Boolean; Arquivo: String);
procedure ImpressoraPadrao(Index: Integer);
procedure NewPage;
procedure FolhaTeste(QtdeLinhas, QtdeColunas: Integer);
procedure TrimLinha(l:integer);
procedure TrimLinhas;
procedure TrimRLinhas;
procedure TrimLLinhas;

implementation

var Linhas      : TStringList;


//Coloca a impressora como padr�o
procedure ImpressoraPadrao(Index: Integer);
var
  Res: DWORD;
  Device : array[0..255] of char;
  Driver : array[0..255] of char;
  Port : array[0..255] of char;
  WindowsStr: array[0..255] of char;
  hDeviceMode: THandle;
begin
  Printer.PrinterIndex := Index;
  // Pega dados da impressora atual
  Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
  // Monta string exigida pela API do Windows
  StrCat( Device, ',');
  StrCat( Device, Driver );
  StrCat( Device, ',');
  StrCat( Device, Port );
  StrPCopy(WindowsStr, 'windows');
  // Torna a impressora a atual;
  WriteProfileString(WindowsStr, 'device', Device);
  SendMessageTimeout(HWND_BROADCAST, WM_WININICHANGE, 0,
DWORD(@WindowsStr),
    SMTO_NORMAL, 1000, Res);
end;

function PegaPortaImpressora : String;
var
  Device : array[0..255] of char;
  Driver : array[0..255] of char;
  Port : array[0..255] of char;
  hDeviceMode: THandle;
begin
   Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
   Result := Port;
end;

//Centraliza o texto
function Centraliza(Texto, Preenchedor: String; Tamanho: Integer): String;
begin
   Texto := Trim(Texto);
   if Length(Texto) > Tamanho then Texto := Copy(Texto , 1, Tamanho);

   while Length(Texto) < Tamanho do begin
      if (Length(Texto) mod 2) = 0 then Texto := Texto + Preenchedor
      else                              Texto := Preenchedor + Texto;
      end;

   Result := Texto;
end;

//Preenche linha com preenchedor
function Preenche(Texto, Preenchedor: String; Tamanho: Integer): String;
var i      :integer;
    texto2 :string;
begin
   texto2 := Texto;
   for i := 1 to Tamanho-Length(Texto) do texto2 := texto2 + Preenchedor;
   Preenche := texto2;
end;


//Coloca o texto a Direita
function Direita(Texto, Preenchedor: String; Tamanho: Integer): String;
var tamanho2, i: Integer;
    texto2     : String;
begin
   tamanho2 := Tamanho - Length(Texto);

   for i := 1 to tamanho2 do texto2 := texto2 + Preenchedor;

   Result := texto2 + Texto;
end;

//Inicia Impress�o Modo Texto
procedure IniciaImpressao;
begin
   Linhas := TStringList.Create;
end;

procedure Imprime(Texto: String; Linha, Coluna: Integer);
var i, z : Integer;
    textox, textoy, espaco : String;
begin
try
   for i := 0 to Coluna-1 do textox := textox + ' ';
   textox := textox + Texto;
   if Linha <= Linhas.Count then begin
      textoy := Linhas[Linha-1];
      if ((Coluna+Length(Texto)) > Length(textoy)) then begin
         for i := 0 to ((Coluna+Length(Texto))-Length(textoy)) do espaco := espaco + ' ';
         textoy := textoy + espaco;
         z := 1;
         for i := Coluna to (Length(Texto)+Coluna)-1 do begin
            textoy[i+1] := Texto[z];
            inc(z);
            end;
          end
      else if Coluna <= Length(textoy) then begin
         z := 1;
         for i := Coluna to ((Length(Texto)+Coluna)-1) do begin
            Textoy[i] := Texto[z];
            inc(z);
            end;
         end
      else begin
         espaco := '';
         for i := 0 to (Coluna-Length(textoy)-1) do espaco := espaco + ' ';
         textoy := textoy + espaco + Texto;
         end;
      Linhas[Linha-1] := textoy;
      end
   else begin
      for i := Linhas.Count to Linha-2 do Linhas.Add('');
      Linhas.Add(Textox);
      end;
except
  ShowMessage('Texto=' + Texto + ';Linha=' + IntToStr(Linha) + ';Coluna=' + IntToStr(Coluna)); // + ';Linhas.Count=' + IntToStr(Linhas.Count));
  // Linhas.SaveToFile('errimpr.log');                      { Comentada porque levanta uma exce��o }
  if not (DebugHook<>0) then raise;
end;
end;

procedure NewPage;
begin
Linhas.Append(#12); //NovaPagina
end;

procedure TrimLinha(l:integer);
begin
Linhas[l-1] := Trim(Linhas[l-1]);
end;

procedure TrimLinhas;
var i : integer;
begin
   for i := 0 to Linhas.Count - 1 do begin
       Linhas[i] := Trim(Linhas[i]);
   end;
end;

procedure TrimLLinhas;
var i : integer;
begin
   for i := 0 to Linhas.Count - 1 do begin
       Linhas[i] := TrimLeft(Linhas[i]);
   end;
end;

procedure TrimRLinhas;
var i : integer;
begin
   for i := 0 to Linhas.Count - 1 do begin
       Linhas[i] := TrimRight(Linhas[i]);
   end;
end;

procedure Imprimir(Visualizar: Boolean; Arquivo: String);
begin
   if Visualizar then begin
      FVisualizaImpressao := TFVisualizaImpressao.Create(nil);
      FVisualizaImpressao.ListBox1.Items.Assign(Linhas);
      FVisualizaImpressao.arquivo := Arquivo;
      FVisualizaImpressao.ShowModal;
      FVisualizaImpressao.Free;
   end
   else begin
      Linhas.SaveToFile(Arquivo);
   end;
   Linhas.Free;
end;

//Imprime Folha de Teste
procedure FolhaTeste(QtdeLinhas, QtdeColunas: Integer);
var i: Integer;
begin
   IniciaImpressao;
   for i := 0 to QtdeLinhas do Imprime(Preenche('', 'X', QtdeColunas), i+1, 0);
   Imprimir(FAlse, 'LPT1');
end;

end.

