unit UCadProduto2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, Menus, DB, ZDataset, ZAbstractRODataset, ZAbstractDataset,
  Buttons, StdCtrls, Mask, JvToolEdit, ComCtrls, Grids, DBGrids, {JvDBCtrl,}
  DBCtrls, ExtCtrls, {JvLookup,} ZSqlUpdate, {JvDBComb,} JvExStdCtrls,
  JvCombobox, JvDBCombobox, JvExControls, JvDBLookup, JvExMask,
  JvExDBGrids, JvDBGrid, ADODB, Types, StrUtils, ToolEdit, CurrEdit;

type
  TFCadProduto2 = class(TFCad)
    QCadastroPROD_ID: TIntegerField;
    QCadastroCODINBS: TStringField;
    QCadastroSN: TStringField;
    QCadastroFLAGNOME: TStringField;
    QCadastroMT: TStringField;
    QCadastroPRECO_FINAL: TFloatField;
    QCadastroPRECO_SEM_DESCONTO: TFloatField;
    QCadastroAPAGADO: TStringField;
    QCadastroENVIADO_FARMACIA: TStringField;
    QCadastroDIGITADO_SITE: TStringField;
    QCadastroCOD_ABC: TIntegerField;
    QCadastroLIBCIP: TStringField;
    QCadastroNOME: TStringField;
    QCadastroAPRES: TStringField;
    QCadastroPRECO_VND: TFloatField;
    QCadastroPRECO_CUSTO: TFloatField;
    QCadastroPRINCIPIO: TStringField;
    QCadastroUNID_CAIXA: TIntegerField;
    QCadastroCLASSE_PRODUTO: TStringField;
    QCadastroIPI: TFloatField;
    QCadastroLISTA: TStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QCadastroDESCRICAO: TStringField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    JvDBLookupCombo1: TJvDBLookupCombo;
    QCadastroOPERADOR: TStringField;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    edBarras: TEdit;
    Label10: TLabel;
    dsFaboratorio: TDataSource;
    dsPAtivo: TDataSource;
    dsFamilia: TDataSource;
    dsClasse: TDataSource;
    dsSClasse: TDataSource;
    JvDBLookupCombo2: TJvDBLookupCombo;
    JvDBLookupCombo3: TJvDBLookupCombo;
    JvDBLookupCombo4: TJvDBLookupCombo;
    JvDBLookupCombo5: TJvDBLookupCombo;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    QCadastroCOD_GUIA: TIntegerField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroFAM_ID: TIntegerField;
    QCadastroPAT_ID: TIntegerField;
    QCadastroCLAS_ID: TIntegerField;
    QCadastroSCLAS_ID: TIntegerField;
    QCadastroREGMS: TStringField;
    QCadastroGENERICO: TStringField;
    QCadastroPORTARIA: TStringField;
    JvDBComboBox1: TJvDBComboBox;
    Label15: TLabel;
    Label16: TLabel;
    JvDBComboBox2: TJvDBComboBox;
    grdBarra: TJvDBGrid;
    Label17: TLabel;
    dsBarras: TDataSource;
    Label18: TLabel;
    DBEdit5: TDBEdit;
    Label19: TLabel;
    DBEdit7: TDBEdit;
    cbbPAtivo: TJvDBLookupCombo;
    Label7: TLabel;
    btnLab: TBitBtn;
    btnGrupos: TBitBtn;
    btnClasse: TBitBtn;
    QCadastroGRUPO_PROD_ID: TIntegerField;
    QCadastroLAB_ID: TIntegerField;
    Label20: TLabel;
    JvDBLookupCombo7: TJvDBLookupCombo;
    dsGrupo: TDataSource;
    btnIncBarra: TSpeedButton;
    btnDelBarra: TSpeedButton;
    btnGravaBarra: TSpeedButton;
    btnCancelBarra: TSpeedButton;
    QCadastroFABR_ID: TIntegerField;
    QCadastroCODBARRAS: TStringField;
    QCadastroDATA: TDateTimeField;
    QCadastroLABORA: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroVIGENCIA: TDateTimeField;
    qPAtivo: TADOQuery;
    qPAtivoPAT_ID: TIntegerField;
    qPAtivoPATIVO: TStringField;
    qFamilia: TADOQuery;
    qFamiliaFAM_ID: TIntegerField;
    qFamiliaFAMILIA: TStringField;
    qClasse: TADOQuery;
    qClasseCLAS_ID: TIntegerField;
    qClasseCLASSE: TStringField;
    qSClasse: TADOQuery;
    qSClasseSCLAS_ID: TIntegerField;
    qSClasseSUBCLASSE: TStringField;
    qSClasseCLAS_ID: TIntegerField;
    qLaboratorio: TADOQuery;
    qLaboratorioLAB_ID: TIntegerField;
    qLaboratorioNOMELAB: TStringField;
    qLaboratorioNOMEFANT: TStringField;
    qBarras: TADOQuery;
    qBarrasBARRAS_ID: TIntegerField;
    qBarrasBARRAS: TStringField;
    qBarrasCODINBS: TStringField;
    qBarrasPROD_ID: TIntegerField;
    qGrupo: TADOQuery;
    qGrupogrupo_prod_id: TIntegerField;
    qGrupodescricao: TStringField;
    Label21: TLabel;
    btn1: TSpeedButton;
    btn2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    dbDesconto: TLabel;
    tsRegraNegocio: TTabSheet;
    cbbRegra: TComboBox;
    btnGravarRegra: TBitBtn;
    lbl1: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit8: TDBEdit;
    ADORegraNegocio: TADOQuery;
    ADORegraNegocioPROG_ID: TIntegerField;
    ADORegraNegocioNOME: TStringField;
    dsRegra: TDataSource;
    dbALT_LIBERADA: TCheckBox;
    edtDesconto: TCurrencyEdit;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    qAutorizador: TADOQuery;
    qAutorizadorPROD_CODIGO: TStringField;
    qAutorizadorPROD_DESCR: TStringField;
    qAutorizadorPRE_VALOR: TFloatField;
    dsAutorizador: TDataSource;
    edtValor: TCurrencyEdit;
    edtBarras: TEdit;
    edtNome: TEdit;
    qAutorizadorPROD_AUT_ID: TIntegerField;
    dbValor: TCurrencyEdit;
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
    procedure btnClasseClick(Sender: TObject);
    procedure btnGruposClick(Sender: TObject);
    procedure btnLabClick(Sender: TObject);
    procedure btnIncBarraClick(Sender: TObject);
    procedure btnDelBarraClick(Sender: TObject);
    procedure btnGravaBarraClick(Sender: TObject);
    procedure btnCancelBarraClick(Sender: TObject);
    procedure dsBarrasStateChange(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure qBarrasAfterInsert(DataSet: TDataSet);
    procedure ButApagaClick(Sender: TObject);
    procedure btnGravarRegraClick(Sender: TObject);
    function Split(Expression:string; Delimiter:string):TStringDynArray;
    procedure dbValorChange(Sender: TObject);
    procedure edtBarrasChange(Sender: TObject);
    procedure tsRegraNegocioEnter(Sender: TObject);


//    procedure SetCheckBoxAlignment(iColNumber, iRowNumber:integer; bChecked :Boolean);
//    procedure AddCheckBox(iColNumber: integer; iRowNumber: Integer);
  private
    { Private declarations }
    apagar: boolean;
    inserir: Boolean;
    listClasses, listGrupos, listLab: String;
  public
    { Public declarations }
  end;

var
  FCadProduto2: TFCadProduto2;

implementation

uses DM, cartao_util, UMenu, uSeleciona, UTipos;

{$R *.dfm}

//Willian - Fun��o para dar Split, SplitString n�o funcionou
function TFCadProduto2.Split(Expression:string; Delimiter:string):TStringDynArray;
var
  Res:        TStringDynArray;
  ResCount:   DWORD;
  dLength:    DWORD;
  StartIndex: DWORD;
  sTemp:      string;
begin
  dLength := Length(Expression);
  StartIndex := 1;
  ResCount := 0;
  repeat
    sTemp := Copy(Expression, StartIndex, Pos(Delimiter, Copy(Expression, StartIndex, Length(Expression))) - 1);
    SetLength(Res, Length(Res) + 1);
    SetLength(Res[ResCount], Length(sTemp));
    Res[ResCount] := sTemp;
    StartIndex := StartIndex + Length(sTemp) + Length(Delimiter);
    ResCount := ResCount + 1;
  until StartIndex > dLength;
  Result := Res;
end;

procedure TFCadProduto2.FormCreate(Sender: TObject);
begin
  chavepri := 'prod_id';
  detalhe  := 'Produto ID: ';
  inherited;
  qGrupo.Open;
  qPAtivo.Open;
  qFamilia.Open;
  qClasse.Open;
  qSClasse.Open;
  qLaboratorio.Open;
  qBarras.Open;


  listClasses:= '';
  listGrupos:= '';
  listLab:= '';
  dbValor.Text := '0,00';
  apagar := false;
  //Willian - Preenchimento do Combo de Regra de Neg�cio
  ADORegraNegocio.SQL.Add('SELECT PROG_ID, NOME FROM PROGRAMAS WHERE APAGADO <> ''S''');
  ADORegraNegocio.Open;
  while not ADORegraNegocio.Eof do begin
    cbbRegra.Items.Add(IntToStr(ADORegraNegocio.Fields[0].Value)+'-'+ADORegraNegocio.Fields[1].Value);
    ADORegraNegocio.Next;
  end;

  QCadastro.Open;
end;

procedure TFCadProduto2.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Produto: '+QCadastroNOME.AsString;
end;

procedure TFCadProduto2.ButBuscaClick(Sender: TObject);
begin
  inherited;
  screen.Cursor := crHourGlass;
  QCadastro.Close;
  QCadastro.Sql.Text := ' SELECT * FROM produtos '+
    ' WHERE apagado <> ''S'' ';
  if listClasses <> '' then
    QCadastro.Sql.Add(' AND clas_id IN ('+listClasses+') ');
  if listGrupos <> '' then
    QCadastro.Sql.Add(' AND grupo_prod_id IN ('+listGrupos+') ');
  if listLab <> '' then
    QCadastro.Sql.Add(' AND lab_id IN ('+listLab+') ');
  if cbbPAtivo.KeyValue <> 0 then
    QCadastro.Sql.Add(' AND pat_id = '+cbbPAtivo.KeyValue+' ');

  if ((Trim(EdCod.Text) <> '') or (Trim(EdNome.Text) <> '') or (Trim(edBarras.Text) <> '')) then
  begin
    if Trim(EdCod.Text) <> '' then
      QCadastro.Sql.Add(' AND prod_id IN ('+EdCod.Text+') ORDER BY produtos.prod_id ')
    else if Trim(EdNome.Text) <> '' then
      QCadastro.Sql.Add(' AND descricao LIKE ''%'+EdNome.Text+'%'' ORDER BY produtos.nome ')
    else
      QCadastro.Sql.Add(' AND prod_id in (SELECT barras.prod_id FROM barras where barras.barras LIKE ''%'+edBarras.Text+'%'' ) ORDER BY nome ');
  end
  else QCadastro.Sql.Add(' ORDER BY prod_id ');
  QCadastro.SQL.Text;
  QCadastro.Open;
  if not QCadastro.IsEmpty then DBGrid1.SetFocus;
  edcod.Clear;
  EdNome.Clear;
  edBarras.Clear;
  cbbPAtivo.KeyValue:= 0;
  screen.Cursor := crDefault;

end;

procedure TFCadProduto2.ButIncluiClick(Sender: TObject);
begin
  inherited;
  qBarras.Close;
  qBarras.Open;
  DBEdit2.SetFocus;
end;

procedure TFCadProduto2.QCadastroAfterInsert(DataSet: TDataSet);
  
begin
  inherited;

  QCadastroAPAGADO.AsString := 'N';
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_ID AS PROD_ID');
  DMConexao.AdoQry.Open;
  QCadastroPROD_ID.AsInteger            := DMConexao.AdoQry.FieldByName('PROD_ID').AsInteger;
  QCadastroPRECO_VND.AsCurrency:= 0.00;
  QCadastroPRECO_CUSTO.AsCurrency:= 0.00;
end;

procedure TFCadProduto2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qGrupo.Close;
  qPAtivo.Close;
  qFamilia.Close;
  qClasse.Close;
  qSClasse.Close;
  qLaboratorio.Close;
  qBarras.Close;
end;

procedure TFCadProduto2.QCadastroBeforePost(DataSet: TDataSet);
  
begin
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;
  if apagar = false then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('select prod_id from produtos where apagado = ''N'' and prod_id in (select prod_id from barras where barras = '''+QCadastro.FieldByName('CODBARRAS').AsString+''')');
    DMConexao.AdoQry.Open;
    if (DMConexao.AdoQry.Fields[0].AsString <> '') and (DMConexao.AdoQry.Fields[0].AsString <> QCadastro.FieldByName('PROD_ID').AsString) then
    begin
      if not MsgSimNao('C�digo de barras ja cadastrado para o produto '+DMConexao.AdoQry.Fields[0].AsString+sLineBreak+'Deseja incluir mesmo assim?') then
      begin
        QCadastro.Cancel;
        Abort;
      end;
    end;
  end;
  if QCadastro.State in [dsInsert] then
  begin
    QCadastro.FieldByName('PRECO_FINAL').AsCurrency:= QCadastro.FieldByName('PRECO_VND').AsCurrency;
    QCadastro.FieldByName('PRECO_SEM_DESCONTO').AsCurrency:= QCadastro.FieldByName('PRECO_CUSTO').AsCurrency;
    QCadastro.FieldByName('ENVIADO_FARMACIA').AsString:= 'N';
    QCadastro.FieldByName('DIGITADO_SITE').AsString:= 'N';
    if QCadastro.FieldByName('CODBARRAS').AsString <> '' then
    begin
      DMConexao.ExecuteSql(' insert into barras (barras_id, prod_id, barras) values (NEXT VALUE FOR SBARRAS_ID,'+QCadastro.FieldByName('PROD_ID').AsString+', '''+QCadastro.FieldByName('CODBARRAS').AsString+''')');
    end;

  end
  else
  begin
    if ((QCadastroCODBARRAS.OldValue <> QCadastroCODBARRAS.AsString) and (QCadastroCODBARRAS.AsString <> '')) then
    begin
      {Comparo se o novo c�digo de barras n�o existe}
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('select barras_id from barras where prod_id = '+QCadastro.FieldByName('PROD_ID').AsString +' and barras = '''+QCadastro.FieldByName('CODBARRAS').AsString+'''');
      DMConexao.AdoQry.Open;
      if DMConexao.AdoQry.IsEmpty then
      begin
        {Compara se era vazio antes}
        if QCadastro.FieldByName('CODBARRAS').OldValue = Null then
        begin
          DMConexao.ExecuteSql(' insert into barras (barras_id, prod_id, barras) values (NEXT VALUE FOR SBARRAS_ID,'+QCadastro.FieldByName('PROD_ID').AsString+', '''+QCadastro.FieldByName('CODBARRAS').AsString+''')');
        end
        {Comparo se o antigo existe}
        else
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('select barras_id from barras where prod_id = '+QCadastro.FieldByName('PROD_ID').AsString +' and barras = '''+QCadastro.FieldByName('CODBARRAS').OldValue+'''');
          DMConexao.AdoQry.SQL.Text;
          DMConexao.AdoQry.Open;
          if not DMConexao.AdoQry.IsEmpty then
          begin
            if MsgSimNao('Deseja adicionar um c�digo de barras adcional?'+sLineBreak+'Caso clique em [N�o], o sistema substituir� o c�digo de barras antigo!') then
            begin
              DMConexao.ExecuteSql(' insert into barras (barras_id, prod_id, barras) values (NEXT VALUE FOR SBARRAS_ID,'+QCadastro.FieldByName('PROD_ID').AsString+', '''+QCadastro.FieldByName('CODBARRAS').AsString+''')');
            end
            else
            begin
              DMConexao.ExecuteSql(' update barras set barras = '''+QCadastro.FieldByName('CODBARRAS').AsString+''' where prod_id = '+QCadastro.FieldByName('PROD_ID').AsString +' and barras = '''+QCadastro.FieldByName('CODBARRAS').OldValue+'''');
            end;
          end
          else
          begin
            DMConexao.ExecuteSql(' insert into barras (barras_id, prod_id, barras) values (NEXT VALUE FOR SBARRAS_ID,'+QCadastro.FieldByName('PROD_ID').AsString+', '''+QCadastro.FieldByName('CODBARRAS').AsString+''')');
        end;
      end;
    end;
  end;
  if apagar = true then begin
    DMConexao.ExecuteSql(' delete from barras where prod_id = '+QCadastro.FieldByName('PROD_ID').AsString +'');
  end;
  //qBarras.Refresh;
  apagar := false;
end;

procedure TFCadProduto2.ButGravaClick(Sender: TObject);
var id1, id2: Integer;
var valor1: string ;
var valor2: string ;
var alteracaoLiberada: string;
begin
  inherited;


  //Willian - Retira v�rgulas dos n�meros e substitui por pontos
  valor1 := DBEdit3.Text;
  valor1 := StringReplace(valor1, ',', '.', [rfReplaceAll]);
  valor2 := dbValor.Text;
  valor2 := StringReplace(valor2,',','.',[rfReplaceAll]);

  if (dbALT_LIBERADA.Checked) then begin
    alteracaoLiberada := 'S';
  end else begin
    alteracaoLiberada := 'N';
  end;

  //Verifica se � inser��o ou altera��o
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT MAX(PROD_ID) FROM PRODUTOS');
  DMConexao.AdoQry.Open;
  id1 := DMConexao.AdoQry.Fields[0].AsInteger;
  id2 := DBEdit1.Field.AsInteger;
  if (id1 = id2) then begin
    //Willian - Insere produto na tabela PRODUTOS_AUTORIZADOR
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_AUT_ID AS PROD_AUT_ID');
    DMConexao.AdoQry.Open;
    DMConexao.AdoQry.SQL.Clear;

    if(valor2 <> '') then begin
      DMConexao.ExecuteSql('INSERT INTO PRODUTOS_AUTORIZADOR VALUES ((SELECT MAX(PROD_AUT_ID)+1 FROM PRODUTOS_AUTORIZADOR),'''+ DBEdit6.Text +''','''+ DBEdit2.Text +''','''+ Copy(JvDBLookupCombo1.Text,1,15) +''', '+valor1+' , '+valor2+' ,'''+alteracaoLiberada+''',NULL,NULL,NULL,NULL,GETDATE(),''N'',NULL)');
    end else begin
      DMConexao.ExecuteSql('INSERT INTO PRODUTOS_AUTORIZADOR VALUES ((SELECT MAX(PROD_AUT_ID)+1 FROM PRODUTOS_AUTORIZADOR),'''+ DBEdit6.Text +''','''+ DBEdit2.Text +''','''+ Copy(JvDBLookupCombo1.Text,1,15) +''', '+valor1+' , 0 ,'''+alteracaoLiberada+''',NULL,NULL,NULL,NULL,GETDATE(),''N'',NULL)');
    end;
        end else begin
    //DMConexao.ExecuteSql('UPDATE PRODUTOS_AUTORIZADOR SET PROD_DESCR = '''+ DBEdit2.Text +''', LABORATORIO = '''+ Copy(JvDBLookupCombo1.Text,1,15) +''', PRE_VALOR ='+valor1.Text+', PRE_VALOR_19 = '+valor2.Text+', ALT_LIBERADA ='''+alteracaoLiberada+'''   WHERE PROD_CODIGO = '''+ DBEdit6.Text +''' ');
  end;

  Self.TextStatus := 'Produto: '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadProduto2.btnClasseClick(Sender: TObject);
begin
  inherited;
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Classes');
  fSeleciona.setTabela('CLASSE');
  fSeleciona.setCodigo('CLAS_ID');
  fSeleciona.setNome('CLASSE');
  fSeleciona.setIDs(listClasses);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listClasses:= fSeleciona.getIDs;
    if listClasses <> '' then
      btnClasse.Font.Style:= [fsBold]
    else
      btnClasse.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFCadProduto2.btnGruposClick(Sender: TObject);
begin
  inherited;
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Grupo de Produtos');
  fSeleciona.setTabela('GRUPO_PROD');
  fSeleciona.setCodigo('GRUPO_PROD_ID');
  fSeleciona.setNome('DESCRICAO');
  fSeleciona.setIDs(listGrupos);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listGrupos:= fSeleciona.getIDs;
    if listGrupos <> '' then
      btnGrupos.Font.Style:= [fsBold]
    else
      btnGrupos.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFCadProduto2.btnLabClick(Sender: TObject);
begin
  inherited;
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Laborat�rios');
  fSeleciona.setTabela('LABORATORIOS');
  fSeleciona.setCodigo('LAB_ID');
  fSeleciona.setNome('NOMELAB');
  fSeleciona.setIDs(listLab);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listLab:= fSeleciona.getIDs;
    if listLab <> '' then
      btnLab.Font.Style:= [fsBold]
    else
      btnLab.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFCadProduto2.btnIncBarraClick(Sender: TObject);
begin
  inherited;
  if (QCadastro.State = dsInsert) then
  begin
    if MsgSimNao('� necess�rio gravar o produto para incluir c�digo de barras sobre ele, deseja salvar ?') then
      ButGrava.Click
    else
      Exit;
  end;
  qBarras.Append;
  grdBarra.Options:= [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
  grdBarra.SetFocus;
end;

procedure TFCadProduto2.btnDelBarraClick(Sender: TObject);
begin
  inherited;
  if (not qBarras.IsEmpty) and MsgSimNao('Confirma a exclus�o do codigo de barras: '+sLineBreak+qBarrasBARRAS.AsString+'?') then
  begin
    if qBarras.FieldByName('BARRAS').AsString = QCadastro.FieldByName('CODBARRAS').AsString then
    begin
      if qBarras.RecordCount = 1 then
      begin
        if not MsgSimNao('Este produto ficar� sem c�digo de barras associado a ele!'+sLineBreak+'Confirma essa opera��o?') then
        begin
          qBarras.Cancel;
          Exit;
        end;
        QCadastro.Edit;
        QCadastro.FieldByName('CODBARRAS').Clear;
        QCadastro.Post;
      end;
    end;
    qBarras.Delete;
  end;
end;

procedure TFCadProduto2.btnGravaBarraClick(Sender: TObject);
begin
  inherited;
  qBarras.Post;
  grdBarra.Options:= [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
end;

procedure TFCadProduto2.btnCancelBarraClick(Sender: TObject);
begin
  inherited;
  qBarras.Cancel;
end;

procedure TFCadProduto2.dsBarrasStateChange(Sender: TObject);
begin
  inherited;
  btnIncBarra.Enabled := ((qBarras.State = dsBrowse) and Incluir);
  btnDelBarra.Enabled  := (qBarras.State = dsBrowse) and Excluir and (not qBarras.IsEmpty);
  btnCancelBarra.Enabled   := dsBarras.State in [dsEdit,dsInsert];
  btnGravaBarra.Enabled  := dsBarras.State in [dsEdit,dsInsert];
end;

procedure TFCadProduto2.TabFichaShow(Sender: TObject);
begin
  inherited;
  dsBarras.OnStateChange(Self);
end;

procedure TFCadProduto2.qBarrasAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SBARRAS_ID AS BARRAS_ID');
  DMConexao.AdoQry.Open;
  QBarrasBARRAS_ID.AsInteger            := DMConexao.AdoQry.FieldByName('BARRAS_ID').AsInteger;
end;

procedure TFCadProduto2.ButApagaClick(Sender: TObject);
begin
  apagar := true;
  inherited;
end;



//Willian - Gravar uma regra de neg�cio por vez
procedure TFCadProduto2.btnGravarRegraClick(Sender: TObject);
var
  conteudoCombo : string;
  desconto : string;
  valorVenda : string;
  alteracaoLiberada: string;
  valor19: string;
  idRegra : TStringDynArray;
begin
  inherited;
  //Valida��o de Campos
  conteudoCombo := cbbRegra.Text;
  if (DBEdit8.Text = '') then begin
    ShowMessage('N�O FOI SELECIONADO PRODUTO');
  end else begin
    //Substituindo poss�veis v�rgulas por pontos para inser��o
    desconto := edtDesconto.Text;
    desconto := StringReplace(desconto, ',', '.', [rfReplaceAll]);
    valorVenda := edtValor.Text;
    valorVenda := StringReplace(valorVenda, ',', '.', [rfReplaceAll]);
    valor19 := dbValor.Text;
    valor19 := StringReplace(valor19,',','.',[rfReplaceAll]);
    //Verifica��o se est� liberada na aba anterior a altera��o de pre�o
    if (dbALT_LIBERADA.Checked) then begin
      alteracaoLiberada := 'S';
    end else begin
      alteracaoLiberada := 'N';
    end;
    //Pegando ID do combobox
    idRegra := Split(conteudoCombo, '-');

    //Willian - Inser��o ou Atualiza��o de Produto Autorizador
    if (inserir = True) then begin
      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SPROD_AUT_ID AS PROD_AUT_ID');
      DMConexao.AdoQry.Open;
      DMConexao.AdoQry.SQL.Clear;
      if(valor19 <> '') then begin
        DMConexao.ExecuteSql('INSERT INTO PRODUTOS_AUTORIZADOR VALUES ((SELECT MAX(PROD_AUT_ID)+1 FROM PRODUTOS_AUTORIZADOR),'''+ edtBarras.Text +''','''+ edtNome.Text +''','''+ Copy(JvDBLookupCombo1.Text,1,15) +''', '+valorVenda+' , '+valor19+' ,'''+alteracaoLiberada+''',NULL,NULL,NULL,NULL,GETDATE(),''N'',NULL)');
      end else begin
        DMConexao.ExecuteSql('INSERT INTO PRODUTOS_AUTORIZADOR VALUES ((SELECT MAX(PROD_AUT_ID)+1 FROM PRODUTOS_AUTORIZADOR),'''+ edtBarras.Text +''','''+ edtNome.Text +''','''+ Copy(JvDBLookupCombo1.Text,1,15) +''', '+valorVenda+' , 0 ,'''+alteracaoLiberada+''',NULL,NULL,NULL,NULL,GETDATE(),''N'',NULL)');
      end;
      inserir := False;
    end else begin
      DMConexao.ExecuteSql('UPDATE PRODUTOS_AUTORIZADOR SET PROD_DESCR = '''+ edtNome.Text +''', PRE_VALOR = '+ valorVenda +' WHERE PROD_AUT_ID = '+ qAutorizadorPROD_AUT_ID.AsString );
    end;
    DMConexao.AdoQry.Close;

    //Inser��o ou atualiza��o de Regra de Neg�cio
    if(conteudoCombo <> 'SELECIONE A REGRA') then begin
      //Tentando excluir Regra de Neg�cio, caso j� exista alguma para o produto  e a regra espec�fica
      try
        DMConexao.ExecuteSql('DELETE FROM PROG_PROD WHERE PROD_ID = '+DBEdit8.Text+' AND PROG_ID = '+idRegra[0]);
      finally
        //Inserindo regra de neg�cio
        if(valor19 <> '') then begin
          DMConexao.ExecuteSql('INSERT INTO PROG_PROD VALUES ('+DBEdit8.Text+', '+idRegra[0]+','+valorVenda+','+ desconto +', 0, ''N'',''S'', NULL,'+valorVenda+','+ valor19 +')');
        end else begin
          DMConexao.ExecuteSql('INSERT INTO PROG_PROD VALUES ('+DBEdit8.Text+', '+idRegra[0]+','+valorVenda+','+ desconto +', 0, ''N'',''S'', NULL,'+valorVenda+',0)');
        end;

      end;
    end;
    ShowMessage('REGRA ADICIONADA COM SUCESSO');
  end;
end;

procedure TFCadProduto2.dbValorChange(Sender: TObject);
var valor : real;
begin
  inherited;
  valor := StrToCurr(dbValor.Text);
  dbValor.Text := FormatFloat('#0,00',(valor));
end;



procedure TFCadProduto2.edtBarrasChange(Sender: TObject);
var prod_id: String;
begin
  inherited;

  try
    //Preenchendo o PROD_ID na aba Regra de Neg�cio
    //quando pesquisa por c�digo de barras
    prod_id := DMConexao.ExecuteScalar('SELECT PROD_ID FROM BARRAS WHERE BARRAS = '''+  edtBarras.Text +'''');
    QCadastro.SQL.Clear;
    QCadastro.SQL.Add('SELECT * FROM PRODUTOS WHERE PROD_ID = '+prod_id);
    QCadastro.Open ;

    //Preenchendo os demais campos referentes ao produto autorizador
    //quando pesquisa por c�digo de barras
    qAutorizador.SQL.Clear;
    qAutorizador.SQL.Add('SELECT PROD_AUT_ID, PROD_CODIGO,PROD_DESCR,PRE_VALOR FROM PRODUTOS_AUTORIZADOR WHERE PROD_CODIGO = '''+ edtBarras.Text +'''');
    qAutorizador.Open;
    edtNome.Text := qAutorizadorPROD_DESCR.AsString;
    edtValor.Text := qAutorizadorPRE_VALOR.AsString;

  //Faz verifica��o se o produto vai ser inserido ou atualizado na tabela
    inserir := False;
  except
    inserir := True;
  end;

end;

procedure TFCadProduto2.tsRegraNegocioEnter(Sender: TObject);
begin
  inherited;
  if(DBEdit8.Text <> '') then begin
    //Preenchendo os demais campos referentes ao produto autorizador
    //quando pesquisa por c�digo de barras
    qAutorizador.SQL.Clear;
    qAutorizador.SQL.Add('SELECT PROD_AUT_ID, PROD_CODIGO, PROD_DESCR, PRE_VALOR FROM PRODUTOS_AUTORIZADOR A INNER JOIN BARRAS B ON A.PROD_CODIGO = B.BARRAS WHERE B.PROD_ID = '+  DBEdit8.Text +'');
    qAutorizador.Open;
    edtBarras.Text := qAutorizadorPROD_CODIGO.AsString;
    edtNome.Text := qAutorizadorPROD_DESCR.AsString;
    edtValor.Text := qAutorizadorPRE_VALOR.AsString;

  //Faz verifica��o se o produto vai ser inserido ou atualizado na tabela
    inserir := False;
  end else begin
    inserir := True;
  end;
end;



end.
