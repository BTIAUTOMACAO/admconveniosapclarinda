{
FORAM DELETADOS DO COMBOBOX - cbModelo
---------------
Padr�o
Layout Antigo
VCT Brasil (Confarma)
MR Card (Bonfarma)
}

unit UGeraCartBenBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, StdCtrls, {JvLookup,} ExtCtrls, Grids, DBGrids, {JvDBCtrl,} DB,
  ZAbstractRODataset, ZDataset, Mask, ToolEdit, RxMemDS, ComCtrls,
  ZAbstractDataset, Buttons, Menus, OleServer, ExcelXP, JvToolEdit,
  {JvDBComb,} JvExDBGrids, JvDBGrid, JvExStdCtrls, JvCombobox, JvDBCombobox,
  JvExMask, JvExControls, JvDBLookup, DBCtrls,ShlObj, ADODB;

type
  TFGeraCartBenBemEstar = class(TF1)
    GroupBox1: TGroupBox;
    DBEmpresa: TJvDBLookupCombo;
    chempresa: TCheckBox;
    ChNomeCart: TCheckBox;
    EdNomeCart: TEdit;
    chSoNaoEmit: TCheckBox;
    ButBusca: TButton;
    Panel2: TPanel;
    ButMarcDesm: TButton;
    ButMarcaTodos: TButton;
    ButDesmTodos: TButton;
    ButProcessa: TButton;
    DBGrid1: TJvDBGrid;
    DataSource1: TDataSource;
    Tempcartoes: TRxMemoryData;
    //Filename: TFilenameEdit;
    Label1: TLabel;
    chMarcaCart: TCheckBox;
    TempcartoesNome: TStringField;
    TempcartoesEmpresa: TStringField;
    chConvLib: TCheckBox;
    chCartLib: TCheckBox;
    TempcartoesTitular: TStringField;
    DSEmpresa: TDataSource;
    Barra: TStatusBar;
    TempcartoesCidade: TStringField;
    TempcartoesCARTAOTITU: TStringField;
    CkUsarCodImp: TCheckBox;
    ButPesConv: TSpeedButton;
    Button1: TButton;
    Bevel1: TBevel;
    RGTipoArq: TRadioGroup;
    Bevel2: TBevel;
    Tempcartoescartao: TStringField;
    Tempcartoescartao_id: TIntegerField;
    TempcartoesMarcado: TStringField;
    Label2: TLabel;
    cbModelo: TComboBox;
    chSoTitular: TCheckBox;
    ckAbreExcel: TCheckBox;
    Tempcartoesconv_id: TIntegerField;
    TempcartoesDATACADASTRO: TDateTimeField;
    TempcartoesCRED_ID: TIntegerField;
    panTodasasdatas: TPanel;
    ckbTodasdatas: TCheckBox;
    rdgDatas: TRadioGroup;
    Label3: TLabel;
    Label4: TLabel;
    DataIni: TJvDateEdit;
    Datafim: TJvDateEdit;
    cbbFiltroCartao: TJvDBComboBox;
    Label5: TLabel;
    chSoDependentes: TCheckBox;
    TempcartoesCHAPA: TFloatField;
    TempcartoesCODCARTIMP: TStringField;
    chImpCartEsp: TCheckBox;
    dsModelosCartoes: TDataSource;
    Label6: TLabel;
    DBText1: TDBText;
    Filename: TJvFilenameEdit;
    QCartoes: TADOQuery;
    QCartoesnome: TStringField;
    QCartoescartaotitu: TStringField;
    QCartoescodcartimp: TStringField;
    QCartoestitular: TStringField;
    QCartoeschapa: TFloatField;
    QCartoescartao: TStringField;
    QCartoescartao_id: TIntegerField;
    QCartoesdtcadastro: TDateTimeField;
    QCartoesempresa: TStringField;
    QCartoesconv_id: TIntegerField;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasmod_cart_id: TIntegerField;
    qModelosCartoes: TADOQuery;
    qModelosCartoesMOD_CART_ID: TIntegerField;
    qModelosCartoesDESCRICAO: TStringField;
    qModelosCartoesOBSERVACAO: TStringField;
    qModelosCartoesAPAGADO: TStringField;
    qModelosCartoesDTAPAGADO: TDateTimeField;
    qModelosCartoesDTALTERACAO: TDateTimeField;
    qModelosCartoesDTCADASTRO: TDateTimeField;
    qModelosCartoesOPERADOR: TStringField;
    qModelosCartoesOPERCADASTRO: TStringField;
    QUpdate: TADOQuery;
    QLotesCartao: TADOQuery;
    QLotesCartaoSEQ_LOTE: TIntegerField;
    QLotesCartaoDATA: TWideStringField;
    QLotesCartaoHORA: TWideStringField;
    QLotesCartaoNOME_ARQ: TStringField;
    QLotesCartaoEMPRES_ID: TIntegerField;
    QLotesCartaoDetails: TADOQuery;
    QLotesCartaoDetailsSEQ_LOTE: TIntegerField;
    QLotesCartaoDetailsCARTAO_ID: TIntegerField;
    TempcartoesEmpres_id: TIntegerField;
    QCartoesempres_id: TIntegerField;
    lkDepartamento: TJvDBLookupCombo;
    Label7: TLabel;
    QDepart: TADOQuery;
    DSDepart: TDataSource;
    QDepartempres_id: TIntegerField;
    QDepartdescricao: TStringField;
    QDepartdept_id: TIntegerField;
    QLotesCartaosetor_id: TIntegerField;
    chkGeraArqPorSetor: TCheckBox;
    QCartoessetor: TStringField;
    QCartoessetor_id: TIntegerField;
    Tempcartoessetor: TStringField;
    Tempcartoessetor_id: TIntegerField;
    Tempcartoesmodelo_cartoes: TStringField;
    QCartoesmodelo_cartoes: TStringField;
    QCartoescidade: TIntegerField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure chempresaClick(Sender: TObject);
    procedure ChNomeCartClick(Sender: TObject);
    procedure ButMarcDesmClick(Sender: TObject);
    procedure ButMarcaTodosClick(Sender: TObject);
    procedure ButDesmTodosClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButProcessaClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RGTipoArqClick(Sender: TObject);
    procedure ButPesConvClick(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure FilenameExit(Sender: TObject);
    procedure ckbTodasdatasClick(Sender: TObject);
    procedure chSoDependentesClick(Sender: TObject);
    procedure chSoTitularClick(Sender: TObject);
    procedure DSEmpresaDataChange(Sender: TObject; Field: TField);
    procedure DBEmpresaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lkDepartamentoEnter(Sender: TObject);
    procedure DBEmpresaEnter(Sender: TObject);
  private
    { Private declarations }
    procedure Marca_Desmarca(Marcar:Boolean);
    procedure AdicionarCartaoById(ID: Integer);
    procedure AdicionarCartoes;
    procedure AdicionarCartoesGrade;
    function PesqSql: TStrings;
    procedure GerarArquivoTexto;
    procedure GerarArquivoTextoComQuebraPorSetor;
    procedure TratarCamposVisiveis;
    procedure SetFieldIndex;
  public
    { Public declarations }
    desc : Boolean;
    campo : string;
    function verificaDepartamento() : Boolean;
    function meusdocpath()  : string;
  end;

var
  FGeraCartBenBemEstar: TFGeraCartBenBemEstar;
  flag : Boolean = False;

implementation

uses DM, UPesqConv, UDBGridHelper, cartao_util, UMenu,
  ClassImpressao, UValidacao, Math;

{$R *.dfm}

function TFGeraCartBenBemEstar.verificaDepartamento() : Boolean;
begin
   if (QDepart.RecordCount <> 0) then
      result := True
   else
      result := False;
end;

procedure TFGeraCartBenBemEstar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = vk_F11 then ButMarcDesm.Click;
  if key = vk_F8  then ButMarcaTodos.Click;
  if key = vk_F9  then ButDesmTodos.Click;
  if key = vk_F7  then ButProcessaClick(nil);
end;

procedure TFGeraCartBenBemEstar.FormCreate(Sender: TObject);
begin
  inherited;
  QEmpresas.Open;
  cbbFiltroCartao.ItemIndex:= 0;
  Filename.FileName := PChar(FMenu.GetPersonalFolder) + '\Cartoes.txt';
  DBEmpresa.KeyValue := QEmpresasEMPRES_ID.AsInteger;
  DMConexao.Config.Open;
  if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
    CkUsarCodImp.Checked:= True
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
    CkUsarCodImp.Checked:= True
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
    CkUsarCodImp.Checked:= True
  else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
    CkUsarCodImp.Checked:= True
  else
    CkUsarCodImp.Checked:= False;
  DMConexao.Config.Close;
  DataIni.Date:= PrimeiroDiaMes(Now);
  Datafim.Date:= Now;
  FMenu.vGeracaoCartao := True;
end;

procedure TFGeraCartBenBemEstar.chempresaClick(Sender: TObject);
begin
  inherited;
  DBEmpresa.Enabled := chempresa.Checked;
end;

procedure TFGeraCartBenBemEstar.ChNomeCartClick(Sender: TObject);
begin
  inherited;
  EdNomeCart.Enabled := ChNomeCart.Enabled;
  ButPesConv.Enabled := ChNomeCart.Enabled;
end;

procedure TFGeraCartBenBemEstar.ButMarcDesmClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesm(Tempcartoes);
end;

procedure TFGeraCartBenBemEstar.Marca_Desmarca(Marcar:Boolean);
begin
end;

procedure TFGeraCartBenBemEstar.ButMarcaTodosClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesmTodos(Tempcartoes,True);
end;

procedure TFGeraCartBenBemEstar.ButDesmTodosClick(Sender: TObject);
begin
  inherited;
  DMConexao.MarcaDesmTodos(Tempcartoes,False);
end;

procedure TFGeraCartBenBemEstar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  QEmpresas.Close;
  Tempcartoes.Close;
  FMenu.vGeracaoCartao := False;
end;

procedure TFGeraCartBenBemEstar.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesmClick(nil);
end;

procedure TFGeraCartBenBemEstar.DBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
var ResultQuery : Integer;
begin
  inherited;
  if campo <> Field.FieldName then
  begin
    Tempcartoes.SortOnFields(Field.FieldName);
    desc := false;
    campo := Field.FieldName;
  end
  else
  begin
    desc := not desc;
    Tempcartoes.SortOnFields(Field.FieldName,True,desc);
  end;
end;

procedure TFGeraCartBenBemEstar.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  
begin
  inherited;
  if key = vk_return then ButMarcDesmClick(nil);
end;

procedure TFGeraCartBenBemEstar.ButProcessaClick(Sender: TObject);
var  mens,setorEmpresTemp,setorEmpresOld,codEmpresTemp,codEmpresOld : string;
     ResultQuery : Integer;
     firstLine : Boolean;

begin
  if DMConexao.ContaMarcados(Tempcartoes) = 0 then
  begin
    MsgInf('Nenhum cart�o selecionado!');
    Exit;
  end;
  mens := 'Confirma gera��o de cart�es para os conv�niados selecionados?';
  if Application.MessageBox(PChar(mens),'Confirma��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDNo then Exit;
  if RGTipoArq.ItemIndex = 1 then begin
     TratarCamposVisiveis;
     SetFieldIndex;
     Grade_to_PlanilhaMarcado(DBGrid1,'Cartoes',True,Filename.FileName,ckAbreExcel.Checked,True);
  end
  else begin
     if chkGeraArqPorSetor.Checked then
        GerarArquivoTextoComQuebraPorSetor
        else
          GerarArquivoTexto;
  end;

  //comecei aqui
  if chkGeraArqPorSetor.Checked then begin
    Tempcartoes.First;
    while not Tempcartoes.Eof do begin
        if TempcartoesMarcado.AsString = 'S' then  begin
            //Tempcartoes.First;
            codEmpresOld := TempcartoesEmpresa.AsString;
            setorEmpresOld := Tempcartoessetor.AsString;
            firstLine := True; // Identifica que estamos na primeira linha do grid
            if chMarcaCart.Checked and (MsgSimNao('Aten��o: Todos cart�es selecionados ser�o marcados como j� emitido = S '+sLineBreak+'Confirma?')) then begin
              mens := 'Arquivo gerado com sucesso!';
              while not Tempcartoes.Eof do begin
                 if TempcartoesMarcado.AsString = 'S' then
                 begin
                   //Esta variavel � uma FLAG para saber o c�digo da empresa que est� sendo percorrida no GRID
                    codEmpresTemp := TempcartoesEmpresa.AsString;
                    setorEmpresTemp := Tempcartoessetor.AsString;

                    if (((codEmpresOld <> codEmpresTemp)   and (setorEmpresOld <> setorEmpresTemp))
                       or ((codEmpresOld = codEmpresTemp)  and (setorEmpresOld <> setorEmpresTemp))
                       or ((codEmpresOld <> codEmpresTemp) and (setorEmpresTemp = '')) or firstLine) then begin

                        ResultQuery := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLOTES_CARTAO');
                        QLotesCartao.Open;
                        QLotesCartao.Insert;
                        QLotesCartaoSEQ_LOTE.Value     := ResultQuery;
                        QLotesCartaoEMPRES_ID.Value    := QEmpresasempres_id.Value;
                        QLotesCartaoDATA.AsDateTime    := Date();
                        if Tempcartoessetor.AsString <> '' then
                           QLotesCartaosetor_id.AsInteger := StrToInt(Tempcartoessetor.AsString);
                        QLotesCartaoHORA.Value         := TimeToStr(Time());
                        QLotesCartaoNOME_ARQ.Value     := Filename.FileName;
                        QLotesCartao.Post;
                    end;
                    codEmpresOld := codEmpresTemp;
                    setorEmpresOld := setorEmpresTemp;
                    firstLine := False;


                    //if chMarcaCart.Checked and (MsgSimNao('Aten��o: Todos cart�es selecionados ser�o marcados como j� emitido = S '+sLineBreak+'Confirma?')) then begin
                       //Tempcartoes.First;
                       //while not Tempcartoes.Eof do begin
                          //if  TempcartoesMarcado.AsString = 'S' then begin
                             DMConexao.AdoQry.SQL.Clear;
                             DMConexao.AdoQry.SQL.Add('INSERT INTO CARTOES_LOTES_DETALHE(SEQ_LOTE,CARTAO_ID,EMPRES_ID) VALUES ('+QLotesCartaoSEQ_LOTE.AsString+','+Tempcartoescartao_id.AsString+','+TempcartoesEmpres_id.AsString+')');
                             DMConexao.AdoQry.ExecSQL;
                             QUpdate.SQL.Text := 'update cartoes set jaemitido = ''S'', dtalteracao = '+FormatDataHoraIB(Now)+' , dtemissao = '+FormatDataHoraIB(Now)+' where cartao_id = '+Tempcartoescartao_id.AsString;
                             QUpdate.ExecSQL;
                             DMConexao.GravaLog('FCadCartoes','Ja Emitido','N','S',Operador.Nome,'Altera��o',Tempcartoescartao_id.AsString,Self.Name,'nao obrigatorio');
                          //end;
                          //Tempcartoes.Next;
                       //end;
                       //mens := mens + #13 + 'Os cart�es selecionados foram marcados como j� emitido = S.';
                    //end;
                    //ShowMessage(mens);


                 end;
                 Tempcartoes.Next;
              end;
              mens := mens + #13 + 'Os cart�es selecionados foram marcados como j� emitido = S.';
              ShowMessage(mens);
            end;
            Exit;
        end
        else
          Tempcartoes.Next;
    end;
  //final do meu m�todo
  end
  else begin
    ResultQuery := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SLOTES_CARTAO');

    QLotesCartao.Open;
    QLotesCartao.Insert;
    QLotesCartaoSEQ_LOTE.Value     := ResultQuery;
    QLotesCartaoEMPRES_ID.Value    := QEmpresasempres_id.Value;
    QLotesCartaoDATA.AsDateTime    := Date();
    if lkDepartamento.Text <> '' then
       QLotesCartaosetor_id.AsInteger := lkDepartamento.KeyValue;
    QLotesCartaoHORA.Value         := TimeToStr(Time());
    QLotesCartaoNOME_ARQ.Value     := Filename.FileName;
    QLotesCartao.Post;

    {QLotesCartaoNOME_ARQ.Value  := }

    mens := 'Arquivo gerado com sucesso!';

    if chMarcaCart.Checked and (MsgSimNao('Aten��o: Todos cart�es selecionados ser�o marcados como j� emitido = S '+sLineBreak+'Confirma?')) then begin
       Tempcartoes.First;
       while not Tempcartoes.Eof do begin
          if  TempcartoesMarcado.AsString = 'S' then begin
             DMConexao.AdoQry.SQL.Clear;
             DMConexao.AdoQry.SQL.Add('INSERT INTO CARTOES_LOTES_DETALHE(SEQ_LOTE,CARTAO_ID,EMPRES_ID) VALUES ('+QLotesCartaoSEQ_LOTE.AsString+','+Tempcartoescartao_id.AsString+','+TempcartoesEmpres_id.AsString+')');
             DMConexao.AdoQry.ExecSQL;
             QUpdate.SQL.Text := 'update cartoes set jaemitido = ''S'', dtalteracao = '+FormatDataHoraIB(Now)+' , dtemissao = '+FormatDataHoraIB(Now)+' where cartao_id = '+Tempcartoescartao_id.AsString;
             QUpdate.ExecSQL;
             DMConexao.GravaLog('FCadCartoes','Ja Emitido','N','S',Operador.Nome,'Altera��o',Tempcartoescartao_id.AsString,Self.Name,'nao obrigatorio');
          end;
          Tempcartoes.Next;
       end;
       mens := mens + #13 + 'Os cart�es selecionados foram marcados como j� emitido = S.';
    end;
    ShowMessage(mens);
  end;
end;

procedure TFGeraCartBenBemEstar.Button1Click(Sender: TObject);
begin
  inherited;
  if fnQtdRegistro('N�o existe nem um registro marcado para ser excluido!',TempCartoes) then Abort;
  prVerfEAbreCon(TempCartoes);
  Tempcartoes.First;
  while not Tempcartoes.Eof do begin
     if TempcartoesMarcado.AsString = 'S' then begin
        Tempcartoes.Delete;
        Tempcartoes.Prior;
     end;
     Tempcartoes.Next;
  end;
end;

procedure TFGeraCartBenBemEstar.RGTipoArqClick(Sender: TObject);
begin
  inherited;
  if RGTipoArq.ItemIndex = 0 then begin
     Filename.FileName := ChangeFileExt(Filename.FileName,'.txt');
     ckAbreExcel.Visible:= False;
  end
  else begin
     Filename.FileName := ChangeFileExt(Filename.FileName,'.xls');
     ckAbreExcel.Visible:= True;
  end;
end;

procedure TFGeraCartBenBemEstar.ButPesConvClick(Sender: TObject);
begin
  inherited;
  FPesqConv := TFPesqConv.create(self);
  FPesqConv.Tag := 1;
  FPesqConv.Caption := 'Pesquisa de Cart�es';
  FPesqConv.ShowModal;
  if FPesqConv.ModalResult = mrOk then begin
     EdNomeCart.Text := FPesqConv.QConveniadosnome.AsString;
     AdicionarCartaoById(FPesqConv.QConveniadosCartao_id.AsInteger);
  end;
  FPesqConv.Free;
end;

procedure TFGeraCartBenBemEstar.AdicionarCartaoById(ID:Integer);
begin
   QCartoes.Sql.Clear;
   QCartoes.Sql := PesqSql;
   Qcartoes.Sql.Add(' and cartoes.cartao_id = '+inttostr(id));
   AdicionarCartoesGrade;
end;

procedure TFGeraCartBenBemEstar.AdicionarCartoes;
begin
  QCartoes.Sql.Clear;
  QCartoes.Sql := PesqSql;
  if chempresa.Checked then
    QCartoes.SQL.Add(' and empresas.empres_id = '+DBEmpresa.KeyValue);
  case cbbFiltroCartao.ItemIndex of
    0: QCartoes.SQL.Add(' and coalesce(cartoes.jaemitido,''N'') = ''N'' ');
    1: QCartoes.SQL.Add(' and coalesce(cartoes.jaemitido,''N'') = ''S'' ');
  end;
  if chConvLib.Checked then
    QCartoes.SQL.Add(' and coalesce(conveniados.liberado,''S'') = ''S'' ');
  if chCartLib.Checked then
    QCartoes.SQL.Add(' and coalesce(cartoes.liberado,''S'') = ''S'' ');
  if chSoTitular.Checked then
    QCartoes.SQL.Add(' and coalesce(cartoes.titular,''S'') = ''S'' ');
  if chSoDependentes.Checked then begin
    QCartoes.SQL.Add(' and coalesce(cartoes.titular,''N'') = ''N'' ');
    QCartoes.SQL.Add(' and coalesce(cartoes.CODCARTIMP,'''') <> '''' ');
  end;
  if not ckbTodasdatas.Checked then
  begin
    case rdgDatas.ItemIndex of
      0: QCartoes.SQL.Add(' and cartoes.dtcadastro between '+FormatDataIB(DataIni.Date,True,Inicial)+' and '+FormatDataIB(Datafim.Date,True,Final));
      1: QCartoes.SQL.Add(' and cartoes.dtemissao between '+FormatDataIB(DataIni.Date,True,Inicial)+' and '+FormatDataIB(Datafim.Date,True,Final));
    end;
  end;
  QCartoes.SQL.Add(' and (empresas.EMPRES_ID !=455 or datediff(year,DATA_NASC,getdate()) >= 18 or (empresas.EMPRES_ID = 455 AND cartoes.JAEMITIDO = ''N'' and VIA = 2))');
  QCartoes.SQL.Add(' GROUP BY empresas.EMPRES_ID, CONVENIADOS.SETOR, CARTOES.NOME, cartoes.cartao_id, CARTOES.TITULAR,CONVENIADOS.CHAPA, cartoes.codcartimp,CONVENIADOS.TITULAR,CONVENIADOS.EMPRES_ID,');
  QCartoes.SQL.Add(' CONVENIADOS.CIDADE,CONVENIADOS.SETOR_ID,CARTOES.DTCADASTRO, empresas.NOMECARTAO,MODELOS_CARTOES.DESCRICAO,CARTOES.CONV_ID ORDER BY CONVENIADOS.setor,CARTOES.NOME');
  QCartoes.SQL.Text;
  AdicionarCartoesGrade;
end;

function  TFGeraCartBenBemEstar.PesqSql:TStrings;
var
  hasDepartamento : Boolean;
begin
   Result := TStringList.Create;
   Result.Add(' Select cartoes.nome, cartoes.cartao_id, cartoes.titular as cartaotitu, conveniados.chapa, cartoes.codcartimp,');
   Result.Add(' conveniados.titular,conveniados.empres_id, conveniados.cidade, conveniados.setor, conveniados.setor_id, cartoes.dtcadastro,');
   if CkUsarCodImp.Checked then
      Result.Add(' codcartimp as cartao, ')
   else
      Result.Add(' cast(cartoes.codigo as varchar(9))+(select str from formatint(2,cartoes.digito)) as cartao, ');
   Result.Add(' empresas.nomecartao as empresa,MODELOS_CARTOES.DESCRICAO modelo_cartoes, cartoes.conv_id from cartoes ');
   Result.Add(' join   conveniados on conveniados.conv_id = cartoes.conv_id and coalesce(conveniados.apagado,''N'') <> ''S'' ');
   Result.Add(' join   empresas    on empresas.empres_id  = conveniados.empres_id ');
   Result.Add(' join  MODELOS_CARTOES on EMPRESAS.MOD_CART_ID = MODELOS_CARTOES.MOD_CART_ID ');
   Result.Add(' where coalesce(cartoes.apagado,''N'') <> ''S'' ');
   //Result.Add(' and conveniados.setor in')
   if flag = True then  //FLAG � UMA VARI�VEL QUE INFORMA QUE A EMPRESA POSSUI DEPARTAMENTOS 
   begin
     if verificaDepartamento then
     begin
       if lkDepartamento.KeyValue <> null then
         Result.Add(' and conveniados.setor = ' + IntToStr(lkDepartamento.KeyValue));
     end;
   end;
end;


procedure TFGeraCartBenBemEstar.AdicionarCartoesGrade;
begin
Screen.Cursor := crHourGlass;
QCartoes.Open;
if not QCartoes.IsEmpty then begin
   if Tempcartoes.IsEmpty then
      Tempcartoes.Open;
   if not Tempcartoes.IsEmpty then
     //if MsgSimNao('Deseja limpar a grade antes de incluir?') then
        Tempcartoes.EmptyTable;

   QCartoes.First;
   while not QCartoes.Eof do begin
      Tempcartoes.Append;
      Tempcartoescartao_id.AsInteger           := QCartoesCARTAO_ID.AsInteger;
      TempcartoesCARTAO.AsString               := QCartoesCARTAO.AsString;
      TempcartoesEmpresa.AsString              := QCartoesEMPRESA.AsString;
      TempcartoesNome.AsString                 := QCartoesNOME.AsString;
      TempcartoesTitular.AsString              := QCartoesTITULAR.AsString;
      TempcartoesCidade.AsString               := QCartoesCIDADE.AsString;
      TempcartoesCARTAOTITU.AsString           := QCartoesCARTAOTITU.AsString;
      Tempcartoesconv_id.AsInteger             := QCartoesCONV_ID.AsInteger;
      TempcartoesDATACADASTRO.AsDateTime       := QCartoesDTCADASTRO.AsDateTime;
      TempcartoesCHAPA.AsString                := QCartoesCHAPA.AsString;
      TempcartoesCODCARTIMP.AsString           := QCartoesCODCARTIMP.AsString;
      TempcartoesEmpres_id.AsString            := QCartoesempres_id.AsString;
      Tempcartoesmodelo_cartoes.AsString       := QCartoesmodelo_cartoes.AsString;
      Tempcartoessetor.AsString                := QCartoessetor.AsString;
      Tempcartoessetor_id.AsInteger            := QCartoessetor_id.AsInteger;  
      Tempcartoes.Post;
      QCartoes.Next;
   end;
   Tempcartoes.First;
end
else begin
   Tempcartoes.Close;
   ShowMessage('Cart�o n�o encontrado. ');
end;
QCartoes.Close;
Screen.Cursor := crDefault;
Barra.SimpleText := IntToStr(Tempcartoes.RecordCount)+' cart�es .';
end;

procedure TFGeraCartBenBemEstar.ButBuscaClick(Sender: TObject);
begin
  inherited;
  AdicionarCartoes;
end;


function TFGeraCartBenBemEstar.meusdocpath() : string;
var
pidl : PItemIDList;
path : array [0..MAX_PATH] of char;
begin
  SHGetSpecialFolderLocation(0, CSIDL_PERSONAL, pidl);
  SHGetPathFromIDList(pidl, path);
  Result := String(path);
end;

procedure TFGeraCartBenBemEstar.GerarArquivoTextoComQuebraPorSetor;
var  mens,setorEmpresTemp,setorEmpresOld,codEmpresTemp,codEmpresOld : string;
     arq,arq_old : TStringList;
     linha : string;
     caminho : String;
     codcartimp,nomeArq,nomeArqOld : String;
     Path: array[0..MAX_PATH] of Char;
     a,c : integer;
     firstLine : Boolean;
begin
  try
    Tempcartoes.First;
    while not Tempcartoes.Eof do begin
      if TempcartoesMarcado.AsString = 'S' then  begin
        //Tempcartoes.First;
        codEmpresOld := TempcartoesEmpresa.AsString;
        setorEmpresOld := Tempcartoessetor.AsString;
        firstLine := True; // Identifica que estamos na primeira linha do grid
        arq := TStringList.Create;
        while not Tempcartoes.Eof do begin
           if TempcartoesMarcado.AsString = 'S' then
           begin
              codEmpresTemp := TempcartoesEmpresa.AsString;
              setorEmpresTemp := Tempcartoessetor.AsString;

              //nomeArq := TStringList.Create;
              if (((codEmpresOld <> codEmpresTemp)   and (setorEmpresOld <> setorEmpresTemp))
                 or ((codEmpresOld = codEmpresTemp)  and (setorEmpresOld <> setorEmpresTemp))
                 or ((codEmpresOld <> codEmpresTemp) and (setorEmpresTemp = '')) or firstLine) then begin

                if firstLine = False then begin
                     caminho := meusdocpath;
                     caminho := caminho + '\Cartoes-' + nomeArq + '.txt';
                     arq_old.SaveToFile(caminho);
                     arq_old.Free;
                     arq.Free;
                     arq := TStringList.Create;
                     arq := TStringList.Create;
                end;
                nomeArq := Tempcartoessetor.AsString;
                arq_old := TStringList.Create;
                //CRIA O CABE�ALHO AO INICIAR UM ARQUIVO, O ARQUIVO � CRIADO A CADA SETOR E EMPRESA DIFERENTE
                if cbModelo.ItemIndex = 0 then
                  linha := '"nome";"empresa";"matrseq";"matricula";"barras";"tarja"'
                else if cbModelo.ItemIndex = 1 then
                  linha := '"dependente";"nome";"empresa";"matrseq";"matricula";"barras";"tarja"'
                else if cbModelo.ItemIndex = 2 then
                  linha := '"nome";"empresa";"matricula";"barras";"tarja"'
                else if cbModelo.ItemIndex = 3 then
                  linha := '"dependente";"nome";"empresa";"matricula";"barras";"tarja"';
                arq.Add(linha);
                arq_old.Add(linha);
                //FIM DA CRIA��O DO CABE�ALHO
              end;
              //Esta variavel � uma FLAG para saber o c�digo da empresa que est� sendo percorrida no GRID
              //codEmpresTemp   := TempcartoesEmpresa.AsString;
              //setorEmpresTemp := Tempcartoessetor.AsString;
              codcartimp := '';
              c := 1;
              a := 4;
              if (chImpCartEsp.Checked) then begin
                while(c < Length(TempcartoesCODCARTIMP.AsString)) do begin
                  codcartimp := codcartimp + ' ' + Copy(TempcartoesCODCARTIMP.AsString,c,4);
                  System.Inc(c,4);
                end;
                codcartimp := Trim(codcartimp);
                c :=  floor(c / 4);
              end else begin
                codcartimp := TempcartoesCODCARTIMP.AsString;
              end;
              if TempcartoesMarcado.AsString = 'S' then begin
              if cbModelo.ItemIndex = 0 then begin// DB Titular
                  linha := '"'+copy(TempcartoesNome.AsString,1,30)+'"';                                 //nome        NOME
                  linha := linha + ';"' + copy(TempcartoesEmpresa.AsString,1,17)+'"';                   //empresa     EMPRESA
                  linha := linha + ';"' + copy(TempcartoesCHAPA.AsString,1,6)+'"';                      //matrseq     CHAPA
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //matricula   CODCARTIMP
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //barras      CODCARTIMP
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //tarja       CODCARTIMP
                  arq.Add(linha);
                  arq_old.Add(linha);
                end else if cbModelo.ItemIndex = 1 then begin// DB Dependente
                  linha := copy(TempcartoesNome.AsString,1,30)+'"';                                     //dependente  DEPENDENTE
                  linha := linha + ';"' + copy(TempcartoesTitular.AsString,1,30)+'"';                   //nome        NOME
                  linha := linha + ';"' + copy(TempcartoesEmpresa.AsString,1,30)+'"';                   //empresa     EMPRESA
                  linha := linha + ';"' + copy(TempcartoesCHAPA.AsString,1,6)+'"';                      //matrseq     CHAPA
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //matricula   CODCARTIMP
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //barras      CODCARTIMP
                  linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //tarja       CODCARTIMP
                  arq.Add(linha);
                  arq_old.Add(linha);
                end else if cbModelo.ItemIndex = 2 then begin// DB Titular
                  linha := '"'+TempcartoesTitular.AsString+'"';                                   //nome        NOME
                  linha := linha + ';"' + TempcartoesEmpresa.AsString+'"';                    //empresa     EMPRESA
                  linha := linha + ';"' + codcartimp+'"';                 //matricula   CODCARTIMP
                  linha := linha + ';"' + codcartimp+'"';                 //barras      CODCARTIMP
                  linha := linha + ';"' + codcartimp+'"';                 //tarja       CODCARTIMP
                  arq.Add(linha);
                  arq_old.Add(linha);
                end else if cbModelo.ItemIndex = 3 then begin// DB Dependente
                  linha := '"'+TempcartoesNome.AsString+'"';                                     //dependente  DEPENDENTE
                  linha := linha + ';"' + TempcartoesTitular.AsString+'"';                   //nome        NOME
                  linha := linha + ';"' + TempcartoesEmpresa.AsString+'"';                   //empresa     EMPRESA
                  linha := linha + ';"' + codcartimp+'"';                 //matricula   CODCARTIMP
                  linha := linha + ';"' + codcartimp+'"';                 //barras      CODCARTIMP
                  linha := linha + ';"' + codcartimp+'"';                 //tarja       CODCARTIMP
                  arq.Add(linha);
                  arq_old.Add(linha);
                end;
              end;
              codEmpresOld := codEmpresTemp;
              setorEmpresOld := setorEmpresTemp;
              firstLine := False;
           end;
           Tempcartoes.Next;
        end;
        caminho := meusdocpath;
        caminho := caminho + '\Cartoes-' + nomeArq + '.txt';
        arq_old.SaveToFile(caminho);
        arq_old.Free;
        arq.Free;
        arq := TStringList.Create;
        arq := TStringList.Create;
        Exit;
      end
      else
        Tempcartoes.Next;
    end;

    except on E:Exception do
     msgerro('Falha ao gerar arquivo texto, erro: '+e.Message);
  end;
end;

procedure TFGeraCartBenBemEstar.GerarArquivoTexto;
var arq : TStringList;
  linha : string;
  codcartimp : String;
  a,c : integer;
begin
  try
    arq := TStringList.Create;
    if cbModelo.ItemIndex = 0 then
      linha := '"nome";"empresa";"matrseq";"matricula";"barras";"tarja"'
    else if cbModelo.ItemIndex = 1 then
      linha := '"dependente";"nome";"empresa";"matrseq";"matricula";"barras";"tarja"'
    else if cbModelo.ItemIndex = 2 then
      linha := '"nome";"empresa";"matricula";"barras";"tarja"'
    else if cbModelo.ItemIndex = 3 then
      linha := '"dependente";"nome";"empresa";"matricula";"barras";"tarja"';
    arq.Add(linha);
    Tempcartoes.First;
    while not Tempcartoes.Eof do begin
      codcartimp := '';
      c := 1;
      a := 4;
      if (chImpCartEsp.Checked) then begin
        while(c < Length(TempcartoesCODCARTIMP.AsString)) do begin
          codcartimp := codcartimp + ' ' + Copy(TempcartoesCODCARTIMP.AsString,c,4);
          System.Inc(c,4);
        end;
        codcartimp := Trim(codcartimp);
        c :=  floor(c / 4);
      end else begin
        codcartimp := TempcartoesCODCARTIMP.AsString;
      end;
      if TempcartoesMarcado.AsString = 'S' then begin
      if cbModelo.ItemIndex = 0 then begin// DB Titular
          linha := '"'+copy(TempcartoesNome.AsString,1,30)+'"';                                 //nome        NOME
          linha := linha + ';"' + copy(TempcartoesEmpresa.AsString,1,17)+'"';                   //empresa     EMPRESA
          linha := linha + ';"' + copy(TempcartoesCHAPA.AsString,1,6)+'"';                      //matrseq     CHAPA
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //matricula   CODCARTIMP
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //barras      CODCARTIMP
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //tarja       CODCARTIMP
          arq.Add(linha);
        end else if cbModelo.ItemIndex = 1 then begin// DB Dependente
          linha := copy(TempcartoesNome.AsString,1,30)+'"';                                     //dependente  DEPENDENTE
          linha := linha + ';"' + copy(TempcartoesTitular.AsString,1,30)+'"';                   //nome        NOME
          linha := linha + ';"' + copy(TempcartoesEmpresa.AsString,1,30)+'"';                   //empresa     EMPRESA
          linha := linha + ';"' + copy(TempcartoesCHAPA.AsString,1,6)+'"';                      //matrseq     CHAPA
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //matricula   CODCARTIMP
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //barras      CODCARTIMP
          linha := linha + ';"' + copy(codcartimp,1,16+c)+'"';                 //tarja       CODCARTIMP
          arq.Add(linha);
        end else if cbModelo.ItemIndex = 2 then begin// DB Titular
          linha := '"'+TempcartoesTitular.AsString+'"';                                   //nome        NOME
          linha := linha + ';"' + TempcartoesEmpresa.AsString+'"';                    //empresa     EMPRESA
          linha := linha + ';"' + codcartimp+'"';                 //matricula   CODCARTIMP
          linha := linha + ';"' + codcartimp+'"';                 //barras      CODCARTIMP
          linha := linha + ';"' + codcartimp+'"';                 //tarja       CODCARTIMP
          arq.Add(linha);
        end else if cbModelo.ItemIndex = 3 then begin// DB Dependente
          linha := '"'+TempcartoesNome.AsString+'"';                                     //dependente  DEPENDENTE
          linha := linha + ';"' + TempcartoesTitular.AsString+'"';                   //nome        NOME
          linha := linha + ';"' + TempcartoesEmpresa.AsString+'"';                   //empresa     EMPRESA
          linha := linha + ';"' + codcartimp+'"';                 //matricula   CODCARTIMP
          linha := linha + ';"' + codcartimp+'"';                 //barras      CODCARTIMP
          linha := linha + ';"' + codcartimp+'"';                 //tarja       CODCARTIMP
          arq.Add(linha);
        end;
      end;
      Tempcartoes.Next;
    end;
    arq.SaveToFile(Filename.FileName);
    arq.Free;
  except on E:Exception do
     msgerro('Falha ao gerar arquivo texto, erro: '+e.Message);
  end;
end;

procedure TFGeraCartBenBemEstar.TratarCamposVisiveis;
var i : integer;
begin
    for i := 0 to Tempcartoes.FieldCount - 1 do begin
        Tempcartoes.Fields[i].Visible := False;
    end;
    for i := 0 to DBGrid1.FieldCount - 1 do begin
        if DBGrid1.Columns[i].Visible then
           Tempcartoes.FieldByName(DBGrid1.Fields[i].FieldName).Visible := true;
    end;
end;

procedure TFGeraCartBenBemEstar.SetFieldIndex;
var i : Integer;
begin
   for i := 0 to DBGrid1.Columns.Count-1 do begin
       DBGrid1.Columns[i].Field.Index        := DBGrid1.Columns[i].Index;
       DBGrid1.Columns[i].Field.DisplayLabel := DBGrid1.Columns[i].Title.Caption;
   end;
end;

procedure TFGeraCartBenBemEstar.FilenameExit(Sender: TObject);
begin
  inherited;
  if RGTipoArq.ItemIndex = 0 then
    Filename.FileName := ChangeFileExt(Filename.FileName,'.txt')
  else
    Filename.FileName := ChangeFileExt(Filename.FileName,'.xls');
end;

procedure TFGeraCartBenBemEstar.ckbTodasdatasClick(Sender: TObject);
begin
  inherited;
  if ckbTodasdatas.Checked then
  begin
    rdgDatas.Enabled:= False;
    DataIni.Enabled:= False;
    Datafim.Enabled:= False;
    Label3.Enabled:= False;
    Label4.Enabled:= False;
  end
  else
  begin
    rdgDatas.Enabled:= True;
    DataIni.Enabled:= True;
    Datafim.Enabled:= True;
    Label3.Enabled:= True;
    Label4.Enabled:= True;
  end;
end;

procedure TFGeraCartBenBemEstar.chSoTitularClick(Sender: TObject);
begin
  inherited;
   if TCheckBox(Sender).Checked then begin
    chSoDependentes.Checked := False;
  end;
end;

procedure TFGeraCartBenBemEstar.chSoDependentesClick(Sender: TObject);
begin
  inherited;
  if TCheckBox(Sender).Checked then begin
    chSoTitular.Checked := False;
  end;
end;

procedure TFGeraCartBenBemEstar.DSEmpresaDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  qModelosCartoes.Close;
  qModelosCartoes.Parameters[0].Value := QEmpresasMOD_CART_ID.Value;
  qModelosCartoes.Open;
end;

procedure TFGeraCartBenBemEstar.DBEmpresaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_TAB) or (Key = VK_RETURN) then
  begin
    QDepart.Close;
    QDepart.Parameters[0].Value := DBEmpresa.KeyValue;
    QDepart.Open;
    if verificaDepartamento then
       lkDepartamento.Enabled := True;
  end;
end;

procedure TFGeraCartBenBemEstar.lkDepartamentoEnter(Sender: TObject);
begin
  inherited;
  //if not verificaDepartamento then
  //begin
  QDepart.Close;
  QDepart.Parameters[0].Value := DBEmpresa.KeyValue;
  QDepart.Open;
  flag := True;
  if verificaDepartamento then
     lkDepartamento.Enabled := True;
  //end;
end;

procedure TFGeraCartBenBemEstar.DBEmpresaEnter(Sender: TObject);
begin
  inherited;
  flag := False;
end;

end.
