unit uSeleciona;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, RxMemDS, Grids, DBGrids, {JvDBCtrl,} StdCtrls, {JvDBComb,}
  Buttons, ExtCtrls, {ZAbstractRODataset, ZDataset,} JvExDBGrids, JvDBGrid,
  ADODB;

type
  TfSeleciona = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    Panel6: TPanel;
    Panel5: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    edTexto: TEdit;
    JvDBGrid1: TJvDBGrid;
    Panel7: TPanel;
    Panel8: TPanel;
    Label2: TLabel;
    JvDBGrid2: TJvDBGrid;
    qTemp: TRxMemoryData;
    qTempSel: TRxMemoryData;
    dsTempSel: TDataSource;
    dsTemp: TDataSource;
    qTempCODIGO: TIntegerField;
    qTempNOME: TStringField;
    qTempSelCODIGO: TIntegerField;
    qTempSelNOME: TStringField;
    cbbBuscaPor: TComboBox;
    Q: TADOQuery;
    procedure edTextoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid2DblClick(Sender: TObject);
  private
    { Private declarations }
    Titulo, Tabela, Codigo, Nome, IDs: String;
    function getTitulo: String;
    function getTabela: String;
    function getCodigo: String;
    function getNome: String;
    procedure PegaDados(pTabela, pCodigo, pNome: String; pTemApag: Boolean = False);
  public
    { Public declarations }
    procedure setTitulo(pTexto: String);
    procedure setIDs(pTexto: String);
    procedure setTabela(pTexto: String);
    procedure setCodigo(pTexto: String);
    procedure setNome(pTexto: String);
    function getIDs: String;
  end;

var
  fSeleciona: TfSeleciona;

implementation

uses DM;

{$R *.dfm}

procedure TfSeleciona.edTextoChange(Sender: TObject);
begin
  case cbbBuscaPor.ItemIndex of
    0: qTemp.Locate('CODIGO',edTexto.Text,[loPartialKey]);
    1: qTemp.Locate('NOME',edTexto.Text,[loPartialKey]);
  end;
end;

function TfSeleciona.getTitulo: String;
begin
  Result:= Titulo;
end;

procedure TfSeleciona.PegaDados(pTabela, pCodigo, pNome: String; pTemApag: Boolean);
begin
  Q.Close;
  Q.SQL.Text:= ' select ' +pCodigo + ' as codigo, ' + pNome + ' as nome from ' + pTabela;
  Q.SQL.Add(' where ' +pCodigo + ' = ' +pCodigo + ' ');
  if pTemApag then
    Q.SQL.Add(' and apagado <> ''S'' ');
  if IDs <> '' then
    Q.SQL.Add(' and ' +pCodigo + ' not in (' +IDs + ') ');
  Q.Open;
  Q.First;
  qTemp.Open;
  qTemp.EmptyTable;
  while not Q.Eof do
  begin
    qTemp.Append;
    qTemp.FieldByName('CODIGO').AsInteger:= Q.FieldByName('CODIGO').AsInteger;
    qTemp.FieldByName('NOME').AsString:= Q.FieldByName('NOME').AsString;
    qTemp.Post;
    Q.Next;
  end;
  qTemp.First;
  Q.Close;
  if IDs = '' then
  begin
    qTempSel.Open;
    qTempSel.EmptyTable;
  end
  else
  begin
    Q.SQL.Text:= ' select ' +pCodigo + ' as codigo, ' + pNome + ' as nome from ' + pTabela;
    Q.SQL.Add(' where ' +pCodigo + ' = ' +pCodigo + ' ');
    if pTemApag then
      Q.SQL.Add(' and apagado <> ''S'' ');
    if IDs <> '' then
      Q.SQL.Add(' and ' +pCodigo + ' in (' +IDs + ') ');
    Q.Open;
    Q.First;
    qTempSel.Open;
    qTempSel.EmptyTable;
    while not Q.Eof do
    begin
      qTempSel.Append;
      qTempSel.FieldByName('CODIGO').AsInteger:= Q.FieldByName('CODIGO').AsInteger;
      qTempSel.FieldByName('NOME').AsString:= Q.FieldByName('NOME').AsString;
      qTempSel.Post;
      Q.Next;
    end;
    qTempSel.First;
    Q.Close;
  end;
end;

procedure TfSeleciona.setTitulo(pTexto: String);
begin
  Titulo:= pTexto;
end;

function TfSeleciona.getCodigo: String;
begin
  Result:= Codigo;
end;

function TfSeleciona.getNome: String;
begin
  Result:= Nome;
end;

function TfSeleciona.getTabela: String;
begin
  Result:= Tabela;
end;

procedure TfSeleciona.setCodigo(pTexto: String);
begin
  Codigo:= pTexto;
end;

procedure TfSeleciona.setNome(pTexto: String);
begin
  Nome:= pTexto;
end;

procedure TfSeleciona.setTabela(pTexto: String);
begin
  Tabela:= pTexto;
end;

procedure TfSeleciona.FormShow(Sender: TObject);
begin
  Self.Caption:= getTitulo;
  PegaDados(getTabela, getCodigo, getNome);

end;

procedure TfSeleciona.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qTemp.Close;
end;

procedure TfSeleciona.FormCreate(Sender: TObject);
begin
  cbbBuscaPor.ItemIndex:= 0;
end;

procedure TfSeleciona.SpeedButton1Click(Sender: TObject);
begin
  qTempSel.Append;
  qTempSel.FieldByName('CODIGO').AsInteger:= qTemp.FieldByName('CODIGO').AsInteger;
  qTempSel.FieldByName('NOME').AsString:=    qTemp.FieldByName('NOME').AsString;
  qTempSel.Post;
  qTemp.Delete;
end;

procedure TfSeleciona.SpeedButton2Click(Sender: TObject);
begin
  qTemp.Append;
  qTemp.FieldByName('CODIGO').AsInteger:= qTempSel.FieldByName('CODIGO').AsInteger;
  qTemp.FieldByName('NOME').AsString:=    qTempSel.FieldByName('NOME').AsString;
  qTemp.Post;
  qTempSel.Delete;
end;

procedure TfSeleciona.SpeedButton3Click(Sender: TObject);
begin
  qTemp.First;
  while not qTemp.Eof do
  begin
    qTempSel.Append;
    qTempSel.FieldByName('CODIGO').AsInteger:= qTemp.FieldByName('CODIGO').AsInteger;
    qTempSel.FieldByName('NOME').AsString:=    qTemp.FieldByName('NOME').AsString;
    qTempSel.Post;
    qTemp.Next;
  end;
  qTemp.EmptyTable;
end;

procedure TfSeleciona.SpeedButton4Click(Sender: TObject);
begin
  qTempSel.First;
  while not qTempSel.Eof do
  begin
    qTemp.Append;
    qTemp.FieldByName('CODIGO').AsInteger:= qTempSel.FieldByName('CODIGO').AsInteger;
    qTemp.FieldByName('NOME').AsString:=    qTempSel.FieldByName('NOME').AsString;
    qTemp.Post;
    qTempSel.Next;
  end;
  qTempSel.EmptyTable;
end;

function TfSeleciona.getIDs: String;
begin
  IDs:= DMConexao.RetornaIDs(qTempSel,'CODIGO');
  Result:= IDs;
end;

procedure TfSeleciona.setIDs(pTexto: String);
begin
  IDs:= pTexto;
end;

procedure TfSeleciona.JvDBGrid1DblClick(Sender: TObject);
begin
  SpeedButton1.Click;
end;

procedure TfSeleciona.JvDBGrid2DblClick(Sender: TObject);
begin
  SpeedButton2.Click;
end;

end.
