unit UCadTaxas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DBCtrls, Menus, DB, ZDataset, ZAbstractRODataset,
  ZAbstractDataset, Buttons, StdCtrls, Mask, JvToolEdit, ComCtrls, Grids,
  DBGrids, {JvDBCtrl,} ExtCtrls, JvExMask, JvExDBGrids, JvDBGrid, ADODB;

type
  TFCadTaxas = class(TFCad)
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QCadastroTAXA_ID: TIntegerField;
    QCadastroDESCRICAO: TStringField;
    QCadastroVALOR: TFloatField;
    QCadastroOPERADOR: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroOPERCADASTRO: TStringField;
    TabSheet1: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    btnGrava: TButton;
    btnCancel: TButton;
    GridCred: TJvDBGrid;
    DSCred: TDataSource;
    Panel6: TPanel;
    Label6: TLabel;
    Button1: TButton;
    UsaCondicao: TDBCheckBox;
    QCadastroVALORREAL: TStringField;
    QCadastroUSACONDICAO: TStringField;
    QCadastroMAIORQUE: TFloatField;
    DBEdit6: TDBEdit;
    rgTipoValor: TDBRadioGroup;
    QCadastroLIQUIDO: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCred: TADOQuery;
    QCredcred_id: TIntegerField;
    QCrednome: TStringField;
    QCreddescontar: TStringField;
    procedure ButIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButEditClick(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure TabSheet1Show(Sender: TObject);
    procedure DSCredStateChange(Sender: TObject);
    procedure btnGravaClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure QCredAfterPost(DataSet: TDataSet);
    procedure QCredAfterInsert(DataSet: TDataSet);
    procedure GridCredTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure Button1Click(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure ButGravaClick(Sender: TObject);
    procedure UsaCondicaoClick(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure rgTipoValorClick(Sender: TObject);
    procedure TabSheet1Hide(Sender: TObject);
    procedure DBEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure QCredBeforePost(DataSet: TDataSet);
  private
    procedure AlterarTodos(SN: Char);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadTaxas: TFCadTaxas;
  tDescontar, fLiberado : string;
  SavePlace : TBookmark;

implementation

uses DM, USelTipoImp, cartao_util, Math, UMenu, UValidacao;

{$R *.dfm}

procedure TFCadTaxas.ButIncluiClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
  QCadastroAPAGADO.AsString := 'N';
  rgTipoValor.ItemIndex:= 0;
  UsaCondicao.Checked:= false;
end;

procedure TFCadTaxas.FormCreate(Sender: TObject);
begin
  chavepri := 'taxa_id';
  detalhe  := 'Taxa ID';
  QCadastro.Open;
  inherited;
end;

procedure TFCadTaxas.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add('Select * from taxas where coalesce(apagado,''N'') <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and taxa_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and descricao like ''%'+EdNome.Text+'%'' ');
  QCadastro.Sql.Add(' order by descricao ');
  QCadastro.Open;
  If not QCadastro.IsEmpty then DBGrid1.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
end;

procedure TFCadTaxas.ButEditClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TFCadTaxas.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Taxa: '+QCadastroDESCRICAO.AsString;
end;

procedure TFCadTaxas.QCadastroBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;
  //if QCadastro.state = dsinsert then begin
   // DMConexao.AdoQry.SQL.Clear;
  //  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXA_ID AS TAXA_ID');
  //  DMConexao.AdoQry.Open;
   // QCadastroTAXA_ID.AsInteger:= DMConexao.AdoQry.FieldByName('TAXA_ID').AsInteger;
  //  QCadastroDTCADASTRO.AsDateTime:= date;
   // QCadastroAPAGADO.AsString := 'N';
  //end;
end;

procedure TFCadTaxas.TabSheet1Show(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then
  begin
    QCred.Close;
    QCred.Parameters.ParamByName('taxas').Value := QCadastroTAXA_ID.AsInteger;
    QCred.Open;
  end;
end;

procedure TFCadTaxas.DSCredStateChange(Sender: TObject);
begin
  inherited;
  if (btnCancel = ActiveControl) or (btnGrava = ActiveControl) then
     GridCred.SetFocus;
  btnCancel.Enabled  := QCred.State in [dsEdit,dsInsert];
  btnGrava.Enabled   := QCred.State in [dsEdit,dsInsert];
end;

procedure TFCadTaxas.btnGravaClick(Sender: TObject);
begin
  inherited;
  if QCred.State in [dsInsert,dsEdit] then
     QCred.Post;
end;

procedure TFCadTaxas.btnCancelClick(Sender: TObject);
begin
  inherited;
  if QCred.State in [dsInsert,dsEdit] then
     QCred.Cancel;
end;

procedure TFCadTaxas.QCredAfterPost(DataSet: TDataSet);
var Sql, SqlLog: string;
begin
  inherited;
  Sql:= '';
  SqlLog:= '';
  if ({QCredDESCONTAR.AsString}tDescontar = 'S') or ({QCredDESCONTAR.AsString}tDescontar = 's') then
  begin
    if QCredDESCONTAR.OldValue = 'N' then
    begin
      sql :=  'insert into rel_taxa_cred(CRED_ID,TAXA_ID) ';
      sql := sql + ' values('+QCredCRED_ID.AsString+','+QCadastroTAXA_ID.AsString+')';

      DMConexao.GravaLog('FCadTaxas','Fornec. a Descontar','N','S',Operador.Nome,'Altera��o',QCadastroTAXA_ID.AsString,'Taxa ID: '+QCadastroTAXA_ID.AsString,Self.Name);
    end;
  end
  else if ({QCredDESCONTAR.AsString}tDescontar = 'N') or ({QCredDESCONTAR.AsString}tDescontar = 'n') then
  begin
    if QCredDESCONTAR.OldValue = 'S' then
    begin
      sql :=  'delete from rel_taxa_cred ';
      sql :=  sql + ' where CRED_ID = '+QCredCRED_ID.AsString+ ' and TAXA_ID = '+QCadastroTAXA_ID.AsString;

      DMConexao.GravaLog('FCadTaxas','Fornec. a Descontar','S','N',Operador.Nome,'Altera��o',QCadastroTAXA_ID.AsString,'Taxa ID: '+QCadastroTAXA_ID.AsString,Self.Name);
    end;
  end;
  if Sql <> '' then
  begin
    DMConexao.ExecuteSql(sql);
  end;
  QCred.Requery;
  QCred.GotoBookmark(SavePlace);
  QCred.FreeBookmark(SavePlace);
  tDescontar := '';
end;

procedure TFCadTaxas.QCredAfterInsert(DataSet: TDataSet);
begin
  inherited;
  if (ActiveControl = GridCred) then
     QCred.Cancel;
end;

procedure TFCadTaxas.GridCredTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
  try
    if Pos(Field.FieldName,QCred.Sort) > 0 then begin
       if Pos(' DESC',QCred.Sort) > 0 then QCred.Sort := Field.FieldName
                                                  else QCred.Sort := Field.FieldName+' DESC';
    end
    else QCred.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCadTaxas.Button1Click(Sender: TObject);
begin
  inherited;
  if fnVerfCampoVazio('Selecione uma Taxa para conseguir Alterar o registro', QCadastroTAXA_ID) then abort;
  if QCadastroTAXA_ID.AsString <> '' then
    case TFSelTipoImp.AbrirJanela(['Alterar todos para S','Alterar todos para N'],0,'Selecione a Op��o','Selecione o tipo de altera��o') of
       0 : AlterarTodos('S');
       1 : AlterarTodos('N');
  end;
end;

procedure TFCadTaxas.AlterarTodos(SN:Char);
var sql: string;
begin
  if SN = 'S' then begin
    if MsgSimNao('Deseja marcar a cobran�a dessa taxa para todos os fornecedores!') then begin
       QCred.First;
       while not QCred.Eof do begin
          if (QCredDESCONTAR.AsString = 'N') or (QCredDESCONTAR.AsString = 'n') then begin
             sql :=  'insert into rel_taxa_cred(CRED_ID,TAXA_ID) ';
             sql := sql + ' values('+QCredCRED_ID.AsString+','+QCadastroTAXA_ID.AsString+')';
             DMConexao.ExecuteSql(sql);
          end;
          QCred.Next;
       end;
       QCred.Requery;
       MsgInf('Altera��es efetuadas com sucesso!');
    end
  end
  else begin
     if MsgSimNao('Deseja desmarcar a cobran�a dessa taxa para todos os fornecedores!') then begin
        sql :=  'delete from rel_taxa_cred where TAXA_ID = '+QCadastroTAXA_ID.AsString;
        DMConexao.ExecuteSql(sql);
        QCred.Requery;
        MsgInf('Altera��es efetuadas com sucesso!');
     end;
  end;
end;

procedure TFCadTaxas.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR STAXA_ID AS TAXA_ID');
  DMConexao.AdoQry.Open;
  QCadastroTAXA_ID.AsInteger:= DMConexao.AdoQry.FieldByName('TAXA_ID').AsInteger;
  case rgTipoValor.ItemIndex of
    0: QCadastroVALORREAL.AsString:= 'S';
    1: QCadastroVALORREAL.AsString:= 'N';
  end;
end;

procedure TFCadTaxas.ButGravaClick(Sender: TObject);
begin
  if DBEdit2.Text = '' then begin
     MsgInf('Favor insira a descri��o da taxa!');
     Exit;
     DBEdit2.SetFocus;
  end;
  if DBEdit3.Text = '' then begin
     MsgInf('Favor insira o valor da taxa!');
     Exit;
     DBEdit3.SetFocus;
  end;
  if UsaCondicao.Checked then begin
     if DBEdit6.Text = '' then begin
        MsgInf('Favor insira o valor da taxa!');
        Exit;
        DBEdit6.SetFocus;
     end;
  end;
  if rgTipoValor.ItemIndex = 1 then begin
     if StrToCurr(DBEdit3.Text) > 100 then begin
        MsgInf('Valor de porcentagem deve ser at� no m�ximo 100!');
        Exit;
        DBEdit3.SetFocus;
     end;
  end;

  inherited;
end;

procedure TFCadTaxas.UsaCondicaoClick(Sender: TObject);
begin
  inherited;
  if UsaCondicao.Checked then
     DBEdit6.Enabled:= True
  else
     DBEdit6.Enabled:= False;
end;

procedure TFCadTaxas.TabFichaShow(Sender: TObject);
begin
  inherited;
  if QCadastroUSACONDICAO.AsString = 'S' then
     UsaCondicao.Checked:= True
  else
     UsaCondicao.Checked:= False;
  if QCadastroVALORREAL.AsString = 'N' then
     rgTipoValor.ItemIndex := 1
  else
     rgTipoValor.ItemIndex := 0;
end;

procedure TFCadTaxas.rgTipoValorClick(Sender: TObject);
begin
  inherited;
{  QCadastro.Edit;
  case rgTipoValor.ItemIndex of
    0: QCadastroVALORREAL.AsString := 'S';
    1: QCadastroVALORREAL.AsString := 'N';
  end;  }
end;

procedure TFCadTaxas.TabSheet1Hide(Sender: TObject);
begin
  inherited;
  QCred.Close;
end;

procedure TFCadTaxas.DBEdit3KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',',',#8,#13]) then Key := #0;
end;

procedure TFCadTaxas.QCredBeforePost(DataSet: TDataSet);
begin
  inherited;
  tDescontar := '';
  QCred.FieldByName('DESCONTAR').AsString := UpperCase(QCred.FieldByName('DESCONTAR').AsString);
  tDescontar := UpperCase(QCred.FieldByName('DESCONTAR').AsString);
  SavePlace := QCred.GetBookmark;
end;

end.
