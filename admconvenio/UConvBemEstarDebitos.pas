unit UConvBemEstarDebitos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, ComCtrls, Menus, Buttons, ExtCtrls, DB, ADODB, Grids,
  DBGrids, JvExDBGrids, JvDBGrid, StdCtrls,math,StrUtils, Mask, DBCtrls;

type
  TfrmConvBemEstarDebitos = class(TF1)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Panel2: TPanel;
    QConvBemEstar: TADOQuery;
    DSConvBemEstar: TDataSource;
    QConvBemEstartitular: TStringField;
    QConvBemEstarconv_id: TIntegerField;
    QConvBemEstarempres_id: TIntegerField;
    QConvBemEstarliberado: TStringField;
    QConvBemEstarvalor: TBCDField;
    txtCodEmpresa: TEdit;
    cbFechamentoAtual: TCheckBox;
    procedure BitBtn1Click(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure QConvBemEstarAfterPost(DataSet: TDataSet);
    procedure txtCodEmpresaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure QConvBemEstarAfterInsert(DataSet: TDataSet);
  private
    function BuscaConvDescontaTaxa(convID: integer):boolean;
  public
    { Public declarations }
    oldValue : String;
    newValue : String;
    valor : string;
  end;

var
  frmConvBemEstarDebitos: TfrmConvBemEstarDebitos;

implementation

uses cartao_util, URotinasTexto, URotinasGrids, DM, UMenu;

{$R *.dfm}

procedure TfrmConvBemEstarDebitos.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if txtCodEmpresa.Text = '' then
  begin
    MsgInf('Digite o c�digo da empresa!');
    txtCodEmpresa.SetFocus;
  end;
  QConvBemEstar.Close;

  QConvBemEstar.Parameters[0].Value := txtCodEmpresa.Text;
  QConvBemEstar.Open;
  txtCodEmpresa.Clear;
  txtCodEmpresa.SetFocus;

end;

procedure TfrmConvBemEstarDebitos.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Check: Integer;
  R: TRect;
begin
  inherited;
  if ((Sender as TDBGrid).DataSource.Dataset.IsEmpty) then
    Exit;

  // Desenha um checkbox no dbgrid
  if Column.FieldName = 'liberado' then
  begin
    TDBGrid(Sender).Canvas.FillRect(Rect);
 
    if ((Sender as TDBGrid).DataSource.Dataset.FieldByName('liberado').AsString = 'S') then
      Check := DFCS_CHECKED
    else
      Check := 0;

    R := Rect;
    InflateRect(R, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(TDBGrid(Sender).Canvas.Handle, R, DFC_BUTTON,
      DFCS_BUTTONCHECK or Check);
  end;

end;

procedure TfrmConvBemEstarDebitos.JvDBGrid1DblClick(Sender: TObject);
VAR A,valorColuna : String;
var coluna : Integer;
begin
  if ((Sender as TDBGrid).DataSource.DataSet.IsEmpty) then
    Exit;

  coluna := JvDBGrid1.SelectedIndex;
  valorColuna := JvDBGrid1.SelectedField.AsString;

  //verifica se a coluna do clique foi a coluna "LIBERADO"
  valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',JvDBGrid1.Columns.Items[4].Field.AsCurrency));
  //COMENTADO APENAS PARA LIBERAR VERS�O PARA EDILMA
  //  if((valor = '0.0') or (JvDBGrid1.Columns.Items[4].Field.AsCurrency = 0)) then
//  begin
//    MsgInf('O valor de desconto n�o pode estar zerado!');
//    Exit;
//  end;
  if(coluna = 3)then
  begin
    oldValue := QConvBemEstar.Fields.FieldByName('liberado').AsString;
    QConvBemEstar.Edit;
    QConvBemEstar.Fields.FieldByName('liberado').AsString :=
    ifThen(QConvBemEstar.Fields.FieldByName('liberado').AsString = 'S','N','S');
    newValue := QConvBemEstar.Fields.FieldByName('liberado').AsString;
    valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',JvDBGrid1.Columns.Items[4].Field.AsCurrency));
    QConvBemEstar.Post;
  end;


end;

procedure TfrmConvBemEstarDebitos.QConvBemEstarAfterPost(
  DataSet: TDataSet);
  var sql, sqlLog: String;
  cadastro, detalhe, sqlQuery: string;
  reg_atual : TBookmark;
begin
  inherited;
  if ((newValue = 'S') or (newValue = 's')) then
  begin
    if not BuscaConvDescontaTaxa(QConvBemEstarconv_id.AsInteger) then
    begin
      if cbFechamentoAtual.Checked then
        sql := 'INSERT INTO CONV_BEMESTAR VALUES('+QConvBemEstarconv_id.AsString+',' +QConvBemEstarempres_id.AsString+', '+QuotedStr(newValue)+', '+valor+', '+QuotedStr('S')+ ','+ QuotedStr(FormatDateTime('dd/mm/yyyy',Date))+ ',null'+')'
      else
        sql := 'INSERT INTO CONV_BEMESTAR VALUES('+QConvBemEstarconv_id.AsString+',' +QConvBemEstarempres_id.AsString+', '+QuotedStr(newValue)+', '+valor+', '+QuotedStr('N')+','+ QuotedStr(FormatDateTime('dd/mm/yyyy',Date))+ ',null'+')' ;
    end;
  end;
  if sql <> '' then
  begin
    DMConexao.AdoQry.SQL.Clear;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.SQL.Text := sql;
    //DMConexao.AdoQry.Open;
    DMConexao.AdoQry.ExecSQL;
    DMConexao.AdoQry.Close;
    //DMConexao.ExecuteSql(sql);
  end;

  
  sqlQuery:= ' Insert into LOGS(LOG_ID, JANELA, CAMPO, VALOR_ANT, VALOR_POS, OPERADOR, '+
           ' OPERACAO, DATA_HORA, ID) values ('+
           ' next value for slog_id, ''FInsereConBemEstar'', ''Cobranca'', ''S'',''N'' , '+QuotedStr(Operador.Nome)+
           ', ''ADESAO BEM ESTAR'', current_timestamp, '+QConvBemEstarconv_id.AsString+')';
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Text := sqlQuery;
  DMConexao.AdoQry.ExecSQL;
  DMConexao.AdoQry.Close;

  QConvBemEstar.DisableControls;
  reg_atual := QConvBemEstar.GetBookmark;
  QConvBemEstar.Requery();
  QConvBemEstar.GotoBookmark(reg_atual);
  QConvBemEstar.freebookmark(reg_atual);
  QConvBemEstar.EnableControls;
  newValue := '';
  oldValue := '';
  valor := '';
end;

function TfrmConvBemEstarDebitos.BuscaConvDescontaTaxa(convID: integer):boolean;
var teste : Variant;
begin
  {if}teste := DMConexao.ExecuteScalar(' select conv_id from CONV_BEMESTAR where CONV_ID  = '+
    IntToStr(convID));// = 0 then
    if(teste = null) then
    begin
      Result:= False
    end
    else
      Result:= True;
end;

procedure TfrmConvBemEstarDebitos.txtCodEmpresaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then BitBtn1.Click;
end;

procedure TfrmConvBemEstarDebitos.FormCreate(Sender: TObject);
begin
  inherited;
  txtCodEmpresa.SetFocus;
end;

procedure TfrmConvBemEstarDebitos.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  inherited;
  try
  if Pos(Field.FieldName,QConvBemEstar.Sort) > 0 then begin
     if Pos(' DESC',QConvBemEstar.Sort) > 0 then QConvBemEstar.Sort := Field.FieldName
                                                  else QConvBemEstar.Sort := Field.FieldName+' DESC';
  end
  else QConvBemEstar.Sort := Field.FieldName;
  except
  end;
end;

procedure TfrmConvBemEstarDebitos.QConvBemEstarAfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  if (ActiveControl = JvDBGrid1) then
     QConvBemEstar.Cancel;
end;

end.
