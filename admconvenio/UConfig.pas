unit UConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, Shellapi,
  ZAbstractRODataset, ZDataset, {JvLookup,} ComCtrls, {JvDBComb,} Grids,
  DBGrids, {JvDBCtrl,} JvToolEdit, JvExDBGrids, JvDBGrid, JvExMask,
  JvExControls, JvDBLookup, JvExStdCtrls, JvCombobox, JvDBCombobox,
  ZAbstractDataset, ADODB;

type
  TFConfig = class(TForm)
    Panel1: TPanel;
    DSConfig: TDataSource;
    DSCredenciados: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    EditCartoes: TMaskEdit;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEditGrade: TDBCheckBox;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBCred: TJvDBLookupCombo;
    JvDBLookupCombo1: TJvDBLookupCombo;
    JvDBLookupCombo2: TJvDBLookupCombo;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    Panel2: TPanel;
    ButOk: TBitBtn;
    ButCancel: TBitBtn;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    TabSheet3: TTabSheet;
    DBRadioGroup1: TDBRadioGroup;
    DSEmpresas: TDataSource;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    Label7: TLabel;
    JvDBLookupCombo3: TJvDBLookupCombo;
    DBCheckBox11: TDBCheckBox;
    DBCheckBox12: TDBCheckBox;
    DBCheckBox13: TDBCheckBox;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBCheckBox15: TDBCheckBox;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBCheckBox16: TDBCheckBox;
    DBCheckBox17: TDBCheckBox;
    DBCheckBox18: TDBCheckBox;
    RGCodCartImp: TRadioGroup;
    SpeedButton1: TSpeedButton;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox14: TDBCheckBox;
    DBCheckBox19: TDBCheckBox;
    DBCheckBox20: TDBCheckBox;
    DBCheckBox21: TDBCheckBox;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBCheckBox22: TDBCheckBox;
    DBCheckBox23: TDBCheckBox;
    TabSheet4: TTabSheet;
    SpeedButton2: TSpeedButton;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    DBCheckBox24: TDBCheckBox;
    DBCheckBox25: TDBCheckBox;
    Label17: TLabel;
    JvDBComboBox1: TJvDBComboBox;
    DBCheckBox26: TDBCheckBox;
    DBCheckBox27: TDBCheckBox;
    DBCheckBox28: TDBCheckBox;
    tabHistorico: TTabSheet;
    PanelHistorico: TPanel;
    LabelDataini: TLabel;
    LabelDataFinal: TLabel;
    btnFirstC: TSpeedButton;
    btnPriorC: TSpeedButton;
    btnNextC: TSpeedButton;
    btnLastC: TSpeedButton;
    dataini: TJvDateEdit;
    datafin: TJvDateEdit;
    btnHist: TBitBtn;
    grdHistorico: TJvDBGrid;
    dsHistorico: TDataSource;
    DBCheckBox29: TDBCheckBox;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBCheckBox30: TDBCheckBox;
    DBEdit12: TDBEdit;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    QEmpresas: TADOQuery;
    QHistorico: TADOQuery;
    QCredenciados: TADOQuery;
    QCredenciadoscred_id: TIntegerField;
    QCredenciadosnome: TStringField;
    QEmpresasempres_id: TIntegerField;
    QEmpresasnome: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButOkClick(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EditCartoesExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure RGCodCartImpClick(Sender: TObject);
    procedure DBEdit6KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure btnHistClick(Sender: TObject);
    procedure tabHistoricoHide(Sender: TObject);
    procedure tabHistoricoShow(Sender: TObject);
    procedure btnFirstCClick(Sender: TObject);
    procedure btnPriorCClick(Sender: TObject);
    procedure btnNextCClick(Sender: TObject);
    procedure btnLastCClick(Sender: TObject);
    procedure dsHistoricoDataChange(Sender: TObject; Field: TField);
    procedure DBEdit11KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit12KeyPress(Sender: TObject; var Key: Char);
  private
    procedure LinkTo(const aAdress: String);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfig: TFConfig;

implementation

uses DM, cartao_util, UDBGridHelper, UMenu, Math;

{$R *.dfm}

procedure TFConfig.FormCreate(Sender: TObject);
begin
 DMConexao.Config.Open;

 if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then begin
    RGCodCartImp.ItemIndex:= 1;
    DBEdit3.Enabled:= False;
    DBEdit4.Enabled:= False;
    DBEdit5.Enabled:= False;
 end
 else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then begin
    RGCodCartImp.ItemIndex:= 2;
    DBEdit3.Enabled:= True;
    DBEdit4.Enabled:= True;
    DBEdit5.Enabled:= False;
 end
 else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then begin
    RGCodCartImp.ItemIndex:= 3;
    DBEdit3.Enabled:= False;
    DBEdit4.Enabled:= False;
    DBEdit5.Enabled:= False;
 end
 else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then begin
    RGCodCartImp.ItemIndex:= 4;
    DBEdit3.Enabled:= False;
    DBEdit4.Enabled:= False;
    DBEdit5.Enabled:= True;
 end
 else begin
    RGCodCartImp.ItemIndex:= 0;
    DBEdit3.Enabled:= False;
    DBEdit4.Enabled:= False;
    DBEdit5.Enabled:= False;
 end;
 TDBGridHelper.DBGridHelperTratrForm(Self,DMConexao.Query1,Operador.ID);
 EditCartoes.Text := DMConexao.Configcodigocartao.AsString;
 QCredenciados.Open;
 QEmpresas.Open;
end;

procedure TFConfig.FormDestroy(Sender: TObject);
begin
 DMConexao.Config.Close;
 QCredenciados.Close;
 QEmpresas.Close;
end;

procedure TFConfig.ButOkClick(Sender: TObject);
begin
  case RGCodCartImp.ItemIndex of
  0:  begin
         DMConexao.Config.Edit;
         DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString:= 'N';
         DMConexao.ConfigUSAINICIALCODCARTIMP.AsString:= 'N';
      end;
  1:  begin
         DMConexao.Config.Edit;
         DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString:= 'S';
         DMConexao.ConfigINCREMENTCODCARTIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString:= 'N';
         DMConexao.ConfigUSAINICIALCODCARTIMP.AsString:= 'N';
      end;
  2:  begin
         DMConexao.Config.Edit;
         DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMP.AsString:= 'S';
         DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString:= 'N';
         DMConexao.ConfigUSAINICIALCODCARTIMP.AsString:= 'N';
      end;
  3:  begin
         DMConexao.Config.Edit;
         DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString:= 'S';
         DMConexao.ConfigUSAINICIALCODCARTIMP.AsString:= 'N';
      end;
  else begin
         DMConexao.Config.Edit;
         DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMP.AsString:= 'N';
         DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString:= 'N';
         DMConexao.ConfigUSAINICIALCODCARTIMP.AsString:= 'S';
      end;
  end;
  if DMConexao.Config.State in [dsInsert,dsEdit] then
     DMConexao.Configcodigocartao.AsString := EditCartoes.Text;
  if DMConexao.Configcodigocartao.AsInteger < 100000000 then begin
     ShowMessage('O numero inicial dos cart�es tem que ser maior que 100.000.000 ');
     EditCartoes.SetFocus;
     Exit;
  end;
  if DMConexao.Config.State in [dsInsert,dsEdit] then begin
     if DBEditGrade.Checked then DMConexao.ConfigEDIT_GRID.AsString := 'S'
                            else DMConexao.ConfigEDIT_GRID.AsString := 'N';
  end;
  if DMConexao.Config.State in [dsInsert,dsEdit]
     then DMConexao.Config.Post;
  Close;
end;

procedure TFConfig.ButCancelClick(Sender: TObject);
begin
  if DMConexao.Config.State in [dsInsert,dsEdit] then DMConexao.Config.Cancel;
  Close;
end;

procedure TFConfig.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if DMConexao.Config.State in [dsInsert,dsEdit] then begin
     DMConexao.Configcodigocartao.AsString := EditCartoes.Text;
     Case Application.MessageBox('Deseja salvar as altera��es?','Confirma��o',MB_YESNOCANCEL+MB_DEFBUTTON1+MB_ICONQUESTION) of
        IDYes : DMConexao.Config.Post;
        IDNo  : DMConexao.Config.Cancel;
        IDCANCEL : SysUtils.Abort;
     end;
  end;
end;

procedure TFConfig.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
     if (TForm(Sender).ActiveControl is TJvDBLookupCombo) then begin
        if TJvDBLookupCombo(TForm(Sender).ActiveControl).ListVisible then begin
           TJvDBLookupCombo(TForm(Sender).ActiveControl).CloseUp(True);
           exit;
        end;
     end;
     Perform(WM_NEXTDLGCTL,0,0);
  end;
  if Key = #27 then ButCancelClick(nil);
end;

procedure TFConfig.EditCartoesExit(Sender: TObject);
begin
  if (Sender as TMaskEdit).Text <> DMConexao.Configcodigocartao.AsString then
    if DMConexao.Config.State = dsBrowse then DMConexao.Config.Edit;
end;

procedure TFConfig.SpeedButton1Click(Sender: TObject);
begin
   MsgInf('A maior numera��o do C�digo de Importa��o '+sLineBreak+'com 14 caracteres �: '+
   FloatToStr(DMConexao.ExecuteScalar('select max(cast(coalesce(codcartimp,0) as double precision)) as codimp from cartoes where apagado <> "S" and codcartimp <> "" and strlen(codcartimp) = 14')));
end;

procedure TFConfig.RGCodCartImpClick(Sender: TObject);
begin
  case RGCodCartImp.ItemIndex of
  0:  begin
         DBEdit3.Enabled:= False;
         DBEdit4.Enabled:= False;
         DBEdit5.Enabled:= False;
      end;
  1:  begin
         DBEdit3.Enabled:= False;
         DBEdit4.Enabled:= False;
         DBEdit5.Enabled:= False;
      end;
  2:  begin
         DBEdit3.Enabled:= True;
         DBEdit4.Enabled:= True;
         DBEdit5.Enabled:= False;
      end;
  3:  begin
         DBEdit3.Enabled:= False;
         DBEdit4.Enabled:= False;
         DBEdit5.Enabled:= False;
      end;
  else begin
         DBEdit3.Enabled:= False;
         DBEdit4.Enabled:= False;
         DBEdit5.Enabled:= True;
      end;
  end;
end;

procedure TFConfig.DBEdit6KeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9', #13,#8,',']) then Key := #0;
end;

procedure TFConfig.LinkTo(const aAdress: String);
var
  buffer: String;
begin
  if Pos(aAdress,'http') = 0 then
    buffer := 'http://' + aAdress
  else
    buffer := aAdress;
  ShellExecute(Application.Handle, nil, PChar(buffer), nil, nil, sw_hide); //SW_SHOWNORMAL);
end;

procedure TFConfig.SpeedButton2Click(Sender: TObject);
begin
  LinkTo(DMConexao.Config.FieldByName('LINK_EST').AsString);
end;

procedure TFConfig.SpeedButton3Click(Sender: TObject);
begin
  LinkTo(DMConexao.Config.FieldByName('LINK_EMP').AsString);
end;

procedure TFConfig.SpeedButton4Click(Sender: TObject);
begin
  LinkTo(DMConexao.Config.FieldByName('LINK_USU').AsString);
end;

procedure TFConfig.SpeedButton5Click(Sender: TObject);
begin
  LinkTo(DMConexao.Config.FieldByName('LINK_OND').AsString);
end;

procedure TFConfig.btnHistClick(Sender: TObject);
var cadastro, field : string;
begin
  if DMConexao.Config.IsEmpty then Exit;
  QHistorico.Close;
  QHistorico.Sql.Clear;
  QHistorico.Sql.Add(' Select * from logs ');
  QHistorico.Sql.Add(' where data_hora between '+QuotedStr(FormatDateTime('mm/dd/yyyy 00:00:00',dataini.Date))+' and '+QuotedStr(FormatDateTime('mm/dd/yyyy 23:59:59',datafin.Date)));
  QHistorico.Sql.Add(' and ID = '+DMConexao.ConfigCOD_ADM_BIG.AsString);
  QHistorico.Sql.Add(' and JANELA = ''FConfig'' ');
  QHistorico.Sql.Add(' and CAMPO <> ''ULTIMA_EXE'' ');
  QHistorico.Sql.Add(' order by data_hora desc ');
  QHistorico.Sql.Text;
  QHistorico.Open;
end;

procedure TFConfig.tabHistoricoHide(Sender: TObject);
begin
  if qHistorico.Active then qHistorico.Close;
end;

procedure TFConfig.tabHistoricoShow(Sender: TObject);
begin
  dataini.Date:= PrimeiroDiaMes(now);
  datafin.Date:= now;
end;

procedure TFConfig.btnFirstCClick(Sender: TObject);
begin
if QHistorico.Active then
  if QHistorico.RecordCount > 0 then
    QHistorico.First;
end;

procedure TFConfig.btnPriorCClick(Sender: TObject);
begin
if QHistorico.Active then
  if QHistorico.RecordCount > 0 then
    QHistorico.Prior;
end;

procedure TFConfig.btnNextCClick(Sender: TObject);
begin
if QHistorico.Active then
  if QHistorico.RecordCount > 0 then
    QHistorico.Next;
end;

procedure TFConfig.btnLastCClick(Sender: TObject);
begin
if QHistorico.Active then
  if QHistorico.RecordCount > 0 then
    QHistorico.Last;
end;

procedure TFConfig.dsHistoricoDataChange(Sender: TObject; Field: TField);
begin
  btnFirstC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnPriorC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Bof);
  btnNextC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Eof);
  btnLastC.Enabled:= (QHistorico.State = dsBrowse) and not (QHistorico.Eof);
end;

procedure TFConfig.DBEdit11KeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#13,#32,#8]) then key := #0;
end;

procedure TFConfig.DBEdit12KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = '.' then Key := ',';
end;

end.



