unit UImportEGeracaoDeCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, CheckLst, Mask, ToolEdit, ExtCtrls, Buttons,
  DB, ZAbstractRODataset, ZDataset, {JvLookup,} ADODB, ZStoredProcedure,
  ZSqlUpdate, ZAbstractDataset, JvToolEdit, ShellApi, JvExControls,
  JvDBLookup, JvExMask;

type
  TFImportEGeraCartao = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    ButAjuda: TSpeedButton;
    Label4: TLabel;
    Bevel1: TBevel;
    //FilenameEdit1: TFilenameEdit;
    Button1: TButton;
    RichEdit1: TRichEdit;
    ProgressBar1: TProgressBar;
    Empresas: TJvDBLookupCombo;
    DSEmpresas: TDataSource;
    Table1: TADOTable;
    FilenameEdit1: TJvFilenameEdit;
    QConv: TADOQuery;
    QConvCONV_ID: TIntegerField;
    QConvEMPRES_ID: TIntegerField;
    QConvCHAPA: TFloatField;
    QConvSENHA: TStringField;
    QConvTITULAR: TStringField;
    QConvCONTRATO: TIntegerField;
    QConvLIMITE_MES: TBCDField;
    QConvLIMITE_TOTAL: TBCDField;
    QConvLIMITE_PROX_FECHAMENTO: TBCDField;
    QConvLIBERADO: TStringField;
    QConvBANCO: TIntegerField;
    QConvTIPOPAGAMENTO: TStringField;
    QConvDTCADASTRO: TDateTimeField;
    QConvDTALTERACAO: TDateTimeField;
    QConvOPERADOR: TStringField;
    QConvSALARIO: TBCDField;
    QConvAPAGADO: TStringField;
    QCartao: TADOQuery;
    QCartaoCARTAO_ID: TIntegerField;
    QCartaoCONV_ID: TIntegerField;
    QCartaoNOME: TStringField;
    QCartaoLIBERADO: TStringField;
    QCartaoCODIGO: TIntegerField;
    QCartaoDIGITO: TWordField;
    QCartaoTITULAR: TStringField;
    QCartaoJAEMITIDO: TStringField;
    QCartaoAPAGADO: TStringField;
    QCartaoLIMITE_MES: TBCDField;
    QCartaoCODCARTIMP: TStringField;
    QCartaoPARENTESCO: TStringField;
    QCartaoDATA_NASC: TDateTimeField;
    QCartaoNUM_DEP: TIntegerField;
    QCartaoFLAG: TStringField;
    QCartaoDTEMISSAO: TDateTimeField;
    QCartaoCPF: TStringField;
    QCartaoRG: TStringField;
    QCartaoVIA: TIntegerField;
    QCartaoDTAPAGADO: TDateTimeField;
    QCartaoDTALTERACAO: TDateTimeField;
    QCartaoOPERADOR: TStringField;
    QCartaoDTCADASTRO: TDateTimeField;
    QCartaoOPERCADASTRO: TStringField;
    QCartaoCRED_ID: TIntegerField;
    QCartaoATIVO: TStringField;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TAutoIncField;
    QEmpresasnome: TStringField;
    QEmpresasUSA_COD_IMPORTACAO: TStringField;
    QConvGRUPO_CONV_EMP: TIntegerField;
    cbPlanHasDependente: TCheckBox;
    cbChapaCadastrada: TCheckBox;
    QCartaoTemp: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    WordField1: TWordField;
    StringField3: TStringField;
    StringField5: TStringField;
    BCDField1: TBCDField;
    StringField7: TStringField;
    DateTimeField1: TDateTimeField;
    IntegerField4: TIntegerField;
    StringField8: TStringField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    StringField10: TStringField;
    IntegerField5: TIntegerField;
    DateTimeField3: TDateTimeField;
    DateTimeField4: TDateTimeField;
    StringField11: TStringField;
    DateTimeField5: TDateTimeField;
    StringField12: TStringField;
    IntegerField6: TIntegerField;
    StringField13: TStringField;
    QCartoesPorConveniado: TADOQuery;
    QCartaoTempEMPRES_ID: TIntegerField;
    QCartaoTempSENHA: TStringField;
    QCartaoTempLIMITE_DIARIO: TBCDField;
    QCartaoTempCONSUMO_ATUAL: TBCDField;
    QCartaoTempCVV: TStringField;
    QCartaoTempJAEMITIDO_CARTAO_NOVO: TStringField;
    QCartaoTempDATA_EMISSAO_CARTAO_NOVO: TDateTimeField;
    QCartaoTempLOG_ID_CARTAO_NOVO: TIntegerField;
    QCartaoTempJAEMITIDO: TStringField;
    QCartaoTempCODCARTIMP: TStringField;
    QCartoesPorConveniadoCARTAO_ID: TIntegerField;
    QCartoesPorConveniadoCONV_ID: TIntegerField;
    QCartoesPorConveniadoNOME: TStringField;
    QCartoesPorConveniadoLIBERADO: TStringField;
    QCartoesPorConveniadoCODIGO: TIntegerField;
    QCartoesPorConveniadoDIGITO: TWordField;
    QCartoesPorConveniadoTITULAR: TStringField;
    QCartoesPorConveniadoJAEMITIDO: TStringField;
    QCartoesPorConveniadoAPAGADO: TStringField;
    QCartoesPorConveniadoLIMITE_MES: TBCDField;
    QCartoesPorConveniadoCODCARTIMP: TStringField;
    QCartoesPorConveniadoPARENTESCO: TStringField;
    QCartoesPorConveniadoDATA_NASC: TDateTimeField;
    QCartoesPorConveniadoNUM_DEP: TIntegerField;
    QCartoesPorConveniadoFLAG: TStringField;
    QCartoesPorConveniadoDTEMISSAO: TDateTimeField;
    QCartoesPorConveniadoCPF: TStringField;
    QCartoesPorConveniadoRG: TStringField;
    QCartoesPorConveniadoVIA: TIntegerField;
    QCartoesPorConveniadoDTAPAGADO: TDateTimeField;
    QCartoesPorConveniadoDTALTERACAO: TDateTimeField;
    QCartoesPorConveniadoOPERADOR: TStringField;
    QCartoesPorConveniadoDTCADASTRO: TDateTimeField;
    QCartoesPorConveniadoOPERCADASTRO: TStringField;
    QCartoesPorConveniadoCRED_ID: TIntegerField;
    QCartoesPorConveniadoATIVO: TStringField;
    QCartoesPorConveniadoEMPRES_ID: TIntegerField;
    QCartoesPorConveniadoSENHA: TStringField;
    QCartoesPorConveniadoLIMITE_DIARIO: TBCDField;
    QCartoesPorConveniadoCONSUMO_ATUAL: TBCDField;
    QCartoesPorConveniadoCVV: TStringField;
    QCartoesPorConveniadoJAEMITIDO_CARTAO_NOVO: TStringField;
    QCartoesPorConveniadoDATA_EMISSAO_CARTAO_NOVO: TDateTimeField;
    QCartoesPorConveniadoLOG_ID_CARTAO_NOVO: TIntegerField;
    QCartoesPorConveniadoCONV_ID_1: TIntegerField;
    QCartoesPorConveniadoEMPRES_ID_1: TIntegerField;
    QCartoesPorConveniadoBANCO: TIntegerField;
    QCartoesPorConveniadoGRUPO_CONV_EMP: TIntegerField;
    QCartoesPorConveniadoCHAPA: TFloatField;
    QCartoesPorConveniadoSENHA_1: TStringField;
    QCartoesPorConveniadoTITULAR_1: TStringField;
    QCartoesPorConveniadoCONTRATO: TIntegerField;
    QCartoesPorConveniadoLIMITE_MES_1: TBCDField;
    QCartoesPorConveniadoLIBERADO_1: TStringField;
    QCartoesPorConveniadoFIDELIDADE: TStringField;
    QCartoesPorConveniadoAPAGADO_1: TStringField;
    QCartoesPorConveniadoDT_NASCIMENTO: TDateTimeField;
    QCartoesPorConveniadoCARGO: TStringField;
    QCartoesPorConveniadoSETOR: TStringField;
    QCartoesPorConveniadoCPF_1: TStringField;
    QCartoesPorConveniadoRG_1: TStringField;
    QCartoesPorConveniadoLIMITE_TOTAL: TBCDField;
    QCartoesPorConveniadoLIMITE_PROX_FECHAMENTO: TBCDField;
    QCartoesPorConveniadoAGENCIA: TStringField;
    QCartoesPorConveniadoCONTACORRENTE: TStringField;
    QCartoesPorConveniadoDIGITO_CONTA: TStringField;
    QCartoesPorConveniadoTIPOPAGAMENTO: TStringField;
    QCartoesPorConveniadoENDERECO: TStringField;
    QCartoesPorConveniadoNUMERO: TIntegerField;
    QCartoesPorConveniadoBAIRRO: TIntegerField;
    QCartoesPorConveniadoCIDADE: TIntegerField;
    QCartoesPorConveniadoESTADO: TIntegerField;
    QCartoesPorConveniadoCEP: TStringField;
    QCartoesPorConveniadoTELEFONE1: TStringField;
    QCartoesPorConveniadoTELEFONE2: TStringField;
    QCartoesPorConveniadoCELULAR: TStringField;
    QCartoesPorConveniadoOBS1: TStringField;
    QCartoesPorConveniadoOBS2: TStringField;
    QCartoesPorConveniadoEMAIL: TStringField;
    QCartoesPorConveniadoCESTABASICA: TBCDField;
    QCartoesPorConveniadoDTULTCESTA: TDateTimeField;
    QCartoesPorConveniadoSALARIO: TBCDField;
    QCartoesPorConveniadoTIPOSALARIO: TStringField;
    QCartoesPorConveniadoCOD_EMPRESA: TStringField;
    QCartoesPorConveniadoFLAG_1: TStringField;
    QCartoesPorConveniadoDTASSOCIACAO: TDateTimeField;
    QCartoesPorConveniadoDTAPAGADO_1: TDateTimeField;
    QCartoesPorConveniadoDTALTERACAO_1: TDateTimeField;
    QCartoesPorConveniadoOPERADOR_1: TStringField;
    QCartoesPorConveniadoDTCADASTRO_1: TDateTimeField;
    QCartoesPorConveniadoOPERCADASTRO_1: TStringField;
    QCartoesPorConveniadoVALE_DESCONTO: TStringField;
    QCartoesPorConveniadoLIBERA_GRUPOSPROD: TStringField;
    QCartoesPorConveniadoCOMPLEMENTO: TStringField;
    QCartoesPorConveniadoUSA_SALDO_DIF: TStringField;
    QCartoesPorConveniadoABONO_MES: TBCDField;
    QCartoesPorConveniadoSALDO_RENOVACAO: TBCDField;
    QCartoesPorConveniadoSALDO_ACUMULADO: TBCDField;
    QCartoesPorConveniadoDATA_ATUALIZACAO_ACUMULADO: TDateTimeField;
    QCartoesPorConveniadoCONSUMO_MES_1: TBCDField;
    QCartoesPorConveniadoCONSUMO_MES_2: TBCDField;
    QCartoesPorConveniadoCONSUMO_MES_3: TBCDField;
    QCartoesPorConveniadoCONSUMO_MES_4: TBCDField;
    QCartoesPorConveniadoPIS: TFloatField;
    QCartoesPorConveniadoNOME_PAI: TStringField;
    QCartoesPorConveniadonome_mae: TStringField;
    QCartoesPorConveniadoCART_TRAB_NUM: TIntegerField;
    QCartoesPorConveniadoCART_TRAB_SERIE: TStringField;
    QCartoesPorConveniadoREGIME_TRAB: TStringField;
    QCartoesPorConveniadoVENC_TOTAL: TBCDField;
    QCartoesPorConveniadoESTADO_CIVIL: TStringField;
    QCartoesPorConveniadoNUM_DEPENDENTES: TIntegerField;
    QCartoesPorConveniadoDATA_ADMISSAO: TDateTimeField;
    QCartoesPorConveniadoDATA_DEMISSAO: TDateTimeField;
    QCartoesPorConveniadoFIM_CONTRATO: TDateTimeField;
    QCartoesPorConveniadoDISTRITO: TStringField;
    QCartoesPorConveniadoSALDO_DEVEDOR: TBCDField;
    QCartoesPorConveniadoSALDO_DEVEDOR_FAT: TBCDField;
    QCartoesPorConveniadoSETOR_ID: TIntegerField;
    QCartoesPorConveniadoCARTAO_BEM_ESTAR: TStringField;
    QCartoesPorConveniadoENVIA_SMS: TStringField;
    QCartoesPorConveniadoNAO_GERA_CARTAO_TITULAR: TStringField;
    QCartoesPorConveniadoTAXA_SERVICO_CANTINA: TStringField;
    QCartoesPorConveniadoSENHA_CANTINEX: TStringField;
    QCartoesPorConveniadoSALDO_DEVEDOR_DUVIDOSO: TBCDField;
    QCartaoEMPRES_ID: TIntegerField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButAjudaClick(Sender: TObject);
    procedure EmpresasKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure GeraCartao(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
    procedure GeraCartaoNovo(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string; logID:String; CartaoID:Integer;codigo_cartao:Integer);
    procedure GeraCartaoNovoTeste(chapa: String; empres_id: String;codBinNovo : String; logID : String);
    procedure GeraCartaoDependente (Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
    function ChapaJaExiste(Chapa : String; Empres_id : String) : Boolean;
  public
    { Public declarations }
  end;

var
  FImportEGeraCartao: TFImportEGeraCartao;

implementation

uses DM, UChangeLog, UMenu, cartao_util, UValidacao, Math;

{$R *.dfm}

procedure TFImportEGeraCartao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key = vk_escape then
  begin
    if application.messagebox('Fechar a janela?','Confirma��o',MB_YESNO+MB_OK+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
      close;
    end;
  end;
end;

procedure TFImportEGeraCartao.FormCreate(Sender: TObject);
begin
  QEmpresas.Open;
  FilenameEdit1.Text:= '"'+FMenu.GetPersonalFolder+'\"';
end;

procedure TFImportEGeraCartao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  QEmpresas.Close;
end;

procedure TFImportEGeraCartao.ButAjudaClick(Sender: TObject);
begin
  FChangeLog := TFChangeLog.Create(Self);
  FChangeLog.Caption := 'Ajuda - Importa��o de Conveniados';
  FChangeLog.RichEdit1.Lines := RichEdit1.Lines;
  FChangeLog.ShowModal;
  FChangeLog.Free;
end;

procedure TFImportEGeraCartao.EmpresasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
    Perform(WM_NEXTDLGCTL,0,0);
  end;
end;


function Split(const Str: string; Delimiter: Char): TStringList;
begin
  Result := TStringList.Create;
  //Result. := Delimiter;
  Result.Text := Str;
end;
procedure TFImportEGeraCartao.Button1Click(Sender: TObject);
var path, nome, chapa, dependente : String;
  conv_id, qtdImp, qtdErr, grupo_conv_emp,cont,empres_id,I,quantidadeDePalavras : Integer;
  limite,limite2 : Double;
  hasDependente : Boolean;
  lista,listaNomesParaInserir : TStringlist;
  logEmpresId, logStatus,logID : String;
  codimp, cvv,binNovo, cardImpNovo,usa_cod_importacao: string;
  erro: Boolean;
  item : TListItem;
begin
  hasDependente := False;
  qtdImp:= 0; qtdErr:= 0;
  erro:= False;
  screen.Cursor  := crHourGlass;
  if Empresas.Text = '' then
  begin
    ShowMessage('Selecione a Empresa para Importa��o.');
    screen.Cursor := crDefault;
    Exit;
  end;
  try
    path := '';
    if versaoOffice < 12 then
    begin
      path := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties=Excel 8.0;Persist Security Info=False';
    end
    else
    begin
      path := 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='+FilenameEdit1.Text+';';
      path := path + ';Extended Properties="Excel 12.0;HDR=YES;"';
    end;
    Table1.Active := False;
    Table1.ConnectionString := path;
    Table1.TableName:= 'Conv$';
    Table1.Active := True;
  except on E:Exception do
    ShowMessage('Erro ao encontrar o arquivo especificado.'+#13+'Erro: '+E.message);
  end;
  ProgressBar1.Position := 0;
  Button1.Enabled := False;
  ProgressBar1.Max := Table1.RecordCount;
  QConv.Open;
  QCartao.Open;
  QCartaoTemp.Open;
  lista := TStringList.Create;
  //listaNomesParaInserir := TStringList.Create;
  logID := DMConexao.ExecuteQuery('select next value for SLOG_NOVOS_CARTOES');
  while not Table1.Eof do
  begin
       empres_id := Empresas.KeyValue;
       //Importa conveniado apenas se a chapa ainda n�o estiver cadastrada.
       if(not ChapaJaExiste(SoNumero(Table1.FieldByName('CHAPA').AsString),IntToStr(empres_id)))then
       begin
          if cbPlanHasDependente.Checked = True then begin
            dependente := Table1.FieldByName('DEPENDENTE').AsString;
            if((dependente <> '') and (Length(dependente)>2)) then begin
                dependente := UpperCase(RemoveAcento(Trim(Table1.FieldByName('DEPENDENTE').AsString)));
                hasDependente := True;
            end;
          end;
          DMConexao.AdoQry.Close;
          DMConexao.AdoQry.SQL.Clear;
          DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCONV_ID');
          DMConexao.AdoQry.Open;
          conv_id := DMConexao.AdoQry.Fields[0].Value;
          nome    := UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString)));
          chapa   := SoNumero(Table1.FieldByName('CHAPA').AsString);
          limite  := Table1.FieldByName('LIMITE').AsFloat;

          QConv.Insert;
          try
            QConv.FieldByName('CONV_ID').AsInteger                := conv_id;
            QConv.FieldByName('EMPRES_ID').AsInteger              := QEmpresasEMPRES_ID.AsInteger;
            QConv.FieldByName('CHAPA').AsString                   := chapa;
            DMConexao.Config.Open;
            if DMConexao.ConfigSENHA_CONV_ID.AsString = 'S' then
            begin
              QConv.FieldByName('SENHA').AsString                := Crypt('E', IntToStr(conv_id), 'BIGCOMPRAS');
            end
            else
            begin
              QConv.FieldByName('SENHA').AsString                := Crypt('E', '1111', 'BIGCOMPRAS');
            end;
            DMConexao.Config.Close;
            QConv.FieldByName('TITULAR').AsString                 := nome;
            QConv.FieldByName('CONTRATO').AsInteger               := conv_id;
            QConv.FieldByName('LIMITE_MES').AsFloat               := limite;
            QConv.FieldByName('LIMITE_TOTAL').AsFloat             := 0;
            QConv.FieldByName('LIMITE_PROX_FECHAMENTO').AsFloat   := 0;
            QConv.FieldByName('LIBERADO').AsString                := 'S';
            QConv.FieldByName('BANCO').AsInteger                  := 0;
            QConv.FieldByName('TIPOPAGAMENTO').AsString           := 'N';
            QConv.FieldByName('DTCADASTRO').AsDateTime            := Date;
            QConv.FieldByName('DTALTERACAO').AsDateTime           := Date;
            QConv.FieldByName('OPERADOR').AsString                := 'CONVERSOR';
            QConv.FieldByName('SALARIO').AsFloat                  := 0;
            QConv.FieldByName('APAGADO').AsString                 := 'N';
            DMConexao.AdoQry.Close;
            DMConexao.AdoQry.SQL.Clear;
            DMConexao.AdoQry.SQL.Add('SELECT GRUPO_CONV_EMP_ID from GRUPO_CONV_EMP WHERE EMPRES_ID = '+QEmpresasempres_id.AsString);
            DMConexao.AdoQry.Open;
            QConv.FieldByName('GRUPO_CONV_EMP').AsInteger         := DMConexao.AdoQry.Fields[0].AsInteger;
            QConv.Post;
            qtdImp:= qtdImp + 1;
            cont := 0;
            if hasDependente then begin
              while(cont < 2) do
              begin
                if cont = 0 then
                        GeraCartao(nome,conv_id,QEmpresasEMPRES_ID.AsInteger,chapa)
                  else
                    GeraCartaoDependente(UpperCase(RemoveAcento(Trim(Table1.FieldByName('DEPENDENTE').AsString))),conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
                cont := cont + 1;
              end;
              hasDependente := False; //Zerando o valor da variavel has Dependente.
            end
            else begin
                GeraCartao(nome,conv_id,QEmpresasEMPRES_ID.AsInteger,chapa);
                binNovo := DMConexao.ExecuteQuery('select cod_card_bin_novo from config');
                GeraCartaoNovoTeste(chapa,QEmpresasempres_id.AsString,binNovo,logID);
            end;
          except
            on E:Exception do
            begin
              erro:= True;
              qtdErr:= qtdErr + 1;
              lista.Append('Erro: '+E.Message+' Cliente: '+Table1.FieldByName('NOME').AsString);
              QConv.Cancel;
            end;
          end;
       end
       else
       begin
          conv_id := DMConexao.ExecuteScalar('select conv_id from conveniados where empres_id = '+QEmpresasEMPRES_ID.AsString+' and chapa = '+Table1.FieldByName('CHAPA').AsString+'');
          nome    := UpperCase(RemoveAcento(Trim(Table1.FieldByName('NOME').AsString)));
          chapa   := SoNumero(Table1.FieldByName('CHAPA').AsString);
          binNovo := DMConexao.ExecuteQuery('select cod_card_bin_novo from config');
          GeraCartaoNovoTeste(chapa,QEmpresasEMPRES_ID.AsString,binNovo,logID);
       end;

       Table1.Next;
       Application.ProcessMessages;
       ProgressBar1.Position := ProgressBar1.Position +1;
  end;
  Button1.Enabled := True;
  screen.Cursor := crDefault;
  Table1.Close;
  QConv.Close;
  QCartao.Close;
  QCartaoTemp.Close;
  lista.SaveToFile(FMenu.GetPersonalFolder+'\erros.txt');
  lista.Free;
  ProgressBar1.Position := 0;
  if erro then
  begin
    if MsgSimNao('A Importa��o foi efetuada, por�m, ocorreram '+IntToStr(qtdErr)+' erro(s) na importa��o. '+sLineBreak+
    'foi gerado um relat�rio e salvo no diret�rio: '+FMenu.GetPersonalFolder+
    '\Erros.txt'+sLineBreak+'Deseja visualizar o relat�rio de erros?') then
      ShellExecute(Handle,'open','c:\windows\notepad.exe',PChar(FMenu.GetPersonalFolder+'\Erros.txt'),nil, SW_SHOWNORMAL);
  end
  else
  begin
    MsgInf('Importa��o Realizada com Sucesso.'+sLineBreak+IntToStr(qtdImp)+' Conveniados Importados');
  end;
end;

function TFImportEGeraCartao.ChapaJaExiste(Chapa : String; Empres_id : String) : Boolean;
var chapaBD : String;
begin
  chapaBD := DMConexao.ExecuteScalar('SELECT(COALESCE((select coalesce(chapa,0) from conveniados where empres_id = '+Empres_id+' AND CHAPA = '+Chapa+'),0))');
  if(chapaBD <> '0') then
    Result := True
    else
      Result := False;
end;

procedure TFImportEGeraCartao.GeraCartao(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
var
  cartao_id, codigo_cartao : Integer;
  codimp: string;
begin
  cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');
  codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');
  QCartao.Append;
  QCartao.FieldByName('CARTAO_ID').AsInteger    := cartao_id;
  QCartao.FieldByName('CONV_ID').AsInteger      := conv_id;
  QCartao.FieldByName('NOME').AsString          := UpperCase(nome);
  QCartao.FieldByName('LIBERADO').AsString      := 'S';
  QCartao.FieldByName('CODIGO').AsInteger       := codigo_cartao;
  QCartao.FieldByName('DIGITO').AsInteger       := DigitoCartao(codigo_cartao);
  QCartao.FieldByName('LIMITE_MES').AsFloat     := 0;
  QCartao.FieldByName('JAEMITIDO').AsString     := 'N';
  QCartao.FieldByName('TITULAR').AsString       := 'S';
  QCartao.FieldByName('DTALTERACAO').AsDateTime := Date;
  QCartao.FieldByName('DTCADASTRO').AsDateTime  := Date;
  QCartao.FieldByName('OPERADOR').AsString      := 'CONVERSOR';
  QCartao.FieldByName('APAGADO').AsString       := 'N';
  QCartao.FieldByName('EMPRES_ID').AsInteger    := Empres_id;
  DMConexao.Config.Open;
 if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
    until (verificaCartaoExistente(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;
  QCartao.Post;
end;

procedure TFImportEGeraCartao.GeraCartaoNovoTeste(chapa: String; empres_id: String; codBinNovo : String; logID : String);
var codimp, cvv, cardImpNovo: string;
    logEmpresId, logStatus,usa_cod_importacao : String;
begin
    logEmpresId := empres_id;
    //logID := DMConexao.ExecuteQuery('select next value for SLOG_NOVOS_CARTOES');

    //Busca todos os cart�es do funcion�rio com a chapa

    QCartoesPorConveniado.Close;
    QCartoesPorConveniado.Parameters[0].Value := chapa;
    QCartoesPorConveniado.Parameters[1].Value := empres_id;
    QCartoesPorConveniado.Open;
    while not QCartoesPorConveniado.Eof do
    begin
      //Enquanto ouver cart�es antigos vinculados ao conveniado gere um cart�o novo para cada.
      DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE(), LOG_ID_CARTAO_NOVO = '+logID+' where CARTAO_ID = '+QCartoesPorConveniadocartao_id.AsString+'');
      DMConexao.ExecuteSql('insert into cartoes_temp select * from cartoes where CARTAO_ID = '+QCartoesPorConveniadocartao_id.AsString+'');
      QCartaoTemp.Parameters[0].Value := QCartoesPorConveniadocartao_id.Value;
      QCartaoTemp.Open;
      QCartaoTemp.Edit;
      QCartaoTempJAEMITIDO.AsString := 'N';
      usa_cod_importacao := DMConexao.ExecuteQuery('select usa_cod_importacao from EMPRESAS where EMPRES_ID = '+empres_id);

      //gera novo carImpNovo
        if usa_cod_importacao = 'S' then begin
          repeat
            codimp := RemoveCaracter(gerarCartao(StrToInt(codBinNovo)));
          until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp));

        end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
          codimp := QCartoesPorConveniadocodigo.AsString
        else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
        begin
          if QCartoesPorConveniadotitular.AsString = 'S' then
            codimp := DMConexao.ObterCodCartImp
          else
            codimp := DMConexao.ObterCodCartImp(False);
        end
        else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
          codimp := DMConexao.ObterCodCartImpMod1(QCartoesPorConveniadoconv_id.AsInteger, QCartoesPorConveniadoempres_id.AsInteger,QCartoesPorConveniadochapa.AsString)
        else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
          codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));

        //fim gera��o de cart�o
        cardImpNovo := codimp;
        QCartaoTempCODCARTIMP.AsString := cardImpNovo;

        //Gera o CVV
        cvv := GerarCVV(cardImpNovo);
        QCartaoTempCVV.AsString := cvv;

        if QCartaoTemp.State in [dsEdit] then begin
          QCartaoTemp.Post;
        end;
        QCartaoTemp.Close;
        QCartoesPorConveniado.Next;
    end;
end;

procedure TFImportEGeraCartao.GeraCartaoNovo(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string; logID:String; CartaoID:Integer;codigo_cartao:Integer);
var
  //cartao_id, codigo_cartao : Integer;

  codimp: string;
begin
  //cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');
  DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE(), LOG_ID_CARTAO_NOVO = '+logID+' where CARTAO_ID = '+IntToStr(CartaoID)+'');
  //codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');
  QCartaoTemp.Append;
  QCartaoTemp.FieldByName('CARTAO_ID').AsInteger    := CartaoID;
  QCartaoTemp.FieldByName('CONV_ID').AsInteger      := conv_id;
  QCartaoTemp.FieldByName('NOME').AsString          := UpperCase(nome);
  QCartaoTemp.FieldByName('LIBERADO').AsString      := 'S';
  QCartaoTemp.FieldByName('CODIGO').AsInteger       := codigo_cartao;
  QCartaoTemp.FieldByName('DIGITO').AsInteger       := DigitoCartao(codigo_cartao);
  QCartaoTemp.FieldByName('LIMITE_MES').AsFloat     := 0;
  QCartaoTemp.FieldByName('JAEMITIDO').AsString     := 'N';
  QCartaoTemp.FieldByName('TITULAR').AsString       := 'S';
  QCartaoTemp.FieldByName('DTALTERACAO').AsDateTime := Date;
  QCartaoTemp.FieldByName('DTCADASTRO').AsDateTime  := Date;
  QCartaoTemp.FieldByName('OPERADOR').AsString      := 'CONVERSOR';
  QCartaoTemp.FieldByName('APAGADO').AsString       := 'N';
  QCartaoTemp.FieldByName('EMPRES_ID').AsInteger     := Empres_id;
  QCartaoTemp.FieldByName('LOG_ID_CARTAO_NOVO').AsInteger     := StrToInt(logID);
  DMConexao.Config.Open;
  if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartaoTemp.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
    until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp));
    QCartaoTemp.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartaoTemp.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartaoTemp.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartaoTemp.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;
  QCartaoTemp.Post;
end;

procedure TFImportEGeraCartao.GeraCartaoDependente(Nome: String; Conv_id: integer; Empres_id: integer; Chapa:string);
var
  cartao_id, codigo_cartao : Integer;
  codimp: string;
begin
  cartao_id     := DMConexao.getGeneratorValue('SCARTAO_ID');
  codigo_cartao := DMConexao.getGeneratorValue('SCARTAO_NUM');
  QCartao.Append;
  QCartao.FieldByName('CARTAO_ID').AsInteger    := cartao_id;
  QCartao.FieldByName('CONV_ID').AsInteger      := conv_id;
  QCartao.FieldByName('NOME').AsString          := UpperCase(nome);
  QCartao.FieldByName('LIBERADO').AsString      := 'S';
  QCartao.FieldByName('CODIGO').AsInteger       := codigo_cartao;
  QCartao.FieldByName('DIGITO').AsInteger       := DigitoCartao(codigo_cartao);
  QCartao.FieldByName('LIMITE_MES').AsFloat     := 0;
  QCartao.FieldByName('JAEMITIDO').AsString     := 'N';
  QCartao.FieldByName('TITULAR').AsString       := 'N';
  QCartao.FieldByName('DTALTERACAO').AsDateTime := Date;
  QCartao.FieldByName('DTCADASTRO').AsDateTime  := Date;
  QCartao.FieldByName('OPERADOR').AsString      := 'CONVERSOR';
  QCartao.FieldByName('APAGADO').AsString       := 'N';
  DMConexao.Config.Open;
 if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));
  end else if QEmpresasUSA_COD_IMPORTACAO.AsString = 'S' then begin
    repeat
      codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN.AsInteger));
    until (verificaCartaoExistente(codimp));
    QCartao.FieldByName('CODCARTIMP').AsString := codimp;
  end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := IntToStr(codigo_cartao);
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImp;
  end
  else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
  begin
    QCartao.FieldByName('CODCARTIMP').AsString := DMConexao.ObterCodCartImpMod1(Conv_id,Empres_id,Chapa);
  end;
  DMConexao.Config.Close;
  QCartao.Post;
end;

end.
