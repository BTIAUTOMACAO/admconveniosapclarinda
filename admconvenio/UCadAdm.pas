unit UCadAdm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB,  StdCtrls, Mask, JvToolEdit, Grids, DBGrids, {JvDBCtrl,}
  Buttons, DBCtrls, ExtCtrls, ComCtrls, Menus, {JvLookup,} XMLDoc, IdHTTP,
  {JvDBComb,} xmldom, XMLIntf, msxmldom, JvExControls, JvDBLookup,
  JvExStdCtrls, JvCombobox, JvDBCombobox, JvExMask, JvExDBGrids, JvDBGrid,
  ADODB;

type
  TFCadAdm = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    TabSheet2: TTabSheet;
    dsContas: TDataSource;
    Panel5: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    dbEdtRazSocial: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    edtCep: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label18: TLabel;
    DBEdit15: TDBEdit;
    SpeedButton1: TSpeedButton;
    cbbUF: TJvDBComboBox;
    Label19: TLabel;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Panel6: TPanel;
    Label17: TLabel;
    JvDBLookupCombo1: TJvDBLookupCombo;
    qContas: TADOQuery;
    qContasconta_id: TIntegerField;
    qContasnome_conta: TStringField;
    QCadastroADM_ID: TIntegerField;
    QCadastroRAZAO: TStringField;
    QCadastroAPAGADO: TStringField;
    QCadastroCONTA_ID: TIntegerField;
    QCadastroFANTASIA: TStringField;
    QCadastroCNPJ: TStringField;
    QCadastroINSC_EST: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroBAIRRO: TStringField;
    QCadastroCIDADE: TStringField;
    QCadastroCEP: TStringField;
    QCadastroUF: TStringField;
    QCadastroRESPONSAVEL: TStringField;
    QCadastroFONE: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroVENCEDIA: TIntegerField;
    QCadastroCOMPLEMENTO: TStringField;
    procedure ButIncluiClick(Sender: TObject);
    procedure ButEditClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButGravaClick(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure ButApagaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadAdm: TFCadAdm;

implementation

uses DM, cartao_util, UValidacao, UMenu;

{$R *.dfm}

procedure TFCadAdm.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtRazSocial.SetFocus;
  QCadastroAPAGADO.AsString := 'N';
end;

procedure TFCadAdm.ButEditClick(Sender: TObject);
begin
  inherited;
  dbEdtRazSocial.SetFocus;
end;

procedure TFCadAdm.FormCreate(Sender: TObject);
begin
  chavepri := 'ADM_ID';
  Detalhe  := 'C�digo: ';
  inherited;
  qContas.Open;
  QCadastro.Open;
end;

procedure TFCadAdm.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add('Select * from administradora where coalesce(apagado,''N'') <> ''S'' ');
  if Trim(EdCod.Text) <> '' then
     QCadastro.Sql.Add(' and adm_id in ('+EdCod.Text+')');
  if Trim(EdNome.Text) <> '' then
     QCadastro.Sql.Add(' and fantasia like ''%'+EdNome.Text+'%'' ');
  QCadastro.Sql.Add(' order by adm_id, fantasia ');
  QCadastro.Open;
  If not QCadastro.IsEmpty then DBGrid1.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
end;

procedure TFCadAdm.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SADM_ID AS ADM_ID');
  DMConexao.AdoQry.Open;
  QCadastroADM_ID.AsInteger := DMConexao.AdoQry.FieldByName('ADM_ID').AsInteger;
  QCadastroAPAGADO.AsString := 'N';
  QCadastroDTCADASTRO.AsString := DateTimeToStr(Now);
  QCadastroOPERCADASTRO.AsString := Operador.Nome;
end;

procedure TFCadAdm.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Administradora: '+QCadastroFANTASIA.AsString;
end;

procedure TFCadAdm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  qContas.Close;
end;

procedure TFCadAdm.ButGravaClick(Sender: TObject);
begin
  if fnVerfCompVazioEmTabSheet('Raz�o social obrigat�rio',dbEdtRazSocial) then abort;
  inherited;
  PageControl2.ActivePageIndex:= 0;
end;

procedure TFCadAdm.QCadastroBeforePost(DataSet: TDataSet);
begin
  inherited;
  if QCadastroCONTA_ID.AsInteger = 0 then
    QCadastroCONTA_ID.Clear;
end;

procedure TFCadAdm.SpeedButton1Click(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: IXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  if (edtCep.Text = '') or (Length(SoNumero(edtCep.Text)) <> 8) then
  begin
    Application.MessageBox('CEP nulo ou inv�lido.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
    exit;
  end;
  Resposta   := TStringStream.Create('');
  TSConsulta := TStringList.Create;
  IdHTTP1:= TIdHTTP.Create(Self);
  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
  TSConsulta.Values['&cep'] := SoNumero(edtCep.Text);
  TSConsulta.Values['&formato'] := 'xml';
  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
  TSConsulta.Free;
  IdHTTP1.Free;
  XMLDocCEP:= NewXMLDocument;
  XMLDocCEP.Active := True;
  XMLDocCEP.Encoding := 'iso-8859-1';
  XMLDocCEP.LoadFromStream(Resposta);
  try
    try
      QCadastro.Edit;
      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
                                                    ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
      QCadastro.FieldByName('BAIRRO').AsString      := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
      QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
      QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
    except
      ShowMessage('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
    end;
  finally
    Resposta.Free;
    XMLDocCEP.Active := False;
    XMLDocCEP := nil;
  end;
end;

procedure TFCadAdm.TabFichaShow(Sender: TObject);
begin
  inherited;
  PageControl2.TabIndex:= 0;
end;

procedure TFCadAdm.GridHistoricoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  try
    if Pos(Field.FieldName,QHistorico.Sort) > 0 then
    begin
      if Pos(' DESC',QHistorico.Sort) > 0 then
        QHistorico.Sort := Field.FieldName
      else
        QHistorico.Sort := Field.FieldName+' DESC';
    end
    else
      QHistorico.Sort := Field.FieldName;
  except
  end;

end;

procedure TFCadAdm.ButApagaClick(Sender: TObject);
var adm_id : string;
begin
   adm_id := QCadastroADM_ID.AsString;
   inherited;
   DMConexao.AdoQry.Close;
   DMConexao.AdoQry.SQL.Clear;
   DMConexao.AdoQry.SQL.Add('UPDATE ADMINISTRADORA SET DTAPAGADO = CURRENT_TIMESTAMP WHERE ADM_ID = '+adm_id+'');
   DMConexao.AdoQry.ExecSQL;
   DMConexao.AdoQry.Close;
   QCadastro.Requery;
end;

end.
