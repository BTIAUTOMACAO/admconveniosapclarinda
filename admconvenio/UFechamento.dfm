inherited FFechamento: TFFechamento
  Left = 176
  Top = 40
  ActiveControl = DataIni
  Caption = 'Fechamento de Empresa'
  ClientHeight = 573
  ClientWidth = 792
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 792
    inherited panTitulo: TPanel
      Width = 1296
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 23
    Width = 792
    Height = 68
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 1
    object Label2: TLabel
      Left = 193
      Top = 6
      Width = 128
      Height = 13
      Caption = 'Empresas com fechamento'
    end
    object Label1: TLabel
      Left = 176
      Top = 28
      Width = 144
      Height = 13
      Caption = 'De                                         '#192
    end
    object ButAbrir: TButton
      Left = 446
      Top = 18
      Width = 75
      Height = 25
      Caption = '&Abrir'
      TabOrder = 1
      OnClick = ButAbrirClick
    end
    object DataIni: TJvDateEdit
      Left = 193
      Top = 21
      Width = 120
      Height = 21
      Hint = 'Data inicial de fechamento'
      DefaultToday = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ShowNullDate = False
      TabOrder = 2
    end
    object RGAutor: TRadioGroup
      Left = 3
      Top = 3
      Width = 166
      Height = 57
      Hint = 
        'Se a op'#231#227'o estiver "Somente Confirmadas" ser'#225' calculada'#13#10'apenas ' +
        'as autoriza'#231#245'es ja confirmadas na administradora'#13#10'e ao efetuar o' +
        ' fechamento, as autoriza'#231#245'es n'#227'o confirmadas'#13#10'ser'#227'o jogadas para' +
        ' o pr'#243'ximo fechamento em aberto.'
      Caption = 'Faturar Autoriza'#231#245'es...'
      ItemIndex = 0
      Items.Strings = (
        'Somente Confirmadas'
        'Todas')
      TabOrder = 0
      OnClick = RGAutorClick
    end
    object ChkMostrarAbertos: TCheckBox
      Left = 193
      Top = 43
      Width = 264
      Height = 16
      Hint = 'Listar todos os fechamentos dispon'#237'veis do sistema.'
      Caption = 'Mostrar Todos Fechamentos Anteriores em Aberto'
      TabOrder = 4
    end
    object DataFin: TJvDateEdit
      Left = 322
      Top = 21
      Width = 120
      Height = 21
      Hint = 'Data final de fechamento'
      DefaultToday = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ShowNullDate = False
      TabOrder = 3
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 496
    Width = 792
    Height = 77
    Align = alBottom
    BorderStyle = bsSingle
    TabOrder = 3
    DesignSize = (
      788
      73)
    object Label63: TLabel
      Left = 27
      Top = 34
      Width = 47
      Height = 13
      Caption = 'Faturadas'
    end
    object ButFechar: TButton
      Left = 276
      Top = 32
      Width = 241
      Height = 33
      Hint = 'Efeuar o fechamento da empresa selecionada'
      Anchors = [akTop]
      Caption = 'Executar o Fechamento e Faturar a Empresa'
      Enabled = False
      TabOrder = 3
      OnClick = ButFecharClick
    end
    object Panel17: TPanel
      Left = 7
      Top = 33
      Width = 17
      Height = 14
      BorderStyle = bsSingle
      Color = clRed
      TabOrder = 4
    end
    object btnMarDes: TButton
      Left = 3
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar ou desmarcar um fornecedor'
      Caption = 'Marca/Desm (F8)'
      TabOrder = 0
      OnClick = btnMarDesClick
    end
    object btnMarTod: TButton
      Left = 112
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar todos os fornecedores'
      Caption = 'Marca Todos (F9) '
      TabOrder = 1
      OnClick = btnMarTodClick
    end
    object btnDesTod: TButton
      Left = 220
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Desmarcar todos os fornecedores'
      Caption = 'Desm. Todos (F10) '
      TabOrder = 2
      OnClick = btnDesTodClick
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 91
    Width = 792
    Height = 405
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Empresas com fechamento na data selecionada'
      object grdFech: TJvDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 377
        Align = alClient
        DataSource = DSEmpresa
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = grdFechDrawColumnCell
        OnDblClick = grdFechDblClick
        OnKeyDown = grdFechKeyDown
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Title.Caption = 'Empres ID'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 270
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_FECHA'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_VENC'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor da Fatura'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 110
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'FATURADA'
            Title.Alignment = taCenter
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FATURA_ID'
            Width = 55
            Visible = True
          end>
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 52
    Top = 344
  end
  object DSEmpresa: TDataSource
    DataSet = MEmpresa
    Left = 48
    Top = 265
  end
  object qContaC: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select * from contacorrente where autorizacao_id = 0')
    Left = 20
    Top = 235
    object qContaCAUTORIZACAO_ID: TIntegerField
      FieldName = 'AUTORIZACAO_ID'
    end
    object qContaCCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object qContaCCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object qContaCCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qContaCDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object qContaCDATA: TDateTimeField
      FieldName = 'DATA'
    end
    object qContaCHORA: TStringField
      FieldName = 'HORA'
      FixedChar = True
      Size = 8
    end
    object qContaCDATAVENDA: TDateTimeField
      FieldName = 'DATAVENDA'
    end
    object qContaCDEBITO: TBCDField
      FieldName = 'DEBITO'
      Precision = 15
      Size = 2
    end
    object qContaCCREDITO: TBCDField
      FieldName = 'CREDITO'
      Precision = 15
      Size = 2
    end
    object qContaCVALOR_CANCELADO: TBCDField
      FieldName = 'VALOR_CANCELADO'
      Precision = 15
      Size = 2
    end
    object qContaCBAIXA_CONVENIADO: TStringField
      FieldName = 'BAIXA_CONVENIADO'
      FixedChar = True
      Size = 1
    end
    object qContaCBAIXA_CREDENCIADO: TStringField
      FieldName = 'BAIXA_CREDENCIADO'
      FixedChar = True
      Size = 1
    end
    object qContaCENTREG_NF: TStringField
      FieldName = 'ENTREG_NF'
      FixedChar = True
      Size = 1
    end
    object qContaCRECEITA: TStringField
      FieldName = 'RECEITA'
      FixedChar = True
      Size = 1
    end
    object qContaCCESTA: TStringField
      FieldName = 'CESTA'
      FixedChar = True
      Size = 1
    end
    object qContaCCANCELADA: TStringField
      FieldName = 'CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qContaCDIGI_MANUAL: TStringField
      FieldName = 'DIGI_MANUAL'
      FixedChar = True
      Size = 1
    end
    object qContaCTRANS_ID: TIntegerField
      FieldName = 'TRANS_ID'
    end
    object qContaCFORMAPAGTO_ID: TIntegerField
      FieldName = 'FORMAPAGTO_ID'
    end
    object qContaCFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qContaCPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object qContaCAUTORIZACAO_ID_CANC: TIntegerField
      FieldName = 'AUTORIZACAO_ID_CANC'
    end
    object qContaCOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object qContaCDATA_VENC_EMP: TDateTimeField
      FieldName = 'DATA_VENC_EMP'
    end
    object qContaCDATA_FECHA_EMP: TDateTimeField
      FieldName = 'DATA_FECHA_EMP'
    end
    object qContaCDATA_VENC_FOR: TDateTimeField
      FieldName = 'DATA_VENC_FOR'
    end
    object qContaCDATA_FECHA_FOR: TDateTimeField
      FieldName = 'DATA_FECHA_FOR'
    end
    object qContaCHISTORICO: TStringField
      FieldName = 'HISTORICO'
      Size = 80
    end
    object qContaCNF: TIntegerField
      FieldName = 'NF'
    end
    object qContaCDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object qContaCDATA_BAIXA_CONV: TDateTimeField
      FieldName = 'DATA_BAIXA_CONV'
    end
    object qContaCDATA_BAIXA_CRED: TDateTimeField
      FieldName = 'DATA_BAIXA_CRED'
    end
    object qContaCOPER_BAIXA_CONV: TStringField
      FieldName = 'OPER_BAIXA_CONV'
      Size = 25
    end
    object qContaCOPER_BAIXA_CRED: TStringField
      FieldName = 'OPER_BAIXA_CRED'
      Size = 25
    end
    object qContaCDATA_CONFIRMACAO: TDateTimeField
      FieldName = 'DATA_CONFIRMACAO'
    end
    object qContaCOPER_CONFIRMACAO: TStringField
      FieldName = 'OPER_CONFIRMACAO'
      Size = 25
    end
    object qContaCCONFERIDO: TStringField
      FieldName = 'CONFERIDO'
      FixedChar = True
      Size = 1
    end
    object qContaCNSU: TIntegerField
      FieldName = 'NSU'
    end
    object qContaCPREVIAMENTE_CANCELADA: TStringField
      FieldName = 'PREVIAMENTE_CANCELADA'
      FixedChar = True
      Size = 1
    end
    object qContaCEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
  end
  object QEmpresa: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'select fatura.id as empres_id, empresas.nome, fatura.fechamento ' +
        'data_fecha, '
      
        'fatura.data_vencimento data_venc, valor as saldo,  fatura.fatura' +
        '_id, empresas.obs_fechamento obs '
      'from fatura join empresas on empresas.empres_id = fatura.id '
      'and coalesce(fatura.tipo,'#39'E'#39')='#39'E'#39)
    Left = 52
    Top = 235
    object QEmpresaempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QEmpresanome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresadata_fecha: TDateTimeField
      FieldName = 'data_fecha'
    end
    object QEmpresadata_venc: TDateTimeField
      FieldName = 'data_venc'
    end
    object QEmpresasaldo: TFloatField
      FieldName = 'saldo'
      DisplayFormat = '#,##0.00'
    end
    object QEmpresafatura_id: TIntegerField
      FieldName = 'fatura_id'
    end
  end
  object QDescEmp: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      
        'Select cc.cred_id, cc.data_fecha_emp, sum(cc.debito-cc.credito) ' +
        'valor, emp.desconto_emp,'
      
        'sum(cc.debito-cc.credito)*emp.desconto_emp/100 as desc_valor fro' +
        'm contacorrente cc'
      
        'join conveniados conv on conv.conv_id = cc.conv_id join empresas' +
        ' emp on conv.empres_id = emp.empres_id'
      
        'where coalesce(cc.baixa_conveniado,'#39'N'#39') <> '#39'S'#39' and coalesce(cc.f' +
        'atura_id,0) = 0 and cc.data_fecha_emp = '#39'10/10/2008'#39' and emp.emp' +
        'res_id = 1036'
      'group by cc.cred_id, cc.data_fecha_emp, emp.desconto_emp')
    Left = 84
    Top = 235
    object QDescEmpcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QDescEmpdata_fecha_emp: TDateTimeField
      FieldName = 'data_fecha_emp'
    end
    object QDescEmpvalor: TBCDField
      FieldName = 'valor'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object QDescEmpdesconto_emp: TBCDField
      FieldName = 'desconto_emp'
      Precision = 6
      Size = 2
    end
    object QDescEmpdesc_valor: TBCDField
      FieldName = 'desc_valor'
      ReadOnly = True
      Precision = 32
      Size = 6
    end
  end
  object MEmpresa: TJvMemoryData
    FieldDefs = <
      item
        Name = 'EMPRES_ID'
        DataType = ftInteger
      end
      item
        Name = 'NOME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'DATA_FECHA'
        DataType = ftDateTime
      end
      item
        Name = 'DATA_VENC'
        DataType = ftDateTime
      end
      item
        Name = 'SALDO'
        DataType = ftFloat
      end
      item
        Name = 'FATURA_ID'
        DataType = ftInteger
      end
      item
        Name = 'OBS'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'MARCADO'
        DataType = ftBoolean
      end
      item
        Name = 'FATURADA'
        DataType = ftString
        Size = 20
      end>
    Left = 52
    Top = 299
    object MEmpresaNOME: TStringField
      DisplayLabel = 'Nome/Raz'#227'o'
      FieldName = 'NOME'
      Size = 60
    end
    object MEmpresaSALDO: TFloatField
      DisplayLabel = 'Saldo'
      FieldName = 'SALDO'
      DisplayFormat = '#,##0.00'
    end
    object MEmpresaFATURA_ID: TIntegerField
      DisplayLabel = 'Fatura ID.'
      FieldName = 'FATURA_ID'
    end
    object MEmpresaOBS: TStringField
      DisplayLabel = 'Obs.'
      FieldName = 'OBS'
      Size = 200
    end
    object MEmpresaMARCADO: TBooleanField
      FieldName = 'MARCADO'
    end
    object MEmpresaDATA_FECHA: TDateTimeField
      DisplayLabel = 'Data Fechamento'
      FieldName = 'DATA_FECHA'
    end
    object MEmpresaDATA_VENC: TDateTimeField
      DisplayLabel = 'Data Vencimento'
      FieldName = 'DATA_VENC'
    end
    object MEmpresaEMPRES_ID: TIntegerField
      DisplayLabel = 'Empresa ID'
      FieldName = 'EMPRES_ID'
    end
    object MEmpresaFATURADA: TStringField
      DisplayLabel = 'Faturada'
      FieldName = 'FATURADA'
    end
  end
  object dlgAbrePDF: TOpenDialog
    DefaultExt = 'pdf'
    Filter = 'Arquivos PDF (*.pdf)|*.pdf'
    InitialDir = '\\tsclient\Z'
    Left = 276
    Top = 283
  end
  object qBoletos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM BOLETOS WHERE BOLETO_ID = 0')
    Left = 404
    Top = 291
    object qBoletosBOLETO_ID: TAutoIncField
      FieldName = 'BOLETO_ID'
      ReadOnly = True
    end
    object qBoletosCAMINHO_ARQUIVO: TStringField
      FieldName = 'CAMINHO_ARQUIVO'
      Size = 100
    end
    object qBoletosDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
    end
    object QBoletosFATURA_ID1: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object qBoletosDATA_ALTERACAO: TDateTimeField
      FieldName = 'DATA_ALTERACAO'
    end
    object qBoletosOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 50
    end
  end
end
