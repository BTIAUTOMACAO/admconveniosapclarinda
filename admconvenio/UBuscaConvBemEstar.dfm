object FBuscaConv: TFBuscaConv
  Left = 465
  Top = 153
  Width = 618
  Height = 473
  Caption = 'Busca Conveniado Bem Estar'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object JvDBGrid1: TJvDBGrid
    Left = 0
    Top = 23
    Width = 610
    Height = 351
    Align = alClient
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'ESPECIALIDADE_ID'
        Title.Caption = 'Cod. Especialidade'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Liberado'
        Width = 175
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LIBERADO'
        Visible = True
      end>
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 23
    Width = 610
    Height = 351
    Align = alClient
    TabOrder = 3
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 610
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Locked = True
    ParentFont = False
    TabOrder = 0
    object panTitulo: TPanel
      Left = 1
      Top = 1
      Width = 784
      Height = 20
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Localizar Conveniado'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object ButClose: TSpeedButton
        Left = 764
        Top = 1
        Width = 20
        Height = 18
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 374
    Width = 610
    Height = 72
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Label1: TLabel
      Left = 1
      Top = 3
      Width = 53
      Height = 13
      Caption = 'Cod. Conv '
      FocusControl = EdCod
    end
    object Label2: TLabel
      Left = 79
      Top = 3
      Width = 48
      Height = 13
      Caption = '&Descricao'
      FocusControl = EdNome
    end
    object Label3: TLabel
      Left = 241
      Top = 3
      Width = 66
      Height = 13
      Caption = 'Cod. Empresa'
      FocusControl = EdCodEmpresa
    end
    object Label4: TLabel
      Left = 319
      Top = 3
      Width = 71
      Height = 13
      Caption = '&Nome Fantasia'
      FocusControl = EdNomeFantasia
    end
    object ButBusca: TBitBtn
      Left = 2
      Top = 40
      Width = 81
      Height = 31
      Hint = 'Buscar cadastro'
      Caption = '&Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = ButBuscaClick
      Glyph.Data = {
        AE030000424DAE03000000000000AE0100002800000020000000100000000100
        08000000000000020000232E0000232E00005E00000000000000512600005157
        61007D5C4000666E7A006D768200717A880087572A00A7714500A5815F00A892
        7F00CE926100FF926100FFAB7700F0B47E00FFB47E00FFB67F0083888F008D92
        9A008792A1009CA3AD0096A2B3009BA7B900A2A3A400A8ABB000AAB2BC00B2B6
        BC00B6B7B800B9BABB00BABBBC00B6BAC100BFC4CC00BDCCE200FFBE8600D4C9
        BF00FFCBAA00FFDFA100FFE3B300FFEBBB00C1C2C300C2C4C700C4C5C500C4C7
        CA00C7C8C800C6C9CD00C8C9C900CACACB00CFCFD000CED1D600D1D1D200D3D4
        D400D4D5D500D1D5DA00D5D9DE00D9DADA00DADADB00DCDCDC00DCDDDD00DDDE
        DE00C8D8EF00CEDFF700DBECFF00FFF7D600FFF2DA00E0E0E100E1E1E100E2E2
        E200E2E3E500E3E4E400E8E8E800E8E9E900E9EAEA00E8EAED00ECECEC00EDED
        ED00EEEFEF00EFEFF000E4F6FF00FFF6E000FFF7E000FFFDE900FFFFE900F0F0
        F000F2F2F200F4F5F500F7F7F700F3FFFF00FFFFF700F8F8F800F9F9F900FAFA
        FA00FAFFFF00FCFCFC00FDFDFD00FFFFFF005D5D5D5D5D5D5D5D5D5D5D5D5D09
        09215D5D5D5D5D5D5D5D5D5D5D5D5D3030455D5D5D5D5D5D5D5D5D5D5D5D000A
        0D065D5D5D5D5D5D5D5D5D5D5D5D162E371A5D5D5D5D5D5D5D5D5D5D5D000A0D
        23065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D000A0D23
        065D5D5D5D5D5D5D5D5D5D5D162E37481A5D5D5D5D5D5D5D5D5D5D02070D2306
        5D5D5D5D5D5D5D5D5D5D5D1B2637481A5D5D5D5D5D5D5D5D5D5D5D010D0D065D
        5D5D5D5D5D5D5D5D5D5D5D1C37371A5D5D5D5D5D271001011027013B12085D5D
        5D5D5D5D442E1C1C2E441C53322C5D5D5D5D5D17041F5A5A1F045D125D5D5D5D
        5D5D5D392A495D5D492A5D325D5D5D5D5D5D4205554D23234D5505295D5D5D5D
        5D5D532D5D594848595D2D455D5D5D5D5D5D2B3A4E255656243E3A115D5D5D5D
        5D5D465259525D5D4B5752315D5D5D5D5D5D195D232323230E205D035D5D5D5D
        5D5D415D48484848383F5D285D5D5D5D5D5D1D5D230F0F0B0B0C5D045D5D5D5D
        5D5D435D4839393030355D2A5D5D5D5D5D5D2F3C4F3D5D5D220C3C135D5D5D5D
        5D5D49595C585D5D453559365D5D5D5D5D5D47145D50230B0C5D14335D5D5D5D
        5D5D54365D5C4830355D364A5D5D5D5D5D5D5D1E154C5D5D4C151E5D5D5D5D5D
        5D5D5D44385B5D5D5B38445D5D5D5D5D5D5D5D5D3418121218345D5D5D5D5D5D
        5D5D5D5D5140323240515D5D5D5D5D5D5D5D}
      NumGlyphs = 2
    end
    object EdNome: TEdit
      Left = 78
      Top = 18
      Width = 161
      Height = 21
      Hint = 'Busca por nome da especialiadade'
      CharCase = ecUpperCase
      TabOrder = 1
    end
    object EdCod: TEdit
      Left = 1
      Top = 18
      Width = 64
      Height = 21
      Hint = 'Busca por c'#243'digo da especialidade'
      AutoSelect = False
      CharCase = ecUpperCase
      TabOrder = 0
      OnKeyPress = EdCodKeyPress
    end
    object EdCodEmpresa: TEdit
      Left = 241
      Top = 18
      Width = 72
      Height = 21
      Hint = 'Busca por c'#243'digo da especialidade'
      AutoSelect = False
      CharCase = ecUpperCase
      TabOrder = 2
    end
    object EdNomeFantasia: TEdit
      Left = 318
      Top = 18
      Width = 161
      Height = 21
      Hint = 'Busca por nome da especialiadade'
      CharCase = ecUpperCase
      TabOrder = 3
    end
    object btnOk: TBitBtn
      Left = 93
      Top = 43
      Width = 77
      Height = 29
      Caption = '&OK'
      TabOrder = 5
      OnClick = btnOkClick
    end
    object btnCancelar: TBitBtn
      Left = 177
      Top = 43
      Width = 77
      Height = 29
      Cancel = True
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 6
      NumGlyphs = 2
    end
  end
  object grdConveniados: TJvDBGrid
    Left = 0
    Top = 23
    Width = 610
    Height = 351
    Align = alClient
    DataSource = DSConv
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 17
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'conv_id_be'
        Width = 86
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titular'
        Width = 187
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'empres_id'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fantasia'
        Visible = True
      end>
  end
  object QConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      'conv_be.conv_id_be, '
      'conv_be.empres_id, '
      'conv_be.titular,'
      'empresas.fantasia '
      'from conveniados_bem_estar conv_be'
      'inner join empresas ON empresas.empres_id = conv_be.empres_id'
      'where conv_id_be = 0')
    Left = 40
    Top = 96
    object QConvconv_id_be: TIntegerField
      FieldName = 'conv_id_be'
    end
    object QConvempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QConvtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QConvfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
  end
  object DSConv: TDataSource
    DataSet = QConv
    Left = 72
    Top = 96
  end
end
