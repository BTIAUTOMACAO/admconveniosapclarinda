inherited frmAESPOdontoDebitos: TfrmAESPOdontoDebitos
  Left = 313
  Top = 19
  Caption = 'Lan'#231'amento de Taxas AESP Odonto'
  ClientHeight = 510
  ClientWidth = 959
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 959
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 23
    Width = 959
    Height = 487
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Lanc. D'#233'bitos'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 951
        Height = 89
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 92
          Height = 13
          Caption = 'C'#243'digo da Empresa'
        end
        object BitBtn1: TBitBtn
          Left = 136
          Top = 16
          Width = 145
          Height = 41
          Caption = 'Buscar'
          TabOrder = 0
          OnClick = BitBtn1Click
          Glyph.Data = {
            7E090000424D7E0900000000000036000000280000001D0000001B0000000100
            1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
            A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
            EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
            AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
            596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
            A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
            D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
            C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
            89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
            D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
            C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
            EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
            F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
            BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
            FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
            EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
            99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
            FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
            D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
            BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
            FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
            C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
            ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
            F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
            FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
            C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
            FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
            D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
            BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
            CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
            D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
            C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
            D400}
        end
        object txtCodEmpresa: TEdit
          Left = 8
          Top = 32
          Width = 121
          Height = 21
          TabOrder = 1
          OnKeyPress = txtCodEmpresaKeyPress
        end
        object cbFechamentoAtual: TCheckBox
          Left = 8
          Top = 64
          Width = 265
          Height = 17
          Caption = 'LAN'#199'AR AUTORIZA'#199#195'O FECHAMENTO ATUAL'
          TabOrder = 2
        end
      end
      object JvDBGrid1: TJvDBGrid
        Left = 0
        Top = 89
        Width = 951
        Height = 319
        Align = alClient
        DataSource = DSConvBemEstar
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = JvDBGrid1ColExit
        OnDrawColumnCell = JvDBGrid1DrawColumnCell
        OnDblClick = JvDBGrid1DblClick
        OnTitleBtnClick = JvDBGrid1TitleBtnClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'CONV_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Width = 400
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Title.Caption = 'EMPRES ID'
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'liberado'
            Title.Caption = 'LIBERADO'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'valor'
            Title.Caption = 'VALOR R$'
            Width = 143
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'quantidade_aesp'
            Title.Caption = 'QTD.'
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 408
        Width = 951
        Height = 51
        Align = alBottom
        TabOrder = 2
        object Label84: TLabel
          Left = 4
          Top = 6
          Width = 70
          Height = 13
          Caption = 'Novo Valor R$'
        end
        object edtLimiteMes: TEdit
          Left = 4
          Top = 21
          Width = 93
          Height = 21
          TabOrder = 1
          Text = '0,00'
        end
        object btnAlterarTodosLimites: TButton
          Left = 110
          Top = 8
          Width = 94
          Height = 34
          Caption = 'Alterar p/ Todos'
          TabOrder = 0
          OnClick = btnAlterarTodosLimitesClick
        end
      end
    end
  end
  inherited PopupBut: TPopupMenu
    Left = 108
    Top = 48
  end
  object QConvBemEstar: TADOQuery
    Connection = DMConexao.AdoCon
    BeforeOpen = QConvBemEstarBeforeOpen
    AfterInsert = QConvBemEstarAfterInsert
    AfterPost = QConvBemEstarAfterPost
    Parameters = <
      item
        Name = 'empres_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select cart.CONV_ID,cart.CARTAO_ID,cart.NOME,cart.EMPRES_ID, coa' +
        'lesce(cae.LIBERADO,'#39'N'#39') liberado,'
      'coalesce(cae.valor,17.0) valor,cart.quantidade_aesp'
      'from CARTOES cart'
      'left join CONV_AESP_ODONTO CAE on cart.CONV_ID = cae.CONV_ID'
      'where cart.EMPRES_ID = :empres_id and cae.CONV_ID is null'
      'order by cart.nome')
    Left = 564
    Top = 199
    object QConvBemEstarCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
      KeyFields = 'CONV_ID'
    end
    object QConvBemEstarCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QConvBemEstarNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QConvBemEstarEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QConvBemEstarliberado: TStringField
      FieldName = 'liberado'
      Size = 1
    end
    object QConvBemEstarvalor: TBCDField
      FieldName = 'valor'
      DisplayFormat = '#,##0.00'
      Precision = 6
      Size = 2
    end
    object QConvBemEstarquantidade_aesp: TIntegerField
      FieldName = 'quantidade_aesp'
    end
  end
  object DSConvBemEstar: TDataSource
    DataSet = QConvBemEstar
    Left = 604
    Top = 199
  end
end
