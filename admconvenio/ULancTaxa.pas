unit ULancTaxa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ToolEdit, CurrEdit, {JvLookup,} Mask, JvToolEdit, DB,
  ExtCtrls, RxMemDS, UGeraTaxa, JvExControls,
  JvDBLookup, JvExMask, JvExStdCtrls, JvEdit, JvValidateEdit, ADODB;

type
  TFLancTaxa = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LabDeb: TLabel;
    LabCred: TLabel;
    Labhist: TLabel;
    Bevel1: TBevel;
    Data: TJvDateEdit;
    DBFornec: TJvDBLookupCombo;
    //Debito: TCurrencyEdit;
    //Credito: TCurrencyEdit;
    EdHist: TEdit;
    Button1: TButton;
    Button2: TButton;
    DSFornec: TDataSource;
    ChExpressao: TCheckBox;
    EdExpressao: TEdit;
    chExpressCredito: TCheckBox;
    Debito: TJvValidateEdit;
    Credito: TJvValidateEdit;
    QFornec: TADOQuery;
    QFornecCRED_ID: TIntegerField;
    QFornecNOME: TStringField;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ChExpressaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGeraTaxa : TFGeraTaxa;
    function CalcExpressao: Variant;
  end;

var
  FLancTaxa: TFLancTaxa;

implementation

uses DM, DateUtils, UExpression, calculadora;

{$R *.dfm}

procedure TFLancTaxa.Button2Click(Sender: TObject);
begin
FLancTaxa.ModalResult := mrCancel;
end;

procedure TFLancTaxa.Button1Click(Sender: TObject);
begin
  if Trim(DBFornec.Text) = '' then begin
    ShowMessage('Informe o fornecedor dos lan�amentos.');
    DBFornec.SetFocus;
    Exit;
  end;
  if Trim(EdHist.Text) = '' then begin
    ShowMessage('Informe o hist�rico dos lan�amentos.');
    EdHist.SetFocus;
    Exit;
  end;
  if not Owner.ClassNameIs('TFLancCPMF') then begin
    if (((debito.Value <= 0)  and (Credito.Value <= 0)) and (not ChExpressao.Checked)) then begin
      ShowMessage('N�o existe valor a ser lan�ado.');
      Exit;
    end;
  end;
  if Application.MessageBox('Confirma os lan�amentos?','Confirma��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDYes then
    FLancTaxa.ModalResult := mrOk;
end;

procedure TFLancTaxa.FormCreate(Sender: TObject);
begin
{TempCampos.Open;}//comentado ariane
QFornec.Open;
DMConexao.Config.Open;                        //Foi chamado pelo lan�amento de CPFM.
if Owner.ClassNameIs('TFLancCPMF') then DBFornec.KeyValue := DMConexao.ConfigCOD_CRED_CPMF.AsInteger
                                   else DBFornec.KeyValue := DMConexao.ConfigCOD_CRED_ADM.AsInteger; //Move o codigo do fornecedor de lan�amentos da administradora.
DMConexao.Config.Close;
data.Date := Date;
end;

procedure TFLancTaxa.FormKeyPress(Sender: TObject; var Key: Char);
begin
if key = #13 then begin
   Key := #0;
   Perform(WM_NEXTDLGCTL,0,0);
end;
end;

procedure TFLancTaxa.ChExpressaoClick(Sender: TObject);
begin
if ChExpressao.Checked then begin
   FExpression := TFExpression.Create(Self);
   {TempCampos.First;
   while not TempCampos.eof do begin
      if TFieldType(TempCamposTipo.AsVariant) in [ftFloat, ftCurrency, ftBCD,ftFMTBcd, ftSmallint, ftInteger, ftWord,ftAutoInc,ftLargeint] then begin
         FExpression.Campos.Items.Add(TempCamposCampo.AsString);
      end;
      TempCampos.Next
   end;  }//comentado ariane
   FExpression.ShowModal;
   if FExpression.ModalResult = mrOk then begin
      EdExpressao.Text := FExpression.Memo1.Text;
      Debito.Value     := 0;
      Credito.Value    := 0;
      Debito.Enabled   := False;
      Credito.Enabled  := False;
   end
   else begin
      ChExpressao.Checked := False;
      Debito.Enabled   := True;
      Credito.Enabled  := True;
   end;
end;   
chExpressCredito.Visible := ChExpressao.Checked;
end;

function TFLancTaxa.CalcExpressao: Variant;
var i : integer; marka : TBookmark;
valor, express, campo : string;
Calc : TCalculadora;
begin
express := EdExpressao.Text;
//marka := FTelaAltLinear.TempCampos.GetBookmark;
{TempCampos.First;
while not TempCampos.Eof do begin
   campo := AnsiUpperCase(TempCamposCampo.AsString);
   if Pos(campo,express) > 0 then begin
      valor := FloatToStr(ArredondaDin(FGeraTaxa.QConveniados.FieldByName(TempCamposFieldName.AsString).AsCurrency));
      valor := StringReplace(Valor,',','.',[rfReplaceAll]);
      express := StringReplace(express,'"'+campo+'"',valor,[rfReplaceAll]);
   end;
   TempCampos.Next;
end;   } //comentado ariane
//FTelaAltLinear.TempCampos.GotoBookmark(marka);
//FTelaAltLinear.TempCampos.FreeBookmark(marka);
Calc := TCalculadora.Create;
for i := 1 to length(express) do if not (express[i] in ['0'..'9','(',')','/','*','+','-','.']) then delete(express,i,1);
Calc.Calcular(express);
try
   StrToFloat(Calc.ResultStr);
   Result := Calc.ResultStr;
except
   Result := 0;
end;
Calc.Free;
end;


end.
