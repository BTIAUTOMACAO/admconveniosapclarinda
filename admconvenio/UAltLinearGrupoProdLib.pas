unit UAltLinearGrupoProdLib;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, Mask, ToolEdit, CurrEdit, {JvLookup,}
  ExtCtrls, {JvDBComb,} JvExControls, JvDBLookup;

type
  TFAltLinearGrupoProdLib = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    edtHistorico: TEdit;
    cbbGrupo: TJvDBLookupCombo;
    btnGravar: TBitBtn;
    btnCancelar: TBitBtn;
    DataSource1: TDataSource;
    rdgLib: TRadioGroup;
    procedure btnGravarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAltLinearGrupoProdLib: TFAltLinearGrupoProdLib;

implementation

uses cartao_util, DM;

{$R *.dfm}

procedure TFAltLinearGrupoProdLib.btnGravarClick(Sender: TObject);
begin
  if cbbGrupo.KeyValue = 0 then
  begin
    MsgInf('Selecione um grupo de produto para a altera��o!');
    cbbGrupo.SetFocus;
  end
  else if edtHistorico.Text = '' then
  begin
    MsgInf('Insira um historico para a altera��o!');
    edtHistorico.SetFocus;
  end
  else
    FAltLinearGrupoProdLib.ModalResult:= mrOk;
end;

procedure TFAltLinearGrupoProdLib.FormCreate(Sender: TObject);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('select grupo_prod_id, descricao from grupo_prod where apagado = ''N'' order by grupo_prod_id ');
  DMConexao.AdoQry.Open;
end;

procedure TFAltLinearGrupoProdLib.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DMConexao.AdoQry.Close;
end;

procedure TFAltLinearGrupoProdLib.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

end.
