inherited FrmCadProdutosAutorizador: TFrmCadProdutosAutorizador
  Left = 259
  Top = 93
  Caption = 'Cadastro de Produtos Autorizador'
  ClientWidth = 811
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel [0]
    Left = 360
    Top = 296
    Width = 32
    Height = 13
    Caption = 'Label7'
  end
  inherited PageControl1: TPageControl
    Width = 811
    ActivePage = TabFicha
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 803
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 719
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 803
      end
      inherited Barra: TStatusBar
        Width = 803
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Width = 803
      end
      inherited Panel3: TPanel
        Width = 803
        object Label3: TLabel
          Left = 24
          Top = 24
          Width = 73
          Height = 13
          Caption = 'C'#243'digo Produto'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 128
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Descri'#231#227'o'
          FocusControl = DBEdit2
        end
        object Label5: TLabel
          Left = 24
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Laborat'#243'rio'
          FocusControl = DBEdit3
        end
        object Label6: TLabel
          Left = 408
          Top = 72
          Width = 28
          Height = 13
          Caption = 'Pre'#231'o'
          FocusControl = DBEdit4
        end
        object Label8: TLabel
          Left = 568
          Top = 72
          Width = 51
          Height = 13
          Caption = 'Pre'#231'o 19%'
          FocusControl = DBEdit4
        end
        object DBEdit1: TDBEdit
          Left = 24
          Top = 40
          Width = 97
          Height = 21
          DataField = 'PROD_CODIGO'
          DataSource = DSCadastro
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 128
          Top = 40
          Width = 573
          Height = 21
          DataField = 'PROD_DESCR'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 24
          Top = 88
          Width = 377
          Height = 21
          DataField = 'LABORATORIO'
          DataSource = DSCadastro
          TabOrder = 2
        end
        object db: TDBCheckBox
          Left = 24
          Top = 120
          Width = 161
          Height = 17
          Hint = 
            'Para produtos pertencentes '#224' lista ABC  FARMA,'#13#10'a altera'#231#227'o de p' +
            're'#231'os n'#227'o '#233' permitida'
          Caption = 'Altera'#231#227'o de Pre'#231'o Liberada'
          DataField = 'ALT_LIBERADA'
          DataSource = DSCadastro
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object DBEdit4: TDBEdit
          Left = 408
          Top = 88
          Width = 145
          Height = 21
          DataField = 'PRE_VALOR'
          DataSource = DSCadastro
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 568
          Top = 88
          Width = 134
          Height = 21
          DataField = 'PRE_VALOR_19'
          DataSource = DSCadastro
          TabOrder = 4
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 803
      end
      inherited GridHistorico: TJvDBGrid
        Width = 803
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 811
  end
  inherited panStatus2: TPanel
    Width = 811
  end
  inherited DSCadastro: TDataSource
    Left = 284
    Top = 296
  end
  inherited QHistorico: TADOQuery
    Left = 316
    Top = 265
  end
  inherited DSHistorico: TDataSource
    Left = 316
    Top = 297
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select  *  from produtos_autorizador where prod_aut_id = 0')
    Left = 284
    Top = 265
    object QCadastroPROD_CODIGO: TStringField
      DisplayLabel = 'C'#243'd. Produto'
      DisplayWidth = 16
      FieldName = 'PROD_CODIGO'
      Size = 13
    end
    object QCadastroPROD_DESCR: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'PROD_DESCR'
      Size = 45
    end
    object QCadastroLABORATORIO: TStringField
      DisplayLabel = 'Laborat'#243'rio'
      FieldName = 'LABORATORIO'
      Size = 15
    end
    object QCadastroPRE_VALOR: TFloatField
      DisplayLabel = 'Pre'#231'o'
      FieldName = 'PRE_VALOR'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
    end
    object QCadastroPRE_VALOR_19: TBCDField
      DisplayLabel = 'Pre'#231'o 19%'
      DisplayWidth = 10
      FieldName = 'PRE_VALOR_19'
      DisplayFormat = '###,###,##0.00'
      EditFormat = '########0.00'
      Precision = 15
      Size = 2
    end
    object QCadastroALT_LIBERADA: TStringField
      DisplayLabel = 'Alt. Pre'#231'o'
      FieldName = 'ALT_LIBERADA'
      Size = 1
    end
    object QCadastroPROD_AUT_ID: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'PROD_AUT_ID'
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroOPERCADASTRO: TStringField
      DisplayLabel = 'Op. Cadastro'
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroAPAGADO: TStringField
      DisplayLabel = 'Apagado'
      FieldName = 'APAGADO'
      Visible = False
      Size = 1
    end
    object QCadastroDTAPAGADO: TDateTimeField
      DisplayLabel = 'Dt. Apagado'
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      DisplayLabel = 'Dt. Altera'#231#227'o'
      FieldName = 'DTALTERACAO'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'DTCADASTRO'
    end
  end
end
