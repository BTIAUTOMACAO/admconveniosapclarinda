inherited frmPgtoEstabAberto: TfrmPgtoEstabAberto
  Left = 172
  Top = 43
  Caption = 'Pagamento de Estabelecimentos'
  ClientHeight = 618
  ClientWidth = 1044
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel6: TPanel
    Width = 1044
  end
  object PanAbe: TPanel [1]
    Left = 0
    Top = 23
    Width = 1044
    Height = 154
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Bevel4: TBevel
      Left = 306
      Top = -8
      Width = 6
      Height = 193
    end
    object Bevel2: TBevel
      Left = 704
      Top = -7
      Width = 9
      Height = 193
    end
    object Label1: TLabel
      Left = 8
      Top = 54
      Width = 78
      Height = 13
      Caption = 'Estabelecimento'
    end
    object cbbEstab: TJvDBLookupCombo
      Left = 63
      Top = 71
      Width = 234
      Height = 21
      Hint = 'Consulta pelo nome do estabelecimento'
      DropDownWidth = 350
      DisplayEmpty = 'Todos estabelecimentos'
      EmptyValue = '0'
      FieldsDelimiter = #0
      LookupField = 'CRED_ID'
      LookupDisplay = 'NOME'
      LookupSource = dsEstab
      TabOrder = 10
      OnChange = cbbEstabChange
    end
    object edtEstab: TEdit
      Left = 8
      Top = 71
      Width = 40
      Height = 21
      Hint = 'Consulta pelo c'#243'digo do fornecedor'
      TabOrder = 9
      OnChange = edtEstabChange
      OnKeyPress = edtEstabKeyPress
    end
    object btnConsultar: TButton
      Left = 712
      Top = 59
      Width = 106
      Height = 90
      Hint = 'Consultar pagamentos em aberto'
      Caption = 'Consultar'
      TabOrder = 8
      OnClick = btnConsultarClick
    end
    object gpbDatas: TGroupBox
      Left = 318
      Top = 55
      Width = 379
      Height = 50
      Caption = 'Data da Baixa Fatura'
      TabOrder = 5
      object lblDe: TLabel
        Left = 2
        Top = 23
        Width = 14
        Height = 13
        Caption = 'De'
      end
      object lblA: TLabel
        Left = 125
        Top = 21
        Width = 7
        Height = 13
        Caption = #192
      end
      object ckbTodasDatas: TCheckBox
        Left = 231
        Top = 7
        Width = 81
        Height = 17
        Hint = 'Consultar todas as datas'
        Caption = 'Todas datas'
        TabOrder = 0
        Visible = False
      end
      object cbAtrasadas: TCheckBox
        Left = 232
        Top = 24
        Width = 137
        Height = 17
        Hint = 
          'Consultar tods os pagamentos incluindo os atrasados. esta op'#231#227'o ' +
          'serve apenas visualiza'#231#227'o de taxas Corrente e Atrasadas.'#13#10'Ou sej' +
          'a n'#227'o '#233' poss'#237'vel baixar pagamentos com esta op'#231#227'o selcionada.'
        Caption = 'Visualizar Atrasadas'
        TabOrder = 4
        Visible = False
      end
      object edtDia: TEdit
        Left = 76
        Top = 16
        Width = 43
        Height = 21
        TabOrder = 2
        OnKeyPress = edtDiaKeyPress
      end
      object dataIni: TJvDateEdit
        Left = 25
        Top = 16
        Width = 90
        Height = 21
        Hint = 'Data inicial da baixa da fatura'
        ShowNullDate = False
        TabOrder = 1
      end
      object DataFin: TJvDateEdit
        Left = 139
        Top = 16
        Width = 90
        Height = 21
        Hint = 'Data inicial da baixa da fatura'
        ShowNullDate = False
        TabOrder = 3
      end
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 4
      Width = 289
      Height = 43
      Caption = 'Estabelecimentos com Pagamento por:'
      TabOrder = 0
      object cbbPgtoPor: TJvDBLookupCombo
        Left = 2
        Top = 16
        Width = 282
        Height = 21
        Hint = 'Selecione a forma de pagamento para pesquisa'
        DeleteKeyClear = False
        EmptyValue = '0'
        LookupField = 'PAGA_CRED_POR_ID'
        LookupDisplay = 'DESCRICAO'
        LookupSource = dsPgtoPor
        TabOrder = 0
        OnChange = cbbPgtoPorChange
      end
    end
    object panFatura: TPanel
      Left = 1192
      Top = 120
      Width = 49
      Height = 17
      BevelOuter = bvNone
      TabOrder = 14
      object Label3: TLabel
        Left = -8
        Top = 56
        Width = 45
        Height = 13
        Caption = 'Fatura N'#186
      end
      object edtFatura: TEdit
        Left = 104
        Top = 56
        Width = 87
        Height = 21
        Hint = 
          'Insira o n'#250'mero da fatura'#13#10'(Nessa consulta, todas as outras info' +
          'rma'#231#245'es ser'#227'o descartadas)'
        TabOrder = 0
      end
    end
    object rdgAutoriz: TRadioGroup
      Left = 712
      Top = 8
      Width = 241
      Height = 43
      Caption = 'Autoriza'#231#245'es'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Todas'
        'Confirmadas')
      TabOrder = 2
    end
    object rdgDataPor: TRadioGroup
      Left = 1159
      Top = 12
      Width = 242
      Height = 37
      Caption = 'Busca de data por'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Per'#237'odo de venda'
        'Data de fechamento')
      TabOrder = 4
      Visible = False
      OnClick = rdgDataPorClick
    end
    object GroupBox1: TGroupBox
      Left = 318
      Top = 7
      Width = 379
      Height = 43
      Caption = 'Data de Compensa'#231#227'o Fatura'
      TabOrder = 1
      object Label4: TLabel
        Left = 2
        Top = 23
        Width = 14
        Height = 13
        Caption = 'De'
      end
      object Edit1: TEdit
        Left = 76
        Top = 16
        Width = 43
        Height = 21
        TabOrder = 0
        Visible = False
        OnKeyPress = edtDiaKeyPress
      end
      object dataCompensa: TJvDateEdit
        Left = 23
        Top = 17
        Width = 90
        Height = 21
        Hint = 'Data inicial da baixa da fatura'
        AutoSelect = False
        ShowNullDate = False
        TabOrder = 1
        OnChange = dataCompensaChange
        OnEnter = dataCompensaEnter
        OnExit = dataCompensaExit
      end
      object cbbSetManual: TCheckBox
        Left = 128
        Top = 18
        Width = 121
        Height = 17
        Hint = 
          'Ao selecionar est'#225' op'#231#227'o o operador pode manipular as datas conf' +
          'orme o desejado.'#13#10'Com o combo deslcelecionado o sistema busca au' +
          'tomaticamente as datas de acordo com o tipo '#13#10'de pagamento escol' +
          'hido.'
        Caption = 'Setar Manualmente'
        TabOrder = 2
      end
    end
    object rdgPagamentos: TRadioGroup
      Left = 712
      Top = 56
      Width = 241
      Height = 50
      Caption = 'Pagamentos'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Todos'
        'Pendentes')
      TabOrder = 6
      Visible = False
      OnClick = rdgPagamentosClick
    end
    object rbEstab: TRadioGroup
      Left = 8
      Top = 104
      Width = 289
      Height = 41
      Align = alCustom
      Caption = 'Tipo Estab.'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Mercado'
        'Posto')
      TabOrder = 11
      Visible = False
      OnClick = rbEstabClick
    end
    object GroupBox3: TGroupBox
      Left = 1158
      Top = 56
      Width = 379
      Height = 49
      Caption = 'Manuten'#231#227'o de Taxa Banc'#225'ria'
      TabOrder = 7
      Visible = False
      object Label2: TLabel
        Left = 2
        Top = 24
        Width = 14
        Height = 13
        Caption = 'R$'
      end
      object btnTaxaBanco: TButton
        Left = 128
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Aplicar Taxa'
        Enabled = False
        TabOrder = 0
        OnClick = btnTaxaBancoClick
      end
      object Button1: TButton
        Left = 208
        Top = 16
        Width = 81
        Height = 25
        Hint = 
          'Valor em R$ para taxas banc'#225'rias(O valor carregado por padr'#227'o '#233' ' +
          'sugerido podendo o mesmo sr alterado)'
        Caption = 'Remover Taxas'
        Enabled = False
        TabOrder = 1
        OnClick = Button1Click
      end
    end
    object rgbVendas: TRadioGroup
      Left = 960
      Top = 8
      Width = 169
      Height = 97
      Caption = 'Vendas'
      ItemIndex = 0
      Items.Strings = (
        'COM VENDA NO PER'#205'ODO'
        'SEM VENDA NO PER'#205'ODO')
      TabOrder = 3
      Visible = False
      OnClick = rdgPagamentosClick
    end
    object GroupBox4: TGroupBox
      Left = 960
      Top = 106
      Width = 289
      Height = 43
      Caption = 'Selecione a Conta Sacada'
      TabOrder = 12
      Visible = False
      object cbbBancoSacado: TJvDBLookupCombo
        Left = 2
        Top = 16
        Width = 282
        Height = 21
        Hint = 'Selecionea a conta da administradora para efetuar  o repasse'
        DeleteKeyClear = False
        EmptyValue = '0'
        LookupField = 'conta_id'
        LookupDisplay = 'nome_convenio'
        LookupSource = DSBancoSacado
        TabOrder = 0
      end
    end
    object JvDBComboBox1: TJvDBComboBox
      Left = 320
      Top = 112
      Width = 233
      Height = 21
      Items.Strings = (
        'RPC'
        'CDC'
        'PT')
      TabOrder = 13
      ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
      ListSettings.OutfilteredValueFont.Color = clRed
      ListSettings.OutfilteredValueFont.Height = -11
      ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
      ListSettings.OutfilteredValueFont.Style = []
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 384
    Width = 1044
    Height = 180
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object Splitter1: TSplitter
      Left = 1041
      Top = 0
      Height = 180
      Align = alRight
    end
    object Splitter2: TSplitter
      Left = 0
      Top = 24
      Width = 801
      Height = 9
      Align = alCustom
    end
    object Panel4: TPanel
      Left = 561
      Top = 0
      Width = 480
      Height = 180
      Align = alRight
      Anchors = [akTop, akBottom]
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 0
        Top = 32
        Width = 450
        Height = 21
        Align = alCustom
        Alignment = taLeftJustify
        Anchors = [akTop]
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = ' Taxas e Descontos do Estabelecimento'
        TabOrder = 0
      end
      object GridDescontos: TJvDBGrid
        Left = 0
        Top = 56
        Width = 553
        Height = 124
        Align = alCustom
        DataSource = dsPgtoDesc
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        PopupMenu = popCancTaxa
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GridDescontosDblClick
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'valor'
            Title.Caption = 'Valor R$'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CRED_ID'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'taxa_id'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'marcado'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id'
            Title.Caption = 'ID'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TAXAS_PROX_PAG_ID_FK'
            Visible = False
          end>
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 552
      Height = 180
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      object Image13: TImage
        Left = 1
        Top = 4
        Width = 37
        Height = 25
        Cursor = crHandPoint
        Hint = 'Confer'#234'ncia de Notas'
        AutoSize = True
        Center = True
        Picture.Data = {
          0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000002500
          0000190802000000394470820000001974455874536F6674776172650041646F
          626520496D616765526561647971C9653C0000032269545874584D4C3A636F6D
          2E61646F62652E786D7000000000003C3F787061636B657420626567696E3D22
          EFBBBF222069643D2257354D304D7043656869487A7265537A4E54637A6B6339
          64223F3E203C783A786D706D65746120786D6C6E733A783D2261646F62653A6E
          733A6D6574612F2220783A786D70746B3D2241646F626520584D5020436F7265
          20352E332D633031312036362E3134353636312C20323031322F30322F30362D
          31343A35363A32372020202020202020223E203C7264663A52444620786D6C6E
          733A7264663D22687474703A2F2F7777772E77332E6F72672F313939392F3032
          2F32322D7264662D73796E7461782D6E7323223E203C7264663A446573637269
          7074696F6E207264663A61626F75743D222220786D6C6E733A786D703D226874
          74703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F2220786D6C6E
          733A786D704D4D3D22687474703A2F2F6E732E61646F62652E636F6D2F786170
          2F312E302F6D6D2F2220786D6C6E733A73745265663D22687474703A2F2F6E73
          2E61646F62652E636F6D2F7861702F312E302F73547970652F5265736F757263
          65526566232220786D703A43726561746F72546F6F6C3D2241646F6265205068
          6F746F73686F7020435336202857696E646F7773292220786D704D4D3A496E73
          74616E636549443D22786D702E6969643A353046383130314536303237313145
          34393145334146443636444336304243302220786D704D4D3A446F63756D656E
          7449443D22786D702E6469643A35304638313031463630323731314534393145
          33414644363644433630424330223E203C786D704D4D3A446572697665644672
          6F6D2073745265663A696E7374616E636549443D22786D702E6969643A353046
          3831303143363032373131453439314533414644363644433630424330222073
          745265663A646F63756D656E7449443D22786D702E6469643A35304638313031
          44363032373131453439314533414644363644433630424330222F3E203C2F72
          64663A4465736372697074696F6E3E203C2F7264663A5244463E203C2F783A78
          6D706D6574613E203C3F787061636B657420656E643D2272223F3ED19A98FB00
          0006354944415478018D564B6F5B45149EC7BDBE8E5DDB6D9D871DE7D1BC1A2A
          DAA64D52503608898A2508F103D8402416482CBA43BCCA0214166C600B08890D
          1208556253554255A596364949881AB510A74D9CC4AFA479D88EEDFB98E19B7B
          9D5BD31684753CF7CCCC39E73BE7CC99732F955212429636B672A5BA250875A7
          9410AA7ED869268A75823F349A4757441257135B84A8C1B30B9E529D9144C418
          EC8C43900A21560ADBA59A138F86C2010D026AD5B3EC8E98FE7F3A40511A073C
          AD98F6D6DE7E24C87BDB8F50DBB6679736926D475B232D084908E526BC57BC55
          275240159A98826926C8B5E8F4DA5AA52D1A1A3C1AF2B720ECF3601825B0592C
          55B3C58763839DD434CD1B77D74FF5274346A06E4B4748178C7046A5B5DF4812
          6CBB1986BE4FB07B28C02F2DED7EB3B0F5D9F9813E40AA3C36F6557A2590702E
          24A0D172B5BEB09C9D782645EBF5FA8D7BEB2303294DD34C4BC21721814975CE
          34671F73CF00E20380377A2B18C33ABB94DE9BBA99EB0AF18BE787FA8E86051C
          048C24B6904CD83AD75012C0ABD5CDB9F4FAC4708AD66AB5E974FE646F82326E
          D9806A3889F8B85341ACB00B188C4F92C1E9B595ED0FAEAEC2F461837EF9DA58
          7B240C7D1014713252388C718DD19A69FE713FFBFC5052E1CDA4F327804799ED
          20868659D40AE2A3443A8E039F1AAB4D0FE42368188E701E6CEEC14D183D128B
          45233101348243A194A1F804679C735A37CD051F6F76B930DCD34124C3E13519
          249A534194DBDBDB7373730811E4EFC289D6D6D653A74F23E1010EC36AC7D25B
          2C1B8121A76A8A3F6A056EE9C0B3AD3B3EDEED747EA8274108C3E141C82378C9
          AD32143065AC61D10BDF03060FC26E83181344939624990C193AEE2D42803155
          0AB6B0EF3EC89EF3F2399BCE1FEF76F1B0EF0A020C4F6E95887060FD60196BFF
          4A9473E168E6B75FD35F7ED6DE7893BCFA3AD134152C714B5D8A3F33B9B1810E
          757EC01BEA42BD34F289E2071E48337791CF9D9D9DF9F979A08280066C8F010F
          42BB300C636C6C941B51BEFD9BBD51A97F77852C4CEB2FBC48DEBB080140E20A
          22C54BEBF9D1FE768537B3941BEC4EE058512F48A93CB0073C1418007047A1E9
          13F6B1D83C0DB444793513F8F5958A1693CFBECFAE6EDA7333EC8BAF0086BA03
          A16FDCDF289CED73F16EFD953DDE033CDDB205EA8D52AE249087FA0EF0707838
          73DFFA938C24D416CCB8F996B972A55677D02EB5D10BB4EF6D118A78C20C0E12
          E74176F3CCB15615DFF45276A03BA131CD069E941447EC0AD2EA43E473777777
          7171112A2077F91F03EA538FB48F876EB1DF3F344918E96554D8272FF091777D
          39D77BB99A2B9EE93DC0EBEF4A6AB8EF8EEA968FECEE6FE1FE5996552E977DE5
          C718C9025A69313EFF8E70D052086EB7797884BDFC03A168FD0D59E021FF6B85
          E2488F8F974A708DDB36D61FC5272B9BC83BF21908041AAA4F3EB443DA9DA9FA
          F4275A382108739861BFF423890D03C397C5294A2932F9E299636D2A9FB7D3B9
          DECE0EF433140B12A2F2E9C628CA9B68DA086E797919A8BEBECFC0BB40C09898
          784E2C7D6FCE7C2ACC52EDF447E4C4246A1BEFC06648F8BD962F8EF8783D9D1D
          8C69C09352282CF5274EB948A540432F140AEE820FD46080073F4647C7359D97
          D76E95EFFD24C73E26AA8F3504FC07CC66B2F9B3FDEEFD437CDDC90434A1DF6C
          1778B8EF580F0683D8F295C1400C9940DDE26A7E8EDFD454BCA3DB746C1E6CD4
          24643C82A22BEC006F7420E1E5339F4AB66B94E3A5853D4F0E23F0105FB55ACD
          E5729882B00B7D3018C3E1706F6FEFF5EBD72F5FBE3C3939198FC7A5DEC28CA7
          E1116A497B7DA33036E8E2CDA6B35DC90EA6F0D4D709AE0B2C8280C788AC542A
          ABABAB40C28A37020CC145A3D1542A0506C068082863668459300A319F709010
          764715DFF880FB3EC2FDEB4B25DD6B2B3905240A0A2A92EC6FA115010379C3FC
          310212BE45B0EBAFABABDE12F3A6C0400F834D470A4D5D68797F3D7B6ED0C5C3
          FB2FD5D116D075DB2D1886D796EAA17237B782FA84839E89A78EC0838037EAA1
          58281607929AE2FB034502E7F17E60CCB4ACF57C711CFD1AE5772F53107AE848
          248C7723DEB8B8E34F35FD1F8B5E427C01407A3CE2436F448FDC2E5598B53FDC
          DDAEBE97F6CA95CC5689F020E70CCE7A39745FD370CF537447583DB074F074D7
          DDC1BD6D9070275E86DC11B13A685B4EAD3B1E891E0AFF0D414A80345B11CFA2
          0000000049454E44AE426082}
        OnClick = Image13Click
      end
      object Image4: TImage
        Left = 39
        Top = 4
        Width = 37
        Height = 25
        Cursor = crHandPoint
        Hint = 'Cadastro de Estabelecimentos'
        AutoSize = True
        Center = True
        Picture.Data = {
          0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000002500
          0000190802000000394470820000001974455874536F6674776172650041646F
          626520496D616765526561647971C9653C0000032269545874584D4C3A636F6D
          2E61646F62652E786D7000000000003C3F787061636B657420626567696E3D22
          EFBBBF222069643D2257354D304D7043656869487A7265537A4E54637A6B6339
          64223F3E203C783A786D706D65746120786D6C6E733A783D2261646F62653A6E
          733A6D6574612F2220783A786D70746B3D2241646F626520584D5020436F7265
          20352E332D633031312036362E3134353636312C20323031322F30322F30362D
          31343A35363A32372020202020202020223E203C7264663A52444620786D6C6E
          733A7264663D22687474703A2F2F7777772E77332E6F72672F313939392F3032
          2F32322D7264662D73796E7461782D6E7323223E203C7264663A446573637269
          7074696F6E207264663A61626F75743D222220786D6C6E733A786D703D226874
          74703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F2220786D6C6E
          733A786D704D4D3D22687474703A2F2F6E732E61646F62652E636F6D2F786170
          2F312E302F6D6D2F2220786D6C6E733A73745265663D22687474703A2F2F6E73
          2E61646F62652E636F6D2F7861702F312E302F73547970652F5265736F757263
          65526566232220786D703A43726561746F72546F6F6C3D2241646F6265205068
          6F746F73686F7020435336202857696E646F7773292220786D704D4D3A496E73
          74616E636549443D22786D702E6969643A454344364131424436343046313145
          34423036464646313242373435303236462220786D704D4D3A446F63756D656E
          7449443D22786D702E6469643A45434436413142453634304631314534423036
          46464631324237343530323646223E203C786D704D4D3A446572697665644672
          6F6D2073745265663A696E7374616E636549443D22786D702E6969643A454344
          3641314242363430463131453442303646464631324237343530323646222073
          745265663A646F63756D656E7449443D22786D702E6469643A45434436413142
          43363430463131453442303646464631324237343530323646222F3E203C2F72
          64663A4465736372697074696F6E3E203C2F7264663A5244463E203C2F783A78
          6D706D6574613E203C3F787061636B657420656E643D2272223F3E5EEC98D000
          000809494441547801AD96796C5CC51DC767E65D7BFB5C7BED8DCF752E831368
          02491A1A1225E5501225112805B5A8820269D456ADA8840AFFF04FFEA95421F5
          500F0968A904AA04E12A9443A8A53421B593E090D3C7DA9BB5B3F7FADAFB1D33
          AFDFE74D82CB9F5557B34FF3E6CDFC3EF3FBCDEF186ADB3621249A9C4B177553
          10BAFC4A31449D0721F5EF78FF1F9B4DA9C248C8AF0D74B640041542C4B30BC5
          1A6F0978BCAA0C00466D5287A18B66AF7CC1FB579AB3DFEB43F5895F994FCB86
          3557A8F85D524F5B13B52CEB6C34D9116C0EFADD8C512E88250417B680186029
          C8B45C5ACA67D3954AC5EF0F288AE276BBF1515535CA2426C9F811CAAE130981
          5D2446D1200D7C46891076AE584DE5E6370D7452C3304E8D2586FA3B7C6EAD66
          DA16074CD417835B5C9CBF168F9D3D3D3C353595CBE5344DF3783C78A201E372
          B93C5E6F20D0D0D0D0D8126C75B9DC92A498A6E1F1FA226BD60389DD82AA29B4
          54D52F4CA7B6AD0B535DD74F8D276E8B84354DADEA9CC3BE4298865E2E2C1416
          E652C944341A05096A0180CD99A60993F0E589E8E0787D3EDFEA8181E6D660B9
          58CCE7E7F58A31B4E9D6AD3BBE49F183E28CBA5556D38D7353896D6BC3B456AB
          0D4FA636F475688A5A3139D603F6EF4F3F29150AB2C4644556550524580ECBF1
          154858C0263636069ED7EB658CE572F9F9F48241AC22598859A7B66F39F4AD6D
          DF879118D650EA5658CD30CEC7525B56775CE70DF575A88A5233380EC612562C
          F7C54C2CEA911B655B11D0C520DC30B9E97401B3D0139CE2902456ABD4F2D945
          83E98B2C93A1276CED731723F76C7E71F7D023E0A14994BA5409BC0B2B79B740
          3F59A9991C334AFAE23BB11F74F9BDAA1496ED3661796CD3CBB8C2A886601126
          1116B37595D428AFEAE7A3A369767949FE97CB73ADCBEF09BB77F8C4AE81C8FD
          BD91751085060D5D8A54338D4B37792393A9F5BD1D9AACE896C3A3C4AEEAD392
          94B6A92913179354C2A82E2CD35A2A1AB96C359E2DC616AB698BABE3196922FA
          F18670A13FB0B64DD9E3255B15DEA9576BE1D59155BDFD80C15A78BA1D9E79E5
          6AEACEBA3DC11BEC59B6A7C9E1CDCE0C2A5482D0B005415C0804BD65734E4D21
          6AC2CE168DB192882F99B157463FF3945A0E451E96F94652F31051CA9AFAFBC9
          F477B6DFB52EDC0539588C277886695E8EAFE0ADEFEE54544937C432CFCEEA65
          AF461B1971132263C82660729B62BF36428B4808332259D1F8787AB6106A0A5A
          46E9AA657CBC58FA7026E159587AFBE8119FE682F1E10E084197CA4C835F9949
          7EA9DF9AEE90067FB18444A197F8E999D1314ADB1BBC9B1B032195B4BBB51E59
          6961D44F891BF989509912C6A44C323B119FC937F88FCF154E4CC4BB45BE299E
          FFC981FD43B7DF09189A65239D39FEA99BE6C44CFABF788AAC98DCE1C1025C58
          9752B3C363D13F8FE696828392BBD2D4A2059A94668FDAE2515D6E8F8F291E6A
          19D333FF8C27BE28D5E8F070F7F6F895FDEF3E7AFEC891D62723EB8620043C84
          97B0A94B61A6B582773A9A8A7485544946E281F130152ED3ECE2972E5E79F6D5
          D3E5F6BB6D459D9D2BE9B2CB124814B35E7752ED6824A1706632DA5B9AEC2D2E
          C20AAF3EF407C253A444465CE7FA7A062104AD8E542466706B6A367DC7C072FC
          81D7BFAA4395258433CC8D79E0F914E3C2E5B1DFBFFEFE867BF736ADEA1B79EB
          F58EDBEF3F1BD7E39313FE55F6D76BB1C7F7EF1D4BC64A0DB3AD2D726C26F384
          E73921158949CE05C6C3A13E08A9371C21129B61F1E96BA915BC7048917176F5
          39C8D13678B158FCD80B2FF4EDDED5D43798FCE8839D0F3E7C3239377AF6723C
          D073207BFAE96F1F9CC95C7C23A9AF6AEF2E17269F721F222A219CFCDD33B226
          3408C7D6A886FC021785CD4CF01237F43B1B4DF5822749F850073AFAC9462A95
          FED9EF9E0F6EEB6B8DB4263E4AF6DFF7F85B337979EABD7273E37D79FDD1035B
          DEAEFCE669FB35C26BD82081EFD617A383C6C82F82BF7DC4F73DA46CBC599C5F
          4DA437D5ED095E4F38047BD2FA02C7D949B3D79ECFE71F3BF62B69CB6EB67E8D
          F1E19B3BF73D703C5E50678F8BDED2AE78F0969D771C2EEC22CAF26C2C84D49B
          EBD1C7082763914C93D484B266712BBE92D7DD8978906ECEE7DCFAFCD43FA82D
          7EF9E22BF9AD7BD9E61D2D27FE7AEFA17DAFC597EC44ACD8B9FAC0ECF06307F7
          BEB4F0C6B1F251E25A2ED2DC3126288EAECCE9FFBAFDE5C30D0FC16596797C26
          F9A57EE970679B5B9219E6392B48AD5A79E6A91FA274886AF9D2E06E6DC7FEC6
          E177F61C7CE0DD4CD5F5B73F564AFACEBED08F9EF86E62A970F293F7264E7F96
          2F26DE7C7684D49CC52782E77A8211E4078D698C50941CA4504358896476D340
          C8A90F67A6525D1DED126A25A278F90C6D5B8C8E9CBA74F17C2679EDD3D9C5B9
          F64875EA72CF37EEC96A4DE1937FE9A7554BF3069A1A4AE5F274348A84D9E8E3
          1F3C7F06C180CC135F57F4481AC8D0CCE042E74291E0F97C3695D91CB9110F7D
          E10E688D5B8426215FDD509310542094C35C26158BC5C6AFCE8C27B30BD3637E
          05B7101367B430BF582C15376CDCB87BDF9EC39D0F8201FF8CAD5EF4491EC06A
          9C1B48BBB6ED51908EEC58E2463C9C99CA84DB83A87FC8C7B669203C2509D700
          E7345151916A1197B2242BAA02BFAD964B8B0BF30B73F9E8E44422716DFB5D77
          DF7ADBD724557B69EE4FCFA49E3C1AFCF1736D3F2784E112640982E083209531
          084964729B23EDCE7D627C36CB6577C0E3A294C1EE100A8B3B34D40514722802
          A776DC00172FCA3048EBB746D40EA4478230E244947961C92A344A01AFDC2804
          D483DF3A32708438A2A572959995B55D6DCE7DA9502A4F670B2622D3B1C8FFFF
          0FCFD098DDD5E20FF8BCFF0190809443BF7DF1A80000000049454E44AE426082}
        OnClick = Image4Click
      end
      object GridLog: TJvDBGrid
        Left = 0
        Top = 56
        Width = 547
        Height = 119
        Hint = 'Use o bot'#227'o direito para excluir alguma empresa do pagamento.'
        Align = alCustom
        Anchors = [akLeft, akTop, akRight]
        DataSource = DSPgtoEmp
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GridLogDrawColumnCell
        AutoAppend = False
        TitleButtons = True
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 16
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'PAGAMENTO_CRED_ID'
            Title.Caption = 'Paga Cred '
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOTE_PAGAMENTO'
            Title.Caption = 'Cod Relat'#243'rio(Lote)'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_HORA'
            Title.Caption = 'Data Pgto'
            Width = 119
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_COMPENSACAO'
            Title.Caption = 'Data Compensa'
            Width = 87
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Title.Caption = 'Operador'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_OPERACAO_LOG'
            Title.Caption = 'Cod Status'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Status'
            Width = 329
            Visible = True
          end>
      end
      object Panel8: TPanel
        Left = 0
        Top = 32
        Width = 547
        Height = 21
        Align = alCustom
        Alignment = taLeftJustify
        Anchors = [akLeft, akTop, akRight]
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = 'LOGS PAGAMENTO/CANCELAMENTO/ANTECIPA'#199#195'O'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object grdPgtoEstab: TJvDBGrid [3]
    Left = 0
    Top = 177
    Width = 1044
    Height = 207
    Hint = 'Clique 2x na grade para marcar ou desmarcar o fornecedor'
    Align = alClient
    DataSource = dsPgtoEstab
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    PopupMenu = JvPopupMenu1
    ReadOnly = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = grdPgtoEstabDblClick
    OnKeyDown = grdPgtoEstabKeyDown
    AutoAppend = False
    TitleButtons = True
    OnTitleBtnClick = grdPgtoEstabTitleBtnClick
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'cred_id'
        Title.Caption = 'Cred_ID'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'#13#10
        Title.Caption = 'Nome'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'correntista'
        Title.Caption = 'Correntista'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ATRASADO'
        Title.Caption = 'Atrasado'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'bruto'
        Title.Caption = 'Bruto R$'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comissao'
        Title.Caption = 'Taxa %'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comissao_adm'
        Title.Caption = 'Comiss'#227'o Adm. R$'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TX_DVV'
        Title.Caption = 'DVV'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'taxa_extra'
        Title.Caption = 'Taxas Extras R$'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'liquido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'L'#237'quido/Repasse R$'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODBANCO'
        Title.Caption = 'Cod. Banco'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'agencia'
        Title.Caption = 'Ag'#234'ncia'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contacorrente'
        Title.Caption = 'Conta Corrente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_BANCO'
        Title.Caption = 'Nome do Banco'
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BAIXADO'
        Title.Caption = 'Baixado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FATURA_ID'
        Title.Caption = 'Cod. Fatura'
        Width = 85
        Visible = True
      end>
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 564
    Width = 1044
    Height = 34
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    BorderStyle = bsSingle
    TabOrder = 6
    object Bevel7: TBevel
      Left = 334
      Top = 4
      Width = 2
      Height = 20
    end
    object BtnImprimir: TBitBtn
      Left = 1191
      Top = 1
      Width = 99
      Height = 28
      Hint = 'Op'#231#245'es de impress'#227'o de pagamentos a fornecedores'
      Caption = 'Visualizar'
      TabOrder = 1
      Visible = False
      OnClick = BtnImprimirClick
      Glyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B0040000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC6CED2A8ADAFB0B1B2A8A6A6868585919394A69698987879A29A9AB4B5B5
        B2B3B4B2B7BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B8BAAAAB
        ACC2C2C2E7E6E6DADADAA0A1A19994945B4A494A4141606060858585AEAEADC2
        C1C1ACADAEB1B5B7FFFFFFFFFFFFFFFFFFBAC1C5A6A7A9B8B8B8E5E5E5F1F1F1
        EAEAEACECECE9999999696965858583939394545454F4F4F6E6E6E969595BAB9
        B9B7B7B7A1A2A4FFFFFFFFFFFFAEB0B1DBDBDAFAFAFAF3F3F3EDEDEDCDCDCDA2
        A2A27D7D7D8F8F8FA6A6A6A2A2A28A8A8A6F6F6F6D6D6D5C5C5C7171719A9B9B
        ACB2B4FFFFFFFFFFFFC3C3C2FFFFFFF2F2F2D0D0D09595959999999E9E9E7878
        787070707171718080809C9C9CAEAEAEA7A7A79090908F8B8DB1B0B2B7BEC1FF
        FFFFFFFFFFAFAFAED5D5D5939393959595C0C0C0C3C3C3C8C8C8BFBFBFA2A2A2
        9191918787877777776F6F6F828282A3A4A36AA27B7FA08BBBBDC5FFFFFFFFFF
        FF7F7F7E939393CDCDCDD7D7D7C7C7C7C1C1C1DADADAC5C5C5CDCDCDC9C9C9C2
        C2C2BDBDBDB5B5B59B9B9B7B7B7B757173848385B6BDC0FFFFFFFFFFFF979695
        F0F0F0D2D2D2C6C6C6C2C2C2DBDBDBBEBEBEC7C7C7C8C8C8B8B8B8B0B0B0BDBD
        BDBDBDBDC1C1C1CFCFCFC7C7C7A3A4A4B4BABEFFFFFFFFFFFFB3B7B8CBCBCBC6
        C6C6C3C3C3CCCCCCB8B8B8DDDDDDF5F5F5F2F2F2E9E9E9DFDFDFD4D4D4BFBFBF
        B1B1B1B1B1B1B1B0B0BBBDBEC1C9CDFFFFFFFFFFFFC4CCCFBDC2C5A5A7A8A8A8
        A8C3C4C4B5B7B8B0B1B1D1D1D1E0E0E0E1E1E1E6E6E6E9EAEAE9E9E9DFDFDFC0
        BFBF9D9E9EBBC1C5CBD3D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFFFE
        CEC7C1A9ADB0A1A5AA9EA1A4A6A8AAB6B7B9C2B6B6C1B7B7B3B4B4A7A9AABBC2
        C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1AFB0DEB094FED5A5F4
        CBA2EECAA7ECD2B7E3D3C2D6CBC1AB8D8DAAA5A7BEC6CAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8A8A8E3AC86FFD2A1FFCE9EFFCF
        9FFFD0A0FFD1A2F0C09BAD8D8DCAD5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB49A95FDD5ADFFD7B0FFD6B0FFD6B0FFD6B0
        FFDDB4C39A8DAF9495FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFB79188FFECD0FFE3C9FFE3C9FFE3C9FFE4CAFDE3C9B0
        8B89BEBCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB2A7AADABBAFFFEFDBFFEBD8FFEBD8FFEBD8FFF2DEDCBFB4AA8383FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA
        A2A3FFFFF8FFFFF8FFFFF8FFFFF8FFFFF9FFFFFEC4A5A1AB9394FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA89799B99797CBB0
        B0CAB0B0CAB0B0CAB0B0CAB1B0C9ADACB59999C2C2C6FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C1C5BDBABDBBB8BBBBB8BB
        BBB8BBBBB8BBBBB8BBBBB7BAC3C5C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
    end
    object BtnEfetuar: TButton
      Left = 344
      Top = 2
      Width = 113
      Height = 25
      Hint = 'Efetuar o pagamento dos fornecedores marcados'
      Caption = '&Efetuar Pagamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BtnEfetuarClick
    end
    object BtnMarcDesm: TButton
      Left = 7
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar ou desmarcar um fornecedor'
      Caption = 'Marca/Desm.(F11)'
      TabOrder = 2
      OnClick = BtnMarcDesmClick
    end
    object BtnMarcaTodos: TButton
      Left = 116
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Marcar todos os fornecedores'
      Caption = 'Marcar Todos(F8)'
      TabOrder = 3
      OnClick = BtnMarcaTodosClick
    end
    object BtnDesmTodos: TButton
      Left = 224
      Top = 2
      Width = 105
      Height = 25
      Hint = 'Desmarcar todos os fornecedores'
      Caption = 'Desm. Todos (F9)'
      TabOrder = 4
      OnClick = BtnDesmTodosClick
    end
    object ckQuebraPagina: TCheckBox
      Left = 622
      Top = 7
      Width = 211
      Height = 17
      Hint = 'Na impress'#227'o, separar um fornecedor por p'#225'gina'
      Caption = 'Imprimir um Estabelecimento por P'#225'gina'
      Checked = True
      State = cbChecked
      TabOrder = 7
      Visible = False
    end
    object bntGerarPDF: TBitBtn
      Left = 1081
      Top = 1
      Width = 96
      Height = 28
      Caption = '&Gerar PDF'
      Enabled = False
      TabOrder = 0
      Visible = False
      OnClick = bntGerarPDFClick
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFAFDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFEFEFFAEAEFF8F8FFFF7F7FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCF8FCFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFDFDFF9B9BFE9797FE6B6BFFF9F9FEFEFEFEFEFE
        FEFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8FCFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFEF9F9FD9D9DFD9A9AFE
        9696FEFEFEFDFDFDFDFEFEFEFDFDFDFEFEFEFDFDFDFDFDFDFEFEFEFDFDFDFFFF
        FDFDF9FBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFEFEFEFC
        FCFCFFFFFCDFDFFC8181FDDEDEFDF0F0FDF8F8FCFEFEFCFEFEFCFFFFFCF5F5FC
        E6E6FDEBEBFDCCC8F7D4CDF5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFDFDFDFBFBFBFBFBFBFFFFFBBEBEFC6767FDC9C9FBB5B5FCAEAEFCB7
        B7FC9E9EFD6E6EFD8484FD8686FDB9B9FCFFFBF9FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFBFBFBFAFAFAFAFAFAFAFAFAFFFFFA5454FDE7E7
        FAF1F1FAEFEFF94141FE8181FC8C8CFC8D8DFC9E9EFBE7E7FAF8F4F7FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFAFAFAF8F8F8F8F8F8F8F8F8
        FBFBF8E1E1F95858FCFFFFF84949FDC8C8FAFFFFF8FEFEF8FFFFF8FEFEF8F9F9
        F8F6F2F6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF9F9F9F7
        F7F7F7F7F7F7F7F7F7F7F7FFFFF77D7DFB5E5EFCAEAEF9FFFFF7F7F7F7F7F7F7
        F7F7F7F7F7F7F7F7F7F4F0F4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF6F6F6F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5D6D6F64848FCEAEAF5F5
        F5F5F5F5F5F5F5F5F5F5F5F5F5F5F4F4F4F1EDF1FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFF6F6F6F4F4F4F4F4F4F4F4F4F4F4F4F5F5F4D2D2
        F65D5DFBF1F1F4F4F4F4F4F4F4F4F4F4F4F4F4F3F3F3F0F0F0EBE7EBFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF4F4F4F2F2F2F2F2F2F2F2F2
        F2F2F2F6F6F2A4A4F67A7AF8EBEBF2F2F2F2F2F2F2F2F2F2F0F0F0EDEDEDEAEA
        EAE5E1E5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF2F2F2F1
        F1F1F1F1F1F1F1F1F1F1F1F7F7F07979F8C1C1F4F0F0F1F1F1F1F1F1F1EEEEEE
        EBEBEBE7E7E7E5E5E5E0DCE0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF1F1EFEFEFEEEFEFEEEFEFEEEFEFEEF6F6ED7272F5AFAFF1EAEAEEEE
        EEECEBEBEBE8E8E8E4E4E4E1E1E1DDDDDDD8D4D8FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF6C6CA66565A26262A06868A165659E6B6B9E4343
        A03737A165659D6B6B9CB5B5C6E3E3E2DCDCDCD6D6D6CECDCEC3C0C3FF00FFFF
        00FFFF00FF00009D0000A60000A7FFFFFF2727B40100A73838B8FFFFFFFFFFFF
        3131B006069EFFFFFF00009300008E0101878787A8CECECDB4B4B4A5A4A59999
        99B59EB5FF00FFFF00FFFF00FF0303C00404CB0404CDFFFFFFFFFFFF1111CE85
        85E6FFFFFFD1D1F5FFFFFF2B2BC5FFFFFFFFFFFF0000A903039E7F7FA5B6B6B5
        AAA9AAB0B0B0C5BAC5FF00FFFF00FFFF00FFFF00FF1A1AD91E1EE21E1EE3FFFF
        FF9F9FF4FFFFFF9797F0FFFFFFCBCBF6FFFFFF7979E6FFFFFF0000C20000BA03
        03AD7676A1C1C0C0FFFFFFDFC8DFFF00FFFF00FFFF00FFFF00FFFF00FF3434EC
        3A3AF43939F4FFFFFFFFFFFFFFFFFF9F9FF6FFFFFFFFFFFF9B9BF1CACAF6FFFF
        FFFFFFFF0000C20303B5706F9FC8C7C6DED5DEFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FF413EF25244F84F41F74638F33932ED3434E93030E52626DF2424DB
        1D1DD51212CE1717C91212C10E0EB81010AD7070A0D0C2CEFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDE0BB
        BBDEBABADCB8B8DAB5B5D6B1B1D2ADADCCA9A9C8A5A5C39D9DBBB883C4FF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object BtnCancelarPag: TButton
      Left = 464
      Top = 2
      Width = 145
      Height = 25
      Hint = 
        'Cancelar o pagamento dos fornecedores marcados e reabrir voltant' +
        'o para o estado de "PENDENTE"'
      Caption = '&Extornar Pagamento '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      Visible = False
      OnClick = BtnCancelarPagClick
    end
    object CheckBox1: TCheckBox
      Left = 840
      Top = 8
      Width = 137
      Height = 17
      Caption = 'Habilitar Cancelamento'
      TabOrder = 8
      Visible = False
    end
  end
  object Panel9: TPanel [5]
    Left = 0
    Top = 598
    Width = 1044
    Height = 20
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 7
    DesignSize = (
      1044
      20)
    object Bevel1: TBevel
      Left = 1218
      Top = 12
      Width = 2
      Height = 20
      Anchors = [akTop, akRight]
    end
    object Panel10: TPanel
      Left = 227
      Top = 0
      Width = 817
      Height = 20
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      Color = clWhite
      TabOrder = 1
      object Label12: TLabel
        Left = 5
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Bruto:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabLiq: TLabel
        Left = 568
        Top = 4
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label14: TLabel
        Left = 517
        Top = 4
        Width = 48
        Height = 13
        Caption = 'L'#237'quido:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabBruto: TLabel
        Left = 41
        Top = 4
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label8: TLabel
        Left = 245
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Descontos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LabDesc: TLabel
        Left = 311
        Top = 4
        Width = 26
        Height = 13
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 227
      Height = 20
      Align = alLeft
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Totaliza'#231#227'o dos Pagamentos Marcados:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object edTaxaBanco: TEdit [6]
    Left = 1160
    Top = 114
    Width = 90
    Height = 21
    TabOrder = 2
    Text = '0'
    Visible = False
    OnEnter = edTaxaBancoEnter
    OnExit = edTaxaBancoExit
    OnKeyPress = edTaxaBancoKeyPress
  end
  object JvDBGrid1: TJvDBGrid [7]
    Left = 0
    Top = 177
    Width = 1044
    Height = 207
    Hint = 'Clique 2x na grade para marcar ou desmarcar o fornecedor'
    Align = alClient
    DataSource = dsPgtoEstab
    DefaultDrawing = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    PopupMenu = JvPopupMenu1
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = grdPgtoEstabDblClick
    OnKeyDown = grdPgtoEstabKeyDown
    AutoAppend = False
    TitleButtons = True
    OnTitleBtnClick = grdPgtoEstabTitleBtnClick
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 16
    TitleRowHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'cred_id'
        Title.Caption = 'Cred_ID'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'#13#10
        Title.Caption = 'Nome'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'correntista'
        Title.Caption = 'Correntista'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ATRASADO'
        Title.Caption = 'Atrasado'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'bruto'
        Title.Caption = 'Bruto R$'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comissao'
        Title.Caption = 'Taxa %'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'comissao_adm'
        Title.Caption = 'Comiss'#227'o Adm. R$'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TX_DVV'
        Title.Caption = 'DVV'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'taxa_extra'
        Title.Caption = 'Taxas Extras R$'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'liquido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'L'#237'quido/Repasse R$'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CODBANCO'
        Title.Caption = 'Cod. Banco'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'agencia'
        Title.Caption = 'Ag'#234'ncia'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contacorrente'
        Title.Caption = 'Conta Corrente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME_BANCO'
        Title.Caption = 'Nome do Banco'
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BAIXADO'
        Title.Caption = 'Baixado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FATURA_ID'
        Title.Caption = 'Cod. Fatura'
        Width = 85
        Visible = True
      end>
  end
  inherited PopupBut: TPopupMenu
    Left = 564
    Top = 104
  end
  object dsEmpr: TDataSource
    DataSet = qEmpr
    Left = 120
    Top = 232
  end
  object dsEstab: TDataSource
    DataSet = qEstab
    Left = 392
    Top = 80
  end
  object dsPgtoEstab: TDataSource
    DataSet = MDPagtoEstab
    OnStateChange = dsPgtoEstabStateChange
    OnDataChange = dsPgtoEstabDataChange
    Left = 248
    Top = 232
  end
  object dsPgtoDesc: TDataSource
    DataSet = qPgtoDesc
    Left = 377
    Top = 257
  end
  object dsPgtoPor: TDataSource
    DataSet = qPgtoPor
    OnDataChange = dsPgtoPorDataChange
    OnUpdateData = dsPgtoPorUpdateData
    Left = 56
    Top = 232
  end
  object dsTemp: TDataSource
    Left = 441
    Top = 233
  end
  object dsBancos: TDataSource
    Left = 529
    Top = 233
  end
  object qPgtoPor: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from paga_cred_por WHERE paga_cred_por_id <> 4')
    Left = 56
    Top = 264
    object qPgtoPorPAGA_CRED_POR_ID: TIntegerField
      FieldName = 'PAGA_CRED_POR_ID'
    end
    object qPgtoPorDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
  end
  object qEmpr: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select empres_id, nome, fantasia '
      'from empresas where coalesce(apagado,'#39'N'#39') <> '#39'S'#39
      'order by nome')
    Left = 120
    Top = 264
    object qEmprempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object qEmprnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEmprfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
  end
  object qEstab: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select cred_id, nome, fantasia from credenciados '
      'where coalesce(apagado,'#39'N'#39') <> '#39'S'#39
      'order by nome')
    Left = 184
    Top = 264
    object qEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qEstabnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object qEstabfantasia: TStringField
      FieldName = 'fantasia'
      Size = 58
    end
  end
  object qPgtoEstab: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    BeforeOpen = qPgtoEstabBeforeOpen
    AfterOpen = qPgtoEstabAfterOpen
    OnCalcFields = qPgtoEstabCalcFields
    DataSource = dsBancos
    Parameters = <
      item
        Name = 'DATA_INICIAL'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'DATA_FINAL'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'DATA_INICIAL_2'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'DATA_FINAL_2'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '       cred.cred_id,'
      #9'   cred.nome, '
      #9'   cred.diafechamento1,'
      #9'   cred.vencimento1,'
      #9'   cred.CORRENTISTA,'
      #9'   cred.cgc,'
      #9'   cred.COMISSAO,'
      #9'   cred.CONTACORRENTE,'
      '                   cred.AGENCIA,'
      '                   cred.ENDERECO,'
      '                   cred.NUMERO,'
      '                   cred.CIDADE,'
      '                   cred.CEP,'
      '                   cred.ESTADO,'
      '                   cred.COMPLEMENTO,'
      '                   cred.CONTA_CDC,'
      '                   cred.TELEFONE1,'
      '                   cred.CNPJ_CPF_CORRENTISTA, '
      
        '                 CAST(((coalesce(cred.TX_DVV,0.0) * 41)/100) AS ' +
        'FLOAT)as TX_DVV,'
      #9'   cred.BANCO as CODBANCO,'
      '                 ba.BANCO as NOME_BANCO,'
      '                 ba.COD_BANCO as COD_BANCO,'
      '                 cc.BAIXA_CREDENCIADO AS BAIXADO,'
      #9'   COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'
      
        #9'   ------------------------------------------------------------' +
        '----------------'
      #9'   --seleciona TAXAS EXTRAS/ RETORNO VALIDADO '
      #9'   (SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA, --as TAXAS_EXTR' +
        'AS'
      #9'   -- fim da consulta que calcula taxas extras'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      #9'   --seleciona TAXA ADMINISTRADORA(COMISS'#195'O)'
      
        #9'   (coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),' +
        '0)) as COMISSAO_ADM, '
      #9'   -- fim da consulta TAXA ADMINISTRADORA(COMISS'#195'O)'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      #9'   --seleciona TOTAL RETIDO PELA ADMINISTRADORA'
      
        #9'   (coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100)' +
        ') /*comissao*/'
      #9'   +'
      #9'   (SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'
      #9'   AS TOTAL_RETIDO_ADM,'
      #9'   -- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      
        #9'   --seleciona o TOTAL L'#205'QUIDO QUE A ADMINISTRADORA DEVE PAGAR ' +
        'PARA O CREDENCIADO'
      #9'   --(BRUTO - (TAXAS EXTRAS - COMISSAO))'
      #9'   ((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0)) '
      #9'   -'
      
        #9'   ((coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100)' +
        ',0))+(SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID))'
      #9'   ) AS LIQUIDO'
      
        #9'   --Fim da consulta TOTAL L'#205'QUIDO QUE A ADMINISTRADORA DEVE PA' +
        'GAR PARA O CREDENCIADO'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      '                 ,'#39'N'#39' as ATRASADO,'
      '                CRED.COD_GERENCIADOR'
      'from credenciados cred'
      
        'LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.' +
        'data between :DATA_INICIAL and :DATA_FINAL'
      'LEFT JOIN BANCOS ba ON ba.codigo = cred.banco '
      
        'INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SE' +
        'G_ID = 14'
      'WHERE PAGA_CRED_POR_ID = 2 '
      
        'GROUP BY cred.CRED_ID,CRED.NOME,cred.diafechamento1,cred.vencime' +
        'nto1,cc.BAIXA_CREDENCIADO,cred.CORRENTISTA,cred.cgc, cred.CNPJ_C' +
        'PF_CORRENTISTA,'
      
        'cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,ba.COD_BANCO,cred.BANC' +
        'O,cred.COMISSAO,cred.TX_DVV,cred.ENDERECO, cred.NUMERO, cred.CID' +
        'ADE, cred.CEP, cred.ESTADO, cred.COMPLEMENTO, cred.TELEFONE1,cre' +
        'd.CONTA_CDC,CRED.COD_GERENCIADOR'
      'UNION'
      'SELECT'
      '       cred.cred_id,'
      #9'   cred.nome, '
      #9'   cred.diafechamento1,'
      #9'   cred.vencimento1,'
      #9'   cred.CORRENTISTA,'
      #9'   cred.cgc,'
      #9'   cred.COMISSAO,'
      #9'   cred.CONTACORRENTE,'
      #9'   cred.AGENCIA,'
      '                   cred.ENDERECO,'
      '                   cred.NUMERO,'
      '                   cred.CIDADE,'
      '                   cred.CEP,'
      '                   cred.ESTADO,'
      '                   cred.COMPLEMENTO,'
      '                   cred.TELEFONE1, '
      '                   cred.CONTA_CDC,'
      '                   cred.CNPJ_CPF_CORRENTISTA,'
      
        '                   CAST(((coalesce(cred.TX_DVV,0.0) * 41)/100) A' +
        'S FLOAT)as TX_DVV,'
      #9'   cred.BANCO as CODBANCO,'
      '                 ba.BANCO as NOME_BANCO, '
      '                 ba.COD_BANCO as COD_BANCO,'
      '                 cc.BAIXA_CREDENCIADO AS BAIXADO,'
      #9'   COALESCE(SUM(CC.DEBITO - CC.CREDITO),0) AS BRUTO,'
      
        #9'   ------------------------------------------------------------' +
        '----------------'
      #9'   --seleciona TAXAS EXTRAS/ RETORNO VALIDADO '
      #9'   (SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID) aS TAXA_EXTRA, --as TAXAS_EXTR' +
        'AS'
      #9'   -- fim da consulta que calcula taxas extras'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      #9'   --seleciona TAXA ADMINISTRADORA(COMISS'#195'O)'
      
        #9'   (coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100),' +
        '0)) as COMISSAO_ADM, '
      #9'   -- fim da consulta TAXA ADMINISTRADORA(COMISS'#195'O)'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      #9'   --seleciona TOTAL RETIDO PELA ADMINISTRADORA'
      
        #9'   (coalesce(((sum(CC.debito - CC.credito))*(cred.COMISSAO/100)' +
        ') /*comissao*/'
      #9'   +'
      #9'   (SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID) /*taxas extras*/ ,0))'
      #9'   AS TOTAL_RETIDO_ADM,'
      #9'   -- Fim da consulta TOTAL RETIDO PELA ADMINISTRADORA'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      
        #9'   --seleciona o TOTAL L'#205'QUIDO QUE A ADMINISTRADORA DEVE PAGAR ' +
        'PARA O CREDENCIADO'
      #9'   --(BRUTO - (TAXAS EXTRAS - COMISSAO))'
      #9'   ((COALESCE(SUM(CC.DEBITO - CC.CREDITO),0)) '
      #9'   -'
      
        #9'   ((coalesce((sum(CC.debito - CC.credito))*(cred.COMISSAO/100)' +
        ',0))+(SELECT COALESCE(SUM(t.valor),0) '
      
        #9'   FROM TAXAS t, rel_taxa_cred rtc WHERE t.taxa_id = rtc.TAXA_I' +
        'D and rtc.cred_id = cred.CRED_ID))'
      #9'   ) AS LIQUIDO'
      
        #9'   --Fim da consulta TOTAL L'#205'QUIDO QUE A ADMINISTRADORA DEVE PA' +
        'GAR PARA O CREDENCIADO'
      
        #9'   ------------------------------------------------------------' +
        '-----------------'
      '                 ,'#39'S'#39' as ATRASADO,'
      '                 CRED.COD_GERENCIADOR'
      'from credenciados cred'
      
        'LEFT JOIN contacorrente cc  ON CC.CRED_ID = cred.CRED_ID AND cc.' +
        'data between :DATA_INICIAL_2 and :DATA_FINAL_2'
      'LEFT JOIN BANCOS ba ON ba.codigo = cred.banco '
      
        'INNER JOIN SEGMENTOS seg ON SEG.SEG_ID = CRED.SEG_ID AND CRED.SE' +
        'G_ID = 14'
      'WHERE PAGA_CRED_POR_ID = 2 '
      
        'GROUP BY cred.CRED_ID,CRED.NOME,cred.diafechamento1,cred.vencime' +
        'nto1,cc.BAIXA_CREDENCIADO,cred.CORRENTISTA,cred.cgc,cred.CNPJ_CP' +
        'F_CORRENTISTA,'
      
        'cred.CONTACORRENTE, cred.AGENCIA,ba.BANCO,ba.COD_BANCO,cc.pagame' +
        'nto_cred_id,cred.BANCO,cred.COMISSAO,cred.TX_DVV,cred.ENDERECO, ' +
        'cred.NUMERO,CRED.COD_GERENCIADOR, cred.CIDADE, cred.CEP, cred.ES' +
        'TADO, cred.COMPLEMENTO, cred.TELEFONE1,cred.CONTA_CDC'
      'order by cred.cred_id')
    Left = 248
    Top = 264
    object qPgtoEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object qPgtoEstabnome: TStringField
      FieldName = 'nome'
      Size = 120
    end
    object qPgtoEstabdiafechamento1: TWordField
      FieldName = 'diafechamento1'
    end
    object qPgtoEstabvencimento1: TWordField
      FieldName = 'vencimento1'
    end
    object qPgtoEstabCORRENTISTA: TStringField
      FieldName = 'CORRENTISTA'
      Size = 40
    end
    object qPgtoEstabcgc: TStringField
      FieldName = 'cgc'
      Size = 18
    end
    object qPgtoEstabCOMISSAO: TBCDField
      FieldName = 'COMISSAO'
      Precision = 6
      Size = 2
    end
    object qPgtoEstabCONTACORRENTE: TStringField
      FieldName = 'CONTACORRENTE'
      Size = 15
    end
    object qPgtoEstabAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 7
    end
    object qPgtoEstabCODBANCO: TIntegerField
      FieldName = 'CODBANCO'
    end
    object qPgtoEstabNOME_BANCO: TStringField
      FieldName = 'NOME_BANCO'
      Size = 45
    end
    object qPgtoEstabBAIXADO: TStringField
      FieldName = 'BAIXADO'
      FixedChar = True
      Size = 1
    end
    object qPgtoEstabBRUTO: TBCDField
      FieldName = 'BRUTO'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qPgtoEstabTAXA_EXTRA: TFloatField
      FieldName = 'TAXA_EXTRA'
      ReadOnly = True
    end
    object qPgtoEstabCOMISSAO_ADM: TBCDField
      FieldName = 'COMISSAO_ADM'
      ReadOnly = True
      Precision = 32
      Size = 6
    end
    object qPgtoEstabTOTAL_RETIDO_ADM: TFloatField
      FieldName = 'TOTAL_RETIDO_ADM'
      ReadOnly = True
    end
    object qPgtoEstabLIQUIDO: TFloatField
      FieldName = 'LIQUIDO'
      ReadOnly = True
    end
    object qPgtoEstabATRASADO: TStringField
      FieldName = 'ATRASADO'
      ReadOnly = True
      Size = 1
    end
    object qPgtoEstabTX_DVV: TFloatField
      FieldName = 'TX_DVV'
      ReadOnly = True
    end
    object qPgtoEstabENDERECO: TStringField
      FieldName = 'ENDERECO'
      ReadOnly = True
      Size = 60
    end
    object qPgtoEstabNUMERO: TIntegerField
      FieldName = 'NUMERO'
      ReadOnly = True
    end
    object qPgtoEstabCEP: TStringField
      FieldName = 'CEP'
      ReadOnly = True
      Size = 9
    end
    object qPgtoEstabCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      ReadOnly = True
      Size = 30
    end
    object qPgtoEstabTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      ReadOnly = True
      Size = 14
    end
    object qPgtoEstabCONTA_CDC: TStringField
      FieldName = 'CONTA_CDC'
      ReadOnly = True
      Size = 14
    end
    object qPgtoEstabCIDADE: TIntegerField
      FieldName = 'CIDADE'
      ReadOnly = True
    end
    object qPgtoEstabESTADO: TIntegerField
      FieldName = 'ESTADO'
      ReadOnly = True
    end
    object qPgtoEstabCOD_GERENCIADOR: TIntegerField
      FieldName = 'COD_GERENCIADOR'
      ReadOnly = True
    end
    object qPgtoEstabCOD_BANCO: TIntegerField
      FieldName = 'COD_BANCO'
      ReadOnly = True
    end
    object qPgtoEstabCNPJ_CPF_CORRENTISTA: TStringField
      FieldName = 'CNPJ_CPF_CORRENTISTA'
      ReadOnly = True
      Size = 25
    end
  end
  object qTemp: TADOQuery
    Parameters = <>
    Left = 440
    Top = 264
  end
  object qBancos: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select'
      '  b.codigo as codbanco,'
      '  b.banco'
      'from'
      'bancos b')
    Left = 528
    Top = 264
    object qBancoscodbanco: TIntegerField
      FieldName = 'codbanco'
    end
    object qBancosbanco: TStringField
      FieldName = 'banco'
      Size = 45
    end
  end
  object frxReport1: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41353.581082893500000000
    ReportOptions.LastChange = 42493.662267534720000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var c : integer = 0;'
      '    '
      'procedure lnDetalheOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {c := c + 1;'
      '  if c mod 2 = 0 then begin             '
      '    lnDetalhe.BeginColor := clGray;'
      '    lnDetalhe.Color :=  clGray;     '
      '    lnDetalhe.EndColor := clGray;'
      '  end else begin'
      '    lnDetalhe.BeginColor := clWhite;'
      '    lnDetalhe.Color :=  clWhite;     '
      '    lnDetalhe.EndColor :=  clWhite;      '
      '  end;}          '
      'end;'
      ''
      'procedure DetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      ' '
      
        '  DetailData1.Visible := (<frxPgtoEstab."MARCADO"> = TRUE) and (' +
        '<frxBancos."CODBANCO"> = <frxPgtoEstab."CODBANCO">);'
      '  //if Frac(<Line> / 2) = 0.5 then'
      '    //Memo3.Color := $00EBEBEB'
      '  //else'
      '    ///Memo3.Color := clWhite;'
      'end;'
      ''
      'procedure Footer1OnBeforePrint(Sender: TfrxComponent);'
      'begin  '
      
        '  Footer1.visible := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1) <' +
        '> 0;'
      'end;'
      ''
      'procedure Memo6OnBeforePrint(Sender: TfrxComponent);'
      
        'var valor : Currency;                                           ' +
        '           '
      'begin'
      '  try                                   '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      Memo6.Font.Color := clRed               '
      '    else'
      '      Memo6.Font.Color := clBlack;'
      '  except'
      '  end;            '
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      
        '//var valor : Currency;                                         ' +
        '             '
      'begin'
      '{  try               '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      Memo12.Font.Color := clRed               '
      '    else'
      '      Memo12.Font.Color := clBlack;'
      '  except'
      '  end;}          '
      'end;'
      ''
      
        'procedure frxPgtoEstabLIQUIDOOnBeforePrint(Sender: TfrxComponent' +
        ');'
      
        'var valor : Currency;                                           ' +
        '           '
      'begin'
      '  try                  '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      frxPgtoEstabLIQUIDO.Font.Color := clRed               '
      '    else'
      '      frxPgtoEstabLIQUIDO.Font.Color := clBlack;'
      '  except'
      '  end;          '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 648
    Top = 216
    Datasets = <
      item
        DataSet = frxBancos
        DataSetName = 'frxBancos'
      end
      item
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'TotalGeral'
        Value = Null
      end
      item
        Name = 'tipoPagamento'
        Value = Null
      end
      item
        Name = 'dtCompensa'
        Value = Null
      end
      item
        Name = 'dataIni'
        Value = Null
      end
      item
        Name = 'dataFin'
        Value = Null
      end
      item
        Name = 'tipoEstab'
        Value = Null
      end
      item
        Name = 'semana'
        Value = Null
      end
      item
        Name = 'codAutorizacao'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object DetailData1: TfrxDetailData
        Height = 18.897650000000000000
        Top = 336.378170000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
        KeepChild = True
        RowCount = 0
        object Memo16: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          ShowHint = False
        end
        object frxPgtoEstabCRED_ID: TfrxMemoView
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'cred_id'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."cred_id"]')
          ParentFont = False
        end
        object frxPgtoEstabNOMEESTAB: TfrxMemoView
          Left = 60.472480000000000000
          Width = 408.189240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'nome'#13#10
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."nome'
            '"]')
        end
        object frxPgtoEstabAGENCIA: TfrxMemoView
          Left = 457.323130000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'agencia'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."agencia"]')
          ParentFont = False
        end
        object frxPgtoEstabCONTACORRENTE: TfrxMemoView
          Left = 514.016080000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'contacorrente'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."contacorrente"]')
          ParentFont = False
        end
        object frxPgtoEstabLIQUIDO: TfrxMemoView
          Left = 941.102970000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'frxPgtoEstabLIQUIDOOnBeforePrint'
          ShowHint = False
          DataField = 'liquido'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."liquido"]')
        end
        object frxPgtoEstabCORRENTISTA: TfrxMemoView
          Left = 684.094930000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'correntista'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."correntista"]')
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 146.000000000000000000
          Height = 96.000000000000000000
          ShowHint = False
          AutoSize = True
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000009200
            0000600806000000F6212B9C000000097048597300000B1300000B1301009A9C
            18000040E169545874584D4C3A636F6D2E61646F62652E786D7000000000003C
            3F787061636B657420626567696E3D22EFBBBF222069643D2257354D304D7043
            656869487A7265537A4E54637A6B633964223F3E0A3C783A786D706D65746120
            786D6C6E733A783D2261646F62653A6E733A6D6574612F2220783A786D70746B
            3D2241646F626520584D5020436F726520352E352D633031342037392E313531
            3438312C20323031332F30332F31332D31323A30393A31352020202020202020
            223E0A2020203C7264663A52444620786D6C6E733A7264663D22687474703A2F
            2F7777772E77332E6F72672F313939392F30322F32322D7264662D73796E7461
            782D6E7323223E0A2020202020203C7264663A4465736372697074696F6E2072
            64663A61626F75743D22220A202020202020202020202020786D6C6E733A786D
            703D22687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F22
            0A202020202020202020202020786D6C6E733A64633D22687474703A2F2F7075
            726C2E6F72672F64632F656C656D656E74732F312E312F220A20202020202020
            2020202020786D6C6E733A786D704D4D3D22687474703A2F2F6E732E61646F62
            652E636F6D2F7861702F312E302F6D6D2F220A20202020202020202020202078
            6D6C6E733A73744576743D22687474703A2F2F6E732E61646F62652E636F6D2F
            7861702F312E302F73547970652F5265736F757263654576656E7423220A2020
            20202020202020202020786D6C6E733A73745265663D22687474703A2F2F6E73
            2E61646F62652E636F6D2F7861702F312E302F73547970652F5265736F757263
            6552656623220A202020202020202020202020786D6C6E733A70686F746F7368
            6F703D22687474703A2F2F6E732E61646F62652E636F6D2F70686F746F73686F
            702F312E302F220A202020202020202020202020786D6C6E733A746966663D22
            687474703A2F2F6E732E61646F62652E636F6D2F746966662F312E302F220A20
            2020202020202020202020786D6C6E733A657869663D22687474703A2F2F6E73
            2E61646F62652E636F6D2F657869662F312E302F223E0A202020202020202020
            3C786D703A43726561746F72546F6F6C3E41646F62652050686F746F73686F70
            204343202857696E646F7773293C2F786D703A43726561746F72546F6F6C3E0A
            2020202020202020203C786D703A437265617465446174653E323031342D3132
            2D30315431373A33333A30332D30323A30303C2F786D703A4372656174654461
            74653E0A2020202020202020203C786D703A4D65746164617461446174653E32
            3031342D31322D30315431373A33383A32302D30323A30303C2F786D703A4D65
            746164617461446174653E0A2020202020202020203C786D703A4D6F64696679
            446174653E323031342D31322D30315431373A33383A32302D30323A30303C2F
            786D703A4D6F64696679446174653E0A2020202020202020203C64633A666F72
            6D61743E696D6167652F706E673C2F64633A666F726D61743E0A202020202020
            2020203C786D704D4D3A496E7374616E636549443E786D702E6969643A336133
            35376136322D646166642D653734342D613465612D3866383066613833653062
            613C2F786D704D4D3A496E7374616E636549443E0A2020202020202020203C78
            6D704D4D3A446F63756D656E7449443E786D702E6469643A3837333166363635
            2D643634332D666134382D623364362D3930303237366533336233303C2F786D
            704D4D3A446F63756D656E7449443E0A2020202020202020203C786D704D4D3A
            4F726967696E616C446F63756D656E7449443E786D702E6469643A3837333166
            3636352D643634332D666134382D623364362D3930303237366533336233303C
            2F786D704D4D3A4F726967696E616C446F63756D656E7449443E0A2020202020
            202020203C786D704D4D3A486973746F72793E0A202020202020202020202020
            3C7264663A5365713E0A2020202020202020202020202020203C7264663A6C69
            207264663A7061727365547970653D225265736F75726365223E0A2020202020
            202020202020202020202020203C73744576743A616374696F6E3E6372656174
            65643C2F73744576743A616374696F6E3E0A2020202020202020202020202020
            202020203C73744576743A696E7374616E636549443E786D702E6969643A3837
            3331663636352D643634332D666134382D623364362D39303032373665333362
            33303C2F73744576743A696E7374616E636549443E0A20202020202020202020
            20202020202020203C73744576743A7768656E3E323031342D31322D30315431
            373A33333A30332D30323A30303C2F73744576743A7768656E3E0A2020202020
            202020202020202020202020203C73744576743A736F6674776172654167656E
            743E41646F62652050686F746F73686F70204343202857696E646F7773293C2F
            73744576743A736F6674776172654167656E743E0A2020202020202020202020
            202020203C2F7264663A6C693E0A2020202020202020202020202020203C7264
            663A6C69207264663A7061727365547970653D225265736F75726365223E0A20
            20202020202020202020202020202020203C73744576743A616374696F6E3E73
            617665643C2F73744576743A616374696F6E3E0A202020202020202020202020
            2020202020203C73744576743A696E7374616E636549443E786D702E6969643A
            32373830353839632D653534392D626334372D613762392D3036333635313036
            323162393C2F73744576743A696E7374616E636549443E0A2020202020202020
            202020202020202020203C73744576743A7768656E3E323031342D31322D3031
            5431373A33333A31312D30323A30303C2F73744576743A7768656E3E0A202020
            2020202020202020202020202020203C73744576743A736F6674776172654167
            656E743E41646F62652050686F746F73686F70204343202857696E646F777329
            3C2F73744576743A736F6674776172654167656E743E0A202020202020202020
            2020202020202020203C73744576743A6368616E6765643E2F3C2F7374457674
            3A6368616E6765643E0A2020202020202020202020202020203C2F7264663A6C
            693E0A2020202020202020202020202020203C7264663A6C69207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            2020202020203C73744576743A616374696F6E3E636F6E7665727465643C2F73
            744576743A616374696F6E3E0A2020202020202020202020202020202020203C
            73744576743A706172616D65746572733E66726F6D206170706C69636174696F
            6E2F766E642E61646F62652E70686F746F73686F7020746F20696D6167652F70
            6E673C2F73744576743A706172616D65746572733E0A20202020202020202020
            20202020203C2F7264663A6C693E0A2020202020202020202020202020203C72
            64663A6C69207264663A7061727365547970653D225265736F75726365223E0A
            2020202020202020202020202020202020203C73744576743A616374696F6E3E
            646572697665643C2F73744576743A616374696F6E3E0A202020202020202020
            2020202020202020203C73744576743A706172616D65746572733E636F6E7665
            727465642066726F6D206170706C69636174696F6E2F766E642E61646F62652E
            70686F746F73686F7020746F20696D6167652F706E673C2F73744576743A7061
            72616D65746572733E0A2020202020202020202020202020203C2F7264663A6C
            693E0A2020202020202020202020202020203C7264663A6C69207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            2020202020203C73744576743A616374696F6E3E73617665643C2F7374457674
            3A616374696F6E3E0A2020202020202020202020202020202020203C73744576
            743A696E7374616E636549443E786D702E6969643A62666631653332612D3233
            38622D623934622D623733342D3036646130656330623835643C2F7374457674
            3A696E7374616E636549443E0A2020202020202020202020202020202020203C
            73744576743A7768656E3E323031342D31322D30315431373A33333A31312D30
            323A30303C2F73744576743A7768656E3E0A2020202020202020202020202020
            202020203C73744576743A736F6674776172654167656E743E41646F62652050
            686F746F73686F70204343202857696E646F7773293C2F73744576743A736F66
            74776172654167656E743E0A2020202020202020202020202020202020203C73
            744576743A6368616E6765643E2F3C2F73744576743A6368616E6765643E0A20
            20202020202020202020202020203C2F7264663A6C693E0A2020202020202020
            202020202020203C7264663A6C69207264663A7061727365547970653D225265
            736F75726365223E0A2020202020202020202020202020202020203C73744576
            743A616374696F6E3E73617665643C2F73744576743A616374696F6E3E0A2020
            202020202020202020202020202020203C73744576743A696E7374616E636549
            443E786D702E6969643A33613335376136322D646166642D653734342D613465
            612D3866383066613833653062613C2F73744576743A696E7374616E63654944
            3E0A2020202020202020202020202020202020203C73744576743A7768656E3E
            323031342D31322D30315431373A33383A32302D30323A30303C2F7374457674
            3A7768656E3E0A2020202020202020202020202020202020203C73744576743A
            736F6674776172654167656E743E41646F62652050686F746F73686F70204343
            202857696E646F7773293C2F73744576743A736F6674776172654167656E743E
            0A2020202020202020202020202020202020203C73744576743A6368616E6765
            643E2F3C2F73744576743A6368616E6765643E0A202020202020202020202020
            2020203C2F7264663A6C693E0A2020202020202020202020203C2F7264663A53
            65713E0A2020202020202020203C2F786D704D4D3A486973746F72793E0A2020
            202020202020203C786D704D4D3A4465726976656446726F6D207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            3C73745265663A696E7374616E636549443E786D702E6969643A323738303538
            39632D653534392D626334372D613762392D3036333635313036323162393C2F
            73745265663A696E7374616E636549443E0A2020202020202020202020203C73
            745265663A646F63756D656E7449443E786D702E6469643A3837333166363635
            2D643634332D666134382D623364362D3930303237366533336233303C2F7374
            5265663A646F63756D656E7449443E0A2020202020202020202020203C737452
            65663A6F726967696E616C446F63756D656E7449443E786D702E6469643A3837
            3331663636352D643634332D666134382D623364362D39303032373665333362
            33303C2F73745265663A6F726967696E616C446F63756D656E7449443E0A2020
            202020202020203C2F786D704D4D3A4465726976656446726F6D3E0A20202020
            20202020203C70686F746F73686F703A436F6C6F724D6F64653E333C2F70686F
            746F73686F703A436F6C6F724D6F64653E0A2020202020202020203C74696666
            3A4F7269656E746174696F6E3E313C2F746966663A4F7269656E746174696F6E
            3E0A2020202020202020203C746966663A585265736F6C7574696F6E3E373230
            3030302F31303030303C2F746966663A585265736F6C7574696F6E3E0A202020
            2020202020203C746966663A595265736F6C7574696F6E3E3732303030302F31
            303030303C2F746966663A595265736F6C7574696F6E3E0A2020202020202020
            203C746966663A5265736F6C7574696F6E556E69743E323C2F746966663A5265
            736F6C7574696F6E556E69743E0A2020202020202020203C657869663A436F6C
            6F7253706163653E36353533353C2F657869663A436F6C6F7253706163653E0A
            2020202020202020203C657869663A506978656C5844696D656E73696F6E3E31
            34363C2F657869663A506978656C5844696D656E73696F6E3E0A202020202020
            2020203C657869663A506978656C5944696D656E73696F6E3E39363C2F657869
            663A506978656C5944696D656E73696F6E3E0A2020202020203C2F7264663A44
            65736372697074696F6E3E0A2020203C2F7264663A5244463E0A3C2F783A786D
            706D6574613E0A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            202020202020202020202020202020202020202020200A3C3F787061636B6574
            20656E643D2277223F3E88C319A900003663494441547801ED7D077C5545F6FF
            F7F597DE217442935EA508D23B28A8A8B836D6825D71D5BF8A6B43C18A8A6545
            57DC75655D75D55D10A54A5540101014417A078190407A7BEFFDBFDF9BDCF092
            BC00798495FC3E0973DECC3D73CE999933E74E9F8BC5E7F3A1A27FC1F0300D07
            C14EB016013DC3792D164B2E4314EBF3CA279CD69107191919F07AC572925CF8
            88880850180A0A0A8C088515C8CCCC945702446BB3D90C7A33E2C4F1E3866CAB
            555985119758AB16EC763BD2D2D2D0BC79731C3A74C8242FE1F7EFDF1FDF7CF3
            8D8133D3559E84D0B3C2A62FDCA940748A178FFCF2C09F2E50B83CBED3C92D8F
            2F10DE62261C28B23C5C303C01326DF1936F842957562DF08B2A1B94ACDCDC5C
            084AC72A4EC64159411992693CA6DC03FBF7571B92A98C53F8BFA72195C9962A
            BF0CB214428692979787ECEC6C285C2ADAC0056348816499B2F7EFDB87EA16C9
            D44660BF4A19922AFB5446A4228AA6A286A42E4BBCA7027593A2ABEEDA026BA9
            CA19527A7ABA312E92C1042A92F01531A49898984062CAC5551B5260D59CB786
            248330BB3A85CDEC6B80EDF1788C2ECCC4F9FBA23D5343AAA811299D6A439216
            CAC2796948320675619A91B9DDEE12B9AE0C430AC680CC4C541B92A989927E50
            86545244F94F6A516414E553948C31E96540C73905971185868696203A0343B2
            B24572539697E31A7359C190A1E9FFD9189184541B92B45016CEA921B1228DA9
            B38C496026CF4A36BA2613674EB985CFCFCF37A6ED9A95C9884242424C36C397
            31A8B5128FC96F44F8FD84868434B758ADD9EC02F7082DC394512A7CB6703A43
            EADDBB37962C5902FDA93CF2CD7CEA5961D357DCA940748A178FFCF2C09F2E50
            B83CBED3C92D8F2F10FE9C1B524E4E8E6134FE89AB62B508A859900AE37068AD
            12C6205AF4A23D9521894786A4752485456F8214E972B920C3918CD28668D205
            EBFB19928532CAAC795D76D965F8EF7FFFCBA8C2854C05CC3C2A6F0A9BBEE24E
            05A253BC78E49707FE7481C2E5F19D4E6E797C81F0E7DC900255B66948322615
            A6A286240394B1C850D43A49865938295271322413772E7CA669A35C0FA1D8B5
            6BD70EF3E7CF478D1A350C9CF2A20069E515B7CEC29B3823A29C1FD129EA74B4
            FE7481C29211084E2737104F79B82A69483240D350D415AA7059595946CB2745
            46454509754E8195A059408E9948CB962DB178F1E26223125E79914F5A79D586
            646821881F8D91CE458BE46F483B77EEC4EAD5AB71CD35D718FB6091919141E4
            B4E22C348EE216A94993268611D5AD5BB784A06A432AA18EE01FFE1786F4F3CF
            3FA343870EF8F2CB2F316CD8B0E0335B414ED3902EBFFC723CF7DC73C6466E69
            11FF2B43F24FD73F4D33EC1FEF1F6619FC1FCF2A5CE5BBB62D5BB61895181E1E
            0EAD7A9F95362AC83C61C2043CF5D453E57299156956989E1536FD72198B2244
            A7A078E49F29884F3CF24FC5239A53C55724EEFF8C21A9D0A7539C682A035801
            96871E7AC8F7F2CB2F9F529C991FD21B747A56D8F40DE4297E44A768F1C8AF28
            98FCE5F1052B3790BC736948B1ECDA52349D2F9D6173D6A6D91AE3A2380B4B64
            A1A3389EDACAB59F3C863530F570EAAEB83C663C9BA01992870B923ED21BD37B
            E260B6480A8B4FFEB986A54B97D6E37AD154A6D38CA065007A651DF3B381D8E7
            58C675F455264D089A127F1F7143843B15906E19E31F23ED61FA1576E43F250F
            E59E32BE22914119D2693218C20C8E6126AE215D260D43339BD2CAF631DE4283
            D0A9B41E0C27127CA4DD4A9E028529C3C241752386430872C7F973258D338171
            DBB88EF4237DEFFFDA90989EFD820B2E788179B99BE0269CCEA590209EC0A2F9
            FA30CF9F31AC677A67E4D67211B6079739B44A8F8AACCC33C15326C0BC9C32BE
            2291E7C2906E6006FFC64CE83424BD73E25439B751F217AC58638CC4B0F1C6CB
            3FC77031E54F2734C499FDED642BDA64F3E6CDE8DCB9F30B6479985011973F66
            CC18F7E79F7FEE15138D4ADE19415536A45A34A2AF58CA8E6A725233F3E165C0
            E2D71EF111C58F251EB8124C46C5E9F4AFDD664188D30A97CD4A6C40B78ED82B
            F7EEDDBBAB4183060C16BAD329AF902AB8DFE4E464777C7CFC3FC97D79F6DAD5
            56CFB164C06A0354401ED7B5D81D0C5AE073D8E148AC0567FD86E0DF8FFBF7EF
            EFF4F6DB6FDB39BB9BCDE701BEAC4C64AD5E095F7E41212F911677082C56F24A
            3B761B5C1734873D2A8631C878E59557223526AB68D94E47CFBA92FC4A814A6D
            9198B10799ABC9046CFF2D0D63A62C411E152D3D0B57083E7A32177A655C619C
            7E5D546AB8C382E87017EA25466240AB44F4689E887097DD9F6BD21B6FBCF1E2
            B871E3D2FD91A753A03F6D05C35790FE3D5F7E5EECF6DE5D919F9A8A90018311
            4243B6D7AA0347BD06B0BADCF031EFAEDAB5E1AA538FE4187DE4C891991B376E
            B4F6EBD7EF281161E98B1660DF1F46C1D6B61D42BB5E04A7C15B1F8E8478C0E6
            84CFED42585223D82222498E541A51C2E4C9933D7AA8089C4E0FACAF8A883B25
            6D651BD28B4CED6102FEB36A17AE9CBC182151216C6A641AC29E3948093AD7EF
            659356E0F12282AC2D1AC4E0A9ABDA616887FA7C32DC381EB4FF60F0E0C1691A
            C01B98A21FF117052BC57BFDF5D7C369B03329AC4FEEB6ADD6BD970E804FABE9
            51D1506BE265E53BD90AD96AD7859D061433F20AB8D9AA90BEC98F3FFEB8A756
            AD5AED121313D7F01947FE3205298F3F0C5B5C022C912C99DD090E7EE04A4880
            359106D8BC35E26E18030BF70C49BF7FECD8B18DA64D9B56C032554891A4277B
            F9EE7C35241B33F619B37D3901E33F5E8B97676F464C24DF50F555425604FC1A
            2D05BDE4CDCCE0042E2B17EFDE7531C6F46D468C0E0A14FCC48DD2815F7FFD75
            8A10AC2C0C1C3810D3A74F8766851CA40A7DD6B07DFBF63F7205FB750A8A4C9F
            351387EEBE05EE3EFD60A5D1B86AB3358A4F804F4DAFD305844720A24347D813
            6A901C377FF4D147D3070CE8FF4CCD9A89E3853838EE0E642E5908578F5EB091
            37B4662D20C40D8B93BAE24D153B9FC3BB5D04CE3644BEF1EAABAFBEF0B3CF3E
            CBA3619C912191AEB08B3D8DDE595F925F2950992D521D666C0B731546C05593
            1761C6CF07111546C50A510960659791955B007B4E3E163F3B0C1D1AC649AA87
            4A0EBBFEFAEB7365340D1B36C47DF7DD87071E78003A8672BAC129F36CE1F925
            5FD3A64DB176ED5AC92B06C619E18F3FFE585B304BF9D09360497EF3551C7BFE
            19B86908D6BAF5D885D585B36E7DF8D882D8A3A2E0888B83AB4D7B8E79ACB2F4
            E757AC58F1F885175EB891F969E1E195A87D575D8282FDFBE0E8D4050EB6606E
            CAB0C6C7C312160E1B1756DD4D9AC14E03635A5A648DEBD9B3E7F10D1B36E8D1
            2B2351A03C30E39577335C1EAD68CA8BAB283E28432A2791D6C4FF4C405A761E
            063E35071B8F6521A4E49846D16705168EBD9329F74F439AE3D5315D0D595C36
            F88403EEEB0F1C38E079F5D557C1F527DC79E79D469C9479F1C51727D0C8F2D9
            A2E4376AD448F1362E1F84718D2BB64B972E164EE7533888FE6DCF9E3D5EB63C
            D8B4691356AE5C098E6D70C71D77E0D65B6F0DA1B05F084904ECBBF57A647D33
            0FF61AB5392672C2CB01B2C766873D269A86100657C7CEA871F7FDB03A9D3878
            F0E070DE815BD2A953A754F23AF3F6ECC6AE416C891C4E766B911CABDBE065A1
            7CA121E48F056C0EC4DE722B22FB0E2439B06EDD3A5BC78E1D75F7CFE5743A73
            7FFAE92730BF465CA01F96572F72228D2492E15CD26C220474A409880F061994
            21318381D29A60B1589E54C4CFFB52D1F3E9B954A40D568B45A83220744EAE07
            B9051E56038751A4703A6C703B390B3A55034E711959F9E89B148B998F0D82D3
            4ECB02327AF5EA15BD6DDB36CF0F3FFC605C55E2D849959FC47D380F5B83CFB9
            66558349A413245D23F67086A5743599390CCF27A8495A4AFF5B82BFBB840F33
            09565F4E3676F6E9C601B515A1FD0721B473571A54A2314BB373C3D8EAE4CC2D
            946239C9203DD6AF5FDF824B1447478F1E9DACE7CCA58BB1EFBA2BE1225F58DF
            0170B56A051BBB4537C75756768BD6905070B555A4067CF9E54CDB8811236548
            962FBEF8C277C51557D8A47FEADA63109CFC0967B017E166423F4234C14B984B
            D8439E85F4671084A3A709A3C5F02BE3C7C2042A438E64CCE1CF100266ACDA8D
            51539621362E143E0E9685F3071951764E017A368F47DBBAD17C232D90717CBB
            E93076A66623CC6DE79A903F47C9706E9E074D63DD98FBF860D48894BDA060E8
            D0A16EAED57876EFDE2D8379951CFD0989848A3A0D6A6793E931C22FAC307A18
            C79F2904646DDA88FD570C87A5200F3EBB83B561E574DFC91916EB9133363677
            887FE01144D3C844CF1629915DA78EFF1ED4F391979F43CAE4E7608B61EB2393
            B6F1962F8DC7A6DBC16CA5EC8D1BA3CEA497D93D720647863FFDE94FD6D75E7B
            CD3771E2C4F0C71F7FFC76A25A127E246C242413980934A0DF89D08BD0961045
            286D25AC6ADFF7C44F24A87CC6388AE14A71411912735426712A7C1E918308F8
            F3A7EBF0D257BF209A951C88D647A2EC8C1CCC7AB43FFAB7ACCDA742B7EB683A
            46BFBA04BF1CC9E0D8D35ED84C15469DFCA57AB2B30BD0BE5604BE7E7C10A243
            9C60D796C3AE2C816389881B6EB8613589EB12CAB802267C3C33CF38894931B0
            B3058C0E610B5286D2401C61DEAF626819613CCBF71C7D1C9BFE771C9BF81442
            865D0A67F39670376CC8E93BBB381A8606C73E8F17F6B878D80ACF9AFBD8D5BA
            39660B678B784CFC7BFF7005F276EE40C82597C2D5B829DC75EAC15E3311560E
            D079279C037670F65613B05A759338E5AEBBEEAAC132A14F9F3EF9E467099041
            FF08E130219D60CBCCF7C46566E5B33B439C173EA7DD66B3C486B1EB646420C7
            72BD4BFCDD2C93877EA5B8CA32A446CCD44AE6A80601C35E5A8845BF1E4684DB
            01955C387FC82BF020C161C5FC2787A271CD08FF28FC65FE668CFBF007441BCB
            0625A20A1F2C405A7A3E46B4AA897F3FDC9F5D2734A0CD3976ECD8BA9A356B76
            2F242AFC4DA6D1ACD971142BB6FC86DF8E6422393D0787D27269781AD5B0C238
            7EABCD5965424C08DA354AC0E076B5512F96DD52213B5B45DF510607B06C63E9
            DF43C0A1A71EC3F1BFBC025B2C2B9B5D912DA126BBB65AB046C7C017158ED0A6
            4D1177D35870FF47FC73B810399CE3AC0BB8DDB3D9939E86DD2306A160EB1658
            22228DD99D3D219186571396A8304224A2070F4378EF7E4A0AB939393B38BACE
            E598AFA58128FA39929EE3FD7EEB11CF8AAD47BC878E64580EA5E5D9523373AC
            6CC72D7954B8C3694783283762239C685437067D5BD642DBFA3170DAAC4512F8
            8EFA7CEFB15C77125129C614942131F1D26E2811B309C8C9F7A0DB635F61FBF1
            6CB8F9C60B571A3239EBEAC01665F184E170158E718A4966AFDF8FD1AF2D8633
            8CD3E162AC5F8086748CDDDF031C6CBF726317BF8893C1FD299978FF9B2DF8F7
            F7BB7120390BE91E9F310EE38B0A1BD3B3F089FA5645C343A3CE67F7EB247BAD
            08176E19D00C0F8D68C35575B688C4F1EDDD4485E731D8DE57908FBDD78F46CE
            FEBD081F3404A175EAD3601C000D4AAD91D7EBE178270E111CFBE8F9D0A14331
            292929275AB56A753FF95FCDF97903765F3E148EAEDD117E6117B8697C707001
            92A035234F6E3642DAB647488B5624379CB2C9121B616CDC9F8A0F58AE196BF6
            E1C8891C6430DF5C7331CA64B3D14834DD2735F3CC97CB8B0296DB4A09B17C69
            BBB74CC4A3A3DAA25B931A85C2F8CB7DCB2B366CD8F05F1D59E6848498E05D50
            86A48C964A721895FDB5709B0E1CC780890B90C5426A9B43387FD0F8E8385B85
            6BBAD5C7F4BBD5A5FBC702D3166DC19DEFAF42945AA49251C693D66A32533331
            E3D10118D2B68E81F3FF99B5660F1EFD682D361D4C43089B77375B452B0D47E9
            16D251B38581A25F6A9E216173F20A90C917E0C65E8DF0F66D3D10C6168B5172
            5EFE580BB825B27B685F636BC3D1A2255C8D9A70AC1367188F233191630E4E16
            5AB7E194DEC8570AF7C76AC4C6C67AFBF5EB3787FC83D366CFC2C11BAE81A343
            7B38931AC3D9200956DEF4D57A932D321A564EFF432FEC0C6BA99B3354255EFF
            FA674CFEF2171CA20185B115D5C4C47AB25014EF335AFFC2D2F09165D6AF6CCB
            C395DD74B6C6712CCFACC706A24B93044521EDC489CF5AB56E7D35B7708C97CA
            4006F9535986349C86F495F23063ED5E8C7EF33B846BEC71B2548A328074387E
            3C0BCF8DEE80FFC737DF40FAFD5CF6E202CCDE740451E17C5355BB7E71E2CDC8
            CA439BC4302C7D663842B8A7E5178DF716FE8A7BA7ADE28AB003E1E4A776E4FC
            490A9F992FBA12783DA85EA4F8632959987C7D473C78491BA18B21FBA7F5D877
            F565B0C7C7C2E6E24482ABD90865976457ABC46500B64CF1B7DC86886EDDC5F3
            C677DF7D777F5C5C5C648B162DB61051F3C88B93903AF50D3839A0B6586C4068
            843143B3706FCDC3D6DB51BF2112EF7FA878A04D1EE6D7873FFDE37BBC3E670B
            D739DD1CCBDBE1654BA338137C0C28DF5C666328B0B3B2C54A3991856E0D6330
            EFCF83F99230CFC0766E24375FB3668D2740E310585039D8A00C2980AC27899B
            40C0E4591BF1C8A7EB1113E562472C4C4950A1B3F876FCE7813EDCEAA85722F2
            AF0BB7707CB406616C494A44143DE89DCB48CDC5670FF4C288CE0D8AB085DEE7
            AB76E3FA3797C119EA828B95524231165D75F2415DAA9DDAE60BCA712D5B0F2E
            3594A02B14856CCD0A39E35CF4E460C4FA2DA8A6FCED5DFCF6E84308193418A1
            5C4C745FD0024E2E285AB97EA469BB358AEB482E969B72B86E35920B9C5F5F74
            D145C97C8C963276B135CBD9B60D61234721A45973B85BB682233E812D511881
            8B91717145ED08398ADC4B337FC2F84F7FE4983154E36F1A5651449197C5D9AF
            D7E78583E552171DAA16586F4451BCBF2763CBE6B062E16303705153A38BCBED
            D1A347D48A152B7203E9C19FF774E1CA32A41F99507B026E7AF73BFC6BE51E44
            C8187CC29404ED9B85F1ED58C81957F3DA513C1DE0C391F45C4C5FBA0D93666C
            04F87696EE12A51735EF29C919B8AD7F53BC7BFBC52584EE3D96818BFFFC3552
            F27DC66CCF5F29E2CD2BF082ED1BC6F46C8C8E8DE290959BCFD66B3B361FCE80
            93E38712C2F8A0B43C54F83C2ABC5BA1C2890552FFFD2F64CE9BCDD6C86D7423
            0692D3773613DC234B84A37133C4703667E53D3DDE146EC28DD6BC891327EE15
            9D8F1FFD3A32F109787EFB0D3086C5E0CA372D9CAD99252A8A03ED1884F71F88
            F0B6861AC582B5BB923170D202789886C36625CE5FA1166465E7A35F8B048CEC
            521F915C3259BF3B05EF2FD9817C5860A36191A184932165B1FB9EF7FFFAA357
            73C390D6D390BA2F5FBE3CBB0461100F9561482AE14EA6DD209BC7227A3F3317
            9B7FCB305A05E2CA3815C6C12322B5D962A992F59C4A433A9C9E0717FB701991
            700623F52C3F870B973999B9B8A16712DEBCE5229E00309A6545816D326E7EFB
            5B7CF4FD3EEEEBB9F8C6FA2B9B2D1185E570F636EDB66EB8F6E226068F7E566E
            3B8CA12F2EE466BB836A17A6241C67ABF9F64D5D705BBF0B4E4650160742259F
            65529CF2FBD82AF0ADE07E1987ED16CB7E9E1FEFFBE4934FB6E61AD01764E062
            137F8BCAC3D04927996C227DFCE6139B4958688466E4AD7C29A7AFDCCB9792E5
            F52B96BAF87476F1C35AD7C447F7F6E2C480F1454C8F7CF4035EFF662B8716CC
            4711CEF49494EA4886D4F302C3903EA3218D5DBE7CF9099326583F2843F27FE3
            99702B166C39FDA84D07523168E27C64FA02BF11A42976B99CDDA96042C87834
            EBF0D7B38C2C97CD767E6E1EEAC747606CBFA6B8FF92D650D7241E1356737ADF
            F799F970B30BB2A852CD08F91498CE8DDEC12D6BE2CB470608530C2934AEF6E3
            BF447A9EB7CCDBABB4536948E32F6D8D67AFEA50CC5381C0173CDE32856B483F
            72C3B6232B6B5905780DD29D47D2D175FC57389EEFA57D59D5331A78FDB0585C
            C228C0B74F0D4197C6850367E105EF7EC3E1C1F41F383C70957941A4EF6CB648
            73B96CD2EBA421DD4A434A13EFD9406518D25D34A4BF281333D6ECC198775670
            BBC8C6F51D1557D8E0A080DDD140AE155DDB3D09DD59E89A5CDC0C24E98EF757
            E0FDA53B11CDA9BB145582865938C119E25B7FEC8CDBFBFBB52C24DACDEEB0EB
            E35F1BDD40C9D90FBB1CF26966F9E4E56DF1C4156D490D70F3771BA7CBA9DC78
            8DE69115BFF641F4162E1BD9EDDCBFF371AF6EC3871F7EF8F2B3CF3EABEE1EAD
            5BB7C69B6FBE79174F22B4EFD2A54B165FC27D84FD145A4008A5EE92E8B7E23E
            5A63FA0DC1D5250266AED98D27FFF1031C1CF3E8D91F3C6CF95B715DE86FE3FA
            96581B12CD4D6A9D57ED45A4861642F881BAEC5C7E5B61010DA97BB39A8A518B
            74130D29530F6703411952A9049FE2F3D3E0DFF31C183EFDDF9F11115AB65965
            F4193B1F297D9C99744D8A41D746F168DF28169776AC5FA6E548CEC845CF09B3
            B1EF786EC0352B9D65B2517BF3FF3C101D1AC452EA49F7C3CE642E5370E3D55D
            7605D8689138CD7E6E747B3C7C691B8369C992252F3FF1C413AFF0866F3C2B5F
            59149E41B6BE369B8F63A283DC34CEA6C1E5737194B6E2E34A85C5C2232E3E6E
            BAE25FFFFA97831728C3C8E420938BBE85E061388F0698C77DC2EC091326F4BB
            EEBAEBE6128F4CB61CD96CB535D629FD8268D2E1E658328C0B8FA23561CEFA7D
            B8E1ADE5F070AD4C7C26DEF43556AC116AC3922787A26EE1C2EB8C6EDDBA5DB5
            6AD52A8E104AA762729D991F9421514BA67429633A95719D1037BFC381F6F77B
            107E1A435205AB2624475D9AC685A58B4199D00C239B6301079536B2431DBC35
            B61B122242949401FF650BF88737BF4518676AAA7C03E9F7A3817D1C07A1EF8C
            BD0871E16E0E61942AE0E0C0F5B3EF77E1B5B95B8CC1B91F8B11145506BBB60F
            79EE69F44549068E0B8BEB79ACF757AE50ABE561F62C065E016E7F64F1E4C03B
            9CFD64BEF4D24BC934A65446E6B46DDBD6BE60C182387E0BE0F88E1D3B5C3CBB
            4D761B87595C122DCA30F9391BB38274E0B5EF1BB815F23C0D4B864611A77719
            3C56B3890B955FAFDB8F773936CAF2022E1E512EAD4F494AE704A25793382C78
            6CB01EB5B9BD8ED3FF6EBFFCF20BABC2A716D2C007F373B686A4D77C33955143
            B38181CF2DC0FA7DC7CB3D3A42F5B16FE76E1007A6362A922F0E4E70DF8C9A44
            5888BDC43840852189A174ADD0A6A66442DDDCDFEFEE69EEF863DC3F56E1ED85
            DB02776B1240D0B8C945413CA94143623764E0801CB678DC522F338E60343C6C
            C52C9C65CD7E6420BA354D10EA8CE1BBEFBE7B951BC88F7021D2C92EE373B642
            43CF90791BE9EA7A3D9E74ABCD16CF300746FCF573791CD4EFE2D869D7D10C6C
            D87D0CBFEC3F815F0F9EC0BEE44CA464E5F3A570F2301F4BAC37C18F4F41AA00
            27D882DFDAA709DEBEA99B50E026F7821E3D7A0C4D4D4DB5D392720D64903F67
            6B480D68443B99B675370BA719DB710E5E352B23AE8CCBE6ECABD7050978F6CA
            76B41D2B5B060BF6734AFFE6BC5FB1746B3215E1204F002D14618F73A1F0AB47
            FB6168FB7AC6F1930193E663DDDEF20D976C8653CB545AAA9AFED2632383983F
            F91C9FC5B8D9053C31040D13C289A990BB9BD3FE99DC72E8C16EE3D30A711612
            2BAB02C3900A68D4EB69345FACDE8D955B8F62378D465D7A3E677A16BE1D36AB
            0D0EBE91763B5B487115CA28F3AB8B052752B3F0DE6D17614CEFA646FCA2458B
            3EEADFBFFF8D7C70D190B2E907ED823224333526AE0DCDF97CB62CFAE51046F0
            54A48BBBF12C1251259DDE88540E7C9FBEA20D1EBFBC5D89C80C36B9C339155F
            B39787E9B9045022B2E8418A48A522EE19D00C53F846ED389C8EBE13E7E138C7
            124EAEB3149155C82B4FEF3AE2D2A94134E68F1FC8A9B51D9EE3A938FEE5175C
            FF61DDB25530DA357E9290236C58E53B9C0869D3CE3805C00C8CE6A76DD279E2
            B2575252D2A37C46D6BAB5841F6031B63ED86250417C01A1454C4B3817222923
            FCC2AE3CFBED1679317CB56E1FDE9EB719DFF225CBA5E1D8D8C5AB5BD62C57A7
            45D5C2B30E90CF3CE571113594DDB8E4160BF00B089F96968D6F9F3E39D3E377
            9CC6F37CD34B240BA19C4CFA41BBA00C89899A094E6106C7E961EA825F71FFF4
            3588E0ECA9F42C5CF1AA348D773EE1BAC7884E2557B415AF3DB63BFEBE9A6B41
            2165D68214CF74909E99837EBC49329BFB6C0B788C77D49425B073C0595ECBA2
            B158465601BB2FA5AE212A1896B453431E9706AEEDDD041FDDD7CB20CC58FC0D
            F65E77055CDC6CD5115A7BADDA0869D2044E1ED4977158B94DA2BD321D9325C3
            DF1FE3DFCD37DFFC2E4F648EE0330EDC733BD266FD0721DCD977707FCE59B71E
            DC4D9AC21116014446C01612CA13003C6A5BF4426890FDC83F5761DAC21DC662
            644498A330E32A06054A1705FC202B8F8EC0CE373492AD67E7C6F1F8950BAC47
            B9DEC6411CA94A3AB5CAE15C7CFDFED9E1A81B136A44F29A533BDE50D9C48750
            D6691AFDA0DD5919120B3493291BCABAF3BD1598B68CD3F0482D0A125BCA15F0
            8D0AB55AF10D67502DEB441BB11CC0AE8E8D8DE94C2D59F4F65DF6DA92531812
            684879E8C3A580396C29A6CEFF150FFC93DB29BCAEC417DC90E7FF23230A77D9
            30AA733D84F04DA6A2980C298A2A83A1729DC67B43DAD531BA50111D9BFA168E
            3EF9086C0935B83716CA95ED50AE4447F3A82C0FE571E0EE6CD400B5C63FC963
            B64637B8F6F9E79FBF82E7A31645474737F6E6E662EFD52390B776352C31DC02
            71713B24241C96588639B5F7DA1C881A32087137DEACA48C05D6DBA77E8BF717
            6D474C028DCCCAA1A35F9ED532CB80C22D3E0CEE5017BDB944D29933DBDA9C85
            0D9A30175BB9E91CC297CB1066FED0D8323969E99D148B593C0CA8ED1446797A
            F6EC19CD319D4761EA278F7ED0EE6C0CA9160D6905536EA8E3B2FD9E998BB5FB
            4E409BB5C49571DABF6A59230CDABF8AE0949B7B5107F871AC5FA8EC41227E7F
            C916DCCE0DD798286E86069872302D9CE04C6A54A7BAF8785C1F3CCA5B2A9367
            6FE240BB64772059826C76799D1BC6620915A7E7A08179D97FCF6DC85A381FA1
            0387C0CD2D0C5B7804CF1FC5F2026314BB3617F8D5538476E864AE4AAF61E5FC
            816B461BB836149ABB733BF68EBA84E7976211D2BB1F4F0C346517C7168807E1
            6CDCF4D555260BBB36EDBB298F1F2ED986319C89C6264680B62254314807DADE
            A913EAC05F6FEF8E3EAD6A17C72DDF7A1843B818EC0873955DC3E39B967A2217
            632F6E8877EEB8D8E0E179F55D494949CD7EFBED379A2A7443C5C007FB733686
            D48D055BA9847773C0DC9767B48F712AAAE30DC295860CAE520F699D88190FF4
            35A2F8A5B5C39CE67278638B11E2062EA4FD7BF53EAE41B11917A214E84D3CC6
            99DBC4ABDAE3318EB1FEF0E6127CB1663F17DE5CA528C1991E8C19CA1DFD9AE0
            8D31DDCAC4F3ED337A5FE6BFE4EB5E44A906C0EC2E75186D0FCF1015ECDA09E7
            052D616FD408561EF9B0E9F807BB341FC747A1DDBA23B46D3B839B53FCAFF89D
            C9876AD7AEBD9908CB89797370E8A6EBE0A8950867B3E6C6F5258D8B1CE4B746
            44C167B3229A77E0EC3C9B94CE3DC08B1F9B856DA939C60BC97136459C747AF6
            B065F98A274B7BB6483C19C1D0DF6880B74C5D8EF8F8F0B243030B709C67B85E
            BC8E271A86B726B571A96012D7B71EA70EB4B695299D181141FE9C8D215DC34C
            7CAC74176D3A84CB5E5C043BDF14B302843781E5C0095EDFBE7FE80578E99A4E
            26BAD8DF7C301503267D834C9E85D540B238A254202D2D0B9A920F68531B439E
            5FC099DE112E1B384B51151A524A5A0EA67245FBB6FECD4BC467D2D81F9EBE1A
            4739150EE12901AEC471DCA41CFA68803C3BCE8DD02BBB36C0B53D9B187CB9DB
            B660CF25038DC3FD96581EA1D5D5EA30764F3CE16871398C816ECCA8ABCDA323
            BA7DF23ADFF677FAF6ED2B43C2515E5D4A9DF2321C3C2960E379232B4F0758C2
            232163E25C1DBEB050D4BCF33E638CB4E0A703B8F4F96F101ECB318CACD9C841
            E10F27683C199A8BBE9CF5CE2D5A072A8C29FC7DE0835598327F0BE2C85BFA9C
            3C77AC50407DCCA4019AAD1857DF3BF39B02EBC86D27FCAE2DD29734A44B9909
            E8F8C7EDD3BE475CD1204EB812C07A4AE7A6EC27F7F4C0E55D1AC2FF4FE391D1
            539660C1E6235CC8646B544A81266D0E0DA0318F762C7D7A282239331CCCA9FF
            773B92B91859964722B279256A265BBF81343A5386FC759C4AF7E455A93C6AB7
            B4D1EAADCCE57ACC3FEEE9891B7B69C782E332EEF61FBCED8F70F5EC0D7B8BD6
            0869DA04EEC4DAB0D5AC698C971CB13C9B44A3926C0157B76FA59C1CAE1FFDD3
            78BEF36664AEF816EEDEFDE1608B145ABF019C3CA36D898BE53DB60838136AF0
            94A55DA478907B64AFCDDD8CB8684D380C54F18FD12273EAFFFCD51DF048D1B6
            8D1999C78177E74767617B4A363473636F6C4619BE963322B93CB0F6C511A811
            E13670E3C78F8F7FE185174EF081B5837CE699C1E05DD02D128DE867266BB493
            7A1B5E5FB015B18602548D8CF173C2E4718A3F876F84B62AF289E09A24D6EF3A
            8A49FFD980E53B520B0DC28FA74490454D3D9E83A72E6F8327AF6C0F2DCC0DE3
            9BBB5C86C403742568F9A081B6C5EBC5BCF103706192D6F6882C72AF7CCDF352
            1FAFE7C5045711E6A4A7F33C715C155E357138128AF6F68EBC3409C75E7B812D
            064F4072342185FB5C6EDE1A8904A222E16ADD0E890F3D0A754DBC84E05DB66C
            59973E7DFA8CA37E6EF0E66463CFC8A1C8FB7533ACA4E5C4DFB8C604B66AB6E8
            587823A3107BD9E588BD760C72B97675E9A4B958B65BE3CC42C33A9933B6B25C
            FE4F3E9A89293774C2B84B0CB517477FBC7C07FEF8F60A4450FFECD78AF10A30
            1FC8E02CB47B520CCFC80F81168299CFAD575E7965EB19336628A17CD2E9E60C
            BDE05DB08614C60CAE67B24D341B1BF6DC7C2CDB7E8CC71D3463A39530A2B493
            12E3B97ACD19A8313EC9E2AEF6D1E379C8A782C289F705666377C35568AE9144
            B382BF9B300C0D3806509A2374C160CB51632C513A2DC9CAA2E1CE7CB00F06B5
            3E39203DC41382039E99873D1C78867046273A9357EB32C91C47DCDEB731A6DE
            DADD4463DFF557226BE346845D7F13A2790CD6C91BB0C6F9218FB770701DE286
            BA2C6594679FB7BCFBEEBB7DEFBFFFFE75149098BB633BF65D3E0C96962D1131
            7C24C2D9A2D9D91259A378FE288FA7866890165E8CB4D03FC035B241CFCEC35E
            1EA9D1C13CF29770D4370D221703D8B57DF6507F98E7E1BFFBF5378C79EB5B1C
            C9F5425B238672FD38C597C272DD3FB0195EB9A9AB11C3AFAABCC8F1D19FF7ED
            DBA77141B6907A41E4070BC11AD27066702613B51DE54CAA330788C7723CE59E
            41229DE10AF8D6999567E194C466B753FFB4203A8320C08F8F53D7142A59E39D
            3B06B528A6B863DA0A4C2BDAF52F46FA05346BEBC97DA567780C248ECDF9CEC3
            6978E13F3F61C5AE14847367DCCC87C9A275161BBBCF651387A1156F5E08EFCD
            48C79E118351C016C512C5FF95D219C6191707B3A14E7E002206561E958DBAFC
            4AC45C364AE460B736994B1A1FB469D366A310E9F3E7E0C02DD772B980171978
            AF1F2194E10AE10D122E21B085D2B5EC9ABC03E76477B7955F6F193C691E5268
            105AA9167F19A02E72D9655FC4C3791771F77EF79134CCE37A9A2E0184387963
            A774A128802CD0CDE4B76EEE8ABB07352706D9DC201ED6AC59B3157CB0117E57
            43BA8B86F41766026BB88B3E98EB175E6E9CF2BC9A509502940FEECA218577DC
            6E1FD40CEF8CED5142EE7B3C967BD7DF56F39280BB04DE7C900275E932942D5E
            3CAFE51C3C918D7CAFC558302D3D18D5F823F9501A9EBBB623C68F6A6F8A4036
            2F43EE1939044E4EED435BF1507FADDAB0D7AB071B676A3E9B9D77D1C2B99A5D
            0B8EBAF50D1EEEF08F69DEFC82A31D3B769A2D44F29BAFE1E8949710CED6C84D
            3ED1D912F80512BE383ECEF8B480E96ADA0C368635F31DC02594239CDDF2629A
            D80303BBF90C4E5CF2D9E25A795498BBC070B3B596F105B0234387D91C9FCE78
            A80FAF5BD595CC0DF3E6CDBB7EC890215BF9406930F6D87EAF1649DF407C9D19
            316E7DDCC55B1F11BCF5A15C0977D640411A5C67B199BF634033BC714B371E68
            E300C54FF07EDE24B9F8F1396CD20BA0DB1E81142163D40AB05A42076768BCF0
            CF21046BB1488E8C8D7BB748E53EE14D3D1BE1BD7B7B196388A268A47DFD250E
            F07AB5A3652B7EFBA8216F8B247035BB0E78870A5AFF71344C42D4D0E1ECE29C
            62F17EFAE9A7BCE2DFE916AE68DF23C4817BC622E3F34F606FD711F6441A5C8D
            5AC6E09AF60C5EBD85BB536784F3EAB668758D6BE83373B0627F1A22028CFB44
            6382CA25E357F7DD382E8C7B96566C4FCE809D4B09268DE96BFB249C85FC66C2
            10B4285C089EC38FA83EFAF0C30F6F268D87E02594D08B9E2B0AC1766D2FB030
            8F28B1FBA6ADC45B8BB71933B64095299A53016D86D185BFDAE5CFE65A8A978B
            8949F161187F591BFCB16F33C6173A1E2C3BC637909BF99670613E5CBA1D63DF
            FB1E4E8EB1DC5CCD3DD3F42D4672166472AA5F90958B3B0636C38B3776E5AD14
            B5F4925C08BF3D351EE99F7C0457870BA17BF970F3D4A1D5C1300FEEE9D03EAF
            20C55E7B2357B42370E2C489DDDF2E5BD66DD8F0E1FFE6FA582F4F5A3A760D1F
            C0A5050F74ED88D77A6155B7C66D104B78A8B132EEE6C726A2698885A90153E7
            6FC65D2C4F7C4D76815A3432234AF9D43D9753F2508B5B275F3CD8179367FC84
            193F1D42046FCE946E957406298EFCCB9EBFC4BCFC398D5F6A7987478037102D
            4332DEAC33D51D7902BA600D69A9C562E9A59DE9FE4F7C8D65DB92E162A14A17
            22608AA5903EBE0FDA05D3605C57903A24466264D78618DDBD11E2B56F77923E
            F3B2CB46FEB16FDF7ECDC68D1B37C9447FF4DD763CCC7B6C87D87C1B67BED995
            A9E5B19668C0289D99E3AD551E63F1212FDFC36BDB1EB4AF136518EBA86E8D4C
            71277DD2EB6311F9070FC03D6418421B26C1A6697FFDFA70D7AA637CFBC81615
            5D4CBF73E7CEEEFC68FC2A0EB48F12199BBDE147EC1DD2974B06ADF8F99B8BA1
            6F2859697CCE7AF519AE0BEDD7F99FCF260F54E937BFB5141FADD8CDE1540877
            F52D3CCC676594EADA621C6FC92FF020872F40476E2ABF3BB63BDAF1D33EDD1F
            FD123FEC39CE1B349C8489941CA6D30C37D6027CFFD24834A56E897FFD99679E
            F984DF075FC5B09CC1F17B19D2121A526F35C793BE588FA35C7B29FA2A883276
            C6C0F221D26545CD987034E0718D26DC1668C2B7D151D20AF4B66F62054DF8E0
            830F16D4AB572F82FFE7C7D4C68D1B0F3313DAC366FD8BD5BB31FFE743D8C76E
            4AE7B133F2BCF0700A232DF12819F81541C470C1349E970BDBB2122EED509F5B
            0C8988E29A9429A7C8A769C3AA5BB5875E7D09DED41463F5DB50B48539E606AB
            8D1BAD708722BC472F44B62D5CD1E657506278F02D6DD4A8517ACB91F5EB261C
            FBF07D58F9CFE729E09D7E1A338D5C9720B5C96BE1974B6247702398EB4845E9
            1A9E74FAF7C5DBF0CFEF7662EFB14CA4F2E80DE7E6B0D1B063B8BBDF88C67015
            6F8DDCD8AB09A238E857D7F5D69C4DD87C289D1F31618BAA021B920A7F8C0557
            4E72EE1DD60217D4320C7FCA5B6FBDF5FEBDF7DEBB9914A256797FB7AEEDCF34
            A489CCC8B976998B172FFAF2C1071F9AC0CFE71D63621184C3DC5CF771D77A3A
            3F153C8ACF25DC210EAAF727A72139239F6FB8DA3A70871C08E580541B9B75B9
            7C10CE6EB004131F0E1F3EBCFEABAF66FDE7965B6E7D908F51840A396ED2F2A0
            A4DDCEF3D93967C448C3800CB390583C0E066909FCA5534BA2938F073963A52D
            192F42FDF850B4A81B8BA24D575205E5A6F0E8C8641E2139486E357786E11B2F
            0A11C1BA60BBB6A634A4E54C3481702E5C2E373EBF623FFEF759B366ADE5BE9C
            14AC1544360F38F1ECB3CFFAB89765AD53A7CE3BFC7EE4CDCC80E2E905E5F298
            C627DC2E98C093822953A64CF933BBCE872A2289EB478743424212BB77EF1ECE
            EF32ED0F0D0DAD9021F2D337EBE6CE9DFBE155575D750F3F81D3A42269579496
            67CB9FE0A9C897F43131F2CA8804BF4F8BC40CC875E721F76768502DF92A0695
            11BD051C40EB43EDFCE841413ED76156F20ACF1C56E8D1850B176EE00AAC9A5D
            19501E133C41C82260D2A449DC9C8CC7830F3E884B2FBD74E4D34F3F7D1F5BA7
            E6090909218CD740DC413F9053539EC5BDB00CE6BD8006F00B0FDC4FE307AC56
            302C7AF65D489E3A75EA4D975C72C99DFCFF4DF4A2884771C5C032EB84A78506
            EEE3207B1B0DEF698E8F1613EF5AB97265277E34E2F1ACACACF6BC2820BDA8A2
            36913993E0230D1B221E49638034562E0EE6F2ECD25FF83F3CADE107C15A33ED
            3FF1EB731D59969AA40F6490278E1E3D9AC9FC6FA4EE522223233BD1F8C239C0
            277960C7A4F4891C2FD7B87EB9EDB6DBEEE2EAFB4E5286118C3CD1573EE5050D
            41B5484A6DDAB469BC66F386253333AB09DF46EE48704F421167082A1C0DC5C7
            83F23E56A297BBE65E56703ED9056A61641432201DB82A2E30E30D43626B047E
            ED15AC4CA122D942C5F6EBD7AF0EF7B81AF03FFEAB49C586B1E7901CC5D366F9
            51E3BCFC634CEF00BBC95DACB80CA65BC04855B416A394EE113E0BC7F186DB95
            949454978C56E2BC04C331DFFCD0888DFBAD0E2BDF6ECFAE5DBBD42D49462A09
            BC3CC09FCFAB475A9C0CE36776B8FAE011AF9D719203CE3A6584BCAB66B7F0E3
            0D1E1A85D265B4E124072C9B6DE4C891F684F8F8689E8A9441D5606CA8C7E375
            9167C7ECD9B30FF14F7C3EA665494A4A028FAC70C1BFCC544F697BF4A2331D1F
            79F4ACF2291D95399B720DC7721A7EB03F411B52C3860DC19B134AD7C99F2882
            2ADE5016C3C1380B99F4F61B0B640C671064402A3C83279D5A241A0EF8112ADD
            843023D40AF9B746A5F9245FF993718956CF0255885ABB406949D9314C406514
            2D83C54E79154E2019A98C910C7A86535A1ADD4A86F80D64D18F781514AF402F
            CC71224C7EE53194CFF2550E55BC7C81E48A4720BCF9AC3464B0642B768AF3C7
            E959F9CC228564D12B74BF9B21E9BFDDD487310BB361FC4A61CAB4A924035981
            1F29468553214FC9568E21993CEA0AA5543D4BA6FCD2A03C0A84577A4A57E140
            2019AAD44071C229FE5432F482C920CCF4C4531AC41F285E062FBD8A5EE9C817
            8856A01645C6249CD2505AC2EBB93C104F99F2FE6E8664E6924DBD19AC2ABE5A
            18BD95FFEBFC2ADD478A127DB4C83F53AF11093B11BE2194C8FBD91A00E5558A
            0BBA6B33530F60485731CE54D8670CBF48F0770BF820A5D22B7652D05FF9B493
            603AC9902C3D2BBEB4F225E3DF8C944F0F57F3C79F9F8FC54E726EE3932A43F4
            6B19567A0206CFB9539A2AB712521EA493D2E5515C79B083113226F188978F85
            EEFFB221AD6111A52C7A50855DA840114819528A1E1527DFA4D59B36900813EF
            2F4771B18CF3773222198889533A26AF89932F837C41018219AF7CC8E8C443F4
            3977EF3205A52963577E54669595E83372A6118A5FBA2866FABF6A4852922A4D
            2DC800965695D598BEE9D42A48A9AA50B312A560198594AB164CCA124E0627A5
            4986E2244761C9920189C74C4774A50DCD9F4E61FFB7592D8464982D92F2AA34
            84974C8178048A135E7953FE9537C50B142F394ADF7C3671F2C5235FFCE295AF
            3298E92AEE74F2C5239AD26908579C671A947FFA5A62507ACAABF8FECA78F94A
            EF9C4065766D2AB02A5F199591986155B0590819998C4D8ABC5D8445209CE24C
            0353E5C850F42CC5EB590666568C642B3D35F3FE7C45E28A3DD14999A2932115
            4714052443E9A8528A5086E79F3FC910526550C5292C508BA2BC295E712AA7F0
            AA40FF97456513282DC50B542EE94861F1CB978CD2F2651CE255195576E94072
            02E699C6723B871A8A570BE62F4B86E4AF6FA557A9509986A4C2AAD066A5995D
            930A2F2528E3264E952A3AE104E213BFA9608585336914367902C5894EF19265
            828C430AD5B32A4DB215F60755888C5486231952BE70A29161A852CC8A16BF68
            9417D1292CD06ABB492F6310BF642A3F3204955906273DC8579EC4AF67C93C95
            7CC99051CA3815567AA6FCF2F22C5AE948F1E25119B4E0A8B495CF7302956548
            528CA930559A14AA67E1F526A8502A8094AE82E96D9692851398CA92C149C152
            B60C4161D12B5E32A448C9957C7575FE74E2952C1354E152A82A4B7932F1A62F
            F9E29782254B78A5A53C2A2C43128D2A4E3492A17495BE7FB9CC67954974320C
            D149A6F22DA352DE15A7D651156DE649711591AF7C9D2ECF92AF722B3DB544D2
            99F8CE295496214919528A322F252AD3AA14294E05D19B21E54BE98A57258946
            203A295FBE69747A16AF2A437C922FE54BBED20944A738C93341CA9431894F46
            60E24D5F71A29101CA6085579A4A5B61E5513402B30CC22B5E749229D92A93F2
            2819F2FDE965942A97E8C42B507A322CE941E9FBD32BDE5FBECA2419C22B3FA6
            914886D2135E79118FC2A2916F1AB0C26BD9E529AF0A9F33A80C4352C5AAA2A5
            1815DCCCAC14A842AAE5D1DB2A85497152AA5930D1988536F1E29162244B8624
            1A2953F21536E5A9D25489269D99AEE99BE9894F72E49B714A4395221A7F2351
            FE8433D3907CA5A34A53E589CF3F6F9267F2C83824D3CC8FF2AA7C8B4615EC9F
            BE7082D3C937756BEAC64C2B609E6930D2B3E46AB0ADD654AD979E631917287D
            C5550A54862199CA305B093363A595208391A25549528C2A458595C2F52C25A8
            B0A211ADE884933C559EE81596118A3E109DE24D10BDF226F9AA5C1982C24A53
            952E3A558CD254C5284E46A467A5211E1982F032443D074A533C92237902D3E8
            1436F9D5228B5F79320DD33FBE3CF9A66CE557FA359F95C74079966C81F4A37C
            4B8F3B694492AFF4CE199C952171866016CCBFD2CDCCAAC28ADF082215562119
            2C762AB02A588A927214A14A915C295FCA124EBC92A767E1850B4427BC3FC898
            45A70A34F1CAAB2A5BE949D1320EFF38C957BE945719A20CC0AC08C92A9D37A5
            A1165932540E55B8C202D10A54A97A16287DBD2067225F72255F7952D9C51F30
            CF3496B57EF5213A81F27EB5E2F4702EE1AC0C491963E6A524558A1E4B83E284
            33E3CD67E104265E617F105DE9B833C5F9CB31C3AA3419932AB1B45CE1152FE3
            91E24D1EF9679AA6E8445F5AB67002BD04A2299DBE70A5794AE34A3F4B5E719E
            692425F25C541F4A4F2D91CA24FA730E676D48E73C87D50954090D541B5295A8
            A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F1D55891C561B
            5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F1D5589
            1C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F
            1D55891C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D
            48E77F1D55891C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF
            33596D48E77F1D55891C561B5295A8A6F33F93FF1F8EEAF28B5166BE5C000000
            0049454E44AE426082}
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Left = 170.078850000000000000
          Top = 7.559060000000000000
          Width = 782.362710000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Pagamento de Fornecedores [tipoEstab]')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 944.882500000000000000
          Top = 64.252010000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            '[Date]')
        end
        object Page: TfrxMemoView
          Left = 891.969080000000000000
          Top = 105.826840000000000000
          Width = 154.960730000000000000
          Height = 30.236240000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages#]')
        end
        object Memo12: TfrxMemoView
          Left = 442.205010000000000000
          Top = 52.913420000000000000
          Width = 472.441250000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[tipoPagamento]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 442.205010000000000000
          Top = 83.149660000000000000
          Width = 393.071120000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dataIni] at'#195#169' [dataFin]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 442.205010000000000000
          Top = 117.165430000000000000
          Width = 393.071120000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dtCompensa]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 52.913420000000000000
          Width = 204.094620000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Tipo de Pagamento: ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 192.756030000000000000
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Per'#195#173'odo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 192.756030000000000000
          Top = 117.165430000000000000
          Width = 260.787570000000000000
          Height = 45.354360000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data de Compensa'#195#167#195#163'o:')
          ParentFont = False
        end
        object semana: TfrxMemoView
          Left = 925.984850000000000000
          Top = 85.039372520000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Semana: [semana]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 914.646260000000000000
          Top = 41.574830000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Repasse N'#194#176': [codAutorizacao]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 18.897650000000000000
        Top = 377.953000000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'Footer1OnBeforePrint'
        object Memo6: TfrxMemoView
          Left = 952.441560000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            
              'R$[FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEstab."LIQUIDO">,DetailDat' +
              'a1))]')
        end
        object Memo11: TfrxMemoView
          Left = 835.276130000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Memo.UTF8 = (
            'Total do Banco:')
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897650000000000000
        Top = 457.323130000000000000
        Width = 1046.929810000000000000
        object Memo22: TfrxMemoView
          Left = 744.567410000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              'Total todos os bancos: R$ [FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEs' +
              'tab."liquido">,DetailData1,2))]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 3.779530000000000000
        Top = 287.244280000000000000
        Width = 1046.929810000000000000
        Condition = 'frxBancos."codbanco"'
        ReprintOnNewPage = True
        StartNewPage = True
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 45.354360000000000000
        Top = 181.417440000000000000
        Width = 1046.929810000000000000
        object Memo4: TfrxMemoView
          Top = 26.456710000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Cod. ')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 60.472480000000000000
          Top = 26.456710000000000000
          Width = 408.189240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Estabelecimento')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 457.323130000000000000
          Top = 26.456710000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Agencia')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 514.016080000000000000
          Top = 26.456710000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Conta')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 684.094930000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Favorecido')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 941.102970000000000000
          Top = 26.456710000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'L'#195#173'quido')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Banco:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 79.370130000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'codbanco'
          DataSet = frxBancos
          DataSetName = 'frxBancos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxBancos."codbanco"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 192.756030000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Nome do Banco:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 325.039580000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'banco'
          DataSet = frxBancos
          DataSetName = 'frxBancos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxBancos."banco"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Top = 313.700990000000000000
        Width = 1046.929810000000000000
        DataSet = frxBancos
        DataSetName = 'frxBancos'
        RowCount = 0
      end
      object PageFooter1: TfrxPageFooter
        Height = 56.692950000000000000
        Top = 498.897960000000000000
        Width = 1046.929810000000000000
        object Line1: TfrxLineView
          Top = 15.118120000000000000
          Width = 1050.709340000000000000
          ShowHint = False
          Diagonal = True
        end
        object SysMemo1: TfrxSysMemoView
          Left = 423.307360000000000000
          Top = 22.677180000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            
              'Total por p'#195#161'gina R$[FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEstab."L' +
              'IQUIDO">,DetailData1))]')
          ParentFont = False
        end
      end
    end
  end
  object frxBancos: TfrxDBDataset
    UserName = 'frxBancos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'codbanco=codbanco'
      'banco=banco')
    DataSet = qBancos
    BCDToCurrency = False
    Left = 704
    Top = 192
  end
  object frxPgtoEstab: TfrxDBDataset
    UserName = 'frxPgtoEstab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nome'#13#10'=nome'#13#10
      'correntista=correntista'
      'cgc=cgc'
      'comissao=comissao'
      'contacorrente=contacorrente'
      'agencia=agencia'
      'bruto=bruto'
      'taxa_extra=taxa_extra'
      'comissao_adm=comissao_adm'
      'total_retido_adm=total_retido_adm'
      'liquido=liquido'
      'cred_id=cred_id'
      'marcado=marcado'
      'diafechamento1=diafechamento1'
      'vencimento1=vencimento1'
      'CODBANCO=CODBANCO'
      'NOME_BANCO=NOME_BANCO'
      'BAIXADO=BAIXADO'
      'ATRASADO=ATRASADO'
      'TX_DVV=TX_DVV')
    DataSet = MDPagtoEstab
    BCDToCurrency = False
    Left = 696
    Top = 232
  end
  object MDPagtoEstab: TJvMemoryData
    Filtered = True
    FieldDefs = <
      item
        Name = 'nome'#13#10
        DataType = ftString
        Size = 120
      end
      item
        Name = 'correntista'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cgc'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'comissao'
        DataType = ftCurrency
      end
      item
        Name = 'contacorrente'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'agencia'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'bruto'
        DataType = ftCurrency
      end
      item
        Name = 'taxa_extra'
        DataType = ftCurrency
      end
      item
        Name = 'comissao_adm'
        DataType = ftCurrency
      end
      item
        Name = 'total_retido_adm'
        DataType = ftCurrency
      end
      item
        Name = 'liquido'
        DataType = ftCurrency
      end
      item
        Name = 'cred_id'
        DataType = ftInteger
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end
      item
        Name = 'diafechamento1'
        DataType = ftInteger
      end
      item
        Name = 'vencimento1'
        DataType = ftInteger
      end
      item
        Name = 'CODBANCO'
        DataType = ftInteger
      end
      item
        Name = 'NOME_BANCO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'BAIXADO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ATRASADO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TX_DVV'
        DataType = ftFloat
      end
      item
        Name = 'PAGAMENTO_CRED_ID'
        DataType = ftInteger
      end
      item
        Name = 'ENDERECO'#13#10
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NUMERO'
        DataType = ftInteger
      end
      item
        Name = 'CIDADE'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ESTADO'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'COMPLEMENTO'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'TELEFONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONTA_CDC'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'COD_GERENCIADOR'
        DataType = ftInteger
      end
      item
        Name = 'CNPJ_CPF_CORRENTISTA'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'MDPagtoEstabField65'
      end
      item
        Name = 'COD_BANCO'
        DataType = ftInteger
      end>
    AfterPost = MDPagtoEstabAfterPost
    Left = 248
    Top = 296
    object MDPagtoEstabnome: TStringField
      FieldName = 'nome'#13#10
      Size = 120
    end
    object MDPagtoEstabcorrentista: TStringField
      FieldName = 'correntista'
    end
    object MDPagtoEstabcgc: TStringField
      FieldName = 'cgc'
    end
    object MDPagtoEstabcomissao: TCurrencyField
      FieldName = 'comissao'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabcontacorrente: TStringField
      FieldName = 'contacorrente'
    end
    object MDPagtoEstabagencia: TStringField
      FieldName = 'agencia'
    end
    object MDPagtoEstabbruto: TCurrencyField
      FieldName = 'bruto'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabtaxa_extra: TCurrencyField
      FieldName = 'taxa_extra'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabcomissao_adm: TCurrencyField
      FieldName = 'comissao_adm'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabtotal_retido_adm: TCurrencyField
      FieldName = 'total_retido_adm'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabliquido: TCurrencyField
      FieldName = 'liquido'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabcred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object MDPagtoEstabmarcado: TBooleanField
      FieldName = 'marcado'
    end
    object MDPagtoEstabdiafechamento1: TIntegerField
      FieldName = 'diafechamento1'
    end
    object MDPagtoEstabvencimento1: TIntegerField
      FieldName = 'vencimento1'
    end
    object MDPagtoEstabCODBANCO: TIntegerField
      FieldName = 'CODBANCO'
    end
    object MDPagtoEstabNOME_BANCO: TStringField
      FieldName = 'NOME_BANCO'
      Size = 50
    end
    object MDPagtoEstabBAIXADO: TStringField
      FieldName = 'BAIXADO'
    end
    object MDPagtoEstabATRASADO: TStringField
      FieldName = 'ATRASADO'
      Size = 1
    end
    object MDPagtoEstabTX_DVV: TFloatField
      FieldName = 'TX_DVV'
      DisplayFormat = '#,##0.00'
    end
    object MDPagtoEstabPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object MDPagtoEstabENDERECO: TStringField
      FieldName = 'ENDERECO'#13#10
      Size = 60
    end
    object MDPagtoEstabNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object MDPagtoEstabCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 60
    end
    object MDPagtoEstabCEP: TStringField
      FieldName = 'CEP'
    end
    object MDPagtoEstabESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 2
    end
    object MDPagtoEstabCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 60
    end
    object MDPagtoEstabTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
    end
    object MDPagtoEstabCONTA_CDC: TStringField
      FieldName = 'CONTA_CDC'
    end
    object MDPagtoEstabCOD_GERENCIADOR: TIntegerField
      FieldName = 'COD_GERENCIADOR'
    end
    object MDPagtoEstabCNPJ_CPF_CORRENTISTA: TStringField
      FieldName = 'CNPJ_CPF_CORRENTISTA'
      Size = 25
    end
    object MDPagtoEstabCOD_BANCO: TIntegerField
      FieldName = 'COD_BANCO'
    end
  end
  object MDPgtoDesc: TRxMemoryData
    FieldDefs = <
      item
        Name = 'cred_id'
        DataType = ftInteger
      end
      item
        Name = 'taxa_id'
        DataType = ftInteger
      end
      item
        Name = 'descricao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'valor'
        DataType = ftFloat
      end
      item
        Name = 'marcado'
        DataType = ftBoolean
      end>
    Left = 376
    Top = 224
    object MDPgtoDesccred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object MDPgtoDesctaxa_id: TIntegerField
      FieldName = 'taxa_id'
    end
    object MDPgtoDescdescricao: TStringField
      FieldName = 'descricao'
    end
    object MDPgtoDescvalor: TFloatField
      FieldName = 'valor'
    end
    object MDPgtoDescmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
  object CDSPagDesc: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 376
    Top = 320
    Data = {
      760000009619E0BD010000001800000005000000000003000000760007637265
      645F6964040001000000000007746178615F6964040001000000000009646573
      63726963616F01004900000001000557494454480200020014000576616C6F72
      0800040000000000076D61726361646F02000300000000000000}
    object CDSPagDesccred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object CDSPagDesctaxa_id: TIntegerField
      FieldName = 'taxa_id'
    end
    object CDSPagDescdescricao: TStringField
      FieldName = 'descricao'
    end
    object CDSPagDescvalor: TFloatField
      FieldName = 'valor'
      DisplayFormat = '#,##0.00'
    end
    object CDSPagDescmarcado: TBooleanField
      FieldName = 'marcado'
    end
  end
  object QAux: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 912
    Top = 200
  end
  object DSAux: TDataSource
    DataSet = QAux
    Left = 912
    Top = 232
  end
  object SD: TJvSaveDialog
    Filter = '.pdf|.pdf'
    Height = 0
    Width = 0
    Left = 648
    Top = 280
  end
  object popCancTaxa: TPopupMenu
    Left = 825
    Top = 622
    object CancelaAutorizao1: TMenuItem
      Caption = 'Cancelar Taxa'
      OnClick = CancelaAutorizao1Click
    end
  end
  object QLancCred: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    Left = 312
    Top = 296
  end
  object qPgtoDesc: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'CRED_ID'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'CRED_ID_1'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT      '
      '     tr.id,'
      '     rtc.CRED_ID,'
      #9' rtc.taxa_id,'
      #9' t.descricao,'
      #9' t.valor,'
      '                 coalesce(tr.marcado,'#39'N'#39') marcado,'
      
        '                 coalesce(tr.TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_' +
        'PAG_ID_FK,'
      '                 '#39#39' AS TIPO_RECEITA        '
      'FROM REL_TAXA_CRED rtc'
      'LEFT JOIN taxas_repasse_temp tr '
      'ON tr.cred_id = rtc.cred_id and tr.taxa_id = rtc.taxa_id'
      'LEFT JOIN taxas_prox_pag TXPP ON TXPP.cred_id = RTC.cred_id'
      ',taxas t'
      'where rtc.taxa_id = t.TAXA_ID '
      'and rtc.cred_id = :CRED_ID'
      'UNION '
      'SELECT      '
      '     TXPP.TAXAS_PROX_PAG_ID ID,'
      '     TXPP.CRED_ID,'
      #9' 0 TAXA_ID,'
      #9' TXPP.descricao,'
      #9' TXPP.valor,'
      '                 coalesce(tr.marcado,'#39'N'#39') marcado,'
      
        '                 coalesce(tr.TAXAS_PROX_PAG_ID_FK,0) TAXAS_PROX_' +
        'PAG_ID_FK,'
      
        '                 COALESCE(TXPP.TIPO_RECEITA,'#39#39')  TIPO_RECEITA   ' +
        '   '
      'FROM TAXAS_PROX_PAG TXPP'
      'LEFT JOIN TAXAS_REPASSE_TEMP TR ON TR.CRED_ID = TXPP.CRED_ID'
      'where TXPP.cred_id = :CRED_ID_1')
    Left = 376
    Top = 288
    object qPgtoDescCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object qPgtoDesctaxa_id: TIntegerField
      FieldName = 'taxa_id'
    end
    object qPgtoDescdescricao: TStringField
      FieldName = 'descricao'
      Size = 60
    end
    object qPgtoDescvalor: TFloatField
      FieldName = 'valor'
      DisplayFormat = '#,##0.00'
    end
    object qPgtoDescmarcado: TStringField
      FieldName = 'marcado'
      ReadOnly = True
      Size = 1
    end
    object qPgtoDescid: TIntegerField
      FieldName = 'id'
    end
    object qPgtoDescTAXAS_PROX_PAG_ID_FK: TIntegerField
      FieldName = 'TAXAS_PROX_PAG_ID_FK'
      ReadOnly = True
    end
    object qPgtoDescTIPO_RECEITA: TStringField
      FieldName = 'TIPO_RECEITA'
      ReadOnly = True
      Size = 1
    end
  end
  object QPgtoEmpr: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select'
      'PAGAMENTO_CRED_ID,'
      'DATA_HORA, '
      'DATA_COMPENSACAO, '
      'OPERADOR,'
      'COD_OPERACAO_LOG,'
      'OPERACAO_LOG_FINANCEIRO.DESCRICAO,'
      'LOTE_PAGAMENTO'
      'from PAGAMENTO_CRED'
      
        'LEFT JOIN OPERACAO_LOG_FINANCEIRO ON OPERACAO_LOG_FINANCEIRO.COD' +
        '_OPERACAO  =  COD_OPERACAO_LOG'
      'AND COD_OPERACAO_LOG IN(1,3)'
      ' where cred_id = 180 '
      'AND DATEPART(YYYY,DATA_PGTO) >= 2015 ')
    Left = 312
    Top = 256
    object intgrfldQPgtoEmprPAGAMENTO_CRED_ID: TIntegerField
      FieldName = 'PAGAMENTO_CRED_ID'
    end
    object QPgtoEmprDATA_COMPENSACAO: TDateTimeField
      FieldName = 'DATA_COMPENSACAO'
    end
    object QPgtoEmprOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QPgtoEmprDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object QPgtoEmprLOTE_PAGAMENTO: TIntegerField
      FieldName = 'LOTE_PAGAMENTO'
    end
    object QPgtoEmprCOD_OPERACAO_LOG: TIntegerField
      FieldName = 'COD_OPERACAO_LOG'
    end
    object QPgtoEmprDATA_HORA: TDateTimeField
      FieldName = 'DATA_HORA'
    end
  end
  object DSPgtoEmp: TDataSource
    DataSet = QPgtoEmpr
    Left = 312
    Top = 224
  end
  object frxReport2: TfrxReport
    Version = '4.12.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41353.581082893500000000
    ReportOptions.LastChange = 42236.657843518520000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var c : integer = 0;'
      '    '
      'procedure lnDetalheOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {c := c + 1;'
      '  if c mod 2 = 0 then begin             '
      '    lnDetalhe.BeginColor := clGray;'
      '    lnDetalhe.Color :=  clGray;     '
      '    lnDetalhe.EndColor := clGray;'
      '  end else begin'
      '    lnDetalhe.BeginColor := clWhite;'
      '    lnDetalhe.Color :=  clWhite;     '
      '    lnDetalhe.EndColor :=  clWhite;      '
      '  end;}          '
      'end;'
      ''
      'procedure DetailData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  DetailData1.Visible := (<frxPgtoEstab."MARCADO"> = TRUE) and (' +
        '<frxBancos."CODBANCO"> = <frxPgtoEstab."CODBANCO">);'
      '  //if Frac(<Line> / 2) = 0.5 then'
      '    //Memo3.Color := $00EBEBEB'
      '  //else'
      '    ///Memo3.Color := clWhite;'
      'end;'
      ''
      'procedure Footer1OnBeforePrint(Sender: TfrxComponent);'
      'begin  '
      
        '  Footer1.visible := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1) <' +
        '> 0;'
      'end;'
      ''
      'procedure Memo6OnBeforePrint(Sender: TfrxComponent);'
      
        'var valor : Currency;                                           ' +
        '           '
      'begin'
      '  try                                   '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      Memo6.Font.Color := clRed               '
      '    else'
      '      Memo6.Font.Color := clBlack;'
      '  except'
      '  end;            '
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      
        '//var valor : Currency;                                         ' +
        '             '
      'begin'
      '{  try               '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      Memo12.Font.Color := clRed               '
      '    else'
      '      Memo12.Font.Color := clBlack;'
      '  except'
      '  end;}          '
      'end;'
      ''
      
        'procedure frxPgtoEstabLIQUIDOOnBeforePrint(Sender: TfrxComponent' +
        ');'
      
        'var valor : Currency;                                           ' +
        '           '
      'begin'
      '  try                  '
      
        '    valor := SUM(<frxPgtoEstab."LIQUIDO">,DetailData1);         ' +
        '                          '
      '    if (valor < 0) then'
      '      frxPgtoEstabLIQUIDO.Font.Color := clRed               '
      '    else'
      '      frxPgtoEstabLIQUIDO.Font.Color := clBlack;'
      '  except'
      '  end;          '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 648
    Top = 312
    Datasets = <
      item
        DataSet = frxBancos
        DataSetName = 'frxBancos'
      end
      item
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'TotalGeral'
        Value = Null
      end
      item
        Name = 'tipoPagamento'
        Value = Null
      end
      item
        Name = 'dtCompensa'
        Value = Null
      end
      item
        Name = 'dataIni'
        Value = Null
      end
      item
        Name = 'dataFin'
        Value = Null
      end
      item
        Name = 'tipoEstab'
        Value = Null
      end
      item
        Name = 'semana'
        Value = Null
      end
      item
        Name = 'codAutorizacao'
        Value = ''
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object DetailData1: TfrxDetailData
        Height = 18.897650000000000000
        Top = 355.275820000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        DataSet = frxPgtoEstab
        DataSetName = 'frxPgtoEstab'
        KeepChild = True
        RowCount = 0
        object Memo16: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          ShowHint = False
        end
        object frxPgtoEstabCRED_ID: TfrxMemoView
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'cred_id'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."cred_id"]')
          ParentFont = False
        end
        object frxPgtoEstabNOMEESTAB: TfrxMemoView
          Left = 60.472480000000000000
          Width = 408.189240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'nome'#13#10
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."nome'
            '"]')
        end
        object frxPgtoEstabAGENCIA: TfrxMemoView
          Left = 449.764070000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'agencia'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."agencia"]')
          ParentFont = False
        end
        object frxPgtoEstabCONTACORRENTE: TfrxMemoView
          Left = 498.897960000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'contacorrente'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."contacorrente"]')
          ParentFont = False
        end
        object frxPgtoEstabLIQUIDO: TfrxMemoView
          Left = 941.102970000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'frxPgtoEstabLIQUIDOOnBeforePrint'
          ShowHint = False
          DataField = 'liquido'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          HAlign = haRight
          Memo.UTF8 = (
            '[frxPgtoEstab."liquido"]')
        end
        object frxPgtoEstabCORRENTISTA: TfrxMemoView
          Left = 687.874460000000000000
          Width = 253.228510000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'correntista'
          DataSet = frxPgtoEstab
          DataSetName = 'frxPgtoEstab'
          Memo.UTF8 = (
            '[frxPgtoEstab."correntista"]')
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 146.000000000000000000
          Height = 96.000000000000000000
          ShowHint = False
          AutoSize = True
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000009200
            0000600806000000F6212B9C000000097048597300000B1300000B1301009A9C
            18000040E169545874584D4C3A636F6D2E61646F62652E786D7000000000003C
            3F787061636B657420626567696E3D22EFBBBF222069643D2257354D304D7043
            656869487A7265537A4E54637A6B633964223F3E0A3C783A786D706D65746120
            786D6C6E733A783D2261646F62653A6E733A6D6574612F2220783A786D70746B
            3D2241646F626520584D5020436F726520352E352D633031342037392E313531
            3438312C20323031332F30332F31332D31323A30393A31352020202020202020
            223E0A2020203C7264663A52444620786D6C6E733A7264663D22687474703A2F
            2F7777772E77332E6F72672F313939392F30322F32322D7264662D73796E7461
            782D6E7323223E0A2020202020203C7264663A4465736372697074696F6E2072
            64663A61626F75743D22220A202020202020202020202020786D6C6E733A786D
            703D22687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F22
            0A202020202020202020202020786D6C6E733A64633D22687474703A2F2F7075
            726C2E6F72672F64632F656C656D656E74732F312E312F220A20202020202020
            2020202020786D6C6E733A786D704D4D3D22687474703A2F2F6E732E61646F62
            652E636F6D2F7861702F312E302F6D6D2F220A20202020202020202020202078
            6D6C6E733A73744576743D22687474703A2F2F6E732E61646F62652E636F6D2F
            7861702F312E302F73547970652F5265736F757263654576656E7423220A2020
            20202020202020202020786D6C6E733A73745265663D22687474703A2F2F6E73
            2E61646F62652E636F6D2F7861702F312E302F73547970652F5265736F757263
            6552656623220A202020202020202020202020786D6C6E733A70686F746F7368
            6F703D22687474703A2F2F6E732E61646F62652E636F6D2F70686F746F73686F
            702F312E302F220A202020202020202020202020786D6C6E733A746966663D22
            687474703A2F2F6E732E61646F62652E636F6D2F746966662F312E302F220A20
            2020202020202020202020786D6C6E733A657869663D22687474703A2F2F6E73
            2E61646F62652E636F6D2F657869662F312E302F223E0A202020202020202020
            3C786D703A43726561746F72546F6F6C3E41646F62652050686F746F73686F70
            204343202857696E646F7773293C2F786D703A43726561746F72546F6F6C3E0A
            2020202020202020203C786D703A437265617465446174653E323031342D3132
            2D30315431373A33333A30332D30323A30303C2F786D703A4372656174654461
            74653E0A2020202020202020203C786D703A4D65746164617461446174653E32
            3031342D31322D30315431373A33383A32302D30323A30303C2F786D703A4D65
            746164617461446174653E0A2020202020202020203C786D703A4D6F64696679
            446174653E323031342D31322D30315431373A33383A32302D30323A30303C2F
            786D703A4D6F64696679446174653E0A2020202020202020203C64633A666F72
            6D61743E696D6167652F706E673C2F64633A666F726D61743E0A202020202020
            2020203C786D704D4D3A496E7374616E636549443E786D702E6969643A336133
            35376136322D646166642D653734342D613465612D3866383066613833653062
            613C2F786D704D4D3A496E7374616E636549443E0A2020202020202020203C78
            6D704D4D3A446F63756D656E7449443E786D702E6469643A3837333166363635
            2D643634332D666134382D623364362D3930303237366533336233303C2F786D
            704D4D3A446F63756D656E7449443E0A2020202020202020203C786D704D4D3A
            4F726967696E616C446F63756D656E7449443E786D702E6469643A3837333166
            3636352D643634332D666134382D623364362D3930303237366533336233303C
            2F786D704D4D3A4F726967696E616C446F63756D656E7449443E0A2020202020
            202020203C786D704D4D3A486973746F72793E0A202020202020202020202020
            3C7264663A5365713E0A2020202020202020202020202020203C7264663A6C69
            207264663A7061727365547970653D225265736F75726365223E0A2020202020
            202020202020202020202020203C73744576743A616374696F6E3E6372656174
            65643C2F73744576743A616374696F6E3E0A2020202020202020202020202020
            202020203C73744576743A696E7374616E636549443E786D702E6969643A3837
            3331663636352D643634332D666134382D623364362D39303032373665333362
            33303C2F73744576743A696E7374616E636549443E0A20202020202020202020
            20202020202020203C73744576743A7768656E3E323031342D31322D30315431
            373A33333A30332D30323A30303C2F73744576743A7768656E3E0A2020202020
            202020202020202020202020203C73744576743A736F6674776172654167656E
            743E41646F62652050686F746F73686F70204343202857696E646F7773293C2F
            73744576743A736F6674776172654167656E743E0A2020202020202020202020
            202020203C2F7264663A6C693E0A2020202020202020202020202020203C7264
            663A6C69207264663A7061727365547970653D225265736F75726365223E0A20
            20202020202020202020202020202020203C73744576743A616374696F6E3E73
            617665643C2F73744576743A616374696F6E3E0A202020202020202020202020
            2020202020203C73744576743A696E7374616E636549443E786D702E6969643A
            32373830353839632D653534392D626334372D613762392D3036333635313036
            323162393C2F73744576743A696E7374616E636549443E0A2020202020202020
            202020202020202020203C73744576743A7768656E3E323031342D31322D3031
            5431373A33333A31312D30323A30303C2F73744576743A7768656E3E0A202020
            2020202020202020202020202020203C73744576743A736F6674776172654167
            656E743E41646F62652050686F746F73686F70204343202857696E646F777329
            3C2F73744576743A736F6674776172654167656E743E0A202020202020202020
            2020202020202020203C73744576743A6368616E6765643E2F3C2F7374457674
            3A6368616E6765643E0A2020202020202020202020202020203C2F7264663A6C
            693E0A2020202020202020202020202020203C7264663A6C69207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            2020202020203C73744576743A616374696F6E3E636F6E7665727465643C2F73
            744576743A616374696F6E3E0A2020202020202020202020202020202020203C
            73744576743A706172616D65746572733E66726F6D206170706C69636174696F
            6E2F766E642E61646F62652E70686F746F73686F7020746F20696D6167652F70
            6E673C2F73744576743A706172616D65746572733E0A20202020202020202020
            20202020203C2F7264663A6C693E0A2020202020202020202020202020203C72
            64663A6C69207264663A7061727365547970653D225265736F75726365223E0A
            2020202020202020202020202020202020203C73744576743A616374696F6E3E
            646572697665643C2F73744576743A616374696F6E3E0A202020202020202020
            2020202020202020203C73744576743A706172616D65746572733E636F6E7665
            727465642066726F6D206170706C69636174696F6E2F766E642E61646F62652E
            70686F746F73686F7020746F20696D6167652F706E673C2F73744576743A7061
            72616D65746572733E0A2020202020202020202020202020203C2F7264663A6C
            693E0A2020202020202020202020202020203C7264663A6C69207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            2020202020203C73744576743A616374696F6E3E73617665643C2F7374457674
            3A616374696F6E3E0A2020202020202020202020202020202020203C73744576
            743A696E7374616E636549443E786D702E6969643A62666631653332612D3233
            38622D623934622D623733342D3036646130656330623835643C2F7374457674
            3A696E7374616E636549443E0A2020202020202020202020202020202020203C
            73744576743A7768656E3E323031342D31322D30315431373A33333A31312D30
            323A30303C2F73744576743A7768656E3E0A2020202020202020202020202020
            202020203C73744576743A736F6674776172654167656E743E41646F62652050
            686F746F73686F70204343202857696E646F7773293C2F73744576743A736F66
            74776172654167656E743E0A2020202020202020202020202020202020203C73
            744576743A6368616E6765643E2F3C2F73744576743A6368616E6765643E0A20
            20202020202020202020202020203C2F7264663A6C693E0A2020202020202020
            202020202020203C7264663A6C69207264663A7061727365547970653D225265
            736F75726365223E0A2020202020202020202020202020202020203C73744576
            743A616374696F6E3E73617665643C2F73744576743A616374696F6E3E0A2020
            202020202020202020202020202020203C73744576743A696E7374616E636549
            443E786D702E6969643A33613335376136322D646166642D653734342D613465
            612D3866383066613833653062613C2F73744576743A696E7374616E63654944
            3E0A2020202020202020202020202020202020203C73744576743A7768656E3E
            323031342D31322D30315431373A33383A32302D30323A30303C2F7374457674
            3A7768656E3E0A2020202020202020202020202020202020203C73744576743A
            736F6674776172654167656E743E41646F62652050686F746F73686F70204343
            202857696E646F7773293C2F73744576743A736F6674776172654167656E743E
            0A2020202020202020202020202020202020203C73744576743A6368616E6765
            643E2F3C2F73744576743A6368616E6765643E0A202020202020202020202020
            2020203C2F7264663A6C693E0A2020202020202020202020203C2F7264663A53
            65713E0A2020202020202020203C2F786D704D4D3A486973746F72793E0A2020
            202020202020203C786D704D4D3A4465726976656446726F6D207264663A7061
            727365547970653D225265736F75726365223E0A202020202020202020202020
            3C73745265663A696E7374616E636549443E786D702E6969643A323738303538
            39632D653534392D626334372D613762392D3036333635313036323162393C2F
            73745265663A696E7374616E636549443E0A2020202020202020202020203C73
            745265663A646F63756D656E7449443E786D702E6469643A3837333166363635
            2D643634332D666134382D623364362D3930303237366533336233303C2F7374
            5265663A646F63756D656E7449443E0A2020202020202020202020203C737452
            65663A6F726967696E616C446F63756D656E7449443E786D702E6469643A3837
            3331663636352D643634332D666134382D623364362D39303032373665333362
            33303C2F73745265663A6F726967696E616C446F63756D656E7449443E0A2020
            202020202020203C2F786D704D4D3A4465726976656446726F6D3E0A20202020
            20202020203C70686F746F73686F703A436F6C6F724D6F64653E333C2F70686F
            746F73686F703A436F6C6F724D6F64653E0A2020202020202020203C74696666
            3A4F7269656E746174696F6E3E313C2F746966663A4F7269656E746174696F6E
            3E0A2020202020202020203C746966663A585265736F6C7574696F6E3E373230
            3030302F31303030303C2F746966663A585265736F6C7574696F6E3E0A202020
            2020202020203C746966663A595265736F6C7574696F6E3E3732303030302F31
            303030303C2F746966663A595265736F6C7574696F6E3E0A2020202020202020
            203C746966663A5265736F6C7574696F6E556E69743E323C2F746966663A5265
            736F6C7574696F6E556E69743E0A2020202020202020203C657869663A436F6C
            6F7253706163653E36353533353C2F657869663A436F6C6F7253706163653E0A
            2020202020202020203C657869663A506978656C5844696D656E73696F6E3E31
            34363C2F657869663A506978656C5844696D656E73696F6E3E0A202020202020
            2020203C657869663A506978656C5944696D656E73696F6E3E39363C2F657869
            663A506978656C5944696D656E73696F6E3E0A2020202020203C2F7264663A44
            65736372697074696F6E3E0A2020203C2F7264663A5244463E0A3C2F783A786D
            706D6574613E0A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020200A20
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020200A20202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020200A2020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020200A202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020200A20202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020200A2020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020200A202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            200A202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020200A20202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020200A2020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020200A202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020200A20202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020200A2020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020202020202020200A
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020200A202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020200A20202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020200A2020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020200A202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020200A20202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020202020202020202020200A2020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20200A2020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020200A202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020200A20202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020200A2020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020200A202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020200A20202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            0A20202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020200A2020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020200A202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020200A20202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            20202020202020202020202020202020202020200A2020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            2020202020202020202020202020202020202020202020202020202020202020
            202020202020202020202020202020202020202020202020200A202020202020
            202020202020202020202020202020202020202020200A3C3F787061636B6574
            20656E643D2277223F3E88C319A900003663494441547801ED7D077C5545F6FF
            F7F597DE217442935EA508D23B28A8A8B836D6825D71D5BF8A6B43C18A8A6545
            57DC75655D75D55D10A54A5540101014417A078190407A7BEFFDBFDF9BDCF092
            BC00798495FC3E0973DECC3D73CE999933E74E9F8BC5E7F3A1A27FC1F0300D07
            C14EB016013DC3792D164B2E4314EBF3CA279CD69107191919F07AC572925CF8
            88880850180A0A0A8C088515C8CCCC945702446BB3D90C7A33E2C4F1E3866CAB
            555985119758AB16EC763BD2D2D2D0BC79731C3A74C8242FE1F7EFDF1FDF7CF3
            8D8133D3559E84D0B3C2A62FDCA940748A178FFCF2C09F2E50B83CBED3C92D8F
            2F10DE62261C28B23C5C303C01326DF1936F842957562DF08B2A1B94ACDCDC5C
            084AC72A4EC64159411992693CA6DC03FBF7571B92A98C53F8BFA72195C9962A
            BF0CB214428692979787ECEC6C285C2ADAC0056348816499B2F7EFDB87EA16C9
            D44660BF4A19922AFB5446A4228AA6A286A42E4BBCA7027593A2ABEEDA026BA9
            CA19527A7ABA312E92C1042A92F01531A49898984062CAC5551B5260D59CB786
            248330BB3A85CDEC6B80EDF1788C2ECCC4F9FBA23D5343AAA811299D6A439216
            CAC2796948320675619A91B9DDEE12B9AE0C430AC680CC4C541B92A989927E50
            86545244F94F6A516414E553948C31E96540C73905971185868696203A0343B2
            B24572539697E31A7359C190A1E9FFD9189184541B92B45016CEA921B1228DA9
            B38C496026CF4A36BA2613674EB985CFCFCF37A6ED9A95C9884242424C36C397
            31A8B5128FC96F44F8FD84868434B758ADD9EC02F7082DC394512A7CB6703A43
            EADDBB37962C5902FDA93CF2CD7CEA5961D357DCA940748A178FFCF2C09F2E50
            B83CBED3C92D8F2F10FE9C1B524E4E8E6134FE89AB62B508A859900AE37068AD
            12C6205AF4A23D9521894786A4752485456F8214E972B920C3918CD28668D205
            EBFB19928532CAAC795D76D965F8EF7FFFCBA8C2854C05CC3C2A6F0A9BBEE24E
            05A253BC78E49707FE7481C2E5F19D4E6E797C81F0E7DC900255B66948322615
            A6A286240394B1C850D43A49865938295271322413772E7CA669A35C0FA1D8B5
            6BD70EF3E7CF478D1A350C9CF2A20069E515B7CEC29B3823A29C1FD129EA74B4
            FE7481C29211084E2737104F79B82A69483240D350D415AA7059595946CB2745
            46454509754E8195A059408E9948CB962DB178F1E26223125E79914F5A79D586
            646821881F8D91CE458BE46F483B77EEC4EAD5AB71CD35D718FB6091919141E4
            B4E22C348EE216A94993268611D5AD5BB784A06A432AA18EE01FFE1786F4F3CF
            3FA343870EF8F2CB2F316CD8B0E0335B414ED3902EBFFC723CF7DC73C6466E69
            11FF2B43F24FD73F4D33EC1FEF1F6619FC1FCF2A5CE5BBB62D5BB61895181E1E
            0EAD7A9F95362AC83C61C2043CF5D453E57299156956989E1536FD72198B2244
            A7A078E49F29884F3CF24FC5239A53C55724EEFF8C21A9D0A7539C682A035801
            96871E7AC8F7F2CB2F9F529C991FD21B747A56D8F40DE4297E44A768F1C8AF28
            98FCE5F1052B3790BC736948B1ECDA52349D2F9D6173D6A6D91AE3A2380B4B64
            A1A3389EDACAB59F3C863530F570EAAEB83C663C9BA01992870B923ED21BD37B
            E260B6480A8B4FFEB986A54B97D6E37AD154A6D38CA065007A651DF3B381D8E7
            58C675F455264D089A127F1F7143843B15906E19E31F23ED61FA1576E43F250F
            E59E32BE22914119D2693218C20C8E6126AE215D260D43339BD2CAF631DE4283
            D0A9B41E0C27127CA4DD4A9E028529C3C241752386430872C7F973258D338171
            DBB88EF4237DEFFFDA90989EFD820B2E788179B99BE0269CCEA590209EC0A2F9
            FA30CF9F31AC677A67E4D67211B6079739B44A8F8AACCC33C15326C0BC9C32BE
            2291E7C2906E6006FFC64CE83424BD73E25439B751F217AC58638CC4B0F1C6CB
            3FC77031E54F2734C499FDED642BDA64F3E6CDE8DCB9F30B6479985011973F66
            CC18F7E79F7FEE15138D4ADE19415536A45A34A2AF58CA8E6A725233F3E165C0
            E2D71EF111C58F251EB8124C46C5E9F4AFDD664188D30A97CD4A6C40B78ED82B
            F7EEDDBBAB4183060C16BAD329AF902AB8DFE4E464777C7CFC3FC97D79F6DAD5
            56CFB164C06A0354401ED7B5D81D0C5AE073D8E148AC0567FD86E0DF8FFBF7EF
            EFF4F6DB6FDB39BB9BCDE701BEAC4C64AD5E095F7E41212F911677082C56F24A
            3B761B5C1734873D2A8631C878E59557223526AB68D94E47CFBA92FC4A814A6D
            9198B10799ABC9046CFF2D0D63A62C411E152D3D0B57083E7A32177A655C619C
            7E5D546AB8C382E87017EA25466240AB44F4689E887097DD9F6BD21B6FBCF1E2
            B871E3D2FD91A753A03F6D05C35790FE3D5F7E5EECF6DE5D919F9A8A90018311
            4243B6D7AA0347BD06B0BADCF031EFAEDAB5E1AA538FE4187DE4C891991B376E
            B4F6EBD7EF281161E98B1660DF1F46C1D6B61D42BB5E04A7C15B1F8E8478C0E6
            84CFED42585223D82222498E541A51C2E4C9933D7AA8089C4E0FACAF8A883B25
            6D651BD28B4CED6102FEB36A17AE9CBC182151216C6A641AC29E3948093AD7EF
            659356E0F12282AC2D1AC4E0A9ABDA616887FA7C32DC381EB4FF60F0E0C1691A
            C01B98A21FF117052BC57BFDF5D7C369B03329AC4FEEB6ADD6BD970E804FABE9
            51D1506BE265E53BD90AD96AD7859D061433F20AB8D9AA90BEC98F3FFEB8A756
            AD5AED121313D7F01947FE3205298F3F0C5B5C022C912C99DD090E7EE04A4880
            359106D8BC35E26E18030BF70C49BF7FECD8B18DA64D9B56C032554891A4277B
            F9EE7C35241B33F619B37D3901E33F5E8B97676F464C24DF50F555425604FC1A
            2D05BDE4CDCCE0042E2B17EFDE7531C6F46D468C0E0A14FCC48DD2815F7FFD75
            8A10AC2C0C1C3810D3A74F8766851CA40A7DD6B07DFBF63F7205FB750A8A4C9F
            351387EEBE05EE3EFD60A5D1B86AB3358A4F804F4DAFD305844720A24347D813
            6A901C377FF4D147D3070CE8FF4CCD9A89E3853838EE0E642E5908578F5EB091
            37B4662D20C40D8B93BAE24D153B9FC3BB5D04CE3644BEF1EAABAFBEF0B3CF3E
            CBA3619C912191AEB08B3D8DDE595F925F2950992D521D666C0B731546C05593
            1761C6CF07111546C50A510960659791955B007B4E3E163F3B0C1D1AC649AA87
            4A0EBBFEFAEB7365340D1B36C47DF7DD87071E78003A8672BAC129F36CE1F925
            5FD3A64DB176ED5AC92B06C619E18F3FFE585B304BF9D09360497EF3551C7BFE
            19B86908D6BAF5D885D585B36E7DF8D882D8A3A2E0888B83AB4D7B8E79ACB2F4
            E757AC58F1F885175EB891F969E1E195A87D575D8282FDFBE0E8D4050EB6606E
            CAB0C6C7C312160E1B1756DD4D9AC14E03635A5A648DEBD9B3E7F10D1B36E8D1
            2B2351A03C30E39577335C1EAD68CA8BAB283E28432A2791D6C4FF4C405A761E
            063E35071B8F6521A4E49846D16705168EBD9329F74F439AE3D5315D0D595C36
            F88403EEEB0F1C38E079F5D557C1F527DC79E79D469C9479F1C51727D0C8F2D9
            A2E4376AD448F1362E1F84718D2BB64B972E164EE7533888FE6DCF9E3D5EB63C
            D8B4691356AE5C098E6D70C71D77E0D65B6F0DA1B05F084904ECBBF57A647D33
            0FF61AB5392672C2CB01B2C766873D269A86100657C7CEA871F7FDB03A9D3878
            F0E070DE815BD2A953A754F23AF3F6ECC6AE416C891C4E766B911CABDBE065A1
            7CA121E48F056C0EC4DE722B22FB0E2439B06EDD3A5BC78E1D75F7CFE5743A73
            7FFAE92730BF465CA01F96572F72228D2492E15CD26C220474A409880F061994
            21318381D29A60B1589E54C4CFFB52D1F3E9B954A40D568B45A83220744EAE07
            B9051E56038751A4703A6C703B390B3A55034E711959F9E89B148B998F0D82D3
            4ECB02327AF5EA15BD6DDB36CF0F3FFC605C55E2D849959FC47D380F5B83CFB9
            66558349A413245D23F67086A5743599390CCF27A8495A4AFF5B82BFBB840F33
            09565F4E3676F6E9C601B515A1FD0721B473571A54A2314BB373C3D8EAE4CC2D
            946239C9203DD6AF5FDF824B1447478F1E9DACE7CCA58BB1EFBA2BE1225F58DF
            0170B56A051BBB4537C75756768BD6905070B555A4067CF9E54CDB8811236548
            962FBEF8C277C51557D8A47FEADA63109CFC0967B017E166423F4234C14B984B
            D8439E85F4671084A3A709A3C5F02BE3C7C2042A438E64CCE1CF100266ACDA8D
            51539621362E143E0E9685F3071951764E017A368F47DBBAD17C232D90717CBB
            E93076A66623CC6DE79A903F47C9706E9E074D63DD98FBF860D48894BDA060E8
            D0A16EAED57876EFDE2D8379951CFD0989848A3A0D6A6793E931C22FAC307A18
            C79F2904646DDA88FD570C87A5200F3EBB83B561E574DFC91916EB9133363677
            887FE01144D3C844CF1629915DA78EFF1ED4F391979F43CAE4E7608B61EB2393
            B6F1962F8DC7A6DBC16CA5EC8D1BA3CEA497D93D720647863FFDE94FD6D75E7B
            CD3771E2C4F0C71F7FFC76A25A127E246C242413980934A0DF89D08BD0961045
            286D25AC6ADFF7C44F24A87CC6388AE14A71411912735426712A7C1E918308F8
            F3A7EBF0D257BF209A951C88D647A2EC8C1CCC7AB43FFAB7ACCDA742B7EB683A
            46BFBA04BF1CC9E0D8D35ED84C15469DFCA57AB2B30BD0BE5604BE7E7C10A243
            9C60D796C3AE2C816389881B6EB8613589EB12CAB802267C3C33CF38894931B0
            B3058C0E610B5286D2401C61DEAF626819613CCBF71C7D1C9BFE771C9BF81442
            865D0A67F39670376CC8E93BBB381A8606C73E8F17F6B878D80ACF9AFBD8D5BA
            39660B678B784CFC7BFF7005F276EE40C82597C2D5B829DC75EAC15E3311560E
            D079279C037670F65613B05A759338E5AEBBEEAAC132A14F9F3EF9E467099041
            FF08E130219D60CBCCF7C46566E5B33B439C173EA7DD66B3C486B1EB646420C7
            72BD4BFCDD2C93877EA5B8CA32A446CCD44AE6A80601C35E5A8845BF1E4684DB
            01955C387FC82BF020C161C5FC2787A271CD08FF28FC65FE668CFBF007441BCB
            0625A20A1F2C405A7A3E46B4AA897F3FDC9F5D2734A0CD3976ECD8BA9A356B76
            2F242AFC4DA6D1ACD971142BB6FC86DF8E6422393D0787D27269781AD5B0C238
            7EABCD5965424C08DA354AC0E076B5512F96DD52213B5B45DF510607B06C63E9
            DF43C0A1A71EC3F1BFBC025B2C2B9B5D912DA126BBB65AB046C7C017158ED0A6
            4D1177D35870FF47FC73B810399CE3AC0BB8DDB3D9939E86DD2306A160EB1658
            22228DD99D3D219186571396A8304224A2070F4378EF7E4A0AB939393B38BACE
            E598AFA58128FA39929EE3FD7EEB11CF8AAD47BC878E64580EA5E5D9523373AC
            6CC72D7954B8C3694783283762239C685437067D5BD642DBFA3170DAAC4512F8
            8EFA7CEFB15C77125129C614942131F1D26E2811B309C8C9F7A0DB635F61FBF1
            6CB8F9C60B571A3239EBEAC01665F184E170158E718A4966AFDF8FD1AF2D8633
            8CD3E162AC5F8086748CDDDF031C6CBF726317BF8893C1FD299978FF9B2DF8F7
            F7BB7120390BE91E9F310EE38B0A1BD3B3F089FA5645C343A3CE67F7EB247BAD
            08176E19D00C0F8D68C35575B688C4F1EDDD4485E731D8DE57908FBDD78F46CE
            FEBD081F3404A175EAD3601C000D4AAD91D7EBE178270E111CFBE8F9D0A14331
            292929275AB56A753FF95FCDF97903765F3E148EAEDD117E6117B8697C707001
            92A035234F6E3642DAB647488B5624379CB2C9121B616CDC9F8A0F58AE196BF6
            E1C8891C6430DF5C7331CA64B3D14834DD2735F3CC97CB8B0296DB4A09B17C69
            BBB74CC4A3A3DAA25B931A85C2F8CB7DCB2B366CD8F05F1D59E6848498E05D50
            86A48C964A721895FDB5709B0E1CC780890B90C5426A9B43387FD0F8E8385B85
            6BBAD5C7F4BBD5A5FBC702D3166DC19DEFAF42945AA49251C693D66A32533331
            E3D10118D2B68E81F3FF99B5660F1EFD682D361D4C43089B77375B452B0D47E9
            16D251B38581A25F6A9E216173F20A90C917E0C65E8DF0F66D3D10C6168B5172
            5EFE580BB825B27B685F636BC3D1A2255C8D9A70AC1367188F233191630E4E16
            5AB7E194DEC8570AF7C76AC4C6C67AFBF5EB3787FC83D366CFC2C11BAE81A343
            7B38931AC3D9200956DEF4D57A932D321A564EFF432FEC0C6BA99B3354255EFF
            FA674CFEF2171CA20185B115D5C4C47AB25014EF335AFFC2D2F09165D6AF6CCB
            C395DD74B6C6712CCFACC706A24B93044521EDC489CF5AB56E7D35B7708C97CA
            4006F9535986349C86F495F23063ED5E8C7EF33B846BEC71B2548A328074387E
            3C0BCF8DEE80FFC737DF40FAFD5CF6E202CCDE740451E17C5355BB7E71E2CDC8
            CA439BC4302C7D663842B8A7E5178DF716FE8A7BA7ADE28AB003E1E4A776E4FC
            490A9F992FBA12783DA85EA4F8632959987C7D473C78491BA18B21FBA7F5D877
            F565B0C7C7C2E6E24482ABD90865976457ABC46500B64CF1B7DC86886EDDC5F3
            C677DF7D777F5C5C5C648B162DB61051F3C88B93903AF50D3839A0B6586C4068
            843143B3706FCDC3D6DB51BF2112EF7FA878A04D1EE6D7873FFDE37BBC3E670B
            D739DD1CCBDBE1654BA338137C0C28DF5C666328B0B3B2C54A3991856E0D6330
            EFCF83F99230CFC0766E24375FB3668D2740E310585039D8A00C2980AC27899B
            40C0E4591BF1C8A7EB1113E562472C4C4950A1B3F876FCE7813EDCEAA85722F2
            AF0BB7707CB406616C494A44143DE89DCB48CDC5670FF4C288CE0D8AB085DEE7
            AB76E3FA3797C119EA828B95524231165D75F2415DAA9DDAE60BCA712D5B0F2E
            3594A02B14856CCD0A39E35CF4E460C4FA2DA8A6FCED5DFCF6E84308193418A1
            5C4C745FD0024E2E285AB97EA469BB358AEB482E969B72B86E35920B9C5F5F74
            D145C97C8C963276B135CBD9B60D61234721A45973B85BB682233E812D511881
            8B91717145ED08398ADC4B337FC2F84F7FE4983154E36F1A5651449197C5D9AF
            D7E78583E552171DAA16586F4451BCBF2763CBE6B062E16303705153A38BCBED
            D1A347D48A152B7203E9C19FF774E1CA32A41F99507B026E7AF73BFC6BE51E44
            C8187CC29404ED9B85F1ED58C81957F3DA513C1DE0C391F45C4C5FBA0D93666C
            04F87696EE12A51735EF29C919B8AD7F53BC7BFBC52584EE3D96818BFFFC3552
            F27DC66CCF5F29E2CD2BF082ED1BC6F46C8C8E8DE290959BCFD66B3B361FCE80
            93E38712C2F8A0B43C54F83C2ABC5BA1C2890552FFFD2F64CE9BCDD6C86D7423
            0692D3773613DC234B84A37133C4703667E53D3DDE146EC28DD6BC891327EE15
            9D8F1FFD3A32F109787EFB0D3086C5E0CA372D9CAD99252A8A03ED1884F71F88
            F0B6861AC582B5BB923170D202789886C36625CE5FA1166465E7A35F8B048CEC
            521F915C3259BF3B05EF2FD9817C5860A36191A184932165B1FB9EF7FFFAA357
            73C390D6D390BA2F5FBE3CBB0461100F9561482AE14EA6DD209BC7227A3F3317
            9B7FCB305A05E2CA3815C6C12322B5D962A992F59C4A433A9C9E0717FB701991
            700623F52C3F870B973999B9B8A16712DEBCE5229E00309A6545816D326E7EFB
            5B7CF4FD3EEEEBB9F8C6FA2B9B2D1185E570F636EDB66EB8F6E226068F7E566E
            3B8CA12F2EE466BB836A17A6241C67ABF9F64D5D705BBF0B4E4650160742259F
            65529CF2FBD82AF0ADE07E1987ED16CB7E9E1FEFFBE4934FB6E61AD01764E062
            137F8BCAC3D04927996C227DFCE6139B4958688466E4AD7C29A7AFDCCB9792E5
            F52B96BAF87476F1C35AD7C447F7F6E2C480F1454C8F7CF4035EFF662B8716CC
            4711CEF49494EA4886D4F302C3903EA3218D5DBE7CF9099326583F2843F27FE3
            99702B166C39FDA84D07523168E27C64FA02BF11A42976B99CDDA96042C87834
            EBF0D7B38C2C97CD767E6E1EEAC747606CBFA6B8FF92D650D7241E1356737ADF
            F799F970B30BB2A852CD08F91498CE8DDEC12D6BE2CB470608530C2934AEF6E3
            BF447A9EB7CCDBABB4536948E32F6D8D67AFEA50CC5381C0173CDE32856B483F
            72C3B6232B6B5905780DD29D47D2D175FC57389EEFA57D59D5331A78FDB0585C
            C228C0B74F0D4197C6850367E105EF7EC3E1C1F41F383C70957941A4EF6CB648
            73B96CD2EBA421DD4A434A13EFD9406518D25D34A4BF281333D6ECC198775670
            BBC8C6F51D1557D8E0A080DDD140AE155DDB3D09DD59E89A5CDC0C24E98EF757
            E0FDA53B11CDA9BB145582865938C119E25B7FEC8CDBFBFBB52C24DACDEEB0EB
            E35F1BDD40C9D90FBB1CF26966F9E4E56DF1C4156D490D70F3771BA7CBA9DC78
            8DE69115BFF641F4162E1BD9EDDCBFF371AF6EC3871F7EF8F2B3CF3EABEE1EAD
            5BB7C69B6FBE79174F22B4EFD2A54B165FC27D84FD145A4008A5EE92E8B7E23E
            5A63FA0DC1D5250266AED98D27FFF1031C1CF3E8D91F3C6CF95B715DE86FE3FA
            96581B12CD4D6A9D57ED45A4861642F881BAEC5C7E5B61010DA97BB39A8A518B
            74130D29530F6703411952A9049FE2F3D3E0DFF31C183EFDDF9F11115AB65965
            F4193B1F297D9C99744D8A41D746F168DF28169776AC5FA6E548CEC845CF09B3
            B1EF786EC0352B9D65B2517BF3FF3C101D1AC452EA49F7C3CE642E5370E3D55D
            7605D8689138CD7E6E747B3C7C691B8369C992252F3FF1C413AFF0866F3C2B5F
            59149E41B6BE369B8F63A283DC34CEA6C1E5737194B6E2E34A85C5C2232E3E6E
            BAE25FFFFA97831728C3C8E420938BBE85E061388F0698C77DC2EC091326F4BB
            EEBAEBE6128F4CB61CD96CB535D629FD8268D2E1E658328C0B8FA23561CEFA7D
            B8E1ADE5F070AD4C7C26DEF43556AC116AC3922787A26EE1C2EB8C6EDDBA5DB5
            6AD52A8E104AA762729D991F9421514BA67429633A95719D1037BFC381F6F77B
            107E1A435205AB2624475D9AC685A58B4199D00C239B6301079536B2431DBC35
            B61B122242949401FF650BF88737BF4518676AAA7C03E9F7A3817D1C07A1EF8C
            BD0871E16E0E61942AE0E0C0F5B3EF77E1B5B95B8CC1B91F8B11145506BBB60F
            79EE69F44549068E0B8BEB79ACF757AE50ABE561F62C065E016E7F64F1E4C03B
            9CFD64BEF4D24BC934A65446E6B46DDBD6BE60C182387E0BE0F88E1D3B5C3CBB
            4D761B87595C122DCA30F9391BB38274E0B5EF1BB815F23C0D4B864611A77719
            3C56B3890B955FAFDB8F773936CAF2022E1E512EAD4F494AE704A25793382C78
            6CB01EB5B9BD8ED3FF6EBFFCF20BABC2A716D2C007F373B686A4D77C33955143
            B38181CF2DC0FA7DC7CB3D3A42F5B16FE76E1007A6362A922F0E4E70DF8C9A44
            5888BDC43840852189A174ADD0A6A66442DDDCDFEFEE69EEF863DC3F56E1ED85
            DB02776B1240D0B8C945413CA94143623764E0801CB678DC522F338E60343C6C
            C52C9C65CD7E6420BA354D10EA8CE1BBEFBE7B951BC88F7021D2C92EE373B642
            43CF90791BE9EA7A3D9E74ABCD16CF300746FCF573791CD4EFE2D869D7D10C6C
            D87D0CBFEC3F815F0F9EC0BEE44CA464E5F3A570F2301F4BAC37C18F4F41AA00
            27D882DFDAA709DEBEA99B50E026F7821E3D7A0C4D4D4DB5D392720D64903F67
            6B480D68443B99B675370BA719DB710E5E352B23AE8CCBE6ECABD7050978F6CA
            76B41D2B5B060BF6734AFFE6BC5FB1746B3215E1204F002D14618F73A1F0AB47
            FB6168FB7AC6F1930193E663DDDEF20D976C8653CB545AAA9AFED2632383983F
            F91C9FC5B8D9053C31040D13C289A990BB9BD3FE99DC72E8C16EE3D30A711612
            2BAB02C3900A68D4EB69345FACDE8D955B8F62378D465D7A3E677A16BE1D36AB
            0D0EBE91763B5B487115CA28F3AB8B052752B3F0DE6D17614CEFA646FCA2458B
            3EEADFBFFF8D7C70D190B2E907ED823224333526AE0DCDF97CB62CFAE51046F0
            54A48BBBF12C1251259DDE88540E7C9FBEA20D1EBFBC5D89C80C36B9C339155F
            B39787E9B9045022B2E8418A48A522EE19D00C53F846ED389C8EBE13E7E138C7
            124EAEB3149155C82B4FEF3AE2D2A94134E68F1FC8A9B51D9EE3A938FEE5175C
            FF61DDB25530DA357E9290236C58E53B9C0869D3CE3805C00C8CE6A76DD279E2
            B2575252D2A37C46D6BAB5841F6031B63ED86250417C01A1454C4B3817222923
            FCC2AE3CFBED1679317CB56E1FDE9EB719DFF225CBA5E1D8D8C5AB5BD62C57A7
            45D5C2B30E90CF3CE571113594DDB8E4160BF00B089F96968D6F9F3E39D3E377
            9CC6F37CD34B240BA19C4CFA41BBA00C89899A094E6106C7E961EA825F71FFF4
            3588E0ECA9F42C5CF1AA348D773EE1BAC7884E2557B415AF3DB63BFEBE9A6B41
            2165D68214CF74909E99837EBC49329BFB6C0B788C77D49425B073C0595ECBA2
            B158465601BB2FA5AE212A1896B453431E9706AEEDDD041FDDD7CB20CC58FC0D
            F65E77055CDC6CD5115A7BADDA0869D2044E1ED4977158B94DA2BD321D9325C3
            DF1FE3DFCD37DFFC2E4F648EE0330EDC733BD266FD0721DCD977707FCE59B71E
            DC4D9AC21116014446C01612CA13003C6A5BF4426890FDC83F5761DAC21DC662
            644498A330E32A06054A1705FC202B8F8EC0CE373492AD67E7C6F1F8950BAC47
            B9DEC6411CA94A3AB5CAE15C7CFDFED9E1A81B136A44F29A533BDE50D9C48750
            D6691AFDA0DD5919120B3493291BCABAF3BD1598B68CD3F0482D0A125BCA15F0
            8D0AB55AF10D67502DEB441BB11CC0AE8E8D8DE94C2D59F4F65DF6DA92531812
            684879E8C3A580396C29A6CEFF150FFC93DB29BCAEC417DC90E7FF23230A77D9
            30AA733D84F04DA6A2980C298A2A83A1729DC67B43DAD531BA50111D9BFA168E
            3EF9086C0935B83716CA95ED50AE4447F3A82C0FE571E0EE6CD400B5C63FC963
            B64637B8F6F9E79FBF82E7A31645474737F6E6E662EFD52390B776352C31DC02
            71713B24241C96588639B5F7DA1C881A32087137DEACA48C05D6DBA77E8BF717
            6D474C028DCCCAA1A35F9ED532CB80C22D3E0CEE5017BDB944D29933DBDA9C85
            0D9A30175BB9E91CC297CB1066FED0D8323969E99D148B593C0CA8ED1446797A
            F6EC19CD319D4761EA278F7ED0EE6C0CA9160D6905536EA8E3B2FD9E998BB5FB
            4E409BB5C49571DABF6A59230CDABF8AE0949B7B5107F871AC5FA8EC41227E7F
            C916DCCE0DD798286E86069872302D9CE04C6A54A7BAF8785C1F3CCA5B2A9367
            6FE240BB64772059826C76799D1BC6620915A7E7A08179D97FCF6DC85A381FA1
            0387C0CD2D0C5B7804CF1FC5F2026314BB3617F8D5538476E864AE4AAF61E5FC
            816B461BB836149ABB733BF68EBA84E7976211D2BB1F4F0C346517C7168807E1
            6CDCF4D555260BBB36EDBB298F1F2ED986319C89C6264680B62254314807DADE
            A913EAC05F6FEF8E3EAD6A17C72DDF7A1843B818EC0873955DC3E39B967A2217
            632F6E8877EEB8D8E0E179F55D494949CD7EFBED379A2A7443C5C007FB733686
            D48D055BA9847773C0DC9767B48F712AAAE30DC295860CAE520F699D88190FF4
            35A2F8A5B5C39CE67278638B11E2062EA4FD7BF53EAE41B11917A214E84D3CC6
            99DBC4ABDAE3318EB1FEF0E6127CB1663F17DE5CA528C1991E8C19CA1DFD9AE0
            8D31DDCAC4F3ED337A5FE6BFE4EB5E44A906C0EC2E75186D0FCF1015ECDA09E7
            052D616FD408561EF9B0E9F807BB341FC747A1DDBA23B46D3B839B53FCAFF89D
            C9876AD7AEBD9908CB89797370E8A6EBE0A8950867B3E6C6F5258D8B1CE4B746
            44C167B3229A77E0EC3C9B94CE3DC08B1F9B856DA939C60BC97136459C747AF6
            B065F98A274B7BB6483C19C1D0DF6880B74C5D8EF8F8F0B243030B709C67B85E
            BC8E271A86B726B571A96012D7B71EA70EB4B695299D181141FE9C8D215DC34C
            7CAC74176D3A84CB5E5C043BDF14B302843781E5C0095EDFBE7FE80578E99A4E
            26BAD8DF7C301503267D834C9E85D540B238A254202D2D0B9A920F68531B439E
            5FC099DE112E1B384B51151A524A5A0EA67245FBB6FECD4BC467D2D81F9EBE1A
            4739150EE12901AEC471DCA41CFA68803C3BCE8DD02BBB36C0B53D9B187CB9DB
            B660CF25038DC3FD96581EA1D5D5EA30764F3CE16871398C816ECCA8ABCDA323
            BA7DF23ADFF677FAF6ED2B43C2515E5D4A9DF2321C3C2960E379232B4F0758C2
            232163E25C1DBEB050D4BCF33E638CB4E0A703B8F4F96F101ECB318CACD9C841
            E10F27683C199A8BBE9CF5CE2D5A072A8C29FC7DE0835598327F0BE2C85BFA9C
            3C77AC50407DCCA4019AAD1857DF3BF39B02EBC86D27FCAE2DD29734A44B9909
            E8F8C7EDD3BE475CD1204EB812C07A4AE7A6EC27F7F4C0E55D1AC2FF4FE391D1
            539660C1E6235CC8646B544A81266D0E0DA0318F762C7D7A282239331CCCA9FF
            773B92B91859964722B279256A265BBF81343A5386FC759C4AF7E455A93C6AB7
            B4D1EAADCCE57ACC3FEEE9891B7B69C782E332EEF61FBCED8F70F5EC0D7B8BD6
            0869DA04EEC4DAB0D5AC698C971CB13C9B44A3926C0157B76FA59C1CAE1FFDD3
            78BEF36664AEF816EEDEFDE1608B145ABF019C3CA36D898BE53DB60838136AF0
            94A55DA478907B64AFCDDD8CB8684D380C54F18FD12273EAFFFCD51DF048D1B6
            8D1999C78177E74767617B4A363473636F6C4619BE963322B93CB0F6C511A811
            E13670E3C78F8F7FE185174EF081B5837CE699C1E05DD02D128DE867266BB493
            7A1B5E5FB015B18602548D8CF173C2E4718A3F876F84B62AF289E09A24D6EF3A
            8A49FFD980E53B520B0DC28FA74490454D3D9E83A72E6F8327AF6C0F2DCC0DE3
            9BBB5C86C403742568F9A081B6C5EBC5BCF103706192D6F6882C72AF7CCDF352
            1FAFE7C5045711E6A4A7F33C715C155E357138128AF6F68EBC3409C75E7B812D
            064F4072342185FB5C6EDE1A8904A222E16ADD0E890F3D0A754DBC84E05DB66C
            59973E7DFA8CA37E6EF0E66463CFC8A1C8FB7533ACA4E5C4DFB8C604B66AB6E8
            587823A3107BD9E588BD760C72B97675E9A4B958B65BE3CC42C33A9933B6B25C
            FE4F3E9A89293774C2B84B0CB517477FBC7C07FEF8F60A4450FFECD78AF10A30
            1FC8E02CB47B520CCFC80F81168299CFAD575E7965EB19336628A17CD2E9E60C
            BDE05DB08614C60CAE67B24D341B1BF6DC7C2CDB7E8CC71D3463A39530A2B493
            12E3B97ACD19A8313EC9E2AEF6D1E379C8A782C289F705666377C35568AE9144
            B382BF9B300C0D3806509A2374C160CB51632C513A2DC9CAA2E1CE7CB00F06B5
            3E39203DC41382039E99873D1C78867046273A9357EB32C91C47DCDEB731A6DE
            DADD4463DFF557226BE346845D7F13A2790CD6C91BB0C6F9218FB770701DE286
            BA2C6594679FB7BCFBEEBB7DEFBFFFFE75149098BB633BF65D3E0C96962D1131
            7C24C2D9A2D9D91259A378FE288FA7866890165E8CB4D03FC035B241CFCEC35E
            1EA9D1C13CF29770D4370D221703D8B57DF6507F98E7E1BFFBF5378C79EB5B1C
            C9F5425B238672FD38C597C272DD3FB0195EB9A9AB11C3AFAABCC8F1D19FF7ED
            DBA77141B6907A41E4070BC11AD27066702613B51DE54CAA330788C7723CE59E
            41229DE10AF8D6999567E194C466B753FFB4203A8320C08F8F53D7142A59E39D
            3B06B528A6B863DA0A4C2BDAF52F46FA05346BEBC97DA567780C248ECDF9CEC3
            6978E13F3F61C5AE14847367DCCC87C9A275161BBBCF651387A1156F5E08EFCD
            48C79E118351C016C512C5FF95D219C6191707B3A14E7E002206561E958DBAFC
            4AC45C364AE460B736994B1A1FB469D366A310E9F3E7E0C02DD772B980171978
            AF1F2194E10AE10D122E21B085D2B5EC9ABC03E76477B7955F6F193C691E5268
            105AA9167F19A02E72D9655FC4C3791771F77EF79134CCE37A9A2E0184387963
            A774A128802CD0CDE4B76EEE8ABB07352706D9DC201ED6AC59B3157CB0117E57
            43BA8B86F41766026BB88B3E98EB175E6E9CF2BC9A509502940FEECA218577DC
            6E1FD40CEF8CED5142EE7B3C967BD7DF56F39280BB04DE7C900275E932942D5E
            3CAFE51C3C918D7CAFC558302D3D18D5F823F9501A9EBBB623C68F6A6F8A4036
            2F43EE1939044E4EED435BF1507FADDAB0D7AB071B676A3E9B9D77D1C2B99A5D
            0B8EBAF50D1EEEF08F69DEFC82A31D3B769A2D44F29BAFE1E8949710CED6C84D
            3ED1D912F80512BE383ECEF8B480E96ADA0C368635F31DC02594239CDDF2629A
            D80303BBF90C4E5CF2D9E25A795498BBC070B3B596F105B0234387D91C9FCE78
            A80FAF5BD595CC0DF3E6CDBB7EC890215BF9406930F6D87EAF1649DF407C9D19
            316E7DDCC55B1F11BCF5A15C0977D640411A5C67B199BF634033BC714B371E68
            E300C54FF07EDE24B9F8F1396CD20BA0DB1E81142163D40AB05A42076768BCF0
            CF21046BB1488E8C8D7BB748E53EE14D3D1BE1BD7B7B196388A268A47DFD250E
            F07AB5A3652B7EFBA8216F8B247035BB0E78870A5AFF71344C42D4D0E1ECE29C
            62F17EFAE9A7BCE2DFE916AE68DF23C4817BC622E3F34F606FD711F6441A5C8D
            5AC6E09AF60C5EBD85BB536784F3EAB668758D6BE83373B0627F1A22028CFB44
            6382CA25E357F7DD382E8C7B96566C4FCE809D4B09268DE96BFB249C85FC66C2
            10B4285C089EC38FA83EFAF0C30F6F268D87E02594D08B9E2B0AC1766D2FB030
            8F28B1FBA6ADC45B8BB71933B64095299A53016D86D185BFDAE5CFE65A8A978B
            8949F161187F591BFCB16F33C6173A1E2C3BC637909BF99670613E5CBA1D63DF
            FB1E4E8EB1DC5CCD3DD3F42D4672166472AA5F90958B3B0636C38B3776E5AD14
            B5F4925C08BF3D351EE99F7C0457870BA17BF970F3D4A1D5C1300FEEE9D03EAF
            20C55E7B2357B42370E2C489DDDF2E5BD66DD8F0E1FFE6FA582F4F5A3A760D1F
            C0A5050F74ED88D77A6155B7C66D104B78A8B132EEE6C726A2698885A90153E7
            6FC65D2C4F7C4D76815A3432234AF9D43D9753F2508B5B275F3CD8179367FC84
            193F1D42046FCE946E957406298EFCCB9EBFC4BCFC398D5F6A7987478037102D
            4332DEAC33D51D7902BA600D69A9C562E9A59DE9FE4F7C8D65DB92E162A14A17
            22608AA5903EBE0FDA05D3605C57903A24466264D78618DDBD11E2B56F77923E
            F3B2CB46FEB16FDF7ECDC68D1B37C9447FF4DD763CCC7B6C87D87C1B67BED995
            A9E5B19668C0289D99E3AD551E63F1212FDFC36BDB1EB4AF136518EBA86E8D4C
            71277DD2EB6311F9070FC03D6418421B26C1A6697FFDFA70D7AA637CFBC81615
            5D4CBF73E7CEEEFC68FC2A0EB48F12199BBDE147EC1DD2974B06ADF8F99B8BA1
            6F2859697CCE7AF519AE0BEDD7F99FCF260F54E937BFB5141FADD8CDE1540877
            F52D3CCC676594EADA621C6FC92FF020872F40476E2ABF3BB63BDAF1D33EDD1F
            FD123FEC39CE1B349C8489941CA6D30C37D6027CFFD24834A56E897FFD99679E
            F984DF075FC5B09CC1F17B19D2121A526F35C793BE588FA35C7B29FA2A883276
            C6C0F221D26545CD987034E0718D26DC1668C2B7D151D20AF4B66F62054DF8E0
            830F16D4AB572F82FFE7C7D4C68D1B0F3313DAC366FD8BD5BB31FFE743D8C76E
            4AE7B133F2BCF0700A232DF12819F81541C470C1349E970BDBB2122EED509F5B
            0C8988E29A9429A7C8A769C3AA5BB5875E7D09DED41463F5DB50B48539E606AB
            8D1BAD708722BC472F44B62D5CD1E657506278F02D6DD4A8517ACB91F5EB261C
            FBF07D58F9CFE729E09D7E1A338D5C9720B5C96BE1974B6247702398EB4845E9
            1A9E74FAF7C5DBF0CFEF7662EFB14CA4F2E80DE7E6B0D1B063B8BBDF88C67015
            6F8DDCD8AB09A238E857D7F5D69C4DD87C289D1F31618BAA021B920A7F8C0557
            4E72EE1DD60217D4320C7FCA5B6FBDF5FEBDF7DEBB9914A256797FB7AEEDCF34
            A489CCC8B976998B172FFAF2C1071F9AC0CFE71D63621184C3DC5CF771D77A3A
            3F153C8ACF25DC210EAAF727A72139239F6FB8DA3A70871C08E580541B9B75B9
            7C10CE6EB004131F0E1F3EBCFEABAF66FDE7965B6E7D908F51840A396ED2F2A0
            A4DDCEF3D93967C448C3800CB390583C0E066909FCA5534BA2938F073963A52D
            192F42FDF850B4A81B8BA24D575205E5A6F0E8C8641E2139486E357786E11B2F
            0A11C1BA60BBB6A634A4E54C3481702E5C2E373EBF623FFEF759B366ADE5BE9C
            14AC1544360F38F1ECB3CFFAB89765AD53A7CE3BFC7EE4CDCC80E2E905E5F298
            C627DC2E98C093822953A64CF933BBCE872A2289EB478743424212BB77EF1ECE
            EF32ED0F0D0DAD9021F2D337EBE6CE9DFBE155575D750F3F81D3A42269579496
            67CB9FE0A9C897F43131F2CA8804BF4F8BC40CC875E721F76768502DF92A0695
            11BD051C40EB43EDFCE841413ED76156F20ACF1C56E8D1850B176EE00AAC9A5D
            19501E133C41C82260D2A449DC9C8CC7830F3E884B2FBD74E4D34F3F7D1F5BA7
            E6090909218CD740DC413F9053539EC5BDB00CE6BD8006F00B0FDC4FE307AC56
            302C7AF65D489E3A75EA4D975C72C99DFCFF4DF4A2884771C5C032EB84A78506
            EEE3207B1B0DEF698E8F1613EF5AB97265277E34E2F1ACACACF6BC2820BDA8A2
            36913993E0230D1B221E49638034562E0EE6F2ECD25FF83F3CADE107C15A33ED
            3FF1EB731D59969AA40F6490278E1E3D9AC9FC6FA4EE522223233BD1F8C239C0
            277960C7A4F4891C2FD7B87EB9EDB6DBEEE2EAFB4E5286118C3CD1573EE5050D
            41B5484A6DDAB469BC66F386253333AB09DF46EE48704F421167082A1C0DC5C7
            83F23E56A297BBE65E56703ED9056A61641432201DB82A2E30E30D43626B047E
            ED15AC4CA122D942C5F6EBD7AF0EF7B81AF03FFEAB49C586B1E7901CC5D366F9
            51E3BCFC634CEF00BBC95DACB80CA65BC04855B416A394EE113E0BC7F186DB95
            949454978C56E2BC04C331DFFCD0888DFBAD0E2BDF6ECFAE5DBBD42D49462A09
            BC3CC09FCFAB475A9C0CE36776B8FAE011AF9D719203CE3A6584BCAB66B7F0E3
            0D1E1A85D265B4E124072C9B6DE4C891F684F8F8689E8A9441D5606CA8C7E375
            9167C7ECD9B30FF14F7C3EA665494A4A028FAC70C1BFCC544F697BF4A2331D1F
            79F4ACF2291D95399B720DC7721A7EB03F411B52C3860DC19B134AD7C99F2882
            2ADE5016C3C1380B99F4F61B0B640C671064402A3C83279D5A241A0EF8112ADD
            843023D40AF9B746A5F9245FF993718956CF0255885ABB406949D9314C406514
            2D83C54E79154E2019A98C910C7A86535A1ADD4A86F80D64D18F781514AF402F
            CC71224C7EE53194CFF2550E55BC7C81E48A4720BCF9AC3464B0642B768AF3C7
            E959F9CC228564D12B74BF9B21E9BFDDD487310BB361FC4A61CAB4A924035981
            1F29468553214FC9568E21993CEA0AA5543D4BA6FCD2A03C0A84577A4A57E140
            2019AAD44071C229FE5432F482C920CCF4C4531AC41F285E062FBD8A5EE9C817
            8856A01645C6249CD2505AC2EBB93C104F99F2FE6E8664E6924DBD19AC2ABE5A
            18BD95FFEBFC2ADD478A127DB4C83F53AF11093B11BE2194C8FBD91A00E5558A
            0BBA6B33530F60485731CE54D8670CBF48F0770BF820A5D22B7652D05FF9B493
            603AC9902C3D2BBEB4F225E3DF8C944F0F57F3C79F9F8FC54E726EE3932A43F4
            6B19567A0206CFB9539A2AB712521EA493D2E5515C79B083113226F188978F85
            EEFFB221AD6111A52C7A50855DA840114819528A1E1527DFA4D59B36900813EF
            2F4771B18CF3773222198889533A26AF89932F837C41018219AF7CC8E8C443F4
            3977EF3205A52963577E54669595E83372A6118A5FBA2866FABF6A4852922A4D
            2DC800965695D598BEE9D42A48A9AA50B312A560198594AB164CCA124E0627A5
            4986E2244761C9920189C74C4774A50DCD9F4E61FFB7592D8464982D92F2AA34
            84974C8178048A135E7953FE9537C50B142F394ADF7C3671F2C5235FFCE295AF
            3298E92AEE74F2C5239AD26908579C671A947FFA5A62507ACAABF8FECA78F94A
            EF9C4065766D2AB02A5F199591986155B0590819998C4D8ABC5D8445209CE24C
            0353E5C850F42CC5EB590666568C642B3D35F3FE7C45E28A3DD14999A2932115
            4714052443E9A8528A5086E79F3FC910526550C5292C508BA2BC295E712AA7F0
            AA40FF97456513282DC50B542EE94861F1CB978CD2F2651CE255195576E94072
            02E699C6723B871A8A570BE62F4B86E4AF6FA557A9509986A4C2AAD066A5995D
            930A2F2528E3264E952A3AE104E213BFA9608585336914367902C5894EF19265
            828C430AD5B32A4DB215F60755888C5486231952BE70A29161A852CC8A16BF68
            9417D1292CD06ABB492F6310BF642A3F3204955906273DC8579EC4AF67C93C95
            7CC99051CA3815567AA6FCF2F22C5AE948F1E25119B4E0A8B495CF7302956548
            528CA930559A14AA67E1F526A8502A8094AE82E96D9692851398CA92C149C152
            B60C4161D12B5E32A448C9957C7575FE74E2952C1354E152A82A4B7932F1A62F
            F9E29782254B78A5A53C2A2C43128D2A4E3492A17495BE7FB9CC67954974320C
            D149A6F22DA352DE15A7D651156DE649711591AF7C9D2ECF92AF722B3DB544D2
            99F8CE295496214919528A322F252AD3AA14294E05D19B21E54BE98A57258946
            203A295FBE69747A16AF2A437C922FE54BBED20944A738C93341CA9431894F46
            60E24D5F71A29101CA6085579A4A5B61E5513402B30CC22B5E749229D92A93F2
            2819F2FDE965942A97E8C42B507A322CE941E9FBD32BDE5FBECA2419C22B3FA6
            914886D2135E79118FC2A2916F1AB0C26BD9E529AF0A9F33A80C4352C5AAA2A5
            1815DCCCAC14A842AAE5D1DB2A85497152AA5930D1988536F1E29162244B8624
            1A2953F21536E5A9D25489269D99AEE99BE9894F72E49B714A4395221A7F2351
            FE8433D3907CA5A34A53E589CF3F6F9267F2C83824D3CC8FF2AA7C8B4615EC9F
            BE7082D3C937756BEAC64C2B609E6930D2B3E46AB0ADD654AD979E631917287D
            C5550A54862199CA305B093363A595208391A25549528C2A458595C2F52C25A8
            B0A211ADE884933C559EE81596118A3E109DE24D10BDF226F9AA5C1982C24A53
            952E3A558CD254C5284E46A467A5211E1982F032443D074A533C92237902D3E8
            1436F9D5228B5F79320DD33FBE3CF9A66CE557FA359F95C74079966C81F4A37C
            4B8F3B694492AFF4CE199C952171866016CCBFD2CDCCAAC28ADF082215562119
            2C762AB02A588A927214A14A915C295FCA124EBC92A767E1850B4427BC3FC898
            45A70A34F1CAAB2A5BE949D1320EFF38C957BE945719A20CC0AC08C92A9D37A5
            A1165932540E55B8C202D10A54A97A16287DBD2067225F72255F7952D9C51F30
            CF3496B57EF5213A81F27EB5E2F4702EE1AC0C491963E6A524558A1E4B83E284
            33E3CD67E104265E617F105DE9B833C5F9CB31C3AA3419932AB1B45CE1152FE3
            91E24D1EF9679AA6E8445F5AB67002BD04A2299DBE70A5794AE34A3F4B5E719E
            692425F25C541F4A4F2D91CA24FA730E676D48E73C87D50954090D541B5295A8
            A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F1D55891C561B
            5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F1D5589
            1C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D48E77F
            1D55891C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF33596D
            48E77F1D55891C561B5295A8A6F33F93D58674FED75195C861B52155896A3AFF
            33596D48E77F1D55891C561B5295A8A6F33F93FF1F8EEAF28B5166BE5C000000
            0049454E44AE426082}
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Left = 170.078850000000000000
          Top = 7.559060000000000000
          Width = 782.362710000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Pagamento de Fornecedores[tipoEstab] - Visualiza'#195#167#195#163'o')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 944.882500000000000000
          Top = 60.472480000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            '[Date]')
        end
        object Page: TfrxMemoView
          Left = 891.969080000000000000
          Top = 102.047310000000000000
          Width = 154.960730000000000000
          Height = 30.236240000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages#]')
        end
        object Memo12: TfrxMemoView
          Left = 442.205010000000000000
          Top = 52.913420000000000000
          Width = 472.441250000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[tipoPagamento]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 442.205010000000000000
          Top = 83.149660000000000000
          Width = 393.071120000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dataIni] at'#195#169' [dataFin]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 442.205010000000000000
          Top = 117.165430000000000000
          Width = 393.071120000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[dtCompensa]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 52.913420000000000000
          Width = 204.094620000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Tipo de Pagamento: ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 192.756030000000000000
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Per'#195#173'odo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 192.756030000000000000
          Top = 117.165430000000000000
          Width = 260.787570000000000000
          Height = 45.354360000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data de Compensa'#195#167#195#163'o:')
          ParentFont = False
        end
        object semana: TfrxMemoView
          Left = 925.984850000000000000
          Top = 81.259842519685040000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Semana: [semana]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 910.866730000000000000
          Top = 37.795300000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Repasse N'#194#176': [codAutorizacao]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 18.897650000000000000
        Top = 396.850650000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'Footer1OnBeforePrint'
        object Memo6: TfrxMemoView
          Left = 952.441560000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            
              'R$[FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEstab."LIQUIDO">,DetailDat' +
              'a1))]')
        end
        object Memo11: TfrxMemoView
          Left = 835.276130000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Memo.UTF8 = (
            'Total do Banco:')
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897650000000000000
        Top = 476.220780000000000000
        Width = 1046.929810000000000000
        object Memo22: TfrxMemoView
          Left = 744.567410000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              'Total todos os bancos: R$ [FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEs' +
              'tab."liquido">,DetailData1,2))]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 283.464750000000000000
        Width = 1046.929810000000000000
        Condition = 'frxBancos."codbanco"'
        ReprintOnNewPage = True
        StartNewPage = True
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 41.574830000000000000
        Top = 181.417440000000000000
        Width = 1046.929810000000000000
        object Memo4: TfrxMemoView
          Top = 26.456710000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Cod. ')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 60.472480000000000000
          Top = 26.456710000000000000
          Width = 408.189240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Estabelecimento')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 449.764070000000000000
          Top = 26.456710000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Agencia')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Top = 26.456710000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Conta')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 687.874460000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Favorecido')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 941.102970000000000000
          Top = 26.456710000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'L'#195#173'quido')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Banco:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 79.370130000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'codbanco'
          DataSet = frxBancos
          DataSetName = 'frxBancos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxBancos."codbanco"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 192.756030000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Nome do Banco:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 325.039580000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'banco'
          DataSet = frxBancos
          DataSetName = 'frxBancos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxBancos."banco"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 3.779530000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        DataSet = frxBancos
        DataSetName = 'frxBancos'
        RowCount = 0
      end
      object SysMemo1: TfrxSysMemoView
        Left = 423.307360000000000000
        Top = 506.457020000000000000
        Width = 619.842920000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8 = (
          
            'Total por p'#195#161'gina R$[FormatFloat('#39'#,##0.00'#39',SUM(<frxPgtoEstab."L' +
            'IQUIDO">,DetailData1))]')
        ParentFont = False
      end
    end
  end
  object QDescontosDePagtosBaixados: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'cred_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'dt_desconto'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'
      'cred_id,'
      'taxa_id,'
      'dt_desconto,'
      'taxas_prox_pag_id_fk,'
      'historico as descricao,'
      'valor,'
      #39#39' marcado,  '
      '0 id,'
      #39#39' TIPO_RECEITA'
      
        'FROM taxas_repasse where cred_id = :cred_id and DT_DESCONTO = :d' +
        't_desconto')
    Left = 64
    Top = 456
    object QDescontosDePagtosBaixadoscred_id: TIntegerField
      FieldName = 'cred_id'
    end
    object QDescontosDePagtosBaixadostaxa_id: TIntegerField
      FieldName = 'taxa_id'
    end
    object QDescontosDePagtosBaixadosdt_desconto: TDateTimeField
      FieldName = 'dt_desconto'
    end
    object QDescontosDePagtosBaixadostaxas_prox_pag_id_fk: TIntegerField
      FieldName = 'taxas_prox_pag_id_fk'
    end
    object QDescontosDePagtosBaixadosdescricao: TStringField
      FieldName = 'descricao'
      Size = 50
    end
    object QDescontosDePagtosBaixadosvalor: TBCDField
      FieldName = 'valor'
      Precision = 8
      Size = 2
    end
    object QDescontosDePagtosBaixadosmarcado: TStringField
      FieldName = 'marcado'
      ReadOnly = True
      Size = 1
    end
    object QDescontosDePagtosBaixadosid: TIntegerField
      FieldName = 'id'
      ReadOnly = True
    end
    object QDescontosDePagtosBaixadosTIPO_RECEITA: TStringField
      FieldName = 'TIPO_RECEITA'
      ReadOnly = True
      Size = 1
    end
  end
  object DSTeste: TDataSource
    DataSet = QDescontosDePagtosBaixados
    Left = 96
    Top = 456
  end
  object JvPopupMenu1: TJvPopupMenu
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 736
    Top = 232
    object ConferirNotas1: TMenuItem
      Caption = 'Conferir Notas'
      OnClick = ConferirNotas1Click
    end
    object LanarTaxas1: TMenuItem
      Caption = 'Lan'#231'ar Taxas'
      OnClick = LanarTaxas1Click
    end
  end
  object DSBancoSacado: TDataSource
    DataSet = QBancoSacado
    Left = 848
    Top = 392
  end
  object QBancoSacado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select '
      'conta_id,'
      'cod_banco,'
      'agencia,'
      'contacorrente,'
      'nome_convenio,'
      'cnpj,'
      'endereco,'
      'coalesce(numero,0) numero,'
      'cidade,'
      'cep,'
      'uf,'
      'correntista_nome correntista'
      'from contas_bancarias')
    Left = 816
    Top = 392
    object QBancoSacadoconta_id: TIntegerField
      FieldName = 'conta_id'
    end
    object QBancoSacadocod_banco: TIntegerField
      FieldName = 'cod_banco'
    end
    object QBancoSacadoagencia: TStringField
      FieldName = 'agencia'
      Size = 10
    end
    object QBancoSacadocontacorrente: TStringField
      FieldName = 'contacorrente'
      Size = 15
    end
    object QBancoSacadonome_convenio: TStringField
      FieldName = 'nome_convenio'
      Size = 30
    end
    object QBancoSacadocnpj: TStringField
      FieldName = 'cnpj'
      Size = 14
    end
    object QBancoSacadoendereco: TStringField
      FieldName = 'endereco'
      Size = 30
    end
    object QBancoSacadonumero: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object QBancoSacadocidade: TStringField
      FieldName = 'cidade'
    end
    object QBancoSacadocep: TStringField
      FieldName = 'cep'
      Size = 8
    end
    object QBancoSacadouf: TStringField
      FieldName = 'uf'
      Size = 2
    end
    object QBancoSacadocorrentista: TStringField
      FieldName = 'correntista'
      Size = 150
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 504
    Top = 352
  end
end
