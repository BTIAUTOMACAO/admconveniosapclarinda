unit URelCredCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, frxClass, frxExportPDF, DB, frxDBSet, ADODB,
  JvMemoryDataset, StdCtrls, ExtCtrls, Buttons, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, ComCtrls, JvExControls,ShellApi, JvDBLookup, Mask,
  JvExMask, JvToolEdit, Menus;

type
  TFRelCredCad = class(TF1)
    GroupBox1: TGroupBox;
    ButListaEmp: TButton;
    GroupBox2: TGroupBox;
    datafin: TJvDateEdit;
    DataIni: TJvDateEdit;
    MCredenciados: TJvMemoryData;
    MCredenciadoscred_id: TIntegerField;
    MCredenciadosnome: TStringField;
    MCredenciadosfantasia: TStringField;
    MCredenciadosendereco: TStringField;
    MCredenciadosnumero: TIntegerField;
    MCredenciadostelefone1: TStringField;
    MCredenciadostelefone2: TStringField;
    MCredenciadosdtcadastro: TDateTimeField;
    MCredenciadosmarcado: TBooleanField;
    frxCredenciados: TfrxReport;
    QCredenciados: TADOQuery;
    QCredenciadoscred_id: TIntegerField;
    QCredenciadosnome: TStringField;
    QCredenciadosfantasia: TStringField;
    QCredenciadosendereco: TStringField;
    QCredenciadosnumero: TIntegerField;
    QCredenciadostelefone1: TStringField;
    QCredenciadostelefone2: TStringField;
    QCredenciadosdtcadastro: TDateTimeField;
    dsCredenciados: TDataSource;
    dbCredenciado: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    ButMarcaDesmEmp: TButton;
    ButMarcaTodasEmp: TButton;
    ButDesmarcaTodosEmp: TButton;
    BitBtn1: TBitBtn;
    bntGerarPDF: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    QCredenciadosdescricao: TStringField;
    MCredenciadosdescricao: TStringField;
    QCredenciadosbairro: TIntegerField;
    QCredenciadoscidade: TIntegerField;
    MCredenciadosbairro: TIntegerField;
    MCredenciadoscidade: TIntegerField;
    procedure BitBtn1Click(Sender: TObject);
    procedure ButListaEmpClick(Sender: TObject);
    procedure bntGerarPDFClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelCredCad: TFRelCredCad;

implementation

uses DM, impressao, Math, cartao_util;

{$R *.dfm}

procedure TFRelCredCad.BitBtn1Click(Sender: TObject);
begin
  inherited;
  frxCredenciados.Variables['dataIni'] := QuotedStr(DateToStr(DataIni.Date));
  frxCredenciados.Variables['dataFin'] := QuotedStr(DateToStr(datafin.Date));

  frxCredenciados.ShowReport();
end;

procedure TFRelCredCad.ButListaEmpClick(Sender: TObject);
begin
  inherited;

  if((DataIni.Date = 0)) then
  begin
     MsgInf('A data inicial n�o pode estar vazia'); DataIni.SetFocus; Exit;
  end;

  if((DataIni.Date > datafin.Date) and (datafin.Date <> 0))then
  begin
     MsgInf('A data inicial n�o pode ser maior do que a data final');
     DataIni.SetFocus;
     Exit;
  end;
  
  QCredenciados.Parameters[0].Value := DataIni.Date;
  QCredenciados.Parameters[1].Value := datafin.Date;

  QCredenciados.Close;
  QCredenciados.Open;
  if QCredenciados.IsEmpty then
  begin
    MsgInf('N�o foi encontrada nenhum credenciado cadastrado no per�odo selecionado.');
    DataIni.SetFocus;
  end;
  QCredenciados.First;
  MCredenciados.Open;
  MCredenciados.EmptyTable;
  MCredenciados.DisableControls;
  while not QCredenciados.Eof do
  begin
    MCredenciados.Append;
    MCredenciadoscred_id.AsInteger       := QCredenciadoscred_id.AsInteger;
    MCredenciadosnome.AsString           := QCredenciadosnome.AsString;
    MCredenciadosfantasia.AsString       := QCredenciadosfantasia.AsString;
    MCredenciadosendereco.AsString       := QCredenciadosendereco.AsString;
    MCredenciadosnumero.AsInteger        := QCredenciadosnumero.AsInteger;
    MCredenciadosbairro.AsString         := QCredenciadosbairro.AsString;
    MCredenciadoscidade.AsString         := QCredenciadoscidade.AsString;
    MCredenciadostelefone1.AsString      := QCredenciadostelefone1.AsString;
    MCredenciadostelefone2.AsString      := QCredenciadostelefone2.AsString;
    MCredenciadosdtcadastro.AsDateTime   := QCredenciadosdtcadastro.AsDateTime;
    MCredenciadosmarcado.AsBoolean       := False;
    MCredenciadosdescricao.AsString      := QCredenciadosdescricao.AsString;
    QCredenciados.Next;
  end;
  MCredenciados.First;
  MCredenciados.EnableControls;
end;

procedure TFRelCredCad.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    BitBtn1.Click;
    if QCredenciadoscred_id.AsInteger <= 1 then
    begin
      frxCredenciados.PrepareReport();
      frxCredenciados.Export(frxPDFExport1);
    end
  else
    begin
      frxCredenciados.PrepareReport();
      frxCredenciados.Export(frxPDFExport1);
    end;
    ShellExecute(Handle, 'open', PAnsiChar(sd.FileName), nil, nil, SW_SHOWNORMAL);
  end;
end;

end.
