unit UManutTaxaFixaPadraoBemEstar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, ADODB, Menus, StdCtrls, Mask, JvExMask, JvToolEdit,
  ComCtrls, Grids, DBGrids, JvExDBGrids, JvDBGrid, Buttons, ExtCtrls,
  DBCtrls;

type
  TFCadManutTaxaFixaPadraoBemEstar = class(TFCad)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    JvDBGrid1: TJvDBGrid;
    DBEdit4: TDBEdit;
    QCadastroTAXA_ID: TIntegerField;
    QCadastroFAIXA_INICIO: TIntegerField;
    QCadastroFAIXA_FIM: TIntegerField;
    QCadastroTAXA_VALOR: TBCDField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroAPAGADO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Valida();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadManutTaxaFixaPadraoBemEstar: TFCadManutTaxaFixaPadraoBemEstar;

implementation

uses DM, UMenu, UValidacao;

{$R *.dfm}

procedure TFCadManutTaxaFixaPadraoBemEstar.FormCreate(Sender: TObject);
begin
  chavepri := 'taxa_id';
  detalhe := 'ID: ';
  QCadastro.Open;
  FMenu.vFCadManutTaxaFixaPadraoBemEstar := True;

  inherited;
end;

procedure TFCadManutTaxaFixaPadraoBemEstar.ButBuscaClick(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  QCadastro.Close;
  QCadastro.SQL.Clear;
  QCadastro.SQL.Add('SELECT * FROM EMP_TAXAS_BEM_ESTAR_PADRAO');
  QCadastro.Open;
  Screen.Cursor := crDefault;
end;

procedure TFCadManutTaxaFixaPadraoBemEstar.QCadastroAfterInsert(
  DataSet: TDataSet);
begin

  inherited;
  {Recuperando o �ltimo ID do conveniado}
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SMANUT_TXFIXA_PADRAO');
  DMConexao.AdoQry.Open;
  QCadastroTAXA_ID.AsInteger := DMConexao.AdoQry.Fields[0].Value;
  {Fim do bloco para recupera��o do ID}
//  QCadastroLIBERADO.AsString          := 'S';


end;

procedure TFCadManutTaxaFixaPadraoBemEstar.QCadastroBeforePost(
  DataSet: TDataSet);
begin
  if QCadastro.State in [dsInsert, dsEdit] then
    valida;

  if QCadastro.State = dsInsert then begin
    QCadastroDTCADASTRO.Value   := Now;
    QCadastroOPERCADASTRO.Value := Operador.Nome;
  end;
  inherited;
  if (not colocouMensagem) then begin
    Abort;
  end;

end;

procedure TFCadManutTaxaFixaPadraoBemEstar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FMenu.vFCadManutTaxaFixaPadraoBemEstar := False;
end;

procedure TFCadManutTaxaFixaPadraoBemEstar.Valida();
begin
  if not (QCadastro.State in [dsInsert, dsEdit]) then
    Exit;

  if fnVerfCompVazioEmTabSheet('Informe a faixa inicial.',DBEdit2)                           then Abort;
  if fnVerfCompVazioEmTabSheet('Informe a faixa final.',DBEdit3)                             then Abort;
  if fnVerfCompVazioEmTabSheet('Informe o valor da taxa.',DBEdit4)                           then Abort;
end;

end.
