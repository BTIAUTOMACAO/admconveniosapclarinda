//DEIXAR 3 LINHAS EM BRANCO NA PARTE QUERY DE CONTACORRENTE POIS ELAS S�O NECESS�RIOAS PARA OS FILTROS!
unit uExportacaoConfabOticas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, DB, ZAbstractRODataset,
  ZDataset, Math, Mask, ToolEdit, DBCtrls, {ClipBrd,} ZAbstractDataset,
  ADODB;

type
  TFExportacaoConfabOticas = class(TForm)
    Panel1: TPanel;
    btnExportar: TButton;
    ProgressBar1: TProgressBar;
    lblStatus: TLabel;
    Label1: TLabel;
    sd: TSaveDialog;
    edtCaminho: TEdit;
    BitBtn1: TBitBtn;
    Bevel1: TBevel;
    dataini: TDateEdit;
    Label2: TLabel;
    datafin: TDateEdit;
    lkpEmp: TDBLookupComboBox;
    Label3: TLabel;
    dsEmp: TDataSource;
    cbNotas: TComboBox;
    Label4: TLabel;
    qEmps: TADOQuery;
    qCc: TADOQuery;
    qEmpsEMPRES_ID: TIntegerField;
    qEmpsFORMA_LIMITE_ID: TIntegerField;
    qEmpsTIPO_CARTAO_ID: TIntegerField;
    qEmpsCRED_ID: TIntegerField;
    qEmpsCONTRATO: TIntegerField;
    qEmpsFECHAMENTO1: TWordField;
    qEmpsFECHAMENTO2: TWordField;
    qEmpsVENCIMENTO1: TWordField;
    qEmpsVENCIMENTO2: TWordField;
    qEmpsINC_CART_PBM: TStringField;
    qEmpsPROG_DESC: TStringField;
    qEmpsNOME: TStringField;
    qEmpsAPAGADO: TStringField;
    qEmpsLIBERADA: TStringField;
    qEmpsFANTASIA: TStringField;
    qEmpsNOMECARTAO: TStringField;
    qEmpsCOMISSAO_CRED: TFloatField;
    qEmpsFATOR_RISCO: TFloatField;
    qEmpsSENHA: TStringField;
    qEmpsCGC: TStringField;
    qEmpsINSCRICAOEST: TStringField;
    qEmpsTELEFONE1: TStringField;
    qEmpsTELEFONE2: TStringField;
    qEmpsFAX: TStringField;
    qEmpsENDERECO: TStringField;
    qEmpsNUMERO: TIntegerField;
    qEmpsBAIRRO: TStringField;
    qEmpsCIDADE: TStringField;
    qEmpsCEP: TStringField;
    qEmpsESTADO: TStringField;
    qEmpsREPRESENTANTE: TStringField;
    qEmpsEMAIL: TStringField;
    qEmpsHOMEPAGE: TStringField;
    qEmpsOBS1: TStringField;
    qEmpsOBS2: TStringField;
    qEmpsTODOS_SEGMENTOS: TStringField;
    qEmpsDT_DEBITO: TDateTimeField;
    qEmpsTAXA_BANCO: TStringField;
    qEmpsVALOR_TAXA: TBCDField;
    qEmpsTAXA_JUROS: TBCDField;
    qEmpsMULTA: TBCDField;
    qEmpsDESCONTO_FUNC: TBCDField;
    qEmpsREPASSE_EMPRESA: TBCDField;
    qEmpsBLOQ_ATE_PGTO: TStringField;
    qEmpsOBS3: TStringField;
    qEmpsPEDE_NF: TStringField;
    qEmpsPEDE_REC: TStringField;
    qEmpsACEITA_PARC: TStringField;
    qEmpsDESCONTO_EMP: TBCDField;
    qEmpsUSA_CARTAO_PROPRIO: TStringField;
    qEmpsCARTAO_INI: TIntegerField;
    qEmpsFIDELIDADE: TStringField;
    qEmpsENCONTRO_CONTAS: TStringField;
    qEmpsSOLICITA_PRODUTO: TStringField;
    qEmpsVENDA_NOME: TStringField;
    qEmpsOBS_FECHAMENTO: TStringField;
    qEmpsLIMITE_PADRAO: TBCDField;
    qEmpsDTAPAGADO: TDateTimeField;
    qEmpsDTALTERACAO: TDateTimeField;
    qEmpsOPERADOR: TStringField;
    qEmpsDTCADASTRO: TDateTimeField;
    qEmpsOPERCADASTRO: TStringField;
    qEmpsVALE_DESCONTO: TStringField;
    qEmpsAGENCIADOR_ID: TIntegerField;
    qEmpsSOM_PROD_PROG: TStringField;
    qEmpsEMITE_NF: TStringField;
    qEmpsRECEITA_SEM_LIMITE: TStringField;
    qEmpsCOMPLEMENTO: TStringField;
    qEmpsUSA_COD_IMPORTACAO: TStringField;
    qEmpsBAND_ID: TIntegerField;
    qEmpsNAO_GERA_CARTAO_MENOR: TStringField;
    qEmpsTIPO_CREDITO: TIntegerField;
    qEmpsDIA_REPASSE: TIntegerField;
    qEmpsOBRIGA_SENHA: TStringField;
    qEmpsQTD_DIG_SENHA: TIntegerField;
    qEmpsUTILIZA_RECARGA: TStringField;
    qEmpsRESPONSAVEL_FECHAMENTO: TStringField;
    qEmpsMOD_CART_ID: TIntegerField;
    qCcmatricula: TFloatField;
    qCcnome: TStringField;
    qCcnf: TIntegerField;
    qCcvalor: TBCDField;
    qCctotal_compra: TFloatField;
    qCccodigo_servico: TStringField;
    qCcDATARECEITA: TDateTimeField;
    qCccred_id: TIntegerField;
    qCcdatavenda: TDateTimeField;
    qCcnome_otica: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    Cancelar : Boolean;
    procedure HabDesabBtnProcesso(Ativar: Boolean);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FExportacaoConfabOticas: TFExportacaoConfabOticas;

implementation

uses DM, URotinasTexto, cartao_util, UValidacao;

{$R *.dfm}

procedure TFExportacaoConfabOticas.HabDesabBtnProcesso(Ativar: Boolean);
begin
  if Ativar then begin
    Screen.Cursor := crHourGlass;
    btnExportar.Caption := '&Cancelar';
    ProgressBar1.Position := 0;
    Cancelar := False;
  end else begin
    Screen.Cursor := crDefault;
    btnExportar.Caption := '&Processar';
  end;
  Application.ProcessMessages;
end;

procedure TFExportacaoConfabOticas.FormCreate(Sender: TObject);
begin
  sd.FileName := ExtractFilePath(Application.ExeName)+'ExportacaoNotasSeg2e3-'+FormatDateTime('yyyymmdd',now);
  edtCaminho.Text := sd.FileName;
//  dataini.Date := now-StrToInt(FormatDateTime('dd',now))+1;
//  datafin.Date := now;
  qEmps.Open;
  if qEmps.RecordCount > 0 then begin
    lkpEmp.KeyValue := qEmps.FieldByName('EMPRES_ID').Value;
  end;
end;

procedure TFExportacaoConfabOticas.btnExportarClick(Sender: TObject);
var S, Valor,dec : String;
    SL, SL2 : TStringList;
    Erro : Boolean;
begin
  if (formatDateTime('mm',dataini.Date) <> formatDateTime('mm',datafin.Date)) then begin
    MsgInf('O per�odo deve apenas de um �nico m�s');
    dataini.SetFocus;
    Abort;
  end;
  if lkpEmp.KeyValue < 0 then begin
    MsgInf('Selecione uma empresa para a exporta��o');
    lkpEmp.SetFocus;
    Abort;
  end;
  if not DirectoryExists(ExtractFilePath(edtCaminho.Text)) then begin
    MsgInf('Caminho n�o encontrado!');
    edtCaminho.SetFocus;
    Abort;
  end;
  if UpperCase(btnExportar.Caption) = '&CANCELAR' then begin
    Cancelar := True;
    Abort;
  end;
  if not fnDataValida(dataini.Text) then begin
    msgInf('Data Inicial inv�lida');
    dataini.SetFocus;
    Abort;
  end;
  if not fnDataValida(datafin.Text) then begin
    msgInf('Data Final inv�lida');
    datafin.SetFocus;
    Abort;
  end; 
  if dataini.Date > datafin.Date then begin
    msgInf('Data Inicial n�o deve ser maior que a data final');
    dataini.SetFocus;
    Abort;
  end;
  ProgressBar1.Position := 0;
  lblStatus.Caption := '';
  qCc.Close;
  case cbNotas.ItemIndex of
    1: qCc.SQL.Add('and coalesce(cc.ENTREG_NF,''S'')=''S''');
    2: qCc.SQL.Add('and coalesce(cc.ENTREG_NF,''N'')=''N''');
  end;
  qCc.Parameters[0].Value := lkpEmp.KeyValue;
  qCc.SQL.Add('and cc.data between ' + QuotedStr(DateToStr(dataini.Date)) + ' and ' + QuotedStr(DateToStr(datafin.Date)));
  qCc.SQL.Add('order by conv.titular');
  //ClipBoard.AsText := qCc.SQL.Text;

  lblStatus.Caption := 'Capturando informa��es...';
  lblStatus.Refresh;
  btnExportar.Enabled := False;
  HabDesabBtnProcesso(True);
  Application.ProcessMessages;
  
  qCc.Open;
  if qCc.RecordCount = 0 then begin
    msgInf('N�o foi encontrado nem 1 (um) registro!');
    Exit;
  end;
  SL := TStringList.Create;
  SL2 := TStringList.Create;
  S := '';
  Erro := False;
  lblStatus.Caption := 'Gerando cabe�alho';
  while not qCc.Eof do begin
    if qCcVALOR.Value > 0.00 then begin
      S := '';
      valor := '';
      dec := '';
      lblStatus.Caption := 'Exportando nota:'+ qCcNF.AsString;
      try
        if Cancelar then
          if MsgSimNao('Gostaria realmente de cancelar o processo?',False) then begin
            Abort;
        end;
        S := S+ fnCompletarCom(qCcMATRICULA.AsString,6) + ',''';
        S := S+ fnCompletarCom(qCcNOME.AsString,30,' ',True) + ''',';
        S := S+ fnCompletarCom(qCcNF.AsString,6) + ',';
        valor := fnSubstituiString(',','.',FormatFloat('#,##0.00',qCcTOTAL_COMPRA.Value));
        dec := Copy(valor,Length(valor)-2,3);
        valor := fnRemoveCaractersQueNaoSejam(Copy(valor,1,Length(valor)-3),crNUM) + dec;
        S := S+ fnCOmpletarCom(valor,12) + ',';
        S := S+ fnCompletarCom(qCcCODIGO_SERVICO.AsString,4) + ',';
        S := S+ '000045,';
        S := S+ fnCompletarCom(qCcDATAVENDA.AsString,10,' ',True) + ',';
        S := S+ FormatDateTime('mmyy',qCcDATAVENDA.AsDateTime)+'45,';
        S := S+ fnCompletarCom(qCcNOME_OTICA.AsString,50,' ',True);
        SL.Add(S);
        except on E:Exception do begin
          Erro := True;
          MsgErro('Erro: '+E.message);
        end;
      end;
    end;
    qCc.Next;
    ProgressBar1.Position := ((qCc.RecNo*100) div qCc.RecordCount) ;
    Application.ProcessMessages;
  end;
  if Cancelar then begin
    MsgErro('N�o foi poss�vel concluir a importa��o. O processo foi cancelado pelo usu�rio!');
    lblStatus.Caption := 'Importa��o n�o concluida. Processo cancelado pelo usu�rio!';
  end;
  if SL.Count > 0 then begin
    if UpperCase(ExtractFileExt(edtCaminho.Text)) <> '.txt' then begin
      SL.SaveToFile(edtCaminho.Text+'.txt');
      AbrirArquivo(ExtractFilePath(edtCaminho.Text),ExtractFileName(edtCaminho.Text)+'.txt',fmOpenRead);
    end else begin
      SL.SaveToFile(edtCaminho.Text);
      AbrirArquivo(ExtractFilePath(edtCaminho.Text),ExtractFileName(edtCaminho.Text),fmOpenRead);
    end;
  end;
  if not Erro then
    lblStatus.Caption := 'Exporta��o concluida com sucesso!'
  else
    lblStatus.Caption := 'Exporta��o concluida com alguns erros!';
  HabDesabBtnProcesso(False);
  FreeAndNil(SL);
  FreeAndNil(SL2);
  qCc.Close;
  qEmps.Close;
end;

procedure TFExportacaoConfabOticas.BitBtn1Click(Sender: TObject);
begin
  if sd.execute then
    edtCaminho.Text := sd.FileName;
end;

end.
