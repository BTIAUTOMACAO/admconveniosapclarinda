unit USenhaLibera;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, JvComponent, {JvTransparentButton,} jpeg, ExtCtrls, {RxGIF,}
  Buttons, DB, JvLabel, JvButton,
  JvExControls, pngimage, ADODB, JvTransparentButton{, acPNG, JvBlinkingLabel};

type
  TFSenhaLibera = class(TForm)
    ButDigitaSenha: TJvTransparentButton;
    ButContinuar: TJvTransparentButton;
    ButSenhaOn: TJvTransparentButton;
    Image1: TImage;
    LabelContinuar: TLabel;
    LabelMensagem: TLabel;
    LabelCnpj: TLabel;
    LabelSenha: TLabel;
    LabelVale: TJvLabel;
    LabelMensagem2: TLabel;
    Timer1: TTimer;
    AdoQry: TADOQuery;
    procedure ButDigitaSenhaClick(Sender: TObject);
    procedure ButContinuarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButSenhaOnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    RazaoSocial: String;
    procedure GravaSenha(Mes: String;Ano: String);
  public
    { Public declarations }
  end;

var
  FSenhaLibera: TFSenhaLibera;

implementation

uses DM, UWebPegaSenha, cartao_util, DateUtils;

{$R *.dfm}

procedure TFSenhaLibera.ButDigitaSenhaClick(Sender: TObject);
var data_valid : TDateTime;
  dia, mes, ano, senha : string;
begin
  Try
    senha:= '';
    senha := InputBox('Senha para Libera��o do Sistema', 'Digite a Senha:', '');
    if senha <> '' then
    begin
      dia := Copy(senha,1,Pos('-',senha)-1);
      dia := Crypt('D',dia,DMConexao.ConfigCOD_ADM_BIG.AsString);
      mes := Copy(senha,Pos('-',senha)+1,length(senha));
      mes := copy(mes,1,Pos('-',mes)-1);
      mes := Crypt('D',mes,DMConexao.ConfigCOD_ADM_BIG.AsString);
      ano := Copy(senha,Pos('-',senha)+1,length(senha));
      ano := Copy(ano,Pos('-',senha)+1,length(ano));
      ano := Crypt('D',ano,DMConexao.ConfigCOD_ADM_BIG.AsString);
      ano := '20'+ano;
      data_valid := EncodeDate(StrToInt(ano), StrToInt(mes), StrToInt(dia));
      if data_valid < Date then
      begin
        ShowMessage('Senha Inv�lida');
      end
      else
      begin
        DMConexao.Config.Edit;
        DMConexao.ConfigRODAR_ATE.AsString := senha;
        DMConexao.Config.Post;
        ModalResult := mrOk;
      end;
    end;
  except
    ShowMessage('Senha Inv�lida');
  end;
end;

procedure TFSenhaLibera.ButContinuarClick(Sender: TObject);
begin
  if LabelContinuar.Caption = '  &Cancelar' then
    ModalResult := mrCancel
  else
    ModalResult := mrOk;
end;

procedure TFSenhaLibera.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_escape then
  begin
    ModalResult := mrCancel;
  end;
end;

procedure TFSenhaLibera.GravaSenha(Mes: String;Ano: String);
var data_valid : TDateTime;
  dia, senha, xano, AdmId : string;
begin
  Try
    senha:= '';
    AdmId:= DMConexao.ExecuteScalar('select cod_adm_big from config');
    dia := DMConexao.ExecuteScalar('select vencedia from administradora');
    data_valid := EncodeDate(StrToInt('20'+ano), StrToInt(mes), StrToInt(dia));
    if data_valid < Date then
    begin
      ShowMessage('Senha Inv�lida');
    end
    else
    begin
      data_valid:= IncDay(data_valid,15);
      data_valid:= GetProxDiaUtil(data_valid);
      DMConexao.Config.Open;
      DMConexao.Config.Edit;
      senha := Crypt('E',IntToStr(GetDia(data_valid)),AdmId);
      senha := senha + '-' + Crypt('E',IntToStr(GetMes(data_valid)),AdmId);
      xano:= IntToStr(GetAno(data_valid));
      xano:= copy(xano,3,2);
      senha := senha + '-' + Crypt('E',xano,AdmId);
      DMConexao.ConfigRODAR_ATE.AsString := senha;
      DMConexao.Config.Post;
      ModalResult := mrOk;
    end;
  except
    ShowMessage('Senha Inv�lida');
  end;
end;

procedure TFSenhaLibera.ButSenhaOnClick(Sender: TObject);
var
   CNPJ, versaonfe: String;
   Retorno: String;
begin
  versaonfe := '';
  {$IFDEF NFE}
  versaonfe := 'NFE '; //Para avisar o pedesenha para verificar se o cliente possui autorizacao para usar o vendasnfe.
  {$ENDIF}

  CNPJ := SoNumero(trim(DMConexao.AdoQry.FieldByName('CNPJ').AsString));

  if CNPJ = '' then
  begin
    MsgErro('Digite seu CNPJ no cadastro da Administradora.');
    Exit;
  end;

  try
    Retorno := GetIWebSenha.PegaSenha(CNPJ, Copy(versaonfe + RazaoSocial, 1, 50), GetIp, '2.0'); // GetBuildInfopega senha pela internet
  except
    on e : exception do
    begin
      MsgErro('Falha ao conectar-se ao servidor!' + #13 + 'Erro: ' + e.Message);
      Exit;
    end;
  end;

  if ((Trim(Copy(Retorno, 1, 5)) <> 'True') and (Trim(Copy(Retorno, 1, 5)) <> 'False'))
  or ((Trim(Copy(Retorno, 6, 5)) <> 'True') and (Trim(Copy(Retorno, 6, 5)) <> 'False')) then
  begin
    MsgErro(Retorno);
    Exit;
  end
  else
  begin
    if Trim(Copy(Retorno, 1, 5)) = 'False' then
    begin
      MsgErro('Senha n�o liberada.'+#13+
              'Verifique sua situa��o financeira com a Big Automa��o.'+#13+
              'BIG Automa��o - Tel.: (0xx17) 3344-3717.');
      Exit;
    end
    else
    begin
      GravaSenha(Copy(retorno, 11, 2), Copy(retorno, 13, 2));
    end;
  end;
end;

procedure TFSenhaLibera.FormCreate(Sender: TObject);
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Text:= ' select cnpj, razao from administradora ';
  DMConexao.AdoQry.Open;
  DMConexao.AdoQry.First;
  if DMConexao.AdoQry.IsEmpty then
  begin
    raise Exception.Create('Defina a Adminstradora!');
    Abort;
  end;
  RazaoSocial:= DMConexao.AdoQry.FieldByName('RAZAO').AsString;
  LabelCnpj.Caption:= DMConexao.AdoQry.FieldByName('CNPJ').AsString;
  LabelSenha.Caption:= DMConexao.ExecuteScalar('select rodar_ate from config');
end;

procedure TFSenhaLibera.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DMConexao.AdoQry.Close;
end;

procedure TFSenhaLibera.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:= False;
  ButSenhaOnClick(nil);
end;

procedure TFSenhaLibera.FormShow(Sender: TObject);
begin
  Timer1.Enabled:= True;
end;

end.
