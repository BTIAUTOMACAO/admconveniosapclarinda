object FImpCobreBem: TFImpCobreBem
  Left = 233
  Top = 181
  Width = 404
  Height = 436
  Caption = 'Impress'#227'o de Boleto CobreBem'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 16
    Width = 379
    Height = 105
    Caption = 'Dados do Cedente'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 63
      Height = 13
      Caption = 'Raz'#227'o Social'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 248
      Top = 16
      Width = 21
      Height = 13
      Caption = 'Cnpj'
      FocusControl = DBEdit2
    end
    object Label12: TLabel
      Left = 16
      Top = 40
      Width = 90
      Height = 13
      Caption = 'Selecione o Banco'
    end
    object Label3: TLabel
      Left = 16
      Top = 56
      Width = 90
      Height = 13
      Caption = 'Selecione o Banco'
    end
    object Label4: TLabel
      Left = 192
      Top = 56
      Width = 130
      Height = 13
      Caption = 'Selecione a Conta Corrente'
    end
    object DBEdit1: TDBEdit
      Left = 16
      Top = 32
      Width = 225
      Height = 21
      DataField = 'RAZAO'
      DataSource = DSAdm
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 248
      Top = 32
      Width = 121
      Height = 21
      DataField = 'CNPJ'
      DataSource = DSAdm
      TabOrder = 1
    end
    object Banco: TJvDBLookupCombo
      Left = 16
      Top = 72
      Width = 169
      Height = 21
      LookupField = 'CODIGO'
      LookupDisplay = 'BANCO'
      LookupDisplayIndex = 1
      LookupSource = DSBanco
      TabOrder = 2
      OnExit = BancoExit
    end
    object Conta: TJvDBLookupCombo
      Left = 192
      Top = 72
      Width = 177
      Height = 21
      LookupField = 'CONTA_ID'
      LookupDisplay = 'DESCRICAO'
      LookupDisplayIndex = 1
      LookupSource = DSConta
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 136
    Width = 379
    Height = 105
    Caption = 'Dados do Sacado'
    TabOrder = 1
    object Label5: TLabel
      Left = 16
      Top = 16
      Width = 28
      Height = 13
      Caption = 'Nome'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Left = 224
      Top = 16
      Width = 55
      Height = 13
      Caption = 'Documento'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Left = 16
      Top = 56
      Width = 46
      Height = 13
      Caption = 'Endere'#231'o'
      FocusControl = DBEdit7
    end
    object DBEdit5: TDBEdit
      Left = 16
      Top = 32
      Width = 200
      Height = 21
      DataField = 'NOME'
      DataSource = DSSacado
      TabOrder = 0
    end
    object DBEdit6: TDBEdit
      Left = 224
      Top = 32
      Width = 145
      Height = 21
      DataField = 'DOC'
      DataSource = DSSacado
      TabOrder = 1
    end
    object DBEdit7: TDBEdit
      Left = 16
      Top = 72
      Width = 353
      Height = 21
      DataField = 'ENDERECO'
      DataSource = DSSacado
      TabOrder = 2
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 256
    Width = 379
    Height = 105
    Caption = 'Dados da Fatura'
    TabOrder = 2
    object Label8: TLabel
      Left = 16
      Top = 16
      Width = 45
      Height = 13
      Caption = 'N'#186' Fatura'
      FocusControl = DBEdit8
    end
    object Label9: TLabel
      Left = 192
      Top = 16
      Width = 56
      Height = 13
      Caption = 'Data Fatura'
      FocusControl = DBEdit9
    end
    object Label10: TLabel
      Left = 16
      Top = 56
      Width = 56
      Height = 13
      Caption = 'Vencimento'
      FocusControl = DBEdit10
    end
    object Label11: TLabel
      Left = 192
      Top = 56
      Width = 24
      Height = 13
      Caption = 'Valor'
      FocusControl = DBEdit11
    end
    object DBEdit8: TDBEdit
      Left = 16
      Top = 32
      Width = 171
      Height = 21
      DataField = 'FATURA_ID'
      DataSource = DSFatura
      TabOrder = 0
    end
    object DBEdit9: TDBEdit
      Left = 192
      Top = 32
      Width = 176
      Height = 21
      DataField = 'DATA_FATURA'
      DataSource = DSFatura
      TabOrder = 1
    end
    object DBEdit10: TDBEdit
      Left = 16
      Top = 72
      Width = 171
      Height = 21
      DataField = 'DATA_VENCIMENTO'
      DataSource = DSFatura
      TabOrder = 2
    end
    object DBEdit11: TDBEdit
      Left = 192
      Top = 72
      Width = 177
      Height = 21
      DataField = 'VALOR'
      DataSource = DSFatura
      TabOrder = 3
    end
  end
  object btnGerar: TButton
    Left = 80
    Top = 373
    Width = 113
    Height = 25
    Caption = 'Gerar'
    TabOrder = 3
    OnClick = btnGerarClick
  end
  object btnCancelar: TButton
    Left = 200
    Top = 373
    Width = 113
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 4
  end
  object DSSacado: TDataSource
    DataSet = QSacado
    Left = 112
    Top = 232
  end
  object DSAdm: TDataSource
    DataSet = DMConexao.Adm
    Left = 264
    Top = 200
  end
  object DSFatura: TDataSource
    DataSet = QFatura
    Left = 144
    Top = 232
  end
  object DSBanco: TDataSource
    DataSet = QBanco
    Left = 184
    Top = 232
  end
  object DSConta: TDataSource
    DataSet = QConta
    Left = 224
    Top = 232
  end
  object QSacado: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select emp.nome as nome, emp.cgc as doc,'
      'emp.endereco, emp.bairro, emp.cidade, emp.estado, emp.cep '
      'from empresas emp')
    Left = 112
    Top = 200
    object QSacadonome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QSacadodoc: TStringField
      FieldName = 'doc'
      Size = 18
    end
    object QSacadoendereco: TStringField
      FieldName = 'endereco'
      Size = 60
    end
    object QSacadobairro: TStringField
      FieldName = 'bairro'
      Size = 30
    end
    object QSacadocidade: TStringField
      FieldName = 'cidade'
      Size = 30
    end
    object QSacadoestado: TStringField
      FieldName = 'estado'
      FixedChar = True
      Size = 2
    end
    object QSacadocep: TStringField
      FieldName = 'cep'
      Size = 9
    end
  end
  object QFatura: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'fatura'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from fatura'
      'where fatura_id = :fatura')
    Left = 144
    Top = 200
    object QFaturaFATURA_ID: TIntegerField
      FieldName = 'FATURA_ID'
    end
    object QFaturaID: TIntegerField
      FieldName = 'ID'
    end
    object QFaturaDATA_FATURA: TDateTimeField
      FieldName = 'DATA_FATURA'
    end
    object QFaturaDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
    end
    object QFaturaVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QFaturaFECHAMENTO: TDateTimeField
      FieldName = 'FECHAMENTO'
    end
    object QFaturaBAIXADA: TStringField
      FieldName = 'BAIXADA'
      FixedChar = True
      Size = 1
    end
    object QFaturaTIPO: TStringField
      FieldName = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object QFaturaSO_CONFIRMADAS: TStringField
      FieldName = 'SO_CONFIRMADAS'
      FixedChar = True
      Size = 1
    end
    object QFaturaOPERADOR: TStringField
      FieldName = 'OPERADOR'
    end
    object QFaturaDESC_EMPRESA: TFloatField
      FieldName = 'DESC_EMPRESA'
    end
    object QFaturaPRE_BAIXA: TStringField
      FieldName = 'PRE_BAIXA'
      FixedChar = True
      Size = 1
    end
    object QFaturaAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QFaturaHORA_FATURA: TStringField
      FieldName = 'HORA_FATURA'
      FixedChar = True
      Size = 10
    end
    object QFaturaDATA_BAIXA: TDateTimeField
      FieldName = 'DATA_BAIXA'
    end
    object QFaturaOBS: TStringField
      FieldName = 'OBS'
      Size = 60
    end
    object QFaturaDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QFaturaDATA_PRE_BAIXA: TDateTimeField
      FieldName = 'DATA_PRE_BAIXA'
    end
  end
  object QBanco: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'select codigo, banco from bancos'
      'where coalesce(apagado,'#39'N'#39') <> '#39'S'#39
      'order by banco')
    Left = 184
    Top = 200
    object QBancocodigo: TIntegerField
      FieldName = 'codigo'
    end
    object QBancobanco: TStringField
      FieldName = 'banco'
      Size = 45
    end
  end
  object QConta: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'banco'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select conta_id, agencia, contacorrente, '
      'coalesce(taxa_debito,0) as taxa_debito, '
      '(coalesce(seq_remessa,0)+1) as seq_remessa,'
      'CONCAT('#39'Ag. '#39',agencia,'#39' - C/C '#39',contacorrente) as descricao'
      'from contas_bancarias'
      'where coalesce(apagado,'#39'N'#39') <> '#39'S'#39
      'and cod_banco = :banco')
    Left = 224
    Top = 200
    object QContaconta_id: TIntegerField
      FieldName = 'conta_id'
    end
    object QContaagencia: TStringField
      FieldName = 'agencia'
      Size = 10
    end
    object QContacontacorrente: TStringField
      FieldName = 'contacorrente'
      Size = 15
    end
    object QContataxa_debito: TBCDField
      FieldName = 'taxa_debito'
      ReadOnly = True
      Precision = 15
      Size = 2
    end
    object QContaseq_remessa: TIntegerField
      FieldName = 'seq_remessa'
      ReadOnly = True
    end
    object QContadescricao: TStringField
      FieldName = 'descricao'
      ReadOnly = True
      Size = 36
    end
  end
end
