unit UAltContaCor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, JvToolEdit, {JvDBCtrl,} Mask, ExtCtrls, Buttons,
  JvExMask, JvDBControls;

type
  TFAltContaCor = class(TForm)
    Panel1: TPanel;
    ButSalvar: TBitBtn;
    ButCancel: TBitBtn;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    JvDBDateEdit1: TJvDBDateEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBComboBox1: TDBComboBox;
    Label11: TLabel;
    DBComboEntregue: TDBComboBox;
    Label12: TLabel;
    Bevel1: TBevel;
    Label13: TLabel;
    JvDBDateEdit2: TJvDBDateEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ButSalvarClick(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses UCadConv, DM;

var oldCredito,oldDebito : Double;
{$R *.dfm}

procedure TFAltContaCor.FormKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then begin
   Key := #0;
   Perform(WM_NEXTDLGCTL,0,0);
end;
end;

procedure TFAltContaCor.ButSalvarClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFAltContaCor.ButCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFAltContaCor.FormCreate(Sender: TObject);
begin
  DMConexao.Config.Open;
  if DMConexao.ConfigFILTRO_ENTREG_NF_EXT.AsString = 'N' then
  begin
    DBComboEntregue.Enabled  := False;
    DBComboEntregue.ReadOnly := True;
  end;
  DMConexao.Config.Close;
end;

end.
