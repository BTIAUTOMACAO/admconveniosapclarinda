unit UBuscaProdutos2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, {ZAbstractRODataset, ZAbstractDataset, ZDataset,} Grids,
  DBGrids, {JvDBCtrl,} StdCtrls, Buttons, {JvLookup,} ExtCtrls, {JvDBComb,} Mask,
  ToolEdit, CurrEdit, JvExDBGrids, JvDBGrid, JvExStdCtrls, JvCombobox,
  JvDBCombobox, JvExControls, JvDBLookup, JvEdit, JvValidateEdit, ADODB,
  JvMemoryDataset;

type
  TFBuscaProdutos2 = class(TForm)
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    edProdID: TEdit;
    edDescricao: TEdit;
    edtPAtivo: TJvDBLookupCombo;
    btnBuscar: TBitBtn;
    edBarras: TEdit;
    grdLista: TJvDBGrid;
    Panel5: TPanel;
    btnMarcDesm: TBitBtn;
    btnMarcaTodos: TBitBtn;
    btnDesmarc: TBitBtn;
    btnRepassar: TBitBtn;
    dsListProd: TDataSource;
    dsPAtivo: TDataSource;
    btnClasse: TBitBtn;
    btnFamilia: TBitBtn;
    btnLab: TBitBtn;
    btnSubClasse: TBitBtn;
    edtTpLista: TJvDBComboBox;
    Label1: TLabel;
    Label2: TLabel;
    edtTpPreco: TJvDBComboBox;
    GroupBox1: TGroupBox;
    Entre: TLabel;
    //vlrIni: TCurrencyEdit;
    //vlrFin: TCurrencyEdit;
    SpeedButton1: TSpeedButton;
    //edtDesconto: TCurrencyEdit;
    Label3: TLabel;
    vlrIni: TJvValidateEdit;
    vlrFin: TJvValidateEdit;
    edtDesconto: TJvValidateEdit;
    qListProd: TADOQuery;
    qListProdPROD_ID: TIntegerField;
    qListProdDESCRICAO: TStringField;
    qListProdBARRAS: TStringField;
    qListProdLIBCIP: TStringField;
    qListProdPRECO_VND: TFloatField;
    qListProdLISTA: TStringField;
    qListProdLAB_ID: TIntegerField;
    qListProdLABOR: TStringField;
    qListProdPAT_ID: TIntegerField;
    qListProdPATIVO: TStringField;
    qListProdCLAS_ID: TIntegerField;
    qListProdCLASSE: TStringField;
    qListProdGRUPO_PROD_ID: TIntegerField;
    qListProdGRUPO: TStringField;
    qListProdREGMS: TStringField;
    qListProdGENERICO: TStringField;
    qListProdDESCONTO: TFloatField;
    qPAtivo: TADOQuery;
    qPAtivoPAT_ID: TIntegerField;
    qPAtivoPATIVO: TStringField;
    MListProd: TJvMemoryData;
    MListProdPROD_ID: TIntegerField;
    MListProdDESCRICAO: TStringField;
    MListProdBARRAS: TStringField;
    MListProdLIBCIP: TStringField;
    MListProdPRECO_VND: TFloatField;
    MListProdLISTA: TStringField;
    MListProdLAB_ID: TIntegerField;
    MListProdLABOR: TStringField;
    MListProdPAT_ID: TIntegerField;
    MListProdPATIVO: TStringField;
    MListProdCLAS_ID: TIntegerField;
    MListProdCLASSE: TStringField;
    MListProdGRUPO_PROD_ID: TIntegerField;
    MListProdGRUPO: TStringField;
    MListProdREGMS: TStringField;
    MListProdGENERICO: TStringField;
    MListProdDESCONTO: TFloatField;
    MListProdMARCADO: TBooleanField;
    procedure btnMarcDesmClick(Sender: TObject);
    procedure btnMarcaTodosClick(Sender: TObject);
    procedure btnDesmarcClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edProdIDKeyPress(Sender: TObject; var Key: Char);
    procedure edBarrasKeyPress(Sender: TObject; var Key: Char);
    procedure edDescricaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtPAtivoKeyPress(Sender: TObject; var Key: Char);
    procedure btnRepassarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdListaDblClick(Sender: TObject);
    procedure grdListaKeyPress(Sender: TObject; var Key: Char);
    procedure btnClasseClick(Sender: TObject);
    procedure btnFamiliaClick(Sender: TObject);
    procedure btnLabClick(Sender: TObject);
    procedure btnSubClasseClick(Sender: TObject);
  private
    { Private declarations }
    ProgramaID: Integer;
    EmpresaID: Integer;
    ChamadoPor, listClasses, listSubClasses, listGrupos, listLab: String;
    prod_sel : String;
    function GetTela: String;
    function GetEmpresa: Integer;
    function GetPrograma: Integer;
    procedure ListProdSel;
  public
    { Public declarations }
    procedure SetTela(Tela: String);
    procedure SetEmpresa(Empr: Integer);
    procedure SetPrograma(prog: Integer);

  end;

var
  FBuscaProdutos2: TFBuscaProdutos2;

implementation

uses DM, cartao_util, UMenu, UDBGridHelper, uSeleciona;

{$R *.dfm}

procedure TFBuscaProdutos2.btnMarcDesmClick(Sender: TObject);
begin
if MListProd.IsEmpty then Exit;
   MListProd.Edit;
   MListProdMarcado.AsBoolean := not MListProdMarcado.AsBoolean;
   MListProd.Post;
   ListProdSel;
end;

procedure TFBuscaProdutos2.ListProdSel;
var marca : TBookmark;
begin
  prod_sel := EmptyStr;
  MListProd.DisableControls;
  marca := MListProd.GetBookmark;
  MListProd.First;
  while not MListProd.Eof do begin
    if MListProdMARCADO.AsBoolean  = true then prod_sel := prod_sel+','+MListProdPROD_ID.AsString;
    MListProd.Next;
  end;
  prod_sel := Copy(prod_sel,2,Length(prod_sel));
  MListProd.GotoBookmark(marca);
  MListProd.FreeBookmark(marca);
  MListProd.EnableControls;
end;

procedure TFBuscaProdutos2.btnMarcaTodosClick(Sender: TObject);
var marca : TBookmark;
begin
  if MListProd.IsEmpty then Exit;
  MListProd.DisableControls;
  marca := MListProd.GetBookmark;
  MListProd.First;
  while not MListProd.eof do begin
    MListProd.Edit;
    MListProdMarcado.AsBoolean := true;
    MListProd.Post;
    MListProd.Next;
  end;
  MListProd.GotoBookmark(marca);
  MListProd.FreeBookmark(marca);
  MListProd.EnableControls;
  ListProdSel;
end;

procedure TFBuscaProdutos2.btnDesmarcClick(Sender: TObject);
var marca : TBookmark;
begin
  if MListProd.IsEmpty then Exit;
  MListProd.DisableControls;
  marca := MListProd.GetBookmark;
  MListProd.First;
  while not MListProd.eof do begin
    MListProd.Edit;
    MListProdMarcado.AsBoolean := false;
    MListProd.Post;
    MListProd.Next;
  end;
  MListProd.GotoBookmark(marca);
  MListProd.FreeBookmark(marca);
  MListProd.EnableControls;
  ListProdSel;
end;

procedure TFBuscaProdutos2.btnBuscarClick(Sender: TObject);
begin
  screen.Cursor := crHourGlass;
  qListProd.Close;
  qListProd.SQL.Clear;

  qListProd.SQL.Add(' SELECT ');
  qListProd.SQL.Add('   p.prod_id, ');
  qListProd.SQL.Add('   p.descricao, ');
  qListProd.SQL.Add('   barras.barras, ');
  qListProd.SQL.Add('   p.libcip, ');
  qListProd.SQL.Add('   coalesce(p.preco_vnd,0.00) preco_vnd, ');
  qListProd.SQL.Add('   '+FormatDimIB(edtDesconto.Value)+' as desconto, ');
  qListProd.SQL.Add('   p.lista, ');
  qListProd.SQL.Add('   p.lab_id, ');
  qListProd.SQL.Add('   coalesce(laboratorios.nomelab,''NAO RELACIONADO'') labor, ');
  qListProd.SQL.Add('   p.pat_id, ');
  qListProd.SQL.Add('   coalesce(pativo.pativo,''NAO RELACIONADO'') pativo, ');
  qListProd.SQL.Add('   p.clas_id, ');
  qListProd.SQL.Add('   coalesce(classe.classe,''NAO RELACIONADO'') classe, ');
  qListProd.SQL.Add('   p.grupo_prod_id, ');
  qListProd.SQL.Add('   coalesce(grupo_prod.descricao,''NAO RELACIONADO'') grupo, ');
  qListProd.SQL.Add('   p.regms, ');
  qListProd.SQL.Add('   p.generico, ');
  qListProd.SQL.Add('   ''N'' marcado ');
  qListProd.SQL.Add(' FROM produtos p ');
  qListProd.SQL.Add('   JOIN barras ON p.prod_id = barras.prod_id ');
  qListProd.SQL.Add('   LEFT JOIN classe ON p.clas_id = classe.clas_id ');
  qListProd.SQL.Add('   LEFT JOIN pativo ON p.pat_id = pativo.pat_id ');
  qListProd.SQL.Add('   LEFT JOIN grupo_prod ON p.grupo_prod_id = grupo_prod.grupo_prod_id ');
  qListProd.SQL.Add('   LEFT JOIN laboratorios ON p.lab_id = laboratorios.lab_id ');
  qListProd.SQL.Add(' WHERE p.apagado <> ''S'' ');

  if listClasses <> '' then
    qListProd.Sql.Add(' AND p.clas_id IN ('+listClasses+') ');
  if listSubClasses <> '' then
    qListProd.Sql.Add(' AND p.sclas_id IN ('+listSubClasses+') ');
  if listGrupos <> '' then
    qListProd.Sql.Add(' AND p.grupo_prod_id IN ('+listGrupos+') ');
  if listLab <> '' then
    qListProd.Sql.Add(' AND p.lab_id IN ('+listLab+') ');
  if edtPAtivo.KeyValue > 0 then
    qListProd.Sql.Add(' AND p.pat_id = '+edtPAtivo.KeyValue+' ');

  if vlrFin.Value > 0 then
    qListProd.Sql.Add(' AND p.preco_vnd between '+FormatDimIB(vlrIni.Value)+' and '+FormatDimIB(vlrFin.Value));

  if not (edtTpLista.Text = '') then
    qListProd.Sql.Add(' AND p.lista = '''+edtTpLista.Values[edtTpLista.ItemIndex]+''' ');
  if not (edtTpPreco.Text = '') then
    qListProd.Sql.Add(' AND p.libcip = '''+edtTpPreco.Values[edtTpPreco.ItemIndex]+''' ');

  if edProdID.Text <> '' then
    qListProd.SQL.Add(' and p.prod_id in ('+SoNumero(edProdID.Text)+') ');
  if edBarras.Text <> '' then
    qListProd.SQL.Add(' and barras.barras like ''%'+edBarras.Text+'%'' ');
  if edDescricao.Text <> '' then
    qListProd.SQL.Add(' and p.descricao like ''%'+edDescricao.Text+'%'' ');
  qListProd.SQL.Text;
  qListProd.Open;
  if qListProd.IsEmpty then
  begin
    MsgInf('Nenhum produto encontrado!');
    edProdID.SetFocus;
    screen.Cursor := crDefault;
    Exit;
  end
  else
    grdLista.SetFocus;
    MListProd.Open;
    MListProd.EmptyTable;
    MListProd.DisableControls;
    while not QListProd.Eof do begin
      MListProd.Append;
      MListProdPROD_ID.AsInteger  := qListProdPROD_ID.AsInteger;
      MListProdDESCRICAO.AsString := qListProdDESCRICAO.AsString;
      MListProdBARRAS.AsString := qListProdBARRAS.AsString;
      MListProdLIBCIP.AsString := qListProdLIBCIP.AsString;
      MListProdPRECO_VND.AsFloat := qListProdPRECO_VND.AsFloat;
      MListProdLISTA.AsString := qListProdLISTA.AsString;
      MListProdLAB_ID.AsInteger := qListProdLAB_ID.AsInteger;
      MListProdLABOR.AsString := qListProdLABOR.AsString;
      MListProdPAT_ID.AsInteger := qListProdPAT_ID.AsInteger;
      MListProdPATIVO.AsString := qListProdPATIVO.AsString;
      MListProdCLAS_ID.AsInteger := qListProdCLAS_ID.AsInteger;
      MListProdCLASSE.AsString := qListProdCLASSE.AsString;
      MListProdGRUPO_PROD_ID.AsInteger := qListProdGRUPO_PROD_ID.AsInteger;
      MListProdGRUPO.AsString := qListProdGRUPO.AsString;
      MListProdREGMS.AsString := qListProdREGMS.AsString;
      MListProdGENERICO.AsString := qListProdGENERICO.AsString;
      MListProdDESCONTO.AsFloat := MListProdDESCONTO.AsFloat;
      MListProdMARCADO.AsBoolean := False;
      MListProd.Post;
      QListProd.Next;
    end;
    MListProd.First;
    MListProd.EnableControls;
    prod_sel := EmptyStr;


  edProdID.Text:= '';
  edBarras.Text:= '';
  edDescricao.Text:= '';
  edtTpLista.ClearSelection;
  edtTpPreco.ClearSelection;
  edtPAtivo.KeyValue:= 0;
  vlrIni.Value:= 0;
  vlrFin.Value:= 0;
  screen.Cursor := crDefault;
end;

procedure TFBuscaProdutos2.FormDblClick(Sender: TObject);
begin
  btnMarcDesm.Click;
end;

procedure TFBuscaProdutos2.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (key = #13) then btnMarcDesm.Click;
end;

procedure TFBuscaProdutos2.edProdIDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then btnBuscar.Click;
end;

procedure TFBuscaProdutos2.edBarrasKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then
    btnBuscar.Click;
end;

procedure TFBuscaProdutos2.edDescricaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then btnBuscar.Click;
end;

procedure TFBuscaProdutos2.edtPAtivoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ((key = #13) and (edtPAtivo.KeyValue > 0)) then btnBuscar.Click;
end;

procedure TFBuscaProdutos2.btnRepassarClick(Sender: TObject);
var strSql: String;
begin
  inherited;
  if DMConexao.ContaMarcados(MListProd) = 0 then
  begin
    MsgErro('N�o existem produtos marcados!');
    Exit;
  end;
  MListProd.First;
  while not MListProd.Eof do
  begin
    if MListProd.FieldByName('MARCADO').AsBoolean = true then
    begin
      if GetTela = 'FGruposValeDesconto' then
      begin
        strSql:= ' select prod_id from vale_produtos where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString;
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(strsql);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          strSql:= 'insert into vale_produtos (prod_id, vale_perc) values ' +
                   '( ' + MListProd.FieldByName('PROD_ID').AsString + ', 0.00)';
          DMConexao.ExecuteSql(strSql);
          DMConexao.GravaLog('FGruposValeDesconto','Produto ID','',MListProd.FieldByName('PROD_ID').AsString,Operador.Nome,
                       'Inclus�o',MListProd.FieldByName('PROD_ID').AsString,'');
        end;
      end;
      if GetTela = 'FCadProgramas' then
      begin
        strSql:= ' select prod_id from prog_prod where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString +
                 ' and prog_id = ' + IntToStr(GetPrograma);
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(strsql);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          strSql:= 'insert into prog_prod (prod_id, prog_id, prc_unit, prc_unit_18, perc_desc, qtd_max, obrig_receita, ativo) values ' +
                   '( ' + MListProd.FieldByName('PROD_ID').AsString + ', ' + IntToStr(GetPrograma) +
                   ', ' + FormatDimIB(MListProd.FieldByName('PRECO_VND').AsCurrency) + ', ' + FormatDimIB(MListProd.FieldByName('PRECO_VND').AsCurrency) + 
                   ', '+FormatDimIB(MListProd.FieldByName('DESCONTO').AsCurrency)+', 0, ''N'',''S'')';
          DMConexao.ExecuteSql(strSql);
          DMConexao.GravaLog('FCadProgramas','Produto ID','',MListProd.FieldByName('PROD_ID').AsString,Operador.Nome,'Inclus�o',
                       IntToStr(GetPrograma),'');
        end
        else
        begin
          DMConexao.ExecuteSql('update prog_prod set ativo = ''S'' where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString +' and prog_id = ' + IntToStr(GetPrograma));
        end;
      end;
      if GetTela = 'FCadEmp' then
      begin
        strSql:= ' select prod_id from prod_bloq where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString +
                 ' and empres_id = ' + IntToStr(GetEmpresa);
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(strsql);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          strSql:= 'insert into prod_bloq (prod_id, empres_id) values ' +
                   '( ' + MListProd.FieldByName('PROD_ID').AsString + ', ' + IntToStr(GetEmpresa) + ')';
          DMConexao.ExecuteSql(strSql);
          //DMConexao.GravaLog('FCadProgramas','Produto ID','',qListProd.FieldByName('PROD_ID').AsString,Operador.Nome,'Inclus�o','FCadPrograma',IntToStr(GetPrograma),'Programa ID');
        end
        else
        begin
          DMConexao.ExecuteSql('delete from prod_bloq where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString +' and empres_id = ' + IntToStr(GetEmpresa));
        end;
      end;
      if GetTela = 'FCadProgrBlackList' then
      begin
        strSql:= ' select prod_id from prog_blacklist where prod_id = ' + MListProd.FieldByName('PROD_ID').AsString +
                 ' and prog_id = ' + IntToStr(GetPrograma);
        DMConexao.AdoQry.SQL.Clear;
        DMConexao.AdoQry.SQL.Add(strsql);
        DMConexao.AdoQry.Open;
        if DMConexao.AdoQry.IsEmpty then
        begin
          strSql:= 'insert into prog_blacklist (prod_id, prog_id) values ' +
                   '( ' + MListProd.FieldByName('PROD_ID').AsString + ', ' + IntToStr(GetPrograma) + ')';
          DMConexao.ExecuteSql(strSql);
          //DMConexao.GravaLog('FCadProgramas','Produto ID','',qListProd.FieldByName('PROD_ID').AsString,Operador.Nome,'Inclus�o','FCadPrograma',IntToStr(GetPrograma),'Programa ID');
        end
        else
        begin
          //DMConexao.ExecuteSql('delete from prod_bloq where prod_id = ' + qListProd.FieldByName('PROD_ID').AsString +' and empres_id = ' + IntToStr(GetEmpresa));
        end;
      end;
    end;
    MListProd.Next;
  end;
end;

function TFBuscaProdutos2.GetEmpresa: Integer;
begin
  Result:= EmpresaID;
end;

procedure TFBuscaProdutos2.SetEmpresa(Empr: Integer);
begin
  EmpresaID:= Empr;
end;

function TFBuscaProdutos2.GetPrograma: Integer;
begin
  Result:= ProgramaID;
end;

procedure TFBuscaProdutos2.SetPrograma(prog: Integer);
begin
  ProgramaID:= prog;
end;

procedure TFBuscaProdutos2.FormCreate(Sender: TObject);
begin
  qPAtivo.Open;
  TDBGridHelper.DBGridHelperTratrForm(Self,DMConexao.AdoQry,Operador.ID);
end;

procedure TFBuscaProdutos2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if qListProd.Active then qListProd.Close;
  qPAtivo.Close;
end;

procedure TFBuscaProdutos2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key in [vk_return,vk_escape]) and (not(ActiveControl is TDBGrid)) then
  begin
    if (ActiveControl is TJvDBLookupCombo) then
    begin
      if TJvDBLookupCombo(ActiveControl).ListVisible then
      begin
        if key = vk_return then
          TJvDBLookupCombo(ActiveControl).CloseUp(true)
        else
          TJvDBLookupCombo(ActiveControl).CloseUp(false);
        exit;
      end;
    end
    else if (ActiveControl is TCustomComboBox) then
    begin
      if TCustomComboBox(ActiveControl).DroppedDown then
      begin
        TCustomComboBox(ActiveControl).DroppedDown := False;
        exit;
      end;
    end;
    if key = vk_return then
      SelectNext(ActiveControl,true,true);
  end;
end;

procedure TFBuscaProdutos2.grdListaDblClick(Sender: TObject);
begin
  btnMarcDesm.Click;
end;

procedure TFBuscaProdutos2.grdListaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key = #13) then btnMarcDesm.Click;
end;

function TFBuscaProdutos2.GetTela: String;
begin
  Result:= ChamadoPor;
end;

procedure TFBuscaProdutos2.SetTela(Tela: String);
begin
  ChamadoPor:= Tela;
end;

procedure TFBuscaProdutos2.btnClasseClick(Sender: TObject);
begin
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Classes');
  fSeleciona.setTabela('CLASSE');
  fSeleciona.setCodigo('CLAS_ID');
  fSeleciona.setNome('CLASSE');
  fSeleciona.setIDs(listClasses);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listClasses:= fSeleciona.getIDs;
    if listClasses <> '' then
      btnClasse.Font.Style:= [fsBold]
    else
      btnClasse.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFBuscaProdutos2.btnFamiliaClick(Sender: TObject);
begin
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Grupo de Produtos');
  fSeleciona.setTabela('FAMILIAS');
  fSeleciona.setCodigo('FAM_ID');
  fSeleciona.setNome('FAMILIA');
  fSeleciona.setIDs(listGrupos);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listGrupos:= fSeleciona.getIDs;
    if listGrupos <> '' then
      btnFamilia.Font.Style:= [fsBold]
    else
      btnFamilia.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFBuscaProdutos2.btnLabClick(Sender: TObject);
begin
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de Laborat�rios');
  fSeleciona.setTabela('LABORATORIOS');
  fSeleciona.setCodigo('LAB_ID');
  fSeleciona.setNome('NOMELAB');
  fSeleciona.setIDs(listLab);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listLab:= fSeleciona.getIDs;
    if listLab <> '' then
      btnLab.Font.Style:= [fsBold]
    else
      btnLab.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

procedure TFBuscaProdutos2.btnSubClasseClick(Sender: TObject);
begin
  fSeleciona:= TfSeleciona.Create(Self);
  fSeleciona.setTitulo('Sele��o de SubClasses');
  fSeleciona.setTabela('SUBCLASSE');
  fSeleciona.setCodigo('SCLAS_ID');
  fSeleciona.setNome('SUBCLASSE');
  fSeleciona.setIDs(listSubClasses);
  fSeleciona.ShowModal;
  if fSeleciona.ModalResult = mrOk then
  begin
    listSubClasses:= fSeleciona.getIDs;
    if listSubClasses <> '' then
      btnClasse.Font.Style:= [fsBold]
    else
      btnClasse.Font.Style:= [];
  end;
  fSeleciona.Free;
end;

end.
