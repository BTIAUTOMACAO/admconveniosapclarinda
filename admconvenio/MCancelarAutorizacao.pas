
{****************************************************************************************************************}
{                                                                                                                }
{                                                XML Data Binding                                                }
{                                                                                                                }
{         Generated on: 16/06/2010 08:43:30                                                                      }
{       Generated from: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MCancelarAutorizacao.xsd   }
{   Settings stored in: D:\Fontes\BigAdmConvenios\BigAdmConvenio_Beta\Schemas\Schemas\MCancelarAutorizacao.xdb   }
{                                                                                                                }
{****************************************************************************************************************}

unit MCancelarAutorizacao;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTCancelarAutorizacaoParam = interface;
  IXMLTCredenciado = interface;
  IXMLTCancelarAutorizacaoRet = interface;

{ IXMLTCancelarAutorizacaoParam }

  IXMLTCancelarAutorizacaoParam = interface(IXMLNode)
    ['{4FE51077-8052-4CAB-8A5A-638E60046CAF}']
    { Property Accessors }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_AUTORIZACAO: LongWord;
    function Get_VALOR: LongWord;
    function Get_OPERADOR: WideString;
    procedure Set_AUTORIZACAO(Value: LongWord);
    procedure Set_VALOR(Value: LongWord);
    procedure Set_OPERADOR(Value: WideString);
    { Methods & Properties }
    property CREDENCIADO: IXMLTCredenciado read Get_CREDENCIADO;
    property AUTORIZACAO: LongWord read Get_AUTORIZACAO write Set_AUTORIZACAO;
    property VALOR: LongWord read Get_VALOR write Set_VALOR;
    property OPERADOR: WideString read Get_OPERADOR write Set_OPERADOR;
  end;

{ IXMLTCredenciado }

  IXMLTCredenciado = interface(IXMLNode)
    ['{61FE328B-5EA4-405E-81A4-F151CDF191A2}']
    { Property Accessors }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
    { Methods & Properties }
    property CODACESSO: LongWord read Get_CODACESSO write Set_CODACESSO;
    property SENHA: WideString read Get_SENHA write Set_SENHA;
  end;

{ IXMLTCancelarAutorizacaoRet }

  IXMLTCancelarAutorizacaoRet = interface(IXMLNode)
    ['{AA105259-6060-4575-940B-2BA5AC7CD62B}']
    { Property Accessors }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
    { Methods & Properties }
    property STATUS: LongWord read Get_STATUS write Set_STATUS;
    property MSG: WideString read Get_MSG write Set_MSG;
  end;

{ Forward Decls }

  TXMLTCancelarAutorizacaoParam = class;
  TXMLTCredenciado = class;
  TXMLTCancelarAutorizacaoRet = class;

{ TXMLTCancelarAutorizacaoParam }

  TXMLTCancelarAutorizacaoParam = class(TXMLNode, IXMLTCancelarAutorizacaoParam)
  protected
    { IXMLTCancelarAutorizacaoParam }
    function Get_CREDENCIADO: IXMLTCredenciado;
    function Get_AUTORIZACAO: LongWord;
    function Get_VALOR: LongWord;
    function Get_OPERADOR: WideString;
    procedure Set_AUTORIZACAO(Value: LongWord);
    procedure Set_VALOR(Value: LongWord);
    procedure Set_OPERADOR(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCredenciado }

  TXMLTCredenciado = class(TXMLNode, IXMLTCredenciado)
  protected
    { IXMLTCredenciado }
    function Get_CODACESSO: LongWord;
    function Get_SENHA: WideString;
    procedure Set_CODACESSO(Value: LongWord);
    procedure Set_SENHA(Value: WideString);
  end;

{ TXMLTCancelarAutorizacaoRet }

  TXMLTCancelarAutorizacaoRet = class(TXMLNode, IXMLTCancelarAutorizacaoRet)
  protected
    { IXMLTCancelarAutorizacaoRet }
    function Get_STATUS: LongWord;
    function Get_MSG: WideString;
    procedure Set_STATUS(Value: LongWord);
    procedure Set_MSG(Value: WideString);
  end;

implementation

{ TXMLTCancelarAutorizacaoParam }

procedure TXMLTCancelarAutorizacaoParam.AfterConstruction;
begin
  RegisterChildNode('CREDENCIADO', TXMLTCredenciado);
  inherited;
end;

function TXMLTCancelarAutorizacaoParam.Get_CREDENCIADO: IXMLTCredenciado;
begin
  Result := ChildNodes['CREDENCIADO'] as IXMLTCredenciado;
end;

function TXMLTCancelarAutorizacaoParam.Get_AUTORIZACAO: LongWord;
begin
  Result := ChildNodes['AUTORIZACAO'].NodeValue;
end;

procedure TXMLTCancelarAutorizacaoParam.Set_AUTORIZACAO(Value: LongWord);
begin
  ChildNodes['AUTORIZACAO'].NodeValue := Value;
end;

function TXMLTCancelarAutorizacaoParam.Get_VALOR: LongWord;
begin
  Result := ChildNodes['VALOR'].NodeValue;
end;

procedure TXMLTCancelarAutorizacaoParam.Set_VALOR(Value: LongWord);
begin
  ChildNodes['VALOR'].NodeValue := Value;
end;

function TXMLTCancelarAutorizacaoParam.Get_OPERADOR: WideString;
begin
  Result := ChildNodes['OPERADOR'].Text;
end;

procedure TXMLTCancelarAutorizacaoParam.Set_OPERADOR(Value: WideString);
begin
  ChildNodes['OPERADOR'].NodeValue := Value;
end;

{ TXMLTCredenciado }

function TXMLTCredenciado.Get_CODACESSO: LongWord;
begin
  Result := ChildNodes['CODACESSO'].NodeValue;
end;

procedure TXMLTCredenciado.Set_CODACESSO(Value: LongWord);
begin
  ChildNodes['CODACESSO'].NodeValue := Value;
end;

function TXMLTCredenciado.Get_SENHA: WideString;
begin
  Result := ChildNodes['SENHA'].Text;
end;

procedure TXMLTCredenciado.Set_SENHA(Value: WideString);
begin
  ChildNodes['SENHA'].NodeValue := Value;
end;

{ TXMLTCancelarAutorizacaoRet }

function TXMLTCancelarAutorizacaoRet.Get_STATUS: LongWord;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLTCancelarAutorizacaoRet.Set_STATUS(Value: LongWord);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLTCancelarAutorizacaoRet.Get_MSG: WideString;
begin
  Result := ChildNodes['MSG'].Text;
end;

procedure TXMLTCancelarAutorizacaoRet.Set_MSG(Value: WideString);
begin
  ChildNodes['MSG'].NodeValue := Value;
end;

end. 