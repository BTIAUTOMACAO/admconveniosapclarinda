unit UCadGeraCartaoNovo;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,} uClassLog,
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls, Mask,
  JvDateTimePicker, JvDBDateTimePicker, JvToolEdit, Menus,
  ToolEdit, RXDBCtrl, {scap32_rt,}ExtDlgs, jpeg, CurrEdit, {JvDBComb,}
  XMLDoc, IdHTTP, JvExStdCtrls, JvCombobox, JvDBCombobox,
  JvExControls, JvDBLookup, JvExMask, JvExDBGrids, JvDBGrid, ADODB,
  JvValidateEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit, ClipBrd, JvDBControls, JvMemoryDataset, U1;

type
  TFCadGeraCartaoNovo = class(TF1)
    Empresa: TPageControl;
    Conveniado: TTabSheet;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    JvDBGrid1: TJvDBGrid;
    Panel2: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    ButMarcaTodos: TButton;
    ButMarcDesm: TButton;
    ButDesmTodos: TButton;
    Panel4: TPanel;
    Label2: TLabel;
    Panel5: TPanel;
    JvDBGrid2: TJvDBGrid;
    Panel7: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label31: TLabel;
    EdCodEmp: TEdit;
    lbl1: TLabel;
    cbbLiberado: TComboBox;
    ButBusca: TBitBtn;
    QCartoesPorEmpresa: TADOQuery;
    DSCartoesPorEmpresa: TDataSource;
    QCartoesPorEmpresaconv_id: TIntegerField;
    QCartoesPorEmpresatitular: TStringField;
    QCartoesPorEmpresanome: TStringField;
    Label3: TLabel;
    lkpEmp: TJvDBLookupCombo;
    QEmpresa: TADOQuery;
    QEmpresaEMPRES_ID: TIntegerField;
    QEmpresaNOME: TStringField;
    QEmpresaname: TStringField;
    dsEmp: TDataSource;
    QCartoesPorEmpresacartao_id: TIntegerField;
    QCartoesPorEmpresajaemitido: TStringField;
    QCartoesPorEmpresaliberado: TStringField;
    CheckBox1: TCheckBox;
    DSCartoes: TDataSource;
    QCartoes: TADOQuery;
    QCartoesCARTAO_ID: TIntegerField;
    QCartoesCONV_ID: TIntegerField;
    QCartoesNOME: TStringField;
    QCartoesLIBERADO: TStringField;
    QCartoesCODIGO: TIntegerField;
    QCartoesDIGITO: TWordField;
    QCartoesTITULAR: TStringField;
    QCartoesJAEMITIDO: TStringField;
    QCartoesAPAGADO: TStringField;
    QCartoesLIMITE_MES: TBCDField;
    QCartoesCODCARTIMP: TStringField;
    QCartoesPARENTESCO: TStringField;
    QCartoesDATA_NASC: TDateTimeField;
    QCartoesNUM_DEP: TIntegerField;
    QCartoesFLAG: TStringField;
    QCartoesDTEMISSAO: TDateTimeField;
    QCartoesCPF: TStringField;
    QCartoesRG: TStringField;
    QCartoesVIA: TIntegerField;
    QCartoesDTAPAGADO: TDateTimeField;
    QCartoesDTALTERACAO: TDateTimeField;
    QCartoesOPERADOR: TStringField;
    QCartoesDTCADASTRO: TDateTimeField;
    QCartoesOPERCADASTRO: TStringField;
    QCartoesCRED_ID: TIntegerField;
    QCartoesATIVO: TStringField;
    QCartoesEMPRES_ID: TIntegerField;
    QCartoesSENHA: TStringField;
    QCartoesPorEmpresaempres_id: TIntegerField;
    QCartoesTemp: TADOQuery;
    QCartoesTempCARTAO_ID: TIntegerField;
    QCartoesTempCONV_ID: TIntegerField;
    QCartoesTempNOME: TStringField;
    QCartoesTempLIBERADO: TStringField;
    QCartoesTempCODIGO: TIntegerField;
    QCartoesTempDIGITO: TWordField;
    QCartoesTempTITULAR: TStringField;
    QCartoesTempJAEMITIDO: TStringField;
    QCartoesTempAPAGADO: TStringField;
    QCartoesTempLIMITE_MES: TBCDField;
    QCartoesTempCODCARTIMP: TStringField;
    QCartoesTempPARENTESCO: TStringField;
    QCartoesTempDATA_NASC: TDateTimeField;
    QCartoesTempNUM_DEP: TIntegerField;
    QCartoesTempFLAG: TStringField;
    QCartoesTempDTEMISSAO: TDateTimeField;
    QCartoesTempCPF: TStringField;
    QCartoesTempRG: TStringField;
    QCartoesTempVIA: TIntegerField;
    QCartoesTempDTAPAGADO: TDateTimeField;
    QCartoesTempDTALTERACAO: TDateTimeField;
    QCartoesTempOPERADOR: TStringField;
    QCartoesTempDTCADASTRO: TDateTimeField;
    QCartoesTempOPERCADASTRO: TStringField;
    QCartoesTempCRED_ID: TIntegerField;
    QCartoesTempATIVO: TStringField;
    QCartoesTempEMPRES_ID: TIntegerField;
    QCartoesTempSENHA: TStringField;
    QCartoesTempLIMITE_DIARIO: TBCDField;
    QCartoesTempCONSUMO_ATUAL: TBCDField;
    QCartoesTempCVV: TStringField;
    DSCartoesTemp: TDataSource;
    QCartoesPorEmpresausa_cod_importacao: TStringField;
    QCartoesPorEmpresacodigo: TIntegerField;
    QCartoesPorEmpresachapa: TFloatField;
    EdCod: TEdit;
    Label4: TLabel;
    Label29: TLabel;
    EdCartaoId: TEdit;
    Label5: TLabel;
    EdNome: TEdit;
    Label6: TLabel;
    EdEmpres_id: TEdit;
    Label56: TLabel;
    EdChapa: TEdit;
    DataSource1: TDataSource;
    QCartoesPorConveniado: TADOQuery;
    DSCartoesPorConveniado: TDataSource;
    butBuscarPorConv: TBitBtn;
    QCartoesPorConveniadocartao_id: TIntegerField;
    QCartoesPorConveniadoconv_id: TIntegerField;
    QCartoesPorConveniadotitular: TStringField;
    QCartoesPorConveniadonome: TStringField;
    QCartoesPorConveniadoliberado: TStringField;
    QCartoesPorConveniadochapa: TFloatField;
    QCartoesPorConveniadojaemitido: TStringField;
    QCartoesPorConveniadocodigo: TIntegerField;
    QCartoesPorConveniadoempres_id: TIntegerField;
    QCartoesPorConveniadousa_cod_importacao: TStringField;
    QCartoesTempJAEMITIDO_CARTAO_NOVO: TStringField;
    chkSoNaoEmit: TCheckBox;
    EdCodCartImp: TEdit;
    Label7: TLabel;
    QCartoesPorEmpresaJAEMITIDO_CARTAO_NOVO: TStringField;
    QCartoesPorConveniadoJAEMITIDO_CARTAO_NOVO: TStringField;
    procedure ButBuscaClick(Sender: TObject);
    procedure lkpEmpEnter(Sender: TObject);
    procedure EmpresaEnter(Sender: TObject);
    procedure lkpEmpExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure butBuscarPorConvClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LimpaEdits;
    procedure BitBtn7Click(Sender: TObject);
    procedure TabSheet1Enter(Sender: TObject);
    procedure ConveniadoEnter(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure ConveniadoShow(Sender: TObject);
    procedure EdCodEmpKeyPress(Sender: TObject; var Key: Char);
    procedure EdCodKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  FCadGeraCartaoNovo: TFCadGeraCartaoNovo;

implementation

uses cartao_util, DM, UMenu;

{$R *.dfm}

procedure TFCadGeraCartaoNovo.ButBuscaClick(Sender: TObject);
begin
  inherited;
//  QCartoesPorEmpresa.Close;
//  if ((Trim(EdCodEmp.Text) = '')) then
//  begin
//     MsgInf('� necess�rio especificar um crit�rio de busca.');
//     EdCodEmp.SetFocus;
//     Exit;
//  end;
  Screen.Cursor := crHourGlass;
  QCartoesPorEmpresa.SQL.Clear;
  QCartoesPorEmpresa.SQL.Add('select');
  QCartoesPorEmpresa.SQL.Add(' cartoes.conv_id,cartoes.titular,cartoes.nome,cartoes.codigo,');
  QCartoesPorEmpresa.SQL.Add(' cartoes.cartao_id,conveniados.liberado,cartoes.jaemitido,conveniados.empres_id,conveniados.chapa,');
  QCartoesPorEmpresa.SQL.Add(' coalesce(JAEMITIDO_CARTAO_NOVO,''N'') JAEMITIDO_CARTAO_NOVO,'  );
  QCartoesPorEmpresa.SQL.Add(' empresas.usa_cod_importacao' );
  QCartoesPorEmpresa.SQL.Add(' from cartoes');
  QCartoesPorEmpresa.SQL.Add(' inner join conveniados on conveniados.conv_id = cartoes.conv_id');
  QCartoesPorEmpresa.SQL.Add(' inner join empresas on empresas.empres_id = conveniados.empres_id' );
  QCartoesPorEmpresa.SQL.Add(' where conveniados.apagado = ''N''');
  if Trim(EdCodEmp.Text) <> '' then
  QCartoesPorEmpresa.SQL.Add(' and conveniados.empres_id = '+EdCodEmp.Text);
  if cbbLiberado.ItemIndex > 0 then
    if cbbLiberado.ItemIndex = 1 then
      QCartoesPorEmpresa.SQL.Add(' and conveniados.liberado = ''S''')
        else
          QCartoesPorEmpresa.SQL.Add(' and conveniados.liberado = ''N''');
  if chkSoNaoEmit.Checked then
    QCartoesPorEmpresa.SQL.Add(' and cartoes.JAEMITIDO_CARTAO_NOVO IS NULL');

  QCartoesPorEmpresa.SQL.Text;

  QCartoesPorEmpresa.Open;

  if QCartoesPorEmpresa.IsEmpty then
  begin
    MsgInf('N�o foi encrontrado nenhum registro!');
    EdCod.SetFocus;
  end;
  //  else
  //LimpaEdits;
  EdCodEmp.SetFocus;
  Screen.Cursor := crDefault;
end;

procedure TFCadGeraCartaoNovo.lkpEmpEnter(Sender: TObject);
begin
  inherited;
  if EdCodEmp.Text = '' then
       lkpEmp.ClearValue
     else if QEmpresa.Locate('empres_id',EdCodEmp.Text,[]) then  begin
       lkpEmp.KeyValue := EdCodEmp.Text;
       lkpEmp.SetFocus;
       //lblStatus.Caption := '';
     end
     else
       lkpEmp.ClearValue;
end;

procedure TFCadGeraCartaoNovo.EmpresaEnter(Sender: TObject);
begin
  inherited;
  QEmpresa.Open;
  
end;

procedure TFCadGeraCartaoNovo.lkpEmpExit(Sender: TObject);
begin
  inherited;
  EdCodEmp.Text := lkpEmp.KeyValue;
end;

procedure TFCadGeraCartaoNovo.BitBtn3Click(Sender: TObject);
var codimp, cvv, cardImpNovo: string;
    logEmpresId, logStatus,logID : String;
begin
  inherited;
  if QCartoesPorEmpresa.RecordCount > 0 then
  begin
    if Application.MessageBox('Confirma a gera��o de nova via do cart�o para todos os conveniados da empresa?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
        DMConexao.Config.Open;
        try
          logEmpresId := QCartoesPorEmpresaempres_id.AsString;
          logID := DMConexao.ExecuteQuery('select next value for SLOG_NOVOS_CARTOES');
          DMConexao.AdoCon.BeginTrans;
          While not QCartoesPorEmpresa.Eof do
          begin
            if(not JaEmitidoCartaoNovo(QCartoesPorEmpresacartao_id.AsString)) then
            begin
              //DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'' where CARTAO_ID = '+QCartoesPorEmpresacartao_id.AsString+'');
              DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE(), LOG_ID_CARTAO_NOVO = '+logID+' where CARTAO_ID = '+QCartoesPorEmpresacartao_id.AsString+'');
              DMConexao.ExecuteSql('insert into cartoes_temp select * from cartoes where CARTAO_ID = '+QCartoesPorEmpresacartao_id.AsString+'');
              QCartoesTemp.Parameters[0].Value := QCartoesPorEmpresacartao_id.Value;
              QCartoesTemp.Open;
              QCartoesTemp.Edit;
              QCartoesTempJAEMITIDO.AsString := 'N';

              //gera novo carImpNovo
              if QCartoesPorEmpresausa_cod_importacao.AsString = 'S' then begin
                repeat
                  codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
                until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp));

              end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
                codimp := QCartoesPorEmpresacodigo.AsString
              else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
              begin
                if QCartoesPorEmpresatitular.AsString = 'S' then
                  codimp := DMConexao.ObterCodCartImp
                else
                  codimp := DMConexao.ObterCodCartImp(False);
              end
              else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
                codimp := DMConexao.ObterCodCartImpMod1(QCartoesPorEmpresaconv_id.AsInteger, QCartoesPorEmpresaempres_id.AsInteger,QCartoesPorEmpresachapa.AsString)
              else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
                codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));

              //fim gera��o de cart�o
              cardImpNovo := codimp;
              QCartoesTempCODCARTIMP.AsString := cardImpNovo;

              //Gera o CVV
              cvv := GerarCVV(cardImpNovo);
              QCartoesTempCVV.AsString := cvv;

              if QCartoesTemp.State in [dsEdit] then begin
                QCartoesTemp.Post;
              end;
              QCartoesTemp.Close;
            end;
            QCartoesPorEmpresa.Next;
          end;
          DMConexao.AdoCon.CommitTrans;
          logStatus := 'C';
          DMConexao.ExecuteSql('insert into LOG_GERACAO_NOVOS_CARTOES values ('+logID+','+logEmpresId+',GETDATE(),'''+Operador.Nome+''','''+logStatus+''' )');
          QCartoesPorEmpresa.First;
        except
          on e:Exception do
          begin
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            logStatus := 'E';
            DMConexao.ExecuteSql('insert into LOG_GERACAO_NOVOS_CARTOES values ('+logID+','+logEmpresId+',GETDATE(),'''+Operador.Nome+''','''+logStatus+''' )');
            MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
            Screen.Cursor := crDefault;
            LimpaEdits;
            lkpEmp.KeyValue := 0;
            EdCodEmp.SetFocus;
            abort;
          end;
        end;
        DMConexao.Config.Close;
        MsgInf('Nova(s) via(s) gerada(s) com sucesso!');
        LimpaEdits;
        lkpEmp.KeyValue := 0;
        EdCodEmp.SetFocus;
        QCartoesPorEmpresa.Close;
    end;
  end;
end;

procedure TFCadGeraCartaoNovo.butBuscarPorConvClick(Sender: TObject);
begin
  inherited;
  QCartoesPorConveniado.Close;
  if ((Trim(EdCod.Text) = '') and (Trim(EdNome.Text) = '') and (Trim(EdCodCartImp.Text) = '')and (Trim(EdChapa.Text) = '') and (Trim(EdEmpres_id.Text) = '')
      and (Trim(EdCartaoId.Text) = '')) then
    begin
       MsgInf('� necess�rio especificar um crit�rio de busca.');
       EdCod.SetFocus;
       Exit;
    end;

  Screen.Cursor := crHourGlass;
  QCartoesPorConveniado.SQL.Clear;
  QCartoesPorConveniado.SQL.Add('select');
  QCartoesPorConveniado.SQL.Add(' cartoes.conv_id,cartoes.titular,cartoes.nome,cartoes.codigo,');
  QCartoesPorConveniado.SQL.Add(' cartoes.cartao_id,conveniados.liberado,cartoes.jaemitido,conveniados.empres_id,conveniados.chapa,');
  QCartoesPorConveniado.SQL.Add(' coalesce(JAEMITIDO_CARTAO_NOVO,''N'') JAEMITIDO_CARTAO_NOVO,'  );
  QCartoesPorConveniado.SQL.Add( ' empresas.usa_cod_importacao' );
  QCartoesPorConveniado.SQL.Add(' from cartoes');
  QCartoesPorConveniado.SQL.Add(' inner join conveniados on conveniados.conv_id = cartoes.conv_id');
  QCartoesPorConveniado.SQL.Add(' inner join empresas on empresas.empres_id = conveniados.empres_id' );
  QCartoesPorConveniado.SQL.Add(' where conveniados.apagado = ''N''');
  if Trim(EdCod.Text) <> '' then
    QCartoesPorConveniado.SQL.Add(' and conveniados.conv_id = '+EdCod.Text);
//  if cbbLiberado.ItemIndex > 0 then
//    if cbbLiberado.ItemIndex = 1 then
//      QCartoesPorConveniado.SQL.Add(' and conveniados.liberado = ''S''')
//        else
//          QCartoesPorConveniado.SQL.Add(' and conveniados.liberado = ''N''');
  if Trim(EdNome.Text) <> '' then
    QCartoesPorConveniado.SQL.Add(' and conveniados.titular like(''%'+EdNome.Text+'%'')');

  if Trim(EdCartaoId.Text) <> ''then
    QCartoesPorConveniado.SQL.Add(' and cartoes.cartao_id = '+EdCartaoId.Text);

  if Trim(EdCodCartImp.Text) <> '' then
    QCartoesPorConveniado.SQL.Add(' and cartoes.codcartimp = '+QuotedStr(EdCodCartImp.Text));

  if Trim(EdChapa.Text) <> ''then
    QCartoesPorConveniado.SQL.Add(' and conveniados.chapa = '+EdChapa.Text);

  if Trim(EdCodEmp.Text) <> ''then
    QCartoesPorConveniado.SQL.Add(' and conveniados.empres_id = '+EdCodEmp.Text);

  QCartoesPorConveniado.SQL.Add(' order by titular desc, nome');
  QCartoesPorConveniado.SQL.Text;

  QCartoesPorConveniado.Open;

  if QCartoesPorConveniado.IsEmpty then
  begin
    MsgInf('N�o foi encrontrado nenhum registro!');
    EdCod.SetFocus;
  end;
//  else
  LimpaEdits;
  EdCod.SetFocus;
  Screen.Cursor := crDefault;

end;


//procedure TFCadGeraCartaoNovo.FormCreate(Sender: TObject);
//begin
//  inherited;
//  detalhe  := 'Empres ID: ';
//end;

procedure TFCadGeraCartaoNovo.LimpaEdits;
begin
  EdCod.Clear;
  EdCodEmp.Clear;
  EdEmpres_id.Clear;
  EdNome.Clear;
  EdCartaoId.Clear;
  EdChapa.Clear;
  cbbLiberado.ItemIndex := 0;
  EdCodCartImp.Clear;
end;

procedure TFCadGeraCartaoNovo.FormCreate(Sender: TObject);
begin
  inherited;
//  detalhe  := 'Empres ID: ';
end;

procedure TFCadGeraCartaoNovo.BitBtn7Click(Sender: TObject);
var codimp, cvv, cardImpNovo: string;
logEmpresId, logStatus, logID : String;
begin
  inherited;
  if QCartoesPorConveniado.RecordCount > 0 then
  begin
    if Application.MessageBox('Confirma a gera��o de nova via do cart�o para todos os conveniados?','Altera��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then
    begin
        DMConexao.Config.Open;
        try
          logEmpresId := QCartoesPorConveniadoempres_id.AsString;
          logID := DMConexao.ExecuteQuery('select next value for SLOG_NOVOS_CARTOES');
          DMConexao.AdoCon.BeginTrans;
          While not QCartoesPorConveniado.Eof do
          begin
            if(not JaEmitidoCartaoNovo(QCartoesPorConveniadocartao_id.AsString)) then
            begin
              DMConexao.ExecuteSql('UPDATE cartoes SET JAEMITIDO_CARTAO_NOVO = ''S'',DATA_EMISSAO_CARTAO_NOVO = GETDATE(), LOG_ID_CARTAO_NOVO = '+logID+' where CARTAO_ID = '+QCartoesPorConveniadocartao_id.AsString+'');
              DMConexao.ExecuteSql('INSERT into cartoes_temp select * from cartoes where CARTAO_ID = '+QCartoesPorConveniadocartao_id.AsString+'');

              QCartoesTemp.Parameters[0].Value := QCartoesPorConveniadocartao_id.Value;
              QCartoesTemp.Open;
              QCartoesTemp.Edit;
              QCartoesTempJAEMITIDO.AsString := 'N';

              //gera novo carImpNovo
              if QCartoesPorConveniadousa_cod_importacao.AsString = 'S' then begin
                repeat
                  codimp := RemoveCaracter(gerarCartao(DMConexao.ConfigCOD_CARD_BIN_NOVO.AsInteger));
                until (verificaCartaoExistente(codimp) and verificaCartaoExistenteTabelaTemp(codimp));

              end else if DMConexao.ConfigMOVER_CODCART_TO_CODIMP.AsString = 'S' then
                codimp := QCartoesPorConveniadocodigo.AsString
              else if DMConexao.ConfigINCREMENTCODCARTIMP.AsString = 'S' then
              begin
                if QCartoesPorConveniadotitular.AsString = 'S' then
                  codimp := DMConexao.ObterCodCartImp
                else
                  codimp := DMConexao.ObterCodCartImp(False);
              end
              else if DMConexao.ConfigINCREMENTCODCARTIMPMOD1.AsString = 'S' then
                codimp := DMConexao.ObterCodCartImpMod1(QCartoesPorConveniadoconv_id.AsInteger, QCartoesPorConveniadoempres_id.AsInteger,QCartoesPorConveniadochapa.AsString)
              else if DMConexao.ConfigUSAINICIALCODCARTIMP.AsString = 'S' then
                codimp :=  DMConexao.ConfigINICIALCODCARTIMP.AsString + FormatFloat('00000000',DMConexao.getGeneratorValue('GEN_INICODCARTIMP'));

              //fim gera��o de cart�o
              cardImpNovo := codimp;
              QCartoesTempCODCARTIMP.AsString := cardImpNovo;

              //Gera o CVV
              cvv := GerarCVV(cardImpNovo);
              QCartoesTempCVV.AsString := cvv;

              if QCartoesTemp.State in [dsEdit] then begin
                QCartoesTemp.Post;
              end;

//              if QCartoesTemp.State in [dsEdit] then begin
//                  DMConexao.GravaLogSemTelaOcorrencia(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '');
//                  QCartoesTemp.Post;
//              end;
//              if QCartoesTemp.State in [dsEdit] then begin
//                  DMConexao.GravaLogSemTelaOcorrencia(Self.Name,StringReplace(detalhe,': ','',[rfReplaceAll]),'',QCartoes.FieldByName('CARTAO_ID').AsString,Operador.Nome,'Altera��o',QCartoes.FieldByName('CARTAO_ID').AsString,'', '');
//                  QCartoesTemp.Post;
//              end;
              //QCartoesTemp.Post;
              QCartoesTemp.Close;

            end;
            QCartoesPorConveniado.Next;
          end;
          DMConexao.AdoCon.CommitTrans;
          logStatus := 'C';
          DMConexao.ExecuteSql('insert into LOG_GERACAO_NOVOS_CARTOES values ('+logID+','+logEmpresId+',GETDATE(),'''+Operador.Nome+''','''+logStatus+''' )');
          QCartoesPorConveniado.First;
        except
          on e:Exception do
          begin
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            logStatus := 'E';
            DMConexao.ExecuteSql('insert into LOG_GERACAO_NOVOS_CARTOES values ('+logID+','+logEmpresId+',GETDATE(),'''+Operador.Nome+''','''+logStatus+''' )');
            MsgErro('Um erro ocorreu durante a inclus�o, opera��o cancelada.'+sLineBreak+'Erro: '+e.Message);
            Screen.Cursor := crDefault;
            abort;
          end;
        end;
        DMConexao.Config.Close;
        MsgInf('Nova(s) via(s) gerada(s) com sucesso!');
        QCartoesPorConveniado.Close;
    end;
  end;
end;


procedure TFCadGeraCartaoNovo.TabSheet1Enter(Sender: TObject);
begin
//  inherited;
  EdCodEmp.SetFocus;
end;

procedure TFCadGeraCartaoNovo.ConveniadoEnter(Sender: TObject);
begin
//  inherited;
  EdCod.SetFocus;
end;

procedure TFCadGeraCartaoNovo.TabSheet1Show(Sender: TObject);
begin
  inherited;
  EdCodEmp.SetFocus;
end;

procedure TFCadGeraCartaoNovo.ConveniadoShow(Sender: TObject);
begin
  inherited;
  EdCod.SetFocus;
end;

procedure TFCadGeraCartaoNovo.EdCodEmpKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
   if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
   if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then ButBusca.Click;
end;

procedure TFCadGeraCartaoNovo.EdCodKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9',CHR(8), CHAR(3), CHAR(22), CHAR(24), Chr(44), #13,#8,',']) then Key := #0;
   if ((key = #13) and (Trim((Sender as TCustomEdit).Text) <> '')) then butBuscarPorConv.Click;
end;

end.


