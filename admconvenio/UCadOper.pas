unit UCadOper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,}
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, {JvMemDS,} Mask, JvToolEdit,
  {JvLookup,} Menus, JvExControls, JvDBLookup, JvExMask, JvExDBGrids,
  JvDBGrid, ADODB;

type
  TFCadOper = class(TFCad)
    QCadastroGRUPO_USU_ID: TIntegerField;
    QCadastroLIBERADO: TStringField;
    QCadastroNOME: TStringField;
    QCadastroSENHA: TStringField;
    QCadastroUSUARIO_ID: TIntegerField;
    QCadastroAPAGADO: TStringField;
    QCadastroOPERADOR: TStringField;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    dbEdtNm: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBGrupo: TJvDBLookupCombo;
    Label5: TLabel;
    DSGrupos: TDataSource;
    ButLimpaSenha: TBitBtn;
    EdGrupoID: TEdit;
    Label6: TLabel;
    EdGrupoNome: TEdit;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroDTCADASTRO: TDateTimeField;
    QGrupos: TADOQuery;
    QGruposgrupo_usu_id: TIntegerField;
    QGruposdescricao: TStringField;
    QCadastroGRUPO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButLimpaSenhaClick(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButIncluiClick(Sender: TObject);
    procedure QCadastroBeforePost(DataSet: TDataSet);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure TabFichaShow(Sender: TObject);
    procedure ButApagaClick(Sender: TObject);
    procedure ButGravaClick(Sender: TObject);
    function VerificaOperPorEmp() : Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadOper: TFCadOper;

implementation

uses DM, {util2,} UValidacao, cartao_util, UAltOperEmp;

{$R *.dfm}

procedure TFCadOper.FormCreate(Sender: TObject);
begin
  chavepri := 'usuario_id';
  Detalhe  := 'Usu�rio ID: ';  
  inherited;
  QGrupos.Open;
end;

procedure TFCadOper.QCadastroAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Operador: '+QCadastroNOME.AsString;
end;

procedure TFCadOper.ButLimpaSenhaClick(Sender: TObject);
begin
  inherited;
  if not QCadastro.IsEmpty then begin
     if Application.MessageBox('Confirma esta opera��o?','Confirma��o',mb_yesno+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then begin
        QCadastro.edit;
        QCadastroSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
        QCadastro.Post;
     end;
  end;
end;

procedure TFCadOper.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' select * from usuarios u where coalesce(apagado,''N'') <> ''S''');
  if Trim(EdCod.Text) <> '' then
     QCadastro.SQL.Add(' and u.usuario_id in ('+EdCod.Text+')');
  If Trim(EdNome.Text) <> '' then
     QCadastro.SQL.Add(' and u.nome like ''%'+EdNome.Text+'%''');
  If Trim(EdGrupoID.Text) <> '' then
     QCadastro.SQL.Add(' and u.grupo_usu_id in ('+EdGrupoID.Text+')');
  If Trim(EdGrupoNome.Text) <> '' then
    QCadastro.SQL.Add(' and u.grupo_usu_id in (select grupo_usu_id from grupo_usuarios where descricao like ''%'+EdGrupoNome.Text+'%'')');
  QCadastro.SQL.Add(' order by nome ');
  QCadastro.Open;
  If not QCadastro.IsEmpty then DBGrid1.SetFocus;
  EdCod.Clear;
  EdNome.Clear;
  EdGrupoID.Clear;
  EdGrupoNome.Clear;
end;

procedure TFCadOper.ButIncluiClick(Sender: TObject);
begin
  inherited;
  dbEdtNm.SetFocus;
  QCadastroAPAGADO.AsString := 'N';
  QCadastroLIBERADO.AsString := 'S';
end;

procedure TFCadOper.QCadastroBeforePost(DataSet: TDataSet);
begin
  if fnVerfCompVazioEmTabSheet('Nome obrigat�rio!',dbEdtNm)  then Abort;
  if fnVerfCompVazioEmTabSheet('Grupo obrigat�rio!',DBGrupo) then Abort;
  if QCadastro.State = dsInsert then
  begin
    QCadastroSENHA.AsString := Crypt('E','1111','BIGCOMPRAS');
    QCadastroAPAGADO.AsString := 'N';
  end;
  inherited;

end;

procedure TFCadOper.QCadastroAfterInsert(DataSet: TDataSet);
var usuario_id : Integer;
begin
  inherited;

  usuario_id := DMConexao.ExecuteQuery('SELECT NEXT VALUE FOR SUSU_ID');
  QCadastroUSUARIO_ID.AsInteger := usuario_id;
end;

procedure TFCadOper.TabFichaShow(Sender: TObject);
begin
  inherited;
  QCadastro.Open;
end;

function TFCadOper.VerificaOperPorEmp() : Integer;
var qtdEmpPorOper  : Integer;
var    form : TFTelaAltOperador;
begin
  qtdEmpPorOper := DMConexao.ExecuteQuery('SELECT COUNT(*) FROM EMPRESAS E INNER JOIN USUARIOS U ON ' +
                       'E.RESPONSAVEL_FECHAMENTO = U.NOME' +
                       ' AND U.NOME LIKE ''%'+QCadastroNOME.AsString+'%'' AND E.APAGADO = ''N''');

  if qtdEmpPorOper >= 1 then
  begin
    if Application.MessageBox('Este usu�rio possui empresas sob sua responsabilidade, deseja trasfer�-la para outro usu�rio!','Confirma��o',mb_yesno+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then begin
      form := TFTelaAltOperador.Create(Self);
      form.Operador_old.Text := QCadastroNOME.AsString;
      form.ShowModal;
      if form.ModalResult = mrOk then
         result := 1
         else
         result := 2;
    end;
  end
  else
  result := 2;
end;

procedure TFCadOper.ButApagaClick(Sender: TObject);
var qtdEmpPorOper  : Integer;
var    form : TFTelaAltOperador;
var result  : Integer;
begin
  inherited;
  if flag = true then
    result := VerificaOperPorEmp
  else
    Exit;
end;

procedure TFCadOper.ButGravaClick(Sender: TObject);
var result : Integer;
begin
   inherited;
    if flag = true then
    begin
      if DBCheckBox1.Checked = False then
      begin
        result := VerificaOperPorEmp;
      end
      else
      exit;

    end
    else
    Exit;
end;

end.
