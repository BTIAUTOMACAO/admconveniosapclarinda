unit FPesqFornec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGrids, DB, DM,
  ADODB;

type
  TF_PesqFornec = class(TForm)
    panel: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    dsrcPesqFornec: TDataSource;
    FDataSetConsulta: TADOQuery;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    function GetDataSetConsulta: TADOQuery;
    procedure SetDataSetConsulta(const Value: TADOQuery);
    { Private declarations }
  public
    { Public declarations }
    {FLancamentos : TFLancamentos;}//COMENTADO ARIANE
    property DataSetConsulta: TADOQuery read GetDataSetConsulta write SetDataSetConsulta;
  end;

implementation


{$R *.dfm}

procedure TF_PesqFornec.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then begin
   FDataSetConsulta.close;
   if Length(Trim(Edit1.Text)) > 2 then
      FDataSetConsulta.SQL.text := 'Select cred_id, nome, fantasia, comissao from credenciados where fantasia like ("%'+Edit1.Text+'%") order by fantasia '
   else
      FDataSetConsulta.SQL.text := 'Select cred_id, nome, fantasia, comissao from credenciados where fantasia like ("'+Edit1.Text+'%") order by fantasia ';
   FDataSetConsulta.Open;
   if not FDataSetConsulta.IsEmpty then DBGrid1.SetFocus else ShowMessage('Fornecedor n�o encontrado!');
end;
if key = vk_escape then close;
end;

procedure TF_PesqFornec.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then if not FDataSetConsulta.IsEmpty then Self.ModalResult := mrOk;
If key = vk_escape then close;
end;

procedure TF_PesqFornec.DBGrid1DblClick(Sender: TObject);
begin
if not FDataSetConsulta.IsEmpty then Self.ModalResult := mrOk;
end;

function TF_PesqFornec.GetDataSetConsulta:TADOQuery;
begin
  Result := FDataSetConsulta;
end;

procedure TF_PesqFornec.SetDataSetConsulta(const Value: TADOQuery);
begin
  FDataSetConsulta := Value;
  dsrcPesqFornec.DataSet := FDataSetConsulta;
end;

end.
