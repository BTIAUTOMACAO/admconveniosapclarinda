unit UCadCredTef;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCad, DB, Grids, DBGrids, {JvDBCtrl,} uClassLog,
  StdCtrls, Buttons, JvEdit, {JvTypedEdit,} DBCtrls, ExtCtrls, ComCtrls, Mask,
  {JvLookup,} JvToolEdit, {JvMemDS,} JvDateTimePicker, JvDBDateTimePicker, Menus, ToolEdit, dateutils,
  CurrEdit, {JvDBComb,} XMLDoc, IdHTTP, ADODB, ComObj, JvExControls,
  JvDBLookup, JvExStdCtrls, JvCombobox, JvDBCombobox, JvExMask,
  JvExDBGrids, JvDBGrid, JvValidateEdit, JvMaskEdit, JvCheckedMaskEdit,
  JvDatePickerEdit, JvDBDatePickerEdit, JvExButtons, JvBitBtn;

type
  TFCadCredTef = class(TFCad)
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox3: TGroupBox;
    TabSheet2: TTabSheet;
    ButLimpaSenha: TBitBtn;
    GroupBox6: TGroupBox;
    Label22: TLabel;
    DBEdit19: TDBEdit;
    Label23: TLabel;
    DBEdit20: TDBEdit;
    Label24: TLabel;
    DBEdit21: TDBEdit;
    GroupBox8: TGroupBox;
    Label27: TLabel;
    DBEdit24: TDBEdit;
    Label28: TLabel;
    DBEdit25: TDBEdit;
    Label25: TLabel;
    dbEdtEmail: TDBEdit;
    Label26: TLabel;
    DBEdit23: TDBEdit;
    GroupBox7: TGroupBox;
    Label29: TLabel;
    DSBanco: TDataSource;
    Label30: TLabel;
    DBEdit26: TDBEdit;
    Label31: TLabel;
    DBEdit27: TDBEdit;
    Label32: TLabel;
    DBEdit28: TDBEdit;
    JvDBLookupCombo2: TJvDBLookupCombo;
    EdCidade: TEdit;
    Label37: TLabel;
    PopupCC: TPopupMenu;
    MenuItem1: TMenuItem;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    Exportarparaoexcel2: TMenuItem;
    Label38: TLabel;
    EdFanta: TEdit;
    PopupDesconto: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PopCodAcesso: TPopupMenu;
    ColocarCodAcessoManual1: TMenuItem;
    PopupAlteraID: TPopupMenu;
    AlterarIddoFornecedor1: TMenuItem;
    PopupComissaoEmpr: TPopupMenu;
    AltLineardeComissoporEmpresa1: TMenuItem;
    PopupMenu1: TPopupMenu;
    este1: TMenuItem;
    DBCheckBox1: TDBCheckBox;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    SpeedButton1: TSpeedButton;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit13: TDBEdit;
    GroupBox9: TGroupBox;
    Label46: TLabel;
    Label50: TLabel;
    DBEdit10: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    pb: TProgressBar;
    tExcel: TADOTable;
    Label34: TLabel;
    DBText1: TDBText;
    DSEstados: TDataSource;
    DuplicarEstabelecimentoalterandoID1: TMenuItem;
    tbCadPos: TTabSheet;
    DSPos: TDataSource;
    Panel24: TPanel;
    Label35: TLabel;
    dbGridPos: TJvDBGrid;
    Panel25: TPanel;
    btnGravarPos: TBitBtn;
    btnCancelarPos: TBitBtn;
    btnExcluirPos: TBitBtn;
    btnInserirPos: TBitBtn;
    dbLkpEstados: TDBLookupComboBox;
    dbLkpCidades: TDBLookupComboBox;
    DSCidades: TDataSource;
    QBanco: TADOQuery;
    QBancoCODIGO: TIntegerField;
    QBancoBANCO: TStringField;
    QBancoAPAGADO: TStringField;
    QBancoLAYOUT: TMemoField;
    QBancoDTAPAGADO: TDateTimeField;
    QBancoDTALTERACAO: TDateTimeField;
    QBancoOPERADOR: TStringField;
    QBancoDTCADASTRO: TDateTimeField;
    QBancoOPERCADASTRO: TStringField;
    QCredenciados: TADOQuery;
    QCredenciadosCRED_ID: TIntegerField;
    QCredenciadosNOME: TStringField;
    QCredenciadosSEG_ID: TIntegerField;
    QCredenciadosLIBERADO: TStringField;
    QCredenciadosCODACESSO: TIntegerField;
    QCredenciadosCOMISSAO: TBCDField;
    QCredenciadosBANCO: TIntegerField;
    QCredenciadosDIAFECHAMENTO1: TWordField;
    QCredenciadosDIAFECHAMENTO2: TWordField;
    QCredenciadosVENCIMENTO1: TWordField;
    QCredenciadosVENCIMENTO2: TWordField;
    QCredenciadosAPAGADO: TStringField;
    QCredenciadosPAGA_CRED_POR_ID: TIntegerField;
    QCredenciadosCONTRATO: TIntegerField;
    QCredenciadosCGC: TStringField;
    QCredenciadosINSCRICAOEST: TStringField;
    QCredenciadosFANTASIA: TStringField;
    QCredenciadosTELEFONE1: TStringField;
    QCredenciadosTELEFONE2: TStringField;
    QCredenciadosFAX: TStringField;
    QCredenciadosCONTATO: TStringField;
    QCredenciadosENVIAR_EMAIL: TStringField;
    QCredenciadosEMAIL: TStringField;
    QCredenciadosHOMEPAGE: TStringField;
    QCredenciadosPOSSUICOMPUTADOR: TStringField;
    QCredenciadosENDERECO: TStringField;
    QCredenciadosNUMERO: TIntegerField;
    QCredenciadosBAIRRO: TStringField;
    QCredenciadosCIDADE: TStringField;
    QCredenciadosCEP: TStringField;
    QCredenciadosESTADO: TStringField;
    QCredenciadosMAQUINETA: TStringField;
    QCredenciadosTIPOFECHAMENTO: TStringField;
    QCredenciadosNOVOCODACESSO: TIntegerField;
    QCredenciadosSENHA: TStringField;
    QCredenciadosAGENCIA: TStringField;
    QCredenciadosCONTACORRENTE: TStringField;
    QCredenciadosOBS1: TStringField;
    QCredenciadosOBS2: TStringField;
    QCredenciadosCORRENTISTA: TStringField;
    QCredenciadosCARTIMPRESSO: TStringField;
    QCredenciadosFORNCESTA: TStringField;
    QCredenciadosCPF: TStringField;
    QCredenciadosACEITA_PARC: TStringField;
    QCredenciadosGERA_LANC_CPMF: TStringField;
    QCredenciadosENCONTRO_CONTAS: TStringField;
    QCredenciadosDTAPAGADO: TDateTimeField;
    QCredenciadosDTALTERACAO: TDateTimeField;
    QCredenciadosOPERADOR: TStringField;
    QCredenciadosDTCADASTRO: TDateTimeField;
    QCredenciadosOPERCADASTRO: TStringField;
    QCredenciadosVALE_DESCONTO: TStringField;
    QCredenciadosCOMPLEMENTO: TStringField;
    QCredenciadosDIA_PAGTO: TIntegerField;
    QCredenciadosTX_DVV: TBCDField;
    QCredenciadosUTILIZA_AUTORIZADOR: TStringField;
    QCredenciadosUTILIZA_COMANDA: TStringField;
    QCredenciadosUTILIZA_RECARGA: TStringField;
    QEstados: TADOQuery;
    QEstadosESTADO_ID: TIntegerField;
    QEstadosUF: TStringField;
    QEstadosICMS_ESTADUAL: TFloatField;
    QCidades: TADOQuery;
    QPos: TADOQuery;
    QCidadesCID_ID: TIntegerField;
    QCidadesESTADO_ID: TIntegerField;
    QCidadesNOME: TStringField;
    Label49: TLabel;
    EdCNPJ: TEdit;
    QPosPOS_SERIAL_NUMBER: TStringField;
    QPosCRED_ID: TIntegerField;
    QPosUSUARIO_RECARGA: TStringField;
    QPosTIPO_RECARGA: TStringField;
    QPosATU_SERVER_IP: TStringField;
    QPosMODELO_POS: TStringField;
    QPosCAPTURA: TStringField;
    lbl8: TLabel;
    cbbLiberado: TComboBox;
    QPosVERSAO_NAVS: TStringField;
    QPosVERSAO_OS: TStringField;
    QPosVERSAO_EOS: TStringField;
    QPosATUALIZOU_LUA: TStringField;
    QPosIP_TEF: TStringField;
    QPosIP_TEF_GERENCIAL: TStringField;
    QPosPORTA_TEF: TStringField;
    QPosCODLOJA: TStringField;
    QPosID_TEMINAL: TStringField;
    QCadastroCRED_ID_TEF: TIntegerField;
    QCadastroNOME: TStringField;
    QCadastroLIBERADO: TStringField;
    QCadastroCODACESSO: TIntegerField;
    QCadastroBANCO: TIntegerField;
    QCadastroAPAGADO: TStringField;
    QCadastroCONTRATO: TIntegerField;
    QCadastroCGC: TStringField;
    QCadastroINSCRICAOEST: TStringField;
    QCadastroFANTASIA: TStringField;
    QCadastroTELEFONE1: TStringField;
    QCadastroTELEFONE2: TStringField;
    QCadastroFAX: TStringField;
    QCadastroCONTATO: TStringField;
    QCadastroENVIAR_EMAIL: TStringField;
    QCadastroEMAIL: TStringField;
    QCadastroHOMEPAGE: TStringField;
    QCadastroENDERECO: TStringField;
    QCadastroNUMERO: TIntegerField;
    QCadastroBAIRRO: TStringField;
    QCadastroCIDADE: TStringField;
    QCadastroCEP: TStringField;
    QCadastroESTADO: TStringField;
    QCadastroSENHA: TStringField;
    QCadastroAGENCIA: TStringField;
    QCadastroCONTACORRENTE: TStringField;
    QCadastroOBS1: TStringField;
    QCadastroOBS2: TStringField;
    QCadastroCORRENTISTA: TStringField;
    QCadastroCPF: TStringField;
    QCadastroDTAPAGADO: TDateTimeField;
    QCadastroDTALTERACAO: TDateTimeField;
    QCadastroOPERADOR: TStringField;
    QCadastroDTCADASTRO: TDateTimeField;
    QCadastroOPERCADASTRO: TStringField;
    QCadastroCOMPLEMENTO: TStringField;
    QPosPOS_PLANTAO_CARD: TStringField;
    procedure ButIncluiClick(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
    procedure ButBuscaClick(Sender: TObject);
    procedure ButNovoCodClick(Sender: TObject);
    procedure ButLimpaSenhaClick(Sender: TObject);
    procedure Valida;
    procedure ButGravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QCadastroAfterScroll(DataSet: TDataSet);
    procedure ButApagaClick(Sender: TObject);
    procedure QCadastroAfterInsert(DataSet: TDataSet);
    procedure TabFichaExit(Sender: TObject);
    procedure QDatasFechaBeforeDelete(DataSet: TDataSet);
    procedure QDatasFechaAfterPost(DataSet: TDataSet);
    procedure TabDatasHide(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QCadastroAfterPost(DataSet: TDataSet);
    procedure ColocarCodAcessoManual1Click(Sender: TObject);
    procedure PopupAlteraIDPopup(Sender: TObject);
    procedure AlterarIddoFornecedor1Click(Sender: TObject);
    procedure PopCodAcessoPopup(Sender: TObject);
    procedure AltLineardeComissoporEmpresa1Click(Sender: TObject);
    procedure QDatasFechaBeforeInsert(DataSet: TDataSet);
    procedure QEmpresLibBeforeInsert(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure dbGridPosColExit(Sender: TObject);
    procedure btnGravarPosClick(Sender: TObject);
    procedure btnCancelarPosClick(Sender: TObject);
    procedure tbCadPosHide(Sender: TObject);
    procedure tbCadPosShow(Sender: TObject);
    procedure qPosBeforePost(DataSet: TDataSet);
    procedure btnInserirPosClick(Sender: TObject);
    procedure dbGridPosTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure btnExcluirPosClick(Sender: TObject);
    procedure qEstadosAfterScroll(DataSet: TDataSet);
    procedure DSEstadosDataChange(Sender: TObject; Field: TField);
    procedure GridHistoricoTitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure btnHistClick(Sender: TObject);
    procedure QCadastroAfterOpen(DataSet: TDataSet);
    procedure QCadastroAfterClose(DataSet: TDataSet);
    procedure DSPosStateChange(Sender: TObject);
  private
    LogDataFecha: TLog;
    datas_alteradas : Boolean;
    procedure TrocarCred_ID(new_CredId: string ; duplica : Boolean = False);
    function ExisteEmprBloq(cred_id, empres_id: integer): boolean;
    procedure GravaComissaoEmpres(cred_id, empres_id: integer;
      valor: currency);
    function ExisteComissEmpr(cred_id, empres_id: integer): boolean;
    procedure AddCabecalhoErro(var Arquivo : TStringList);
    procedure carregaICMS_ESTADUAL(estado : string = '');
    { Private declarations }
  public
    function geraID: Integer;
    procedure limparCampos;
  end;

var
  FCadCredTef: TFCadCredTef;
  cLiberado : string;
  tDescontar, fLiberado : string;
  cComissao : currency;
  SavePlace : TBookmark;
  SavePlace1 : TBookmark;
  vvelho, vnovo, sqlQuery : String;
implementation

uses DM, Math, StrUtils, UMenu, cartao_util,
  UCons_Autor, UAltLinCombo, UDBGridHelper, UValidacao, UaltContatoCred,
  uPgtoEstabAberto;

{$R *.dfm}
procedure TFCadCredTef.AddCabecalhoErro(var Arquivo : TStringList);
begin
if Pos('---'+DateTimeToStr(DMConexao.fnData)+'---',Arquivo.Text) <= 0 then
  //Adiciono na parte de cima o cabe�alho e logo em seguida o Arquivo
  Arquivo.Text := '----------------'+#13+'---'+DateTimeToStr(Now)+'---'+#13+'----------------' + #13+ Arquivo.Text;
end;

procedure TFCadCredTef.carregaICMS_ESTADUAL(estado : string);
begin
{  if not QCadastro.IsEmpty then begin
    qEstados.Close;
    if Trim(estado) <> '' then
      qEstados.Params[0].AsString := estado
    else
      qEstados.Params[0].AsString := QCadastroESTADO.AsString;
    qEstados.Open;
  end;}      
end;

procedure TFCadCredTef.Valida;
begin
  //Nome
  if Trim(QCadastroNOME.AsString) = '' then
  begin
    MsgInf('Digite o Nome!');
    PageControl2.ActivePageIndex := 0;
    DBEdit2.SetFocus;
    Abort;
  end;
  //Nome fantasia
  if Trim(QCadastroFANTASIA.AsString) = '' then
    QCadastroFANTASIA.AsString:= copy(QCadastroNOME.AsString,1,45);
  //Codigo de Acesso
  if Trim(QCadastroCODACESSO.AsString) = '' then
    QCadastroCODACESSO.AsString := IntToStr(100+QCadastroCRED_ID_TEF.AsInteger)+FormatFloat('00', DigitoControle(100+QCadastroCRED_ID_TEF.AsInteger));
  //Liberado
  if Trim(QCadastroLIBERADO.AsString) = '' then
    QCadastroLIBERADO.AsString := 'N'
  else if ((QCadastroLIBERADO.AsString <> 'S') and (QCadastroLIBERADO.AsString <> 'N')) then
    QCadastroLIBERADO.AsString := 'N';
  //Contrato
  if Trim(QCadastroCONTRATO.AsString) = '' then
    QCadastroCONTRATO.AsString := QCadastroCRED_ID_TEF.AsString;
  If Trim(QCadastroESTADO.AsString) = '' then
  begin
    MsgInf('Selecione o estado do estabelecimento!');
    PageControl2.ActivePageIndex := 0;
    //cbbUF.SetFocus;
    dbLkpEstados.SetFocus;
    Abort;
  end;
  //Banco
  if QCadastroBANCO.IsNull then QCadastroBANCO.AsInteger := 0;
  //Data Altera��o
  QCadastroDTALTERACAO.AsDateTime := Date;
  //Senha e Data do Cadastro
  if QCadastro.State = dsInsert then
  begin
    QCadastroDTCADASTRO.AsDateTime := Date;
  end;
end;

procedure TFCadCredTef.ButIncluiClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TFCadCredTef.ButBuscaClick(Sender: TObject);
begin
  inherited;
  QCadastro.Close;
  QCadastro.Sql.Clear;
  QCadastro.Sql.Add(' Select cc.* from credenciados_tef cc where coalesce(cc.apagado,''N'') <> ''S'' ');
  if cbbLiberado.ItemIndex > 0 then
    if cbbLiberado.ItemIndex = 1 then
      QCadastro.SQL.Add(' and cc.liberado = ''S''')
        else
          QCadastro.SQL.Add(' and cc.liberado = ''N''')
  else
  if Trim(EdCod.Text) <> '' then
    QCadastro.Sql.Add(' and cc.cred_id in ('+EdCod.Text+')');
  if Trim(EdFanta.Text) <> '' then
    QCadastro.Sql.Add(' and cc.fantasia like ''%'+EdFanta.Text+'%''');
  if Trim(EdNome.Text) <> '' then
    QCadastro.Sql.Add(' and cc.nome like ''%'+EdNome.Text+'%''');
  if Trim(EdCNPJ.Text) <> '' then
     QCadastro.SQL.Add(' and cc.cgc like''%'+EdCNPJ.Text+'%''');
  if Trim(EdCidade.Text) <> '' then
    QCadastro.Sql.Add(' and cc.cidade like ''%'+EdCidade.Text+'%''');
  QCadastro.Sql.Add(' order by cc.nome ');
  QCadastro.Open;
  if not QCadastro.IsEmpty then
  begin
    Self.TextStatus := 'Estabelecimento: ['+QCadastroCRED_ID_TEF.AsString+'] - '+QCadastroNOME.AsString;
    DBGrid1.SetFocus;
  end
  else
    EdCod.SetFocus;
  EdCod.Clear;
  EdFanta.Clear;
  EdNome.Clear;
  EdCidade.Clear;
  cbbLiberado.ItemIndex := 0;
end;

procedure TFCadCredTef.ButNovoCodClick(Sender: TObject);
var codAcesso : String;
begin
  if not QCadastro.IsEmpty then
    if Application.MessageBox(Pchar('Aten��o ser� gerado um novo c�digo de acesso para o estabelecimento.'+slineBreak+'Confirma esta opera��o?'),'Aten��o',MB_YESNO+MB_DEFBUTTON1+MB_ICONQUESTION) = IDYes then begin
      codAcesso := QCadastroCODACESSO.AsString;
      QCadastro.Edit;
    repeat
      Randomize;
      QCadastroCODACESSO.AsString := IntToStr(QCadastroCRED_ID_TEF.AsInteger)+ IntToStr(Random(10));
      DMConexao.AdoQry.SQL.Text := 'select Cred_id from credenciados where coalesce(apagado,''N'') <> ''S'' and codacesso = '+QCadastroCODACESSO.AsString;
      DMConexao.AdoQry.Open;
    until DMConexao.AdoQry.IsEmpty;
    DMConexao.AdoQry.Close;
    QCadastro.Post;
    DMConexao.ExecuteSql('UPDATE TRANSACOES SET CODACESSO = ' + QCadastroCODACESSO.AsString + ' WHERE CODACESSO = ' + codAcesso);
  end;
end;

procedure TFCadCredTef.ButLimpaSenhaClick(Sender: TObject);
begin
  QCadastro.Edit;
  QCadastro.FieldByName('SENHA').AsString :=  Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.Post;
end;

procedure TFCadCredTef.ButGravaClick(Sender: TObject);
begin
  Valida;
  inherited;
  
end;

procedure TFCadCredTef.FormCreate(Sender: TObject);
begin
  chavepri := 'cred_id_tef';
  detalhe  := 'Cred ID TEF: ';
  inherited;
  qEstados.Open;
  qCidades.Open;
  QCadastro.Open;
end;

procedure TFCadCredTef.QCadastroAfterScroll(DataSet: TDataSet);
var bm : TBookmark;
begin
  inherited;
  if not QCadastro.ControlsDisabled then Self.TextStatus := 'Estabelecimento: ['+QCadastroCRED_ID_TEF.AsString+'] - '+QCadastroNOME.AsString;
  //if QCadastroCIDADE.AsString <> '' then
  //  dbLkpCidades.KeyValue := LowerCase(QCadastroCIDADE.AsString);
  //carregaICMS_ESTADUAL;
end;

procedure TFCadCredTef.ButApagaClick(Sender: TObject);
var cred_id : string;
begin
  if not QCadastro.IsEmpty then
  begin
    cred_id := QCadastroCRED_ID_TEF.AsString;
    DMConexao.AdoQry.Close;
    DMConexao.AdoQry.Sql.Text := 'Select count(autorizacao_id) as num from contacorrente where baixa_credenciado <> ''S'' and cred_id = '+QCadastroCRED_ID_TEF.AsString;
    DMConexao.AdoQry.Open;
    If DMConexao.AdoQry.FieldByName('num').AsInteger > 0 then
    begin
      Application.MessageBox('Esse estabelecimento possui movimenta��o de conta corrente.'+#13+'N�o � possivel excluir este estabelecimento.','Aten��o',MB_OK+MB_DEFBUTTON1+MB_ICONINFORMATION);
    end
    else
      inherited;
      DMConexao.AdoQry.Close;

      DMConexao.AdoQry.SQL.Clear;
      DMConexao.AdoQry.SQL.Add('UPDATE CREDENCIADOS SET DTAPAGADO = CURRENT_TIMESTAMP WHERE CRED_ID = '+cred_id+'');
      DMConexao.AdoQry.ExecSQL;
      DMConexao.AdoQry.Close;
      QCadastro.Requery;
  end;
end;

procedure TFCadCredTef.QCadastroAfterInsert(DataSet: TDataSet);
begin
  inherited;

  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SCRED_ID_TEF AS CRED_ID_TEF');
  DMConexao.AdoQry.Open;
  QCadastroCRED_ID_TEF.AsInteger            := DMConexao.AdoQry.FieldByName('CRED_ID_TEF').AsInteger;
  QCadastroCODACESSO.AsString           := IntToStr(QCadastroCRED_ID_TEF.AsInteger)+FormatFloat('0', DigitoControle(QCadastroCRED_ID_TEF.AsInteger));
  QCadastroAPAGADO.AsString             := 'N';
  QCadastroLIBERADO.AsString            := 'S';
  QCadastroSENHA.AsString               := Crypt('E', '1111', 'BIGCOMPRAS');
  QCadastro.FieldByName('DTCADASTRO').AsString := DateTimeToStr(Now);
  QCadastro.FieldByName('OPERCADASTRO').AsString := Operador.Nome;
  QCadastro.Tag := 1;//para informar que est� inserindo.
end;

procedure TFCadCredTef.TabFichaShow(Sender: TObject);
var s : String;
begin
  inherited;
  DBEdit2.SetFocus;
  if not QCadastro.IsEmpty then begin
    QCadastro.Edit;
    s := QCadastroCIDADE.AsString;
    QCadastroCIDADE.AsString := '';
    QCadastroCIDADE.AsString := s;
    QCadastro.Cancel;
  end;
end;

procedure TFCadCredTef.TabFichaExit(Sender: TObject);
begin
  inherited;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFCadCredTef.QDatasFechaBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  ShowMessage('N�o � poss�vel excluir.');
  SysUtils.Abort;
end;

procedure TFCadCredTef.QDatasFechaAfterPost(DataSet: TDataSet);
begin
  inherited;
  datas_alteradas := True;
end;

procedure TFCadCredTef.TabDatasHide(Sender: TObject);
begin
  inherited;
{
  if datas_alteradas then begin
     Application.MessageBox(PChar('Alguma(s) data(s) foi(ram) alterada(s).'+#13+'A atualiza��o de datas ser� efetuada....'),'Aten��o..',MB_ICONINFORMATION+MB_OK);
     try
        Screen.Cursor := crHourGlass;
        DM1.Query1.Close; //update somente para que o trigger de datas da contacorrente busque as novas datas.
        DM1.Query1.SQL.Text := 'Update contacorrente set autorizacao_id = autorizacao_id where cred_id = '+QCadastroCRED_ID.AsString;
        DM1.Query1.ExecSQL;
        datas_alteradas := False;
        Screen.Cursor := crDefault;
        Application.MessageBox('Datas atualizadas com sucesso....','Confirma��o..',MB_ICONINFORMATION+MB_OK);
     except on E:Exception do
        ShowMessage('Erro ao atualizar as datas. erro: '+E.Message);
     end;
  end;
}
end;

procedure TFCadCredTef.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if datas_alteradas then TabDatasHide(nil);
  inherited;
end;

procedure TFCadCredTef.QCadastroAfterPost(DataSet: TDataSet);
begin
  inherited;
  if QCadastro.Tag = 1 then begin
     try
        QCadastro.Tag := 0;
     except on E:Exception do
        ShowMessage('Erro ao criar as datas do estabelecimento.');
     end;
  end;

end;

procedure TFCadCredTef.ColocarCodAcessoManual1Click(Sender: TObject);
var ncod : string;
begin
  inherited;
  if MsgSimNao('Deseja alterar o c�digo de acesso?') then begin
     ncod := SoNumero( InputBox('Informe o novo c�d. acesso','Nov c�d. acesso','') );
     if ncod <> '' then begin
        DMConexao.AdoQry.Close;
        DMConexao.AdoQry.Sql.Text := 'Select nome from credenciados where codacesso = '+ncod;
        DMConexao.AdoQry.Open;
        if not DMConexao.AdoQry.IsEmpty then
           MsgErro('Ja exite um estabelecimento com este c�digo de acesso, nome: '+DMConexao.AdoQry.Fields[0].AsString)
        else begin
           QCadastro.Edit;
           QCadastroCODACESSO.AsInteger := StrToInt(ncod);
           QCadastro.Post;
        end;
        DMConexao.AdoQry.Close;
     end;
  end
end;

procedure TFCadCredTef.PopupAlteraIDPopup(Sender: TObject);
begin
  inherited;
  PopupAlteraID.Items[0].Enabled := not QCadastro.IsEmpty;
end;



procedure TFCadCredTef.AlterarIddoFornecedor1Click(Sender: TObject);
var fornew : string;
begin
  inherited;
if MsgSimNao('Deseja alterar o c�digo do estabelecimento?') then
   fornew:= SoNumero( InputBox('Digite o novo Id para o Estabelecimento','Novo ID','') );
   if Trim(fornew) <> '' then begin
      TrocarCred_ID(fornew);
   end;
end;

procedure TFCadCredTef.TrocarCred_ID(new_CredId:String ; duplica : Boolean);
var existe : boolean; nome:string;
old_credID : string;
begin
   if not duplica then begin
     DMConexao.AdoQry.Close;
     DMConexao.AdoQry.Sql.Text := 'Select nome from credenciados where cred_id = '+new_CredId;
     DMConexao.AdoQry.Open;
     existe := not DMConexao.AdoQry.IsEmpty;
     nome := DMConexao.AdoQry.Fields[0].AsString;
     DMConexao.AdoQry.Close;
     if existe then begin
        MsgErro('Ja existe um estabelecimento cadastrado com este Id, Nome: '+nome);
     end
   end else begin
      Screen.Cursor := crHourGlass;
      DMConexao.AdoCon.BeginTrans;
      try
         old_credID := QCadastroCRED_ID_TEF.AsString;
         if not duplica then begin
           QCadastro.Edit;
           QCadastroCRED_ID_TEF.AsString := new_CredId;
           QCadastro.Post;
         end;
         with DMConexao do begin
            //Atualiza todas tabelas que usam empres_id.
            AdoQry.SQL.Text := 'update back_alt_linear set chave_prim = '+new_CredId+' where chave_prim = '+old_credID+' and tabela = ''CREDENCIADOS'' ';
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update cad_conferencia set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update conferencia set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_adm = '+new_CredId+' where cod_cred_adm = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_baixa = '+new_CredId+' where cod_cred_baixa = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update config set cod_cred_cpmf = '+new_CredId+' where cod_cred_cpmf = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update config_webfornecedores set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update contacorrente set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update desconto set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update dia_fecha_cred set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.SQL.Text := 'update logs set id = '+new_CredId+' where id = '+old_credID+' and cadastro = ''Cadastro de Estabelecimentos'' ';
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update eventos set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update fidelidade set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update lancamentos set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update autor_transacoes set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update transacoes set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
            AdoQry.Sql.Text := 'update pagamento_cred set cred_id = '+new_CredId+' where cred_id = '+old_credID;
            AdoQry.ExecSQL;
         end;
         DMConexao.AdoCon.CommitTrans;
         Screen.Cursor := crDefault;
         MsgInf('Altera��o Conclu�da com sucesso!');
      except
         on e:Exception do begin
            QCadastro.Edit;
            QCadastroCRED_ID_TEF.AsString := old_credID;
            QCadastro.Post;
            DMConexao.AdoCon.RollbackTrans;
            Screen.Cursor := crDefault;
            MsgErro('Problemas na altera��o do ID, erro: '+e.Message);
         end;
      end;
   end;
Screen.Cursor := crDefault;
end;
procedure TFCadCredTef.PopCodAcessoPopup(Sender: TObject);
begin
  inherited;
  PopCodAcesso.Items[0].Enabled := not QCadastro.IsEmpty;
end;

function TFCadCredTef.ExisteEmprBloq(cred_id: integer; empres_id: integer):boolean;
begin
  if DMConexao.ExecuteScalar(' select empres_id from cred_emp_Lib where empres_id = '+
    IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id),0) = 0 then
    Result:= False
  else
    Result:= True;
end;

function TFCadCredTef.ExisteComissEmpr(cred_id: integer; empres_id: integer):boolean;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add(' select empres_id from cred_emp_comissao where empres_id = '+IntToStr(empres_id)+' ');
  DMConexao.AdoQry.SQL.Add(' and cred_id = '+IntToStr(cred_id)+'');
  DMConexao.AdoQry.Open;
  if DMConexao.AdoQry.Fields[0].AsString = '' then
     Result:= false
  else
    Result:= true;

  //if DMConexao.ExecuteScalar(' select empres_id from cred_emp_comissao where empres_id = '+
  //  IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id)) = 0 then
 //   Result:= true
  //else
   // Result:= false;
end;

procedure TFCadCredTef.AltLineardeComissoporEmpresa1Click(Sender: TObject);
var empres_id: Integer;
  valor: Currency;
begin
  inherited;
  FAltLinCombo := TFAltLinCombo.Create(self);
  FAltLinCombo.lbCombo.Caption := 'Selecione uma empresa';
  FAltLinCombo.lbValor.Caption := 'Valor da Comiss�o';
  FAltLinCombo.Caption := 'Alt. Linear de Comiss�o por Empresa';
  FAltLinCombo.Q.Close;
  FAltLinCombo.Q.SQL.Clear;
  FAltLinCombo.Q.SQL.Add(' select empres_id, nome from empresas where coalesce(apagado,"N") <> "S" order by nome ');
  FAltLinCombo.Q.Open;
  FAltLinCombo.Combo.LookupDisplay := 'NOME';
  FAltLinCombo.Combo.LookupField := 'EMPRES_ID';
  FAltLinCombo.ShowModal;
  if FAltLinCombo.ModalResult = mrOk then
  begin
    QCadastro.First;
    empres_id := FAltLinCombo.Combo.KeyValue;
    valor := FAltLinCombo.edValorReal.Value;
    while not QCadastro.Eof do
    begin
      GravaComissaoEmpres(QCadastroCRED_ID_TEF.AsInteger,empres_id,valor);
      QCadastro.Next;
    end;
    MsgInf('Altera��o linear efetuada com sucesso!');
  end;
  QCadastro.First;
  FAltLinCombo.Free;
end;

procedure TFCadCredTef.GravaComissaoEmpres(cred_id: Integer; empres_id: Integer; valor:Currency);
var sql: String;
  vlrantigo: Currency;
begin
  vlrantigo:= 0;
  if ExisteComissEmpr(cred_id,empres_id) then
  begin
    vlrantigo:= DMConexao.ExecuteScalar('select COMISSAO from CRED_EMP_LIB where empres_id = '+IntToStr(empres_id)+' and cred_id = '+IntToStr(cred_id),0);
    sql :=  'update CRED_EMP_COMISSAO set COMISSAO = '+FormatDimIB(valor)+
    ' where CRED_ID = '+IntToStr(cred_id)+' and EMPRES_ID = '+IntToStr(empres_id);
  end
  else
  begin
    sql :=  'insert into CRED_EMP_COMISSAO(CRED_ID, EMPRES_ID, COMISSAO) '+
    ' values('+IntToStr(cred_id)+','+IntToStr(empres_id)+', '+FormatDimIB(valor)+')';
  end;
  DMConexao.ExecuteSql(sql);
  DMConexao.GravaLog('FCadCred','Comissao por Emp.',FormatDinBR(vlrantigo),FormatDinBR(valor),
    Operador.Nome,'Altera��o',QCadastroCRED_ID_TEF.AsString,Self.Name);
end;


procedure TFCadCredTef.QDatasFechaBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadCredTef.QEmpresLibBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not Incluir then begin
     ShowMessage('Opera��o n�o permitida para o usu�rio.');
     SysUtils.Abort;
  end;
end;

procedure TFCadCredTef.limparCampos;
Var
i : Integer;
begin
  for i := 0 to ComponentCount -1 do
  begin
    if Components[i] is TEdit then
    begin
      TEdit(Components[i]).Text := '';
    end;

    if Components[i] is TMemo then
    begin
      TMemo(Components[i]).Clear;
    end;
  end;
end;

function TFCadCredTef.geraID: Integer;
begin
  DMConexao.AdoQry.Close;
  DMConexao.AdoQry.SQL.Clear;
  DMConexao.AdoQry.SQL.Add('SELECT NEXT VALUE FOR SATEND_ID_CRED');
  DMConexao.AdoQry.Open;
  Result := DMConexao.AdoQry.Fields[0].AsInteger;
end;

procedure TFCadCredTef.SpeedButton1Click(Sender: TObject);
var
  Resposta: TStringStream;
  TSConsulta: TStringList;
  XMLDocCEP: TXMLDocument;
  IdHTTP1: TIdHTTP;
begin
  inherited;
  if (DBEdit9.Text = '') or (Length(SoNumero(DBEdit9.Text)) <> 8) then
  begin
    Application.MessageBox('CEP nulo ou inv�lido.', 'Erro - Aviso do Sistema', mb_iconwarning+mb_ok);
    exit;
  end;
  Resposta   := TStringStream.Create('');
  TSConsulta := TStringList.Create;
  IdHTTP1:= TIdHTTP.Create(Self);
  IdHTTP1.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV2';
  TSConsulta.Values['&cep']  := SoNumero(DBEdit9.Text);
  TSConsulta.Values['&formato']  := 'xml';
  IdHTTP1.Post('http://cep.republicavirtual.com.br/web_cep.php?', TSConsulta, Resposta);
  TSConsulta.Free;
  IdHTTP1.Free;
  XMLDocCEP:= TXMLDocument.Create(self);
  XMLDocCEP.Active := True;
  XMLDocCEP.Encoding := 'iso-8859-1';
  XMLDocCEP.LoadFromStream(Resposta);
  try
    try
      QCadastro.Edit;
      QCadastro.FieldByName('ENDERECO').AsString := XMLDocCEP.DocumentElement.ChildNodes['tipo_logradouro'].NodeValue +
                                                    ' ' + XMLDocCEP.DocumentElement.ChildNodes['logradouro'].NodeValue;
      QCadastro.FieldByName('BAIRRO').AsString      := XMLDocCEP.DocumentElement.ChildNodes['bairro'].NodeValue;
      QCadastro.FieldByName('CIDADE').AsString   := XMLDocCEP.DocumentElement.ChildNodes['cidade'].NodeValue;
      QCadastro.FieldByName('ESTADO').AsString   := XMLDocCEP.DocumentElement.ChildNodes['uf'].NodeValue;
    except
      ShowMessage('Foi encontrado um erro na busca!'+sLineBreak+'Mensagem Original: '+XMLDocCEP.DocumentElement.ChildNodes['resultado_txt'].NodeValue);
    end;
  finally
    Resposta.Free;
    XMLDocCEP.Active := False;
    XMLDocCEP.Free;
  end;
  DBEdit15.SetFocus;
end;

procedure TFCadCredTef.dbGridPosColExit(Sender: TObject);
begin
  inherited;
  if qPos.State in [dsEdit] then qPos.Post;
end;

procedure TFCadCredTef.btnGravarPosClick(Sender: TObject);
begin
  inherited;
  qPos.Post;  
end;

procedure TFCadCredTef.btnCancelarPosClick(Sender: TObject);
begin
  inherited;
  qPos.Cancel;
end;

procedure TFCadCredTef.btnExcluirPosClick(Sender: TObject);
begin
  inherited;
  qPos.Delete;
end;

procedure TFCadCredTef.tbCadPosHide(Sender: TObject);
begin
  inherited;
  QPos.Close;
end;

procedure TFCadCredTef.tbCadPosShow(Sender: TObject);
begin
  inherited;
  QPos.Close;
  QPos.Parameters.ParamByName('cred_id').Value := QCadastroCRED_ID_TEF.AsInteger;
  QPos.Open;
end;

procedure TFCadCredTef.qPosBeforePost(DataSet: TDataSet);
begin
  inherited;
  qPosCRED_ID.AsInteger := QCadastroCRED_ID_TEF.AsInteger;
  qPosPOS_PLANTAO_CARD.AsString := 'N';
end;

procedure TFCadCredTef.btnInserirPosClick(Sender: TObject);
begin
  inherited;
  qPos.Append;
  dbGridPos.SetFocus;
end;

procedure TFCadCredTef.dbGridPosTitleBtnClick(Sender: TObject; ACol: Integer;
  Field: TField);
begin
  inherited;
    try
    if Pos(Field.FieldName,QPos.Sort) > 0 then begin
       if Pos(' DESC',QPos.Sort) > 0 then QPos.Sort := Field.FieldName
                                                  else QPos.Sort := Field.FieldName+' DESC';
    end
    else QPos.Sort := Field.FieldName;
    except
    end;
end;

procedure TFCadCredTef.qEstadosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  //carregaICMS_ESTADUAL(cbbUf.Text);
end;

procedure TFCadCredTef.DSEstadosDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (qCadastro.State in [dsInsert, dsEdit]) and (not qCidades.Locate('NOME',dbLkpCidades.KeyValue,[])) then
     qCadastroCIDADE.AsString := '';
end;


procedure TFCadCredTef.GridHistoricoTitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
   try
    if Pos(Field.FieldName,QHistorico.Sort) > 0 then
    begin
      if Pos(' DESC',QHistorico.Sort) > 0 then
        QHistorico.Sort := Field.FieldName
      else
        QHistorico.Sort := Field.FieldName+' DESC';
    end
    else
      QHistorico.Sort := Field.FieldName;
  except
  end;

end;

procedure TFCadCredTef.btnHistClick(Sender: TObject);
begin
  inherited;
//  if not QCadastro.IsEmpty then begin
//    QLogOcorrencias.Close;
//    QLogOcorrencias.SQL.Clear;
//    QLogOcorrencias.SQL.Add('SELECT * FROM logs_ocorrencias WHERE id = '+QCadastroCRED_ID.AsString+'');
//    if((dtIniHistorico.Date > 0) and (dtFimHistorico.Date > 0)) then begin
//        QLogOcorrencias.SQL.Add(' and convert(varchar(10), DATA_HORA, 103) between '+QuotedStr(dtIniHistorico.Text)+' and '+QuotedStr(dtFimHistorico.Text));
//        QLogOcorrencias.SQL.Add(' and janela  = ''TabAtendimento''');
//    end;
//    QLogOcorrencias.Open;
//  end;
end;


procedure TFCadCredTef.QCadastroAfterOpen(DataSet: TDataSet);
begin
  inherited;
   QBanco.Open;
end;

procedure TFCadCredTef.QCadastroAfterClose(DataSet: TDataSet);
begin
  inherited;
  QBanco.Close;
end;

procedure TFCadCredTef.DSPosStateChange(Sender: TObject);
begin
  inherited;
  btnGravarPos.Enabled := dsPos.State in [dsInsert, dsEdit];
  btnCancelarPos.Enabled := dsPos.State in [dsInsert, dsEdit];
  btnExcluirPos.Enabled := not (dsPos.State in [dsInsert, dsEdit]);
  btnInserirPos.Enabled := not (dsPos.State in [dsInsert, dsEdit]);
end;

end.
