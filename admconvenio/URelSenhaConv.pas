unit URelSenhaConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, DB, ADODB, Grids, DBGrids, JvExDBGrids, JvDBGrid, StdCtrls,
  Mask, JvExMask, JvToolEdit, JvMemoryDataset, Menus, Buttons, ExtCtrls,
  ComCtrls, JvExComCtrls, JvProgressBar, JvDBProgressBar, JvExControls,
  JvLabel, JvStatusBar, JvDialogs, frxClass, frxExportPDF, frxDBSet;

type
  TFRelSenhaConv = class(TF1)
    Panel1: TPanel;
    DataIni: TJvDateEdit;
    DataFin: TJvDateEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Panel2: TPanel;
    btnMarDes: TButton;
    btnMarTod: TButton;
    btnDesTod: TButton;
    QConv: TADOQuery;
    DSConv: TDataSource;
    MDConv: TJvMemoryData;
    lblResult: TLabel;
    cbFiltraPeriodo: TCheckBox;
    MDConvconv_id: TIntegerField;
    MDConvtitular: TStringField;
    MDConvtelefone: TStringField;
    MDConvcelular: TStringField;
    MDConvendereco: TStringField;
    MDConvnumero: TIntegerField;
    MDConvbairro: TStringField;
    MDConvcidade: TStringField;
    MDConvestado: TStringField;
    MDConvcep: TStringField;
    MDConvempres_id: TIntegerField;
    MDConvfantasia: TStringField;
    JvStatusBar1: TJvStatusBar;
    lbStatus: TJvLabel;
    BitBtn1: TBitBtn;
    MDConvmarcado: TBooleanField;
    frxPDFExport1: TfrxPDFExport;
    sd: TJvSaveDialog;
    bntGerarPDF: TBitBtn;
    frxReport1: TfrxReport;
    dbConv: TfrxDBDataset;
    btnAlterarSenhas: TBitBtn;
    QCART: TADOQuery;
    QCARTSENHA: TStringField;
    QCARTCARTAO_ID: TIntegerField;
    QCARTCONV_ID: TIntegerField;
    QCARTCODCARTIMP: TStringField;
    QCARTTITULAR: TStringField;
    cbSenhaMaior4Digitos: TCheckBox;
    GroupBox2: TGroupBox;
    cbAlteraTodasSenhasDaEmpresa: TCheckBox;
    txtEmpresID: TEdit;
    btnAlterarSenhasNovo: TBitBtn;
    Label2: TLabel;
    cbFiltraModeloCartao: TCheckBox;
    lblDrogabella: TLabel;
    lblPlantao: TLabel;
    lblAlimentacao: TLabel;
    lblRefeicao: TLabel;
    lblMaxCard: TLabel;
    lblTotal: TLabel;
    procedure cbFiltraPeriodoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnMarDesClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure btnMarTodClick(Sender: TObject);
    procedure btnDesTodClick(Sender: TObject);
    procedure bntGerarPDFClick(Sender: TObject);
    procedure JvDBGrid1TitleBtnClick(Sender: TObject; ACol: Integer;
      Field: TField);
    procedure btnAlterarSenhasClick(Sender: TObject);
    procedure btnAlterarSenhasNovoClick(Sender: TObject);
    procedure cbAlteraTodasSenhasDaEmpresaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure cbAlteraTodasSenhasDaEmpresaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelSenhaConv: TFRelSenhaConv;
  somaConveniados,somaConveniadosMaiorQuatro : Integer;

implementation

uses cartao_util;

{$R *.dfm}

procedure TFRelSenhaConv.cbFiltraPeriodoClick(Sender: TObject);
begin
  inherited;
  if cbFiltraPeriodo.Checked then begin
     DataIni.Enabled := True;
     DataFin.Enabled := True;
  end
     else
     begin
     DataIni.Enabled := False;
     DataFin.Enabled := False;
     end;

end;

procedure TFRelSenhaConv.BitBtn1Click(Sender: TObject);
var senhaDecriptada,senhaDoisDigitos,senhaDoisUltimos,quatroUltimosDigitoscartao : String;
    contSenhasAlteradasBella,contotal ,contSenhasAlteradasPlantao,contSenhasAlteradasMaxcard, contSenhasAlteradasAlimentacao, contSenhasAlteradasRefeicao, contModeloDeCartoes : Integer;
begin
  inherited;
  lblResult.Caption := '';
  Screen.Cursor := crHourGlass;
  if cbFiltraPeriodo.Checked = True then
  begin
    QConv.SQL.Clear;
    QConv.SQL.Add('SELECT CART.USU_SENHA,CART.EMP_FOR_ID AS CODCARTIMP FROM USUARIOS_WEB CART WHERE CART.USU_APAGADO = ''N'' and CART.USU_LIBERADO = ''S''');
    //QConv.SQL.Add(' and CART.DTCADASTRO between '+QuotedStr(DataIni.Text)+' and '+QuotedStr(DataFin.Text)+'');
  end
  else if cbFiltraModeloCartao.Checked then
  begin
      QConv.SQL.Clear;

      //QConv.SQL.Add('SELECT CART.USU_SENHA,CART.EMP_FOR_ID AS CODCARTIMP FROM USUARIOS_WEB CART WHERE CART.USU_APAGADO = ''N'' and CART.USU_LIBERADO = ''S''');
      QConv.SQL.Add(
        'SELECT DISTINCT(A.CARTAO_ID),b.CODCARTIMP,b.senha,E.MOD_CART_ID FROM CONTACORRENTE A '+
        'INNER JOIN CARTOES B ON A.CARTAO_ID = B.CARTAO_ID ' +
        'INNER JOIN CONVENIADOS C ON C.CONV_ID = A.CONV_ID ' +
        'INNER JOIN EMPRESAS E ON E.EMPRES_ID = B.EMPRES_ID ' +
        'WHERE DATA  BETWEEN ''01/01/2018'' AND GETDATE() ' +
        'AND C.LIBERADO = ''S'' AND E.MOD_CART_ID IN(1,2,3,4,7) ORDER BY MOD_CART_ID '
      );

      QConv.Open;
      QConv.First;
      somaConveniados := 0;
      while not QConv.Eof do
      begin
        if Length(QConv.Fields[1].AsString) = 16 then
        begin

           try
              quatroUltimosDigitoscartao := Copy(QConv.Fields[1].AsString, Length(QConv.Fields[1].AsString) - 3, Length(QConv.Fields[1].AsString));
              senhaDecriptada := Crypt('D',QConv.Fields[2].AsString,'BIGCOMPRAS');
            except
              on E : Exception do
                ShowMessage(E.ClassName+' error raised, with message : '+E.Message + senhaDecriptada);
            end;


          if Length(senhaDecriptada) = 4 then
          begin
            senhaDoisDigitos := Copy(senhaDecriptada,1,2);
            senhaDoisUltimos := Copy(senhaDecriptada,3,2);
            if ((senhaDecriptada = quatroUltimosDigitoscartao) or (senhaDoisDigitos = senhaDoisUltimos))  then
            begin
                contotal := contotal + 1;
                if QConv.Fields[3].AsInteger = 1 then
                begin
                  contSenhasAlteradasBella := contSenhasAlteradasBella + 1;
                end
                else if QConv.Fields[3].AsInteger  = 2 then
                begin
                  contSenhasAlteradasPlantao := contSenhasAlteradasPlantao + 1;
                end
                else if QConv.Fields[3].AsInteger = 3 then
                begin
                  contSenhasAlteradasAlimentacao := contSenhasAlteradasAlimentacao + 1;
                end
                else if QConv.Fields[3].AsInteger = 4 then
                begin
                  contSenhasAlteradasRefeicao := contSenhasAlteradasRefeicao + 1;
                end
                else begin
                  contSenhasAlteradasMaxcard := contSenhasAlteradasMaxcard + 1;
                end
            end;
          end;
        end;
        QConv.Next;
      end;



  end;
//  QConv.Open;
//  QConv.First;
//  somaConveniados := 0;
//  while not QConv.Eof do
//  begin
//    if Length(QConvCODCARTIMP.AsString) = 16 then
//    begin
//      quatroUltimosDigitoscartao := Copy(QConvCODCARTIMP.AsString, Length(QConvCODCARTIMP.AsString) - 3, Length(QConvCODCARTIMP.AsString));
//      if Crypt('D',QConvSENHA.AsString,'BIGCOMPRAS') <> quatroUltimosDigitoscartao then
//      begin
//        //senhaDecriptada := Crypt('D',QConvSENHA.AsString,'BIGCOMPRAS');
//  //      if (Length(senhaDecriptada)) > 4 then
//  //      begin
//  //        Inc(somaConveniadosMaiorQuatro,1);
//  //      end
//        //else
//        //begin
//       Inc(somaConveniados,1);
//        //end;
//      end;
//    end;
//    QConv.Next;
//  end;

  lblResult.Visible := True;
  //lblResult.Caption := lblResult.Caption + ' '+IntToStr(somaConveniados)+ ' registros. ' + IntToStr(somaConveniadosMaiorQuatro) + ' possuem senha mair que 4 d�gitos';
  lblDrogabella.Caption := lblDrogabella.Caption + IntToStr(contSenhasAlteradasBella);
  lblPlantao.Caption := lblPlantao.Caption + IntToStr(contSenhasAlteradasPlantao);
  lblAlimentacao.Caption := lblAlimentacao.Caption + IntToStr(contSenhasAlteradasAlimentacao);
  lblRefeicao.Caption := lblRefeicao.Caption + IntToStr(contSenhasAlteradasRefeicao);
  lblMaxCard.Caption := lblMaxCard.Caption + IntToStr(contSenhasAlteradasMaxcard);
  lblTotal.Caption := lblTotal.Caption + IntToStr(contotal);
  lbStatus.Caption := '';


end;

procedure TFRelSenhaConv.BitBtn1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  lbStatus.Caption := lbStatus.Caption + 'Aguarde enquanto efetuamos a pesquisa...';
end;

procedure TFRelSenhaConv.btnMarDesClick(Sender: TObject);
begin
  inherited;
  if MDConv.IsEmpty then Exit;
     MDConv.Edit;
     MDConvmarcado.AsBoolean := not MDConvmarcado.AsBoolean;
     MDConv.Post;

end;

procedure TFRelSenhaConv.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  //btnMarDes.Click;
end;

procedure TFRelSenhaConv.btnMarTodClick(Sender: TObject);
begin
  inherited;
  if MDConv.IsEmpty then Exit;
  MDConv.DisableControls;
  MDConv.First;
  while not MDConv.eof do begin
    MDConv.Edit;
    MDConvMarcado.AsBoolean := true;
    MDConv.Post;
    MDConv.Next;
  end;
  MDConv.EnableControls;

end;

procedure TFRelSenhaConv.btnDesTodClick(Sender: TObject);
begin
  inherited;
  if MDConv.IsEmpty then Exit;
  MDConv.DisableControls;
  MDConv.First;
  while not MDConv.eof do begin
    MDConv.Edit;
    MDConvMarcado.AsBoolean := false;
    MDConv.Post;
    MDConv.Next;
  end;
  MDConv.EnableControls;
end;

procedure TFRelSenhaConv.bntGerarPDFClick(Sender: TObject);
begin
  inherited;
  sd.Filter := '.pdf|.pdf';
  if sd.Execute then begin
    if ExtractFileExt(sd.FileName) = '' then
      sd.FileName := sd.FileName + '.pdf';
    frxPDFExport1.FileName := sd.FileName;
    frxReport1.ShowReport();
    frxReport1.PrepareReport();
    frxReport1.Export(frxPDFExport1);
  end;
end;

procedure TFRelSenhaConv.JvDBGrid1TitleBtnClick(Sender: TObject;
  ACol: Integer; Field: TField);
begin
  //inherited;
  MsgInf('teste');
  try
    if Pos(Field.FieldName,QConv.Sort) > 0 then begin
       if Pos(' DESC',QConv.Sort) > 0 then QConv.Sort := Field.FieldName
                                                  else QConv.Sort := Field.FieldName+' DESC';
    end
    else QConv.Sort := Field.FieldName;
    except
    end;

  QConv.First;
  MDConv.Open;
  MDConv.EmptyTable;
  MDConv.DisableControls;

//  while not QConv.Eof do
//  begin
//     if Crypt('D',QConvSENHA.AsString,'BIGCOMPRAS') <> '1111' then
//     begin
//       MDConv.Append;
//       MDConvtitular.AsString     := QConvTITULAR.AsString;
//       MDConvconv_id.AsInteger    := QConvCONV_ID.AsInteger;
//       MDConvtelefone.AsString    := QConvtelefone1.AsString;
//       MDConvcelular.AsString     := QConvcelular.AsString;
//       MDConvendereco.AsString    := QConvendereco.AsString;
//       MDConvnumero.AsInteger     := QConvnumero.AsInteger;
//       MDConvbairro.AsString      := QConvbairro.AsString;
//       MDConvcidade.AsString      := QConvcidade.AsString;
//       MDConvestado.AsString      := QConvestado.AsString;
//       MDConvcep.AsString         := QConvcep.AsString;
//       MDConvempres_id.AsInteger  := QConvempres_id.AsInteger;
//       MDConvfantasia.AsString    := QConvfantasia.AsString;
//
//       QConv.Next;
//     end;
  end;
//  MDConv.First;
//  MDConv.EnableControls;


procedure TFRelSenhaConv.btnAlterarSenhasClick(Sender: TObject);
var senha : String;
begin
  inherited;
  lblResult.Caption := '';
  Screen.Cursor := crHourGlass;
  QCART.SQL.Clear;
  QCART.SQL.Add('SELECT CART.SENHA,CART.CARTAO_ID,CART.TITULAR,CART.CODCARTIMP,CART.CONV_ID FROM CARTOES CART WHERE CART.APAGADO = ''N''');
  QCART.Open;
  QCART.First;
  somaConveniados := 0;
  while not QCART.Eof do
  begin
    try

      if QCARTTITULAR.AsString = 'N' then
      begin
        if not(QCARTCODCARTIMP.AsString = '') then
        begin
          if (not (QCARTSENHA.AsString = '')) and (Length(QCARTSENHA.AsString) > 2) then
          begin
              //altera a senha para os quatro �ltimos d�gitos do cart�o.
              QCART.Edit;
              senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
              QCARTSENHA.AsString := senha;
              QCART.Post;
              Inc(somaConveniados,1);
          end
          else
          begin
              //altera a senha para os quatro �ltimos d�gitos do cart�o.
              QCART.Edit;
              senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
              QCARTSENHA.AsString := senha;
              QCART.Post;
              senha := Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS');
              Inc(somaConveniados,1);
          end;
        end;

      end



      else
      begin
        if not(QCARTCODCARTIMP.AsString = '') then
        begin
          if (not (QCARTSENHA.AsString = '')) and (Length(QCARTSENHA.AsString) > 4) then
          begin
            if Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS') = '1111' then
            begin
              //altera a senha para os quatro �ltimos d�gitos do cart�o.
              QCART.Edit;
              //senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
              senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
              QCARTSENHA.AsString := senha;
              QCART.Post;
              //MsgInf('codcartimp: '+QCARTCODCARTIMP.AsString);
              senha := Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS');
              //MsgInf('senha descriptografada: ' + senha);
              Inc(somaConveniados,1);
            end
            else if Length(Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS')) <> 4  then
            begin
              //altera a senha para os quatro �ltimos d�gitos do cart�o.
              QCART.Edit;
              //senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
              senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
              QCARTSENHA.AsString := senha;
              QCART.Post;
              //MsgInf('codcartimp: '+QCARTCODCARTIMP.AsString);
              senha := Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS');
              //MsgInf('senha descriptografada: ' + senha);
              Inc(somaConveniados,1);
            end;
          end

          
          else
          begin
             //altera a senha para os quatro �ltimos d�gitos do cart�o.
              QCART.Edit;
              //senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
              senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
              QCARTSENHA.AsString := senha;
              QCART.Post;
              //MsgInf('codcartimp: '+QCARTCODCARTIMP.AsString);
              senha := Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS');
              //MsgInf('senha descriptografada: ' + senha);
              Inc(somaConveniados,1);
          end;
        end;
      end;
    Except
      MsgInf(QCARTSENHA.AsString+' cartao_id = '+QCARTCARTAO_ID.AsString);
    end;
    QCART.Next;
  end;

  lblResult.Visible := True;
  lblResult.Caption := lblResult.Caption + ' '+IntToStr(somaConveniados)+ ' registros alterados.';
  Screen.Cursor := crDefault;
  lbStatus.Caption := '';
end;

procedure TFRelSenhaConv.btnAlterarSenhasNovoClick(Sender: TObject);
var senha : String;
begin
  inherited;
  if not cbAlteraTodasSenhasDaEmpresa.Checked then
  begin
    MsgInf('� necess�rio selecionar um crit�rio para resetar a senha.');
    Exit;
  end;
  lblResult.Caption := '';
  Screen.Cursor := crHourGlass;
  QCART.SQL.Clear;
  QCART.SQL.Add('SELECT CART.SENHA,CART.CARTAO_ID,CART.TITULAR, ');
  QCART.SQL.Add('CART.CODCARTIMP,CART.CONV_ID ');
  QCART.SQL.Add('FROM CARTOES CART WHERE CART.APAGADO = ''N''');
  if(cbAlteraTodasSenhasDaEmpresa.Checked) then
    QCART.SQL.Add(' AND EMPRES_ID = '+txtEmpresID.Text);
  QCART.Open;
  QCART.First;
  somaConveniados := 0;
  while not QCART.Eof do
  begin
    try
      if not(QCARTCODCARTIMP.AsString = '') then
      begin
        //altera a senha para os quatro �ltimos d�gitos do cart�o.
        QCART.Edit;
        //senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, 13, 4),'BIGCOMPRAS');
        senha := Crypt('E',Copy(QCARTCODCARTIMP.AsString, Length(QCARTCODCARTIMP.AsString) - 3, Length(QCARTCODCARTIMP.AsString)),'BIGCOMPRAS');
        QCARTSENHA.AsString := senha;
        QCART.Post;
        //MsgInf('codcartimp: '+QCARTCODCARTIMP.AsString);
        senha := Crypt('D',QCARTSENHA.AsString,'BIGCOMPRAS');
        //MsgInf('senha descriptografada: ' + senha);
        Inc(somaConveniados,1);
      end;

    Except
      MsgInf(QCARTSENHA.AsString+' cartao_id = '+QCARTCARTAO_ID.AsString);
    end;
    QCART.Next;
  end;

  lblResult.Visible := True;
  lblResult.Caption := lblResult.Caption + ' '+IntToStr(somaConveniados)+ ' registros alterados para a empresa Cod: '+txtEmpresID.Text;
  Screen.Cursor := crDefault;
  cbAlteraTodasSenhasDaEmpresa.Checked := False;
  lbStatus.Caption := '';
  txtEmpresID.Text := '';
  txtEmpresID.Visible := False;
  Label2.Visible := False;

end;

procedure TFRelSenhaConv.cbAlteraTodasSenhasDaEmpresaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if cbAlteraTodasSenhasDaEmpresa.Checked then
  begin
    txtEmpresID.Visible := True;
    Label2.Visible := True;
    txtEmpresID.SetFocus;
  end;
end;

procedure TFRelSenhaConv.cbAlteraTodasSenhasDaEmpresaClick(
  Sender: TObject);
begin
  inherited;
  if cbAlteraTodasSenhasDaEmpresa.Checked then
  begin
    txtEmpresID.Visible := True;
    Label2.Visible := True;
  end;
end;

end.
