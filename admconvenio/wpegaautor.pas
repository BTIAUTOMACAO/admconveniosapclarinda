// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/wpegaautor/wpegaautor.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (07/08/2009 16:48:43 - 1.33.2.5)
// ************************************************************************ //

unit wpegaautor;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : WPegaAutor
  // soapAction: WPegaAutor/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : WPegaAutorSoap
  // service   : WPegaAutor
  // port      : WPegaAutorSoap
  // URL       : http://localhost/wpegaautor/wpegaautor.asmx
  // ************************************************************************ //
  WPegaAutorSoap = interface(IInvokable)
  ['{1F5247FD-AF18-E2B8-7F7E-4EF4E8C63511}']
    function  ConsultaCartao(const cartao: WideString; const codimport: WideString): WideString; stdcall;
    function  ConsultaCartaoSeg(const cartao: WideString; const codimport: WideString; const codacesso: WideString): WideString; stdcall;
    function  ConsultaNome(const nome: WideString; const empres_id: WideString; const codimport: WideString): WideString; stdcall;
    function  InformaNF(const autorizacao_id: WideString; const nf: WideString; const comrec: WideString): WideString; stdcall;
    function  InformaNFRecHistOperConf(const autorizacao_id: WideString; const nf: WideString; const comrec: WideString; const historico: WideString; const operador: WideString; const confirmada: WideString): WideString; stdcall;
    function  GeraAutorizacao(const cartao: WideString; const codimport: WideString; const valor: WideString; const codacesso: WideString; const senha: WideString; const formapagto_id: Integer): WideString; stdcall;
    function  SolicitarAutorizacao(const cartao: WideString; const codimport: WideString; const valor: WideString; const codacesso: WideString; const senha: WideString; const formapagto_id: Integer; const nf: WideString; const receita: WideString): WideString; stdcall;
    function  cancelaautor(const autorizacao_id: WideString; const valor: WideString): WideString; stdcall;
    function  consultaautor(const autorizacao_id: WideString): WideString; stdcall;
    function  ConsultaFormasPgto: WideString; stdcall;
    function  ValidarFormasPgto(const empres_id: WideString; const codacesso: WideString): Boolean; stdcall;
    function  ConsultaRegrasDescEmpresa(const empres_id: WideString): WideString; stdcall;
    function  ConsultaEmpresa(const empres_id: WideString): WideString; stdcall;
    function  saldo_disponivel(const cartao: WideString): WideString; stdcall;
    function  ValidarLogin(const cartao: WideString; const senha: WideString): WideString; stdcall;
    function  ConsultarCartoes(const nome_cartao: WideString; const empresa_id: Integer; const codimport: WideString): WideString; stdcall;
    function  ObterGruposProd: WideString; stdcall;
    function  ObterPagamentos(const codacesso: Integer): WideString; stdcall;
    function  ObterDetalhePagamento(const pagamento_cred_id: Integer): WideString; stdcall;
    function  ObterAutorizacoesPagamento(const pagamento_cred_id: Integer): WideString; stdcall;
    function  InformarProdutos(const autorizacao_id: WideString; const produtos: WideString): WideString; stdcall;
  end;

function GetWPegaAutorSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): WPegaAutorSoap;


implementation

function GetWPegaAutorSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): WPegaAutorSoap;
const
  defWSDL = 'http://localhost/wpegaautor/wpegaautor.asmx?WSDL';
  defURL  = 'http://localhost/wpegaautor/wpegaautor.asmx';
  defSvc  = 'WPegaAutor';
  defPrt  = 'WPegaAutorSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as WPegaAutorSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(WPegaAutorSoap), 'WPegaAutor', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(WPegaAutorSoap), 'WPegaAutor/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(WPegaAutorSoap), ioDocument);
  
end. 