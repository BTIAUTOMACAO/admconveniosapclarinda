unit uStringGridHelper;

interface

uses sysutils, grids, dbgrids, menus, Classes, Forms, Windows, strutils, db, DBTables, DBClient,
     math, comObj, dialogs, controls, Graphics, messages, TypInfo, Variants, Excelxp, JvMemoryDataset;

type

  TStringGridHelper = class(TComponent)
  private
    Grid : TStringGrid;
    GridKeyDown : TKeyEvent;
    GridColumnCellEvent : TDrawCellEvent;
    procedure TratarPoupUp;
    procedure ExportarExcel(Sender: TObject);
    procedure ExportarTexto(Sender: TObject);
    procedure DBGridAutoScroll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZebrarGrid(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    //procedure LocateGrade;
  public
    procedure AddHelperToGrid(StringGrid:TStringGrid);
    class procedure StringGridHelperTratrForm(Form:TForm);
  end;

implementation

uses uRotinasPopUp, uMigracao;

{ TStringGridHelper }

procedure TStringGridHelper.TratarPoupUp;
var PopUp : TPopupMenu;
begin
   if Assigned(Grid.PopupMenu) then
      PopUp := Grid.PopupMenu
   else
      PopUp := TPopupMenu.Create(nil);

   if PopUp.Items.Count > 0 then begin
      PopUp.Items.Add(ItemMenuFactory('-','Sep1' ,PopUp,nil));
   end;

   if (PopUp.Items.Find('Exportar para Excel') = nil) then
      PopUp.Items.Add(ItemMenuFactory('Exportar para Excel','ExpExcel',PopUp,ExportarExcel));

   if (PopUp.Items.Find('Exportar para Texto') = nil) then
      PopUp.Items.Add(ItemMenuFactory('Exportar para Texto','ExpText',PopUp,ExportarTexto));

//   if (PopUp.Items.Find('Imprimir',) = nil) then begin
//      PopUp.Items.Add(ItemMenuFactory('-','Sep3',PopUp,nil));
//      PopUp.Items.Add(ItemMenuFactory('Imprimir','Imp',PopUp,Imprimir));
//   end;
   Grid.PopupMenu := PopUp;
end;

procedure TStringGridHelper.DBGridAutoScroll(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Assigned(GridKeyDown) then
     GridKeyDown(Sender,Key,Shift);
  if dgRowSelect in TDBGrid(Sender).Options then begin
     case Key  of
        VK_LEFT  : begin
           TDBGrid(Sender).Perform(WM_HSCROLL,0,0);
           Key := 0;
        end;
        VK_RIGHT : begin
          TDBGrid(Sender).Perform(WM_HSCROLL,1,0);
          Key := 0;
        end;
     end;
  end;
//  if (( ssCtrl in Shift ) and (Key = $46)) then
//     LocateGrade;
end;

procedure TStringGridHelper.ExportarExcel(Sender: TObject);
begin
  prExportarExcel(Grid);
end;

procedure TStringGridHelper.ExportarTexto(Sender: TObject);
begin
  prExportarTexto(Grid);
end;

procedure TStringGridHelper.AddHelperToGrid(StringGrid: TStringGrid);
var UsandoTitleClick : Boolean;
begin
   Grid := StringGrid;
   TratarPoupUp;
   GridKeyDown := StringGrid.OnKeyDown;
   //StringGrid.OnKeyDown := DBGridAutoScroll;
   GridColumnCellEvent := StringGrid.OnDrawCell;
   StringGrid.OnDrawCell := ZebrarGrid;

   UsandoTitleClick := IsPublishedProp(Grid,'OnTitleBtnClick') and (GetMethodProp(Grid,'OnTitleBtnClick').code <> nil);

//   if (not Assigned(Grid.OnTitleClick) and (not UsandoTitleClick)) then
//      Grid.OnTitleClick := TitleClick;
end;

class procedure TStringGridHelper.StringGridHelperTratrForm(Form: TForm);
var i : Integer; Helper : TStringGridHelper;
begin
   for i := 0 to pred(Form.ComponentCount) do
       if Form.Components[i] is TStringGrid then begin
          Helper := TStringGridHelper.Create(Form);
          Helper.AddHelperToGrid( (Form.Components[i] as TStringGrid) );
       end;
end;

procedure TStringGridHelper.ZebrarGrid(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  If (arow > 0) then // testa se n�o � a primeira linha (fixa)
     if (odd(arow)) then begin // verifica se a linha � impar
        TStringGrid(Sender).Canvas.Font.Color:= clBlack;
        TStringGrid(Sender).Canvas.Brush.Color:= clWhite;
     end
     else begin
        TStringGrid(Sender).Canvas.Font.Color:= clBlack;
        TStringGrid(Sender).Canvas.Brush.Color:= $0EEEEEE;
     end;
  TStringGrid(Sender).Canvas.FillRect(Rect); // redesenha a celula
  TStringGrid(Sender).Canvas.TextOut(Rect.Left+2,Rect.Top,TStringGrid(Sender).Cells[acol,arow]);
end;

{procedure TStringGridHelper.LocateGrade;
var i : integer; FPesqGrade : TFPesqGrade;
begin
  if not Grid.DataSource.DataSet.IsEmpty then begin
    FPesqGrade := TFPesqGrade.Create(Screen.ActiveForm);
    SetLength(FPesqGrade.campos,Grid.Columns.Count);
    for i := 0 to Grid.Columns.Count - 1 do begin
      FPesqGrade.CBCampos.Items.Add(Grid.Columns[i].Title.Caption);
      FPesqGrade.campos[i].index := i;
      FPesqGrade.campos[i].fieldname := Grid.Columns[i].FieldName;
     end;
     FPesqGrade.CBCampos.ItemIndex := 0;
     FPesqGrade.Grade := Grid;
     FPesqGrade.ShowModal;
     FPesqGrade.Free;
     Grid.SetFocus;
  end;
end;}

end.
