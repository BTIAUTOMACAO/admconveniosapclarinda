object FSelTipoImp: TFSelTipoImp
  Left = 289
  Top = 186
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Selecionando tipo de impress'#227'o'
  ClientHeight = 208
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 162
    Width = 354
    Height = 2
  end
  object ButOk: TButton
    Left = 187
    Top = 176
    Width = 75
    Height = 25
    Caption = '&Ok'
    ModalResult = 1
    TabOrder = 0
  end
  object ButCanc: TButton
    Left = 269
    Top = 176
    Width = 75
    Height = 25
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 7
    Top = 5
    Width = 339
    Height = 147
    Caption = 'Selecione o tipo de impress'#227'o'
    TabOrder = 2
  end
end
