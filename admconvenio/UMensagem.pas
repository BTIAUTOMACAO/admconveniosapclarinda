unit UMensagem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, Buttons, ExtCtrls, Grids, DBGrids, {JvDBCtrl,}
  ComCtrls, ToolWin, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  StdCtrls, ImgList, DBCtrls, ADODB, OleCtrls, SHDocVw, JvComCtrls,
  JvExDBGrids, JvDBGrid;

type
  TFMensagem = class(TF1)
    Panel3: TPanel;
    Panel4: TPanel;
    QMensagens: TZQuery;
    DSMensagens: TDataSource;
    ImageList1: TImageList;
    Query1: TZReadOnlyQuery;
    DSQuery1: TDataSource;
    QMensagensMENSAGENS_ID: TIntegerField;
    QMensagensREMETENTE_TP: TStringField;
    QMensagensREMETENTE_ID: TIntegerField;
    QMensagensDESTINATARIO_TP: TStringField;
    QMensagensDESTINATARIO_ID: TIntegerField;
    QMensagensNOME: TStringField;
    QMensagensDATAHORA: TDateTimeField;
    QMensagensASSUNTO: TStringField;
    QMensagensMENSAGEM: TStringField;
    QMensagensLIDO: TStringField;
    QMensagensAPAGADO: TStringField;
    PopupMenu1: TPopupMenu;
    Responder1: TMenuItem;
    Apagar1: TMenuItem;
    N2: TMenuItem;
    Imprimir1: TMenuItem;
    TreeView1: TTreeView;
    N3: TMenuItem;
    Marcarcomolido1: TMenuItem;
    Marcarcomonolido1: TMenuItem;
    Panel8: TPanel;
    WebBrowser1: TWebBrowser;
    Panel9: TPanel;
    Panel7: TPanel;
    JvDBGrid1: TJvDBGrid;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1CellClick(Column: TColumn);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TreeView1CustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure Responder1Click(Sender: TObject);
    procedure Apagar1Click(Sender: TObject);
    procedure Imprimir1Click(Sender: TObject);
    procedure DSMensagensDataChange(Sender: TObject; Field: TField);
    procedure JvTreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure JvTreeView1CustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure QMensagensAfterRefresh(DataSet: TDataSet);
    procedure QMensagensAfterOpen(DataSet: TDataSet);
    procedure Marcarcomolido1Click(Sender: TObject);
    procedure Marcarcomonolido1Click(Sender: TObject);
  private
    procedure AbreMensagens;
    procedure MontarMensagem;
    procedure WBPrintWithDialog(WB: TWebBrowser);
    procedure ContarNovasMsg;
    procedure MontarTelaInicio;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMensagem: TFMensagem;

implementation

uses DM, cartao_util, UNovaMensagem;

{$R *.dfm}

procedure TFMensagem.TreeView1Change(Sender: TObject; Node: TTreeNode);
var i: Integer;
begin
  inherited;
  AbreMensagens;
end;

procedure TFMensagem.AbreMensagens;
begin
  QMensagens.Close;
  QMensagens.SQL.Clear;
  if not TreeView1.Items[0].Selected then
  begin
    ToolButton2.Enabled:= True;
    ToolButton3.Enabled:= True;
    if ((TreeView1.Items[1].Selected) or (TreeView1.Items[2].Selected) or (TreeView1.Items[3].Selected)) then
    begin
      JvDBGrid1.Columns[0].Title.Caption:= 'De...';
      ToolButton4.Enabled:= True;
      QMensagens.SQL.Add(' select mensagens_id, remetente_tp, remetente_id, destinatario_tp, destinatario_id, ');
      QMensagens.SQL.Add(' case when remetente_tp = "EMPRESAS" ');
      QMensagens.SQL.Add(' then (select "Empr: "||empres_id||" - "||nome from empresas where empres_id = remetente_id) ');
      QMensagens.SQL.Add(' else (select "Forn: "||cred_id||" - "||nome from credenciados where cred_id = remetente_id) ');
      QMensagens.SQL.Add(' end as nome, datahora, assunto, mensagem, lido, apagado ');
      QMensagens.SQL.Add(' from mensagens where apagado <> "S" and destinatario_tp = "ADMINISTRADORA" ');
      if TreeView1.Items[2].Selected then
      begin
        QMensagens.SQL.Add(' and remetente_tp = "EMPRESAS" ');
      end
      else if TreeView1.Items[3].Selected then
      begin
        QMensagens.SQL.Add(' and remetente_tp = "CREDENCIADOS" ');
      end;
      QMensagens.SQL.Add(' order by datahora desc ');
    end
    else if ((TreeView1.Items[4].Selected) or (TreeView1.Items[5].Selected) or (TreeView1.Items[6].Selected)) then
    begin
      ToolButton4.Enabled:= False;
      JvDBGrid1.Columns[0].Title.Caption:= 'Para...';
      QMensagens.SQL.Add(' select mensagens_id, remetente_tp, remetente_id, destinatario_tp, destinatario_id, ');
      QMensagens.SQL.Add(' case when destinatario_tp = "EMPRESAS" ');
      QMensagens.SQL.Add(' then (select "Empr: "||empres_id||" - "||nome from empresas where empres_id = destinatario_id) ');
      QMensagens.SQL.Add(' else (select "Forn: "||cred_id||" - "||nome from credenciados where cred_id = destinatario_id) ');
      QMensagens.SQL.Add(' end as nome, datahora, assunto, mensagem, lido, apagado ');
      QMensagens.SQL.Add(' from mensagens where apagado <> "S" and remetente_tp = "ADMINISTRADORA" ');
      if TreeView1.Items[5].Selected then
      begin
        QMensagens.SQL.Add(' and destinatario_tp = "EMPRESAS" ');
      end
      else if TreeView1.Items[6].Selected then
      begin
        QMensagens.SQL.Add(' and destinatario_tp = "CREDENCIADOS" ');
      end;
      QMensagens.SQL.Add(' order by datahora desc ');
    end;
    QMensagens.Open;
  end
  else
  begin
    MontarTelaInicio;
  end;
end;

procedure TFMensagem.MontarTelaInicio;
var msg: TStringList;
  adm: String;
begin
  ToolButton2.Enabled:= False;
  ToolButton3.Enabled:= False;
  ToolButton4.Enabled:= False;
  DM1.Adm.Open;
  adm:= DM1.AdmRAZAO.AsString;
  DM1.Adm.Close;
  msg := TStringList.Create;
  msg.Append('');
  msg.Append('<html>');
  msg.Append('<body>');
  msg.Append('<b>'+adm+'</b><br>');
  msg.Append('<b> M�dulo de Controle de Mensagens</b><br>');
  msg.Append('Clique na arvore para filtrar o tipo de consulta');
  msg.Append('</body>');
  msg.Append('</html>');
  msg.SaveToFile(ExtractFilePath(Application.ExeName)+'msg2.html');
  msg.Free;
  WebBrowser1.Navigate(FileName2URL(ExtractFilePath(Application.ExeName)+'msg2.html'));
end;

procedure TFMensagem.ToolButton1Click(Sender: TObject);
begin
  inherited;
  FNovaMensagem:= TFNovaMensagem.Create(nil);
  FNovaMensagem.ConfiguraTela('','','');
  FNovaMensagem.ShowModal;
  FNovaMensagem.Free;
  if QMensagens.Active then
    QMensagens.Refresh;
end;

procedure TFMensagem.ContarNovasMsg;
var empr, forn: Integer;
begin
  empr:= 0;
  forn:= 0;
  empr:= DM1.ExecuteScalar('select count(*) from mensagens where apagado <> "S" and lido = "N" '+
                           'and remetente_tp = "EMPRESAS" and destinatario_tp = "ADMINISTRADORA"',0);
  forn:= DM1.ExecuteScalar('select count(*) from mensagens where apagado <> "S" and lido = "N" '+
                           'and remetente_tp = "CREDENCIADOS" and destinatario_tp = "ADMINISTRADORA"',0);
  if empr > 0 then
  begin
    TreeView1.Items[2].Text:= 'Empresas ('+IntToStr(empr)+')';
  end
  else
  begin
    TreeView1.Items[2].Text:= 'Empresas';
  end;
  if forn > 0 then
  begin
    TreeView1.Items[3].Text:= 'Estabelecimentos ('+IntToStr(forn)+')';
  end
  else
  begin
    TreeView1.Items[3].Text:= 'Estabelecimentos';
  end;
  if (forn+empr) > 0 then
  begin
    TreeView1.Items[1].Text:= 'Caixa de entrada ('+IntToStr(forn+empr)+')';
  end
  else
  begin
    TreeView1.Items[1].Text:= 'Caixa de entrada';
  end;
  TreeView1.Refresh;
end;

procedure TFMensagem.FormCreate(Sender: TObject);
var x: byte;
begin
  inherited;
  TreeView1.FullExpand;
  TreeView1.Repaint;
  TreeView1.Selected := TreeView1.Items[0];
  ContarNovasMsg;
end;

procedure TFMensagem.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if ((QMensagens.FieldByName('LIDO').AsString = 'N') and (QMensagens.FieldByName('REMETENTE_TP').AsString <> 'ADMINISTRADORA')) then
  begin
    JvDBGrid1.Canvas.Font.Style := [fsBold];
    JvDBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TFMensagem.JvDBGrid1CellClick(Column: TColumn);
begin
  inherited;
  if QMensagens.FieldByName('LIDO').AsString = 'N' then
  begin
    QMensagens.DisableControls;
    QMensagens.Edit;
    QMensagens.FieldByName('LIDO').AsString:= 'S';
    QMensagens.Post;
    ContarNovasMsg;
    QMensagens.EnableControls;
  end;
end;

procedure TFMensagem.ToolButton2Click(Sender: TObject);
begin
  inherited;
  if QMensagens.RecordCount > 0 then
    begin
    if MsgSimNao('Deseja excluir a mensagem?') then
    begin
      QMensagens.Edit;
      QMensagens.FieldByName('APAGADO').AsString:= 'S';
      QMensagens.Post;
      QMensagens.Refresh;
    end;
  end else
    MsgInf('N�o existe registro para ser apagado!');
end;

procedure TFMensagem.MontarMensagem;
var msg: TStringList;
  mensagem, adm: String;
  i: Integer;
begin
  DM1.Adm.Open;
  adm:= DM1.AdmRAZAO.AsString;
  DM1.Adm.Close;
  mensagem:= QMensagens.FieldByName('MENSAGEM').AsString;
  mensagem:= StringReplace(mensagem,sLineBreak,'<br>',[rfReplaceAll]);
  msg := TStringList.Create;
  msg.Append('');
  msg.Append('<html>');
  msg.Append('<body>');
  if QMensagens.FieldByName('REMETENTE_TP').AsString = 'ADMINISTRADORA' then
  begin
    msg.Append('<b>Mensagem de</b>: '+adm+'<br />');
    msg.Append('<b>Para</b>: '+QMensagens.FieldByName('NOME').AsString+'<br />');
  end
  else
  begin
    msg.Append('<b>Mensagem de</b>: '+QMensagens.FieldByName('NOME').AsString+'<br />');
    msg.Append('<b>Para</b>: '+adm+'<br />');
  end;
  msg.Append('<b>Data</b>: '+FormatDateTime('dd/MM/yyyy hh:mm:ss',QMensagens.FieldByName('DATAHORA').AsDateTime)+'<br />');
  msg.Append('<b>Assunto</b>: '+QMensagens.FieldByName('ASSUNTO').AsString+'<br /><br />');
  msg.Append(mensagem);
  msg.Append('</body>');
  msg.Append('</html>');
  try
    msg.SaveToFile(ExtractFilePath(Application.ExeName)+'msg.html');
  except
    //MsgErro('N�o foi poss�vel carregar a imagem!');
  end;
  msg.Free;
  WebBrowser1.Navigate(FileName2URL(ExtractFilePath(Application.ExeName)+'msg.html'));
end;

procedure TFMensagem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  DeleteFile(ExtractFilePath(Application.ExeName)+'msg.html');
end;

procedure TFMensagem.TreeView1CustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  inherited;
  if Node.Selected then
    TreeView1.Canvas.Font.Style := [fsBold];
end;

procedure TFMensagem.ToolButton3Click(Sender: TObject);
begin
  inherited;
  if QMensagens.FieldByName('LIDO').AsString = 'N' then
  begin
    QMensagens.DisableControls;
    QMensagens.Edit;
    QMensagens.FieldByName('LIDO').AsString:= 'S';
    QMensagens.Post;
    QMensagens.EnableControls;
  end;
end;

procedure TFMensagem.ToolButton4Click(Sender: TObject);
begin
  inherited;
  FNovaMensagem:= TFNovaMensagem.Create(nil);
  if QMensagensREMETENTE_TP.AsString = 'EMPRESAS' then
  begin
    FNovaMensagem.ConfiguraTela('','Empr: '+QMensagensREMETENTE_ID.AsString,'[Resp] '+copy(QMensagensASSUNTO.AsString,1,92));
  end
  else
  begin
    FNovaMensagem.ConfiguraTela('','Forn: '+QMensagensREMETENTE_ID.AsString,'[Resp] '+copy(QMensagensASSUNTO.AsString,1,92));
  end;
  FNovaMensagem.ShowModal;
  FNovaMensagem.Free;
  QMensagens.Refresh;
end;

procedure TFMensagem.Responder1Click(Sender: TObject);
begin
  inherited;
  ToolButton4.Click;
end;

procedure TFMensagem.Apagar1Click(Sender: TObject);
begin
  inherited;
  ToolButton2.Click;
end;

procedure TFMensagem.Imprimir1Click(Sender: TObject);
begin
  inherited;
  WBPrintWithDialog(WebBrowser1);
end;

procedure TFMensagem.WBPrintWithDialog(WB: TWebBrowser) ;
var
  vIn, vOut: OleVariant;
begin
  WB.ControlInterface.ExecWB(OLECMDID_PRINT,
    OLECMDEXECOPT_PROMPTUSER, vIn, vOut)
end;

procedure TFMensagem.DSMensagensDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  MontarMensagem;
  if ((QMensagensDESTINATARIO_TP.AsString = 'ADMINISTRADORA') and (QMensagensLIDO.AsString = 'S')) then
  begin
    ToolButton3.Enabled:= False;
  end
  else
  begin
    ToolButton3.Enabled:= True;
  end;
  if QMensagensLIDO.AsString = 'S' then
  begin
    Marcarcomolido1.Enabled:= False;
    Marcarcomonolido1.Enabled:= True;
  end
  else
  begin
    Marcarcomolido1.Enabled:= True;
    Marcarcomonolido1.Enabled:= False;
  end;
end;

procedure TFMensagem.JvTreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  inherited;
  AbreMensagens;
end;

procedure TFMensagem.JvTreeView1CustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  inherited;
  if Node.Selected then
    TreeView1.Canvas.Font.Style := [fsBold];
end;

procedure TFMensagem.QMensagensAfterRefresh(DataSet: TDataSet);
begin
  inherited;
  ContarNovasMsg;
end;

procedure TFMensagem.QMensagensAfterOpen(DataSet: TDataSet);
begin
  inherited;
  ContarNovasMsg;
end;

procedure TFMensagem.Marcarcomolido1Click(Sender: TObject);
begin
  inherited;
  QMensagens.DisableControls;
  QMensagens.Edit;
  QMensagens.FieldByName('LIDO').AsString:= 'S';
  QMensagens.Post;
  QMensagens.EnableControls;
end;

procedure TFMensagem.Marcarcomonolido1Click(Sender: TObject);
begin
  inherited;
  QMensagens.DisableControls;
  QMensagens.Edit;
  QMensagens.FieldByName('LIDO').AsString:= 'N';
  QMensagens.Post;
  QMensagens.EnableControls;
end;

end.
