object FGeraLista: TFGeraLista
  Left = 262
  Top = 127
  ActiveControl = CheckList
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Gerador de Listagem'
  ClientHeight = 497
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 686
    Height = 497
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Configura'#231#227'o da Listagem'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 678
        Height = 469
        Align = alClient
        BorderStyle = bsSingle
        TabOrder = 0
        object Label1: TLabel
          Left = 284
          Top = 264
          Width = 106
          Height = 16
          Caption = 'T'#237'tulo da Listagem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 268
          Top = 329
          Width = 137
          Height = 16
          Caption = 'Cabe'#231'alho das Colunas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 276
          Top = 372
          Width = 120
          Height = 16
          Caption = 'Detalhe das Colunas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Bevel1: TBevel
          Left = 510
          Top = 177
          Width = 150
          Height = 2
        end
        object Label10: TLabel
          Left = 607
          Top = 424
          Width = 53
          Height = 13
          Caption = 'F1 - Help'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 311
          Height = 257
          BorderStyle = bsSingle
          TabOrder = 0
          object CheckList: TCheckListBox
            Left = 1
            Top = 1
            Width = 184
            Height = 251
            Hint = 
              'Pressione Control + Seta para baixo ou para cima para mover os i' +
              'tens.'#13#10'Use seta para baixo ou para cima para mudar de item.'
            OnClickCheck = CheckListClickCheck
            Align = alLeft
            ItemHeight = 13
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = CheckListClick
            OnKeyDown = CheckListKeyDown
          end
          object Button1: TButton
            Left = 193
            Top = 155
            Width = 100
            Height = 25
            Caption = '&Marca Todos'
            TabOrder = 1
            OnClick = Button1Click
          end
          object Button2: TButton
            Left = 193
            Top = 195
            Width = 100
            Height = 25
            Caption = '&Desmarca Todos'
            TabOrder = 2
            OnClick = Button2Click
          end
          object ButAbaixo: TButton
            Left = 193
            Top = 72
            Width = 100
            Height = 25
            Caption = 'A&baixo'
            TabOrder = 3
            OnClick = ButAbaixoClick
          end
          object ButAcima: TButton
            Left = 193
            Top = 32
            Width = 100
            Height = 25
            Caption = 'A&cima'
            TabOrder = 4
            OnClick = ButAcimaClick
          end
        end
        object EdTitulo: TEdit
          Left = 6
          Top = 281
          Width = 661
          Height = 20
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Orator10 BT'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 1
          OnClick = EdTituloClick
          OnEnter = EdTituloEnter
          OnExit = EdTituloExit
          OnKeyDown = EdTituloKeyDown
        end
        object EdAdicional: TEdit
          Left = 6
          Top = 302
          Width = 661
          Height = 20
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Orator10 BT'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 2
          OnClick = EdTituloClick
          OnEnter = EdTituloEnter
          OnExit = EdTituloExit
          OnKeyDown = EdTituloKeyDown
        end
        object EdColun: TEdit
          Left = 6
          Top = 345
          Width = 661
          Height = 20
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Orator10 BT'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 3
          OnClick = EdTituloClick
          OnEnter = EdTituloEnter
          OnKeyDown = EdTituloKeyDown
        end
        object ChTotlinhas: TCheckBox
          Left = 7
          Top = 424
          Width = 166
          Height = 17
          Caption = 'Mostrar total de Registros'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object ChUsaSep: TCheckBox
          Left = 183
          Top = 424
          Width = 169
          Height = 17
          Caption = 'Usar separador de linhas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object EdDet: TEdit
          Left = 6
          Top = 389
          Width = 661
          Height = 20
          Hint = 'Coloque o nome interno do campo onde deseja que ele apare'#231'a.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Orator10 BT'
          Font.Style = []
          MaxLength = 128
          ParentFont = False
          TabOrder = 4
          OnClick = EdTituloClick
          OnEnter = EdTituloEnter
          OnKeyDown = EdTituloKeyDown
        end
        object Barra: TStatusBar
          Left = 1
          Top = 445
          Width = 672
          Height = 19
          Panels = <
            item
              Width = 60
            end
            item
              Width = 200
            end
            item
              Width = 50
            end>
        end
        object ButGeraLista: TBitBtn
          Left = 510
          Top = 195
          Width = 150
          Height = 25
          Hint = 'Gera a listagem para o visualizador.'
          Caption = '&Gerar Listagem'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = ButGeraListaClick
        end
        object Panel3: TPanel
          Left = 310
          Top = 0
          Width = 185
          Height = 257
          BorderStyle = bsSingle
          TabOrder = 9
          object Label4: TLabel
            Left = 1
            Top = 1
            Width = 179
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Detalhes do Campo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbname: TLabel
            Left = 1
            Top = 156
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbdescri: TLabel
            Left = 1
            Top = 27
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 1
            Top = 57
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Tipo do Campo'
          end
          object Label8: TLabel
            Left = 1
            Top = 143
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Nome Interno do Campo'
          end
          object Label9: TLabel
            Left = 1
            Top = 14
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Descri'#231#227'o do Campo'
          end
          object lbtipo: TLabel
            Left = 1
            Top = 70
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 1
            Top = 100
            Width = 179
            Height = 13
            Align = alTop
            Caption = 'Tamanho do Campo'
          end
          object lbtamanho: TLabel
            Left = 1
            Top = 113
            Width = 179
            Height = 30
            Align = alTop
            AutoSize = False
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object RichEdit1: TRichEdit
            Left = -4
            Top = 253
            Width = 185
            Height = 89
            Lines.Strings = (
              'Configura'#231#227'o da Listagem:'
              
                '    Para configurar uma listagem basta selecionar os campos dand' +
                'o um clique com o mouse ou pressionando a barra de espa'#231'os sobre' +
                ' o campo desejado na janelinha de campos, ap'#243's selecionar os cam' +
                'pos desejados para sair na listagem, coloque-os na ordem que des' +
                'ejar marcando um item e clicando no bot'#227'o '#8220'acima'#8221' ou '#8220'abaixo'#8221' ta' +
                'mb'#233'm '#233' poss'#237'vel alterar a ordem dos campos pressionando a tela c' +
                'ontrol + seta para baixo ou para cima. No campo '#8220'T'#237'tulo da lista' +
                'gem'#8221' '#233' poss'#237'vel informar um t'#237'tulo para a listagem este contendo' +
                ' no m'#225'ximo duas linha, marcando a op'#231#227'o '#8220'T'#237'tulo Centralizado'#8221', a' +
                's duas linhas do t'#237'tulo sempre ficar'#227'o centralizadas na listagem' +
                ', marcando a op'#231#227'o '#8220'Mostrar Data'#8221' ser'#225' adicionada a data atual n' +
                'o formato dd/mm/aaaa ao lado direito da primeira linha do titulo' +
                '. No campo '#8220'Cabe'#231'alho das Colunas'#8221' '#233' poss'#237'vel alterar os nomes d' +
                'os campos a serem listados; exemplo: campo chapa para algumas em' +
                'presas pode ser interessante o t'#237'tulo como matricula. Tamb'#233'm '#233' p' +
                'oss'#237'vel configurar manualmente o espa'#231'amento entre os campos, pa' +
                'ra isto defina o espa'#231'amento igual no campo '#8220'Cabe'#231'alho das Colun' +
                'as'#8221' e no campo '#8220'Detalhe das colunas'#8221' para que a listagem n'#227'o sai' +
                'a desalinhada, nesta segunda n'#227'o altere os nomes dos campos, poi' +
                's atrav'#233's destes nomes o programa saber'#225' qual o valor a ser info' +
                'rmado.'
              
                '    Ap'#243's configurar a listagem '#233' poss'#237'vel verificar como esta sa' +
                'ir'#225' na impressora, clicando na aba '#8220'Visualiza'#231#227'o da primeira p'#225'g' +
                'ina'#8221'. Estando a configura'#231#227'o no formato necess'#225'rio para o operad' +
                'or do sistema '#233' poss'#237'vel salva-l'#225' no banco de dados do sistema p' +
                'ara que possa ser reutilizada posteriormente, para isso clique e' +
                'm '#8220'Salvar Modelo como...'#8221' e informe uma descri'#231#227'o para este mode' +
                'lo. Para reutilizar um modelo j'#225' configurado e salvo, basta clic' +
                'ar em '#8220'Abrir Modelo'#8221' e selecionar o modelo desejado. Caso o mode' +
                'lo aberto for alterado e o operador do sistema quiser salvar as ' +
                'altera'#231#245'es efetuadas no modelo corrente basta clicar no bot'#227'o '#8220'S' +
                'alvar Modelo'#8221' ou '#8220'Salvar Modelo como...'#8221' caso queira criar um no' +
                'vo modelo a partir de um preexistente.'
              
                '    Para renomear um modelo existente abra a janela de sele'#231#227'o d' +
                'e modelos clicando no bot'#227'o '#8220'Abrir Modelo'#8221', ent'#227'o clique com o b' +
                'ot'#227'o direito na grade de modelos e clique em '#8220'Alterar Descri'#231#227'o ' +
                'do Modelo'#8221' e informe a nova descri'#231#227'o.'
              
                '    Para que os n'#250'meros das p'#225'ginas sejam mostrados na listagem ' +
                'marque a op'#231#227'o '#8220'Mostrar N'#186' da P'#225'gina'#8221', as op'#231#245'es '#8220'Mostrar total ' +
                'de registros'#8221' e '#8220'Usar separador de linhas'#8221' servem respectivament' +
                'e para que ao final da listagem seja exibida uma linha informand' +
                'o o total de registros listados e para que cada linha seja separ' +
                'ada por outra linha contendo caracteres do tipo h'#237'fen '#39'-'#39'.')
            TabOrder = 0
            Visible = False
            WordWrap = False
          end
        end
        object AbreMod: TBitBtn
          Left = 510
          Top = 62
          Width = 150
          Height = 25
          Hint = 'Abre modelo gravado.'
          Caption = '&Abrir Modelo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          OnClick = AbreModClick
        end
        object SalvaMod: TBitBtn
          Left = 510
          Top = 99
          Width = 150
          Height = 25
          Hint = 'Salva o modelo em aberto.'
          Caption = 'Sa&lvar Modelo'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          OnClick = SalvaModClick
        end
        object SalvaModComo: TBitBtn
          Left = 510
          Top = 136
          Width = 150
          Height = 25
          Hint = 'Salvar novo modelo'
          Caption = '&Salvar Modelo como..'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          OnClick = SalvaModComoClick
        end
        object Panel4: TPanel
          Left = 496
          Top = 0
          Width = 176
          Height = 53
          BorderStyle = bsSingle
          TabOrder = 13
          object Label6: TLabel
            Left = 1
            Top = 1
            Width = 170
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Descri'#231#227'o do Modelo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object labmodelo: TLabel
            Left = 1
            Top = 14
            Width = 170
            Height = 13
            Align = alTop
            Caption = 'Novo Modelo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object ProgressBar1: TProgressBar
          Left = 263
          Top = 446
          Width = 409
          Height = 17
          TabOrder = 14
        end
        object Button3: TButton
          Left = 510
          Top = 231
          Width = 150
          Height = 25
          Caption = 'E&xportar para arquivo'
          TabOrder = 15
          OnClick = Button3Click
        end
        object Button4: TButton
          Left = 6
          Top = 264
          Width = 98
          Height = 16
          Caption = 'Centralizar Titulo'
          TabOrder = 16
          OnClick = Button4Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Visualiza'#231#227'o da primeira p'#225'gina'
      ImageIndex = 1
      object ListPrev: TListBox
        Left = 3
        Top = 4
        Width = 669
        Height = 460
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Orator10 BT'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object DSRels: TDataSource
    Left = 368
    Top = 224
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    FileName = 'arquivo.txt'
    Filter = 'Arquivos Texto (*.txt)|(*.txt)'
    Left = 442
    Top = 160
  end
  object SaveDialog2: TSaveDialog
    DefaultExt = 'xml'
    FileName = 'arquivo.xml'
    Filter = 'Arquivos XML (*.xml)|(*.xml)'
    Left = 370
    Top = 160
  end
end
