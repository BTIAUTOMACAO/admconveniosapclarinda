unit FRelBemEstarCreditos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, frxClass, frxExportPDF, frxGradient, frxDBSet, DBCtrls,
  StdCtrls, Mask, JvExMask, JvToolEdit, ExtCtrls, Grids, DBGrids,
  JvExDBGrids, JvDBGrid, Buttons, ComCtrls, Menus, ZDataset, DB,
  ZAbstractRODataset, ZAbstractDataset, ShellApi, ADODB, JvMemoryDataset;

type
  TFrmRelBemEstarCreditos = class(TF1)
    GroupBox1: TGroupBox;
    btnVisualizar: TBitBtn;
    btnGerarPDF: TBitBtn;
    lblFechamento: TLabel;
    rgTipo: TRadioGroup;
    frxReport1: TfrxReport;
    dbConveniados: TfrxDBDataset;
    frxGradientObject1: TfrxGradientObject;
    frxPDFExport1: TfrxPDFExport;
    sd: TSaveDialog;
    dsFechamentos: TDataSource;
    qryConv: TADOQuery;
    dsConv: TDataSource;
    qryConvTOTAL: TCurrencyField;
    qryConvDATA: TStringField;
    dataInicial: TJvDateEdit;
    dataFinal: TJvDateEdit;
    Label1: TLabel;
    qData: TADOQuery;
    CurrencyField1: TCurrencyField;
    q3: TStringField;
    qryConvDATAF: TStringField;
    qryConvNOME: TStringField;
    qryConvFANTASIA: TStringField;
    dbData: TfrxDBDataset;
    dsdata: TDataSource;
    ReportData: TfrxReport;
    qryConvEMPRES_ID: TIntegerField;
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure ButMarcDesm_EmpClick(Sender: TObject);
    procedure ButMarcaTodos_EmpClick(Sender: TObject);
    procedure ButDesmTodos_EmpClick(Sender: TObject);
    procedure btnGerarPDFClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dsConvDataChange(Sender: TObject; Field: TField);
    procedure SetarVisibilidade(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);







  private
    conv_sel : String;
  public
    { Public declarations }
  end;

var
  
  FrmRelBemEstarCreditos: TFrmRelBemEstarCreditos;

implementation

uses DM, cartao_util;

{$R *.dfm}

procedure TFrmRelBemEstarCreditos.SetarVisibilidade(Sender: TObject);
begin
  inherited;
  if(rgTipo.ItemIndex = 0)then
  begin
        dataInicial.Visible := False;
  end else begin

  end;
end;

procedure TFrmRelBemEstarCreditos.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ButMarcDesm_EmpClick(nil);
end;

procedure TFrmRelBemEstarCreditos.ButMarcDesm_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesm(QConv);
end;

procedure TFrmRelBemEstarCreditos.ButMarcaTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv);
end;

procedure TFrmRelBemEstarCreditos.ButDesmTodos_EmpClick(Sender: TObject);
begin
  inherited;
  //Dm1.MarcaDesmTodos(qConv,False);
end;

procedure TFrmRelBemEstarCreditos.btnGerarPDFClick(Sender: TObject);
begin
  inherited;

    sd.Filter := '.pdf|.pdf';
    if sd.Execute then begin
      if ExtractFileExt(sd.FileName) = '' then
        sd.FileName := sd.FileName + '.pdf';
      frxPDFExport1.FileName := sd.FileName;
      btnVisualizar.Click;
      if(rgTipo.ItemIndex = 0)then
        begin
           ReportData.Export(frxPDFExport1);
        end else begin
           frxReport1.Export(frxPDFExport1);
      end;
    
    end;

end;

procedure TFrmRelBemEstarCreditos.btnVisualizarClick(Sender: TObject);

begin
  inherited;

   
  
  if(rgTipo.ItemIndex = 0)then
    begin
      qData.Close;
      qData.SQL.Clear;
      qData.SQL.Add(' SELECT CONVERT(VARCHAR, DATA_HORA, 103) AS DATA,  SUM(VALOR)  AS TOTAL ');
      qData.SQL.Add(' FROM LANCAMENTO_BEMESTAR_DEBITOS A INNER JOIN EMPRESAS B  ON A.EMPRES_ID = B.EMPRES_ID  ');
      qData.SQL.Add('  WHERE A.DATA_HORA between '+QuotedStr(DateToStr(dataInicial.Date))+' and '+QuotedStr(DateToStr(dataFinal.Date)+' 23:59:59'));
      qData.SQL.Add(' GROUP BY CONVERT(VARCHAR, DATA_HORA, 103) ORDER BY CONVERT(VARCHAR, DATA_HORA, 103) ');
      qData.SQL.Text;
      qData.Open;
      if qData.IsEmpty then
        begin
          MsgInf('N�o foi encontrado nenhum lan�amento.');

          abort;
        end;
      ReportData.ShowReport;
    end else
    begin

      qryConv.Close;
      qryConv.SQL.Clear;
      qryConv.SQL.Add(' SELECT CONVERT(VARCHAR, DATA_HORA, 103) AS DATA, A.EMPRES_ID, B.NOME,B.FANTASIA,CONVERT(VARCHAR, A.DATA_FECHA_EMP, 103) AS DATAF, SUM(VALOR)  AS TOTAL ');
      qryConv.SQL.Add(' FROM LANCAMENTO_BEMESTAR_DEBITOS A INNER JOIN EMPRESAS B  ON A.EMPRES_ID = B.EMPRES_ID  ');
      qryConv.SQL.Add('  WHERE A.DATA_HORA between '+QuotedStr(DateToStr(dataInicial.Date))+' and '+QuotedStr(DateToStr(dataFinal.Date)+' 23:59:59'));
      qryConv.SQL.Add(' GROUP BY A.EMPRES_ID,B.NOME,B.FANTASIA, CONVERT(VARCHAR, DATA_HORA, 103),CONVERT(VARCHAR, A.DATA_FECHA_EMP, 103) ORDER BY CONVERT(VARCHAR, DATA_HORA, 103), A.EMPRES_ID ');
      qryConv.SQL.Text;
      qryConv.Open;
      if qryConv.IsEmpty then
        begin
          MsgInf('N�o foi encontrado nenhum lan�amento.');

          abort;
        end;
      frxReport1.ShowReport;
    end;









end;

procedure TFrmRelBemEstarCreditos.dsConvDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnGerarPDF.Enabled   := (not TDataSource(Sender).DataSet.IsEmpty) and (TDataSource(Sender).DataSet.Active);
end;

procedure TFrmRelBemEstarCreditos.FormCreate(Sender: TObject);
begin
  inherited;
  dataInicial.Date :=  Date();
  dataFinal.Date := Date();
  rgTipo.SetFocus;
end;

procedure TFrmRelBemEstarCreditos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {if (key = VK_F8) then
    Dm1.MarcaDesmTodos(qConv)
  else if (key = VK_F9) then
    Dm1.MarcaDesmTodos(qConv,False)
  else if (key = VK_F11) then
    Dm1.MarcaDesm(qConv);   }
end;
















end.
