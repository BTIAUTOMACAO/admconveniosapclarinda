object FImportConveniadosProdutor: TFImportConveniadosProdutor
  Left = 499
  Top = 203
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Importa'#231#227'o Padr'#227'o de Conveniados (Produtor)'
  ClientHeight = 258
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 394
    Height = 242
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 0
    object Label1: TLabel
      Left = 17
      Top = 56
      Width = 173
      Height = 13
      Caption = 'Selecione o arquivo para importa'#231#227'o'
    end
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 100
      Height = 13
      Caption = 'Selecione a Empresa'
    end
    object ButAjuda: TSpeedButton
      Left = 3
      Top = 207
      Width = 60
      Height = 22
      Hint = 'Ajuda'
      Caption = 'Ajuda'
      Flat = True
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00C3CACE00A9AFB300777C7E00505355004043440042454600565A
        5B0084898C00B0B7BA00C5CDD100FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00BFC6CA008A909200EAD6CB00F4E8E300F4F4F300F0F2F300F2F1F100F3E5
        DD00CCBAAE00484B4C00989EA100C3CACE00FF00FF00FF00FF00FF00FF00C0C8
        CC0098979600F5EBE500F0F1F200E4DDDB00BE8B7200C2785200D5AB9600E9E9
        E900F0F1F200F2E1D80037393A00979DA000C4CCD000FF00FF00FF00FF00A2A6
        A700F7F1ED00F0F4F500C8805D00BF5B2A00C2663800ECEEEF00C25D2B00BD59
        2700D1A18900EFF1F100F3E2D90044474800ABB2B500C7CFD300FF00FF00F1DE
        D300F4F6F600C46E4300C5633200CB663300C06F4700FEFFFF00CA622E00C865
        3300C2613000D0987C00F2F3F400DCC8BD00787D7F00C2C9CD00D3D1D000F9FA
        F900E1C1B100C6633200CC673500CC673400CC663200CA602B00CC663300CC66
        3400CA653300C05E2D00EBE6E300F6EDE80045484900B4BCBF00ECD8CE00FAFD
        FE00C6663600CD683600CD683500CC663300C7673700FFFFFF00CA5F2900CC66
        3300CC673400C8653400CE896600F7F9FA0094878000A5ACAF00F0DCD100FEFF
        FF00CA653200D06B3800CC663300CC663300CC632F00E4EAEC00D98C6500CC66
        3300CC673400CC683600C6693900F7FAFC00C6B5AB00A0A6AA00F0DCD100FFFF
        FF00D26C3900D36E3B00CC663300CC663300CC663300C5683900FFFFFF00D787
        5F00CD673400CF6B3900CA6B3B00FCFFFF00C7B6AC00A2A9AC00EFDBCF00FFFF
        FF00DF794600DB764400CE683500CB612C00CB643000CC663300ECE7E500FFFF
        FE00CD632F00D4703E00D6855C00FFFFFF00B4A49D00B0B7BA00DCD3CD00FFFF
        FF00FBC9AE00EE8A5700D2764800F1F6F900EBC2AE00CB632F00ECE2DD00F9FC
        FD00D56D3800DC784600F8DFD300FDFAF900686C6E00BFC6CA00FF00FF00F6E8
        E000FFFFFF00FFA36E00FA956100CDC6C300FFFFFF00FCF5F100FFFFFF00E3CA
        BE00EC865300EC936600FFFFFF00EFDBD0009EA5A800C6CED200FF00FF00D3CB
        C600FFFFFF00FFFFFF00FFD09C00FFBB8700E9BFAA00D7C7C000F0C1A900FFA3
        7000FFAE7E00FFFFFF00FCF7F500888A8A00BFC7CB00FF00FF00FF00FF00C7CF
        D300E6D5CD00FFFFFF00FFFFFF00FFFFED00FFFBD100FFEFBB00FFE6B700FFF6
        E800FFFFFF00FDF9F800A9A19D00BBC2C600C7CFD300FF00FF00FF00FF00FF00
        FF00C7CFD300D2CAC600F5E6DE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFC
        FB00F2DFD500A9A9A900BFC6CA00C7CFD300FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DAD5D100EEDAD000F1DDD200EAD9CE00D4D1
        D000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = ButAjudaClick
    end
    object Bevel1: TBevel
      Left = 0
      Top = 108
      Width = 379
      Height = 2
    end
    object Button1: TButton
      Left = 124
      Top = 181
      Width = 131
      Height = 25
      Caption = '&Executar Importa'#231#227'o'
      TabOrder = 4
      OnClick = Button1Click
    end
    object RichEdit1: TRichEdit
      Left = 4
      Top = 112
      Width = 53
      Height = 25
      Lines.Strings = (
        'Importa'#231#227'o de Conveniados'
        '  Para realizar a importa'#231#227'o de um arquivo dos conveniados, '
        '  '#233' necess'#225'rio que o arquivo seja do tipo (.xls) Excel 97-2003 e'
        '  esteja no formato esperado pelo sistema.'
        ''
        
          'Favor usar a Planilha "Importa'#231#227'o Padr'#227'o de Conveniados (Produto' +
          'r).xlsx" na pasta ADMConv'#234'nio_Manuais para importar os conveniad' +
          'os.'
        ''
        'Obs.: Caso tenha algum erro na importa'#231#227'o ser'#225' gerado um arquivo'
        '          chamado C:\erros.txt'
        ''
        'Segue abaixo o formato para o arquivo.'
        ''
        
          '  Campo CHAPA obrigat'#243'rio contendo o n'#250'mero de chapa/matricula d' +
          'o '
        '  conveniado.'
        '  (Obs: Campo do tipo num'#233'rico sem decimais).'
        ''
        '  Campo NOME contendo o nome conveniado.'
        ''
        '  Campo LIMITE contendo o valor do novo limite do conveniado.'
        
          '  (Obs: Campo do tipo num'#233'rico, caso haja decimais separar por v' +
          #237'rgula).'
        ''
        '  Aten'#231#227'o: O nome da planilha deve ser CONV.'
        '  Para o nome do arquivo '#233' sugerido que n'#227'o contenha espa'#231'os.'
        ''
        
          '  * Entende-se por nome do campo o conte'#250'do da primeira c'#233'lula d' +
          'e uma '
        '  coluna de uma planilha do excel.'
        ''
        
          '  Exemplo do formato da planilha, onde todos campos para atualiz' +
          'a'#231#227'o'
        '  foram selecionados:'
        
          'CHAPA |'#9'NOME'#9'                                 | CPF  |'#9' RG      ' +
          ' |'#9'DT_NASCIMENTO'#9'  | LIMITE'#9'     |       NUMDEP        |        ' +
          ' DEP1              |    DEP2      |       DEP3'#9'           |     ' +
          '  DEP4        |DEP5'
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37737  |'#9'ANDREA M S OLIMPIO              | 111  |'#9'111111 |'#9'01/01' +
          '/2010'#9'  |300,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37745  |'#9'ARACI M TAKAI YAMANAKA   | 222  |'#9'222222 |'#9'02/02/2010'#9' ' +
          ' |60,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37736  |'#9'JOSE AP RODRIGUES OLIMPIO  | 333  |'#9'333333 |'#9'03/03/2010' +
          #9'  |10,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37738  |'#9'JOSE FERNANDO DA SILVA'#9' | 444  |'#9'444444 |'#9'04/04/2010'#9'  ' +
          '|5,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37743  |'#9'PAULO CESAR DA SILVA'#9' | 555  |'#9'555555 |'#9'05/05/2010'#9'  |3' +
          '30,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        
          '37744  |'#9'REGINA MARTA DA SILVA'#9' | 666  |'#9'666666 |'#9'06/06/2010'#9'  |' +
          '5330,00'#9#9#9#9#9#9
        
          '--------|----------------------------------|------|----------|--' +
          '----------------------|------------|------------------|---------' +
          '------------|------------|-----------------------|--------------' +
          '-|------'
        ''
        ''
        ''
        ''
        
          'Para maiores informa'#231#245'es acesse a pasta "ADMConv'#234'nio - Manuais" ' +
          'dentro do "pool" '
        
          'atrav'#233's do caminho \\servidor\pool\ADMConv'#234'nio_Manuais e abra o ' +
          'arquivo Importa'#231#227'o_Padr'#227'o_Conveniados _Produtor.pdf ')
      TabOrder = 2
      Visible = False
      WordWrap = False
    end
    object Empresas: TJvDBLookupCombo
      Left = 16
      Top = 24
      Width = 358
      Height = 21
      LookupField = 'EMPRES_ID'
      LookupDisplay = 'EMPRES_ID;NOME'
      LookupDisplayIndex = 1
      LookupSource = DSEmpresas
      TabOrder = 0
      OnKeyPress = EmpresasKeyPress
    end
    object FilenameEdit1: TJvFilenameEdit
      Left = 18
      Top = 76
      Width = 353
      Height = 21
      Filter = 'Planilha do Excel (*.xls;*.xlsx)|*.xls;*.xlsx'
      InitialDir = 'C:\'
      TabOrder = 1
      Text = 'C:\AdmCartaoBella\'
      OnKeyPress = EmpresasKeyPress
    end
    object cbImportaApenasDependente: TCheckBox
      Left = 152
      Top = 144
      Width = 201
      Height = 17
      Caption = 'Importa Apenas Dependente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 242
    Width = 394
    Height = 16
    Align = alBottom
    TabOrder = 2
  end
  object cbPlanHasDependente: TCheckBox
    Left = 152
    Top = 120
    Width = 201
    Height = 17
    Caption = 'Planilha com Dependentes?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object DSEmpresas: TDataSource
    DataSet = QEmpresas
    Left = 280
    Top = 8
  end
  object Table1: TADOTable
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Teste de Convers' +
      'ao.xls;Extended Properties=Excel 8.0;Persist Security Info=False'
    TableDirect = True
    TableName = 'Conv$'
    Left = 88
    Top = 128
  end
  object QConv: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select CONV_ID,'
      'EMPRES_ID,'
      'CHAPA,'
      'SENHA,'
      'TITULAR,'
      'CPF,'
      'RG,'
      'DT_NASCIMENTO,'
      'CONTRATO,'
      'LIMITE_MES,'
      'LIMITE_TOTAL,'
      'LIMITE_PROX_FECHAMENTO,'
      'LIBERADO,BANCO,'
      'TIPOPAGAMENTO,'
      'DTCADASTRO,'
      'DTALTERACAO,'
      'OPERADOR,'
      'SALARIO,'
      'APAGADO,'
      'GRUPO_CONV_EMP,'
      'SERIE,'
      'MODELO_CARTAO_CANTINEX'
      'from CONVENIADOS')
    Left = 24
    Top = 24
    object QConvCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QConvEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QConvCHAPA: TFloatField
      FieldName = 'CHAPA'
    end
    object QConvSENHA: TStringField
      FieldName = 'SENHA'
      Size = 40
    end
    object QConvTITULAR: TStringField
      FieldName = 'TITULAR'
      Size = 58
    end
    object QConvCONTRATO: TIntegerField
      FieldName = 'CONTRATO'
    end
    object QConvLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QConvLIMITE_TOTAL: TBCDField
      FieldName = 'LIMITE_TOTAL'
      Precision = 15
      Size = 2
    end
    object QConvLIMITE_PROX_FECHAMENTO: TBCDField
      FieldName = 'LIMITE_PROX_FECHAMENTO'
      Precision = 15
      Size = 2
    end
    object QConvLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QConvBANCO: TIntegerField
      FieldName = 'BANCO'
    end
    object QConvTIPOPAGAMENTO: TStringField
      FieldName = 'TIPOPAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object QConvDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QConvDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QConvOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QConvSALARIO: TBCDField
      FieldName = 'SALARIO'
      Precision = 15
      Size = 2
    end
    object QConvAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QConvGRUPO_CONV_EMP: TIntegerField
      FieldName = 'GRUPO_CONV_EMP'
    end
    object QConvSERIE: TStringField
      FieldName = 'SERIE'
      FixedChar = True
      Size = 4
    end
    object QConvMODELO_CARTAO_CANTINEX: TIntegerField
      FieldName = 'MODELO_CARTAO_CANTINEX'
    end
    object QConvCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QConvRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QConvDT_NASCIMENTO: TDateTimeField
      FieldName = 'DT_NASCIMENTO'
    end
  end
  object QCartao: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from CARTOES where cartao_id = 0')
    Left = 176
    Top = 72
    object QCartaoCARTAO_ID: TIntegerField
      FieldName = 'CARTAO_ID'
    end
    object QCartaoCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCartaoNOME: TStringField
      FieldName = 'NOME'
      Size = 58
    end
    object QCartaoLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCartaoCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QCartaoDIGITO: TWordField
      FieldName = 'DIGITO'
    end
    object QCartaoTITULAR: TStringField
      FieldName = 'TITULAR'
      FixedChar = True
      Size = 1
    end
    object QCartaoJAEMITIDO: TStringField
      FieldName = 'JAEMITIDO'
      FixedChar = True
      Size = 1
    end
    object QCartaoAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCartaoLIMITE_MES: TBCDField
      FieldName = 'LIMITE_MES'
      Precision = 15
      Size = 2
    end
    object QCartaoCODCARTIMP: TStringField
      FieldName = 'CODCARTIMP'
    end
    object QCartaoPARENTESCO: TStringField
      FieldName = 'PARENTESCO'
      Size = 40
    end
    object QCartaoDATA_NASC: TDateTimeField
      FieldName = 'DATA_NASC'
    end
    object QCartaoNUM_DEP: TIntegerField
      FieldName = 'NUM_DEP'
    end
    object QCartaoFLAG: TStringField
      FieldName = 'FLAG'
      FixedChar = True
      Size = 1
    end
    object QCartaoDTEMISSAO: TDateTimeField
      FieldName = 'DTEMISSAO'
    end
    object QCartaoCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object QCartaoRG: TStringField
      FieldName = 'RG'
      Size = 13
    end
    object QCartaoVIA: TIntegerField
      FieldName = 'VIA'
    end
    object QCartaoDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCartaoDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCartaoOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCartaoDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCartaoOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCartaoCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
    end
    object QCartaoATIVO: TStringField
      FieldName = 'ATIVO'
      FixedChar = True
      Size = 1
    end
    object QCartaoSENHA: TStringField
      FieldName = 'SENHA'
      Size = 45
    end
    object QCartaoCVV: TStringField
      FieldName = 'CVV'
      FixedChar = True
      Size = 3
    end
    object QCartaoEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCartaoSERIE: TStringField
      FieldName = 'SERIE'
      FixedChar = True
      Size = 4
    end
    object QCartaoMODELO_CARTAO_CANTINEX: TIntegerField
      FieldName = 'MODELO_CARTAO_CANTINEX'
    end
    object QCartaodt_nascimento_cartao: TStringField
      FieldName = 'dt_nascimento_cartao'
      Size = 10
    end
  end
  object QEmpresas: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select '
      '  empres_id, '
      '  nome, '
      '  USA_COD_IMPORTACAO,'
      '  MOD_CART_ID'
      'from empresas '
      'where apagado <> '#39'S'#39' '
      'order by empres_id')
    Left = 232
    Top = 16
    object QEmpresasempres_id: TAutoIncField
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object QEmpresasnome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object QEmpresasUSA_COD_IMPORTACAO: TStringField
      FieldName = 'USA_COD_IMPORTACAO'
      FixedChar = True
      Size = 1
    end
    object QEmpresasMOD_CART_ID: TIntegerField
      FieldName = 'MOD_CART_ID'
    end
  end
end
