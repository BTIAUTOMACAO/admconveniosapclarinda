inherited FManutTaxaBenificiarioBemEstar: TFManutTaxaBenificiarioBemEstar
  Left = 342
  Top = 150
  Caption = 'Manuten'#231#227'o de Taxas de Mensalidade por Benifici'#225'rio - Bem Estar'
  ClientWidth = 873
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel [0]
    Left = 8
    Top = 32
    Width = 32
    Height = 13
    Caption = 'Label4'
  end
  inherited PageControl1: TPageControl
    Width = 873
    ActivePage = TabFicha
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Width = 865
        DesignSize = (
          865
          72)
        inherited ButBusca: TBitBtn
          Left = 772
          Top = 7
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 781
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 865
        Columns = <
          item
            Expanded = False
            FieldName = 'CONV_ID'
            Title.Caption = 'Cod. Conv'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMPRES_ID'
            Title.Caption = 'Cod. Empres'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONV_ID_BE'
            Title.Caption = 'Cod. Conv Bem Estar'
            Width = 108
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EMPRES_ID_BE'
            Title.Caption = 'Cod. Empres Bem Estar'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Title.Caption = 'Liberado'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAXA_ID'
            Title.Caption = 'Cod. Taxa'
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAXA_VALOR'
            Title.Caption = 'Valor da taxa R$'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Width = 865
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Width = 865
      end
      inherited Panel3: TPanel
        Width = 865
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 861
          Height = 391
          Align = alTop
          Caption = 'Manuten'#231#227'o'
          TabOrder = 0
          object Label11: TLabel
            Left = 8
            Top = 208
            Width = 112
            Height = 13
            Caption = 'Quantidade de Cart'#245'es:'
          end
          object Label12: TLabel
            Left = 264
            Top = 208
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object GroupBox2: TGroupBox
            Left = 2
            Top = 129
            Width = 857
            Height = 72
            Align = alTop
            Caption = 'Bem Estar'
            TabOrder = 0
            object Label4: TLabel
              Left = 56
              Top = 48
              Width = 42
              Height = 13
              Caption = 'Conv ID:'
            end
            object Label5: TLabel
              Left = 184
              Top = 48
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object Label7: TLabel
              Left = 512
              Top = 48
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object edtConvIdBem: TDBEdit
              Left = 104
              Top = 40
              Width = 73
              Height = 21
              DataField = 'CONV_ID_BE'
              DataSource = DSCadastro
              TabOrder = 0
            end
            object edtNomeBem: TDBEdit
              Left = 224
              Top = 40
              Width = 281
              Height = 21
              DataField = 'NM_Conv_Be'
              DataSource = DSCadastro
              TabOrder = 1
            end
            object btnBuscaConvBemEstar: TBitBtn
              Left = 8
              Top = 32
              Width = 41
              Height = 33
              TabOrder = 2
              OnClick = btnBuscaConvBemEstarClick
              Glyph.Data = {
                7E090000424D7E0900000000000036000000280000001D0000001B0000000100
                1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
                A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
                EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
                AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
                596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
                A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
                D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
                C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
                89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
                D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
                C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
                EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
                F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
                BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
                FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
                EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
                99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
                FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
                D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
                BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
                FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
                C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
                ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
                F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
                FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
                C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
                FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
                BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
                CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400}
            end
            object edtEmpresaBem: TDBEdit
              Left = 560
              Top = 40
              Width = 281
              Height = 21
              DataField = 'NM_Emp_BE'
              DataSource = DSCadastro
              TabOrder = 3
            end
          end
          object GroupBox3: TGroupBox
            Left = 2
            Top = 57
            Width = 857
            Height = 72
            Align = alTop
            Caption = 'Origem'
            TabOrder = 1
            object Label8: TLabel
              Left = 56
              Top = 40
              Width = 42
              Height = 13
              Caption = 'Conv ID:'
            end
            object Label9: TLabel
              Left = 184
              Top = 40
              Width = 31
              Height = 13
              Caption = 'Nome:'
            end
            object Label10: TLabel
              Left = 512
              Top = 40
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object edtConvId: TDBEdit
              Left = 104
              Top = 32
              Width = 73
              Height = 21
              DataField = 'CONV_ID'
              DataSource = DSCadastro
              TabOrder = 0
            end
            object edtNome: TDBEdit
              Left = 224
              Top = 32
              Width = 281
              Height = 21
              DataField = 'NM_Conv'
              DataSource = DSCadastro
              TabOrder = 1
            end
            object edtEmpresa: TDBEdit
              Left = 560
              Top = 32
              Width = 281
              Height = 21
              DataField = 'NM_Emp'
              DataSource = DSCadastro
              TabOrder = 2
            end
            object btnBuscaConvPadrao: TBitBtn
              Left = 8
              Top = 24
              Width = 41
              Height = 33
              TabOrder = 3
              OnClick = btnBuscaConvPadraoClick
              Glyph.Data = {
                7E090000424D7E0900000000000036000000280000001D0000001B0000000100
                1800000000004809000000000000000000000000000000000000C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4AFB4B797989A7E7C85C4
                A3A9CACCCFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D49EA1A460768E5C6FAF9281A1C49FA5CACCCF
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4809FB24AAFFF3C85DF5A6DAD9181A1C29EA4CACBCEC8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D486BF
                EA5DBFFD49AEFE3D84DF5B6DAC9181A1C4A0A6CACBCEC8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D485BDEA5CBFFD4A
                AFFF3C84DE596DAE9181A0C4A1A6CAC9CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D483BDEB5CBFFD49AEFE3C84DE
                596DAD93819FC3A2A7C9C8CCC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D486BDEA5BBEFD4AAFFF3C83DD596EAD9180
                A0C2A4ABCACDD1C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D487BEEA5ABDFD49AEFE3B83DE546DAF988FA2C8CED0C8
                D0D4C4C4C7BBB2B5BEB2B1C6BDBCCDC3C3C7BEC1C7C8CBC8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D487BEEA55BBFE4CAFFF598BCA878C95BABDBFC1B4B6BA9690BD9182
                C09C8BC7A797D0B1A1CDAC9FC4A7A3C1B2B5C8CFD3C8D0D4C8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D1D5
                89BFE985C1EAB4C0C7A19896AA8A86CC9E8DE0C0A2F3E7BBFAF8CDFEFED8FAF8
                D5F3EDCEE0CFB9C9A9A0B99F9FC8CFD3C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BFCED8C9C9
                C7BA9896CE9B88F4DAB1FFFAC9FFFECEFFFFD1FFFFD9FFFFDFFFFFE2FFFFF4F4
                EEE9CBAFA5B39A9BC8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C6BBBDCE9D8DF4D7AEFF
                F7C8FFF1BEFFFBC7FFFFD6FFFFE1FFFFEAFFFFF3FFFFFFFFFFFFF4EEE3BA958D
                BDB7BAC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9CED2C9AAA8E0BCA1FFF6C5FFE9B6FFECB9FFFDCB
                FFFFD8FFFFE2FFFFEFFFFFFCFFFFFEFFFFFAFFFFE4E0D0B6A78485C9CED2C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C4C7CDA49AF1DFB7FFF2C3FFDFACFFECB9FFFDCAFFFFD7FFFFE3FFFF
                EFFFFFF7FFFFF6FFFFEDFFFFDAF3EDC9AA837DC9C4C7C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA3
                99FAF5C5FFECBAFFD9A6FFE6B3FFFBC7FFFFD5FFFFDFFFFFE8FFFFECFFFFEBFF
                FFE6FFFFD8F9F6CDB8968AC9C1C4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9C2C5C9A298FEFDCAFFE7B2FF
                D6A3FFE8B4FFF7C3FFFFCDFFFFD7FFFFDDFFFFDFFFFFDFFFFFDCFFFFD3FDFCD0
                BB9688C9C2C5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C9C1C4CBA499FAF4C4FFF3C7FFE9BDFFE2B2FFECB8
                FFFBC8FFFFCFFFFFD2FFFFD6FFFFD5FFFFD1FFFFCDF8E9BEB3897FC9C1C4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C9C8CCC9A79FEEDCBAFFFCDAFFEECEFFDFB2FFDAA8FFEDBBFFF1BFFFF6
                C4FFFBC9FFFBC8FFF3C1FFFECBF3D6ACA57C7AC9C8CCC8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C9D1D5C5AB
                ACDBBEA8FFFFEDFFFDFBFFFBF5FFE3BFFFE4B4FFE5B2FFE5B3FFE7B5FFE6B4FF
                F6C1FFF1C3D3A98EAE9497C9D1D5C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8C3C6C4A097EDDECFFF
                FFFFFFFFFFFFEAC9FFEABFFFDAA9FFD4A1FFDEABFFF4BFFFF2C6EDC098AC817E
                C6CBCFC8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C2B6BAC19E98EDE0D7FFFFFDFFFFEC
                FFF7CAFFF2BEFFF6C2FFFBCBFFEBBDEEBD95B78780C0B9BCC8D0D4C8D0D4C8D0
                D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C2B8BBB38B89CBA791EDD6B1F8E9C0FDF0C5F9E6
                BBF1D2AAD3A793B78E8CC1BBBEC8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C7C9CDB7A1A3A98181AC817AB98D85C59891BE9595BFA9ABC8
                CCD0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D400C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
                C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
                D400}
            end
          end
          object GroupBox4: TGroupBox
            Left = 2
            Top = 15
            Width = 857
            Height = 42
            Align = alTop
            TabOrder = 2
            object Label3: TLabel
              Left = 2
              Top = 15
              Width = 17
              Height = 13
              Align = alCustom
              Caption = ' ID:'
              FocusControl = DBEdit1
            end
            object DBEdit1: TDBEdit
              Left = 26
              Top = 12
              Width = 65
              Height = 21
              Hint = 'C'#243'digo do taxa'
              TabStop = False
              Color = clBtnFace
              DataField = 'ID'
              DataSource = DSCadastro
              ReadOnly = True
              TabOrder = 0
            end
          end
          object edtQtdeCartoes: TDBEdit
            Left = 8
            Top = 224
            Width = 121
            Height = 21
            TabOrder = 3
          end
          object edtValor: TDBEdit
            Left = 264
            Top = 224
            Width = 121
            Height = 21
            DataField = 'TAXA_VALOR'
            DataSource = DSCadastro
            TabOrder = 4
          end
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 865
      end
      inherited GridHistorico: TJvDBGrid
        Width = 865
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 873
  end
  inherited panStatus2: TPanel
    Width = 873
  end
  inherited DSCadastro: TDataSource
    Top = 312
  end
  inherited PopupGrid1: TPopupMenu
    Left = 396
    Top = 288
  end
  inherited QHistorico: TADOQuery
    Left = 460
    Top = 281
  end
  inherited DSHistorico: TDataSource
    Left = 460
    Top = 313
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'SELECT '
      'CONV_BE.ID,'
      'CONV_BE.CONV_ID,'
      'CONV_BE.CONV_ID_BE,'
      'CONV_BE.EMPRES_ID,'
      'CONV_BE.EMPRES_ID_BE,'
      'CONV_BE.TAXA_VALOR,'
      'CONV_BE.DTCADASTRO,'
      'CONV_BE.OPERCADASTRO,'
      'CONV_BE.LIBERADO,'
      'CONV_BE.TAXA_ID'
      'FROM CONVENIADOS_BEM_ESTAR_HAS_EMP_TAXAS CONV_BE '
      'WHERE CONV_BE.ID = 0;')
    Top = 281
    object QCadastroNM_Emp: TStringField
      FieldKind = fkLookup
      FieldName = 'NM_Emp'
      LookupDataSet = QAux_Conv
      LookupKeyFields = 'empres_id'
      LookupResultField = 'fantasia'
      KeyFields = 'EMPRES_ID'
      Lookup = True
    end
    object QCadastroID: TIntegerField
      FieldName = 'ID'
    end
    object QCadastroCONV_ID: TIntegerField
      FieldName = 'CONV_ID'
    end
    object QCadastroCONV_ID_BE: TIntegerField
      FieldName = 'CONV_ID_BE'
    end
    object QCadastroEMPRES_ID: TIntegerField
      FieldName = 'EMPRES_ID'
    end
    object QCadastroEMPRES_ID_BE: TIntegerField
      FieldName = 'EMPRES_ID_BE'
    end
    object QCadastroTAXA_VALOR: TBCDField
      FieldName = 'TAXA_VALOR'
      DisplayFormat = '#,##0.00'
      Precision = 6
      Size = 2
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroLIBERADO: TStringField
      FieldName = 'LIBERADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroTAXA_ID: TStringField
      FieldName = 'TAXA_ID'
      FixedChar = True
      Size = 8
    end
    object QCadastroNM_Conv: TStringField
      FieldKind = fkLookup
      FieldName = 'NM_Conv'
      LookupDataSet = QAux_Conv
      LookupKeyFields = 'conv_id'
      LookupResultField = 'titular'
      KeyFields = 'CONV_ID'
      Lookup = True
    end
    object QCadastroNM_Conv_Be: TStringField
      FieldKind = fkLookup
      FieldName = 'NM_Conv_Be'
      LookupDataSet = QAuxConv_BE
      LookupKeyFields = 'conv_id_be'
      LookupResultField = 'titular'
      KeyFields = 'CONV_ID_BE'
      Lookup = True
    end
    object QCadastroNM_Emp_BE: TStringField
      FieldKind = fkLookup
      FieldName = 'NM_Emp_BE'
      LookupDataSet = QAuxConv_BE
      LookupKeyFields = 'empres_id'
      LookupResultField = 'fantasia'
      KeyFields = 'EMPRES_ID_BE'
      Lookup = True
    end
  end
  object QAux: TADOQuery
    Active = True
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select empres_id, fantasia from empresas where empres_id = 45')
    Left = 592
    Top = 320
    object QAuxempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QAuxfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
  end
  object DSAux: TDataSource
    DataSet = QAux
    Left = 630
    Top = 323
  end
  object DataSource1: TDataSource
    DataSet = QAux_Conv
    Left = 630
    Top = 323
  end
  object QAux_Conv: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'conv_id'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select'
      'emp.empres_id, '
      'emp.fantasia,'
      'conv.titular,'
      'conv.conv_id'
      'FROM empresas emp '
      'INNER JOIN '
      'conveniados conv ON conv.empres_id = emp.empres_id'
      'and conv.conv_id = :conv_id ')
    Left = 592
    Top = 320
    object IntegerField1: TIntegerField
      FieldName = 'empres_id'
    end
    object StringField1: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
    object QAux_Convtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QAux_Convconv_id: TIntegerField
      FieldName = 'conv_id'
    end
  end
  object QAuxConv_BE: TADOQuery
    Connection = DMConexao.AdoCon
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'conv_id_be'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT conv_be.conv_id_be, conv_be.titular, conv_be.empres_id, e' +
        'mp.fantasia '
      'FROM CONVENIADOS_BEM_ESTAR conv_be '
      'INNER JOIN empresas emp ON conv_be.empres_id = emp.empres_id'
      'AND conv_be.CONV_ID_BE = :conv_id_be ')
    Left = 592
    Top = 360
    object QAuxConv_BEconv_id_be: TIntegerField
      FieldName = 'conv_id_be'
    end
    object QAuxConv_BEtitular: TStringField
      FieldName = 'titular'
      Size = 58
    end
    object QAuxConv_BEempres_id: TIntegerField
      FieldName = 'empres_id'
    end
    object QAuxConv_BEfantasia: TStringField
      FieldName = 'fantasia'
      Size = 60
    end
  end
  object DSAuxConv_BE: TDataSource
    DataSet = QAuxConv_BE
    Left = 630
    Top = 363
  end
end
