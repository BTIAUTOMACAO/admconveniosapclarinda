inherited FCadProgramas: TFCadProgramas
  Left = 282
  Top = 110
  Caption = 'Cadastro de Regras de Conv'#234'nios'
  ClientHeight = 508
  ClientWidth = 749
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 1
    Top = 1
    Width = 59
    Height = 13
    Caption = 'Programa ID'
  end
  object Label4: TLabel [1]
    Left = 73
    Top = 1
    Width = 91
    Height = 13
    Caption = 'Nome do Programa'
  end
  object Label5: TLabel [2]
    Left = 1
    Top = 49
    Width = 83
    Height = 13
    Caption = 'Layout do Cupom'
  end
  inherited PageControl1: TPageControl
    Width = 749
    Height = 467
    ActivePage = TabProdutos
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 348
        Width = 741
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 642
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 741
        Height = 348
        Columns = <
          item
            Expanded = False
            FieldName = 'PROG_ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 420
        Width = 741
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 407
        Width = 741
      end
      inherited Panel3: TPanel
        Width = 741
        Height = 407
        object Label6: TLabel
          Left = 9
          Top = 10
          Width = 59
          Height = 13
          Caption = 'Programa ID'
          FocusControl = DBEdit1
        end
        object Label7: TLabel
          Left = 81
          Top = 10
          Width = 91
          Height = 13
          Caption = 'Nome do Programa'
          FocusControl = DBEdit2
        end
        object Label11: TLabel
          Left = 560
          Top = 10
          Width = 22
          Height = 13
          Caption = 'Final'
        end
        object Label12: TLabel
          Left = 448
          Top = 10
          Width = 27
          Height = 13
          Caption = 'Inicial'
        end
        object DBEdit1: TDBEdit
          Left = 9
          Top = 25
          Width = 65
          Height = 21
          Color = clBtnFace
          DataField = 'PROG_ID'
          DataSource = DSCadastro
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 81
          Top = 25
          Width = 360
          Height = 21
          CharCase = ecUpperCase
          DataField = 'NOME'
          DataSource = DSCadastro
          TabOrder = 1
        end
        object JvDBDateEdit1: TJvDBDateEdit
          Left = 560
          Top = 25
          Width = 105
          Height = 21
          DataField = 'DT_FIM'
          DataSource = DSCadastro
          DefaultToday = True
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 3
        end
        object rdgVerificaValor: TDBRadioGroup
          Left = 8
          Top = 50
          Width = 265
          Height = 79
          Hint = 
            'Aplicar desconto nos valores definidos em:'#13#10'B = Sistema BELLA Co' +
            'nv'#13#10'A = BELLA Adm Conv'#234'nios'#13#10'T = Verifica o menos pre'#231'o entre el' +
            'es'
          Caption = 'Aplicar Descontos em cima dos valores unit'#225'rios de:'
          DataField = 'VERIFICA_VALOR'
          DataSource = DSCadastro
          Items.Strings = (
            'Sistema do Cliente'
            'Bella Adm Conv'#234'nios'
            'Menor valor entre ambos')
          TabOrder = 4
          Values.Strings = (
            'B'
            'A'
            'M')
        end
        object JvDBDateEdit2: TJvDBDateEdit
          Left = 448
          Top = 25
          Width = 105
          Height = 21
          DataField = 'DT_INICIO'
          DataSource = DSCadastro
          DefaultToday = True
          NumGlyphs = 2
          ShowNullDate = False
          TabOrder = 2
        end
      end
    end
    object TabProdutos: TTabSheet [2]
      Caption = '&Produtos'
      ImageIndex = 2
      OnHide = TabProdutosHide
      OnShow = TabProdutosShow
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 741
        Height = 31
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        DesignSize = (
          741
          31)
        object btnRepassar: TBitBtn
          Left = 3
          Top = 3
          Width = 430
          Height = 25
          Caption = '&Incluir Produtos'
          TabOrder = 0
          OnClick = btnRepassarClick
          Glyph.Data = {
            5A030000424D5A030000000000005A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004900000000000000117611001379
            1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
            37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
            4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
            6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
            7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
            840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
            C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
            D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
            DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
            4848484848484848484848484848484848484848484848484848484848484848
            484848480C00000C484848484848484848484848302323304848484848484848
            4848484801161601484848484848484848484848243A3A244848484848484848
            4848484802181802484848484848484848484848253C3C254848484848484848
            48484848031A1A03484848484848484848484848263D3D264848484848484848
            48484848041B1B04484848484848484848484848273F3F274848484848484813
            0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
            1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
            2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
            0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
            484848480D21210D484848484848484848484848324444324848484848484848
            4848484810282810484848484848484848484848354545354848484848484848
            4848484812292912484848484848484848484848374646374848484848484848
            48484848152A2A15484848484848484848484848394747394848484848484848
            484848481E16161E484848484848484848484848423A3A424848484848484848
            484848484848484848484848484848484848484848484848484848484848}
          NumGlyphs = 2
        end
        object btnImportListaProg: TBitBtn
          Left = 651
          Top = 3
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          BiDiMode = bdRightToLeft
          Caption = 'Import. Lista'
          ParentBiDiMode = False
          TabOrder = 2
          OnClick = btnImportListaProgClick
        end
        object btnBuscar: TBitBtn
          Left = 584
          Top = 3
          Width = 57
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '&Buscar'
          TabOrder = 1
          OnClick = btnBuscarClick
        end
        object edtBusca: TEdit
          Left = 536
          Top = 4
          Width = 41
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          CharCase = ecUpperCase
          TabOrder = 4
          OnKeyDown = edtBuscaKeyDown
        end
        object cbCampos: TComboBox
          Left = 440
          Top = 4
          Width = 89
          Height = 21
          Style = csDropDownList
          CharCase = ecUpperCase
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 3
          Text = 'COD. BARRAS'
          Items.Strings = (
            'COD. BARRAS'
            'DESCRI'#199#195'O')
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 31
        Width = 741
        Height = 408
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object PageControl2: TPageControl
          Left = 224
          Top = 0
          Width = 826
          Height = 510
          ActivePage = TabSheet1
          Align = alCustom
          Style = tsFlatButtons
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Rela'#231#227'o de Produtos'
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 741
          Height = 258
          Align = alClient
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object Panel4: TPanel
            Left = 2
            Top = 225
            Width = 737
            Height = 31
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 1
            object lblSomaProd: TLabel
              Left = 304
              Top = 8
              Width = 95
              Height = 13
              Caption = 'Total de produtos: 0'
            end
            object btnGravar: TBitBtn
              Left = 1
              Top = 2
              Width = 75
              Height = 25
              Caption = '&Gravar'
              TabOrder = 0
              OnClick = btnGravarClick
              Glyph.Data = {
                A6030000424DA603000000000000A60100002800000020000000100000000100
                08000000000000020000232E0000232E00005C00000000000000343434003535
                3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
                49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
                63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
                800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
                A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
                B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
                BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
                C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
                D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
                CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
                E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
                F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
                3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
                3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
                2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
                284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
                234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
                54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
                3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
                323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
                5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
                57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
                58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
                5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
                5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
                53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
                5B5B5B5B5B5B5B5B5B5B}
              NumGlyphs = 2
            end
            object btnApagar: TBitBtn
              Left = 153
              Top = 2
              Width = 75
              Height = 25
              Caption = '&Apagar'
              TabOrder = 2
              OnClick = btnApagarClick
              Glyph.Data = {
                82030000424D8203000000000000820100002800000020000000100000000100
                08000000000000020000232E0000232E000053000000000000000021CC000324
                CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
                F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
                FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
                FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
                FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
                CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
                D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
                F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
                FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
                E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
                F400FFFFFF005252525252525252525252525252525252525252525252525252
                525252525252525216002552525252525225001652525252341A4A5252525252
                524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
                4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
                1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
                2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
                30471E4C52525252523B05211212121221053B5252525252524C244731313131
                47244C525252525252523C0617131317063C52525252525252524D2636323236
                264D52525252525252523D0714141414073D52525252525252524E2733333333
                274E525252525252523E08151521211515083E5252525252524F283535474735
                35284F52525252523F091818210909211818093F525252524F29434347292947
                4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
                4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
                2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
                512E472E4852525223104452525252525244102352525252492F515252525252
                52512F4952525252525252525252525252525252525252525252525252525252
                525252525252}
              NumGlyphs = 2
            end
            object btnCancelar: TBitBtn
              Left = 77
              Top = 2
              Width = 75
              Height = 25
              Caption = '&Cancelar'
              TabOrder = 1
              OnClick = btnCancelarClick
              Glyph.Data = {
                0E040000424D0E040000000000000E0200002800000020000000100000000100
                08000000000000020000232E0000232E000076000000000000000021CC001031
                DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
                DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
                FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
                F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
                F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
                FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
                E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
                ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
                FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
                C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
                CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
                D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
                E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
                E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
                F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
                75757575757575757575757575757575757575757575622F080000082F627575
                757575757575705E493434495E70757575757575753802030E11110E03023875
                7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
                7575757567354D5354555554534D35677575756307102A00337575442C151007
                63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
                367575604B545568345E7575745B544B607575171912301C3700317575401219
                1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
                057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
                0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
                217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
                3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
                65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
                757575756C566058483434485860566C75757575754324283237373228244375
                75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
                757575757575736A5D55555D6A73757575757575757575757575757575757575
                757575757575757575757575757575757575}
              NumGlyphs = 2
            end
          end
          object grdProd: TJvDBGrid
            Left = 2
            Top = 2
            Width = 737
            Height = 223
            Align = alClient
            BorderStyle = bsNone
            DataSource = dsProdutos
            DefaultDrawing = False
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
            PopupMenu = ppmProd
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnColExit = grdProdColExit
            AutoAppend = False
            TitleButtons = True
            OnTitleBtnClick = grdProdTitleBtnClick
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 17
            Columns = <
              item
                Expanded = False
                FieldName = 'prod_id'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'descricao'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'codbarras'
                Width = 100
                Visible = True
              end
              item
                Color = 16776176
                Expanded = False
                FieldName = 'prc_unit_18'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Vl. 18%'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Color = 16776176
                Expanded = False
                FieldName = 'perc_desc'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 79
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prc_unit'
                Title.Caption = 'Vl 17%'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prc_unit_19'
                Title.Caption = 'Vl 19%'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESCONTO'
                Visible = False
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'qtd_max'
                Title.Alignment = taCenter
                Width = 75
                Visible = True
              end
              item
                Alignment = taCenter
                ButtonStyle = cbsEllipsis
                Expanded = False
                FieldName = 'obrig_receita'
                PickList.Strings = (
                  'S'
                  'N')
                Title.Alignment = taCenter
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'laboratorios'
                Width = 180
                Visible = True
              end>
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 258
          Width = 741
          Height = 150
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 2
          object Panel7: TPanel
            Left = 1
            Top = 1
            Width = 739
            Height = 148
            Align = alClient
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Caption = 'Panel7'
            TabOrder = 0
            object Panel10: TPanel
              Left = 2
              Top = 115
              Width = 735
              Height = 31
              Align = alBottom
              BevelInner = bvRaised
              BevelOuter = bvLowered
              TabOrder = 1
              object SpeedButton1: TSpeedButton
                Left = 255
                Top = 5
                Width = 28
                Height = 22
                Flat = True
                Glyph.Data = {
                  5A030000424D5A030000000000005A0100002800000020000000100000000100
                  08000000000000020000232E0000232E00004900000000000000117611001379
                  1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
                  37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
                  4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
                  6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
                  7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
                  840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
                  C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
                  D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
                  DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
                  4848484848484848484848484848484848484848484848484848484848484848
                  484848480C00000C484848484848484848484848302323304848484848484848
                  4848484801161601484848484848484848484848243A3A244848484848484848
                  4848484802181802484848484848484848484848253C3C254848484848484848
                  48484848031A1A03484848484848484848484848263D3D264848484848484848
                  48484848041B1B04484848484848484848484848273F3F274848484848484813
                  0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
                  1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
                  2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
                  0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
                  484848480D21210D484848484848484848484848324444324848484848484848
                  4848484810282810484848484848484848484848354545354848484848484848
                  4848484812292912484848484848484848484848374646374848484848484848
                  48484848152A2A15484848484848484848484848394747394848484848484848
                  484848481E16161E484848484848484848484848423A3A424848484848484848
                  484848484848484848484848484848484848484848484848484848484848}
                NumGlyphs = 2
                OnClick = SpeedButton1Click
              end
              object SpeedButton2: TSpeedButton
                Left = 284
                Top = 5
                Width = 28
                Height = 22
                Flat = True
                Glyph.Data = {
                  BE020000424DBE02000000000000BE0000002800000020000000100000000100
                  08000000000000020000232E0000232E000022000000000000000526CF000D2E
                  D4001335DB001436D9001D3FDE002446E2002A4CE6002F51F0004059D9004769
                  FF004D6FFF005476FF005B7DFF006183FF006587FF00728CFF00B8B9B900BABB
                  BC00BEBFBF0087A9FF00C1C2C300C4C5C500C5C6C700C8C9C900C9CACA00D1D2
                  D200D3D4D400D6D6D700D7D8D800D9DADA00DBDBDC00DCDDDD00E5E5E600FFFF
                  FF00212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121210800010304050606050403010008212117101112141516161514121110
                  172121020B0B0B0B0B0B0B0B0B0B0B0B022121121B1B1B1B1B1B1B1B1B1B1B1B
                  1221210713131313131313131313131307212118202020202020202020202020
                  1821210F090A0B0C0D0E0E0D0C0B0A090F21211F191A1B1C1D1E1E1D1C1B1A19
                  1F21212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121212121212121212121212121212121212121212121212121212121212121
                  2121}
                NumGlyphs = 2
                OnClick = SpeedButton2Click
              end
              object Label9: TLabel
                Left = 3
                Top = 9
                Width = 23
                Height = 13
                Caption = 'Qtde'
              end
              object Label10: TLabel
                Left = 141
                Top = 10
                Width = 14
                Height = 13
                Caption = '(%)'
              end
              object edQtd: TEdit
                Left = 31
                Top = 5
                Width = 63
                Height = 21
                TabOrder = 0
                Text = '1'
              end
              object UpDown1: TUpDown
                Left = 94
                Top = 5
                Width = 16
                Height = 21
                Associate = edQtd
                Position = 1
                TabOrder = 1
              end
              object edDesc: TJvValidateEdit
                Left = 159
                Top = 6
                Width = 93
                Height = 21
                CriticalPoints.MaxValueIncluded = False
                CriticalPoints.MinValueIncluded = False
                DisplayFormat = dfFloat
                DecimalPlaces = 2
                TabOrder = 2
              end
            end
            object grdQtde: TJvDBGrid
              Left = 2
              Top = 2
              Width = 735
              Height = 113
              Align = alClient
              BorderStyle = bsNone
              DataSource = dsProdQtde
              DefaultDrawing = False
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              AutoAppend = False
              SelectColumnsDialogStrings.Caption = 'Select columns'
              SelectColumnsDialogStrings.OK = '&OK'
              SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
              EditControls = <>
              RowsHeight = 16
              TitleRowHeight = 17
              Columns = <
                item
                  Expanded = False
                  FieldName = 'qtd'
                  Title.Caption = 'Quantidade'
                  Width = 83
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'perc_desc'
                  Title.Caption = 'Descontos'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'desconto'
                  Title.Caption = 'Pre'#231'o'
                  Width = 100
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object tabEmpr: TTabSheet [3]
      Caption = 'Empresas'
      ImageIndex = 4
      OnHide = tabEmprHide
      OnShow = tabEmprShow
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 741
        Height = 439
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 406
          Width = 737
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object btnGravaEmpr: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Gravar'
            TabOrder = 0
            OnClick = btnGravaEmprClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancelEmpr: TBitBtn
            Left = 80
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Cancelar'
            TabOrder = 1
            OnClick = btnCancelEmprClick
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
          object BitBtn1: TBitBtn
            Left = 216
            Top = 3
            Width = 97
            Height = 25
            Caption = 'Alterar todos'
            TabOrder = 2
            OnClick = BitBtn1Click
            Glyph.Data = {
              E6020000424DE602000000000000E60000002800000020000000100000000100
              08000000000000020000232E0000232E00002C00000000000000323232000054
              0000116E110065656500323298003232CC003265CC004C7FE5006565DD006565
              FF0021872100329832004CB24C0065CC65006598FF008787870098989800A9A9
              A900A8AAAA00B2B3B400B5B6B700BABABA00BBBCBD00BCBDBE0098BEFF00BFBF
              C000C3C4C400C9CACA00CCCCCC00CDCECE00CECFCF00D2D3D300D4D5D500D8D9
              D900DCDCDC00DDDDDD00DFDFE000CCE5FF00E2E3E300EAEAEB00EBEBEB00F2F2
              F200F7F7F700FFFFFF002B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B2B000004062B2B2B2B2B2B2B2B2B2B2B2B1212141B2B
              2B2B2B2B2B2B2B2B2B2B2B00180E0E012B2B2B2B2B2B2B2B2B2B2B1228242412
              2B2B2B2B2B2B2B2B2B2B2B072518010A012B2B2B2B2B2B2B2B2B2B202A281216
              122B2B2B2B2B2B2B2B2B2B0E25020B0A0A012B2B2B2B2B2B2B2B2B242A131A16
              16122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E1A
              1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E
              1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B1621
              1E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16
              211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B
              16211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A032B2B2B2B2B2B2B2B
              2B16211E1A1616192B2B2B2B2B2B2B2B2B2B0A0D0C0B0F15032B2B2B2B2B2B2B
              2B2B16211E1A1D26192B2B2B2B2B2B2B2B2B2B0A0D111C0F052B2B2B2B2B2B2B
              2B2B2B162122271D172B2B2B2B2B2B2B2B2B2B2B10231108052B2B2B2B2B2B2B
              2B2B2B2B2029221E172B2B2B2B2B2B2B2B2B2B2B2B1009092B2B2B2B2B2B2B2B
              2B2B2B2B2B201F1F2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B}
            NumGlyphs = 2
          end
        end
        object grdEmpr: TJvDBGrid
          Left = 2
          Top = 2
          Width = 737
          Height = 404
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsProgEmpr
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'empres_id'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nome'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fantasia'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Liberado'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              PickList.Strings = (
                'S'
                'N')
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end>
        end
      end
    end
    object tabCred: TTabSheet [4]
      Caption = 'Estabelecimentos'
      ImageIndex = 5
      OnHide = tabCredHide
      OnShow = tabCredShow
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 741
        Height = 439
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel16: TPanel
          Left = 2
          Top = 406
          Width = 737
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 1
          object btnGravaCred: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Gravar'
            TabOrder = 0
            OnClick = btnGravaCredClick
            Glyph.Data = {
              A6030000424DA603000000000000A60100002800000020000000100000000100
              08000000000000020000232E0000232E00005C00000000000000343434003535
              3500383838003A3A3A003B3B3B00404040004444440046464600484848004949
              49004E4E4E005353530056565600575757005A5A5A005C5C5C005F5F5F006363
              63006565650067676700686868006C6C6C0071717100767676007C7C7C008080
              800081818100878787008C8C8C00919191009595950098989800A5A6A700A7A8
              A900A9A9A900A8A9AA00A8AAAA00AAABAC00ACADAE00ADAEAF00ADAFAF00AEAF
              B000B0B1B200B3B3B300B3B4B400B4B5B600B6B7B800B7B8B900B8B9B900BABB
              BC00BBBCBD00BCBDBE00BEBFBF00D9CCBE00FFD0A000FFD6AB00FFDCB800C0C1
              C200C2C2C200C3C4C400C5C6C700C7C8C800CACACB00CCCCCC00CCCDCD00CFCF
              D000D1D1D200D1D2D200D3D3D300D9DADA00DDDDDD00DEDFDF00FFE2C300FFE6
              CC00FFEEDD00E4E5E500E7E7E700E8E8E800E9EAEA00ECECEC00EDEDED00FFF1
              E400FFF6EE00F0F0F000F1F1F100F2F2F200F6F6F600F9F9F900FBFBFB00FFFC
              F800FEFEFE00FFFFFF005B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B5B5B3F031517191B1B1917150301125B5B5B4E23343B
              3D3E3E3D3B342320325B5B3F000322141422222222220312005B5B4E20234533
              3345454545452332205B5B0214072B0E0E2B2B2B2B2B0714025B5B213327472E
              2E47474747472733215B5B04150C3A08083A3A3A3A3A0C15045B5B24342D4B28
              284B4B4B4B4B2D34245B5B05161344030344444444441316055B5B2539334F23
              234F4F4F4F4F3339255B5B0617173A4646464646463A1717065B5B263B3B4B54
              54545454544B3B3B265B5B09181818181818181818181818095B5B293C3C3C3C
              3C3C3C3C3C3C3C3C295B5B0A1A151212121212121212151A0A5B5B2A3D343232
              323232323232343D2A5B5B0B1B354A4A4A4A4A4A4A4A351B0B5B5B2C3E4D5656
              5656565656564D3E2C5B5B0D1C515151515151515151511C0D5B5B2D40575757
              57575757575757402D5B5B0F1D525252525252525252521D0F5B5B2F41585858
              58585858585858412F5B5B101E595959595959595959591E105B5B30425A5A5A
              5A5A5A5A5A5A5A42305B5B111F5B5B5B5B5B5B5B5B5B5B1F115B5B31435B5B5B
              5B5B5B5B5B5B5B43315B5B1C1436373848494948383736141C5B5B40334C4E50
              53555553504E4C33405B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B
              5B5B5B5B5B5B5B5B5B5B}
            NumGlyphs = 2
          end
          object btnCancelCred: TBitBtn
            Left = 80
            Top = 3
            Width = 75
            Height = 25
            Caption = 'Cancelar'
            TabOrder = 1
            OnClick = btnCancelCredClick
            Glyph.Data = {
              0E040000424D0E040000000000000E0200002800000020000000100000000100
              08000000000000020000232E0000232E000076000000000000000021CC001031
              DA001D3BD5001739E4001C3EE2001E40E3002547DB002644DC00314CD6003D57
              DB002446E8002648E9002D4FE500284AEA002749F3002A4CF3002F51F4003254
              FF00385AF4003B5DFF003F61E7003E60FD00425CDE00445FE400536ADF004163
              F7004062FC004062FF004365FF00496BFA004E6DF8004F71F0004E70F700546F
              F0005476ED005D74E6005775FD00587AFF005C7EFB00647EF8005F81FE006183
              FF006385FE006783FF006984FC00698BFD006F91FD007D8EE5007088F5007F90
              E5007698FF007B92FC00B5B6B700BCBDBE008697EC0087A9FF009BA8EC009DAA
              ED0096A7F8009FB0FF00BFBFC000A3AFEE00A1B2FF00A8B5F200B2BFF900B2BF
              FD00BFC0C100B3C1FE00B8C4FE00C0C1C200C1C2C300C2C3C300C3C4C400C4C5
              C500C5C5C600C5C6C700C6C7C800C7C8C800C8C9C900C9CACA00CACACB00CBCB
              CC00CBCCCD00CECFCF00CFCFD000D0D0D100D1D2D200D2D3D300D3D4D400D5D5
              D600D6D7D700D7D8D800D8D9D900D9DADA00DADADB00DBDBDC00DDDEDE00DFDF
              E000D5DAF700D7DCF800DEE2F900DEE4FE00E2E3E300E3E4E400E4E5E500E5E5
              E600E8E8E800EBEBEB00ECECEC00EEEEEE00E0E6FF00E4E8FE00F4F4F400F6F6
              F600F7F7F700F8F8F800F9F9F900FFFFFF007575757575757575757575757575
              75757575757575757575757575757575757575757575622F080000082F627575
              757575757575705E493434495E70757575757575753802030E11110E03023875
              7575757575663C424C51514C423C66757575757539010F131B1C1C1B130F0139
              7575757567354D5354555554534D35677575756307102A00337575442C151007
              63757570474F5D346075756D5D534F47707575360D1A1C37003175756F271A0D
              367575604B545568345E7575745B544B607575171912301C3700317575401219
              1775755254525E5568345E75756B525452757505250B753E1C37003175750B25
              057575465A4A756A5568345E75754A5A4675750A290475753E1C370031750429
              0A7575495D4575756A5568345E75455D49757521260C3F75753E1C3700230C26
              217575565B4C6A75756A556834564C5B5675753A201F166475753E1C37001F20
              3A7575675756507175756A5568345657677575651E2D14093D7575181C372D1E
              65757572565F524E6975755355685F5672757575411D2E2206000006222E1D41
              757575756C566058483434485860566C75757575754324283237373228244375
              75757575756C595C616868615C596C757575757575756E3B2B1C1C2B3B6E7575
              757575757575736A5D55555D6A73757575757575757575757575757575757575
              757575757575757575757575757575757575}
            NumGlyphs = 2
          end
          object BitBtn2: TBitBtn
            Left = 216
            Top = 3
            Width = 97
            Height = 25
            Caption = 'Alterar todos'
            TabOrder = 2
            OnClick = BitBtn2Click
            Glyph.Data = {
              E6020000424DE602000000000000E60000002800000020000000100000000100
              08000000000000020000232E0000232E00002C00000000000000323232000054
              0000116E110065656500323298003232CC003265CC004C7FE5006565DD006565
              FF0021872100329832004CB24C0065CC65006598FF008787870098989800A9A9
              A900A8AAAA00B2B3B400B5B6B700BABABA00BBBCBD00BCBDBE0098BEFF00BFBF
              C000C3C4C400C9CACA00CCCCCC00CDCECE00CECFCF00D2D3D300D4D5D500D8D9
              D900DCDCDC00DDDDDD00DFDFE000CCE5FF00E2E3E300EAEAEB00EBEBEB00F2F2
              F200F7F7F700FFFFFF002B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B2B000004062B2B2B2B2B2B2B2B2B2B2B2B1212141B2B
              2B2B2B2B2B2B2B2B2B2B2B00180E0E012B2B2B2B2B2B2B2B2B2B2B1228242412
              2B2B2B2B2B2B2B2B2B2B2B072518010A012B2B2B2B2B2B2B2B2B2B202A281216
              122B2B2B2B2B2B2B2B2B2B0E25020B0A0A012B2B2B2B2B2B2B2B2B242A131A16
              16122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E1A
              1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16211E
              1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B1621
              1E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B16
              211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A012B2B2B2B2B2B2B2B2B
              16211E1A1616122B2B2B2B2B2B2B2B2B2B0A0D0C0B0A0A032B2B2B2B2B2B2B2B
              2B16211E1A1616192B2B2B2B2B2B2B2B2B2B0A0D0C0B0F15032B2B2B2B2B2B2B
              2B2B16211E1A1D26192B2B2B2B2B2B2B2B2B2B0A0D111C0F052B2B2B2B2B2B2B
              2B2B2B162122271D172B2B2B2B2B2B2B2B2B2B2B10231108052B2B2B2B2B2B2B
              2B2B2B2B2029221E172B2B2B2B2B2B2B2B2B2B2B2B1009092B2B2B2B2B2B2B2B
              2B2B2B2B2B201F1F2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B
              2B2B2B2B2B2B2B2B2B2B}
            NumGlyphs = 2
          end
        end
        object grdCred: TJvDBGrid
          Left = 2
          Top = 2
          Width = 737
          Height = 404
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsProgCred
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'cred_id'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'nome'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'fantasia'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'liberado'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              PickList.Strings = (
                'S'
                'N')
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'obriga_desconto'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 103
              Visible = True
            end>
        end
      end
    end
    object tabBlackList: TTabSheet [5]
      Caption = 'BlackList'
      ImageIndex = 6
      OnShow = tabBlackListShow
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 741
        Height = 31
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object btnIncProdBlack: TBitBtn
          Left = 3
          Top = 3
          Width = 702
          Height = 25
          Caption = '&Incluir Produtos'
          TabOrder = 0
          OnClick = btnIncProdBlackClick
          Glyph.Data = {
            5A030000424D5A030000000000005A0100002800000020000000100000000100
            08000000000000020000232E0000232E00004900000000000000117611001379
            1300177D17001C821C00228822002C922C002F952F0031973100359B3500379D
            37003EA43E003FA53F004D984D0042A8420043A9430045AB450048AE48004BB1
            4B004DB34D005EAA5E0050B6500051B7510054BA540055BB550057BD57006DB9
            6D005BC15B0060C7600065CC65006BD26B007FCB7F0070D7700076DD76007BE2
            7B007CE37C00B3B5B500B5B6B700B7B8B900B9BABA00BBBCBD0080E7800084EB
            840087EE8700BFC0C100C1C2C300C2C3C300C4C5C500C5C5C600C7C8C800C8C9
            C900C9CACA00CACACB00CBCBCC00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0
            D100D1D1D200D1D2D200D2D3D300D4D5D500D5D5D600D6D6D700D8D9D900DBDB
            DC00DCDDDD00DFDFE000E2E2E200E3E4E400E5E5E600E7E7E700FFFFFF004848
            4848484848484848484848484848484848484848484848484848484848484848
            484848480C00000C484848484848484848484848302323304848484848484848
            4848484801161601484848484848484848484848243A3A244848484848484848
            4848484802181802484848484848484848484848253C3C254848484848484848
            48484848031A1A03484848484848484848484848263D3D264848484848484848
            48484848041B1B04484848484848484848484848273F3F274848484848484813
            0507090A0E1C1C0E0A090705134848372B2D2F3033404033302F2D2B37484806
            1D1D1D1D1D1D1D1D1D1D1D1D0648482C4141414141414141414141412C484808
            2A2A2A2A221F1F222A2A2A2A0848482E4747474744424244474747472E484819
            0B0F11141720201714110F0B1948483E313436383B43433B383634313E484848
            484848480D21210D484848484848484848484848324444324848484848484848
            4848484810282810484848484848484848484848354545354848484848484848
            4848484812292912484848484848484848484848374646374848484848484848
            48484848152A2A15484848484848484848484848394747394848484848484848
            484848481E16161E484848484848484848484848423A3A424848484848484848
            484848484848484848484848484848484848484848484848484848484848}
          NumGlyphs = 2
        end
        object btnImportListaBlack: TBitBtn
          Left = 707
          Top = 3
          Width = 75
          Height = 25
          BiDiMode = bdRightToLeft
          Caption = 'Import. Lista'
          Enabled = False
          ParentBiDiMode = False
          TabOrder = 1
        end
      end
      object Panel17: TPanel
        Left = 0
        Top = 31
        Width = 741
        Height = 408
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object Panel8: TPanel
          Left = 2
          Top = 375
          Width = 737
          Height = 31
          Align = alBottom
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 0
          object btnApagaProdBlack: TBitBtn
            Left = 3
            Top = 3
            Width = 75
            Height = 25
            Caption = '&Apagar'
            TabOrder = 0
            OnClick = btnApagaProdBlackClick
            Glyph.Data = {
              82030000424D8203000000000000820100002800000020000000100000000100
              08000000000000020000232E0000232E000053000000000000000021CC000324
              CE000728D2000C2DD5001233DA001739DE001E40E3002446E8002B4DED003153
              F1003254FF003759F6003759FF003C5EF9003D5FFF004062FD004365FF004567
              FF004E70FF00587AFF006183FF006B8DFF007F90E5007092FF007496FF007C9E
              FF00B6B7B800B7B8B900B9BABA00BABBBC00BDBEBE008191E60082A4FF0087A9
              FF009FB0FE00A1B2FF00BFC0C100BFC7F200C2C3C300C5C5C600C7C8C800CACA
              CB00CBCCCD00CDCECE00CECFCF00CFCFD000D0D0D100D1D1D200D1D2D200D4D5
              D500D6D7D700D9DADA00DBDBDC00DCDDDD00DEDFDF00C0C8F300C1C9F400C2CA
              F400C4CCF600C5CDF700C7CFF800C8D1F900CAD2FA00CBD4FB00CDD5FD00CED7
              FD00CFD8FE00DFDFE000D0D8FF00E2E2E200E3E4E400E5E5E600E8E8E800E8E9
              E900EEEEEE00EEEFEF00EFEFF000F0F0F000F1F1F100F2F2F200F3F3F300F4F4
              F400FFFFFF005252525252525252525252525252525252525252525252525252
              525252525252525216002552525252525225001652525252341A4A5252525252
              524A1A345252521F010A01375252525237010A011F5252341B2A1B4A52525252
              4A1B2A1B34525202210C0C0238525238020C0C210252521C472B2B1C4B52524B
              1C2B2B471C52523903210E0E033939030E0E21033952524B1D472D2D1D4B4B1D
              2D2D471D4B5252523A042111110404111121043A525252524C1E4730301E1E30
              30471E4C52525252523B05211212121221053B5252525252524C244731313131
              47244C525252525252523C0617131317063C52525252525252524D2636323236
              264D52525252525252523D0714141414073D52525252525252524E2733333333
              274E525252525252523E08151521211515083E5252525252524F283535474735
              35284F52525252523F091818210909211818093F525252524F29434347292947
              4343294F525252400B1919210B40400B2119190B405252502A4545472A50502A
              4745452A5052520D2120210D415252410D2120210D52522C4746472C51525251
              2C4746472C5252220F210F4252525252420F210F225252482E472E5152525252
              512E472E4852525223104452525252525244102352525252492F515252525252
              52512F4952525252525252525252525252525252525252525252525252525252
              525252525252}
            NumGlyphs = 2
          end
        end
        object grdBlackList: TJvDBGrid
          Left = 2
          Top = 2
          Width = 737
          Height = 373
          Align = alClient
          BorderStyle = bsNone
          DataSource = dsBlackList
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'prod_id'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'codbarras'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'descricao'
              Width = 300
              Visible = True
            end>
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 741
      end
      inherited GridHistorico: TJvDBGrid
        Width = 741
        Height = 392
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 749
  end
  inherited panStatus2: TPanel
    Width = 749
  end
  inherited DSCadastro: TDataSource
    Left = 20
    Top = 328
  end
  inherited PopupBut: TPopupMenu
    Left = 332
    Top = 257
  end
  inherited PopupGrid1: TPopupMenu
    Left = 331
    Top = 209
  end
  inherited QHistorico: TADOQuery
    Left = 60
    Top = 296
  end
  inherited DSHistorico: TDataSource
    Left = 60
    Top = 328
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from programas where apagado <> '#39'S'#39' and prog_id = 0')
    Left = 20
    Top = 296
    object QCadastroPROG_ID: TIntegerField
      DisplayLabel = 'Prog. ID'
      FieldName = 'PROG_ID'
    end
    object QCadastroNOME: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'NOME'
      Size = 60
    end
    object QCadastroDT_FIM: TDateTimeField
      FieldName = 'DT_FIM'
    end
    object QCadastroVERIFICA_VALOR: TStringField
      FieldName = 'VERIFICA_VALOR'
      FixedChar = True
      Size = 1
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QCadastroCUPOM: TStringField
      FieldName = 'CUPOM'
      Size = 100
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroOPERADOR: TStringField
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDT_INICIO: TDateTimeField
      FieldName = 'DT_INICIO'
    end
  end
  object dsProdutos: TDataSource
    DataSet = qProdutos
    OnStateChange = dsProdutosStateChange
    OnDataChange = dsProdutosDataChange
    Left = 101
    Top = 329
  end
  object dsProdQtde: TDataSource
    DataSet = qProdQtde
    Left = 140
    Top = 329
  end
  object ppmProd: TPopupMenu
    Left = 48
    Top = 219
    object AlteraolineardeDesconto1: TMenuItem
      Caption = 'Altera'#231#227'o linear de Desconto'
      OnClick = AlteraolineardeDesconto1Click
    end
  end
  object dsProgEmpr: TDataSource
    DataSet = qProgEmpr
    OnStateChange = dsProgEmprStateChange
    Left = 212
    Top = 329
  end
  object dsProgCred: TDataSource
    DataSet = qProgCred
    OnStateChange = dsProgCredStateChange
    Left = 252
    Top = 329
  end
  object dsBlackList: TDataSource
    DataSet = qBlackList
    Left = 180
    Top = 328
  end
  object qProdutos: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = qProdutosBeforePost
    AfterPost = qProdutosAfterPost
    OnCalcFields = qProdutosCalcFields
    Parameters = <
      item
        Name = 'programa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '    pp.prod_id,'
      '    p.descricao,'
      '    pp.prog_id,'
      
        '    ( select TOP 1 barras from barras where prod_id = p.prod_id ' +
        ' ) codbarras,'
      '    coalesce(pp.prc_unit, 0.00) prc_unit,'
      '    coalesce(pp.prc_unit_18, 0.00) prc_unit_18,'
      '    coalesce(pp.prc_unit_19, 0.00) prc_unit_19,'
      '    coalesce(pp.perc_desc, 0.00) perc_desc,'
      '    coalesce(pp.qtd_max, 0) qtd_max,'
      '    coalesce(l.nomelab,'#39'NAO RELACIONADO'#39') laboratorios,'
      '    coalesce(pp.obrig_receita, '#39'N'#39') obrig_receita'
      'from prog_prod pp'
      '    join produtos p on pp.prod_id = p.prod_id'
      '    left join laboratorios l on p.lab_id = l.lab_id'
      'where pp.ativo = '#39'S'#39
      '    and pp.prog_id = :programa'
      'order by p.descricao')
    Left = 100
    Top = 297
    object qProdutosprod_id: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'prod_id'
      ReadOnly = True
    end
    object qProdutosdescricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'descricao'
      ReadOnly = True
      Size = 90
    end
    object qProdutosprog_id: TIntegerField
      DisplayLabel = 'Prog. ID'
      FieldName = 'prog_id'
      ReadOnly = True
    end
    object qProdutoscodbarras: TStringField
      DisplayLabel = 'Cod. Barras'
      FieldName = 'codbarras'
      ReadOnly = True
      Size = 13
    end
    object qProdutosprc_unit: TBCDField
      DisplayLabel = 'Pre'#231'o Uni.'
      FieldName = 'prc_unit'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 2
    end
    object qProdutosprc_unit_18: TBCDField
      DisplayLabel = 'Pre'#231'o Uni. 18'
      FieldName = 'prc_unit_18'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 2
    end
    object qProdutosprc_unit_19: TBCDField
      DisplayLabel = 'Pre'#231'o Uni. 19'
      FieldName = 'prc_unit_19'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 2
    end
    object qProdutosperc_desc: TBCDField
      DisplayLabel = 'Perc. Desc.'
      FieldName = 'perc_desc'
      DisplayFormat = '##0.00'
      Precision = 5
      Size = 2
    end
    object qProdutosqtd_max: TIntegerField
      DisplayLabel = 'Qtd. Max.'
      FieldName = 'qtd_max'
    end
    object qProdutoslaboratorios: TStringField
      DisplayLabel = 'Laborat'#243'rios'
      FieldName = 'laboratorios'
      ReadOnly = True
      Size = 50
    end
    object qProdutosobrig_receita: TStringField
      DisplayLabel = 'Obriga Receita'
      FieldName = 'obrig_receita'
      Size = 1
    end
  end
  object qProdQtde: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'prog'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'prod'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select d.prog_id, d.prod_id, d.qtd, d.perc_desc, '
      'pp.prc_unit-((pp.prc_unit*d.perc_desc)/100) as desconto'
      'from desc_prog_prod d'
      
        'join prog_prod pp on d.prod_id = pp.prod_id and d.prog_id = pp.p' +
        'rog_id'
      'where d.prog_id = :prog'
      'and d.prod_id = :prod')
    Left = 140
    Top = 297
    object qProdQtdeprog_id: TIntegerField
      FieldName = 'prog_id'
    end
    object qProdQtdeprod_id: TIntegerField
      FieldName = 'prod_id'
    end
    object qProdQtdeqtd: TIntegerField
      FieldName = 'qtd'
    end
    object qProdQtdeperc_desc: TBCDField
      FieldName = 'perc_desc'
      Precision = 3
      Size = 2
    end
    object qProdQtdedesconto: TBCDField
      FieldName = 'desconto'
      ReadOnly = True
      Precision = 24
      Size = 8
    end
  end
  object qBlackList: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <
      item
        Name = 'programa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '    pb.prod_id,'
      '    p.descricao,'
      '    pb.prog_id,'
      '    b.barras as codbarras'
      'from prog_blacklist pb'
      'join produtos p on pb.prod_id = p.prod_id'
      'join barras b on p.prod_id = b.prod_id'
      'where pb.prog_id = :programa')
    Left = 180
    Top = 297
    object qBlackListprod_id: TIntegerField
      DisplayLabel = 'Prod. ID'
      FieldName = 'prod_id'
    end
    object qBlackListdescricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'descricao'
      Size = 90
    end
    object qBlackListprog_id: TIntegerField
      DisplayLabel = 'Prog. ID'
      FieldName = 'prog_id'
    end
    object qBlackListcodbarras: TStringField
      DisplayLabel = 'Cod. Barras'
      FieldName = 'codbarras'
      Size = 13
    end
  end
  object qProgEmpr: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = qProgEmprBeforePost
    AfterPost = qProgEmprAfterPost
    Parameters = <
      item
        Name = 'prog'
        DataType = ftInteger
        Size = 20
        Value = 0
      end>
    Prepared = True
    SQL.Strings = (
      'select'
      '    e.empres_id,'
      '    e.nome, '
      '    e.fantasia,'
      '    p.prog_id,'
      
        '    case when p.prog_id is null then '#39'N'#39' else '#39'S'#39' end as liberad' +
        'o'
      'from empresas e'
      
        'left join prog_empr p on e.empres_id = p.empres_id and p.prog_id' +
        ' =:prog'
      'where e.apagado = '#39'N'#39)
    Left = 212
    Top = 297
    object qProgEmprempres_id: TAutoIncField
      DisplayLabel = 'Empresa ID'
      FieldName = 'empres_id'
      ReadOnly = True
    end
    object qProgEmprnome: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'nome'
      Size = 60
    end
    object qProgEmprfantasia: TStringField
      FieldName = 'Fantasia'
      Size = 60
    end
    object qProgEmprprog_id: TIntegerField
      DisplayLabel = 'Prog. ID'
      FieldName = 'prog_id'
    end
    object qProgEmprliberado: TStringField
      FieldName = 'Liberado'
      Required = True
      Size = 1
    end
  end
  object qProgCred: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = qProgCredBeforePost
    AfterPost = qProgCredAfterPost
    Parameters = <
      item
        Name = 'PROG_ID'
        DataType = ftInteger
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '    p.prog_id,'
      '    c.cred_id,'
      '    c.nome,'
      '    c.fantasia,'
      
        '    case when p.prog_id is null then '#39'N'#39' else '#39'S'#39' end as liberad' +
        'o,'
      
        '    case when p.obriga_desconto <> '#39'S'#39' then '#39'N'#39' else '#39'S'#39' end as ' +
        'obriga_desconto'
      'from credenciados c'
      
        'left join prog_cred p on c.cred_id = p.cred_id and p.prog_id = :' +
        'PROG_ID'
      'where c.apagado = '#39'N'#39)
    Left = 252
    Top = 297
    object qProgCredprog_id: TIntegerField
      DisplayLabel = 'Prog. ID'
      FieldName = 'prog_id'
    end
    object qProgCredcred_id: TIntegerField
      DisplayLabel = 'Cred. ID'
      FieldName = 'cred_id'
    end
    object qProgCrednome: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'nome'
      Size = 60
    end
    object qProgCredfantasia: TStringField
      DisplayLabel = 'Fantasia'
      FieldName = 'fantasia'
      Size = 58
    end
    object qProgCredliberado: TStringField
      DisplayLabel = 'Liberado'
      FieldName = 'liberado'
      Required = True
      Size = 1
    end
    object qProgCredobriga_desconto: TStringField
      DisplayLabel = 'Obriga Desconto'
      FieldName = 'obriga_desconto'
      Required = True
      Size = 1
    end
  end
end
