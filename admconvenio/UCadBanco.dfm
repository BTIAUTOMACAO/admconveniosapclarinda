inherited FCadBanco: TFCadBanco
  Left = 243
  Top = 48
  Caption = 'Cadastro de Bancos'
  ClientHeight = 633
  ClientWidth = 848
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label16: TLabel [0]
    Left = 608
    Top = 176
    Width = 117
    Height = 13
    Caption = 'MENSAGEM1_BOLETO'
  end
  inherited PageControl1: TPageControl
    Width = 848
    Height = 592
    ActivePage = TabSheet1
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        Top = 473
        Width = 840
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
        inherited ButAtualiza: TBitBtn
          Left = 737
        end
      end
      inherited DBGrid1: TJvDBGrid
        Width = 840
        Height = 473
        Columns = <
          item
            Expanded = False
            FieldName = 'CODIGO'
            Width = 101
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BANCO'
            Title.Caption = 'Nome'
            Width = 317
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Title.Caption = 'Data '#250'ltima altera'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end>
      end
      inherited Barra: TStatusBar
        Top = 545
        Width = 840
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel2: TPanel
        Top = 532
        Width = 840
        TabOrder = 2
      end
      inherited Panel3: TPanel
        Width = 840
        Height = 532
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 836
          Height = 463
          Align = alClient
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 10
            Top = 20
            Width = 33
            Height = 13
            Caption = 'C'#243'digo'
            FocusControl = dbEdtCodigo
          end
          object Label4: TLabel
            Left = 74
            Top = 20
            Width = 63
            Height = 13
            Caption = 'Raz'#227'o Social'
            FocusControl = dbEdtRazaoSocial
          end
          object dbEdtCodigo: TDBEdit
            Left = 10
            Top = 35
            Width = 57
            Height = 21
            Color = clWhite
            DataField = 'CODIGO'
            DataSource = DSCadastro
            TabOrder = 0
          end
          object dbEdtRazaoSocial: TDBEdit
            Left = 74
            Top = 35
            Width = 333
            Height = 21
            CharCase = ecUpperCase
            DataField = 'BANCO'
            DataSource = DSCadastro
            TabOrder = 1
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 465
          Width = 836
          Height = 65
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 1
          object Label14: TLabel
            Left = 16
            Top = 16
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
          end
          object Label13: TLabel
            Left = 144
            Top = 16
            Width = 44
            Height = 13
            Caption = 'Operador'
          end
          object DBEdit11: TDBEdit
            Left = 144
            Top = 30
            Width = 161
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
          object DBEdit12: TDBEdit
            Left = 16
            Top = 30
            Width = 113
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
        end
      end
      object cbDescTaxa: TDBCheckBox
        Left = 14
        Top = 72
        Width = 137
        Height = 17
        Caption = 'Desc. Taxa Banc'#225'ria'
        DataField = 'DESC_TAXA'
        DataSource = DSCadastro
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object TabSheet1: TTabSheet [2]
      Caption = 'Contas existentes neste banco'
      ImageIndex = 3
      OnShow = TabSheet1Show
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 840
        Height = 564
        ActivePage = TabSheet3
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = 'Em Grade'
          object JvDBGrid1: TJvDBGrid
            Left = 0
            Top = 0
            Width = 832
            Height = 533
            Align = alClient
            DataSource = DSContas
            DefaultDrawing = False
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = JvDBGrid1DblClick
            OnKeyDown = JvDBGrid1KeyDown
            AutoAppend = False
            TitleButtons = True
            OnTitleBtnClick = JvDBGrid1TitleBtnClick
            SelectColumnsDialogStrings.Caption = 'Select columns'
            SelectColumnsDialogStrings.OK = '&OK'
            SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
            EditControls = <>
            RowsHeight = 16
            TitleRowHeight = 17
            Columns = <
              item
                Expanded = False
                FieldName = 'COD_BANCO'
                Title.Caption = 'Cod. do Banco'
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AGENCIA'
                Width = 85
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CONTACORRENTE'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CONTRATO'
                Title.Caption = 'Contrato/Conv'#234'nio'
                Width = 114
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ_REMESSA'
                Title.Caption = #218'ltima Remessa'
                Width = 85
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Em Ficha'
          ImageIndex = 1
          OnShow = TabSheet3Show
          object Panel4: TPanel
            Left = 0
            Top = 482
            Width = 832
            Height = 51
            Align = alBottom
            BorderStyle = bsSingle
            TabOrder = 1
            object DBNavigator3: TDBNavigator
              Left = 266
              Top = 9
              Width = 160
              Height = 30
              DataSource = DSContas
              VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
              Flat = True
              ConfirmDelete = False
              TabOrder = 3
            end
            object buteditconta: TBitBtn
              Left = 8
              Top = 9
              Width = 85
              Height = 30
              Caption = 'Editar'
              TabOrder = 0
              OnClick = buteditcontaClick
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000BEC1C0957157
                8C7067999798B4C0C5CBD5DACFD7DBCDD5D9CCD4D8CAD2D5C9D1D5C8D0D4C8D0
                D4C8D0D4C8D0D4C8D1D5C5CCD0BDC2C4B5BABCBABFC1C9CFD2CBD4D8C9D2D6C8
                D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4B7B3A8A73D00
                B45427A1572D87422679523F89807EAAACAFBDC8CDC8D5DBCFD8DBCAD1D6C9D1
                D5C8D0D4C8D0D4C9D1D6C1C7CAA9A7A7B4B3B3B4B3B3A8A9A9A7A9AAB8B9BAC4
                C9CCC6CED2CAD2D6CAD2D6C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4BBB5A6A03700
                E8C0A2FFF7D9F6C49BDC9A67BC6938914419804F378B7065A7A8A9CCD3D7CAD2
                D6C8D0D4C8D0D4C9D2D6C1C7CAA7A7A6DEDEDEF7F7F7E4E4E3D1D0D0BCBCBCA9
                A9A9ABAAAAB6B9BAC0C6C9C8D1D5C8D0D4C8D0D4C8D0D4C8D0D4BEB3A2A53900
                E5C1A4FFF3D9FFE8C9FFF1CBFFE9C1FFD8AEF0B271CB6C2A825037C4CFD4CCD4
                D8C8D0D4C8D0D4C9D2D6C1C7CAA7A7A7DEDEDEF6F6F6F1F1F1F4F4F4F1F1F1EB
                EBEBD8D8D8BDBDBDACABABCED4D7C7D0D4C8D0D4C8D0D4C8D0D4C1B3A0AD3F00
                EACDB7FFF2DDFFE8C5BBCEC2F5DBBCFFDDB6FFCC67F4B166895134C5D0D8CBD3
                D7C8D0D4C8D0D4C9D1D6C3C9CBAAA9A9E4E4E4F7F7F7F1F1F1DEDEDEEAEAEAED
                EDEDD9D9D9D6D6D6ADADACD2D6D8C7D0D4C8D0D4C8D0D4C8D0D4C0B09CB34802
                EDD4C3FFFFE4B9D5CC068ECCD1DBCDFACD86F8C369E5A26E7D4C33C7D5DECDD5
                D9C9D1D5C8D0D4C9D1D5C4CACDACABABE8E8E8FDFDFDE1E1E1B0B0B0E5E5E5E1
                E1E1D6D6D6D6D6D6A9A9A8D3D8DBC6CFD4C8D0D4C8D0D4C8D0D4BEAB96BA4F05
                F7E2D3B1DBE335A5CB55B4CF8ACED6B39C7E4E7377E79C52B16E35BCB0A4C4CF
                D7CBD4DACBD3D7CAD2D7C4CACDADADADEFEFEFE6E6E6BDBDBDC7C7C7D4D4D4CD
                CDCDADADADCDCDCDB8B7B7CBCED0CDD4D7C7D0D4C8D0D4C8D0D4BCA58FBF550B
                FEEFE280CBE3C7DFD9DEE6D63DAACCEEEBD3C2A884F5C895FFE6B6ECB983D19D
                69C0AB96C4C9C7CFD6D5C6CBCEAFAFAFF6F6F6D9D9D9E5E5E5EDEDEDBFBFBFEF
                EFEFD0D0D0DFDFDFEEEEEEDCDCDCCDCDCDCBCDCFC9D0D4C9D1D5BAA087C25D14
                F8F3F3FFFFF4FFF8E7FEF6DF3EACCEC1DCD1FFFDDDDC9963AD7240F2D9BBF9D2
                9FCFB0876F7BAC808BBCC7CDCEB2B2B2F7F7F7FEFEFEFAFAFAF7F7F7C2C2C2E0
                E0E0FBFBFBD1D1D1B7B7B7EBEBEAE8E7E7D5D5D4C6C6C6C3C6C8B8997DC6641B
                FCFAFBFFFCF7FFF6ECFFFBE78CCAD865B7CEFFFFE8DFA0717B5D4BCED2D1D6C6
                B5C5C0BB2A59D84E6DCBC9CBCCB5B4B4FBFBFBFEFEFEFAFAFAFBFBFBDADADAC8
                C8C8FCFCFCD6D6D6A9AAABCFD6D9D0D4D6DFE0DFBDBEBEC3C3C3B79573CB6C2A
                FCFCFCFFFFFEFFFAF7FFF9EDE5F1E425A1CDDDF1EBE6A36C8C6547CDD9E2C8D3
                DAC7CFD2C1C9D5C6CED7C5CBCEB9B9B9FCFCFCFFFFFFFDFDFDFCFCFCF4F4F4BA
                BABAEEEEEED7D7D7AAACADC9D2D7C8D0D4C8D0D4C7CFD3C9D1D5B6916DCA7035
                F9F9F8FEFFFFFFFFFFFFFFFFFFFFFFAFDCE8D9F6F8E09D67896A4FCFD9DFC9D1
                D5C8D0D4C8D1D4C9D1D5C5CACDBBBBBAFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFE5
                E5E5F3F3F3D5D4D4ABADAECAD3D7C8D0D4C8D0D4C8D0D4C8D0D4BA966EC45103
                B85D25C47953D19C80DFBEABECD8CCFBECE2FFFFFFDE9F6F8C6E51CFDADFC9D1
                D5C8D0D4C8D0D4C8D1D5C7CCCFB0AFAFB4B4B4C3C3C3D2D2D2E1E1E1ECECECF6
                F6F6FFFFFFD6D6D6ABADAECAD3D7C8D0D4C8D0D4C8D0D4C8D0D4BCA57EE9872F
                D16102CB5400C44900BF4400BD4906BD551BC67342C4641C917F6BD0D8DFC8D0
                D4C8D0D4C8D0D4C7D0D4CDD3D5C6C5C4B4B4B4AEAEAEAAAAAAAAAAAAAEAEAEB3
                B3B3BFBFBFB8B8B7B4B6B7CAD2D6C8D0D4C8D0D4C8D0D4C8D0D4BABFBFB8B7A6
                C8BA9BD2B78CD1A870D09550CF822FD37719D5680ADB6404968779D0DADFC8D0
                D4C8D0D4C8D0D4C8D0D5C8CFD2CCCFCFCCCFD0D4D5D5D2D1D1C9C8C8C0BFBFBC
                BBBBB7B6B6B7B6B5B9BBBBC9D2D6C8D0D4C8D0D4C8D0D4C8D0D4CCD4D9CCD5DC
                CAD4DBC2CED6BBC7CFB9C4CAC0C7C5C7C7BDC4BDA7BAA683B2B2AECED6DBC9D1
                D5C9D1D5C9D1D5C9D1D5C9D1D5C9D1D6C8D1D5CDD4D7CED3D6CBD1D3CAD0D2CC
                D1D4CCD2D4CBD0D3C5CBCEC9D2D6C9D1D5C9D1D5C9D1D5C9D1D5}
              NumGlyphs = 2
            end
            object butincconta: TBitBtn
              Left = 93
              Top = 9
              Width = 85
              Height = 30
              Caption = 'Incluir'
              TabOrder = 1
              OnClick = butinccontaClick
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000120B0000120B00000000000000000000CAD1D5CAD1D5
                CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1
                D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D500000000000000000000000000000000
                0000000000000000000000000000000000000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FF
                FF000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC6FFFFC6
                FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFF000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFCCFFFFC6FFFFC6FFFFC9FFFFCCFFFFC6FFFFC6FF
                FF000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC9FFFFC8
                FFFFC6FFFFC0FFFFCBFFFFC6FFFFC6FFFF000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFC6FFFFCDFFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FF
                FF000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC6FFFFC6
                FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFF000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FF
                FF000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC6FFFFC6
                FFFFC6FFFFC7FFFFC6FFFFC6FFFFC6FFFF000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFBDFAF9C6FF
                FF000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC6FFFFC2
                FFFFC7FFFFC6FFFFBEFEFE96A8A8676D69000000CAD1D5CAD1D5CAD1D5CAD1D5
                000000C6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFC6FFFFB1E5E5C6CCCC0000
                00000000CAD1D5CAD1D5CAD1D5CAD1D5000000C6FFFFC6FFFFC6FFFFC6FFFFC6
                FFFFC6FFFFC6FFFF898C8C000000000000CAD1D5C9D0D4CAD1D5CAD1D5CAD1D5
                000000000000000000000000000000000000000000000000000000000000CAD1
                D5C9D0D4CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CA
                D1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5}
            end
            object butdelconta: TBitBtn
              Left = 178
              Top = 9
              Width = 85
              Height = 30
              Caption = 'Apagar'
              TabOrder = 2
              OnClick = butdelcontaClick
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000CAD1D5CAD1D5
                CAD1D5C9D0D5BCC4CBB0B9BFAAB2B8ADB6BCB7BFC5C1C8CECAD2D6CFD6DACBD2
                D6CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CBD1D5C6CBCEBDBEBFB4B5B6B0
                B2B2B8BABAC1C2C3C6CBCFCED5D9CDD5D9CAD1D5CAD1D5CAD1D5CAD1D5CBD2D5
                C8CFD5BBC1C2B4B5AAB8B5A1B6B19AA49F8F95958B8B8E8C8F9699AFB6BCCBD3
                D7CBD2D6C9D0D4CAD1D5CAD1D5CAD1D5CAD2D7C7CACBB8B8B7AFAEAEB1B0B0A5
                A5A49796968F8E8E8D8C8C9FA1A2C1C5C8CED5D9CAD1D6CAD1D5CAD1D5C9D0D5
                C8D0D1E1E0C9FBEFC3FFE6BCF4D3B6F2CABDF3D1BCE1D8BDBAB3A181847E8F96
                9AC4CCD0CBD3D7CAD1D5CAD1D5CAD1D6C9CFD2D1D2D2E1E1E0E6E6E6D7D7D7CA
                CACAC8C8C8D2D2D2C8C8C89898987F7F7EADB0B1CDD4D9CAD1D6CAD1D5C8D0D5
                D3CCBDFDEAB9FBE7C6E5C3BBDFB6B9C0C6A9BED7ABBEE2A9E8F7CDECE8CF8F92
                88979EA1CAD2D7CBD2D6CAD1D5CAD2D6C8CCCED0CFCEE8E8E8D5D5D5BBBBBBB9
                B9B9C4C4C4D1D1D1DDDDDDF4F4F4C2C2C2838282B7BABCCDD4D8CAD1D6C9D1D7
                DEBE96F7C589EEDBD4E5CFD5F3CFD687BF7A079E0B129E132FAC2FCCF1B8E7DC
                C48A8C87BFC7CDCCD3D7CAD1D5CCD2D7C7CACBB1B1B1CDCDCDD7D7D7CFCFCFCB
                CBCB999999838383888888B0B0B0F6F6F6A7A7A79D9C9CD1D5D7C9D1D7CACECE
                ECC190FAD9BBF5EEF2EEDEDFF5C9D088BF7B008F0057BB569FD89B7ACA6CE4D0
                99988D84B5BFC6CDD4D8CAD1D5CDD2D6C4C4C4BEBEBEE5E5E5ECECECD3D3D3C8
                C8C89494947C7C7CC5C5C5BBBBBBC2C2C2ABABAB969595CED2D4C9D1D7CFC8BD
                FAD4ABFFFCF8FDFDFFE4C2C4D9B4AEC9D9B596D18F7CCB7CDBF1D4C6E8B4DACD
                90A49386ABB6BBCCD4D8CAD2D6C9CFD3C1C1C1E1E1E1FFFFFFEAEAEAB1B1B1C2
                C2C2D0D0D0B7B7B7CBCBCBECECECCFCFCFADADAD939393C7CCCEC8D1D7DDCCB8
                FFE9CCFFFFFFF0E2E0D4A1A5D4B5AC99C989E3F5E09DD99C1FA22377C76AFEE5
                BAAB9A85A2AAB1CCD3D8CAD2D6C8CDCFCDCDCCF6F6F6FDFDFDC3C3C3A4A4A4B7
                B7B7CBCBCBF2F2F2A5A5A5919191D0D0D0C4C4C48E8E8DC0C5C7C8CED3E6D8C0
                FFFBDFFFFFF3F0DFD5E7D0CBEDD3DB99C18B3DB23C3EB03D00890046AF3AFFF6
                D0B6AD9499A0A6CBD2D7C9D0D4CBCCCCE1E1E0FDFDFDF6F6F6D2D2D2D4D4D4CD
                CDCDA3A3A3A2A2A28B8B8B737373C7C7C7DFDFDF929191B8BCBECCCFCFEEE5C9
                FFFFDFFFFEE3FFFDE6FFFFF0F2F9FBF3E9F196CE9049B64A5BBB5C85CB79FFFF
                E4C3C1AA969C9ECAD1D6C9D0D4CFD0D0EDEDECFAFAFAF8F8F8FAFAFAFEFEFEF4
                F4F4DDDDDDAEAEAEA2A2A2A8A8A8D7D7D7F1F1F19B9B9AB1B5B8D2D1CDF8EDCC
                FFFFE1FFFFE0FEFFE3E8FFF4E7FFFAF1FBF9FEF1F5FCFAF7FFFFFEF6FEEFFFFF
                F4CECEB9959B9AC9D1D6C9D0D4D4D5D5F4F4F3FAFAFAFAFAFAFBFBFBFCFCFCFD
                FDFDF6F6F6F2F2F2FDFDFDFEFEFEFDFDFDF6F6F6A6A5A5ADB1B4CFD0D0EEE0C6
                FFF6D3FCF5D4EDF3D7DCFAF0EDFDF5F8FCF4EED6D3F9EBECF7F3E9FDE1BDFFF8
                DADADDC5969B9AC8D0D5CAD1D5D1D1D2E6E6E5F1F1F1ECECECF0F0F0F8F8F8FC
                FCFCEDEDEDD5D5D5F7F7F7E3E3E3E3E3E3F8F8F8B2B1B1A9ADB0CAD2D7CED0CF
                D9D2C5D4D0C4C3D7DADCF7F9ECFFFFECFFFFE7ECECE6F8FBE1DBC2F8BA77FFD2
                99E4D3AD999E9CC8D0D5CAD1D6CCD1D3CECECECECFCFCBCBCBE3E3E3FBFBFBFF
                FFFFF7F7F7EDEDEDF3F3F3BDBDBDB7B7B7D5D5D5B1B1B0AAAEB1CAD1D5C7D0D5
                C4CED5C5CED3BFD1DBB8D7E5B6E2F0B5ECF6B7F8FEADF6FCD2C8A9FEBF82FFC7
                89E6BD8FAFB1B0C9D1D7CAD1D5CAD1D5C9D0D4C8CFD3CACFD2D5D4D3DCDBDBE7
                E7E6EFEEEEFBFBFAE3E2E2B7B7B6BCBCBBBEBEBDAEADADBBBFC1CAD1D5CAD1D4
                C9D1D5C9D1D5CBD0D3CACED1C6CFD4C1CFD7B8D7E3AED9E2BFC8C6DDCBB9E1CE
                B8D1C9BEC5CDD1CAD2D5CAD1D5CAD1D5CAD1D5CAD1D5CBD1D5CBCED0CACED0CB
                CFD1CED1D3D8DBDDCDD1D3C1C5C7C6CACCC5C8CAC4C7C9CAD0D3CDD4D8CDD4D8
                CDD4D8CDD4D8CDD4D8CDD4D8CED4D8CED4D7D0D3D6D1D3D6CED5DACBD5DCCAD5
                DCCCD5DBCED5D9CDD4D8CDD4D8CDD4D8CDD4D8CDD4D8CDD4D8CDD4D9CDD4D9CD
                D4D8CCD4D8CBD2D7CCD4D8CED6DACED5D9CED5DACED6DACDD4D8}
              NumGlyphs = 2
              Spacing = 2
            end
            object butsalvaconta: TBitBtn
              Left = 430
              Top = 9
              Width = 85
              Height = 30
              Caption = 'Gravar'
              TabOrder = 4
              OnClick = butsalvacontaClick
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000CED5D8CCD3D6
                CCD3D6D0D8DDCDD6DACBD3D6CDD4D7CCD4D7CBD2D6CBD2D6CBD2D6CFD6D9D1D7
                DBCCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CCD4D7D1D6DAD4D8DBD4D8DBD4
                D8DBD3D7DAD3D7DAD3D7DAD6D9DCD1D6DACBD2D6CCD3D6CDD4D7CCD3D6CDD6DB
                CCD5D9B7B8B8A1A3A391949586898A8487888588897E818274777764696C949B
                A1D2D9DDCFD6DACAD1D5CAD1D5CBD2D6CDD4D8C8CBCDC5C6C7C3C3C2BCBBBBBB
                BABABBBABAB7B7B7B2B1B1ABABAABEC0C0CFD5DACBD3D7CBD2D5CED6DAB8B5B2
                9A8D82663E17A19080D0D5DABABBBDBFC1C3D5D8DBD3D6DAD0D4D98E88834F40
                2E787B80A4ABAFCDD4D8CBD2D7C7CBCCBBBBBC929191BEBDBDE6E6E6D8D8D8DB
                DBDBE8E8E8E8E8E8E7E7E7BCBCBC919191B0B1B1C1C4C6CDD3D8CFD8DD82674E
                5822005B2800AF9A82F1F1F1ECE8E6EBE8E4EAE5E1EAE6E1ECECEBE5CBAC804D
                0F19170E535658C9D1D5D6DBDEABABAA828282838382C2C2C2F6F6F6F1F1F1F1
                F1F1F0F0F0EFEFEFF3F3F3DFDFDF9797977C7C7CA09F9FD2D7DAD0D8DD816850
                874E0C754308A67A50E2B48EDAAD87D9AC85CE9453CD924FCE9554D093508856
                165F4101656356C5CFD7D6DBDEABABAB969696919191AFAFAFD0D0D0CCCCCCCC
                CCCCBDBDBDBCBCBCBEBEBEBDBDBD9C9C9C8E8E8EA5A4A4D1D6D9D0D8DD7F664F
                935D277A4B17A67849E9B388E1AC80E1AB80DCA36ED59550D38D3ED08E42925D
                1CA16A1E726B5EC1CCD5D6DBDEAAA9A99E9E9E969696AEAEAECFCFCFCBCBCBCA
                CACAC5C5C5BEBEBEB9B9B9B9B9B99F9F9FA5A5A5ABA9A9D0D5D8D0D9DD7F654F
                9360317D5020995B31B47B4BAC7543AD7543AE784AAA723DA2611DB1722A9A63
                22BC7D3781786EC1CCD4D6DBDEAAA9A9A0A0A09999999F9F9FB0B0B0ADADADAD
                ADADAEAEAEABABABA2A2A2A9A9A9A2A2A2B0B0B0B1B1B0D0D5D7D0D9DD7F654F
                935F2F7F4E1C7E58418BB6A87FB7A97FB5A87FB5A780B6AA80BFB57D90747D42
                03BC7D37867D73C1CDD5D6DBDEAAA9A99F9F9F9999999D9D9DD2D2D2D3D3D3D2
                D2D2D2D2D2D2D2D2D8D8D8BEBEBE909090B0B0B0B4B3B3D1D5D8D0D9DD7F654F
                935F2F7D4A16848B7AB3F6FD9FE5EFA0E5EEA0E5EEA0E5EEA4EFF593C8C77552
                1FBD7A31857D72C1CDD5D6DBDEAAA9A99F9F9F959595B9B9B9F8F8F8EEEEEEEF
                EFEFEFEFEFEFEFEFF4F4F4E0E0E09A9A9AAEAEAEB3B3B2D1D5D8D0D9DD7F654F
                925E2E8D590B92B698B9EAEEA9CDCCAACFCFA9CFCFA9CFCFACCECFACECF28F92
                5BBC7325847C72C2CDD5D6DBDEA9A9A99F9F9F9B9B9BD0D0D0F1F1F1E0E0E0E1
                E1E1E1E1E1E1E1E1E1E1E1F2F2F2BEBEBEA9A9A9B2B2B1D0D5D8D0D9DD7F644E
                925E2E9F7D429AC7B0B3E9EEA6D3D7A8D4D6A9D4D4A9D4D4ABD4D4A9EBEF99A4
                79BE7628877D72C2CDD5D6DBDEA9A9A99F9F9FAFAFAFDADADAF2F2F2E4E4E4E4
                E4E4E4E4E4E4E4E4E4E4E4F2F2F2C8C8C8ABABABB3B3B2D0D5D8D0D9DD7E634C
                925E2DAFA4A1A2D6DCAFF3EEAEE9DBABEDE8AAEFF0AAEFEFABEFEFA4F7F9A3BC
                B7C3803D8E8477C2CED5D6DBDDA9A8A89E9E9EC9C9C9E5E5E5F7F7F7EEEEEEF1
                F1F1F4F4F4F4F4F4F4F4F4F9F9F9D7D7D7B1B1B1B7B7B7D1D6D9D0D9DC7A5D49
                905828B7AEA7B9CAD4D1BFA4D08C3ABAA487A7B1B8A9AFB2ADADAFB1DBE2ACCA
                C6D2976A938B83C2CDD3D6DBDEA7A6A59C9C9CCECECEDFDFDFD8D8D8B8B8B8C8
                C8C8D2D2D2D0D0D0CECECEE8E8E8DFDFDFC0C0C0BBBBBAD0D5D8CDD5D9A9A6A4
                90745B8A6D38A79C84B3B7A8A5B58E9FBCA899C1B89AC0B69BBFB69DD1C78E95
                708C5E37A9A39DCDD6DBCCD4D7C0C4C6AFAFAEA9A9A8C4C3C3D3D3D3CFCFCFD5
                D5D5D9D9D9D8D8D8D8D8D8E2E2E2C0C0C0A4A3A2BFC2C2CDD4D8CCD2D6CED6DB
                C3CACFB4B4AD9D8E778D7D618C87718E856B9084679084678F84688C7F61A39D
                8DBAC0C1C8CED1CBD2D6CAD1D5CBD2D7D1D5D8C5C9CAB9BABAB3B2B2B8B8B7B7
                B6B6B6B6B6B7B6B6B7B6B6B4B3B3BCBEBFCED1D2D0D5D8C9D0D5CFD5D9CCD3D7
                CDD4D9D0D9E0CED6DBCDD3D7CED2D6CED2D6CED2D6CED2D6CED2D6CDD2D6CFD6
                DCCFD6DBCDD4D8CCD3D7CCD3D7CCD3D7CCD3D7CCD4D9D1D7DBD4D8DBD2D7DAD2
                D7DAD2D7DAD2D7DAD2D7DAD3D8DBCFD6DACCD3D7CCD3D7CED4D8}
              NumGlyphs = 2
            end
            object butcancelconta: TBitBtn
              Left = 515
              Top = 9
              Width = 85
              Height = 30
              Caption = 'Cancelar'
              TabOrder = 5
              OnClick = ButCancelaClick
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000CED5D8CCD3D6
                CCD3D6CBD2D6D8DFE4CDD4D693988F93988F93988F93988F9A9F98DAE1E5D2D9
                DDCBD2D6CCD3D6CCD3D6CCD3D6CCD3D6CCD3D6CBD2D6D2D8DDCDD4D7ADB2B5AE
                B3B6AEB3B6ADB2B5B1B6B9D3DADECFD6DACBD2D6CCD3D6CDD4D7CCD3D6CAD1D5
                CBD2D5D9E1DF868C84434747000044000041000041000045000044565955A2A8
                A6D9E0E4CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5D0D7DCA9ADB08A8C8D7B7B7B7C
                7B7B7C7B7B7C7C7B7E7E7E8E9092B5BABDD1D8DDCAD1D5CBD2D5CCD3D7CBD2D5
                DBE4E180828D0500710000B30600D70700D60400D30000CE0000A70000700F0F
                43A8AEA6D9E0E4C9D0D4CAD1D5CAD1D5D1D8DCAAAFB2898A899C9C9BAAAAAAA9
                A9A9A9A9A9A7A7A79C9C9C888787808080B6BBBED1D8DDCAD1D5CCD3D7D2D9D7
                7D7EB90003B31B0EFA3100FF2E00FF3100FF2800FB1200E20000D40000C70000
                90121242A2A8A6D0D8DCCAD1D5CCD3D7B6BABD9F9F9FB6B6B5B9B9B9B6B6B6B7
                B7B7B5B5B5ADADADA8A8A8A5A5A5949494808080B4BBBDCED5D9CCD3D7D1D6D4
                1B20A40049F61F28FF3B0BFF3103FF2900FF2B00FF3000FF2804F31C0AE60000
                C800006D4D504CD9E1E5CAD1D5CBD3D7A0A1A1BAB9B9BCBCBCB8B8B8B7B7B7B4
                B4B4B5B5B5B6B6B6B4B4B4B1B1B1A5A5A58989878C8F90D2DADEDAE1DC6666AF
                3146E0194EFF2D03FFBDABFFF4F2FF7F63FF1D03FFA692FFF5F0FF8777F40300
                D40000A8080944969B94D9E1E5AFB3B5B5B5B4C2C2C2B4B4B4E6E6E6FBFBFBD4
                D4D4AFAFAFDFDFDFFCFCFCD7D7D7A5A5A59C9C9C7F7E7EAFB4B6DCE2DC5657AA
                3A4EE73560FF2A00FF3917FFD2C8FFFFFFFFB8A7FFFFFFFFAC9AFF2C00FF1100
                E30000CF0000428D9289DBE3E7ABAEB0B8B8B7C9C9C9B2B2B2B9B9B9EEEEEEFF
                FFFFE6E6E6FFFFFFE4E4E4B5B5B5ADADADA7A7A77D7D7BAAAFB1DCE2DC595AAB
                394DE53263FF2F03FF2200FF3411FFD8CFFFFFFFFFB4A4FF1D00FF2B00FF2900
                F80500D40000418F948BDBE3E7ACAFB1B8B8B7C9C9C9B5B5B5B2B2B2B6B6B6F0
                F0F0FFFFFFE6E6E6B0B0B0B4B4B4B5B5B5A9A9A97D7C7CABB0B2DCE2DC595AAB
                3C48E81E88FF1814FF2900FFA592FFFEFDFFDCD2FFFDFCFF7E63FF1F00FF3200
                FF0600D70000418F948BDBE3E7ACAFB1B8B7B7CDCDCDB7B7B7B2B2B2DDDDDDFF
                FFFFF3F3F3FEFEFED3D3D3B0B0B0B8B8B8AAAAAA7D7C7CABB0B2DDE3DC5555AA
                3B46D42A93FF0F45FFAE9AFFFFFFFFA895FF331AFFCFC3FFFFFFFF8164FF2600
                FF0600D90000408C9188DCE4E8ABAEB0B3B2B1D0D0D0BEBEBEE1E1E1FFFFFFE2
                E2E2B6B6B6EDEDEDFFFFFFD4D4D4B4B4B4ABABAB7C7B7BA9AEB0CFD6D8B9BFCD
                2C28A8497AFC0E83FF3579FF5632FF3400FF2300FF3C0FFF5731FF532CFF2F00
                FF0000AB3C4047CCD4D5CDD4D8C5CCD0A2A3A4C8C8C8CACACACDCDCDC4C4C4B6
                B6B6B1B1B1BABABAC4C4C4C3C3C3B8B8B89C9B9B888A8BCBD3D6CCD2D7D5DCD8
                5D5AAB3544CF3896FF0073FF0034FF1B1BFF3200FF2C00FF2500FF2B00FF2300
                FC000256858B83D7DFE3C9D0D4CDD5D9ACAFB1B1B0B0D3D3D3C6C6C6BABABAB7
                B7B7B6B6B6B4B4B4B3B3B3B4B4B4B6B6B6848383A6AAACD1D9DDCCD3D7CCD3D6
                D1D9D55A58B53545CC4474FC2089FF1E87FF144BFF2521FF3400FF1D00FB0000
                7A7E847ED9E1E3C9D0D4CAD1D5CAD2D6CCD3D7AEB0B2B2B1B1C8C7C7CDCDCDCD
                CDCDC1C1C1BBBBBBB7B7B7B4B4B38E8D8DA3A7AAD1D8DCCAD1D5CCD3D7CAD1D5
                CFD6D7D1D9D55C5AAC2D2AA83F4AD33C46E53146E5373EE40A03C82727A47C82
                A8DCE3DCCBD2D6CAD1D5CAD1D5CAD1D5CBD2D6CCD3D8ACAFB1A2A2A3B4B3B3B9
                B9B8B7B7B6B6B6B5A6A5A59F9FA0AFB4B7CFD6DBCBD2D6CBD2D6CCD2D6CAD1D5
                CAD1D5CCD3D6D5DCD8B9BFCD595AAA5A5AAB5B5BAB5A5DAA656AB0D0D8D4D1D8
                D9CBD2D6CAD1D5CAD1D5CAD1D5CAD1D5CAD1D5CBD2D6CDD4D9C5CCD0AAAEAFAB
                AEB0ABAEB0ABAEB0AEB2B4CAD2D6CDD4D8CBD2D6CAD1D5CAD1D5CFD5D9CCD3D7
                CCD3D7CCD3D7CCD2D7CFD6D8DCE2DDDCE2DCDCE2DCDCE2DCDAE1DCCCD3D7CCD3
                D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CCD3D7CDD4D8D0D8DCD0
                D8DCD0D8DCD0D8DCD0D7DCCCD3D7CCD3D7CCD3D7CCD3D7CED4D8}
              NumGlyphs = 2
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 832
            Height = 482
            Align = alClient
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object Label7: TLabel
              Left = 15
              Top = 305
              Width = 73
              Height = 13
              Caption = 'Contrato/Conv.'
              FocusControl = DBEdit5
              Visible = False
            end
            object GroupBox3: TGroupBox
              Left = 9
              Top = 10
              Width = 680
              Height = 100
              Align = alCustom
              Caption = 'Dados da Conta Corrente do Cedente'
              TabOrder = 0
              object Label5: TLabel
                Left = 10
                Top = 15
                Width = 39
                Height = 13
                Caption = 'Ag'#234'ncia'
                FocusControl = DBEdit3
              end
              object Label6: TLabel
                Left = 105
                Top = 15
                Width = 71
                Height = 13
                Caption = 'Conta Corrente'
                FocusControl = DBEdit4
              end
              object Label24: TLabel
                Left = 230
                Top = 15
                Width = 58
                Height = 13
                Caption = 'CNPJ / CPF'
                FocusControl = DBEdit19
              end
              object Label20: TLabel
                Left = 373
                Top = 15
                Width = 115
                Height = 13
                Caption = 'C'#243'd. Convenio/Cedente'
                FocusControl = DBEdit16
              end
              object Label21: TLabel
                Left = 502
                Top = 15
                Width = 136
                Height = 13
                Caption = 'Nome no Convenio/Cedente'
                FocusControl = DBEdit17
              end
              object Label23: TLabel
                Left = 10
                Top = 55
                Width = 46
                Height = 13
                Caption = 'Endere'#231'o'
                FocusControl = DBEdit18
              end
              object Label25: TLabel
                Left = 235
                Top = 55
                Width = 12
                Height = 13
                Caption = 'N'#186
                FocusControl = DBEdit20
              end
              object Label30: TLabel
                Left = 289
                Top = 55
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = DBEdit25
              end
              object Label26: TLabel
                Left = 376
                Top = 55
                Width = 33
                Height = 13
                Caption = 'Cidade'
                FocusControl = DBEdit21
              end
              object Label27: TLabel
                Left = 529
                Top = 55
                Width = 21
                Height = 13
                Caption = 'CEP'
                FocusControl = DBEdit22
              end
              object Label28: TLabel
                Left = 641
                Top = 55
                Width = 14
                Height = 13
                Caption = 'UF'
                FocusControl = DBEdit23
              end
              object DBEdit3: TDBEdit
                Left = 10
                Top = 30
                Width = 88
                Height = 21
                DataField = 'AGENCIA'
                DataSource = DSContas
                TabOrder = 0
              end
              object DBEdit4: TDBEdit
                Left = 105
                Top = 30
                Width = 118
                Height = 21
                DataField = 'CONTACORRENTE'
                DataSource = DSContas
                TabOrder = 1
              end
              object DBEdit19: TDBEdit
                Left = 230
                Top = 30
                Width = 137
                Height = 21
                DataField = 'CNPJ'
                DataSource = DSContas
                TabOrder = 2
              end
              object DBEdit16: TDBEdit
                Left = 373
                Top = 30
                Width = 123
                Height = 21
                DataField = 'COD_CONV'
                DataSource = DSContas
                TabOrder = 3
              end
              object DBEdit17: TDBEdit
                Left = 502
                Top = 30
                Width = 169
                Height = 21
                CharCase = ecUpperCase
                DataField = 'NOME_CONVENIO'
                DataSource = DSContas
                TabOrder = 4
              end
              object DBEdit18: TDBEdit
                Left = 10
                Top = 70
                Width = 219
                Height = 21
                DataField = 'ENDERECO'
                DataSource = DSContas
                TabOrder = 5
              end
              object DBEdit20: TDBEdit
                Left = 235
                Top = 70
                Width = 49
                Height = 21
                DataField = 'NUMERO'
                DataSource = DSContas
                TabOrder = 6
              end
              object DBEdit25: TDBEdit
                Left = 289
                Top = 70
                Width = 81
                Height = 21
                DataField = 'COMPLEMENTO'
                DataSource = DSContas
                TabOrder = 7
              end
              object DBEdit21: TDBEdit
                Left = 376
                Top = 70
                Width = 147
                Height = 21
                DataField = 'CIDADE'
                DataSource = DSContas
                TabOrder = 8
              end
              object DBEdit22: TDBEdit
                Left = 529
                Top = 70
                Width = 106
                Height = 21
                DataField = 'CEP'
                DataSource = DSContas
                TabOrder = 9
              end
              object DBEdit23: TDBEdit
                Left = 641
                Top = 70
                Width = 30
                Height = 21
                DataField = 'UF'
                DataSource = DSContas
                TabOrder = 10
              end
            end
            object GroupBox5: TGroupBox
              Left = 9
              Top = 112
              Width = 271
              Height = 140
              Caption = 'Dados para d'#233'bito em conta cor. (Febraban 4.0 150)'
              TabOrder = 1
              object Label18: TLabel
                Left = 10
                Top = 95
                Width = 75
                Height = 13
                Caption = 'Taxa D'#233'bito CC'
                FocusControl = DBEdit6
              end
              object Label19: TLabel
                Left = 92
                Top = 95
                Width = 63
                Height = 13
                Caption = 'Ult. Remessa'
                FocusControl = DBEdit15
              end
              object Label22: TLabel
                Left = 170
                Top = 95
                Width = 49
                Height = 13
                Caption = 'Usa Digito'
              end
              object Label29: TLabel
                Left = 232
                Top = 95
                Width = 22
                Height = 13
                Caption = 'DAC'
                FocusControl = DBEdit24
              end
              object DBEdit6: TDBEdit
                Left = 12
                Top = 110
                Width = 73
                Height = 21
                Hint = 
                  'Este campo serve para informar o valor '#13#10'da taxa banc'#225'ria para c' +
                  'ada d'#233'bito em conta.'
                DataField = 'TAXA_DEBITO'
                DataSource = DSContas
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
              end
              object DBEdit15: TDBEdit
                Left = 91
                Top = 110
                Width = 73
                Height = 21
                DataField = 'SEQ_REMESSA'
                DataSource = DSContas
                TabOrder = 2
              end
              object JvDBComboBox1: TJvDBComboBox
                Left = 170
                Top = 110
                Width = 55
                Height = 21
                DataField = 'USA_DIGITO'
                DataSource = DSContas
                Items.Strings = (
                  'Sim'
                  'N'#227'o')
                TabOrder = 3
                Values.Strings = (
                  'S'
                  'N')
                ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
                ListSettings.OutfilteredValueFont.Color = clRed
                ListSettings.OutfilteredValueFont.Height = -11
                ListSettings.OutfilteredValueFont.Name = 'MS Sans Serif'
                ListSettings.OutfilteredValueFont.Style = []
              end
              object DBEdit24: TDBEdit
                Left = 232
                Top = 110
                Width = 25
                Height = 21
                DataField = 'DAC'
                DataSource = DSContas
                TabOrder = 4
              end
              object rgTipoLayout: TDBRadioGroup
                Left = 12
                Top = 15
                Width = 245
                Height = 75
                Caption = 'Tipo do Layout'
                DataField = 'TIPO_LAYOUT'
                DataSource = DSContas
                Items.Strings = (
                  'Febraban de D'#233'b. Aut. - Vers'#227'o 04'
                  'CNAB - Vers'#227'o 01.0 (Ita'#250')'
                  'CNAB - Vers'#227'o 04.0 (Ita'#250')')
                TabOrder = 0
                Values.Strings = (
                  'FEBRABAN04'
                  'CNAB010'
                  'CNAB040')
              end
            end
            object GroupBox4: TGroupBox
              Left = 287
              Top = 112
              Width = 402
              Height = 140
              Caption = 'Dados para boleto'
              TabOrder = 2
              object Label8: TLabel
                Left = 348
                Top = 15
                Width = 42
                Height = 13
                Caption = 'Protestar'
              end
              object Label9: TLabel
                Left = 277
                Top = 15
                Width = 62
                Height = 13
                Caption = 'Dias protesto'
                FocusControl = DBEdit7
              end
              object Label10: TLabel
                Left = 10
                Top = 55
                Width = 93
                Height = 13
                Caption = 'Mensagem 1 boleto'
                FocusControl = DBEdit8
              end
              object Label11: TLabel
                Left = 10
                Top = 95
                Width = 93
                Height = 13
                Caption = 'Mensagem 2 boleto'
                FocusControl = DBEdit9
              end
              object Label12: TLabel
                Left = 206
                Top = 15
                Width = 53
                Height = 13
                Caption = 'Perc. Juros'
                FocusControl = DBEdit10
              end
              object Label15: TLabel
                Left = 66
                Top = 15
                Width = 54
                Height = 13
                Caption = 'Perc. Multa'
                FocusControl = DBEdit13
              end
              object Label17: TLabel
                Left = 136
                Top = 15
                Width = 56
                Height = 13
                Caption = 'Perc. Desc.'
                FocusControl = DBEdit14
              end
              object Label31: TLabel
                Left = 10
                Top = 15
                Width = 36
                Height = 13
                Caption = 'Carteira'
                FocusControl = DBEdit26
              end
              object DBEdit7: TDBEdit
                Left = 277
                Top = 30
                Width = 65
                Height = 21
                DataField = 'DIAS_PROTESTO'
                DataSource = DSContas
                TabOrder = 4
              end
              object DBEdit8: TDBEdit
                Left = 10
                Top = 70
                Width = 382
                Height = 21
                DataField = 'MENSAGEM1_BOLETO'
                DataSource = DSContas
                TabOrder = 6
              end
              object DBEdit9: TDBEdit
                Left = 8
                Top = 110
                Width = 383
                Height = 21
                DataField = 'MENSAGEM2_BOLETO'
                DataSource = DSContas
                TabOrder = 7
              end
              object DBEdit10: TDBEdit
                Left = 206
                Top = 30
                Width = 65
                Height = 21
                DataField = 'PERC_JUROS'
                DataSource = DSContas
                TabOrder = 3
              end
              object DBEdit13: TDBEdit
                Left = 66
                Top = 30
                Width = 65
                Height = 21
                DataField = 'PERC_MULTA'
                DataSource = DSContas
                TabOrder = 1
              end
              object DBEdit14: TDBEdit
                Left = 136
                Top = 30
                Width = 65
                Height = 21
                DataField = 'PERC_DESC'
                DataSource = DSContas
                TabOrder = 2
              end
              object DBProtestar: TDBComboBox
                Left = 348
                Top = 30
                Width = 45
                Height = 21
                Style = csDropDownList
                DataField = 'PROTESTAR'
                DataSource = DSContas
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ItemHeight = 13
                Items.Strings = (
                  'N'
                  'S')
                ParentFont = False
                TabOrder = 5
              end
              object DBEdit26: TDBEdit
                Left = 10
                Top = 30
                Width = 50
                Height = 21
                DataField = 'CARTEIRA'
                DataSource = DSContas
                TabOrder = 0
              end
            end
            object DBEdit5: TDBEdit
              Left = 15
              Top = 321
              Width = 108
              Height = 21
              DataField = 'CONTRATO'
              DataSource = DSContas
              TabOrder = 3
              Visible = False
            end
          end
        end
      end
    end
    object TabLayoutBoleto: TTabSheet [3]
      Caption = 'Layout Boleto'
      ImageIndex = 4
      OnShow = TabLayoutBoletoShow
      object Splitter1: TSplitter
        Left = 742
        Top = 0
        Width = 2
        Height = 564
        Align = alRight
      end
      object Panel5: TPanel
        Left = 744
        Top = 0
        Width = 96
        Height = 564
        Align = alRight
        TabOrder = 1
        object LV: TListView
          Left = 1
          Top = 1
          Width = 94
          Height = 562
          Align = alClient
          Columns = <
            item
              Caption = 'Campo'
              Width = 120
            end
            item
              Caption = 'Detalhe'
              Width = 350
            end>
          ColumnClick = False
          GridLines = True
          Items.Data = {
            B20100000800000000000000FFFFFFFFFFFFFFFF010000000000000008446174
            615F446F63114461746120646F20446F63756D656E746F00000000FFFFFFFFFF
            FFFFFF010000000000000009446174615F56656E631C4461746120646F205665
            6E63696D656E746F20646F20426F6C65746F00000000FFFFFFFFFFFFFFFF0100
            00000000000006466174757261104E756D65726F206461204661747572610000
            0000FFFFFFFFFFFFFFFF01000000000000000E5361635F42616972726F5F4365
            701642616972726F20652043657020646F2053616361646F00000000FFFFFFFF
            FFFFFFFF01000000000000000D5361635F4369646164655F5566194369646164
            6520652045737461646F20646F2053616361646F00000000FFFFFFFFFFFFFFFF
            0100000000000000075361635F456E6412456E64657265E76F20646F20536163
            61646F00000000FFFFFFFFFFFFFFFF0100000000000000095361635F52617A61
            6F1652617AE36F20536F6369616C20646F2053616361646F00000000FFFFFFFF
            FFFFFFFF01000000000000000556616C6F720F56616C6F7220646F20426F6C65
            746FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ReadOnly = True
          RowSelect = True
          SortType = stText
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 742
        Height = 564
        Align = alClient
        TabOrder = 0
        object MmBoleto: TMemo
          Left = 1
          Top = 1
          Width = 740
          Height = 489
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          WordWrap = False
          OnChange = MmBoletoChange
          OnClick = MmBoletoClick
          OnEnter = MmBoletoEnter
          OnKeyDown = MmBoletoKeyDown
          OnKeyPress = MmBoletoKeyPress
        end
        object Panel7: TPanel
          Left = 1
          Top = 509
          Width = 740
          Height = 54
          Align = alBottom
          BorderStyle = bsSingle
          TabOrder = 2
          object Button1: TButton
            Left = 24
            Top = 16
            Width = 118
            Height = 25
            Caption = 'Gravar'
            TabOrder = 0
            OnClick = Button1Click
          end
        end
        object LayBarra: TStatusBar
          Left = 1
          Top = 490
          Width = 740
          Height = 19
          Panels = <>
          SimplePanel = True
        end
      end
    end
    inherited TabHistorico: TTabSheet
      inherited PanelHistorico: TPanel
        Width = 840
      end
      inherited GridHistorico: TJvDBGrid
        Width = 840
        Height = 517
      end
    end
  end
  inherited PanStatus: TPanel
    Width = 848
  end
  inherited panStatus2: TPanel
    Width = 848
  end
  inherited DSCadastro: TDataSource
    Left = 328
    Top = 573
  end
  inherited PopupBut: TPopupMenu
    Left = 463
    Top = 534
  end
  inherited PopupGrid1: TPopupMenu
    Left = 505
    Top = 535
  end
  inherited QHistorico: TADOQuery
    Left = 420
    Top = 533
  end
  inherited DSHistorico: TDataSource
    OnDataChange = DSHistoricoDataChange
    Left = 420
    Top = 573
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from bancos where codigo = -1')
    Left = 328
    Top = 541
    object QCadastroCODIGO: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'CODIGO'
      Required = True
    end
    object QCadastroBANCO: TStringField
      DisplayLabel = 'Banco'
      FieldName = 'BANCO'
      Size = 45
    end
    object QCadastroAPAGADO: TStringField
      DisplayLabel = 'Apagado'
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroOPERCADASTRO: TStringField
      DisplayLabel = 'Opera'#231#227'o Cadastral'
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroLAYOUT: TMemoField
      DisplayLabel = 'Layout'
      FieldName = 'LAYOUT'
      BlobType = ftMemo
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
    end
    object QCadastroDESC_TAXA: TStringField
      FieldName = 'DESC_TAXA'
      FixedChar = True
      Size = 1
    end
  end
  object DSContas: TDataSource
    DataSet = QContas
    OnStateChange = DSContasStateChange
    Left = 375
    Top = 573
  end
  object QContas: TADOQuery
    Connection = DMConexao.AdoCon
    AfterInsert = QContasAfterInsert
    Parameters = <
      item
        Name = 'codbanco'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      
        'Select * from contas_bancarias where apagado <> '#39'S'#39' and cod_banc' +
        'o = :codbanco'
      '')
    Left = 380
    Top = 538
    object QContasCONTA_ID: TIntegerField
      FieldName = 'CONTA_ID'
    end
    object QContasCOD_BANCO: TIntegerField
      DisplayLabel = 'C'#243'd. do Banco'
      FieldName = 'COD_BANCO'
    end
    object QContasAGENCIA: TStringField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'AGENCIA'
      Size = 10
    end
    object QContasCONTACORRENTE: TStringField
      DisplayLabel = 'Conta Corrente'
      FieldName = 'CONTACORRENTE'
      Size = 15
    end
    object QContasCONTRATO: TStringField
      FieldName = 'CONTRATO'
      Size = 10
    end
    object QContasPROTESTAR: TStringField
      FieldName = 'PROTESTAR'
      FixedChar = True
      Size = 1
    end
    object QContasDIAS_PROTESTO: TIntegerField
      FieldName = 'DIAS_PROTESTO'
    end
    object QContasMENSAGEM1_BOLETO: TStringField
      FieldName = 'MENSAGEM1_BOLETO'
      Size = 50
    end
    object QContasMENSAGEM2_BOLETO: TStringField
      FieldName = 'MENSAGEM2_BOLETO'
      Size = 50
    end
    object QContasARQUIVO_BOLETO: TStringField
      FieldName = 'ARQUIVO_BOLETO'
      Size = 50
    end
    object QContasPERC_JUROS: TFloatField
      FieldName = 'PERC_JUROS'
    end
    object QContasPERC_MULTA: TFloatField
      FieldName = 'PERC_MULTA'
    end
    object QContasPERC_DESC: TFloatField
      FieldName = 'PERC_DESC'
    end
    object QContasAPAGADO: TStringField
      FieldName = 'APAGADO'
      FixedChar = True
      Size = 1
    end
    object QContasIDENT_TITULO: TIntegerField
      FieldName = 'IDENT_TITULO'
    end
    object QContasSEQ_ARQ: TIntegerField
      FieldName = 'SEQ_ARQ'
    end
    object QContasTAXA_DEBITO: TBCDField
      FieldName = 'TAXA_DEBITO'
      Precision = 15
      Size = 2
    end
    object QContasSEQ_REMESSA: TIntegerField
      DisplayLabel = 'Ultima Remessa'
      FieldName = 'SEQ_REMESSA'
    end
    object QContasCOD_CONV: TStringField
      FieldName = 'COD_CONV'
      Size = 13
    end
    object QContasNOME_CONVENIO: TStringField
      FieldName = 'NOME_CONVENIO'
      Size = 30
    end
    object QContasUSA_DIGITO: TStringField
      FieldName = 'USA_DIGITO'
      FixedChar = True
      Size = 1
    end
    object QContasCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QContasTIPO_LAYOUT: TStringField
      FieldName = 'TIPO_LAYOUT'
    end
    object QContasDAC: TStringField
      FieldName = 'DAC'
      FixedChar = True
      Size = 1
    end
    object QContasENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 30
    end
    object QContasNUMERO: TStringField
      FieldName = 'NUMERO'
      Size = 5
    end
    object QContasCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 15
    end
    object QContasCIDADE: TStringField
      FieldName = 'CIDADE'
    end
    object QContasCEP: TStringField
      FieldName = 'CEP'
      Size = 8
    end
    object QContasUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
    object QContasCARTEIRA: TStringField
      FieldName = 'CARTEIRA'
      Size = 5
    end
  end
end
