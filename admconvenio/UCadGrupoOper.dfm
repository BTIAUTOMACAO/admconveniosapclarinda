inherited FCadGrupoOper: TFCadGrupoOper
  Left = 187
  Top = 153
  Caption = 'Cadastro de Grupo de Operadores'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = TabPermis
    inherited TabGrade: TTabSheet
      inherited Panel1: TPanel
        inherited ButBusca: TBitBtn
          OnClick = ButBuscaClick
        end
      end
      inherited DBGrid1: TJvDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'GRUPO_USU_ID'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Width = 224
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ADMINISTRADOR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIBERADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DTALTERACAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OPERADOR'
            Visible = True
          end>
      end
    end
    inherited TabFicha: TTabSheet
      inherited Panel3: TPanel
        object GroupBox1: TGroupBox
          Left = 2
          Top = 2
          Width = 705
          Height = 124
          Align = alTop
          Caption = 'Principal'
          TabOrder = 0
          object Label3: TLabel
            Left = 32
            Top = 24
            Width = 43
            Height = 13
            Caption = 'Grupo ID'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 104
            Top = 24
            Width = 48
            Height = 13
            Caption = 'Descri'#231#227'o'
            FocusControl = dbEdtDesc
          end
          object DBEdit1: TDBEdit
            Left = 32
            Top = 40
            Width = 65
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'GRUPO_USU_ID'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object dbEdtDesc: TDBEdit
            Left = 104
            Top = 40
            Width = 329
            Height = 21
            CharCase = ecUpperCase
            DataField = 'DESCRICAO'
            DataSource = DSCadastro
            TabOrder = 1
          end
          object CheckLib: TDBCheckBox
            Left = 32
            Top = 95
            Width = 97
            Height = 17
            Caption = 'Liberado'
            DataField = 'LIBERADO'
            DataSource = DSCadastro
            TabOrder = 3
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object heckADM: TDBCheckBox
            Left = 32
            Top = 73
            Width = 97
            Height = 17
            Caption = 'Administrador'
            DataField = 'ADMINISTRADOR'
            DataSource = DSCadastro
            TabOrder = 2
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 392
          Width = 705
          Height = 65
          Align = alBottom
          Caption = 'Dados da '#250'ltima altera'#231#227'o'
          TabOrder = 1
          object Label14: TLabel
            Left = 16
            Top = 21
            Width = 118
            Height = 13
            Caption = 'Data da '#218'ltima Altera'#231#227'o'
            FocusControl = DBEdit12
          end
          object Label13: TLabel
            Left = 144
            Top = 21
            Width = 44
            Height = 13
            Caption = 'Operador'
            FocusControl = DBEdit11
          end
          object DBEdit12: TDBEdit
            Left = 16
            Top = 36
            Width = 113
            Height = 21
            TabStop = False
            DataField = 'DTALTERACAO'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 144
            Top = 36
            Width = 161
            Height = 21
            TabStop = False
            DataField = 'OPERADOR'
            DataSource = DSCadastro
            ReadOnly = True
            TabOrder = 1
          end
        end
      end
    end
    object TabPermis: TTabSheet [2]
      Caption = '&Permiss'#245'es'
      ImageIndex = 3
      OnShow = TabPermisShow
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 709
        Height = 491
        Align = alClient
        Caption = 'Permiss'#245'es Para o Grupo'
        TabOrder = 0
        object Label5: TLabel
          Left = 11
          Top = 19
          Width = 114
          Height = 13
          Caption = 'M'#243'dulos do Sistema'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 339
          Top = 19
          Width = 244
          Height = 13
          Caption = 'Configura'#231#227'o de M'#243'dulos para este Grupo.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SpeedButton2: TSpeedButton
          Left = 307
          Top = 208
          Width = 26
          Height = 25
          Caption = '<'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 307
          Top = 168
          Width = 26
          Height = 25
          Caption = '>>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 307
          Top = 232
          Width = 26
          Height = 25
          Caption = '<<'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton4Click
        end
        object SpeedButton1: TSpeedButton
          Left = 307
          Top = 143
          Width = 26
          Height = 25
          Caption = '>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = SpeedButton1Click
        end
        object Label7: TLabel
          Left = 586
          Top = 19
          Width = 3
          Height = 13
        end
        object JvDBGrid1: TJvDBGrid
          Left = 19
          Top = 35
          Width = 289
          Height = 380
          DataSource = DSModulos
          DefaultDrawing = False
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = JvDBGrid1DblClick
          AutoAppend = False
          TitleButtons = True
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'DESCRICAO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 247
              Visible = True
            end>
        end
        object JvDBGrid2: TJvDBGrid
          Left = 339
          Top = 35
          Width = 454
          Height = 380
          Hint = 'Clique sobre o titulo da coluna para atera'#231#227'o linear.'
          DataSource = DSPermissoes
          DefaultDrawing = False
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgConfirmDelete, dgCancelOnExit]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          AutoAppend = False
          TitleButtons = True
          OnTitleBtnClick = JvDBGrid2TitleBtnClick
          SelectColumnsDialogStrings.Caption = 'Select columns'
          SelectColumnsDialogStrings.OK = '&OK'
          SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
          EditControls = <>
          RowsHeight = 16
          TitleRowHeight = 17
          Columns = <
            item
              Expanded = False
              FieldName = 'modulo'
              ReadOnly = True
              Title.Caption = 'Descri'#231#227'o'
              Width = 252
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACESSA'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Acessar'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INCLUI'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Incluir'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALTERA'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Alterar'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EXCLUI'
              PickList.Strings = (
                'S'
                'N')
              Title.Caption = 'Excluir'
              Width = 41
              Visible = True
            end>
        end
      end
    end
  end
  inherited DSCadastro: TDataSource
    Left = 436
  end
  inherited PopupGrid1: TPopupMenu
    Left = 116
    Top = 136
  end
  inherited QHistorico: TADOQuery
    Left = 476
  end
  inherited DSHistorico: TDataSource
    Left = 476
    Top = 192
  end
  inherited QCadastro: TADOQuery
    AfterInsert = QCadastroAfterInsert
    SQL.Strings = (
      'select * from grupo_usuarios where grupo_usu_id = 0 ')
    Left = 436
    object QCadastroGRUPO_USU_ID: TIntegerField
      DisplayLabel = 'Grupo ID'
      FieldName = 'GRUPO_USU_ID'
      Required = True
    end
    object QCadastroDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Size = 25
    end
    object QCadastroADMINISTRADOR: TStringField
      DisplayLabel = 'Administrador'
      FieldName = 'ADMINISTRADOR'
      Size = 1
    end
    object QCadastroLIBERADO: TStringField
      DisplayLabel = 'Lib.'
      FieldName = 'LIBERADO'
      Size = 1
    end
    object QCadastroAPAGADO: TStringField
      FieldName = 'APAGADO'
      Size = 1
    end
    object QCadastroOPERADOR: TStringField
      DisplayLabel = 'Operador'
      FieldName = 'OPERADOR'
      Size = 25
    end
    object QCadastroNIVEL_ALFABETICO: TStringField
      DisplayLabel = 'N'#237'vel Alfab'#233'tico'
      FieldName = 'NIVEL_ALFABETICO'
      Required = True
      Size = 1
    end
    object QCadastroNIVEL_NUMERICO: TSmallintField
      DisplayLabel = 'N'#237'vel Num'#233'rico'
      FieldName = 'NIVEL_NUMERICO'
    end
    object QCadastroOPERCADASTRO: TStringField
      FieldName = 'OPERCADASTRO'
      Size = 25
    end
    object QCadastroDTAPAGADO: TDateTimeField
      FieldName = 'DTAPAGADO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTALTERACAO: TDateTimeField
      FieldName = 'DTALTERACAO'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QCadastroDTCADASTRO: TDateTimeField
      FieldName = 'DTCADASTRO'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object DSModulos: TDataSource
    DataSet = QModulos
    Left = 136
    Top = 208
  end
  object DSPermissoes: TDataSource
    DataSet = QPermissoes
    Left = 480
    Top = 240
  end
  object QPermissoes: TADOQuery
    Connection = DMConexao.AdoCon
    BeforePost = QPermissoesBeforePost
    Parameters = <
      item
        Name = 'grupo_id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'Select * from permissoes where grupo_id = :grupo_id'
      'order by modulo')
    Left = 448
    Top = 240
    object QPermissoesPERM_ID: TIntegerField
      FieldName = 'PERM_ID'
    end
    object QPermissoesGRUPO_ID: TIntegerField
      FieldName = 'GRUPO_ID'
    end
    object QPermissoesMODULO_ID: TIntegerField
      FieldName = 'MODULO_ID'
    end
    object QPermissoesINCLUI: TStringField
      FieldName = 'INCLUI'
      FixedChar = True
      Size = 1
    end
    object QPermissoesALTERA: TStringField
      FieldName = 'ALTERA'
      FixedChar = True
      Size = 1
    end
    object QPermissoesEXCLUI: TStringField
      FieldName = 'EXCLUI'
      FixedChar = True
      Size = 1
    end
    object QPermissoesACESSA: TStringField
      FieldName = 'ACESSA'
      FixedChar = True
      Size = 1
    end
    object QPermissoesmodulo: TStringField
      DisplayWidth = 100
      FieldKind = fkLookup
      FieldName = 'modulo'
      LookupDataSet = QModulos
      LookupKeyFields = 'MODULO_ID'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'MODULO_ID'
      Size = 80
      Lookup = True
    end
  end
  object QModulos: TADOQuery
    Connection = DMConexao.AdoCon
    Parameters = <>
    SQL.Strings = (
      'Select * from modulos order by descricao')
    Left = 96
    Top = 208
    object QModulosMODULO_ID: TIntegerField
      FieldName = 'MODULO_ID'
    end
    object QModulosDESCRICAO: TStringField
      DisplayWidth = 100
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object QModulosNOME: TStringField
      FieldName = 'NOME'
      Size = 50
    end
  end
end
