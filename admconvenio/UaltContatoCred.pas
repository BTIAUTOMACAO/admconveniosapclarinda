unit UaltContatoCred;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, DBCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TFAltContatoCred = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    txtTelefone1: TEdit;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    txtNome1: TEdit;
    btnAddTel: TBitBtn;
    txtTel2: TEdit;
    procedure btnAddTelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAltContatoCred: TFAltContatoCred;

implementation

uses UCadCred;

{$R *.dfm}

procedure TFAltContatoCred.btnAddTelClick(Sender: TObject);
begin
  txtTel2.Visible := True;
  txtTel2.SetFocus;
end;

end.
