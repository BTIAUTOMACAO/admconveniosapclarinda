unit ULancamentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U1, Menus, StdCtrls, {JvLookup,} CurrEdit, Mask, ToolEdit, Buttons,
  DB, ExtCtrls, Dateutils, DBCtrls,UTipos, JvExControls, JvDBLookup, JvExMask, JvToolEdit,
  JvExStdCtrls, JvEdit, JvValidateEdit, ADODB;


type
  TFLancamentos = class(TF1)
    GroupBox1: TGroupBox;
    DBCredenciado: TJvDBLookupCombo;
    EdCred_id: TEdit;
    GroupBox2: TGroupBox;
    DBEmpresa: TJvDBLookupCombo;
    EdEmpres_id: TEdit;
    GroupBox3: TGroupBox;
    EdCartao: TEdit;
    EdChapa: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    Label3: TLabel;
    f: TGroupBox;
    Label4: TLabel;
    LabEmp: TLabel;
    Label6: TLabel;
    LabCre: TLabel;
    //EDData: TDateEdit;
    Label8: TLabel;
    EdNf: TEdit;
    Label9: TLabel;
    ComRec: TComboBox;
    Label10: TLabel;
    //Debito: TCurrencyEdit;
    //Credito: TCurrencyEdit;
    Label11: TLabel;
    Label12: TLabel;
    Button1: TButton;
    ButPesqConv: TSpeedButton;
    DSQCredenciados: TDataSource;
    DSQEmpresas: TDataSource;
    DataSource3: TDataSource;
    EdHist: TEdit;
    Label14: TLabel;
    DBFormapgto: TDBLookupComboBox;
    Label13: TLabel;
    DSForma: TDataSource;
    Label15: TLabel;
    LabAguarde: TLabel;
    Label16: TLabel;
    ComEntregue: TComboBox;
    Panel1: TPanel;
    rdgUsaWebService: TRadioGroup;
    Label5: TLabel;
    Memo1: TMemo;
    Debito: TJvValidateEdit;
    Credito: TJvValidateEdit;
    EDData: TJvDateEdit;
    QCredenciados: TADOQuery;
    QCredenciadoscred_id: TIntegerField;
    QCredenciadosnome: TStringField;
    QCredenciadosfantasia: TStringField;
    QCredenciadoscodacesso: TIntegerField;
    QCredenciadosliberado: TStringField;
    QCredenciadosvencimento1: TWordField;
    QEmpresas: TADOQuery;
    QEmpresasempres_id: TIntegerField;
    QEmpresasnome: TStringField;
    QEmpresasfantasia: TStringField;
    QEmpresasliberada: TStringField;
    QConveniado: TADOQuery;
    QConveniadoconv_id: TIntegerField;
    QConveniadocartao_id: TIntegerField;
    QConveniadonome: TStringField;
    QConveniadocodigo: TIntegerField;
    QConveniadodigito: TWordField;
    QConveniadocartlib: TStringField;
    QConveniadoconvlib: TStringField;
    QConveniadochapa: TFloatField;
    QConveniadoempres_id: TIntegerField;
    QContaCorrente: TADOQuery;
    QContaCorrenteAUTORIZACAO_ID: TIntegerField;
    QContaCorrenteCARTAO_ID: TIntegerField;
    QContaCorrenteCONV_ID: TIntegerField;
    QContaCorrenteCRED_ID: TIntegerField;
    QContaCorrenteDIGITO: TWordField;
    QContaCorrenteDATA: TDateTimeField;
    QContaCorrenteHORA: TStringField;
    QContaCorrenteDATAVENDA: TDateTimeField;
    QContaCorrenteDEBITO: TBCDField;
    QContaCorrenteCREDITO: TBCDField;
    QContaCorrenteVALOR_CANCELADO: TBCDField;
    QContaCorrenteBAIXA_CONVENIADO: TStringField;
    QContaCorrenteBAIXA_CREDENCIADO: TStringField;
    QContaCorrenteENTREG_NF: TStringField;
    QContaCorrenteRECEITA: TStringField;
    QContaCorrenteCESTA: TStringField;
    QContaCorrenteCANCELADA: TStringField;
    QContaCorrenteDIGI_MANUAL: TStringField;
    QContaCorrenteTRANS_ID: TIntegerField;
    QContaCorrenteFORMAPAGTO_ID: TIntegerField;
    QContaCorrenteFATURA_ID: TIntegerField;
    QContaCorrentePAGAMENTO_CRED_ID: TIntegerField;
    QContaCorrenteAUTORIZACAO_ID_CANC: TIntegerField;
    QContaCorrenteOPERADOR: TStringField;
    QContaCorrenteDATA_VENC_EMP: TDateTimeField;
    QContaCorrenteDATA_FECHA_EMP: TDateTimeField;
    QContaCorrenteDATA_VENC_FOR: TDateTimeField;
    QContaCorrenteDATA_FECHA_FOR: TDateTimeField;
    QContaCorrenteHISTORICO: TStringField;
    QContaCorrenteNF: TIntegerField;
    QContaCorrenteDATA_ALTERACAO: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CONV: TDateTimeField;
    QContaCorrenteDATA_BAIXA_CRED: TDateTimeField;
    QContaCorrenteOPER_BAIXA_CONV: TStringField;
    QContaCorrenteOPER_BAIXA_CRED: TStringField;
    QContaCorrenteDATA_CONFIRMACAO: TDateTimeField;
    QContaCorrenteOPER_CONFIRMACAO: TStringField;
    QContaCorrenteCONFERIDO: TStringField;
    QContaCorrenteNSU: TIntegerField;
    QContaCorrentePREVIAMENTE_CANCELADA: TStringField;
    QContaCorrenteEMPRES_ID: TIntegerField;
    QFormaspgto: TADOQuery;
    QFormaspgtoforma_id: TIntegerField;
    QFormaspgtodescricao: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure EdCred_idKeyPress(Sender: TObject; var Key: Char);
    procedure EdCred_idChange(Sender: TObject);
    procedure DBCredenciadoChange(Sender: TObject);
    procedure DBEmpresaChange(Sender: TObject);
    procedure EdEmpres_idChange(Sender: TObject);
    procedure EdCartaoExit(Sender: TObject);
    procedure EdChapaExit(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButPesqConvClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DebitoExit(Sender: TObject);
    procedure CreditoExit(Sender: TObject);
    procedure ComRecKeyPress(Sender: TObject; var Key: Char);
    procedure rdgUsaWebServiceClick(Sender: TObject);
  private
    { Private declarations }
    Autors : Array [1..24] of TAutorizacao;
    FocoCodFor : Boolean;
    procedure Valida;
    function venc_fornec:TDateTime;
    procedure achaConveniado(Tipo: Integer);
    procedure ListaAutors(retorno:string);
  public
    { Public declarations }
    Lanc_Webservice, Path_WebService: string;
    informar_emp : Boolean;
  end;


implementation

uses DM, UPesqConv, WPegaAutor, UMenu, cartao_util;

{$R *.dfm}

procedure TFLancamentos.FormCreate(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  DMConexao.Config.Open;
  if DMConexao.ConfigCRE_FANTASIA.AsString = 'S' then begin
     LabCre.Caption := 'Nome Fantasia';
     DBCredenciado.LookupDisplay := 'FANTASIA';
     QCredenciados.SQL.Text := QCredenciados.SQL.Text + ' order by fantasia ';
  end
  else
     QCredenciados.SQL.Text := QCredenciados.SQL.Text + ' order by nome ';
  if DMConexao.ConfigEMP_FANTASIA.AsString = 'S' then begin
     LabEmp.Caption := 'Nome Fantasia';
     DBEmpresa.LookupDisplay := 'FANTASIA';
     QEmpresas.SQL.Text := QEmpresas.SQL.Text + ' order by fantasia ';
  end
  else
     QEmpresas.SQL.Text := QEmpresas.SQL.Text + ' order by nome ';
  DMConexao.Config.Close;
  QEmpresas.Open;
  QCredenciados.Open;
  EDData.Date := Date;
  DMConexao.Config.Open;  //Define se o foco ap�s o lan�amentos vai para o fornecedor.
  FocoCodFor := (DMConexao.ConfigFLANCAMENTOS_FOCO_FORNEC.AsString = 'S');
  if DMConexao.ConfigFLANCAMENTOS_AUTOR_WEBSERVICE.AsString = 'S' then begin
     Path_WebService := DMConexao.ConfigPATH_WEBSERVICE.AsString;
     rdgUsaWebService.ItemIndex:= 0;
     QFormaspgto.Open;
     QFormaspgto.First;
     DBFormapgto.KeyValue := QFormaspgtoFORMA_ID.AsVariant;
     if Path_WebService <> '' then
        Path_WebService := Path_WebService + 'wpegaautor.asmx'
     else
        ShowMessage('Caminho do webservice n�o encontrado, por favor atualize as configura��es do sistema.');
  end
  else rdgUsaWebService.ItemIndex:= 1;
  informar_emp := (DMConexao.ConfigINFORMA_EMP_LANC_IND.AsString = 'S');
  DMConexao.Config.Close;
  DBFormapgto.Enabled := (rdgUsaWebService.ItemIndex=0);
  Screen.Cursor := crDefault;
end;

procedure TFLancamentos.EdCred_idKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#13,#8]) then Key := #0;

end;

procedure TFLancamentos.EdCred_idChange(Sender: TObject);
begin
if Trim(EdCred_id.Text) <> '' then begin
   if QCredenciados.Locate('cred_id',EdCred_id.Text,[]) then
      DBCredenciado.KeyValue := EdCred_id.Text
   else begin
      DBCredenciado.ClearValue;
   end;
end
else DBCredenciado.ClearValue;
end;

procedure TFLancamentos.DBCredenciadoChange(Sender: TObject);
begin
EdCred_id.Text := DBCredenciado.KeyValue;
end;

procedure TFLancamentos.DBEmpresaChange(Sender: TObject);
begin
if DBEmpresa.KeyValue <> null then
   EdEmpres_id.Text := DBEmpresa.KeyValue;
end;

procedure TFLancamentos.EdEmpres_idChange(Sender: TObject);
begin
if Trim(EdEmpres_id.Text) <> '' then begin
   if QEmpresas.Locate('empres_id',EdEmpres_id.Text,[]) then
      DBEmpresa.KeyValue := EdEmpres_id.Text
   else begin
      DBEmpresa.ClearValue;
   end;
end
else DBEmpresa.ClearValue;
end;

procedure TFLancamentos.EdCartaoExit(Sender: TObject);
begin
if Trim(EdCartao.Text) <> '' then
   achaConveniado(3);
end;

procedure TFLancamentos.EdChapaExit(Sender: TObject);
begin
if Trim(EdChapa.Text) <> '' then begin
   if Trim(EdEmpres_id.Text) = '' then begin
      ShowMessage('Informe a empresa.');
      EdEmpres_id.SetFocus;
      Exit;
   end;
   achaConveniado(2);
end;
end;

procedure TFLancamentos.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key = vk_return then ButPesqConv.Click;
end;

procedure TFLancamentos.ButPesqConvClick(Sender: TObject);
begin
   if (Trim(EdEmpres_id.Text) = '') and informar_emp then begin
      ShowMessage('Selecione a empresa.');
      EdEmpres_id.SetFocus;
      Exit;
   end;
   QConveniado.Close;
   FPesqConv := TFPesqConv.Create(self);
   FPesqConv.FLancamentos := Self;
   FPesqConv.Edit1.Text := EdNome.Text;
   FPesqConv.ShowModal;
   if FPesqConv.ModalResult = mrok then begin
      achaConveniado(1);
   end
   else begin
      EdChapa.Clear;
      EdNome.Clear;
      EdCartao.Clear;
      EdNome.SetFocus;
   end;
   FPesqConv.QConveniados.Close;
   FPesqConv.Free;
   DecimalSeparator := ',';
end;

procedure TFLancamentos.Button1Click(Sender: TObject);
var Retorno, valor : string; i : integer;  Saldo : Currency;
begin
   Valida;
   if Application.MessageBox('Confirma o lan�amento?','Confirma��o',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYes then begin
      if QEmpresasLIBERADA.AsString <> 'S' then
         if Application.MessageBox(PChar('A empresa '+QEmpresasNOME.AsString+' n�o est� liberada.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then Exit;
      if QConveniadoCONVLIB.AsString <> 'S' then
         if Application.MessageBox(PChar('O titular deste cart�o  n�o est� liberado.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then Exit;
      if QConveniadoCARTLIB.AsString <> 'S' then
         if Application.MessageBox(PChar('Este cart�o  n�o est� liberado.'+#13+'Deseja continuar o lan�amento?'),'Confirma��o',mb_yesno+mb_defbutton1+MB_ICONQUESTION) = IDNo then Exit;
      if ((rdgUsaWebService.ItemIndex=0) and (Debito.Value > 0)) then begin
         if Trim(Path_WebService) = '' then begin
            ShowMessage('Caminho do webservice n�o encontrado, favor atualizar suas configura��es.');
            Exit;
         end;
         Try
            valor := IntToStr(Trunc(Debito.Value*100)); //passar como inteiro.
            LabAguarde.Show;
            Repaint;
            Retorno := GetWPegaAutorSoap(False,Path_WebService).GeraAutorizacao(EdCartao.Text,'N',valor,QCredenciadosCODACESSO.AsString,'1111',QFormaspgtoFORMA_ID.AsInteger);
            DecimalSeparator := ',';
            if Pos('mensagem',retorno) > 0 then begin
               Memo1.Lines.Add('----------------------------------------------------------------------------------------------------------------');
               Memo1.Lines.Add('Problema na autoriza��o: '+StringReplace(retorno,''#$A' ',sLineBreak,[rfReplaceAll])+sLineBreak+FormatDateTime('dd/mm/yyyy hh:nn:ss',Now));
               Memo1.Lines.Add('----------------------------------------------------------------------------------------------------------------');
            end
            else begin
               ListaAutors(Retorno);
               for i := 1 to 24 do begin
                  if Autors[i].Autor_id <> '' then begin
                     GetWPegaAutorSoap(False,Path_WebService).InformaNFRecHistOperConf(Autors[i].Autor_id,EdNf.Text,Copy(ComRec.Text,1,1),EdHist.Text,Operador.Nome,Copy(ComEntregue.Text, 1, 1));
                     Memo1.Lines.Add(' Autoriza��o: '+Autors[i].Autor_id+Autors[i].dig);
                  end
                  else Break;
               end;
               DecimalSeparator := ',';
               Memo1.Lines.Add(' Valor Total: '+FormatFloat('###,###,##0.00',Debito.Value));
               Memo1.Lines.Add(' Titular: '+QConveniadoNOME.AsString+' data/hora: '+FormatDateTime('dd/mm/yyyy',EDData.Date)+' '+TimeToStr(now));
               Memo1.Lines.Add(' Credenciado: '+QCredenciadosNOME.AsString);
               Memo1.Lines.Add('----------------------------------------------------------------------------------------------------------------');
           end;
           LabAguarde.Hide;
           Repaint;
         except
           on E:Exception do ShowMessage('Erro: '+E.Message);
         end;
      end
      else begin
         if (Debito.Value > 0) then begin
            Saldo := 0;// comentado ariane DMConexao.GetSaldoRestanteConv(QConveniadoCONV_ID.AsInteger,EDData.Date);
            if Debito.Value > Saldo then begin
               if not MsgSimNao('Aten��o!'+sLineBreak+sLineBreak+
                                'O Conveniado nao possu� saldo para lan�amento com este valor nesta data!'+sLineBreak+
                                'Saldo Restante para esta data: R$ '+FormatDinBR(Saldo)+sLineBreak+sLineBreak+
                                'Confirma o Lan�amento estourando o limite?',False) then begin

                  Exit;
               end;
            end;
         end;
         {DM1.StoredProc1.StoredProcName := 'PEGA_AUTORIZACAO_ID';
         DM1.StoredProc1.ExecProc;  }//comentado ariane
         QContaCorrente.Open;
         QContaCorrente.Append;
         {QContaCorrenteAUTORIZACAO_ID.AsInteger   := DM1.StoredProc1.Params[0].AsInteger;  }//comentado ariane
         QContaCorrenteDIGITO.AsInteger           := DigitoControle(QContaCorrenteAUTORIZACAO_ID.AsFloat);
         QContaCorrenteCONV_ID.AsInteger          := QConveniadoconv_id.AsInteger;
         QContaCorrenteCARTAO_ID.AsInteger        := QConveniadocartao_id.AsInteger;
         QContaCorrenteCREDITO.AsFloat            := Credito.Value;
         QContaCorrenteCRED_ID.AsInteger          := QCredenciadoscred_id.AsInteger;
         QContaCorrenteDATA.AsDateTime            := EDData.Date;
         QContaCorrenteDEBITO.AsFloat             := Debito.Value;
         QContaCorrenteCREDITO.AsFloat            := Credito.Value;
         QContaCorrenteHORA.AsString              := FormatDateTime('hh:nn:ss',now);
         QContaCorrenteOPERADOR.AsString          := Operador.Nome;
         QContaCorrenteHISTORICO.AsString         := EdHist.Text;
         QContaCorrenteBAIXA_CONVENIADO.AsString  := 'N';
         QContaCorrenteBAIXA_CREDENCIADO.AsString := 'N';
         QContaCorrenteNF.AsString                := EdNf.Text;
         QContaCorrenteRECEITA.AsString           := 'N';
         QContaCorrenteDIGI_MANUAL.AsString       := 'S';
         if ComRec.ItemIndex = 1 then QContaCorrenteRECEITA.AsString := 'S';
         if ComEntregue.ItemIndex = 1 then
           QContaCorrenteENTREG_NF.AsString := 'S'
         else
           QContaCorrenteENTREG_NF.AsString := 'N';
         QContaCorrente.Post;
         if Debito.Value > 0 then Memo1.Lines.Add(' Autorizacao:'+QContaCorrenteAUTORIZACAO_ID.AsString+FormatFloat('00',QContaCorrenteDIGITO.AsFloat)+' D�bito Valor: '+FormatFloat('########0.00',Debito.Value))
                             else Memo1.Lines.Add(' Autorizacao:'+QContaCorrenteAUTORIZACAO_ID.AsString+FormatFloat('00',QContaCorrenteDIGITO.AsFloat)+' Cr�dito Valor: '+FormatFloat('########0.00',Credito.Value));
         Memo1.Lines.Add(' Titular: '+QConveniadonome.AsString+' data/hora: '+FormatDateTime('dd/mm/yyyy',EDData.Date)+' '+TimeToStr(now));
         Memo1.Lines.Add(' Credenciado: '+QCredenciadosnome.AsString);
         Memo1.Lines.Add('-----------------------------------------------------------------------------------------------');

      end;

      EDData.Date := Date;
      EdNf.Clear;
      Debito.Value := 0;
      Credito.Value := 0;
      QConveniado.Close;
      QContaCorrente.Close;
      EdCartao.Clear;
      EdChapa.Clear;
      EdNome.Clear;
      if FocoCodFor then EdCred_id.SetFocus
                    else EdEmpres_id.SetFocus;

   end;
end;


procedure TFLancamentos.DebitoExit(Sender: TObject);
begin
if debito.Value > 0 then credito.Value := 0;

end;

procedure TFLancamentos.CreditoExit(Sender: TObject);
begin
if credito.value > 0 then debito.value := 0;
end;

procedure TFLancamentos.Valida;
begin
if ((Debito.Value <= 0) and (credito.Value <= 0)) then begin
   ShowMessage('Informe o valor a ser lan�ado.');
   debito.SetFocus;
   Sysutils.Abort;
end;
if ((Debito.Value > 0) and (credito.Value > 0)) then begin
   ShowMessage('Informe o valor de d�bito ou cr�dito a ser lan�ado.');
   debito.SetFocus;
   Sysutils.Abort;
end;
if ((QConveniadocartao_id.AsInteger <= 0) or (not QConveniado.Active)) then begin
   ShowMessage('Cart�o n�o encontrado.');
   EdCartao.SetFocus;
   Sysutils.Abort;
end;
if Trim(EdEmpres_id.Text) = '' then begin
   ShowMessage('Informe a empresa.');
   EdEmpres_id.SetFocus;
   Sysutils.Abort;
end;
if Trim(EdCred_id.Text) = '' then begin
   ShowMessage('Informe a empresa.');
   EdCred_id.SetFocus;
   Sysutils.Abort;
end;
if Trim(EdHist.Text) = '' then begin
   ShowMessage('Informe o hist�rico do lan�amento.');
   EdCred_id.SetFocus;
   Sysutils.Abort;
end;
end;

procedure TFLancamentos.ComRecKeyPress(Sender: TObject; var Key: Char);
begin
//inherited;
//if key = #13 then debito.SetFocus;
end;

function TFLancamentos.venc_fornec:TDateTime;
begin
Result := EncodeDate(YearOf(EDData.Date),MonthOf(EDData.Date),word(QCredenciadosVENCIMENTO1.AsInteger));
if Result <= EDData.Date then Result := IncMonth(Result);
end;

procedure TFLancamentos.achaConveniado(Tipo:Integer); //Tipo 1 por Cartao ID 2 por chapa 3 por cartao.
var retorno : Boolean;
begin
   QConveniado.Close;
   QConveniado.SQL.Clear;
   QConveniado.SQL.Add(' Select cartoes.conv_id, cartoes.cartao_id, cartoes.nome, ');
   QConveniado.SQL.Add(' cartoes.codigo, cartoes.digito, cartoes.liberado as cartlib, ');
   QConveniado.SQL.Add(' conveniados.liberado as convlib, conveniados.chapa, conveniados.empres_id ');
   QConveniado.SQL.Add(' from cartoes ');
   QConveniado.SQL.Add(' join conveniados on conveniados.conv_id = cartoes.conv_id ');
   QConveniado.SQL.Add(' where conveniados.apagado <> "S" and cartoes.apagado <> "S" ');
   if Trim(EdEmpres_id.Text) <> '' then
      QConveniado.SQL.Add(' and conveniados.empres_id = '+EdEmpres_id.Text)
   else
     if informar_emp then begin
         EdEmpres_id.SetFocus;
         raise Exception.Create('Selecione a empresa.');
      end;
   case Tipo of
   1 : QConveniado.SQL.Add(' and cartoes.cartao_id = '+FPesqConv.QConveniadoscartao_id.AsString);
   2 : begin
         if (Trim(EdEmpres_id.Text) = '') then begin
             EdEmpres_id.SetFocus;
             raise Exception.Create('Selecione a empresa.'+#13+'Empresa obrigat�ria para pesquisa por chapa.');
          end;
          QConveniado.SQL.Add(' and cartoes.titular = "S" ');
          QConveniado.SQL.Add(' and conveniados.chapa = '+EdChapa.Text);
       end;
   3 : begin
          if Length(EdCartao.Text) = 11 then begin
             QConveniado.SQL.Add(' and ( ( cartoes.codigo = '+Copy(EdCartao.Text,1,9));
             QConveniado.SQL.Add(' and cartoes.digito = '+Copy(EdCartao.Text,10,2)+')');
             QConveniado.SQL.Add(' or ');
          end
          else
             QConveniado.SQL.Add(' and (');
          QConveniado.SQL.Add(' cartoes.codcartimp = "'+EdCartao.Text+'" )');
       end;
   end;
   QConveniado.SQL.Add(' order by cartoes.titular desc ');
   QConveniado.Sql.Text;
   QConveniado.Open;
   if ((QConveniado.RecordCount > 1) and (Tipo in [1,3])) then begin // teste para verificar se existe mais de um cartao com o mesmo codcartimp.
      EdNome.Text  := EdCartao.Text;
      ButPesqConv.Click;
      Exit;
   end;
   if not QConveniado.IsEmpty then begin
      EdEmpres_id.Text := QConveniadoEMPRES_ID.AsString;
      EdNome.Text      := QConveniadoNOME.AsString;
      EdChapa.Text     := IntToStr(Trunc(QConveniadoCHAPA.AsFloat));
      EdCartao.Text    := QConveniadoCODIGO.AsString+FormatFloat('00',QConveniadoDIGITO.AsFloat);
      EDData.SetFocus;
      if rdgUsaWebService.ItemIndex=0 then
        retorno := GetWPegaAutorSoap(False,Path_WebService).ValidarFormasPgto(DBEmpresa.KeyValue,QCredenciadosCODACESSO.AsString)
      else
        retorno := False;
      DBFormapgto.Enabled:= retorno;
   end
   else begin
      QConveniado.Close;
      ShowMessage('Nenhum cart�o encontrado.');
      Case Tipo Of
         1 : EdNome.SetFocus;
         2 : EdChapa.SetFocus;
         3 : EdCartao.SetFocus;
      end;
   end;
end;


Procedure TFLancamentos.ListaAutors(retorno:string);
var i : Integer; autor : string;
begin
Retorno := Copy(Retorno,Pos('=',retorno)+1,length(retorno));
i := 0;
while Retorno <> '' do begin
  System.Inc(i);
  autor := copy(Retorno,1,Pos('#',Retorno)-1);
  Autors[i].Autor_id := Copy(autor,1,Length(autor)-2);
  Autors[i].dig      := Copy(autor,Length(autor)-1,2);
  Retorno := StringReplace(Retorno,autor+'#','',[]);
end;
end;

procedure TFLancamentos.rdgUsaWebServiceClick(Sender: TObject);
begin
  inherited;
  DBFormapgto.Enabled := (rdgUsaWebService.ItemIndex=0);
end;

end.
